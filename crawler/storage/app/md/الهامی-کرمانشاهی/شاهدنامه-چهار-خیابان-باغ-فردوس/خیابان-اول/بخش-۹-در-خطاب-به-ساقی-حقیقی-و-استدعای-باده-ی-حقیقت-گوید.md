---
title: >-
    بخش ۹ - در خطاب به ساقی حقیقی و استدعای باده ی حقیقت گوید
---
# بخش ۹ - در خطاب به ساقی حقیقی و استدعای باده ی حقیقت گوید

<div class="b" id="bn1"><div class="m1"><p>بده ساقی آن جام یاقوت نام</p></div>
<div class="m2"><p>که چونان ندیده است جمشید جام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>درافکن به بلور یاقوت ناب</p></div>
<div class="m2"><p>به جام هلالی بیار آفتاب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ازآن می غم از خاطرم ساز دور</p></div>
<div class="m2"><p>که سرهای عشاق از آن پر زشور</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خرد سوز مردان صاحب نظر</p></div>
<div class="m2"><p>جنون بخش رندان بی پا و سر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از آن می که جان در سرود آورد</p></div>
<div class="m2"><p>جم و جام بر وی درود آورد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بهای یکی جرعه زان سرخ می</p></div>
<div class="m2"><p>پر از لعل دیهیم کاووس کی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز لعل بتان چاشنی خیزتر</p></div>
<div class="m2"><p>زوصل حبیبان دل انگیزتر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آیا میگسارنده ساقی دمی</p></div>
<div class="m2"><p>مرا گر رها خواهی از هر غمی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به جان و سر ساقی سلسبیل</p></div>
<div class="m2"><p>دمادم به من می تو بنما سبیل</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>که می خوردن هرکه او راست دوست</p></div>
<div class="m2"><p>به طاق دو مردانه ابروی اوست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>همان طاق ابرو که جان آفرین</p></div>
<div class="m2"><p>بنا کرد از آن طاق محراب دین</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بیاور مرا ساقیا جام می</p></div>
<div class="m2"><p>دمادم به یاد وی و نام وی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چه جام؟ از خدا پر می باقیا</p></div>
<div class="m2"><p>امیر غدیر خمش ساقیا</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چه جامی؟ که خورشید از آن منجلی</p></div>
<div class="m2"><p>پر از باده ی صاف مهر علی (ع)</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>از آن باده حق داده تاج رضا</p></div>
<div class="m2"><p>به مستان میخانه ی مرتضی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>به موی تو ساقی که بامهر شاه</p></div>
<div class="m2"><p>کم از پر کاهست کوه گناه</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چه جز مهر او در دل پاک نیست</p></div>
<div class="m2"><p>گنه پیشه را از گنه باک نیست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بده می که حق راست فضل عظیم</p></div>
<div class="m2"><p>کف ساقی حوض کوثر کریم</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>نخواهد که مستان او شرمسار</p></div>
<div class="m2"><p>شوند از گناهان به روز شمار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بده ساقیا ساغری زان شراب</p></div>
<div class="m2"><p>که تا نقش هستی بشویم به آب</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>سوی عالم جان و دل بگذرم</p></div>
<div class="m2"><p>ازین کشور آب و گل برپرم</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>دم از مهر ساقی کوثر زنم</p></div>
<div class="m2"><p>در آن بزم مستانه ساغر زنم</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>خوشا وقت رند صبوحی زده</p></div>
<div class="m2"><p>خوشا حال مست در میکده</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>مرامی به جام ای بت دل بر آر</p></div>
<div class="m2"><p>بن شاخ اندوهم از گل بی آر</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>دمی تا بود زندگانی مرا</p></div>
<div class="m2"><p>بیفزا به می شادمانی مرا</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>که دنیا نپاید به کس پایدار</p></div>
<div class="m2"><p>نماند کسی زنده جز کردگار</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بده می که دیگر نیارم به یاد</p></div>
<div class="m2"><p>که چون شد بساط سلیمان به باد</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بیا ساقی ای کام بخش دلم</p></div>
<div class="m2"><p>چراغ شبستان مه محفلم</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>چو لعل خود از طبع گوهر فشان</p></div>
<div class="m2"><p>مرا رشته های گهر برفشان</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>که تا زین نهم برسمند گستری</p></div>
<div class="m2"><p>سبک پویه رخش ستایشگری</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>بتازم چنان کاندر آورد من</p></div>
<div class="m2"><p>نگردد سخن گستری مرد من</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>بتازم بدانگونه تیغ درود</p></div>
<div class="m2"><p>که ترک سپهرم سرآرد فرود</p></div></div>
<div class="n" id="bn33"><p>مرحوم (الهامی ) توانمندانه باغ فردوس خود را چهار بخش – حرکت شهادت ح امام حسین (ع) و یاران او – اسارت اهلبیت (ع)و قیام و خونخواهی مختار تقسیم و تنظیم کرده است.</p></div>