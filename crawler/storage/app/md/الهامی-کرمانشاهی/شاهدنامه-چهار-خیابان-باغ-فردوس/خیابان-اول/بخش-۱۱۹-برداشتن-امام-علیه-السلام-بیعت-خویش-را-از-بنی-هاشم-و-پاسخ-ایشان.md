---
title: >-
    بخش ۱۱۹ - برداشتن امام علیه السلام بیعت خویش را از بنی هاشم و پاسخ ایشان
---
# بخش ۱۱۹ - برداشتن امام علیه السلام بیعت خویش را از بنی هاشم و پاسخ ایشان

<div class="b" id="bn1"><div class="m1"><p>رها کردم از بند پیمانتان</p></div>
<div class="m2"><p>درود خدا برتن و جانتان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شما هم سوی منزل خویشتن</p></div>
<div class="m2"><p>بگیرید ره زین بزرگ انجمن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از آن سروران شبل شیر خدای</p></div>
<div class="m2"><p>سپهدار عباس رزم آزمای</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به پوزش چنین گفت با شه سخن</p></div>
<div class="m2"><p>که ای خاک پای تو دیهیم من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مرا بسته عشقت به یک تار موی</p></div>
<div class="m2"><p>که بربسته شد راهم از چار سوی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کهین بنده در پیش تخت توام</p></div>
<div class="m2"><p>به زنهار بیدار بخت توام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>من این عشق پاک از تو آموختم</p></div>
<div class="m2"><p>بربود تو بود خود سوختم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مران با وفا بنده ای را چو من</p></div>
<div class="m2"><p>مکن سرو یازنده را از چمن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>پس از آن جهانجوی پاکیزه خوی</p></div>
<div class="m2"><p>بدانسان سرودند اخوان اوی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سپس با یلان عقیلی نژاد</p></div>
<div class="m2"><p>بفرمود شه کای جوانان راد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز کردار مسلم به هر دوجهان</p></div>
<div class="m2"><p>شما سرفرازید و روشن روان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ازیدر هم اکنون سپارید گام</p></div>
<div class="m2"><p>سوی تربت پاک خیرالانام</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بگفتد: کای داور اولیا</p></div>
<div class="m2"><p>گرانمایه سبط شه انبیا</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>پس ازتو نخواهیم پاینده گی</p></div>
<div class="m2"><p>که نفرین سزد بر چنین زنده گی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>پی یاری ات تا سر آید زمان</p></div>
<div class="m2"><p>همه بسته داریم محکم میان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>نه ماییم از دوده ی شیر حق؟</p></div>
<div class="m2"><p>که بازوی دین بود و شمشیر حق؟</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>پسندی که بردوده ی پاک ننگ</p></div>
<div class="m2"><p>زما آید ای شاه پیروز جنگ</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گرفتیم عشق تو ای رهنما</p></div>
<div class="m2"><p>نبسته است از شش جهت ره به ما</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چه گوییم درپاسخ خلق باز</p></div>
<div class="m2"><p>چو کردند ما را نکوهش طراز</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>که رفتید با داور خود حسین</p></div>
<div class="m2"><p>کزو بودتان فخر در عالمین</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چو پیش آمدش کارو رنجی بزرگ</p></div>
<div class="m2"><p>سپردید اورا به چنگال گرگ؟</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>زهمراهی اش روی بر گاشتید</p></div>
<div class="m2"><p>ورا زیر شمشیر بگذا شتید</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>چو اندر وفا دیدشان استوار</p></div>
<div class="m2"><p>بدیشان بخواند آفرین شهریار</p></div></div>