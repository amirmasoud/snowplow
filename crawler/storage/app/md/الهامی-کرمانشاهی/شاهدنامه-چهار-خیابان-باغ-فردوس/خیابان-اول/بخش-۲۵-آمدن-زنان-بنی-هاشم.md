---
title: >-
    بخش ۲۵ - آمدن زنان بنی هاشم
---
# بخش ۲۵ - آمدن زنان بنی هاشم

<div class="b" id="bn1"><div class="m1"><p>که ناگه رسیدند زار و نوان</p></div>
<div class="m2"><p>برشاه دین هاشمی بانوان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همه مویه ساز و همه موی کن</p></div>
<div class="m2"><p>همه دست غم برسر و سینه زن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شه آن بانوان را چو آنگونه دید</p></div>
<div class="m2"><p>همان آه و فغان ایشان شنید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بفرمود کای داغدیده زنان</p></div>
<div class="m2"><p>مباشید اینگونه برسر زنان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شکیب اندرین کار پیش آورید</p></div>
<div class="m2"><p>همه رو به مشکوی خویش آورید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به زنهار یزدان بمانید و بس</p></div>
<div class="m2"><p>که زنهار او به نه زنهار کس</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چولختی تسلی زغم دادشان</p></div>
<div class="m2"><p>سوی پرده ی خود فرستاد شان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>رسول خدا را زاهل حرم</p></div>
<div class="m2"><p>زنی بود در پرده بس محترم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به دوده گزین و به دانش تمام</p></div>
<div class="m2"><p>کزو بود خوشنود خیرالانام</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کجا ام سلمه ورا نام بود</p></div>
<div class="m2"><p>ازو شاه دین شاد و پدرام بود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چو بشنید شه دارد آهنگ راه</p></div>
<div class="m2"><p>سوی او خرامید با اشک و آه</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به شه گفت:کای پاک فرزند من</p></div>
<div class="m2"><p>امید دل آرزمند من</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>تو از مادر و جد خود یادگار</p></div>
<div class="m2"><p>به نزد منی اندرین روزگار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>به گیتی دراز پنج آل عبا (ع)</p></div>
<div class="m2"><p>تو ماندستی ازخسرو دین به جا</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>مسوزان دل من به نار فراق</p></div>
<div class="m2"><p>زیثرب مکن عزم مرز عراق</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>شنیدم ز پیغمبر دادگر</p></div>
<div class="m2"><p>درآندم که می داد ما را خبر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>که نوشد حسین آب تیغ بلا</p></div>
<div class="m2"><p>زبد خواه دین تشنه درکربلا</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>درین روزم آن ساعت آمد به یاد</p></div>
<div class="m2"><p>که آن ساعت اندر جهان خود مباد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>یکی بشنو ازمادرپیر پند</p></div>
<div class="m2"><p>ز یثرب زمین بار هجرت مبند</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>زقتل خود ای شاه آخر زمان</p></div>
<div class="m2"><p>به گردون مبر دود ازاین دودمان</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>شهش گفت: کای مادر محترم</p></div>
<div class="m2"><p>رسول خدا را گزین تر حرم</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>همی دانم آن مرز را کاندر آن</p></div>
<div class="m2"><p>شوم کشته ازکین بد گوهران</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>به یزدان که چرخ و زمین آفرید</p></div>
<div class="m2"><p>شهی چون رسول امین آفرید</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>که دانم ز مردان دین چند تن</p></div>
<div class="m2"><p>شود بی سر ازتیغ کین بهر من</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>وگر خواهی اینک نمایم تو را</p></div>
<div class="m2"><p>حجاب از نظر برگشایم تو را</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>پس آنگه به انگشت معجز نمای</p></div>
<div class="m2"><p>اشارت نمود آن شه پاک رای</p></div></div>