---
title: >-
    بخش ۱۱۴ - انجمن فرمودن امام(ع)با عمر بن سعد ملعون درمیان دو لشگر و چگونگی آن حال
---
# بخش ۱۱۴ - انجمن فرمودن امام(ع)با عمر بن سعد ملعون درمیان دو لشگر و چگونگی آن حال

<div class="b" id="bn1"><div class="m1"><p>شبی راد فرزند خیرالبشر</p></div>
<div class="m2"><p>هیونی فرستاد سوی عمر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که ای کوفیان را سرانجمن</p></div>
<div class="m2"><p>زلشگر بپیمای ره سوی من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بیا تا میان دو لشگر به هم</p></div>
<div class="m2"><p>نشینیم و گوییم ازبیش وکم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>فرستاد ه آمد به کردار باد</p></div>
<div class="m2"><p>پیام شهنشه به دژخیم داد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عمر اندر آن تیره شام سیاه</p></div>
<div class="m2"><p>سوی لشگر شاه پیمودراه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به همراه آن زنشت تیره روان</p></div>
<div class="m2"><p>تنی بیست ازکوفیان شد روان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پرستنده گان جای پرداختند</p></div>
<div class="m2"><p>میان دو صف مسند انداختند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شهنشاه و سالار کوفی سپاه</p></div>
<div class="m2"><p>نشستند باهم درآن جایگاه</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تو با این شناسایی ازدشمنی</p></div>
<div class="m2"><p>چرا با من این بد سگالی کنی؟</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مرایار شو تا خداوندگار</p></div>
<div class="m2"><p>تو را یارگردد به روز شمار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نه من پاک فرزندم پیغمبرم؟</p></div>
<div class="m2"><p>گل باغ شیر خدا حیدرم؟</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>زمن دست بردار تا زین دیار</p></div>
<div class="m2"><p>شوم سوی یثرب زمین رهسپار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>مکن زشت از جنگ من نام خود</p></div>
<div class="m2"><p>بیندیش لختی زانجام خود</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>به شه پاسخ آورد آن پر فساد</p></div>
<div class="m2"><p>که اندیشه دارم زابن زیاد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چو یار تو گردم –کند با شتاب</p></div>
<div class="m2"><p>مرا خانه ی زنده گانی خراب</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>کشد کودک و بسته گانم امیر</p></div>
<div class="m2"><p>کند دختران و زنانم اسیر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بدان بد گهر گفت دارای دین</p></div>
<div class="m2"><p>که اندیشه کن از جهان آفرین</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>هر آنچ از تو گیرند دینار و زر</p></div>
<div class="m2"><p>ببخشم تو را من از آن بیشتر</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>یکی خانه بخشم به یثرب زمین</p></div>
<div class="m2"><p>تو را دلنشین همچو خلد برین</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بدین سوی دارمت پاس از بدی</p></div>
<div class="m2"><p>بدانسوی از کیفر ایزدی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>عمر گفت: ازمیر کوفه دیار</p></div>
<div class="m2"><p>فزون دارم اندیشه ای شهریار</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چو دید آن خداوند گیتی پناه</p></div>
<div class="m2"><p>که بدخواه را برده شیطان ز راه</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بدو گفت: کای تیره مغز پلید</p></div>
<div class="m2"><p>چنان خواهم از آنکه جان آفرین</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>که در بستر خواب بی سر شوی</p></div>
<div class="m2"><p>دمی درجهان شادمان نغنوی</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>به قتلم ازآن کرده ای سخت پی</p></div>
<div class="m2"><p>که شاید یزیدت دهد ملک ری</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>زخشم خدا برتو آید نهیب</p></div>
<div class="m2"><p>هم از گندم ری شوی بی نصیب</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>چنین گفت با داور دین عمر</p></div>
<div class="m2"><p>گر، از گندم ری نبینم ثمر</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>زکشت وی ام خوشه ی جو بس است</p></div>
<div class="m2"><p>که این آرزو در دل هر کس است</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>چو گفت این، شهنشاه ازو روی تافت</p></div>
<div class="m2"><p>عمر هم به لشگرگه خود شتافت</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>از این مجلس شاه دین با عمر</p></div>
<div class="m2"><p>چو فرزند مرجانه شد با خبر</p></div></div>