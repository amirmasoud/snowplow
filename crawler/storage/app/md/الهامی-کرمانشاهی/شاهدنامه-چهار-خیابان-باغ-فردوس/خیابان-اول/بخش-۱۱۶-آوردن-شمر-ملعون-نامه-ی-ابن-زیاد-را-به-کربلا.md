---
title: >-
    بخش ۱۱۶ - آوردن شمر ملعون نامه ی ابن زیاد را به کربلا
---
# بخش ۱۱۶ - آوردن شمر ملعون نامه ی ابن زیاد را به کربلا

<div class="b" id="bn1"><div class="m1"><p>بد اختر برآشفت و با شمرگفت</p></div>
<div class="m2"><p>که ای اهرمن آمده با تو جفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو این آتش فتنه افرختی</p></div>
<div class="m2"><p>که تا حشر از آن عالمی سوختی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به نام من آوردی این ننگ را</p></div>
<div class="m2"><p>نهشتی به صلح آورم جنگ را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کجا سبط فرخنده نام رسول</p></div>
<div class="m2"><p>کند بیعت شاه شامی قبول؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>میان هر که با او پی جنگ بست</p></div>
<div class="m2"><p>دل احمد و مرتضی راشکست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بدو گفت شم تبه روزگار</p></div>
<div class="m2"><p>همان دون و دد فطرت نابکار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گرت باشد اندیشه و واهمه</p></div>
<div class="m2"><p>ز رزم گزین زاده ی فاطمه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سپهداری این سپاه گشن</p></div>
<div class="m2"><p>به من واگذار و برو –زی وطن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بدو گفت اهریمن از روی طیش</p></div>
<div class="m2"><p>که جز من نشاید سپهدار جیش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به ناپاکرایی من از تو –برم</p></div>
<div class="m2"><p>به کین گستری کی ز تو کمترم؟</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خود این زشت کردار دارم قبول</p></div>
<div class="m2"><p>کنم رزم با نور چشم رسول</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بگفت این و با لشگر کینه جوی</p></div>
<div class="m2"><p>سوی خرگه شاه بنهاد روی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>خروشید کوس و بلرزید دشت</p></div>
<div class="m2"><p>غبار زمین ز آسمان درگذشت</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>سر بانوان خواهر شهریار</p></div>
<div class="m2"><p>چو بشنید غوغای اسب و سوار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ز برج سراپرده چون آفتاب</p></div>
<div class="m2"><p>خرامید سوی شه کامیاب</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بدید آن شهنشاه جن و بشر</p></div>
<div class="m2"><p>به زانو نهاده سر تا جور</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>غنوده است و عباس نزدش به پای</p></div>
<div class="m2"><p>چو حیدر به نزد رسول خدای</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>همی گفت زار وهمی ریخت آب</p></div>
<div class="m2"><p>ز مژگان به رخسار چون آفتاب</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>که ای بخت خوابیده بیدار شو</p></div>
<div class="m2"><p>هیاهوی رزم آوران راشنو</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>تو بخت منی چند مانی به خواب؟</p></div>
<div class="m2"><p>یکی وارهان مر مرا زاضطراب</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بود فتنه بیدار و تو گرم خواب</p></div>
<div class="m2"><p>یکی سر بر آر ای شه کامیاب</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ز افغان خواهر سر از خواب ناز</p></div>
<div class="m2"><p>برآورد دارای مرز حجاز</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بدو گفت کای پرده گی خواهرم</p></div>
<div class="m2"><p>به جا مانده از مهربان مادرم</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>کنون دید چشم روانم به خواب</p></div>
<div class="m2"><p>رخ پاک پیغمبر و بوتراب</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>حسن نیز با مهربان مادرم</p></div>
<div class="m2"><p>که پرورد مانند جان دربرم</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>مرا گفت پیغمبر بی قرین</p></div>
<div class="m2"><p>که مهمان مایی به خلد برین</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>چو بانو چنین از برادر شنید</p></div>
<div class="m2"><p>سرشکش به سیمای سیمین چکید</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>به سر بر خورشان همی خاک ریخت</p></div>
<div class="m2"><p>به تابنده مه عقد پروین گسیخت</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بدو گفت دارای پیروزمند</p></div>
<div class="m2"><p>که ای غمزده خواهر مستمند</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>ره ناله از دل مفرمای باز</p></div>
<div class="m2"><p>به درد و غم اندر-بسوز و بساز</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>صبوری بجوی از خدای جهان</p></div>
<div class="m2"><p>که باشد دراین پرده رازی نهان</p></div></div>