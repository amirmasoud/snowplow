---
title: >-
    بخش ۲۹ - درستودن ذوالجناح و سوار شدن امام برآن و بیرون رفتن از مدینه به عراق
---
# بخش ۲۹ - درستودن ذوالجناح و سوار شدن امام برآن و بیرون رفتن از مدینه به عراق

<div class="b" id="bn1"><div class="m1"><p>بدش یادگار آن خداوند دین</p></div>
<div class="m2"><p>یکی باره ازسید المرسلین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جهان تنگ درپیش یک گام او</p></div>
<div class="m2"><p>براق شهادت زحق نام او</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یکی گوی زر قرص خور در دمش</p></div>
<div class="m2"><p>یکی نعل سیمین هلال ازسمش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>فتاده زیک سم آن تیز تک</p></div>
<div class="m2"><p>مه نو گه دو به بام فلک</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شدی آن همایون تک رهنمون</p></div>
<div class="m2"><p>به یک جنبش ازهر دوگیتی برون</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نمودی سوار ار برای شنا</p></div>
<div class="m2"><p>رکاب ار به پهلوی او آشنا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به کیهان ندیدیش بی شک و ریب</p></div>
<div class="m2"><p>مگر دربیابان و میدان غیب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>همین بس که آن باره ی سرفراز</p></div>
<div class="m2"><p>بدی اسب پیغمبر بی نیاز</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ندیده گه تک ستاره خوی اش</p></div>
<div class="m2"><p>ندیده است چشمی غبار پی اش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>برآمد شهنشاه گردون براق</p></div>
<div class="m2"><p>بدان باره چون مصطفی بربراق</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نه شه گر عنانش نگه داشتی</p></div>
<div class="m2"><p>قدم زانسوی چرخ بگذاشتی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>زشوق سواری چنان ذوالجناح</p></div>
<div class="m2"><p>برون آمدش ازجوارح جناح</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چو بنشست بر پشت آن شاه دین</p></div>
<div class="m2"><p>رسید این ندا زآسمان بر زمین</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>که زین تو ای باره عرش خدای</p></div>
<div class="m2"><p>بود راکبت ایزد رهنمای</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>سمندی چنان را سواری چنین</p></div>
<div class="m2"><p>سزا باشد اربر نشیند به زین</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ازین به نیامد به کیهان سوار</p></div>
<div class="m2"><p>مگر احمد و صاحب ذوالفقار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>توگفتی چو آن شه به زین برنشست</p></div>
<div class="m2"><p>که یزدان به عرش برین برنشست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>سرودشان دویدندش ازچپ و راست</p></div>
<div class="m2"><p>زنه پرده ی چرخ فریاد خاست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>روان آن شهنشاه را بنده وار</p></div>
<div class="m2"><p>قضا و قدر از یمین و یسار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>نشستند زان پس یلان دلیر</p></div>
<div class="m2"><p>براسبان آهو تک خود چو شیر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>یکی کاروان شد ز یثرب روان</p></div>
<div class="m2"><p>خداوند دین میر آن کاروان</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>همه دشت بانگ روارو گرفت</p></div>
<div class="m2"><p>زماه علم چرخ پرتو گرفت</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بهین معنی صورت کاف و نون</p></div>
<div class="m2"><p>چو جان ازتن یثرب آمد برون</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چه جانی که جز پاک ذات خدای</p></div>
<div class="m2"><p>فدایش همه اهل هردوسرای</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>همه نوریان پیکری تابناک</p></div>
<div class="m2"><p>درآن پیکر آن پاک تن جان پاک</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>چنین جان زپیکر چو بیرون شود</p></div>
<div class="m2"><p>همه آفرینش دگرگون شود</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>چه آید ازآن تن که جانیش نیست</p></div>
<div class="m2"><p>تو بیجان تنی هیچ دیدی که زیست؟</p></div></div>