---
title: >-
    بخش ۶۲ - ذکر شهادت شاهزاده احمد بن حسن علیه السلام
---
# بخش ۶۲ - ذکر شهادت شاهزاده احمد بن حسن علیه السلام

<div class="b" id="bn1"><div class="m1"><p>که شهزاده ی راد و هشیار بود</p></div>
<div class="m2"><p>به دین و به دانش پدر وار بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ده وشش گذشته بدو سالیان</p></div>
<div class="m2"><p>پی خدمت عم کمر بر میان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پس از مرگ قاسم به نزدیک شاه</p></div>
<div class="m2"><p>بیامد بگفت ای شه دین پناه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مراهم بده رخصت کار زار</p></div>
<div class="m2"><p>کزین نابکاران برآرم دمار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به پایت فشانم سرو جان خویش</p></div>
<div class="m2"><p>بپیوندم آنگه به یاران خویش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شهنشه بدو گفت ازمن مخواه</p></div>
<div class="m2"><p>که بفرستمت سوی این رزمگاه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تو از رفته گان یادگار منی</p></div>
<div class="m2"><p>شکیب دل بی قرار منی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>برشاه بس لایه بسیار کرد</p></div>
<div class="m2"><p>که راضیش بر اذن پیگار کرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نخستین جوان رفت سوی حرم</p></div>
<div class="m2"><p>پس آنگه به میدان کین زد علم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>رجز خواند وبردشمنان حمله کرد</p></div>
<div class="m2"><p>در آن حمله افکند هشتاد مرد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از آن پس همی خواست کز رزمگاه</p></div>
<div class="m2"><p>بیاید ببوسد سم اسب شاه</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گرفتند گردش سواران جنگ</p></div>
<div class="m2"><p>گشادند بازو به تیغ و خدنگ</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>جوان بار دیگر بدیشان بتاخت</p></div>
<div class="m2"><p>سبک دست و تیغ دلیری فراخت</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>همی بر خروشید و زد بر سپاه</p></div>
<div class="m2"><p>چو سوزنده آتش که افتد به کاه</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>زهم رشته ی عمر مردان گسیخت</p></div>
<div class="m2"><p>به خون برادر بسی خون بریخت</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>نه بیم از سنان سوارانش بود</p></div>
<div class="m2"><p>نه باکی ز خنجر گذارانش بود</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>به شمشیر از آن فرقه ی نابکار</p></div>
<div class="m2"><p>بیفکند پنجاه تن نامدار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بیامد بر عم فرخنده نام</p></div>
<div class="m2"><p>بدو گفت کای سبط خیر الانام</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>مراتشنگی برده از کار سخت</p></div>
<div class="m2"><p>بدانسان که لرزم چو شاخ درخت</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>رسد گر یکی قطره آبم به کام</p></div>
<div class="m2"><p>سپه را به هم در نوردم تمام</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>شهنشه به رویش همی بنگریست</p></div>
<div class="m2"><p>نبودش چو آبی چو باران گریست</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بدو گفت از تشنه کامی شکیب</p></div>
<div class="m2"><p>بورز و نگه دار پا در رکیب</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>برو سوی میدان که شوی بتول</p></div>
<div class="m2"><p>دهد آبت ازن چشمه سار رسول (ص)</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ببوسید فرخ جوان دست شاه</p></div>
<div class="m2"><p>دگر بار و آمد سوی رزمگاه</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بزد خویش را برسپاه گشن</p></div>
<div class="m2"><p>بیفکند زا گمرهان شصت تن</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>زبس بر تنش زخم کاری رسید</p></div>
<div class="m2"><p>نگون ز اسب شد از جهان پا کشید</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>شهنشاه زی پهنه یکران بماند</p></div>
<div class="m2"><p>بسی کشت و آن کشته را برنشاند</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بیاورد و بنهاد در خیمه گاه</p></div>
<div class="m2"><p>دریغ از چنان نامور پور شاه</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>جوانان بسی کشتی ای روزگار</p></div>
<div class="m2"><p>به رخ هر یکی چون شکفته بهار</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>خردمند آن کز تو بر تافت روی</p></div>
<div class="m2"><p>به دل نامدش از تو هیچ آرزو</p></div></div>