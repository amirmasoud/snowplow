---
title: >-
    بخش ۳۲ - کشتن پسر جناب حبیب ابن مظاهر حامل سر پدر را در دروازه ی مکه
---
# بخش ۳۲ - کشتن پسر جناب حبیب ابن مظاهر حامل سر پدر را در دروازه ی مکه

<div class="b" id="bn1"><div class="m1"><p>شنیدم یکی پور بودش حبیب</p></div>
<div class="m2"><p>که درکودکمی بدسعادت نصیب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به بطحا زمین اندرون جای داشت</p></div>
<div class="m2"><p>قدم روزی از شهر بیرون گذاشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>براو بر به ناگه سواری گذشت</p></div>
<div class="m2"><p>که تازان همی آمد از پهندشت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سری بربه فتراکش آویخته</p></div>
<div class="m2"><p>به خونش بر باره آمیخته</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو لختی بدان پاک سر بنگریست</p></div>
<div class="m2"><p>سرباب خود را بدید و گریست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یکی سنگ سخت اززمین برگرفت</p></div>
<div class="m2"><p>عنان سمند ستمگر گرفت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بدان سنگ بشکافت مغز سوار</p></div>
<div class="m2"><p>بیفکندش از باره ی راهوار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بیاورد از آن پس سرباب پیر</p></div>
<div class="m2"><p>بشستش به مشک و گلاب وعبیر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به خاکش نهان کرد و شد مویه گر</p></div>
<div class="m2"><p>همی تا که بد زنده بهر پدر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به فرخ حبیب از جهان آفرین</p></div>
<div class="m2"><p>زاندازه بیرون رساد آفرین</p></div></div>