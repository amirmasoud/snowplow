---
title: >-
    بخش ۱۳ - فرستادن عمرسعد سامر ازوی را به میدان
---
# بخش ۱۳ - فرستادن عمرسعد سامر ازوی را به میدان

<div class="b" id="bn1"><div class="m1"><p>زمیدان چو برگشت دارای دین</p></div>
<div class="m2"><p>عمر آن ستمکار پر خشم وکین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به لشکر درش مرد بودی سترگ</p></div>
<div class="m2"><p>که بگرفتی او بره از چنگ گرگ</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تن او بار و بی باک و خودکام بود</p></div>
<div class="m2"><p>پلیدی که خود سامرش نام برد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به او گفت زی پهنه بردار گام</p></div>
<div class="m2"><p>میان دلیران برافراز نام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو این بدگهر زان ستمگر شنید</p></div>
<div class="m2"><p>سراپا به پولاد شد ناپدید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نشست از بر باره ی تیزگام</p></div>
<div class="m2"><p>برآمد به میدان و برگفت نام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سر نیزه ی جانگسل کرد راست</p></div>
<div class="m2"><p>هماورد از لشگر شاه خواست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زهیرابن حسان یل رزمخواه</p></div>
<div class="m2"><p>خم آورد بالا بر چتر شاه</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به پوزش چنین گفت کای شهریار</p></div>
<div class="m2"><p>نبینی که در پهنه ی کارزار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>یکی اهرمن می خروشد همی</p></div>
<div class="m2"><p>که دریا ز بیمش نجوشد همی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>همی خواست از لشکر شاه مرد</p></div>
<div class="m2"><p>که با وی بگردد به دشت نبرد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مرا بخش دستوری جنگ اوی</p></div>
<div class="m2"><p>که بیرون کنم نیزه از چنگ اوی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به خون درکشم یال شیریش را</p></div>
<div class="m2"><p>کنم خرد پشت دلیرش را</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بدو داد دستوری جنگ شاه</p></div>
<div class="m2"><p>که بینند رزمش دو رویه سپاه</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>دلاور به زین تکاور نشست</p></div>
<div class="m2"><p>یکی نیزه ی شست بازش به دست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بیامد به نزد هماورد و گفت</p></div>
<div class="m2"><p>که با جانت امروز شد مرگ جفت</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چو سامر بدید آن بر مرد جنگ</p></div>
<div class="m2"><p>همان نیزه ی جانشکارش به چنگ</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بدو گفت کای پر دل سرفراز</p></div>
<div class="m2"><p>مشو غره بر این سنان دراز</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>تو مردی وگردی و اسب افکنی</p></div>
<div class="m2"><p>کماندار و بی باک و خنجر زنی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>دریغا که مغزت نباشد به سر</p></div>
<div class="m2"><p>خرد نیست با این شکوه و هنر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>وگرنه چرا دست از جان و مال</p></div>
<div class="m2"><p>کشی ای یل پر دل بی همال</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>عبث در ره شاه یثرب دیار</p></div>
<div class="m2"><p>به کشتن چرا می دهی خویش زار</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>یزید و معاویه را باش یار</p></div>
<div class="m2"><p>که گردی زمال جهان کامگار</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بدو پر هنر گفت کای کینه جوی</p></div>
<div class="m2"><p>چرا آب شرمت نیاید به جوی</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>که گوید زشاهنشه رهنمون</p></div>
<div class="m2"><p>بکش دست از بهر دنیای دون</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>همی خواست سامر که گوید سخن</p></div>
<div class="m2"><p>دگر باره با مرد شمشیر زن</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>نهشتش جوان برگشاید زبان</p></div>
<div class="m2"><p>بزد نیزه اش راست اندر دهان</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>برآمد سنان از پس گردنش</p></div>
<div class="m2"><p>بشد شادمان دوزخ از مردنش</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>چو شد کشته سامر به دشت نبرد</p></div>
<div class="m2"><p>بغرید چون شیر کوشنده مرد</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>که هر کس نداند نژاد مرا</p></div>
<div class="m2"><p>همان توده ی پاکزاد مرا</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>زچهرم چو شاداب رخ مام کرد</p></div>
<div class="m2"><p>زهیر بن حسان مرانام کرد</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>یکی شیر مرد اسد گوهرم</p></div>
<div class="m2"><p>بلند آسمانی نکو اخترم</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>دلیر و جوانمردی آزاده ام</p></div>
<div class="m2"><p>زشیران شمشیر زن زاده ام</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>اگر هست مردی شتابد به جنگ</p></div>
<div class="m2"><p>به پای خود آید به کام نهنگ</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>چو لشکر بدیدند او را ز دور</p></div>
<div class="m2"><p>رمیدند از وی چو از شیر – گور</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>نپیچید زی رزم او کس عنان</p></div>
<div class="m2"><p>یلان را بلرزید تن چون سنان</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>از آن رو که نام آوران عرب</p></div>
<div class="m2"><p>دلیران شام و عراق وحلب</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>به پیگار بد خواه درجنگ ها</p></div>
<div class="m2"><p>از او دیده بودند آهنگ ها</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>عمر چون چنین دید برزد خروش</p></div>
<div class="m2"><p>که ای نامداران پولاد پوش</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>یلی نیست کورا به دام آورد؟</p></div>
<div class="m2"><p>رود سوی میدان و نام آورد</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>به لشکر یکی مرد رزم آزمای</p></div>
<div class="m2"><p>بدی دیو هنجار و ناپاک رای</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>بخواندند نصربن کعبش به نام</p></div>
<div class="m2"><p>نهنگ دمان را کشیدی به کام</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>مر او را بزرگان کوفه دیار</p></div>
<div class="m2"><p>به او بر شمردند با صد سوار</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>به رزم زهیر گزین بی درنگ</p></div>
<div class="m2"><p>دوانید هر کس به میدان جنگ</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>بگفتا هنرمند را ای دلیر</p></div>
<div class="m2"><p>که یال و برت هست مانند شیر</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>چه نازی بدین آب داده سنان</p></div>
<div class="m2"><p>هم اکنون ز رزمم بپیچی عنان</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>زهیر این چو بشنید از آن کینه ساز</p></div>
<div class="m2"><p>بدو گفت کای دیو نیرنگ باز</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>نگه کن بدین نیزه و چنگ من</p></div>
<div class="m2"><p>همین پهلوی جامه ی جنگ من</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>اگر دل نشد آب اندر برت</p></div>
<div class="m2"><p>ز پولاد هندی بود پیکرت</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>براین بود روباره نیرنگ باز</p></div>
<div class="m2"><p>که با شیر غژمان کند حیله ساز</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>به ناگاه زخمی زند تاکه مرد</p></div>
<div class="m2"><p>نگون گردد از باره ی رهنورد</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>بدانست آهنگ او را جوان</p></div>
<div class="m2"><p>به یک نیزه کردش به دوزخ روان</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>چو پردخته ی گیتی از تیره بخت</p></div>
<div class="m2"><p>زآهن برادرش پو شید رخت</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>ددی بود پرورده ازباب ومام</p></div>
<div class="m2"><p>که خود صالح بد گهر داشت نام</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>به سوک برادر همی مویه کرد</p></div>
<div class="m2"><p>بزد اسب و آمد به دشت نبرد</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>همان نیزه نام آور ارجمند</p></div>
<div class="m2"><p>به سوی پلید بد اختر فکند</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>بگشت از برزین مرآن بدگمان</p></div>
<div class="m2"><p>که گرداند از خویش زخم سنان</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>رسید آن چمان چرمه ی گرمپوی</p></div>
<div class="m2"><p>سوار از بر زین در آمد به روی</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>بیاویخت یک پای او با رکاب</p></div>
<div class="m2"><p>تکاور به پویه شده اندر او ریز ریز</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>چو این زاده ی نصربن کعب دید</p></div>
<div class="m2"><p>به مرگ سواران فغان برکشید</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>برانگیخت توسن چو باد سیاه</p></div>
<div class="m2"><p>بیامد سوی پهنه ی رزمگاه</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>نکرد ه سنان راست دشمن هنوز</p></div>
<div class="m2"><p>که مرد سرافراز گیتی فروز</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>به یک زخم آن نیزه ی بندبند</p></div>
<div class="m2"><p>نگون کردش از زین تازی سمند</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>چو اززین نگونسار شد آن پلید</p></div>
<div class="m2"><p>به دوزخ درون خویشتن را بدید</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>پس از او زهیر آن سرافراز مرد</p></div>
<div class="m2"><p>سبک بر پیاده سپه حمله کرد</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>بدان آبداده سنان بلند</p></div>
<div class="m2"><p>فزون کشت نام آور ارجمند</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>چو ازکشته چون پشته بنمود دشت</p></div>
<div class="m2"><p>دمان سوی میدان کین بازگشت</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>همی برخروشید چون شیر مست</p></div>
<div class="m2"><p>همان اژدها سان سنانش به دست</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>همی ویله کرد وهمی گفت مرد</p></div>
<div class="m2"><p>اگر هست اسب افکند در نبرد</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>چو لشکر بر و جوشن او به خون</p></div>
<div class="m2"><p>بدیدند آغشته و لعلگون</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>سر افکنده در پیش و ترسان شدند</p></div>
<div class="m2"><p>به جان زان دلاور هراسان شدند</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>کشیدند یکسر ز رزمش عنان</p></div>
<div class="m2"><p>زآسیب نوک زدوده سنان</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>تو گفتی سرنیزه اش مرگ بود</p></div>
<div class="m2"><p>که جان سواران زتن می ربود</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>هم آخر به فرمان بن سعد دون</p></div>
<div class="m2"><p>برانگیخت مردی به رزمش هیون</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>بدان نیزه او را ز زین برگرفت</p></div>
<div class="m2"><p>دو لشگر ازو ماند اندر شگفت</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>بیامد دگر مرد و شیر جوان</p></div>
<div class="m2"><p>به یک نیزه کردش به دوزخ روان</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>چنین تا به دوزخ روان کرد تفت</p></div>
<div class="m2"><p>بدان نیزه جنگی دو ده مرد وهفت</p></div></div>