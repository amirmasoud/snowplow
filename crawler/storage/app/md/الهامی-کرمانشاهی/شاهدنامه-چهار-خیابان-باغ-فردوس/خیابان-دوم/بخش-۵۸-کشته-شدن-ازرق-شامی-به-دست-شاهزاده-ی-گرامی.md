---
title: >-
    بخش ۵۸ - کشته شدن ازرق شامی به دست شاهزاده ی گرامی
---
# بخش ۵۸ - کشته شدن ازرق شامی به دست شاهزاده ی گرامی

<div class="b" id="bn1"><div class="m1"><p>همی گفت کز زاده ی بوتراب</p></div>
<div class="m2"><p>مرا خانه ی دودمان شد خراب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو کوهی ز آهن بر آمد به زین</p></div>
<div class="m2"><p>هیون تاخت در دشت کین خشمگین</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به آیینه ی روی فرزند شاه</p></div>
<div class="m2"><p>چو شد روبرو گفت آن کینه خواه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>که ای هاشمی کودک نامور</p></div>
<div class="m2"><p>بکشتی زمن چار نامی پسر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>که همتای هر یک به مردی نبود</p></div>
<div class="m2"><p>ز تاری سواران با کبر و خود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هم ایدون به خون درکشم پیکرت</p></div>
<div class="m2"><p>ز مرگت بسوزم دل مادرت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بغرید مردانه هاشم نژاد</p></div>
<div class="m2"><p>که ای بد گهر مردناپاکزاد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چه نالی تو بر چار فرزند خویش</p></div>
<div class="m2"><p>چه سوزی به داغ جگر بند خویش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>غم خویش خور سوک ایشان مدار</p></div>
<div class="m2"><p>که اینک سرآرم تو را روزگار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به ناگاه چشم خداوند دین</p></div>
<div class="m2"><p>به ازرق فتاد اندر آن دشت کین</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>که یکران به رزم جوان تاخته</p></div>
<div class="m2"><p>سنان دلیری برافراخته</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به پور برادر بترسید سخت</p></div>
<div class="m2"><p>فرو ریخت خون از مژه لخت لخت</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>برآورد گریان دو دست نیاز</p></div>
<div class="m2"><p>به درگاه داد آور چاره ساز</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بگفت ای خدای بی انباز من</p></div>
<div class="m2"><p>که هستی نیوشنده ی راز من</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>در این کارزارش یکی یار باش</p></div>
<div class="m2"><p>یتیم حسن (ع) را نگهدار باش</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بده چیره دستیش بردشمنا</p></div>
<div class="m2"><p>به مرگش مسوزان روان منا</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>خدایا ببخشا جوانیش را</p></div>
<div class="m2"><p>میاور به سر زندگانیش را</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>دراین گفتگو بود فرخنده شاه</p></div>
<div class="m2"><p>که ناگه خروش آمد از رزمگاه</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>سنان یلی کرد ازرق دراز</p></div>
<div class="m2"><p>سوی مینمه قاسم سرفراز</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گران کرد قاسم رکاب سمند</p></div>
<div class="m2"><p>سبک نیزه بر نیزه ی او فکند</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چو اندر میان طعن چندی گذشت</p></div>
<div class="m2"><p>رخ ازرق از خشم چون قیر گشت</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>سنانی بزد بر بر اسب مرد</p></div>
<div class="m2"><p>که ماند او پیاده به دشت نبرد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>یکی اسب زرینه بر گستوان</p></div>
<div class="m2"><p>شهنشه فرستاد بهر جوان</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>جوان زد بن نیزه را برزمین</p></div>
<div class="m2"><p>چو پران تذروی در آمد به زین</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>خروشان سوی ازرق آورد روی</p></div>
<div class="m2"><p>بدو حمله ور گشت پرخاشجوی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>به دست دویل شد دو تیغ آخته</p></div>
<div class="m2"><p>دو بازو به جنگ آمد افراخته</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>که از بیم شیر فلک باخت رنگ</p></div>
<div class="m2"><p>بیفکند بهرام خنجر زچنگ</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>چو بر تیغ بگشود چشم</p></div>
<div class="m2"><p>هم آورد بد گوهر آمد به خشم</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بگفت این پرند آور آبدار</p></div>
<div class="m2"><p>خود از کشته پور من است ای سوار</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>به زهر آب خورده است این تیغ تیز</p></div>
<div class="m2"><p>که با وی همی جست خواهی ستیز</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>بدو گفت شهزاده ی نامدار</p></div>
<div class="m2"><p>بدین سرفشان تیغ زهر آبدار</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>هم ایدون فرستمت سوی پسر</p></div>
<div class="m2"><p>که مهمانش گردد به دوزخ پدر</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>ازین تیغ نوشانمت زهر مرگ</p></div>
<div class="m2"><p>کفن سازمت آهنین ساز وبرگ</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>نبیند سرت زین سپس خود جنگ</p></div>
<div class="m2"><p>نه ترکش به کار آیدت نی خدنگ</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>بسایم چو افتد ز زین پیکرت</p></div>
<div class="m2"><p>به نعل سم باره آهن سرت</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>شنیدستم این ازسواران کار</p></div>
<div class="m2"><p>که هشیار مردی تو در کارزار</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>بسی از فنون نبرد آگهی</p></div>
<div class="m2"><p>کنون بینمت مغز ازهش تهی</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>ابرباره ناکرده تنگ استوار</p></div>
<div class="m2"><p>چرا تاختی سوی جنگ ای سوار</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>یکی درنگر تنگ بگسسته را</p></div>
<div class="m2"><p>ابر باره ستوار نابسته را</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>خم آورد بال ازرق بد گهر</p></div>
<div class="m2"><p>که برتنگ توسن گشاید نظر</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>ندادش مجال آسمان یلی</p></div>
<div class="m2"><p>بیفراشت بازو چو جدش علی (ع)</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>بزد تیغ و کرد از میانش دونیم</p></div>
<div class="m2"><p>سپه را همه گشت دل پر زبیم</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>بدو از جهان آفرین آفرین</p></div>
<div class="m2"><p>رسید از بلند آسمان بر زمین</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>شهنشه فرستاد وی را درود</p></div>
<div class="m2"><p>وزان زخم مردانه او را ستود</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>چو از زین فتاد آن دو نیمه سوار</p></div>
<div class="m2"><p>شد او را یله باره ی راهوار</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>بر آن نامور باره ی تیزگام</p></div>
<div class="m2"><p>زر آموده زین و ستام و لگام</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>گزین پور سبط رسول امین</p></div>
<div class="m2"><p>بزد آن بن نیزه را بر زمین</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>بجست از بر باره ی راهوار</p></div>
<div class="m2"><p>برآب اسب زرینه زین شد سوار</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>یدک ساخت اسب خود و سوی شاه</p></div>
<div class="m2"><p>به پیروزی آمد از آن رزمگاه</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>چو پیش آمد از باره گی شد فرمود</p></div>
<div class="m2"><p>دو رخ بر رکاب شهنشاه سود</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>ز رخ سودنش بررکاب هیون</p></div>
<div class="m2"><p>رکاب آهنین بود شد سیمگون</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>همی گفت زار ای شه نینوای</p></div>
<div class="m2"><p>جهان را خداوند فرمانروای</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>از این رزمگه سرفراز آمدم</p></div>
<div class="m2"><p>بکشتم بسی خصم وباز آمدم</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>چو ارزق دلیری فکندم به خاک</p></div>
<div class="m2"><p>نمودم برو، جوشنش چاک چاک</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>فکندم در این پهنه پیکرش را</p></div>
<div class="m2"><p>همان چار پور دلاورش را</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>به پاداش این خدمت ای شهریار</p></div>
<div class="m2"><p>یکی جرعه آبم بده خوشگوار</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>که از تشنگی در تنم تاب نیست</p></div>
<div class="m2"><p>بخواهم کنون مردن ار آب نیست</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>تو آب آفرین من چنین تشنه لب</p></div>
<div class="m2"><p>دهم جان دراین کارزار ای عجب</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>شهنشه چو دید آنهمه لابه اش</p></div>
<div class="m2"><p>ز مژگان به رخسار خونابه اش</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>سرتاجدار اندر افکند زیر</p></div>
<div class="m2"><p>زمین را نمود از سر شک آبگیر</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>بگفت ای زرویت دلم شاد کام</p></div>
<div class="m2"><p>نکو جستی امروز در جنگ نام</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>هزار آفرین بر دلیریت باد</p></div>
<div class="m2"><p>بدان پنجه و زور شیریت باد</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>روان منت برخی جان شود</p></div>
<div class="m2"><p>ز کار تو خشنود یزدان شود</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>چه سازم که در دست من آب نیست</p></div>
<div class="m2"><p>ز شرم تو در تن مرا تاب نیست</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>دم دیگرت ساقی سلسبیل</p></div>
<div class="m2"><p>به مینو کند آب کوثر سبیل</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>دم آخرین است لختی بچم</p></div>
<div class="m2"><p>به بدرود غمدیده گان حرم</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>کزین رزم دیگر نیایی تو باز</p></div>
<div class="m2"><p>نسازد کسی دیده سوی تو باز</p></div></div>