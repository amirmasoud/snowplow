---
title: >-
    بخش ۵۵ - عزم رزم فرمودن ح – قاسم و گفتگوی او با عروس
---
# بخش ۵۵ - عزم رزم فرمودن ح – قاسم و گفتگوی او با عروس

<div class="b" id="bn1"><div class="m1"><p>چو آمد به نزدیک پرده سرای</p></div>
<div class="m2"><p>برآمد زلشگر غو کوس و نای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یکی زان گشن لشگر آواز داد</p></div>
<div class="m2"><p>که هان ای شهنشاه حیدر نژاد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گرت هست مردی به میدان گمار</p></div>
<div class="m2"><p>وگرنه بیا خودسوی کارزار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو این ویله زان کینه جو اهرمن</p></div>
<div class="m2"><p>نیوشید داماد شمشیر زن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رگ هاشمی غیرت حیدری</p></div>
<div class="m2"><p>همان گوهر و فر نام آوری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نهشتش به خرگه رود ای فسوس</p></div>
<div class="m2"><p>رهاکرد ازدست دست عروس</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بدو گفت با ناله ی جان خراش</p></div>
<div class="m2"><p>که من رفتم اینگ تو پدرام باش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زمان وصال من آید به پای</p></div>
<div class="m2"><p>نبینی مرا جز به دیگر سرای</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اگر چه مرا دل پر از مهر تست</p></div>
<div class="m2"><p>دو بیننده ام روشن ازچهر تست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>همین بودم از دور گیتی هوس</p></div>
<div class="m2"><p>که از تو نمانم جدا یک نفس</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ولیکن چه سازم که چرخ کهن</p></div>
<div class="m2"><p>نگشته است یک لحظه برمیل من</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نبینی که بدخواه از شهریار</p></div>
<div class="m2"><p>همی مرد خواهد پی کارزار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>تو نک می پسندی بزرگان من</p></div>
<div class="m2"><p>بجویند رزم سپاه گشن</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بمانم من و کامرانی کنم</p></div>
<div class="m2"><p>پس از مرگشان زنده گانی کنم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>تویی گرچه مانند جان درتنم</p></div>
<div class="m2"><p>نکو باشد امروز جان دادنم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>در اینجا نه جای سرور من است</p></div>
<div class="m2"><p>بود ماتمم این نه سور من است</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چو رفتم ز دنیا به فردوس بر</p></div>
<div class="m2"><p>کنم تازه آیین سور دگر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>به خلوتگه خلد بنشانمت</p></div>
<div class="m2"><p>به سر برگلاب و گل افشانمت</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>نبینم به چیزی مگر روی تو</p></div>
<div class="m2"><p>نبویم مگر سنبل موی تو</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>دراین روز اگر جان نبازیم زار</p></div>
<div class="m2"><p>به قرب خدایی نیابیم بار</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>وصال خدا بهتر از وصل تست</p></div>
<div class="m2"><p>شنو تا چه گویم بیابش درست</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>تو و خویشتن رادر این کارزار</p></div>
<div class="m2"><p>ببخشم به دیدار پروردگار</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>روم خویشتن سوی شمشیر وتیر</p></div>
<div class="m2"><p>فرستم ترا سوی کوفه اسیر</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>سوی کوفه با کاروان تو من</p></div>
<div class="m2"><p>بیایم به سر گر نیایم به تن</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>کنم تا به غم درنماند دلت</p></div>
<div class="m2"><p>سرخویش آویزه ی محملت</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ز دنبالت ای ماه خرگاه عشق</p></div>
<div class="m2"><p>به سر می سپارم همی راه عشق</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>زداماد ناشاد چون نو عروس</p></div>
<div class="m2"><p>شنید این به سرزد دودست فسوس</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>به زاری بیاویخت بردامنش</p></div>
<div class="m2"><p>نگه داشت بر جای از رفتنش</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بگفت ای سرافراز جفت جوان</p></div>
<div class="m2"><p>مساز از جدایی مر خسته جان</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>نه اینست در مهر آیین و خوی</p></div>
<div class="m2"><p>که رخ نانموده بپوشند روی</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>پسر عم به تیغ غمم خون مریز</p></div>
<div class="m2"><p>سوی تیغ وشمشیر مشتاب تیز</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>بگفتم مگر غمگسارم تویی</p></div>
<div class="m2"><p>پناه از بد روزگارم تویی</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>بگویم ترا گر غمی دارمی</p></div>
<div class="m2"><p>همه راز دل با تو بگذارمی</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>ز بی مهری تو نبد باورم</p></div>
<div class="m2"><p>که بر غم فزایی غم دیگرم</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>پدر دست من زان به دستت سپرد</p></div>
<div class="m2"><p>که دشمن نیارد به من دستبرد</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>چو باشد زتو سایه ای برسرم</p></div>
<div class="m2"><p>بماند به جا یاره و چادرم</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>تو ا زچه زمن روی برکاشتی</p></div>
<div class="m2"><p>چنین بی کس و خوار بگذاشتی</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>مرا می گذاری دراین دامگاه</p></div>
<div class="m2"><p>که خود در جنان سازی آرامگاه</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>ز آزاده گان این سزاوار نیست</p></div>
<div class="m2"><p>سزاوار یار وفادار نیست</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>چو بشنید شهزاده گفتار جفت</p></div>
<div class="m2"><p>دمی زار بگریست و آنگاه گفت</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>که ای از غمت سوخته جان من</p></div>
<div class="m2"><p>ازین بیش بر جانم آتش مزن</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>مرو راه بر خویش از غم نهیب</p></div>
<div class="m2"><p>بخواه و ز دادار کیهان شکیب</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>ازین رزم جستن مرا چاره نیست</p></div>
<div class="m2"><p>ز دشمن دگر تاب بیغاره نیست</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>پدر چون به دست منت داد دست</p></div>
<div class="m2"><p>به مهر شهادت به من عقد بست</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>کنون می روم تا که گردم شهید</p></div>
<div class="m2"><p>به توفیق یزدان وبخت سعید</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>دمی دیگرم روی و مو غرق خون</p></div>
<div class="m2"><p>ببینی به خاک اوفتاده نگون</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>ندانم از آن پس چه پیش آوری</p></div>
<div class="m2"><p>شکیبا شوی یا که غم پروری</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>زگفت جوان جفت بگریست زار</p></div>
<div class="m2"><p>به وی گفت با دیده ی اشکبار</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>اگر جست خواهی به ناچار جنگ</p></div>
<div class="m2"><p>زخون گرددت موی ورو لاله رنگ</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>به تو نا بمانم بمویم همی</p></div>
<div class="m2"><p>دورخ رابه خونابه شویم همی</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>نرانم مگر نام تو بر زبان</p></div>
<div class="m2"><p>نمانم مگر با غمت شادمان</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>دراین گیتی اینم زسوک تو کار</p></div>
<div class="m2"><p>بفرما چو آیم به روز شمار</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>چه باشد نشان بهر بشناختن؟</p></div>
<div class="m2"><p>مرا از تو در آن بزرگ انجمن؟</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>که آنجا بدان باز جویم تو را؟</p></div>
<div class="m2"><p>همه هر چه دیدم بگویم ترا؟</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>بزد دست شهزاده ی راستین</p></div>
<div class="m2"><p>جدا کرد یک پاره از آستین</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>بگفت اندر آن پهنه ی پر هراس</p></div>
<div class="m2"><p>بدین پاره ی آستینم شناس</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>کنونم بهل تا روم سوی جنگ</p></div>
<div class="m2"><p>که از من برفته است تاب درنگ</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>عروس سیه روز ناگشته شاد</p></div>
<div class="m2"><p>به ناچار دامانش از کف نهاد</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>چو باز دمان از بر او جوان</p></div>
<div class="m2"><p>برون آمد و شد بر شه، روان</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>بیفکند خوند را درآغوش شاه</p></div>
<div class="m2"><p>بگفت ای درت عرش را سجده گاه</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>ممانم ازین بیش در انتظار</p></div>
<div class="m2"><p>زیاران بگذشته دورم مدار</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>چو امر برادرت آمد به جای</p></div>
<div class="m2"><p>مراسوی خلد برین ره نمای</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>شهنشاه داماد را خواند پیش</p></div>
<div class="m2"><p>بدو داد برنده شمشیرخویش</p></div></div>