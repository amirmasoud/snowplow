---
title: >-
    بخش ۴۲ - ذکر مبارزت وشهادت جعفر و عبدالرحمن و موسی وعلی پسران عقیل بن ابیطالب علیه السلام
---
# بخش ۴۲ - ذکر مبارزت وشهادت جعفر و عبدالرحمن و موسی وعلی پسران عقیل بن ابیطالب علیه السلام

<div class="b" id="bn1"><div class="m1"><p>چوفرخ گهر جعفر بن عقیل</p></div>
<div class="m2"><p>بدید آن که پور برادر قتیل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زخشم آمدش دل چو دریابه جوش</p></div>
<div class="m2"><p>زداغ برادر پسر زد خروش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به فرمان شه سوی پیگار تاخت</p></div>
<div class="m2"><p>همه رزمگه پر تن کشته ساخت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سوار و پیاده بسی کرد پست</p></div>
<div class="m2"><p>سپس خود هم ازاین جهان رخت بست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>برادر یکی عبدرحمن به نام</p></div>
<div class="m2"><p>بد اورا که بر چرخ گردی لگام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پس از وی به خونخواهی آمد برون</p></div>
<div class="m2"><p>همی تیغ و بازو نمود آزمون</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از آن ناکسان چون گروهی بکشت</p></div>
<div class="m2"><p>خود افتاد از پا ز زخم درشت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دوکوشنده شیراز کنام یلی</p></div>
<div class="m2"><p>یکی بود موسی و دیگر علی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>که بدشان عقیل سرافراز باب</p></div>
<div class="m2"><p>نمودند در جنگ جستن شتاب</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چو کردند رنگین به خون تیغ و چنگ</p></div>
<div class="m2"><p>به گیتی نکردند لختی درنگ</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شتابان برفتند سوی بهشت</p></div>
<div class="m2"><p>زبیداد آن لشگر بدسرشت</p></div></div>