---
title: >-
    بخش ۷۸ - مبارزت ح ابو الفضل علیه السلام با دست چپ
---
# بخش ۷۸ - مبارزت ح ابو الفضل علیه السلام با دست چپ

<div class="b" id="bn1"><div class="m1"><p>بر آن لشکر کشن شد حمله ور</p></div>
<div class="m2"><p>کس از آفرینش نکرد آن هنر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شگفتی چنین کاردست خداست</p></div>
<div class="m2"><p>ندارد بلی دست حق چپ وراست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>علی دست حق اوست دست علی</p></div>
<div class="m2"><p>دوبینی در اینجا بود احولی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به یکدست فرزند ضرغام دین</p></div>
<div class="m2"><p>همی کشت خصم وهمی جست کین</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همان مشک را نیز بردوش داشت</p></div>
<div class="m2"><p>بدو بسته جان و دل و هوش داشت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حکیم طفیل از کمین ناگهان</p></div>
<div class="m2"><p>بدو تاخت مانند برق جهان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بزد تیغ و دست چپ از پیکرش</p></div>
<div class="m2"><p>بیفکند با آن پرند آورش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دو دستش چو گردید ازتن جدا</p></div>
<div class="m2"><p>بیفتاد از پای دست خدا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سپهدار دین را چو افتاد دست</p></div>
<div class="m2"><p>قوی پشت شاهنشه دین شکست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نبی (ص) و علی راز سررفت هوش</p></div>
<div class="m2"><p>برآمد ز خاتون محشر خروش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>حسن در جنان جامه بر تن درید</p></div>
<div class="m2"><p>حسین علی از جهان دل برید</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بلرزید ارکان عرش برین</p></div>
<div class="m2"><p>به سر دست زد جبرئیل امین</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به جای دو یا زنده دست آن جناب</p></div>
<div class="m2"><p>برآورد پای بلند از رکاب</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>زدی برسر و سینه ی هر که پای</p></div>
<div class="m2"><p>شدی زین جهان جانش دوزخ گرای</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>به ناگه ز لشگر یکی تیر تفت</p></div>
<div class="m2"><p>بیامد بدان مشک و آبش برفت</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چو برگرم خاک آب سردش بریخت</p></div>
<div class="m2"><p>سپهدار را رشته ی جان گسیخت</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>به خود گفت دیگر ز کوشش چه سود؟</p></div>
<div class="m2"><p>همه کوششم بهر این آب بود</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>دریغا همه رنج من شد به باد</p></div>
<div class="m2"><p>کسی را چنین نامرادی مباد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بدین گونه لختی چو پیگار کرد</p></div>
<div class="m2"><p>بیفکند مردان به دشت نبرد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>زبس خورد بر پیکرش تیر تیز</p></div>
<div class="m2"><p>بشد مشک بر حال او اشک ریز</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>دراین بد که ناپاکخو حرمله</p></div>
<div class="m2"><p>ز شست ستم کرد تیری یله</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بزد راست بر چشم آن نامدار</p></div>
<div class="m2"><p>جهان بر جهان بین او گشت تار</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>چپ و راست ازدرد افشاند سر</p></div>
<div class="m2"><p>که از دیده تیرش برآید مگر</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>نیامد برون تیر و شد بی قرار</p></div>
<div class="m2"><p>زآسیب پیکان زهر آبدار</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>دو پای ازرکاب آن یل حق پرست</p></div>
<div class="m2"><p>برآورد بر کوهه ی زین نشست</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>همی خواست کز دیده ی پر زخون</p></div>
<div class="m2"><p>به زانو کشد نوک پیکان برون</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>به ناگاه شومی بجست از کمین</p></div>
<div class="m2"><p>به دست اندرش گرزه ی آهنین</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بدانسان زد آن گرز بر مغفرش</p></div>
<div class="m2"><p>که اززین نگون گشت جنگی برش</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>تو گفتی نگون گشت عرش برین</p></div>
<div class="m2"><p>ز پشت سپهری به روی زمین</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>ز زین چون درافتاد سالار شاه</p></div>
<div class="m2"><p>برادرش را خواند با درود و آه</p></div></div>