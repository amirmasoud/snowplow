---
title: >-
    بخش ۸۰ - وصیت حضرت ابوالفضل به امام علیه السلام و شهادت آنجناب
---
# بخش ۸۰ - وصیت حضرت ابوالفضل به امام علیه السلام و شهادت آنجناب

<div class="b" id="bn1"><div class="m1"><p>بگفتا که ای شاه یزدانشناس</p></div>
<div class="m2"><p>به پروردگار جهان مرسپاس</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که دادم به راهت سرو جان پاک</p></div>
<div class="m2"><p>نبردم من این آرزو را به خاک</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دم آخرینم رسیدی به سر</p></div>
<div class="m2"><p>تن از بوی تو یافت جانی دگر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کنون گر رسد مرگ من باک نیست</p></div>
<div class="m2"><p>که انجا م هر زنده جز خاک نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سه خواهش مرا هست ای شهریار</p></div>
<div class="m2"><p>زراه کرم سوی من گوش دار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نخستین بود تا روانم به تن</p></div>
<div class="m2"><p>مبر سوی خیمه تن چاک من</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>که از کودکان توام شرمسار</p></div>
<div class="m2"><p>ز ناوردن آب شیرین گوار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دوم آنکه درماتم من منال</p></div>
<div class="m2"><p>مکن گریه اندر بر بدسگال</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو گریی تو بدخواه خندان شود</p></div>
<div class="m2"><p>به کین خواستن تیز دندان شود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سیم آنکه گفتی که از همرهان</p></div>
<div class="m2"><p>نمابد درین روز کس زنده جان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مگر سید الساجدین پور من</p></div>
<div class="m2"><p>که باشد پس از من امام زمن</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ازین درچو رفتی به سوی حرم</p></div>
<div class="m2"><p>چنین کن سفارش بدان محترم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>که چون جای گیری به یثرب دیار</p></div>
<div class="m2"><p>رها گشتی از پیچش روزگار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ز عمت دو کودک بود درسرای</p></div>
<div class="m2"><p>به جا مانده دل خسته و غمفزای</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>تو آن نورسان را پرستار باش</p></div>
<div class="m2"><p>زهر بد به گیتی نگهدار باش</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>یتیم اند مشکن دل زارشان</p></div>
<div class="m2"><p>پدروار بنگر به دیدارشان</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>به گفتار او شه بنالید سخت</p></div>
<div class="m2"><p>فرو ریخت خون از مژه لخت لخت</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>سترداز رخ و چشم او خون و خاک</p></div>
<div class="m2"><p>ببوسیدش آن چهره ی تابناک</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>جوان دیده بر روی شه برگشاد</p></div>
<div class="m2"><p>کشیدآه واندر برش جان بداد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>خنک دوستداری که در پای یار</p></div>
<div class="m2"><p>چو جان داد یار آردش در کنار</p></div></div>