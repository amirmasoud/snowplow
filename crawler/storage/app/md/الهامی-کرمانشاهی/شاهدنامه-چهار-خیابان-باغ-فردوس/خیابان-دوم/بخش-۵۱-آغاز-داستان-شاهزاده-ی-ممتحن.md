---
title: >-
    بخش ۵۱ - آغاز داستان شاهزاده ی ممتحن
---
# بخش ۵۱ - آغاز داستان شاهزاده ی ممتحن

<div class="b" id="bn1"><div class="m1"><p>چو قاسم پسرزاده ی مرتضی (ع)</p></div>
<div class="m2"><p>روان تن شاه دین مجتبی (ع)</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قسیم جهیم و جنان را پسر</p></div>
<div class="m2"><p>از آن قاسمش نام کرده پدر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یکی ماهرو نورس دل دونیم</p></div>
<div class="m2"><p>ز دریای توحید در یتیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پدر گر نبودش ولی آن پسر</p></div>
<div class="m2"><p>پدر بود بر آدم بوالبشر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مراو را بدی روز و شب غمگسار</p></div>
<div class="m2"><p>به جای پدر عم والاتبار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز فرزند نیکو ترش داشتی</p></div>
<div class="m2"><p>گرامی چو جان در برش داشتی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نهشتی که بادی براو کج وزد</p></div>
<div class="m2"><p>همی پروراندش چنان چون سزد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به نوشین لبش مهر تبخاله بود</p></div>
<div class="m2"><p>مه چارده سیزده ساله بود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هنوزش به مه نارسیده کلف</p></div>
<div class="m2"><p>به دشمن شکاری چو شاه نجف</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بدی در نیاکان او هر کمال</p></div>
<div class="m2"><p>مر او را بداد ایزد ذوالجلال</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نبی (ص) گر بدی فر پروردگار</p></div>
<div class="m2"><p>ازو بود فر نبی(ص) آشکار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>علی بود اگر شیر شمشیر حق</p></div>
<div class="m2"><p>بدآن ناموور بچه ی شیر حق</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بتول (ع) اربدی دردرج رسول (ص)</p></div>
<div class="m2"><p>مراو نیز بد مهر برج بتول (ع)</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>حسن (ع) گر شهنشاه آزاد بود</p></div>
<div class="m2"><p>ز پشت وی این پاک شهزاده بود</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>حسین ار بدی قدرت کردگار</p></div>
<div class="m2"><p>ازو اقتدار حسن (ع) آشکار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>من خاکی و مدح آن جان پاک</p></div>
<div class="m2"><p>کجا عالم جان کجا مشت خاک</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>مگر بخشدم خود زبانی دگر</p></div>
<div class="m2"><p>جز این جسم و جان جسم وجانی دگر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ایا تازه داماد گلگون قبا</p></div>
<div class="m2"><p>که شد سور تو ماتم مجتبی (ع)</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>تو آنی که در جان بود مسکنت</p></div>
<div class="m2"><p>روان پیمبر (ص) بود درتنت</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چو در پیکرت نوک پیکان خلید</p></div>
<div class="m2"><p>الم بر روان پیمبر (ص) رسید</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>حسین آن زمان دست از جان کشید</p></div>
<div class="m2"><p>که آغشته در خون تنت رابدید</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>کنون راز رانم زکردار تو</p></div>
<div class="m2"><p>به خردی دلیرانه پیکار تو</p></div></div>