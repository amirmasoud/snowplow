---
title: >-
    بخش ۸۱ - شهادت حضرت عباس علیه السلام
---
# بخش ۸۱ - شهادت حضرت عباس علیه السلام

<div class="b" id="bn1"><div class="m1"><p>چو از دامگاه بلا رسته شد</p></div>
<div class="m2"><p>به جانان همی جانش پیوسته شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نبی در جنانش به بر بر کشید</p></div>
<div class="m2"><p>زدست پدر آب کوثر چشید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>درودش زحق بر تن و جان پاک</p></div>
<div class="m2"><p>بدان تربت و قبه ی تابناک</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نگیرد پس از زاده ی بوتراب</p></div>
<div class="m2"><p>نه دستی عنان و نه پایی رکاب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نه شمشیر بادا نه خفتان نه خود</p></div>
<div class="m2"><p>نه ابر سیاه ونه چرخ کبود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نه گرداد گردون نه ماناد خاک</p></div>
<div class="m2"><p>نه تابد به چرخ اختر تابناک</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نگونسار بادا فلک برتراب</p></div>
<div class="m2"><p>درخشان درفش بلند آفتاب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نجوشاد نم در رگ تیره ابر</p></div>
<div class="m2"><p>بریزاد سرپنجه غژمان هژیر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به هر چشم جوشن زخون رود –باد</p></div>
<div class="m2"><p>زغم مویه گر جان داوود باد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نگردد دگر باره ی رهنورد</p></div>
<div class="m2"><p>نپوشد دلیری سلیح نبرد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>علم را بر از تیغ بادا قلم</p></div>
<div class="m2"><p>شود نیزه چون تیغ بالاش خم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دلا مهر بردار از این جهان</p></div>
<div class="m2"><p>که در وی نماند دلی شادمان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به مردی چو عباس شد چیردست</p></div>
<div class="m2"><p>دگرها زوی چون توانند رست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>یکی روز اگر سر برافرازدت</p></div>
<div class="m2"><p>دگر روز با سر بیاندازدت</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ابوالفضل چون زین جهان رخت بست</p></div>
<div class="m2"><p>به عرش برین نزد داور نشست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>شهنشه به بالین آن کشته زار</p></div>
<div class="m2"><p>بگریید لختی چو ابر بهار</p></div></div>