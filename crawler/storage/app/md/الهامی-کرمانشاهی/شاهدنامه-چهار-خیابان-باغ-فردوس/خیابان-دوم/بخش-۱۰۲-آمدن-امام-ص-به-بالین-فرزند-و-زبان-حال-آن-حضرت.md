---
title: >-
    بخش ۱۰۲ - آمدن امام (ص) به بالین فرزند و زبان حال آن حضرت
---
# بخش ۱۰۲ - آمدن امام (ص) به بالین فرزند و زبان حال آن حضرت

<div class="b" id="bn1"><div class="m1"><p>شهنشه چو بشنید آوای پور</p></div>
<div class="m2"><p>شکیب از دلش رفت و ازدیده نور</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو آن شیر کو بچه ی خویشتن</p></div>
<div class="m2"><p>ببیند به زخم گران خسته تن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>غریوان و غژمان سوی پهنه تاخت</p></div>
<div class="m2"><p>چنان کافرینش ازو زهره باخت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>برآهیخته ذوالفقار دوسر</p></div>
<div class="m2"><p>وزو آتش خشم حق شعله ور</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سپه کان بدیدند یکسو شدند</p></div>
<div class="m2"><p>گسسته دم وسست نیرو شدند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو آمد به بالین فرزند شاه</p></div>
<div class="m2"><p>به دریای خون دیدش اندرشناه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سرو پیکر از تیغ کین چاک چاک</p></div>
<div class="m2"><p>زخاشاک بسترش وبالین زخاک</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زاختر فزون زخم بر پیکرش</p></div>
<div class="m2"><p>دو پیکر پدید آمده از سرش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>برآورده آه ازدل دردناک</p></div>
<div class="m2"><p>ززین اندر افکند خود را به خاک</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به دست وبه زانو همی رفت پیش</p></div>
<div class="m2"><p>چنان تا به بالین فرزند خویش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گرفت آن سر چاک را درکنار</p></div>
<div class="m2"><p>که ازتاج خورشید و مه داشت عار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>پس آنگه به رخسار او سود روی</p></div>
<div class="m2"><p>زدش بوسه ها برلب وروی و موی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>برافراشت سرزان رخ پر زخون</p></div>
<div class="m2"><p>شدش لعل آن ریش کافور گون</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>خروشید ازدرد دل چند بار</p></div>
<div class="m2"><p>چنین گفت با دیده ی اشگبار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>که بعد از تو ای زاده ی بوتراب</p></div>
<div class="m2"><p>شود خانه ی آفرینش خراب</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>پس ازتو جوانا ز ابر هلاک</p></div>
<div class="m2"><p>ببارد به فرق جهان تیره خاک</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>تو رفتی ازین گیتی کینه سنج</p></div>
<div class="m2"><p>بیاسودی از درد و تیمار و رنج</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>پدر ماند تنها و بی غمگسار</p></div>
<div class="m2"><p>به گرد اندرش دشمن بی شمار</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چو شهزاده آوای شه را شنود</p></div>
<div class="m2"><p>دم واپسین دیده از هم گشود</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>لبان چو گل غنچه از هم شکفت</p></div>
<div class="m2"><p>پدر را درودی فرستاد و گفت</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>که من رفتم ای باب ناشادمان</p></div>
<div class="m2"><p>تو در پاس بخشنده یزدان بمان</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>مرا مصطفی (ص) داد جامی پرآب</p></div>
<div class="m2"><p>که بنشاند ازجان من سوز وتاب</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ز بهر تو آماده فرموده نیز</p></div>
<div class="m2"><p>بهشتی یکی جام کافور بیز</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>همی گوید ای تشنه لب کن شتاب</p></div>
<div class="m2"><p>بگیر و بنوش ازمن این جام آب</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بگفت این وزین دیر ناپایدار</p></div>
<div class="m2"><p>به سوی نیاکان خود بست بار</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>شهنشه چون جاندادن اوبدید</p></div>
<div class="m2"><p>تو گفتی که هوش ازسرش بر پرید</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>روان کرد از خون چشمان دورود</p></div>
<div class="m2"><p>زآهش جهان گشت پر تیره دود</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>همی گفت پورا – یلا –فرخا</p></div>
<div class="m2"><p>ستاره رخا شکرین پاسخا</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>مرا یادگار از همایون پدر</p></div>
<div class="m2"><p>چه زود آمدت زندگانی به سر</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>سرور دلم دیده ی روشنم</p></div>
<div class="m2"><p>خزان کردی از مرگ خود گلشنم</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>ایا اختر تابناک پدر</p></div>
<div class="m2"><p>چه زود آمدت زندگانی به سر</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>چنین است هر اختر صبحدم</p></div>
<div class="m2"><p>که تابش فزون دارد وزیست کم</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>مپندار کز تو فرامش کنم</p></div>
<div class="m2"><p>چو گویم سخن یا که خامش کنم</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>به گفتار اندر تویی بر زبان</p></div>
<div class="m2"><p>به خاموشی ام یادت اندر روان</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>بگریم به چهر دلارای تو</p></div>
<div class="m2"><p>ویا بر خرامنده بالای تو؟</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>ندانم بگریم به زخم سرت</p></div>
<div class="m2"><p>ویا بر پر ازخاک و خون پیکرت؟</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>بگریم براین کام خشکیده ات</p></div>
<div class="m2"><p>ویا بر دل کام نادیده ات؟</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>گروهی که کشتندت ای کشته زار</p></div>
<div class="m2"><p>برآرد خداوند ازایشان دمار</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>مرا ای جوان داغت از پا فکند</p></div>
<div class="m2"><p>نهال حیات من از بن بکند</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>ببر سوی جدت پیام مرا</p></div>
<div class="m2"><p>به مام و پدر گو پیام مرا</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>دم دیگر آیم به سوی توشاد</p></div>
<div class="m2"><p>تو را زین ره اندیشه دردل مباد</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>پس آنگه تن کشته را آن جناب</p></div>
<div class="m2"><p>بیفکند بر زین اسب عقاب</p></div></div>