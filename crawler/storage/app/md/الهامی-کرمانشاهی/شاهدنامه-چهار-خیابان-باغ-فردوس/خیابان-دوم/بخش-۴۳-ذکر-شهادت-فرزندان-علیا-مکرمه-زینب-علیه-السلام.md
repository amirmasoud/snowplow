---
title: >-
    بخش ۴۳ - ذکر شهادت فرزندان علیا مکرمه زینب علیه السلام
---
# بخش ۴۳ - ذکر شهادت فرزندان علیا مکرمه زینب علیه السلام

<div class="b" id="bn1"><div class="m1"><p>وزآن پس دو نوباوه حیدر نژاد</p></div>
<div class="m2"><p>که شان باب عبدالله پاک راد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یکی عون و دیگر محمد به نام</p></div>
<div class="m2"><p>نهادند مردانه در رزم گام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بدادند چون داد نام آوری</p></div>
<div class="m2"><p>نمودند چون خال را یاوری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ازین دامگاه فنا تاختند</p></div>
<div class="m2"><p>به خلد اندر آن جایگه ساختند</p></div></div>