---
title: >-
    بخش ۷۴ - به میدان رفتن حضرت عباس علیه السلام به فرمان امام علیه السلام
---
# بخش ۷۴ - به میدان رفتن حضرت عباس علیه السلام به فرمان امام علیه السلام

<div class="b" id="bn1"><div class="m1"><p>پس آنگه برآمد به زین خدنگ –</p></div>
<div class="m2"><p>به فرمان روان شد سوی دشت جنگ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به دست اندرش آسمانی درفش</p></div>
<div class="m2"><p>هواشد زگرد سمندش بنفش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کمند ی چو ثعبان و تیغی چو برق</p></div>
<div class="m2"><p>سراپا به دریای پولاد غرق</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به زین تافتی چهره ی آن جناب</p></div>
<div class="m2"><p>چو از تیغ کوه بلند آفتاب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عنان را به اسب گرانمایه داد</p></div>
<div class="m2"><p>بیامد به نزدیک لشگر ستاد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بزد بر زمین رایت ارجمند</p></div>
<div class="m2"><p>نگه خیره بر سوی دشمن فکند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو دیدند لشگر سرو پیکرش</p></div>
<div class="m2"><p>همان حیدری جوشن و مغفرش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بگفتند خود مرتضی زنده شد</p></div>
<div class="m2"><p>رخش اندرین پهنه تابنده شد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>یکی گفت کاین شبل شیرخداست</p></div>
<div class="m2"><p>به بالا ودیدار خود مرتضی است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مراو را پدر کرده عباس نام</p></div>
<div class="m2"><p>به مردی نهد بر سر چرخ گام</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گر این نامور رای جنگ آورد</p></div>
<div class="m2"><p>سر نام ما زیر ننگ آورد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>شنیدند نامش چو نام آوران</p></div>
<div class="m2"><p>بماندند برجا کران تا کران</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چنان شد دل جنگیان از نهیب</p></div>
<div class="m2"><p>که خوشیدشان پای اندر رکیب</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>سواری ندانستی از بیم جان</p></div>
<div class="m2"><p>که دارد به کف پار دم یا عنان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>سپهبد زمانی خموش ایستاد</p></div>
<div class="m2"><p>تو گفتی که کوهی است برپشت باد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ازآن پس برآورد اوا بلند</p></div>
<div class="m2"><p>بگفتا به سالار ناهوشمند</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>که هان ای عمر این خداوند فرد</p></div>
<div class="m2"><p>که بینی ستاده به دشت نبرد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>حسین علی سبط پیغمبر (ص) است</p></div>
<div class="m2"><p>که برهر که اندر جهان سرورست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بودساقی کوثر او را پدر</p></div>
<div class="m2"><p>که بد کارفرمای تیغ دوسر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گزین مادرش هست فرخ بتول</p></div>
<div class="m2"><p>که بد پاره ی جسم پاک رسول (ص)</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بد او بانوی بانوان جهان</p></div>
<div class="m2"><p>به خرم جنان نیز شاه زنان</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>به کابین او کوثر وسلسبیل</p></div>
<div class="m2"><p>فرات است و جیحون وسیحون ونیل</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>خود او وارث علم پیغمبر است</p></div>
<div class="m2"><p>امام است وسوی خدارهبر است</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چرا با چنین شاه کین آوری</p></div>
<div class="m2"><p>شکست از چه بررکن دین آوری</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ببرده است از راه اهریمنت</p></div>
<div class="m2"><p>به یزدان خودساخته دشمنت</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>فراتی که کابین زهرا بود</p></div>
<div class="m2"><p>روان رایگان سوی صحرا بود</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>دد ودام نوشند از آن روز وشب</p></div>
<div class="m2"><p>تو بندیش بر پور او ای عجب</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>گر این اهلبیت رسول امین</p></div>
<div class="m2"><p>بمیرند تشنه دراین سرزمین</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>چو لب تشنه آیی به روز شمار</p></div>
<div class="m2"><p>چه عذر آوری نزد پروردگار</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>به من داده فرمان شهنشاه دین</p></div>
<div class="m2"><p>که از وی بگویم ترا اینچنین</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>که کشتی تو اخوان ویاران من</p></div>
<div class="m2"><p>نهشتی تنی زان بزرگ انجمن</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>ندارم کنون من به جز طفل چند</p></div>
<div class="m2"><p>دراین خسروی بارگاه بلند</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>دل جمله از تشنگی سوخته</p></div>
<div class="m2"><p>جگر تفته وسینه افروخته</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>چه دارند این خردسالان گناه</p></div>
<div class="m2"><p>که گردند ازتشنه کامی تباه</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>از آن پیش کز تشنگی جان دهند</p></div>
<div class="m2"><p>بگو تاکه آبی بدیشان دهند</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>کنی گر چنین من به روز شمار</p></div>
<div class="m2"><p>چو آیم بر پاک پروردگار</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>بگیرم یکی راه بخشش به پیش</p></div>
<div class="m2"><p>نخواهم زتو خون یاران خویش</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>خود او هر چه خواهند بتوان کند</p></div>
<div class="m2"><p>که چاره به کار جهانیان کند</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>ودیگر ازین بیش برمن سیاه</p></div>
<div class="m2"><p>نسازید روز و نبندید راه</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>کزین مرز با آل خیر البشر</p></div>
<div class="m2"><p>شوم سوی هندوستان رهسپر</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>ندارم به ملک عراق و حجاز</p></div>
<div class="m2"><p>سرمویی اندر جهان من نیاز</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>زفرخنده سالار شمشیر زن</p></div>
<div class="m2"><p>شنیدند لشگر چو زینسان سخن</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>گروهی نشستند بر جای و زار</p></div>
<div class="m2"><p>گرستند برغربت شهریار</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>سپهدار و آن لشگر تیره جان</p></div>
<div class="m2"><p>ببستند از پاسخ وی زبان</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>مگر شمر دون وشبث کز سپاه</p></div>
<div class="m2"><p>برفتند نزدیک سالار شاه</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>بگفتند کای صفدر نامجوی</p></div>
<div class="m2"><p>ازیدر برو با برادر بگوی</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>میان هوا گر شود پر زآب</p></div>
<div class="m2"><p>نهد روی بر پشت آب آفتاب</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>شما تشنه کامان زما بی دریغ</p></div>
<div class="m2"><p>نبینید آبی بجز آب تیغ</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>مگر آنکه پیمان به دارای شام</p></div>
<div class="m2"><p>ببندید و خوانید او را امام</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>سپهبد به گفتار آن تیره هوش</p></div>
<div class="m2"><p>تبسم کنان داد یک لخت گوش</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>پس آنگه بیامد برشه بگفت</p></div>
<div class="m2"><p>سراسر سخن ها که گفت وشنفت</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>شهنشه دلش آمد از غم به درد</p></div>
<div class="m2"><p>دو گلگونه از اشک پر ژاله کرد</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>ابوالفضل استاده اندر برش</p></div>
<div class="m2"><p>دل آشفته و دست کرده به کش</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>به ناگه خروش آمد از خیمه گاه</p></div>
<div class="m2"><p>خروشی کز آن تیره شد مهر و ماه</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>بسی کودک خرد زار و ملول</p></div>
<div class="m2"><p>زاولاد شیر خدا و رسول</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>زخرگه به یکره برون آمدند</p></div>
<div class="m2"><p>خروشان ز سوز درون آمدند</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>دو گلگونه ازدرد دل ها سیه</p></div>
<div class="m2"><p>عقیق لب از تشنگی چون شبه</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>جهان زآهشان پرغو العطش</p></div>
<div class="m2"><p>رخ مهر تابنده فیروزه وش</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>سکینه به پیش اندرون مویه گر</p></div>
<div class="m2"><p>خروشان ز پی دختران دگر</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>همه بلبل آسا به افغان و آه</p></div>
<div class="m2"><p>سرآهنگشان ناز پرورد شاه</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>به بازو فکنده، یکی خشک مشک</p></div>
<div class="m2"><p>دو جوبسته بر روی گلگون ز اشک</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>دمان سوی سالار شاه آمدند</p></div>
<div class="m2"><p>چو سیاره گان گرد ماه آمدند</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>یکی اندر آویخت بر دامنش</p></div>
<div class="m2"><p>یکی بوسه زد بر سم توسنش</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>یکی گفت کای عم والاگهر</p></div>
<div class="m2"><p>به ما مهربان چون گرامی پدر</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>زبی آبی، آتش به ما درفتاد</p></div>
<div class="m2"><p>دهد خاک ما تشنه کامی به باد</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>تو دریای فیض خدایی مخواه</p></div>
<div class="m2"><p>ز بی آبی این کودکان را تباه</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>یکی گفت بر ما نگر، یک نفس</p></div>
<div class="m2"><p>که جز تو نداریم فریاد رس</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>زسوز دل و تابش آفتاب</p></div>
<div class="m2"><p>هم ایدون بمیریم اگر نیست آب</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>تو مپسند ای غیرت کردگار</p></div>
<div class="m2"><p>که ما تشنه لب جان سپاریم زار</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>از آن کودکان پور شیر خدای</p></div>
<div class="m2"><p>چو دید آنهمه زاری جانگزای</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>دل نازکش زآتش غم بسوخت</p></div>
<div class="m2"><p>زغیرت دو گلگونه اش برفروخت</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>بر خسرو دین ببوسید خاک</p></div>
<div class="m2"><p>بگفت ای مرا بهتر از جان پاک</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>به تنگ آمدم از چنین زنده گی</p></div>
<div class="m2"><p>نخواهم دگر روز پاینده گی</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>مراشرم این کودکان کرد آب</p></div>
<div class="m2"><p>ندارم دگر با چنین درد تاب</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>بفرما که تازم به میدان جنگ</p></div>
<div class="m2"><p>رها گردم ازدست این روز تنگ</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>زمرگ برادر تو را غم مباد</p></div>
<div class="m2"><p>چنان دان که هرگز زمادر نزاد</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>مرا شیر حق بهر این روزگار</p></div>
<div class="m2"><p>به مردانگی گشت آموزگار</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>مرا از ازل کردگار جهان</p></div>
<div class="m2"><p>امانت نهاده است درتن روان</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>که بازم به راه تواش بی دریغ</p></div>
<div class="m2"><p>نتابم رخ از زخم پیکان و تیغ</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>بفرما که تازم به میدان کین</p></div>
<div class="m2"><p>به جای آورم امر جان آفرین</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>شهنشه بدو خواند بس آفرین</p></div>
<div class="m2"><p>سپس گفت کای پور ضرغام دین</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>به من گر همی بایدت یاوری</p></div>
<div class="m2"><p>همی کوش کابی به دست آوری</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>که این کودکان تر نمایند کام</p></div>
<div class="m2"><p>از آن پس بکن روز بدخواه شام</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>ابولفضل ازین مژده دلشاد گشت</p></div>
<div class="m2"><p>زبند غم و رنج آزاد گشت</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>ببوسید پیش برادر زمین</p></div>
<div class="m2"><p>چو حیدر بر سیدالمرسلین (ص)</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>چو لختی به شه پوزش اندر گرفت</p></div>
<div class="m2"><p>همان مشک خشکیده در بر گرفت</p></div></div>