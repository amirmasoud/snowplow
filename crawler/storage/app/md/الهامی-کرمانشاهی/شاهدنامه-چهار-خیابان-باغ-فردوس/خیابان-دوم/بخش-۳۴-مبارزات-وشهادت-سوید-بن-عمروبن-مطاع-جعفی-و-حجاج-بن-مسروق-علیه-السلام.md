---
title: >-
    بخش ۳۴ - مبارزات وشهادت سوید بن عمروبن مطاع جعفی و حجاج بن مسروق علیه السلام
---
# بخش ۳۴ - مبارزات وشهادت سوید بن عمروبن مطاع جعفی و حجاج بن مسروق علیه السلام

<div class="b" id="bn1"><div class="m1"><p>سپس مرد جعفی سوید شجاع</p></div>
<div class="m2"><p>که اورا پدر بود عمرو مطاع</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به امر امام زمان رزم جست</p></div>
<div class="m2"><p>زمین را به خون دلیران بشست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زبسیاری زخم مدهوش شد</p></div>
<div class="m2"><p>دمی چند بی تاب وبی توش شد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بدانگه که آمد به جاهوش اوی</p></div>
<div class="m2"><p>چنین آمد آواز درگوش اوی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>که شد کشته فرزند شیر خدا</p></div>
<div class="m2"><p>چو بشنید آزاد مرد این ندا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یکی دشته در موزه ی خویش داشت</p></div>
<div class="m2"><p>برآورد و رو سوی لشکر گذاشت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ازآنان همی کشت تا کشته شد</p></div>
<div class="m2"><p>به خاک وبه خون اندر آغشته شد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از آن پس دلیری که رزم آزمود</p></div>
<div class="m2"><p>سرافراز حجاج مسروق بود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>روان شد به میدان چو شیر ژیان</p></div>
<div class="m2"><p>فزون کشت از لشکر کوفیان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سرانجام در یاری شاه دین</p></div>
<div class="m2"><p>مکان جست اندر بهشت برین</p></div></div>