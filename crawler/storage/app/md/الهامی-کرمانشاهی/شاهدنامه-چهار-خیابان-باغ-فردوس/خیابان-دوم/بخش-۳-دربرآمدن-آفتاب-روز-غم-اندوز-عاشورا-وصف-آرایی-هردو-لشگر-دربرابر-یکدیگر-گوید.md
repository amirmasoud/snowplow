---
title: >-
    بخش ۳ - دربرآمدن آفتاب روز غم اندوز عاشورا وصف آرایی هردو لشگر دربرابر یکدیگر گوید
---
# بخش ۳ - دربرآمدن آفتاب روز غم اندوز عاشورا وصف آرایی هردو لشگر دربرابر یکدیگر گوید

<div class="b" id="bn1"><div class="m1"><p>سپیده چو زین چرخ پیروز گون</p></div>
<div class="m2"><p>عیان گشت خور چون یکی طشت خون</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ستاره چو خون ریخت از دیده چرخ</p></div>
<div class="m2"><p>جهان از همه خون فلک داد برخ</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زنو دست صباغ این کهنه دن</p></div>
<div class="m2"><p>که او را بود پیشه دستان و فن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>درآن روز آهنگ نیرنگ کرد</p></div>
<div class="m2"><p>زخون کرته ی خاک را رنگ کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هوا گشت بر گونه ی آذرخش</p></div>
<div class="m2"><p>زمین شد به کردار کان بدخش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سپاه خدا سجده پرداختند</p></div>
<div class="m2"><p>یکی نامور انجمن ساختند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>برایشان یکی خطبه برخواند شاه</p></div>
<div class="m2"><p>که ای نامداران ناورد خواه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به رزم اندرین روز پای آورید</p></div>
<div class="m2"><p>همان امر یزدان به جای آورید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گواهی دهم کشته گردید زار</p></div>
<div class="m2"><p>شما اندرین روز و این کارزار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نماند بجز سیدالساجدین</p></div>
<div class="m2"><p>که باشد پس از من مرا جانشین</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بگفتند یاران فرخ تبار</p></div>
<div class="m2"><p>که هستیم بر عهد خود استوار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>زچرخ برین جبرئیل امین</p></div>
<div class="m2"><p>بزد بانگ بر لشگر شاه دین</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>که ای لشگر پاک پروردگار</p></div>
<div class="m2"><p>بیایید بر باره گی ها سوار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بلی آن سپه جیش یزدان بدند</p></div>
<div class="m2"><p>که از مردن خویش شادان بدند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>زگردنده گردون چو شه رابه گوش</p></div>
<div class="m2"><p>رسید از خجسته سروش این خروش</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>به حکم خداوند جان آفرین</p></div>
<div class="m2"><p>بیاراست لشگر به میدان کین</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>سی و دو سرافراز جنگی سوار</p></div>
<div class="m2"><p>پیاده چهل مرد خنجر گذار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>همین بود یاران فرخنده شاه</p></div>
<div class="m2"><p>دریغا از آن خسرکم سپاه</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>شهنشه چو زینت به لشگر بداد</p></div>
<div class="m2"><p>علم را به دست برادر بداد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ابر میمنه رایتی برفراشت</p></div>
<div class="m2"><p>زهیر سرافراز را برگماشت</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ابر میسره بود بافر و زیب</p></div>
<div class="m2"><p>خردمند پور مظاهر حبیب</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>خود آن دادگر داور رهنمای</p></div>
<div class="m2"><p>چو ایمان به قلب سپه کرد جای</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>وزان پس یکی آتش افروختند</p></div>
<div class="m2"><p>همای هیزم کنده را سوختند</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>پر آتش شد آن کنده برجای آب</p></div>
<div class="m2"><p>که دشمن نیارد به خرگه شتاب</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>وزان سوی سالار تاریک تن</p></div>
<div class="m2"><p>بیاراست لشگرگه خویشتن</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ز دروازه ی کوفه تا کربلا</p></div>
<div class="m2"><p>درفش سپه گشت پرچم گشا</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ستاده به سرغین کوس و بنه</p></div>
<div class="m2"><p>کجا عمرو حجاج در میمنه</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>گزین کرد بن سعد تاری تنا</p></div>
<div class="m2"><p>ابر میسره شمر ذی الجوشنا</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>درفش سپه را همان بدنژاد</p></div>
<div class="m2"><p>به دست ورید غلامش بداد</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>سواران که بودند با تیغ و خود</p></div>
<div class="m2"><p>سپهدارشان عروه ی قیس بود</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>پیاده سپه را شبث بود سر</p></div>
<div class="m2"><p>که نفرین یزدان بدان بدگهر</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>درآن دشت بیننده تا کرد کار</p></div>
<div class="m2"><p>پس و پشت هم بود ترک و سوار</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>همه بد گهر زاده ی تیره هوش</p></div>
<div class="m2"><p>سپردار و خنجر زن و درع پوش</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>ز موج سواران زرینه زین</p></div>
<div class="m2"><p>چو دریای سیماب جنبان زمین</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>بلا راست گفتی یکی میغ بود</p></div>
<div class="m2"><p>که باران او نیزه و تیغ بود</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>چو آراست کار سران سپاه</p></div>
<div class="m2"><p>چنین گفت بن سعد بی فر و جاه</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>که ای لشگر کوفه جنگ آورید</p></div>
<div class="m2"><p>به سالار دین کارتنگ آوردید</p></div></div>