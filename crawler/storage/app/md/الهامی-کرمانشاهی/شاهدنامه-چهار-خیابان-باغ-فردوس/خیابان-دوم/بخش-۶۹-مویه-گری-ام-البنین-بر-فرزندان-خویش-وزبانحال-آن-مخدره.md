---
title: >-
    بخش ۶۹ - مویه گری ام البنین بر فرزندان خویش وزبانحال آن مخدره
---
# بخش ۶۹ - مویه گری ام البنین بر فرزندان خویش وزبانحال آن مخدره

<div class="b" id="bn1"><div class="m1"><p>شنیدم ز دانا کز اینسان سرود</p></div>
<div class="m2"><p>که بر جان و پیکرش بادا درود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که در یثرب آگه چو ام البنین</p></div>
<div class="m2"><p>شد ازمرگ آن چار پور گزین</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همه روزه باحال افسرده گان</p></div>
<div class="m2"><p>چمیدی سوی تربت مرده گان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>برآراستی با سری پر ز شور</p></div>
<div class="m2"><p>زخاک سیه صورت چار گور</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نشستی و از دل کشیدی خروش</p></div>
<div class="m2"><p>چنان کز نوا مرغ گشتی خموش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همی مویه کردی دلی سوگوار</p></div>
<div class="m2"><p>به عبدالله و جعفر نامدار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گهی نام عثمان ببردی به درد</p></div>
<div class="m2"><p>کشیدی ز سوزان جگر آه سرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>روان کردی از دیده گان ژرف رود</p></div>
<div class="m2"><p>جهان گشتی از آه وی پر ز دود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سپس بر سپهدار فرخنده شاه</p></div>
<div class="m2"><p>کشیدی ز دل آتش افروز، آه</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بگفتی که پورا – سرا –سرورا</p></div>
<div class="m2"><p>جوانا –یلا – زاده ی حیدرا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بگریم به پر خون بریده سرت</p></div>
<div class="m2"><p>ویا بردو بازوی زور آورت</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>که در خون کشید آن برنامور؟</p></div>
<div class="m2"><p>که آسیمه گشتی ازو شیر نر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>زمادر جدا گشته دور از دیار</p></div>
<div class="m2"><p>شدی کشته دریاری شهریار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بگریم بدان برز و بالای تو</p></div>
<div class="m2"><p>ویا حیدری فر والای تو</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>که افکند آن دست های بلند</p></div>
<div class="m2"><p>که بد صاحب پنجه ی زورمند</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>جوانا سپردی ندانم روان</p></div>
<div class="m2"><p>چسان؟تشنه لب نزد آب روان</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>سکینه طلب از تو چون کرد آب</p></div>
<div class="m2"><p>چو آبی نبودت چه بودت جواب؟</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چو ازجان همی خواستی شست دست</p></div>
<div class="m2"><p>نگفتی مرا مادری نیز هست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>نگفتی که باشد دو چشمش به راه</p></div>
<div class="m2"><p>برفتی و روزش نمودی سیاه</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>جوانا چرا آنکه زد بر تو تیغ</p></div>
<div class="m2"><p>نیاورد برمام پیرت دریغ</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>برادر چو آمد به بالین تو</p></div>
<div class="m2"><p>بدیدآن بر چاک خونین تو</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ندانم که اورا چه آمد به سر</p></div>
<div class="m2"><p>برآنم که خم گشت او را کمر</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>جوانا توسالار لشگر بدی</p></div>
<div class="m2"><p>علمدار خیل برادر بدی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>نماندی نگهبان لشگر چرا</p></div>
<div class="m2"><p>شکستی تو پشت برادر چرا</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>دریغا نبودم در آن کارزار</p></div>
<div class="m2"><p>که لختی بگریم به تو زار زار</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بشویم به خون حلقه ی جوشنت</p></div>
<div class="m2"><p>کشم نوک تیر از تن روشنت</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>سرت را گذارم به زانو همی</p></div>
<div class="m2"><p>نهم بر به زخم تنت مرهمی</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>چرا ای روان من دل دونیم</p></div>
<div class="m2"><p>نمودی دو فرزند خودرا یتیم</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>به طفلان تو ای گرامی پسر</p></div>
<div class="m2"><p>چه گویم چو خواهند ازمن پدر</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>چو لختی همی زار گفتی چنین</p></div>
<div class="m2"><p>سرودی مر آن بانوی دل غمین</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>که ای تشنه لب کشته فرزند من</p></div>
<div class="m2"><p>روان تن من جگر بند من</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>نمویم دگر بر تو با اشک و آه</p></div>
<div class="m2"><p>سزد مویه وزاری ام بهر شاه</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>ننالم اگر زنده مانم به کس</p></div>
<div class="m2"><p>همه زاری ام بر حسین (ع) است وبس</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>که تو مادری باشدت مویه ساز</p></div>
<div class="m2"><p>ولی مادرش نیست شاه حجاز</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>ندارد اگر مادر آن شاه دین</p></div>
<div class="m2"><p>کنیز بتول است ام البنین</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>بگریم براو زار روز و شبان</p></div>
<div class="m2"><p>چو بر پور خود مادر مهربان</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>نگریم به عباس نام آورم</p></div>
<div class="m2"><p>ننالم به عبدالله و جعفرم</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>بدو تا که باشم بمویم همی</p></div>
<div class="m2"><p>دورخ زآب دیده بشویم همی</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>مرااین همه غلغل و زمزمه</p></div>
<div class="m2"><p>بود بهر نور دل فاطمه</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>دریغا از آن شاه بیکس دریغ</p></div>
<div class="m2"><p>که دور از تنش سرشد از زخم تیغ</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>دریغا از آن شاه بیکس دریغ</p></div>
<div class="m2"><p>که دور از تنش سر شد از زخم تیغ</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>دریغا ز آزاده ی بوتراب</p></div>
<div class="m2"><p>که از خون او لاله گون شد تراب</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>دریغا از آن روی خورشید وش</p></div>
<div class="m2"><p>که شد زعفرانی زتاب عطش</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>دریغ آن تن پروریده به ناز</p></div>
<div class="m2"><p>که شد دشمن دین بدو اسب تاز</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>زافغان آن بانوی خونجگر</p></div>
<div class="m2"><p>زن و مرد یثرب به هر بام ودر</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>شب وروز بودند گریان همه</p></div>
<div class="m2"><p>دل از آتش سوک بریان همه</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>شنیدم که مروان تاری روان</p></div>
<div class="m2"><p>که در مرز یثرب بدی حکمران</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>یکی روز ازشهر شد سوی دشت</p></div>
<div class="m2"><p>بدان بانوی مویه گر برگذشت</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>چو بر حالت زار وی بنگریست</p></div>
<div class="m2"><p>دل سخت وی نرم گشت و گریست</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>چسان ناله می کرد کان زشت مرد</p></div>
<div class="m2"><p>به آن دشمنی ها بدو گریه کرد</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>نه یکشب زمانی به راحت غنود</p></div>
<div class="m2"><p>نه یکروز از گریه آرام بود</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>شب و روز بودش خورش خون دل</p></div>
<div class="m2"><p>قرین تنش محنت جان گسل</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>بدی همچو لاله دلش داغدار</p></div>
<div class="m2"><p>که تا رفت از این دار ناپایدار</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>بدان بانوی خسرو راستین</p></div>
<div class="m2"><p>درود فزون از جهان آفرین</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>دگر باد بر چار فرزند او</p></div>
<div class="m2"><p>سلام از جهانبان خداوند او</p></div></div>