---
title: >-
    بخش ۳۶ - حمله نمودن جناب عابس برلشگر مخالف
---
# بخش ۳۶ - حمله نمودن جناب عابس برلشگر مخالف

<div class="b" id="bn1"><div class="m1"><p>زهر سو گروهی براو تاختند</p></div>
<div class="m2"><p>سنان برکشیدند و تیغ آختند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نبرد دلاور چو اینگونه دید</p></div>
<div class="m2"><p>یکی دشنه ی آبگون بر کشید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>برافکند اسب و بیازید دست</p></div>
<div class="m2"><p>سروتن بسی کرد با خاک پست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به هر سو که تیغ آختی برسران</p></div>
<div class="m2"><p>زمین گشتی از بار سرها گران</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زدی خویش را برسپه یک تنه</p></div>
<div class="m2"><p>نه از میسره اش باک نز میمنه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>توگفتی که ابری شد او شعله بار</p></div>
<div class="m2"><p>سپه بداندیش خاشاک وخار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>و یا بود چون سیل بنیانکنا</p></div>
<div class="m2"><p>و یا برق سوزنده ی خرمنا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>همی تیغ بر فرق مردان بسود</p></div>
<div class="m2"><p>به ناگاه در گوش عشقش سرود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>که تا کی کشی دشمن زشتخوی</p></div>
<div class="m2"><p>بکش دست از جان و جانان بجوی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>برون آی از خویش و هستی ببین</p></div>
<div class="m2"><p>بخور شربت عشق و مستی ببین</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>زهم رشته ی جان و تن درگسل</p></div>
<div class="m2"><p>بپیوند با مهر دلدار دل</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دلاور چو دریا درآمد به جوش</p></div>
<div class="m2"><p>ز ساز محبت دلش پرخروش</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ز سودای جانانه دیوانه شد</p></div>
<div class="m2"><p>بد انسان که از خویش بیگانه شد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>پس آنگاه از تارک ارجمند</p></div>
<div class="m2"><p>سبکرو دو جوشن به یک سو فکند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>برون کرد رخت از تن رزمخواه</p></div>
<div class="m2"><p>نبد باکش از تیغ و تیره سپاه</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>روان دلیری برهنه تنا</p></div>
<div class="m2"><p>بزد خویش را برصف دشمنا</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>یکی گفتش ای پر دل تیز چنگ</p></div>
<div class="m2"><p>چرا دور کردی ز تن رخت جنگ</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>تنی را که آسیب دید از حریر</p></div>
<div class="m2"><p>برهنه کنی پیش شمشیر و تیر</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بدو گفت عابس که درراه دوست</p></div>
<div class="m2"><p>همان به چو مغز اندر آیم ز پوست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چنین می پسندد مرا عشق یار</p></div>
<div class="m2"><p>تن و جان دراین راه ناید به کار</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بگفت این وزد خویش رابرسپاه</p></div>
<div class="m2"><p>پر از ویله گردید ازو رزمگاه</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>عمر چون برهنه تن او را بدید</p></div>
<div class="m2"><p>به لشگر خروشی زدل برکشید</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>که یکرویه کار ای سوران کنید</p></div>
<div class="m2"><p>تن مرد را سنگباران کنید</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>به گرد اندرش پره لشگر زدند</p></div>
<div class="m2"><p>به عریان تنش سنگ کین برزدند</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>سوار سرافراز ناورد جوی</p></div>
<div class="m2"><p>از آن سنگباران نپیچید روی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>پی یاری خسرو کربلا</p></div>
<div class="m2"><p>سپر کرد تن پیش تیر بلا</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>نه بیمش ز تیغ و نه پروا زتیر</p></div>
<div class="m2"><p>به جسمش بدی سنگ خارا حریر</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>نکرد ایچ از یاری شه دریغ</p></div>
<div class="m2"><p>همی راند بر ترک بدخواه تیغ</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>ز سوی دگر گام بر گام او</p></div>
<div class="m2"><p>همان بنده ی نیک فرجام او</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>سر سرکشان از تن افکند پست</p></div>
<div class="m2"><p>فری زان هنرمندی و تیغ و دست</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>ازآن پس که بسیار کردند جنگ</p></div>
<div class="m2"><p>برآن هر دوبسیار شد کار تنگ</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>نگون شد تن نام بردارشان</p></div>
<div class="m2"><p>سرآمد زمان در به پیگارشان</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>بدانسو که جستند بستند رخت</p></div>
<div class="m2"><p>به مینو نهادند شاهانه تخت</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>شنیدم سر عابس نامور</p></div>
<div class="m2"><p>بریدند و بردند نزد عمر</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>همی هر کسی گفت ازآن سپاه</p></div>
<div class="m2"><p>که از تیغ من گشت عابس تباه</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>شد این گفتگو تا بدان جا دراز</p></div>
<div class="m2"><p>که با هم نمودند پیگار ساز</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>عمر گفت عابس نبود آن سوار</p></div>
<div class="m2"><p>که یکتن سر آرد بر او روزگار</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>پی چیست این شورش و همهمه</p></div>
<div class="m2"><p>شریک اید در خون عابس همه</p></div></div>