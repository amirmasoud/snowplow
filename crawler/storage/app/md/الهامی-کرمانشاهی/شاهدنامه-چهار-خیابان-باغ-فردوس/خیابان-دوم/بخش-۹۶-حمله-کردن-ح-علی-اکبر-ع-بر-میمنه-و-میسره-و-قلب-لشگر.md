---
title: >-
    بخش ۹۶ - حمله کردن ح – علی اکبر(ع)بر میمنه و میسره و قلب لشگر
---
# بخش ۹۶ - حمله کردن ح – علی اکبر(ع)بر میمنه و میسره و قلب لشگر

<div class="b" id="bn1"><div class="m1"><p>نهنگی دمان بر کشید ازنیام</p></div>
<div class="m2"><p>که جان دلیران کشیدی به کام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بدان تیغ شیر کنام نبرد</p></div>
<div class="m2"><p>به لشگر چو جدش علی حمله کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سپاهی درآمد ز گرگان کین</p></div>
<div class="m2"><p>به پیگار آن یوسف مصر دین</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سبک تیغ شهزاده شد سرگرای</p></div>
<div class="m2"><p>زمین شد پر از پیکر و دست و پای</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زیک برق کز دشنه ی او بجست</p></div>
<div class="m2"><p>تن و جان صد مرد جنگی بخست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز بس کشته افکند در کارزار</p></div>
<div class="m2"><p>زخون پهنه گردید دریا کنار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یلان سپه زهره بشکافتند</p></div>
<div class="m2"><p>ز پیکار شهزاده رو تافتند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به پهنه عمر از آنگونه کار</p></div>
<div class="m2"><p>بپیچید بر خویش مانند مار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زنا پاکرایی یکی چاره جست</p></div>
<div class="m2"><p>که گردد شکست سپه زو درست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به لشگر یکی بد گهر مرد بود</p></div>
<div class="m2"><p>که چون ژنده پیلان به ناورد بود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تن او بار و خود کام و جنگی بدی</p></div>
<div class="m2"><p>به رزم سواران درنگی بدی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بدی نام او طارق بن شبیب</p></div>
<div class="m2"><p>دل جنگجویان ازو پر نهیب</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>درآن روز بودش به خرگاه جای</p></div>
<div class="m2"><p>نهشته برون سوی ناورد پای</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>تن او بار او طارق بن شبیب</p></div>
<div class="m2"><p>دل جنگجویان ازو پر نهیب</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>درآن روز بودش به خرگاه جای</p></div>
<div class="m2"><p>نهشته برون سوی ناورد پای</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>نوندی فرستاد میر سپاه</p></div>
<div class="m2"><p>سوی طارق شوم بی آب و جاه</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چو آمد بدو گفت کای نامدار</p></div>
<div class="m2"><p>نبینی که در پهنه ی کارزار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چه کرده است این کودک خردسال</p></div>
<div class="m2"><p>ابا نامداران با سفت ویال</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>تنی از سواران جنگی نماند</p></div>
<div class="m2"><p>که شمشیر بر مغفر اونراند</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>یکی دیده بگشا دراین پهندشت</p></div>
<div class="m2"><p>ببین چون زتیغش پر از کشته گشت</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>همه لشگر از رزم سیر آمدند</p></div>
<div class="m2"><p>دریده بر – از جنگ شیر آمدند</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>برآشفت طارق به سالار گفت</p></div>
<div class="m2"><p>که ما نا ترا چشم دانش بخفت</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>چنین گفتگو با دلیران چرا</p></div>
<div class="m2"><p>چنین سخره با شرزه شیران چرا</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چو من نامداری سواری دلیر</p></div>
<div class="m2"><p>که بگریزد از حمله اش نره شیر</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ابا رزم نادیده اندک به سال</p></div>
<div class="m2"><p>شماری هماورد ودانی همال</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>من و رزم با کودکان این مباد</p></div>
<div class="m2"><p>که از من کنند این به افسانه یاد</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>عمر گفت با وی که ای پهلوان</p></div>
<div class="m2"><p>که چون تو ندانم تنی از گوان</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>نیای همین کودک پاکرای</p></div>
<div class="m2"><p>علی (ع) بود شمشیر و شیر خدای</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>نه مرحب نه حارث نه عمر وسترگ</p></div>
<div class="m2"><p>نه شیبه نه عتبه سران بزرگ</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>تنی زنده زیشان ز رزمش نجست</p></div>
<div class="m2"><p>زتیغش زجان جمله شستند دست</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>هنوز از زمین های بطحا دیار</p></div>
<div class="m2"><p>بجوشد، نم خون چو دریا کنار</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>مر این خردسال است در کارزار</p></div>
<div class="m2"><p>پدر را زشیر خدا یادگار</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>شگفتی بسی اندرین پهندشت</p></div>
<div class="m2"><p>ز خردان ایشان پدیدار گشت</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>ز مردان جنگی نکو نیست لاف</p></div>
<div class="m2"><p>اگر مردی؟ اینک تو اینک مصاف</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>برو تا ببینی بر و بال اوی</p></div>
<div class="m2"><p>همان حیدری تیغ و چنگال اوی</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>اگر زنده برگشتی از رزمگاه</p></div>
<div class="m2"><p>بخوان خویش را پهلوان سپاه</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>سرش را گر آوردی ایدون به من</p></div>
<div class="m2"><p>سرت را برافرازم از انجمن</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>بخواهم که سازد ترا شهریار</p></div>
<div class="m2"><p>ابر موصل و ورقه، فرمانگذار</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>ز گفتار او گشت طارق دژم</p></div>
<div class="m2"><p>دو جوشن بپوشید در زیر هم</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>به آهن نهان گشت پا تا به سر</p></div>
<div class="m2"><p>سبک جست بر باره ی راهور</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>یکی تیغش اندر میان سر گرای</p></div>
<div class="m2"><p>به دستش یکی نیزه ی جانگزای</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>بغرید کای نو رسیده سوار</p></div>
<div class="m2"><p>ندیده نبرد دلیران کار</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>هنوزت نه گاه نبردست و جنگ</p></div>
<div class="m2"><p>که یازی به مردم کشی تیغ و چنگ</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>چه آتش بد این کش برافروختی</p></div>
<div class="m2"><p>که جان دلیران بدان سوختی</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>به رزم من ایدر یکی پای دار</p></div>
<div class="m2"><p>که اینک سر آید ترا روزگار</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>بگفت این و پیچان سنان کرد راست</p></div>
<div class="m2"><p>تو گفتی سنانش یکی اژدهاست</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>نبرد آزما پور شیر خدای</p></div>
<div class="m2"><p>چو این دید بر زین بیفشرد پای</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>یکی نیزه چون اژدهای کلیم</p></div>
<div class="m2"><p>کزو جان فرعونیان کرد بیم</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>بیفکند بر نیزه ی هم نبرد</p></div>
<div class="m2"><p>به گردون برانگیخت از پهنه گرد</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>بدانسان بگشتند بر گردهم</p></div>
<div class="m2"><p>که گاو زمین را بشد پشت خم</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>دلیران لشگر برآن ترکتاز</p></div>
<div class="m2"><p>به جای دو دیده دهان کرده باز</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>به ناگاه شهزاده ی ارجمند</p></div>
<div class="m2"><p>زکف نیزه ی خودبه یک سو فکند</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>بیازید سر پنجه ی رزمساز</p></div>
<div class="m2"><p>زطارق گرفت آن سنان دراز</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>بیفکندش ا زدست بردشت جنگ</p></div>
<div class="m2"><p>چو طارق بدید این بیازید چنگ</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>یکی تیغ بیرون کشید ازقراب</p></div>
<div class="m2"><p>به خونریزی زاده ی بوتراب</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>چو کرد اهرمن تیغ و بازو بلند</p></div>
<div class="m2"><p>برانگیخت شهزاده ازجا سمند</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>گرفت از هوا دست و تیغش بهم</p></div>
<div class="m2"><p>به سر پنجه بازوی اوداد خم</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>به نیرو بیفشرد دستش چنان</p></div>
<div class="m2"><p>که شد ازبن ناخنش خون روان</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>برآورد از دست او تیغ تیز</p></div>
<div class="m2"><p>به بدخواه شد بسته راه ستیز</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>بزد اسب وبر وی بغرید سخت</p></div>
<div class="m2"><p>به زیر سپر شد نهان تیره بخت</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>گزین زاده ی شه خدا را ستود</p></div>
<div class="m2"><p>بیاورد آن تیغ بر وی فرود</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>زخود وسروسینه اندر گذشت</p></div>
<div class="m2"><p>زتنگ تکاور پدیدار گشت</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>بیفتاد در پهنه ی کارزار</p></div>
<div class="m2"><p>دونیمه ستور ودو پیکر سوار</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>بدان کشته بس تاخت یکران جوان</p></div>
<div class="m2"><p>شکست از پی باره اش استخوان</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>رسید از خدای جهان آفرین</p></div>
<div class="m2"><p>پیاپی بدان دست وتیغ آفرین</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>پیمبر (ص) به مینو برافروخت روی</p></div>
<div class="m2"><p>درود آمد ازشیر یزدان بر اوی</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>شه کربلا از گرانمایه پور</p></div>
<div class="m2"><p>چو دید آن هنرمندی و فر و زور</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>پیاپی خدای جهان را ستود</p></div>
<div class="m2"><p>به پور جوان آفرین ها نمود</p></div></div>