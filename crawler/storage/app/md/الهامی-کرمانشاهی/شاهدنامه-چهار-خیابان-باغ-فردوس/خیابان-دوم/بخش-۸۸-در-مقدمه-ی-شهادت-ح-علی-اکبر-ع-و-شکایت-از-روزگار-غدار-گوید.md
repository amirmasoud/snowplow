---
title: >-
    بخش ۸۸ - در مقدمه ی شهادت ح علی اکبر(ع) و شکایت از روزگار غدار گوید
---
# بخش ۸۸ - در مقدمه ی شهادت ح علی اکبر(ع) و شکایت از روزگار غدار گوید

<div class="b" id="bn1"><div class="m1"><p>چو رزم سپهبد به پایان رسید</p></div>
<div class="m2"><p>سخن باید از رزم اکبر شنید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>برا ای دل از سینه وقت غم است</p></div>
<div class="m2"><p>به هش باش هنگامه ی ماتم است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ولی سخت باید چو خاراستان</p></div>
<div class="m2"><p>که تاب آورد اندرین داستان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یکی خطبه آغازم ایدون به درد</p></div>
<div class="m2"><p>بنالم ازین گنبد گرد گرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>که یک لحظه بر کام نیکان نگشت</p></div>
<div class="m2"><p>بدان را همی نیک بد سر گذشت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به خاک آورد تاج آزاده گان</p></div>
<div class="m2"><p>به کام دل اهرمن زاده گان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به فرجام آن را که خواندنش یار</p></div>
<div class="m2"><p>برآرد ز جان ناگهانش دمار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زکس آب شرمش نیاید به روی</p></div>
<div class="m2"><p>به بازیچه ماند همه کار اوی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>برویاند از باغ شاخی بلند</p></div>
<div class="m2"><p>کند باغبان رابدو پایبند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چو پر مایه گردید وبالا کشید</p></div>
<div class="m2"><p>یکی باد جانکاه آرد پدید</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بپیچد در آن شاخ او بشکند</p></div>
<div class="m2"><p>دل باغبانش پر از خون کند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نه با پیر مهرش نه برنا نه خرد</p></div>
<div class="m2"><p>خنک آنکه مهرش ز خاطر سترد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>الا ای جوانان خورشید چهر</p></div>
<div class="m2"><p>مباشید ایمن ز گشت سپهر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>منازید بر قد دلجوی خویش</p></div>
<div class="m2"><p>وزان پر شکن مشکبو موی خویش</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بسا راست بالا چو تیر آسمان</p></div>
<div class="m2"><p>خم آرد بدو همچو پشت کمان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بسا مهروش چهره ی تابناک</p></div>
<div class="m2"><p>شود کاسته چون مه نو به خاک</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>جوانان بسی بهتر ازما سپهر</p></div>
<div class="m2"><p>بپرورد و از جمله ببرید مهر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>همه خوب دیدار و نیکو سرشت</p></div>
<div class="m2"><p>چو خرم بهار ودل آرا بهشت</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>همه رسته شمشادشان برسمن</p></div>
<div class="m2"><p>زگل بسته آذین به سرو چمن</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>به زندان گورند خوابیده زار</p></div>
<div class="m2"><p>بود روزی آن جمله را مور ومار</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>همان پیکر همچو سرو بلند</p></div>
<div class="m2"><p>جدا گشته از یکدگر بند بند</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>زهر بند ایشان نواها چو نی</p></div>
<div class="m2"><p>برآید که ای نوجوانان حی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>چو دامن کشان سوی ما بگذرید</p></div>
<div class="m2"><p>ز ناکامی ما به یاد آورید</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بدان برز وبالا و آن فر ویال</p></div>
<div class="m2"><p>که داری کنون نوجوانا مبال</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>سر آرد جهان روز برنایی ات</p></div>
<div class="m2"><p>زتن بگسلاند توانایی ات</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>گرت نسترن زار باشد رخان</p></div>
<div class="m2"><p>ورت زلفکان چون بنفشه ستان</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>چو اندر رسد پیری آن نسترن</p></div>
<div class="m2"><p>شود زرد گل وان بنفشه سمن</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>به هر جای هامون که پا می نهی</p></div>
<div class="m2"><p>به عبرت ببین تا کجا می نهی</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>گشایی اگر چشم دل بر جهان</p></div>
<div class="m2"><p>ببینی چه در خاک دارد نهان</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>چه زلفان مشگین چه چشمان مست</p></div>
<div class="m2"><p>چه لب های میگون صهبا پرست</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>بسی تن به زیر زمین گشته خاک</p></div>
<div class="m2"><p>به پاکیزه گی بهتر از جان پاک</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>کجا یوسف و آن دلارا جمال</p></div>
<div class="m2"><p>که از خوبرویان نبودش همال</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>شد از دوری اش چشم یعقوب کور</p></div>
<div class="m2"><p>به بر درکشیدش زلیخای گور</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>کجا احمد مرسل آن جان پاک</p></div>
<div class="m2"><p>که برعرش رفتی چو بر روی خاک</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>کجا مرتضی صاحب انما</p></div>
<div class="m2"><p>که ذاتش بسودی به ذات خدا</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>کجا مجتبی نور چشم بتول</p></div>
<div class="m2"><p>که بر دوش خود می نهادش رسول</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>کجا آن جوانان هاشم نژاد</p></div>
<div class="m2"><p>که درکربلا رفت جانشان به باد</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>سراسر جهان چشم ایشان بدوخت</p></div>
<div class="m2"><p>شگفتا دلش چون بدیشان بسوخت</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>به ویژه جوان شه تشنه کام</p></div>
<div class="m2"><p>علی اکبر آن شبه خیرالانام</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>که چون او جوانی ز مادر نزاد</p></div>
<div class="m2"><p>به دیدار زیبا و روشن نهاد</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>جوانان پس از مرگ این نوجوان</p></div>
<div class="m2"><p>ممانید دلشاد و آسوده جان</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>به رعنا جوانی اگر بگذرید</p></div>
<div class="m2"><p>از آن گهر زیبا به یاد آورید</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>چو شاخ گل تازه جنبد زباد</p></div>
<div class="m2"><p>ز سرو قد اکبر آرید زیاد</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>خداوند هر آشکار و نهان</p></div>
<div class="m2"><p>اگر جز جهان بقا وین جهان</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>طرازد هزاران جهان دگر</p></div>
<div class="m2"><p>چو اکبر نیارد جوان دگر</p></div></div>