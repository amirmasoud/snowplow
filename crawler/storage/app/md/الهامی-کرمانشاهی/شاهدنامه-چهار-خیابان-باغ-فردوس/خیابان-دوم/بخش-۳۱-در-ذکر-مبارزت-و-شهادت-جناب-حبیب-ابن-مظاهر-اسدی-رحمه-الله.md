---
title: >-
    بخش ۳۱ - در ذکر مبارزت و شهادت جناب حبیب ابن مظاهر اسدی رحمه الله
---
# بخش ۳۱ - در ذکر مبارزت و شهادت جناب حبیب ابن مظاهر اسدی رحمه الله

<div class="b" id="bn1"><div class="m1"><p>زهیر از جهان چون بپرداخت جای</p></div>
<div class="m2"><p>حبیب گزین گشت رزم آزمای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مرآن پیر فرخ که بادش درود</p></div>
<div class="m2"><p>زاصحاب پیغمبر پاک بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>که در کودکی شاه بطحا دیار</p></div>
<div class="m2"><p>بپرورده او را بسی در کنار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بسی بوسه داده به چشم و سرش</p></div>
<div class="m2"><p>به هر جمع بوده ستایشگرش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پرستنده ای بود شب زنده دار</p></div>
<div class="m2"><p>شه اولیا را به جان دستیار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کتاب خدا را ز بر داشتی</p></div>
<div class="m2"><p>به طاعت شب و روز بگذاشتی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به هر شب در آن پیر فرخ گهر</p></div>
<div class="m2"><p>یکی ختم قرآن نمودی زبر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>برادرش خواندی همال بتول</p></div>
<div class="m2"><p>وزو شاد بودی روان رسول</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>فزون برگذشته بدو ماه و سال</p></div>
<div class="m2"><p>ز پیری خمیده قدش چون هلال</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چو دستوری آن پیر فرخنده خواست</p></div>
<div class="m2"><p>بدو گفت شاهنشه داد راست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>که زین غم میافزا مرا رنج و درد</p></div>
<div class="m2"><p>همان تا دگر کس بجوید نبرد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نوانی ترا تاب پیگار نیست</p></div>
<div class="m2"><p>شتابت به مرگ خود از بهر چیست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>فراغت مرا پشت می بشکند</p></div>
<div class="m2"><p>همان کوی آباد ویران کند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>یکی کهنه تیغی تو در دست من</p></div>
<div class="m2"><p>روانکاه و تن سوز و زشت اهرمن</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>به مرگ تو ای پیرمرد سره</p></div>
<div class="m2"><p>نگون گرددم رایت میسره</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>مرا یادگاری تو از بوتراب</p></div>
<div class="m2"><p>چو بینمت یاد آورم زان جناب</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بدو گفت پیر ای خداوند من</p></div>
<div class="m2"><p>ببین بر دل آرزومند من</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بر آر از شهادت مرا کام جان</p></div>
<div class="m2"><p>سرافراز گردان به هر دو جهان</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>مرا موی از آن گشت کافور گون</p></div>
<div class="m2"><p>که رنگین شود در رکابت به خون</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>به پیرانه سر نوجوانی کنم</p></div>
<div class="m2"><p>پس از مرگ خود کامرانی کنم</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>جوانان چو دادند جان شادمان</p></div>
<div class="m2"><p>چرا پیر مردان نبازند جان</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>من از کوفه بهر همین آمدم</p></div>
<div class="m2"><p>به جانبازی شاه دین آمدم</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>به ناچار شه داد دستوری اش</p></div>
<div class="m2"><p>اگر چند غمگین بد از دوری اش</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>سوار جوان نیرو و پیر سر</p></div>
<div class="m2"><p>بیامد به میدان و چون شیر نر</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>خروشید کای لشکر سنگدل</p></div>
<div class="m2"><p>که پیغمبر است از شما تنگدل</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>جهانجو حبیب مظاهر منم</p></div>
<div class="m2"><p>شهنشاه را یار و ناصر منم</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>چو آرم به مردی کمر بر میان</p></div>
<div class="m2"><p>نترسم ز صد بیشه شیر ژیان</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>چو شمشیر در پهنه ی کین زنم</p></div>
<div class="m2"><p>سرو ترک مردان به خاک افکنم</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>اگر چند از سالخوردی توان</p></div>
<div class="m2"><p>براز خود ندانم یکی پهلوان</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بسی سرد و گرم جهان دیده ام</p></div>
<div class="m2"><p>من از مرگ هرگز نترسیده ام</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>منم سرفشان تیغ دست یلی</p></div>
<div class="m2"><p>یک جان نثار از حسین علی</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>فرستید گردی به میدان کار</p></div>
<div class="m2"><p>که مردی هر کس شود آشکار</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>بگفت این و بر دشمنان حمله کرد</p></div>
<div class="m2"><p>بیافکند برخاک شصت و دو مرد</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>یکی زشت مرد تمیمی نژاد</p></div>
<div class="m2"><p>بیفکند میدان بر آن کارزار</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>بزد نیزه ای کش تن جنگجوی</p></div>
<div class="m2"><p>نگون آمد از باره ی گرمپوی</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>همی خواست برخیزد از جای خویش</p></div>
<div class="m2"><p>دگر باره گیرد ره جنگ پیش</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>بدیل صریم ازکمین گاه تاخت</p></div>
<div class="m2"><p>سرازنامور پیکرش دور ساخت</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>بیاویخت بر گردن بادپای</p></div>
<div class="m2"><p>روان شد سوی مکه آن تیره رای</p></div></div>