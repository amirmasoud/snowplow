---
title: >-
    بخش ۳۷ - درذکر مبارزت و شهادت عبدالرحمن بن عروه وعبدالله غفاری رحمهما الله
---
# بخش ۳۷ - درذکر مبارزت و شهادت عبدالرحمن بن عروه وعبدالله غفاری رحمهما الله

<div class="b" id="bn1"><div class="m1"><p>چو آن خواجه و بنده ی پاکدین</p></div>
<div class="m2"><p>گرفتند جا در بهشت برین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دومرد غفاری ز یاران شاه</p></div>
<div class="m2"><p>دلیرانه جستند رزم سپاه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یکی عبد رحمن بن عروه بود</p></div>
<div class="m2"><p>که افراشتی سربه چرخ کبود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دگر پور عبدالله نیکنام</p></div>
<div class="m2"><p>که دین را بدی شیر شرزه کنام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به دستوری شه دمان تاختند</p></div>
<div class="m2"><p>به ترک یلان تیغ کین آختند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نبرد آزموده دو خنجر گذار</p></div>
<div class="m2"><p>نمودند با دشمنان کار زار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ازآن پس که خستند تن ها به تیغ</p></div>
<div class="m2"><p>سروجان بدادند خود بی دریغ</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز یزدان بدین نامداران درود</p></div>
<div class="m2"><p>که در راه دین بیم جانشان نبود</p></div></div>