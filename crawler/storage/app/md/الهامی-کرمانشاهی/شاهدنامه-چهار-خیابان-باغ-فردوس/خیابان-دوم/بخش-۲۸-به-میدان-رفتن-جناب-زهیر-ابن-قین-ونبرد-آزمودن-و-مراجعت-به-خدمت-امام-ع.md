---
title: >-
    بخش ۲۸ - به میدان رفتن جناب زهیر ابن قین ونبرد آزمودن و مراجعت به خدمت امام(ع)
---
# بخش ۲۸ - به میدان رفتن جناب زهیر ابن قین ونبرد آزمودن و مراجعت به خدمت امام(ع)

<div class="b" id="bn1"><div class="m1"><p>زهیرابن قین آن سوار نبرد</p></div>
<div class="m2"><p>بیامد برشاه رخ پر زگرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رکاب سمندش ببوسید و گفت</p></div>
<div class="m2"><p>که ای آشکارا بتو هر نهفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چه باشیم و چندان درنگ آوریم</p></div>
<div class="m2"><p>همان به که کوشیم و جنگ آوریم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دمی با سپه ترکتازی کنیم</p></div>
<div class="m2"><p>سپس جاودان سرفرازی کنیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بده رخصتم تا دراین دشت جنگ</p></div>
<div class="m2"><p>کنم چهر ه از خون خود لاله رنگ</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شهنشاه پوزش ازآن مرد راد</p></div>
<div class="m2"><p>پذیرفت و دستوری جنگ داد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به میدان درآمد زهیر دلیر</p></div>
<div class="m2"><p>خروشید برآن سپه همچو شیر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>که ای قوم آهن دل نابکار</p></div>
<div class="m2"><p>به کین با خدا و خداوندگار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>منم چاکری زآستان حسین (ع)</p></div>
<div class="m2"><p>مرانام فرخ – زهیر ابن قین</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مرا گر بر آیم به یکران عزم</p></div>
<div class="m2"><p>چو ایوان بزم است میدان رزم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خوش آندم که در پهنه ی کارزار</p></div>
<div class="m2"><p>کنم جان نثار ره شهریار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ایا نابکاران دور از حیا</p></div>
<div class="m2"><p>حسین(ع) است سبط شه انبیا(ص)</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>شما از چه رو آب شیرین گوار</p></div>
<div class="m2"><p>ببستید بر روی آن شهریار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بگفت این و مانند شیر ژیان</p></div>
<div class="m2"><p>برآهیخت شمشیر گیتی ستان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>تو گفتی که شمشیر او اژدهاست</p></div>
<div class="m2"><p>دمش همچو بارنده ابر بلاست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>به یکدم صد و بیست مرد وسوار</p></div>
<div class="m2"><p>بیافکند ز آن لشکر نابکار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>پس از زرمگه شد وبه نزدیک شاه</p></div>
<div class="m2"><p>بدوآفرین خواند گیتی پناه</p></div></div>