---
title: >-
    بخش ۱۰ - به میدان رفتن حر نبرد آزمون و برگشتن به خدمت امام علیه السلام
---
# بخش ۱۰ - به میدان رفتن حر نبرد آزمون و برگشتن به خدمت امام علیه السلام

<div class="b" id="bn1"><div class="m1"><p>پس از نزد آن داده جان بازگشت</p></div>
<div class="m2"><p>روان سوی شاه سرافراز گشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به پای تکاور نهادش جبین</p></div>
<div class="m2"><p>بگفت: ای خداوند دنیا و دین</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تودانی پسر کشته راتاب نیست</p></div>
<div class="m2"><p>نخواهد پس ازمرگ فرزند زیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نخستین منت بستم ای شاه راه</p></div>
<div class="m2"><p>من آوردمت سوی این رزمگاه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همی خواهم آزاد سازی مرا</p></div>
<div class="m2"><p>دل ازهر غمی شاد سازی مرا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>که اول شهید رکابت شوم</p></div>
<div class="m2"><p>ز سر داده گان جنابت شوم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به جان سرافراز پاکان خویش</p></div>
<div class="m2"><p>به فر و شکوه نیاکان خویش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دلم مشکن ای تاجور شهریار</p></div>
<div class="m2"><p>نبرد سپه را به من واگذار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به رخ چشم مهرش شهنشه گشاد</p></div>
<div class="m2"><p>پس آنگاه دستوری جنگ داد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>برآمد به زین آن سوار نبرد</p></div>
<div class="m2"><p>زهامون به گردون برانگیخت گرد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چو از دور یال هژیر دمان</p></div>
<div class="m2"><p>به چشم آمدش دشمن بد گمان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>زمین پر ز دریای جوشنده دید</p></div>
<div class="m2"><p>همه رزمگه شیر کوشنده دید</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>سپهبد چو دید آن سپه رااز دور</p></div>
<div class="m2"><p>چوشیری که چشم افکند سوی گور</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>به ایشان خروشید و از خشم گفت</p></div>
<div class="m2"><p>که ای لشگر شوم با دیو جفت</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>سپهدار حر ریاحی منم</p></div>
<div class="m2"><p>که سر پنجه با شیر گردون زنم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>منم مرد مهمان نواز عرب</p></div>
<div class="m2"><p>به مردانگی یکه تاز عرب</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>من آنم که بختم ز غم شاد کرد</p></div>
<div class="m2"><p>مرا شهریار من آزاد کرد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>درآن دم که جز جوشنم برگ نیست</p></div>
<div class="m2"><p>هماورد را چاره جز مرگ نیست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چو بر دسته ی تیغ دست آورم</p></div>
<div class="m2"><p>سرترک ترک فلک بردرم</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>به شیر ار برم حمله بی جان شود</p></div>
<div class="m2"><p>وگر اژدها نیز پیچان شود</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>به کین چرمه ام چون بپوید همی</p></div>
<div class="m2"><p>سر سرکشان گور جوید همی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>زآب دم تیغ من زیر گرد</p></div>
<div class="m2"><p>بگردد بسی آسیای نبرد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>وزان نامجوتر نبینی سوار</p></div>
<div class="m2"><p>که باشد سنان مرا پایدار</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>سرنیزه ام جان ستاند همی</p></div>
<div class="m2"><p>دم تیغ من سر فشاند همی</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>اگرهست مردی بیاید به جنگ</p></div>
<div class="m2"><p>ببیند که چونم بود تیغ و چنگ</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>یکی مرد سالار ناپاکرای</p></div>
<div class="m2"><p>به لشگر درش بود رزم آزمای</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ددی بود صفوان ورا نام زشت</p></div>
<div class="m2"><p>بداندیش و پتیاره و بد سرشت</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>زره دار و خنجر زن و تیغ باز</p></div>
<div class="m2"><p>کمان کش سنان افکن و اسب تاز</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>سپر بند و ترکش کش و گرد گیر</p></div>
<div class="m2"><p>دژم خوی و ناپاک رای و شریر</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بدو گفت بن سعد:کای نامجوی</p></div>
<div class="m2"><p>برو با جوان ریاحی بگوی</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>که از ما چرا روی برتافتی؟</p></div>
<div class="m2"><p>چه دیدی که آن سوی بشتافتی؟</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>به مکر و فریبش به سوی من آر</p></div>
<div class="m2"><p>وگرنه سر آور بدو روزگار</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>برون تاخت عفریت پیکر هیون</p></div>
<div class="m2"><p>که نیروی بازو کند آزمون</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>بیامد به حر دلاور بگفت:</p></div>
<div class="m2"><p>کت اندرز من باید اکنون شنفت</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>تو بودی یلی بخرد و هوشیار</p></div>
<div class="m2"><p>شدی با عدوی یزید از چه یار؟</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>نمودی چا با حسین آشتی؟</p></div>
<div class="m2"><p>ز فرمانده ی کوفه رخ کاشتی</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>ز گفتار او شد سپهبد دژم</p></div>
<div class="m2"><p>بسو از غضب دست و دندان به هم</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>بدو گفت: کای زشت نا پاکخوی</p></div>
<div class="m2"><p>فرو بندلختی دم از گفتگوی</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>ستمگر یزید بداندیش کیست؟</p></div>
<div class="m2"><p>عبیدالله شوم بد گیش کیست؟</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>چه مرداست بن سعد پتیاره زاد</p></div>
<div class="m2"><p>که نام و نشانش به گیتی مباد</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>مراگویی از راست ره باز گرد</p></div>
<div class="m2"><p>که ازحق به اهریمن انباز کرد</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>کس از پور پیغمبر نامدار</p></div>
<div class="m2"><p>کشددست بهر یکی نابکار؟</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>که بدکار و شوم و زنازاده است</p></div>
<div class="m2"><p>ره حق پرستی زکف داده است</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>ازیدر مپیمای دیگر سخن</p></div>
<div class="m2"><p>به رزم آمدی جنگ را ساز کن</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>هم اکنون به تو خورد خواهد دریغ</p></div>
<div class="m2"><p>سرو پیکر و جوشن و خود وتیغ</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>ستمگر چو این دید با ترکتاز</p></div>
<div class="m2"><p>سنان کرد سوی دلاور دراز</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>سپهند چو شیر ژیان بی درنگ</p></div>
<div class="m2"><p>بزد نیزه بر نیزه ی مرد جنگ</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>شکست آن سنان رازهم بند بند</p></div>
<div class="m2"><p>پس آنگه به چالاکی آن ارجمند</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>همان نیزه کش بد به دست استوار</p></div>
<div class="m2"><p>بزد بر تهیگاه تازی سوار</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>گرفتش زین و زدش برزمین</p></div>
<div class="m2"><p>بدو گفت سالار دین آفرین</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>بردار سه بودش یک از یک بتر</p></div>
<div class="m2"><p>همه پر دل و گرد و پرخاشگر</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>به یک ره سوی پهنه درتاختند</p></div>
<div class="m2"><p>به فرخ سپهدار تیغ آختند</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>ریاحی نسب مردناورد جوی</p></div>
<div class="m2"><p>بدان هر سه با تیغ بنهاد روی</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>یکی را بزد چنگ و از زین ربود</p></div>
<div class="m2"><p>بینداخت سوی سپهر کبود</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>به کتف دگر یک چنان تیغ راند</p></div>
<div class="m2"><p>که نیمش بر پشت توسن بماند</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>دگر یک زبیم دلاور گریخت</p></div>
<div class="m2"><p>سپهبد ز پی رفت و خونش بریخت</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>پس آنگاه از پهنه شد سوی شاه</p></div>
<div class="m2"><p>درون پر نیایش زبن عذر خواه</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>شهنشه بدو گفت: کای پاکزاد</p></div>
<div class="m2"><p>خدای جهان از تو خوشنود باد</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>بچم سوی پیکار و دلشاد باش</p></div>
<div class="m2"><p>چو نام خود از دوزخ آزاد باش</p></div></div>