---
title: >-
    بخش ۹۳ - رفتن ح – علی اکبر به وداع امام سجاد وبازگشت به خدمت پدر
---
# بخش ۹۳ - رفتن ح – علی اکبر به وداع امام سجاد وبازگشت به خدمت پدر

<div class="b" id="bn1"><div class="m1"><p>به بالین اوگریه ها سرنمود</p></div>
<div class="m2"><p>همی چهره بر پای سجاد سود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شه خسته چون بوی اکبر شنید</p></div>
<div class="m2"><p>زجاجست و تنگش به بر برکشید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به بدرود هم در خروش آمدند</p></div>
<div class="m2"><p>همی ناله ازنای دل برزدند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>امام امم سیدالساجدین</p></div>
<div class="m2"><p>به فرخ برادرش گفت اینچنین</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>که ای لعل تو عیسی جان من</p></div>
<div class="m2"><p>بدین درد جانکاه درمان من</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>طبیبانه بنشین به بالین من</p></div>
<div class="m2"><p>بکن چاره ی درد دیرین من</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مرا جان ز دیدارت آمد به تن</p></div>
<div class="m2"><p>مرو تا بماند به تن جان من</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به پوزش خم آورد بالا جوان</p></div>
<div class="m2"><p>بر آن جهان داور ناتوان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>که ای حجت دادگر کردگار</p></div>
<div class="m2"><p>پس از تاجور باب در روزگار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>پی آنکه برخی کنم جان تو را</p></div>
<div class="m2"><p>سر خود کنم گوی چوگان ترا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>پدر داده دستوری ام بهر جنگ</p></div>
<div class="m2"><p>که سازم زخون چهره یاقوت رنگ</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تو نیزم اجازت بده چون پدر</p></div>
<div class="m2"><p>به پیگار این لشگر بد گهر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>که گاه فدا گشتم دیر شد</p></div>
<div class="m2"><p>دل من زجان و جهان سیرشد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چو از شاه بیمار دستور یافت</p></div>
<div class="m2"><p>دگر باره سوی شهنشه شتافت</p></div></div>