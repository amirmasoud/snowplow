---
title: >-
    بخش ۸۱ - بی ادبی نمودن ابن زیاد بد
---
# بخش ۸۱ - بی ادبی نمودن ابن زیاد بد

<div class="b" id="bn1"><div class="m1"><p>چه دندان و لب؟ آنکه خیرالبشر</p></div>
<div class="m2"><p>بدان بوسه می داد شام و سحر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چه لب ها؟ که بوسید روح الامین</p></div>
<div class="m2"><p>ز روی نیازش به عرش برین</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یکی پیر بود اندر آن انجمن</p></div>
<div class="m2"><p>ز یاران پیغمبر موتمن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>که او بود زید ابن ارقم به نام</p></div>
<div class="m2"><p>بسی برده سر، با رسول انام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو دید از بد اندیش آن کار زشت</p></div>
<div class="m2"><p>بشد خشمگین، بیمش ازدل بهشت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خروشید و گفتا به بیدادگر</p></div>
<div class="m2"><p>که آخر بکن شرمی از دادگر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مزن چوب بر لب، چنین شاه را</p></div>
<div class="m2"><p>زماهی مکن تیره تا ماه را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>که من خود بدیدم ز حد بیشتر</p></div>
<div class="m2"><p>همی زد بدان بوسه، خیرالبشر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>لبی کان زیان نبی را مکید</p></div>
<div class="m2"><p>کجا می توان زیر چوب تو دید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بگفت این و بگریست پس زار زار</p></div>
<div class="m2"><p>غو گریه برخاست از هر کنار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بداختر بدو گفت: ای بی خرد</p></div>
<div class="m2"><p>چرا می زنی بهر ما فال بد؟</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چو درشادی ما نباشی توشاد</p></div>
<div class="m2"><p>تو را چشم، بی گریه هرگز مباد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چو پیری رسد راست قد، خم شود</p></div>
<div class="m2"><p>خرد مرد آگاه را کم شود</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>همان سستی رای و موی سپید</p></div>
<div class="m2"><p>زشمشیر خشم منت بر خرید</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>وگرنه بفرمودی تا سرت</p></div>
<div class="m2"><p>بگیرند از این بی خرد پیکرت</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بدو پیر گفتا: که ای نابکار</p></div>
<div class="m2"><p>بداندیش پیغمبر و کردگار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>تو را گر بد آمد زگفتار من</p></div>
<div class="m2"><p>به جانت زنم آتش از این سخن</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بدیدم همی شاه لولاک را</p></div>
<div class="m2"><p>حسن (ع) راواین کشته ی پاک را</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>به زانو نشاند از یمین و یسار</p></div>
<div class="m2"><p>بگفتا: که ای پاک پروردگار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>من این هر دو نوباوه ی خویش را</p></div>
<div class="m2"><p>که هستند مرهم دل ریش را</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>سپردم به تو از بد بدکنشت</p></div>
<div class="m2"><p>همی تا در آیند اندر بهشت</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>همی خواهشم هست اسلامیان</p></div>
<div class="m2"><p>کز این دو بدارند دست زبان</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>کس آزارد ار زاده گان مرا</p></div>
<div class="m2"><p>چنان دادن که آزرده جان مرا</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>تو اکنون چنان دان که با چوب کین</p></div>
<div class="m2"><p>زنی بر لب سیدالمرسلین (ص)</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بگفت این و برخاست مردکهن</p></div>
<div class="m2"><p>برون رفت گریان از آن انجمن</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>همی گفت: کای کوفیان پلید</p></div>
<div class="m2"><p>چه بد برشما از پیمبر رسید؟</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>که کشتید فرزانه فرزند او</p></div>
<div class="m2"><p>همه دوده و خویش و پیوند او</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>به جایش گزیدید بیگانه را</p></div>
<div class="m2"><p>همان پور ناپاک مرجانه را</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>که گیرد روان از روان شما</p></div>
<div class="m2"><p>شویدش چو بنده بدان شما</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>پس از رفتن زید، پور زیاد</p></div>
<div class="m2"><p>سر شاه را برگرفت از عناد</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>زمانی بدان روی و مو، خیره شد</p></div>
<div class="m2"><p>یکی هول از آن بردلش چیره شد</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>نماند ایچ نیرو در آن بد نهاد</p></div>
<div class="m2"><p>سر پاک شه را به زانو نهاد</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>چکید از سر شه یکی قطره خون</p></div>
<div class="m2"><p>به رانش، وزآنسوی آمد برون</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>فرو رفت برخاک آن خون پاک</p></div>
<div class="m2"><p>همی تاکه بد زنده، بد درد ناک</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>وز آن بوی بد آمدی بر مشام</p></div>
<div class="m2"><p>نهادی برآن مشک هر صبح و شام</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>ولی بوی گندش همی شد فزون</p></div>
<div class="m2"><p>چو گندی که آمد ز زخم هیون</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>بد اختر بشد تیره زان، دستبرد</p></div>
<div class="m2"><p>به دست اندرش بود یک تیغ خرد</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>نگویم بدان تیغ با شه چه کرد؟</p></div>
<div class="m2"><p>دل مصطفی (ص) را نیارم به درد</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>حرم را سپس گفت آن بد گمان</p></div>
<div class="m2"><p>برد، جا به زندان دهد روزبان</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>به نزدیک مسجد یکی خانه بود</p></div>
<div class="m2"><p>بسی سال بود آن که ویرانه بود</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>درآن خانه آل علی (ع) را مقام</p></div>
<div class="m2"><p>بدادند چون شد جهان، نیلفام</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>به کاشانه ی چرخ برگرد ماه</p></div>
<div class="m2"><p>گرفتند چو اختران جایگاه</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>گزیدند برگرد دخت بتول (س)</p></div>
<div class="m2"><p>به ویرانه، جا کودکان رسول</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>هم آغوششان یاد جان های پاک</p></div>
<div class="m2"><p>خورش، خون دل بو و بستر، زخاک</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>سرشک سوان بود برجای آب</p></div>
<div class="m2"><p>نه درجسم، تاب و نه درچشم، خواب</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>دلا خویش را خوش به گیتی مساز</p></div>
<div class="m2"><p>که او بد نواز است و نیکوگذار</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>گمان بهی، بر زمانه مبر</p></div>
<div class="m2"><p>که او بد نواز است و نیکو گداز</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>گمان بهی، بر زمانه مبر</p></div>
<div class="m2"><p>که این باغ را نیست جز رنج، بر</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>شنیدی که با آل حیدر چه کرد</p></div>
<div class="m2"><p>مراین چرخ پر فتنه ی گرد گرد</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>به ویرانه جا داد آن را کز اوی</p></div>
<div class="m2"><p>شد اینگونه گردنده و تیز پوی</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>چو با داور خود چنین کرد، کار</p></div>
<div class="m2"><p>تو امید آسایش از وی مدار</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>یکی گوش بگشا بر این داستان</p></div>
<div class="m2"><p>که گیتی چه ها کرد با راستان</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>ز زندان مغرب چو کرد آفتاب</p></div>
<div class="m2"><p>سوی کوی و بازار مشرق شتاب</p></div></div>