---
title: >-
    بخش ۱۶ - پاکشیدن سپاه کوفه و شام از مبارزات امام
---
# بخش ۱۶ - پاکشیدن سپاه کوفه و شام از مبارزات امام

<div class="b" id="bn1"><div class="m1"><p>چنان تاختش شه به میدان کین</p></div>
<div class="m2"><p>که لرزید برخویش گاو زمین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به گرد اندر آمد سرماه و مهر</p></div>
<div class="m2"><p>بترسید بر خویش شیر سپهر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به تن زهره ی کوهسار آب شد</p></div>
<div class="m2"><p>زمین همچو جنبنده سیماب شد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همه آفرینش پر از ویله گشت</p></div>
<div class="m2"><p>غو خاکیان ز آسمان درگذشت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دهان اژدر مرگ بنمود باز</p></div>
<div class="m2"><p>جهان را دم آخر آمد فراز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نخستین سوی میمنه حمله کرد</p></div>
<div class="m2"><p>برآورد از دشت ناورد گرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو یک لخت بر آن سپه راند تیغ</p></div>
<div class="m2"><p>فرو ریخت سرها چو بارنده میغ</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>همه پهنه شد پر سرو پا و دست</p></div>
<div class="m2"><p>علم با علمدار گردید پست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ملخ چون رمد از پی باره گی</p></div>
<div class="m2"><p>پراکنده گشتند یکباره گی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گریزنده از بیم شمشیر شاه</p></div>
<div class="m2"><p>نیارست کردن پس خود نگاه</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز پیشی گرفتن ز بهر فرار</p></div>
<div class="m2"><p>پیاده شدی پایمال از سوار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به چشم یلان روشنایی نماند</p></div>
<div class="m2"><p>پدر را به پور آشنایی نماند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ز چنگ سواران بیفتاد تیغ</p></div>
<div class="m2"><p>همی هر کسی خورد برخود دریغ</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>پس سرکشان خسرو ارجمند</p></div>
<div class="m2"><p>درخشان پرند و خروشان سمند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>همی راند تیغ و همی گفت: هان</p></div>
<div class="m2"><p>کجا می گریزند ای گمرهان؟</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بسی مرگ بهتر بود بهر مرد</p></div>
<div class="m2"><p>که بنشیند از ننگ بر روش گرد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ولیکن بسی به بود ننگ و عار</p></div>
<div class="m2"><p>چو نامش کشد سوی دوزخ مهار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>تهی شد چو از کوفیان دست راست</p></div>
<div class="m2"><p>سوی دست چپ تیغ شه گشت راست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بپاشید از هم صف میسره</p></div>
<div class="m2"><p>به خاک اندر افکند جمعی سره</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>همی گفت:کای مردم تیره بخت</p></div>
<div class="m2"><p>به یزدان بخشنده سوگند سخت</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بخوردم که دریاری این زنان</p></div>
<div class="m2"><p>نتابم رخ از تیر و تیغ وسنان</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>صف میسره از دم تیغ شاه</p></div>
<div class="m2"><p>نهادند رخ سوی قلب سپاه</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>چو قلب و جناح سپه شد یکی</p></div>
<div class="m2"><p>به پیکار شد گرم شاه اندکی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>دم ذوالفقارش چو ابر بهار</p></div>
<div class="m2"><p>ببارید خون اندر آن کارزار</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>همی تا بدانجا که رفتی نگاه</p></div>
<div class="m2"><p>پر از کشته شد دشت آوردگاه</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>به گردون بر آمد غو الفرار</p></div>
<div class="m2"><p>به هر سو دوان باره ی بی سوار</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>دل خصم در بر چو سیماب ناب</p></div>
<div class="m2"><p>بلرزیدی و پایش اندر رکاب</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>تن سرکشان را کفن گشت برگ</p></div>
<div class="m2"><p>سراسیمه شد چرخ و بیچاره مرگ</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>شد از کار دست قضا و قدر</p></div>
<div class="m2"><p>اجل ناتوان خفت در رهگذر</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>پراکنده گشتند یکسر سپاه</p></div>
<div class="m2"><p>چو از باد صرصر یکی مشت کاه</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>بلی چون خدا خود نمایی کند</p></div>
<div class="m2"><p>که یارد که رزم آزمایی کند؟</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>ببین تا چه بیند به خود روزگار</p></div>
<div class="m2"><p>چو دست خدا برکشد ذوالفقار</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>گذشته از آن کان شه داد راست</p></div>
<div class="m2"><p>بیفکند برخاک آن را که خاست</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>روان همه بر لب استاده بود</p></div>
<div class="m2"><p>به فرمان وی گوش بنهاده بود</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>که گر یک اشاره کند بیدرنگ</p></div>
<div class="m2"><p>به دوزخ گرایند از آن جای تنگ</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>چو لختی شهنشاه پیگار کرد</p></div>
<div class="m2"><p>بشد پهنه پر کشته ی اسب و مرد</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>به میدان عنان تافت از قلبگاه</p></div>
<div class="m2"><p>باستاد در پیش روی سپاه</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>بزد تکیه بر نیزه کز رنج جنگ</p></div>
<div class="m2"><p>بیاساید آن شاه پیروز چنگ</p></div></div>