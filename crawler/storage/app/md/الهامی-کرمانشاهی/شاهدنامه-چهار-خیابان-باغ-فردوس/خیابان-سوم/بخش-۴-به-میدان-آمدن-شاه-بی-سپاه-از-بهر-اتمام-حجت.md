---
title: >-
    بخش ۴ - به میدان آمدن شاه بی سپاه از بهر اتمام حجت
---
# بخش ۴ - به میدان آمدن شاه بی سپاه از بهر اتمام حجت

<div class="b" id="bn1"><div class="m1"><p>بیامد به پیش سپاه ایستاد</p></div>
<div class="m2"><p>به اتمام حجت، زبان برگشاد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بفرمود: آیا در این پهندشت</p></div>
<div class="m2"><p>کسی هست کز جان تواند گذشت؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کسی هست، کآید مددکار من</p></div>
<div class="m2"><p>در این بی سپاهی شود یار من؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کسی هست کز خشم پروردگار</p></div>
<div class="m2"><p>بترسد، هراسد ز انجام کار؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بگرداند از آل احمد (ص) بدی</p></div>
<div class="m2"><p>به نیکی سپارد ره ایزدی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به راه خدا جانفشانی کند</p></div>
<div class="m2"><p>هم از دشمنش جانستانی کند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو برخاست از شهریار، این ندا</p></div>
<div class="m2"><p>که جان همه دوستانش، فدا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نخست آمد از ایزدی بارگاه</p></div>
<div class="m2"><p>خطابی بدو کای جهان را پناه</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تویی عاشق من در این داوری</p></div>
<div class="m2"><p>زمن خواه اگر بایدت یاوری</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در این بی پناهی پناهت منم</p></div>
<div class="m2"><p>ظفر بخش در رزمگاهت منم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بخواه آنچه خواهی زدادار خویش</p></div>
<div class="m2"><p>مدان هیچ کس را مدد کار خویش</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>پس از روح پاک فرستاده گان</p></div>
<div class="m2"><p>به پاسخ بدان شاه آزاده گان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چنین آمد از باغ مینو درود</p></div>
<div class="m2"><p>که ای پادشاه فراز و فرود</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بگو تا بیاییم زی کربلا</p></div>
<div class="m2"><p>تو را یار باشیم در هر بلا</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>هم آغوش گردیم با کشته گان</p></div>
<div class="m2"><p>به راه تو از خویش بگذشته گان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>روان نبی (ص) نیز با درد جفت</p></div>
<div class="m2"><p>به پور گرانمایه «لبیک »گفت</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>که ای بی سپه مانده فرزند من</p></div>
<div class="m2"><p>خزاندیده شاخ برومند من</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بگو تا بیایم ز خلد برین</p></div>
<div class="m2"><p>پی یاری تو به روی زمین</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>کنم جان خود برخی جان تو</p></div>
<div class="m2"><p>سر خویش بازم به میدان تو</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>از آن پس بیامد ز شیر خدا</p></div>
<div class="m2"><p>به فرزند از بام عرش این ندا</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>که ای پور آزاده ی سرفراز</p></div>
<div class="m2"><p>گرت هست بر یاری من نیاز</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بگو تا بیایم در آن کارزار</p></div>
<div class="m2"><p>به کار آورم جوهر ذوالفقار</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>رسیدش هم از مجتبی (ع) این خروش</p></div>
<div class="m2"><p>درآن دشت پر محنت و غم به گوش</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>که ای با من انباز و همتا ویار</p></div>
<div class="m2"><p>برادر زمام و پدر یادگار</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بگو تا که آیم به سوی تو من</p></div>
<div class="m2"><p>ببازم سر خود به کوی تو من</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>چو عباس (ع) و قاسم (ع) فدایت شوم</p></div>
<div class="m2"><p>شهید صف کربلایت شوم</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>پس از اولیا این ندا گشت راست</p></div>
<div class="m2"><p>که برمات ای شاه فرمان رواست</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بگو تا بدانجا سپه برکشیم</p></div>
<div class="m2"><p>زخصمت به شمشیر کیفر کشیم</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>ز روشن روان شهیدان پیش</p></div>
<div class="m2"><p>که بودند با آن خداوند خویش</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>درود آمدش کای خداوندگار</p></div>
<div class="m2"><p>بگو تا گراییم زی کارزار</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>به راه تو بازیم جان ها مگر</p></div>
<div class="m2"><p>شود روی ما پیش حق سرخ تر</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>همه ساکنان سپهر و زمین</p></div>
<div class="m2"><p>زبگدشته و آینده ی مومنین</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>همه آفرینش ز خوب و ز زشت</p></div>
<div class="m2"><p>چه اهل جهنم چه اهل بهشت</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>همه اذن یاری ز شه خواستند</p></div>
<div class="m2"><p>به جانبازی اش پوزش آراستند</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>شهنشاه جز ایزد دادراست</p></div>
<div class="m2"><p>درآن رزم از آن جمله یاری نخواست</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>پس آنگاه آن شاه با دین و داد</p></div>
<div class="m2"><p>دمی تکیه بر نیزه ی خویش داد</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>شهیدان خود را همی بنگریست</p></div>
<div class="m2"><p>از آنان همی یاد کرد وگریست</p></div></div>