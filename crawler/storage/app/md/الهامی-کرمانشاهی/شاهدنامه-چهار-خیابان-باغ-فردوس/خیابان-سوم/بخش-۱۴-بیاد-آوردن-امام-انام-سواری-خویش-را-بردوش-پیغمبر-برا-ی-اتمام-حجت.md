---
title: >-
    بخش ۱۴ - بیاد آوردن امام انام سواری خویش را بردوش پیغمبر برا ی اتمام حجت
---
# بخش ۱۴ - بیاد آوردن امام انام سواری خویش را بردوش پیغمبر برا ی اتمام حجت

<div class="b" id="bn1"><div class="m1"><p>و دیگر به یثرب یکی روز نو</p></div>
<div class="m2"><p>بیامد روان ها به عشرت گرو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همه خردسالان آن سرزمین</p></div>
<div class="m2"><p>بپوشیده زربفت ابریشمین</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یکایک ابر اشتری راهوار</p></div>
<div class="m2"><p>به بازیچه درکوی و برزن سوار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>درآن روز، من باگرامی حسن (ع)</p></div>
<div class="m2"><p>برفتیم نزد رسول زمن (ص)</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بگفتیم کای پادشاه حجاز</p></div>
<div class="m2"><p>درین روز جنشی نو آمد، فراز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همه کودکان با دلی شاد خوار</p></div>
<div class="m2"><p>سوارند بر ناقه ی راهوار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نداریم ما ناقه ی تندتاز</p></div>
<div class="m2"><p>که هستیم شهزاده گان حجاز</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پیمبر (ص) گلوی من ولعل او</p></div>
<div class="m2"><p>ببوسید و گفت:ای دو پاکیزه خو</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بود برهیون گر نیاز شما</p></div>
<div class="m2"><p>منم ناقه ی سرفراز شما</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بگفت این و کرد از یمین و یسار</p></div>
<div class="m2"><p>ابردوش فرخنده ما را سوار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بگفتیم:کای شاه هفت و چهار</p></div>
<div class="m2"><p>چرا ناقه ی ما ندارد مهار؟</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>پیمبر (ص) دوگیسوی چون مشک ناب</p></div>
<div class="m2"><p>گشود از سرو کرد پر پیچ وتاب</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به ما داد و گفتا: مهارست این</p></div>
<div class="m2"><p>چو شد دست ما جای حبل المتین</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بگفتیم:کای شاه گیتی پناه</p></div>
<div class="m2"><p>چرا ناقه ی ما نپوید به راه؟</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بیامد شهنشاه در هروله</p></div>
<div class="m2"><p>بیفتاد در قدسیان ولوله</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>دگر باره گفتیم: کای مقتدا</p></div>
<div class="m2"><p>چرا ناقه ی ما ندارد صدا؟</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>به خوشنودی ما رسول عرب (ص)</p></div>
<div class="m2"><p>به آوای العفو بگشود لب</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>به ناگه سروشش بگفتا: اگر</p></div>
<div class="m2"><p>بگویی تو العفو بار دگر</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>به جوش آوری بخشش کردگار</p></div>
<div class="m2"><p>شود دوزخ افسرده و سرد نار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>کنون آن تنی را که گشته سوار</p></div>
<div class="m2"><p>ابر دوش پیغمبر تاجدار</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>شما از چه خواهید با تیغ کین</p></div>
<div class="m2"><p>بریزید خونش دراین سرزمین؟</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>اگر من نه فرزند پیغمبرم (ص)</p></div>
<div class="m2"><p>چرا هست دستار او برسرم؟</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>همان جوشن او تنم راست رخت</p></div>
<div class="m2"><p>سپردارم از حمزه ی (ع) نیکبخت</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>میان بسته دارم به تیغ پدر</p></div>
<div class="m2"><p>که نامش بود ذوالفقار دو سر</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چه سازید اگر شکوه ها از شما</p></div>
<div class="m2"><p>نمایند اینان به روز جزا؟</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>که کشتید یار و تبار مرا</p></div>
<div class="m2"><p>همان دوده ی نامدار مرا</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>کنون بهر قتل من آماده اید</p></div>
<div class="m2"><p>به خونریزی ام سخت استاده اید</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بود همره من حریم رسول (ص)</p></div>
<div class="m2"><p>همان پرده گی دختران بتول</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>و دیگر بسی کودک خردسال</p></div>
<div class="m2"><p>زاولاد پیغمبر (ص) بی همال</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>که جز من ندارند فریاد رس</p></div>
<div class="m2"><p>نه محرم نه مونس نه غمخوارکس</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>گذارید ازین دشت پر شور و شر</p></div>
<div class="m2"><p>برمشان به سوی دیار دگر</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>به بطحا و یثرب اگر نیست بار</p></div>
<div class="m2"><p>نهم سر سوی روم یا زنگبار</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>و یا آبی ای مردم پر جفا</p></div>
<div class="m2"><p>ببخشید به عترت مصطفی (ص)</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>که از تشنگی دست شسته زجان</p></div>
<div class="m2"><p>ندارید امید آن براین این بر آن</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>زگفتار آن داور بی سپاه</p></div>
<div class="m2"><p>برآمد زماهی فغان تا به ماه</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>به لشگر درافتاد افغان وشور</p></div>
<div class="m2"><p>فرو ریختند از مژه آب شور</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>زدل برکشیدند آنسان نوا</p></div>
<div class="m2"><p>که پرناله شد پهنه ی نینوا</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>همه باره گی ها به زیر سوار</p></div>
<div class="m2"><p>گرستند بر غربت شهریار</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>چنان شد که در پهنه ی رزمگاه</p></div>
<div class="m2"><p>پراکنده گشتند یکسره سپاه</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>چو این دید شمر آن زنازاده مرد</p></div>
<div class="m2"><p>زلشگر برون رفت و فریاد کرد</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>که ای پور لب تشنه ی بوتراب</p></div>
<div class="m2"><p>نخواهیم دادن یکی قطره آب</p></div></div>