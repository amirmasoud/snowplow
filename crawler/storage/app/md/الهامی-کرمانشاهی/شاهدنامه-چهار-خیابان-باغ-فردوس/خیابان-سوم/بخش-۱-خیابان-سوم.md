---
title: >-
    بخش ۱ - خیابان سوم
---
# بخش ۱ - خیابان سوم

<div class="b" id="bn1"><div class="m1"><p>یگانه خداوند رابنده ایم</p></div>
<div class="m2"><p>به جان ودل اورا پرستنده ایم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که دم های بربسته ی ما گشاد</p></div>
<div class="m2"><p>هم او دادمان هر چه بایست داد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نخواهیم جز وی کسی را خدای</p></div>
<div class="m2"><p>جز او برجهان نیست فرمانروای</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دراندیشه هر چت بگنجد نه اوست</p></div>
<div class="m2"><p>که او برتر ازوهم و ازگفت وگوست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جز او ذات اورا که داند که چیست؟</p></div>
<div class="m2"><p>مرا او را بجز او شناسنده کیست؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ازو باد بگزیده گان را درود</p></div>
<div class="m2"><p>که آمد سروش از خداشان فرود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به ویژه مهین داور انبیا</p></div>
<div class="m2"><p>فروزنده ی فره ی کبریا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جهان را فرستاده ی واپسین</p></div>
<div class="m2"><p>به هر آفریننده ای او گزین</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>محمد (ص) که یزدان بدو تاج داد</p></div>
<div class="m2"><p>همش تیغ و منشور و معراج داد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بدو می سزد گرنیایش کنیم</p></div>
<div class="m2"><p>همان دوده اش را ستایش کنیم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نخستین برادرش حیدر که بود</p></div>
<div class="m2"><p>جهان را خدیو از فراز و فرود</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دگر جفت با یازده پور او</p></div>
<div class="m2"><p>کشان آفرید ایزد از نور او</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به ویژه گزین شاه وسالار عشق</p></div>
<div class="m2"><p>شه لشگر آرای پیکار عشق</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نگهبان تخت شهادت حسین (ع)</p></div>
<div class="m2"><p>جهان شرف خسرو خافقین</p></div></div>