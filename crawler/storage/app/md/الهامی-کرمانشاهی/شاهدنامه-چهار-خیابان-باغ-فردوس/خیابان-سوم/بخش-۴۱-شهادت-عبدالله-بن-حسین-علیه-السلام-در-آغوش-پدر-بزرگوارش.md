---
title: >-
    بخش ۴۱ - شهادت عبدالله بن حسین علیه السلام در آغوش پدر بزرگوارش
---
# بخش ۴۱ - شهادت عبدالله بن حسین علیه السلام در آغوش پدر بزرگوارش

<div class="b" id="bn1"><div class="m1"><p>یکی پور بودش به پرده سرای</p></div>
<div class="m2"><p>سمن بوی، گل روی و خورشید رای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل شاه آکنده از مهر او</p></div>
<div class="m2"><p>رخ روشنش تازه با چهر او</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نرفته بر او سال بیش از چهار</p></div>
<div class="m2"><p>کجا خوانده عبداللهش شهریار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>درآن دم دلش کرد یاد پدر</p></div>
<div class="m2"><p>زخرگه سوی پهنه بنهاد سر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دویدی به هر سوی جویای باب</p></div>
<div class="m2"><p>به گردن ز جذب نهانش طناب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کشیدش اجل تا بدانجا زمام</p></div>
<div class="m2"><p>که بد خفته بر خاک فرخ امام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پدر را چو شهزاده زانگونه دید</p></div>
<div class="m2"><p>غمین گشت و از دل خروشی کشید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به صندوق علم خدایی نشست</p></div>
<div class="m2"><p>بیفکند بر گردن باب، دست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بدان چهر پر خون بسایید روی</p></div>
<div class="m2"><p>به خون لعلگون کرد رخسار و موی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بگفتا به زاری که ای باب من</p></div>
<div class="m2"><p>شکیب دل و جان بی تاب من</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>زما از چه ره روی بنهفته ای؟</p></div>
<div class="m2"><p>بر این گرم خاک، از چه رو خفته ای؟</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>که زد اینهمه زخم بر پیکرت؟</p></div>
<div class="m2"><p>چرا گشته پرخاک و خون افسرت؟</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ز خرگاه خود پا کشیدی چرا؟</p></div>
<div class="m2"><p>که بر تو پدید آید این ماجرا</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ز هجران تو عمه ی دل فگار</p></div>
<div class="m2"><p>ندارد به جز ناله ی زار، کار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بود خواهرم چشم بر راه تو</p></div>
<div class="m2"><p>بگردد همی گرد خرگاه تو</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>به سایه بچم از بر آفتاب</p></div>
<div class="m2"><p>به دلجویی بانوان کن شتاب</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چو تو خفته باشی به خاک اینچنین</p></div>
<div class="m2"><p>شود خیمه ات غارت اهل کین</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>پسندی چو من کودکی خردسال</p></div>
<div class="m2"><p>شود زیر سم ستم پایمال؟</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>همی کرد شیرین زبانی و تیر</p></div>
<div class="m2"><p>کشید از بر باب روشن ضمیر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>شهنشاه از خاک برداشت سر</p></div>
<div class="m2"><p>کشیدش چو جای گرامی به بر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ببوسیدش آن روی خورشید وش</p></div>
<div class="m2"><p>دگر آن لب نیلگون از عطش</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>نهادش لب پر زخون برگلوی</p></div>
<div class="m2"><p>همی دست سایید بر روی و موی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بدو گفت: کای نور چشم ترم</p></div>
<div class="m2"><p>سرور روان الم پرورم</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ز خرگه بدین سو چرا تاختی؟</p></div>
<div class="m2"><p>دل مادر از درد بگداختی</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>مکن بیش از این ناله ی دلخراش</p></div>
<div class="m2"><p>برو مونس مادر پیر باش</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>که این بدمنش قوم را شرم نیست</p></div>
<div class="m2"><p>به دل از خدا هیچ آزرم نیست</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>نبخشند برما ز خرد و بزرگ</p></div>
<div class="m2"><p>بدرند از هم چو درنده گرگ</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>توکی تاب شمشیر دارد تنت؟</p></div>
<div class="m2"><p>برو تا نگشته خبر دشمنت</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>مرا بیش از این در شکنجه مخواه</p></div>
<div class="m2"><p>که مرگت کند روز بر من سیاه</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>شهنشاهزاده به پیش پدر</p></div>
<div class="m2"><p>زلب تشنگی شکوه بنمود سر</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>که ای باب از تشنگی سوختم</p></div>
<div class="m2"><p>چو خاشاک از آتش بیفروختم</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>به تیغ ار بریزند خونم زبر</p></div>
<div class="m2"><p>مرا هست از این تشنگی سهل تر</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>به کامم رسان جرعه ی آب سرد</p></div>
<div class="m2"><p>بهل تا در آید سرم زیر گرد</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>از آن آبخواهی شه انس و جان</p></div>
<div class="m2"><p>تو گفتی که افتادش آتش به جان</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>فرو ماند در کار، فرزند راد</p></div>
<div class="m2"><p>به رخ از مژه سیل خون برگشاد</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>دراین حال بد شاه با پور خویش</p></div>
<div class="m2"><p>به ناگاه دد گوهری زشت کیش</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>ز لشگر به بالین شه درگذشت</p></div>
<div class="m2"><p>ز افغان او دید پر ناله دشت</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>به چشم آمدش کودکی خردسال</p></div>
<div class="m2"><p>که نالد چو مرغان بشکسته بال</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>ز نرگس چکد ژاله بر لاله اش</p></div>
<div class="m2"><p>بسوزد دل سنگ بر ناله اش</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>به رخ بسته از آب مژگان دو جوی</p></div>
<div class="m2"><p>لبش آب گوی و دلش تاب جوی</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>به گوشش دو آویزه چون ماه نو</p></div>
<div class="m2"><p>رخش برده از مهر تابان گرو</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>شده زعفرانی گل و روی او</p></div>
<div class="m2"><p>پریشان بر و سنبل مشکبو</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>زخون پدر لعلگون کاکلش</p></div>
<div class="m2"><p>شده شاخه ی ارغوان سنبلش</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>بدیدش چو دژخیم ناهوشمند</p></div>
<div class="m2"><p>براو تاخت چون دیو بگسسته بند</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>سرو دست شهزاده بگرفت سخت</p></div>
<div class="m2"><p>که دورش کند از شه نیکبخت</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>گرفتش شهنشاه دست دگر</p></div>
<div class="m2"><p>که برجای خود باز دارد مگر</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>پلید ستم پیشه زینسان چو دید</p></div>
<div class="m2"><p>به سختی ز آغوش شاهش کشید</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>بر دیده ی شاه دنیا و دین</p></div>
<div class="m2"><p>کشیدش به خاشاک و خار و زمین</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>زپس، روی، شهزاده بر خاک سود</p></div>
<div class="m2"><p>شد آن چهره ی ارغوانش کبود</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>بپیچید مویش به دست آن لعین</p></div>
<div class="m2"><p>برآوردش از جای و زد بر زمین</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>کشید از میان خنجر آبدار</p></div>
<div class="m2"><p>شهنشه همی دید و بگریست زار</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>یکی پای بردوش فرزند شاه</p></div>
<div class="m2"><p>نهاد و همی کرد آن شه نگاه</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>گرفتی ز نخدانش آن کینه جوی</p></div>
<div class="m2"><p>شهنشه ز فرزند برتافت روی</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>دلش بر نتابید آن درد را</p></div>
<div class="m2"><p>مگر بد زآهن دل آن مرد را</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>که ببرید در پیش روی امام</p></div>
<div class="m2"><p>ز پیکر سر پور او تشنه کام</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>دریغا از آن کودک ماهرو</p></div>
<div class="m2"><p>که شد سر جدا از تن پاک او</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>شگفتا ز تاب و توان امام</p></div>
<div class="m2"><p>زهی صبر فرزند خیرالانام</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>سپهرا چرا می نگشتی خراب؟</p></div>
<div class="m2"><p>نگشتی چرا تیره ای آفتاب؟</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>جهانا چو با داور خود چنین</p></div>
<div class="m2"><p>نمودی به ما تا چه سازی زکین</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>خنک آنکه بر مهر تو دل نبست</p></div>
<div class="m2"><p>به مردی ز بند تو نامرد جست</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>پس از قتل شهزاده ی بی گناه</p></div>
<div class="m2"><p>دگر باره از خویشتن رفت شاه</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>بدانسان چو یک لخت بگذشت باز</p></div>
<div class="m2"><p>به خویش آمد و کرد بیننده باز</p></div></div>