---
title: >-
    بخش ۱۱۸ - در مناجات بدرگاه قاضی الحاجات گوید
---
# بخش ۱۱۸ - در مناجات بدرگاه قاضی الحاجات گوید

<div class="b" id="bn1"><div class="m1"><p>خدایا به یعقوب آل رسول (ص)</p></div>
<div class="m2"><p>بدان یوسف مصر جان بتول (س)</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که درچاه زندان دوزخ تنم</p></div>
<div class="m2"><p>مسوزان به جنت بده مسکنم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خود این دانم ای داور رهنمون</p></div>
<div class="m2"><p>که دارم گنه ز آفرینش فزون</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سبک تر بود لیک از پر کاه</p></div>
<div class="m2"><p>برکوه بخشایشت آن گناه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چه غم با چنان بخشش بی شمار</p></div>
<div class="m2"><p>من از تیره گی دارم کردگار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به ویژه که خواهشگرم هست شاه</p></div>
<div class="m2"><p>در آن گرم روز انداران پیشگاه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بدین نامه من یکجهان تیره کار</p></div>
<div class="m2"><p>چون خود سوی مینو کشانم زنار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو این نامه من یکجهان تیره کار</p></div>
<div class="m2"><p>چون خود سوی مینو کشانم زنار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو این نامور نامه پرداختم</p></div>
<div class="m2"><p>تن از بند دوزخ رها ساختم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>جهان پادشاها تو کامم بر آر</p></div>
<div class="m2"><p>مسازم بر مردمان شرمسار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>برآمد ازین نامه چون کام من</p></div>
<div class="m2"><p>چو آغاز نیکو کن انجام من</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به شاهنشهان فراز و فرود</p></div>
<div class="m2"><p>ز پروردگار جهانبان درود</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به یزدان بخشنده از من سپاس</p></div>
<div class="m2"><p>که چندان روان مرا داشت پاس</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>که سیم خیابان بپرداختم</p></div>
<div class="m2"><p>به گردون سر فخر افراختم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ببردم بسی اندرین نامه رنج</p></div>
<div class="m2"><p>نه از بهر نام و زر و مال و گنج</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>امیدم به فضل خدا بود و بس</p></div>
<div class="m2"><p>نبد یاورم غیر او هیچ کس</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>به هر بیت در شان این خاندان</p></div>
<div class="m2"><p>خدا خانه ای وعده داد از جنان</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>امید آنکه من برخورم زین نوید</p></div>
<div class="m2"><p>ز بخشایش حق نگردم نمید</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>خدایا به خیرم نما خاتمه</p></div>
<div class="m2"><p>به حق گزین دوده ی فاطمه (ع)</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>مرا بخش چندان بگیتی زمان</p></div>
<div class="m2"><p>گشاده دل و طبع و گویا زبان</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>که این نامور نامه گردد تمام</p></div>
<div class="m2"><p>بدانسان که بپذیرد او را امام</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>کنون این نیوشنده گفتار نو</p></div>
<div class="m2"><p>ز چارم خیابان تو از من شنو</p></div></div>