---
title: >-
    بخش ۷۴ - خطبه خواندن حضرت زینب (س) در خارج کوفه
---
# بخش ۷۴ - خطبه خواندن حضرت زینب (س) در خارج کوفه

<div class="b" id="bn1"><div class="m1"><p>سر از برج محمل چو مهر از سپهر</p></div>
<div class="m2"><p>برون کرد و گفتا پر از خشم چهر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که هان ای گروه تبه روزگار</p></div>
<div class="m2"><p>که پیوسته تان بر کژی رفته کار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چه دارید افغان و زاری؟ خموش</p></div>
<div class="m2"><p>ببندید دم باز دارید گوش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز فرمان بانو، سپاه ستم</p></div>
<div class="m2"><p>بماندند برجای خود بسته دم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گره کوس را نعره شد درگلوی</p></div>
<div class="m2"><p>دهان بست، شیپور ازهای و هوی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زبان جرس لال شد در دمش</p></div>
<div class="m2"><p>علم، خشک شد در هوا پرچمش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گلوگاه ها بسته شد بر نفس</p></div>
<div class="m2"><p>فتاد از هوا بانگ پر مگس</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>توگفتی که آفاق، ویرانه گشت</p></div>
<div class="m2"><p>روان ها زتن، پاک، بیگانه گشت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سپس بانوی دین سخن ساز کرد</p></div>
<div class="m2"><p>به نام خدا خطبه آغاز کرد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تو گفتی مگر باب او حیدر است</p></div>
<div class="m2"><p>که برپاک یزدان ستایشگر است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خداوند و باب و نیا را ستود</p></div>
<div class="m2"><p>بدانسان که از وی سزاوار بود</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>سپس گفت: ای کوفیان دو رنگ</p></div>
<div class="m2"><p>که کردار تان جمله ریو است و رنگ</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>برآرید از آل احمد (ص) دمار</p></div>
<div class="m2"><p>پس آنگه برایشان بگریید زار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>مبادا تهی چشمهاتان زاشک</p></div>
<div class="m2"><p>که برچشم بی گریه آرید رشک</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>همه کارتان رنج و اندوه باد</p></div>
<div class="m2"><p>به گیتی نمانید یک روز شاد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>شما را چو آن زن بود داستان</p></div>
<div class="m2"><p>که سازد همی پنبه را ریسمان</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چه بدها که نفس و هوای شما</p></div>
<div class="m2"><p>نهاده است در پیش پای شما</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>روا برشما گشته خشم خدای</p></div>
<div class="m2"><p>به دوزخ شما راست پیوسته جای</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>مرا این ناله و گریه از بهر کیست؟</p></div>
<div class="m2"><p>شما را به خود زار باید گریست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بخندید اندک بگریید بیش</p></div>
<div class="m2"><p>از این ننگ کامد شما را به پیش</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>گر ازگریه سازید خود را هلاک</p></div>
<div class="m2"><p>نگردید از رنگ این ننگ، پاک</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>سر نامتان اندر آمد به خواب</p></div>
<div class="m2"><p>نشوید چنین ننگ را هیچ آب</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>که برسبط احمد کشیدند تیغ</p></div>
<div class="m2"><p>نخوردید بر رهبر خود دریغ</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>که شد کشته با تیر و زوبین و خشت</p></div>
<div class="m2"><p>سر نوجوانان خرم بهشت</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>دلیری نمودید با شاه خویش</p></div>
<div class="m2"><p>پناه و نماینده ی راه خویش</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>به جاه بلند پیمبر، شکست</p></div>
<div class="m2"><p>فکندید و شد نام اسلام پست</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>سرا پرده اش را زکین سوختید</p></div>
<div class="m2"><p>وزآن بهر خود آتش افروختید</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بدا روزگار شما، زین گناه</p></div>
<div class="m2"><p>که شد رنجتان در ره دین تباه</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>همه سودتان با زیان بر، یکی است</p></div>
<div class="m2"><p>شما را به خود زار باید گریست</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>برید از خدا، دست های شما</p></div>
<div class="m2"><p>بشد خشم یزدان سزای شما</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>هلا کوفیان خاک غمتان به سر</p></div>
<div class="m2"><p>که هستید ازکار خود بی خبر</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>یکی نیک بینید درکار خویش</p></div>
<div class="m2"><p>چه کردند با شاه و سالار خویش</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>ببینید تا خود ز خیرالابشر</p></div>
<div class="m2"><p>بریدید ازکین، کدامین جگر؟</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>چه کردند با پرده گی های او؟</p></div>
<div class="m2"><p>ببردید از خود چه سان آبرو؟</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>چه خون ها که از وی فرو ریختند؟</p></div>
<div class="m2"><p>چه آشوب در دین برانگیختند؟</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>از این کرده ی زشت نادل پسند</p></div>
<div class="m2"><p>چه لب ها که مردم به دندان گزند</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>شگفتی نباشد که از این ستم</p></div>
<div class="m2"><p>شکافد زمین، کوه، پاشد زهم</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>و یا آسمان خون ببارد به خاک</p></div>
<div class="m2"><p>ز اندوه این ماتم دردناک</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>ولیکن بود کیفر آن جهان</p></div>
<div class="m2"><p>بسی سخت تر، نیست بر ما نهان</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>که فردا ندارید فریاد رس</p></div>
<div class="m2"><p>رسد خشم یزدان چو از پیش و پس</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>نباشید خوشدل بدین روز چند</p></div>
<div class="m2"><p>که بینید کامی ز چرخ بلند</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>که پیش خدا از دمی کمتر است</p></div>
<div class="m2"><p>کسی از بر داد یزدان نرست</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>خدا درکمین ستمکارهاست</p></div>
<div class="m2"><p>ورا با بد اندیش دین کارهاست</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>چو گفتار بانو درآمد به بن</p></div>
<div class="m2"><p>ازآن نغز گفتار و محکم سخن</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>هر آنکس که بشنید شد درشگفت</p></div>
<div class="m2"><p>سرانگشت حیرت به دندان گرفت</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>یکی مرد پیر از میان گروه</p></div>
<div class="m2"><p>بگریید چون ابر نیسان به کوه</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>به زاری بگفتا: که ای دخت شاه</p></div>
<div class="m2"><p>سخن راست گفتی و هستم گواه</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>فدای شما باب و مامم که هست</p></div>
<div class="m2"><p>مقام شما برتر از هر چه هست</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>جوانتان زهر نوجوان بهتر است</p></div>
<div class="m2"><p>به هر پیر پیرانتان مهتر است</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>از این کار ما را یکی ننگ خاست</p></div>
<div class="m2"><p>که آن داستان تا قیامت به جاست</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>بدا حال ما کوفیان، زین گناه</p></div>
<div class="m2"><p>که شد روی ما در دو گیتی سیاه</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>سپس فاطمه دختر شهریار</p></div>
<div class="m2"><p>که از خون داماد بودش نگار</p></div></div>