---
title: >-
    بخش ۲ - درستایش شمس المشرقین حضرت ابا عبدالله الحسین علیه السلام
---
# بخش ۲ - درستایش شمس المشرقین حضرت ابا عبدالله الحسین علیه السلام

<div class="b" id="bn1"><div class="m1"><p>پسین رهبر از پنج آل عبا</p></div>
<div class="m2"><p>شهید ستم شاه گلگون قبا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شهی پر جبریل پیراهنش</p></div>
<div class="m2"><p>به پاکی پر از روح قدسی تنش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هنوزش به قنداقه بد بسته پای</p></div>
<div class="m2"><p>که شد مهد او تا به عرش خدای</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از آتش ببردند سوی فلک</p></div>
<div class="m2"><p>که پایش ببوسند خیل ملک</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو این نوگل از باغ احمد برست</p></div>
<div class="m2"><p>بشد کار یزدان شناسی درست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بدو معنی عشق کرد آشکار</p></div>
<div class="m2"><p>هم او بود معشوق پروردگار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گذشت ازسرو جان و مال وعیال</p></div>
<div class="m2"><p>پی دیدن حضرت ذوالجلال</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بکشتند او را غریب از دیار</p></div>
<div class="m2"><p>نه لشگر ورا بود ونه دستیار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نه کس داشت پیمان اورا نگاه</p></div>
<div class="m2"><p>نه در بی پناهیش بد یک پناه</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نه پاس جلالش نگه داشتند</p></div>
<div class="m2"><p>نه در بی پناهیش بد یک پناه</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نه پاس جلالش نگه داشتند</p></div>
<div class="m2"><p>نه بر زاری اش گوش بگذاشتند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نه برزخم او مرهمی جز سرشک</p></div>
<div class="m2"><p>نه بر خستگی غیر تیغش پزشک</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نه تابوت جز ریگ گرم زمین</p></div>
<div class="m2"><p>نه کافور جز تربت عنبرین</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نه ماتمگری جز وحوش وطیور</p></div>
<div class="m2"><p>نه تن پوش جز نعل سم ستور</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نکردش کفن کس به فرخنده تن</p></div>
<div class="m2"><p>مگر خون که بودش به جای کفن</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>زمین گشت سیراب از خون او</p></div>
<div class="m2"><p>خود او تشنه لب رفت دربزم هو</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ندانم به سربود او را چه شور</p></div>
<div class="m2"><p>که گه بود دردیر و گه درتنور</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گه آویزه چون میوه بد بر درخت</p></div>
<div class="m2"><p>گهی افسر نیزه ی شوربخت</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چو یک نیزه شد سوی بالا سرش</p></div>
<div class="m2"><p>عیان گشت معراج پیغمبرش</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چو پیمود با سر همی راه عشق</p></div>
<div class="m2"><p>بشد خون او خون یزدان پاک</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>گرفت از خداوند خود خونبها</p></div>
<div class="m2"><p>به ملک بقا خلعت کبریا</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چو ازدشمنان اندر آخر نفس</p></div>
<div class="m2"><p>بسی خواست آب ونپذیرفت کس</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>خدایش به هنگامه ی داوری</p></div>
<div class="m2"><p>ببخشید منشور خواهشگری</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چو از رنج بد در دلش گنج ها</p></div>
<div class="m2"><p>بشد تربتش داروی رنج ها</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بود قبه ی پاکش اندر زمین</p></div>
<div class="m2"><p>مطاف ملک همچو عرش برین</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>زکعبه فزون باشدش جاه و آب</p></div>
<div class="m2"><p>دعاها همه اندر آن مستجاب</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>به امید خلق فرود و فراز</p></div>
<div class="m2"><p>به درگاه او سوده روی نیاز</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>خدایا در آن درگهم خاک کن</p></div>
<div class="m2"><p>زهر بد روان مرا پاک کن</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>ببخشا روایی به گفتار من</p></div>
<div class="m2"><p>که از رزم شه باز رانم سخن</p></div></div>