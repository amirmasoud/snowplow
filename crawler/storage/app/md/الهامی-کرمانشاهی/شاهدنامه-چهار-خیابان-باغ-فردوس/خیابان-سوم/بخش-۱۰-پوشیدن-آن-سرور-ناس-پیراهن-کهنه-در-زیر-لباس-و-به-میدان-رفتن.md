---
title: >-
    بخش ۱۰ - پوشیدن آن سرور ناس پیراهن کهنه در زیر لباس و به میدان رفتن
---
# بخش ۱۰ - پوشیدن آن سرور ناس پیراهن کهنه در زیر لباس و به میدان رفتن

<div class="b" id="bn1"><div class="m1"><p>شهنشه چو با دختر این زار گفت</p></div>
<div class="m2"><p>به خواهر از آن پس چنین بازگفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که پیراهنی دیده بس روزگار</p></div>
<div class="m2"><p>بدانسان که کس را نیاید به کار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بیاور به من تاکه در زیر رخت</p></div>
<div class="m2"><p>بپوشانمش برتن ای نیکبخت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>که چون دشمن از من برد ساز و برگ</p></div>
<div class="m2"><p>مگر او بماند مرا رخت مرگ</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بیاورد آن بانوی مهر ورز</p></div>
<div class="m2"><p>یکی جامه ی تنگ بی قدر وارز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شهش گفت: این جامه ی سوگوار</p></div>
<div class="m2"><p>بود پوشش آنکه گشته است خوار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>من ار چند تنها و بی یاورم</p></div>
<div class="m2"><p>نه خوارم بر آورده ی داورم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دگر جامه آور فراخ و بلند</p></div>
<div class="m2"><p>که پوشد ورا مردم ارجمند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بیاورد بانو به افغان وآه</p></div>
<div class="m2"><p>دگر جامه زانسان که فرمود شاه</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شهنشه در آن جامه افکند چاک</p></div>
<div class="m2"><p>بپوشیدش اندر بر تابناک</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دگر جامه و جوشنش بر زبر</p></div>
<div class="m2"><p>بپوشید و بر بست محکم کمر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ز گفتار و کردار آن شهریار</p></div>
<div class="m2"><p>همه پرده گی ها گرستند زار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>شهنشاهشان کرد خامش به پند</p></div>
<div class="m2"><p>سپس گفت گریان به بانگ بلند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>کسی هست تا؟ آورد پیش من</p></div>
<div class="m2"><p>سمند مرا اندرین انجمن</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نبد کس که اسب آورد بهر شاه</p></div>
<div class="m2"><p>به پا خاست زینب به افغان و آه</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>برفت و بیاورد اسب نیا</p></div>
<div class="m2"><p>بر شاه دین خسرو اولیا</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>گرفتند خونین دلان یک به یک</p></div>
<div class="m2"><p>همه گرد آن باره ی تیز تک</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>به دست این رکاب آن عنانش گرفت</p></div>
<div class="m2"><p>از آن حال شد زینب به افغان و آه</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بگفتا: که ای یادگار مهان</p></div>
<div class="m2"><p>دگر خواهری جز من اندر جهان؟</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>زبهر برادر کشید اسب پیش</p></div>
<div class="m2"><p>که تاوی بتازد سوی مرگ خویش؟</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>شهنشاه گریان بر آمد به زین</p></div>
<div class="m2"><p>بدو مویه گر بانوان گزین</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چنین بود اگر حال در آشکار</p></div>
<div class="m2"><p>ولی در نهان بد دگرگونه کار</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>عنان دار او بود روح الامین</p></div>
<div class="m2"><p>دوان قابض روح اندر یمین</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>مکاییل بگرفته او را رکاب</p></div>
<div class="m2"><p>قضا و قدر پیشرو با شتاب</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>سرافیل با صور نوبت زنش</p></div>
<div class="m2"><p>گذشته ز عرش برین گردنش</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>زخیمه روان گشت زی دشت کین</p></div>
<div class="m2"><p>عیان دست یزدانش در آستین</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>زاسبش پدیدار فر براق</p></div>
<div class="m2"><p>ز بانگ سمش ناله ی الفراق</p></div></div>