---
title: >-
    بخش ۱۸ - آمدن گروهی دیگر از جنیان و منصوره باسپاهی از ملک به یاوری امام انام
---
# بخش ۱۸ - آمدن گروهی دیگر از جنیان و منصوره باسپاهی از ملک به یاوری امام انام

<div class="b" id="bn1"><div class="m1"><p>نبخشید دستورشان شاه دین</p></div>
<div class="m2"><p>هم آنان برفتند از آن سرزمین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از آن پس فرود آمدند از فلک</p></div>
<div class="m2"><p>سپاهی بدان سرزمین از ملک</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بدند آن سپه چار و بیور هزار</p></div>
<div class="m2"><p>ابر ناقه های بهشتی سوار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پی یاری آن خداوند فرد</p></div>
<div class="m2"><p>بر آراسته تن به رخت نبرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>علم هایشان جمله از نور بود</p></div>
<div class="m2"><p>برایشان سپهدار منصور بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کشیدند قدسی سپه هر طرف</p></div>
<div class="m2"><p>رده در رده پره در پره صف</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سپهدار منصور از آن سپاه</p></div>
<div class="m2"><p>سوی مرکز شاه پیمود راه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به پوزش ز پشت بهشتی سمند</p></div>
<div class="m2"><p>فرود آمد آن قدسی ارجمند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بزد بوسه بر سم یکران شاه</p></div>
<div class="m2"><p>بگفت: ای شه آفرینش پناه</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>من و این سپه ز آسمان آمدیم</p></div>
<div class="m2"><p>به خدمت گزاری چمان آمدیم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>رسانیده جان آفرینت سلام</p></div>
<div class="m2"><p>تو را داده زان پس بدینسان پیام</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>که من از تو خوشنودم وشادمان</p></div>
<div class="m2"><p>فرستاد اینک سپه ز آسمان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>که یار تو باشند در این نبرد</p></div>
<div class="m2"><p>بر آرند از دشمنان تو گرد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نسازم هم از پایگاه تو کم</p></div>
<div class="m2"><p>سر مویی ای پادشاه امم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>من اینک میان بسته ام استوار</p></div>
<div class="m2"><p>که در این نبردت شوم دستیار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بدو گفت فرمانده ی آب و خاک</p></div>
<div class="m2"><p>که ای پیک یکتا خداوند پاک</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>مگر خفته ای؟ هان! توبیدار شو</p></div>
<div class="m2"><p>برو عاجزان را مدد کار شو</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بر آنی کسی را کنی یاوری</p></div>
<div class="m2"><p>که از بنده گی یافته داوری</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>سپهر و زمین زیر فرمان اوست</p></div>
<div class="m2"><p>جهان بسته ی بند پیمان اوست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>تو از امر من بآسمان زیستی</p></div>
<div class="m2"><p>اگر من نباشم تو خود کیستی؟</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>منت بر گشودم به پرواز پر</p></div>
<div class="m2"><p>منت تاج نصرت نهادم به سر</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>زامر جهاندار یزدان من</p></div>
<div class="m2"><p>روان ها بود زیر فرمان من</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>اگر یک اشارت کنم در زمان</p></div>
<div class="m2"><p>برآید ازین کالبدها روان</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>نه بیچاره از کثرت دشمنم</p></div>
<div class="m2"><p>که از دوست عهدی است برگردنم</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>منم کشته ی عشق جانان خویش</p></div>
<div class="m2"><p>نپیچم سر از عهد و پیمان خویش</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>تو یاری به عشق ابد چون کنی؟</p></div>
<div class="m2"><p>خداوند خود را مدد چون کنی؟</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>برو من نخواهم بجز دوست یار</p></div>
<div class="m2"><p>مرا بس بود یار من دستیار</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>دراین دشت باید که من سردهم</p></div>
<div class="m2"><p>چو بی سر شدم بر سر افسر نهم</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>جهانی که خود در پناه من است</p></div>
<div class="m2"><p>کجا در خور دستگاه من است؟</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بباید کشم رخت از این جهان</p></div>
<div class="m2"><p>زنم خرگه خویش در لامکان</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>چو منصور از شاه رخصت نیافت</p></div>
<div class="m2"><p>ابا لشکرش سوی بالا شتافت</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>به درگاه یزدان بگفت آفرین</p></div>
<div class="m2"><p>بگفت آنچه بشنید از شاه دین</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>خطاب آمد او را ز عرش برین</p></div>
<div class="m2"><p>که بار دگر تاز سوی زمین</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>درآنجا که او را بود جایگاه</p></div>
<div class="m2"><p>پرستنده اش باش با این سپاه</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>به زوار قبرش پرستار باش</p></div>
<div class="m2"><p>برایشان همی نور رحمت بپاش</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>به فرمان دادار با آن سپاه</p></div>
<div class="m2"><p>دگر باره منصور با اشک وآه</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>ز گردنده گردان فرود آمدند</p></div>
<div class="m2"><p>در آن سرزمین با درود آمدند</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>هم اکنون به گرد حریم حسین (ع)</p></div>
<div class="m2"><p>شب و روز گردند با شور و شین</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>همی نور از قبه ی شهریار</p></div>
<div class="m2"><p>نشانند بر عرش پروردگار</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>چو رفتند آنان شه داد راست</p></div>
<div class="m2"><p>از آن کشن لشکر هماورد خواست</p></div></div>