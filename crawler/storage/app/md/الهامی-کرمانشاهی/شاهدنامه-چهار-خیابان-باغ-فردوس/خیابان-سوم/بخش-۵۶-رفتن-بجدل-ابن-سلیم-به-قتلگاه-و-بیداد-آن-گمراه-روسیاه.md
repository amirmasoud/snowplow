---
title: >-
    بخش ۵۶ - رفتن بجدل ابن سلیم به قتلگاه و بیداد آن گمراه روسیاه
---
# بخش ۵۶ - رفتن بجدل ابن سلیم به قتلگاه و بیداد آن گمراه روسیاه

<div class="b" id="bn1"><div class="m1"><p>نبد هیچ کس نزد آن کشتگان</p></div>
<div class="m2"><p>نه دشمن، نه غمخوار و نه پاسبان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیامد ددی بد رگ و زشت خیم</p></div>
<div class="m2"><p>که بد ناماو بجدل ابن سلیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همی گشت در قتلگه بیدرنگ</p></div>
<div class="m2"><p>که افتد مگر چیزی او را به چنگ</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو نومید شد بازگشت او به خشم</p></div>
<div class="m2"><p>که ناگه فتادش سوی شاه چشم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به انگشت او دید انگشتری</p></div>
<div class="m2"><p>درخشان چو در آسمان مشتری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بکوشید کارد ز دستش برون</p></div>
<div class="m2"><p>نیامد که بد خشک گشته به خون</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سر اهرمن از غضب خیره گشت</p></div>
<div class="m2"><p>هم از آز چشم خرده تیره گشت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>یکی خنجر آبگون برکشید</p></div>
<div class="m2"><p>برآن قفل بر بسته، کردش کلید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شدی کاشکی خشک انگشت من</p></div>
<div class="m2"><p>و یا خون شدی کلک در مشت من</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مرا این جانگزا قصه ننوشتمی</p></div>
<div class="m2"><p>که اندر جهان تخم غم کشتمی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دریغا از آن داور جم خدم</p></div>
<div class="m2"><p>که از کف نگین داد و انگشت هم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>اگر باب او در ره بی نیاز</p></div>
<div class="m2"><p>یک انگشتری داد اندر نماز</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>مر این پور راد از کرم گستری</p></div>
<div class="m2"><p>ببخشید انگشت و انگشتری</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>کشیدی چو او دست از هر چه هست</p></div>
<div class="m2"><p>نه سر ماند برجا نه انگشت دست</p></div></div>