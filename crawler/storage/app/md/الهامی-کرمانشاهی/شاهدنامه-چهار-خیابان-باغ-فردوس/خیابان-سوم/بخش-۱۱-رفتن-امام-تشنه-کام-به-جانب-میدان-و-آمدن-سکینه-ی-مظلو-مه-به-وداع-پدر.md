---
title: >-
    بخش ۱۱ - رفتن امام تشنه کام به جانب میدان و آمدن سکینه ی مظلو مه به وداع پدر
---
# بخش ۱۱ - رفتن امام تشنه کام به جانب میدان و آمدن سکینه ی مظلو مه به وداع پدر

<div class="b" id="bn1"><div class="m1"><p>چو شد دور لختی ز پرده سرای</p></div>
<div class="m2"><p>ستاد اسب پیغمبر رهنمای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نمی رفت هرچ اش برانگیخت شاه</p></div>
<div class="m2"><p>که بد دست حق بسته پایش زراه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همی برخروشید و افشاند دم</p></div>
<div class="m2"><p>همی بر زمین کوفت رویینه سم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>که شاها ز رفتار دارم معاف</p></div>
<div class="m2"><p>که خفته است بردست من کوه قاف</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگر چند در پیش فرمان شاه</p></div>
<div class="m2"><p>سبک آیدم کوه چون پر کاه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مراین کوه از عرش سنگین تراست</p></div>
<div class="m2"><p>که عرش آفرین را گزین دختر است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شهنشاه سوی زمین بنگریست</p></div>
<div class="m2"><p>که نارفتن باره بیند که چیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گزین دخت را دید افتاده زار</p></div>
<div class="m2"><p>گرفته سم اسب را درکنار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>فرود آمد از باره گی بی درنگ</p></div>
<div class="m2"><p>کشیدش چوجان اندر آغوش تنگ</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>همی دست بر روی و مویش بسود</p></div>
<div class="m2"><p>سکینه (س) به زاری همی بر فزود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>زمانی بدانگونه بگذشت کار</p></div>
<div class="m2"><p>دل شه ز شوق شهادت فگار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نه او خود ز شه دست برداشتی</p></div>
<div class="m2"><p>نه آزردنش شه رواداشتی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>درآندم به آغوش فرخنده باب</p></div>
<div class="m2"><p>ربودش به امر خداوند خواب</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چو یک لخت بگذشت بیدار شد</p></div>
<div class="m2"><p>روان آب چشمش به رخسار شد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>زدامان شه خویش را دور کرد</p></div>
<div class="m2"><p>بدو گفت: بشتاب سوی نبرد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>شهنشه بفرمودش ای جان باب</p></div>
<div class="m2"><p>روانت چه دید؟ اندرین طرفه خواب</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>که تا این زمان داشتی اشک و آه</p></div>
<div class="m2"><p>نهشتی که تازم به جنگ سپاه</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>کنونم برانگیزی از بهر جنگ</p></div>
<div class="m2"><p>همی زاری آری که منما درنگ</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بگفتا: در این دم که خوابم ربود</p></div>
<div class="m2"><p>پیمبر (ص) به من روی فرخ نمود</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بفرمود کز دامن باب دست</p></div>
<div class="m2"><p>رها کن میفکن به عزمش شکست</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بهل تا رود سوی میدان کین</p></div>
<div class="m2"><p>که این است فرمان جان آفرین</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بگفت این و گریان سوی خیمه گاه</p></div>
<div class="m2"><p>برفت و نشست از بر باره شاه</p></div></div>