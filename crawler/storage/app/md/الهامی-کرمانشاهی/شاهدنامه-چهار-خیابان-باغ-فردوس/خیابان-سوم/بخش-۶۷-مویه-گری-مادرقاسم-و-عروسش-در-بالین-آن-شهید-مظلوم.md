---
title: >-
    بخش ۶۷ - مویه گری مادرقاسم و عروسش در بالین آن شهید مظلوم
---
# بخش ۶۷ - مویه گری مادرقاسم و عروسش در بالین آن شهید مظلوم

<div class="b" id="bn1"><div class="m1"><p>یکی گفتی ای سرو بستان من</p></div>
<div class="m2"><p>بپرورده از شیر دامان من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که بنمود دور از کنار منت؟</p></div>
<div class="m2"><p>که رنگین به خون کرد زاینسان تنت؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>که بر نو جوانیت رحمت نکرد؟</p></div>
<div class="m2"><p>مرا کرد همخوابه ی داغ و دود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یکی گفتی ای از حسن (ع) یادگار</p></div>
<div class="m2"><p>سرور دل مادر داغدار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تو رفتی که باز آیی از کارزار</p></div>
<div class="m2"><p>نهادی مرا دیده در انتظار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کنون کامدم من به بالین تو</p></div>
<div class="m2"><p>کشیدم به بر جسم خونین تو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چرا پیش مادر نگویی سخن؟</p></div>
<div class="m2"><p>زبدها که دیدی ز چرخ کهن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خدا را ایا پور نادیده کام</p></div>
<div class="m2"><p>یکی پاسخ آور به غمدیده مام</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>یکی دیده بگشا به روی عروس</p></div>
<div class="m2"><p>ببین در غمت دردریغ و فسوس</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دلم را از این بیش غمگین مخواه</p></div>
<div class="m2"><p>سخن گوی با ناز پرورد شاه</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>یکی گفتی ای محرم راز من</p></div>
<div class="m2"><p>در آن دم که بودی تو دمساز من</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بگفتی نگیرم دل از مهر تو</p></div>
<div class="m2"><p>نبندم دمی دیده از چهر تو</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ز رویم کنون از چه بستی نگاه؟</p></div>
<div class="m2"><p>که از سیلی شمر بینی سیاه</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>زمن پرس کو یاره و معجرت</p></div>
<div class="m2"><p>چرا چاک شد گوش و عریان برت؟</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>به ناگاه چشم غمین دخت شاه</p></div>
<div class="m2"><p>سکینه فتاد اندر آن جایگاه</p></div></div>