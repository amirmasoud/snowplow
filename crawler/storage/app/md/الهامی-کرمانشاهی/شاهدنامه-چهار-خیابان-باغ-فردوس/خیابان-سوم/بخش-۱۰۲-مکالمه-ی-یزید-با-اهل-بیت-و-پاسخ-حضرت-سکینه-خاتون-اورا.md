---
title: >-
    بخش ۱۰۲ - مکالمه ی یزید با اهل بیت و پاسخ حضرت سکینه خاتون اورا
---
# بخش ۱۰۲ - مکالمه ی یزید با اهل بیت و پاسخ حضرت سکینه خاتون اورا

<div class="b" id="bn1"><div class="m1"><p>ببردندشان بسته دریک کمند</p></div>
<div class="m2"><p>همه زار و گریان و بازو به بند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بپوشیده با آستین روی خویش</p></div>
<div class="m2"><p>فکنده سر از شرم دشمن بپیش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یکایک از ایشان بپرسید نام</p></div>
<div class="m2"><p>همی تا رسید او به دخت امام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بپرسید کاین دخت موینده کیست</p></div>
<div class="m2"><p>گرفته برخ دست خود بهر چیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو فرخ سکینه زوی آن شنفت</p></div>
<div class="m2"><p>خروشید و زارید و با وی بگفت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>که ما را زکین تو ای زشت رای</p></div>
<div class="m2"><p>کهن چادری هم نمانده به جای</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>که با وی توان روی خود را ببست</p></div>
<div class="m2"><p>رخ خود بپوشیم زانرو به دست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>که هستیم در نزد نامحرمان</p></div>
<div class="m2"><p>به ما دوخته دیده ها مردمان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز گفتار او شد بد اختر غمین</p></div>
<div class="m2"><p>بگریید و گفتا به بانو چنین</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سیه روی فرزند مرجانه باد</p></div>
<div class="m2"><p>که او داد این خاندان را به باد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ولیکن از این کار ناچار بود</p></div>
<div class="m2"><p>که باب تو با ما به پیکار بود</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بکوشید از بهر ملک و شهی</p></div>
<div class="m2"><p>ندانست بختش و او را شکست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بدو گفت نوباوه ی شاه دین</p></div>
<div class="m2"><p>که ای دشمن سیدالمرسلین</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ز یزدان بکن شرم و چندی مناز</p></div>
<div class="m2"><p>که کشتی حسین (ع) پور شاه حجاز</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>پدرم از خداوند در این جهان</p></div>
<div class="m2"><p>بدین شاه بود آشکار و نهان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>به مرگش چنین شادمانی مکن</p></div>
<div class="m2"><p>که گیتی تو را هم برآرد ز بن</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بیندیش تا روز محشر جواب</p></div>
<div class="m2"><p>چه گویی به پیغمبر کامیاب</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گمانت که او خون فرزند خویش</p></div>
<div class="m2"><p>دگر آنهمه یارو پیوند خویش</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>نجوید ز تو ای زنا زاده مرد</p></div>
<div class="m2"><p>بسا زود کز تو برآرند گرد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چو دید اینچنین خواهر شهریار</p></div>
<div class="m2"><p>سر بانوان زینب داغدار</p></div></div>