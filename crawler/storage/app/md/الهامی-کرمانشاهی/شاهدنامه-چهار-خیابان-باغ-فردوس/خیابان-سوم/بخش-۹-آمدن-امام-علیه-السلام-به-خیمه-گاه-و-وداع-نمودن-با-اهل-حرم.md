---
title: >-
    بخش ۹ - آمدن امام علیه السلام به خیمه گاه و وداع نمودن با اهل حرم
---
# بخش ۹ - آمدن امام علیه السلام به خیمه گاه و وداع نمودن با اهل حرم

<div class="b" id="bn1"><div class="m1"><p>زتیر بداندیش چون روزگار</p></div>
<div class="m2"><p>سر آمد بدان کودک شیرخوار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیامد شه دین به سوی حرم</p></div>
<div class="m2"><p>رخ از گریه پرخون دل از غم دژم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همه بانوان حرم را بخواند</p></div>
<div class="m2"><p>ابا کودکان در بر خود نشاند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همی دست بر روی هر یک بسود</p></div>
<div class="m2"><p>به بدرود ایشان زبان برگشود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بگفتا که ای آل خیرالانام</p></div>
<div class="m2"><p>شما را یکایک درود و سلام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زدیدار من توشه گیرید باز</p></div>
<div class="m2"><p>که آمد مرا گاه رفتن فراز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مرا شوق دیدار پروردگار</p></div>
<div class="m2"><p>زتن برده آرام و از دل قرار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>میان من و او حجابی نماند</p></div>
<div class="m2"><p>به جز جان که باید به راهش فشاند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کنون می روم سوی میدان کین</p></div>
<div class="m2"><p>به مهمانی پاک جان آفرین</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>پس از من خداوند بس یارتان</p></div>
<div class="m2"><p>به هر سختی اندر مددکارتان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>زهر بدکه از دشمن آید به سر</p></div>
<div class="m2"><p>پناهنده باشید بردادگر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>علی (ع) کو پس از من شه عالم است</p></div>
<div class="m2"><p>در این راهتان محرم و همدم است</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>در این گفتگو بود شاه امم</p></div>
<div class="m2"><p>که ناگاه بیرون دوید از حرم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>یکی ماهرو کودک خردسال</p></div>
<div class="m2"><p>به بالا و چهر و سخن بی مثال</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>سکینه(ع) زشه داشت فرخنده نام</p></div>
<div class="m2"><p>بدو بود دارای دین شادکام</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>دمی از کنارش بنگذاشتی</p></div>
<div class="m2"><p>نکوتر ز جان در برش داشتی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>زبانی خوش و نغز گفتار داشت</p></div>
<div class="m2"><p>دل آراتر از خلد دیدار داشت</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بیامد بر شاه با اشک و آه</p></div>
<div class="m2"><p>نمودش در آغوش بر جایگاه</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>زدش بوسه بسیار بردست و پای</p></div>
<div class="m2"><p>بدو گفت با دیده ی اشک زای</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>که ای خاک پای تو تاج سرم</p></div>
<div class="m2"><p>سرافراز باب بلند افسرم</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>دراین خردسالی یتیم ام مکن</p></div>
<div class="m2"><p>زهجران خود دل دو نیم ام مکن</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>کنار خود از من مگردان تهی</p></div>
<div class="m2"><p>زمن مگسلان اختر فرهی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>دو فرخ برادرم در این زمین</p></div>
<div class="m2"><p>به خون خفتشان پیکر نازنین</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>سپهدار عم جهانجوی من</p></div>
<div class="m2"><p>جدا شد به شمشیر دستش زتن</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>زاولاد هاشم هر آنکس که بود</p></div>
<div class="m2"><p>جهانشان همه گشت هستی درود</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>چو گشت اسپری روزگار همه</p></div>
<div class="m2"><p>توماندی به جا یادگار همه</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>تو نیز اینچنین دل نهادی به مرگ</p></div>
<div class="m2"><p>سلیح نبرد آمدت ساز و برگ</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>پس از تو که در برفشاند مرا؟</p></div>
<div class="m2"><p>که سوی خود از مهر خواند مرا؟</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بدو گفت شاه ای نکو دخترم</p></div>
<div class="m2"><p>نباشد چو دیگر کسی یاورم</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>به مرگ ار نهم تن شگفتی مدار</p></div>
<div class="m2"><p>که بی یاوران را همین است کار</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>سکینه (س) بدو گفت زارای پدر</p></div>
<div class="m2"><p>به یثرب دگر باره ما را ببر</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>که درسایه ی تربت مصطفی (ص)</p></div>
<div class="m2"><p>نبینیم از بدسگالان جفا</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>شهش گفت: هیهات ازین آرزوی</p></div>
<div class="m2"><p>که ره گر نبد بسته از چار سوی</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>نمی خواستم خویش را مبتلا</p></div>
<div class="m2"><p>نمی ماندم اندر زمین بلا</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>دگر روی یثرب نخواهید دید</p></div>
<div class="m2"><p>مگر من شوم از میان ناپدید</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>سکینه (س) از آن گفته بگریست زار</p></div>
<div class="m2"><p>فرو ریخت خون از مژه بر کنار</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>شهش گفت: ای بانوی بانوان</p></div>
<div class="m2"><p>مسوزان دلم تاکه دارم روان</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>کنون گاه این زاری و گریه نیست</p></div>
<div class="m2"><p>به مرگم بسی زار خواهی گریست</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>نگون از بر زین چو گشتم به روی</p></div>
<div class="m2"><p>به من هر چه خواهی به زاری بموی</p></div></div>