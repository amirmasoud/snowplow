---
title: >-
    بخش ۸ - به میدان بردن امام حضرت علی اصغر را به طلب آب و شهادت آن جناب
---
# بخش ۸ - به میدان بردن امام حضرت علی اصغر را به طلب آب و شهادت آن جناب

<div class="b" id="bn1"><div class="m1"><p>بفرمود با زینب (ع) غمزده</p></div>
<div class="m2"><p>که ای خواهر زار ماتمزده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>برون آر آن گل عذار مرا</p></div>
<div class="m2"><p>همان کودک شیرخوار مرا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>که شش مه به سر برده از زنده گی</p></div>
<div class="m2"><p>بود چون ستاره به رخشنده گی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>که بینم دمی نازنین رود را</p></div>
<div class="m2"><p>به رویش دهم بوسه بدرود را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گرانمایه را زینب از خیمه گاه</p></div>
<div class="m2"><p>برفت و بیاورد نزدیک شاه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همی گفت زار ای شه دین فروز</p></div>
<div class="m2"><p>نخورده است آب این گرامی سه روز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ببر با خود او را سوی رزمگاه</p></div>
<div class="m2"><p>برای وی از لشگر آبی بخواه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مگر بخشش آرند بر خردی اش</p></div>
<div class="m2"><p>رهانندش از دست تاب عطش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چنان کن که سیراب باز آری اش</p></div>
<div class="m2"><p>که جان های ما سوخت از زاری اش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شه آن نغز نوباوه را برگرفت</p></div>
<div class="m2"><p>چو جان گرامیش ور برگرفت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ببوسید گلدسته ی خویش را</p></div>
<div class="m2"><p>کزو داشت مرهم دل ریش را</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به گنج ولایت همان گوهرش</p></div>
<div class="m2"><p>به جا بود و بگرفت از خواهرش</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نبی (ص) وار بردش به معراج عشق</p></div>
<div class="m2"><p>بکرد آن دمش دره التاج عشق</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>در آغوش شه روی نوازده پور</p></div>
<div class="m2"><p>همی تافت چون از بر چرخ هور</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>پدر بد به عرش خدا گوشوار</p></div>
<div class="m2"><p>پسر در برش گوهری شاهوار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بیامد به پیش سپاه ایستاد</p></div>
<div class="m2"><p>به اتمام حجت زبان برگشاد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>که ای دشمنان جهان آفرین</p></div>
<div class="m2"><p>کشیده سر از حکم جان آفرین</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بکشتید یاران و خویشان من</p></div>
<div class="m2"><p>گذشتید از عهد و پیمان من</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>و یا آنکه بخشید یک جرعه آب</p></div>
<div class="m2"><p>که از او عطش برده یکباره تاب</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>مرا گر گنهکار پنداشتید</p></div>
<div class="m2"><p>که بر من چنین بد روا داشتید</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چه کرده است این کودک بیگناه؟</p></div>
<div class="m2"><p>که از تشنگی گشت باید تباه</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>سه روز است این گل نخورده است آب</p></div>
<div class="m2"><p>به خود دارد از تشنگی از تشنگی پیچ وتاب</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بدین پهنه آوردمش آب خواه</p></div>
<div class="m2"><p>یکی سوی او بنگرید ای سپاه</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>مگر رحم آرید بر حال او</p></div>
<div class="m2"><p>ببینید بر خردی سال او</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چشانید او را یکی جرعه آب</p></div>
<div class="m2"><p>بسی گفت و ازکس نیامد جواب</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>عمر گفت: ای لشگر آبش دهید</p></div>
<div class="m2"><p>بد انسان که باید جوابش دهید</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>تبه گوهری نام او حرمله</p></div>
<div class="m2"><p>نمود از زه کینه تیری یله</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>زبانگ زه و جور آن بدگمان</p></div>
<div class="m2"><p>خروشی برآمد زهفت آسمان</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بلرزید بر خویش عرش برین</p></div>
<div class="m2"><p>بزد دست بر فرق روح الامین</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>از آن تیر شد تیره چشم سپهر</p></div>
<div class="m2"><p>زاندوه شد زرد رخسار مهر</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>از آن تیر شد چیده برگ گلی</p></div>
<div class="m2"><p>که بد جبرییلش کمین بلبلی</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>گلویی از آن تیر آسیب یافت</p></div>
<div class="m2"><p>کز آسیب آن قلب حیدر شکافت</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>برون شد چو پیکان ز شست پلید</p></div>
<div class="m2"><p>به حلقوم نوباوه ی شه رسید</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>بزد چاک و از سوی دیگر بجست</p></div>
<div class="m2"><p>به بازوی فرخ پدر بر نشست</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>ببرید از گوش او تا به گوش</p></div>
<div class="m2"><p>برآورد شهزاده از دل خروش</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>بپیچید بر خویش از تاب درد</p></div>
<div class="m2"><p>برون دست ها راز قنداقه کرد</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>حمایل بیفکند بر دوش باب</p></div>
<div class="m2"><p>شگفتا جهان چون نگشتی خراب؟</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>شه از نای او برکشید آن خدنگ</p></div>
<div class="m2"><p>شده پر و پیکانش یاقوت رنگ</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>از آن نای خشکیده چون ناودان</p></div>
<div class="m2"><p>روان گشت خون وان شه غیب دان</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>به گردون بیفشاندی آن خون پاک</p></div>
<div class="m2"><p>نهشتی که یک قطره آید به خاک</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>چو دانستی ار خون آن بیگناه</p></div>
<div class="m2"><p>به خاک آید از وی نروید گیاه</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>عذاب خدایی رسد در زمان</p></div>
<div class="m2"><p>بدان قوم بد اختر از آسمان</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>شهنشه از آن درد بگریست سخت</p></div>
<div class="m2"><p>فرو ریخت خون از مژه لخت لخت</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>سپس سر برآورد سوی فراز</p></div>
<div class="m2"><p>بگفتا: که ای داور بی نیاز</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>تو دانی که من زین ستمگر سپاه</p></div>
<div class="m2"><p>چه دیدم در این سهمگین رزمگاه</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>از این سخت تر بر من آسان بود</p></div>
<div class="m2"><p>چو دردیده ی پاک یزدان بود</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>نباشد به درگاهت ای ذوالمنن</p></div>
<div class="m2"><p>کم از ناقه ی صالح این پور من</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>تو این پیشکش راز من در پذیر</p></div>
<div class="m2"><p>بود خرد اگر خرده بر من مگیر</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>به دست اندرون داشتم من همین</p></div>
<div class="m2"><p>که آوردمش برخی واپسین</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>تو افزون شمارش اگر اندک است</p></div>
<div class="m2"><p>بزرگش بفرمای گر کودک است</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>برفت آنچه بر من ز اعدای من</p></div>
<div class="m2"><p>بیندوزش از بهر فردای من</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>بگفت این و بوسید روی پسر</p></div>
<div class="m2"><p>پسر گشت خندان به روی پدر</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>وزین آشیان مرغ روحش بجست</p></div>
<div class="m2"><p>به مینو در آغوش زهرا نشست</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>شهنشه از آن جای یکران براند</p></div>
<div class="m2"><p>به نزد دگر کشتگانش رساند</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>فرود آمد و خواند بر وی نماز</p></div>
<div class="m2"><p>پس از کار شکرانه ی بی نیاز</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>یکی قبر کوچک ابا نوک تیغ</p></div>
<div class="m2"><p>بکند و نهفتش به خاک ای دریغ</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>همانا بدآگاه فرخنده شاه</p></div>
<div class="m2"><p>که آید به پایان چو رزم سپاه</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>تن کشتگان را به سم ستور</p></div>
<div class="m2"><p>بسایند آن مردم پر غرور</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>ندارد تن کودک این توش و تاب</p></div>
<div class="m2"><p>از آتش نهان کرد اندر تراب</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>و یا شرمگین بود از مادرش</p></div>
<div class="m2"><p>نبرد آن تن کشته را در برش</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>دریغا از آن اختر تابناک</p></div>
<div class="m2"><p>که افکند تیر و بالش به خاک</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>دریغا از آن لؤلؤی شاهوار</p></div>
<div class="m2"><p>که یاقوت گون گشتش از خون عذار</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>دریغا از آن نغز گلبن که داد</p></div>
<div class="m2"><p>خزان ستم برگ و بارش به باد</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>دریغا ازآن مرغ جبریل فر</p></div>
<div class="m2"><p>که بسمل شد از ناوک چار پر</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>دریغا از آن باز عرشی شکار</p></div>
<div class="m2"><p>که افکند پر عقابش زکار</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>دریغا از آن لعل ناخورده شیر</p></div>
<div class="m2"><p>که سیراب کردندش از آب تیر</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>ندانم چه بگذشت برمادرش</p></div>
<div class="m2"><p>چو شه رفت در خیمه بی اصغرش</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>من ازکار این کودکم در شگفت</p></div>
<div class="m2"><p>بزرگ است خردش نباید گرفت</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>ملک در خروش آمد از ماتمش</p></div>
<div class="m2"><p>شفق جامه در خون کشید از غمش</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>غمین گشت جان نبی (ص) دربهشت</p></div>
<div class="m2"><p>بگریید بانوی حورا سرشت</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>شکیبا کناد ای رسول خدای</p></div>
<div class="m2"><p>تو را کردگارت به دیگر سرای</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>زمینو بدین اختر دین فروز</p></div>
<div class="m2"><p>خروش تو درگوشم آید هنوز</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>بسی آمد از مرگ او برتو رنج</p></div>
<div class="m2"><p>بسی دید روشن روانت شکنج</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>دل نازکت ای شفیع جزا</p></div>
<div class="m2"><p>چسان تاب آورد در این عزا؟</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>به آن پر بها گوهرکان عشق</p></div>
<div class="m2"><p>به آن آب خورده ز پیکان عشق</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>که این بنده ای شه فرامش مکن</p></div>
<div class="m2"><p>چراغ دلم را تو خامش مکن</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>ببخشا به من تیره گی های من</p></div>
<div class="m2"><p>همان در گنه خیره گی های من</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>ندارد تن من ز تو آن عتاب</p></div>
<div class="m2"><p>شها روز محشر زمن رخ متاب</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>مرا چون تو شاهی پناهم تو باش</p></div>
<div class="m2"><p>به روز جزا عذار خواهم توباش</p></div></div>