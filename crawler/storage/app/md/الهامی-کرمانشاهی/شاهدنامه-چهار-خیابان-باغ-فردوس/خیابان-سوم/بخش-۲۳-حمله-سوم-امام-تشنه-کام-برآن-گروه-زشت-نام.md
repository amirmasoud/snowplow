---
title: >-
    بخش ۲۳ - حمله سوم امام تشنه کام برآن گروه زشت نام
---
# بخش ۲۳ - حمله سوم امام تشنه کام برآن گروه زشت نام

<div class="b" id="bn1"><div class="m1"><p>چو شیر خدا از پی کارزار</p></div>
<div class="m2"><p>برآهیخت آن آذر آبدار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بزد برصف کین چو باد وزان</p></div>
<div class="m2"><p>فرو ریخت سرها چو برگ رزان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همی خورد زخم و همی کشت مرد</p></div>
<div class="m2"><p>خود وباره گشته نهان زیر گرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زدی هر که را تیغ بران به فرق</p></div>
<div class="m2"><p>زتنگ ستورش برون شد چو برق</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زدی هر که را برمیان تیغ تیز</p></div>
<div class="m2"><p>دو نیمه فتادی به دشت ستیز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زگرد سواران درآن دار و گیر</p></div>
<div class="m2"><p>رخ روشن روز شد همچو قیر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دم تیغ شاه آتشی برفروخت</p></div>
<div class="m2"><p>تن سرکشان را سراسر بسوخت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بسا زین و توسن که شد واژگون</p></div>
<div class="m2"><p>سوارش بزد غوطه در موج خون</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چه مور و ملخ ازدم تیغ شاه</p></div>
<div class="m2"><p>پراکنده گشتند کوفی سپاه</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>حسین (ع) و براسب پیمبر سوار</p></div>
<div class="m2"><p>به کف تیغ شیر خدا استوار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به گفتن نگنجد که اندر نبرد</p></div>
<div class="m2"><p>بدان بدسگالان یزدان چه گرد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ز بس کوشش و گرمی آفتاب</p></div>
<div class="m2"><p>وزان خون که رفت از تنش همچو آب</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چنان تشنه کامی بدو چیره گشت</p></div>
<div class="m2"><p>که بیننده ی روشنش تیره گشت</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ز سوز عطش سینه اش برفروخت</p></div>
<div class="m2"><p>زدود دلش جان اختر بسوخت</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ز تابی که بد درتن روشنش</p></div>
<div class="m2"><p>به بر تفته شد آهنین جوشنش</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>عقیق لبش گشت همرنگ مشک</p></div>
<div class="m2"><p>زبان دردهان گشت چون چوب خشک</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>زبس تشنگی شاه از کارزار</p></div>
<div class="m2"><p>عنان را بتابید زی رود بار</p></div></div>