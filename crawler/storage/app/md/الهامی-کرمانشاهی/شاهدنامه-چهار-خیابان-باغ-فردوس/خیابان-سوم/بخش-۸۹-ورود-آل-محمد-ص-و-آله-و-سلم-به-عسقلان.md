---
title: >-
    بخش ۸۹ - ورود آل محمد (ص) و آله و سلم به عسقلان
---
# بخش ۸۹ - ورود آل محمد (ص) و آله و سلم به عسقلان

<div class="b" id="bn1"><div class="m1"><p>بدی عسقلان را یکی مرزبان</p></div>
<div class="m2"><p>که یعقوب بد نام آن بد گمان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مر این بدگهر بود در کربلا</p></div>
<div class="m2"><p>به رزم خداوند اهل ولا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به مردم چنین گفت آن زشت خوی</p></div>
<div class="m2"><p>که آیینه بندند بازار و کوی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نوازند رامشگران رود و دف</p></div>
<div class="m2"><p>تماشاچیان پای کوبند و کف</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو آل پیمبر بدان زشت بوم</p></div>
<div class="m2"><p>رسیدند همچون اسیران روم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زهر سو شدند انجمن مرد و زن</p></div>
<div class="m2"><p>به همراه رامشگران چنگ زن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یکی مرد بازارگان نیک کام</p></div>
<div class="m2"><p>که خود بد ضریر خزاعیش نام</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در آن تازگی کرده آنجا مکان</p></div>
<div class="m2"><p>چو دید آنچنان شادی از مردمان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز مردی بپرسید کای نیک یار</p></div>
<div class="m2"><p>مگر روز عیدی است در این دیار؟</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بگفتا: گمانم که باشی غریب</p></div>
<div class="m2"><p>کز این شادمانی نداری نصیب</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بگفتا: بلی نیستم زین دیار</p></div>
<div class="m2"><p>بگفتش: تو هم رو به شادی بیار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>که شخصی برآویخت با شاه شام</p></div>
<div class="m2"><p>همی خواست گردد به مردم امام</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>فرستاد لشگر عبیدزیاد</p></div>
<div class="m2"><p>بکشتند او را و، طی شد فساد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>کنون اهل او رابدینجا اسیر</p></div>
<div class="m2"><p>بیارند و مردم به فرمان میر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>سپه را به شادی پذیره شوند</p></div>
<div class="m2"><p>که دل های بدخواه تیره شوند</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بپرسید از او مرد بازارگان</p></div>
<div class="m2"><p>که بد دشمن شاه از مشرکان</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>و یا بد مسلمان و از اهل دین</p></div>
<div class="m2"><p>بگفتا بدو مرد پاسخ چنین</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>که آن مرد بد سبط خیرالانام</p></div>
<div class="m2"><p>حسین علی (ع) داشت فرخنده نام</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چو بشنید این، زد به هر دو دست</p></div>
<div class="m2"><p>که آوخ که پشت پیمبر شکست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بیفتاد برخاک و می گفت زار</p></div>
<div class="m2"><p>دریغا، ز خیرالبشر یادگار</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>پس آنگاه برجست و آمد دوان</p></div>
<div class="m2"><p>به نزدیک آن غمزده کاروان</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چو افتاد آنگونه چشم ضریر</p></div>
<div class="m2"><p>برآن شاه بیمار و خیل اسیر</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>خروشان به خاک سیه برنشست</p></div>
<div class="m2"><p>زغم زد همی برسر و سینه دست</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بدو گفت آن ناتوان شهریار</p></div>
<div class="m2"><p>که گویا نه ای مردم این دیار</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>که این قوم شادند و تو در غمی</p></div>
<div class="m2"><p>نشسته به خاکی و در ماتمی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بگفتا: بلی ای جهان را پناه</p></div>
<div class="m2"><p>من امروز اینجا رسیدم ز راه</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بدی کور ای کاش بیننده ام</p></div>
<div class="m2"><p>نمی آفرید آفریننده ام</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>که بینم شما را چنین سوگوار</p></div>
<div class="m2"><p>برهنه سر و بر شترها سوار</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بگفتا بدو شاه کای نیکنام</p></div>
<div class="m2"><p>ز تو بوی یاری رسد برمشام</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>خروشان بزد دست بر سر ضریر</p></div>
<div class="m2"><p>بگفتا که ای شاه گردون سریر</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>من اندر جهان زنده تا بوده ام</p></div>
<div class="m2"><p>پرستنده ای از شما بوده ام</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>بفرما به من آنچه فرمان تو راست</p></div>
<div class="m2"><p>که امر تو برجسم و جانم رواست</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>بگفتش گرفتار زنجیر غم</p></div>
<div class="m2"><p>که داری گرایدون به همره درم</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>ببر نزد این بد گهر نیزه دار</p></div>
<div class="m2"><p>که بر نیزه دارد سر شهریار</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>بدوده که بخشد به یاران خویش</p></div>
<div class="m2"><p>که از ما برانند یک لخت پیش</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>بود کان تماشاییان شریر</p></div>
<div class="m2"><p>روند از میان زنان اسیر</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>به دنبال سرها بگیرند راه</p></div>
<div class="m2"><p>بر این بیکسان کمتر افتد نگاه</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>برفت و بکرد آنچه فرموده بود</p></div>
<div class="m2"><p>بیامد دمان تا برشاه زود</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>که گر خدمتی هست فرمای باز</p></div>
<div class="m2"><p>بفرمود: کاین بانوان حجاز</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>ندارد در بر لباسی کز آن</p></div>
<div class="m2"><p>بپوشند خود را ز نامحرمان</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>گرت هست از جامه چیزی به بار</p></div>
<div class="m2"><p>برو بهر این بی پناهان بیار</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>برفت و بیاورد آن نیکنام</p></div>
<div class="m2"><p>سراپای رختی ز بهر امام</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>به هریک از آن بیکسان، مرد راه</p></div>
<div class="m2"><p>بدانسان که بایست پوشش بداد</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>که ناگه ز دنبال برخاست غو</p></div>
<div class="m2"><p>گروهی روان شمرشان پیشرو</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>همی بد گهر نعره بر می کشید</p></div>
<div class="m2"><p>که دشمن بشد کشته، شادی کنید</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>ضریر خزاعی چو زینسان بدید</p></div>
<div class="m2"><p>شکیب از دلش رفت و بر وی دوید</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>گرفتش عنان سخت پس ناگهان</p></div>
<div class="m2"><p>بیفکند بر رویش آب دهان</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>بدوگفت: کای از نژاد حرام</p></div>
<div class="m2"><p>پلید و تبهکار از باب و مام</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>نداری مگر هیچ شرم از خدا</p></div>
<div class="m2"><p>که سازی سر پور زهرا (س) جدا؟</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>حریم نبی را به بازارها</p></div>
<div class="m2"><p>کشانی، کنی هر دم آزارها</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>ابا اینچنین زشتی ای مرد دون</p></div>
<div class="m2"><p>کشی نعره ی شادمانی کنون؟</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>تفو باد بر این دو موی پلید</p></div>
<div class="m2"><p>که بر روی بی آبرویت دمید</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>نماید به هر دو سرا داورت</p></div>
<div class="m2"><p>چو چشم دلت کور چشم سرت</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>چو دشنام بسیار شمر پلید</p></div>
<div class="m2"><p>بدانگونه زان مرد برنا شنید</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>به یاران خود کرد ناپاک روی</p></div>
<div class="m2"><p>که گیرند اطراف این یاوه گوی</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>زنیدش به چوب و به مشت و به سنگ</p></div>
<div class="m2"><p>نمایید خاکش ز خون لاله رنگ</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>به فرمان شمر آن گروه شریر</p></div>
<div class="m2"><p>گرفتند پیرامن آن دلیر</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>زدندش همی تا ازو رفت توش</p></div>
<div class="m2"><p>بیفتاد و برخاک بسپرد هوش</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>گمانشان که یکباره مرده ست مرد</p></div>
<div class="m2"><p>بهشتند او را درآنجای، فرد</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>چو نیمی ز شب رفت آمد به هوش</p></div>
<div class="m2"><p>به هر سوی لختی فرا داد گوش</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>چو نشنید از هیچ سوبانگ کس</p></div>
<div class="m2"><p>بجنبید برخویش و برزد نفس</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>به دست و به زانو روان گشت تفت</p></div>
<div class="m2"><p>همی تا به یک بقعه اندر برفت</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>در آنجا یکی انجمن دید مرد</p></div>
<div class="m2"><p>همه زار و گریان و نالان به درد</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>نشسته همه با گریبان چاک</p></div>
<div class="m2"><p>به جای کله هشته بر فرق، خاک</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>از ایشان بپرسید کای مردمان</p></div>
<div class="m2"><p>شما را چه آمد بسر از زمان</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>همه اهل این شهر خندان و شاد</p></div>
<div class="m2"><p>درغم به روی شما چون گشاد</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>مرا بس شگفت آید از کارتان</p></div>
<div class="m2"><p>از آن خنده وین گریه ی زارتان</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>بگفتند تو دوست یا دشمنی</p></div>
<div class="m2"><p>سروشی و یا زشت اهریمنی</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>اگر دشمنی رو سر خویش گیر</p></div>
<div class="m2"><p>چو یاران ره خرمی پیش گیر</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>اگر دوستی ناله سرکن چو ما</p></div>
<div class="m2"><p>به سوک شهنشاه ارض و سما</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>بگفتا که دشمن نیم دوستم</p></div>
<div class="m2"><p>که دشمن دریده به تن پوستم</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>بگفت آنچه آن روزش آمد به سر</p></div>
<div class="m2"><p>ز بد خواه آل نبی سر بسر</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>چو آگاه گشتند از حال وی</p></div>
<div class="m2"><p>بگفتند کای مرد فرخنده پی</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>چو تو ما ز یاران پیغمبریم</p></div>
<div class="m2"><p>غلامان شیر خدا حیدریم</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>بود گریه ی ما بدان شهسوار</p></div>
<div class="m2"><p>که باشد سرش بر سنان استوار</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>چو بشنید این مویه پرداز شد</p></div>
<div class="m2"><p>به ایشان دران سوک انباز شد</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>دگر روز کاین مهر گردون مسیر</p></div>
<div class="m2"><p>روان شد به گردون سپاه شریر</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>پی کوچ رایت بیفراختند</p></div>
<div class="m2"><p>از آنجا سوی بعلبک تاختند</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>همه اهل آن مرز پیر و جوان</p></div>
<div class="m2"><p>زن و مرد با زیب زیور روان</p></div></div>