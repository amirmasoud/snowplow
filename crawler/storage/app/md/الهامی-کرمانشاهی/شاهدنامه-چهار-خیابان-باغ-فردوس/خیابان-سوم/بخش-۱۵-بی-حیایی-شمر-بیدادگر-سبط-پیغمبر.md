---
title: >-
    بخش ۱۵ - بی حیایی شمر بیدادگر سبط پیغمبر
---
# بخش ۱۵ - بی حیایی شمر بیدادگر سبط پیغمبر

<div class="b" id="bn1"><div class="m1"><p>مگر آنکه بیعت به دارای شام</p></div>
<div class="m2"><p>ببندی و دانیش برخود امام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وگرنه مپیچان دگر کار را</p></div>
<div class="m2"><p>شو آماده با خصم پیکار را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو گفتار او را شهنشه شنید</p></div>
<div class="m2"><p>بدو بر خروشید و گفت: ای پلید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گمانتان زکشتن مرا هست باک</p></div>
<div class="m2"><p>نترسم بجز از خداوند پاک</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دل شیر کوشنده ی ذوالمنن</p></div>
<div class="m2"><p>بود در میان دو پهلوی من</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>روان نبی آن خداوند پاک</p></div>
<div class="m2"><p>مرا خفته در پیکر تابناک</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو خواهم که قدرت نمایی کنم</p></div>
<div class="m2"><p>همه کرده ی کبریایی کنم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بود تا که پیمان ستانم به مشت</p></div>
<div class="m2"><p>نبیند کس از من به پیگار، پشت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بود تا به کف قبضه ی ذوالفقار</p></div>
<div class="m2"><p>نخواهم ز بدخواه دین زینهار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نشاید خداوند را بنده گی</p></div>
<div class="m2"><p>شه دین کجا و پرستنده گی؟</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تفو پور مرجانه ی زشت کیش</p></div>
<div class="m2"><p>که خداوند مرا پیرو شاه خویش</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مرا تا که درکالبد هست جان</p></div>
<div class="m2"><p>نباشم درین کار همداستان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چو این دید بن سعد تاریک هوش</p></div>
<div class="m2"><p>زلشگر جدا گشت و برزد خروش</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بگفتا: که بازاده ی بوتراب</p></div>
<div class="m2"><p>نکو نیست دیگر سوال و جواب</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ببارید بر پیکر او خدنگ</p></div>
<div class="m2"><p>میارید بر کشتن او درنگ</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>هزاری سه پنج از سپاه شریر</p></div>
<div class="m2"><p>به زه بر نهادند پرنده تیر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چو نمرود حق را نشان ساختند</p></div>
<div class="m2"><p>به یکره رها از کمان ساختند</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>از آن تیر باران نیامد گزند</p></div>
<div class="m2"><p>بدان شاه کافرکش دیوبند</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>فرو ماند اهریمن اندر شگفت</p></div>
<div class="m2"><p>زکار شه اندازه ها برگرفت</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>دگر ره ز نو چاره ای ساز کرد</p></div>
<div class="m2"><p>سران را به سوی خود آواز کرد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>از ایشان سواری که بد رزمخواه</p></div>
<div class="m2"><p>یکایک فرستاد زی رزم شاه</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>به یک زخم آن قدرت کردگار</p></div>
<div class="m2"><p>برآوردی از جان هر یک دمار</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>چو زیشان بسی خفت برخون و خاک</p></div>
<div class="m2"><p>سپه را بشد زهره از بیم چاک</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>شهنشه از ایشان همی خواست مرد</p></div>
<div class="m2"><p>نمی کرد کس آرزوی نبرد</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چو دیگر نیامد کسی رزمخواه</p></div>
<div class="m2"><p>بزد شاه دین خوش را برسپاه</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>تو گفتی که در پیکر ذوالجناح</p></div>
<div class="m2"><p>چو باز دمنده بر آمد جناح</p></div></div>