---
title: >-
    بخش ۵۲ - روایت فاطمه ی نو عروس در غارت خیمه گاه گوید
---
# بخش ۵۲ - روایت فاطمه ی نو عروس در غارت خیمه گاه گوید

<div class="b" id="bn1"><div class="m1"><p>زخرگه برون آمدم با شتاب</p></div>
<div class="m2"><p>دلی پر ز آتش دو دیده پر آب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به ناگه بیفتادم آن دم نگاه</p></div>
<div class="m2"><p>سوی کشته ی شاه و یاران شاه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بدیدم بدن هایی از نور پاک</p></div>
<div class="m2"><p>فتاده همه غرقه در خون و خاک</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به نزدیک هم خفته بی سر، تنا</p></div>
<div class="m2"><p>چو قربانی حاجیان در منا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>درآندم به اندیشه رفتم فرو</p></div>
<div class="m2"><p>همی داشتم با خود این گفتگو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>که امروز این مردم زشت نام</p></div>
<div class="m2"><p>بکشتند مردان ما را تمام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>که بودند مرد افکن و تیغ زن</p></div>
<div class="m2"><p>چه سازند با ما که هستیم زن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو مردان بریزند اگر خون ما</p></div>
<div class="m2"><p>بود یاری بخت وارون ما</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از این زندگی، مرگ بهتر بسی</p></div>
<div class="m2"><p>که افتد به ما چشم هر ناکسی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>من استاده، حیران زکار سپهر</p></div>
<div class="m2"><p>که از ما چرا پاک ببریده مهر؟</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به ناگه یکی خصم چون پیل مست</p></div>
<div class="m2"><p>بیامد یکی نیزه او را به دست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>رخش تیره و سهمگین سرخ موی</p></div>
<div class="m2"><p>دو چشمش کبود و پر از چین به روی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گرازان و تازان چو گرگ دژم</p></div>
<div class="m2"><p>گریزان ز وی آهوان حرم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>رسیدی به هر یک از آن بانوان</p></div>
<div class="m2"><p>بیازردی او را به چوب و سنان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>کشیدی به سختی زسر معجرش</p></div>
<div class="m2"><p>گرفتی ازو رخت و هم زیورش</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>سراسیمه از بیم آن زشت گرگ</p></div>
<div class="m2"><p>همی خرد جستی پناه از بزرگ</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>من از دیدنش سخت ترسان شدم</p></div>
<div class="m2"><p>گریزان به سوی بیابان شدم</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چو دیدم گریزنده آن زشت مرد</p></div>
<div class="m2"><p>زدنبال من اندر آمد چو گرد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بن نیزه بر دوش من کوفت سخت</p></div>
<div class="m2"><p>فتادم به رو همچو برگ از درخت</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ربود از سرم معجر آن نابکار</p></div>
<div class="m2"><p>به گوشم بدید اندرون گوشوار</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>گرفت و چنانش به سختی کشید</p></div>
<div class="m2"><p>کز آن پرده ی گوش من بر درید</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>روان گشت بر چهر خونم زگوش</p></div>
<div class="m2"><p>زآسیب آن زخم رفتم زهوش</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>فتادم چو بر خاک بی توش وتاب</p></div>
<div class="m2"><p>همی تافت بر پیکرم آفتاب</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چو لختی برآمد به هوش آمدم</p></div>
<div class="m2"><p>خروشی زبالین به گوش آمدم</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>یکی برسرم گریه می کرد زار</p></div>
<div class="m2"><p>کشیدی زدل ناله همچون هزار</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>چو کردم نگه دیدم آن زینب (س) است</p></div>
<div class="m2"><p>که از بهر من روز او چون شب است</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>چو دید آنکه آمد روانم به تن</p></div>
<div class="m2"><p>بفرمود کای مونس جان من</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>مرا کاشکی تن شدی بی روان</p></div>
<div class="m2"><p>نمی دیدم اینسان تو را بی توان</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>به پاخیز وزین بیش ایدر مپای</p></div>
<div class="m2"><p>منت می برم گر تو را نیست پای</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>که آشفته ام بهر طفلان شاه</p></div>
<div class="m2"><p>دگر بهر آن خسته ی بی پناه</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>ندانم کز آن قوم بیدادگر</p></div>
<div class="m2"><p>مرآن بیکسان را چه آمد به سر</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>بگفتم: که ای بانوی غمگسار</p></div>
<div class="m2"><p>چه سان سر برهنه شوم رهسپار؟</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>یکی جامعه ی کهنه آور به من</p></div>
<div class="m2"><p>که با آن بپوشم سر خویشتن</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>به من گفت بانو بسی پر ز آه</p></div>
<div class="m2"><p>مرا بین وزان پس زمن جامه خواه</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>به چیزی اگر دسترس داشتم</p></div>
<div class="m2"><p>سر خود برهنه بنگذاشتم</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>چو دیدم نکو عصمت کردگار</p></div>
<div class="m2"><p>نبودش چو من چادر و گوشوار</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>به جز موی چیزیش برسر نبود</p></div>
<div class="m2"><p>بر و دوشش از تازیانه کبود</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>چو دیدم چنان از سرم هوش شد</p></div>
<div class="m2"><p>غم خویشم از دل فراموش شد</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>از آنجا برفتیم پس با شتاب</p></div>
<div class="m2"><p>سوی خیمه بر چهره از کف نقاب</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>چو رفتم بدیدم که درخیمه گاه</p></div>
<div class="m2"><p>نمانده به جا غیر خاک سیاه</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>شهنشاه بیمار در آستان</p></div>
<div class="m2"><p>بیفتاده برخاک و خفته سنان</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>تن نازکش تفته ازتاب تب</p></div>
<div class="m2"><p>ز بی آبی اش خشک گردیده لب</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>نشستیم بر گرد او مویه گر</p></div>
<div class="m2"><p>که ما را از تن این پس چه آید به سر؟</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>زتاراج خرگاه زین العباد</p></div>
<div class="m2"><p>ستم ها که آن شاه دید از عناد</p></div></div>