---
title: >-
    بخش ۶۶ - زاری کردن مادر غمدیده علی اکبر(ع)برروی نعش پسر جوان گوید
---
# بخش ۶۶ - زاری کردن مادر غمدیده علی اکبر(ع)برروی نعش پسر جوان گوید

<div class="b" id="bn1"><div class="m1"><p>بیامد به بالین پور جوان</p></div>
<div class="m2"><p>بیفتاد بر خاک زار و نوان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هم آغوش شد سرو آزاد را</p></div>
<div class="m2"><p>به زاری برافراخت فریاد را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بزد بوسه بر پا و دست و سرش</p></div>
<div class="m2"><p>بدان هر دو گلبرگ از خون ترش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ببوسید لعل شکر پاسخش</p></div>
<div class="m2"><p>ببویید آن سنبل فرخش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بمالید چون غاره خونش به روی</p></div>
<div class="m2"><p>سپس روی بنهاده بر روی اوی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بگفتا: جوانا، سرا، سرورا</p></div>
<div class="m2"><p>به دیدار و گفتار پیغمبرا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز رویم چرا دیده بر بسته ای</p></div>
<div class="m2"><p>همانا ز بس تاختن خسته ای</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تن افکار گردیدی از ترکتاز</p></div>
<div class="m2"><p>که اینگونه بر خوابت آمد نیاز</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>یکی سر بر آر گزین رود من</p></div>
<div class="m2"><p>از این خواب خوش، بهر بدرود من</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چه سان می سپارم من این راه چون؟</p></div>
<div class="m2"><p>دلم پیش تو، تن به پشت هیون</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خدا راجوان رحمی آخر به پیر</p></div>
<div class="m2"><p>دراین روز تنگم تو شو دستگیر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>الا ای بهار خزان دیده ام</p></div>
<div class="m2"><p>شکیب دلم، بینش دیده ام</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>مرا کاش می گشت بیننده، کور</p></div>
<div class="m2"><p>و یا خوابگاهم شدی خاک گور</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نمی دیدمت خفته در خون و خاک</p></div>
<div class="m2"><p>سرت برنی و پیکرت چاک چاک</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>که افکندت از اسب ای شهسوار</p></div>
<div class="m2"><p>که مادر زمرگش شود سوگوار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>که طاووس باغ مرا پر شکست؟</p></div>
<div class="m2"><p>که بر من در شادکامی ببست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>گل فاطمه را که پژمرده کرد؟</p></div>
<div class="m2"><p>نبی را ز داغش که افسرده کرد؟</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ایا یوسف من تو را گاه سور</p></div>
<div class="m2"><p>به کابین در آمد زلیخای گور</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>تو را رخت دامادی آمد کفن</p></div>
<div class="m2"><p>دریغا که آن هم نداری به تن</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>من از خاک خوشنودم ای کامیاب</p></div>
<div class="m2"><p>که پوشیده دارد تنت ز آفتاب</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>نهاده زمین مادرانه سرت</p></div>
<div class="m2"><p>به سینه که بر سر نبد مادرت</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ز سویی نشستند با صد فسوس</p></div>
<div class="m2"><p>به بالین داماد، مام و عروس</p></div></div>