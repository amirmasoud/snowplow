---
title: >-
    بخش ۷۶ - مفاخرت یکی از کوفیان و پاسخ حضرت فاطمه اورا
---
# بخش ۷۶ - مفاخرت یکی از کوفیان و پاسخ حضرت فاطمه اورا

<div class="b" id="bn1"><div class="m1"><p>پی خود ستایی زبان برگشود</p></div>
<div class="m2"><p>دو بیتی به تازی زبان بر سرود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که از تیغ ما کشته شد بوتراب</p></div>
<div class="m2"><p>سر زاده گانش در آمد به خواب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ببستیم چون مردم ترک و زنج</p></div>
<div class="m2"><p>زنان و را دست دادیم رنج</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خروشید بانو به وی گفت: هان</p></div>
<div class="m2"><p>چه گفتی که سنگت رسد بردهان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بنازی زقتل گروهی که کرد</p></div>
<div class="m2"><p>ز هر عیبشان پاک یزدان فرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زبان بند و بر زشتی خود مبال</p></div>
<div class="m2"><p>که باشد بدی کیفر بد سگال</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از آنتان به ما رشک آمد پدید</p></div>
<div class="m2"><p>که یزدانمان از شما برگزید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نباشد چو دریا به جوش از سراب</p></div>
<div class="m2"><p>نگیرد گنه کس به دریای آب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به هرکس خدا داد آن را که خواست</p></div>
<div class="m2"><p>کسی را نه یارای چون و چراست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زگفتار بانو چو ابر بهار</p></div>
<div class="m2"><p>گرستند مرد و زن کوفه زار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خروش زن و مرد ازکوی و بام</p></div>
<div class="m2"><p>گذشت از برگنبد نیلفام</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بگفتند: کای دختر شهریار</p></div>
<div class="m2"><p>از این بیش ما را مکن شرمسار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چو خاموش شد فاطمه از خروش</p></div>
<div class="m2"><p>ز غم زد دل ام کلثوم، جوش</p></div></div>