---
title: >-
    بخش ۹۵ - مسلمان شدن مرد نصارا به اعجاز مطهر امام(ع)و شهادت آن با سعادت
---
# بخش ۹۵ - مسلمان شدن مرد نصارا به اعجاز مطهر امام(ع)و شهادت آن با سعادت

<div class="b" id="bn1"><div class="m1"><p>همی خواست آن مرد فرخنده پی</p></div>
<div class="m2"><p>سوی بیت مقدس کند راه طی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بد استاده آنجا و کردی نظر</p></div>
<div class="m2"><p>چو بشنید قرآن از آن پاک سر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مرا گفت کای یار بنگر شگفت</p></div>
<div class="m2"><p>که دیده سر بی تن آید به گفت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بکن آگه از نام این سر مرا</p></div>
<div class="m2"><p>گمانم که او هست پیغمبرا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بگفتم نه خود پور پیغمبر است</p></div>
<div class="m2"><p>که ما را به اسلام او رهبر است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یکی تیغ برنده آن نیکبخت</p></div>
<div class="m2"><p>همیشه نهان داشت در زیر رخت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو بشنید گفتار من بی دریغ</p></div>
<div class="m2"><p>مسلمانی آورد و بگرفت تیغ</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بزد بر سپه خویش را بی درنگ</p></div>
<div class="m2"><p>بیالود از خونشان تیغ و چنگ</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به پایان بشد کشته در کار زار</p></div>
<div class="m2"><p>چو دید آنچنان ام کلثوم زار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بدو سخن بگریست از روی درد</p></div>
<div class="m2"><p>ستودش بدان کار نیکو که کرد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بگفت ای عجب مرد عیسی پرست</p></div>
<div class="m2"><p>پی یاری ما برآورده دست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ولی رو سیه امت مصطفی (ص)</p></div>
<div class="m2"><p>نجویند با ما به غیر از جفا</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چو شد کشته ترسا برفتم روان</p></div>
<div class="m2"><p>همی تا به نزد شه ناتوان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بدیدمش بسته به زنجیر پای</p></div>
<div class="m2"><p>سرش بی عمامه تنش بی ردای</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>شدم پیش و بوسیدمش پا و دست</p></div>
<div class="m2"><p>بپرسید نامم شه حق پرست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بدادمش از نام خود آگهی</p></div>
<div class="m2"><p>هم از خدمتی کامد از این رهی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بفرمود کز جامه با خویشتن</p></div>
<div class="m2"><p>چه داری بیاور به نزدیک من</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ز پوشش مرا هر چه بودی به بر</p></div>
<div class="m2"><p>برون کردم از تن ز پا تا به سر</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>نهشتم که چیزی بماند به جای</p></div>
<div class="m2"><p>ببردم به نزدیک آن رهنمای</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بپذیرفت آن را شه دادراست</p></div>
<div class="m2"><p>مرا مزد نیک از خداوند خواست</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>به اهل حرم آن خداوند راد</p></div>
<div class="m2"><p>به هریک یکی بهره زان جامه داد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>وزان پاره ای خویش بر سر ببست</p></div>
<div class="m2"><p>که بودش برهنه سرحقپرست</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>سوی شهر زان پس گرفتند راه</p></div>
<div class="m2"><p>اسیران ز دنبال و در پیش شاه</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>من او را همی رفتم اندر رکاب</p></div>
<div class="m2"><p>شتابان به سرخاک و دو دیده آب</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>به ناگه یکی غرفه آمد پدید</p></div>
<div class="m2"><p>دران پنج زن جمله زشت و پلید</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>وز آنان مهین تر یکی پیر زال</p></div>
<div class="m2"><p>بداندیش و از تخمه ی بد سگال</p></div></div>