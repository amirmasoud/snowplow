---
title: >-
    بخش ۵۰ - زبان حال حضرت زینب (س) با مرکب غرقه به خون برادر
---
# بخش ۵۰ - زبان حال حضرت زینب (س) با مرکب غرقه به خون برادر

<div class="b" id="bn1"><div class="m1"><p>سر بانوان بهشت برین</p></div>
<div class="m2"><p>مهین پرده گی دخت ضرغام دین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گرفتش روان کرده از دیده آب</p></div>
<div class="m2"><p>به دستی عنان و به دستی رکاب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همی زار گفت ای سمند نبی</p></div>
<div class="m2"><p>سرافراز اسب شه یثربی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ایا باد پیمای بیدای عشق</p></div>
<div class="m2"><p>ایا رفرف عرش پیمای عشق</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بلند آسمانی که بودت به زین</p></div>
<div class="m2"><p>چرا پشتش انداختی بر زمین؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو دوشت تهی ماند از بار عشق</p></div>
<div class="m2"><p>چرا دور گشتی ز سالار عشق</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نگشتی چرا سایبان برسرش</p></div>
<div class="m2"><p>که کمتر بسوزد زخور پیکرش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نماندی چو نهاد برخاک، سر</p></div>
<div class="m2"><p>کشی از برش، ناوک چارپر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گمانم بدین در از آن تاختی</p></div>
<div class="m2"><p>بر و یال خود غرق خون ساختی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>که گویی مرا کشته شد شاه تو</p></div>
<div class="m2"><p>فتاد از سپهر مهی، ماه تو</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از این آمدن بردی آرام من</p></div>
<div class="m2"><p>که باز آمدی بی دل آرام من</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بیا تا زنم بوسه ات بر رکاب</p></div>
<div class="m2"><p>که از خون شاه است بروی خضاب</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ببویم همی موی دلجوی تو</p></div>
<div class="m2"><p>که بوی حسین (ع) آید از موی تو</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>دریغا به چشمم نمانده است آب</p></div>
<div class="m2"><p>که شویم تو را یال از خون خضاب</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نماندی مرا کاشکی روزگار</p></div>
<div class="m2"><p>که اینگونه بینم تو را بی سوار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چنین روز اگر دیدمی من به خواب</p></div>
<div class="m2"><p>از این پیش رفتی مرا صبر و تاب</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>کنونم که پیش آمده، چون کنم؟</p></div>
<div class="m2"><p>مگر رو چو مجنون به هامون کنم</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ولیکن چه سازم به طفلان شاه</p></div>
<div class="m2"><p>که دیگر ندارند جز من پناه</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ایا چرخ دیگر مگردم به سر</p></div>
<div class="m2"><p>ایا خاک برخود مخواهم گذر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چه سان بی برادر کنم زنده گی؟</p></div>
<div class="m2"><p>سرآید مرا کاش پاینده گی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>همانا در این روز پر شور و شر</p></div>
<div class="m2"><p>ز نو گشته ام بی نیا و پدر</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>از این پیش در سایه ی فرشاه</p></div>
<div class="m2"><p>زمن جست هر بی پناهی پناه</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>کنون در زمانه پناهم نماند</p></div>
<div class="m2"><p>شدم بنده چون پادشاهم ماند</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ندانم از این پس مرا چاره چیست</p></div>
<div class="m2"><p>که از محرمی برسرم سایه نیست</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چو لشگر بتازد سوی خیمه گاه</p></div>
<div class="m2"><p>چه سازم بدین خردسالان شاه</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>چو آن شیون افتاد در بانوان</p></div>
<div class="m2"><p>سمند شهنشاه زار و نوان</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>دگر باره آمد سوی قتلگاه</p></div>
<div class="m2"><p>به بدرود زد بوسه بر پای شاه</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>خداوند خود را چو بدرود کرد</p></div>
<div class="m2"><p>چو سیل دمان سر سوی رودکرد</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بیفکند خود را به آب فرات</p></div>
<div class="m2"><p>وز آن سو برون رفت و شد در فلات</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>کسی دیگر او را به گیتی ندید</p></div>
<div class="m2"><p>تو گفتی مگر سوی گردون پرید</p></div></div>