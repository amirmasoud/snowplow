---
title: >-
    بخش ۱۱۵ - زبان حال و ترجمه ی اشعار حضرت ام کلثوم (ع) در درواز ه ی مدینه
---
# بخش ۱۱۵ - زبان حال و ترجمه ی اشعار حضرت ام کلثوم (ع) در درواز ه ی مدینه

<div class="b" id="bn1"><div class="m1"><p>چه از دور شد باره ی آن دیار</p></div>
<div class="m2"><p>به چشم دل افسرده گان آشکار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو دریای گردون به جوش آمدند</p></div>
<div class="m2"><p>بسی سخت تر در خروش آمدند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به رخ ام کلثوم خوناب راند</p></div>
<div class="m2"><p>به تازی زبان شعر چندی بخواند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>که درخویش ای تختگاه رسول</p></div>
<div class="m2"><p>مده راه و منمای ما را قبول</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو رفتیم از تو سری داشتیم</p></div>
<div class="m2"><p>وزان سر، به سر افسری داشتیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>برون از تو کاری که ما کرده ایم</p></div>
<div class="m2"><p>برفتیم شاد و غم آورده ام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بد آویزه ی عرش سالار ما</p></div>
<div class="m2"><p>برادرش میر و علمدار ما</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو پروانه بودیم و او شمع ما</p></div>
<div class="m2"><p>پریشان نبد خاطر جمع ما</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>یکی دوده بودیم آراسته</p></div>
<div class="m2"><p>پر از نو جوانان نو خاسته</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بسی خردسالان ز دخت و پسر</p></div>
<div class="m2"><p>به سیمای تابان چو شمس و قمر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>زنان در پس پرده با گوشوار</p></div>
<div class="m2"><p>جوانان بر اسبان تازی سوار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>کنون کامدم ای مدینه ز راه</p></div>
<div class="m2"><p>نداریم با خود علمدار و شاه</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>شد آن شمع از باد فتنه خموش</p></div>
<div class="m2"><p>سپردند پروانه گان نیز هوش</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>از آن نوجوانان مینو خصال</p></div>
<div class="m2"><p>وزان خردسالان نیکو جمال</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نباشد یکی زنده همراه ما</p></div>
<div class="m2"><p>که با ما درآید به بنگاه ما</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بگو ای مدینه به شاه حرم</p></div>
<div class="m2"><p>شه آسمان تخت پروین علم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>که ماییم اولاد تو ای رسول</p></div>
<div class="m2"><p>پس ازتو چنین گشته زار و ملول</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بکشتند چشم و چراغ تو را</p></div>
<div class="m2"><p>حسین (ع) آن سهی سرو باغ تو را</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>دریدند ما را زخرد و بزرگ</p></div>
<div class="m2"><p>به دندان شمشیر امت چو گرگ</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>فکندند پر خون برهنه به خاک</p></div>
<div class="m2"><p>تنی را که بود او ترا جان پاک</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>همان پرده گی دختران تو را</p></div>
<div class="m2"><p>به چرخ شرف اختران تو را</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>اسیرانه درشهر و بازارها</p></div>
<div class="m2"><p>کشیدند و دادند آزارها</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>زنانی که بر رویشان بی نقاب</p></div>
<div class="m2"><p>نبودی رضا بنگرد آفتاب</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>برهنه بر چشم نامحرمان</p></div>
<div class="m2"><p>ببردند بسته به یک ریسمان</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>الا ای مدینه به سوی رسول</p></div>
<div class="m2"><p>چو بردی پیامم بگو با بتول</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>که ای دختر پادشاه حرم</p></div>
<div class="m2"><p>همال علی(ع) مادر محترم</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>تو می دیدی ار دختران را به شام</p></div>
<div class="m2"><p>برهنه بردیده ی خاص و عام</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>به هر کوی و بازار اشتر سوار</p></div>
<div class="m2"><p>گشاده سر و مو پریشان و زار</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>ز بیخوابی شب تو گفتی که نور</p></div>
<div class="m2"><p>برون رفته از چشم و گردیده کور</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>و گر دیدی آندم که دربند بود</p></div>
<div class="m2"><p>همان ناتوان کت جگر بند بود</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>اگر تا قیامت بدی زنده جان</p></div>
<div class="m2"><p>کشیدی ز دل زان مصیب فغان</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>یکی مادرا سر ز تربت برآر</p></div>
<div class="m2"><p>ستمدیده گان را بشو غمگسار</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>به یاد یکی سخت هنگامه بین</p></div>
<div class="m2"><p>همه زاده گان را سیه جامه بین</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>چو لختی چنین گوهر داغ سفت</p></div>
<div class="m2"><p>خروشید و با شاه بیمار گفت</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>که بشتاب از ایدر به درد و محن</p></div>
<div class="m2"><p>به سوی ستودان عمت حسن (ع)</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>بدو کو برادرت آن شاه دین</p></div>
<div class="m2"><p>نهان گشت درخاک گرم زمین</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>چه کردی اگر دیدی ای شهریار</p></div>
<div class="m2"><p>حریم برادرت را خوار و زار</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>گهی سر برهنه به هر مرز و بوم</p></div>
<div class="m2"><p>گهی جا به ویران نموده چو بوم</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>پس آورد رو سوی شاه امم</p></div>
<div class="m2"><p>همی گفت باله و درد غم</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>که ای آفرینش تو را بنده گان</p></div>
<div class="m2"><p>سپهر و زمینت پرستنده گان</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>بکشتند فرخ برادرم را</p></div>
<div class="m2"><p>ز پرده کشیدند خواهرم را</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>ربودند آویز و خلخال پای</p></div>
<div class="m2"><p>ز دخت برادرم ای رهنما</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>سکینه خروشان حزین فاطمه</p></div>
<div class="m2"><p>همه زاده گان تو در واهمه</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>که اینکه به آتش بسوزند مان</p></div>
<div class="m2"><p>و یا تن به ناوک بدوزندمان</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>شها تاجدارا به تربت بموی</p></div>
<div class="m2"><p>رخ از مرگ فرزند در خون بشوی</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>به روز سیاه من ای مردمان</p></div>
<div class="m2"><p>ببارید خون از مژه این زمان</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>بدین گونه آن بانوان مویه گر</p></div>
<div class="m2"><p>برفتند تا قبر خیرالبشر</p></div></div>