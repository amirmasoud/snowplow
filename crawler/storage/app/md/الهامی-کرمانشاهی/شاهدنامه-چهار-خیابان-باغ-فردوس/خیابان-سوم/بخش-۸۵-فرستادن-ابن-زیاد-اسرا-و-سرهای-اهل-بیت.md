---
title: >-
    بخش ۸۵ - فرستادن ابن زیاد اسرا و سرهای اهل بیت
---
# بخش ۸۵ - فرستادن ابن زیاد اسرا و سرهای اهل بیت

<div class="b" id="bn1"><div class="m1"><p>یکی نامه آمد به ابن زیاد</p></div>
<div class="m2"><p>ز نزد یزید آن سر هر فساد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که سرهای آل علی (ع) را تمام</p></div>
<div class="m2"><p>ابا اهل بیتش سوی شهر شام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>روان ساز با مردمی هوشمند</p></div>
<div class="m2"><p>که در ره نیاید برایشان گزند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به امر یزید آن بد اندیش دین</p></div>
<div class="m2"><p>یکی لشگر آراست از اهل کین</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سپرد آن سران و زنان اسیر</p></div>
<div class="m2"><p>به ضجر بن قیس و به شمر شریر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دگر محسن شوم ناپاک زاد</p></div>
<div class="m2"><p>که بد تغلبه باب آن بد نژاد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به نیزه برافراختند آن سپاه</p></div>
<div class="m2"><p>سر پاک شه را و یاران شاه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زنان حریم رسول خدای</p></div>
<div class="m2"><p>برهنه سر و بر شتر بسته پای</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زکوفه سپردند یک لخت راه</p></div>
<div class="m2"><p>به نزدیک رود فرات آن سپاه</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گزیدند منزل به ویرانه ای</p></div>
<div class="m2"><p>نه ویرانه ای بلکه غمخانه ای</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سر شاه و یاران آن شاه را</p></div>
<div class="m2"><p>که بودند پرتو فکن، ماه را</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به دیوار ویرانه آویختند</p></div>
<div class="m2"><p>فلک را به سر خاک غم بیختند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>وزآن پس به می برگشودند دست</p></div>
<div class="m2"><p>چو از باده ی ناب گشتند مست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بدیدند آن مردمان پلید</p></div>
<div class="m2"><p>شد از غیب ناگاه دستی پدید</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>به کلکی ز فولا د با خون نوشت</p></div>
<div class="m2"><p>به دیوار، کای مردم بد سرشت</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>به دشت بلا تشنه با تیغ کین</p></div>
<div class="m2"><p>بکشتید سبط رسول امین</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>همانا شما راست، زان شهریار</p></div>
<div class="m2"><p>امید شفاعت به روز شمار؟</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>به پا خاستند آن گروه شریر</p></div>
<div class="m2"><p>که شاید بگیرند دست دبیر</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ز بیننده ی آن سپاه پلید</p></div>
<div class="m2"><p>مرآن دست فرخنده شد ناپدید</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>دگر ره چو زان کار لختی گذشت</p></div>
<div class="m2"><p>همان دست غیبی پدیدار گشت</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>نوشت آن کسانی که کشتند زار</p></div>
<div class="m2"><p>لب تشنه، شه را لب رودبار</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>نگردند روز جزا کامیاب</p></div>
<div class="m2"><p>خداشان به دوزخ نماید عذاب</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>دگر ره سوی دست بردند دست</p></div>
<div class="m2"><p>نهان گشت زان قوم شیطان پرست</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>از آن کار، پر بیم گشت آن سپاه</p></div>
<div class="m2"><p>همان دم از آنجا گرفتند راه</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>به نزدیک تکریت چون آمدند</p></div>
<div class="m2"><p>پذیرنده مردم برون آمدند</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>درآنجا گروهی زترسا بدند</p></div>
<div class="m2"><p>که درکیش و دین مسیحا بدند</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>زنی چند دیدند اشتر سوار</p></div>
<div class="m2"><p>سری چند بر نیزه ها استوار</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>پژوهش نمودند کاین قوم زار</p></div>
<div class="m2"><p>اسیران رومند یا زنگبار؟</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بگفتند: خود آل پیغمبرند</p></div>
<div class="m2"><p>که اشتر سوار و برهنه سرند</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>چو بر حال ایشان شناسا شدند</p></div>
<div class="m2"><p>همان دم به سوی کلیسا شدند</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>تعجب کنان دست بر سر زدند</p></div>
<div class="m2"><p>همی زار ناقوس غم بر زدند</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>که فرزند پیغمبر خویش را</p></div>
<div class="m2"><p>بکشتند و شادند زین ماجرا</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>کنون آل او را برهنه، سوار</p></div>
<div class="m2"><p>نمودند بر ناقه ها سوگوار</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>شمارند خود را مسلمان همی</p></div>
<div class="m2"><p>بدانند از اهل ایمان همی</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>تفو باد بر رسم و آیینشان</p></div>
<div class="m2"><p>کند داور پاک، نفرینشان</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>خدایا تو ما را بدیشان مگیر</p></div>
<div class="m2"><p>که هستیم از کار این قوم سیر</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>ز تکریت چون آن گروه لئام</p></div>
<div class="m2"><p>برفتند با آل خیرالانام</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>به صحرای نخله خروش و فغان</p></div>
<div class="m2"><p>شنیدند از لشگر جنیان</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>که بودند در ماتم شهریار</p></div>
<div class="m2"><p>خروشان و گریان چو ابر بهار</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>رسیدند چون آن بد اختر سپاه</p></div>
<div class="m2"><p>شتابان به مرشاد از گرد راه</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>زن و مرد آنجا ز پیر و جوان</p></div>
<div class="m2"><p>چو دیدند سرها و آن بانوان</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>بدادند با گریه و مویه سر</p></div>
<div class="m2"><p>درود فراوان به خیرالبشر</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>به بدخواه دین امیر عرب</p></div>
<div class="m2"><p>پیاپی به نفرین گشودند لب</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>به حران درون چون سپاه آمدند</p></div>
<div class="m2"><p>تن آسوده از رنج راه آمدند</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>به تلی یکی مرد موسی پرست</p></div>
<div class="m2"><p>بدش خانه و جایگاه نشست</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>ز حق آن سرافراز فرخ نهاد</p></div>
<div class="m2"><p>بدش نام یحیی و نیکو نژاد</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>چو دید آن سپه را و چندین اسیر</p></div>
<div class="m2"><p>برای تماشا بیامد به زیر</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>سر شاه را دید کاندر سنان</p></div>
<div class="m2"><p>دمادم همی جنبد او را لبان</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>چو نیکو نیوشید آن سرفراز</p></div>
<div class="m2"><p>همی خواند قرآن به لحن حجاز</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>زلشگر بپرسید: کاین سر زکیست؟</p></div>
<div class="m2"><p>دگر باب و مام ورا نام چیست؟</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>چو گفتند و بشناخت او شاه را</p></div>
<div class="m2"><p>پدرش آن شه عرش خرگاه را</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>ز دینی که خود داشت پیچید سر</p></div>
<div class="m2"><p>درآمد به آیین خیرالبشر</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>زسر، پاک دستار خود راد مرد</p></div>
<div class="m2"><p>بیفکند آن را، سپس پاره کرد</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>به هر زن یکی پاره چون زان بداد</p></div>
<div class="m2"><p>یکی جامه ی خز بدش پاکزاد</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>بیاورد آن را به نزد امام</p></div>
<div class="m2"><p>بگفتا: که ای پور خیرالانام</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>مر این جامه ی نغز و دینار چند</p></div>
<div class="m2"><p>مرا هست، بپذیر از این مستمند</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>چو دیدند کردار آن نیکبخت</p></div>
<div class="m2"><p>سپه نعره ها برکشیدند سخت</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>که ای مرد زین جا برو برکنار</p></div>
<div class="m2"><p>تو را چیست با دشمن شاه کار؟</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>چو این دید یحیی کشید از میان</p></div>
<div class="m2"><p>سبک تیغ و زد بر صف کوفیان</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>بسی کشت و شد کشته در راه دین</p></div>
<div class="m2"><p>به جانش درود از جهان آفرین</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>به حران مر او را یکی بارگاه</p></div>
<div class="m2"><p>بود تاکنون همچو در چرخ، ماه</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>پناه خلایق بود در گهش</p></div>
<div class="m2"><p>به رتبت، بر، ازچرخ، خاک رهش</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>از آن جایگه چون سپردند راه</p></div>
<div class="m2"><p>به سوی نصیبین کوفه سپاه</p></div></div>