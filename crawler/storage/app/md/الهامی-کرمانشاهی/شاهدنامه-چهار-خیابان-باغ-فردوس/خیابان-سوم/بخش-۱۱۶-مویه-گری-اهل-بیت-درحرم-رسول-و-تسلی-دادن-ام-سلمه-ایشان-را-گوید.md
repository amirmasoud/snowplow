---
title: >-
    بخش ۱۱۶ - مویه گری اهل بیت درحرم رسول و تسلی دادن ام سلمه ایشان را گوید
---
# بخش ۱۱۶ - مویه گری اهل بیت درحرم رسول و تسلی دادن ام سلمه ایشان را گوید

<div class="b" id="bn1"><div class="m1"><p>زبان های آتشفشان همچو شمع</p></div>
<div class="m2"><p>در آن تربت پاک گشتند جمع</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چگویم چه گفتند و چون گشت کار</p></div>
<div class="m2"><p>قیامت درآن لحظه گشت آشکار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>برآمد ز افغان آن قوم آه</p></div>
<div class="m2"><p>ز هر چوب و هر خشت آن بارگاه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به تربت نبی دست غم زده به سر</p></div>
<div class="m2"><p>خبر شد چو از مرگ فرخ پسر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>برآورد از غم بدانسان خروش</p></div>
<div class="m2"><p>که آمد نیوشنده گان را به گوش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>روان پیمبر چو از غم گریست</p></div>
<div class="m2"><p>ازان گریه اهل دو عالم گریست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به ناگاه زینب (س) برآورد آه</p></div>
<div class="m2"><p>بدان سان که شد روی گردون سیاه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به زاری بگفت ای رسول انام</p></div>
<div class="m2"><p>مرا مهربان تر زباب و زمام</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>منم پیک سوک جگر بند تو</p></div>
<div class="m2"><p>خبر دارم از مرگ فرزند تو</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در این گفته ی خویش دارم گواه</p></div>
<div class="m2"><p>بیاوردمش اندرین بارگاه</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بگفت این و از زیر چادر برون</p></div>
<div class="m2"><p>برآورد پیراهنی پر ز خون</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بیفکند بر روی صندوق شاه</p></div>
<div class="m2"><p>که در مرگ پور تو، اینم گواه</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بکشتند امت به شمشیر کین</p></div>
<div class="m2"><p>حسین (ع) تو را ای خداوند دین</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>در آندم بر آمد ز درگاه غو</p></div>
<div class="m2"><p>شد آن مجلس از ماتم گریه نو</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>همال نبی ام سلمه ز در</p></div>
<div class="m2"><p>درآمد شخوده رخ و مویه گر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>به یکدست او شیشه ای پر زخون</p></div>
<div class="m2"><p>که بد تربت داور رهنمون</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>به دستی دگر دست دخت امام</p></div>
<div class="m2"><p>که او را پدر داده بد نام مام</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>به یکبار آل پیمبر همه</p></div>
<div class="m2"><p>گرفتند پیراهن فاطمه</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>کشیدند او را خروشان به بر</p></div>
<div class="m2"><p>بسی بوسه دادند بر چشم و سر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>زکف درگسستند پیوند صبر</p></div>
<div class="m2"><p>خروش از مدینه برآمد به ابر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>به یثرب زن و مرد هرکس که بود</p></div>
<div class="m2"><p>همی موی کند و همی رخ خشنود</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ز بس شد خشوده رخ سیمگون</p></div>
<div class="m2"><p>زهر کوی گفتی روانست خون</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>سرانجام جفت رسول امین</p></div>
<div class="m2"><p>دلش سوخت بر بانوان غمین</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>به اندرز ایشان همی لب گشاد</p></div>
<div class="m2"><p>شکیبایی از سوگواری بداد</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>به ایوان خون برد همراهشان</p></div>
<div class="m2"><p>نشستند در ماتم شاهشان</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>همی زنده بودند تا درجهان</p></div>
<div class="m2"><p>به ماتم بدند آشکار و نهان</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>شب و روز بودند پر آب چهر</p></div>
<div class="m2"><p>چنین است کردار گردان سپهر</p></div></div>