---
title: >-
    بخش ۱۳ - داستان بچه آهویی که صیادی به پیغمبر هدیه می دهد
---
# بخش ۱۳ - داستان بچه آهویی که صیادی به پیغمبر هدیه می دهد

<div class="b" id="bn1"><div class="m1"><p>یکی روز پیغمبر سرفراز</p></div>
<div class="m2"><p>به مسجد درون بود بهر نماز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیامد یکی مرد نخجیر گیر</p></div>
<div class="m2"><p>بیاورد از بهر آن بی نظیر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یکی بره آهوی خوش خط و خال</p></div>
<div class="m2"><p>به فرخ حسن (ع) دادش آن بی همال</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرا دل زانده در آمد به جوش</p></div>
<div class="m2"><p>همی خواستم تابر آرم خروش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دل پاک فرمانگذار حرم</p></div>
<div class="m2"><p>نتابید تا من بمانم دژم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بنالید بر درگه بی نیاز</p></div>
<div class="m2"><p>که یارب حسین (ع) مرا شاد ساز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو یک لخت بگذشت ناگه زدشت</p></div>
<div class="m2"><p>یکی ماده آهو پدیدار گشت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به پیش اندرش بچه ی او دمان</p></div>
<div class="m2"><p>بیامد بر پادشاه زمان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به فرمان یزدان زبان برگشاد</p></div>
<div class="m2"><p>بگفتا: که ای شاه روشن نهاد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دو کودک مرا بود نوشنده شیر</p></div>
<div class="m2"><p>یکی را زمن برد نخجیر گیر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بیاورد شه را ره آورد داد</p></div>
<div class="m2"><p>دل من بداد دیگری بود شاد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>که ناگه رسیدم زگردون به گوش</p></div>
<div class="m2"><p>زفرخ سروشان چرخ این خروش</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>که ای ماده آهوی رعنا خرام</p></div>
<div class="m2"><p>برو تا به درگاه خیرالانام</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ببر بچه ی خویش را با شتاب</p></div>
<div class="m2"><p>پی هدیه ی آن شه کامیاب</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>که او نیز بخشد به پورش حسین (ع)</p></div>
<div class="m2"><p>از آن پیش کاید سرشکش زعین</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>که گرد ریزد از دیده یک قطره آب</p></div>
<div class="m2"><p>دل قدسیان گردد از غم کباب</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>از آن بانگ با بچه ام بی هراس</p></div>
<div class="m2"><p>دوان آمدم دارم از حق سپاس</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>که اینجا رسیدم از آن پیشتر</p></div>
<div class="m2"><p>که چشم حسین (ع) آید از اشک تو</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>پیمبر (ص) از آن حال گردید شاد</p></div>
<div class="m2"><p>مر آن اهوک را به من باز داد</p></div></div>