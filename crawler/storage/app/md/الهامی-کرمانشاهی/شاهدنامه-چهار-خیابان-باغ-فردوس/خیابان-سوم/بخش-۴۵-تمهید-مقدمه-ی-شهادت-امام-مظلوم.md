---
title: >-
    بخش ۴۵ - تمهید مقدمه ی شهادت امام مظلوم
---
# بخش ۴۵ - تمهید مقدمه ی شهادت امام مظلوم

<div class="b" id="bn1"><div class="m1"><p>چو فرمان بود از امامان پاک</p></div>
<div class="m2"><p>که گوییم از این ماتم هولناک</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بگویم و گرنه مرا بد گریز</p></div>
<div class="m2"><p>ازاین گفته گرمی شدم ریز ریز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تو نیز ای نیوشای آوای من</p></div>
<div class="m2"><p>چو آهنگ غم برکشد نای من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نگهدار دل را به چنگ سکون</p></div>
<div class="m2"><p>که از دیده با خون نیاید برون</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر این داستان را بخوانی به کوه</p></div>
<div class="m2"><p>کمر خم کند وزغم آید ستوه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شود چشمه سارش زانده، سراب</p></div>
<div class="m2"><p>زدامان او خون بجوشد، نه آب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اگر چند یکتا خدای جهان</p></div>
<div class="m2"><p>بود پاک از شادی و اندهان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ولیکن نخست آنکه بزم عزا</p></div>
<div class="m2"><p>بیاراست زین ماتم جانگزا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خدای جهان آفرین بود و بس</p></div>
<div class="m2"><p>که او بود و جز او نبد هیچ کس</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زهی ماتم شاه لب تشنه گان</p></div>
<div class="m2"><p>که زد خیمه برساحت لامکان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بدو چشم لاهوتیان، خون گریست</p></div>
<div class="m2"><p>وزان جمله پیغمبر، افزون گریست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دل آنکه چون حلقه ی خاتم است</p></div>
<div class="m2"><p>درین غم دل سید خاتم است</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ایا عرش درگاه پیغمبر</p></div>
<div class="m2"><p>خدا را نبی، خلق را رهبرا</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>به ریحانه ی خویش لختی بموی</p></div>
<div class="m2"><p>دو گلگونه از آب مژگان بشوی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>فرو ریز از دیده ی بو تراب</p></div>
<div class="m2"><p>دو جوی سرشک روان برتراب</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>که کشتند طاووس باغ تو را</p></div>
<div class="m2"><p>وزو تازه کردند داغ تو را</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ایا تابش افزای رخشنده هور</p></div>
<div class="m2"><p>که خاک رهت سرمه ی چشم حور</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>کنیزت ز جان مریم پرده گی</p></div>
<div class="m2"><p>تو را هاجر و ساره چون برده گی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>به سوگ جگر گوشه کن جامه چاک</p></div>
<div class="m2"><p>که دردانه ات خفته در خون و خاک</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بشد بی سر آن زیب آغوش تو</p></div>
<div class="m2"><p>روان داد پرورده ی دوش تو</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ایا خفته در خوابگاه بقیع</p></div>
<div class="m2"><p>جگر تفته از تاب زهر نقیع</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>زخون لعل شد آن در ناب سود</p></div>
<div class="m2"><p>که یاقوت آویزه ی عرش بود</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>تو را جان زنوشیدن آب، شد</p></div>
<div class="m2"><p>ز بی آبی آن شاه، بی تاب شد</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ایا اهل بیتی کتان بی نیاز</p></div>
<div class="m2"><p>به تشریف تطهیر داد امتیاز</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>شه ناتوان را به سوگ پدر</p></div>
<div class="m2"><p>ممانید غمگین و خونین جگر</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>شکیبا دهیدش که بس خسته است</p></div>
<div class="m2"><p>به زنجیر بیداد بر بسته است</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>یتیم است و بیمار خونین سرشک</p></div>
<div class="m2"><p>غل آهنینش به جای پزشک</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>شها داورا، ایزد دادگر</p></div>
<div class="m2"><p>شکیبا دهادت به سوگ پدر</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>ایا پیشوایان دین مبین</p></div>
<div class="m2"><p>به کیهان پس از سیدالمرسلین</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>شما داوران را شکیبا، خدا</p></div>
<div class="m2"><p>دهادا به سوگ شه کربلا</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>ایا حجت عصر و صاحب زمان</p></div>
<div class="m2"><p>خدیو زمین، داور آسمان</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>تو را زیبد ای مظهر دادگر</p></div>
<div class="m2"><p>که بر پا کنی بزم سوگ پدر</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>پسر به، به مرگ پدر مویه ساز</p></div>
<div class="m2"><p>شود ای جهان داور سرفراز</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>به سوگ پدر برکش از سینه آه</p></div>
<div class="m2"><p>نهان ساز تن در پرند سیاه</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>تو را شاید از غم کنی جامه، چاک</p></div>
<div class="m2"><p>که شد ریخته خون یزدان پاک</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>ایا ای برید خداوند وحی</p></div>
<div class="m2"><p>که آوردی از لامکان امر و نهی</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>شهی کت بدی پیک پروردگار</p></div>
<div class="m2"><p>ز گهواره جنبانیش افتخار</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>به شمشیر پولاد، شمر پلید</p></div>
<div class="m2"><p>به ده ضربت از پیکرش سر برید</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>در این ره مرا پای رفتار نیست</p></div>
<div class="m2"><p>از این بیشتر تاب گفتار نیست</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>شنو تا چسان شمر بد روزگار</p></div>
<div class="m2"><p>ز تن کرد دور آن سر تاجدار</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>کسی چون نپذرفت از آن سپاه</p></div>
<div class="m2"><p>که برگیرد از تن سر پاک شاه</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>بشد شمر بی داد و دین، خشمگین</p></div>
<div class="m2"><p>همی بر خروشید و گفتا چنین:</p></div></div>