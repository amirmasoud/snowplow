---
title: >-
    بخش ۵۴ - روایت حمید بن مسلم کوفی
---
# بخش ۵۴ - روایت حمید بن مسلم کوفی

<div class="b" id="bn1"><div class="m1"><p>بینم که آن مردم پر زکین</p></div>
<div class="m2"><p>چه سازند با آل حبل المتین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به ناگه پدیدار شد شمر دون</p></div>
<div class="m2"><p>به گردش گروهی به بد رهنمون</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رسیدند آنجا که بیمار بود</p></div>
<div class="m2"><p>همان شاه با درد و تیمار بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بگفتند با شمر کاین ناتوان</p></div>
<div class="m2"><p>به جا مانده از هاشمی زاده گان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همان به که با خنجر آبدار</p></div>
<div class="m2"><p>رهانیمش از پیچش روزگار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو دیدم که آن مردم بد سرشت</p></div>
<div class="m2"><p>شدند استوار اندر آن کار زشت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بگفتم بدیشان به بانگی درشت</p></div>
<div class="m2"><p>که ای قوم! بیمار را کس نکشت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چه کرده است این کودک بی پناه</p></div>
<div class="m2"><p>به یزدان پناهید از این گناه</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از اینگونه کردم بس اندرزها</p></div>
<div class="m2"><p>که برهانمش از دم اژدها</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>عمر نیز ناگاه آنجا رسید</p></div>
<div class="m2"><p>مر آن پیچش و گفتگو را شنید</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به شمر تبهکار بر زد خروش</p></div>
<div class="m2"><p>که درقتل این خسته چندین مکوش</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>همین یک پرستار بهر زنان</p></div>
<div class="m2"><p>به جا مانده زو باز گردان عنان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چو آمد عمر نزد آن بانوان</p></div>
<div class="m2"><p>کشیدند از دل سراسر فغان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چنان شیون و ناله کردند سر</p></div>
<div class="m2"><p>که بگریست برحال ایشان عمر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>به لشگر خروشید دنیا پرست</p></div>
<div class="m2"><p>کز این بیکسان باز دارید دست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>نکوشد دگر کس به آزارشان</p></div>
<div class="m2"><p>بس است اینهمه رنج و تیمارشان</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>شد از گردشان چون سپه برکنار</p></div>
<div class="m2"><p>به پیش عمر لابه کردند زار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>که ما عترت پاک پیغمبرم</p></div>
<div class="m2"><p>روا نیست کاینسان برهنه سریم</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بگو تا ز چیزی که ناید به کار</p></div>
<div class="m2"><p>نمایند چندان به ما واگذار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>که پو شیم با آن سر خویش را</p></div>
<div class="m2"><p>زخود شاد کن داور خویش را</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>عمر گفت: یاران از این سود، دست</p></div>
<div class="m2"><p>بدارید و باز آورید آنچه هست</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>به هرکس دهید آنچه دارد نیاز</p></div>
<div class="m2"><p>که اندام خود را بپو شند باز</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بسی گفت و با وی ندادند گوش</p></div>
<div class="m2"><p>شد او خسته و خواستاران خموش</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چو بیچاره شد روی از ایشان بکاشت</p></div>
<div class="m2"><p>تنی را برایشان نگهبان گماشت</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بگفتا: سراسر ز خرد و بزرگ</p></div>
<div class="m2"><p>به یک خیمه همچون اسیران ترک</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بیاریدشان گرد و دارید پاس</p></div>
<div class="m2"><p>مبادا گریزد تنی از هراس</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>دریغا بدی در زمانه نماند</p></div>
<div class="m2"><p>که آل علی (ع) دفترش را نخواند</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>شگفتا که از شومی آن ستم</p></div>
<div class="m2"><p>نپاشید پیوند گیتی ز هم</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>چنین بوده تا بوده آیین چرخ</p></div>
<div class="m2"><p>که شایسته کس را نداده است برخ</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>ستمکار را بهره عیش است و گنج</p></div>
<div class="m2"><p>نکوکار پیوسته با درد و رنج</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>جهان همچو دریای پهناور است</p></div>
<div class="m2"><p>که لولوش در زیر خس برسر است</p></div></div>