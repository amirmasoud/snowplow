---
title: >-
    بخش ۹۶ - وارد کردن اهل بیت حضرت خیرالانان
---
# بخش ۹۶ - وارد کردن اهل بیت حضرت خیرالانان

<div class="b" id="bn1"><div class="m1"><p>پرستشگهی بود نزدیک راه</p></div>
<div class="m2"><p>به درگاه آن کینه گستر سپاه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حریم نبی را نگه داشتند</p></div>
<div class="m2"><p>به مسجد زنان دیده بگماشتند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به یاد آمد از شاه لولاکشان</p></div>
<div class="m2"><p>برآمد خروش از دل چاکشان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نمودند با مصطفی (ع) این خطاب</p></div>
<div class="m2"><p>که ای خانه ی دین پس ازتو خراب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در اسلام مسجد تو کردی بنا</p></div>
<div class="m2"><p>ز تو گشت محراب و منبر به پا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز تیغ کجت پشت دین شد چو راست</p></div>
<div class="m2"><p>ز گلدسته ها بانگ تکبیر خاست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تو را چون سوی جنت آمد شتاب</p></div>
<div class="m2"><p>پرستشگه آباد شد ما خراب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شگفت است کردار این مردمان</p></div>
<div class="m2"><p>که سازند این خانه بهر امان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نمایند برخانه حرمت فزون</p></div>
<div class="m2"><p>ز جسم خدا خانه ریزند خون</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>پرستش نمایند برنام تو</p></div>
<div class="m2"><p>مر این امت زشت فرجام تو</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نمایند با زاده گانت چنین</p></div>
<div class="m2"><p>که هرگز نکردی تو با مشرکین</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>برآور سر از تربت پاک خویش</p></div>
<div class="m2"><p>ببین تاکه ما را چه آمد به پیش</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>در این دم بیامد یکی مرد پیر</p></div>
<div class="m2"><p>نگه کرد بر آن گروه اسیر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بگفتا به یزدان داور سپاس</p></div>
<div class="m2"><p>که اسلام را از شما داشت پاس</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>به شمشیر شد کشته مردانتان</p></div>
<div class="m2"><p>برستند مردم ز دستانتان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>همین بد ز داور سزای شما</p></div>
<div class="m2"><p>که بر فتنه می بود رای شما</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>شه خسته را، دیده پر شد ز آب</p></div>
<div class="m2"><p>بگفتا که ای پیر بشنو جواب</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>کلام خدا هیچگه خوانده ای</p></div>
<div class="m2"><p>سخن هیچ از معنی اش رانده ای</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بگفتا بلی خوانده باشم بسی</p></div>
<div class="m2"><p>چو من نیست دانا به آن هر کسی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بگفتا درآنجا که گوید خدا</p></div>
<div class="m2"><p>که میگوی با امتت احمدا (ص)</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>که مزدی نمیخواهم از بیش و کم</p></div>
<div class="m2"><p>مگر نیکویی با ذوی القربی ام</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>مراد از ذوی القربی ای مرد کیست</p></div>
<div class="m2"><p>که نیکی بایشان برای نبی است</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بگفتا ذوی القربی اند آل او</p></div>
<div class="m2"><p>که برجای هستند زان پاکخو</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>شهش گفت جز ما ازان شهریار</p></div>
<div class="m2"><p>مجو یادگاری در این روزگار</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>سپس شاه گفتا جهان آفرین</p></div>
<div class="m2"><p>به فرقان نبی را بگفتا چنین</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>که ما اهل بیت تو را از بدی</p></div>
<div class="m2"><p>نمودیم پاک از ره ایزدی</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>همان دوده ی پاکجانیم ما</p></div>
<div class="m2"><p>که پاک از خدای جهانیم ما</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>زحق حکم تطهیر در شان ماست</p></div>
<div class="m2"><p>پیمبر نیا باب شیر خداست</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>چو پیر این شنید از خداوند دین</p></div>
<div class="m2"><p>سرافکنده در زیر و شد شرمگین</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>سپس سر بر آورد و بگریست زار</p></div>
<div class="m2"><p>بگفتا خطا کردم ای شهریار</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>ببخشا گناهم که نشناختم</p></div>
<div class="m2"><p>ازین کار دین راز کف باختم</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>سپس رو سوی قبله آورد مرد</p></div>
<div class="m2"><p>برآورد از سینه آوا به درد</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>به سوی خدازان گنه بازگشت</p></div>
<div class="m2"><p>پس آورد بر سوی شه بازگشت</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>بیفکند خود را به پای شتر</p></div>
<div class="m2"><p>فرو ریخت بر پایش از دیده در</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>بغلطید برخاک در پیش شاه</p></div>
<div class="m2"><p>که شاها بیا بگذر ازاین گناه</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>شهش گفت بخشید یزدان ترا</p></div>
<div class="m2"><p>بدید از گنه چون پشیمان ترا</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>چو بشنید این از شه حقپرست</p></div>
<div class="m2"><p>به زاری سوی ایزد افراشت دست</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>بگفتا که ای پاک جان آفرین</p></div>
<div class="m2"><p>نخواهم دگر زندگی شرمگین</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>نمودی اگر توبه ی من قبول</p></div>
<div class="m2"><p>به خلدم روان ساز پیش رسول</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>هماندم روانش برآمد زتن</p></div>
<div class="m2"><p>بشد سوی منزلگه خویشتن</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>خوشا عاشق مرده در پای دوست</p></div>
<div class="m2"><p>اگر مرگ این است مردن نکوست</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>شد آن روز آل علی (ص) را مقام</p></div>
<div class="m2"><p>به ویرانه ای پشت مسجد به شام</p></div></div>