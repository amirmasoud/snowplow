---
title: >-
    بخش ۴۹ - زبان حال علیا جاه سکینه خاتون با اسب بی صاحب پدر
---
# بخش ۴۹ - زبان حال علیا جاه سکینه خاتون با اسب بی صاحب پدر

<div class="b" id="bn1"><div class="m1"><p>پدر کشته دخت رسول انام</p></div>
<div class="m2"><p>سکینه، شکیب روان امام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به عناب نسرین که بی آب خست</p></div>
<div class="m2"><p>بیافکند بر گردن باره دست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بگفتا: که ای اسب فرخنده شاه</p></div>
<div class="m2"><p>تو آن دم که رفتی سوی رزمگاه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نبودت چنین ساز یاقوتگون</p></div>
<div class="m2"><p>نبودت چنین یال و بر، غرق خون</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تو در سایه ی شیر یزدان بدی</p></div>
<div class="m2"><p>که را زهره؟ تا با تو سازد بدی؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بگو: از چه باز آمدی بی سوار</p></div>
<div class="m2"><p>مگر کشته شد آن جهان شهریار؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو می رفت آن شه بسی تشنه بود</p></div>
<div class="m2"><p>چو یاقوت من بود لعلش کبود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو از کوهه ات کوه ایمان فتاد</p></div>
<div class="m2"><p>خدا را بگو کس به وی آب داد؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>و یا زآبگون دشنه سیراب شد</p></div>
<div class="m2"><p>در آن بستم گرم در خواب شد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در آن دم که می شست از خویش دست</p></div>
<div class="m2"><p>بگو تا جهان بین او را که بست؟</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کسی بست زخم تن روشنش؟</p></div>
<div class="m2"><p>که سوده نگردد بدان جوشنش</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>و یا دشمنش برد رخت وکلاه؟</p></div>
<div class="m2"><p>بشد مرهمش خاک آوردگاه</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>خدا را، به بالین، سرش جای داشت</p></div>
<div class="m2"><p>و یا شمر آن را به نی برفراشت؟</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>وز آن پس بنالید و برسر بزد</p></div>
<div class="m2"><p>چنان کز پدر کشته گان می سزد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>به افغان همی گفت: کای باب من</p></div>
<div class="m2"><p>فروغ جهان بین بی خواب من</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>کجا دور شد سایه ات از سرم</p></div>
<div class="m2"><p>به تاراج شد باره و معجرم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>دریغ ای پدر کردی آواره ام</p></div>
<div class="m2"><p>چه باشد بگو بعد از این چاره ام</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>دریغا از این رنج و راه دراز</p></div>
<div class="m2"><p>که از آن رهایی نیابیم باز</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چو لختی چنین گفت، خاموش شد</p></div>
<div class="m2"><p>بیفتاد از پا و بیهوش شد</p></div></div>