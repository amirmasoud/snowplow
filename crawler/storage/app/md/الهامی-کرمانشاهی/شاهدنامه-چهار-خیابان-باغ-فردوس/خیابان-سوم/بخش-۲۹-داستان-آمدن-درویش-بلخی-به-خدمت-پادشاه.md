---
title: >-
    بخش ۲۹ - داستان آمدن درویش بلخی به خدمت پادشاه
---
# بخش ۲۹ - داستان آمدن درویش بلخی به خدمت پادشاه

<div class="b" id="bn1"><div class="m1"><p>چو درویش از خویشتن رسته ای</p></div>
<div class="m2"><p>دل اندر ره نیستی بسته ای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز تاج تولا سرش تاجدار</p></div>
<div class="m2"><p>زهر ترک آن ترک جان آشکار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زچهل تارش این نکته بودی عیان</p></div>
<div class="m2"><p>که اندر وفا سخت بسته میان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همی گفت بر دوش او تخت پوست</p></div>
<div class="m2"><p>که در مغز او نیست جز یاد دوست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز گیسو به گردن کمند رضا</p></div>
<div class="m2"><p>به جان و به دل پیرو مرتضی (ع)</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز چوبش یکی کاسه برکف پرآب</p></div>
<div class="m2"><p>به دل عشقش آتش فکنده به آب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو از دور شه روی او را بدید</p></div>
<div class="m2"><p>ز سیمای او گشت بر وی پدید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>که باشد یکی ز آشنایان او</p></div>
<div class="m2"><p>ز یاران فرخنده رایان او</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به بر خواند او را شه ماه و مهر</p></div>
<div class="m2"><p>نظر بر وی افکند از روی مهر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هم از آن نظر کارها ساختش</p></div>
<div class="m2"><p>زبند دو گیتی رها ساختش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دل دیگرش داد و جان دگر</p></div>
<div class="m2"><p>کشیدش به سوی جهان دگر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به نرمی بدو گفت: کای رهنمود</p></div>
<div class="m2"><p>چه جویی در این پهندشت نبرد؟</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چه خواهی؟ که ای؟ از کجا آمدی؟</p></div>
<div class="m2"><p>شتابان بدین جا چرا آمدی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>در این دشت جز تیر و شمشیر نیست</p></div>
<div class="m2"><p>برو گر تو را زهره ی شیر نیست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>به شه گفت آن مرد شوریده حال</p></div>
<div class="m2"><p>که بلخ است جای من ای بی همال</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>سوی تربت شیر پروردگار</p></div>
<div class="m2"><p>ز بهر زیارت شدم رهسپار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>شب دوش با جان اندوهگین</p></div>
<div class="m2"><p>غنودم درین سرزمین خشمگین</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ازین برشده خرگه شاهوار</p></div>
<div class="m2"><p>شنیدم یکی کودک بی قرار</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ز لب تشنگی دم به دم می سرود</p></div>
<div class="m2"><p>ز سوز عطش تابم از تن ربود</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>سپیده چو این شمع آتش بدن</p></div>
<div class="m2"><p>پدید آمد از این زمرد لگن</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>من این کاسه بردم سوی رودبار</p></div>
<div class="m2"><p>نمودم پر از آب شیرین گوار</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>که شاید بدان خردسالش دهم</p></div>
<div class="m2"><p>دلش را ز سوز عطش وارهم</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>گرفت ازکفش شاه کشکول آب</p></div>
<div class="m2"><p>فرو ریخت آن آب را بر تراب</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بدو گفت: آب آفرینیم ما</p></div>
<div class="m2"><p>نه از تشنه کامی حزینیم ما</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>همه، تشنه ی وصل جانانه ایم</p></div>
<div class="m2"><p>از آن در هیاهو چو دیوانه ایم</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>نگه کرد ژولیده، دید آن زمین</p></div>
<div class="m2"><p>یکی ژوف دریا شده، سهمگین</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ز هر ناخن پنچ انگشت شاه</p></div>
<div class="m2"><p>یکی رود از آب بگرفته راه</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>ز یکسو گشاده در آسمان</p></div>
<div class="m2"><p>ملایک ستاده عنان در عنان</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>به کف هر یکی را یکی جام آب</p></div>
<div class="m2"><p>پی هدیه ی آن شه کامیاب</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>فروماند درویش اندر شگفت</p></div>
<div class="m2"><p>سپس شاه، کشکول را برگرفت</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>پر از ریگ کرد و بدان مرد داد</p></div>
<div class="m2"><p>چو نیکو نگه کرد آن پاک زاد</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>مرآن کاسه را دید بر جای سنگ</p></div>
<div class="m2"><p>بیاکنده از گوهر رنگ رنگ</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>چو آن دید شوریده ی ژنده پوش</p></div>
<div class="m2"><p>به خاک اندر افتاد و برزد خروش</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>ببوسید سم سمند امام</p></div>
<div class="m2"><p>بگفتا: که شاها تو را چیست نام؟</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>که در مشت تو سنگ مرجان شود</p></div>
<div class="m2"><p>وز انگشت تو خاک، عمان شود</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>ملایک همه زیر فرمان توست</p></div>
<div class="m2"><p>سپهر و زمین، گوی چوگان توست</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>بدو گفت: شاه علی (ع) گوهرم</p></div>
<div class="m2"><p>بود دختر مصطفی (ص) مادرم</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>حسینم (ع) که پروردی ام درکنار</p></div>
<div class="m2"><p>گرامی چو جان شیر پروردگار</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>مر این قوم در بند قتل منند</p></div>
<div class="m2"><p>از آنرو که با مرتضی (ع) دشمنند</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>چو آن راز جو آگه از کار شد</p></div>
<div class="m2"><p>دل روشنش جای تیمار شد</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>به حال شهنشاه بگریست زار</p></div>
<div class="m2"><p>وزو خواست دستوری کارزار</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>چو دستور دادش به میدان شتافت</p></div>
<div class="m2"><p>سنانی شکسته به ره بازیافت</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>ربود از زمین آن شکسته سنان</p></div>
<div class="m2"><p>بزد خویش را بر سپاهی گران</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>بدان نیزه کشت از سپه پنج مرد</p></div>
<div class="m2"><p>سپس جان خود برخی شاه کرد</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>ولیکن شنیدم ز دانشوران</p></div>
<div class="m2"><p>که از بعد شه چون اسد گوهران</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>به دفن شهنشاه بشتافتند</p></div>
<div class="m2"><p>ورا زنده درکشتگان یافتند</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>ولی بود اندام او پاره پار</p></div>
<div class="m2"><p>چو به گشت زی نینوا رهسپار</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>همه عمر آنجا پرستنده بود</p></div>
<div class="m2"><p>ز ما باد بر جان پاکش درود</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>ایا سالک راه فقر و فنا</p></div>
<div class="m2"><p>که اندر دلت خفته گنج غنا</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>نهشته سر اندرکمند جهان</p></div>
<div class="m2"><p>نگشته دلت پای بند جهان</p></div></div>