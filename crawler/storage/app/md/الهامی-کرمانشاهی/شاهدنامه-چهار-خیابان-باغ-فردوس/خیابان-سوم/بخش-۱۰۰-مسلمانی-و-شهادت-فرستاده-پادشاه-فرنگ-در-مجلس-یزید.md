---
title: >-
    بخش ۱۰۰ - مسلمانی و شهادت فرستادهٔ پادشاه فرنگ در مجلس یزید
---
# بخش ۱۰۰ - مسلمانی و شهادت فرستادهٔ پادشاه فرنگ در مجلس یزید

<div class="b" id="bn1"><div class="m1"><p>بگفتا که ای کار فرمای شام</p></div>
<div class="m2"><p>بیندوختی بهر خود زشت نام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یکی داستان گویمت می شنو</p></div>
<div class="m2"><p>وزان پس به هر ره که خواهی برو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یکی ژرف دریا بود در فرنگ</p></div>
<div class="m2"><p>چو دریای چرخ برین، نیل رنگ</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زجوش و خروشش فلک درستوه</p></div>
<div class="m2"><p>همی موج خیزد ازو کوه کوه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بود در میانش یکی مرغزار</p></div>
<div class="m2"><p>پر از سرو کاج و تذرو و هزار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>درختان او سبز و خرم زمین</p></div>
<div class="m2"><p>هوا مشکبار و فضا عنبرین</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>درازیش صد فرسخ افزونتر است</p></div>
<div class="m2"><p>به هشتاد فرسنگ پهناور است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در آن مرغزار است شهری بزرگ</p></div>
<div class="m2"><p>به هر کویش از آب نهری سترگ</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>درآن شهر باشد برون از شمار</p></div>
<div class="m2"><p>زکافور و عنبر ز مشک تتار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>همه اهل این شهر هر کس که هست</p></div>
<div class="m2"><p>ز خرد و بزرگ اند عیسی پرست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کلیسای حافر درآنجا بود</p></div>
<div class="m2"><p>که چون قبله ی مرد ترسا بود</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>یکی حقه با گوهر هفت رنگ</p></div>
<div class="m2"><p>بپردخته استادهای فرنگ</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به محراب آن معبد آویخته</p></div>
<div class="m2"><p>همان حقه از زر برانگیخته</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>سمی از خر عیسی پاک جان</p></div>
<div class="m2"><p>بود اندر آن درج گوهر نهان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>برای زیارت ز راه دراز</p></div>
<div class="m2"><p>به هرسال قومی به عجز و نیاز</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بیایند و برخاک سایند سر</p></div>
<div class="m2"><p>به نزدیک آن درج و آن سم خر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بگیرند راه مناجات پیش</p></div>
<div class="m2"><p>ز یزدان بخواهند حاجات خویش</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>کسانی که هستند در بند دین</p></div>
<div class="m2"><p>چنینند در خدمت مرسلین</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>تو فرزند پیغمبر خویشتن</p></div>
<div class="m2"><p>کشی زار و پس در بر انجمن</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بیاری زنانی که در روزگار</p></div>
<div class="m2"><p>گزید از همه خلقشان کردگار</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>زهی کیش و ملت زهی داد و دین</p></div>
<div class="m2"><p>ترا باد خشم جهان آفرین</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ازین پس زکردار تو زشت کار</p></div>
<div class="m2"><p>بود داستان ها به هر روزگار</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>چو گفتار او را بد اختر شنید</p></div>
<div class="m2"><p>به جز کشتن مرد چاره ندید</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>به دژخیم گفتا که با تیغ تیز</p></div>
<div class="m2"><p>برون بر از اینجا و خونش بریز</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>و گرنه برد نام ما زیر ننگ</p></div>
<div class="m2"><p>از اینجا رود چون به شهر فرنگ</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>فرستاده را گرچه کشتن خطاست</p></div>
<div class="m2"><p>ولیکن ازو چون بد آید رواست</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>چو ترسا شنید این ببوسید خاک</p></div>
<div class="m2"><p>بگفتا سپاسم به یزدان پاک</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>که خوابی که دیدم همه راست شد</p></div>
<div class="m2"><p>دلم آنچه از بخت میخواست شد</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>شب دوش عیسای مینو سرشت</p></div>
<div class="m2"><p>مرا داده بد مژده سوی بهشت</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>کنونم ره مینو آمد پدید</p></div>
<div class="m2"><p>بگفت این و سوی سرشه دوید</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>مرآنرا از آن طشت زر و برگرفت</p></div>
<div class="m2"><p>به زاری بر او مویه اندر گرفت</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>ببوسید و گفت ای حبیب رسول</p></div>
<div class="m2"><p>ز من کن تو قربانی ام را قبول</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>به نزد پیمبر گواهم تو باش</p></div>
<div class="m2"><p>به روز قیامت پناهم تو باش</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>که من جان سپردم به آیین او</p></div>
<div class="m2"><p>گرفتم ره ملت و دین او</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>بگفت این و دژخیم ببرید سر</p></div>
<div class="m2"><p>از آن نو مسلمان فرخ گهر</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>زکینش اگر کشت شاه دمشق</p></div>
<div class="m2"><p>نمیرد که زنده است جانش به عشق</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>پس از وی بر آشفت بطریق روم</p></div>
<div class="m2"><p>که بودی نشسته درآن بزم شوم</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>بگفتا بدان نابکار پلید</p></div>
<div class="m2"><p>که چون تو ستمگستری کس ندید</p></div></div>