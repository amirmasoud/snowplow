---
title: >-
    بخش ۹۸ - گفتگوی ثمره ابن جندب با یزید در یاری سرامام شهید(ع)
---
# بخش ۹۸ - گفتگوی ثمره ابن جندب با یزید در یاری سرامام شهید(ع)

<div class="b" id="bn1"><div class="m1"><p>نگویم به آن چوب دستی چه کرد</p></div>
<div class="m2"><p>که قلب نبی زان شود پر ز درد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همی گفت دارم ازین سر عجب</p></div>
<div class="m2"><p>کزین خوب تر نیست دندان و لب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یکی مرد ز اصحاب خیرالانام</p></div>
<div class="m2"><p>که خود ثمره و جندبش بود نام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>درآنجای بد دید تا او چه کرد</p></div>
<div class="m2"><p>خروشید بروی که ای زشت مرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چه کارست این؟ شرمی ازکردگار</p></div>
<div class="m2"><p>که با چشم خود دیده ام چند بار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پیمبر بر این لب بسی بوسه داد</p></div>
<div class="m2"><p>به بدخواه او لب به نفرین گشاد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مکن جان احمد (ص) پر ز درد</p></div>
<div class="m2"><p>کسی با خداوند خود این نکرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>برآشفت و گفتا بد اختر بدوی</p></div>
<div class="m2"><p>که لب را ببند از چنین گفتگوی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نبودی ز یاران آن شاه اگر</p></div>
<div class="m2"><p>کنون از تنتدور می گشت سر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به پا خاست ثمره بگفت ای پلید</p></div>
<div class="m2"><p>کسی اینچنین تیره رایی ندید</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به من میکنی این چنین احترام</p></div>
<div class="m2"><p>که هستم ز یاران خیرالانام</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ولی خون بریزی ز فرزند او</p></div>
<div class="m2"><p>همه دوده و پاک پیوند او</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>سرش را بیاری به بزم شراب</p></div>
<div class="m2"><p>بیاری از این کرده ی ناصواب</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>زنان تو در پرده ها شادمان</p></div>
<div class="m2"><p>عیال نبی نزد نامحرمان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>مسلمانی این نیست ایمرد زشت</p></div>
<div class="m2"><p>بدین کیش خندند اهل کنشت</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ز گفتار آن پیر بی واهمه</p></div>
<div class="m2"><p>برآمد از آن انجمن همهمه</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ز غوغا بترسید برخود یزید</p></div>
<div class="m2"><p>بر آشفت بر پیر مرد سعید</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بسی ناسزا گفت و راندش زپیش</p></div>
<div class="m2"><p>برون رفت مرد از پی کار خویش</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>یهودی یکی مرد جالوت نام</p></div>
<div class="m2"><p>که بد پیشوای یهودان به شام</p></div></div>