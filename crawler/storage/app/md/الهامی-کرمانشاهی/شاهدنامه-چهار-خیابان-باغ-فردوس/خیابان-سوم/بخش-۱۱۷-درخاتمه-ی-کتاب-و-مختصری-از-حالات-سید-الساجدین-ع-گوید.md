---
title: >-
    بخش ۱۱۷ - درخاتمه ی کتاب و مختصری از حالات سید الساجدین(ع)گوید
---
# بخش ۱۱۷ - درخاتمه ی کتاب و مختصری از حالات سید الساجدین(ع)گوید

<div class="b" id="bn1"><div class="m1"><p>گرت برنشاند به گاه بلند</p></div>
<div class="m2"><p>هم اندازد آخر به چاه گزند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همی تا توانی به نیکی بکوش</p></div>
<div class="m2"><p>تو خاکی چو دریای آتش بجوش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اجل چونکه سال حیاتت شمرد</p></div>
<div class="m2"><p>چنان کن که مردم نگویند مرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ببخش و بخور کز پس مردنت</p></div>
<div class="m2"><p>ابا دیگری خورد خواهد زنت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ره عزلت و نیستی پیش گیر</p></div>
<div class="m2"><p>از آن پیش کت مرد باید، بمیر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نمرده تو در ماتم خود بموی</p></div>
<div class="m2"><p>به آب مژه تیرگی ها بشوی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جهان در بر اهل دل اندکی است</p></div>
<div class="m2"><p>چو می بگذرد رنج و راحت یکیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مخور غم ز مرگ کس اندر جهان</p></div>
<div class="m2"><p>که شد فوت تن زنده گانی جان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اگر غم خوری بهر آنشاه خور</p></div>
<div class="m2"><p>که بد خلق را سوی حق راهبر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چو او کشته شد دین و دانش بمرد</p></div>
<div class="m2"><p>چنان دان همه آفرینش بمرد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ببین تا که در سوک این شاه دین</p></div>
<div class="m2"><p>چه بد پیشه ی سیدالساجدین</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چهل سال جاکرد در گوشه ای</p></div>
<div class="m2"><p>نبودش جز اشک روان توشه ای</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>همه گریه میکرد در مرگ باب</p></div>
<div class="m2"><p>نخورد او به راحت دمی نان و آب</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>اگر نان گرم و اگر آب سرد</p></div>
<div class="m2"><p>بدیدی زدی ناله با داغ و درد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>که افسوس لب تشنه جان دادشاه</p></div>
<div class="m2"><p>نخورد آب در پهنه ی رزمگاه</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>درآن روز در بد بود کز تیغ کین</p></div>
<div class="m2"><p>بداد او سر و جان به جان آفرین</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>یکی روز در کوچه مردی دمان</p></div>
<div class="m2"><p>گذشتی که ناگاه از ناودان</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بسر آمدش آب و شد بیمناک</p></div>
<div class="m2"><p>که آن آب شاید نبوده است پاک</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>یکی گفتش ای مرد پرهیزکار</p></div>
<div class="m2"><p>از اینگونه پنداشت آزرم دار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>مر این آب از آبحیوان به اسب</p></div>
<div class="m2"><p>چنین آب ناید خضر را به دست</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>از این آب شو پیکر خویشتن</p></div>
<div class="m2"><p>ز آلوده گی ها بکن پاک تن</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بپرسید کاین آب چبود بگوی</p></div>
<div class="m2"><p>کز آن خویش باید درهم شتشوی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>به پاسخ بدو گفت گوینده این</p></div>
<div class="m2"><p>بود آب چشم خداوند دین</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>که در ماتم باب گریان بود</p></div>
<div class="m2"><p>ز آه دلش چرخ بریان بود</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چنان ریزد از دیده اشک روان</p></div>
<div class="m2"><p>که چون سیل شد آید از ناودان</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ز داغ پدر همچو ابر بهار</p></div>
<div class="m2"><p>همی گریه کردی به شام و نهار</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>که تا شد به خلد برین آنجناب</p></div>
<div class="m2"><p>ز دیدار جد و پدر کامیاب</p></div></div>