---
title: >-
    شمارهٔ ۹
---
# شمارهٔ ۹

<div class="b" id="bn1"><div class="m1"><p>در عشق اگر راهبری داشته باشی</p></div>
<div class="m2"><p>آن نخل بلندی که بری داشته باشی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مشکن دل کس بی گنهی گر تو بخواهی</p></div>
<div class="m2"><p>جا در دل صاحب نظری داشته باشی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نه چرخ برین را سپر از هم بشکافی</p></div>
<div class="m2"><p>ای تیر دعا گر اثری داشته باشی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خورشید حقیقت نگری در همه ذرات</p></div>
<div class="m2"><p>ای دل به خدا گر بصری داشته باشی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر ساحت افلاک پری از قفس تن</p></div>
<div class="m2"><p>از عشق اگر بال و پری داشته باشی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چشم از رخ تو بازنگریم که مبادا</p></div>
<div class="m2"><p>دزدیده نظر با دگری داشته باشی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خشکیده نگردد لبت از گرمی محشر</p></div>
<div class="m2"><p>الهامی اگر چشم تری داشته باشی</p></div></div>