---
title: >-
    شمارهٔ ۱
---
# شمارهٔ ۱

<div class="b" id="bn1"><div class="m1"><p>دلم از وحشت تنهایی شبها خون است</p></div>
<div class="m2"><p>غمم از حسرت آن چهره روز افزون است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با تو شادم اگرم جای به دوزخ بدهند</p></div>
<div class="m2"><p>بی تو در خلد برین خاطر من محزون است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل مجروح من از قطره ی خون بیش نبود</p></div>
<div class="m2"><p>پس چرا دامنم از خون جگر جیحون است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بعد ازینم سر و کاری نبود هیچ به عقل</p></div>
<div class="m2"><p>عاقل آن است که از عشق رخش مجنون است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مدتی رفت که از هجر تو بیمارم و تو</p></div>
<div class="m2"><p>می نپرسی ز من خسته که حالت چون است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تو پریشان مگر آن زلف مسلسل کردی</p></div>
<div class="m2"><p>که پریشانی آشفته دلان افزون است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به تماشای گلستان و گلم نیست نیاز</p></div>
<div class="m2"><p>که کنار و برم از لخت جگر گلگون است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به درآی از دو جهان و به فراغت بنشین</p></div>
<div class="m2"><p>عالم عشق ازین هر دو جهان بیرون است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نیست نسبت، قد موزون تو را هیچ به سرو</p></div>
<div class="m2"><p>سرو موزون بود اما نه چنان موزون است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دل الهامی اگر گشته پریشان نه شگفت</p></div>
<div class="m2"><p>همه دانند دل غمزده دیگرگون است</p></div></div>