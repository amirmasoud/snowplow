---
title: >-
    شمارهٔ ۷
---
# شمارهٔ ۷

<div class="b" id="bn1"><div class="m1"><p>با کاروان عشق تو چون همسفر شدم</p></div>
<div class="m2"><p>پای هوا شکستم و ره را به سر شدم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر کهربا ز جزع نشاندم عقیق ناب</p></div>
<div class="m2"><p>چو لعل ز عشق تو خونین جگر شدم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دادم چو دل به ابرو [و] چشم سیاه تو</p></div>
<div class="m2"><p>تیغ جفا و تیر بلا را سپر شدم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بستم نظر ز ملک دو عالم به مسکنت</p></div>
<div class="m2"><p>تا خاک راه مردم صاحب نظر شدم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در لوح سینه نقش غمت را نگاشتم</p></div>
<div class="m2"><p>از سوز عشق و راز محبت خبر شدم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از بس که تیر غم به دلم جا گرفته است</p></div>
<div class="m2"><p>سر تا قدم چو مرغ همه بال و پر شدم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>این قسمتم ز خوان محبت نصیب شد</p></div>
<div class="m2"><p>الهامی ار به رندی و مستی سمر شدم</p></div></div>