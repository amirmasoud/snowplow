---
title: >-
    شمارهٔ ۹
---
# شمارهٔ ۹

<div class="b" id="bn1"><div class="m1"><p>تا دلم شد به خم زلف رسای تو اسیر</p></div>
<div class="m2"><p>بازوی عقل مرا کرد جنون در زنجیر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کرده آهوی دلم را به خطای تن من</p></div>
<div class="m2"><p>در نیستان مژه شیر نگاهت نخجیر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از پی صید من از طرّه و ابروی و مژه</p></div>
<div class="m2"><p>گه کمند آوری و گاه کمان گاهی تیر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>صنما! سنگدلا! سر و قد! سیمبرا!</p></div>
<div class="m2"><p>نیست بازیچه چنین عشق مرا سهل مگیر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>من خود این فال زدم تا که بدیدم رخ تو</p></div>
<div class="m2"><p>کاخرت سلطنت حسن شود عالمگیر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر ز ابروی و مژه تیر و کمان برگیری</p></div>
<div class="m2"><p>به یکی لحظه کنی ملک جهان را تسخیر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>با غم عشق تو در عین جوانی پیرم</p></div>
<div class="m2"><p>همچو من کیست که در عین جوانی شده پیر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>حالیا تا که تو را دست دهد با من مست</p></div>
<div class="m2"><p>باده پیمای و قدح نوش و ز می ساغر گیر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>که به گوش دل من گفت سروشی امروز</p></div>
<div class="m2"><p>روز جشن اسدالله بود و عید غدیر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>اندرین روز به فرموده ی یزدان، احمد</p></div>
<div class="m2"><p>کرد سلطان نجف را به همه خلق امیر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کیست سلطان نجف شیر خداوند علی</p></div>
<div class="m2"><p>که نبی راست طرازنده ی دیهیم و سریر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>آنکه چون ایزد دادار علیم است و حکیم</p></div>
<div class="m2"><p>آنکه چون احمد مختار بشیر است و نذیر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>جلوه ی شاهد غیب آنکه بود بی شک و ریب</p></div>
<div class="m2"><p>از نهان راز دل خلق جهان جمله خبیر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>آن امیری که نبودش ز پس پیغمبر</p></div>
<div class="m2"><p>همچو ذات احدیت نه عدیل و نه نظیر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>جزیی از دفتر وصفش نتوانند نگاشت</p></div>
<div class="m2"><p>گر فلک صفحه شود اهل سماوات دبیر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>جامه ی خالقی و کسوت مخلوقیّت</p></div>
<div class="m2"><p>آن به بالاش طویل این به قدش بود قصیر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چو زر بیغش خورشید شود پاک عیار</p></div>
<div class="m2"><p>مس قلبی که ببیند ز ولایش اکسیر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>توتیا گر کشد از خاک در شاه به چشم</p></div>
<div class="m2"><p>مور در چاه ببیند به شب تیره، ضریر</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>احمد ار شمس وجود است علی چون قمر است</p></div>
<div class="m2"><p>در سپهر شرف این هر دو ندارد نظیر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>این کلف بر رخ ماه فلکی دانی چیست؟</p></div>
<div class="m2"><p>لطمه زد غیرت رایش به رخ بدر منیر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>عذرخواه همه ی اهل گنه گر شود او</p></div>
<div class="m2"><p>چه کند بار خدا گر نشود عذر پذیر</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ای صفات احدیت شده از دست تو فاش</p></div>
<div class="m2"><p>که حلیمیّ و کریمیّ و سمیعیّ و بصیر</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>دست تدبیر تو چون تیغ برآرد نه عجب</p></div>
<div class="m2"><p>سپر اندازد اگر در بر تیغش تقدیر</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>کفر با مهر تو زان دین که نه با مهر تو بِه</p></div>
<div class="m2"><p>بلکه جز مهر تو دینی نکند کس تصویر</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>دادخواه آمده ام بر درت از جور سپهر</p></div>
<div class="m2"><p>ای غنی همچو خداوند بده داد فقیر</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>عرض حاجت نتوان کرد به درگاهت از آنک</p></div>
<div class="m2"><p>همه ی اهل جهان را تویی آگه ز ضمیر</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>دست دست تو بود ز آنکه تویی دست خدای</p></div>
<div class="m2"><p>زنده فرمای و بمیران و ببخشای و بگیر</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>لذت مهر توام کی رود از دل که مرا</p></div>
<div class="m2"><p>با دل آمیخته مهر تو چو شکّر با شیر</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>زان به ملک سخنم گشت لقب الهامی</p></div>
<div class="m2"><p>که مرا مدح تو الهام شد از حی قدیر</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>تا رخ باغ شود همچو طبرخون به بهار</p></div>
<div class="m2"><p>تا شود چهره ی گلزار به دی مه چو زریر</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>یار و بدخواه تو را چون سحر و شام بود</p></div>
<div class="m2"><p>دو رخ آموده به کافور و براندوده به قیر</p></div></div>