---
title: >-
    شمارهٔ ۱۲ -  فی تهنیة مولود السلطان خلّد الله ملکهُ
---
# شمارهٔ ۱۲ -  فی تهنیة مولود السلطان خلّد الله ملکهُ

<div class="b" id="bn1"><div class="m1"><p>عید مولود شهنشاه ملایک پاسبان</p></div>
<div class="m2"><p>باد مسعود و مبارک بر خدیو کامران</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شرزه شیر بیشهٔ مردی حسام السلطنه</p></div>
<div class="m2"><p>آنکه تیغش سرفشان است و سنانش جان ستان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شهریار راستین و میر دریا آستین</p></div>
<div class="m2"><p>قهرمان ماء و طین و داور گیتی ستان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بازو مردان ایران تیغ دست پادشه</p></div>
<div class="m2"><p>والی ملک خراسان برق کشت ترکمان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کی برد جان از دم شمشیر او بدخواه او</p></div>
<div class="m2"><p>فی المثل گر در زمین پنهان شود یا آسمان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>غارت یک دشت ضیغم با خدنگ راست رَو</p></div>
<div class="m2"><p>آفت یک پهنه لشکر با حسام خون فشان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای که در رادی نزاده چون تو مام روزگار</p></div>
<div class="m2"><p>ای که در مردی ندیده چون تو چشم آن جهان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ای خدیو دادگر کاندر زمان عدل تو</p></div>
<div class="m2"><p>گلّه را سازد حراست گرگ بهتر از شبان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>[میش از داد تو خسبد بر سرین نر پلنگ</p></div>
<div class="m2"><p>ماده آهو پا نهد بر دیده ی شیر ژیان]</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چشم کش نادیده از تو ظلم غیر از کان و یم</p></div>
<div class="m2"><p>گوش کس نشنیده از تو زور جز چاچی کمان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>[در هوای امن تو تیهو پرد با جرّه باز</p></div>
<div class="m2"><p>صعوه اندر چنگل شاهین نماید آشیان]</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مدح تو غیب است و وهم من ازو آگاه نیست</p></div>
<div class="m2"><p>کس ز غیب آگه نباشد جز خدای غیب دان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>وهم من گوید تو آنی کز کمال منزلت</p></div>
<div class="m2"><p>پایهٔ قدر تو جا دارد به فرق فرقدان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>وین فروتر پایهٔ جاه تو باشد ای امیر</p></div>
<div class="m2"><p>وهم کوته بین من را باد خاک اندر دهان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>آن زمان یارد شناسد پایه ی تو وهم من</p></div>
<div class="m2"><p>گر کسی یارد رود زی آسمان با ریسمان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>هم از آن بِه در دعا کوشم چه نارم مدح کرد</p></div>
<div class="m2"><p>ای امیر کامران و ای خدیو مستعان</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>تا بپاید آسمان و تا بماند روزگار</p></div>
<div class="m2"><p>با رخ فرّخ بپای و با دل خرّم بمان</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>هم به درگاه تو الهامی بماند شادخوار</p></div>
<div class="m2"><p>تا ثناگوی تو باشد از دل و جان جاودان</p></div></div>