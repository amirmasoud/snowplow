---
title: >-
    شمارهٔ ۵ -  در تهنیت عید غدیر گوید
---
# شمارهٔ ۵ -  در تهنیت عید غدیر گوید

<div class="b" id="bn1"><div class="m1"><p>ساقیا می ده که می جان است گویی نیست هست</p></div>
<div class="m2"><p>جان چه باشد باده ایمان است گویی نیست هست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر نداری صاف، دردم می کشد دُردم بده</p></div>
<div class="m2"><p>درد ما را دُرد درمان است گویی نیست هست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ساقیا عید غدیر است و به منبر مصطفی</p></div>
<div class="m2"><p>شیر یزدان را ثناخوان است گویی نیست هست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ساقیا عید غدیر است و به منبر مصطفی</p></div>
<div class="m2"><p>شیر یزدان را ثنا خوان است گویی نیست هست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>وارث محراب و منبر از پس خیرالبشر</p></div>
<div class="m2"><p>خسرو دین شاه مردان است گویی نیست هست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تخت احمد را به فرمان خداوند احد</p></div>
<div class="m2"><p>حیدر کرّار سلطان است گویی نیست هست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در چنین عهد همایون فال جشن انبساط</p></div>
<div class="m2"><p>در تمام ملک امکان است گویی نیست هست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>باده ی وحدت بگیر از ساقی خمّ غدیر</p></div>
<div class="m2"><p>کان شرابت روح ایمان است گویی نیست هست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بی شراب و شاهد ار باشی درین عید سعید</p></div>
<div class="m2"><p>در حقیقت عین عصیان است گویی نیست هست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کشور توحید را از فتنه ی کفر و نفاق</p></div>
<div class="m2"><p>شوهر زهرا نگهبان است گویی نیست هست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ساقیا دردِه شراب نابم از خمّ غدیر</p></div>
<div class="m2"><p>کان ولای شیر یزدان است گویی نیست هست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ساقی کوثر علی مرتضی کز منزلت</p></div>
<div class="m2"><p>عین دادار دو کیهان است گویی نیست هست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>او یدالله است و دست اوست فوق دستها</p></div>
<div class="m2"><p>شاهد او نصّ قرآن است گویی نیست هست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ذات او آیینه ای باشد بر حق کاندرو</p></div>
<div class="m2"><p>آشکارا حیّ سبحان است گویی نیست هست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>آن ولی حق وصیّ مطلق پیغمبر است</p></div>
<div class="m2"><p>آیه ی تبلیغ است گویی نیست هست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>آنکه اندر بیشه ی توحید شیر داور است</p></div>
<div class="m2"><p>خوابگاهش عرش رحمان است گویی نیست هست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>نوح کشتیبان بود هر کس که با او یار شد</p></div>
<div class="m2"><p>فارغ از آسیب طوفان است گویی نیست هست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>قدسیان را در سجود آدم او مسجود بود</p></div>
<div class="m2"><p>منکر این قول شیطان است گویی نیست هست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>دین حق را تیغ جانسوز امیرالمؤمنین</p></div>
<div class="m2"><p>پاسبان از کفر و طغیان است گویی نیست هست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>از برای نصرت دین نایب تیغ علی</p></div>
<div class="m2"><p>پور شاهنشاه ایران است گویی نیست هست</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>دادگستر شاه مسعود آنکه در دوران او</p></div>
<div class="m2"><p>گرگ اندر گله چوپان است گویی نیست هست</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>در سپهر حشمت و تمکین چو نیکو بنگری</p></div>
<div class="m2"><p>مهر ظل ظل السلطان است گویی نیست هست</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>رمح او در نفی قبطی سیرتان دین حق</p></div>
<div class="m2"><p>راستی مانند ثعبان است گویی نیست هست</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>لطمه زد خشمش به چهر آسمان هفتمین</p></div>
<div class="m2"><p>زان شبه گون روی کیوان است گویی نیست هست</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>گر بخواهد چون خور آسان گیرد او ملک جهان</p></div>
<div class="m2"><p>یار او شاه خراسان است گویی نیست هست</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>قرة العینش جلال الدوله نور انجم است</p></div>
<div class="m2"><p>کاختر ملک سلیمان است گویی نیست هست</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>چاکران در گه او مهتران کشورند</p></div>
<div class="m2"><p>میر آنها میرتومان است گویی نیست هست</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>آنکه سرهای عدو روز وغا او راست گو</p></div>
<div class="m2"><p>تیغ او خم گشته چوگان است گویی نیست هست</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>رای او خورشید تابان است در برج اسد</p></div>
<div class="m2"><p>دست او خود ابر نیسان است گویی نیست هست</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>سجده را در پیشگاه شاه مسعود این امیر</p></div>
<div class="m2"><p>همچنان خم گشته مژگان است گویی نیست هست</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>گوید او را گر یمین الدوله در آتش بچم</p></div>
<div class="m2"><p>چون سمندر عبد فرمان است گویی نیست هست</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>تیغ او باشد حصاری آهنین بر گرد ملک</p></div>
<div class="m2"><p>فتنه در آن سوی بنیان است گویی نیست هست</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>هست اندر مغز چرخ از هیبتش رنج دوار</p></div>
<div class="m2"><p>زین سبب پیوسته گردان است گویی نیست هست</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>شاخ آهو نیزه را ماند به دور عدل او</p></div>
<div class="m2"><p>کافت ضرغان غژمان است گویی نیست هست</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>تا به بستان باد ننگیزد غبار از عدل او</p></div>
<div class="m2"><p>خار جاروب گلستان است گویی نیست هست</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>گو نتابد آفتاب اندر زمان و آسمان</p></div>
<div class="m2"><p>چهر او خورشید رخشان است گویی نیست هست</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>آن درخت بارور باشد که در باغ کرم</p></div>
<div class="m2"><p>بار و برگ او ز احسان است گویی نیست هست</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>گر حسام الملک نبود مظهر الطاف حق</p></div>
<div class="m2"><p>چون بری از عیب و نقصان است گویی نیست هست</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>ای حسام خسرو غازی که از ایمای تو</p></div>
<div class="m2"><p>دشمنت ببریده شریان است گویی نیست هست</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>گو بتازد لشکر افراسیاب انقلاب</p></div>
<div class="m2"><p>رستم عزمت به میدان است گویی نیست هست</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>احمد لطف تو یار من بود کاندر سخن</p></div>
<div class="m2"><p>بنده ی من روح حسان است گویی نیست هست</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>باغ فردوس مرا در پر نویسد جبرئیل</p></div>
<div class="m2"><p>کان امین وحی سبحان است گویی نیست هست</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>چون به الهامی کند روح القدس وحی سخن</p></div>
<div class="m2"><p>طبع او مرغ سخندان است گویی نیست هست</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>تا جهان باشد تو را در مسند عزّ و شرف</p></div>
<div class="m2"><p>یار الطاف جهانبان است گویی نیست هست</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>تا بود گیتی تو اندر او دل خرم بمان</p></div>
<div class="m2"><p>عمر گیتی بس فراوان است گویی نیست هست</p></div></div>