---
title: >-
    شمارهٔ ۳
---
# شمارهٔ ۳

<div class="b" id="bn1"><div class="m1"><p>مژده که میلاد شاه عرش مکان است</p></div>
<div class="m2"><p>عید پیمبر شه زمین و زمان است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>باعث ایجاد کاینات محمد</p></div>
<div class="m2"><p>کز رخ او جلوه ی خدای عیان است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نور نخستین و خلق اول احمد</p></div>
<div class="m2"><p>کز همه پیش است و اخر همگان است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آدم اول رسول خاتم امی</p></div>
<div class="m2"><p>کز گهر آدم است و بهتر از آن است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بنده ی ایزد نما و علت اشیا</p></div>
<div class="m2"><p>داور آفاق و مالک دو جهان است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پیکرش از نور بود و عنصرش از جان</p></div>
<div class="m2"><p>سایه نبودش از آنکه جسمش جان است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بی مثل آمد چو ذات ایزد یکتا</p></div>
<div class="m2"><p>هرچه در آید به فکر برتر از آن است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>غیر علی کس نبود یار و معینش</p></div>
<div class="m2"><p>ناصر دینش خدیو ملک ستان است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ناصر دین شاه تاجور که به خفتانش</p></div>
<div class="m2"><p>خفته تو گویی هزار شیر ژیان است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آنکه ز رشک بنان و کف کریمش</p></div>
<div class="m2"><p>خون به دل معدن است و در رگ کان است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بر سر مطبخ سرای شاه جهانبان</p></div>
<div class="m2"><p>گنبد پیروزه فام همچو دخان است</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>شاه بود تاج بخش و پورش مسعود</p></div>
<div class="m2"><p>ملک ستان و خدیو شاه نشان است</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>سایه ی سلطان یمین دولت کو را</p></div>
<div class="m2"><p>فتح چو زه بسته بر دو سوی کمان است</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>آنگه حسامش به رزم قابض روح است</p></div>
<div class="m2"><p>و آنکه عطایش به بزم معطی جان است</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نیمه ی ایران ز عدل کارگزارانش</p></div>
<div class="m2"><p>خرّم و آباد همچو باغ جنان است</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ویژه ز دوده ی حسان ملک شهنشاه</p></div>
<div class="m2"><p>آنکه زمانش زمان عدل و امان است</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>میر معظم که از لطافت عنصر</p></div>
<div class="m2"><p>رای وی آگه ز رازهای نهان است</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گو مخور ای بینوا دگر غم روزی</p></div>
<div class="m2"><p>جود امیر بزرگوار ضمان است</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ای که ز تأثیر اسم اعظم عدلت</p></div>
<div class="m2"><p>گرگ پی پاش گله بِه ز شبان است</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>مهر تو احباب را شکفته بهار است</p></div>
<div class="m2"><p>قهر تو بدخواه را چو باد خزان است</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>رمح تو نیش قضا بود که ز بیمش</p></div>
<div class="m2"><p>خون به رگ روزگار در هیجان است</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بود مداین چه سان به دوره ی کسری</p></div>
<div class="m2"><p>کشور ما از عدالت تو چنان است</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>تیغ به دریای دست راد تو ای میر</p></div>
<div class="m2"><p>راست تو گویی یکی نهنگ دمان است</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>روز نبرد از نهیب لشکر عزمت</p></div>
<div class="m2"><p>کز دو طر پهنه پر ز برق سنان است</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>رایت دشمن چو مرغ سوی نشیمن</p></div>
<div class="m2"><p>رو به هزیمت نهاده در طیران است</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>اکه به سرپنجه ی تو قبضه ی تیغ است</p></div>
<div class="m2"><p>ملک مصون ز انقلاب و از حدثان است</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>راست روی در زمان عدل تو ای میر</p></div>
<div class="m2"><p>بر همه تن واجب است گر سرطان است</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>سم ننهاده است گر سمند تو بر چرخ</p></div>
<div class="m2"><p>بر رخ او از هلال این چه نشان است</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>اسب تو باد بزان بود که به پویه</p></div>
<div class="m2"><p>بر زبر کوه همچو دشت دوان است</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>نی نی باد است خانه زاد سمندت</p></div>
<div class="m2"><p>زان به جنوب و شمال در جولان است</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>ر تو به هیبت به کوه خاره ببینی</p></div>
<div class="m2"><p>کوه مخوانش دگر که آب روان است</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>در دل الهامی از عنایت یزدان</p></div>
<div class="m2"><p>وحی سماوی و روج قدس نهان است</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>لیک فتاده به قعر چاه مذلت</p></div>
<div class="m2"><p>یوسف جانش ز کینه ی اخوان است</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>خواستم از این دیار رخت ببندم</p></div>
<div class="m2"><p>سوی دیاری که خسرو ملکان است</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>سود برند اهل ذوق از سخن من</p></div>
<div class="m2"><p>قافیه گر شایگان شود چه زیان است</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>تا که به هر سال عید احمد مختار</p></div>
<div class="m2"><p>سنت اسلام و جشن پیر و جوان است</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>شاه بماناد و پور شاه بماناد</p></div>
<div class="m2"><p>میر بماناد و هرکه خادم آن است</p></div></div>