---
title: >-
    شمارهٔ ۱۱ -  وله فی المدح
---
# شمارهٔ ۱۱ -  وله فی المدح

<div class="b" id="bn1"><div class="m1"><p>هلال غره ی شوّال را زدوده حسام</p></div>
<div class="m2"><p>پدید گشت چو تیغ خدایگان ز نیام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چه روی داده ندانم هلال را کاین سان</p></div>
<div class="m2"><p>خمیده پشت بر آمد ز چرخ مینافام؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خود ار به روزه گشودن جواز داد چراست</p></div>
<div class="m2"><p>چون روزه داران لاغر تن و ضعیف اندام؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به شکل سیمین جامی پدید گشت و درین</p></div>
<div class="m2"><p>کنایتی است که از می کشید باید جام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کنون به گوشه ی ابرو هلال رندان را</p></div>
<div class="m2"><p>خبر دهد که سر آمد زمان ماه صیام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حرام کرد می، ار زانکه ماه روزه به ما</p></div>
<div class="m2"><p>کنون مباح شد آن می که پیش بود حرام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مقیم مسجد بودم مهی اگر چه کنون</p></div>
<div class="m2"><p>کنم به میکده سالی اگر دهند مُقام</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زمان بزم خواص آمد و فراغت و حال</p></div>
<div class="m2"><p>هزار شکر که رستم ز ازدحام عوام</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کنون رکوع صراحی به بزم مستان بین</p></div>
<div class="m2"><p>به ماه پیش دیدی بسی رکوع امام</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بنوش باده به فتوای من به ناله ی چنگ</p></div>
<div class="m2"><p>بویژه از کف مه طلعتی لطیف اندام</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کسی که کشتهٔ تیر نگاه یار نگشت</p></div>
<div class="m2"><p>عجب مراست که چون می نهد به محشر گام؟</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بنوش جامی و جامی مرا بده که کشم</p></div>
<div class="m2"><p>به طاق ابروی عمّ شهنشه اسلام</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>حسام سلطنت آن نامور امیر که هست</p></div>
<div class="m2"><p>درنده ناخن تیغش چو پنجهٔ ضرغام</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>برد ضیا ز فروغ ضمیر او خورشید</p></div>
<div class="m2"><p>کند حذر ز نهیب حسام او بهرام</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>زمانه بودی چون بُختی گسسته مهار</p></div>
<div class="m2"><p>به دست او نسپردی گرش خدای زمام</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>نه باد راست به آیین عزم او جنبش</p></div>
<div class="m2"><p>نه کوه راست به کردار حزم او آرام</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>خدایگانا در نامه ی ملوک جهان</p></div>
<div class="m2"><p>ترا نخست به مردی نگاشت باید نام</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>دل عدو شکرد هیبت تو چون زوبین</p></div>
<div class="m2"><p>صف سپه شکند صولت تو چون صمصام</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ملک به است ز کیخسرو [و] ز افریدون</p></div>
<div class="m2"><p>تو در نبرد دلاورتری ز رستم و سام</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>شهان جهان بگرفتند اگر به تیغ و سپاه</p></div>
<div class="m2"><p>تو شهر و باره گشایی همی به پیک و پیام</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>هر آنکه گردش چشمی نظر ز لطف تو دید</p></div>
<div class="m2"><p>دگر نگردد گرد دلش غم ایام</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چو پسته هر که نخندد به عهد تو ز خوشی</p></div>
<div class="m2"><p>زمانه اش به در آرد [ز] پوست چون بادام</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>اگرچه چرخ بپرورد شیرمرد بسی</p></div>
<div class="m2"><p>زمانه را چو تو شیری برون نشد ز کنام</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>که جز تو پیکر گردان درید با دم تیغ؟</p></div>
<div class="m2"><p>که جز تو گردن مردان کشید در خم خام؟</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>هلال نیست که گردد پدید هر سر ماه</p></div>
<div class="m2"><p>ز رشک تیغ تو کاهد به چرخ بدر تمام</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>همی بگردد تا گرد خاکدان افلاک</p></div>
<div class="m2"><p>همی بتابد تا در دل فلک اجرام</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ز فیض رحمت پروردگار و رأفت شاه</p></div>
<div class="m2"><p>تو را سعادت جاوید باد و عزّ مدام</p></div></div>