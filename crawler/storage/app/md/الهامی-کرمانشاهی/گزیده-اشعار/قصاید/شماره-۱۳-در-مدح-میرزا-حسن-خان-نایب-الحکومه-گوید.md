---
title: >-
    شمارهٔ ۱۳ -  در مدح میرزا حسن خان نایب الحکومه گوید
---
# شمارهٔ ۱۳ -  در مدح میرزا حسن خان نایب الحکومه گوید

<div class="b" id="bn1"><div class="m1"><p>عید قربان آمد ای دلبر شوم قربان تو</p></div>
<div class="m2"><p>ساغری می ده که جان سازم فدای جان تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نیست کاری جان فدا کردن به راه دوستان</p></div>
<div class="m2"><p>جان چه باشد آنچه دارم آن شود قربان تو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من کدامم کیستم از خود چه دارم چیستم؟</p></div>
<div class="m2"><p>از تو دارم هرچه دارم جمله باشد آن تو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در منای عشق بوسم دست و بازوی قضا</p></div>
<div class="m2"><p>گر نهد روزی به حلقم خنجر برّان تو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>روز محشر می نمایم بر شهیدان فخرها</p></div>
<div class="m2"><p>گر به خون خویش رنگین بنگرم دامان تو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دانه و دام دلم شد در بیابان الست</p></div>
<div class="m2"><p>خال هندوی تو و گیسوی مشک افشان تو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>همچو مرغ بسمل اندر خون خود پر می زند</p></div>
<div class="m2"><p>در درون سینه دل از حسرت مژگان تو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در بهشت ار نیست ثعبان از چه رو باشد همی</p></div>
<div class="m2"><p>در بهشت روی تو آن زلف چون ثعبان تو</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>لؤلؤ و مرجان ز درج دیده می ریزم همی</p></div>
<div class="m2"><p>روز و شب از اشتیاق لؤلؤ و مرجان تو</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هیچ نندیشد ز خشم خواجه ی آزادگان</p></div>
<div class="m2"><p>من عجب دارم ز کار نرگش فتّان تو</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>صدر نیک اختر حسن خان آنکه گردون گویدش</p></div>
<div class="m2"><p>حلقه ام هستم من اندر باب عزّ و شأن تو</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>منشی گردون بدو گوید که من هستم همی</p></div>
<div class="m2"><p>خوشه چین خرمن افکار مدحت خوان تو</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گویدش مه می کنم من در سپهر منزلت</p></div>
<div class="m2"><p>کسب نور از آفتاب خاطر تابان تو</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>سعد اکبر گویدش من آن مبارک بنده ام</p></div>
<div class="m2"><p>که بروبم با مژه گرد از بر ایوان تو</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گویدش ناف غزال چین که من خون می خورم</p></div>
<div class="m2"><p>کز چه عنبر بار باشد کلک تنک افشان تو؟</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ای سپهر عزت و رفعت که جا دارد همی</p></div>
<div class="m2"><p>گر بگویم هست کیوان چاکر دربان تو</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>روح قاآن می برد اندر بیابان عدم</p></div>
<div class="m2"><p>سجده از خجلت به پیش جود بی پایان تو</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>نفکنی هرگز به عارض چین و بر ابرو گره</p></div>
<div class="m2"><p>گر شود خلق جهانی فی المثل مهمان تو</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>صاحب خلدی و نیران گر بگویی نیستم</p></div>
<div class="m2"><p>مهر تو خلد تو باشد قهر تو نیران تو</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>تا تو کردی جای اندر مسند فرماندهی</p></div>
<div class="m2"><p>هِشت هر گردنکشی سر بر خط فرمان تو</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>هست کرمانشاه اکنون باغ داد کسروی</p></div>
<div class="m2"><p>کاندرو روید گیاه عدل در دوران تو</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>تو علی و آل او را پیروی در دین حق</p></div>
<div class="m2"><p>شافع جرم تو در محشر بود ایمان تو</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>کی تو را پروا بود از سوزش نار جحیم</p></div>
<div class="m2"><p>چون نخوابد تا سحرگه دیده ی گریان تو</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>حلّه ی رحمت ز مهر مرتضی پوشیده ای</p></div>
<div class="m2"><p>تا نبیند کس به محشر پیکر عریان تو</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>هر که زد کوس هنر در پهنه ی دانشوری</p></div>
<div class="m2"><p>تیغ بشکست و سپر انداخت در میدان تو</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>تو نه مرد باده و جامی تو را کافی بود</p></div>
<div class="m2"><p>در تنور دل کباب سینهٔ بریان تو</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>تا دَمَد هر صبحگه رخشنده شید از خاوران</p></div>
<div class="m2"><p>پرتو افکن باد مهر عارض رخشان تو</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>فخر کن الهامی از رفعت به مرد شیروان</p></div>
<div class="m2"><p>زان که مدح صدر باشد زینت دیوان تو</p></div></div>