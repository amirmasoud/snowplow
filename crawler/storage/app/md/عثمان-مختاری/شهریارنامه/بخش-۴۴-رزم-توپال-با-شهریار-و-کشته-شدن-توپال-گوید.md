---
title: >-
    بخش ۴۴ - رزم توپال با شهریار و کشته شدن توپال گوید
---
# بخش ۴۴ - رزم توپال با شهریار و کشته شدن توپال گوید

<div class="b" id="bn1"><div class="m1"><p>چو آمد برآویخت توپال شیر</p></div>
<div class="m2"><p>به پیش صف شاه ارژنگ چیر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو ارژنگ آندید آمد بکین</p></div>
<div class="m2"><p>بجنبید گفتی ز لشکر زمین</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گرفتند توپال را در زمان</p></div>
<div class="m2"><p>بیامد هیاهوی کند آوران</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ستمکاره توپال شیر نر</p></div>
<div class="m2"><p>درافتاد در لشکر بی شمر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جهان کرد بر شاه ارژنگ تنگ</p></div>
<div class="m2"><p>ز خون شد زمین هر طرف لاله رنگ</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سپهبد به نیروی پروردگار</p></div>
<div class="m2"><p>برون آمد از لشکر بی شمار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بیاورد شنگاوه در پیش شاه</p></div>
<div class="m2"><p>کشیدند از رزم دست آن سپاه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دو لشکر دگر باره بر جای شد</p></div>
<div class="m2"><p>همی برفلک ناله نای شد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ستم کاره کوپال در دشت جنگ</p></div>
<div class="m2"><p>به ایستاده و گرز کینش به چنگ</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سپهدار برداشت پیچان کمند</p></div>
<div class="m2"><p>برآورد کردند سرکش سمند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>برآویخت با گرد توپال شیر</p></div>
<div class="m2"><p>میان دو لشکر چو شیر دلیر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بدانست توپال کاندر نبرد</p></div>
<div class="m2"><p>نتابد ابا آن یل شیره مرد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بتابید سر پیل را در زمان</p></div>
<div class="m2"><p>که آمد سپهبد چو شیر ژیان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>درانداخت در گردنش خم خام</p></div>
<div class="m2"><p>سرگرد توپال آمد بدام</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>کشیدش ز بالا بزیر آن دلیر</p></div>
<div class="m2"><p>فرو رفت و برداشت خنجر چو شیر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>سرش را بزد دست و از تن برید</p></div>
<div class="m2"><p>ز میدان کین پیش شاه آورید</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>برآورد دم ناله کوه نای</p></div>
<div class="m2"><p>فرو رفت ارژنگ از باد پای</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>سر بی بها برگرفت آن زمان</p></div>
<div class="m2"><p>بخون پدر خورد خونش روان</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ببوسید روی و بر شهریار</p></div>
<div class="m2"><p>بسر کرد ارژنگ شه زر نثار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چو هیتال آن دید بگریست خون</p></div>
<div class="m2"><p>که شد بختم اکنون و گشتم نگون</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>برادر بشد کشته خویش من</p></div>
<div class="m2"><p>گرامی سه فرزند من پیش من</p></div></div>