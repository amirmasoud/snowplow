---
title: >-
    بخش ۸۶ - آمدن ارهنگ به میدان خورشید مینو گوید
---
# بخش ۸۶ - آمدن ارهنگ به میدان خورشید مینو گوید

<div class="b" id="bn1"><div class="m1"><p>برانگیخت از جای سرکش سمند</p></div>
<div class="m2"><p>ستمکاره ارهنگ پولادوند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سر ره بخورشید مینو گرفت</p></div>
<div class="m2"><p>ازو ماند خورشید مینو شگفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یکی دیو واژونه دید او بلند</p></div>
<div class="m2"><p>بدستش کمان و برش (بر) کمند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز خورشید مینو بپرسید نام</p></div>
<div class="m2"><p>نخست آن ستمکاره دیو فام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مرا نام خورشید مینوی گفت</p></div>
<div class="m2"><p>که پیکان کلکم دل سنگ سفت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یکی از غلامان زال زرم</p></div>
<div class="m2"><p>که در رزم جوشان چو شیر نرم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هنر از تهمتن بیاموختم</p></div>
<div class="m2"><p>هم از زال رز تیر اندوختم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مرا زال فرزند گوید همی</p></div>
<div class="m2"><p>ز من رزم شیران بجوید همی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تهی دیدی از شیر این بیشه را</p></div>
<div class="m2"><p>که بر رزم ما بستی اندیشه را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گر از بیشه بیرون شد از شیر نر</p></div>
<div class="m2"><p>پلنگ ژیان هست در رهگذر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به بیشه درون بچه شیر هست</p></div>
<div class="m2"><p>همان نیزه و گرز شمشیر هست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بگفت این و برداشت چاچی کمان</p></div>
<div class="m2"><p>خروشان چو در بیشه شیر ژیان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بهر گوشه بر تیر باران گرفت</p></div>
<div class="m2"><p>چو شیران کمین سواران گرفت</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>به جوشن درون دیو واژونه بود</p></div>
<div class="m2"><p>نیامد از ارهنگ پیکانش زود</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چو بر کبر او تیر یاری نکرد</p></div>
<div class="m2"><p>به تیر و کمان استواری نکرد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>کمان را به قربان نهان کرد و تیغ</p></div>
<div class="m2"><p>برافروخت از برق چون تیره میغ</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بدو اندر آمد ز روی ستیز</p></div>
<div class="m2"><p>بدست اندرون تیغ چون برق تیز</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چو ارهنگ دید او برآورد تیغ</p></div>
<div class="m2"><p>تو گوئی که بد برق در دست میغ</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بهم بربکین تیغ تیز آختند</p></div>
<div class="m2"><p>نبردی چو شیر ژیان ساختند</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ز یکسوی خورشید و یکسوی دیو</p></div>
<div class="m2"><p>به کیوان رسانده ز میدان غریو</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>تو گفتی که خورشید و آن دیو نر</p></div>
<div class="m2"><p>زحل بود کرده قران با قمر</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>به تنگ اندر آمدش دیودمند</p></div>
<div class="m2"><p>ستمکاره ارهنگ پولادوند</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>یکی تیغ زد بر سرنامدار</p></div>
<div class="m2"><p>چنان چونکه خورشید بر کوهسار</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>سر و ترک خورشید مینو برید</p></div>
<div class="m2"><p>ز نارنج شخرف بر گل دمید</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ز مریخ خورشید چون زخم یافت</p></div>
<div class="m2"><p>عنان را بپیچید بیرون شتافت</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>به لشکر که آمد دمان و ستوه</p></div>
<div class="m2"><p>بجوشید چون بحر زابل گروه</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>چه سام آن چنان دید برخواست اسب</p></div>
<div class="m2"><p>درآمد به میدان چه آذرگشسب</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>سر راه برکرد ارهنگ بست</p></div>
<div class="m2"><p>گران گرزه گاو پیکر بدست</p></div></div>