---
title: >-
    بخش ۴۰ - مجلس آراستن ارژنگ شاه با شهریار گوید
---
# بخش ۴۰ - مجلس آراستن ارژنگ شاه با شهریار گوید

<div class="b" id="bn1"><div class="m1"><p>چنین گفت ارژنگ با نامور</p></div>
<div class="m2"><p>که این جام و آئینه را در نگر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که تا هریک از این دو صنعت بود</p></div>
<div class="m2"><p>وزین هر دو بسیار حکمت بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مر این جام را جام انجم نمای</p></div>
<div class="m2"><p>بخواند خردمند مشکل گشای</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرآینه آینه حکمت است</p></div>
<div class="m2"><p>دو حکمت درو کرده ار صنعت است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چنین کرد انجام انجم نمای</p></div>
<div class="m2"><p>خردمند صنعت گر دلگشای</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>که هرگاه باشد پر از باده جام</p></div>
<div class="m2"><p>نماید درو شکل انجم تمام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ستاره هرآنچه اندر افلاک هست</p></div>
<div class="m2"><p>نمایان درین جام زر پاک هست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بداند هرآنکس که آرد بدست</p></div>
<div class="m2"><p>بدو نیک گیتی ز بالا و پست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ببیند همه سعد نحس سپهر</p></div>
<div class="m2"><p>و زین جام با گردش ماه و مهر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شما را مدار سپهر جهان</p></div>
<div class="m2"><p>شود اندرین جام حکمت عیان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>همان طالع شاه کشورگشای</p></div>
<div class="m2"><p>توان دید در جام انجم نمای</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دگر طالع هر که در عالم است</p></div>
<div class="m2"><p>نمایان درین جام پرحکمت است</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>دویم حکمتش آنکه زین جام می</p></div>
<div class="m2"><p>هرآنکس که نوشد به آئین کی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>برآرد چو از لب برآرد خروش</p></div>
<div class="m2"><p>سپارت بگوید که بادات نوش</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>شنیدی چو تعریف جام کهن</p></div>
<div class="m2"><p>ازآئینه هم نیز بشنو سخن</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>کز آئینه دل شود زنگ غم</p></div>
<div class="m2"><p>شنیدم کاین مانده از شاه جم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>به هندوستان اندران یادگار</p></div>
<div class="m2"><p>مر آئینه در جام گوهر نگار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ولیکن مر آئینه از روی بود</p></div>
<div class="m2"><p>که نزدیک شاه جهان جوی بود</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>مر آئینه کو ز فولاد خاست</p></div>
<div class="m2"><p>سکندر بهنگام خود کرد راست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>کنون حکمت آینه کوش کن</p></div>
<div class="m2"><p>ز گوینده داستان کهن</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>دو رو داشت آئینه دیو سار</p></div>
<div class="m2"><p>به یک روی کج و به یک روی راست</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بهر روی از آن حکمتی شد پدید</p></div>
<div class="m2"><p>که ماندش شگفت آنکه او را شنید</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>چنین بود حکمت بروی دراز</p></div>
<div class="m2"><p>که باهم دو کس کر شدی رزمساز</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>زبهر متاع خرید و فروخت</p></div>
<div class="m2"><p>یکی زین دو گر چشم رایش بدوخت</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>شدی آن دو کس پیش آئینه شاد</p></div>
<div class="m2"><p>بکردی به آئینه سوگند یاد</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بدیدی درآئینه گر مردروی</p></div>
<div class="m2"><p>قسم راست بودی از آن گفتگوی</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>در آئینه گر رو ندادی فروغ</p></div>
<div class="m2"><p>شدی شرمسار او ز گفت دروغ</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بدان روی دیگر چنین بوده است</p></div>
<div class="m2"><p>مر آئینه کو ز حکمت بخاست</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>که در وی اگر خسته ای بنگرید</p></div>
<div class="m2"><p>رخ خود در آئینه دیدی سفید</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>شدی شاد گر غم نگردد تباه</p></div>
<div class="m2"><p>وگر مردنی بود دیدی سیاه</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>سپهدار از این بشد شادمان</p></div>
<div class="m2"><p>دعا کرد ارژنگ را در زمان</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>که گردون بکام شهنشاه باد</p></div>
<div class="m2"><p>سر و دانشت برتر ازماه باد</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>سمند ترا نعل بادا هلال</p></div>
<div class="m2"><p>کمند تو در گردن بدسکال</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>کمین چاکرت نامور شهریار</p></div>
<div class="m2"><p>ببوسید دستش یل نامدار</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>چهل اسب بازین زر پیش داد</p></div>
<div class="m2"><p>چهل نازنین خوبرو بیش داد</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>همه ماهرویان پروین عذار</p></div>
<div class="m2"><p>همه مشکمویان آهو شکار</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>وزین پس یکی مجلس آراست شاه</p></div>
<div class="m2"><p>که بردی بدو جشن اورنگ ماه</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>می و نقل و ساقی گلچهره بود</p></div>
<div class="m2"><p>فروزنده تر مجلس از مهره بود</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>ز لحن مغنی ز سر رفت هوش</p></div>
<div class="m2"><p>چو بلبل دم نای میزد خروش</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>رخ سرکشان بود گلگون ز می</p></div>
<div class="m2"><p>همی مرغ بود و می و نقل و نی</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>چو بر سبز میدان نیلی حصار</p></div>
<div class="m2"><p>ززنگار زد خیمه شاه تتار</p></div></div>