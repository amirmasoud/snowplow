---
title: >-
    بخش ۱۱۰ - مکر کردن دلارام در خلاصی شهریار از بند فرانک گوید
---
# بخش ۱۱۰ - مکر کردن دلارام در خلاصی شهریار از بند فرانک گوید

<div class="b" id="bn1"><div class="m1"><p>فرانک شد آگه ز جوهر فروش</p></div>
<div class="m2"><p>بفرمود در دم به شیران زوش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که دروازه ها را بگیرند تنگ</p></div>
<div class="m2"><p>دلیران همه تیغ روئین به چنگ</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مر آن خواجه را پیش من آورید</p></div>
<div class="m2"><p>روانش بدین انجمن آورید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بگفتند مر فهر تجار را</p></div>
<div class="m2"><p>مر آن خواجه مکر کردار را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فرانک سر بانوان جهان</p></div>
<div class="m2"><p>تو را خوانده زی شهر برکش عنان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دلارام خوانی پر از لعل کرد</p></div>
<div class="m2"><p>به مکر اندر آتش همی نعل کرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بیامد بنزد فرانک چو باد</p></div>
<div class="m2"><p>بکرد آفرین و زمین بوسه داد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مر آن خوان گوهر بر شاه برد</p></div>
<div class="m2"><p>تو گفتی ستاره بر ماه برد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بکردش فرانک بسی آفرین</p></div>
<div class="m2"><p>که نو شه بزی خواجه پاکدین</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>یکی مجلس آراست بر روی او</p></div>
<div class="m2"><p>فرانک بزیب و برنگ و به بو</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گه رفتن آمد چو مه را فراز</p></div>
<div class="m2"><p>فرانک یکی اسپ با زین و ساز</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ببخشید با خلعت شاهوار</p></div>
<div class="m2"><p>دلارام را آن مه گلعذار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>برفت از بر شاه روز دگر</p></div>
<div class="m2"><p>به شد پیش شاه آن مه سیمبر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>دو خوان دگر پر ز گوهر ببرد</p></div>
<div class="m2"><p>فرانک به گنجور خود آن سپرد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بدان روز هم مجلسی ساز کرد</p></div>
<div class="m2"><p>در گنج و بخشش بدو باز کرد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>دو اسپ دگر داد با زین زر</p></div>
<div class="m2"><p>دلارام را آن مه سیم بر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>مرصع بگو هر یکی تاج داشت</p></div>
<div class="m2"><p>کازین پیش آن تاج مهراج داشت</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>فرانک ز سر تاج را برگرفت</p></div>
<div class="m2"><p>نو آئین یکی تاج بر سر گرفت</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چنین گفت مر فهر تجار را</p></div>
<div class="m2"><p>مرآن خواجه مکرکردار را</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>که دادم به تو تاج مهراج را</p></div>
<div class="m2"><p>بنه بر سر این مایه ور تاج را</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>دلارام آن تاج زر برگفت</p></div>
<div class="m2"><p>به فال نکو تاج بر سرگرفت</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>که بگرفتم از وی چه او تاج را</p></div>
<div class="m2"><p>گرفتم همان تاج مهراج را</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بدانکه که بنهاد بر سر کلاه</p></div>
<div class="m2"><p>نمودار شد موی او دید شاه</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بدوز آن سخن هیچ پیدا نکرد</p></div>
<div class="m2"><p>بر نامدارانش رسوا نکرد</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>دلارام از این بود غافل که شاه</p></div>
<div class="m2"><p>بدید است مویش بزیر کلاه</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>سه هفته بدین رسم و آئین و فر</p></div>
<div class="m2"><p>همین این گهر برد آن داد زر</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>فرانک شبی گفت مر فهر را</p></div>
<div class="m2"><p>که امشب مپوشان ز ما چهر را</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>یکی باش امشب به نزدیک من</p></div>
<div class="m2"><p>که سازیم با هم یکی انجمن</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>دلارام آن شب بر شاه ماند</p></div>
<div class="m2"><p>تو گفتی که زهره بر ماه ماند</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>چو از شب یکی بهر بگذشت راست</p></div>
<div class="m2"><p>فرانک همانگاه از جا بخواست</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>گرفت آن زمان دست آن نیک خواه</p></div>
<div class="m2"><p>بدو گفت بردار از سر کلاه</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>دلارام برداشت تاج از سرش</p></div>
<div class="m2"><p>فرو ریخت موی سیه از برش</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>تو گفتی بگل سنبل آمد فرود</p></div>
<div class="m2"><p>و یا آنکه با آتش آمیخت دود</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>فرانک بدانست که آن دختر است</p></div>
<div class="m2"><p>نه تجار دارنده گوهر است</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>دلارام را گفت برگوی راست</p></div>
<div class="m2"><p>که زینگونه تزویر و مکر از کجاست</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>دلارام گفتا که ای تاجدار</p></div>
<div class="m2"><p>پدر بر پدر شاه و هم شهریار</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>سربانوانی و شاه نوی</p></div>
<div class="m2"><p>بعز و باقبال کیخسروی</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>منم دختر سعد بازارگان</p></div>
<div class="m2"><p>که در شهر کشمیر دارم مکان</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>پدر مایه ور بود و با جاه بود</p></div>
<div class="m2"><p>همه شه شناسنده و شاه بود</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>به هیتال شاه آن همی بود دوست</p></div>
<div class="m2"><p>چنان چون که یک مغز بود و دو پوست</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>سه سال است ای شاه آزادگان</p></div>
<div class="m2"><p>که مرد است آن پیر بازارگان</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>چو آن پیر بازارگان بست رخت</p></div>
<div class="m2"><p>شدش جای بر تخته از روی تخت</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>بما بر ستم کرد کشمیر شاه</p></div>
<div class="m2"><p>که بادش نهان تخت و تاج و کلاه</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>برادر دو بودم گرفت او به بند</p></div>
<div class="m2"><p>درآورد آن شاه ناارجمند</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>ز ما آنچه بود از پدر خواسته</p></div>
<div class="m2"><p>گرفت آن ستمکاره ناکاسته</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>برادر بزیر شکنجه بمرد</p></div>
<div class="m2"><p>همه مال و اسباب سعد آن ببرد</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>گریزان من از پیش کشمیر شاه</p></div>
<div class="m2"><p>رسیدم بدرگاه این بارگاه</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>به بستم چو تجار شمشیر من</p></div>
<div class="m2"><p>گریزنده گشتم ز کشمیر من</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>بر این ره یکی مرد دیدم دلیر</p></div>
<div class="m2"><p>که می رفت در راه مانند تیر</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>مر او را غلامان گرفتند زود</p></div>
<div class="m2"><p>بخم کمندش به بستند زود</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>نخستین گمان بردمی شهریار</p></div>
<div class="m2"><p>برازنده تخت گوهرنگار</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>که رخشنده از فر تو تاج شد</p></div>
<div class="m2"><p>همانا که خود زنده مهراج شد</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>که جاسوس دزدان صحرا بود</p></div>
<div class="m2"><p>که زینگونه آن دشت پیدا بود</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>بزه چرم بنهادمش در دو گوش</p></div>
<div class="m2"><p>که از حرف گفتن چرائی خموش</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>بگو تا کئی اندرین رهگذار</p></div>
<div class="m2"><p>کمین را کجا دزد دارد قرار</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>بگفتا که جاسوس دزدان نه ام</p></div>
<div class="m2"><p>بجز در ره نیک مردان نه ام</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>یکی مرد بیچاره ام در گذار</p></div>
<div class="m2"><p>ندانم کجا دزد دارد قرار</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>غلامان کشیدند رختش ز تن</p></div>
<div class="m2"><p>یکی نامه اش بود در پیرهن</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>بخوان نامه اش ای سر تاج خواه</p></div>
<div class="m2"><p>که زیبد تو را تاج مهراج شاه</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>بخوان تا بدانی که در نامه چیست</p></div>
<div class="m2"><p>بهند اندرون دشمن و دوست کیست</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>بگفت این وزر کش برون کرد شاد</p></div>
<div class="m2"><p>مر آن نامه را داد با شاه راد</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>فرانک چو بگشاد آن نامه سر</p></div>
<div class="m2"><p>بخواند و رخش گشت مانند زر</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>نوشته چنین بود کای شاه صور</p></div>
<div class="m2"><p>ز تخت و ز ملک تو بدخواه دور</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>جهان روشن از رای فرصور باد</p></div>
<div class="m2"><p>کجا دشمنش هست در گور باد</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>مر این نامه از پیش ارژنگ شاه</p></div>
<div class="m2"><p>به نزدیک شه صور بافر و کاه</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>که روشن از او تخت کشمیر باد</p></div>
<div class="m2"><p>سر دشمنش زیر شمشیر باد</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>بداند شهنشاه با فر و جاه</p></div>
<div class="m2"><p>که سایم همی بند در زیر چاه</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>بیاری من گر سپاه آوری</p></div>
<div class="m2"><p>برونم از این تیره چاه آوری</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>ز بند و ز زندان رهانی مرا</p></div>
<div class="m2"><p>دراورنگ شاهی نشانی مرا</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>هر آن ملک خواهی ز هندوستان</p></div>
<div class="m2"><p>سپارم مرا او را به شاه جهان</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>و یا آگه از کار کن زال را</p></div>
<div class="m2"><p>که بردارد از کینه کوپال را</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>تهمتن بیاید ابا سرکشان</p></div>
<div class="m2"><p>بدین کین یکی سوی هندوستان</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>تهمتن شود کینه را خواستار</p></div>
<div class="m2"><p>رهاند ز بند گران شهریار</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>کنون چشم ارژنگ بر راه تست</p></div>
<div class="m2"><p>زبان پر ز دشنام بدخواه تست</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>فرانک بدین حیله در چاه شد</p></div>
<div class="m2"><p>رخش تیره چون در ذنب ماه شد</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>ز جان دشمن شاه کشمیر شد</p></div>
<div class="m2"><p>دلش نیز مانند شمشیر شد</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>به فهر آن زمان گفت آن گلعذار</p></div>
<div class="m2"><p>شوم پیش او کینه را خواستار</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>برون از سراندیب لشکر برم</p></div>
<div class="m2"><p>تزلزل بدان بوم و بر در برم</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>چو از کین برم سوی شمشیر دست</p></div>
<div class="m2"><p>ببرم سر شاه کشمیر پست</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>بکوبم بفیلان همه مرز اوی</p></div>
<div class="m2"><p>ز خون دشت کشمیر سازم چو جوی</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>به صورت اگرچه زن افتاده ام</p></div>
<div class="m2"><p>بسیرت چه مردان آزاده ام</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>بود گر چه آهوی نر گر دلیر</p></div>
<div class="m2"><p>شود عاجز آخر بر ماده شیر</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>دلارام گفتش که ای شهریار</p></div>
<div class="m2"><p>مر این کینه را خود مشو خواستار</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>تو شاهی نگهدار تمکین خود</p></div>
<div class="m2"><p>که تمکین ز شاهان گیتی سزد</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>چو باشی تو در تخت و لشکر به جنگ</p></div>
<div class="m2"><p>سرنام ناید به چنگال ننگ</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>مبادا شکستیت آید پدید</p></div>
<div class="m2"><p>نیابی در بسته را خود کلید</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>تو بر جای باش و روان کن سپاه</p></div>
<div class="m2"><p>سپه راست تیغ و شهان را کلاه</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>فرانک چو بشنید گفتش پسند</p></div>
<div class="m2"><p>چنین تا بر آمد ز که خور بلند</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>سپه را درم داد و درع و ستور</p></div>
<div class="m2"><p>فرستاد زی شاه کشمیر صور</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>سپهدار بر آن سپه باجگیر</p></div>
<div class="m2"><p>بشد سوی کشمیر مانند شیر</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>ز لشکر بدرگاه شه کس نماند</p></div>
<div class="m2"><p>که زی شهر کشمیر لشکر نراند</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>همی شاه ماند و دگر ارده شیر</p></div>
<div class="m2"><p>که بودی نگهبان آن نره شیر</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>بهر گه که رفتی بر شهریار</p></div>
<div class="m2"><p>بگفتی که ای نامور غم مدار</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>از آن بند کردم تو را من رها</p></div>
<div class="m2"><p>برستی چه از شیر از دم اژدها</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>ازین تیره چه نیزت آرم برون</p></div>
<div class="m2"><p>ولیکن مرا نیست فرصت کنون</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>ولیکن به شرطی که کردی نخست</p></div>
<div class="m2"><p>بداری همان عهد خود را درست</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>به بخشی به من دخت توپال را</p></div>
<div class="m2"><p>برآری چه از کینه کوپال را</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>سپهبد بگفتا که پیمان یکی ست</p></div>
<div class="m2"><p>چنان چون که یزدان کیهان یکی ست</p></div></div>