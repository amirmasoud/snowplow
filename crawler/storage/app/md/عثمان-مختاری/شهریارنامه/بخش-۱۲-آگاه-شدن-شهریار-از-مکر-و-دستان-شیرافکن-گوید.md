---
title: >-
    بخش ۱۲ - آگاه شدن شهریار از مکر و دستان شیرافکن گوید
---
# بخش ۱۲ - آگاه شدن شهریار از مکر و دستان شیرافکن گوید

<div class="b" id="bn1"><div class="m1"><p>جهانجوی شیرافکن آمد ز راه</p></div>
<div class="m2"><p>بدیبا بیاراست آن قلعه گاه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بهشتی شد از بس که دیبای زر</p></div>
<div class="m2"><p>بهر کوی و برزن کشیدند در</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>برافراز هر روزن از داد نای</p></div>
<div class="m2"><p>زن و مرد آن قلعه بربط سرای</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دم نای کر کرد گوش سپهر</p></div>
<div class="m2"><p>به نظاره آمد در آن قلعه مهر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سپهدار آمد در ایوان او</p></div>
<div class="m2"><p>نبد آگه از مکر و دستان او</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سه روز اندرون قلعه مهمان شدند</p></div>
<div class="m2"><p>ز می خرم و شاد و خندان شدند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ولیکن نبد آگه آن نامجوی</p></div>
<div class="m2"><p>که دام اوفکند است مهمان اوی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کش آرد بدام و به بند آورد</p></div>
<div class="m2"><p>سر نامور در کمند آورد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز دانا شنیدم من این داستان</p></div>
<div class="m2"><p>که میگفت از گفته راستان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>که هرکس که چه در سر ره کند</p></div>
<div class="m2"><p>رهش را زمان سوی آن چه کند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز گردان دو صد مرد جنگی گزید</p></div>
<div class="m2"><p>بدیشان به پیمان سخن گسترید</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چنین است پیمان که در جشن گاه</p></div>
<div class="m2"><p>چه فردا نشیند سپهبد بگاه</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چه سرگرم گردد هم آنکه ز می</p></div>
<div class="m2"><p>زنم دست بردست گویم که هی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>سران سربسر حمله آور شوید</p></div>
<div class="m2"><p>یکایک بر این دلاور شوید</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ز بالای تختش بزیر آورید</p></div>
<div class="m2"><p>بخم کمندش بزیر آورید</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ز فکرش چه بهزاد آگاه شد</p></div>
<div class="m2"><p>دلش تیره زان کار بدخواه شد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بدل گفت این کای سزای منست</p></div>
<div class="m2"><p>که این کپسوان در سرای منست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>نباشد سزاوار زه دار و گیر</p></div>
<div class="m2"><p>که دام افکنم در ره نره شیر</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>همان به کزین کارش آگه کنم</p></div>
<div class="m2"><p>و زو دست بدخواه کوته کنم</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بشد زود و این با سپهبد بگفت</p></div>
<div class="m2"><p>سپهبد چو بشنید ماندش شگفت</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چنین داد پاسخ بدان نامدار</p></div>
<div class="m2"><p>که کردی نهان بدان آشکار</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ترا باد سرسبز و فرخنده بخت</p></div>
<div class="m2"><p>بود روشن از روی تو تاج و تخت</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بشد تا به نزد فرانک چو باد</p></div>
<div class="m2"><p>بدان ماه رخ کرد آن نیز یاد</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>فرانک بدو گفت ای نام دار</p></div>
<div class="m2"><p>برآریم فردا از ایشان دمار</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>نگفتم که بیرون میارش ز بند</p></div>
<div class="m2"><p>همان تا بماند بخم کمند</p></div></div>