---
title: >-
    بخش ۸ - صف آراستن دو لشکر در برابر همدیگر گوید
---
# بخش ۸ - صف آراستن دو لشکر در برابر همدیگر گوید

<div class="b" id="bn1"><div class="m1"><p>بشد شاد هیتال بر پیل کوس</p></div>
<div class="m2"><p>ببست و جهان شد بگرد آبنوس</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دلیران هندی پی کین همه</p></div>
<div class="m2"><p>بروها بکردند پرچین همه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز قطران یکی بحر آمد بجوش</p></div>
<div class="m2"><p>بشد سر ز بانگ تبیره ز هوش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بلرزید کوه از دم گاودم</p></div>
<div class="m2"><p>بتن زهره شیر گردیده گم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ببردند گردان بهامون درفش</p></div>
<div class="m2"><p>شد از تیغ هامان سراسر بنفش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یلان صف کشیدند نیزه بکف</p></div>
<div class="m2"><p>نیستان زمین گشته از هر طرف</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>غو بادپایان ز گردون گذشت</p></div>
<div class="m2"><p>ز بانگ تبیره جهان خیره گشت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زمین سراندیب چون کوه شد</p></div>
<div class="m2"><p>ز بس کشته در دشت انبوه شد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بقلب سپه شاه هیتال بود</p></div>
<div class="m2"><p>به دستش ز کین تیغ کوپال بود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به یکدست جمهور زرین کلاه</p></div>
<div class="m2"><p>جهان بود از گرد لشکر سپاه</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به یکدست او گرد زرفام بود</p></div>
<div class="m2"><p>که رزمش به ساطور خودکام بود</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به پیش سپه داشت توپال جای</p></div>
<div class="m2"><p>همی ناله آمد ز هند و درای</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>وزین روی ارژنگ صف برکشید</p></div>
<div class="m2"><p>پی رزم هیتال لشکر کشید</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>به قلب سپه جای خود ساخت شاه</p></div>
<div class="m2"><p>نهاد از بر کوهه پیل گاه</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>دو لشکر چو زین گونه بستند صف</p></div>
<div class="m2"><p>یلان تیغ هندی گرفته به کف</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ز گردان مغرب یکی شیره مرد</p></div>
<div class="m2"><p>به میدان درآمد که جوید نبرد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>دلیری ز لشکر برون راند اسب</p></div>
<div class="m2"><p>بزین برخروشان چه آزرگشسب</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>سرافراز را نام فرهنگ بود</p></div>
<div class="m2"><p>ورا رزم با فیل نر ننگ بود</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چه آمد سر ره به شیرویه بست</p></div>
<div class="m2"><p>یکی تیز زوبین گرفته بدست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>به شیرویه گفت ای یل سرفراز</p></div>
<div class="m2"><p>بگیر از من این حربه جانگداز</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بگفت این و آمد بتنگ اندرش</p></div>
<div class="m2"><p>برافروخت ژوبین زد بر سرش</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چنان چونکه بیرون شد از پشت او</p></div>
<div class="m2"><p>نماندش بجز باد در مشت او</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ز بر زیر آمد سرتاج ور</p></div>
<div class="m2"><p>سرش زیر پا گشت و پا زیر سر</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چو شد کشته شیرویه رزمخواه</p></div>
<div class="m2"><p>یکی دیگر آمد ز مغرب سپاه</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ورا نیز فرهنگ آورد زیر</p></div>
<div class="m2"><p>ز بالای زین بر بیک چوب تیر</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>یکی دیگر آمد ورا نیز کشت</p></div>
<div class="m2"><p>همی کشت و در رزم ننمود پشت</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>چه هیتال ازقلب لشکر بدید</p></div>
<div class="m2"><p>بلرزید از خود چه از باد بید</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بدو گفت جمهور کای شهریار</p></div>
<div class="m2"><p>ز رزم یلان رنجه خاطر مدار</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>و دیگر ز ما کشته شد یک دو مرد</p></div>
<div class="m2"><p>چنین است آئین روز نبرد</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>من اکنون دلیری فرستم بجنگ</p></div>
<div class="m2"><p>که جنگ آورد روز کین با نهنگ</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>یکی زین سپه زنده نگذارد او</p></div>
<div class="m2"><p>چه نهصد منی گرز بردار او</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>وز آن پس نگه سوی زرفام کرد</p></div>
<div class="m2"><p>که زی رزم رای آور ای شیره مرد</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>سراین دلاور بیاور برم</p></div>
<div class="m2"><p>بدان تایکی رزم تو بنگرم</p></div></div>