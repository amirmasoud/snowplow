---
title: >-
    بخش ۱۳ - رفتن شهریار بشکار و کشتن شیرافکن را گوید
---
# بخش ۱۳ - رفتن شهریار بشکار و کشتن شیرافکن را گوید

<div class="b" id="bn1"><div class="m1"><p>چو خور سر زد از چتر فیروزه رنگ</p></div>
<div class="m2"><p>سپهبد کمر کینه را بست تنگ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بفرمود تا اسب او زین کنند</p></div>
<div class="m2"><p>یلان رابه نخجیر آئین کنند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نشست از بر اسب با ماهروی</p></div>
<div class="m2"><p>برون شد ز قلعه سبک جنگجوی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جهانجوی بهزاد هم برنشست</p></div>
<div class="m2"><p>برون آمد از قلعه چون فیل مست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چه شیرافکن آن رای نخجیر دید</p></div>
<div class="m2"><p>تبه کردنش را نه تدبیر دید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همان نیز آمد برون از حصار</p></div>
<div class="m2"><p>ابا باز شاهین برای شکار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سوار صد از دز بزیر آمدند</p></div>
<div class="m2"><p>به نخجیر کردن دلیر آمدند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو آمد به نزدیکی شهریار</p></div>
<div class="m2"><p>ستمکاره شیرافکن کامکار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سپهبد چو دیدش برآشفت تند</p></div>
<div class="m2"><p>کز آن تندیش رعد گردید کند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خروشان بدو گفت کای بیخرد</p></div>
<div class="m2"><p>چه بد دیدی از من که کردی تو بد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز مرد خردمند کی این سزاست</p></div>
<div class="m2"><p>که رخ بربپیچی تواز راه راست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>من آزاد کردم سرت را ز بند</p></div>
<div class="m2"><p>بدی ورنه اکنون بخم کمند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بگفت این و از پی برانگیخت اسپ</p></div>
<div class="m2"><p>کش از زین برآرد چو آذر گشسب</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>دگرباره آردش سر در کمند</p></div>
<div class="m2"><p>چنانست کردار چرخ بلند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بدانست شیرافکن نامدار</p></div>
<div class="m2"><p>که با وی نتابد بهنگام کار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>عنان را بپیچید مرد دلیر</p></div>
<div class="m2"><p>چو روبه گریزان شد از نره شیر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>سپه دار برداشت پیچان کمند</p></div>
<div class="m2"><p>چو باد از پسش راند سرکش سمند</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>خروشید کای مرد با نام ننگ</p></div>
<div class="m2"><p>گریزان چرائی ز شیران جنگ</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ترا گر بدی نام و ننگ و نژاد</p></div>
<div class="m2"><p>نگشتی به مزدوری دیو شاد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ترا دیو و اژده ازراه برد</p></div>
<div class="m2"><p>هرآن چه که کندی بدان چاه برد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ندانستی ای ابله بیخرد</p></div>
<div class="m2"><p>که بد را مکافات بد میرسد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>به تنگ اندرش چون درآمد فکند</p></div>
<div class="m2"><p>خم خام آمد برش زیر بند</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ز پشت تکاور کشیدش بزیر</p></div>
<div class="m2"><p>فرود آمد و بست دستش چو شیر</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>سپردش به بهزاد کاو را بدار</p></div>
<div class="m2"><p>و گرنه تو دانی بدارش برآر</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بدو گفت بهزاد کای کامران</p></div>
<div class="m2"><p>بگویم شگفتی یکی داستان</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بدارم گر او راکنون زیر بند</p></div>
<div class="m2"><p>از او بر من آید دمادم گزند</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>من و این دلاور ز یک مادریم</p></div>
<div class="m2"><p>بدین قلعه با هم کنون یاوریم</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>پدرمان دو باشد ایا نامدار</p></div>
<div class="m2"><p>شگفتی بسی هست در روزگار</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>ز عم من ای گرد فرخنده زور</p></div>
<div class="m2"><p>یکی دختری مانده بهتر ز حور</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>کنون عاشق این کس بدان دختر است</p></div>
<div class="m2"><p>بدین کین من در دلش اندر است</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>بدو در نیارد سر آن ماهروی</p></div>
<div class="m2"><p>که مهرم بدل دارد آن نیکخوی</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>اگر یابد از من رها مرد کین</p></div>
<div class="m2"><p>مرا می کشد ای سوار گزین</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>یکی آنکه کردمت آگاه نیز</p></div>
<div class="m2"><p>ز مکر و ز دستان بدخواه نیز</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>دوم آنکه این دخت را دید شست</p></div>
<div class="m2"><p>سرش رابباید ز تن کرد بست</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>چه دشمن بدست آیدت کش مدار</p></div>
<div class="m2"><p>وگرنه پشیمانی آرد ببار</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>بگفت این و برداشت خنجر ز کین</p></div>
<div class="m2"><p>نه شرم از برادر نه از راه دین</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>بزد تیغ از تن سرش را برید</p></div>
<div class="m2"><p>تن نامدارش به خون در کشید</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>ز بهر زن او را چنان کشت زار</p></div>
<div class="m2"><p>که گم باد نام زن از روزگار</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>که ناگه خروشی برآمد ز دشت</p></div>
<div class="m2"><p>سواری صد از دشت دیدار گشت</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>همه خسته و رنجه و زخم دار</p></div>
<div class="m2"><p>خروشان و جوشان چو ابر بهار</p></div></div>