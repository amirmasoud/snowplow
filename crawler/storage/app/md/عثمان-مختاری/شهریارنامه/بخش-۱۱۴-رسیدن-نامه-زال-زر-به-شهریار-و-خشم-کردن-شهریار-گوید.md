---
title: >-
    بخش ۱۱۴ - رسیدن نامه زال زر به شهریار و خشم کردن شهریار گوید
---
# بخش ۱۱۴ - رسیدن نامه زال زر به شهریار و خشم کردن شهریار گوید

<div class="b" id="bn1"><div class="m1"><p>جهان دیده دهقان چنین کرد یاد</p></div>
<div class="m2"><p>که آن نامه زال پاکیزه زاد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که زی فرامرز کردش روان</p></div>
<div class="m2"><p>شب تیره بر سوی هندوستان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به راهی که آمد از آن شهریار</p></div>
<div class="m2"><p>برآن ره فرستاده را شد گذار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کشن لشکری دید آن نامدار</p></div>
<div class="m2"><p>زده خیمه در پیش دریا گذار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خیالش چنان کان فرامرز بود</p></div>
<div class="m2"><p>که با لشکر خود در آن مرز بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به نزدیک لشکر که آمد فراز</p></div>
<div class="m2"><p>ز مردی بپرسید آن سرفراز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>که این لشکر بی کران زان کیست</p></div>
<div class="m2"><p>بدو گفت آن مرد نام تو چیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مرا نام گفتا که ارشیون است</p></div>
<div class="m2"><p>به زابل مرا مأمن و مسکن است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>یکی نامه از زال دارم بکش</p></div>
<div class="m2"><p>به نزد فرامرز شمشیرکش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بود گفت این لشکر بی کران</p></div>
<div class="m2"><p>ز پور تهمتن سر سروران</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بیا تا ترا سوی آن یل برم</p></div>
<div class="m2"><p>کازین هدیه بر چرخ ساید سرم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به بردش همانگاه آن نامدار</p></div>
<div class="m2"><p>به نزدیک شیر ژیان شهریار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>سپهبد چه آن نامه برخواند و خشم</p></div>
<div class="m2"><p>گرفت و فرو ریخت آب از دو چشم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>روان پاسخ نامه زال کرد</p></div>
<div class="m2"><p>سرنامه بر تیغ و کوپال کرد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>که بر من نیا کینه دارد ازین</p></div>
<div class="m2"><p>که من سوی هند آمدم بهر کین</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بدیدی مرا چون به گاه شتاب</p></div>
<div class="m2"><p>شماری ز ترکان افراسیاب</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>نخستین از این دوده بودم بکین</p></div>
<div class="m2"><p>از آن من شدم سوی هندو زمین</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>که آرم سپاهی پدید از هنر</p></div>
<div class="m2"><p>که سامم نگوید دگر بی پدر</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بداند که از بی پدر نیز کار</p></div>
<div class="m2"><p>بیاید چه پیدا شود کارزار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>کنون از تو ایران و هم سیستان</p></div>
<div class="m2"><p>من و شاه ارژنگ و هندوستان</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>فرستاده راخلعت زرنگار</p></div>
<div class="m2"><p>بداد و روان کرد یل شهریار</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>وز آنجای برگشت آن نامور</p></div>
<div class="m2"><p>دل آکنده از خشم و پرکینه سر</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>چنین تا بیامد به شهر سراند</p></div>
<div class="m2"><p>به نزدیک ارژنگ شاه بلند</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>سراسر به ارژنگ گفت این سخن</p></div>
<div class="m2"><p>وز آن پس سپهدار شمشیر زن</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>سپه را سوی چین روان کرد شاد</p></div>
<div class="m2"><p>به جمهور گفت آن زمان پاکزاد</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>که ارجاسپ اکنون به ایران شده</p></div>
<div class="m2"><p>به جنگ دلیران و شیران شده</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>تهی مانده زو تخت افراسیاب</p></div>
<div class="m2"><p>سپه برد زی بلخ زان سوی آب</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>من اکنون سوی ملک توران ر(و)م</p></div>
<div class="m2"><p>بباید برین راه کین نغنوم</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>سر تخت توران به چنگ آورم</p></div>
<div class="m2"><p>جهان بر بداندیش تنگ آورم</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>وز آن جا سپه سوی ایران برم</p></div>
<div class="m2"><p>برزم یلان نره شیران برم</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>هنر از نهان آشکارا کنم</p></div>
<div class="m2"><p>نه مردم بدین گر مدارا کنم</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>بگیرم سر تخت لهراسپ را</p></div>
<div class="m2"><p>ببرم سر شوم ارجاسپ را</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>یکی سازم ایران و توران بهم</p></div>
<div class="m2"><p>بگویم جواب یلان بیش و کم</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>چو رستم بیاید ز خاور زمین</p></div>
<div class="m2"><p>بدرد ز نعل تکاور زمین</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>نمایم بدو نامه زال زر</p></div>
<div class="m2"><p>تهمتن بخواند همه سر بسر</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>بداند که از من نیامد گناه</p></div>
<div class="m2"><p>نخست اندرین رزم و این کینه گاه</p></div></div>