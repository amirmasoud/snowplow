---
title: >-
    بخش ۹۲ - آمدن ارهنگ به پای حصار سیستان و بیرون آمدن زال زر گوید
---
# بخش ۹۲ - آمدن ارهنگ به پای حصار سیستان و بیرون آمدن زال زر گوید

<div class="b" id="bn1"><div class="m1"><p>کمر بند را کرد ارهنگ تنگ</p></div>
<div class="m2"><p>کشید از بر باره زین خدنگ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سوی سیستان جنگ آورد دیو</p></div>
<div class="m2"><p>ز زیر و ز بالا برآمد غریو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در شهر و بیرون برآمد خروش</p></div>
<div class="m2"><p>دویدند بر باره شیران زوش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سپهدار آمد به پیش حصار</p></div>
<div class="m2"><p>ز بس ناوک انداز خنجر گذار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو آن رستخیز گران زال دید</p></div>
<div class="m2"><p>جهان پر ز شمشیر و کوپال دید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بزد دست و برداشت کوپال زال</p></div>
<div class="m2"><p>پی کین ارهنگ بفراخت بال</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به پوشید دستان سلیح نبرد</p></div>
<div class="m2"><p>نشست از بر باره ره نورد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در قلعه بگشاد و آمد برون</p></div>
<div class="m2"><p>تو گفتی که پیل آمد از که نگون</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چه آمد یکی نعره زد شیر مست</p></div>
<div class="m2"><p>کزان نعره خورشید در خون نشست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>برآن لشکر دیو بر حمله برد</p></div>
<div class="m2"><p>به چنگ اندرون گرزه سام کرد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بگرز آن سرافراز با رای و زور</p></div>
<div class="m2"><p>سپه را از آنجایگه کرد دور</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دو صد مرد نامی بگرز گران</p></div>
<div class="m2"><p>بکشت آن سرافراز جنگاوران</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چه از قلب گه گردار هنگ دید</p></div>
<div class="m2"><p>رخ زال و کوپال و آن چنگ دید</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>برانگیخت از جای شبرنگ تند</p></div>
<div class="m2"><p>ز تندیش شد تیغ خونریز کند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بزد دست برداشت کوپال را</p></div>
<div class="m2"><p>چنین گفت مر نامور زال را</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>که ای پیر فرتوده گوژپشت</p></div>
<div class="m2"><p>چه پیری مکن رای رزم درشت</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>که رزم از جوانان نوخاسته است</p></div>
<div class="m2"><p>کازیشان دل شیر نر کاسته است</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>سرت را هم اکنون بزیر آورم</p></div>
<div class="m2"><p>ز بالا چو بر دست تیر آورم</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بگو کز دلیران تو را نام چیست</p></div>
<div class="m2"><p>کزین گونه پیری به گیتی نزیست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گه جنگ کوپال و کین آورد</p></div>
<div class="m2"><p>فلک را زبر زمین آورد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چنین پاسخش داد دستان پیر</p></div>
<div class="m2"><p>که ای بیهده گوی واژون بزیر</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>مرا پیر خوانی و خود را جوان</p></div>
<div class="m2"><p>جوانی و پیری به بیند نوان</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>توان آنکه دارد دلیر است و بس</p></div>
<div class="m2"><p>نگوئی گه رزم پیر است و بس</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>شود شیر هر چند در بیشه پیر</p></div>
<div class="m2"><p>شود تند و نر شیر نخجیرگیر</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چه گر پیر در چشم اهریمنم</p></div>
<div class="m2"><p>جوان است بازوی فیل افکنم</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>مرا نام دستان نهاد است سام</p></div>
<div class="m2"><p>کند پخته گرزم سر شیر خام</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>شد از گردش چرخم ار گوژپشت</p></div>
<div class="m2"><p>نگه کن بدین یال و کوپال و مشت</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بدانست ارهنگ کوهست زال</p></div>
<div class="m2"><p>که زین گونه بفراخت گرزش به یال</p></div></div>