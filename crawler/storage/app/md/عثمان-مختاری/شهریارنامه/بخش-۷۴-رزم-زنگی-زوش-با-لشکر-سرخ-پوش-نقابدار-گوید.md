---
title: >-
    بخش ۷۴ - رزم زنگی زوش با لشکر سرخ پوش نقابدار گوید
---
# بخش ۷۴ - رزم زنگی زوش با لشکر سرخ پوش نقابدار گوید

<div class="b" id="bn1"><div class="m1"><p>فرود آمد آنگه یل دیوبند</p></div>
<div class="m2"><p>برآمد دم نای هندی بلند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شد آگه از آن سرخ پوش سوار</p></div>
<div class="m2"><p>که آمد ز پس نامور شهریار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بشد در شگفت آن یل نامور</p></div>
<div class="m2"><p>همی لب گزید و بجنباند سر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>که چل روز رفت و بیامد چنین</p></div>
<div class="m2"><p>مراین نامور یل ز مغرب زمین</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بفرمود تا کوس بنواختند</p></div>
<div class="m2"><p>همی رزم را باز پرداختند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چه روز دگر شد زمین عطربار</p></div>
<div class="m2"><p>دو لشکر ببستند تنگ استوار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>برا(فرا)ختند از دورویه علم</p></div>
<div class="m2"><p>جهانی پر از ناله گاودم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دو لشکر برابر چه گشتند راست</p></div>
<div class="m2"><p>تو گفتی که از دشت کین کوه خاست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دلیری بناوردگه راند اسپ</p></div>
<div class="m2"><p>برش رفت زنگی چه آذرگشسپ</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چه زنگی سر ره بدان دیو بست</p></div>
<div class="m2"><p>یکی حربه از چوب بودش بدست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>پیاده به نزد همآورد رفت</p></div>
<div class="m2"><p>همآورد را نیزه کین گرفت</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به نیزه بر زنگئی زوش شد</p></div>
<div class="m2"><p>چه آتش که از باد در جوش شد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بزد دست زنگی سنانش گرفت</p></div>
<div class="m2"><p>اجل بود گفتی که جانش گرفت</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>کشیدش ز دست و گرفتش کمر</p></div>
<div class="m2"><p>یکی برخروشید چون شیر نر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>کشیدش ز پشت تکاور بزیر</p></div>
<div class="m2"><p>ز هم بر دریدش چه خرگوش شیر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>یکی دیگر آمد میان سپاه</p></div>
<div class="m2"><p>ز هم بر دریدش همان گه سیاه</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>برآمد فغان از همه دشت و راغ</p></div>
<div class="m2"><p>چه سنجد ملخ پیش چنگال زاغ</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چنین تا از ایشان دو ده مرد کشت</p></div>
<div class="m2"><p>به چنگال و دندان سیاه درشت</p></div></div>