---
title: >-
    بخش ۱۰۵ - فرستادن ارجاسپ ارهنگ را بری گوید
---
# بخش ۱۰۵ - فرستادن ارجاسپ ارهنگ را بری گوید

<div class="b" id="bn1"><div class="m1"><p>وزین روی ارهنگ ره کردمی</p></div>
<div class="m2"><p>چنین تا بیامد ز جرجان بری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دلیران ایران ز خرد و بزرگ</p></div>
<div class="m2"><p>همه تیز دندان بکین همچو گرگ</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به ری در همه جمع بودند شاد</p></div>
<div class="m2"><p>که آرند زی بلخ لشکر چه باد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>که از ره سپاه گران در رسید</p></div>
<div class="m2"><p>بگردون چکاچاک خنجر رسید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سر سروران نرم در زیر پای</p></div>
<div class="m2"><p>جهان در خروش از دم کر نای</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز خون دشت ری شکل دریا گرفت</p></div>
<div class="m2"><p>ز تیغ آتش فتنه بالا گرفت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>میان سپه اندر ارهنگ بود</p></div>
<div class="m2"><p>یکی تیغش از کینه در چنگ بود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سر ره بر او بست فیروز طوس</p></div>
<div class="m2"><p>برآمد ز لشکر دم بوق کوس</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بزد دیو وارونه از کینه چنگ</p></div>
<div class="m2"><p>ربودش چه مرغ از جناح خدنگ</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خروشان زدش بر زمین همچو کوس</p></div>
<div class="m2"><p>ببست آن زمان دست فیروز طوس</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بدو اندر آویخت رهام شیر</p></div>
<div class="m2"><p>بر آمد ز میدان کین دار و گیر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>در افکند وارونه دیو دمند</p></div>
<div class="m2"><p>بیال سرافراز خم کمند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ورا نیز بربست چون فیل مست</p></div>
<div class="m2"><p>از آن پس به گرز گران برد دست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>سپه چون بدیدند آن رستخیز</p></div>
<div class="m2"><p>نهادند یکسر سر اندر گریز</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بماندند برجا درفش بلند</p></div>
<div class="m2"><p>ابا کوس خرگاه رومی پرند</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بدان کوهپایه نهادند سر</p></div>
<div class="m2"><p>بدیشان نه خود ونه درع و سپر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چو زان کینه پرداخت ارهنگ زود</p></div>
<div class="m2"><p>سپه برد سوی صفاهان چه دود</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>قیامت بدآن بوم و بر آورید</p></div>
<div class="m2"><p>فلک را زبر بر زمین آورید</p></div></div>