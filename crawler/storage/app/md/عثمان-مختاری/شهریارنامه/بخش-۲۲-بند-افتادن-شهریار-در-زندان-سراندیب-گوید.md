---
title: >-
    بخش ۲۲ - بند افتادن شهریار در زندان سراندیب گوید
---
# بخش ۲۲ - بند افتادن شهریار در زندان سراندیب گوید

<div class="b" id="bn1"><div class="m1"><p>بعاس آن زمان گفت هیتال شاه</p></div>
<div class="m2"><p>که برکش همین شب مر او را به راه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به سوی سراندیب بر بند کن</p></div>
<div class="m2"><p>دل از بند او شاد خورسند کن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سراندیب را خود کمیندار باش</p></div>
<div class="m2"><p>شب و روز هشیار و بیدار باش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>فکندند یل را به پشت نوند</p></div>
<div class="m2"><p>ببردند آن گاه در زیر بند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به سوی سراندیب در پیش شاه</p></div>
<div class="m2"><p>شب تیره با سروران سپاه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به زندان مهراج کردش به بند</p></div>
<div class="m2"><p>جهان جوی را عاس ناارجمند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خود آن جای شد پاسبان دلیر</p></div>
<div class="m2"><p>ابا ده هزار از یلان همچو شیر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سپهبد به زندان مهراج ماند</p></div>
<div class="m2"><p>جدا او هم از باره و تاج ماند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دگر روز دستور آمد بگاه</p></div>
<div class="m2"><p>به هیتال گفت ای شبان کلاه</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>یکی نامه ای کن به نزدیک زال</p></div>
<div class="m2"><p>بگویش که ای پهلو بی همال</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نبیر تو ایدر به بند من است</p></div>
<div class="m2"><p>گرفتار خم کمند من است</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بدین گونه در هند آمد نهان</p></div>
<div class="m2"><p>برآورد این فتنه از هندیان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>دلیری روانکن بدین با یلان</p></div>
<div class="m2"><p>که او رابیارد به زابلستان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>من و شاه ارژنگ و هندو سپاه</p></div>
<div class="m2"><p>ببینیم تا بر که گردد کلاه</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>به ارژنگ گو هند یابد قرار</p></div>
<div class="m2"><p>دهد باز با شاه ایران دیار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>به من گر بگیرد قرار این زمین</p></div>
<div class="m2"><p>دهم باژ و هرگز نیایم بکین</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چو این بشنود زال سام سوار</p></div>
<div class="m2"><p>فرستد یقین در پی شهریار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ز هندوستان سوی ایران برند</p></div>
<div class="m2"><p>بر نامور شاه شیران برند</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>از آن پس بود رزم ارژنگ شاه</p></div>
<div class="m2"><p>به بیند تا برکه گردد کلاه</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>پسندید هیتال این عزم اوی</p></div>
<div class="m2"><p>فرستاد نامه سوی رزمجوی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>برزال رز کاینچنین است کار</p></div>
<div class="m2"><p>بداند جهاندار زابل دیار</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>فرستاده زی زابلستان برفت</p></div>
<div class="m2"><p>ز درگاه هیتال چون باد تفت</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>پس آگه ازین گشت ارژنگ شاه</p></div>
<div class="m2"><p>برآشفت و برکرد جامه سیاه</p></div></div>