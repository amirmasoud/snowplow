---
title: >-
    بخش ۱۰ - رزم لشکر ارژنگشاه با زرفام گوید
---
# بخش ۱۰ - رزم لشکر ارژنگشاه با زرفام گوید

<div class="b" id="bn1"><div class="m1"><p>بجنبید لشکر چه دریای چین</p></div>
<div class="m2"><p>دمیدند در دم گره بر جبین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خروشیدن سنج و شیپور و کوس</p></div>
<div class="m2"><p>همی گفت بر نامداران فسوس</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>وزین روی جمهور و هیتال شاه</p></div>
<div class="m2"><p>کشیدند لشکر سوی رزمگاه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>برانگیخت زرفام فیلش ز جای</p></div>
<div class="m2"><p>برآمد غو طیل و آوای نای</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز بس کز دو رویه برآمد خروش</p></div>
<div class="m2"><p>شد از جوش دریای بی بن خموش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تو گوئی بدرید گوش سپهر</p></div>
<div class="m2"><p>درافتاد از طاق فیروزه مهر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز بیم دم تیغ الماس رنگ</p></div>
<div class="m2"><p>جگر آب شد در درون نهنگ</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز بس خون فرو ریخت روز نبرد</p></div>
<div class="m2"><p>نه برخاست ز آن دشت تا حیزه گرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به چشم دلیران و مردان جنگ</p></div>
<div class="m2"><p>فزون از مژه بود تیر خدنگ</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شکست از سوی شاه ارژنگ بود</p></div>
<div class="m2"><p>سه روز اندرون رزمگه جنگ بود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>فتاده تن نامداران بخاک</p></div>
<div class="m2"><p>سر و پشت و پهلو به شمشیر چاک</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چه ارژنگ دید آن چنان کارزار</p></div>
<div class="m2"><p>بگرداند رو از دم گیر و دار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>همی شد گریزان ز هیتال شاه</p></div>
<div class="m2"><p>دمان و رمان از پس او سپاه</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>همه راه پرخسته و کشته شد</p></div>
<div class="m2"><p>ز کشته بهر سوی صد پشته شد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>یکی کوه پیش اندر آمد براه</p></div>
<div class="m2"><p>بنه سوی که برد فرخنده شاه</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>یکی کنده در پیش که کرد زود</p></div>
<div class="m2"><p>کز آنجای رفتن نبودش بسود</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>کز آن کوه ره دورند تا سرند</p></div>
<div class="m2"><p>بکه جای گیرد یکی کنده کند</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>همه مردمش خسته و بسته دید</p></div>
<div class="m2"><p>گهی دست و گه لب بدندان گزید</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چه زد بر سرش گرد آن جام زر</p></div>
<div class="m2"><p>شد از دود آن قلعه زیر و زبر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>زمانی چه شد برطرف گشت دود</p></div>
<div class="m2"><p>در دز بروی سپهبد گشود</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ز در بند دز زود بیرون دوید</p></div>
<div class="m2"><p>از آن کوه سر سوی هامون دوید</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بیآمد بدان چشمه کامد نخست</p></div>
<div class="m2"><p>تنش بود لرزان دلش بود سست</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بدان چشمه سر یکدمی بغنوید</p></div>
<div class="m2"><p>بناگه شد از دشت کردی پدید</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>سواری برون آمد از تیره کرد</p></div>
<div class="m2"><p>که نعل مستورش جهان تیره کرد</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>فتاده ز سر خود و کیش از میان</p></div>
<div class="m2"><p>نه برجای تیر و نه بر زه کمان</p></div></div>