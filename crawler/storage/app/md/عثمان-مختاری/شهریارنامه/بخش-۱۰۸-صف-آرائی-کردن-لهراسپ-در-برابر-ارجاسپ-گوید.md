---
title: >-
    بخش ۱۰۸ - صف آرائی کردن لهراسپ در برابر ارجاسپ گوید
---
# بخش ۱۰۸ - صف آرائی کردن لهراسپ در برابر ارجاسپ گوید

<div class="b" id="bn1"><div class="m1"><p>ابا مادر و مرد فرزانه شاد</p></div>
<div class="m2"><p>بره بر همی رفت مانند دود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سحر گه که برزد خور از کوه شید</p></div>
<div class="m2"><p>شب تیره شد یا به دامن کشید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یکی گرد بر پیش ره بر بخواست</p></div>
<div class="m2"><p>که شد بر سپهر برین گرد راست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بدان لشکر اردشیر سوار</p></div>
<div class="m2"><p>که آمد سپهبد پی کارزار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بیامد به نزدیک گشتاسپ شاد</p></div>
<div class="m2"><p>پیاده شد و پای او بوسه داد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چه گرگین و گرگوی و روئین گرد</p></div>
<div class="m2"><p>که گوی از دلیری به گیتی ببرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ابا آن پراکنده لشکر ز راه</p></div>
<div class="m2"><p>رسیدند آن هر سه فرخ کلاه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سپاه پراکنده باز آمدند</p></div>
<div class="m2"><p>دو بهره همه خسته و دردمند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>وز آنجای کردند سر روی راه</p></div>
<div class="m2"><p>کنون بشنو از کار ارجاسپ شاه</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چهارم ز کاریکه آمد خبر</p></div>
<div class="m2"><p>که آمد ز ایران سپه بیشمر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چو گشتاسپ با گرد گرگین شیر</p></div>
<div class="m2"><p>دگر اردشیر سوار دلیر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>رسد دمبدم لشکر بیکران</p></div>
<div class="m2"><p>همه نامداران جنگ آوران</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>سپهدار دستان برآوردگار</p></div>
<div class="m2"><p>سپه برد بیرون بدشت حصار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>سپه راست کرد وبرآراست جنگ</p></div>
<div class="m2"><p>قضای جهان گشت بر مرد تنگ</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>فرامرز آمد به پیش سپاه</p></div>
<div class="m2"><p>به قلب سپه در جهاندار شاه</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>یمین سپه پارس پرهیزگار</p></div>
<div class="m2"><p>چو خورشید مینو بدی از یسار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>وز آن روی صف بست ارجاسپ نیز</p></div>
<div class="m2"><p>کمر بست برکین لهراسپ تیز</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>دم نای ژوبین بدرید گوش</p></div>
<div class="m2"><p>ز بانگ تبیره جهان در خروش</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>جهان را دگر فتنه بیدار شد</p></div>
<div class="m2"><p>سر پر دلان پر ز پیکار شد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>علم اژدهاییست گفتی غنیم</p></div>
<div class="m2"><p>مه از میخهای علم برد و نیم</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>سر فتنه جویان ز کین گرم بود</p></div>
<div class="m2"><p>بسر دیده ها راند آزرم بود</p></div></div>