---
title: >-
    بخش ۱۱۶ - دار زدن ارجاسپ گودرز پیر را گوید
---
# بخش ۱۱۶ - دار زدن ارجاسپ گودرز پیر را گوید

<div class="b" id="bn1"><div class="m1"><p>دگرپور کشواد گودرز را</p></div>
<div class="m2"><p>مر آن پیر بارای و باارز را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>برآن تا سرانشان ببرم ز تن</p></div>
<div class="m2"><p>به خون پسر اندرین انجمن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یلان را چنان بسته بر دار خوار</p></div>
<div class="m2"><p>بفرمود آن ترک شوریده کار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>که از تن سرانشان ببرند پست</p></div>
<div class="m2"><p>کازین پیر دید است توران شکست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بدو گفت بیورد که ای شهریار</p></div>
<div class="m2"><p>سر بی گنه را میاور بدار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز فیروز و رهام نامد گناه</p></div>
<div class="m2"><p>تو این کینه از پور گودرز خواه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چه او برد خسرو به ایران زمین</p></div>
<div class="m2"><p>برو بر به شاهی بگرد آفرین</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شنیدی به کوه هماون چه کرد</p></div>
<div class="m2"><p>بدان روز پیکار و دشت نبرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دگر آنکه کشته است پیران به جنگ</p></div>
<div class="m2"><p>سرو سینه کان زوست در زیر سنگ</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بکن پیر گودرز یل را بدار</p></div>
<div class="m2"><p>چه رهام و فیروز یل را بدار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>اگر بود خواهد تو را بوق و کوس</p></div>
<div class="m2"><p>به بندد کمر گرد فیروز طوس</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به پیش جهانجوی ارجاسپ شاه</p></div>
<div class="m2"><p>چه رهام دیگر سران سپاه</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چه بشنید ارجاسپ گفتار اوی</p></div>
<div class="m2"><p>پسندید افروخت از کینه روی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>جهانجوی رهام را در زمان</p></div>
<div class="m2"><p>ابا گرد فیروز روشن روان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>فرستاد زی حصن روئین چه باد</p></div>
<div class="m2"><p>بدان حصن شان بند بر پا نهاد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>یلان را چه بردند از پیش شاه</p></div>
<div class="m2"><p>یکی دار زد ترک پیش سپاه</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بفرمود گودرز را بسته خوار</p></div>
<div class="m2"><p>ستیزنده دژخیم آرد بدار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>روان برد دژخیم گودرز را</p></div>
<div class="m2"><p>مرآن نامور پیر باارز را</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بدارش درآورد در دم دلیر</p></div>
<div class="m2"><p>بکردند از کینه باران تیر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>فلک برکشیدش بسی روزگار</p></div>
<div class="m2"><p>سرانجام کردش زمان خوشه دار</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چنین است کردار این گوژپشت</p></div>
<div class="m2"><p>که هر گوهری آرد از کان به مشت</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>دو روزش بافراز دارد بدست</p></div>
<div class="m2"><p>سیم روزش اندازد از دست پست</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>عروس جهان گرچه با زیور است</p></div>
<div class="m2"><p>به بین کاو بعقد بسی شوهر است</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>خرد پیشه کس خواستارش مشو</p></div>
<div class="m2"><p>که هر روزه اش شوهری هست نو</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چه نو بیند از کهنه برد امید</p></div>
<div class="m2"><p>خنک آنک از او دامن خود کشید</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>چه شد کشته گودرز کشوادگان</p></div>
<div class="m2"><p>مر آن پیر سر شیر آزادگان</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>خروش آمد و ناله نای و زنگ</p></div>
<div class="m2"><p>برفت از رخ مهر گردنده رنگ</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>فرستاد دستان فرخ کلاه</p></div>
<div class="m2"><p>که از چیست آن ناله پیش سپاه</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>فرستاده ای را فرستاد زود</p></div>
<div class="m2"><p>که تازان پژوهش کند همچه دود</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>سوار اندر آمد به پیش سپاه</p></div>
<div class="m2"><p>بدانست تا چیست آمد ز راه</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>بگفت این بدستان که گودرز پیر</p></div>
<div class="m2"><p>بشد کشته ای زال فرخ سریر</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>برآمد کنون کام تورانیان</p></div>
<div class="m2"><p>که شد کشته گودرز کشوادگان</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>چه بشنید دستان سام سوار</p></div>
<div class="m2"><p>ببارید از دیده خون بر کنار</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>برانگیخت چون شیر از پیش صف</p></div>
<div class="m2"><p>گران گرزه سام نیرم به کف</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>دمان پیش صف آمد آن پیل مست</p></div>
<div class="m2"><p>ز ترکان بسی کرد با خاک پست</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>ستمدیده گودرز را در ربود</p></div>
<div class="m2"><p>از افراز دارو بگردید زود</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>به لشکرگه آورد آن کشته را</p></div>
<div class="m2"><p>مر آن کشته خون برآغشته را</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>برآمد ز گردان ایران خروش</p></div>
<div class="m2"><p>چه دریا که از باد آید بجوش</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>به خیمه درآورد زالش ز راه</p></div>
<div class="m2"><p>بیامد همان گاه لهراسپ شاه</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>سر نامدارش به زانو نهاد</p></div>
<div class="m2"><p>برخ چشمه خون ز رخ برگشاد</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>سران سپه جمله افغان کشان</p></div>
<div class="m2"><p>همه اشک ریزان همه موکنان</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>ز تن جامه افکند یل ارده شیر</p></div>
<div class="m2"><p>ببارید اشک و بنالید دیر</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>بزاری همی گفت یل با نیا</p></div>
<div class="m2"><p>که زار و سپهدار و کند آورا</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>مرا گر چه بیژن نبودی پسر</p></div>
<div class="m2"><p>نیارا بدیدم بجای پدر</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>کنون بیتوام زندگانی چه سود</p></div>
<div class="m2"><p>که برخاک تیره سپهرت بسود</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>کجا گیو تا بندد از کین میان</p></div>
<div class="m2"><p>گشاید دوباز و بگرز گران</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>بجوید از این بی بنان کین تو</p></div>
<div class="m2"><p>سر خصمت آرد به بالین تو</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>کجا بیژن گیو لشکرشکن</p></div>
<div class="m2"><p>که کین تو جوید در این انجمن</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>به یزدان که تا کین نجویم ترا</p></div>
<div class="m2"><p>نه بندازم از تن زره ایدرا</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>ابا او جهان جوی فرخنده زال</p></div>
<div class="m2"><p>ز غم ناله کرد و خود و پرو بال</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>بسر دست بنهاد و لختی گریست</p></div>
<div class="m2"><p>سرانجام گیتی بجز گریه چیست</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>همی گفت زار ای سپهدار شیر</p></div>
<div class="m2"><p>دریغ از بر و بال گودرز پیر</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>ندیدم دگر باره رخسار تو</p></div>
<div class="m2"><p>دریغ از تو و کارکردار تو</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>دریغ از نشست و بر و افسرت</p></div>
<div class="m2"><p>که پر تیر کین بینم اکنون برت</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>تن نازکت کش ز گل بود عار</p></div>
<div class="m2"><p>کنون بینم از خار پیکان فکار</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>به نزدیک خسرو روانت رسید</p></div>
<div class="m2"><p>سرانجام زین ره زیانت رسید</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>تو رفتی و ما نیز آئیم بس</p></div>
<div class="m2"><p>تو پیشی در این راه ما خود ز بس</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>روان تو خرم چه خورشید باد</p></div>
<div class="m2"><p>به مینوی جان تو جاوید باد</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>وز آن پس کجا جسته بد دوختند</p></div>
<div class="m2"><p>برش عود و عنبر همی سوختند</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>به شستش کفن دوز از آب گلاب</p></div>
<div class="m2"><p>به تن بر همی ریخت از دیده آب</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>مر آن ریش کافور گون شانه کرد</p></div>
<div class="m2"><p>به تابوت جا آن یل از خانه کرد</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>یکی دخمه کردش روان زال زر</p></div>
<div class="m2"><p>ز خیمه به دخمه شد آن نامور</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>سرانجام گیتی چه این است و بس</p></div>
<div class="m2"><p>ازین جای زیر زمین است وبس</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>به بد تا توانی مکن رای هیچ</p></div>
<div class="m2"><p>به نیکان گرای و به نیکان بسیج</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>نه نیکی درو شرمساری بود</p></div>
<div class="m2"><p>که نیکی درو رستگاری بود</p></div></div>