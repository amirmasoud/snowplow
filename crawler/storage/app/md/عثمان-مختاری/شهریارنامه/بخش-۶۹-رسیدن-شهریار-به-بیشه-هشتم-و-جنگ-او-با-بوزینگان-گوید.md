---
title: >-
    بخش ۶۹ - رسیدن شهریار به بیشه هشتم و جنگ او با بوزینگان گوید
---
# بخش ۶۹ - رسیدن شهریار به بیشه هشتم و جنگ او با بوزینگان گوید

<div class="b" id="bn1"><div class="m1"><p>بدین منزل هشتم از روزگار</p></div>
<div class="m2"><p>چه بینم ایا نامدار سوار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بگفتا چه سر بر زند آفتاب</p></div>
<div class="m2"><p>ببینی یکی ژرف دریای آب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>که فیل از دمش کی گدا آورد</p></div>
<div class="m2"><p>کجا پیل را در چهار آورد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چه زان ژرف نیل ای یل نامدار</p></div>
<div class="m2"><p>به نیروی دادار آری گذار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یکی بیشه بینی چه خرم بهشت</p></div>
<div class="m2"><p>همه ساله آن بیشه اردی بهشت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در آن بیشه بوزینگانند بس</p></div>
<div class="m2"><p>که هریک چه شیر ژیانند بس</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>همه تیز دندان چه شیر نراند</p></div>
<div class="m2"><p>به نیرو ز شیران تواناترند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در این بیشه از بیم بوزینگان</p></div>
<div class="m2"><p>نیارد گذار اژدهای دمان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>فتد گر به چنگالشان شیر نر</p></div>
<div class="m2"><p>درین بیشه ای شیر پرخاشخور</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بدرند از هم چه شیران زوش</p></div>
<div class="m2"><p>چنان چونکه بد روز کین گربه موش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شگفتی سه چیز اندرین بیشه است</p></div>
<div class="m2"><p>که گم اندرین بیشه اندیشه هست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نخستین بود کان آهن ربا</p></div>
<div class="m2"><p>دگر سنگ یاقوت ای با بها</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>دگر کان پا زهر ای نامدار</p></div>
<div class="m2"><p>دراین بیشه باشد بهر سو هزار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>کس از بیم بوزینگان دلیر</p></div>
<div class="m2"><p>در این بیشه ناید بود گردلیر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>که با آهن از بیشه پر خطر</p></div>
<div class="m2"><p>نیارد کند باد صرصر گذر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>کنون بفکن از تن سلیح گران</p></div>
<div class="m2"><p>کزین بیشه نتوان شدن برگران</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>سپهدار گفتا که ای با خرد</p></div>
<div class="m2"><p>چنین گفته کای از شهان در خورد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>همانا که دل با منت صاف نیست</p></div>
<div class="m2"><p>که گفتار سرد تو جز لاف نیست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>به من بر نه گفتارت آمد درست</p></div>
<div class="m2"><p>که پیمان شکن بوده ای از نخست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بهربیشه کایم به ترسانیم</p></div>
<div class="m2"><p>نه بینی همی فر یزدانیم</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چه آهن بیندازم از چنگ دور</p></div>
<div class="m2"><p>چسان رزم سازم گه کین و شور</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>نباشد چه گرز گرانم بدست</p></div>
<div class="m2"><p>سردیو چون آرم از بر به پست</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>همان به که بر بندمت استوار</p></div>
<div class="m2"><p>چنین بسته باز آرم از کارزار</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بدو گفت جمهور کای شهریار</p></div>
<div class="m2"><p>بدان پاک یزدان پروردگار</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>که مهر و مه و آسمان آفرید</p></div>
<div class="m2"><p>گل و لاله و بوستان آفرید</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>که روشن روانم هوادار تست</p></div>
<div class="m2"><p>دل و جان شیرین خریدار تست</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>مرا همچو فرزندی از روزگار</p></div>
<div class="m2"><p>بترسم که گردد ترا کار زار</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بکوشم که برهانمت تن ز رنج</p></div>
<div class="m2"><p>که شیرین روان است نایاب گنج</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>پی دختری ره سپردن خطاست</p></div>
<div class="m2"><p>زنان را سر موی مردی بهاست</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>که گم باد نام زن اندر جهان</p></div>
<div class="m2"><p>چه گر پاریایست و روشن روان</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>چه زنگی شنید این چنین گفتگوی</p></div>
<div class="m2"><p>بدانست تا چیست از هر دو روی</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>روان جست در بیشه مانند باد</p></div>
<div class="m2"><p>بیامد دگر باره آن دیوزاد</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>کلیمی پر از سیر آورد پیش</p></div>
<div class="m2"><p>فرو ریخت در پیش آن پاک کیش</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>بر افراز سنگ آن زمان کوفت نرم</p></div>
<div class="m2"><p>بمالید بر ترک جوشنش گرم</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>به تیغ و به تیر و به بر گستوان</p></div>
<div class="m2"><p>بمالید سیر آن سیه در زمان</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>وزین پس چنین گفت برکش براه</p></div>
<div class="m2"><p>چه دید آن چنان پهلوان سپاه</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>به زنگی بگفت این چه تدبیر بود</p></div>
<div class="m2"><p>که بر جوشنش بازوی شیر بود</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>بدو گفت کآهن آیا نیک خواه</p></div>
<div class="m2"><p>از آهن ربا سیر دارد نگاه</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>برفتند شادان از آن جایگاه</p></div>
<div class="m2"><p>روان پیش اسپ سپهبد سیاه</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>چه شب از تن افکند کحلی پرند</p></div>
<div class="m2"><p>درفش شه خاوری شد بلند</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>رسیدند نزدیک آن رود آب</p></div>
<div class="m2"><p>بگاهی که سوزد به کوه آفتاب</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>سپهبد ندانست راه گذار</p></div>
<div class="m2"><p>فرو ماند بر جا یل نامدار</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>بزد دست زنجان و برداشتش</p></div>
<div class="m2"><p>ابا اسپ از آب بگذاشتش</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>همان کرد جمهور را با سه گرد</p></div>
<div class="m2"><p>بدان روی برد آن زمان هم چه گرد</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>بشد شاد از آن زنگی آن شیر مست</p></div>
<div class="m2"><p>بسر برش مالید آن گاه دست</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>ببوسید پایش هماندم سیاه</p></div>
<div class="m2"><p>نهادند زنجان سر سوی راه</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>چه آمد بدان بیشه آن پهلوان</p></div>
<div class="m2"><p>خبر شد بر شاه بوزینگان</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>بیامد ره بیشه را کرد تنگ</p></div>
<div class="m2"><p>بیاراست ره را به آئین جنگ</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>بدیدند بوزینه را صد هزار</p></div>
<div class="m2"><p>همه تیز دندان چه گرگ شکار</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>سپهبد چه آمد به نزدیکشان</p></div>
<div class="m2"><p>برو بر دویدند بوزینگان</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>سپهبد بزد دست برداشت گرز</p></div>
<div class="m2"><p>درآمد بکین و برافراشت برز</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>از ایشان همی کشت آن نامدار</p></div>
<div class="m2"><p>بگرز گران اندران کارزار</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>چه زنگی چنان دید مانند دود</p></div>
<div class="m2"><p>درآمد بکینه به ایشان فرود</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>دو تا و سه تا را گرفتی به چنگ</p></div>
<div class="m2"><p>گرفتی و خوردی ز کین روز جنگ</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>ازایشان چنان می دریدند بزور؟</p></div>
<div class="m2"><p>که شیر ژیان درگه کینه گور</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>گروهی ز بوزینگان نژند</p></div>
<div class="m2"><p>برفتند در دم بدار بلند</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>گروه دگر زو گریزان شدند</p></div>
<div class="m2"><p>چه موران به سوراخ خیزان شدند</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>چه گردید بیشه تهی از ددان</p></div>
<div class="m2"><p>برفتند مانند شیر ژیان</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>ز یاقوت و از سنگ آهن ربا</p></div>
<div class="m2"><p>زپازهر دیگر هم از کهربا</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>بسی برگرفتند و رفتند تیز</p></div>
<div class="m2"><p>از آن بیشه بیرون سری پرستیز</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>فرود آمد آن گاه آن کامیاب</p></div>
<div class="m2"><p>غنودند تا سرکشید آفتاب</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>به جمهور گفت ای شه پاک دین</p></div>
<div class="m2"><p>نهم منزلت چیست برگوی هین</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>بدو گفت ای کرد بارای نیو</p></div>
<div class="m2"><p>نهم منزلت هست مأوای دیو</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>ز دیوان چه زآن روی داری گذار</p></div>
<div class="m2"><p>بود ای سپهدار مغرب دیار</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>بکوه نخستین که در راه ماست</p></div>
<div class="m2"><p>بدو اندر آن جای بدخواه ماست</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>بود جای مأوای مضراب دیو</p></div>
<div class="m2"><p>کزو شهر مغرب بود در غریو</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>سراسر پر از دیو آن بیشه است</p></div>
<div class="m2"><p>که در چنگشان سنگ چون شیشه است</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>سراسر مطیع اند مضراب را</p></div>
<div class="m2"><p>ببردند در کین ز مه تاب را</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>یکی دیو وارونه سردارشان</p></div>
<div class="m2"><p>همه رهزنی هست کردارشان</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>مر آن دیو را نام سگسار شد</p></div>
<div class="m2"><p>که شیر از نهیبش در آزار شد</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>سرش بر طریق سر سگ بود</p></div>
<div class="m2"><p>ستمکاره و زشت و بد رگ بود</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>از ایشان دو بهره ز ملکم خراب</p></div>
<div class="m2"><p>شده نامور گرد با جاه آب</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>ازایشان چه بر گردی ای نامدار</p></div>
<div class="m2"><p>به بینی بناگه یکی کوهسار</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>یکی کوه بینی به غایت بلند</p></div>
<div class="m2"><p>که خور با سر او رود با کمند</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>چه سایه برش چرخ گردیده است</p></div>
<div class="m2"><p>بلندیش برتر ز اندیشه است</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>بکینش حریف نهنگ سپهر</p></div>
<div class="m2"><p>زره تیغ او تیغ بر ترک مهر</p></div></div>