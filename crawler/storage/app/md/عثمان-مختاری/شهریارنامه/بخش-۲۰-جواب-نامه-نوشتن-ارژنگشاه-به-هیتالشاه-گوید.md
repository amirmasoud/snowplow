---
title: >-
    بخش ۲۰ - جواب نامه نوشتن ارژنگشاه به هیتالشاه گوید
---
# بخش ۲۰ - جواب نامه نوشتن ارژنگشاه به هیتالشاه گوید

<div class="b" id="bn1"><div class="m1"><p>چنین پاسخ نامه گردید زود</p></div>
<div class="m2"><p>که جز رزم نبود دگر هیچ سود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو فردا سر از خواب دوشین کشم</p></div>
<div class="m2"><p>سپه بار دیگر بدین کین کشم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>درآیم به میدان هیتال شاه</p></div>
<div class="m2"><p>یکی رزم سازم درین کینه گاه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به بینیم تا بر که باشد سپهر</p></div>
<div class="m2"><p>سپهر بلند فروزنده مهر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فرستاده آن نامه بگرفت برد</p></div>
<div class="m2"><p>به نزدیک هیتال با وی سپرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>و زین روی ارژنگ فرمود کوس</p></div>
<div class="m2"><p>نوازند هنگام بانگ خروس</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>که فردا کمر کینه را بسته ام</p></div>
<div class="m2"><p>که از رزم هیتال دلخسته ام</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو فردا کشد تیغ تیز آفتاب</p></div>
<div class="m2"><p>غو کوس سازد تهی سر ز خواب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>یکی رزم سازیم در کینه گاه</p></div>
<div class="m2"><p>که از خون زند موج دریا به ماه</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بگفت این و شد زی سراپرده شاه</p></div>
<div class="m2"><p>بخوابید بر تخت آن نیکخواه</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سپهدار آمد به خرگاه باز</p></div>
<div class="m2"><p>بخوابید بر تخت زر سرفراز</p></div></div>