---
title: >-
    بخش ۶۷ - رسیدن شهریار به بیشه ششم و جنگ او با غولان گوید
---
# بخش ۶۷ - رسیدن شهریار به بیشه ششم و جنگ او با غولان گوید

<div class="b" id="bn1"><div class="m1"><p>شب تیره در بیشه راندند اسپ</p></div>
<div class="m2"><p>خروشان بکردار آذرگشسب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هوا سرد بود از دم زمهریر</p></div>
<div class="m2"><p>فرود آمد آنگه یل شیرگیر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بسی هیمه کردند از بیشه جمع</p></div>
<div class="m2"><p>دل خویش کردند ز اندیشه جمع</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همانگاه آتش برافروختند</p></div>
<div class="m2"><p>همه چشم دانش فرو دوختند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بیامد بناگه یکی مرد پیر</p></div>
<div class="m2"><p>به نزدیک آن پهلوان دلیر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بزاری همی گفت کای پهلوان</p></div>
<div class="m2"><p>یکی مرد پیرم بدل ناتوان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یکی پور بودم جوان و ملول</p></div>
<div class="m2"><p>ربودش ز من اندرین شهر غول</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کنون هستم اندر پناه شما</p></div>
<div class="m2"><p>نمایم در این بیشه راه شما</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نمایند این بیشه ایدر به پای</p></div>
<div class="m2"><p>که نبود در این بیشه آرام جای</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>که هستم بسی دل از اینجا ملول</p></div>
<div class="m2"><p>که هست این همه بیشه مأوای غول</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سپهدار گفتا به جمهور شاه</p></div>
<div class="m2"><p>کزیدر بیا تا بگیریم راه</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بدو گفت جمهور کای سرفراز</p></div>
<div class="m2"><p>خرد را بکن چشم دانش فراز</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>همانا که غولست این ناسزا</p></div>
<div class="m2"><p>که زینگونه آمد به نزدیک ما</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ورا گر توانی بیاور بدست</p></div>
<div class="m2"><p>وگرنه بما غول آرد شکست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>جهان جوی برداشت از سفره نان</p></div>
<div class="m2"><p>بدان غول گفتا بیا و ستان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بشد پیش تا نان ستاند از اوی</p></div>
<div class="m2"><p>گرفتش سر و دست آن جنگجوی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ببست آن زمانش جهان جوی دست</p></div>
<div class="m2"><p>به پایش نگه کرد آن شیر مست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چه خر بود پایش پر از موی و سم</p></div>
<div class="m2"><p>چه خر داشت سم و چه خر داشت دم</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>سرش خواست از تن ببرد روان</p></div>
<div class="m2"><p>یکی نعره زد غول تیره روان</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ز هر سوی او غول آمد هزار</p></div>
<div class="m2"><p>چه دید آن چنان گرد خنجرگزار</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ز غولان فرو ماند اندر عجب</p></div>
<div class="m2"><p>ز بس غول دید اندر آن تیره شب</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>همه نره غولان بالا بلند</p></div>
<div class="m2"><p>که برقد ایشان نبودی کمند</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>سراسر تنان شان پر از موی بود</p></div>
<div class="m2"><p>همه بیشه زیشان پر از بوی بود</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>برون آمد از بیشه پانصد هزار</p></div>
<div class="m2"><p>بدینگونه غولان با گیر و دار</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ستادند در گرد ایشان همه</p></div>
<div class="m2"><p>مر آن نره غولان خروشان همه</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>مرآن غول کآن شیر نر بسته بود</p></div>
<div class="m2"><p>بدان بستگی جان او خسته بود</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>قضا را که آن شاه غولان بدی</p></div>
<div class="m2"><p>که از بهر او غول نالان بدی</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>سپهدار را گفت آن نره غول</p></div>
<div class="m2"><p>کازین لشکر من نباشی ملول</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>منم شاه غولان درین بیشه در</p></div>
<div class="m2"><p>رها کن مرا ای یل نامور</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>که تا این سپه را برم از برت</p></div>
<div class="m2"><p>کسی کشته ناید از این لشکرت</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>سپهدار گفتا که از رای خویش</p></div>
<div class="m2"><p>مبادا بگردی بد آری به پیش</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>بگفتا به خورشید تابان قسم</p></div>
<div class="m2"><p>که ازکین نیارم برت با دودم</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>سپهدار گفتا به یزدان پاک</p></div>
<div class="m2"><p>کاز کین نسازم ترا من هلاک</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>سپهدار چون نام یزدان ببرد</p></div>
<div class="m2"><p>فروغ از دل نره غولان ببرد</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>دراندیشه آن نره غولان شدند</p></div>
<div class="m2"><p>گریزنده از نام یزدان شدند</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>مرآن غول کش بود در بند پای</p></div>
<div class="m2"><p>بمرد او چه بشنید نام خدای</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>نبد پا که بگریزد از بند گرد</p></div>
<div class="m2"><p>ز بیم یل نام یزدان بمرد</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>چنین تا سر از پرده برداشت شید</p></div>
<div class="m2"><p>نیامد ز غولان دگر خود پدید</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>سپهبد همی بر(د) نام خدای</p></div>
<div class="m2"><p>خداوند روزی ده و رهنمای</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>بدانست کز نام یزدان پاک</p></div>
<div class="m2"><p>مر ان نره غولان شد آندم هلاک</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>بر اتش نهاد و تنش را بسوخت</p></div>
<div class="m2"><p>دگر باره چشم خرد را بدوخت</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>زبوی بدش باز غولان همه</p></div>
<div class="m2"><p>رسیدند چون نره شیران همه</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>سپهبد دگر برد نام خدای</p></div>
<div class="m2"><p>دگر باره رفتند غولان ز جای</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>سپهبد به جمهور گفتا که هین</p></div>
<div class="m2"><p>بسیج سفر کن بزین برنشین</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>بزین بر نشستند رفتند شاد</p></div>
<div class="m2"><p>ازآن بیشه دلشاد و خرم چه باد</p></div></div>