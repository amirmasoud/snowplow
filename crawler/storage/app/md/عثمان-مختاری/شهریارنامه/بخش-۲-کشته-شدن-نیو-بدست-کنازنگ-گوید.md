---
title: >-
    بخش ۲ - کشته شدن نیو بدست کنازنگ گوید
---
# بخش ۲ - کشته شدن نیو بدست کنازنگ گوید

<div class="b" id="bn1"><div class="m1"><p>زمین را بخون لعل گون ساختند</p></div>
<div class="m2"><p>تو گفتی فلک را نگون ساختند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کنازنگ زوبین هندی گرفت</p></div>
<div class="m2"><p>بر نیو آمد چو دیو شکفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چنان زدش بر سینه زخم درشت</p></div>
<div class="m2"><p>چه زوبین برون رفت بازاو پشت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چه کوه از بر اسب غلطید نیو</p></div>
<div class="m2"><p>به میدان روان شد کنازنگ دیو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چه شد کشته نیو اندران رزمگاه</p></div>
<div class="m2"><p>سپاهش گریزان بشد پیش شاه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شنیدند آن نامدارن ازوی</p></div>
<div class="m2"><p>برفتند پیش یل جنگجوی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بگفتند توپال خنجر گزار</p></div>
<div class="m2"><p>پیام آوریده بر شهریار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چه فرمان دهد سرور انجمن</p></div>
<div class="m2"><p>بگفتا کش آرید نزدیک من</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ببردند آنرا بر شاه نو</p></div>
<div class="m2"><p>چه توپال آمد بر شاه کو</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زمین را ببوسید گفتا بدوی</p></div>
<div class="m2"><p>که ای نامورشاه آزاده خوی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>درودت رسانم بهیتال شاه</p></div>
<div class="m2"><p>همی گویدت ای سرافرازگاه</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دو هفته است کاشوب جوئیم و جنگ</p></div>
<div class="m2"><p>پی کین بهر سوی بربسته تنگ</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>دمی پهلوانان نیاسوده اند</p></div>
<div class="m2"><p>یکی جام با هم نپیموده اند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نه با کام دل هیچ پرداختند</p></div>
<div class="m2"><p>نه یک لحظه خوان خورش ساختند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نه از بستر خواب یکدم غنود</p></div>
<div class="m2"><p>نه کس از میان تیغ کین برگشود</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>سپه را چنین جام می آرزوست</p></div>
<div class="m2"><p>اگرچه بسی را شما تندخوست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>دو هفته کنون رامش آریم و جام</p></div>
<div class="m2"><p>وز آن پس بکوشیم از بهرنام</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چه بشنید گفتار مرد دلیر</p></div>
<div class="m2"><p>بدو گفت کای مرد برنای خیر</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ز من شاه را گوی پاسخ چنین</p></div>
<div class="m2"><p>که هر چند خواهی تو رامش گزین</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>به چنگ از سر کار خواهی سپاه</p></div>
<div class="m2"><p>نپیچیم ما سر ز فرمان شاه</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چه آن گفته بشنید توپال زود</p></div>
<div class="m2"><p>بر شاه هیتال آمد چه دود</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بیامد بر شاه و آن باز گفت</p></div>
<div class="m2"><p>چه بشنید هیتال این درشگفت</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بفرمود تا مؤبد آمد به پیش</p></div>
<div class="m2"><p>نشاندش به نزدیکی تخت خویش</p></div></div>