---
title: >-
    بخش ۱۰۹ - داستان خلاص شدن شهریار از بند فرانک گوید
---
# بخش ۱۰۹ - داستان خلاص شدن شهریار از بند فرانک گوید

<div class="b" id="bn1"><div class="m1"><p>کنون ای سراینده داستان</p></div>
<div class="m2"><p>ز گفتار دهقان روشن روان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مر این رزمگه ایدر اکنون بدار</p></div>
<div class="m2"><p>سخن گستر از نامور شهریار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>که کردش فرانک به بند اندرون</p></div>
<div class="m2"><p>بگویم کنون تا که شد کار چون</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جهان جوی هشت ماه در بند ماند</p></div>
<div class="m2"><p>که در دیده جز اشک خونین نراند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر این هشت مه بود آشوب و جنگ</p></div>
<div class="m2"><p>ز لشکر بدی دشت ناورد تنگ</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گهی در سر اندیب و گه در سرند</p></div>
<div class="m2"><p>ز بند جهان جوی مگشاد بند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دلارام گفتا بگردان خویش</p></div>
<div class="m2"><p>که آن زن بما مکر آورد پیش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کنون من هم از مکر کاری کنم</p></div>
<div class="m2"><p>که اندر جهان یادگاری کنم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به مکر و تزویرش آرم بدست</p></div>
<div class="m2"><p>که جز مکر وی را نباید شکست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به آئین بازارگا(نا)ن لباس</p></div>
<div class="m2"><p>بپوشید و شد با هزاران سپاس</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چهل اشتر از لعل و در بار کرد</p></div>
<div class="m2"><p>همی نام خود قهر تجار کرد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ز مردان دو صد گرد با خویش برد</p></div>
<div class="m2"><p>شترها به زنجان زنگی سپرد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به سوی سراندیب برداشت راه</p></div>
<div class="m2"><p>از اینگونه آن ماه شد کینه خواه</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>شبی بود در خیمه آن ماه شاد</p></div>
<div class="m2"><p>که مضراب دیو اندر آمد چو باد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>که شاید رباید مه زاد را</p></div>
<div class="m2"><p>به بند آورد سرو آزاد را</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>شد آگاه از آن دیو آن سیمبر</p></div>
<div class="m2"><p>بزد دست و خنجر کشید از کمر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چو آمد به نزدیک او نره دیو</p></div>
<div class="m2"><p>بزد تیغ بانوی با رای و نیو</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بینداخت و بگذاشت او را بتن</p></div>
<div class="m2"><p>ز پیش پری شد نهان اهرمن</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>سحرگه از آنجای بربست بار</p></div>
<div class="m2"><p>شتر کرد زنجان زنگی قطار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چنین تا که منزل سراندیب شد</p></div>
<div class="m2"><p>جهانی پر از زینت و زیب شد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>به دشت سراندیب آمد فرود</p></div>
<div class="m2"><p>بزد خیمه خویش نزدیک رود</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>صد از نامداران شمشیر زن</p></div>
<div class="m2"><p>که در رزم بودند چون اهرمن</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>سپرد آن دلاور بزنکی زوش</p></div>
<div class="m2"><p>که بنشین کمین را و بگشای گوش</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چو آواز شیپور من بشنوی</p></div>
<div class="m2"><p>نباید که بر جایگه بغنوی</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بیا با دلیران خنجر گذار</p></div>
<div class="m2"><p>به نزدیک من درگه کارزار</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>به شد گرد زنجان و شد در کمین</p></div>
<div class="m2"><p>چنان چونکه بر گور شیر عرین</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>خبر شد سراندیب یانرا که باز</p></div>
<div class="m2"><p>بیامد یکی خواجه سرفراز</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>ورا بار یکسر جواهر بود</p></div>
<div class="m2"><p>جواهر شناس است و ماهر بود</p></div></div>