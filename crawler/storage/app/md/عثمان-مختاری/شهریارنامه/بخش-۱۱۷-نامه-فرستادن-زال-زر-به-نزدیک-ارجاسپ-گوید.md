---
title: >-
    بخش ۱۱۷ - نامه فرستادن زال زر به نزدیک ارجاسپ گوید
---
# بخش ۱۱۷ - نامه فرستادن زال زر به نزدیک ارجاسپ گوید

<div class="b" id="bn1"><div class="m1"><p>چه خوابید در دخمه گودرز پیر</p></div>
<div class="m2"><p>برآمد خروش از یلان دلیر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نزاد است گفتی مگر مادرش</p></div>
<div class="m2"><p>ندید است گیتی سر و افسرش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فرستاد کس پیش ارجاسپ زال</p></div>
<div class="m2"><p>که ای ترک بد طینت و بدسکال</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چه بود آنکه کردی در این کینه گاه</p></div>
<div class="m2"><p>نبد شرمت از داور هور و ماه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چرا کشتی این پیر فرتوده را</p></div>
<div class="m2"><p>جهان دیده و دهر پیموده را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نه زین کشتن ایوان شود زآن تو</p></div>
<div class="m2"><p>که نفرین بد باد بر جان تو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نه مردم نخواهم اگر کین اوی</p></div>
<div class="m2"><p>از آن ترک بدگوهر کینه جوی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کنون باش آماده جنگ من</p></div>
<div class="m2"><p>که بینی از این پرهنر چنگ من</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>که فردا چه خورشید خنجر کشد</p></div>
<div class="m2"><p>سر زنگئی شب به خون درکشد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بگویم جهان جو فرامرز را</p></div>
<div class="m2"><p>که سازد ز خون رود این مرز را</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چه گر نیست در بیشه شیر ژیان</p></div>
<div class="m2"><p>به کین بسته دارد پلنگی میان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دلیران چو صف برکشند از دو روی</p></div>
<div class="m2"><p>برآید ز هر دو سپه گفتگوی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به میدان کین رزم آن من است</p></div>
<div class="m2"><p>کازین غم در آتش روان من است</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>پر آرم چه آرم بناورد گرز</p></div>
<div class="m2"><p>نمایم بدین پیره سر یال و برز</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>به بینی که دستان سام سوار</p></div>
<div class="m2"><p>چسان با دلیران کند کارزار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>فرستاده رفت این به ارجاسپ گفت</p></div>
<div class="m2"><p>بخندید و ارجاسپ شد در شگفت</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بفرمود کان مرد را یوزبان</p></div>
<div class="m2"><p>سر از تن ببرند اندر زمان</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>وز آن پس بفرمود تا کوس جنگ</p></div>
<div class="m2"><p>زدند و به بستند بر بور تنگ</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>دلیران کمر کینه را استوار</p></div>
<div class="m2"><p>پی رزم کردند مردانه وار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>وزین روی دستان چاگاه شد</p></div>
<div class="m2"><p>که گرد سپه باز بر ماه شد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بزد کوس و بر باره کین نشست</p></div>
<div class="m2"><p>کمر تنگ و گرز گرانش بدست</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>دو لشکر چه دریا به جوش آمدند</p></div>
<div class="m2"><p>به میدان کین رزم کوش آمدند</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>صف کین ز هر دو طرف راست شد</p></div>
<div class="m2"><p>که دل در تن کوه در کاست شد</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ز نالیدن نای جنبید کوه</p></div>
<div class="m2"><p>زمین کوه گردید از بس گروه</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>فرامرز آمد به پیش نیا</p></div>
<div class="m2"><p>چنین گفت کای گرد فرمانروا</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>من امروز گز خواب برخاستم</p></div>
<div class="m2"><p>ز یزدان دادار این خواستم</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>که کین جهان دیده گودرز پیر</p></div>
<div class="m2"><p>بخواهم از این دشت ناورد چیز</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>چنین گفت زالش که مردانه باش</p></div>
<div class="m2"><p>برزم اندرون گرد فرزانه باش</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>چنانم امید است ز یزدان پاک</p></div>
<div class="m2"><p>که دشمنت را سر درآرد به خاک</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>فرامرز پوشید گبر نبرد</p></div>
<div class="m2"><p>برانگیخت باد و برآورد گرد</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>به میدان کین آمد از پیش صف</p></div>
<div class="m2"><p>گران گرزه گاو پیکر به کف</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>به دشنام ارجاسپ را برشمرد</p></div>
<div class="m2"><p>بدان پس هماورد خود خواست گرد</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>کمان کرد ارجاسپ کاین نامور</p></div>
<div class="m2"><p>بود گرد دستان فرخنده فر</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>بپوشید ارجاسپ ساز نبرد</p></div>
<div class="m2"><p>نشست از برباره ره نورد</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>برانگیخت که کوب سرکش ز جای</p></div>
<div class="m2"><p>برآمد خروشیدن کره نای</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>کمر تنگ و در دست گرز گران</p></div>
<div class="m2"><p>به آوردگه رفت ترک دمان</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>که در کینه گه آمد آن بدسکال</p></div>
<div class="m2"><p>بدانست کاو نیست فرخنده زال</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>فرامرز را گفت برگوی نام</p></div>
<div class="m2"><p>زگردان که واز دلیران کدام</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>فرامرز دانست که ارجاسپ اوست</p></div>
<div class="m2"><p>ستیزنده بر جان لهراسپ اوست</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>چنین داد پاسخ بدو بدسکال</p></div>
<div class="m2"><p>نبیره جهان جوی فرخنده زال</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>فرامرز پور یل تاج بخش</p></div>
<div class="m2"><p>تهمتن خداوند کوپال و رخش</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>تو ایران ز رستم تهی یافتی</p></div>
<div class="m2"><p>که زی مرز ایران عنان تافتی</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>ندانی که دربیشه باشد پلنگ</p></div>
<div class="m2"><p>تهی نیست بیشه ز شیران جنگ</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>بگفت این و برداشت آن یل سنان</p></div>
<div class="m2"><p>برآمد به ارجاسپ اندر زمان</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>چه ارجاسپ گشت از سنانش ستوه</p></div>
<div class="m2"><p>کشید از میان تیغ و آمد چه کوه</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>سپهبد برآورد آتش ز دود</p></div>
<div class="m2"><p>ز جا باز سرکش برانگیخت زود</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>همی دید ازکینه گه زال زر</p></div>
<div class="m2"><p>به نزد سرافراز پرخاشخور</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>بدست دو یل تیغ تارک شکاف</p></div>
<div class="m2"><p>نمایان چه برق از سر کوه قاف</p></div></div>