---
title: >-
    بخش ۸۷ - رزم سام با ارهنگ دیو و گرفتار شدن سام گوید
---
# بخش ۸۷ - رزم سام با ارهنگ دیو و گرفتار شدن سام گوید

<div class="b" id="bn1"><div class="m1"><p>بدو گفت ارهنگ کای نامدار</p></div>
<div class="m2"><p>چه نامی ز گردان زابل دیار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مرا نام گفتا بود سام شیر</p></div>
<div class="m2"><p>که گیرد کمندم بکین دم شیر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فرامرز رستم بود باب من</p></div>
<div class="m2"><p>ندارد هژیر ژیان تاب من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بدو گفت ارهنگ کای ارجمند</p></div>
<div class="m2"><p>بخواهم ز تو خون پولادوند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کزین تخمه شوم بر باد شد</p></div>
<div class="m2"><p>چه بر زی رزم پولاد شد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کمان را بزه کرد او استوار</p></div>
<div class="m2"><p>بسام اندر آمد چه ابر بهار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کمان نیز بر زه روان سام کرد</p></div>
<div class="m2"><p>برآمد ز میدان ناورد گرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بهم بر ز کین تیر کین آختند</p></div>
<div class="m2"><p>چه شیر اندر آن رزم می تاختند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چه ترکش تهی شد ز تیر خدنگ</p></div>
<div class="m2"><p>بیازید ارهنگ از کینه چنگ</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کمربند سام دلاور گرفت</p></div>
<div class="m2"><p>ربودش ز پشت تکاور شگفت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بزیر کش آوردش آن اهرمن</p></div>
<div class="m2"><p>ببردش ز میدان روان اهرمن</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>سپردش به دیوان و آمد دوان</p></div>
<div class="m2"><p>به میدان کین دیو تیره روان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>روان مرزبان رفت برساخت کار</p></div>
<div class="m2"><p>برآراست با دیو نر کارزار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بیازید آن دیو واژونه چنگ</p></div>
<div class="m2"><p>ورا نیز کند از فراز خدنگ</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>سپردش به دیوان و آمد چو شیر</p></div>
<div class="m2"><p>به میدان کین دیو وارون دلیر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>تخاره به میدان او رفت شاد</p></div>
<div class="m2"><p>ورا نیز بربود و بردش چه باد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>زواره چه زآن دیو آن ضربه دید</p></div>
<div class="m2"><p>بزردی رخش گشت چون شنبلید</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چه بگریخت خورشید تابان ز شب</p></div>
<div class="m2"><p>زمانه ز گفتار بربست لب</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>زواره بگردید از آوردگاه</p></div>
<div class="m2"><p>چه ارهنگ ازین رو به دیگر سپاه</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>زواره یکی نامه زی زال کرد</p></div>
<div class="m2"><p>به نامه درون شرح احوال کرد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>فرستاد خورشید را خسته نیز</p></div>
<div class="m2"><p>سوی سیستان در شب تیره نیز</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>وزین روی ارهنگ دیو دمند</p></div>
<div class="m2"><p>مر آن هر سه یل را بخم کمند</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>فرستاد نزدیک ارجاسپ شاه</p></div>
<div class="m2"><p>سوی بلخ با چند مرد از سپاه</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چه نامه بر زال نیرم رسید</p></div>
<div class="m2"><p>سر شکش ز مژگان برخ برچکید</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>نوشته که ای باب فرخنده رای</p></div>
<div class="m2"><p>چو نامه بخوانی بپرداز جای</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بنه سوی هندوستان کن روان</p></div>
<div class="m2"><p>که بر ما سر آمد همانا زمان</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>دو پور مرا با جهان جوی سام</p></div>
<div class="m2"><p>به نیروی بگرفت آن تیره فام</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>هم آورد ارهنگ در جنگ نیست</p></div>
<div class="m2"><p>گه کین دلیری چه ارهنگ نیست</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>سطبرش دو بازوی و یال بلند</p></div>
<div class="m2"><p>تو گوئی که شد زنده پولادوند</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>گذشته ز رستم گه گیر و دار</p></div>
<div class="m2"><p>نباشد چو خورشید مینو سوار</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>ز خورشید مینو بپرس این سخن</p></div>
<div class="m2"><p>که چونست در رزم آن اهرمن</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>چو شیر است در رزم آهنگ او</p></div>
<div class="m2"><p>ندارد کسی تاب در جنگ او</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>نه اینجا فرامرز و نه رستم است</p></div>
<div class="m2"><p>همه شادمانی کنون ماتم است</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>تو پیری و نبود تو را تاب جنگ</p></div>
<div class="m2"><p>جوان است این دیو فیروز چنگ</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>چه پیری کند مرد را پایمال</p></div>
<div class="m2"><p>چسان گرز کین رابرآرد بیال</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>که من چون درآید سحرگاه خور</p></div>
<div class="m2"><p>به بندم پی کینه او کمر</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>یکی رزم سازم به ارهنگ دیو</p></div>
<div class="m2"><p>کمر تنگ سازم پی جنگ دیو</p></div></div>