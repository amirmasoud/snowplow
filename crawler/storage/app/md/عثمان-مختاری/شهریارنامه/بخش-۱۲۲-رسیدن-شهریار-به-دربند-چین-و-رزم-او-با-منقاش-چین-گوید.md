---
title: >-
    بخش ۱۲۲ - رسیدن شهریار به دربند چین و رزم او با منقاش چین گوید
---
# بخش ۱۲۲ - رسیدن شهریار به دربند چین و رزم او با منقاش چین گوید

<div class="b" id="bn1"><div class="m1"><p>چه کشتیش آمد به دریای چین</p></div>
<div class="m2"><p>برون آمد از کشتی آن گرد کین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بزد خیمه در پیش دریا کنار</p></div>
<div class="m2"><p>جهان جوی شیراوژن نامدار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همه مرز چین برهم آمد ازین</p></div>
<div class="m2"><p>که آمد سپاهی به دربند چین</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چه بشنید خاقان یکی لشکری</p></div>
<div class="m2"><p>فرستاد در دم بکین داوری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>که گر رزم جویند رزم آورند</p></div>
<div class="m2"><p>مبادا که این بوم و بر بسپرند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سپهبد یکی ترک منقاش نام</p></div>
<div class="m2"><p>سپه برد بیرون ز چین کاه شام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دوره شش هزار ازیلان دلیر</p></div>
<div class="m2"><p>بهمراه آن ترک آمد چو شیر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چونزد سپاه سپهبد رسید</p></div>
<div class="m2"><p>بزد کوس و صف بست و لشکر کشید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>فرستاد مردی هم اندر زمان</p></div>
<div class="m2"><p>به پیش سپهدار روشن روان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کازین آمدن کام و رای تو چیست</p></div>
<div class="m2"><p>کئی و چه نامی درای تو چیست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>که این مرز چین است و شیران چین</p></div>
<div class="m2"><p>بهر بیشه دارنده ره در کمین</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به توران اگر برد خواهی سپاه</p></div>
<div class="m2"><p>تو را نیست زین مرز خونکاره راه</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>براه دگر سوی توران خرام</p></div>
<div class="m2"><p>مبادا که آید سرت زیر دام</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>وگر جنگجو سوی چین آمدی</p></div>
<div class="m2"><p>بگام نهنگان کین آمدی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بگو تا ببندم کمر بهر جنگ</p></div>
<div class="m2"><p>یکی برگشایم ازین کین دو چنگ</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>که منقاش چینی بود نام من</p></div>
<div class="m2"><p>سر شیر جنگی است در دام من</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>فرستاده رفت و بیامد چو باد</p></div>
<div class="m2"><p>سپهدار پاسخ چنین باز داد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>که با چین میانم نباشد نبرد</p></div>
<div class="m2"><p>به توران سپه برد خواهم چه گرد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>یکی راه بگشای تا بگذرم</p></div>
<div class="m2"><p>سپه را ز چین سوی توران برم</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>وگرنه چه برگرز دست آورم</p></div>
<div class="m2"><p>به ترکان چین بر شکست آورم</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>فرستاد برگشت چون باد زود</p></div>
<div class="m2"><p>بگفت آنکه از گرد بشنیده بود</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چو بشنید منقاش آمد بجوش</p></div>
<div class="m2"><p>برزم اندر آمد چو شیران زوش</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>میان سپاه اندر آواز داد</p></div>
<div class="m2"><p>که ای هندی تیره بدنژاد</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>همی راه جوئی ز شیران چین</p></div>
<div class="m2"><p>که رانی سپه سوی توران زمین</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>گرت راه باید کنون زین سپاه</p></div>
<div class="m2"><p>ز شیران تهی کن به شمشیر راه</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>سپهبد چه بشنید برکرد اسب</p></div>
<div class="m2"><p>خروشید برسان آذر گشسب</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بگردان چین گفت یک تن عنان</p></div>
<div class="m2"><p>به پیچید بدین کین رزم آوران</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>که تنها بسم چینیان را به جنگ</p></div>
<div class="m2"><p>به گفت این و آمد کمر کرده تنگ</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بمنقاش بربست ره استوار</p></div>
<div class="m2"><p>خروشان ابرسان ابر بهار</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>کمان گرد بر زه ستمکاره مرد</p></div>
<div class="m2"><p>برآویخت با شیر اندر نبرد</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>بگرد سپهبد ببارید تیر</p></div>
<div class="m2"><p>چنان چون که از ابر زی زمهریر</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>سپهبد چه در زیر پولاد بود</p></div>
<div class="m2"><p>خدنگش به پولاد چون باد بود</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>بزد دست و برداشت گرز گران</p></div>
<div class="m2"><p>درآمد به تنگ اندرش پهلوان</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>فرو کوفت برترک آن ترک گرز</p></div>
<div class="m2"><p>که شد ترک را نام با ترک برز</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>برآورد گرزش ز منقارش گرد</p></div>
<div class="m2"><p>چنان چون سرش را به پرخاش کرد</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>وز آن پس برآورد گرز گران</p></div>
<div class="m2"><p>درآمد برآن لشکر چینییان</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>بهرسوی کآن شیر برکاشتی</p></div>
<div class="m2"><p>ز خون لاله در دشت چین کاشتی</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>سرو ترک می کرد با ترک نرم</p></div>
<div class="m2"><p>کجا آختی دست با گرز گرم</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>چو ترکان بدیدند رزم درشت</p></div>
<div class="m2"><p>ز پیش سپهبد نمودند پشت</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>سپهدار برگشت از رزمگاه</p></div>
<div class="m2"><p>بیامد به نزدیک جمهور شاه</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>به جمهور گفت کای شه کامکار</p></div>
<div class="m2"><p>یک امروز داریم رای شکار</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>چو زی صیدگه بازگردم به کام</p></div>
<div class="m2"><p>به پیچم سوی شهر خاقان لگام</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>گمانم که خاقان سپاه آورد</p></div>
<div class="m2"><p>که بر مایکی تنگ راه آورد</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>بگفت و به نخجیر آورد روی</p></div>
<div class="m2"><p>بکردار شیران نخجیر جوی</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>به دشتی کجا جای نخجیر بود</p></div>
<div class="m2"><p>بدان که نشیمنگه شیر بود</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>یکی سهمگین نره شیر ژیان</p></div>
<div class="m2"><p>بدآن دشت نخجیر بودش مکان</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>خروشید مانند شیر سپهر</p></div>
<div class="m2"><p>به پیچید از جنگ او دیو چهر</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>چه گردان به نخجیر در تاختند</p></div>
<div class="m2"><p>گذر سوی آن شیر نر ساختند</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>چو شیر ژیان دیدشان در ستیز</p></div>
<div class="m2"><p>برافروخت آتش ز دندان تیز</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>میان سواران درافتاد شیر</p></div>
<div class="m2"><p>بسی را ز بالا برآورد زیر</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>سواران نهادند رخ در گریز</p></div>
<div class="m2"><p>نبد شان چو با شیر جای ستیز</p></div></div>