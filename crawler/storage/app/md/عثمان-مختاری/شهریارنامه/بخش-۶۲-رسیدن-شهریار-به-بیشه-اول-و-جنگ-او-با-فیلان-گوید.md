---
title: >-
    بخش ۶۲ - رسیدن شهریار به بیشه اول و جنگ او با فیلان گوید
---
# بخش ۶۲ - رسیدن شهریار به بیشه اول و جنگ او با فیلان گوید

<div class="b" id="bn1"><div class="m1"><p>به جمهور گفت ای شه پاکزاد</p></div>
<div class="m2"><p>پس پشت من زانکه دارد نژاد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو آمد بدان بیشه آن نامدار</p></div>
<div class="m2"><p>یکی پیل آمد بر شهریار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سری همچو گنبد تنی همچو کوه</p></div>
<div class="m2"><p>زمین زیر پایش بدی در ستوه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>برون از دهانش که دندان بدی</p></div>
<div class="m2"><p>که پیشش بکین موم سندان بدی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سوی نامور همچو ابر بلای</p></div>
<div class="m2"><p>برآورد خرطوم چون اژدهای</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سپهبد برآورد گرز گران</p></div>
<div class="m2"><p>بزد برسر پیل جنگی روان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کش از سر فرو ریخت مغزش به خاک</p></div>
<div class="m2"><p>سپهبد به یک گرز کردش هلاک</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سرش چون فرو ریخت آن نیکرای</p></div>
<div class="m2"><p>درآمد چه کوهی همان دم ز جای</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دگر باره آمد یکی نره پیل</p></div>
<div class="m2"><p>چنان کآید از کوه دریای نیل</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>برآن حمله آورد چون کوه تند</p></div>
<div class="m2"><p>نشد گرد را دست شمشیر کند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به پشتش چنان کوفت گرز درشت</p></div>
<div class="m2"><p>که در ناف او ریخت مهره ز پشت</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>یکی دیگر آمد سوی یل به جنگ</p></div>
<div class="m2"><p>جهانجوی خرطوم او را به چنگ</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گرفت و ز سرکردش اندرزمان</p></div>
<div class="m2"><p>درآمد ز پا پیل جنگی دمان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>رسیدند پیلان گروه ها گروه</p></div>
<div class="m2"><p>تو گفتی که از پیل شد بیشه کوه</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>هم اندر زمان گرزه گاوسر</p></div>
<div class="m2"><p>برآورد زی پیل شد شیر نر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ز پیلان دو صد فیل جنگی بکشت</p></div>
<div class="m2"><p>دگر پیلکان جمله دادند پشت</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ره بیشه از فیل پرداخت یل</p></div>
<div class="m2"><p>ز فیلان همه دشت را ساخت تل</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چو از فیل جنگی برآورد گرد</p></div>
<div class="m2"><p>بزد اسب رخ بر سوی راه کرد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>از آن بیشه شیران برون تاختند</p></div>
<div class="m2"><p>مر آن بیشه را از پس انداختند</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>جهان جوی آمد همانگه فرود</p></div>
<div class="m2"><p>رخ خویش بر خاک تیره بسود</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>سپاس جهان آفرین کرد یاد</p></div>
<div class="m2"><p>که بیرون از آن بیشه آمد چه باد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>به جمهور گفت آن زمان نامدار</p></div>
<div class="m2"><p>درخت امیدت برآورد بار</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>از آن بیشه شیران برون تاختند</p></div>
<div class="m2"><p>مر آن بیشه را از پس انداختند</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>از این بیشه اندیشه کوتاه شد</p></div>
<div class="m2"><p>ز پیلان چو کوه آن زمان راه شد</p></div></div>