---
title: >-
    بخش ۹۸ - گریختن لهراسپ از بلخ و گرفتار آمدن گودرز گوید
---
# بخش ۹۸ - گریختن لهراسپ از بلخ و گرفتار آمدن گودرز گوید

<div class="b" id="bn1"><div class="m1"><p>چنین تاز که خور برآمد بلند</p></div>
<div class="m2"><p>ستمکاره ترکان به غارت بدند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زن ومرد یکسر برافراز بام</p></div>
<div class="m2"><p>اجل تیغ کین برکشید از نیام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دلیران بلخی گشادند چنگ</p></div>
<div class="m2"><p>به ترکان نهادند شمشیر و سنگ</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چنان فتنه ای در سر بلخ شد</p></div>
<div class="m2"><p>که از بیم خور چون مه سلخ شد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز بس تیغ کین ریخت در شهر خون</p></div>
<div class="m2"><p>همه کوچه ها گشت شنجرف گون</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بدین گونه تا گشت خورشید راست</p></div>
<div class="m2"><p>بدان فتنه از بلخ از چپ و راست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز ترکان ارجاسپ با ده هزار</p></div>
<div class="m2"><p>بشد کشته هر سوی در بلخ زار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در آخر چه دانست هر کس که شاه</p></div>
<div class="m2"><p>برون رفت از بلخ و از رزمگاه</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دل دست اینان برون شد ز کار</p></div>
<div class="m2"><p>تهی کرد هرکس سر از کار زار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بکشتند ترکان فزون از شمار</p></div>
<div class="m2"><p>زن و مرد بلخی در آن کارزار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>همه کاخ ایوان لهراسپ شاه</p></div>
<div class="m2"><p>به چنگ اندر آورد ارجاسپ شاه</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ز اسباب شاهی هر آن چیز بود</p></div>
<div class="m2"><p>بدست آمد آورد ارجاسپ زود</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>از آن مردم شهر کآمد اسیر</p></div>
<div class="m2"><p>ز خرد و بزرگ ز برنا و پیر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ز دانا شنیدم که بد سی هزار</p></div>
<div class="m2"><p>ندیده سپهر این چنین کارزار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گرفتند گودرز کشواد را</p></div>
<div class="m2"><p>مرآن نامور پیر باداد را</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>نبودش توان تا کند کارزار</p></div>
<div class="m2"><p>چنان بد که بدخسته آن نامدار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>یکی آنکه فرسوده و پیر بود</p></div>
<div class="m2"><p>کمان آن قدر است چون تیر بود</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>سه دیگر ز نادیدن گیو شیر</p></div>
<div class="m2"><p>قدش چون کمان گشته بد گوشه گیر</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چنین خسته هم باش بستند دست</p></div>
<div class="m2"><p>سرآن را سر از تن صد افکند پست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>زنان را بزیر شکنجه برنج</p></div>
<div class="m2"><p>همی داشتند از پی مال گنج</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چه از غارت و تاخت پرداختند</p></div>
<div class="m2"><p>به ایوان ها آتش انداختند</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>همه کاخ لهراسپ را سوخت پاک</p></div>
<div class="m2"><p>بشد بلخ مانند یک توده خاک</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>از ایران ببردند نزدیک شاه</p></div>
<div class="m2"><p>همی کرد ارجاسپ به ایشان نگاه</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>به بردند گودرز را بسته پیش</p></div>
<div class="m2"><p>دلش خسته و سر فکنده ز پیش</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ز پیری الف قد او دال بود</p></div>
<div class="m2"><p>بر آن پیر سر بند بر یال بود</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بپرسید ارجاسپ کاین پیر کیست</p></div>
<div class="m2"><p>که مادر به حالش بخواهد گریست</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بگفتند گودرز پیر است این</p></div>
<div class="m2"><p>که در رزم جوشان چه شیر است این</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>ز گردان ما صد دلاور بکشت</p></div>
<div class="m2"><p>بدان تا ببستیم دستش به پشت</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بدو گفت ارجاسب که ای شوم کار</p></div>
<div class="m2"><p>که بادی ز کار خودت شرمسار</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>ز پیش تو این فتنه آمد نخست</p></div>
<div class="m2"><p>که خسرو به ایران کشیدی درست</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>کمر کینه را بهر افراسیاب</p></div>
<div class="m2"><p>به بستی و رفتی بر آن روی آب</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>به ترکان نبردی بیاراستی</p></div>
<div class="m2"><p>کازو شیر را دل به تن کاستی</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>چنین تا که شد کار خسرو بلند</p></div>
<div class="m2"><p>بشد گرم و کردی کمر باز تنگ</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>به دست تو شد کشته پیران پیر</p></div>
<div class="m2"><p>کنون خون پیرانت کرده اسیر</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>بدو گفت گودرز کای بی بها</p></div>
<div class="m2"><p>تهی بیشه دیدی ز نر اژدها</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>تهمتن مگر نیست آید به جنگ</p></div>
<div class="m2"><p>بیازد سوی گرده سام چنگ</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>بیاید دمادم ز خاور زمین</p></div>
<div class="m2"><p>بدرد ز نعل تکاور زمین</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>همان رستم است آنکه افراسیاب</p></div>
<div class="m2"><p>همیشه از او بد دو دیده پر آب</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>فرامرز آید ز هندوستان</p></div>
<div class="m2"><p>ابا شهریار جهان پهلوان</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>چه بانو گشسب و چه پرهیزگار</p></div>
<div class="m2"><p>بیایند با لشکر سی هزار</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>دگر پور بیژن سوار دلیر</p></div>
<div class="m2"><p>جهان جو سپهدار یل ارده شیر</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>دگر گرد فیروز یل پور طوس</p></div>
<div class="m2"><p>چه رهام گودرز با بوق کوس</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>چه گر گوی گرگین میلاد تیز</p></div>
<div class="m2"><p>بیایند با گرز و پولاد نیز</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>دگر نامور پور لهراسپ شاه</p></div>
<div class="m2"><p>چه کردش همی نام گشتاسپ شاه</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>چه شیر ژیان لشکر آید به جنگ</p></div>
<div class="m2"><p>ز کین گرزه گاو پیکر به چنگ</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>وزین روی دستان سام سوار</p></div>
<div class="m2"><p>ابا زابلی نامور سی هزار</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>نشانند بر تخت لهراسپ را</p></div>
<div class="m2"><p>نمانند برجای ارجاسپ را</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>همان است این مرز ایران زمین</p></div>
<div class="m2"><p>کز اینجا گریزان بشد شاه چین</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>نه این بیشه از شیر نر شد تهی</p></div>
<div class="m2"><p>که جوئی تو زین مرز تاج و شهی</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>مرا بود هشتاد پور گزین</p></div>
<div class="m2"><p>همه کشته گشتند در گاه کین</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>بخون سیاوخش پاکیزه تن</p></div>
<div class="m2"><p>همه کشته گشتند یک انجمن</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>سرا زندگانی نیاید بکار</p></div>
<div class="m2"><p>بگیری ابر تخت و ارثت؟ زار</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>که چون رستم آید بدین کینه گاه</p></div>
<div class="m2"><p>نه سر با تو ماند نه تخت و کلاه</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>چو بشنید ارجاسپ این گفتگوی</p></div>
<div class="m2"><p>بپیچید از پیر گودرز روی</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>به فرمود او را بدارند سخت</p></div>
<div class="m2"><p>به زنجیر پولاد در بند سخت</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>اسیران که بودند در بند اوی</p></div>
<div class="m2"><p>بفرمود آن ترک پرخاشجوی</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>که بردندشان سوی توران زمین</p></div>
<div class="m2"><p>بروئین دز اندر دلیران کین</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>به فرمود آنجا همه خشت و خاک</p></div>
<div class="m2"><p>بدز درکشند از پی قلعه پاک</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>دلیران که دنبال بانو شدند</p></div>
<div class="m2"><p>زنان دست از کین به زانو شدند</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>به بانو رسیدند در مرغزار</p></div>
<div class="m2"><p>سوران ترکان به صد گیرو دار</p></div></div>