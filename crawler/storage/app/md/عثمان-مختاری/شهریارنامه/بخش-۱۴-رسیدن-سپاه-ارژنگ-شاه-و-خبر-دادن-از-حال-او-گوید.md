---
title: >-
    بخش ۱۴ - رسیدن سپاه ارژنگ شاه و خبر دادن از حال او گوید
---
# بخش ۱۴ - رسیدن سپاه ارژنگ شاه و خبر دادن از حال او گوید

<div class="b" id="bn1"><div class="m1"><p>چه دیدند روی سپهدار شیر</p></div>
<div class="m2"><p>فکندند تن را ز بالا بزیر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همه پیش او در خروش آمدند</p></div>
<div class="m2"><p>چو دریای جوشان بجوش آمدند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>که ای گرد ما را به فریاد رس</p></div>
<div class="m2"><p>که هستیم یکسر در آتش چه خس</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سپهدار از ایشان بپرسید راز</p></div>
<div class="m2"><p>بگفتند کای گرد گردن فراز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دلیران ارژنگ شاهیم ما</p></div>
<div class="m2"><p>که زاری ز بدخواه داریم ما</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جهانجوی ارژنگشاه بزرگ</p></div>
<div class="m2"><p>چه غر مست مانده به چنگال گرگ</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بکوه اندرون مانده بی زاد و خورد</p></div>
<div class="m2"><p>برآورده بدخواه از آنشاه گرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>یلان جهانجوی شاه سرند</p></div>
<div class="m2"><p>بکوه سراندیب بیخور بدند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خورش جز گیا نیست در کوهسار</p></div>
<div class="m2"><p>جهانجوی ماند است در کوه خوار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>یکی را بفرمود تا در زمان</p></div>
<div class="m2"><p>رود پیش ارژنگ شاه جهان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بگوید که شاها بدل غم مدار</p></div>
<div class="m2"><p>که آمد جهانجوی یل شهریار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دلیریکه بد تندتر ز آن گروه</p></div>
<div class="m2"><p>برون راند و شد تازیان سوی کوه</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>که شه را از آن کار آگه کند</p></div>
<div class="m2"><p>یلان را دل از رنج کوته کند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>سپهبد ازین روی برساخت کار</p></div>
<div class="m2"><p>به بهزاد بسپرد گنج و حصار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>وز آن بس چنین گفت با ماهروی</p></div>
<div class="m2"><p>که ای برده روی تو از ماه گوی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>تو با گرد بهزاد ایدر بمان</p></div>
<div class="m2"><p>بدان تا من آیم زی آزادگان</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>فرانک بدو گفت ای نامدار</p></div>
<div class="m2"><p>بکام تو گردد جهان پایدار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>مرا بودن ایدر نه در خور بود</p></div>
<div class="m2"><p>روم زی سراندیب بهتر بود</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بدان تا ببینم سرانجام کار</p></div>
<div class="m2"><p>ببخشید اگر یاریم کردگار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>که دیگر ببینم رخ پهلوان</p></div>
<div class="m2"><p>که پیشست بسیار رنج گران</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بگفت این و شد زی سراندیب شاد</p></div>
<div class="m2"><p>وزین رو سپهدار فرخ نژاد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ابا نامداران سوی کوه شد</p></div>
<div class="m2"><p>شب تیره رو سوی انبوه شد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>وزین روی آمد سوار از گروه</p></div>
<div class="m2"><p>بشد پیش ارژنگ در بزر کوه</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>که شاها مخور غم که آمد براه</p></div>
<div class="m2"><p>جهانجوی داماد فرخنده گاه</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>شه او را ببخشید سر تابپای</p></div>
<div class="m2"><p>هرآن چیز پوشیده بد جابجای</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>که بر گو کجا دیدی آن شیر را</p></div>
<div class="m2"><p>خداوند کوپال و شمشیر را</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>سراسر بشه گفت آن چیز دید</p></div>
<div class="m2"><p>چه بشنید شه شادمانی گزید</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بفرمود تا کوس بنواختند</p></div>
<div class="m2"><p>پی رزم و کین گردن افراختند</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>برآمد خروش ازمیان گروه</p></div>
<div class="m2"><p>بجنبید گوئی سراندیب کوه</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>دم نای شادی بدرید گوش</p></div>
<div class="m2"><p>چو دریا شد آن کوه آمد بجوش</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>ز لشکر دلیران گروها گروه</p></div>
<div class="m2"><p>ز شادی دویدند بر بزر کوه</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>چو از تیره شب پاسی اندر گذشت</p></div>
<div class="m2"><p>یل نیو آمد خروشان بدشت</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>گدازان و تازان و خنجر بدست</p></div>
<div class="m2"><p>چو ابر خروشان و چون فیل مست</p></div></div>