---
title: >-
    بخش ۱۱۸ - پیدا شدن ابر تیره و بردن فرامرز گوید
---
# بخش ۱۱۸ - پیدا شدن ابر تیره و بردن فرامرز گوید

<div class="b" id="bn1"><div class="m1"><p>که ناگاه ابری برآمد سیاه</p></div>
<div class="m2"><p>فرامرز را برد از آوردگاه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یکی نعره برخواست از تیره ابر</p></div>
<div class="m2"><p>چنان چونکه از بیشه غرنده ببر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چنان چونکه برخیزد از خاک دود</p></div>
<div class="m2"><p>مرآن ابر تیره هوا کرد زود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هوا کرد ازدیده شد ناپدید</p></div>
<div class="m2"><p>خروش از دو لشکر بگردون رسید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چه زال آن چنان دید بارید آب</p></div>
<div class="m2"><p>برآن نیزه چون ابر شد آفتاب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بدل گفت از ایران بگردید بخت</p></div>
<div class="m2"><p>کجا داد خسرو به لهراسپ تخت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>که ازگاه نوذر شه نامدار</p></div>
<div class="m2"><p>چنین تا بهنگام این شهریار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شکستی چنین کس ز ایران ندید</p></div>
<div class="m2"><p>خروش از دو لشکر بگردون رسید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کنون تا دگر خود چه آید پدید</p></div>
<div class="m2"><p>نبرد دلیران و شیران که دید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چه ارجاسپ دید آن ستیزنده ابر</p></div>
<div class="m2"><p>گریزان چه روبه شد از پیش ببر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بدان روز آهنگ میدان نشد</p></div>
<div class="m2"><p>نبرد دلیران و شیران نشد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چه روز دگر شد جهان عطربار</p></div>
<div class="m2"><p>دو لشکر رخ آورد زی کارزار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به پیش سپه پیل و در قلب شاه</p></div>
<div class="m2"><p>پیاده رخ آورد پیش سپاه</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>همی خواست دستان که آید به جنگ</p></div>
<div class="m2"><p>که خورشید مینو یل تیز چنگ</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بزد اسپ و رخ سوی شه آورید</p></div>
<div class="m2"><p>پیاده شد آن پیلتن چون سزید</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>زمین بوسه زد پیش لهراسپ شاه</p></div>
<div class="m2"><p>ز شه خواست آهنگ آوردگاه</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بدو گفت لهراسپ بردار گام</p></div>
<div class="m2"><p>که مردی دلیری و بارای و نام</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>برانگیخت آن مرکب دشت پوی</p></div>
<div class="m2"><p>که جستی به میدان گه کین چو گوی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>برفتن چو باد و به تیزی چو تحنش</p></div>
<div class="m2"><p>به گرمی چو برق و به سرعت چو رخش</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چنان بود آن مرکب اندر شتاب</p></div>
<div class="m2"><p>که گر از پس او بدی آفتاب</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ز سایه گذشتی هم اندر زمان</p></div>
<div class="m2"><p>چو باد از سر زلف عنبرفشان</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>سراپای میدان بگردید مرد</p></div>
<div class="m2"><p>چه ارجاسپ دیدش به دشت نبرد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>سواری به میدان فرستان زود</p></div>
<div class="m2"><p>ابا کمتر و گرز و شمشیر و خود</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چو آمد کمان کرد بر زه سوار</p></div>
<div class="m2"><p>برآمد خروشیدن گیر و دار</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ز هر دو طرف تیره باران شدند</p></div>
<div class="m2"><p>چو شیران خنجرگزاران شدند</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>سرانجام خورشید چون باد زود</p></div>
<div class="m2"><p>زدش تیر بر ترک پولاد زود</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>سر و ترک با هم بهم بربدوخت</p></div>
<div class="m2"><p>بزخم اندر آتش ز پیکان بسوخت</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>دلیری دگر پیش او راند اسپ</p></div>
<div class="m2"><p>خروشید مانند آذرگشسپ</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بزد تیر و بردوختش در زمان</p></div>
<div class="m2"><p>سپر در زره در تنش پهلوان</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>چنین تا فکند از دلیران دوهشت</p></div>
<div class="m2"><p>به پیکان و تیر اندر آن پهن دشت</p></div></div>