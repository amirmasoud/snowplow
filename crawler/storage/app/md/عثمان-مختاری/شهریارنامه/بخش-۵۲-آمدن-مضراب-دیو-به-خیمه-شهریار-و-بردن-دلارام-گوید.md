---
title: >-
    بخش ۵۲ - آمدن مضراب دیو به خیمه شهریار و بردن دلارام گوید
---
# بخش ۵۲ - آمدن مضراب دیو به خیمه شهریار و بردن دلارام گوید

<div class="b" id="bn1"><div class="m1"><p>شب تیره ای بود مانند قیر</p></div>
<div class="m2"><p>نتابنده ماه و نه تابنده شیر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فلک بسته گویا در صبحگاه</p></div>
<div class="m2"><p>گشاده در دوزخ و دود آه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز ماهی سیه تا به مه بد جهان</p></div>
<div class="m2"><p>سر پاسبانان به خواب گران</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>که ناگاه مضراب وارونه کار</p></div>
<div class="m2"><p>بیامد بر خیمه شهریار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پری را مر آن دیو بد در کمین</p></div>
<div class="m2"><p>کمین برگشود و ربود از زمین</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مهی نو که گفتی که اکنون ربود</p></div>
<div class="m2"><p>و یا دیو مهر سلیمان ربود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یکی تندی بادی برآمد نژند</p></div>
<div class="m2"><p>خرامنده سروی ز بستان بکند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عقابی برون تاخت با صد شکوه</p></div>
<div class="m2"><p>خرامان تذروی ببرد از گروه</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز چنگال بلبل گلی بود زاغ</p></div>
<div class="m2"><p>و یا کشت بادی فروزان چراغ</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>یکی نعره زد ماه تابنده چهر</p></div>
<div class="m2"><p>که بربود دیو آن مهت از سپهر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چو خواب گران است ای گرد نیو</p></div>
<div class="m2"><p>بهوش آ که بردم ستمکاره دیو</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چو بشنید آواز دختر دلیر</p></div>
<div class="m2"><p>سر از خواب برداشت آن نره شیر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ندید آن دلاور دلارام را</p></div>
<div class="m2"><p>نگار سمن بود خود کام را</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>شب تیره و ماه پیدا نبود</p></div>
<div class="m2"><p>برآمد ز جان سرافراز دود</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>همی بود با ناله آن کامیاب</p></div>
<div class="m2"><p>چنین تا برآمد ز کوه آفتاب</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بخواند آن زمان گرد جمهور را</p></div>
<div class="m2"><p>بگفتا که ای کرد فرمانروا</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>دلارام را برد مضراب ریو</p></div>
<div class="m2"><p>بمن گوی اکنون یکی جای دیو</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بدان تا بسویش برانم سمند</p></div>
<div class="m2"><p>سر دیو وارونه آرم به بند</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>دلارام را نیز آرم بدست</p></div>
<div class="m2"><p>ببرم سر دیو واژون پرست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چو جمهور بشنید آهی کشید</p></div>
<div class="m2"><p>بلرزید و رخ کرد چون شنبلید</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>سپهدار را گفت بگذر ازین</p></div>
<div class="m2"><p>مکن رای آهنگ دیو چنین</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>که آن دیو واژونه نابکار</p></div>
<div class="m2"><p>نتابد رخ از لشکر صد هزار</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>گه کین چو زی سنگ چنگ آورد</p></div>
<div class="m2"><p>سر پیل نو زیر سنگ آورد</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بود این پسر زاده اهرمن</p></div>
<div class="m2"><p>ورا هست در کوهساران مکن</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>دو دیگر کزین جای تا جای اوی</p></div>
<div class="m2"><p>بسی هست راه ای جهان جنگجوی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>به مغرب ورا جای در کوهسار</p></div>
<div class="m2"><p>بود ای دلاور یل کامکار</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>یکی قلعه در کوه دارد بلند</p></div>
<div class="m2"><p>بود اندر آن قلعه دیو نژند</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>سپهبد چو بشنید شد خشمناک</p></div>
<div class="m2"><p>بگفتا بدارنده آب و خاک</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>که زین برنگردم ز پشت سمند</p></div>
<div class="m2"><p>بدان تا نیارم سرش زیر بند</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>کنون ترک جام و می و خواب کن</p></div>
<div class="m2"><p>بسیج ره رزم مضراب کن</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>کنون راه بنماد شو راهبر</p></div>
<div class="m2"><p>مرا بر سر دیو بدخواه بر</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>بدو گفت جمهور کای نامدار</p></div>
<div class="m2"><p>دو راهست اید(ر) مغرب دیار</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>یکی از ره ژرف دریا بود</p></div>
<div class="m2"><p>یکی دیگر از سوی صحرا بود</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>ز دریا توان بر بریدن دو ماه</p></div>
<div class="m2"><p>به شش ماه صحرای راه هست راه</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>بدین راه رو کانت نیکوتر است</p></div>
<div class="m2"><p>ازین دو کدامت ببین در خور است</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>بگفتا که دور است از این هر دو راه</p></div>
<div class="m2"><p>شکیبا نباشد دلم بر دو ماه</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>که این جای ارژنگ تنها بود</p></div>
<div class="m2"><p>دو دشمن بدینسان توانا بود</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>نشانی که از راه نزدیک هست</p></div>
<div class="m2"><p>مرا روشن این روز تاریک هست</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>بدو گفت جمهور کای پاکزاد</p></div>
<div class="m2"><p>یکی راه نزدیک دارم بیاد</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>ولیکن مر آن راه دارد خطر</p></div>
<div class="m2"><p>ز عفریت و از شیر و غولان نر</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>همه پشته پر مار و پر جاودان</p></div>
<div class="m2"><p>همه جای دیوان بد کاردان</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>توان رفت زین راه هر شب و روز</p></div>
<div class="m2"><p>ایا نامور گرد گیتی فروز</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>همه بیشه یکسر پر از گرگ و پیل</p></div>
<div class="m2"><p>ز هر بیشه تا بیشه ای بیست میل</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>چنین است نه بیشه در پیش راه</p></div>
<div class="m2"><p>که گفتم ابا پهلوان سپاه</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>سپهدار گفتا بسیج سفر</p></div>
<div class="m2"><p>بکن با دو صد مرد پرخاشخور</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>که در راه هر یک چو شیر نوند</p></div>
<div class="m2"><p>نبرده سوار دلیران بوند</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>بسیج سفر گرد جمهور کرد</p></div>
<div class="m2"><p>جهان را پر از فتنه و شور کرد</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>سپهدار گفتا به ارژنگ شاه</p></div>
<div class="m2"><p>سپردم تو را با خداوندگاه</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>هم ای در بمان با سپاه گران</p></div>
<div class="m2"><p>مکن جنگ می باش با سروران</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>وگر بر تو تنگ آید این روزگار</p></div>
<div class="m2"><p>سپه بر سوی کوه کش کن حصار</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>بدان تا من آیم از این راه باز</p></div>
<div class="m2"><p>چو کوهست با درد و رنج دراز</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>بدو گفت ارژنگ کای کامکار</p></div>
<div class="m2"><p>چرائی چنین سست نااعتبار</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>دو لشکر چنین تا در کارزار</p></div>
<div class="m2"><p>مبادا که برمن شود کارزار</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>گر این سرخ پوش اندر آید به تاو</p></div>
<div class="m2"><p>بمانم به چنگال او چون چکاو</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>همه کار من گشت خواهد تباه</p></div>
<div class="m2"><p>بترسم درآید به خاکم کلاه</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>مزن با درفش ستیزنده مشت</p></div>
<div class="m2"><p>بخود برمکن روزگارت درشت</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>بدین راه هرگز نرفت آدمی</p></div>
<div class="m2"><p>ازین رو ندید است کس خرمی</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>سپهدار گفتا مکن روی زرد</p></div>
<div class="m2"><p>ازین کار دل را می آور به درد</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>نیای من آن رستم زال زر</p></div>
<div class="m2"><p>به مردی شد از هفت خوان ره سپر</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>کجا بود در بند کاووس شاه</p></div>
<div class="m2"><p>شد از هفت خو(ا)ن پهلوان سپاه</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>به دیوان مازنداران رو نهاد</p></div>
<div class="m2"><p>ز بند گران داد شه را نژاد</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>ز دیوان بپرداخت آن بوم و بر</p></div>
<div class="m2"><p>به دیوان جهان کرد زیر و زبر</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>که عندی و سجه چو دیو سفید</p></div>
<div class="m2"><p>قمیران و اولاد و ارژنگ بید</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>سرانشان بگرز گران نرم کرد</p></div>
<div class="m2"><p>چو برگرز دست یلی گرم کرد</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>کنون من ازآن تخمه دارم نژاد</p></div>
<div class="m2"><p>نترسم از آن راه با کین و زاد</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>یکی پاسخ افکند زی سرخ پوش</p></div>
<div class="m2"><p>که ای نامور گرد با رای و هوش</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>سه هفته نگهدار بر جای پای</p></div>
<div class="m2"><p>بدان تا من آیم از ین ره بجای</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>از آن پس بکوشیم در کارزار</p></div>
<div class="m2"><p>به بینیم تا چیست انجام کار</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>بگفت این سروری راه آورید</p></div>
<div class="m2"><p>ز ره گرد بر اوج ماه آورید</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>ولیکن از آن روی آن سرخ پوش</p></div>
<div class="m2"><p>چو روز دگر شد برآمد بجوش</p></div></div>