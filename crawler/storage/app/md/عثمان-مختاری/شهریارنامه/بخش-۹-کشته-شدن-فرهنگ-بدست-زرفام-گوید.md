---
title: >-
    بخش ۹ - کشته شدن فرهنگ بدست زرفام گوید
---
# بخش ۹ - کشته شدن فرهنگ بدست زرفام گوید

<div class="b" id="bn1"><div class="m1"><p>دلاور برون راند آن فیل زود</p></div>
<div class="m2"><p>خروشان وجوشان بیامد چه دود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به فرهنگ بربست راه ستیز</p></div>
<div class="m2"><p>به کف داشت از کینه ساطور تیز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به فرهنگ گفتن که اندر نبرد</p></div>
<div class="m2"><p>ندیدی هنرهای مردان مرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نمودی به مردان ما دستبرد</p></div>
<div class="m2"><p>ولی کس ز من گوی مردی نبرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به من در جهان کس هم آورد نیست</p></div>
<div class="m2"><p>بمغرب زمین کس چه من مرد نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کنم پیکرت رابساطور نرم</p></div>
<div class="m2"><p>که من چون پلنگم توئی همچو غرم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بدو گفت فرهنگ کای پرگزاف</p></div>
<div class="m2"><p>ز مردان نه نیکوست آئین لاف</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>من امروز بینم یکی مرگ تو</p></div>
<div class="m2"><p>بکوبم بگرز گران ترک تو</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بگفت این و زی رزم آورد روی</p></div>
<div class="m2"><p>برآمد ز گردان یکی های هوی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>یکی تیر برداشت پیکان سترک</p></div>
<div class="m2"><p>بزد بر بر زنده پیل بزرگ</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نشد تیر بر فیل او کارگر</p></div>
<div class="m2"><p>که بودی در آهن ز پا تا به سر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>برافراخت زرفام ساطور کین</p></div>
<div class="m2"><p>خروشان و بر ابرو افکنده چین</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>درآمد خروشان بتنگ اندرش</p></div>
<div class="m2"><p>برافروخت به ساطور و زد بر سرش</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>زمن گفت مانا که برگشت بخت</p></div>
<div class="m2"><p>زمن بخت خواهد بپرداخت رخت</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بگفت این و یکباره جنگ آورید</p></div>
<div class="m2"><p>مر این مغربی را بچنگ آورید</p></div></div>