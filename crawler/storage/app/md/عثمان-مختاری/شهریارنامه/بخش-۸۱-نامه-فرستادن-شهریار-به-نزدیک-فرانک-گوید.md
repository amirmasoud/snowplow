---
title: >-
    بخش ۸۱ - نامه فرستادن شهریار به نزدیک فرانک گوید
---
# بخش ۸۱ - نامه فرستادن شهریار به نزدیک فرانک گوید

<div class="b" id="bn1"><div class="m1"><p>سپه را بسوی سراندیب برد</p></div>
<div class="m2"><p>سر سروران را ز سر شیب بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بگرد سراندیب آمد فرود</p></div>
<div class="m2"><p>یکی نامه آن شیر بنوشت زود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بنزد فرانک مه گلرخان</p></div>
<div class="m2"><p>مه گلرخان و مه بانوان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>که از بند بیرون کن ارژنگ را</p></div>
<div class="m2"><p>وگرنه برآرا ره جنگ را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بیزدان که این قلعه آرم بدست</p></div>
<div class="m2"><p>برین مردم شهر آرم شکست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سراندیب را سازم از کین خراب</p></div>
<div class="m2"><p>ز دریا ببندم درین شهر آب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>برش زیر و زیرش به بالا کنم</p></div>
<div class="m2"><p>مقام نهنگان دریا کنم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو نامه بنزد فرانک رسید</p></div>
<div class="m2"><p>ازین نامه اش نیش بر رگ رسید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زمانی بدین کار کرد او سکال</p></div>
<div class="m2"><p>ره آشتی را ببست او خیال</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>که اکنون دلارام بانوی اوست</p></div>
<div class="m2"><p>پرستنده طاق ابروی اوست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بدو گر بدین کینه چنگ آورم</p></div>
<div class="m2"><p>سر نام در زیر ننگ آورم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بمکرش همانا گه آرم بدست</p></div>
<div class="m2"><p>که از مکر بتوان سپاهی شکست</p></div></div>