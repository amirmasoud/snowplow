---
title: >-
    بخش ۷ - کشتن عباس هامان پسر هیتال شاه را گوید
---
# بخش ۷ - کشتن عباس هامان پسر هیتال شاه را گوید

<div class="b" id="bn1"><div class="m1"><p>وزآن روی بشنو سخن از سپاه</p></div>
<div class="m2"><p>ز ارژنگ گردان زرین کلاه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چه رفت از پی گود یل شهریار</p></div>
<div class="m2"><p>بگشتند گردان در آن مرغزار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پی اسب آن نامور یافتند</p></div>
<div class="m2"><p>برآن سوی گردان عنان تافتند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بدیدند اسبش به آن کوهسار</p></div>
<div class="m2"><p>گرفتند و بردند مردان کار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به نزدیک ارژنگشاه بزرگ</p></div>
<div class="m2"><p>بگفتند کای نامدار سترک</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بدیدیم اسب یل نامدار</p></div>
<div class="m2"><p>که کردی چرا در لب جویبار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ندیدیم ما پهلوان را بدشت</p></div>
<div class="m2"><p>ندانم چرخ از برش چون گذشت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چه ارژنگ ازین کار آگاه شد</p></div>
<div class="m2"><p>بره درد و شادیش کوتاه شد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بفرمود کاید برش عاس تیز</p></div>
<div class="m2"><p>که در کشته بودی چه الماس تیز</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بدو گفت ای گرد والانسب</p></div>
<div class="m2"><p>ز من دخت هیتال کردی طلب</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سپارم بتو کشور و دخترش</p></div>
<div class="m2"><p>بدانگه که از تن ببرم سرش</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>کنون گم شد از من یل نامدار</p></div>
<div class="m2"><p>جهان پهلوان گرد خنجرگزار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بداند که این گرد هیتال شوم</p></div>
<div class="m2"><p>نمانم بیک تن درین مرز و بوم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ز کین دست بر تیغ تیز آورد</p></div>
<div class="m2"><p>به ما بر یکی رستخیز آورد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>از آن پیش کآگاه گردد ازین</p></div>
<div class="m2"><p>یکی چاره پیش آر ای گرد کین</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بیاری برم گر سر شاه را</p></div>
<div class="m2"><p>سپارم بتو کشور و ماه را</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>تو خود شرط کردی که از تن سرش</p></div>
<div class="m2"><p>ببری بگیری ز من دخترش</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بدو عاس گفت ای شه نام دار</p></div>
<div class="m2"><p>کنون هست هنگامه گیر و دار</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>هرآن چیز گفتی بجای آورم</p></div>
<div class="m2"><p>بخنجر سرش زیر پای آورم</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بناگه برآمد غو کره نای</p></div>
<div class="m2"><p>ابانای و سرغین و هندی درآی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>همان نعره فیل و آوای کوس</p></div>
<div class="m2"><p>ز درگاه هیتال با صد فسوس</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چه خورشید برداشت از کوه سر</p></div>
<div class="m2"><p>ز کارآگهان مردی آمد ز در</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>که شاها زمغرب در آمد سپاه</p></div>
<div class="m2"><p>که از گردشان گشت گم مهر و ماه</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ز مغرب سپاهی ز در در رسید</p></div>
<div class="m2"><p>کشان کرد بر فرق مه بر رسید</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>همه عاد مانند مردان کار</p></div>
<div class="m2"><p>همه شیره مردان خنجرگزار</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>سرانی که هر یک بروز نبرد</p></div>
<div class="m2"><p>برآرند از فیل و از شیر گرد</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>چه جمهور زرفام با گیر و دار</p></div>
<div class="m2"><p>رسیدند با لشکر بیشمار</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>چه ارژنگ بشنید دلتنگ شد</p></div>
<div class="m2"><p>دلش سست و از روی اورنگ شد</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بدو عاس گفت ای شه نامدار</p></div>
<div class="m2"><p>مخور غم دل خویش رنجه مدار</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>همین شب روم سوی هیتال من</p></div>
<div class="m2"><p>سراز کین ببرمش از یال من</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>چه هیتال را سر ببرم به تیغ</p></div>
<div class="m2"><p>نمایند این لشکر ازماگریغ</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>بگفت این بیرون شد از پیش شاه</p></div>
<div class="m2"><p>شب تیره آمد میان سپاه</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>نه آوای زنگ و نه نای جرس</p></div>
<div class="m2"><p>نه های کشیک چی نه هوئی عسس</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>چه آمد بنزدیک خرگاه شاه</p></div>
<div class="m2"><p>یکی اژدها دید در پیش گاه</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>کش از دم همی آتش آمد برون</p></div>
<div class="m2"><p>نیارست از بیم رفتن درون</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>چنان بد که هیتال تیره روان</p></div>
<div class="m2"><p>به جادوگری کرده بد آن نشان</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>که از دشمن ایمن بود گاه خواب</p></div>
<div class="m2"><p>چنین تا برآمد ز که آفتاب</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>ز مرجانه این سحر آموخته</p></div>
<div class="m2"><p>به شاگردیش دل برافروخته</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>چنین کرده مرجانه پیمان به شاه</p></div>
<div class="m2"><p>که گر زی تو آید ز دشمن سپاه</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>بیایم بسازم همه کار تو</p></div>
<div class="m2"><p>بهر کار باشم هوادار تو</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>چه دید اژدها را جهاندیده عاس</p></div>
<div class="m2"><p>بگردید از آنجای دل پرهراس</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>بخرگاه ماهان برون رفت تفت</p></div>
<div class="m2"><p>بخنجر سرش را ز تن برگرفت</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>روان برد نزدیک ارژنگشاه</p></div>
<div class="m2"><p>نهاد آن سر بی بهایش بگاه</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>بپرسید شه کین سر از آن کیست</p></div>
<div class="m2"><p>که بر جان او زار باید گریست</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>چنین داد پاسخ که ای شهریار</p></div>
<div class="m2"><p>سر گرد هامان خنجرگزار</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>رسانیدم اینک بنزدیک تخت</p></div>
<div class="m2"><p>ازو گشته بدبخت بد یاربخت</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>چه رفتم بنزدیک هیتال شاه</p></div>
<div class="m2"><p>بدیدم یکی اژدهای سیاه</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>که بد خفته در پیش تخت بلند</p></div>
<div class="m2"><p>بترسیدم آید به من زو گزند</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>چه ارژنگ بشنید ازو شاد شد</p></div>
<div class="m2"><p>تو گفتی که از بند آزاد شد</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>سرش در سنان برد دربارگاه</p></div>
<div class="m2"><p>زدند و بدیدند یکسر سپاه</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>چه روز دگر خسرو خاوری</p></div>
<div class="m2"><p>برآمد بر این طاق نیلوفری</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>بهیتال گفتند جاوید مان</p></div>
<div class="m2"><p>که هامانت بربست رخت از جهان</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>برافروخت هیتال بگریست زار</p></div>
<div class="m2"><p>برو روز روشن چه شب گشت تار</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>تنش را بآتش فکندند زود</p></div>
<div class="m2"><p>بماتم سه روز اندرون شاه بود</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>چهارم چو شد خاست آوای زنگ</p></div>
<div class="m2"><p>که از مغرب آمد سپاهی به جنگ</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>جهاندار جمهور زرفام شیر</p></div>
<div class="m2"><p>رسیدند زی شاه با دار و گیر</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>چه بشنید هیتال بربست کوس</p></div>
<div class="m2"><p>جهان شد ز گرد سپه آبنوس</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>پذیره بیامد باین داد و دین</p></div>
<div class="m2"><p>بر نامور شاه مغرب زمین</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>ورا دید شاد و بایوان شدند</p></div>
<div class="m2"><p>برآمد خروش تبیره بلند</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>همه شب بدو داشت هیتال روی</p></div>
<div class="m2"><p>ز ارژنگ بودش همه گفتگوی</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>بدو گفت جمهور کای نامور</p></div>
<div class="m2"><p>بدان آمدم بسته کین را کمر</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>که تا این سپه را ز کین بشکنم</p></div>
<div class="m2"><p>نه گر این کنم پس نه مردم زنم</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>بفرمای تا نای کین دردمند</p></div>
<div class="m2"><p>که امروز مائیم خصم بلند</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>سر شاه ارژنگ آرم بچنگ</p></div>
<div class="m2"><p>چه زی تیغ دست آورم روز جنگ</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>مر آن زابلی را سر آرم بدست</p></div>
<div class="m2"><p>تنش را بخاک افکنم زار و پست</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>بخون سه فرزند هیتال من</p></div>
<div class="m2"><p>سرانشان بکوبم بکوپال من</p></div></div>