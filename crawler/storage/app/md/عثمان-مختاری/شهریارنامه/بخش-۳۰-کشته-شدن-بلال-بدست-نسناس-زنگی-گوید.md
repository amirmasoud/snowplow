---
title: >-
    بخش ۳۰ - کشته شدن بلال بدست نسناس زنگی گوید
---
# بخش ۳۰ - کشته شدن بلال بدست نسناس زنگی گوید

<div class="b" id="bn1"><div class="m1"><p>بدو گفت کای زنگی دیو چهر</p></div>
<div class="m2"><p>بگیری زمن دستبردی بدهر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چنانت ز میدان فرستم بدر</p></div>
<div class="m2"><p>که بر تو بگریند مام و پدر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بگفت این و برداشت گرز کشن</p></div>
<div class="m2"><p>بغرید ماننده اهرمن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بزد بر سر گرد نسناس تیز</p></div>
<div class="m2"><p>مر آن گرز کین همچو الماس تیز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فتاد از بر فیل بر خاک خوار</p></div>
<div class="m2"><p>به یک گرز برگشت از کارزار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو هیتال دید آن رقیب سپاه</p></div>
<div class="m2"><p>که شد کشته بلال در رزمگاه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز سر شهپر خسروی برگرفت</p></div>
<div class="m2"><p>ببارید خون دست بر سر گرفت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو برگشت بخت از من خاکسار</p></div>
<div class="m2"><p>چو شد کشته بلال خنجر گزار</p></div></div>