---
title: >-
    بخش ۲۲ - خبر   فرستادن   کرشاسب   پیش   پدر
---
# بخش ۲۲ - خبر   فرستادن   کرشاسب   پیش   پدر

<div class="b" id="bn1"><div class="m1"><p>فرسته برون کرد گردی گزین</p></div>
<div class="m2"><p>بدادش عرابی نوندی به زین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یکی دشت پیمان برّنده راغ</p></div>
<div class="m2"><p>به دیدار و رفتار زاغ و نه زاغ</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سیه چشم و گیسوفش و مشک دُم</p></div>
<div class="m2"><p>پری پوی و آهو تک و گور سم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>که اندام مه تازش و چرخ گرد</p></div>
<div class="m2"><p>زمین کوب و دریا بُرو ره نورد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به پستی چو آب و به بالا چو ابر</p></div>
<div class="m2"><p>شناور چو ماغ و دلاور چو ببر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از اندیشه دل سبک پوی تر</p></div>
<div class="m2"><p>ز رای خردمند ره جوی تر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو شب بد ولیکن چه بشتافتی</p></div>
<div class="m2"><p>به تک روز بگذشته دریافتی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به گامی شمردی کُه از وی زور</p></div>
<div class="m2"><p>بدیدی شب از دور بر موی مور</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بجستی به یک جستن از روی زم</p></div>
<div class="m2"><p>بگشتی به ناورد بر یک درم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چو بر آب جستی چو بر کوه راه</p></div>
<div class="m2"><p>به روز از خور افزون شدی شب زماه</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>برو مژده بر چون ره اندر گرفت</p></div>
<div class="m2"><p>جهان گفتی از باد تک برگرفت</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چنان شد میان هوا تیرپوی</p></div>
<div class="m2"><p>که چوگان بُدَش دست و خورشید گوی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>همی جست چون تیر و رفتار تیر</p></div>
<div class="m2"><p>ز نعلش زمین چون ز باد آبگیر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>فروهشته پُش چون زره بر عنان</p></div>
<div class="m2"><p>برافراشته گوش ها چون سنان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>همی بست از گرد تک چشم مهر</p></div>
<div class="m2"><p>همی کافت از شیهه گوش سپهر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>سوارش ازو باز ناورد پای</p></div>
<div class="m2"><p>مگر بر در شاه زابل خدای</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>رسانید مژده به شاه دلیر</p></div>
<div class="m2"><p>که بر اژدها چیره شد نرّه شیر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ز شادی برو جان برافشاندند</p></div>
<div class="m2"><p>بر آن مژده بر آفرین خواندند</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>دهانش ز یاقوت کردند پُر</p></div>
<div class="m2"><p>دو دستش ز دینار و دامن ز دُر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>به شبرنگ بر نیز دیبای لعل</p></div>
<div class="m2"><p>فکندند و زرینش کردند نعل</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چو باران درم ریختند از برش</p></div>
<div class="m2"><p>گرفتند در مشک سارا سرش</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>برفتند نزد سپهبد سیاه</p></div>
<div class="m2"><p>کشیدند پس اژدها را به راه</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ز گردون بهم بیست و از پیل پنج</p></div>
<div class="m2"><p>بُد از بار آن اژدها زیر رنج</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>همه ره ز بس بار آن کوه نیل</p></div>
<div class="m2"><p>ز گردون همه بیش نالید پیل</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بزرگان ابا اثرط سرفراز</p></div>
<div class="m2"><p>درفش و سپه پیش بردند باز</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ز کوس و تبیره برآمد خروش</p></div>
<div class="m2"><p>جهان شد پر از رامش و نای و نوش</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>همه شهر و ره بود پُرخواسته</p></div>
<div class="m2"><p>به آذین و گنبد بیاراسته</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>شده کوی و برزن چو باغ ارم</p></div>
<div class="m2"><p>زبر مشک و در پای ریزان درم</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>پذیره شد از شهر برنا و پیر</p></div>
<div class="m2"><p>از آن اژدها خیره وز زخم تیر</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>به صحرا برون چرمش آکنده کاه</p></div>
<div class="m2"><p>نهادند تا دید ضحاک شاه</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>بدان خرمی بزمی افکند پی</p></div>
<div class="m2"><p>کزآن بزم ماه آرزو کرد می</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>بفرمود کامروز دل شادکام</p></div>
<div class="m2"><p>همه یاد گرشاسب گیرید جام</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>زره دادش و خود و زرّین سپر</p></div>
<div class="m2"><p>کلاه و نگین، اسپ و تیغ و کمر</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>همان جوشن خویش و خفتان جنگ</p></div>
<div class="m2"><p>به خروارها دیبه رنگ رنگ</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>از آن کاژدها کشت و شیری نمود</p></div>
<div class="m2"><p>درفش چنان ساخت کز هردو بود</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>به زیر درفش اژدها سیاه</p></div>
<div class="m2"><p>زّبر شیر زرّین و بر سرش ماه</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>زمین همه زاول و بوم بُست</p></div>
<div class="m2"><p>بدو داد و بنوشت عهدی درست</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>جهان پهلوانی مرو را سپرد</p></div>
<div class="m2"><p>وزآنجای لشکر سوی هند بُرد</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>مرین داستان را سرانجام کار</p></div>
<div class="m2"><p>نبشتند هرکس در آن روزگار</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>به رود و ره جام برداشتند</p></div>
<div class="m2"><p>به ایوان ها نیز بنگاشتند</p></div></div>