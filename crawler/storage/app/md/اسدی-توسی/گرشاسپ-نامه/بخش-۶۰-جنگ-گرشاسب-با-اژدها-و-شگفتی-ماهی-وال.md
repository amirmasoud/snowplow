---
title: >-
    بخش ۶۰ - جنگ گرشاسب با اژدها و شگفتی ماهی وال
---
# بخش ۶۰ - جنگ گرشاسب با اژدها و شگفتی ماهی وال

<div class="b" id="bn1"><div class="m1"><p>برفتند و آمد جزیری پدید</p></div>
<div class="m2"><p>که آن جا به جز اژدها کس ندید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بدانسان بزرگ اژدها کز دو میل</p></div>
<div class="m2"><p>بیوباشتندی به دَم زنده پیل</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز زهرش همه کوه و هامون سیاه</p></div>
<div class="m2"><p>دم و دودشان رفته بر چرخ و ماه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یکایک پراکنده بر دشت و غار</p></div>
<div class="m2"><p>زبان چون درخت و دهان چون دهار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یکی را دُم از حلقه هر سو چو دام</p></div>
<div class="m2"><p>دمان آتش از زخم دندان و کام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یکی زوکشان گیسوان گرد خویش</p></div>
<div class="m2"><p>به سر بر سرو رسته چون گاومیش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سپهبد برآراست رفتن به جنگ</p></div>
<div class="m2"><p>گرفتند دامنش گردان به چنگ</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>همی گفت هر کس که با جان ستیز</p></div>
<div class="m2"><p>مجوی و مشو در دم رستخیز</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بسی اژدهای دمان ایدرست</p></div>
<div class="m2"><p>کز آن کش تو کشتی بسی مهترست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چه با اژدها رزم را ساختن</p></div>
<div class="m2"><p>چه مر مرگ را بآرزو خواستن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>همان نیز ملاّح فرزانه هوش</p></div>
<div class="m2"><p>مشو گفت و بر جان سپردن مکوش</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بدین گونه مارست کز زهر تاب</p></div>
<div class="m2"><p>کند مرد را آرزومند آب</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>لبان کفته و تشنه و روی زرد</p></div>
<div class="m2"><p>بود دل طپان تا بمیرد به درد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>همان نیز مارست کز زهر و خشم</p></div>
<div class="m2"><p>بمیرد هر آنکس برافکند چشم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>وز آن مار کز دمش باد سموم</p></div>
<div class="m2"><p>به مردار بر آید گدازد چو موم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>دگر هست کز وی تن مرد خون</p></div>
<div class="m2"><p>گرد جوش وز پوست آید برون</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>و ز آن هم که گر کشته زهر اوی</p></div>
<div class="m2"><p>کسی بیند او نیز میرد به بوی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>همی بسپری روی دولت به پای</p></div>
<div class="m2"><p>همی بر کنی بیخ شادی ز جای</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>سپهبد برآشفت و گفت از نبرد</p></div>
<div class="m2"><p>مرا چرخ گردان نگوید که گرد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>به یزدان که داد از بر خاک و آب</p></div>
<div class="m2"><p>زمین را درنگ و زمان را شتاب</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>کزین جایگه برنگردم کنون</p></div>
<div class="m2"><p>مگر رانده از اژدها جوی خون</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>نه بور نبردی به کار آیدم</p></div>
<div class="m2"><p>نه زایدر کسی دستیار آیدم</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بگفت این و ترکش پر از تیر کرد </p></div>
<div class="m2"><p>بپوشید خفتان زره زیر کرد</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>سپر در برافکند با گرز و تیغ </p></div>
<div class="m2"><p>برون رفت برسان غرنده میغ </p></div></div>
<div class="b" id="bn25"><div class="m1"><p>سراسر شخ و سنگلاخ درشت</p></div>
<div class="m2"><p>بگشت و از آن اژدها شش بکشت</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>به شمشیر تنشان همه ریزه کرد</p></div>
<div class="m2"><p>سرانشان ببرید و بر نیزه کرد</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بیاورد تا دید یکسر سپاه</p></div>
<div class="m2"><p>همی گفت هر کس که این کینه خواه</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>دلاور چه گردست از اینسان دلیر</p></div>
<div class="m2"><p>که بر هر که رزم آورد هست چیر</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>اگر اژدها باشد ار پیل و کرگ</p></div>
<div class="m2"><p>بر تیغ او نیست ایمن ز مرگ</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>همانروز کردند از آن کُه گذر</p></div>
<div class="m2"><p>رسیدند نزد جزیری دگر</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>جزیری ز بس بیشه نادیده مرز</p></div>
<div class="m2"><p>مرو را بسی مردم کشت ورز</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>گروه ورا پیشه پر خاش بود</p></div>
<div class="m2"><p>درختان گل و کشتشان ماش بود</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>یکی مرده ماهی همان روزگار</p></div>
<div class="m2"><p>برافکنده موجش به سوی کنار</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>ارش هفتصد بود بالای او</p></div>
<div class="m2"><p>فزون از چهل بود پهنای او</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>دُمش بود بهری فتاده ز بند</p></div>
<div class="m2"><p>ندانست انداز آن کس که چند</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>شده ده هزار انجمن مرد و زن</p></div>
<div class="m2"><p>به نی پشتها بسته بر وی رسن</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>رسن ها سوی بیشه باز آخته</p></div>
<div class="m2"><p>کشان بر درخت و گره ساخته</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>ز گردش همه هر دو لشکر به جوش</p></div>
<div class="m2"><p>وزیشان رسیده به پروین خروش</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>زمان تا زمان خاستی موج سخت</p></div>
<div class="m2"><p>گسستی رسن چند کندی درخت</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>کشیدند از آب اندورن همگروه</p></div>
<div class="m2"><p>به کشتی به خشکی مر آن پاره کوه</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>برو ز آن سیاهان ابر کوه و راغ</p></div>
<div class="m2"><p>شد انبوه بر بوم چون خیل زاغ</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>بسی گوهر و زر بُد اوباشته</p></div>
<div class="m2"><p>همه سینش از عنبر انباشته</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>بیامد کس ِ شاه برداشت پاک</p></div>
<div class="m2"><p>برون کرد دندانش و زد مغز چاک</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>بسی روغن از مغز و از چشم اوی</p></div>
<div class="m2"><p>گرفتند افزون ز سیصد سبوی</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>دگر هر چه ماند از بزرگان و خرد</p></div>
<div class="m2"><p>ز بهر خورش پاره کردند و برد</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>بماند از شگفتی سپهبد به جای</p></div>
<div class="m2"><p>بدو گفت مهراج فرخنده رای</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>که این ماهیست آن که خوانند وال</p></div>
<div class="m2"><p>وزین مه بس افتد هم ایدر به سال</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>بود نیز چندانکه بی رنج و غم</p></div>
<div class="m2"><p>بیوبارد این کشتی ما به دم</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>چو بینند کآید ز دریا برون</p></div>
<div class="m2"><p>ز سهمش که کشتی کند سرنگون</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>ز بوق و دهل وز جرس وز خروش</p></div>
<div class="m2"><p>رسانند بر چرخ گردنده جوش</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>به هر سو سک ترش دارند و تیز</p></div>
<div class="m2"><p>بریزند تا زود گیرد گریز</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>همیدون یکی ماهی دیگرست</p></div>
<div class="m2"><p>کزین وال تنش اندکی کمتر ست</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>کجا او گذشت این دگر ماهیان</p></div>
<div class="m2"><p>گریزند و باشند تا ماهیان</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>یکی خُرد ماهیست با او به کین</p></div>
<div class="m2"><p>چو دیدش جهد در قفاش از کمین</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>به دندان گشایدش در مغز راه</p></div>
<div class="m2"><p>برآرد سر از درد ماهی به ماه</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>دگر هست مرغی به تن لعل رنگ</p></div>
<div class="m2"><p>مِه از باز چون او به منقار و چنگ</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>مرین ماهی خرد را دشمنست</p></div>
<div class="m2"><p>همه روز گردانش پیرامنست</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>چو بیند کش اندر قفا ره گشاد</p></div>
<div class="m2"><p>درآید ربایدش ازو همچو باد</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>گر آن مرغ فریاد رس نیست زود</p></div>
<div class="m2"><p>برآرد به سه روزش از مغز دود</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>به گیتی در از زندگان نیست چیز</p></div>
<div class="m2"><p>کش اندر نهان دشمنی نیست نیز</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>یکی گفت دیگر ز کشتی کشان</p></div>
<div class="m2"><p>که دیدم دگر ماهیی زین نشان</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>ز دریا فتاده به خشکی برون</p></div>
<div class="m2"><p>درازای او چار صدرش فزون</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>به کام اندرش کشتی لخت لخت</p></div>
<div class="m2"><p>بدو در نه مردم بمانده نه رخت</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>شکمش هم آن گه که بشکافتیم</p></div>
<div class="m2"><p>یکی زنده ماهی دراو یافتیم</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>ز سی رش فزون بود از بیش و کم</p></div>
<div class="m2"><p>بُدش ماهیی یک رش اندر شکم</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>همان ماهی خُرد بُد زنده نیز</p></div>
<div class="m2"><p>ازین به شگفت ار بجویی چه چیز </p></div></div>
<div class="b" id="bn67"><div class="m1"><p>شگفت خداوند چرخ بلند</p></div>
<div class="m2"><p>به گیتی که داند شمردن که چند </p></div></div>
<div class="b" id="bn68"><div class="m1"><p>به هر کاری او راست کام و توان</p></div>
<div class="m2"><p>که فرمانش بی رنج دارد روان</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>ز خون تبه مشک بویا کند</p></div>
<div class="m2"><p>ز خاک سیه جان گویا کند</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>پدید آورد تیره سنگی در آب</p></div>
<div class="m2"><p>کند زو همان آب دُرّ خوشاب</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>به جایی که بایسته بیند همی</p></div>
<div class="m2"><p> ز هر سان شگفت آفریند همی</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>بدان تا شگفتی چنین گونه گون</p></div>
<div class="m2"><p>بود بر تواناییش رهنمون</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>بَر شاه آن جای از آن پس به کام</p></div>
<div class="m2"><p>ببودند یک هفته با بزم و جام</p></div></div>