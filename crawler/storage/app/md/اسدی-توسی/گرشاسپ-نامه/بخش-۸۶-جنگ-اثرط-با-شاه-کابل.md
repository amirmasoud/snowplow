---
title: >-
    بخش ۸۶ - جنگ اثرط با شاه کابل
---
# بخش ۸۶ - جنگ اثرط با شاه کابل

<div class="b" id="bn1"><div class="m1"><p>وز آن سو چو از شهر داور سپاه</p></div>
<div class="m2"><p>سوی جنگ برد اثرط کینه خواه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سپه سی هزار از یلان داشت بیش</p></div>
<div class="m2"><p>دوصد پیل برگستوان دار پیش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دلیران پرخاش دو رویه صف</p></div>
<div class="m2"><p>کشیدند جان برنهاده به کف</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سواران شد آمد فزون ساختند</p></div>
<div class="m2"><p>یلان از کمین ها برون تاختند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به کوه اندر از کوس کین ناله خاست</p></div>
<div class="m2"><p>ز پیکان در ابر آهنین ژاله خاست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شتاب اندر آمیخت کین با درنگ</p></div>
<div class="m2"><p>شد از خون و از گرد گیتی دو رنگ</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هوا تف خشت درفشان گرفت</p></div>
<div class="m2"><p>سر تیغ هرسو سرافشان گرفت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تو گفتی ز بس خون که بارد همی</p></div>
<div class="m2"><p>جهان زخم خنجر سرآرد همی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>درآورده خرطوم پیلان به هم</p></div>
<div class="m2"><p>چو ماران خم اندر فکنده به خم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>همی خون و خوی برهم آمیختند</p></div>
<div class="m2"><p>به دندان ز زخم آتش انگیختند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گرفتند پیلان اثرط گریز</p></div>
<div class="m2"><p>بر آمد ز زابل گروه رستخیز</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>فراوان کس از پیل افتاد پست</p></div>
<div class="m2"><p>بسی کس نگون ماند بی پا و دست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>فکند این سلیح آن دگر رخت ریخت</p></div>
<div class="m2"><p>دلاور ز بددل همی به گریخت</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>زد اثرط برون ادهم تیزگام</p></div>
<div class="m2"><p>یلان را همی خواند یک یک به نام</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>عنان چند را باز پیچید و گفت</p></div>
<div class="m2"><p>نیستاد کس مانده با درد جفت</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بدش ریدگان سرایی هزار</p></div>
<div class="m2"><p>هزار دگر گرد خنجر گزار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بدین مایه لشکر بیفشرد پای</p></div>
<div class="m2"><p>فرو داشت چندان سپه را بجای</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چپ و راست با نامداران جنگ</p></div>
<div class="m2"><p>همی جست جنگ از پی نام و ننگ</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>عنان را به حمله بسودن گرفت</p></div>
<div class="m2"><p>سران را به نیزه ربودن گرفت	</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>کجا گردی انگیختی در نبرد</p></div>
<div class="m2"><p>به خون باز بنشاندی آن تیره گرد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چنین تا فروشد سپهری درفش</p></div>
<div class="m2"><p>زشب گشت زربفت گیتی بنفش</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>به راه سکاوند چون باد تفت</p></div>
<div class="m2"><p>شب قیرگون روی بنهاد ورفت</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بر دامن کوهی آمد فرود</p></div>
<div class="m2"><p>همه راغ او بیشه ی کلک بود</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>گریزندگان را گروها ، گروه</p></div>
<div class="m2"><p>همی خواند از هر رهی سوی کوه</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>پراکنده گرد آمدش پیل شست</p></div>
<div class="m2"><p>دگر ده هزار از یلان چیره دست</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>همه خسته و مانده و تافته</p></div>
<div class="m2"><p>ز بس تشنگی کام و دل کافته</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>طلایه پراکنده بر کوه و دشت</p></div>
<div class="m2"><p>ببد تا سپاه شب از جا بگشت</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>چو دینار گردون برآمد ز خم</p></div>
<div class="m2"><p>ستد یک یک از سبز مینا درم</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>درفش شه کابل آمد پدید</p></div>
<div class="m2"><p>سپاه از پسش یکسر اندر رسید</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>سراسیمه ماندند زاول سپاه</p></div>
<div class="m2"><p>به اثرط نمودند هر گونه راه</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>چه سازیم گفتند چاره که جنگ	</p></div>
<div class="m2"><p>فراز آمد و شد جهان تار و تنگ</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>ستوهیم هم مرد و هم بارگی</p></div>
<div class="m2"><p>شده در دم مرگ یکبارگی</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>ز چندین سپه نیست ناخواسته کس</p></div>
<div class="m2"><p>ره دور پیشست و دشمن ز پس</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>چنین گفت اثرط که یک بار نیز</p></div>
<div class="m2"><p>بکوشیم تا بخش کمتر نگردد نه بیش</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>جهاندار بخشی که کردست پیش</p></div>
<div class="m2"><p>از آن بخش کمتر نگردد به بیش</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>همه کار پیکار و رزم ایزدیست</p></div>
<div class="m2"><p>که داند که فرجام پیروز کیست</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>به هر سختیی تا بود جان به جای</p></div>
<div class="m2"><p>نباید بریدن امید از خدای</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>چه خواهد بدن مرگ فرجام کار</p></div>
<div class="m2"><p>چه در بزم مردن چه در کارزار</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>بگفت این و خفتان و مغفر بخواست</p></div>
<div class="m2"><p>بزد کوس و صف سپه کرد راست</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>شد اندر زمان روی چرخ بنفش</p></div>
<div class="m2"><p>پر از مه ز بس ماه روی درفش</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>ز خون یلان و ز گرد سپاه	</p></div>
<div class="m2"><p>زمین گشت لعل و هوا شد سیاه</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>ز بس گرز بر ترگ ها کوفتن</p></div>
<div class="m2"><p>فتاد آسمان ها در آشوفتن</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>سرتیغ در چرخ مه تاب داد</p></div>
<div class="m2"><p>سنان باغ کین را به خون آب داد</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>بد از زخم گردان سراسیمه کوه</p></div>
<div class="m2"><p>ز بانگ ستوران ستاره ستوه</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>شده پاره بر شیر مردان زره</p></div>
<div class="m2"><p>ز خون بسته بر نیزه هاشان گره</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>زمین از پی پیل پرژرف چاه</p></div>
<div class="m2"><p>چو کاریز یلان خون را به هر چاه راه</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>خزان است آن دشت گفتی به رنگ</p></div>
<div class="m2"><p>درختان یلان باغ میدان جنگ</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>چمن صف دم بد دلان باد سرد</p></div>
<div class="m2"><p>روان خون می و چهرها برگ زرد</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>شد از کشته پرپشته بالا و پست</p></div>
<div class="m2"><p>سرانجام بد خواه شد چیره دست</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>به زاول گره بخت بربیخت گرد</p></div>
<div class="m2"><p>همه روی برگاشتند از نبرد</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>یکی کوه و دیگر بیابان گرفت</p></div>
<div class="m2"><p>بماند از بد بخت اثرط شگفت</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>برآهخت تیغ اندر آمد به پیش</p></div>
<div class="m2"><p>دو تن را فکند از دلیران خویش</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>بسی خورد سوگندهای درشت</p></div>
<div class="m2"><p>که هر کاو نماید به بدخواه پشت</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>نیام سر تیغ سازم برش</p></div>
<div class="m2"><p>کنم افسر دار بی تن سرش</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>وگر من به تنهایی اندر ستیز</p></div>
<div class="m2"><p>بمانم دهم سر نگیرم گریز</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>دگر باره گردان پرخاشجوی</p></div>
<div class="m2"><p>به ناکام زی رزم دادند روی</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>ده و گیر برخاست بادار و برد</p></div>
<div class="m2"><p>هوا چون بیابان شد از تیره گرد</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>بیابانی آشفته همرنگ قیر</p></div>
<div class="m2"><p>درو غول مرگ و گیا خشت وتیر</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>ز چرخ کمان گفته شد کوه برز</p></div>
<div class="m2"><p>درید آسمان از چکاکاک گرز</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>ببارید چندان نم خون ز تیغ</p></div>
<div class="m2"><p>که باران به سالی نبارد ز میغ</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>یکی بهره شد کشته زاول گروه</p></div>
<div class="m2"><p>دگر گشته از جنگ جستن ستوه</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>چنان غرقه درخون که هرکس که زیست</p></div>
<div class="m2"><p>به آوازه بشناختندی که کیست</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>به اندرز کردن همه خستگان</p></div>
<div class="m2"><p>وز آن خستگان زارتر بستگان</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>غریو از همه زار برخاسته</p></div>
<div class="m2"><p>بریده دل از جان و از خواسته</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>همی گفت هرکس برین دشت کین</p></div>
<div class="m2"><p>بکوشید تا تیره شب همچنین</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>مگر شب بدین چاره افسون کنیم</p></div>
<div class="m2"><p>سر از چنبر مرگ بیرون کنیم</p></div></div>