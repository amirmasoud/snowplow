---
title: >-
    بخش ۹۱ - رفتن گرشاسب به ساختن سیستان و اتمام آن
---
# بخش ۹۱ - رفتن گرشاسب به ساختن سیستان و اتمام آن

<div class="b" id="bn1"><div class="m1"><p>سپهبد گرفت از پدر پند یاد</p></div>
<div class="m2"><p>وزآنجا سوی سیستان رفت شاد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اسیران که از کابل آورده بود</p></div>
<div class="m2"><p>به یک جایگه گردشان کرده بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بفرمود خون همه ریختن</p></div>
<div class="m2"><p>وزیشان گل باره انگیختن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یکی نیمه بُد کرده دیوار شهر</p></div>
<div class="m2"><p>دگر نیمه کردند از آن گل دو بهر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ازآن خون به ریگ اندرون خاست مار</p></div>
<div class="m2"><p>کرا آن گزیدی بکردی فکار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو آن شهر پردخت و باره بساخت</p></div>
<div class="m2"><p>برو پنج درآهنین برنشاخت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چوباد آمدی ریگ برداشتی</p></div>
<div class="m2"><p>همه شهر و برزن بینباشتی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چنان کان برهمن ورا داد پند</p></div>
<div class="m2"><p>که از چوب و ازخاره ورغی ببند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>یله کرد ازآن سوکه بدآب مرغ</p></div>
<div class="m2"><p>ببست از سوی دامن ریگ و رغ</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زیک سوش بدریگ ده جافره</p></div>
<div class="m2"><p>دگر سوش دریا که خوانی زره</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>میانش دری باد را برگشاد</p></div>
<div class="m2"><p>ازآن پس نبد بیم اش ازریگ وباد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بُد از طوس وکرمان فراوان گروه</p></div>
<div class="m2"><p>به لشکر در از پایکاری ستوه</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>زتاراج کابل زنان داشتند</p></div>
<div class="m2"><p>به خوالیگریشان همی داشتند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>همه روز مردان ایشان دوبهر</p></div>
<div class="m2"><p>به مزدور کاری بدندی به شهر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چو گشتندی ازکار پرداخته </p></div>
<div class="m2"><p>بدندی زنان دیگ ها ساخته</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>خورش ها یکی روز بفروختند</p></div>
<div class="m2"><p>دگرباره باز آتش افروختند</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>به مردان سپردند یکسر درم</p></div>
<div class="m2"><p>همین پیشه کردند مردان به هم</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>به بازار خوالیگری ساختند</p></div>
<div class="m2"><p>شتالنگ با کعبتین باختن</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>همه کار ایشان بُدست از نخست</p></div>
<div class="m2"><p>همان از بلایه زنان کار سست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بدان در کز این کار جستند نام 	</p></div>
<div class="m2"><p>همان از بلایه زنان کار سست</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ز هر شهر و کشور بدو داد روی</p></div>
<div class="m2"><p>شد آن شهر پردخته در هفت سال</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>تو گفتی بهشتی بری سیستان</p></div>
<div class="m2"><p>یکی نیست از خرمی سیست آن</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ازو نیز برخاست مردان مرد</p></div>
<div class="m2"><p>که بُد هریکی لشکری در نبرد</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ازآن پس به شاهی سپهدار گرد</p></div>
<div class="m2"><p>نشست و به داد و دهش دست برد</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>فراوان برآمد برو سالیان</p></div>
<div class="m2"><p>هواش آنچه بُدیافت هرسالی آن</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>چنان پیلتن شدکه از گام پنج</p></div>
<div class="m2"><p>نبردش فزون هیچ اسپی به رنج</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>نشستن همه بود برزنده پیل</p></div>
<div class="m2"><p>همش پیل بارنج بردی دومیل</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>چنین آمد این گنبد تیز پوی</p></div>
<div class="m2"><p>بگردد همه چیز از گشتِ اوی</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>یکی جامه دارد جهان سال و ماه</p></div>
<div class="m2"><p>برونش سپید و درونش سیاه</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بگرداند این جامه هر گه برون</p></div>
<div class="m2"><p>بدان تا بگردیم ما گونه گون</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>توای خفته از خواب بیدار گرد</p></div>
<div class="m2"><p>که شد پاک عمرت به خواب و به خورد</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>به خانه درون خواب و در گور خواب</p></div>
<div class="m2"><p>به بیداریت پس کی آید شتاب</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>کنی خانه تا زنده ای سال و ماه</p></div>
<div class="m2"><p>درو پس کی ات باشد آرامگاه</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>تو خوش خفته و مرگ برخاسته</p></div>
<div class="m2"><p>شبیخونت را لشکر آراسته</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>به دیگر جهان دار از این جای گوش</p></div>
<div class="m2"><p>چو کوشیدی این را مر آن را بکوش</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>از ایدر بخواهی شدن بی گمان</p></div>
<div class="m2"><p>که اینجات خانست و آنجات مان</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>شود زنده این جهان مرده زود</p></div>
<div class="m2"><p>بدان جا توان جاودان زنده بود</p></div></div>