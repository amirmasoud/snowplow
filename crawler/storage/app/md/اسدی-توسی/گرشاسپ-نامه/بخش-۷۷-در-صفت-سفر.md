---
title: >-
    بخش ۷۷ - در صفت سفر
---
# بخش ۷۷ - در صفت سفر

<div class="b" id="bn1"><div class="m1"><p>پدر گفت گرت ازشدن چاره نیست</p></div>
<div class="m2"><p>بدین دیگر اندرز باری بایست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیا کس که او جُست راه دراز</p></div>
<div class="m2"><p>چو شد نیز نامد سوی خانه باز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یکی از پی مرگ و از روز تنگ</p></div>
<div class="m2"><p>دگر از پی دشمن و نام و ننگ</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شدن دانی از خانه روز نخست</p></div>
<div class="m2"><p>ولیک آمدن را ندانی درست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بلایی ز دوزخ سفر کردنست</p></div>
<div class="m2"><p>غم چیز و تیمار جان خوردنست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>درو رنج باید کشیدن بسی</p></div>
<div class="m2"><p>جفا بردن از دست هر ناکسی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به ره چون شوی هیچ تنها مپوی</p></div>
<div class="m2"><p>نخستین یکی نیک همره بجوی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کجا رفت خواهی ببر بردنی</p></div>
<div class="m2"><p>بپرهیز و مَستان ز کس خوردنی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو تنها بُوی رنج دیده بسی</p></div>
<div class="m2"><p>مده اسپ را بر نشیند کسی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مشو در ره تنگ هرگز سوار</p></div>
<div class="m2"><p>ز دزدان بپرهیز در دهگذار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مکن تیره شب آتش تابناک</p></div>
<div class="m2"><p>وگر چاره نبود فکن در مغاک</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به هر ره مشو تا ندانی درست</p></div>
<div class="m2"><p>هر آبی مخور نازموده نخست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>همی تا بود دشت و آباد جای</p></div>
<div class="m2"><p>به ویرانی اندر مکن هیچ رای</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>به کاری چو در ره درایی ز زین</p></div>
<div class="m2"><p>نخست از پس و پیش هر سو ببین</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>به هنجار ره چو درافتی ز راه</p></div>
<div class="m2"><p>همی کن به ره داغ هر پی نگاه</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>کجا گم شدی چون فرو رفت هور</p></div>
<div class="m2"><p>بر آن برنشان ستاره ستور</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>وگر جای آرام در خور بود</p></div>
<div class="m2"><p>بُوی تا گه روز بهتر بود </p></div></div>
<div class="b" id="bn18"><div class="m1"><p>به رفتن مرنجان چنان بارگی</p></div>
<div class="m2"><p>که آرد گه کار بیچارگی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ز یک روزه دو روزه ره ساختن</p></div>
<div class="m2"><p>به از اسپ کشتن ز بس تاختن</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>به هر جای از اسپ مگذار چنگ</p></div>
<div class="m2"><p>همیشه عنان دار یا پالهنگ</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>به ره خوب جایی گزین بی گزند</p></div>
<div class="m2"><p>بَر خویش دار اسپ و گرز و کمند</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>همیشه کمان بر زه آورده باش</p></div>
<div class="m2"><p>پسیچ کمین گاه‌ها کرده باش</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>پیاده ممان کت بگیرد عنان</p></div>
<div class="m2"><p>ز خود دور دارش به تیر و سنان</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ز چیز کسان وز بد انگیختن</p></div>
<div class="m2"><p>بپرهیز و ز خیره خون ریختن</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>مشو شب به شهر اندر از ره فراز</p></div>
<div class="m2"><p>بر چشمه و آب منزل مساز</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>مدار اسپ و ناآزموده رهی</p></div>
<div class="m2"><p>مکن جز که با مهربان همرهی</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>به شهری که بد باشد آب و هوا</p></div>
<div class="m2"><p>مجوی و مخور هر چت آید هوا</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>به بیماری اندیشه را تیز کن</p></div>
<div class="m2"><p>ز هر خوردنی زود پرهیز کن</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>چو بینی‌ خورش‌های‌ خوش گرد خویش</p></div>
<div class="m2"><p>بیندیش تلخی دارو ز پیش</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>مشو یار بدخواه و همکار بد</p></div>
<div class="m2"><p>که تنها بسی به که با یار بد</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>نباید که بد پیشه باشدت دوست</p></div>
<div class="m2"><p>که هرکس چنانت شمارد که اوست</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>مخور باده چندان کت اید گزند</p></div>
<div class="m2"><p>مشو مست از و خرمی کن پسند</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>مگو راز با زفت و بیچاره دل</p></div>
<div class="m2"><p>مخواه آرزو تا نگردی خجل</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>ز پنهان مردم به دل ترس دار</p></div>
<div class="m2"><p>که پنهان مردم فزون ز آشکار</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>همه جانور در جهان گونه گون</p></div>
<div class="m2"><p>برون پیسه باشند و مردم درون</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>مشو سوی رودی که نانی به در</p></div>
<div class="m2"><p>به یک ماه دیر آی و بر پل گذر</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>به گرداب در غرقگان را دلیر</p></div>
<div class="m2"><p>مگیر ار نباشی بر آن آب چیر</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>شنا بر چو بی آشنا را گرد</p></div>
<div class="m2"><p>چو زیرک نباشد نخست او مُرد</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>چو در دشمنی جایی افتدت رای</p></div>
<div class="m2"><p>درآن دشمنی دوستی را بپای</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>چنان بر سوی دوستی نیز راه</p></div>
<div class="m2"><p>که مر دشمنی را بود جایگاه</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>به دشمن چو داری به چیزی نیاز</p></div>
<div class="m2"><p>زی‌ او خوش‌ چو زی‌ دوستان سرفراز</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>گر از خواسته نام جویی و لاف</p></div>
<div class="m2"><p>بخور بی نکوهش بده بی گزاف</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>چنان خور که نایدت درد و گداز</p></div>
<div class="m2"><p>چنان بخش کت نفکند در نیاز</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>خوری و بپوشی ز روی خرد</p></div>
<div class="m2"><p>از آن به که بنهی و دشمن خورد</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>ز بهر خور و پوش باید درم</p></div>
<div class="m2"><p>چو این دو نباشد چه بیش و چه کم</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>مبر غم به چیزی که رفتت ز دست</p></div>
<div class="m2"><p>مرین را نگه‌ دار اکنون که هست</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>چو اندک بود خواسته با کسی</p></div>
<div class="m2"><p>ز رادیش زفتی نکوتر بسی</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>درم زیر خاک اندر انباشتن</p></div>
<div class="m2"><p>به از دست پیش کسان داشتن</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>به خانه در از یافتن زرّ ناب</p></div>
<div class="m2"><p>چنان است کندر جهان آفتاب</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>همه کارها را سرانجام بین</p></div>
<div class="m2"><p>چو بدخواه چینه نهد دام بین</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>مخند ار کسی را رخ از درد زرد</p></div>
<div class="m2"><p>که آگه نیی زو تو او راست درد</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>چو از سخت کاری برستی ز بخت</p></div>
<div class="m2"><p>دگر تن میفکن در آن کار سخت</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>خوی آن که نشناسی و رای اوی</p></div>
<div class="m2"><p>نهان راز و تدبیر با او مگوی</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>که گر نیک باشد بود نیکساز</p></div>
<div class="m2"><p>وگر بد بود بد سگالدت باز</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>مکن دزدی و چیز دزدان مخواه</p></div>
<div class="m2"><p>تن از طمع مکفن به زندان و چاه</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>ز دزدان هر آن کس که پذیرفت چیز</p></div>
<div class="m2"><p>به دزدی ورا زود گیرند نیز</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>چو خواهی که چیزی ندزددت کس</p></div>
<div class="m2"><p>جهان را همه دزد پندار و بس</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>به گفتار با مهتران بر مجوش</p></div>
<div class="m2"><p>به زور آنکه پیش از تو با او مکوش</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>مزن رای با تنگ دست از نیاز</p></div>
<div class="m2"><p>که جز راه بد ناردت پیش باز</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>ز بهر گلو پارسابب مکن</p></div>
<div class="m2"><p>به خوان کسان کدخدایی مکن</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>مشو یار بخت و کم بوده چیز</p></div>
<div class="m2"><p>که از شومی‌اش بهره یابی تو نیز</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>مکن خو به پُر خفتن اندر نهفت</p></div>
<div class="m2"><p>که با کاهلی خواب شب هست جفت</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>برین باش یکسر که دادمت پند</p></div>
<div class="m2"><p>گرفتش به بر دیر و بگریست چند</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>سپهبد دل از هر بدی ساده کرد</p></div>
<div class="m2"><p>بدین پند کار ره آماده کرد</p></div></div>