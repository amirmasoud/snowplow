---
title: >-
    بخش ۴۸ - گشتن گرشاسب با مهراج گرد هند
---
# بخش ۴۸ - گشتن گرشاسب با مهراج گرد هند

<div class="b" id="bn1"><div class="m1"><p>یکی مرد ملاح بُد راهبر</p></div>
<div class="m2"><p>که بودش همه راه دریا ز بر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بُد آگه که در هر جزیره چه چیز </p></div>
<div class="m2"><p>زبان همه پاک دانست نیز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به دریا هر آنجا که آب آزمای</p></div>
<div class="m2"><p>ببویید آن گل بگفت از کجای </p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو دریا به شورش گرفتی شتاب</p></div>
<div class="m2"><p>یکی طشت بودش بکردی پر آب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همه بودنی ها درو کم و بیش</p></div>
<div class="m2"><p>بدیدی چو در آینه چهر خویش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ورا رهبری داد مهراج شاه</p></div>
<div class="m2"><p>به سوی جزیری گرفتند راه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>که خوانند برطایل آنرا به نام</p></div>
<div class="m2"><p>جزیری همه جای شادی و کام </p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پر آب خوش و میوه هر سو به بار</p></div>
<div class="m2"><p>گل گونه گون گرد او صد هزار </p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز خوشی زمین چون دل شاد بود</p></div>
<div class="m2"><p>ز باران هوا چون کف راد بود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چو رنگ رخ یار شاخ از سمن</p></div>
<div class="m2"><p>چو موی سر زنگی آب از شکن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خروش رباب و نواهای نای</p></div>
<div class="m2"><p>ره چنگ و دستان بربط سرای</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>همی آمد از بیشه هر سو فراز</p></div>
<div class="m2"><p>نه گوینده پیدا نه دستان نواز</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>تو گفتی همه بیشه بزم پریست</p></div>
<div class="m2"><p>درختش ز هر سو به رامشگریست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چنان هر زمان بانگ برخاستی</p></div>
<div class="m2"><p>که می خواره را آرزو خواستی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>دل پهلوان خیره شد ز آن خروش</p></div>
<div class="m2"><p>به هر گوشه ای گشت و بنهاد گوش</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>نه کس دید و نه مرغ و دیو و پری</p></div>
<div class="m2"><p>نه کمتر شد آن بانگ رامشگری</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ز ملاح از آن بانگ پرسید باز</p></div>
<div class="m2"><p>نداند کس این گفت پیدا و راز</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>همان جا شب تیره بر دشت و راغ</p></div>
<div class="m2"><p>یکی روشنی دید همچون چراغ</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بپرسید از آن پهلوان سترگ</p></div>
<div class="m2"><p>بگفتند گاویست آبی بزرگ</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چو دم زد فتد روشنی در هوا</p></div>
<div class="m2"><p>بدان روشنایی کند شب چرا</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چنین هر شب از دور پیدا شود</p></div>
<div class="m2"><p>سپیده دمان باز دریا شود</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ز دام و دد و بوی نخچیر گیر</p></div>
<div class="m2"><p>گریزان بود بر سه پرتاب تیر</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ببودند روزی وز آن جایگاه</p></div>
<div class="m2"><p>کشیدند سوی صواحل سپاه</p></div></div>