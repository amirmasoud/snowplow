---
title: >-
    بخش ۹۴ - رزم گرشاسب با منهراس
---
# بخش ۹۴ - رزم گرشاسب با منهراس

<div class="b" id="bn1"><div class="m1"><p>گرفتند از آنجای راهِ دراز</p></div>
<div class="m2"><p>جزیری پدید آمد از دور باز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یکی مرد پویان ز بالا به پست</p></div>
<div class="m2"><p>خروشان گلیمی فشانان به دست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو دیدند بُد ز اندلس مهتری</p></div>
<div class="m2"><p>به پرسش گرفتندش از هر دری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چنین گفت کز بخت روز نژند</p></div>
<div class="m2"><p>مرا باد کشتی بایدر فکند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ازین کُه دمان نره دیوی شگفت</p></div>
<div class="m2"><p>برون آمد و کشتی ما گرفت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دو صد مرده بودیم نگذاشت کس</p></div>
<div class="m2"><p>همه خورد و من مانده‌ام زنده بس</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سپهبد مرورا به کشتی نشاست</p></div>
<div class="m2"><p>به کین جستن دیو خفتان بخواست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گرفتند لشکر به یک ره خروش</p></div>
<div class="m2"><p>که او منهراس است با او مکوش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>که با خشم چشم ار بر آغالدت</p></div>
<div class="m2"><p>به یک دَم همه زور بفتالدت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دژ آگاه دیوی بد و منکرست</p></div>
<div class="m2"><p>به بالا چهل رش ز تو برترست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به سنگی کند با زمین پست کوه</p></div>
<div class="m2"><p>سپاه جهان گردد از وی ستوه</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چو غرد برد هوش و جان از هژبر</p></div>
<div class="m2"><p>ز دندان درخش آیدش وز دم ابر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به جستن بگیرد ز گردون عقاب</p></div>
<div class="m2"><p>نهنگ آرد از ژرف دریای آب</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بدین کوه شهری بدُست استوار</p></div>
<div class="m2"><p>درو کودک و مرد و زن بی‌شمار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ز مردم وی آن شهر پرداختست</p></div>
<div class="m2"><p>نشمین به غاری درون ساختست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چو بیند یکی کشتی از دور راه</p></div>
<div class="m2"><p>بگیرد کند مردمان را تباه</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ز دریا نهنگ او به خشکی برد</p></div>
<div class="m2"><p>به خورشید بریان کند پس خورد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چو جان شد به در باز ناید ز پس</p></div>
<div class="m2"><p>ز مادر دوباره نزادست کس</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>سپهدار گفت از من آغاز کار</p></div>
<div class="m2"><p>خود این رزم کرد آرزو شهریار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ازین زشت پتیاره چندین چه باک</p></div>
<div class="m2"><p>همین دم ز کوهش کشم در مغاک</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>جز از بیم جان گر دگر نیست چیز</p></div>
<div class="m2"><p>چنان چون مرا جان و راهست نیز</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>به شیری توان شیر کردن شکار</p></div>
<div class="m2"><p>به گرد سواران رسد هم سوار</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بسی لابه کردند و نشنید گرد</p></div>
<div class="m2"><p>پیاده برون رفت و کس را نبرد</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>همی گشت بر گرد آن کوه برز</p></div>
<div class="m2"><p>به بازو  کمان و به کف تیغ و گرز</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>به ناگه بدان دیوش افتاد چشم</p></div>
<div class="m2"><p>ورا دید در ژرف غاری به خشم</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>یکی جانور گونه پر جنگ و جوش</p></div>
<div class="m2"><p>که هرکش بدیدی برفتی ز هوش</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>چو شیرانش چنگال و چون غول روی</p></div>
<div class="m2"><p>به کردار میشان همه تنش موی</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>دو گوشش چون دو پرده پهن و دراز</p></div>
<div class="m2"><p>برون رسته دندان چو یشک گراز</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>ستبری دو بازو مه از ران پیل</p></div>
<div class="m2"><p>رخش زرد و دیگر همه تن چو نیل</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>همی ریخت غاز از غرنبیدنش</p></div>
<div class="m2"><p>همی شد نوان کُه ز جنبیدنش</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>ز صدرش فزون ماهی خورده بود</p></div>
<div class="m2"><p>ز پیش استخوان‌هاش گسترده بود</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>دل شیر جنگی برآورد شور</p></div>
<div class="m2"><p>به یزدان پناهید و زو خواست زور</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>گشاد از خم چرخ تیری به خشم</p></div>
<div class="m2"><p>زدش بر قفا برد بیرون ز چشم</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>غریوی برآمد از آن نره دیو</p></div>
<div class="m2"><p>که برزد به هم غار و که ز آن غریو</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>دمان تاخت کآید به بالا ز زیر</p></div>
<div class="m2"><p>دَرِ غاز بگرفت گرد دلیر</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>به خنجر یکی پنجه بنداختش</p></div>
<div class="m2"><p>در آن غاز هر سو همی تاختش</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>به هر گوشه کز غاز سر بر زدی</p></div>
<div class="m2"><p>یکی گرزش او زود بر سر زدی</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>فغانی ز دیو و خروشی از اوی</p></div>
<div class="m2"><p>به خون غرقه دیو و به خوی جنگجوی</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>نبودش برون راه کآید به جنگ</p></div>
<div class="m2"><p>برو بر شد آن غار زندان تنگ</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>ز خونش که شد در هوا شاخ شاخ</p></div>
<div class="m2"><p>همی لاله رُست از شخ سنگلاخ</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>خروشش همی برگذشت از سپهر</p></div>
<div class="m2"><p>دَمش آتش و دود بر زد به مهر</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>چو بیچاره شد کوه کندن گرفت</p></div>
<div class="m2"><p>ز بر سنگ خارا فکندن گرفت</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>به هر سنگ کافکندی از خشم و کین</p></div>
<div class="m2"><p>هوا تیره گردید و لرزان زمین</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>گرفته رهش پهلوان سپاه</p></div>
<div class="m2"><p>همی داشت از سنگ او تن نگاه</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>گهی گرز کین کوفتش گاه سنگ</p></div>
<div class="m2"><p>در آن غار کرده برو راه تنگ</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>سرانجان سنگی گران از برش</p></div>
<div class="m2"><p>فرو هشت کافشاند خون از سرش</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>تن نیلگونش و شی پوش گشت</p></div>
<div class="m2"><p>چو کوهی بیفتاد و بی‌هوش گشت</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>سبک پهلوان پیش کآید به هوش</p></div>
<div class="m2"><p>به غار اندرون رفت چون شیر زوش</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>دو دست و دو پایش به خم کمند</p></div>
<div class="m2"><p>فرو بست و دندانش از بُن بکند</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>گزید از سپه مرد بیش از شمار</p></div>
<div class="m2"><p>به کشتیش بردند از آن ژرف غار</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>همی غرقه شد کشتی از بار اوی</p></div>
<div class="m2"><p>سپه خیره یکسر ز دیدار اوی</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>رسن‌های کشتی جدا هر کسی</p></div>
<div class="m2"><p>ببستند بر دست و پایش بسی</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>چو هُش یافت هرگاه گشتی دمان</p></div>
<div class="m2"><p>گسستی فراوان رسن هر زمان</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>زدی نعره‌ای سهمگین کز خروش</p></div>
<div class="m2"><p>شدی کوه جنبان و دریا به جوش</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>جهان پهلوان پیش دادآفرین</p></div>
<div class="m2"><p>بسی کرد با مهر یاد آفرین</p></div></div>