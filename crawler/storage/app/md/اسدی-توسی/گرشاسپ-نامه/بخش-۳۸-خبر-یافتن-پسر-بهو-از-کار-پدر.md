---
title: >-
    بخش ۳۸ - خبر یافتن پسر بهو از کار پدر
---
# بخش ۳۸ - خبر یافتن پسر بهو از کار پدر

<div class="b" id="bn1"><div class="m1"><p>وز آنسو چو پور بهو رفت پیش</p></div>
<div class="m2"><p>به شهر سرندیب با عم خویش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همی ساخت بر کشتن عم کمین</p></div>
<div class="m2"><p>نهان عم به خون جستنش همچنین</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سرانجام کار آن پسر یافت دست</p></div>
<div class="m2"><p>عمش را کشت و به شاهی نشست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پس آگاهی آمد ز مهراج شاه</p></div>
<div class="m2"><p>ز درد پدر گشت روزش سیاه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یکی هفته بنشست با سوک و درد</p></div>
<div class="m2"><p>سر هفته لشکر همه گرد کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بسی گنج زر و درم برفشاند</p></div>
<div class="m2"><p>صد و بیست کشتی سپه در نشاند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سپهدار جنگ آور رزم ساز</p></div>
<div class="m2"><p>فرستادش از پیش مهراج باز</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو رفتند نیمی ره از بیش و کم</p></div>
<div class="m2"><p>سپه باز خوردند هر دو به هم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سبک بست گرشاسب کین را میان</p></div>
<div class="m2"><p>همان شست کشتی از ایرانیان </p></div></div>
<div class="b" id="bn10"><div class="m1"><p>همه خنجر و نیزه برداشتند</p></div>
<div class="m2"><p>ز کیوان غو کوس بگذاشتند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چنان گشت کشتی که در کارزار</p></div>
<div class="m2"><p>به زخم سوار اندر آمد سوار </p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به هر سو دژی خاست تا زان به جنگ </p></div>
<div class="m2"><p>ازو خشت بارنده و تیر و سنگ</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ز تف سر تیغ وز عکس آب</p></div>
<div class="m2"><p>همی در هوا گشت کرکس کباب</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چنان تیر بارید هر گرد گیر</p></div>
<div class="m2"><p>که هر ماهیی ترکشی شد ز تیر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>همی موج بر اوج مه راه زد</p></div>
<div class="m2"><p>ز ماهی تن کشته بر ماه زد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>از آتش همه روی دریا به چهر</p></div>
<div class="m2"><p>چنان شد که شب از ستاره سپهر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>شد از خون تن ماهیان لعل پوش</p></div>
<div class="m2"><p>دل میغ زد ز آب شنگرف جوش</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چنان بود موج از سر بیشمار</p></div>
<div class="m2"><p>که گرد چمن میوه بارد ز بار </p></div></div>
<div class="b" id="bn19"><div class="m1"><p>همی رفت هر کشتیی سرنگون</p></div>
<div class="m2"><p>درآویخته بادبان پُر ز خون </p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چو اسپان جنگی دوان خیل خیل</p></div>
<div class="m2"><p>برافکنده از لعل دیبا جلیل</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>سپهدار با خیل زاول گروه</p></div>
<div class="m2"><p>به پیش اندر آورده کشتی چو کوه</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چپ و راست تیغ ارغوان بار کرد</p></div>
<div class="m2"><p>به هر کشتی از کشته انبار کرد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>به یک ساعت از گرز یکماهه بیش </p></div>
<div class="m2"><p>همی ماهیان را خورش داد پیش</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ز کشتی به کشتی همی شد چو گرد</p></div>
<div class="m2"><p>همی کوفت گرز و همی کشت مرد</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چنین تا به جنگاوه جنگجوی</p></div>
<div class="m2"><p>رسید از کمین کرد آهنگ اوی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>سرش را به گرز گران کوفت خرد</p></div>
<div class="m2"><p>تنش را به کام نهنگان سپرد</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>یلان ز آتش رزم و از بیم تاب</p></div>
<div class="m2"><p>همی تن فکندند هر سو در آب</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>چهل کشتی از موج باد شگرف</p></div>
<div class="m2"><p>ز دشمن نگون شد به دریای ژرف</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>دگر در گریز آن کجا مانده بود</p></div>
<div class="m2"><p>نهادند سر زی سرندیب زود</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>گرفتند سی کشتی ایران سپاه</p></div>
<div class="m2"><p>بکشتند هر کس که بد کینه خواه</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>همه بادبان ها برافراشتند</p></div>
<div class="m2"><p>به دمّ گریزنده برداشتند</p></div></div>