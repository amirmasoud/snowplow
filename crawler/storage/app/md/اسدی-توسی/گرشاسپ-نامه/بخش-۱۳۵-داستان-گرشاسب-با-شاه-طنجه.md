---
title: >-
    بخش ۱۳۵ - داستان گرشاسب با شاه طنجه
---
# بخش ۱۳۵ - داستان گرشاسب با شاه طنجه

<div class="b" id="bn1"><div class="m1"><p>کنون از شه طنجه و پهلوان</p></div>
<div class="m2"><p>شنو کار کین جستن هر دوان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بدان گه که از نزد ضحاک شاه</p></div>
<div class="m2"><p>سوی طنجه شد پهلوان سپاه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز دریا و خشک آنچه آورده بود</p></div>
<div class="m2"><p>به دست شه طنجه بسپرده بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>که تا باز خواهد چه آرد هوا</p></div>
<div class="m2"><p>بدین کرده بد مرد چندی گوا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سرآمد مر آن شاه را روزگار</p></div>
<div class="m2"><p>پسرش از پس او شده شهریار </p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پسر نیز رفته به راه پدر</p></div>
<div class="m2"><p>نبیره ببسته به جایش کمر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چنان بود رای شه سرفراز</p></div>
<div class="m2"><p>که آن خواسته خواهد از طنجه باز</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بر این کار پوینده ای کرد راست</p></div>
<div class="m2"><p>ز شاه کیان هم بدین نامه خواست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شه طنجه را طمع بربود و گفت</p></div>
<div class="m2"><p>که این آگهی با دلم نیست جفت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گذشتست از این کار سالی دویست</p></div>
<div class="m2"><p>مرا سال نیز از چهل بیش نیست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چنین دام هرگز مگستر به راه</p></div>
<div class="m2"><p>ز گنجم گرت رای چیزیست خواه</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نهی پایت از پایه بیرون همی</p></div>
<div class="m2"><p>که خرگوش گیری به گردون همی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>سپهبد بدانست کآنست رنگ</p></div>
<div class="m2"><p>به جنگ آید آن خواسته باز چنگ</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ده و دو هزار از سران سپاه</p></div>
<div class="m2"><p>گزید و برون شد به فرمان شاه</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>به فرخ نریمان چنین کرد یاد</p></div>
<div class="m2"><p>که کارت همه راه دین باد و داد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گر آیم من ار نه به هر بیش و کم</p></div>
<div class="m2"><p>مزن جز به رای شهنشاه دم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ببوسیدش از مهر و لشکر کشید</p></div>
<div class="m2"><p>خبر چون بر شاه طنجه رسید</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>پراکند بس گنج و کین کرد ساز</p></div>
<div class="m2"><p>بی اندازه آورد لشکر فراز</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>شد از بس که بودش سپاه گران</p></div>
<div class="m2"><p>زمین چون سپهر از کران تا کران</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>برآمد سپهدار با لشکرش </p></div>
<div class="m2"><p>ز گرد ابر بست از بر کشورش</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بر طنجه نزدیک یک روز راه</p></div>
<div class="m2"><p>به گرد دهی خیمه زد با سپاه </p></div></div>
<div class="b" id="bn22"><div class="m1"><p>مه ده یکی پیر بد نامجوی</p></div>
<div class="m2"><p>بسی سال پیموده گردون بدوی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>فراوان ز نزل و علف برشمرد</p></div>
<div class="m2"><p>همه برد نزد سپهدار گرد</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>از آن خواسته گفت دارم خبر</p></div>
<div class="m2"><p>که در طنجه بنهادی از پیشتر</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>برادرم زندست و با من گواست</p></div>
<div class="m2"><p>در آن نامه هم نام و هم خط ماست</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>از آن شاد شد پهلوان چون شنود</p></div>
<div class="m2"><p>سوی طنجه شه نامه ای ساخت زود</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>سر نامه کرد از جهاندار یاد</p></div>
<div class="m2"><p>خداوند دین و خداوند داد</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>فرازنده هفت چرخ سپهر</p></div>
<div class="m2"><p>فروزنده گیتی از ماه و مهره</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>دگر گفت کای گمره از کردگار</p></div>
<div class="m2"><p>چه طمع است کاندر دلت کرد کار</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بود نزد فرزانه کمتر کس آن </p></div>
<div class="m2"><p>که خیره کند طمع چیز کسان</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>نکوتر بود نام زفتی بسی</p></div>
<div class="m2"><p>ز خوانی که با طمع بنهد کسی</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>همانا به چشمت هزاک آیدم</p></div>
<div class="m2"><p>و یا چون تو ابله فغاک آیدم</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>کزینسان سخن های غاب آوری</p></div>
<div class="m2"><p>همی چشم دل را به خواب آوری</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>کرا رنگ چهره سیه تر ز زنگ</p></div>
<div class="m2"><p>بدو کی پدید آید از شرم رنگ</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>هنرهام هر کس شنیدست و دید</p></div>
<div class="m2"><p>تو از ابلهی چون کنی ناپدید</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>کجا من شتاب آورم بر درنگ</p></div>
<div class="m2"><p>نوند زمان را شود پای لنگ</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>اگر بر زمین برزنم بانگ تیز</p></div>
<div class="m2"><p>جهد مرده از گور بی رستخیز</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>به گهواره در هند کودک خروش</p></div>
<div class="m2"><p>چو گیرد به نامم نباشد خموش</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>به چین آتشی کاید از آسمان</p></div>
<div class="m2"><p>برند از تف تیغ تیزم گمان</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>یکی خواسته کان جهان را بهاست</p></div>
<div class="m2"><p>چو من گردی آورده از چپ و راست</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>چو در گنجت ای زاغ رخ تیره روز</p></div>
<div class="m2"><p>نهفتی چو اندر زمین زاغ کوز</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>کنون گویی آگه نی ام ز آن درست</p></div>
<div class="m2"><p>همه کس شناسند کآن نزد تست</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>سرانت گواه اند بسیار و من</p></div>
<div class="m2"><p>فرستادم اینک به نزدت دو تن</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>اگر چند باشند بسیار کس</p></div>
<div class="m2"><p>گوا نزد داور دو آرند و بس</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>اگر باز بفرستی آن خواسته</p></div>
<div class="m2"><p>زان هم که بودست آراسته</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>هم از من بود پایه ات نزد شاه</p></div>
<div class="m2"><p>هم از شاه یابی بزرگی و جاه</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>وگر ناوری آنچه رای آورم</p></div>
<div class="m2"><p>سرو افسرت زیر پای آورم</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>بر از چرخ کیوان گر ایوان تست</p></div>
<div class="m2"><p>وگر نام دیوان به دیوان تست</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>سرت را ز گرودن به گرد آورم</p></div>
<div class="m2"><p>دل دوستانت به درد آورم</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>پیمبر براهیم بود آن زمان</p></div>
<div class="m2"><p>بدش نام زردشت از آسمان</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>به صحفش بر این خورد سوگند نیز</p></div>
<div class="m2"><p>بدان دو گوا داد بسیار چیز</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>به هم با فرستاده شان رنجه کرد</p></div>
<div class="m2"><p>فرستاده آهنگ زی طنجه کرد</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>چو شه نامه برخواند آن هر دو تن</p></div>
<div class="m2"><p>گوایی بدادند بر انجمن</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>جز ایشان گوا بود دیگر بسی</p></div>
<div class="m2"><p>ولیکن نیارست دم زد کسی</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>دژم زی فرسته شه آورد روی</p></div>
<div class="m2"><p>بدو گفت رو پهلوان را بگوی</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>چو دیوار بر برف سازی نخست</p></div>
<div class="m2"><p>نگون زود گردد به بنیاد سست</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>نه هرچ آن بگویند باشد همان</p></div>
<div class="m2"><p>بر راست گم زود گردد گمان</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>به مردی و گنج و سپاه از تو کم</p></div>
<div class="m2"><p>نی ام چیست این طمع پر باد و دم</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>نبودی مرا در جوانی همال</p></div>
<div class="m2"><p>کنون چون بوی کت بفرسود سال</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>یکی مویم افتاد در کار زار</p></div>
<div class="m2"><p>اگر بینی از بیمت آید چومار</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>مرا با شهنشاه از این نیست جنگ</p></div>
<div class="m2"><p>به جنگم تویی آمده تیز چنگ</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>فرستادگان را به خواری براند</p></div>
<div class="m2"><p>دو ره صد هزار از یلان را بخواند</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>در آهن بیاراست صد زنده پیل</p></div>
<div class="m2"><p>ز طنجه برون خیمه زد بر دو میل</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>بد از سرفرازان یکی کینه توز</p></div>
<div class="m2"><p>سپهدار او بود نامش متوز</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>ز لشکرش نیمی بدو داد بیش</p></div>
<div class="m2"><p>ز بهر نبردش فرستاد پیش</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>فرسته خبر زی سپهدار برد</p></div>
<div class="m2"><p>سپهبد سبک دست پیکار برد</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>بیآورد نزدیک دشمن سپاه</p></div>
<div class="m2"><p>به جنگ اندر آمد هم از گرد راه</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>طلایه بزد بر طلایه نخست</p></div>
<div class="m2"><p>به خون هر سوی غرقه شد بوم و رست</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>به پیچش گرفتند گردان عنان</p></div>
<div class="m2"><p>سوی سینه ها راست کرده سنان</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>توگفتی ز بس گرد بالا و پست</p></div>
<div class="m2"><p>که هامون به گردون درآورد دست</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>یکی ژرف دریا شد از خون زمین</p></div>
<div class="m2"><p>که بد نزد او چشمه دریای چین</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>زمانه زمین را همی خون گریست</p></div>
<div class="m2"><p>ستاره ندانست رفتن که چیست</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>گرفتند زاول گروه بی شمار</p></div>
<div class="m2"><p>سلیح و ستور اندر آن کار زار</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>چو چرخ شب آرایش از سر گرفت</p></div>
<div class="m2"><p>ز ماه تمام آینه برگرفت</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>فرو هشت زلفین مشکین نگون</p></div>
<div class="m2"><p>ز زر خال زد بر رخ نیلگون</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>نفرمود پیکار دیگر متوز</p></div>
<div class="m2"><p>که شد گاه آورد و بگذشت روز</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>به گردان فرستان گرد سپاه</p></div>
<div class="m2"><p>که دارید امشب شبیخون نگاه</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>کمین ساخت هر جای بالای و شیب</p></div>
<div class="m2"><p>سپاهش کس آن شب نخفت از نهیب</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>همه شب ز بیم شبیخون متوز</p></div>
<div class="m2"><p>همی بود بیدار تا گشت روز</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>چو بازی برآورد چرخ روان</p></div>
<div class="m2"><p>به زرین و سیمین دو گوی دوان</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>یکی گوی سیمین فرو برد سر</p></div>
<div class="m2"><p>دگر گوی زرین برآورد سر</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>دو لشکر سنان ها برافراختند</p></div>
<div class="m2"><p>کمینگه گرفتند و صف ساختند</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>زمین را سپهر از گرانی سپاه</p></div>
<div class="m2"><p>نداند همی داشت گفتی نگاه</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>جهان پهلوان درع گردی چو گرد</p></div>
<div class="m2"><p>بپوشید و بگرفت گرز نبرد</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>بر او هفتصد سال بگذشته بود</p></div>
<div class="m2"><p>ز گشت سپهری کهن گشته بود</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>خروشید گفتا مرا خیره خیر</p></div>
<div class="m2"><p>ز بیغاره دشمن کهن خواند و پیر</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>کنون به کنم رزم و کوشش ز بن</p></div>
<div class="m2"><p>که بهتر کند کار تیغ کهن</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>کهن بهتر از رنگ یاقوت و زر</p></div>
<div class="m2"><p>همیدون می از نو کهن نیکتر</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>مرا گشت چرخ ارچه خم داد پشت</p></div>
<div class="m2"><p>همان بیش زورم به زخم درشت</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>بگفت این و با لشکر از چپ و راست</p></div>
<div class="m2"><p>به جنگ آمد و گرد کوشش بخاست</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>پر از بومهن شد سراسر جهان</p></div>
<div class="m2"><p>ستاره هویدار و گردون نهان</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>ز بس در زمین از تف نعل تاب</p></div>
<div class="m2"><p>به دریای قلزم به جوش آمد آب</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>همی تا دو صد میل در که خروش</p></div>
<div class="m2"><p>فتادی و باز آمدی باز گوش</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>زبر آسمانی بد از تیره گرد</p></div>
<div class="m2"><p>زمین زیر دریا بد از خون مرد</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>سواران در آن ژرف دریا نوان</p></div>
<div class="m2"><p>چو کشتی درفش از برش بادبان</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>پر از دام هامون ز خم کمند</p></div>
<div class="m2"><p>به هر دام درمانده گردی به بند</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>شده لعل گرد از دم خون و تیغ</p></div>
<div class="m2"><p>چو گاه شب از عکس خورشید میغ</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>ز بس کاینه بد درفشان ز پیل</p></div>
<div class="m2"><p>همی خاست آتش ز دریای نیل</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>سپهدار با گرز و نیزه به چنگ</p></div>
<div class="m2"><p>پیاده همی تاخت هر سو به جنگ</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>به هر گنبدی جست پنجاه گام</p></div>
<div class="m2"><p>همی کوفت گرز و همی گفت نام</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>گهی دوخت با سینه خرطوم پیل</p></div>
<div class="m2"><p>گهی ریخت خون همچون دریای نیل</p></div></div>
<div class="b" id="bn102"><div class="m1"><p>چه خیل پیاده چه خیل سوار</p></div>
<div class="m2"><p>ز بد خواه چندان بیفکند خوار</p></div></div>
<div class="b" id="bn103"><div class="m1"><p>که مر مرگ را گشت چنگال سست</p></div>
<div class="m2"><p>شد از دست او پیش یزدان نخست</p></div></div>
<div class="b" id="bn104"><div class="m1"><p>به درعش در از زخم مردان جنگ</p></div>
<div class="m2"><p>به هر حلقه در بود تیری خدنگ</p></div></div>
<div class="b" id="bn105"><div class="m1"><p>شل و ناوک و تیر در مغفرش</p></div>
<div class="m2"><p>فزون زانبه موی بد بر سرش</p></div></div>
<div class="b" id="bn106"><div class="m1"><p>که و دشت پر کشته بد پیش و پس</p></div>
<div class="m2"><p>چنین تا شب از رزم ناسود کس</p></div></div>
<div class="b" id="bn107"><div class="m1"><p>شب تیره چون شعر بافنده گشت</p></div>
<div class="m2"><p>کبود و سیه بافت بر کوه و دشت</p></div></div>
<div class="b" id="bn108"><div class="m1"><p>مراین را به زر پود در تار زد</p></div>
<div class="m2"><p>مر آن را به مشک آب آهار زد</p></div></div>
<div class="b" id="bn109"><div class="m1"><p>در جنگ هر دو سپه شد فراز</p></div>
<div class="m2"><p>به سوی سپه پهلوان گشت باز</p></div></div>
<div class="b" id="bn110"><div class="m1"><p>ز خون دید هر جای جویی روان</p></div>
<div class="m2"><p>همی هر کسی گفت با پهلوان</p></div></div>
<div class="b" id="bn111"><div class="m1"><p>که فردا اگر پیشت آید متوز</p></div>
<div class="m2"><p>نخستین جز از وی ز کس کین متوز</p></div></div>
<div class="b" id="bn112"><div class="m1"><p>که سالار این بیکران لشکر اوست</p></div>
<div class="m2"><p>برین شهسواران خاور سر اوست</p></div></div>
<div class="b" id="bn113"><div class="m1"><p>درفشش نهنگست و خفتان پلنگ</p></div>
<div class="m2"><p>سیاه اسپ و برگستوان لعل رنگ</p></div></div>
<div class="b" id="bn114"><div class="m1"><p>ز پولاد و در آژده مغفرش</p></div>
<div class="m2"><p>پرندین نشان بسته اندر سرش</p></div></div>
<div class="b" id="bn115"><div class="m1"><p>نبرده درفشش برون سپاه</p></div>
<div class="m2"><p>بیاید بود هر سوی کینه خواه</p></div></div>
<div class="b" id="bn116"><div class="m1"><p>برون آمد امروز تند از کمین</p></div>
<div class="m2"><p>فراوان سران زد زما بر زمین</p></div></div>
<div class="b" id="bn117"><div class="m1"><p>ندیدیم جز تو چنان نیز گرد</p></div>
<div class="m2"><p>به زور تن و مردی و دستبرد</p></div></div>
<div class="b" id="bn118"><div class="m1"><p>جهان پهلوان گفت کامروز جنگ</p></div>
<div class="m2"><p>چو شد تیز جستمش نآمد به چنگ</p></div></div>
<div class="b" id="bn119"><div class="m1"><p>چو خور تیغ رخشان ز تاری نیام</p></div>
<div class="m2"><p>کشد گردد از خون شب لعل فام</p></div></div>
<div class="b" id="bn120"><div class="m1"><p>هر آنجا که فردا به چنگ آرمش</p></div>
<div class="m2"><p>به یک دم زدن زنده نگذارمش</p></div></div>
<div class="b" id="bn121"><div class="m1"><p>وز آن سو سپه با متوز دلیر</p></div>
<div class="m2"><p>سخن راندند از سپهدار چیر</p></div></div>
<div class="b" id="bn122"><div class="m1"><p>که گفتند گرشاسب پیرست و سست </p></div>
<div class="m2"><p>جوان کی تواند چنان رزم جست</p></div></div>
<div class="b" id="bn123"><div class="m1"><p>کنون تیز دندان تر آمد به جنگ</p></div>
<div class="m2"><p>که دندان نماندستش از بس درنگ</p></div></div>
<div class="b" id="bn124"><div class="m1"><p>کجا جستی از جای و جستی ستیز</p></div>
<div class="m2"><p>چو آتش بدی تند و چون باد تیز</p></div></div>
<div class="b" id="bn125"><div class="m1"><p>فکندی به هر زخم پیلی نگون</p></div>
<div class="m2"><p>بکشتی به هر حمله ده تن فزون</p></div></div>
<div class="b" id="bn126"><div class="m1"><p>گرفتی دم اسپ و بفراختی</p></div>
<div class="m2"><p>به هم با سوارش بینداختی</p></div></div>
<div class="b" id="bn127"><div class="m1"><p>متوز جفا پیشه گفت این نبرد</p></div>
<div class="m2"><p>همه سخت از آن باد بودست و گرد</p></div></div>
<div class="b" id="bn128"><div class="m1"><p>چو گردد شب از تیرگی نا امید</p></div>
<div class="m2"><p>سپیده برآرد درفش سپید</p></div></div>
<div class="b" id="bn129"><div class="m1"><p>من و گرز و گرشاسب و آوردگاه</p></div>
<div class="m2"><p>سرش بر سنان آورم پیش شاه</p></div></div>