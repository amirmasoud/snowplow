---
title: >-
    بخش ۶۶ - شگفتی جزیرۀ بند آب
---
# بخش ۶۶ - شگفتی جزیرۀ بند آب

<div class="b" id="bn1"><div class="m1"><p>چو رفتند یک ماه دیگر به کام</p></div>
<div class="m2"><p>یکی کوه دیدند بندآب نام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حصاری بر آن کُه ز جزع سیاه</p></div>
<div class="m2"><p>بلندیش بگرفته بر ماه راه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به زیر درش نردبانی ز سنگ</p></div>
<div class="m2"><p>درازاش سی پایه پهناش تنگ</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مه از پیل بر نردبان یک سوار</p></div>
<div class="m2"><p>گرفته در حصن را رهگذر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یکی دست او بر عنان ساخته</p></div>
<div class="m2"><p>دگر زی سرین ستور آخته</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بپرسید ملاح را پهلوان</p></div>
<div class="m2"><p>که از چیست این اسپ و این نردوان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چنین گفت کاین را نهان ز اندرون</p></div>
<div class="m2"><p>طلسمست کآن کس نداد که چون</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>برین نردبان هر که بنهاد پای</p></div>
<div class="m2"><p>به سنگ این سوارش رباید ز جای</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>یکی را به خفتان و درع و سپر</p></div>
<div class="m2"><p>فرستاد تا بر شود بر زبر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نخستین که بر پایه رفت ای شگفت</p></div>
<div class="m2"><p>سوار از بر اسپ جنبش گرفت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بزد نعره و سنگی انداخت زیر</p></div>
<div class="m2"><p>که شد مرد بی هوش و بفتاد دیر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دگر شد یکی گردن افراخته</p></div>
<div class="m2"><p>یکی تنگ پنبه سپر ساخته</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چنان سنگی آمدش کز جای خویش</p></div>
<div class="m2"><p>نگون از پس افتاده ده گام پیش</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>به هر پایه هر سنگ کآمد ز بر</p></div>
<div class="m2"><p>به ده من گرانتر بدی زآن دگر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چنین تا ز یک پایه بر چار شد</p></div>
<div class="m2"><p>دو تن کشته آمد دو افکار شد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>کسی بر نشد نیز و پس پهلوان</p></div>
<div class="m2"><p>بفرمود کندن بُن نردوان</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چهی ژرف دیدند صد باز راه</p></div>
<div class="m2"><p>یکی چرخ گردنده بُده در به چاه</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ز چه سار زنجیری آویخته</p></div>
<div class="m2"><p>همه زرّ و با گوهر آمیخته</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>سر حلقه در خم چرخ استوار</p></div>
<div class="m2"><p>دگر سر کمر بر میان سوار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>شکستند چرخ و به چه درفکند</p></div>
<div class="m2"><p>گسستند زنجیر یکسر ز بند</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>همان گه نگون شد سوار از فراز</p></div>
<div class="m2"><p>درِ بسته حصن شد زود باز</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>سپهدار با ویژگان سپاه</p></div>
<div class="m2"><p>درون رفت و کردند هر سو نگاه</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>سرایی بُد از رنگ همچون بهار</p></div>
<div class="m2"><p>زگرد وی ایوان بلورین چهار</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ز هر پیکری جانور بیکران</p></div>
<div class="m2"><p>از ایوان برآویخته پیکران</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ز دیو و ز مردم ز پیل و نهنگ</p></div>
<div class="m2"><p>ز نخچیر و از مرغ و شیر و پلنگ</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>هم از خم آن طاق ها سرنگون</p></div>
<div class="m2"><p>نگاریده ازگوهر گونه گون</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>تو گفتی کنون کرده اند از نهاد</p></div>
<div class="m2"><p>نه نم دیده زابر و نه گردی زباد</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>از آن گوهران درهم افتاده تاب</p></div>
<div class="m2"><p>جهان کرده روشنتر از آفتاب</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بسی شمع بر هر سوی از لاژورد</p></div>
<div class="m2"><p>دو یاقوت بر هر یکی سرخ و زرد</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>به روز آن گهرها چو بشگفته باغ</p></div>
<div class="m2"><p>به شب هر یکی همچو روشن چراغ</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>ز پیش هر ایوان درختی ز زر</p></div>
<div class="m2"><p>زبرجد برو برگ و یاقوت بر</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>یکی تخت بر سایه هر درخت</p></div>
<div class="m2"><p>ز گوهر همه پایه و روی تخت</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>زمین جزع یک پاره هموار بود</p></div>
<div class="m2"><p>چنان کاندرو چهره دیدار بود</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>یکی خانه دیدند از لاژورد</p></div>
<div class="m2"><p>برآورده از شفشه زر زرد</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>چو زلف بتان شفشها تافته</p></div>
<div class="m2"><p>سراسر به یاقوت و دُر بافته</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>یکی پهن تابوت زرین دروی</p></div>
<div class="m2"><p>جهان زو چو از مشک بگرفته بوی</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>بفرمود گرشاسب کآنرا ز جای</p></div>
<div class="m2"><p>بیارند بیرون میان سرای</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>نبد هیچکس رابه تابوت دست</p></div>
<div class="m2"><p>هر آن کسکه شد نزدش افتاد پست</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>وگر زآن گهرها ببردی کسی</p></div>
<div class="m2"><p>ندیدی ره ار چند جُستی بسی</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>به دیگر یکی خانه رفتند باز</p></div>
<div class="m2"><p>به زیر زمین کرده راهی دراز</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>همه خانه بُد سنگ همرنگ نیل</p></div>
<div class="m2"><p>درو چشمه آب زرین دو میل</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>به هر میل بر مهره ای از بلور</p></div>
<div class="m2"><p>برو گوهری چون درفشنده هور</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>گهرها فروزان در آب از فراز</p></div>
<div class="m2"><p>وزو نور داده همه خانه باز</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>برِچشمه تختی و مردی بروی</p></div>
<div class="m2"><p>بمرده به چادر نهنبیده روی</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>یکی لاژوردینش لوحی زبر</p></div>
<div class="m2"><p>بر آن لوح سی خط نبشته به زر</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>سپهبد به ملاح گفت این بخوان</p></div>
<div class="m2"><p>چو بر خواند گشتش زریری رخان</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>نبشته چنین بُد که هر کز خرد</p></div>
<div class="m2"><p>بدینجای آرام من بنگرد</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>سزد گر ز مهر سرای سپنج</p></div>
<div class="m2"><p>بتابد دل و تن ندارد به رنج</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>منم پور هوشنگ شاه بلند</p></div>
<div class="m2"><p>جهاندار طهمورث دیو بند</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>حصار و طلسمی چنین ساختم</p></div>
<div class="m2"><p>بسی گوهر و گنج پرداختم</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>اگر بنگری کمترین گوهری</p></div>
<div class="m2"><p>بها بیشتر دارد از کشوری</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>به چندین گهر در سپنجی سرای</p></div>
<div class="m2"><p>چو من شه نمادم که ماند به جای</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>تو ای پهلوان گرد جوینده کام</p></div>
<div class="m2"><p>که گرشاسب خواندت هر کس به نام</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>زما بر تو باد آفرین و درود</p></div>
<div class="m2"><p>چو آیی بدین کاخ ما در فرود</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>طلسمی که بستم تو دانی گشاد</p></div>
<div class="m2"><p>چو دیدی ز کردار ما دار یاد</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>نگر تا نبندی دل اندر جهان</p></div>
<div class="m2"><p>نباشی ازو ایمن اندر نهان</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>که گیتی یکی نغز بازیگرست</p></div>
<div class="m2"><p>که هزمانش نو بازی دیگرست</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>بهر نیک و هر بد که دارد پسیچ</p></div>
<div class="m2"><p>نگیرد به یک سان بر آرام هیچ</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>چو بر قسمت از ابر و چو آتش ز سنگ</p></div>
<div class="m2"><p>کجا روشنیش ندارد درنگ</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>دهد اندک اندک به روز دراز</p></div>
<div class="m2"><p>پس آن گه ستاند به یک بار باز</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>سر رنج هر کس برد باز بُن</p></div>
<div class="m2"><p>کند تازه امید و تنها کهن</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>به تدبیر اویی و او همچنین</p></div>
<div class="m2"><p>به تدبیر مرگ تو اندر کمین</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>بگرد از وی و سوی یزدان گران</p></div>
<div class="m2"><p>به هر کار فرمان یزدان بپای</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>اگر چه شهی بر زمین و زمان</p></div>
<div class="m2"><p>خداوند را بنده ای بی گمان</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>شوی کار دیو بدآیین کنی</p></div>
<div class="m2"><p>پس آنگاه بر دیو نفرین کنی</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>اگر دیو راهی نمودی درست</p></div>
<div class="m2"><p>نبردی ز ره خویشتن را نخست</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>مخور غم فراوان ز روی خرد</p></div>
<div class="m2"><p>که کمتر زید آن که او غم خورد</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>نشاید بداندیش بودن بسی</p></div>
<div class="m2"><p>کند زندگی تلخ بر هر کسی</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>درازست ره باشی پرداخته</p></div>
<div class="m2"><p>همه توشه یکبارگی ساخته</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>میفزای بار گنه کز گناه</p></div>
<div class="m2"><p>چو بارت گران شد بمانی به راه</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>بدان کوش کایزد چو خواندت پیش</p></div>
<div class="m2"><p>نیایدت شرم از گناهان خویش</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>به نزدیک تابوت زرّین مگرد</p></div>
<div class="m2"><p>که دیدی در آن خانهٔ لاژورد</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>که هست اندرو حلقه و یاره چند</p></div>
<div class="m2"><p>ز حوا بماندست با گیسبند</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>همان جامه کایزد به دست سروش</p></div>
<div class="m2"><p>به آدم فرستاد کآنرا بپوش</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>دگر گوهری کو دهد اندر آب</p></div>
<div class="m2"><p>به تاریکی اندر چو خورشید تاب</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>کزین جایگاه این سه چیز آن بَرَد</p></div>
<div class="m2"><p>که یکی پیمبر بود با خرد</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>زید تا جهان باشد ایزدپرست</p></div>
<div class="m2"><p>نهان آورد آب حیوان به دست</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>چنان گردد این کاخ از آن پس نهان</p></div>
<div class="m2"><p>که نیزش نبیند کس اندر جهان</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>دژم شد سپهدار و مهراج شاه</p></div>
<div class="m2"><p>گرستند یکسر سران سپاه</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>یکی بر گناهان و کردار خویش</p></div>
<div class="m2"><p>یکی بر غریبی و تیمار خویش</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>بر آن هم نشان کاخ بگذاشتند</p></div>
<div class="m2"><p>به کشتی رَهِ دور برداشتند</p></div></div>