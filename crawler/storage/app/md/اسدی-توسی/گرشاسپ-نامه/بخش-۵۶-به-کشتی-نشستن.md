---
title: >-
    بخش ۵۶ - به کشتی نشستن
---
# بخش ۵۶ - به کشتی نشستن

<div class="b" id="bn1"><div class="m1"><p>چو سه روز بگذشت و شد راست باد</p></div>
<div class="m2"><p>به کشتی نشستند و رفتند شاد </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به دریا و خشکی ز کشتی کشان</p></div>
<div class="m2"><p>هر آن کس که داد از شگفتی نشان </p></div></div>
<div class="b" id="bn3"><div class="m1"><p>برفتند سیصد هزاران فزون </p></div>
<div class="m2"><p>بدیدند از جانور گونه گون </p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چه برسانِ پرنده و چارپای </p></div>
<div class="m2"><p>چه هم گونه دیو مردم نمای </p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یکی را سه رو پای و چنگل هزار </p></div>
<div class="m2"><p>یکی بهره را سر دو و چشم چار </p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یکی را دُم ماهی و چنگ شیر </p></div>
<div class="m2"><p>دهان از بَرِ سینه و چشم زیر </p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یکی را تن اسپ و خرطوم پیل </p></div>
<div class="m2"><p> رخش لعل و اندام همرنگ نیل </p></div></div>
<div class="b" id="bn8"><div class="m1"><p>یکی را سر گاو و یشک نهنگ </p></div>
<div class="m2"><p> یکی را تن مردم و شاخ رنگ </p></div></div>
<div class="b" id="bn9"><div class="m1"><p>همه زین نشان گونه گون جانور </p></div>
<div class="m2"><p>نمودند در آب با یکدگر </p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چنین تا کُهی کآن نه بس دور بود </p></div>
<div class="m2"><p>سَر مرز او نزد فیصور بود </p></div></div>