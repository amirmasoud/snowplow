---
title: >-
    بخش ۱۸ - آمدن ضحاک به مهمانی اثرط و دیدن گرشاسب را
---
# بخش ۱۸ - آمدن ضحاک به مهمانی اثرط و دیدن گرشاسب را

<div class="b" id="bn1"><div class="m1"><p>همان سال ضحاک کشورستان</p></div>
<div class="m2"><p>ز بابل بیامد به زابلستان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به هندوستان خواست بردن سپاه</p></div>
<div class="m2"><p>که رفتی بدان بوم هر چندگاه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>درِ گنج اثرط سبک باز کرد</p></div>
<div class="m2"><p>سپه را به نزل و علف ساز کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بزد کوس و با لشکر و پیل و ساز</p></div>
<div class="m2"><p>سه منزل شد از پیش ضحاک باز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فرود آوریدش به ایوان خویش</p></div>
<div class="m2"><p>سران را همه خواند مهمان خویش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کیانی یکی جشن سازید و سور</p></div>
<div class="m2"><p>که آمد ز مینو بدان جشن حور</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دَم مشک از مغز بر میغ شد</p></div>
<div class="m2"><p>دِلِ میغ ازو عنبر آمیغ شد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز عکس می زرد و جام بلور</p></div>
<div class="m2"><p>سپهری شد ایوان پُر از ماه و هور</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به تلّ بود زرّ ریخته زیر گام</p></div>
<div class="m2"><p>به خرمن برافروخته عودِ خام</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کشیده رَده ریدگان سرای</p></div>
<div class="m2"><p>به رومی عمود و به چینی قبای</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دو گلشان به باد از شبه دِرع ساز</p></div>
<div class="m2"><p>دو سٌنبل به میدان گل گوی باز</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>می زرد کف بر سرش تاخته</p></div>
<div class="m2"><p>چو دٌرّ از بَرِ زرّ بگداخته</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>شهان پاک با یاره و طوقِ زر</p></div>
<div class="m2"><p>همان پهلوانان به زرّین کمر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>شده هر دل از خرّمی نازجوی</p></div>
<div class="m2"><p>لَبِ می کشان با قدح رازگوی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نوازان نوازنده در چنگ چنگ</p></div>
<div class="m2"><p>ز دل برده بگماز چون زنگ زنگ</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ز بس کز نوا بود در چرخ جوش</p></div>
<div class="m2"><p>همی زهره مر ماه را گفت نوش</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>همه چشم ضحاک از آن بزم و سور</p></div>
<div class="m2"><p>به گرشاسب بٌد خیره مانده ز دور</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>که از چهر و بالا و فرّ و شکوه</p></div>
<div class="m2"><p>همانند او کس نبد زآن گروه</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>به اثرط چنین گفت کز چرخ سر</p></div>
<div class="m2"><p>اگر بگذرانی، سزد زین پسر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>هنرهاش زآنسان شنیدم بسی</p></div>
<div class="m2"><p>که نادیده باور ندارد کسی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ستود اثرط از پیش ضحاک را</p></div>
<div class="m2"><p>به رخساره ببسود مر خاک را</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>به فرّ تو شاه جهاندار گفت</p></div>
<div class="m2"><p>چنانست کش در هنر نیست جفت</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>چو او بانگ بر جنگی ادهم زند</p></div>
<div class="m2"><p>سپاهی به یک حمله بر هم زند</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>سنانش آتش کین فروزد همی</p></div>
<div class="m2"><p>خدنگش دل شیر دوزد همی</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>کس ار هست بدخواه شاه زمین</p></div>
<div class="m2"><p>فرستش بَرِ وی به پرخاش و کین</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>که گر هست میدانش چرخ اسپ میغ</p></div>
<div class="m2"><p>سرش پیشت آرد بریده به تیغ</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>جهاندار گفتا چنینست راست</p></div>
<div class="m2"><p>بدین، برز و بالا و چهرش گواست</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>هنر هرجه در مرد والا بود</p></div>
<div class="m2"><p>به جهرش بر از دور پیدا بود</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>چو گوهر میان گهردار سنگ</p></div>
<div class="m2"><p>که بیرون پدیدار باشدش رنگ</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>شنیدم هنرهاش و دیدم کنون</p></div>
<div class="m2"><p>به دیدار هست از شنودن فزون</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>به جمشید ماند به چهر و به پوست</p></div>
<div class="m2"><p>گواهی دهم من که از تخم اوست</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>بدین یال و گردی بَر و گرده گاه</p></div>
<div class="m2"><p>چه سنجد به چنگالِ او کینه خواه</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>کنون آمدست اژدهایی پدید</p></div>
<div class="m2"><p>کزآن اژدها مِه دگر کس ندید</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>از آن گه که گیتی ز طوفان برست</p></div>
<div class="m2"><p>ز دریا برآمد به خشکی نشست</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>گرفته نشیمن شکاوند کوه</p></div>
<div class="m2"><p>همی دارد از رنج گیتی ستوه</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>میان بست بایدش بر تاختش</p></div>
<div class="m2"><p>وزان زشت پتیاره کین آختن</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>چنین گفت گرشاسب کز فرّ شاه</p></div>
<div class="m2"><p>ببندم بر اهریمنِ تیره راه</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>مرا چون به کف گرز و شبرنگ زیر</p></div>
<div class="m2"><p>به پیشم چه نر اژدها و چه شیر</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>کنم ز اژدهای فلک سر زکین</p></div>
<div class="m2"><p>چه باک آیدم ز اژدهای زمین</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>سرِ اژدها بسته دام گیر</p></div>
<div class="m2"><p>تو اندیشه او مبر، جام گیر</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>مهان بر ستایش گشادند لب</p></div>
<div class="m2"><p>همه روز ازین بٌد سخن تا به شب</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>چو در سبز بُستان شکوفه برُست</p></div>
<div class="m2"><p>جهان زردی از رخ به عنبر بشست</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>گسستند بزم نی و رود و باد</p></div>
<div class="m2"><p>پراکنده گشت انجمن مست و شاد</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>به گرشاسب گفت اثرط ای شوربخت</p></div>
<div class="m2"><p>ز شاه از چه پذرفتی این جنگ سخت</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>نه هر جایگه راست گفتن سزاست</p></div>
<div class="m2"><p>فراوان دروغست کان به زراست</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>نگر جنگ این اژدها سرسری</p></div>
<div class="m2"><p>چنان جنگ های دگر نشمری</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>نه گورست کافتد به زخم دُرشت</p></div>
<div class="m2"><p>نه شیری که شاید به شمشیر کشت</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>نه دیوی که آید به خم کمند</p></div>
<div class="m2"><p>نه گردی کِش از زین توانی فکند</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>دمان اژدهاییست کز جنگ او</p></div>
<div class="m2"><p>سُته شد جهان پاک بر چنگ او</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>زدندش بسی تیر مویی ندوخت</p></div>
<div class="m2"><p>تنش هم ز نفظ و ز آتش نسوخت</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>مشو غرّه زین مردی و زورِ تن</p></div>
<div class="m2"><p>به من برببخشای و بر خویشتن</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>به خوان بر نیاید همی میهمان</p></div>
<div class="m2"><p>کش از آرزو در دل آید گمان</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>به گیتی کسی مرد این جنگ نیست</p></div>
<div class="m2"><p>اگر تو نیازی، بدین ننگ نیست</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>فکندن به مردی تن اندر هلاک</p></div>
<div class="m2"><p>نه مردیست کز باد ساریست پاک</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>هر امید را کار ناید به برگ</p></div>
<div class="m2"><p>بس امید کانجام آن هست مرگ</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>بدو گفت گرشاسب مَندیش هیچ</p></div>
<div class="m2"><p>تو از بهر شه بزم و رامش بسیچ</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>شما را می و شادی و بمّ و زیر</p></div>
<div class="m2"><p>من و اژدها و کُه و گُرز و تیر</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>اگر کوه البرز یک نیمه اوست</p></div>
<div class="m2"><p>سرش کنده گیر از که آکنده پوست</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>همه کس ز گرشاسب دل برگرفت</p></div>
<div class="m2"><p>که تند اژدهایی بٌد آن بس شگفت</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>به دُم رود جیحون بینباشتی</p></div>
<div class="m2"><p>به دَم زنده پیلی بیو باشتی</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>ز برش ار پریدی عقاب دلیر</p></div>
<div class="m2"><p>بیفتادی از بوی زهرش به زیر</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>کُهی جانور بُد رونده ز جای</p></div>
<div class="m2"><p>به سینه زمین در به تن سنگ سای</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>چو سیل از شکنج و چو آتش ز جوش</p></div>
<div class="m2"><p>چو برق از درخش و چورعد از خروش</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>سرش بیشه از موی وچون کوه تن</p></div>
<div class="m2"><p>چو دودش دَم و همچو دوزخ دهن</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>دو چشم کبودش فروزان ز تاب</p></div>
<div class="m2"><p>چو دو آینه در تَف آفتاب</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>زبانش چو دیوی سیه سر نگون</p></div>
<div class="m2"><p>که هزمان ز غاری سرآرد برون</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>ز دنبال او دشت هرجای جوی</p></div>
<div class="m2"><p>به هر جوی در رودی از زهر اوی</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>تنش پُر پشیزه ز سر تا میان</p></div>
<div class="m2"><p>به کردار بر عیبه برگستوان</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>ازو هر پشیزه چو گیلی سپر</p></div>
<div class="m2"><p>نه آهن نه آتش برو کارگر</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>نشسته نمودی چو کوهی به جای</p></div>
<div class="m2"><p>ستان خفته چندانکه پیلی به پای</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>کجا او شدی از دَم زهر بیز</p></div>
<div class="m2"><p>دو منزل بُدی دام و دَد را گریز</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>ز دندان به زخم آتش افروختی</p></div>
<div class="m2"><p>درخت و گیاها همی سوختی</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>پس از بهر جنگش یل زورمند</p></div>
<div class="m2"><p>یکی چرخ فرمود سهمن بلند</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>کمانی چو چفته ستونی ستبر</p></div>
<div class="m2"><p>زهش چون کمندی ز چرم هژبر</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>که بر زه نیامد به ده مرد گرد</p></div>
<div class="m2"><p>نه یکیّ توانستش از جای برد</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>چنان بود تیرش که ژوپین گران</p></div>
<div class="m2"><p>شمردند هر تیر خشتی گران</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>ز کردار آن چرخ بازوگسل</p></div>
<div class="m2"><p>خبر یافت ضحاک و شد خیره دل</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>به اثرط بفرمود و گفتا به گاه</p></div>
<div class="m2"><p>به دشت آر گرشاسب را با سپاه</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>که تا زین دلیران ایران، هنر</p></div>
<div class="m2"><p>ببیند چو گردند با یکدگر</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>سواری او نیز ما بنگریم</p></div>
<div class="m2"><p>به میدان هنرهای او بشمریم</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>چو از خواب روز اندرآمد به خشم</p></div>
<div class="m2"><p>رخش شست چشمه به زر آب چشم</p></div></div>