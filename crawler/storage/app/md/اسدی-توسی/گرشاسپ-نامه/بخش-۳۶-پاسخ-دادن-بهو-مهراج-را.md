---
title: >-
    بخش ۳۶ - پاسخ دادن بهو مهراج را
---
# بخش ۳۶ - پاسخ دادن بهو مهراج را

<div class="b" id="bn1"><div class="m1"><p>بهو گفت با بسته دشمن به پیش</p></div>
<div class="m2"><p>سخن گفتن آسان بود کم و بیش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>توان گفت بد با زبونان دلیر</p></div>
<div class="m2"><p>زبان چیره گردد چو شد دست چیر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بنه نام دیوانه بر هوشیار</p></div>
<div class="m2"><p>پس آن گاه بر کودکانست کار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ترا پادشاهی به من گشت راست</p></div>
<div class="m2"><p>ولیک از خوی بد ترا کس نخواست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گهر گر نبودم هنر بُد بسی</p></div>
<div class="m2"><p>ازین روی را خواستم هر کسی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به زور و هنر پادشاهی و تخت</p></div>
<div class="m2"><p>نیابد کسی جز به فرخنده بخت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هنر بُد مرا بخت فرخ نبود</p></div>
<div class="m2"><p>چو باشد هنر بخت نبود چه سود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هنرها ز بخت بد آهو بود</p></div>
<div class="m2"><p>ز بخت آوران زشت نیکو بود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>پدر نیز پندت هم از بیم گفت</p></div>
<div class="m2"><p>که با من هنر بیشتر دید جفت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به من بود شاهی سزاوارتر</p></div>
<div class="m2"><p>که دارم هنر از تو بسیار تر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تو دادی سرندیب از آن پس به من</p></div>
<div class="m2"><p>فکندیم دور از بر خویشتن</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>پس اندر نهان خون من خواستی</p></div>
<div class="m2"><p>نبد سود هر چاره کآراستی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>من از بیم بر تو سپه ساختم</p></div>
<div class="m2"><p>همه گنج و گاهت بر انداختم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چو شاهیت یکسر مرا خواست شد</p></div>
<div class="m2"><p>ازین زابلی کار تو راست شد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>اگر نامدی او به فریاد تو</p></div>
<div class="m2"><p>بُدی کم کنون بیخ و بنیاد تو</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>تو بودی به پیشم سرافکنده پست</p></div>
<div class="m2"><p>چنان چون منم پیش تو بسته دست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بشد تند مهراج و گفتا دروغ</p></div>
<div class="m2"><p>بَرِ راست هرگز نگیرد فروغ</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>پدرت آنکه زو نازش و نام توست </p></div>
<div class="m2"><p>نیای مرا پیلبان بُد نخست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>گهی چند سرهنگ درگاه شد</p></div>
<div class="m2"><p>پس آن گه سرندیب را شاه شد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>تو در پای پیلان بُدی خاشه روب</p></div>
<div class="m2"><p>کواره کشی پیشه با رنج و کوب</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چو رفت او بجایش ترا خواستم</p></div>
<div class="m2"><p>شهی دادمت کارت آراستم</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>کنون کت نشاندم بجای شهی</p></div>
<div class="m2"><p>همی جای من خواهی از من تهی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>کسی کش بود دیده از شرم پاک</p></div>
<div class="m2"><p>ز هر زشت گفتن نیایدش باک</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بتر هر زمان مردم بدگهر</p></div>
<div class="m2"><p>که گوساله هر چند مِه گاو تر</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>برآشفت گرشاسب از کین و خشم</p></div>
<div class="m2"><p>بزد بر بهو بانگ و بر تافت چشم</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بفرمود تا هر که بدخواه و دوست </p></div>
<div class="m2"><p>ز سیلی به گردنش بردند پوست</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>درآکند خاکش به کام و دهن</p></div>
<div class="m2"><p>ببردند بر دست و گردن رسن</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>همیدون به بندش همی داشتند</p></div>
<div class="m2"><p>برو چند دارنده بگماشتند</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>همان گاه زنگی زمین بوسه داد</p></div>
<div class="m2"><p>به گرشاسب بر آفرین کرد یاد</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بدو گفت دانی که از روی بخت</p></div>
<div class="m2"><p>ز من بُد که شد بر بهو کار سخت</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>بدو رهنمونی منت ساختم</p></div>
<div class="m2"><p>چو بستیش بر دوش من تاختم</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>دگر کم همه خرد کردی دهن</p></div>
<div class="m2"><p>به سیصد منی مشت دندان شکن</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>مرا تا بوم زنده و هوشمست</p></div>
<div class="m2"><p>تف مشت تو در بنا گوشمست</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>کنون گر بدین بنده رای آوری</p></div>
<div class="m2"><p>سزد کانچه گفتی به جای آوری</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>سپهبد بخندید و بنواختش</p></div>
<div class="m2"><p>سزا خلعت و بارگی ساختش</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>میان بزرگانش سالار کرد</p></div>
<div class="m2"><p>درفش و سپاهش پدیدار کرد</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>چنین بود گیتی و چونین بود</p></div>
<div class="m2"><p>گهش مهربانی و گه کین بود</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>یکی را دهد رنج و بُرّد ز گنج</p></div>
<div class="m2"><p>یکی را دهد گنج نابرده رنج</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>همه کارش آشوب و پنداشتیست</p></div>
<div class="m2"><p>ازو آشتی جنگ و جنگ آشتیست</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>کرا بیش بخشد بزرگی و ناز</p></div>
<div class="m2"><p>فزونتر دهد رنج و گرم و گداز</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>درو هر که گویی تن آسان ترست</p></div>
<div class="m2"><p>همو بیش با رنج و دردسرست</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>توان خو ازو دست برداشتن</p></div>
<div class="m2"><p>وزین خو نشایدش برگاشتن</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>از آن پس بهو چون به بند اوفتاد</p></div>
<div class="m2"><p>سپهدار و مهراج گشتند شاد</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>همه شب به رود و می دلفروز</p></div>
<div class="m2"><p>ببودند تا بر زد از خاک روز</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>چو گردون پیروزه از جوشنش</p></div>
<div class="m2"><p>بکند آن همه کوکب روشنش</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>سپاه بهو رزم را کرد رای</p></div>
<div class="m2"><p>کشیدند صف پیش پرده سرای</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>ندیدندش و جست هر کس بسی</p></div>
<div class="m2"><p>فتادند ازو در گمان هر کسی</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>گه بگریخت در شب نهان از سپاه</p></div>
<div class="m2"><p>وگر شد به زنهار مهراج شاه</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>ز جان یکسر امّید برداشتند</p></div>
<div class="m2"><p>سلیح و بُنه پاک بگذاشتند </p></div></div>
<div class="b" id="bn50"><div class="m1"><p>گریزان سوی بیشه و دشت و کوه</p></div>
<div class="m2"><p>نهادند سرها گروه ها گروه</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>دلیران ایران هر آنکس که بود</p></div>
<div class="m2"><p>پی گردشان برگرفتند زود</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>نهادند جنگی ستیزندگان</p></div>
<div class="m2"><p>سنان در قفای گریزندگان</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>فکندند چندان ازیشان نگون</p></div>
<div class="m2"><p>که بُد کشته هر سو سه منزل فزون</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>جهان بود پر خیمه و چارپای</p></div>
<div class="m2"><p>سلیح و بنه پاک مانده به جای</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>ز خرگاه وز فرش وز سیم و زر</p></div>
<div class="m2"><p>ز درع و ز خفتان ز خود و سپر </p></div></div>
<div class="b" id="bn56"><div class="m1"><p>همه هر چه بُد برکه و دشت و غار</p></div>
<div class="m2"><p>سلیح نبردی هزاران هزار</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>همی گرد کردند بیش از دو ماه</p></div>
<div class="m2"><p>یکی کوه بُد سرکشیده به ماه</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>که پیلی به گردش به روز دراز</p></div>
<div class="m2"><p>نگشتی نرفتیش مرغ از فراز</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>سپهبد بهین بر گزید از میان</p></div>
<div class="m2"><p>ببخشید دیگر بر ایرانیان</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>همانجا یکی هفته دل شادکام</p></div>
<div class="m2"><p>برآسود با بخشش و رود و جام</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>چو هفته سرآمد به مهراج گفت </p></div>
<div class="m2"><p>که این کار با کام دل گشت جفت</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>بفرمایید ار نیز کاریست شاه</p></div>
<div class="m2"><p>وگر نیست دستور باشد به راه</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>بدو گفت مهراج کز فرّ بخت</p></div>
<div class="m2"><p>ز تو یافتم پادشاهی و تخت</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>نماند ست کاری فزاینده نام</p></div>
<div class="m2"><p>کنون چون بهو را فکندی به دام</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>پسر با برادرش هر دو به هم</p></div>
<div class="m2"><p>سرندیب دارند با باد و دم</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>رویم اندرین چاره افسون کنیم</p></div>
<div class="m2"><p>ز چنگالشان شهر بیرون کنیم</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>جهان پهلوان گرد گردنفراز </p></div>
<div class="m2"><p>چو بشنید گفتار مهراج باز</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>اسیران هر آنکس که آمد به مشت</p></div>
<div class="m2"><p>کرا کشت بایست یکسر بکشت</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>بهو را به خواری و بند گران</p></div>
<div class="m2"><p>به دژها فرستاد با دیگران </p></div></div>
<div class="b" id="bn70"><div class="m1"><p>وز آنجا سپه برد زی زنگبار</p></div>
<div class="m2"><p>بشد تا جزیری به دریا کنار </p></div></div>
<div class="b" id="bn71"><div class="m1"><p>پر از کوه و بیشه جزیری فراخ</p></div>
<div class="m2"><p>درختش همه عود گسترده شاخ</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>کُهش کان ارزیر و الماس بود</p></div>
<div class="m2"><p>همه بیشه اش جای نسناس بود</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>ز گِردش صدف بیکران ریخته</p></div>
<div class="m2"><p>به گل موج دریا برآمیخته</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>سپاه آن صدف ها همی کافتند</p></div>
<div class="m2"><p>به خروار دُر هر کسی یافتند</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>چنان بود ازو هر دُر شاهوار </p></div>
<div class="m2"><p>کجا ژاله گردد سرشک بهار</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>چو سیصد هزار از دَرِ تاج بود</p></div>
<div class="m2"><p>که در پنج یک بهر مهراج بود</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>به گرشاسب بخشید پاک آنچه یافت</p></div>
<div class="m2"><p>وز آنجا سوی راه دریا شتافت</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>به یک کوهشان جای آرام بود</p></div>
<div class="m2"><p>کجا نام او ذات اوهام بود</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>به نزد سرندیب کوهی بلند</p></div>
<div class="m2"><p>پر از بیشه و مردم کشتمند</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>ز غواص دیدند مردی هزار</p></div>
<div class="m2"><p>رده ساخته گرد دریا کنار</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>گروهی شده زآب جویان صدف</p></div>
<div class="m2"><p>گروهی صدف کاف خنجر به کف</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>سپهدار مهراج و چندین گروه</p></div>
<div class="m2"><p>ستادند نظاره شان گرد کوه</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>ز دُر آنچه نیکوتر آمد به دست</p></div>
<div class="m2"><p>گزیدند بیش از دو صد بار شست</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>به مهراج دادند و مهراج شاه</p></div>
<div class="m2"><p>به گرشاسب بخشید و ایران سپاه</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>همان گه غریوی ز لشکر بخاست</p></div>
<div class="m2"><p>کزاین بیشه ناگاه بر دست راست</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>دویدند دو دیو و از ما دو مرد</p></div>
<div class="m2"><p>ربودند و بردند و کشتند و خورد</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>سپهبد سبک جست با گرز جنگ</p></div>
<div class="m2"><p>بپوشید درع و میان بست تنگ</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>یکی گفت تندی مکن با غریو</p></div>
<div class="m2"><p>درین بیشه نسناس باشد نه دیو</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>به بالا یکایک چو سرو بلند</p></div>
<div class="m2"><p>به اندام پر موی چو ن گوسپند</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>همه سرخ موی و همه سبز موی</p></div>
<div class="m2"><p>دو سوی قفا چشم و دو سوی روی</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>به اندام هم ماده هم نیز نر</p></div>
<div class="m2"><p>همی بچه زایند چون یکدگر</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>دو زیشان در آرند پیلی به زیر</p></div>
<div class="m2"><p>کشند و خورند و نگردند سیر</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>یکی به ز ما صد به جنگ و ستیز</p></div>
<div class="m2"><p>فزونشان تک از تازی اسپان تیز</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>سپهبد به دادار سوگند خورد</p></div>
<div class="m2"><p>که امروز تنها نمایم نبرد</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>کُشم هر چه نسناس آیدم پیش</p></div>
<div class="m2"><p>اگر صد هزارند و زین نیز بیش</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>بگفت این و شد سوی بیشه دمان</p></div>
<div class="m2"><p>همی گشت با گرز و تیر و کمان</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>ز نسناس شش دید جایی به هم</p></div>
<div class="m2"><p>یکی پیل کشته دریده شکم</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>چو دیدندش از جایگه تاختند</p></div>
<div class="m2"><p>ز پیرامنش جنگ برساختند</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>به خنجر دو را پای بفکند و دست</p></div>
<div class="m2"><p>دو را زیر گرز گران کرد پست</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>دو با خشم و کین زو در آویختند</p></div>
<div class="m2"><p>به دندان از آن خون همی ریختند</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>بزد هر دو را خنجر دل شکاف</p></div>
<div class="m2"><p>بدریدشان از گلو تا به ناف</p></div></div>
<div class="b" id="bn102"><div class="m1"><p>سرانشان به لشکر گه آورد شاد</p></div>
<div class="m2"><p>به بزم اندرون پیش گردان نهاد</p></div></div>
<div class="b" id="bn103"><div class="m1"><p>بماندند ازو خیره دل هر کسی</p></div>
<div class="m2"><p>بُد از هر زبان آفرینش بسی</p></div></div>
<div class="b" id="bn104"><div class="m1"><p>بفرمود تا پوستهاشان به درگاه</p></div>
<div class="m2"><p>به کشتی کشند اندر آکنده کاه</p></div></div>