---
title: >-
    بخش ۷۴ - بازگشت گرشاسب و صفت خواسته
---
# بخش ۷۴ - بازگشت گرشاسب و صفت خواسته

<div class="b" id="bn1"><div class="m1"><p>چنین تا بقنو جشن آورد شاد</p></div>
<div class="m2"><p>پس آن گه در گنج ها برگشاد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مهی شاد و مهمان همی داشتش</p></div>
<div class="m2"><p>که یک روز بی بزم نگذاشتش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سَر ماه چندانش هدیه ز گنج</p></div>
<div class="m2"><p>ببخشید کآمد شمردنش رنج</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز خرگاه و از خیمه و فرش و رخت</p></div>
<div class="m2"><p>ز طوق و کمر ز افسر و تاج و تخت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هم از زر ساوه هم از رسته نیز</p></div>
<div class="m2"><p>هم از در و یاقوت و هر گونه چیز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هم از شیر و طاووس و نخچیر و باز</p></div>
<div class="m2"><p>بدادش بسی چیز زرینه ساز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>درونشان ز کافور و از مشک پُر</p></div>
<div class="m2"><p>نگاریده بیرون ز یاقوت و دُر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زبر جد سرو گاوی از زر ناب</p></div>
<div class="m2"><p>سم از جزع و دندان ز دُر خوشاب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گهرهای کانی ز پا زهر و زهر</p></div>
<div class="m2"><p>چهل پیل و منشور ده باره شهر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به برگستوان پنجه اسپ گزین</p></div>
<div class="m2"><p>دگر صد شتر با ستام و به زین</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز خفتان و از درع و جوشن هزار</p></div>
<div class="m2"><p>ز خشت و ز خنجر فزون از شمار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ز دینار و ز نقره خروار شست</p></div>
<div class="m2"><p>ز زربفت خلعت صدوبیست دست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>پرستار سیصد بتان چگل</p></div>
<div class="m2"><p>سرایی دو صد ریدک دلگسل</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>هرآن زر که از باژ در کشورش</p></div>
<div class="m2"><p>رسیدی ز هر نامداری برش</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ازو خشت زرین همی ساختی</p></div>
<div class="m2"><p>یکی چشمه بُد در وی انداختی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>صدش داد از آن همچو آتش به رنگ</p></div>
<div class="m2"><p>که هر خشت ده من بر آمد به سنگ</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>یکی حله دادش دگر کز شهان</p></div>
<div class="m2"><p>جزو هیچکس را نبد در جهان</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>برو هر زمان از هزاران فزون</p></div>
<div class="m2"><p>پدید آمدی پیکر گونه گون</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بُدی روز لعلی شب تیره زرد</p></div>
<div class="m2"><p>نه نم یافتی ز ابر و نز باد گرد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>کرا تن ز دردی هراسان شدی</p></div>
<div class="m2"><p>چو پوشیدی آنرا تن آسان شدی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ازو هر کسی بوی خوش یافتی</p></div>
<div class="m2"><p>به تاریکی از شمع به تافتی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>به ایرانیان هر کس از سرکشان</p></div>
<div class="m2"><p>بسی چیز بخشید هم زین نشان</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>پس از بهر ضحاکِ شه ساز کرد</p></div>
<div class="m2"><p>بسی گونه گون هدیه آغاز کرد</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>سراپرده دیبه بر رنگ نیل</p></div>
<div class="m2"><p>که پیرامن دامنش بُد دو میل</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چو شهری دو صد برج گردش بپای</p></div>
<div class="m2"><p>سپه را به هر برج بر کرده جای</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>یکی فرش دیبا دگر رنگ رنگ</p></div>
<div class="m2"><p>که بد کشوری پیش پهناش تنگ</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ز هر کوه و دریا و هر شهر و بر</p></div>
<div class="m2"><p>ز خاور زمین تا در باختر</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>نگاریده بر گرد او گونه گون</p></div>
<div class="m2"><p>کز آنجا چه آرند و آن بوم چون</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>ز زر و زبرجد یکی نغز باغ</p></div>
<div class="m2"><p>درو هر گل از گوهری شب چراغ</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>درختی درو شاخ بروی هزار</p></div>
<div class="m2"><p>ز پیروزه برگش ز یاقوت بار</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>به هر شاخ بر مرغی از رنگ رنگ</p></div>
<div class="m2"><p>زبرجد بر منقار و بسّد به چنگ</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>چو آب اندرو راه کردی فراخ</p></div>
<div class="m2"><p>درخت از بن آن بر کشیدی به شاخ</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>سر از شاخ هر مرغ بفراختی</p></div>
<div class="m2"><p>همی این از آن به نوا ساختی</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>درم بُد دگر نام او کیموار</p></div>
<div class="m2"><p>ازو بار فرمود شش پیلوار</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>به ده پیل بر مشک بیتال بود</p></div>
<div class="m2"><p>که هر نافه زو هفت مثقال بود</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>ده از عنبر و زعفران بود نیز</p></div>
<div class="m2"><p>ده از عود و کافور و هر گونه چیز</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>ز سیم سره خایه صد بار هشت</p></div>
<div class="m2"><p>که هر یک به مثقال صد بر گذشت</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>سپیدیش کافور و زردیش زر</p></div>
<div class="m2"><p>یکی بهره را شوشها زو گهر</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>سخنگوی طوطی دوصد جفت جفت</p></div>
<div class="m2"><p>به زرّین قفس ها و دیبا نهفت</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>کت و خیمه و خرگه و شاروان</p></div>
<div class="m2"><p>ز هر گونه چندان که ده کاروان</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>ز گاوان گردونگش و بارکش</p></div>
<div class="m2"><p>خورش گونه گون بار صد بار شش</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>هزار دگر بار دندان پیل</p></div>
<div class="m2"><p>هزار و دو صد صندل و عود و نیل</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>ز دیبای رنگین صد و بیست تخت</p></div>
<div class="m2"><p>ز مرجان چهل مهد و پنجه درخت</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>دو صد جوشن و هفتصد درع و ترگ</p></div>
<div class="m2"><p>صد و بیست بند از سروهای کرگ</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>چهل تنگ بار از مُلمع خُتو</p></div>
<div class="m2"><p>ز گوهر ده افسر ز گنج بهو</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>ز کرگ از هزاران نگارین سپر</p></div>
<div class="m2"><p>سه چندان نی رمح بسته به زر</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>سریری ز زر بر دو پیل سپید</p></div>
<div class="m2"><p>ز یاقوت تاجی چو رخشنده شید</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>از آن آهن لعلگون تیغ چار</p></div>
<div class="m2"><p>هم از روهنی و بلالک هزار</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>هزار از بلورین طبق نابسود</p></div>
<div class="m2"><p>که هر یک به رنگ آب افسرده بود</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>ز جام و پیاله نود بار شست </p></div>
<div class="m2"><p>ز بیجاده سی خوان و پنجاه دست</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>ز زر چار صد بار دینار گنج</p></div>
<div class="m2"><p>به خروار نقره دوصد بار پنج </p></div></div>
<div class="b" id="bn52"><div class="m1"><p>ز زر کاسه هفتاد خروار واند </p></div>
<div class="m2"><p>ز سیمینه آلت که داند که چند</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>هزار و دو صد جفت بردند نام</p></div>
<div class="m2"><p>ز صندوق عود و ز یاقوت جام</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>هم از شاره و تلک و خز و پرند</p></div>
<div class="m2"><p>هم از مخمل و هر طرایف ز هند</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>هزار اسپ کُه پیکر تیز گام</p></div>
<div class="m2"><p>به برگستوان و به زرین ستام</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>هزار دگر کرگان ستاغ</p></div>
<div class="m2"><p>به هر یک بر از نام ضحاک داغ</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>ده و دوهزار از بت ماهروی</p></div>
<div class="m2"><p>چه ترک و چه هندو همه مشکموی</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>ز دُر و زبر جد ز بهر نثار</p></div>
<div class="m2"><p>به صد جام بر ریخته سی هزار</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>یکی درج زَرین نگارش ز دُر </p></div>
<div class="m2"><p>درونش ز هر گوهری کرده پُر</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>گهر بُد کز آب آتش انگیختی</p></div>
<div class="m2"><p>گهر بُد کزو مار بگریختی</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>گهر بُدکزو اژدها سرنگون</p></div>
<div class="m2"><p>فتادی و جستی دو چشمش برون</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>گهر بُد که شب نورش آب از فراز</p></div>
<div class="m2"><p>بدیدی به شمعت نبودی نیاز</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>یکی گوهر افزود دیگر بدان</p></div>
<div class="m2"><p>که خواندیش دانا شه گوهران</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>همه گوهری را زده گام کم</p></div>
<div class="m2"><p>کشیدی سوی از خشک نم</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>چنین بُد هزار و دو صد پیلوار</p></div>
<div class="m2"><p>همیدون ز گاوان ده و شش هزار</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>صدو بیست پیل دگر بار نیز</p></div>
<div class="m2"><p>بُداز بهر اثرط ز هر گونه چیز</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>یکی نام با این همه خواسته </p></div>
<div class="m2"><p>درو پوزش بی کران خواسته</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>سپهبد بنه پیش را بار کرد</p></div>
<div class="m2"><p>بهو را بیاورد و بردار کرد</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>تنش را به تیر سواران بدوخت</p></div>
<div class="m2"><p>کرا بند بُد کرده بآتش بسوخت</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>گلیمی که باشد بدان سر سیاه</p></div>
<div class="m2"><p>نگردد بدین سر سپید این مخواه</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>نبایدت رنج ار بود بخت یار</p></div>
<div class="m2"><p>چه شد بخت بد چاره ناید به کار</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>خوی گیتی اینست و کردارش این</p></div>
<div class="m2"><p>نه مهرش بود پایدار و نه کین</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>چو شاهیست بیدادگر از سرشت</p></div>
<div class="m2"><p>که باکش نیاید ز کردار زشت</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>نش از آفرین ناز و نز غم نژند</p></div>
<div class="m2"><p>نه‌ شرم از نکوهش نه‌ بیم از گزند</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>چه خواند به نام و چه راند به ننگ</p></div>
<div class="m2"><p>میان اندرون بس ندارد درنگ</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>چو سایست از ابر و چه رفتن ز آب</p></div>
<div class="m2"><p>چو مهمانیی تو که بینی به خواب</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>چو تدبیر درویش گم بوده بخت</p></div>
<div class="m2"><p>کز اندیشه خود را دهد تاج و تخت</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>نهند گنج و سازد سرای نشست</p></div>
<div class="m2"><p>چو دید آنگهی باد دارد به دست</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>انوشه کسی کاو نکو نام مُرد</p></div>
<div class="m2"><p>چو ایدر تنش ماند نیکی ببرد</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>کسی کو نکو نام میرد همی</p></div>
<div class="m2"><p>ز مرگش تأسف خورد عالمی</p></div></div>