---
title: >-
    بخش ۱۲۹ - نامه گرشاسب به نزد فریدون
---
# بخش ۱۲۹ - نامه گرشاسب به نزد فریدون

<div class="b" id="bn1"><div class="m1"><p>سپهبد گزید این همه چار ماه</p></div>
<div class="m2"><p>یکی نامه فرمود نزدیک شاه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نویسنده قرطاس بر برگرفت</p></div>
<div class="m2"><p>سر خامه در مشک و عنبر گرفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر آمد ز شاخ آن نگونسار سار</p></div>
<div class="m2"><p>که بر سیم بارد ز منقار قار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سواری سه اسپه پیاده روان</p></div>
<div class="m2"><p>تنش رومی و چهره از هندوان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همان شیرخواره کش از قیر شیر</p></div>
<div class="m2"><p>ز گهواره بر جست گویا و پیر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همه تنش چشم و همه چشم گوش</p></div>
<div class="m2"><p>همه گوش دل ها همه دل خروش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دویدندش با سرنگونی به راه</p></div>
<div class="m2"><p>سخن گفتنش بر سپیدی سیاه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نگارید نام خدای از نخست</p></div>
<div class="m2"><p>که بی نام او دین نیاید درست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خداوند هرچ آشکارست و راز</p></div>
<div class="m2"><p>از آهو همه پاک و دور از نیاز</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بری از گهر بی گزند از زمان</p></div>
<div class="m2"><p>فزون از نشان و برون از گمان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دگر آفرین کرد بر شاه نو</p></div>
<div class="m2"><p>که بادش بلند افسر و گاه نو</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>خدیو زمانه کی فرمند</p></div>
<div class="m2"><p>گشاینده گیتی و ضحاک بند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>شه خاور و خسرو باختر</p></div>
<div class="m2"><p>کیومرثی تخم و جمشید فر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>فرستاده از دین به کشور درود</p></div>
<div class="m2"><p>گذارنده بی کشتی اروند رود</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>دهد شاه را بنده مژده ز بخت</p></div>
<div class="m2"><p>که بنوشتم این دیو کش راه سخت</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>به خون بداندیش ز الماس کین</p></div>
<div class="m2"><p>بشستم همه بوم ماچین و چین</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ز جیحون شدم تا بد آن جا که مهر</p></div>
<div class="m2"><p>بر آن بوم تا بد نخست از سپهر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>به هر شاه بر باژ کردم نخست</p></div>
<div class="m2"><p>جز از کام شه کس نیارست جست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>به فغفور در سرکشی کار کرد</p></div>
<div class="m2"><p>نشد رام و آهنگ پیکار کرد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بسی پند دادم برش خوار بود</p></div>
<div class="m2"><p>نپذرفت کش بخت بد یار بود</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>دل خیره در رای فرهنگ تاب</p></div>
<div class="m2"><p>بپیچد همی چون سرش ز آفتاب</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>فرستاد پیشم سپه چند بار</p></div>
<div class="m2"><p>پراکنده بیش از هزاران هزار</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>همان جادوان ساخت تا روز جنگ</p></div>
<div class="m2"><p>نمودند هرگونه افسون و رنگ</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ز سرما و آوای دیو و هژبر</p></div>
<div class="m2"><p>ز مار بپر و اژدهای در ابر</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>برآمد به هم بیست رزم گران</p></div>
<div class="m2"><p>شد افکنده سیصد هزار از سران</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>سرانجام هم بخت شه بود چیر</p></div>
<div class="m2"><p>درآمد سر بخت بدخواه زیر</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>همه بوم چین گشت بر هم زده</p></div>
<div class="m2"><p>بتان برده بتخانه آتشکده</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>دگر سی هزار از گرفتاریان</p></div>
<div class="m2"><p>جز از بردگان اند و زنهاریان</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>به بند اندرون بسته هشتاد شاه</p></div>
<div class="m2"><p>که با کوس زرین و گنج اند و گاه</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>مگر شاه فغفور کش نیست بند</p></div>
<div class="m2"><p>که شه بود و بندش ندیدم پسند</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>ز گنجش یکی بهره برداشتیم</p></div>
<div class="m2"><p> دگر دست نابرده بگذاشتم</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>مگر شاه با مهر پیش آیدش</p></div>
<div class="m2"><p>ببخشید گناه و بخشایدش </p></div></div>
<div class="b" id="bn33"><div class="m1"><p>نریمان یل مژدگان آورست</p></div>
<div class="m2"><p>که مر شاه را بنده کهترست</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>به هر رزمگه در بدادست داد</p></div>
<div class="m2"><p>چو آید کند هر چه رفتست یاد</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>نشستست بنده دو دیده به راه</p></div>
<div class="m2"><p>بدان تا نمایش چه آید ز شاه</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>چه فرمان دهد دیگر از رزم سخت</p></div>
<div class="m2"><p>کرا دارد ارزانی این تاج و تخت</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>به عنوان بر از بنده شاه گفت</p></div>
<div class="m2"><p>که از فر او هست با ماه جفت</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>همه کار فغفور زیبای او</p></div>
<div class="m2"><p>بیاراست آن رسم دربای او</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>صد و ده شتر را درم بار کرد</p></div>
<div class="m2"><p>چهل دیگر از بار دینار کرد</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>دگر چارصد دست زربفت چین</p></div>
<div class="m2"><p>گزید آنچه پوشیدی از به گزین</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>سرا پرده و خیمه پیشکار</p></div>
<div class="m2"><p>عماری و پیل و کت شاهوار</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>کنیزان دوشیزه تیرست و شست</p></div>
<div class="m2"><p>به رخ هر یک آرایش بت پرست</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>به دستور او یک به یک برشمرد</p></div>
<div class="m2"><p>سخن راند پس با نریمان گرد</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>که در ره چنان دار کارش به برگ</p></div>
<div class="m2"><p>که نبود نیازش به یک کاه برگ</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>مکن کم ز خوردش همه رسم و ساز</p></div>
<div class="m2"><p>وز او مردمش را مدار ایچ باز</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>از آن پس چهل جفت یاره ز زر</p></div>
<div class="m2"><p>گزین کرد و صد گوشوار از گهر</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>دو  صد دانه یاقوت و لعل آبدار</p></div>
<div class="m2"><p>ز در و زبر جد دو ره صد هزار</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>بفرمود کاین با تو همراه کن</p></div>
<div class="m2"><p>چو رفتی نثار شهنشاه کن</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>گره شد ز غم بر رخ شاه چین</p></div>
<div class="m2"><p>ز کاهش چو افتاد بر ماه چین </p></div></div>
<div class="b" id="bn50"><div class="m1"><p>ز خسته دل زار و چشم دژم</p></div>
<div class="m2"><p>سرشت آتش درد بآب بقم</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>همی گفت کای پادشاهی دریغ</p></div>
<div class="m2"><p>که ماهت نهان شد به تاریک میغ</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>بدی باغ آراسته پرنگار</p></div>
<div class="m2"><p>درختانت کندند یکسر ز بار</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>سپهری بدی روشن از تو جهان</p></div>
<div class="m2"><p>شدند اختران و آفتابت نهان</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>عروسی نو آیین بدی گاه را</p></div>
<div class="m2"><p>ربودند ناگه ز تو شاه را</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>ندانم که کی بینمت نیز باز</p></div>
<div class="m2"><p>ابا روز شادی و آرام و ناز</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>دو جزعش ز لؤلؤ شده ناپدید</p></div>
<div class="m2"><p>همی زد ز خون نقطه بر شنبلید</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>برآرد جهان سرکشان را زکار</p></div>
<div class="m2"><p>کند نرمشان گردش روزگار</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>سپهر روانرا ببد دستبرد</p></div>
<div class="m2"><p>بسست این چنین چند خواهی شمرد</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>یکی دایره ست آبگون چنبری</p></div>
<div class="m2"><p>فراوان درین دایره داوری</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>نه مر پادشاه و نه مر بنده را</p></div>
<div class="m2"><p>شناسد نه نادان نه داننده را</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>تو ای دانشی چند نالی ز چرخ</p></div>
<div class="m2"><p>که ایزد بدی دادت از چرخ برخ</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>نگر نیک و بد تا چه کردی ز پیش</p></div>
<div class="m2"><p>بیابی همان باز پاداش خویش</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>چو از تو بود کژی و بی رهی</p></div>
<div class="m2"><p>گناه از چه بر چرخ گردان نهی</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>ز یزدان شمر نیک و بدها درست</p></div>
<div class="m2"><p>که گردون یکی ناتوان همچو تست</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>نریمان چو دید اشک فغفور و درد</p></div>
<div class="m2"><p>رخش گشته ماننده برگ زرد</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>بدو گفت مندیش چندان به راه</p></div>
<div class="m2"><p>شکیب آر تا من سوم پیش شاه</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>به یزدان که بنشینم آن گه ز پای</p></div>
<div class="m2"><p>نگر کامت آرم سراسر به جای</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>شد و برد پیش آن همه خواسته</p></div>
<div class="m2"><p>اسیران و خوبان آراسته</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>همه راه پیوسته پنجاه میل</p></div>
<div class="m2"><p>ستور و شتر بود و گردون و پیل</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>ز گردون به گردون شده بانگ و جوش</p></div>
<div class="m2"><p>جهان پر درای و جرس پر خروش</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>شه چین جدا با فغستان و رخت</p></div>
<div class="m2"><p>همی رفت بر پیل با تاج و تخت</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>ورا جای بر زنده پیلی سپید</p></div>
<div class="m2"><p>مهان بر هیونان عودی هوید</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>سخن جز به دستور سالار بار</p></div>
<div class="m2"><p>نگفتی به ره در نهان و آشکار</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>خور و پوشش و فرش و خوبان به هم</p></div>
<div class="m2"><p>نکرد ایچ از آن رسم کش بود کم</p></div></div>