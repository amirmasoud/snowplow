---
title: >-
    بخش ۵۷ - شگفتی دیگر جزیره که کرگدن داشت
---
# بخش ۵۷ - شگفتی دیگر جزیره که کرگدن داشت

<div class="b" id="bn1"><div class="m1"><p>از آن کوه ملاح بگذشت خواست</p></div>
<div class="m2"><p>سپهدار گفت این شتابت چراست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بمان تا برین گنگ باز از شگفت </p></div>
<div class="m2"><p>چه بینیم کان یاد باید گرفت </p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بدو گفت ملاح مفزای کار</p></div>
<div class="m2"><p>که ایدر بود کرگدن بی شمار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به بالای گاوی پر از خشم و شور </p></div>
<div class="m2"><p>یکی جانور مه ز پیلان به زور</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سرو دارد از باز مردی فزون </p></div>
<div class="m2"><p>سرش چون سنان تن چو زآهن ستون </p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به زخم سرو کُه درآرد ز پای </p></div>
<div class="m2"><p>زند پیل را بر رباید ز جای </p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دلاور نبرد ایچ تیمار مرگ</p></div>
<div class="m2"><p>میان بست بر جنگ و پیکار کرگ </p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بدو گفت کام من این بُد ز بخت </p></div>
<div class="m2"><p>که پیش آیدم روزی این رزم سخت </p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کنون بور آهو تک کرگ دَن </p></div>
<div class="m2"><p>کمان و کمین من و کرگدن </p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نبد باکم از ببر و از اژدها </p></div>
<div class="m2"><p>بدینسان ددی را چه باشد بها </p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز کشتی برون رفت بر زه کمان </p></div>
<div class="m2"><p>یکی کرگدن دید کآمد دمان </p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چو نیزه سرو راست کرده بدوی </p></div>
<div class="m2"><p>همان گه خدنگی یل نامجوی </p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بپیوست و ز آنسان در آهیخت زوش </p></div>
<div class="m2"><p>که پیکان به ناخن بُد و زه به گوش</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>زبان و گلوگاه و یک نیمه تن </p></div>
<div class="m2"><p>فرو دوخت با گردن کرگدن </p></div></div>
<div class="b" id="bn15"><div class="m1"><p>همه گنگ تا شب بدینسان بگشت </p></div>
<div class="m2"><p>بیفکند از آن کرگدن سی و هشت </p></div></div>
<div class="b" id="bn16"><div class="m1"><p>به خنجر سروشان بیفکند و برد </p></div>
<div class="m2"><p>بَر شاه مهراج و او را سپرد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>سپه پاک و مهراج گشتند شاد </p></div>
<div class="m2"><p>بر او هر کسی آفرین کرد یاد</p></div></div>