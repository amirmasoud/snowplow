---
title: >-
    بخش ۹ - در سبب گفتن قصه گوید
---
# بخش ۹ - در سبب گفتن قصه گوید

<div class="b" id="bn1"><div class="m1"><p>یکی کار جستم همی ارجمند</p></div>
<div class="m2"><p>که نامم شود زو به گیتی بلند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگر نامهٔ رفتنم را نوید</p></div>
<div class="m2"><p>دهند این دو پیک سیاه و سپید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به رفتن بود خوش دل شاد من</p></div>
<div class="m2"><p>به نیکی کند هرکسی یاد من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مهی بُد سر داد و بنیاد دین</p></div>
<div class="m2"><p>گرانمایه دستور شاه زمین</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>محمّد مه جود و چرخ هنر</p></div>
<div class="m2"><p>سمعیل حصّی مر او را پدر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ردی دانش آرای یزدان پرست</p></div>
<div class="m2"><p>زمین حلم و دریا دل و راد دست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز چرخ روان تا بره تیره خاک</p></div>
<div class="m2"><p>چه و چون گیتی بدانسته پاک</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خوی نیک و خوبی و فرزانگی </p></div>
<div class="m2"><p>ره رادی و رأی مردانگی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نکوبختی و دانش و کلک وتیغ</p></div>
<div class="m2"><p>خدا ایچ ناداشته زو دریغ</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>برادرش والا براهیم راد</p></div>
<div class="m2"><p>گزین جهان گرد مهتر نژاد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خنیده به کلک و ستوده به تیر</p></div>
<div class="m2"><p>بدین گنج بخش و بدان شهر گیر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دو پرورده شاه بدخواه سوز</p></div>
<div class="m2"><p>یکی داد و ورز و یکی دین فروز</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>جهان را چو دو دیدهٔ روزگار</p></div>
<div class="m2"><p>زمان را چو دو دست فرمانگزار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ز هرکس فزون جاهشان نزد شاه</p></div>
<div class="m2"><p>گذشته درفش مهیشان ز ماه</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>به بگماز یک روز نزدیک خویش</p></div>
<div class="m2"><p>مرا هر دو مهتر نشاندند پیش</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بسی یاد نام نکو رانده شد</p></div>
<div class="m2"><p>بسی دفتر باستان خوانده شد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ز هر گونه رأیی فکندند بن</p></div>
<div class="m2"><p>پس آن گه گشادند بند سخن </p></div></div>
<div class="b" id="bn18"><div class="m1"><p>که فردوسی طوسی پاک مغز </p></div>
<div class="m2"><p>بدادست داد سخن های نغز </p></div></div>
<div class="b" id="bn19"><div class="m1"><p>به شهنامه گیتی بیاراستست</p></div>
<div class="m2"><p>بدان نامه نام نکو خواستست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>تو همشهری او را و هم پیشه ای</p></div>
<div class="m2"><p>هم اندر سخن چابک اندیشه ای </p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بدان همره از نامهٔ باستان </p></div>
<div class="m2"><p>به شعر آر خرّم یکی داستان </p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بسا نامداران که بردند رنج</p></div>
<div class="m2"><p>نهانی نهادند هر جای گنج</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>سرانجام رفتند و بگذاشتند</p></div>
<div class="m2"><p>نه زیشان کسی بهره برداشتند</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>تو زین داستان گنجی اندر جهان</p></div>
<div class="m2"><p>بمانی که هرگز نگردد کمی</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>همش هرکسی یابد از آدمی</p></div>
<div class="m2"><p>هم از برگرفتن نگیرد کمی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بُوی مانده فرزند ایدر بجای</p></div>
<div class="m2"><p>که همواره نام تو ماند بپای</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ز دانش یکی خرم نهی </p></div>
<div class="m2"><p>که از میوه هرگز نگردد تهی</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>جهان جاودانه نماند به کس</p></div>
<div class="m2"><p>بهین چیز از و نیک نامست و بس</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>کنون کان یاقوت دانش بکن </p></div>
<div class="m2"><p>ز دریای اندیشه دُر دَر فکن</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>خرد آتش تیز و دل بوته ساز </p></div>
<div class="m2"><p>سخن زرِّ کن پاک بر هم گداز</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>پس این زر و این گوهران بار کن</p></div>
<div class="m2"><p>در این گنج یکباره انبار کن</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>زکس یاد این گنج بر دل میار</p></div>
<div class="m2"><p>جز از شاه ارّانی شهریار</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>مجوی اندرین کار جز کام اوی</p></div>
<div class="m2"><p>منه مُهر بر وی به جز نام اوی</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>که تا جایگه یافتی نخجوان</p></div>
<div class="m2"><p>بدین شاه شد بخت پیرت جوان</p></div></div>