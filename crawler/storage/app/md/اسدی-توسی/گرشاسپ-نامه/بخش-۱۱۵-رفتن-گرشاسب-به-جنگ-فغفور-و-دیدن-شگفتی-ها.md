---
title: >-
    بخش ۱۱۵ - رفتن گرشاسب به جنگ فغفور و دیدن شگفتی‌ها
---
# بخش ۱۱۵ - رفتن گرشاسب به جنگ فغفور و دیدن شگفتی‌ها

<div class="b" id="bn1"><div class="m1"><p>سپهدار چون هفته‌ای سور کرد</p></div>
<div class="m2"><p>از آن پس شد آهنگ فغفور کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همه راه خاقان بپرداخته</p></div>
<div class="m2"><p>به هر جای نزل و علف ساخته </p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سه منزل بدش با سپه رهنمای همی </p></div>
<div class="m2"><p>ورا کرد بدرود و شد بازجای </p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شد شتابان سپهدار گو</p></div>
<div class="m2"><p>نریمان و زاول گره پیشرو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به مرز بیابانی آمد فراز زمینش </p></div>
<div class="m2"><p>که گفتی جهانیست گسترده باز </p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همه داغ پای پری</p></div>
<div class="m2"><p>زمانه گم اندر وی از رهبری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نه گردون سپرده درازای او</p></div>
<div class="m2"><p>نه خورشید پیموده پهنای او</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به هر سوش دیوی دژ آگاه بود </p></div>
<div class="m2"><p>به هر گوشه صد غول گمراه بود </p></div></div>
<div class="b" id="bn9"><div class="m1"><p>همان تار پرنده هزمان ز گرد </p></div>
<div class="m2"><p>چو تیر آمدی در نشستی به مرد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بکشتند از آن غول بسیار و مار </p></div>
<div class="m2"><p>به ده روز کردند از آنجا گذار </p></div></div>
<div class="b" id="bn11"><div class="m1"><p>رسیدند جایی چراگاه گور</p></div>
<div class="m2"><p>درو شیرگون چشمه آب شور</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چو نخچیر از تشنگی در گذار</p></div>
<div class="m2"><p>به نزدیک ان چشمه رفتی فراز </p></div></div>
<div class="b" id="bn13"><div class="m1"><p>شدی نرم نرم آب آن چشمه زیر </p></div>
<div class="m2"><p>پس آشفته گشتی چو غرنده شیر </p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بجستی و نخچیر را بی‌درنگ</p></div>
<div class="m2"><p>همان گه بیوباشتی چون نهنگ </p></div></div>
<div class="b" id="bn15"><div class="m1"><p>پس از یک زمان استخوانهاش پاک</p></div>
<div class="m2"><p>بدی گرد آن چشمه بر تیره خاک </p></div></div>
<div class="b" id="bn16"><div class="m1"><p>نه بشناخت آن آب را کس ز شیر </p></div>
<div class="m2"><p>نه دانست کز چیست نخچیرگیر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>دگر سنگ دیدند کوچک بسی</p></div>
<div class="m2"><p>که چون زآن دو بر هم بسودی کسی </p></div></div>
<div class="b" id="bn18"><div class="m1"><p>همان گاه بادی شگرف آمدی</p></div>
<div class="m2"><p>پس از باد باران و برف آمدی </p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ولیکن چو زآن جا به بومی دگر </p></div>
<div class="m2"><p>ببردی نبودی ورا آن هنر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>دگر سنگ بد نیز کز بیم نم</p></div>
<div class="m2"><p>چو ابر آمدی برزندی به هم</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>سبک زآن هوا ابر بگریختی</p></div>
<div class="m2"><p>نه روز برف و ژاله نه نم ریختی </p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ز مرز بیابان چو برتر کشید</p></div>
<div class="m2"><p>سپه را سوی شهر ساجر کشید </p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بزد خیمه با لشکر از گرد شهر</p></div>
<div class="m2"><p>برون شد که گیرد ز نخچیر بهر </p></div></div>
<div class="b" id="bn24"><div class="m1"><p>در و دشت و که دید زاندازه بیش </p></div>
<div class="m2"><p>رم گور و آهو و غژغا و میش </p></div></div>
<div class="b" id="bn25"><div class="m1"><p>همان روز بفکند بسیار گور</p></div>
<div class="m2"><p>به‌ خون‌ غرقه هر سو همی‌ تاخت بور </p></div></div>
<div class="b" id="bn26"><div class="m1"><p>درختی بر چشمه‌ساری بدید </p></div>
<div class="m2"><p>عنان ره انجام از آن سو کشید </p></div></div>
<div class="b" id="bn27"><div class="m1"><p>چو نزدیک‌ شد‌ خاست‌ یک بانگ‌ سخت </p></div>
<div class="m2"><p>زنی دید ناگه که جست از درخت </p></div></div>
<div class="b" id="bn28"><div class="m1"><p>یکی شیرخواره گرفته به بر</p></div>
<div class="m2"><p>همی تاخت ز آهو به تک تیزتر</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بپرسید کاین زن براینگونه چیست </p></div>
<div class="m2"><p>یکی گفت کاین هم چو ما آدمیست</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>درین بیشها گرد این دشت و کوه </p></div>
<div class="m2"><p>بدینسان بی‌اندازه بینی گروه </p></div></div>
<div class="b" id="bn31"><div class="m1"><p>چو آهو به تک همچو مردم به روی </p></div>
<div class="m2"><p>چو دیوان به‌ ناخن چو میشان به‌ موی </p></div></div>
<div class="b" id="bn32"><div class="m1"><p>ز بن هیچ با ما نگرند رام</p></div>
<div class="m2"><p>بمیرند زود آنچه گیری به دام </p></div></div>
<div class="b" id="bn33"><div class="m1"><p>از ایشان چو بیمار گردد یکی</p></div>
<div class="m2"><p>برندش براین تیغ کوه اندکی </p></div></div>
<div class="b" id="bn34"><div class="m1"><p>به شیونگری گردش اندر خروش </p></div>
<div class="m2"><p>برآرند و زی ابر دارند گوش </p></div></div>
<div class="b" id="bn35"><div class="m1"><p>گرش ابر تیره ز دیده به اشک</p></div>
<div class="m2"><p>بشوید درستی گرد بی‌ پزشک </p></div></div>
<div class="b" id="bn36"><div class="m1"><p>وگر هیچ باران نبارد ز میغ</p></div>
<div class="m2"><p>بمیرد به زیر افکنندش ز تیغ</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>نریمان یکی از درختی ربود</p></div>
<div class="m2"><p>بر پهلوان برد و او را نمود</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>به ره در همه بازویش خسته کرد</p></div>
<div class="m2"><p>همی بود تا مرد و چیزی نخورد </p></div></div>
<div class="b" id="bn39"><div class="m1"><p>ز نخچیر چون شد سپهدار باز </p></div>
<div class="m2"><p>بیآمد کس شاه ساجر فراز</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>فرستاده با هدیه بسیار چیز</p></div>
<div class="m2"><p>به پوزش پیامی نکو داده نیز</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>که دانم کز ایران به کین آمدی</p></div>
<div class="m2"><p>به پیکار فغفور چین آمدی</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>من او را یکی بنده کهترم</p></div>
<div class="m2"><p>نگهبان یک مرز ازین کشورم </p></div></div>
<div class="b" id="bn43"><div class="m1"><p>سه ماهه ز ما تا بدو هست راه </p></div>
<div class="m2"><p>نخستین ازو هر چه باید بخواه</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>هرآن گه کز او کام تو گشت راست </p></div>
<div class="m2"><p>همه بندگانیم و فرمان تراست</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>به هر شهر ازین مرز دیگر بپوی </p></div>
<div class="m2"><p>ز هر شاه باژی که باید بجوی </p></div></div>
<div class="b" id="bn46"><div class="m1"><p>سپهبد سخنهاش بر جای دید</p></div>
<div class="m2"><p>پسندید و آن کرد کاو رای دید </p></div></div>
<div class="b" id="bn47"><div class="m1"><p>ز زاول گره هر که بودند گرد</p></div>
<div class="m2"><p>همان گه به فرخ نریمان سپرد </p></div></div>
<div class="b" id="bn48"><div class="m1"><p>به هر شهر فرمود تا با سپاه</p></div>
<div class="m2"><p>بگردد ز شاهان بود باژخواه</p></div></div>