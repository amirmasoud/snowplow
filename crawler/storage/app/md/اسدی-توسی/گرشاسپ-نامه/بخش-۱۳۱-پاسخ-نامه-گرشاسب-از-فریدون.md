---
title: >-
    بخش ۱۳۱ - پاسخ نامه گرشاسب از فریدون
---
# بخش ۱۳۱ - پاسخ نامه گرشاسب از فریدون

<div class="b" id="bn1"><div class="m1"><p>نبشت آن گهی پاسخش باز و گفت</p></div>
<div class="m2"><p>رسید آن سخن های با مهر جفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یکی نامه گویا چو فرخ سروش</p></div>
<div class="m2"><p>که از در معنی صدف کرده گوش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پیام آورش مژده را مایه بود</p></div>
<div class="m2"><p>خرد را سخنهاش پیرایه بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>روان ها شد از مژده شادی سرشت</p></div>
<div class="m2"><p>به هر دل دری بگشاد از بهشت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ترا تا گشادست دست بلند</p></div>
<div class="m2"><p>بود بی گمان پای دشمن به بند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تو شیری و تیغ تو ز الماس ابر</p></div>
<div class="m2"><p>روان بار ابر و عنان دار ببر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هوا نیست نز گرد تو تیره فام</p></div>
<div class="m2"><p>زمین نیست نسپرده اسپت به گام</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز خون کف شیران به کفشیر تست</p></div>
<div class="m2"><p>دل و رزم و کین جفت شمشیر تست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هنرها چنین از تو نبود شگفت</p></div>
<div class="m2"><p>دلیری و رزم از تو باید گرفت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تو رنجی و من بر خورم از جهان</p></div>
<div class="m2"><p>همانا که تو دستی و من دهان </p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بیآمد به مژده نریمان گرد</p></div>
<div class="m2"><p>همه هر چه گفتی یکایک شمرد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>اگر چند فغفور کژی گزید</p></div>
<div class="m2"><p>ز ما راستکاری و خوبی سزید</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بدو چون ترا نیکویی بود رای</p></div>
<div class="m2"><p>به نیکی فرستادمش باز جای</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چو آید بدو باز بسپار چین</p></div>
<div class="m2"><p>به چینش از رخ بخت بزدای چین</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بر او باژ و ساو همه چین نخست</p></div>
<div class="m2"><p>نبشت و ستد عهدی از وی درست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>به نزل و علف هر که بودند شاه</p></div>
<div class="m2"><p>بفرمود کآیند پیشش به راه</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>دو منزل شدش همره و گشت باز</p></div>
<div class="m2"><p>سپه راند فغفور با کام و ناز</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>به بزم و به خوان هم بدان رسم پیش </p></div>
<div class="m2"><p>همی زیست در ره چو در شهر خویش</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بزرگان بدین مژده برخاستند</p></div>
<div class="m2"><p>همه چین و جندان بیاراستند</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>زمین سر به سر دیبه چین گرفت</p></div>
<div class="m2"><p>هوا از درم ریز پروین گرفت</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>همی هر سوی آذین دیبا زدند</p></div>
<div class="m2"><p>ز شادی ثری بر ثریا زدند</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>همه خاک ره گل شد از بس گلاب</p></div>
<div class="m2"><p>ز گل گل دمید از نرمی لعل ناب</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>صدف گشت هامون ز بس در نثار</p></div>
<div class="m2"><p>شد از نافه ابر آهوی مشک بار</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چنان بد ز بس گرد اسپ سپاه </p></div>
<div class="m2"><p>که از بر ندیدند کس مهر و ماه</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>جهان پهلوان با بزرگان چین</p></div>
<div class="m2"><p>پذیره شدش چند منزل زمین</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>چو فغفور بنهاد در کاخ پای</p></div>
<div class="m2"><p>بیامد سر خادمان سرای</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ز گرشاسب آزادی آورد پیش</p></div>
<div class="m2"><p>همان نیز خاتون از اندازه بیش</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>که بر ما ز تو مهر به داشتست</p></div>
<div class="m2"><p>پس پرده بیگانه نگذاشتست</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>ز دروای ما هر چه بایست نیز</p></div>
<div class="m2"><p>همی داد خرم ز هر گونه چیز</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>ازین مژده فغفور شادی گرفت</p></div>
<div class="m2"><p>چنین کار ازو گفت نبود شگفت</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>کند هر کس آن کآید از گوهرش</p></div>
<div class="m2"><p>که هر شاخ چون تخمش آرد برش</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>دگر روز شبگیر با فرهی</p></div>
<div class="m2"><p>چو بنشست برگاه شاهنشهی</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>بزرگان چین سر برافراختند</p></div>
<div class="m2"><p>بر شاه چین آمدن ساختند</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>سلب هر چه شان بد کبود و سیاه</p></div>
<div class="m2"><p>فکندند یکسر ز شادی شاه</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>چنان پادشاهی بر او راست شد</p></div>
<div class="m2"><p>کا گاهش بر از مه همی خواست شد</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>نخست از همه کس که بد نامدار</p></div>
<div class="m2"><p>جهان پهلوان برد پیشش نثار</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>خراجی که در چین ز هر سو فراز</p></div>
<div class="m2"><p>ستد بد بدو نیز بسپرد باز</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>بدو داد باز آن همه شاه چین</p></div>
<div class="m2"><p>بسی هدیه بخشید نیزش جز این</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>از آن پس به نزدیک شاه کیان</p></div>
<div class="m2"><p>یکی نامه فرمود بر پرنیان</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>گه رفتنش با مهان سپاه</p></div>
<div class="m2"><p>برون رفت پیشش دو منزل به راه</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>ورا کرد بدرود و برگشت شاد</p></div>
<div class="m2"><p>جهان پهلوان سر سوی ره نهاد</p></div></div>