---
title: >-
    بخش ۱۱۲ - نامه گرشاسب به خاقان
---
# بخش ۱۱۲ - نامه گرشاسب به خاقان

<div class="b" id="bn1"><div class="m1"><p>چو در کشورش پهلوان سپاه</p></div>
<div class="m2"><p>در و دشت زد خیمه بی‌راه و راه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نویسنده را گفت هین خامه گیر</p></div>
<div class="m2"><p>به خاقان یکی نامه کن بر حریر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بخوانش به فرمانبری پیش باز</p></div>
<div class="m2"><p>بگو باژ بپذیر یا رزم ساز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به دست دبیر اندرون شد قلم</p></div>
<div class="m2"><p>یکی ابر زرین کش از مشک نم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همی تاخت اشک گلاب و عبیر</p></div>
<div class="m2"><p>ز صحرای سیمین ز دریای قیر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو غواص زی در داننده راه</p></div>
<div class="m2"><p>همی‌ زد به دریای معنی شناه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر آن در که شایسته دیدی درست</p></div>
<div class="m2"><p>بسفتی به الماس دانش نخست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو سفتی برو مشک برتاختی</p></div>
<div class="m2"><p>وز اندیشه‌اش رشته‌ها ساختی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>همه نامه از در فرهنگ و هوش</p></div>
<div class="m2"><p>بیاراست چون تخت گوهر فروش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به نام جهان داور آغاز کرد</p></div>
<div class="m2"><p>که از تیره شب روز را باز کرد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گران ساخت خاک و سبک باد پاک</p></div>
<div class="m2"><p>روان گرد گردون و آرام خاک</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گهرها نگارید و تن‌ها سرشت</p></div>
<div class="m2"><p>سپردن رهش بر خردها نوشت</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>که گیتی به شاه آفریدون سپرد</p></div>
<div class="m2"><p>بدو سیرت بد ز کشور ببرد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ز ضحاک ناپاک بستند شهی</p></div>
<div class="m2"><p>برای فریدون با فرهی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نبشته شد این نامه دلفروز</p></div>
<div class="m2"><p>ز گرشاسب فرخ شه نیمروز</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>به خاقان یغر شاه توران زمین</p></div>
<div class="m2"><p>که مهرست شاهی و نامش نگین</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بدان ای سزا پیشگاه بلند</p></div>
<div class="m2"><p>که اختر یکی رای روشن فکند</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>سپهر از دل هر بربود درد</p></div>
<div class="m2"><p>ز چهر شهی بخت بزدود گرد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>جهان نوعروسی گرانمایه شد</p></div>
<div class="m2"><p>شهی تاجش و داد پیرایه شد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>زمانه نگاریدش از فر و چهر</p></div>
<div class="m2"><p>ستاره نثار آوریدش سپهر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>زدین جامه کرد ایزد اندر برش</p></div>
<div class="m2"><p>فلک زایمنی کله زد بر سرش</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چو این نوعروس از در گاه شد</p></div>
<div class="m2"><p>فریدون فرخ بر او شاه شد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>به فر کیی واختر خوب و بخت</p></div>
<div class="m2"><p>ز ضحاک تازی ستد تاج و تخت</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>برآمد به مه دین یزدان پاک</p></div>
<div class="m2"><p>سر جادویی‌ها فروشد به خاک</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>از ایران کنون من به فرمان شاه</p></div>
<div class="m2"><p>بدین مرز آن برکشیدم سپاه</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>که آیی به فرمانبری شاه را</p></div>
<div class="m2"><p>بوی خاکبوس آن کیی گاه را</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>نخست از تو خواهیم پرداختن</p></div>
<div class="m2"><p>پس آن گه به فغفور چین تاختن</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بدین نامه سر تا به سر پند تست</p></div>
<div class="m2"><p>به کار آری ار بخت پیوند تست</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>چو خواندی ز پیش آی پرداخته</p></div>
<div class="m2"><p>همه راه نزل و علف ساخته</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>سزا باژ بپذیر و هدیه بساز</p></div>
<div class="m2"><p>و گرنه به جنگ آر لشکر فراز</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>گه رزم پیروزی او را سزاست</p></div>
<div class="m2"><p>که بر دین کند رزم بر راه راست</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>چو پردخته شد نامه را مهر کرد</p></div>
<div class="m2"><p>فرستاد گردی شتابان چو گرد</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>فرستاده چون پیش شه شد زمین</p></div>
<div class="m2"><p>به رخسارگان رفت و کرد آفرین</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>به اسپ سخن داد پیش‌اش لگام</p></div>
<div class="m2"><p>بر آهخت تیغ پیام از نیام</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>به میدان دانش سواری گرفت</p></div>
<div class="m2"><p>چو بشنید شه بردباری گرفت</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>بدو گفت شاه تو از تخم کیست</p></div>
<div class="m2"><p>به‌ نزدیک او رسم ضحاک چیست</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>چه ورزد از آیین دین کم و بیش</p></div>
<div class="m2"><p>چه گوید ز یزدان و از راه کیش</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>چنین داد پاسخ که شه را نخست</p></div>
<div class="m2"><p>خرد باید و رای و راه درست</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>کف راد و داد و نژاد و گهر</p></div>
<div class="m2"><p>نکوکاری و راستگویی و فر</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>فریدون شه را بدینسان هزار</p></div>
<div class="m2"><p>هنر هست و هم یاری از روزگار</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>فزون‌ زان به‌ کوه اندرون‌ نیست سنگ</p></div>
<div class="m2"><p>که در گنج او گوهرست رنگ‌رنگ</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>رهش دین یزدان کیومرثی</p></div>
<div class="m2"><p>نژاد و بزرگیش طهمورثی</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>به دل کیش ضحاک را دشمنست</p></div>
<div class="m2"><p>به‌نزدش چه اوی و چه اهریمنست</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>بد و نیک از ایزد شناسد درست</p></div>
<div class="m2"><p>یکی داندش هم به دین درست</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>جهان گوید ایزد پدید آورید</p></div>
<div class="m2"><p>همو بازگرداندش ناپدید</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>به پل چینود که چون تیغ تیز</p></div>
<div class="m2"><p>گذارست و هم نامه و رستخیز</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>بپرسد خدای از همه خوب و زشت</p></div>
<div class="m2"><p>بدان راست دوزخ بهان را بهشت</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>برش پارسا مرد نامی ترست</p></div>
<div class="m2"><p>هم از زر دانش گرامی ترست</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>چنانست دادش که روباه پیر</p></div>
<div class="m2"><p>برد بچه را تا دهد شیر شیر</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>چو بشنید خاقان پسندید و گفت</p></div>
<div class="m2"><p>گراین هست شاه ترا نیست جفت</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>ولیکن چو پرسیدم از تو بسی</p></div>
<div class="m2"><p>بمان تا بپرسم ز دیگر کسی</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>اگر چند فرزند چون دیو زشت</p></div>
<div class="m2"><p>بود نزد مادر چو حور بهشت</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>هنر آن پسندیده‌تر دان و بیش</p></div>
<div class="m2"><p>که دشمن پسندد به ناکام خویش</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>نباید که شاهان پژوهش کنند</p></div>
<div class="m2"><p>مرا همچو غمران نکوهش کنند</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>برآسای یک هفته تا روی کار</p></div>
<div class="m2"><p>ببینیم و پاسخ کنیم آشکار</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>بفرمود کاخی سزاوار اوی</p></div>
<div class="m2"><p>بسازند درخور همه کار اوی</p></div></div>