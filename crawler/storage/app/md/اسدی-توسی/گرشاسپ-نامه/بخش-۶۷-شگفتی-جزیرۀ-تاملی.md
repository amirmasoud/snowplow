---
title: >-
    بخش ۶۷ - شگفتی  جزیرۀ  تاملی
---
# بخش ۶۷ - شگفتی  جزیرۀ  تاملی

<div class="b" id="bn1"><div class="m1"><p>سوی تاملی شاد خوار آمدند</p></div>
<div class="m2"><p>به نزدیک دریا کنار آمدند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پر انبوه مردم یکی جای بود</p></div>
<div class="m2"><p>همه بومشان باغ و کشت و درود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مگر آب خوش کان ز باران بدی</p></div>
<div class="m2"><p>بدلشان در اندوه و بار آن بدی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو بر روی چرخ ابر دامن کشان</p></div>
<div class="m2"><p>شدی چون صدف های لولو فشان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همه کوزه و مشک ها در شتاب</p></div>
<div class="m2"><p>بکردندی از قطر باران پر آب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو باران نبودی جگر تافته</p></div>
<div class="m2"><p>بُدندی لب از تشنگی کافته</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بپرسید ازیشان یل نامدار</p></div>
<div class="m2"><p>که باران نبارد چه سازید کار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بتی را نمودند و لوحی بهم</p></div>
<div class="m2"><p>ز مس لوح و آن بت ز چوب بقم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بر آن لوح چون خط یویانیان</p></div>
<div class="m2"><p>چهل حرف و شش هیکل اندر میان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به باران چو داریم گفتند کام</p></div>
<div class="m2"><p>برآریم این لوح و بت را به بام</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>پس این لوح و بت را به سر برنهیم</p></div>
<div class="m2"><p>نیایش کنان دست بر سر نهیم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>برهنه زن و مرد هر سو بسی</p></div>
<div class="m2"><p>زاری زده بر میان هر کسی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بگرییم و آریم چندان خروش</p></div>
<div class="m2"><p>که دریا و کُه گیرد از ناله جوش</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>همان گه بر آید یکی تیره ابر</p></div>
<div class="m2"><p>کند روی گردون چو پشت هژبر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چنان زآب دیده بشوید زمین</p></div>
<div class="m2"><p>کزو موج خیزد چو دریای چین</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>یل نیو گفتا کنون کایدریم</p></div>
<div class="m2"><p>کنید این که بی آزمون نگذریم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>نگیرد چنین چاره گفتند ساز</p></div>
<div class="m2"><p>جز آن گه که باشد به باران نیاز</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>کنون کآبمان هست ده ره بهم</p></div>
<div class="m2"><p>گرآییم ناید یکی قطره نم</p></div></div>