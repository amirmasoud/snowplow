---
title: >-
    بخش ۸۴ - جنگ شاه کابل با زابلیان وشکسته شدن اثرط
---
# بخش ۸۴ - جنگ شاه کابل با زابلیان وشکسته شدن اثرط

<div class="b" id="bn1"><div class="m1"><p>چوباز سپیده بزد پر باز </p></div>
<div class="m2"><p>از او زاغ شب شد گریزنده باز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شه کابل آورد لشکر به جنگ</p></div>
<div class="m2"><p>برابر دو صد برکشیدند تنگ</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بپیوست رزمی گران کز سپهر</p></div>
<div class="m2"><p>گریزنده شد ماه و گم گشت مهر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>برآورد ده و دار و گیر و گریز</p></div>
<div class="m2"><p>زهر سو سرافشان بُد و ترگ ریز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جهان جوش گردان سرکش گرفت</p></div>
<div class="m2"><p>به دریا زتیغ آب آتش گرفت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همه دشت تابان ز الماس بود</p></div>
<div class="m2"><p>همه کوه در بانگ سر پاس بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فکنده سر نیزه ی جان ستان</p></div>
<div class="m2"><p>یکی را نگون و یکی را ستان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زبس خون خسته زمی لاله زار</p></div>
<div class="m2"><p>وز آن خستگان خاسته ناله زار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تن پیل پرخون و پرتیر و خشت</p></div>
<div class="m2"><p>چو زآب بقم رسته بر کوه کشت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به تیغ وسنان و به گرز گران</p></div>
<div class="m2"><p>بکشتند چندان ز یکدیگران</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>که شد مرگ از آن خوار برچشم خویش</p></div>
<div class="m2"><p>سته گشت و نفرید بر خشم خویش</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دل جنگیان شد ز کوشش ستوه</p></div>
<div class="m2"><p>شکست اندر آمد به زاول گروه</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ز پیش سپه نوشیار دلیر</p></div>
<div class="m2"><p>درآمد بغرید چون تند شیر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>کزین غرچگان چیست چندین گریغ</p></div>
<div class="m2"><p>بکوشید هم پشت با گرز و تیغ</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>همان لشکرست این که در کارزار</p></div>
<div class="m2"><p>گریزان شدند از شما چند بار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>سپه را به یک بار پس باز برد</p></div>
<div class="m2"><p>به نیزه فکند از یلان چند گرد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>تنوره زد از گردش اندر سپاه</p></div>
<div class="m2"><p>ز هر سو به زخمش گرفتند راه</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بینداختندش به شمشیر دست</p></div>
<div class="m2"><p>فکندند بی جانش بر خاک پست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>پسرش از دلیری بیفشرد پای</p></div>
<div class="m2"><p>ستد کینه زان جنگجویان بجای</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>نخست از یلان پنج بفکند تفت</p></div>
<div class="m2"><p>پدر را ببست از بر زین و رفت</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>دلیران زاول همه ترگ و تیغ</p></div>
<div class="m2"><p>فکندند و جستند راه گریغ</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>از ایشان همه دشت سر بود ودست</p></div>
<div class="m2"><p>گرفتند بسیار و کشتند و خست</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>چوشب خیمه زد از پرند سیاه</p></div>
<div class="m2"><p>درو فرش سیمین بگسترد ماه</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>شه کابل آنجا که پیروز گشت</p></div>
<div class="m2"><p>بزد با سپه پرخون و پرخاک و گرد</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>گریزندگان نزد اثرط به درد</p></div>
<div class="m2"><p>رسیدند پرخون و پرخاک و گرد</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بدادندش از هر چه بُد آگهی</p></div>
<div class="m2"><p>بماند از هش و رای مغزش تهی</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>زدرد سپه وز غم نوشیار</p></div>
<div class="m2"><p>به دل درش با زهر شد نوش یار</p></div></div>