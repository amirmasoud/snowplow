---
title: >-
    بخش ۱۰۶ - پذیره شدن شاه روم گرشاسب را
---
# بخش ۱۰۶ - پذیره شدن شاه روم گرشاسب را

<div class="b" id="bn1"><div class="m1"><p>سه منزل پذیره شدش با سپاه</p></div>
<div class="m2"><p>زد آذین دیبا و گنبد به راه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیاراست ایوان چو باغ ارم</p></div>
<div class="m2"><p>نثارش گهر کرد و مشک و درم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به شادیش بر تخت شاهی نشاست</p></div>
<div class="m2"><p>بسی پوزش از بهر دختر بخواست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بدش نغز رامشگری چنگ زن</p></div>
<div class="m2"><p>یکی نیمه مرد و یکی نیمه زن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سر هر دو از تن به هم رسته بود</p></div>
<div class="m2"><p>تنانشان به هم باز پیوسته بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چنان کآن زدی این زدی نیز رود</p></div>
<div class="m2"><p>ورآن گفتی این نیز گفتی سرود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یکی گر شدی سیر از خورد و چیز</p></div>
<div class="m2"><p>بدی آن دگر هم چنو سیر نیز</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بفرمود تا هر دو می خواستند</p></div>
<div class="m2"><p>ره چنگ رومی بیاراستند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نواشان ز خوشی همی برد هوش</p></div>
<div class="m2"><p>فکند از هوا مرغ را در خروش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ببودند یک هفته دلشاد و مست</p></div>
<div class="m2"><p>که ناسود یک ساعت از جام دست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سر هفته با پهلوان شاه شاد</p></div>
<div class="m2"><p>یکی کاخ شاهانه را در گشاد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>سرایی پدید آمد آراسته</p></div>
<div class="m2"><p>به از نو بهشتی پر از خواسته</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>دراو خرم ایوان برابر چهار</p></div>
<div class="m2"><p>ز رنگش گهرها چو باغ بهار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>یکی قصرش از سیم و دیگر ز زر</p></div>
<div class="m2"><p>سیم جزع و چارم بلورین گهر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>درش بر شبه در و بیجاده بود</p></div>
<div class="m2"><p>زمینش همه مرمر ساده بود</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>دو صد خانه هم زین نشان در سرای</p></div>
<div class="m2"><p>سراسر به سیمین ستون ها بپای</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>به هر خانه در تختی از پیشگاه</p></div>
<div class="m2"><p>بر تخت زرین یکی زیرگاه</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>به هر تخت بر خسروی افسری</p></div>
<div class="m2"><p>سزاوار هر افسری پرگری</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>در آن روشن ایوان که بود از بلور</p></div>
<div class="m2"><p>دو بت کرده زرین چو ماه و چو هور</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>یکی چون از چهره دیگر چو مرد</p></div>
<div class="m2"><p>ز یاقوتشان تاج و از لاژورد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>دو صد گونه کرسی در ایوان ز زر</p></div>
<div class="m2"><p>بتی کرده بر هر یکی از گهر</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>یکی خادم از پیش هر بت شمن</p></div>
<div class="m2"><p>بر آتش دمان مشک و عنبر به من</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>یکی میل از سیم بفراخته</p></div>
<div class="m2"><p>یکی چرخ گردان بر آن ساخته</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ز زر برج ها و اختران سپهر</p></div>
<div class="m2"><p>روان کرده از چرخ با ماه و مهر</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>شب و روز با ساعت و سال و ماه</p></div>
<div class="m2"><p>بدیدی در او هر که کردی نگاه</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>به پدرام باغی شد اندر سرای</p></div>
<div class="m2"><p>چو باغ بهشتی خوش و دلگشای</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>برآورده دیوارها از رخام</p></div>
<div class="m2"><p>رهش مرمر و جوی ها سیم خام</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>به دیوار بر جوی ها ساخته</p></div>
<div class="m2"><p>به هر نایژه آب رز تاخته</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>همه باغ طاووس و رنگین تذرو</p></div>
<div class="m2"><p>خرامنده در سایه نوژ و غرو</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>گلی بد که شب تافتی چون چراغ</p></div>
<div class="m2"><p>به روزی دو ره بشکفیدی به باغ</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>دو صد گونه گل بد میان فرزد</p></div>
<div class="m2"><p>فروزان چو در شب ز چرخ اورمزد</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>گلی بد که همواره کفته بدی</p></div>
<div class="m2"><p>به گرما و سرما شکفته بدی</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>درخت فراوان بد از میوه دار</p></div>
<div class="m2"><p>به هر شاخ بر پنج شش گونه بار</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>قفس ها ز هرشاخی آویخته</p></div>
<div class="m2"><p>در او مرغ دستان برانگیخته</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>به هر گوشه از زر یکی آبگیر</p></div>
<div class="m2"><p>گلاب آبش و ریگ مشک و عبیر</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>بسی ماهی از سیم و از زر ناب</p></div>
<div class="m2"><p>به نیرنگ کرده روان زیر آب</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>در آن باغ یک ماه دیگر به ناز</p></div>
<div class="m2"><p>ببودند و با باده و رود و ساز</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>سر مه یکی نامه آمد پگاه</p></div>
<div class="m2"><p>ز جفت سپهبد به نزدیک شاه</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>بسی لابه ها ساخته زی پدر</p></div>
<div class="m2"><p>که از پهلوان چیست نزدت خبر</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>ز هرچ آگهی زو سود ار گزند</p></div>
<div class="m2"><p>بدان هم رسان زود نزدم نوند</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>که هست از گه رفتنش سال پنج</p></div>
<div class="m2"><p>من اندر جداییش با درد و رنج</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>تنم گویی از غم به خار اندرست</p></div>
<div class="m2"><p>دل از تف به خونین بخار اندرست</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>از آن روز کم روشنی بهره نیست</p></div>
<div class="m2"><p>مرا باری آن روز با شب یکسیت</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>مدان هیچ درد آشکار و نهفت</p></div>
<div class="m2"><p>چو درد جدایی ز شایسته جفت</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>بجوشید مغز سپهبد ز مهر</p></div>
<div class="m2"><p>به خون زآب مژگان بیاراست چهر</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>کهن بویه جفت نو باز کرد</p></div>
<div class="m2"><p>هم اندر زمان راه را ساز کرد</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>به شهر کسان گر چه بسیار بود</p></div>
<div class="m2"><p>دل از خانه نشکیبد و زاد و بود</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>بدانست رازش نهان شاه روم</p></div>
<div class="m2"><p>شد از غم گدازنده مانند موم</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>سبک هدیه دختر از تخت عاج</p></div>
<div class="m2"><p>بیاراست با افسر و طوق و تاج</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>هم از یاره و زیور و گوشواره</p></div>
<div class="m2"><p>دو نعلین زرین گوهر نگار</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>ز دیبا و پرنون شتروار شست</p></div>
<div class="m2"><p>ز پوشیدنی جامه پنجاه دست</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>پرستار تیرست و خادم چهل</p></div>
<div class="m2"><p>طرازی دو صد ریدک دلگسل</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>ز زرینه آلت به خروارها</p></div>
<div class="m2"><p>ز فرش و طوایف دگر بارها</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>عماری ده از عود بسته به زر</p></div>
<div class="m2"><p>کمرشان بر از رستهای گهر</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>از استر صد آرایش بارگاه</p></div>
<div class="m2"><p>یکی نیمه زآن چرمه دیگر سیاه</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>همیدون سزاوار داماد نیز</p></div>
<div class="m2"><p>بیاراست از هدیه هر گونه چیز</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>ز دیبا و دینار و خفتان و تیغ</p></div>
<div class="m2"><p>هم از تازی اسپان چو پوینده میغ</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>بی اندازه سیمین و زرین دده</p></div>
<div class="m2"><p>درون مشک و بیرون به زر آزده</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>روان کوشکی یکسر از عود خام</p></div>
<div class="m2"><p>به زرین فش و بند زرین قوام</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>یکی ماه کردار زرین سپر</p></div>
<div class="m2"><p>کلاهی چو پروین ز رخشان گهر</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>هم از بهر ضحاک یک ساله نیز</p></div>
<div class="m2"><p>بدو داد باژ و ز هرگونه چیز</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>ببخشید گنجی به ایران سپاه</p></div>
<div class="m2"><p>برون رفت یک روزه با او به راه</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>ورا کرد بدرود و زاو گشت باز</p></div>
<div class="m2"><p>سپهدار برداشت راه دراز</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>فرستاد کس نزد عم زاد خویش</p></div>
<div class="m2"><p>که در طنجه بگذاشت بودش ز پیش</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>بفرمود تا نزد او بی هراس</p></div>
<div class="m2"><p>به راه آورد لشکر و منهراس</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>به طرطوس شد کرد ماهی درنگ</p></div>
<div class="m2"><p>سپه برد از آنجا به دژهوخت گنگ</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>چو شد نزد ضحاک شاه آگهی</p></div>
<div class="m2"><p>بیاراست ایوان و تخت شهی</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>سپه پاک با سروان سترگ</p></div>
<div class="m2"><p>همان پیل و بالا و کوس بزرگ</p></div></div>