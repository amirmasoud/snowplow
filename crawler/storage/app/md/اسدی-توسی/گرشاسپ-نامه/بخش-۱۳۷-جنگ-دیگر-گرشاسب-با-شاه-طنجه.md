---
title: >-
    بخش ۱۳۷ - جنگ دیگر گرشاسب با شاه طنجه
---
# بخش ۱۳۷ - جنگ دیگر گرشاسب با شاه طنجه

<div class="b" id="bn1"><div class="m1"><p>چو شاه حبش سوی خاور گریخت</p></div>
<div class="m2"><p>همه رخت و دینار و گوهر بریخت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شه روم بنشست بر تخت عاج</p></div>
<div class="m2"><p>درآویخت زایوان پیروزه تاج</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دو لشکر به هم کینه خواه آمدند</p></div>
<div class="m2"><p>دلیران ناوردگاه آمدند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>غو کوس تند شد و گرد میغ</p></div>
<div class="m2"><p>در آن میغ خون آب شد برق تیغ</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>برآویخت یک باره با مهر خشم</p></div>
<div class="m2"><p>خرد را سترگی فرو بست چشم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همی تاخت خنجر ز گرد سیاه</p></div>
<div class="m2"><p>چو ایمان پاک از میان گناه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کمان شد یکی برزگر تخم کار</p></div>
<div class="m2"><p>وزآن تخم پیکان و دل کشتزار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از آن تخم هر کشت کآمد درست</p></div>
<div class="m2"><p>ز خون خورد آب و برش مرگ رست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز پاشیده خرطوم پیلان به تیغ</p></div>
<div class="m2"><p>تو گفی همه مار بارد ز میغ</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سر خشت گفتی می آشام شد</p></div>
<div class="m2"><p>صفش بزم و می خون و دل جام شد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دلیران بر اسپان کفک افکنان</p></div>
<div class="m2"><p>بدین دست گرز و به دیگر عنان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>روان خون به زخم از بر پشت پیل</p></div>
<div class="m2"><p>چو ز آب بقم چشمه بر کوه نیل</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>روان هر سوی اسپی هراسان ز جای</p></div>
<div class="m2"><p>سوارش نه پیدا و زین زیر پای</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>سپهدار بر زنده پیلی دمان</p></div>
<div class="m2"><p>همی تاخت آورده بر زه کمان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>کجا بد سری با درفشی به دست</p></div>
<div class="m2"><p>به پیکان همی دوخت و افکند پست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ز تیرش تو گفتی که در مغز و ترگ</p></div>
<div class="m2"><p>همی آشیان کرد زنبور مرگ</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چو یک چند بر پیل پیوست جنگ</p></div>
<div class="m2"><p>پیاده ببد تیغ و نیزه به چنگ</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>برد بر کمربند چاک زره</p></div>
<div class="m2"><p>به نعره گسست از گریبان گره</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>به تیغ و سنان هر کجا کینه توخت</p></div>
<div class="m2"><p>گهی دل درید و گهی سینه دوخت</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>همی داد شمشیرش اندر شتاب</p></div>
<div class="m2"><p>هم اندر هوا کرکسان را کباب</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>به هر بار کاو گرز بفراشتی</p></div>
<div class="m2"><p>به زنهار مه بانگ برداشتی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>به هر تیر کاو برگشادی ز زه</p></div>
<div class="m2"><p>زمانه زدی نعره گفتی که زه</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>سر خنجرش لاله کارنده بود</p></div>
<div class="m2"><p>ز درع یلان حلقه بارنده بود</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>تو گفتی به هر حلقه گردون دو نیم</p></div>
<div class="m2"><p>همی وی نگارد ز پولاد میم </p></div></div>
<div class="b" id="bn25"><div class="m1"><p>هزار از دلیران جوینده کین</p></div>
<div class="m2"><p>به گردش تنوره زدند از کمین</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بدانسان زدندش همی چپ و راست</p></div>
<div class="m2"><p>که در کوه و دریا چکاچاک خاست</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>شل و خنجر و گرز چندان سپاه</p></div>
<div class="m2"><p>چه بر ترگ او بر چه بر کوه کاه</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>تو گفتی همی زخم آن سرکشان</p></div>
<div class="m2"><p>گل افشان شمردی نه آهن فشان</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>شه طنجه آمد چو تند اژدها</p></div>
<div class="m2"><p>بر او کرد در گرد خشتی رها</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>نبد سود برگاشت روی از نبرد</p></div>
<div class="m2"><p>برادرش پیش اندر آمد چو گرد</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>بپوشیده خفتان و نیزه به دست</p></div>
<div class="m2"><p>برادرش به زین اسپ چون کوه پولاد بست</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>بینداخت زی پهلان خشت و رفت</p></div>
<div class="m2"><p>پسش پهلوان رفت چون باد تفت</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>گرفتش دم اسپ و از جای خویش</p></div>
<div class="m2"><p>برآورد و بنداخت سی گام پیش</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>برآنگونه زد نعره ی کوه کاف</p></div>
<div class="m2"><p>که سیمرغ بگریخت از کوه قاف</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>تن افکند بر قلب لشکر به کین</p></div>
<div class="m2"><p>دلیران ایران پسش هم چنین</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>چنان جنگ بر جنگیان تیز شد</p></div>
<div class="m2"><p>که دست و گریبان هم آویز شد</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>تو گفتی ز خون چرخ جوشد همی</p></div>
<div class="m2"><p>زمین چادر لعل پوشد همی</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>به هر گوشه آویزش سخت بود</p></div>
<div class="m2"><p>سر و کار با گردش بخت بود</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>ز غریدن کوس ترسان هژبر</p></div>
<div class="m2"><p>عقاب از تف تیغ پران در ابر</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>ز گرد آسمان در سیاهی شده</p></div>
<div class="m2"><p>ز جوشن زمین پشت ماهی شده</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>بریده ز تن جان امید از نهیب</p></div>
<div class="m2"><p>چو عشق از دل مهرجویان شکیب</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>گشاینده شمشیر بند از زره</p></div>
<div class="m2"><p>چو باد از سر زلف خوبان گره</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>چو ابرش شده چرمه از خون مرد</p></div>
<div class="m2"><p>شده باز چون چرمه ابرش ز گرد</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>یلان را رخ و کام پرخون و خاک</p></div>
<div class="m2"><p>چه خفتان چه برگستوان چاک چاک</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>بریده بر او جوشن از تیغ تیز</p></div>
<div class="m2"><p>زره پاره و ترگ ها ریز ریز</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>فسرده به خون اندرون تیغ ز مشت</p></div>
<div class="m2"><p>پر از آبله کف ز زخم درشت</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>شه طنجه برگاشت روی از نهیب</p></div>
<div class="m2"><p>سپاهش گرفتند بالا و شیب</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>گریزنده دیدی گروها گروه</p></div>
<div class="m2"><p>چه از سوی دریا چه از سوی کوه</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>چو نخچیر بر که یکی با شتاب</p></div>
<div class="m2"><p>یکی همچو ماهی دوان زیر آب</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>دگر تن به شهر اندر انداختند</p></div>
<div class="m2"><p>به باره ره جنگ برساختند</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>چو بفکند زرین سپر آسمان</p></div>
<div class="m2"><p>مه نو به زه کرد سیمین کمان</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>خبر زان بنه شد به گرشاسب زود</p></div>
<div class="m2"><p>کجا شه به کشتی فرستاده بود</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>برافکند کس تا گرفتند پاک</p></div>
<div class="m2"><p>شه طنجه را دل شد از درد چاک</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>فروهشت در شب ز باره رسن</p></div>
<div class="m2"><p>به دریا گریزنده شد با دو تن</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>سیه پوش گیتی چو شد زرد پوش</p></div>
<div class="m2"><p>که کهربا برزد از چرخ جوش</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>سپهدار با شهر برساخت جنگ</p></div>
<div class="m2"><p>بپیوست رزمی گران بی درنگ</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>چو لشکر شد آگه که بگریخت شاه</p></div>
<div class="m2"><p>دگر کس نیارست شد رزم خواه</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>تن از باره یکسر فکندند زیر</p></div>
<div class="m2"><p>به کین دست ایرانیان گشت چیر</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>فکندند در شهر خرسنگ و خاک</p></div>
<div class="m2"><p>از آن پس به آتش سپردند پاک</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>شه طنجه را نزد دریا کنار</p></div>
<div class="m2"><p>گرفتند از ایران گروهی سوار</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>که زورقش را باد گم کرده بود</p></div>
<div class="m2"><p>ز دریا به خشک از پس آورده بود</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>ورا زی سپهدار با آن دو تن</p></div>
<div class="m2"><p>ببردند در حلق بسته رسن</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>سپهدار گفت ای بد زشت کیش</p></div>
<div class="m2"><p>خوی بد چنین آورد کار پیش</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>خوی نیک همچون فرشتست پاک</p></div>
<div class="m2"><p>خوی بد چو دیوست بی ترس و باک</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>ز فرزند وز جفت و تخت شهی</p></div>
<div class="m2"><p>بماندی و خواهی شد از جان تهی</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>پس آن خواسته جملگی را درست</p></div>
<div class="m2"><p>همیدون از آن هر دو تن بازجست</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>ببریدشان گوشت یکسر به گاز</p></div>
<div class="m2"><p>بمردند و کس هیچ نگشاد راز</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>چنینست کار طمع را نهاد</p></div>
<div class="m2"><p>بسا کس که داد از طمع جان به باد</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>ز طمعست کوته زبان مرد آز</p></div>
<div class="m2"><p>چو شد طمع کوته زبان شد دراز</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>چو برداشتی طمع از آنچت هواست</p></div>
<div class="m2"><p>سخن گر ز کس برنداری رواست</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>از آن هر سه چون پهلوان دل بشست</p></div>
<div class="m2"><p>همه کاخ شه گشت و هر سو بجست</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>ز سنگ سیه خانه ای ناگهان</p></div>
<div class="m2"><p>بدید اندرو کرده گنجش نهان</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>همه چیزها یک به یک برده نام</p></div>
<div class="m2"><p>به سنگ اندرون کنده دیوار و بام</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>به در بر نوشته که این خواسته</p></div>
<div class="m2"><p>جهان پهلوان راست ناکاسته</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>ببد شاد دل وز جهان آفرین</p></div>
<div class="m2"><p>برآن شاه کآن ساخت کرد آفرین</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>ببرد آن همه خواسته سر به سر</p></div>
<div class="m2"><p>از آن پس نیازرد کس را دگر</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>همه طنجه را از سر آباد کرد</p></div>
<div class="m2"><p>اسیرانش را یکسر آزاد کرد</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>فراوان ز هر شهر و هر بوم و مرز</p></div>
<div class="m2"><p>نشاند اندرو مردم کشت ورز</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>هم از تخم شه پادشاهی نشاست</p></div>
<div class="m2"><p>بر او رسم باژ آنچه بد کرد راست</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>نوندی بدین مژده زی شهریار</p></div>
<div class="m2"><p>در افکند و ره را برآراست کار</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>چه چیز آمد این خواسته کز جهان</p></div>
<div class="m2"><p>کسی نیست بی آزش اندر نهان</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>چو باشد جهانی بدو دشمنست</p></div>
<div class="m2"><p>چو نبود غم جان و رنج تنست</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>ایا آز را داده گردن به مهر</p></div>
<div class="m2"><p>دوان پیش او هر زمان تازه چهر</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>به گیتی در آنست درویش تر</p></div>
<div class="m2"><p>کش از آز بر دل گره بیش تر</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>هر آن سر که او آز را افسرست</p></div>
<div class="m2"><p>به خاک اندرست ار ز مه برترست</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>بوی بنده آز تا زنده ای</p></div>
<div class="m2"><p>پس آزاد هرگز نئی بنده ای</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>یکی چاه تاریک ژرفست آز</p></div>
<div class="m2"><p>بنش ناپدید و سرش پهن باز</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>سراییست بروی بی اندازه در</p></div>
<div class="m2"><p>چو یک در ببندی گشاید دگر</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>به هر راه غولیست گسترده دام</p></div>
<div class="m2"><p>منه تا توان اندرین دام گام</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>پراکنده عمر و درم گرد گشت</p></div>
<div class="m2"><p>بخور کت به خواری بباید گذشت</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>چنان کآمدی رفت خواهی تهی</p></div>
<div class="m2"><p>تو گنج از پی گنج بانی نهی</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>نهم گویی ازبهرفرزند چیز</p></div>
<div class="m2"><p>مبر غم که چیزش بود بی تو نیز</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>کسی را جهانبان ز بن نافرید</p></div>
<div class="m2"><p>که از پیش روزی نکردش پدید</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>ترا داد و آنکس که پیوند تست</p></div>
<div class="m2"><p>دهد نیز آن را که فرزند تست</p></div></div>