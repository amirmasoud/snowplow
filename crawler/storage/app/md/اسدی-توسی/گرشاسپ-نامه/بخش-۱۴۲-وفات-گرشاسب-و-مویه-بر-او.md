---
title: >-
    بخش ۱۴۲ - وفات گرشاسب و مویه بر او
---
# بخش ۱۴۲ - وفات گرشاسب و مویه بر او

<div class="b" id="bn1"><div class="m1"><p>از آن پس چو روز دهم بود خواست</p></div>
<div class="m2"><p>خورش آرزو کرد و بنشست راست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بخورد اندکی وز خورش بازماند</p></div>
<div class="m2"><p>سبک سام را با نریمان بخواند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چنین گفت کز بهر زخمم زمان</p></div>
<div class="m2"><p>گشاید کنون مرگ تیر از کمان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بوید از پی جان غمگین من</p></div>
<div class="m2"><p>یک امروز هردو به بالین من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مگر کم روان چون هراسان شود</p></div>
<div class="m2"><p>به روی شما مرگم آسان شود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بگفت این و از دیده آب دریغ</p></div>
<div class="m2"><p>ببارید چون ژاله بارد ز میغ</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دمش هر زمان گشت کوتاه تر</p></div>
<div class="m2"><p>دلش زان دگر گیتی آگاه تر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به لب باد سردی برآورد و گفت</p></div>
<div class="m2"><p>که ای پاک دادار بی یار و جفت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جهان را جهاندار و یزدان توی</p></div>
<div class="m2"><p>برآرنده چرخ گردان توی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زمین و زمان کرده تست راست</p></div>
<div class="m2"><p>برآن و براین پادشایی تراست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>همه پادشاهان به تو زنده اند</p></div>
<div class="m2"><p>توی پادشه دیگران بنده اند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به تو هم به پیغمبران تو پاک</p></div>
<div class="m2"><p>گوایی دهم ترسم از تست و باک</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>پشیمانم از هرچه کردم گناه</p></div>
<div class="m2"><p>ببخشای و نزد خودم ده پناه</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چو گفت این سخن جان به یزدان سپرد</p></div>
<div class="m2"><p>گرفتند زاری بزرگان و خرد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>از ایوان به کیوان برآمد خروش</p></div>
<div class="m2"><p>زبر زآن فغان خاست و ز شهر جوش</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بر آن خانه پاک آتش اندر زدند</p></div>
<div class="m2"><p>همه کاخ و گلشن به هم برزدند</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>دل و جان هرکس چنان غم گرفت</p></div>
<div class="m2"><p>که ماهی به دریاب ماتم گرفت</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>هوا زاشک مرغان پر از ژاله شد</p></div>
<div class="m2"><p>که از بانگ نخچیر پرناله شد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>همان روز بگرفت نیز آفتاب</p></div>
<div class="m2"><p>نمود ابر از آن پی به باران شتاب</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>به هر گوشه ای گریه ای خاسته</p></div>
<div class="m2"><p>به هر خانه ای شیون آراسته</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>زنان رخ زنان بانگ و زاری کنان</p></div>
<div class="m2"><p>کنان مویه و موی مشکین کنان</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>به فندق دو گلنار کرده فکار</p></div>
<div class="m2"><p>به در از دو پیلسته شویان نگار</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بزرگان همه در سیاه و کبود</p></div>
<div class="m2"><p>ز دو دیده ابر از دو رخ کرده رود</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>سرشک همه لعل و رخسار زرد</p></div>
<div class="m2"><p>بر از زخم نیلی و لب لاجورد</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بریده دم اسپ بیش از هزار</p></div>
<div class="m2"><p>نگون کرده زین و آلت کارزار</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ز خون پشت صندوق پیلان بنفش</p></div>
<div class="m2"><p>شکسته تبیره دریده درفش</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>عقابان و بازان رها کرده پاک</p></div>
<div class="m2"><p>بر یوز و پیلان پر از گرد و خاک</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>در ایوانش بردند بر تخت زر</p></div>
<div class="m2"><p>بپوشیده خفتان و بسته کمر</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>یکی گرز بر کتف و تیغ آخته</p></div>
<div class="m2"><p>درفشش فراز سر افراخته</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>به برگستوان باره پیشش بپای</p></div>
<div class="m2"><p>برو هرکسی گشته زاری فزای</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>همی گفت سام ای یل سرفراز</p></div>
<div class="m2"><p>برفتی چنان کت نبینیم باز</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>درفشان مهی بودی از راستی</p></div>
<div class="m2"><p>چو گشتی تمام آمدت کاستی</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>نبود از تو نزدیکتر کس دگر</p></div>
<div class="m2"><p>کنون از توام نیست کس دورتر</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>به تو شادتر من بدم زانجمن</p></div>
<div class="m2"><p>کسی نیست غمگین تر اکنون ز من</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>ببستی در بار چون بر سپاه</p></div>
<div class="m2"><p>شدی سوی آن برترین جایگاه</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>همانا که در خواب خوش رفته ای</p></div>
<div class="m2"><p>چه خوابی که تا جاودان خفته ای</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>نریمان همی گفت زار ای دلیر</p></div>
<div class="m2"><p>کجات آن دل و زور و بازوی چیر</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>کجات آن سواری وصف ساختن</p></div>
<div class="m2"><p>کجات آن به هر کشوری تاختن</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>جهان گشتی و رنج برداشتی</p></div>
<div class="m2"><p>چو گنجت بینباشت بگذاشتی</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>همه کشورت کز تو آباد شد</p></div>
<div class="m2"><p>به باد پسین دست با باد شد</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>کهان سوی فرمانت دارند چشم</p></div>
<div class="m2"><p>چبودت که با ما به جنگی و خشم</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>نه در بزم دینار باری همی</p></div>
<div class="m2"><p>نه در رزم خنجر گزاری همی</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>نمودی به هر کشور آیین خویش</p></div>
<div class="m2"><p>کشیدی ز هر دشمنی کین خویش</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>کنون باز رزم از چه آراستی</p></div>
<div class="m2"><p>که اسپ و سلیح و کمر خواستی</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>به هند ار به چین برد خواهی سپاه</p></div>
<div class="m2"><p>که بر مه کشیدی درفش سیاه</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>بدی از دل و دست دریا و میغ</p></div>
<div class="m2"><p>یکی مشت خاکی کنون ای دریغ</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>دریغا تهی از تو زابلستان</p></div>
<div class="m2"><p>دریغا جهان بی تو کشور ستان</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>دریغا که بدخواه دلشاد گشت</p></div>
<div class="m2"><p>دریغا که رنجت همه باد گشت</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>همی گرید ابر از دریغت به مهر</p></div>
<div class="m2"><p>سلب هم به سوکت سی کرد چهر</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>کس از مرگ نرسد به مردی و فر</p></div>
<div class="m2"><p>کجا تو نرستی به چندین هنر</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>چو شیون از اندازه بگذاشتند</p></div>
<div class="m2"><p>پس آنگاهش از تخت برداشتند</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>به مشک و گلابش بشستند پاک</p></div>
<div class="m2"><p>سپردندش اندر ستودان به خاک</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>ببسند از آن پس برش راه بار</p></div>
<div class="m2"><p>نبد پهلوان گفتی از بیخ و بار</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>چنینست گیتی ز نزدیک و دور</p></div>
<div class="m2"><p>گهی سوگ و ماتم گهی بزم و سور</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>به کردار دریاست کز وی به چنگ</p></div>
<div class="m2"><p>یکی در دارد یکی ریگ و سنگ</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>سرانجام از او ایمنی نیست روی</p></div>
<div class="m2"><p>که هر کش پرستد بمیرد در اوی</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>چو پایی تو ای پیر مانده شگفت</p></div>
<div class="m2"><p>که بارت شد و کاروان برگرفت</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>به پیری چرا گشت آز تو بیش</p></div>
<div class="m2"><p>جوانان نگر چند رفتند پیش</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>ترا آنکه شد گوش دارد همی</p></div>
<div class="m2"><p>وزاو دل ترا یاد نارد همی</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>چو همراه شد توشه ساز و مییست</p></div>
<div class="m2"><p>که دورست ره وز شدن چاره نیست</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>درین ره مدان توشه و بار نیک</p></div>
<div class="m2"><p>به از دانش نیک و کردار نیک</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>از این گیتی ار پاک و دانا شوی</p></div>
<div class="m2"><p>به هر گامی آنجا توانا شوی</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>که نادان بد آنجای خوارست و زشت</p></div>
<div class="m2"><p>شه آنجاست درویش نیکو سرشت</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>به دانایی این ره به جایی بری</p></div>
<div class="m2"><p>به بی دانشی هیچ ره نسپری</p></div></div>