---
title: >-
    بخش ۱۱۳ - قصه خاقان با برادرزاده
---
# بخش ۱۱۳ - قصه خاقان با برادرزاده

<div class="b" id="bn1"><div class="m1"><p>برادر بد آن شاه را سروری</p></div>
<div class="m2"><p>خنیده به مردی به هر کشوری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پدرشان ز گیتی چو بربست رخت</p></div>
<div class="m2"><p>شدند این دو جوینده تاج و تخت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زمانی نشدشان دل از جنگ سیر</p></div>
<div class="m2"><p>سرانجام خاقان یغر گشت چیر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>برادرش کشته شد از پیش اوی</p></div>
<div class="m2"><p>پسر ماند از او سرکشی کینه‌جوی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دلیری که نامش تکین‌تاش بود</p></div>
<div class="m2"><p>همه ساله با عم به پرخاش بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نهان هر گهی تاختن ساختی</p></div>
<div class="m2"><p>به تاراج بومش برانداختی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زمانی ز کین پدر توختن</p></div>
<div class="m2"><p>نیاسودی از غارت و سوختن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>یکی بهره بگرفته بد کشورش</p></div>
<div class="m2"><p>شکسته بسی گونه‌گون لشکرش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>همین هفته کآمد سپهبد فراز</p></div>
<div class="m2"><p>همی خواست آمد سوی جنگ باز</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در اندیشه خاقان گرفتار بود</p></div>
<div class="m2"><p>کش از هر دو سو رزم‌ و پیکار بود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به هم با مهان انجمن کرد و گفت</p></div>
<div class="m2"><p>که گردون ندانم چه دارد نهفت</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>از این پهلوان وز برادر پسر</p></div>
<div class="m2"><p>ندانم چه آورد خواهم به سر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ز دو رویه دشمن ندانم برست</p></div>
<div class="m2"><p>نه پیداست کاختر کرا یاورست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چنانم که سرگشته‌ای روز تنگ</p></div>
<div class="m2"><p>رهش پیش غرقاب وز پس نهنگ</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>کنون چاره جویید تا چون کنیم</p></div>
<div class="m2"><p>که این خار از پای بیرون کنیم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ره آموز و روزه ده و چاره گر</p></div>
<div class="m2"><p>بوند این سه سر بی پدر را پدر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بسی رای زد هر کس از روی کار</p></div>
<div class="m2"><p>سرانجام گفتند کای شهریار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چو آتش نمایدت از دور دود</p></div>
<div class="m2"><p>از آن به که سوزدت نزدیک زود</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>شهان و بزرگان روی زمین</p></div>
<div class="m2"><p>چه فرخ پدرت و چه فغفور چین</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>همه باژ ضحاک را داده‌اند</p></div>
<div class="m2"><p>ز کامش برون گام ننهاده‌اند</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>فریدون از او به به‌ فرنگ و فر</p></div>
<div class="m2"><p>همیدون به داد و نژاد و گهر</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>گر او را تو فرمان بری ننگ نیست</p></div>
<div class="m2"><p>ترا با سپهدار او جنگ نیست</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>هر آن ریش کز مرهم آید به راه</p></div>
<div class="m2"><p>تو داغش کنی پیش گردد تباه</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>همه کاخ و ایوان به بزم و به خوان</p></div>
<div class="m2"><p>بیارای و این پهلوان را بخوان</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بر او بر شمر هدیه چندان ز گنج</p></div>
<div class="m2"><p>کس آسان شود هر چه دیدست رنج</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>پس آن گه بدو از برادر پسر</p></div>
<div class="m2"><p>بخوان نامه های گله سر به سر</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>که او خود ز دشمن کشد کین تو</p></div>
<div class="m2"><p>نهد بر سپهر برین زین تو</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>به‌ دست کسان چون توان گشت شیر</p></div>
<div class="m2"><p>نباید ترا پیش او شد دلیر</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>پسندید خاقان و پیش گوان</p></div>
<div class="m2"><p>بفرمود پاسخ سوی پهلوان</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>پس از نام و یاد جهان آفرین</p></div>
<div class="m2"><p>ز دل بر سپهبد گرفت آفرین</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>دگر گفت کز باژ و هدیه ز گنج</p></div>
<div class="m2"><p>دهم هر چه گویی ندارم به رنج</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>سزد شاه ایران اگر سر کشست</p></div>
<div class="m2"><p>که او را چو گرد لشکر کشست</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>اگر خواهد از من شه نام جوی</p></div>
<div class="m2"><p>فرستم سرم بر طبق پیش اوی</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>بدین باژ دو دیده گوهر کنم</p></div>
<div class="m2"><p>ز تن پوستم بدره زر کنم</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>ولی آرزو دارم از تو یکی</p></div>
<div class="m2"><p>که آری به کاخم درنگ اندکی</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>بوی شاد یک هفته مهمان من</p></div>
<div class="m2"><p>بیارای این میهن و مان من</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>به جای فریدون اگر دانی ام</p></div>
<div class="m2"><p>گز این آرزو شاد گردانی ام</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>فرستاده را باره خویش داد</p></div>
<div class="m2"><p>وز اندازه دیبا و زر بیش داد</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>کسی کردش و شد فرسته چو باد</p></div>
<div class="m2"><p>پیام آنچه بد گفت و نامه بداد</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>سپهدار از آن گفتها گشت رام</p></div>
<div class="m2"><p>که پیغام بد با نوید و خرام</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>سوی شاه با لشکر آغاز کرد</p></div>
<div class="m2"><p>وز آن روی خاقان بشد ساز کرد</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>هزار اسپ از فسیله گزید</p></div>
<div class="m2"><p>دوره ده هزار از بره سربرید</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>ز گاوان فربه همی چهل هزار</p></div>
<div class="m2"><p>ز نخچیر و مرغ تن فزون از شمار</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>دو ره صد هزار دگر گوسفند</p></div>
<div class="m2"><p>همه کشت و بر دشت و صحرا فکند</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>پذیره به پیش سپهدار شد</p></div>
<div class="m2"><p>چو یکجای دیدارشان باز شد </p></div></div>
<div class="b" id="bn46"><div class="m1"><p>به بر یکدگر را هم از پشت زین</p></div>
<div class="m2"><p>گرفتند این شاد از آن آن از این</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>به یکجای بودند هوش هر دوان</p></div>
<div class="m2"><p>همه راه هم پرسش و هم عنان</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>سپهدار با هر که بود از سپاه</p></div>
<div class="m2"><p>نشستند بر خوان هم از گرد راه</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>ز هر خوردنی ساز چندان گروه</p></div>
<div class="m2"><p>یکی دشت بد گردش اندر دو کوه</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>پر از گور و نخچیر کوهش همه</p></div>
<div class="m2"><p>به دشت اندر از گور و آهو رمه</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>به هر گام جامی پر از لعل می</p></div>
<div class="m2"><p>طبق‌های نقل و درم زیرپی</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>رده در رده کاسه و خوان و جام</p></div>
<div class="m2"><p>فروزان به مجمر دورن عود خام</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>به زیر از طوایف نهفته زمین</p></div>
<div class="m2"><p>ز بر کله در کله دیبای چین</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>سپاهی ز شهد و شکر ساخته</p></div>
<div class="m2"><p>همه نیزه در دست و تیغ آخته</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>گروهی به پیکار رفته فراز</p></div>
<div class="m2"><p>گروهی به نخچیر با یوز و باز</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>ز حلوا به هر صیفی میوه‌دار</p></div>
<div class="m2"><p>همه برکشان شکر و قند بار</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>طبق‌ها و جام از کران تا کران</p></div>
<div class="m2"><p>به مشک و می اندوده و زعفران</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>سپهریست هر جام گفتی مگر</p></div>
<div class="m2"><p>مهش انگبین و ستاره شکر</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>کمربسته در پیش خوبان پرست</p></div>
<div class="m2"><p>همه باده و باد بیزان به دست</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>چنان روشن از می بلورین ایاغ</p></div>
<div class="m2"><p>کز او کور دیده به‌شب بی‌چراغ</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>دم نای هر جای و چنگ و رباب</p></div>
<div class="m2"><p>پراکنده مستان بر آتش کباب</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>گرفته خورش‌ها همه کوه و دشت</p></div>
<div class="m2"><p>کشان پیشکار آب و دستار و طشت</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>به بوی خورش‌ها ددان تاخته</p></div>
<div class="m2"><p>زبر در هوا مرغ صف ساخته</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>نسشته به خوان یکسر ایرانیان</p></div>
<div class="m2"><p>همه چینیان پیش بسته میان</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>شب و روز خاقان پرستش نمای</p></div>
<div class="m2"><p>کمربسته پیش سپهبد به پای</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>جدا خوانش هر روز دادی بلاش</p></div>
<div class="m2"><p>یکی ابر بد ویژه دینار پاش</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>سر هفته آمد نوندی فراز</p></div>
<div class="m2"><p>که آورد لشکر تکین‌تاش باز</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>ز ناگه خروشی برآمد به ابر</p></div>
<div class="m2"><p>شد آن بزم بر سان کام هژبر</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>سپهبد به‌ خاقان یغر گفت چیست</p></div>
<div class="m2"><p>چه‌ لشکر رسید و تکین‌تاش کیست</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>بگسترد خاقان سخن سر به‌ سر</p></div>
<div class="m2"><p>گله هر چه بدش از برادر پسر</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>سپهدار گفت اینست غمری دلیر</p></div>
<div class="m2"><p>کز اینسان از سر خویش سیر</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>من اینجا و او رزمکوش آمدست</p></div>
<div class="m2"><p>همانا که خونش به جوش آمدست</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>یکست ابلهان را شتاب و شکیب</p></div>
<div class="m2"><p>سواران بد را چه بالا چه شیب</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>ترا دل بدین غم نباید سپرد</p></div>
<div class="m2"><p>که تنها بس او را نریمان گرد</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>گرش صدهزاراند گردان جنگ</p></div>
<div class="m2"><p>همه درگه جنگ و کین تیز چنگ</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>ببینی که چون گویم ای شیر هین</p></div>
<div class="m2"><p>که خونشان ستاند به شمشیر کین</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>چنان کن که شبگیر با یوز و باز</p></div>
<div class="m2"><p>خرامیم مر جنگ را پیشباز</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>می و بزم کاینجاست آنجا بریم</p></div>
<div class="m2"><p>نریمان زند تیغ و ما می‌خوریم</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>من از ویژه‌گردان گزینم هزار</p></div>
<div class="m2"><p>تو بگزین هم از لشکر اندک سوار</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>بدان تا چو اندک نماید سپاه</p></div>
<div class="m2"><p>دلیری کند دشمن آید به راه</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>مگر ناگهش سر به دام آورم</p></div>
<div class="m2"><p>وز این کار فرجام نام آوردم</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>چو پر حواصل برآورد زاغ</p></div>
<div class="m2"><p>برافروخت ز ایوان نیلی چراغ</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>همان نامزد کرد اندک سپاه</p></div>
<div class="m2"><p>ببردند و راندند یک هفته راه</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>به‌ بزم و به‌ نخچیر بر کوه و دشت</p></div>
<div class="m2"><p>چنین تا به دژی دیدار گشت</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>بر آن تیغ دژ از بر کوهسار</p></div>
<div class="m2"><p>تکین‌تاش با جنگیان ده‌هزار</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>بگفتند از ایران دلیری سترگ</p></div>
<div class="m2"><p>رسیدست نو با سپاهی بزرگ</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>ز خاقان یغر جنگ تو خواستست</p></div>
<div class="m2"><p>وز ایران نبرد ترا خاستست</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>ز تیغ دژ آمد به پایین کوه</p></div>
<div class="m2"><p>بزد صف کین با سپه همگروه</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>نیامدش باک از دلیری که بود</p></div>
<div class="m2"><p>چو گرد سپه دید بشتافت زود</p></div></div>