---
title: >-
    بخش ۱۱۰ - رفتن گرشاسب با نریمان به توران
---
# بخش ۱۱۰ - رفتن گرشاسب با نریمان به توران

<div class="b" id="bn1"><div class="m1"><p>به فرخ ترین فال گیتی فروز</p></div>
<div class="m2"><p>سپه راند از آمل شه نیمروز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سوی شیرخانه به شادی و کام</p></div>
<div class="m2"><p>که خوانی ورا بلخ بامی به نام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به کیلف شد از بلخ گاه بهار</p></div>
<div class="m2"><p>وزان جایگه کرد جیحون گذار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همه ماورالنهر تا مرز چین</p></div>
<div class="m2"><p>شمردندی آن گاه توران زمین</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از آموی و زم تا به چاچ و ختن</p></div>
<div class="m2"><p>ز شنگان و ختلان شهان تن‌به‌تن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز نزل و علف هر کجا یافتند</p></div>
<div class="m2"><p>ببردند و با هدیه بشتافتند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بدان گه سمرقند کرده بنود</p></div>
<div class="m2"><p>زمین‌اش به جز خاک خورده بنود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سپهبد همی راند تا شهر چاچ</p></div>
<div class="m2"><p>ز گردش بزرگان با تخت و عاج</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دهی دید خوش دل بدو رام کرد</p></div>
<div class="m2"><p>ستاره زد آن جا و آرام کرد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>برآسود یک هفته و بود شاد</p></div>
<div class="m2"><p>به دل داد نخچیر و شادی بداد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>میان ده اندر دژی بد کهن</p></div>
<div class="m2"><p>کس آغاز آن را ندانست و بن</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>برآمد یکی بومهن نیم شب</p></div>
<div class="m2"><p>تو گفتی زمین دارد از لرزه تب</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>یکی گوشه دژ نگونسار شد</p></div>
<div class="m2"><p>چهل دیگ رویین پدیدار شد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>همه دیگ‌ها سرگرفته به گل</p></div>
<div class="m2"><p>چو دیدند پر زر بد آن هر چهل</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>به هر یک درون خرمنی زر ناب</p></div>
<div class="m2"><p>درخشنده چون اخگر و آفتاب</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>سپهدار برداشت پاک آنچه بود</p></div>
<div class="m2"><p>بر آن ده بسی نیکوی‌ها فزود</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>وزآنجا سپه راند و بشتافت تفت</p></div>
<div class="m2"><p>به شادی به شهر سپنجاب رفت</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بدان مرز هرچ از بزرگان بدند</p></div>
<div class="m2"><p>دگر کارداران و دهقان بدند</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ستایش کنان پاک رفتند پیش</p></div>
<div class="m2"><p>همه ساخته هدیه ز اندازه بیش</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>سپه برد از آن مرز و شد شاد و چیر</p></div>
<div class="m2"><p>بسی کوه پیش آمدش سردسیر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>همه کان گهر بد دل سنگ و خاک</p></div>
<div class="m2"><p>ز زر و مس و آهن و سیم پاک</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>یکی خانه بر هر که از خاره سنگ</p></div>
<div class="m2"><p>بر افراز غاری رهش تار و تنگ</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ز نوشادر آن خانه‌ها پر بخار</p></div>
<div class="m2"><p>که بردندی از وی به هر شهریار</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>از آن سیم و زر لشکر و پیلوان</p></div>
<div class="m2"><p>ببردند چندان که بدشان توان</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>سپهبد کجا شد همی مژده داد</p></div>
<div class="m2"><p>ز فرخ فریدن با فر و داد</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>که بستد ز ضحاک شاهنشهی</p></div>
<div class="m2"><p>جهان شد ز بیداد و از بد تهی</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ز شادی رخ دهر شاداب کرد</p></div>
<div class="m2"><p>گذر بر سر آب شاداب کرد</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>چو از رود بگذشت بفکند رخت</p></div>
<div class="m2"><p>چهان پر گل و سبزه دید و درخت</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>میان گل و سوسن و مرغزار</p></div>
<div class="m2"><p>روان چشمه آب بیش از هزار</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>ز گل دشت طاووس رنگین شده</p></div>
<div class="m2"><p>از ابر آسمان پشت شاهین شده</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>به آواز بلبل گشاده دهن</p></div>
<div class="m2"><p>دریده گل از بانگ او پیرهن</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>لب چشمه‌ها بر شخنشار و ماغ</p></div>
<div class="m2"><p>زده صف سمانه همه دشت و راغ</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>پر از مرغ مَرغ و گل سرخ و زرد</p></div>
<div class="m2"><p>ز ناژ و ز بید و هم از روز گرد</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>سراینده سار و چکاوک ز سرو</p></div>
<div class="m2"><p>چمان بر چمن‌ها کلنگ و تذور</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>پراکنده با مشکدم سنگخوار</p></div>
<div class="m2"><p>خروشان به هم شارک و لاله سار</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>ز هر سو رم آهو و رنگ و غرم</p></div>
<div class="m2"><p>ز دل‌ها دم کل زداینده گرم</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>همان جا به نخچیر با باز و یوز</p></div>
<div class="m2"><p>ببد هفته‌ای شاد و گیتی فروز</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>بزرگان آن مرز ز اندازه بیش</p></div>
<div class="m2"><p>شدندش ز هر مرز با نزل پیش</p></div></div>