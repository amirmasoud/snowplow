---
title: >-
    بخش ۷۱ - درختی که هفت گونه بارش بود
---
# بخش ۷۱ - درختی که هفت گونه بارش بود

<div class="b" id="bn1"><div class="m1"><p>به شهری رسیدند خرّم دگر</p></div>
<div class="m2"><p>پُرآرایش و زیب و خوبی و فر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز بیرونش بتخانه ای پر نگار</p></div>
<div class="m2"><p>براو بی کران برده گوهر به کار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نهاده در ایوانش تختی ز عاج</p></div>
<div class="m2"><p>بتی در وی از زر با طوق و تاج</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>درختی گشن رسته در پیش تخت</p></div>
<div class="m2"><p>که دادی بر از هفت سان آن درخت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز انگور و انجیر و نارنج و سیب</p></div>
<div class="m2"><p>ز نار و ترنج و بِه دلفریب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نه باری بدینسان به بار آمدی</p></div>
<div class="m2"><p>که هر سال بارش دو بار آمدی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر آن برگ کز وی شدی آشکار</p></div>
<div class="m2"><p>بُدی چهره آن بت بر و بر نگار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز شهر آن که بیمار بودی و سُست</p></div>
<div class="m2"><p>چو خوردی از آن میوه گشتی درست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>برو چون مهِ نو یکی داس بود</p></div>
<div class="m2"><p>که تیزیش مانند الماس بود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کسی کاو شدی پیش آن بُت شمن</p></div>
<div class="m2"><p>فدا کردی از بهر او خویشتن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بن داس در نوک شاخی دراز</p></div>
<div class="m2"><p>ببستی و زی خود کشیدی فراز</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>فکندیش در حلق چون خم شست</p></div>
<div class="m2"><p>به یک ره رها کردی آن گه ز دست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>سرش را چو گویی برانداختی</p></div>
<div class="m2"><p>چنین خویشتن را فدا ساختی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>همان گاه بودی به یک زخم سخت</p></div>
<div class="m2"><p>تنش بر زمین و سرش بر درخت</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>سپهدار با ویژگان سپاه</p></div>
<div class="m2"><p>به دیدار آن خانه شد هم ز راه</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بدید آن درخت نوآیین به بار</p></div>
<div class="m2"><p>چو باغی پُر از گونه گون میوه دار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>سرش سایه گسترده بر کاخ بر</p></div>
<div class="m2"><p>بر از هفت گونه به هر شاخ بر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>هم از کار آن داس بر خیره ماند</p></div>
<div class="m2"><p>بر آن بت بنفرید و ز آن جا براند</p></div></div>