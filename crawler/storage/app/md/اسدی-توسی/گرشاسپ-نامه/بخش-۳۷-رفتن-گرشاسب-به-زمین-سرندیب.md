---
title: >-
    بخش ۳۷ - رفتن گرشاسب به زمین سرندیب
---
# بخش ۳۷ - رفتن گرشاسب به زمین سرندیب

<div class="b" id="bn1"><div class="m1"><p>دگر روز مهراج گردنفراز</p></div>
<div class="m2"><p>بسی کشتی آورد هر سو فراز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به ایرانیان داد کشتی چو شست</p></div>
<div class="m2"><p>دگر کشتی او با سپه بر نشست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز کشتی شد آن آب ژرف از نهاد</p></div>
<div class="m2"><p>چو دشتی در آن کوه تازان ز باد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تو گفتی که کیمخت هامون چو نیل</p></div>
<div class="m2"><p>به حمله بدرّد همی زنده پیل</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو پیلی به میدان تک زودیاب</p></div>
<div class="m2"><p>ورا پیلبان با دو میدانش آب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تکش تیز و رفتنش بی دست و پای</p></div>
<div class="m2"><p>نه خوردنش کام و نه خفتنش رای</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فزون خم خرطومش از سی کمند</p></div>
<div class="m2"><p>ز دندانش بر پشت ماهی گزند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به رفتن برآورده پر مرغ وار</p></div>
<div class="m2"><p>همی ره به سینه خزیده چو مار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گهی حلقه خرطومش اندر شکم</p></div>
<div class="m2"><p>گهی بسته با گاو و ماهی به هم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>یکی دشتش از پیش سیماب رنگ</p></div>
<div class="m2"><p>سراسر چو پولاد بزدوده زنگ</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>زمینی بمانند گردان سپهر</p></div>
<div class="m2"><p>درو چون در آیینه دیدار چهر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بیابانی آشفته بی سنگ و خاک</p></div>
<div class="m2"><p>مغاکش گهی کوه و گه کُه مغاک</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>یکی دشت سیمین بی آتش به جوش</p></div>
<div class="m2"><p>گه آسوده از نعره گه با خروش</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بدیدن چنان کابگینه درنگ</p></div>
<div class="m2"><p>ز شورش چو کوبند بر سنگ سنگ</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>دوان او در آن دشت و راه دراز</p></div>
<div class="m2"><p>گهی شیب تازنده گاهی فراز</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گهی چون یکی خانه در ژرف غار</p></div>
<div class="m2"><p>گهی چون دژی از بر کوهسار</p></div></div>