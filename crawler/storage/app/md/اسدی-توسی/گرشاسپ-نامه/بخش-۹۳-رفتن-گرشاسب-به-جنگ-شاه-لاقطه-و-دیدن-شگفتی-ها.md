---
title: >-
    بخش ۹۳ - رفتن گرشاسب به جنگ شاه لاقطه و دیدن شگفتی ها
---
# بخش ۹۳ - رفتن گرشاسب به جنگ شاه لاقطه و دیدن شگفتی ها

<div class="b" id="bn1"><div class="m1"><p>چو شد بر جزیره یکی بیشه دید</p></div>
<div class="m2"><p>همه دامن بیشه لشکر کشید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شه لاقطه بود کطری به نام</p></div>
<div class="m2"><p>دلیری جهانگیر و جوینده کام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جهان پیش چشمش به هنگام خشم</p></div>
<div class="m2"><p>کم از سایه پشه بودی به چشم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو آگه شد از کار گرشاسب زود</p></div>
<div class="m2"><p>بفرمود تا لشکرش هر چه بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به هامون سراسر جبیره شدند</p></div>
<div class="m2"><p>به پیکار جستن پذیره شدند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سه منزل به جنگ آمد از پیش باز</p></div>
<div class="m2"><p>دمان با گران لشکری رزم ساز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>همه ساخته ترگ و خفتان جنگ</p></div>
<div class="m2"><p>ز دندان ماهی و چرم پلنگ</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سر گوسفندان فلاخن به دست</p></div>
<div class="m2"><p>گرفتند کوشش چو پیلان مست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اگر ترگ و خود ار سپر یافتند</p></div>
<div class="m2"><p>به سنگ فلاخن همی کافتند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سبک رزم را پهلوان سترگ</p></div>
<div class="m2"><p>فروکوفت زرینه کوس بزرگ</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>غَو مهره و کوس بگذشت از ابر</p></div>
<div class="m2"><p>دم نای بدرید گوش هژبر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دلیران ایران به کین آختن</p></div>
<div class="m2"><p>گرفتند هر سو کمین ساختن</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ز خون رخ به غنجار بندود خور</p></div>
<div class="m2"><p>ز گرد اندر آورد چادر به سر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ز یک روی سنگ و دگر روی تیر</p></div>
<div class="m2"><p>ببارید و شد چهر گیتی چو قیر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>شد از بیم رخ ها به رنگ رزان</p></div>
<div class="m2"><p>سَرِ تیغ چون دست وشی رزان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>هوا بانگ زخم فلاخن گرفت</p></div>
<div class="m2"><p>جهان آتش و سنگ آهن گرفت</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>پر از گرد کین پرده مهر شد</p></div>
<div class="m2"><p>ز پیکان سپهر آبله چهر شد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چنان خاست رزمی که بالا و پست</p></div>
<div class="m2"><p>بُد از خون نوان همچو از باده مست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>کُه از تابش تیغ لرزان شده</p></div>
<div class="m2"><p>زریر از رخ بددل ارزان شد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ستیزندگان نیزه با خشم و شور</p></div>
<div class="m2"><p>فرو خوابنیده به یال ستور</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>لب کین کشان کافته زیر کف</p></div>
<div class="m2"><p>ز گرمای خورشید خفتان چو خَف</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>میان در سپهدار چون کوه برز</p></div>
<div class="m2"><p>پیاده دو دستی همی کوفت گرز</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>کمند از گره کرده پنجاه کرد</p></div>
<div class="m2"><p>ز ماهی همی برد و بر ماه کرد</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>به هر حمله سی گام جستی ز جای</p></div>
<div class="m2"><p>به هر زخم گردی فکندی ز پای</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>شدی بازو و خنجرش نو به نو</p></div>
<div class="m2"><p>گهی خشت کار و گهی سر درو</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>خروشش چنان دشت بشکافتی</p></div>
<div class="m2"><p>که در وی سپاهی گذر یافتی</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>تو گفتی مگر ابر غران شدست</p></div>
<div class="m2"><p>و یا کوه پولاد پران شدست</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>گهی نیزه زد گاه گرز نبرد</p></div>
<div class="m2"><p>از آن دیو ساران برآورد گرد</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>چوزو کطری آن جنگ و پیکار دید</p></div>
<div class="m2"><p>برش مرد پیکار بیکار دید</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>به دل گفت هرگز چنین دستبرد</p></div>
<div class="m2"><p>ندیدم به میدان ز مردان گرد</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>شدن پیش گرزش که یارا کند</p></div>
<div class="m2"><p>به جنگ از سپر کوه خارا کند</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>مرا بادی این کینه زو آختن</p></div>
<div class="m2"><p>که ماندست از آویزش و تاختن</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>درآمد چو تندر خروشنده سخت</p></div>
<div class="m2"><p>به دست استخوان ماهیی چون درخت</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>بزد بر سرش لیک نامد زیان</p></div>
<div class="m2"><p>سبک پهلوان همچو شیر ژیان</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>چنان زدش گرزی که بی زور شد</p></div>
<div class="m2"><p>زمینش همان جا که بُد گور شد</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>گریزان سپاهش گروها گروه</p></div>
<div class="m2"><p>نهادند سر سوی دریا و کوه</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>دلیران ایران ز پس تا به شهر</p></div>
<div class="m2"><p>برفتند و کشتند از ایشان دو بهر</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>از آن پس به تاراج دادند روی</p></div>
<div class="m2"><p>فتادند در شهر و بازار و کوی</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>ز زر و ز سیم و ز گستردنی</p></div>
<div class="m2"><p>ندیدند کس چیز جز خوردنی</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>در خانه‌ شان پاک و دیوار و بام</p></div>
<div class="m2"><p>ز ماهی استخوان بود از عود خام</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>ز مردان که بُد پاک برنا و پیر</p></div>
<div class="m2"><p>بکشتند و دیگر گرفتند اسیر</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>از ایوان کطری چو سیصد کنیز</p></div>
<div class="m2"><p>ببردند و جفت و دو دخترش نیز</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>یکی خانه سر بر مه افراشته</p></div>
<div class="m2"><p>پر از عود و عنبر بر انباشته</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>سپهبد همه سوی کشتی کشید</p></div>
<div class="m2"><p>وزان بردگان بهترین برگزید</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>ز هر چیز ده کشتی انبار کرد</p></div>
<div class="m2"><p>دو صد گرد بروی نگهدار کرد</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>سوی طنجه نزدیک عم زاده باز</p></div>
<div class="m2"><p>فرستاد و او راه را کرد ساز</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>به گرد جزیره به گشتن گرفت</p></div>
<div class="m2"><p>بدان تا چه آیدش پیش از شگفت</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>همه نیشکر بُد درو دشت و غار</p></div>
<div class="m2"><p>دگر بیشه بُد هر سوی میوه دار</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>بسی میوه ها بُد که نشناختند</p></div>
<div class="m2"><p>نیارست کس خورد و بنداختند</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>ز هر جانور کان شناسد کسی</p></div>
<div class="m2"><p>نبد چیز الا تشی بُد بسی</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>که نامش به سوی دری چون کشی</p></div>
<div class="m2"><p>یکی سنگه خواندش و دیگر تشی</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>به تن هر یکی مهتر از گاومیش</p></div>
<div class="m2"><p>چو ژوبین بر او خار یک بیشه بیش</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>گه کین تن از خم کمان ساختی</p></div>
<div class="m2"><p>وز آن خار او خشت کردند و تیر</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>سه هفته بدینگونه بُد سرفراز</p></div>
<div class="m2"><p>بدان تا رسد کشتی از طنجه باز</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>به ره باد کژ گشت و آشوب خاست</p></div>
<div class="m2"><p>همی برد ده روز کشتی چو خاست</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>پس آن کشتی و بردگان با سپاه</p></div>
<div class="m2"><p>به دریا چو رفتند یک روزه راه</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>فتادند روز دهم یکسره</p></div>
<div class="m2"><p>به خرم کُهی نام او قاقره</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>جزیری پر از لشکر بی شمار</p></div>
<div class="m2"><p>شهی مر ورا نام او کوشمار</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>چو دیدند کشتی دویدند زود</p></div>
<div class="m2"><p>به تاراج بردند پاک آنچه بود</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>دلیران ایران یکی رزم سخت</p></div>
<div class="m2"><p>بکردند و اختر نبد یار و بخت</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>گرفتار آمد صد و شصت گرد</p></div>
<div class="m2"><p>دگر غرقه گشتند و کس جان نبرد</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>یکی کشتی و چند تن ناتوان </p></div>
<div class="m2"><p>بجستند و رفتند زی پهلوان</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>بدادند آگاهی از هر چه بود</p></div>
<div class="m2"><p>سپهبد سپه رزم را ساخت زود</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>شتابان رهِ قاقره بر گرفت</p></div>
<div class="m2"><p>جزیری به ره پیشش آمد شگفت</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>کُهی در میان جزیره دو نیم</p></div>
<div class="m2"><p>یکی کان زر و دگر کان سیم</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>سه منزل فزون بیشه و مرغزار</p></div>
<div class="m2"><p>دوان هر سوی روبه بی‌شمار</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>به بر زرد یکسر به تن لعل پوش</p></div>
<div class="m2"><p>همه شکل دنبال و کافور گوش</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>به نزدیک آن کوه بر پنج میل</p></div>
<div class="m2"><p>برابر کُهی بود هم رنگِ نیل</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>به چاره بر آن کُه نرفتی کسی</p></div>
<div class="m2"><p>وزو عنبر افتادی ایدر بسی</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>خور روبهان پاک عنبر بدی</p></div>
<div class="m2"><p>دگر تازه گل‌های نوبر بدی</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>از آن روبهان هر سک اندر سپاه</p></div>
<div class="m2"><p>فکندند بسیار بی راه و راه</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>ز تن پوستهاشان برون آختند</p></div>
<div class="m2"><p>وزان جامه گونه گون آختند</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>از آن جامه هر کاو شبی داشتی</p></div>
<div class="m2"><p>دَم عنبرش مغز انباشتی</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>بسی زآن دو کُه زر ببردند و سیم</p></div>
<div class="m2"><p>وز آنجا برفتند بی ترس و بیم</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>رسیدند نزد جزیری دگر</p></div>
<div class="m2"><p>درو نز گیا چیز و نز جانو</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>زمینش همه شوره و ریگ نرم</p></div>
<div class="m2"><p>چو جوشنده آب اندر و خاک گرم</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>ز تفته بر و بوم او گاه گاه</p></div>
<div class="m2"><p>دمان آتشی بر زدی سر به ماه</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>برو هر که رفتی هم اندر شتاب</p></div>
<div class="m2"><p>شدی غرقه در ریگ و گشتی کباب</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>ز ماهی استخوان شاخها بر کنار</p></div>
<div class="m2"><p>بُد افکنده هر یک فزون از چنار</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>دگر مُهره بد هر سوی افتاده چند</p></div>
<div class="m2"><p>که هر یک مِه از گنبدی بُد بلند</p></div></div>