---
title: >-
    بخش ۸۹ - نشستن گرشاسب بر تخت کابل
---
# بخش ۸۹ - نشستن گرشاسب بر تخت کابل

<div class="b" id="bn1"><div class="m1"><p>به ایوان کابل شه آورد روی</p></div>
<div class="m2"><p>بیامد نشست از بر تخت اوی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گهر یافت چندان زهرگونه ساز</p></div>
<div class="m2"><p>که گر بشمری عمر باید دراز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چه بر پیل و اشتر چه بر گاومیش</p></div>
<div class="m2"><p>به اثرط فرستاد از اندازه بیش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یکی کاروان بُد همه سیم و زر</p></div>
<div class="m2"><p>به کابل سری زو به زابل دگر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از آن پس به تخت مهی بر نشست</p></div>
<div class="m2"><p>به شادی به نخچیر و می برد دست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کنیزان گلرخ فزون از هزار</p></div>
<div class="m2"><p>به دست آمدش هر یکی چون بهار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>میانشان یکی ماه دلخواه بود</p></div>
<div class="m2"><p>که دخت شه و بر بتان شاه بود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نگاری که گر چهرش از چرخ مهر</p></div>
<div class="m2"><p>بدیدی بدادی بر آن چهر مهر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به رخسار خوبش بر از هر نگار</p></div>
<div class="m2"><p>مشاطه شده ماه را روزگار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز ره برده رفتار سرو روان</p></div>
<div class="m2"><p>ز عنبر زده نقطه بر ارغوان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دو سوسنش پر پیکر نیکوی</p></div>
<div class="m2"><p>دو بادام پر سرمه جادوی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به خنده لبش لاله می سرشت</p></div>
<div class="m2"><p>چو بر لاله ژاله به باغ بهشت</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>هزارش گره سنبل پر شکن</p></div>
<div class="m2"><p>به هم بر زره ساز و چنبرفکن</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>سر هر شکن مشک را مایه دار</p></div>
<div class="m2"><p>خم هر گره بر گلی سایه دار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>به مهرش دل پهلوان گشت راست</p></div>
<div class="m2"><p>ز مادرش در حال وی را بخواست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چنان شیفته شد بدان دلفریب </p></div>
<div class="m2"><p>که بی او زمانی نکردی شکیب</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ز نخچیر چون باز پرداختی</p></div>
<div class="m2"><p>همه بزم با ماهرخ ساختی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>کنیزک همی تشنه خون اوی</p></div>
<div class="m2"><p>به درد پدر زو شده کینه جوی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چنان ساخت با مادر آن شوم بهر</p></div>
<div class="m2"><p>که بکشد جهان پهلوان را به زهر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>هویدا همی بود خاموش و نرم</p></div>
<div class="m2"><p>همی کرد باز از نهان داغ گرم</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>به گاهی که آمد ز نخچیر باز</p></div>
<div class="m2"><p>جهان پهلوان دیده رنج دراز</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>به هم دختر و مادر زشت رای</p></div>
<div class="m2"><p>ستادند پیشش پرستش نمای</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>گرفته پری چهره جام بلور </p></div>
<div class="m2"><p>پُر از لعل می چون درفشنده هور</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چو نخچیر کردی کنون سور کن</p></div>
<div class="m2"><p>به می ماندگی از تنت دور کن</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>جهان پهلوان کرد زی می نگاه</p></div>
<div class="m2"><p>همه جام‌ می دید گشته سیاه</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>به یاد آمدش گفته برهمن</p></div>
<div class="m2"><p>گرفتش به خور گفت بر یاد من</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>دو گلنار دختر چو دینار شد</p></div>
<div class="m2"><p>دو جزعش ز لؤلؤ صدف وار شد</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>به ناکام ازو بستد و هم به جای</p></div>
<div class="m2"><p>بخورد و بیفتاد بی جان ز پای</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>دل مادر از درد شد ناتوان</p></div>
<div class="m2"><p>بجوشید با خشم دل پهلوان</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>به خنجر تن هر دو را پاره کرد</p></div>
<div class="m2"><p>سرانشان ز تن کند و بر باره کرد</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>هر آن کاو نترسد ز دستان زن</p></div>
<div class="m2"><p>ازو در جهان رای دانش مزن</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>زن نیک در خانه نازست و گنج</p></div>
<div class="m2"><p>زن بد چو دیوست و مار شکنج</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>ز دستان زن هر که ناترس کار</p></div>
<div class="m2"><p>روان با خرد نیستش سازگار</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>زنان چون درختند سبز آشکار</p></div>
<div class="m2"><p>ولیک از نهان زهر دارند بار</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>هنرشان همینست کاندر گهر</p></div>
<div class="m2"><p>به گاه زهه مردم آرند بر</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>چو پرداخت از آن هر دو زن پهلوان</p></div>
<div class="m2"><p>یکی را گزید از میان گوان</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>مرو را به کابل به شاهی نشاند</p></div>
<div class="m2"><p>به زوال شد و یک مه آنجا بماند</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>اسیران که بگرفت در کارزار</p></div>
<div class="m2"><p>فرستاد زی سیستان سی هزار</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>که سوگند بودش به یزدان پاک</p></div>
<div class="m2"><p>که آنجا به خونشان کند گِل ز خاک</p></div></div>