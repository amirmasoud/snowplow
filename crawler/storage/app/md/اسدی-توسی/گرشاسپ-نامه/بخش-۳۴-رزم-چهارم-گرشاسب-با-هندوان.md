---
title: >-
    بخش ۳۴ - رزم چهارم گرشاسب با هندوان
---
# بخش ۳۴ - رزم چهارم گرشاسب با هندوان

<div class="b" id="bn1"><div class="m1"><p>چو ز ایوان مینای پیروزه هور</p></div>
<div class="m2"><p>بکند آن همه مهره های بلور</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز دریای آب آتش سند روس</p></div>
<div class="m2"><p>در افتاد در خانه آبنوس</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز هندو جهان پیل و لشکر گرفت</p></div>
<div class="m2"><p>غو کوس کوه و زمین بر گرفت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هزاران هزار از سپه بد سوار</p></div>
<div class="m2"><p>ز پیلان جنگی ده و شش هزار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به برگستوان پیل پوشیده تن</p></div>
<div class="m2"><p>پر از ناوک انداز و آتش فکن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز بس قیر چهران زده صف چو مور</p></div>
<div class="m2"><p>ببد روز تا رو سیه گشت هور</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>همان شب که شد گفتی از روزگار</p></div>
<div class="m2"><p>ازو هندوی کرده بُد کردگار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز کوس و ز زنگ و درای و خروش</p></div>
<div class="m2"><p>ز شیپور و ز ناله نای و جوش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تو گفتی زمانه سرآید همی</p></div>
<div class="m2"><p>به هم کوه و دشت اندر آید همی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز هندو سپه بود ده میل بیش</p></div>
<div class="m2"><p>ز پس صفّ پیلان سواران ز پیش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به دیبا بیاراسته پیل چار</p></div>
<div class="m2"><p>ز زرّ طوقشان وز گهر گوشوار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ابر کوهه پیل در قلبگاه</p></div>
<div class="m2"><p>بلورین یکی تخت چون چرخ ماه</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بهو از بر تخت بنشسته پست</p></div>
<div class="m2"><p>به سر بر یکی تاج و گرزی به دست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>درفشی سر از شیر زرّینه ساز</p></div>
<div class="m2"><p>پرندش ز سیمرغ پر کرده باز</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ز بر چتری از دمّ طاووس نر</p></div>
<div class="m2"><p>فروهشته زو رشته های گهر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>وز اینروی مهراج بر تیغ کوه</p></div>
<div class="m2"><p>به دیدار ایرانیان با گروه</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>زده پیل پیکر درفش از برش</p></div>
<div class="m2"><p>ز یاقوت تخت از گهر افسرش</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>فرازش یکی نیلگون سایبان</p></div>
<div class="m2"><p>ز گوهر چو شب ز اختران آسمان</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بدینسان نظاره دو شاه از دو روی</p></div>
<div class="m2"><p>میان در دو لشکر بهم کینه جوی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>سپهبد سبک رزم آغاز کرد</p></div>
<div class="m2"><p>بزد کوس کین جنگ را ساز کرد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>از آن ده دلاور یل نامدار</p></div>
<div class="m2"><p>که سالار بُد هر یکی بر هزار</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>به هر سو یکی به سپه برگماشت</p></div>
<div class="m2"><p>بر قلب زاول گره باز داشت</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بگردنکشان گفت یکسر به تیر</p></div>
<div class="m2"><p>کنید آسمان تیره بر ماه و تیر</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>همه جنگ با پیل داران کنید</p></div>
<div class="m2"><p>بریشان چنان تیر باران کنید</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>که در چشم هر پیلبانی به جنگ</p></div>
<div class="m2"><p>فزون از مژه تیر باشد خدنگ</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بگیرید ره بر بهو هم گروه</p></div>
<div class="m2"><p>مدارید از آن تخت و پیلان شکوه</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>که من هم کنون تخت و آن تاج را</p></div>
<div class="m2"><p>دهم با بهو هدیه مهراج را</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>ز شست خدنگ افکنان خاست جوش</p></div>
<div class="m2"><p>کمان کوش ها گشت همراز گوش</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>هوا پر ز زنبور شد تیز پر</p></div>
<div class="m2"><p>خدنگین تن و آهنین نیشتر</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>کشیدند شمشیر شیران هند</p></div>
<div class="m2"><p>گرفتند کوشش دلیران سند</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>زمین همچو دریا شد از گرز و تیغ</p></div>
<div class="m2"><p>وزو گرد برخاست مانند میغ</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>همه دشت از خشت شد کشت زار</p></div>
<div class="m2"><p>همه کشته پر هندوان کُشته زار</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>بگردید گردون کوشش ز گرد</p></div>
<div class="m2"><p>برون تافت از میغ ماه نبرد</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>ز خون هفت دریا بر آمد به هم</p></div>
<div class="m2"><p>زمین از دگر سو برون داد نم</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>به هر گام بی تن سری ترگ دار</p></div>
<div class="m2"><p>بُد افکنده چون مجمری پر بخار</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>شده گرد چون چرخی و خشت و شل</p></div>
<div class="m2"><p>ستاره شده برج او مغز و دل</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>جهان ز آتش تیغ ها تافته</p></div>
<div class="m2"><p>دل کُه ز بانگ یلان کافته</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>ز چرخ اختران برگرفته غریو</p></div>
<div class="m2"><p>ز کوه و بیابان رمان غول و دیو</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>به دریا درون خسته درندگان</p></div>
<div class="m2"><p>ز پرواز بر مانده پرندگان</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>بخار و دم خون ز گرز و ز تیغ</p></div>
<div class="m2"><p>چو قوس قزح بُد که تابد ز میغ</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>به هر جای جویی بد از خون روان</p></div>
<div class="m2"><p>بکشتند چندان از آن هندوان</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>که صندوق پیلان شد از زیر پی</p></div>
<div class="m2"><p>ز بس کشته هندو چو چرخشت می</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>همان ده سر گرد از ایران سپاه</p></div>
<div class="m2"><p>گرفتند هر سو یکی رزمگاه</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>سم اسپ سنبان زمین کرد پست</p></div>
<div class="m2"><p>گروها گره را گراهون شکست</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>سرافشان همی کرد در صف هژبر</p></div>
<div class="m2"><p>کمینگاه بگرفت بهپور ببر</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>همی ریخت آذر شن و برز هم</p></div>
<div class="m2"><p>به خنجر یلان را سر و برز هم</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>به هر سو کجا گرد گرداب شد</p></div>
<div class="m2"><p>ز خون گرد آن دشت گرداب شد</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>کجا گرز نشواد و ارفش گرفت</p></div>
<div class="m2"><p>جهان زخم پولاد و آتش گرفت</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>سپهدار در قلب از آن سو به جنگ</p></div>
<div class="m2"><p>گهش نیزه و گاه خنجر به چنگ</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>یلان هر سو از بیمش اندر گریز</p></div>
<div class="m2"><p>گرفته ز تیغش جهان رستخیز</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>کمندش بگسترده از خم خام</p></div>
<div class="m2"><p>همه دشتِ رزم اژدها وار دام</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>پی پیل پی خسته در دام او</p></div>
<div class="m2"><p>سواران خبه در خم خام او</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>از آوای گرزش همی ریخت کوه</p></div>
<div class="m2"><p>شده چرخ گردان ز گردش ستوه</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>سنانش همی مرگ را جنگ داد</p></div>
<div class="m2"><p>خدنگش همی ریگ را رنگ داد</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>کجا خنجرش رزمسازی گرفت</p></div>
<div class="m2"><p>همی در کفش مهره بازی گرفت</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>مرآن مهره کاندر هوا باختی</p></div>
<div class="m2"><p>سَر سروران بود کانداختی</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>به یک ره فزون از هزاران سوار</p></div>
<div class="m2"><p>سنان کرده بُد در کمرش استوار</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>نه بر زین بجنبید گرد دلیر</p></div>
<div class="m2"><p>نه از زخم شد مانده نز جنگ سیر</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>تو گفتی تنش کوه آهن کشست</p></div>
<div class="m2"><p>همان اسپش از باد و از آتشست</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>ز بس کشته کافکنده از پیش و پس</p></div>
<div class="m2"><p>خروش سروش آمد از برکه بس</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>همان شاه مهراج بر جنگجوی</p></div>
<div class="m2"><p>نهاده ز کُه دیده بر ترگ اوی </p></div></div>
<div class="b" id="bn62"><div class="m1"><p>اگر گردی از زین ربودی ز جای</p></div>
<div class="m2"><p>وگر زنده پیلی فکندی ز پای</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>ز کُه با سپه نعره برداشتی</p></div>
<div class="m2"><p>غو کوس از چرخ بگذاشتی</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>مه پیلبانان شد آگه که بخت</p></div>
<div class="m2"><p>ربود از بهو تخت و شد کار سخت</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>نه با چرخ شاید به نزد آزمود</p></div>
<div class="m2"><p>نه چون بخت بد شد بود چاره سود</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>بیامد بَر ِ پهلوان سوار</p></div>
<div class="m2"><p>به زنهار با پیل بیش از هزار</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>سپهبد نوازیدش و داد چیز</p></div>
<div class="m2"><p>همیدون بزرگان و مهراج نیز</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>سیه دیده گیتی بهو پیش چشم</p></div>
<div class="m2"><p>بر آشفت با پیلبانان به خشم</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>به بیغاره گفتا ندارید باک</p></div>
<div class="m2"><p>سپارید پیلان به مهراج پاک</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>سران این سخن راست پنداشتند</p></div>
<div class="m2"><p>ز هر سو همین بانگ برداشتند</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>همه پیلبانان از آن گفت و گوی</p></div>
<div class="m2"><p>به زنهار مهراج دادند روی</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>براندند از آن روی پیلان رمه</p></div>
<div class="m2"><p>به نرد بهو صد نماند از همه</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>پشیمان شد از گفته خود بهو</p></div>
<div class="m2"><p>ندید اندر آن چاره از هیچ سو</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>به زیر آمد از پیل و بالای خواست</p></div>
<div class="m2"><p>به ناکام رزمی گران کرد راست</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>یلان را به پیکار و کین برگماشت</p></div>
<div class="m2"><p>به صد چاره آن رزم تا شب بداشت</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>چو برزد سر از کُه درفش بنفش</p></div>
<div class="m2"><p>مه نو شدش ماه روی درفش</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>غَوِ طبل برگشتن از رزمگاه</p></div>
<div class="m2"><p>برآمد شب از جنگ بربست راه</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>بهو ماند بیچاره و خیره سر</p></div>
<div class="m2"><p>دلش تیره ، گیتی ز دل تیره تر</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>همی گفت ترسم که از بهر سود</p></div>
<div class="m2"><p>سپاهم به دشمن سپارند زود</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>نه راهست نه روی بگریختن</p></div>
<div class="m2"><p>نه سودی ز پیکار و آویختن</p></div></div>