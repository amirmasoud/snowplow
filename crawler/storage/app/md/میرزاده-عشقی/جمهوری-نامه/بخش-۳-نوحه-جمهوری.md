---
title: >-
    بخش ۳ - نوحه جمهوری
---
# بخش ۳ - نوحه جمهوری

<div class="n" id="bn1"><p>در صفحه پنجم آخرین شماره قرن بیستم عکس تابوتی را نشان می دهد که عده ای از آن مشایعت می نمایند. مرغان لاشخور سر تابوت در پروازند و پائین عکس چنین نوشته شده «جنازه مرحوم جمهوری قلابی » سپس این اشعار به چاپ رسیده است:</p></div>
<div class="b2" id="bn2"><p>آه که جمهوری ما شد فنا</p>
<p>پیرهن لاشخوران شد قبا</p></div>
<div class="b" id="bn3"><div class="m1"><p>شد فکلم چرک و کتم شد کثیف</p></div>
<div class="m2"><p>مشت جماعت کلهم کرده قیف</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>«ژندره » شد این کراوات ظریف</p></div>
<div class="m2"><p>(نم دو دیو) زین حرکات عفیف</p></div></div>
<div class="b2" id="bn5"><p>گشته طرف، ملت جاهل به ما</p>
<p>آه که جمهوری ما شد فنا</p></div>
<div class="b" id="bn6"><div class="m1"><p>بیرق جمهوری اگر شد نگون</p></div>
<div class="m2"><p>جان وی از پیری او شد برون</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>غصه نخور می زنم «انژکسیون »</p></div>
<div class="m2"><p>زنده شود لیک به حال جنون</p></div></div>
<div class="b2" id="bn8"><p>بام زند بر سر خلق خدا</p>
<p>آه که جمهوری ما شد فنا</p></div>
<div class="b" id="bn9"><div class="m1"><p>من که یکی فعله ام، ای کردگار</p></div>
<div class="m2"><p>صحبت جمهور، مرا کرده خوار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شد شب عیدی، جگرم داغدار</p></div>
<div class="m2"><p>طفلک من، مانده به زیر آوار</p></div></div>
<div class="b2" id="bn11"><p>در جلو حمله قزاق ها</p>
<p>شکر که جمهوریتان شد فنا</p></div>
<div class="b" id="bn12"><div class="m1"><p>بیرق قرمز جگرم کرد خون</p></div>
<div class="m2"><p>رفت جگرگوشه ز دستم برون</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>دولت ما گشته دچار جنون</p></div>
<div class="m2"><p>شکر که آخر علمش شد نگون</p></div></div>
<div class="b2" id="bn14"><p>بس که نمودند خلایق دعا</p>
<p>شکر که جمهوریتان شد فنا</p></div>
<div class="b" id="bn15"><div class="m1"><p>من که یکی لشخور آزاده ام</p></div>
<div class="m2"><p>بهر فروش وطن، آماده ام</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>لنگ بود امشبه، عراده ام</p></div>
<div class="m2"><p>در پی این تازه لش افتاده ام</p></div></div>
<div class="b2" id="bn17"><p>تا بکنم لقمه‌ای از آن جدا</p>
<p>آه که جمهوری ما شد فنا</p></div>
<div class="b" id="bn18"><div class="m1"><p>«جغدکی » آنجا سر تابوت بود</p></div>
<div class="m2"><p>از سخن لشخوره، مبهوت بود</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>نوحه کنان در طلب قوت بود</p></div>
<div class="m2"><p>عاشق سرداری ماهوت بود</p></div></div>
<div class="b2" id="bn20"><p>بال به هم برزد و گفت ای خدا</p>
<p>آه که جمهوری ما شد فنا</p></div>
<div class="b" id="bn21"><div class="m1"><p>لاشخوران جانب لش پر زدند</p></div>
<div class="m2"><p>از غم این فاجعه بر سر زدند</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بر سر و بر سینه مکرر زدند</p></div>
<div class="m2"><p>چنگ به تابوت پر از زر زدند</p></div></div>
<div class="b2" id="bn23"><p>سهم ربودند از آن سکه ها</p>
<p>آه که جمهوری ما شد فنا</p></div>
<div class="b" id="bn24"><div class="m1"><p>یک سگ بیچاره، عقب مانده بود</p></div>
<div class="m2"><p>دیر ترک نوحه خود خوانده بود</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>زوزه کنان در پی لش رانده بود</p></div>
<div class="m2"><p>بوی لشش معده گدازنده بود</p></div></div>
<div class="b2" id="bn26"><p>هو زدی اندر پی شاه و گدا</p>
<p>آه که جمهوری ما شد فنا</p></div>
<div class="b" id="bn27"><div class="m1"><p>دید یکی زارعک لخت عور</p></div>
<div class="m2"><p>لش کشی لاشخوران را ز دور</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>گفت که ای مست شراب غرور</p></div>
<div class="m2"><p>حسرت جمهور ببر سوی گور</p></div></div>
<div class="b2" id="bn29"><p>آه یتیمان فقیر از قفا</p>
<p>شکر که جمهوریتان شد فنا</p></div>