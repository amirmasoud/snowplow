---
title: >-
    شمارهٔ ۳ - تأثیر سخن
---
# شمارهٔ ۳ - تأثیر سخن

<div class="b" id="bn1"><div class="m1"><p>شنیدم نویسنده ای در قدیم</p></div>
<div class="m2"><p>نویسنده پاک رأی و حکیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گزیده یکی چامه، انشا نمود</p></div>
<div class="m2"><p>شه عصر از آن نامه رسوا نمود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گرفت آن هجانامه، آنسان رواج</p></div>
<div class="m2"><p>که شه را درون شد، به سر بیم تاج</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بفرمود کآن نامه ها سوختند</p></div>
<div class="m2"><p>لب آن نویسنده را دوختند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر شه بسی نامه، آتش زدند</p></div>
<div class="m2"><p>بدش آتش قهر، آبش زدند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>قضا را در آن دم، یکی تند باد</p></div>
<div class="m2"><p>به دامان شه، مشتی آتش نهاد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شه از آن بلا، راه رفتن گرفت</p></div>
<div class="m2"><p>ولیکن یکی میخش، دامن گرفت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مر آن شاه را میخ، بر تخت دوخت</p></div>
<div class="m2"><p>نگهداشت تا آن که، بر تخت سوخت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سراپرده و تخت شه هر چه بود</p></div>
<div class="m2"><p>گرفت آتش و زور آتش فزود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به ده ثانیه یکسر، آن بارگاه</p></div>
<div class="m2"><p>بیفتاد در چنگ آتش چو شاه</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به فکر شه آنگه نبد هیچ کس</p></div>
<div class="m2"><p>تمامی به فکر خود از پیش و پس</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>کس از خویش، بر شه نپرداختی</p></div>
<div class="m2"><p>در آن دم کسی شاه نشناختی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بر مردم آن روز سخت و سیاه</p></div>
<div class="m2"><p>همه گونه بد فکر، جز فکر شاه</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>زبانه کشان آتش، از قول شاه</p></div>
<div class="m2"><p>چنین ز آن نویسنده بد عذرخواه:</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>که گر نامه های تو افروختم</p></div>
<div class="m2"><p>به جبرانش این بس، که خود سوختم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>اگر دوختم من، لبانت به سیخ</p></div>
<div class="m2"><p>کنون دوختم، جان خود را به میخ!</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>نویسنده بر هر که، آهش گرفت</p></div>
<div class="m2"><p>شه ار بود بر تخت، آتش گرفت</p></div></div>