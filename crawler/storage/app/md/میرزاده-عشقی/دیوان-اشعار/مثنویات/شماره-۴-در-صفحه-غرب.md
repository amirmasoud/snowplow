---
title: >-
    شمارهٔ ۴ - در صفحه غرب
---
# شمارهٔ ۴ - در صفحه غرب

<div class="b" id="bn1"><div class="m1"><p>بهارا به پائیز ما، دیده دوز</p></div>
<div class="m2"><p>ز کلک تموزین دی، وی بسوز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ملک را ز ما نیز این نکته گوی:</p></div>
<div class="m2"><p>که پیش آمده ملک نبود نکوی!</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شها! صفحه غرب؛ اقلیم تو</p></div>
<div class="m2"><p>بیندیش زانگه که افتد گرو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به پاداش این جمله باد گران</p></div>
<div class="m2"><p>که آورده اینسان ز ری بس خزان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رخ ما شده زرد: زین باد زرد</p></div>
<div class="m2"><p>ازین باد شد، خاک، ما را به سر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خود این ملک غربت، به زر می برند</p></div>
<div class="m2"><p>ز کشور فروشان دون می خرند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هوا تیره گردید در این محال</p></div>
<div class="m2"><p>ز باد جنوب و ز باد شمال</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بلند است ابر ره، از هر کنار</p></div>
<div class="m2"><p>هم اندر یمین و هم اندر یسار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سوئی رعد پلتیک، در غرش است</p></div>
<div class="m2"><p>هم از سوی دیگر ز زر بارش است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>اهالی همه خواب و غفلت زده</p></div>
<div class="m2"><p>خمار زرین باده در میکده</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز باران بیگانه، آغشته اند</p></div>
<div class="m2"><p>همه پیرو اجنبی گشته اند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>یکی بنده بند: روسان شده</p></div>
<div class="m2"><p>دگر پای بند: پروسان شده!</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نهان گشته خورشید خاور نشان</p></div>
<div class="m2"><p>در این گیر و دار، از زمین و زمان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نشانهای خود، جمله برداشتند</p></div>
<div class="m2"><p>سپس آن بیگانه بگذاشتند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>همی دانم ای شاه شمس شموس</p></div>
<div class="m2"><p>بباید زمانی که روس و پروس:</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>به رسم نبرد، فتنه برپا کنند</p></div>
<div class="m2"><p>مر این سو زمین را اروپا کنند</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>سپس تیر و توپ و خدنگ و تفنگ</p></div>
<div class="m2"><p>بپاشند هر سو، بر آئین جنگ</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بسی قتل و غارت نمایند بر</p></div>
<div class="m2"><p>مر آن مردمی را که دارند زر</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>سپس خویشتن هیچ نی باختند</p></div>
<div class="m2"><p>شهیدان شهوت، بسی ساختند</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>در این ره کجا، کشته بنهاده اند؟</p></div>
<div class="m2"><p>ز ایرانیان بود، ار داده اند!</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>کجا رزمگه خاک، از آن شده؟</p></div>
<div class="m2"><p>خراب ار شده، ملک ایران شده!</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>اگر اقتدار است، آنان برند</p></div>
<div class="m2"><p>وگر افتخار است، ایشان برند</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>فقط پس، هوس، دونی و گمرهی</p></div>
<div class="m2"><p>بماند بر ایرانیان تهی!</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بود تیره ما را، افق آنچنان</p></div>
<div class="m2"><p>که مر عاجز است، از بیانش زبان!</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>شهنشه خود این، گر تماشا کند</p></div>
<div class="m2"><p>شهنشاهی خویش، حاشا کند</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>(ملک احمدی) نامدار جهان</p></div>
<div class="m2"><p>ز تو ننگ باشد، شهی این چنان!</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ترا گیتی ای شاه، خوش آفرید</p></div>
<div class="m2"><p>ولی این شهی، زشت بهرت گزید</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>نشایسته تو شاه ایران شدی</p></div>
<div class="m2"><p>نگهبان این ملک ویران شدی!</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>ایا خسرو کشور پاک جم</p></div>
<div class="m2"><p>ترا کشوری، بایدا چون ارم</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>نه این خاور دوزخی مردمان</p></div>
<div class="m2"><p>که تاریخشان باید این داستان</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>خلاصه چنین گشته بد، بخت ما</p></div>
<div class="m2"><p>کنون چاره کار، هان شود بخت ما</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>گر از من بپرسد، کسی بی درنگ</p></div>
<div class="m2"><p>نگر گویم ار گویمش: چاره جنگ</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>کنون چاره ما، به جز جنگ نیست</p></div>
<div class="m2"><p>چه روی سیاهی، دگر رنگ نیست</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>همه ما که بایست کشته شویم</p></div>
<div class="m2"><p>به دست دو دسته، دو دسته شویم</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>مرا این ملک را، ای شها رزمگه!</p></div>
<div class="m2"><p>نبایستی آخر، نمودن نگه؟</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>چه بهتر که از بهر ایران زمین</p></div>
<div class="m2"><p>بپا گردد این داستان این چنین</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>همی گر بجنگم، به خود این زمان</p></div>
<div class="m2"><p>ببایستی اول، شهریار از میان</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>یکی بیرق شیر و خورشید نر</p></div>
<div class="m2"><p>بباید شدن هادی ما نه زر</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>ولیکن کنون، جمله زر دیده اند</p></div>
<div class="m2"><p>پسندیده وی، پسندیده اند</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>ایا غربیان مبارک نژاد!</p></div>
<div class="m2"><p>شما را چرا شوم گشته نهاد؟</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>گر ایران زمین است این مرز و بوم</p></div>
<div class="m2"><p>ز چه دست روس و پروسند عموم</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>ایا دیو دینان دون دغل!</p></div>
<div class="m2"><p>شما را چه در سر بود زین عمل؟</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>هم از سایه شوکت شهریار</p></div>
<div class="m2"><p>بر آرمتان ای نابکاران دمار!</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>مرا نیز باشد بیانی چو سام</p></div>
<div class="m2"><p>به عنوان وی، بس سپاه کلام</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>به میدان کاغذ چه کلکم نهم</p></div>
<div class="m2"><p>(مانور) سپاه خود آنگه دهم</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>شما ای سران سپه ساز خصم!</p></div>
<div class="m2"><p>مر این نیست بستوده آئین و رسم</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>ایا بنده گیران خود زر خرید</p></div>
<div class="m2"><p>قلم برکشیدم، علم درکشید</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>کجا می گذارند که بلوا کنید</p></div>
<div class="m2"><p>سپس غارت ملک دارا کنید</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>من آن رزمخواه جبلی منم</p></div>
<div class="m2"><p>همان عشقی جنگ ملی منم</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>بود مار بر سنگ و سنگم به چنگ</p></div>
<div class="m2"><p>بسی ننگ باشد کنونم درنگ</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>ولی ار خود آنم، کنم بندگی</p></div>
<div class="m2"><p>به بیگانگان، ننگم است زندگی!</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>الا ای شه! اقلیمت ایران زمین:</p></div>
<div class="m2"><p>بپرورده دشمن بسی در کمین</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>وطن گشته بی کس از این ناکسان</p></div>
<div class="m2"><p>که باشد عیان هر کجا چون خسان!</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>چو نیکو مرا خامه ام این نوشت:</p></div>
<div class="m2"><p>که بیگانه به، از خود بدسرشت!</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>شها اندرین نامه چاکر نیم</p></div>
<div class="m2"><p>ترا بنده پاک است منکر نیم</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>همه مر بر این ریشه اند و بطون</p></div>
<div class="m2"><p>ولی اهرمن پیشه باشند چون</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>همه: شیوه شهرتی، چیده اند!</p></div>
<div class="m2"><p>مرآئین «عشقی » نه بگزیده اند</p></div></div>