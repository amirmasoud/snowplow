---
title: >-
    شمارهٔ ۱۱ - در هجو ضیاء الواعظین
---
# شمارهٔ ۱۱ - در هجو ضیاء الواعظین

<div class="b" id="bn1"><div class="m1"><p>چراغ الذاکرین، آن مرد جیغو</p></div>
<div class="m2"><p>کند در مجلس شوری هیاهو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چراغ الذاکرین، مانند زاغ است</p></div>
<div class="m2"><p>گمان دارد که مجلس مثل باغ است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نماید جیر و ویر نابه‌هنگام</p></div>
<div class="m2"><p>دهد بر حامیان ملک دشنام!</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همه دانند خوب، این هوچی لوس</p></div>
<div class="m2"><p>ز شه زر خواست اما گشت مأیوس</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همه دانند خوب، این روضه‌خوان است</p></div>
<div class="m2"><p>که مزد روضه او یک قران است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اگر اولادهای شمر ملعون</p></div>
<div class="m2"><p>دهند از مزد روضه پول افزون:</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>برای شمر، او خواند ثناها</p></div>
<div class="m2"><p>برای آن لعین، گوید دعاها:</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>غرض اینست، بر این آدم لوس</p></div>
<div class="m2"><p>دهند ار پول خواند روضه معکوس</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دو سال قبل، این هوچی آرام</p></div>
<div class="m2"><p>به (سردار سپه) می‌داد دشنام</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گرفتند و دو سه سیلی زدندش</p></div>
<div class="m2"><p>که تا ایمن بمانند از گزندش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>فرستادند او را شهر سمنان</p></div>
<div class="m2"><p>به (سردار سپه) با آه و افغان:</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نوشت او کاغذ با آب و تابی</p></div>
<div class="m2"><p>تملق گفت و دادندش جوابی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بیاوردند او را سوی تهران</p></div>
<div class="m2"><p>بدادندش خسارت بیست تومان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چو رفت این بیست تومان توی جیبش</p></div>
<div class="m2"><p>بشد آن مبلغ عالی نصیبش:</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>به (سردار سپه) مداح گردید</p></div>
<div class="m2"><p>همه چیز ورا دیگر پسندید</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>نوشت این مرد: آزادی‌پرست است</p></div>
<div class="m2"><p>شکست او، به آزادی شکست است</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بدو گفتند: یاران صمیمی</p></div>
<div class="m2"><p>چه هست این حرف و آن حرف قدیمی؟</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چرا حرف تو هر روزی به رنگی‌ست</p></div>
<div class="m2"><p>مگر مغز تو چون توت‌فرنگی‌ست!</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ز یاران این ملامت‌ها چو بشنفت</p></div>
<div class="m2"><p>جواب دوستان را این چنین گفت:</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>«به (سردار سپه) من زشت گفتم</p></div>
<div class="m2"><p>به رشوت بیست تومانی گرفتم »</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>«به او بد گفتم و داد او به من بیست</p></div>
<div class="m2"><p>اگر خوبش بگویم حق من چیست؟»</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>«بداعی بیست تومان داده (سردار)</p></div>
<div class="m2"><p>نیم زین گنج، دیگر دست بردار!»</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>«دگر دنباله مسلک نگیرم</p></div>
<div class="m2"><p>که تا زو بیست تومان‌ها بگیرم»</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>«چرا زیرا که بنده گشنه هستم</p></div>
<div class="m2"><p>که حمالی نمی‌آید ز دستم»</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>«تملق گویم و گیرم اعانه</p></div>
<div class="m2"><p>خرم سر چشمه من یک باب خانه»</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>«دعا گویم، به نام نیزه‌بازی</p></div>
<div class="m2"><p>خورم با چای، نان پادرازی»</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>«به پولداران کنم خدمتگزاری</p></div>
<div class="m2"><p>نمایم لقمه‌ای نان کوفت کاری»</p></div></div>