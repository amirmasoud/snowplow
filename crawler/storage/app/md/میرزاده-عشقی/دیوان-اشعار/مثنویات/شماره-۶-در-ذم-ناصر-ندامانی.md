---
title: >-
    شمارهٔ ۶ - در ذم ناصر ندامانی
---
# شمارهٔ ۶ - در ذم ناصر ندامانی

<div class="b" id="bn1"><div class="m1"><p>آنکو نموده است: استیضاح از جهان</p></div>
<div class="m2"><p>اکنون ز وی کنند، ستیضاح ابلهان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گویند از چه ناصرالاسلام این همه؟</p></div>
<div class="m2"><p>. . . حبیب می کند انگشتی در دهان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پاسخ دهد ز حالت امروز مملکت</p></div>
<div class="m2"><p>حیرت زده همی نهم انگشت بر زبان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گویند از چه روست که همواره خفته او</p></div>
<div class="m2"><p>پشت حبیب ز اول شب تا سحرگهان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گویند مگر تو این نشنیدی که در وطن</p></div>
<div class="m2"><p>جنگ است، گشته ام عقب سنگری نهان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گویند رسم جنگ ندانی، چه می کنی؟</p></div>
<div class="m2"><p>گوید که داده اند نشانم نظامیان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بنگر چگونه ز آتش ظالم درازکش</p></div>
<div class="m2"><p>بنموده بس که، مهر زنم تیر بر نشان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گویند عقب نشست کماندان ز سنگرش</p></div>
<div class="m2"><p>تو زان خود چگونه، نجنبیده تا به هان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گوید که من به سنگر خود، پشت چون کنم؟</p></div>
<div class="m2"><p>ترسم که از عقب بخورم تیر ناگهان!</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گویند هیچ تا بکنون تیر خورده ای؟</p></div>
<div class="m2"><p>گوید چرا چرا دو هزاران تا به هان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گه تیر می زنیم و گهی تیر می خوریم</p></div>
<div class="m2"><p>گه پشتمان به زین و گهی زین به پشتمان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>باشم وکیل و (ناصرالاسلام) ملتم</p></div>
<div class="m2"><p>کافر شود هر آن که، برد بد به من گمان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>(عشقی) اگر تو مرد مسلمان و مؤمنی</p></div>
<div class="m2"><p>بر صحت حمل کن، همه اعمال مؤمنان</p></div></div>