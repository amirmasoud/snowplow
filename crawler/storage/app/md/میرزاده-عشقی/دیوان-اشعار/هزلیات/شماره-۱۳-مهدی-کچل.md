---
title: >-
    شمارهٔ ۱۳ - مهدی کچل
---
# شمارهٔ ۱۳ - مهدی کچل

<div class="b" id="bn1"><div class="m1"><p>کار و بارت جور، مهیا شده</p></div>
<div class="m2"><p>نور علی نور، مهیا شده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دخترکی خوب، مهیا شده</p></div>
<div class="m2"><p>خفته و خود عور، مهیا شده</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تاری و تنبور، مهیا شده</p></div>
<div class="m2"><p>هم آب انگور، مهیا شده</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بس آجیل شور، مهیا شده</p></div>
<div class="m2"><p>تریاک و وافور، مهیا شده</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مهدی کچل سور مهیا شده</p></div>
<div class="m2"><p>چرچر ما جور و مهیا شده</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>لوطی حسین صاحب عنوان شود</p></div>
<div class="m2"><p>بند قبا قرمزی و خان شود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دور دگر، دوره دونان شود</p></div>
<div class="m2"><p>یکسره این ملک پریشان شود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خاطر ما، جمله پریشان شود</p></div>
<div class="m2"><p>تا که ورا سفره، پر از نان شود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>یا که فلان، گربه هر خوان شود</p></div>
<div class="m2"><p>ایران، ویران ز وزیران شود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مهدی کچل سور مهیا شده</p></div>
<div class="m2"><p>چر چر ما جور و مهیا شده</p></div></div>