---
title: >-
    شمارهٔ ۱۱ - مستزاد مجلس چهارم
---
# شمارهٔ ۱۱ - مستزاد مجلس چهارم

<div class="b" id="bn1"><div class="m1"><p>این مجلس چارم به خدا ننگ بشر بود!</p></div>
<div class="m2"><p>دیدی چه خبر بود؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر کار که کردند، ضرر روی ضرر بود</p></div>
<div class="m2"><p>دیدی چه خبر بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>این مجلس چهارم «خودمانیم » ثمر داشت؟</p></div>
<div class="m2"><p>والله ضرر داشت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>صد شکر که عمرش، چو زمانه بگذر بود</p></div>
<div class="m2"><p>دیدی چه خبر بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دیگ وکلا جوش زد و کف شد و سر رفت</p></div>
<div class="m2"><p>باد همه در رفت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ده مژده که عمر وکلا، عمر سفر بود</p></div>
<div class="m2"><p>دیدی چه خبر بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دیگر نکند هو، نزند جفته «مدرس»</p></div>
<div class="m2"><p>در سالون مجلس</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بگذشت دگر، مدتی ار محشر خر بود</p></div>
<div class="m2"><p>دیدی چه خبر بود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دیگر نزد با «قر و قنبیله » معلق</p></div>
<div class="m2"><p>یعقوب جعلق</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>یعقوب خر بارکش این دو نفر بود</p></div>
<div class="m2"><p>دیدی چه خبر بود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سرمایه بدبختی ایران، دو قوام است</p></div>
<div class="m2"><p>این سکه به نام است</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>یک ملتی از این دو نفر، خون به جگر بود</p></div>
<div class="m2"><p>دیدی چه خبر بود</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>آن کس که قوام است و به دولت همه کاره است</p></div>
<div class="m2"><p>از بس که بود پست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>در بی شرفی، عبرت تاریخ سیر بود</p></div>
<div class="m2"><p>دیدی چه خبر بود</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بر سلطنت آن کس که قوام است و بخوبر</p></div>
<div class="m2"><p>شد دو سیه ها پر:</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>زین دزد که دزدیش، از اندازه به در بود</p></div>
<div class="m2"><p>دیدی چه خبر بود</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>هر دفعه که این قحبه، رئیس الوزرا شد</p></div>
<div class="m2"><p>دیدی که چها شد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>این دوره چه گویم، که مضارش چقدر بود؟</p></div>
<div class="m2"><p>دیدی چه خبر بود</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>آن واقعه مسجدیان! کم ضرری داشت؟</p></div>
<div class="m2"><p>یا کم خطری داشت؟</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>آن فتنه ز مشروطه، شکاننده کمر بود</p></div>
<div class="m2"><p>دیدی چه خبر بود</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>آن روز که در جامعه: آن نهضت خر شد</p></div>
<div class="m2"><p>دیدی چه خبر بود</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>از غیظ جهان در نظرم، زیر و زبر بود</p></div>
<div class="m2"><p>دیدی چه خبر بود</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>در بیستمین قرن، ز پس حربه تکفیر؟</p></div>
<div class="m2"><p>ای ملت اکبیر!</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>افسوس نفهمید که آن از چه ممر بود</p></div>
<div class="m2"><p>دیدی چه خبر بود</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>تکفیر سلیمان نمازی دعائی</p></div>
<div class="m2"><p>ملت به کجائی؟</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>این مسئله کی، منطقی اهل نظر بود</p></div>
<div class="m2"><p>دیدی چه خبر بود</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>از من به قوام این بگو: الحق که نه مردی</p></div>
<div class="m2"><p>زین کار که کردی</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>ریدی به سر هر چه که عمامه به سر بود</p></div>
<div class="m2"><p>دیدی چه خبر بود</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>من دشمن دین نیستم، اینگونه نبینم</p></div>
<div class="m2"><p>من حامی دینم</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>دستور ز لندن بد و با دست بقر بود</p></div>
<div class="m2"><p>دیدی چه خبر بود</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>با «آشتیانی » ز چه این مرد کم از زن</p></div>
<div class="m2"><p>شد دست به گردن</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>ای کاش که بر گردن این هر دو تبر بود</p></div>
<div class="m2"><p>دیدی چه خبر بود</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>آن کس که زند این تبر، آن «سید ضیا» بود</p></div>
<div class="m2"><p>او دست خدا بود</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>بر مردم ایران، به خدا نور بصر بود</p></div>
<div class="m2"><p>دیدی چه خبر بود</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>کافی نبود هر چه (ضیا) را بستائیم</p></div>
<div class="m2"><p>از عهده نیائیم</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>من چیز دگر گویم و او چیز دگر بود</p></div>
<div class="m2"><p>دیدی چه خبر بود</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>دیدی که ( . . . ) وکلا را همه خر کرد</p></div>
<div class="m2"><p>درب همه تر کرد</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>در مجلس چارم، خر نر با خر نر بود</p></div>
<div class="m2"><p>دیدی چه خبر بود</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>زد صدمه ( . . . ) بسی از کینه به ملت</p></div>
<div class="m2"><p>با نصرت دولت</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>آن پوزه که عکس العمل قرص قمر بود</p></div>
<div class="m2"><p>دیدی چه خبر بود</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>شهزاده فیروز همان قحبه خائن</p></div>
<div class="m2"><p>با آن پز چون جن</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>هم صیغه «کرزن » بد و هم فکر ددر بود</p></div>
<div class="m2"><p>دیدی چه خبر بود</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>خواهر زن کرزن که محمد ولی میرزاست</p></div>
<div class="m2"><p>مطلب همه اینجاست</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>چون موش، مدام از پی دزدیدن زر بود</p></div>
<div class="m2"><p>دیدی چه خبر بود</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>سید تقی آن کلفت ممد ولی میرزا</p></div>
<div class="m2"><p>مجلس چو شد افنا</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>این جنده زن، افسرده تر از خفته ذکر بود</p></div>
<div class="m2"><p>دیدی چه خبر بود</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>هر چند که «یعقوب» بنام است به پستی</p></div>
<div class="m2"><p>در دزدپرستی</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>این مرد که زان مرد که هم (مردکه)تر بود</p></div>
<div class="m2"><p>دیدی چه خبر بود</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>آن شیخک کرمانی زر مسلک ریقو</p></div>
<div class="m2"><p>کم مدرک و پررو</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>هر روز سر سفره اشراف، دمر بود</p></div>
<div class="m2"><p>دیدی چه خبر بود</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>شد مصرف پرچانگی شیخ محلات</p></div>
<div class="m2"><p>مجلس همه اوقات</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>خیلی دگر این شیخ پدر سوخته لچر بود</p></div>
<div class="m2"><p>دیدی چه خبر بود</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>سرچشمه پستی و خداوند تلون</p></div>
<div class="m2"><p>آقای «تدین »</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>این زنجلب از «داور» زن قحبه بتر بود</p></div>
<div class="m2"><p>دیدی چه خبر بود</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>آقای لسان «عرعر» و تیز و لگدی داشت</p></div>
<div class="m2"><p>خوب این چه بدی داشت</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>چون چاره اش آسان، دو سه من یونجه تر بود</p></div>
<div class="m2"><p>دیدی چه خبر بود</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>می خواست (ملک) خود برساند به وزارت</p></div>
<div class="m2"><p>با زور سفارت</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>افسوس که عمامه، برایش سر خر بود</p></div>
<div class="m2"><p>دیدی چه خبر بود</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>آن شیخک خولی پز و بدریخت امین نیست</p></div>
<div class="m2"><p>اینست و جز این نیست</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>آن کس که رخش همچو سرین بز گر بود</p></div>
<div class="m2"><p>دیدی چه خبر بود</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>تسبیح به کف، جامه تقوای به تن شد</p></div>
<div class="m2"><p>خواهان وطن شد</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>گویم ز چه عمامه به سر، در پی شر بود</p></div>
<div class="m2"><p>دیدی چه خبر بود</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>عمامه به سر هر که، که بنهاد دو کون است</p></div>
<div class="m2"><p>یک کونش که کونست:</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>آن گنبد مندیل سرش، کون دگر بود</p></div>
<div class="m2"><p>دیدی چه خبر بود</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>آن مردکه خر، که وکیل همدان است</p></div>
<div class="m2"><p>دیدی که چسان است</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>یک پارچه کون، از بن پا تا پس سر بود</p></div>
<div class="m2"><p>دیدی چه خبر بود</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>آن معتمدالسلطنه خائن مأبون</p></div>
<div class="m2"><p>در پشت تریبون</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>یک روز که در جایگه خویش پکر بود</p></div>
<div class="m2"><p>دیدی چه خبر بود</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>می گفت که بر کرسی مجلس چو نشینم</p></div>
<div class="m2"><p>از دست نشینم</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>راحت نیم ای کاش که این کرسی ذکر بود</p></div>
<div class="m2"><p>دیدی چه خبر بود</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>اغلب وکلا این سخن، از وی چو شنفتند</p></div>
<div class="m2"><p>احسنت بگفتند</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>دیدند در این نطق، بسی حسن اثر بود</p></div>
<div class="m2"><p>دیدی چه خبر بود</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>افسار وکیل همدان را چو ببستند</p></div>
<div class="m2"><p>یاران بنشستند</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>گفتند که این (ما چه خر) آبستن زر بود</p></div>
<div class="m2"><p>دیدی چه خبر بود</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>این مجلس چارم، چه بگویم که چها داشت</p></div>
<div class="m2"><p>(سلطان علما) داشت</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>پس من خرم، این مرد که گر نوع بشر بود؟</p></div>
<div class="m2"><p>دیدی چه خبر بود</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>از بس که شد آبستن و زائید فراوان</p></div>
<div class="m2"><p>قاطر شده ارزان</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>گوئی کمر آشتیانی، ز فنر بود</p></div>
<div class="m2"><p>دیدی چه خبر بود</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>مستوفی از آن نطق که چون توپ صدا کرد</p></div>
<div class="m2"><p>مشت همه وا کرد</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>فهماند که در مجلس چارم چه خبر بود</p></div>
<div class="m2"><p>دیدی چه خبر بود</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>من نیز یکی، حرف بگفتم وکلا را</p></div>
<div class="m2"><p>در مجلس شورا</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>هر چند که از حرف، در ایران چه ثمر بود</p></div>
<div class="m2"><p>دیدی چه خبر بود</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>نه سال گذشته که گذشتم ز مداین</p></div>
<div class="m2"><p>گشتم ز مداین</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>آزرده بدانسان که پدر مرده پسر بود</p></div>
<div class="m2"><p>دیدی چه خبر بود</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>ویرانه یکی قصر شد، از دور نمایان شود</p></div>
<div class="m2"><p>در قافله یاران:</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>گفتند که این راه پر از خوف و خطر بود</p></div>
<div class="m2"><p>دیدی چه خبر بود</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>جائی است خطرناک و پر از سارق و جانی</p></div>
<div class="m2"><p>آن گونه که دانی</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>عریان شود آن کس که از آن راهگذر بود</p></div>
<div class="m2"><p>دیدی چه خبر بود</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>کسرای عدالتگر، اگر زنده بد این عصر</p></div>
<div class="m2"><p>اینسان نبد این قصر</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>گفتم که به اعصار گذشته، چه مگر بود؟</p></div>
<div class="m2"><p>دیدی چه خبر بود</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>گفتند که بوداست عدالتگه ساسان</p></div>
<div class="m2"><p>آن روز که ایران:</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>سرتاسر به سرش، مملکت علم و هنر بود</p></div>
<div class="m2"><p>دیدی چه خبر بود</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>من در غم این، کز چه عدالتگه کشور؟</p></div>
<div class="m2"><p>شد دزدگه آخر</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>زین نکته، غم اندر دل من بی حد و مر بود</p></div>
<div class="m2"><p>دیدی چه خبر بود</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>این منزل دزدان شدن، بارگه داد!</p></div>
<div class="m2"><p>بیرون نشد از یاد</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>همواره همین مسئله، در مد نظر بود</p></div>
<div class="m2"><p>دیدی چه خبر بود</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>تا این که در این دوره بدیدم وکلا را</p></div>
<div class="m2"><p>در مجلس شورا</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>دیدم دگر این باره، از آن باره بتر بود</p></div>
<div class="m2"><p>دیدی چه خبر بود</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>ویران شده، شد دزدگه، آن بنگه کسرا!</p></div>
<div class="m2"><p>وین مجلس شورا</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>ویران نشده دزدگه و مرکز شر بود</p></div>
<div class="m2"><p>دیدی چه خبر بود</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>این مجلس شورا نبد و بود کلوپی</p></div>
<div class="m2"><p>یک مجمع خوبی</p></div></div>
<div class="b" id="bn102"><div class="m1"><p>از هر که شب از گردنه، بردار و ببر بود</p></div>
<div class="m2"><p>دیدی چه خبر بود</p></div></div>
<div class="b" id="bn103"><div class="m1"><p>هرگز یکی از این وکلا زنده نبودی</p></div>
<div class="m2"><p>پاینده نبودی</p></div></div>
<div class="b" id="bn104"><div class="m1"><p>این جامعه زنده نما زنده اگر بود</p></div>
<div class="m2"><p>دیدی چه خبر بود</p></div></div>
<div class="b" id="bn105"><div class="m1"><p>وآنگه شدی از بیخ و بن، این عدل مظفر</p></div>
<div class="m2"><p>با خاک برابر</p></div></div>
<div class="b" id="bn106"><div class="m1"><p>حتی نه به تاریخ، از آن نقش صور بود</p></div>
<div class="m2"><p>دیدی چه خبر بود</p></div></div>
<div class="b" id="bn107"><div class="m1"><p>تنها نه همین کاخ، سزاوار خرابی است</p></div>
<div class="m2"><p>این حرف حسابیست</p></div></div>
<div class="b" id="bn108"><div class="m1"><p>ای کاش که سرتاسر «ری » زیر و زبر بود</p></div>
<div class="m2"><p>دیدی چه خبر بود</p></div></div>
<div class="b" id="bn109"><div class="m1"><p>ای «ری » تو چه خاکی که چه ناپاک نهادی!:</p></div>
<div class="m2"><p>تو شهر فسادی</p></div></div>
<div class="b" id="bn110"><div class="m1"><p>از شر تو یک مملکتی، پر ز شر بود!</p></div>
<div class="m2"><p>دیدی چه خبر بود</p></div></div>
<div class="b" id="bn111"><div class="m1"><p>«شمر» از پی تو، جد مرا کشت، چنان زار</p></div>
<div class="m2"><p>لعنت به تو صد بار</p></div></div>
<div class="b" id="bn112"><div class="m1"><p>صد لعن به او نیز، که رنجش به هدر بود</p></div>
<div class="m2"><p>دیدی چه خبر بود</p></div></div>
<div class="b" id="bn113"><div class="m1"><p>ای کاش که یک روز، ببینم درین شهر</p></div>
<div class="m2"><p>از خون همه نهر</p></div></div>
<div class="b" id="bn114"><div class="m1"><p>در هر گذری، لخته خون تا به کمر بود</p></div>
<div class="m2"><p>دیدی چه خبر بود</p></div></div>
<div class="b" id="bn115"><div class="m1"><p>از کوه (وزو) آنچه که شد خطه (پمپی)</p></div>
<div class="m2"><p>آن به که شود ری</p></div></div>
<div class="b" id="bn116"><div class="m1"><p>ای کاش که در کوه دماوند اثر بود</p></div>
<div class="m2"><p>دیدی چه خبر بود</p></div></div>
<div class="b" id="bn117"><div class="m1"><p>این طبع تو «عشقی » به خدائی خداوند</p></div>
<div class="m2"><p>از کوه دماوند:</p></div></div>
<div class="b" id="bn118"><div class="m1"><p>محکم تر و معظم تر و آتشکده تر بود</p></div>
<div class="m2"><p>دیدی چه خبر بود</p></div></div>