---
title: >-
    شمارهٔ ۲۱ - نمایندگان ریاکار
---
# شمارهٔ ۲۱ - نمایندگان ریاکار

<div class="b" id="bn1"><div class="m1"><p>رند شیادی که دارایی وی</p></div>
<div class="m2"><p>یک کت و شلوار و یک سرداری است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ریش بتراشیده، اسبیل از دو سوی</p></div>
<div class="m2"><p>راست بالا رفته، کج دمداری است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گرچه او را نیست، دیناری به جیب</p></div>
<div class="m2"><p>هیکلش چون مردم درباری است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در خیابان هرکه بیندش این چنین</p></div>
<div class="m2"><p>گوید این شارژدافر بلغاری است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شغل این جنتلمن عالیجناب</p></div>
<div class="m2"><p>در خیابان‌ها قدم‌برداری است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مسلکش دزدی ز هر ره شد، کنون:</p></div>
<div class="m2"><p>للعجب بهر وطن غمخواری است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از قضا روزی، خیابان دیدمش</p></div>
<div class="m2"><p>تند از بالا روان چاپاری است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ظهر تابستان و خور بالای سر</p></div>
<div class="m2"><p>از در و دیوار، آتش باری است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>داده او تغییر پز، من در عجب!</p></div>
<div class="m2"><p>کاین چه طرز تازه طراری است؟</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>جبه و لباده و شال و قبا</p></div>
<div class="m2"><p>در برش جای کت و سرداری است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بر سرش عمامه، رنگی نو ظهور</p></div>
<div class="m2"><p>فینه‌ای و رشته چلواری است</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هشته یک خروار ریش و عقل مات</p></div>
<div class="m2"><p>زین خر و زین ریش یک خرواری است</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>زود بگرفتم سر راهش که هان:</p></div>
<div class="m2"><p>بازت این چه بازی و بی‌عاری است؟</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>خر ز گرمای هوا، تب می‌کند</p></div>
<div class="m2"><p>ای خر این پالانت سنگین باری است!</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>وآنگه این ریش دم گامیش چیست؟</p></div>
<div class="m2"><p>کاین چنین در صورتت گلناری است!</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گفت این ریشی که بینی ریش نیست:</p></div>
<div class="m2"><p>ریشخند مردم بازاری است!</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>تازه در خط وکالت رفته‌ام</p></div>
<div class="m2"><p>با عوامم عزم خوش‌رفتاری است!</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گفتمش: تغییر «اونیفورم» هم</p></div>
<div class="m2"><p>در وکالت، چون نظام اجباری است؟</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>هشتی عمامه، کله برداشتی!</p></div>
<div class="m2"><p>گفت: این رسم کله‌برداری است!</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>وین لباس و هیکل مردم‌فریب</p></div>
<div class="m2"><p>اولین فرمول مردم‌داری است!!</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ریش انباری ز رأی مردم است</p></div>
<div class="m2"><p>رأی مردم اندر آن انباری است</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>اولین شرط وکالت: ریش و آنک</p></div>
<div class="m2"><p>می‌تراشد از وکالت عاری است</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>دیدمش آنگه که می‌گفت این سخن</p></div>
<div class="m2"><p>آبی از بینی به ریشش جاری است</p></div></div>
<div class="b2" id="bn24"><p>کاتلین شرطش کثافت‌کاری است!</p>
<p>کاولین شرطش کثافت‌کاری است!</p></div>