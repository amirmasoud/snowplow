---
title: >-
    شمارهٔ ۱۷ - گردون من
---
# شمارهٔ ۱۷ - گردون من

<div class="b" id="bn1"><div class="m1"><p>دل پر خون من را کس ندارد</p></div>
<div class="m2"><p>سر مجنون من را کس ندارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همه کس را، ز گردون دل کباب است</p></div>
<div class="m2"><p>ولی گردون من را کس ندارد</p></div></div>