---
title: >-
    شمارهٔ ۱۶ - افتخار شاعر
---
# شمارهٔ ۱۶ - افتخار شاعر

<div class="b" id="bn1"><div class="m1"><p>سوگند به مردی، ار پی زر گردم</p></div>
<div class="m2"><p>نامردم اگر، ز گفته ام برگردم!</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خوانند مرا همسر قارون و روچلد</p></div>
<div class="m2"><p>گر زآنکه کلانترین توانگر گردم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بگذار، ادیب بی بضاعت باشم</p></div>
<div class="m2"><p>با سعدی و شکسپیر، همسر گردم</p></div></div>