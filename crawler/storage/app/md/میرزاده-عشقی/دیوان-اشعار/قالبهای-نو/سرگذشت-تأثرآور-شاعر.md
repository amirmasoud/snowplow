---
title: >-
    سرگذشت تأثرآور شاعر
---
# سرگذشت تأثرآور شاعر

<div class="b" id="bn1"><div class="m1"><p>در منتهاالیه خیابان، بود پدید</p></div>
<div class="m2"><p>تهران برون شهر، خرابه یکی بنای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گسترده مه، ز روزنه شاخ‌های بید</p></div>
<div class="m2"><p>فرشی که تا بد، ار بلرزد همی هوای</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ساعت دوازده است، هلا نیمه‌شب رسید</p></div>
<div class="m2"><p>جز وای وای جغد نیاید دگر صدای</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یک بیست‌ساله شاعری، آواره و فرید</p></div>
<div class="m2"><p>با هیکل نحیف و خیالات غم‌فزای</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از دست میخ کفش به پا، گه همی‌جهید</p></div>
<div class="m2"><p>در کفش می‌نمود، همی جابه‌جای پای</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون دلش خوراک و چو پیراهن شهید</p></div>
<div class="m2"><p>دوشش عبای کهنه، کفن در بر گدای</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شام از پس گرسنگی، مدتی مدید</p></div>
<div class="m2"><p>یک نیمه نان بخورده، پس کوچه در خفای</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در لرزه و تعب، ز تب و نوبه می‌مکید</p></div>
<div class="m2"><p>اندر دهانش انگشت، از حسرت دوای</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ناگه سکوت، پرده شب را ز هم درید</p></div>
<div class="m2"><p>از دست حزن خویش، چو بگریست های های</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خوابید روی خاک و عبا بر سرش کشید</p></div>
<div class="m2"><p>سنگی نهاد زیر سرش، بهر متکای</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>با آن که در نتیجه عشق وطن گزید</p></div>
<div class="m2"><p>در این خراب مانده وطن در خرابه‌های</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بازش ببین کزو، در و دیوارش می‌شنید</p></div>
<div class="m2"><p>دایم ز شام تا سحر، این ناله کی خدای!</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گر این چنین به خاک وطن، شب سحر کنم</p></div>
<div class="m2"><p>خاک وطن که رفت چه خاکی به سر کنم؟</p></div></div>