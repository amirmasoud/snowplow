---
title: >-
    شمارهٔ ۲۲ - در لباس دین
---
# شمارهٔ ۲۲ - در لباس دین

<div class="b" id="bn1"><div class="m1"><p>ای صدرنشینان که همه مصدر دینید</p></div>
<div class="m2"><p>«صدر» ار ز میان رفت، شما صدر نشینید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>امروز نشینید و بر این مسند و فرداست:</p></div>
<div class="m2"><p>کز ذیل گرفته، همه با صدر قرینید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عمر این دو سه روز است که هر روز به آن روز</p></div>
<div class="m2"><p>گوئید نه عمر است، و پی روز پسینید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سنجید که عمر این دو سه روز است: ولی کی</p></div>
<div class="m2"><p>آن روز، کز آن بعد «دگرروز» نبینید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای زمره انگشت نما گشته به تقوی</p></div>
<div class="m2"><p>در حلقه مردان خدا، همچو نگینید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>امروز که بنشسته بصدرید به دنیا</p></div>
<div class="m2"><p>فرداست که در صفحه «فردوس برینید!»</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای رتبه شما راست به دنیا و به عقبی</p></div>
<div class="m2"><p>زیرا که شما حافظ این دین مبینید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از پرتو دین هر دو جهانست شما را</p></div>
<div class="m2"><p>دین گر ز میان رفت نه آنید و نه اینید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بنشسته همی دشمن آئین به کمینتان</p></div>
<div class="m2"><p>پرسم ز شما هیچ شما هم به کمینید؟</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>من مردم (عشقم) ز چه رو غمخور دینم؟</p></div>
<div class="m2"><p>این غصه شماراست شما حافظ دینید؟!</p></div></div>