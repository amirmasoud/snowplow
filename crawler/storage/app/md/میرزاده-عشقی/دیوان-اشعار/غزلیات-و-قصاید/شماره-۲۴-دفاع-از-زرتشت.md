---
title: >-
    شمارهٔ ۲۴ - دفاع از زرتشت
---
# شمارهٔ ۲۴ - دفاع از زرتشت

<div class="b" id="bn1"><div class="m1"><p>ای دختران ترک! خدا را، حیا کنید</p></div>
<div class="m2"><p>باری در این معامله، شرم از خدا کنید!</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یا رخ نهان کنید که دل نابرید یا</p></div>
<div class="m2"><p>با عاشقان دلشده کمتر جفا کنید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یا وعده نادهید که با ما وفا کنید</p></div>
<div class="m2"><p>یا برقرار وعده، خودتان وفا کنید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یغما نموده اید دل و دین ما بلی</p></div>
<div class="m2"><p>کی عادت قدیمی خود رها کنید؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ترک ختا همیشه به یغما به نام بود</p></div>
<div class="m2"><p>یک چندهم رواست که ترک خطا کنید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جائی کشید کار ز یغما که این زمان</p></div>
<div class="m2"><p>یغمای شت، پیمبر پیشین ما کنید؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زرتشت دل نبود که آن را توان ربود!</p></div>
<div class="m2"><p>حاشا قیاس دل، ز چه با انبیا کنید؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زرتشت بردنی نبود، این طمع چه سود؟</p></div>
<div class="m2"><p>تنها همان، ببردن دل اکتفا کنید!</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>امروز قصد بردن پیغمبران، کنید:</p></div>
<div class="m2"><p>فردا بعید نیست که قصد خدا کنید!</p></div></div>