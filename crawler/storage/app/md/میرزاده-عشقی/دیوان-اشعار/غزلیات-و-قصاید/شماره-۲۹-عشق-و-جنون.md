---
title: >-
    شمارهٔ ۲۹ - عشق و جنون
---
# شمارهٔ ۲۹ - عشق و جنون

<div class="b" id="bn1"><div class="m1"><p>یاران عبث نصیحت بی‌حاصلم کنید</p></div>
<div class="m2"><p>دیوانه‌ام من عقل ندارم ولم کنید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ممنونِ این نَصایِحم اما من آنچنان</p></div>
<div class="m2"><p>دیوانه‌ای نیم که شما عاقلم کنید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مجنونم آنچنان که مجانین ز من رمند</p></div>
<div class="m2"><p>وای ار به مجلسِ عُقَلا داخلم کنید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>من مُطَّلِع نِیَم که چه با من نموده عشق؟</p></div>
<div class="m2"><p>خوب است این قضیه، سوال از دلم کنید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یک ذره غیر عشق و جنون، ننگرید هیچ</p></div>
<div class="m2"><p>در من اگر که تجزیه آب و گلم کنید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کم طعنه‌ام زنید که غرقی به بحرِ بُهت؟</p></div>
<div class="m2"><p>مَردید اگر؟ هدایت بر ساحلم کنید!</p></div></div>