---
title: >-
    شمارهٔ ۱۳ - استاد عشق
---
# شمارهٔ ۱۳ - استاد عشق

<div class="b" id="bn1"><div class="m1"><p>عاشقی را شرط تنها ناله و فریاد نیست</p></div>
<div class="m2"><p>تا کسی از جان شیرین نگذرد فرهاد نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا نشد رسوای عالم کس نشد استاد عشق</p></div>
<div class="m2"><p>نیم رسوا عاشق، اندر فن خود استاد نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای دل از حال من و بلبل چه می‌پرسی برو</p></div>
<div class="m2"><p>ما دو تن شوریده را کاری به جز فریاد نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به به از این مجلس ملی و آزادی فکر</p></div>
<div class="m2"><p>من چه بنویسم قلم در دست کس آزاد نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رأی من اینست کاندید از برای انتخاب</p></div>
<div class="m2"><p>اندرین دوره مناسب‌تر کس از شداد نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حرف‌های تازه را فرعون هم ناگفته بود</p></div>
<div class="m2"><p>بلکه از چنگیز هم تاریخ را در یاد نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای خدا این مهر استبداد را ویران نما</p></div>
<div class="m2"><p>گرچه در سرتاسرش یک گوشه‌ای آباد نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر که جمهوری است این اوضاع برگیر و به بند</p></div>
<div class="m2"><p>هیچ آزادی طلب بر ضد استبداد نیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>قلب (عشقی) بین که چون سرتاسر ایران‌زمین</p></div>
<div class="m2"><p>از جفای گلرخان یک گوشه‌اش آباد نیست</p></div></div>