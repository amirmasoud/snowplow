---
title: >-
    شمارهٔ ۱۲ - چکامه جنگ: شکایت از مهاجرین و پیش آمدهای ایام مهاجرت
---
# شمارهٔ ۱۲ - چکامه جنگ: شکایت از مهاجرین و پیش آمدهای ایام مهاجرت

<div class="b" id="bn1"><div class="m1"><p>نوع بشر سلاله قابیل جابری:</p></div>
<div class="m2"><p>آموخت از نیاش، به جای برادری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جنگ است جنگ، خاک اروپا نهفته است</p></div>
<div class="m2"><p>در زیر یک صحیفه پولاد اخگری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ایتالی و فرانسه و روس و انگلیس</p></div>
<div class="m2"><p>بلغار و ترک و ژرمن و اتریش و هنگری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بس بمب و توپ جای به جا کرده کوه و دشت</p></div>
<div class="m2"><p>ترسم دگر فتد، کره از این مدوری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دریای آهن است نه عنوان رسم جنگ</p></div>
<div class="m2"><p>باران آتش است نه آئین عسگری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ایران در این میانه، نه اندر صف جدال</p></div>
<div class="m2"><p>نی مانده زین مجادله، بی بهره و بری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یک دسته ای ز نخبه ایرانیان شدند</p></div>
<div class="m2"><p>در فکر استفاده، از اوضاع حاضری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در دیده خشم روس و به دل کین انگلیس</p></div>
<div class="m2"><p>در سر هوای یاری آلمان عبقری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>رفتیم د برابر دشمن که تا کنیم</p></div>
<div class="m2"><p>ابراز زورمندی و اثبات قادری</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>امید ما به یاری آلمان و وی نداشت</p></div>
<div class="m2"><p>جز بذل زر، طریق دگر بهر یاوری</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بغداد را گرفت و جلو آمد انگلیس</p></div>
<div class="m2"><p>اول به زور جنگ و دوم با مدبری</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>آمد شمال و مغرب ایران به چنگ روس</p></div>
<div class="m2"><p>ویران نمود سر به سر، از فرط جابری</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گشتیم ما مهاجر و بدبخت و دربدر</p></div>
<div class="m2"><p>گردون به ما نمود، نهایت ستمگری</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>یک سوی تیغ روس، رسیدست تا کرند</p></div>
<div class="m2"><p>با آن رسوم وحشی و آئین بربری</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>یک سو به خانقین، کشیدست انگلیس</p></div>
<div class="m2"><p>تیغی که دارد، آهنش آب مزوری</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چیزی نماند کاین دو، به هم در رسند و ما</p></div>
<div class="m2"><p>هر یک نشان شویم، به صد پاره پیکری</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بین دو تیغ پیکر ما اوفتاده است</p></div>
<div class="m2"><p>در سرزمین «قصر» به سختی و مضطری</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>هر چند کافی است پی رفع این دو تیغ</p></div>
<div class="m2"><p>تنها «نظام السلطنه » با تیغ حیدری</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>لیک او هم آزمود که دشمن هزارها</p></div>
<div class="m2"><p>از ما فزون تر است اگر نیک بشمری</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>نی آن که دل بباخت، ولیکن نظر نمود</p></div>
<div class="m2"><p>چنگی به دل نمی زند، اکنون دلاوری</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>از رزم پس کناره کشی را صلاح دید</p></div>
<div class="m2"><p>بر هر نفر سپس ز مقامات لشکری</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>اخطار شد که گشته ز هر سو خطر پدید</p></div>
<div class="m2"><p>جد کن که جان خویش ز یک سو بدر بری</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>آن به که پیش خصم به تسلیم رو نمود</p></div>
<div class="m2"><p>وآنگاه چشم داشت به الطاف داوری</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>تنها «نظام سلطنه » را این اجازتست</p></div>
<div class="m2"><p>با چند تن ز هیئت ملی و کشوری</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>تا آن که بر ممالک ترکیه رو کنند</p></div>
<div class="m2"><p>لیک این اجازه نیست همی بهر دیگری</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>این زشت ماجرا چو به من نیز شد بیان</p></div>
<div class="m2"><p>گشتم ز فرط انده و افسوس بستری</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>کردم هزار ناله، کشیدم هزار آه</p></div>
<div class="m2"><p>نفرین ببخت کردم و رسم مقدری</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>کای ناسزا زمانه بی اعتدال دون!</p></div>
<div class="m2"><p>بر ما جفا گذشت ز حد جفاگری</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>ما را گذاردند، رفیقان نیمه راه</p></div>
<div class="m2"><p>اینگونه در مخافت و گشتند اسپری</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بگرفته ششدر غم و افکار مهره وار</p></div>
<div class="m2"><p>در خانه حریف، گرفتار ششدری</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>از بهر یک تن من، این گنبد فراخ</p></div>
<div class="m2"><p>گشته چو چشم تنگ لئیم از حسد وری</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>بیچاره من، فلک زده من، شور بخت من</p></div>
<div class="m2"><p>سرگشته حوادث این دهر سرسری</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>چون من به تیره اختری ای مادر سپهر</p></div>
<div class="m2"><p>دیگر مزای، هست اگر مهر مادری؟</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>من یک تنه بسم به جهان، گر که لازم است</p></div>
<div class="m2"><p>کامل ترین نمونه یی از تیره اختری!</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>سوی کدام خاک، توانم پناه برد؟</p></div>
<div class="m2"><p>پشت کدام سنگ، توان گشت سنگری؟</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>این حکم داد کیست که جمعی همی کنند!</p></div>
<div class="m2"><p>بر دوست پشت، جانب دشمن مجاوری!</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>این حکم زور زاده شور مدرس است</p></div>
<div class="m2"><p>آن به که بیش از این، ننماید مشاوری</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>این عنصر کثیف لجوج سیاه فکر</p></div>
<div class="m2"><p>این موذی مدرس علم مزوری</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>چرکین عمامه، وصله قبا، پاره شب کلاه</p></div>
<div class="m2"><p>اشتر قواره، خیره نگه، چهره قنبری</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>پاپوش پاره، وصله قبا، ژنده پیرهن</p></div>
<div class="m2"><p>آن هیکل تمام عیار از جلنبری</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>بر ما شدست این پز مضحک زمامدار</p></div>
<div class="m2"><p>این قائد عبا به سر خاله چادری</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>بنگر چها کشیدم از او من که باطنش</p></div>
<div class="m2"><p>صد بار بدتر است از این وضع ظاهری</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>اطراف وی گرفته گروهی برای دخل</p></div>
<div class="m2"><p>چونان که در پرستش گوساله سامری</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>پس لطمه ها که عاقبت ایران زمین خورد</p></div>
<div class="m2"><p>زین مرد حیله روبهی و کینه اشتری</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>معلوم نیست بهر چه کرده مسافرت؟</p></div>
<div class="m2"><p>بهر وطن نبوده، قسم بر مهاجری!</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>تنها نه او خراب، برون آمد از میان</p></div>
<div class="m2"><p>و آنان که کرده اند در این راه رهبری!</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>دادند هر یک از دگری بهتر امتحان</p></div>
<div class="m2"><p>در اجنبی پرستی و بیگانه پروری!</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>صندوق های لیره جلو، دوش استران</p></div>
<div class="m2"><p>واندر عقب مهاجر و انصار چرچری</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>دنبال بارهای زر، از بس دویده اند</p></div>
<div class="m2"><p>آموختند خوب، همه رسم شاطری!</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>درویش وار رو به بیابان نهاده اند</p></div>
<div class="m2"><p>قومی برای کسب مقام توانگری</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>زین قوم پولکی، هنر جنگ می نخواه!</p></div>
<div class="m2"><p>هرگز مجو ز جنس مؤنث مذکری؟</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>یک جنگ کرده اند که شد رو سفید از آن</p></div>
<div class="m2"><p>جنگی که کرده اند، یهودان خیبری</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>دو روز جنگ بود و دو سال رجعت است</p></div>
<div class="m2"><p>این بد من آنچه دیدم از ایشان بهادری!</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>آن جنگ هم نه بهر وطن بد نه بهر دین</p></div>
<div class="m2"><p>یا جنگ بهر زر بد و بد جنگ زرگری</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>آنقدر ما بدیم که این روز بد کم است</p></div>
<div class="m2"><p>بهر جزای ما برس، ای روز بدتری!!</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>ای آسمان ببار در این مملکت بلای</p></div>
<div class="m2"><p>این قوم را زوال ده، ای چرخ چنبری!</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>آه مرا نمی نگری، کوری ای سپهر!</p></div>
<div class="m2"><p>نفرین من نمی شنوی، ای فلک کری!</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>این هم نگفته می نگذارم که بین ما</p></div>
<div class="m2"><p>باشد بسی کسان، هم از این عیب ها بری</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>آنها همه مهاجر پاکند و صاف قلب</p></div>
<div class="m2"><p>وجدان جمله پاکتر، از پیکر پری</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>لیکن همه کناره نموده ز کارها</p></div>
<div class="m2"><p>وز دم همه گرفته و مأیوس و قرقری</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>زینها چو بگذری، همه آنان نموده اند</p></div>
<div class="m2"><p>بهر زر این مهاجرت و این مسافری!</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>«یعقوب » نام سید رسوای بدسگال</p></div>
<div class="m2"><p>آن کس که من ندیده ام، آدم به آن خری؟</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>یک مشت لیره دارد و بر کف گرفته است</p></div>
<div class="m2"><p>با آن قیافه و پز منحوس شندری</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>گوید که منکر عمل کیمیا کجاست؟</p></div>
<div class="m2"><p>اینک مهاجری، عمل کیمیاگری!</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>این است آن که بهر مدرس کند مدام</p></div>
<div class="m2"><p>در گاه و نابگاه، همی پای منبری</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>ابله منم که صرف، پی لیلی وطن</p></div>
<div class="m2"><p>رو کرده ام به دشت چو مجنون عامری</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>هر آنچه می رسد به من از زود باوریست</p></div>
<div class="m2"><p>بس رنجها کشیدم، ازین زود باوری!</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>یک ابلهی دیگر این؛ کین گه خطر</p></div>
<div class="m2"><p>فکر نجات نیستم از فرط دلخوری</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>مدح «نظام سلطنه » فرمانده قوا</p></div>
<div class="m2"><p>البته بهتر است ز افسرده خاطری</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>تاریخ اگر چه زین عمل آرد به من شکست</p></div>
<div class="m2"><p>خواند مرا مدیحه سرا همچو انوری</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>لیکن به یک جوان چو من صاحب آرزو</p></div>
<div class="m2"><p>چون گفته شد که در خطر از هر سو اندری</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>از ترس جان خویش، به فرمانده قوا</p></div>
<div class="m2"><p>ناچار گوید، این سخنان دری وری:</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>ای مظهر کمال و مقامات سنجری</p></div>
<div class="m2"><p>ای مرکز صفات و خیالات نادری!</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>گر چه ظفر نیافتی اما مظهر است</p></div>
<div class="m2"><p>در جبهه مهین تو، نور مظفری</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>ایران نمی رود در کف، این ملک جسته است</p></div>
<div class="m2"><p>از چنگ فتنه های مغول و سکندری</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>جنگ این زمانه، همچو قمار است غم مدار</p></div>
<div class="m2"><p>هر چند باختی تو، در آخر همی بری</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>خورشید تا غروب نگردد، سحر چنان</p></div>
<div class="m2"><p>سازد جهان مسخر، از انوار اخگری</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>جانا تو هم فراز سپهری به ملک ما</p></div>
<div class="m2"><p>یعنی تو نیز همسر خورشید خاوری</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>امروز اگر غروب کنی از وطن چه غم؟</p></div>
<div class="m2"><p>فردا کنی طلوع و به چنگش درآوری</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>چشم وطن به روی تو، روشن بود کنون</p></div>
<div class="m2"><p>خورشید مائی، ار چه ز خورشید برتری</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>روز وطن به ما، پس از این روز شب بود</p></div>
<div class="m2"><p>زآن چون گذر کنی تو که خورشید انوری</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>من خامشم تو خویش بیندیش، این نکوست</p></div>
<div class="m2"><p>اینگونه مردمی بگذاری و بگذری؟</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>گر چه جسارتست ولی عرض می کنم:</p></div>
<div class="m2"><p>حیف است از توئی که ز یاران شدی بری</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>هر یک به یک طریق ز سر باز کرده ای</p></div>
<div class="m2"><p>این نیست لایق تو که بر هر سر افسری</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>سر بوده‌ای همیشه، بر این هیئت و کنون</p></div>
<div class="m2"><p>باید که سرنپیچی از آیین سروری</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>تو چون سری و هیئت ما چون تن تواند</p></div>
<div class="m2"><p>ای سر کجا روی؟ که تن خود نمی بری؟</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>باری در این میانه یکی من ز خدمتت</p></div>
<div class="m2"><p>گر مرده ای رها ننمایم مجاوری</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>زین قصه حال خویش به درگاه حضرتت</p></div>
<div class="m2"><p>خاطرنشان همی کنم و یادآوری</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>در کشتی ای نشاند، یکی طرفه ناخدای</p></div>
<div class="m2"><p>با خود کبوتری ز پی نیک منظری</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>کشتی چو شد به مرکز دریا، شروع گشت</p></div>
<div class="m2"><p>توفان و ناخدای شد از ترس لنگری</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>وآنگاه خیره شد به کبوتر که بایدت</p></div>
<div class="m2"><p>بهر نجات خویش ز کشتی برون بری</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>بیچاره در زمان، به هوا شد ولیک دید</p></div>
<div class="m2"><p>آبست موج تیره، ز هر سو که بنگری</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>دید او به هیچ گونه، به ساحل نمی رسد</p></div>
<div class="m2"><p>نی از ره پریدن و نی با شناوری</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>برگشت از هوا و به کشتی نشست و گفت</p></div>
<div class="m2"><p>با ناخدای این سخن از روی مضطری:</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>از ساحل آنچنان که بیاورده یی مرا</p></div>
<div class="m2"><p>بایست تا به ساحل دیگر مرا بری</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>من آن کبوترم، هله در بحر هولناک</p></div>
<div class="m2"><p>ای ناخدا کنون، به خدایم چه بسپری؟</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>من بر فراز دوش تو، باری گران نیم</p></div>
<div class="m2"><p>آن به مرا چو مردم دیگر نه بنگری</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>من هم به هر کجا که خودت می روی ببر</p></div>
<div class="m2"><p>خواهی نپیچی ار سر از آئین رهبری</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>بیهوده نیست گفتم، اگر بر تو ناخدای</p></div>
<div class="m2"><p>بی خود نبود بهر تو کردم کبوتری</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>یعنی بیا از آینه خاطرم ببر</p></div>
<div class="m2"><p>با دست لطف، گرد و غباری مکدری</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>خالق نموده یاوریت تا تو هم به خلق</p></div>
<div class="m2"><p>در وقت خود، دریغ نداری ز یاوری</p></div></div>
<div class="b" id="bn102"><div class="m1"><p>بشنو ز من که نیک تو خواهم، منه ز دست</p></div>
<div class="m2"><p>آئین بنده داری و دستور سروری</p></div></div>
<div class="b" id="bn103"><div class="m1"><p>اینهم بدان که این سخنان بهر رشوه نیست</p></div>
<div class="m2"><p>در حق من مباد که این ظن بد بری؟</p></div></div>
<div class="b" id="bn104"><div class="m1"><p>«قاآنی »ام نه من که زخم خامه بهر آز</p></div>
<div class="m2"><p>نی چامه ساز، بهر درم همچو عنصری</p></div></div>
<div class="b" id="bn105"><div class="m1"><p>حاشا گمان مدار که من کرده ام شعار</p></div>
<div class="m2"><p>لاشه خوری طریقتم، از راه شاعری</p></div></div>
<div class="b" id="bn106"><div class="m1"><p>هر چند لاشه خور نیم، اما مهاجرم</p></div>
<div class="m2"><p>صد بار لاشه به، ز حقوق مهاجری!</p></div></div>
<div class="b" id="bn107"><div class="m1"><p>آن به که حرف آخر خود را بگویمت</p></div>
<div class="m2"><p>شاید اثر کند به تو این حرف آخری:</p></div></div>
<div class="b" id="bn108"><div class="m1"><p>من تازه شاعرم، سخن اینسان سروده ام</p></div>
<div class="m2"><p>وای ار که کهنه کار شوم در سخنوری؟</p></div></div>
<div class="b" id="bn109"><div class="m1"><p>حیف است این قریحه زیبا بیوفتد</p></div>
<div class="m2"><p>در چنگ روزگار سیاه سلندری!</p></div></div>
<div class="b" id="bn110"><div class="m1"><p>شاید همین قریحه، در آینده آورد</p></div>
<div class="m2"><p>الواح به ز گفته سعدی و انوری</p></div></div>
<div class="b" id="bn111"><div class="m1"><p>(عشقی) تو خویش، همسر دیگر کسان مکن</p></div>
<div class="m2"><p>نی دیگران کنند، همی با تو همسری</p></div></div>