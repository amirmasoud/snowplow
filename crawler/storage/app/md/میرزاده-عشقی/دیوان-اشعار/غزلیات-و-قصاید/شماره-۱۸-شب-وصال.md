---
title: >-
    شمارهٔ ۱۸ - شب وصال
---
# شمارهٔ ۱۸ - شب وصال

<div class="b" id="bn1"><div class="m1"><p>امشب آماده یار و بزم و شرابست</p></div>
<div class="m2"><p>گو که همین امشبم ز عمر حسابست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر شبم از هجر، آب دیده روان بود</p></div>
<div class="m2"><p>امشبم از شوق وصل، دیده پرآبست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>لب به لب میگسارش نازده مستم</p></div>
<div class="m2"><p>آنچه زیادست این میانه شرابست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نقش گل سرخ بر حباب چراغست</p></div>
<div class="m2"><p>خوبی این منظر نکو ز دو بابست:</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>روی فروزان یار و گونه سرخش</p></div>
<div class="m2"><p>حقه آن سرخ گل، به روی حبابست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عمر پر از یادگار جور به جور است</p></div>
<div class="m2"><p>عشق فقط یادگار عهد شبابست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بیست و دو سالست، تند می روی ای عمر!</p></div>
<div class="m2"><p>اندکی امشب تأمل این چه شتابست؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>روز خراب من، از خرابی بختم:</p></div>
<div class="m2"><p>نیست: که از اصل، روزگار خرابست!</p></div></div>