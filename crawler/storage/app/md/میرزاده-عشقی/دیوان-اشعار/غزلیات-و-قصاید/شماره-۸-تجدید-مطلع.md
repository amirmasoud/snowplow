---
title: >-
    شمارهٔ ۸ - تجدید مطلع
---
# شمارهٔ ۸ - تجدید مطلع

<div class="b" id="bn1"><div class="m1"><p>در سر پیری برهنه پا بد «مولییر»</p></div>
<div class="m2"><p>گاو بدزدید در شباب «شکسپیر»</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زنده در آتش «برونو» را بفکندند</p></div>
<div class="m2"><p>مرده وی را کنند این همه تکبیر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>«بن جبرول » آن همه ز خلق ستم دید</p></div>
<div class="m2"><p>شد «روسو» در عهد خویش آن همه تحقیر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از پی تجلیل نامشان نک میلیون:</p></div>
<div class="m2"><p>میلیون اصراف می کنندی و تبذیر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>من نیز آنگه که می بمیرم و ماند</p></div>
<div class="m2"><p>شهرت من همچو، خسروان جهانگیر:</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آنگه بینند صد کنایه ز هر حرف</p></div>
<div class="m2"><p>سنجند از هر سخن، هزاران تعبیر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آن یک، اشعار من نماید تخمیس</p></div>
<div class="m2"><p>وین یک، گفتار من نماید تفسیر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>همچو سگان بینشان، پی ستخوانم</p></div>
<div class="m2"><p>جنگ بیفتد، فتم من آنگه عجب گیر!</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ترک سراید که ترک بودست او ترک</p></div>
<div class="m2"><p>شاهد من، شرح نظم وقعه «ازمیر»</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هندو گوید که هندوست او هندو</p></div>
<div class="m2"><p>دفتر اشعارش کشف گشته به کشمیر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ژرمن گوید که از منست او از من</p></div>
<div class="m2"><p>هست به برلن ازو هزاران تصویر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تاریخ آنگه گوید، افسوس افسوس</p></div>
<div class="m2"><p>سود نبرد، آن ادیب، از این همه تحریر!</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>پستی این عصر گوید: ار نه به تاریخ</p></div>
<div class="m2"><p>هیچ ندارند سیر و گرسنه توفیر</p></div></div>