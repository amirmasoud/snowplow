---
title: >-
    شمارهٔ ۲۷ - خنده شاعر
---
# شمارهٔ ۲۷ - خنده شاعر

<div class="b" id="bn1"><div class="m1"><p>من که خندم، نه بر اوضاع کنون می‌خندم</p></div>
<div class="m2"><p>من بدین گنبد بی‌سقف و ستون می‌خندم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو به فرمانده اوضاع کنون می‌خندی</p></div>
<div class="m2"><p>من به فرماندهی کن فیکون می‌خندم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همه کس بر بشر بوقلمونی خندد</p></div>
<div class="m2"><p>من به حزب فلک بوقلمون می‌خندم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خلق خندند به هر آبله‌رخساری و، من:</p></div>
<div class="m2"><p>به رخ این فلک آبله‌گون می‌خندم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هرکس ایدون، به جنون من مجنون خندد</p></div>
<div class="m2"><p>من بر آن کس که بخندد به جنون می‌خندم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آنچه بایست: به تاریخ گذشته خندم</p></div>
<div class="m2"><p>کرده‌ام خنده، بر آینده کنون می‌خندم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هرکه چون من ثمر علم فلاکت دیدی:</p></div>
<div class="m2"><p>مردی از گریه، من دلشده خون می‌خندم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بعد از این من زنم از علم و فنون دم، حاشا!</p></div>
<div class="m2"><p>من به هرچه بتر علم و فنون می‌خندم!</p></div></div>