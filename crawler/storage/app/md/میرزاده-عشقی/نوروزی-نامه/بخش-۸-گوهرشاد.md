---
title: >-
    بخش ۸ - گوهرشاد
---
# بخش ۸ - گوهرشاد

<div class="b" id="bn1"><div class="m1"><p>ایزد اندر عالمت، ای عشق تا بنیاد داد</p></div>
<div class="m2"><p>عالمی بر باد شد، بنیادت ای بر باد باد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من نه آن بودم که آسان رفتم، اندر دام عشق</p></div>
<div class="m2"><p>آفرین بر فرط، استادی آن صیاد باد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سنگدل صیاد، آخر رحم کن، این صید تو:</p></div>
<div class="m2"><p>تا به کی در بند باشد؟ لحظه‌ای آزاد باد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ناله من چون رسد، هرشب به گوش بیستون</p></div>
<div class="m2"><p>بانگ برآرد که: فرهاد و فغانش یاد باد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بیستون! فرهاد را هرگز به من نسبت مده</p></div>
<div class="m2"><p>از زمین تا آسمان فرق من و فرهاد باد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>من به مژگان می‌کنم، آن کار، کو با تیشه کرد</p></div>
<div class="m2"><p>صدهزاران فرق ریزه‌موی با پولاد باد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سوختی بر باد دادی، جان و عقل و دین و دل</p></div>
<div class="m2"><p>خانه‌ام کردی خراب! ای خانه‌ات آباد باد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>من که می‌دانم ز عشق تو، نخواهم برد جان</p></div>
<div class="m2"><p>پس سخن آزاد گویم، هرچه باداباد، باد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گوهری در خانه شهزاده آزاده‌ای‌ست</p></div>
<div class="m2"><p>هرکه دست آورد، آن یکدانه گوهر، شاد باد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دائما رسوای عام و مبتلای طعن خلق</p></div>
<div class="m2"><p>همچو (عشقی) هرکه اندر دام عشق افتاد باد</p></div></div>