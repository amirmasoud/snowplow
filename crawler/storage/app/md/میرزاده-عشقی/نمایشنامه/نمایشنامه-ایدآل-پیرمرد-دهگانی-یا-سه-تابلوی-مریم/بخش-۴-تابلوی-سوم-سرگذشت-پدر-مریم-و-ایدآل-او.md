---
title: >-
    بخش ۴ - تابلوی سوم: سرگذشت پدر مریم و ایدآل او
---
# بخش ۴ - تابلوی سوم: سرگذشت پدر مریم و ایدآل او

<div class="b" id="bn1"><div class="m1"><p>ز مرگ مریم، اینک سه روز بگذشته</p></div>
<div class="m2"><p>سر مزار وی، آن پیرمرد سرگشته:</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نشسته رخ بسر زانوان خود هشته</p></div>
<div class="m2"><p>من از سیاحت بالای کوه برگشته</p></div></div>
<div class="b2" id="bn3"><p>بر آن شدم که من آن پیر را دهم تسکین</p></div>
<div class="b" id="bn4"><div class="m1"><p>(من): خدات صبر دهد زین مصیبت عظمی:</p></div>
<div class="m2"><p>حقیقتا که دلم سوخت، از برای شما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>(پیرمرد): مگر بگوش شما هم رسیده قصه ما!</p></div>
<div class="m2"><p>(من): شنیده ام گل عمر تو چیده اند، خدا:</p></div></div>
<div class="b2" id="bn6"><p>به خاک تیره سپارد جوانی گلچین!</p></div>
<div class="b" id="bn7"><div class="m1"><p>(پیرمرد):درون خاک، مرا دختری جوان افتاد</p></div>
<div class="m2"><p>برای آنکه جوانی شود دو روزی شاد!</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>(من):بر آن جوانک ناپاک روح لعنت باد،</p></div>
<div class="m2"><p>خدای داند هرگه از او نمایم یاد:</p></div></div>
<div class="b2" id="bn9"><p>هزارگونه به نوع بشر کنم نفرین!</p></div>
<div class="b" id="bn10"><div class="m1"><p>بشر مگوی، بر این نسل فاسد میمون</p></div>
<div class="m2"><p>بشر نه! افعی بادست و پاست این دد دون!</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هزار مرتبه گفتم که تف بر این گردون</p></div>
<div class="m2"><p>ببین به شکل بنی آدم آمدست برون!</p></div></div>
<div class="b2" id="bn12"><p>چقدر آلت قتاله زین کهن ماشین؟</p></div>
<div class="b" id="bn13"><div class="m1"><p>(پیرمرد):تو ز آن جوان شده ای دشمن بشر، او کیست؟</p></div>
<div class="m2"><p>بشر هزار برابر بتر بود او چیست؟</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>از او بترها دیدم من، اینکه چیزی نیست!</p></div>
<div class="m2"><p>برای ذم بشر: سرگذشت من کافیست!</p></div></div>
<div class="b2" id="bn15"><p>اگر بخواهی، آگه شوی بیا بنشین</p></div>
<div class="b" id="bn16"><div class="m1"><p>نشستم و بنمود، او شروع بر اظهار:</p></div>
<div class="m2"><p>(پیرمرد):من اهل کرمان بودم در آن خجسته دیار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>قرین عزت بودم، نه همچو اکنون خوار</p></div>
<div class="m2"><p>که شغل دولتیم بود و دولت بسیار</p></div></div>
<div class="b2" id="bn18"><p>بهر وظیفه که بودم بدم درست و امین</p></div>
<div class="b" id="bn19"><div class="m1"><p>هزار و سیصد و هجده ز جانب تهران</p></div>
<div class="m2"><p>بشد جوانک جلفی، حکومت کرمان</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>مرا که سابقه ها بد به خدمت دیوان</p></div>
<div class="m2"><p>معاونت بسپرد او به موجب فرمان</p></div></div>
<div class="b2" id="bn21"><p>ز فرط لطف مرا کرده بد، به خویش رهین</p></div>
<div class="b" id="bn22"><div class="m1"><p>پس از دو ماهی، روزی به شوخی و خنده</p></div>
<div class="m2"><p>بگفت: خانمکی خواهم از تو زیبنده!</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>برو بجوی که جوینده است یابنده</p></div>
<div class="m2"><p>بگفتمش که خود این کار، ناید از بنده!</p></div></div>
<div class="b2" id="bn24"><p>برای من بود، این امر حکمران توهین!</p></div>
<div class="b" id="bn25"><div class="m1"><p>قسم به مردی من مردم و نه نامردم</p></div>
<div class="m2"><p>به آبروی در این شهر زندگی کردم</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>جواب داد که قربان مردمی گردم</p></div>
<div class="m2"><p>من این سخن پی شوخی به پیش آوردم</p></div></div>
<div class="b2" id="bn27"><p>مرنج از من از این شوخی و مباش غمین!</p></div>
<div class="b" id="bn28"><div class="m1"><p>چو دید آب ز من، گرم مینشاید کرد</p></div>
<div class="m2"><p>میانه اش پس از آن روز، گشت با من سرد</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>پس از دو روزی، روزی بهانه ای آورد</p></div>
<div class="m2"><p>مرا بداد فکندند سخت و تا می خورد</p></div></div>
<div class="b2" id="bn30"><p>زدند بر بدن من، چماقهای وزین!</p></div>
<div class="b" id="bn31"><div class="m1"><p>نمود منفصلم از مشاغل دیوان</p></div>
<div class="m2"><p>برای من نه دگر، رتبه ماند و نی عنوان</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>ببین شرافت و مردانگی، در این دوران</p></div>
<div class="m2"><p>گذشته زآن که ندارد ثمر، دهد خسران!</p></div></div>
<div class="b2" id="bn33"><p>بسان صحبت نادان و جامه چرمین</p></div>
<div class="b" id="bn34"><div class="m1"><p>به شهر کرمان، بدنام مرده شوئی بود</p></div>
<div class="m2"><p>که بین مرده شوان شسته آبروئی بود</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>کریه منظر و رسوا و زشتخوئی بود</p></div>
<div class="m2"><p>خلاصه آدم بی شرم و چشم و روئی بود</p></div></div>
<div class="b2" id="bn36"><p>شبی به نزد حکومت برفت آن بیدین</p></div>
<div class="b" id="bn37"><div class="m1"><p>حکومت آنچه به من گفت، گفتمش بیجاست</p></div>
<div class="m2"><p>که این عمل نه سزاوار مردمان خداست</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>به او چو گفت: تو گوئی که از خدا می خواست</p></div>
<div class="m2"><p>جواب داد که البته این وظیفه ماست!</p></div></div>
<div class="b2" id="bn39"><p>من آن کسم که بگویم بر این دعا آمین</p></div>
<div class="b" id="bn40"><div class="m1"><p>برفت زود، در آغاز دخترش را برد</p></div>
<div class="m2"><p>چو سرد گشت از او، رفت خواهرش را برد</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>برای آخر سر نیز همسرش را برد</p></div>
<div class="m2"><p>چو خسته گشت ز زنها، برادرش را برد!</p></div></div>
<div class="b2" id="bn42"><p>نثار کرد بر او هر چه داشت در خورجین!</p></div>
<div class="b" id="bn43"><div class="m1"><p>بدین وسیله بر حکمران مقرب شد</p></div>
<div class="m2"><p>رفیق و روز و هم آهنگ خلوت شب شد</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>به شغل دولتی آن مرده شو، مجرب شد</p></div>
<div class="m2"><p>خلاصه صاحب عنوان و شغل و منصب شد</p></div></div>
<div class="b2" id="bn45"><p>به بخت نیک، ز نیروی ننگ گشت قرین!</p></div>
<div class="b" id="bn46"><div class="m1"><p>به آن سیاه دل، از بس که خلق رو دادند</p></div>
<div class="m2"><p>پس از دو ماه، مقام مرا بدو دادند</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>زمام مردم کرمان، به مرده شو دادند</p></div>
<div class="m2"><p>تعارفات بر او از هزار سو دادند</p></div></div>
<div class="b2" id="bn48"><p>قباله هائی از املاک و اسبها با زین</p></div>
<div class="b" id="bn49"><div class="m1"><p>ز من شنو که چسان سخت شد به من دنیا</p></div>
<div class="m2"><p>زنم ز گرسنگی داد عمر خود به شما</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>نبود هیچ به جز خاک، فرش خانه ما</p></div>
<div class="m2"><p>به جز گرسنگی و حسرت و غم و سرما</p></div></div>
<div class="b2" id="bn51"><p>نماند خوردنئی خانه من مسکین</p></div>
<div class="b" id="bn52"><div class="m1"><p>پس از سه سال که بودم به سختی و ذلت</p></div>
<div class="m2"><p>شنیده شد که به تهران گروهی از ملت</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>بخواستند عدالت سرائی از دولت</p></div>
<div class="m2"><p>چو در مذلت من، ظلم گشته بد علت</p></div></div>
<div class="b2" id="bn54"><p>بدم نیامد ازین نغمه عدالت گین</p></div>
<div class="b" id="bn55"><div class="m1"><p>فتادم از پی غوغا و انجمن سازی</p></div>
<div class="m2"><p>به شب کمیته و هر روز پارتی بازی</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>همیشه نامه شب؛ بهر حاکم اندازی</p></div>
<div class="m2"><p>در این طریق نمودم ز بس که جانبازی</p></div></div>
<div class="b2" id="bn57"><p>شدند دور و برم جمع، جمله معتقدین</p></div>
<div class="b" id="bn58"><div class="m1"><p>مرا بخواست پس، آن مرده شوی بی سر و پا</p></div>
<div class="m2"><p>به من بگفت که مشروطه کی شود اجرا؟</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>چه حکم شاه در ایران زمین چه حکم خدا</p></div>
<div class="m2"><p>مده تو گوش بر این حرفهای پا به هوا!</p></div></div>
<div class="b2" id="bn60"><p>بگفتمش که لکم دینکم ولی دین</p></div>
<div class="b" id="bn61"><div class="m1"><p>عوض نکردم، آئین خویشتن باری</p></div>
<div class="m2"><p>ز بس نمودم، در عزم خویش پاداری</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>شبانه عاقبت آن مرده شوی ادباری</p></div>
<div class="m2"><p>برون نمود ز کرمان مرا به صد خواری</p></div></div>
<div class="b2" id="bn63"><p>به جرم اینکه، تو در شهر کرده ای تفتین</p></div>
<div class="b" id="bn64"><div class="m1"><p>من و دو تن پسرم، شب پیاده از کرمان</p></div>
<div class="m2"><p>برون شدیم زمستان سخت یخ بندان</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>نه توشه ای و نه روپوش، مفلس و عریان</p></div>
<div class="m2"><p>چه گویمت که چه بر ما گذشت از بوران</p></div></div>
<div class="b2" id="bn66"><p>رسید نعش من و بچه هام تا نائین</p></div>
<div class="b" id="bn67"><div class="m1"><p>چو ماجرای مرا، اهل شهر بشنفتند</p></div>
<div class="m2"><p>تمام مردم مشروطه خواه آشفتند</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>چو میهمان عزیزی، مرا پذیرفتند</p></div>
<div class="m2"><p>چرا که مردم آن روزه، راست می گفتند</p></div></div>
<div class="b2" id="bn69"><p>نه مثل مردم امروزه بد دل و بی دین!</p></div>
<div class="b" id="bn70"><div class="m1"><p>بدون سابقه و آشنائی روشن</p></div>
<div class="m2"><p>به این دلیل که مشروطه خواه هستم من</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>یکی اعانه به من داد و آن دگر مسکن</p></div>
<div class="m2"><p>خلاصه آخر از آن مردمان گرفتم زن</p></div></div>
<div class="b2" id="bn72"><p>چو داد سر خط مشروطه، شه مظفر دین</p></div>
<div class="b" id="bn73"><div class="m1"><p>درست روزی، کآن شهریار اعلان داد</p></div>
<div class="m2"><p>یگانه دختر ناکام من، ز مادر زاد</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>تمام مردم، دلشاد مرگ استبداد</p></div>
<div class="m2"><p>من از دو مسئله خوشحال و خرم و دلشاد</p></div></div>
<div class="b2" id="bn75"><p>یکی ز زادن مریم، دگر ز وضع نوین</p></div>
<div class="b" id="bn76"><div class="m1"><p>سپس چو دوره فرزند شه مظفر شد</p></div>
<div class="m2"><p>تو خویش دانی، اوضاع طور دیگر شد</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>میان خلق و شه، ایجاد کین و کیفر شد</p></div>
<div class="m2"><p>به توپ بستن مجلس، قضیه منجر شد</p></div></div>
<div class="b2" id="bn78"><p>زمانه گشت دوباره به کام مرتجعین</p></div>
<div class="b" id="bn79"><div class="m1"><p>دوباره سلطنت خودسری، بشد اعلان</p></div>
<div class="m2"><p>مرا که بیم خطر بود، اندر آن دوران</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>بر آن شدم که به شهری روم شوم پنهان</p></div>
<div class="m2"><p>شدم ز نائین بیرون، به جانب تهران</p></div></div>
<div class="b2" id="bn81"><p>ولی نه از ره نیزار، از طریق خمین</p></div>
<div class="b" id="bn82"><div class="m1"><p>به ری رسیدم و پنهان شدم، دو روزی چند</p></div>
<div class="m2"><p>ولی چه فایده، آخر فتادم اندر بند</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>پلیس مخفی آمد به محبسم افکند!</p></div>
<div class="m2"><p>چه محبسی که هوائی نداشت غیر از گند</p></div></div>
<div class="b2" id="bn84"><p>چه کلبه ای که پلاسی نداشت جز سر گین؟</p></div>
<div class="b" id="bn85"><div class="m1"><p>دو هفته بر من در آن سیاه چال گذشت</p></div>
<div class="m2"><p>در آن دو هفته چه گویم به من چه حال گذشت؟</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>دو هفته مثل دو هفتصد هزار سال گذشت</p></div>
<div class="m2"><p>پس از دو هفته از آنجا یک از رجال گذشت</p></div></div>
<div class="b2" id="bn87"><p>مرا خلاص نمود، آن بزرگ پاک آئین</p></div>
<div class="b" id="bn88"><div class="m1"><p>یکی دو ماه ز بعد خلاصی ام دوران</p></div>
<div class="m2"><p>دگر نماند بد آنسان و گشت دیگر سان</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>که رفته رفته شورش فتاد در جریان</p></div>
<div class="m2"><p>نوید نهضت ستارخان و باقرخان</p></div></div>
<div class="b2" id="bn90"><p>فکند سخت تزلزل، به تخت و تاج و نگین</p></div>
<div class="b" id="bn91"><div class="m1"><p>به خاصه آنکه خبرها، رسید از گیلان</p></div>
<div class="m2"><p>ز وضع شورش و از قتل آقابالاخان</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>فتاد غلغله در شهر و حومه تهران</p></div>
<div class="m2"><p>که عنقریب به شه می شود چنین و چنان</p></div></div>
<div class="b2" id="bn93"><p>چنانکه کرد به ملت، خود او چنان و چنین!</p></div>
<div class="b" id="bn94"><div class="m1"><p>سپس من و پسرانم چو این چنین دیدیم</p></div>
<div class="m2"><p>بدان لحاظ که مشروطه می پرستیدیم</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>به سوی رشت، شبانه روانه گردیدیم</p></div>
<div class="m2"><p>چهار پنج شبی، بین راه خوابیدیم</p></div></div>
<div class="b2" id="bn96"><p>که تا به خطه گیلان شدیم جایگزین</p></div>
<div class="b" id="bn97"><div class="m1"><p>ز جیب خویش خریدیم اسب و زین و تفنگ</p></div>
<div class="m2"><p>قبول زر ننمودیم از کمیته جنگ</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>که زر گرفتن، بهر عقیده باشد ننگ</p></div>
<div class="m2"><p>خلاصه آنکه، پس از مشقهای رنگارنگ</p></div></div>
<div class="b2" id="bn99"><p>شدیم رهسپر جنگ هر سه چون تابین</p></div>
<div class="b" id="bn100"><div class="m1"><p>همین که گشت به قزوین صدای تیر بلند</p></div>
<div class="m2"><p>دو تن جوان من، اول بروی خاک افکند!</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>یکی از ایشان بروی سینه ام جان کند</p></div>
<div class="m2"><p>زدند نزد پدر غوطه آن دو تن فرزند!</p></div></div>
<div class="b2" id="bn102"><p>میان خون خود و خاک خطه قزوین</p></div>
<div class="b" id="bn103"><div class="m1"><p>ولیک با همه حس و مهر اولادی</p></div>
<div class="m2"><p>چو طفلکانم دادند جان در آن وادی</p></div></div>
<div class="b" id="bn104"><div class="m1"><p>به طیب خاطر گفتم: فدای آزادی</p></div>
<div class="m2"><p>مرا بد از پی مشروطه، عشق فرهادی</p></div></div>
<div class="b2" id="bn105"><p>ولیک حیف که آن تلخ بود، نی شیرین</p></div>
<div class="b" id="bn106"><div class="m1"><p>چو دور ری، بنمودند شهسواری‌ها</p></div>
<div class="m2"><p>مجاهدین و سپهدار و بختیاری‌ها</p></div></div>
<div class="b" id="bn107"><div class="m1"><p>گرفت خاتمه، عمر سیاه‌کاری‌ها</p></div>
<div class="m2"><p>وزیر خائن بگریخت با فراری‌ها</p></div></div>
<div class="b2" id="bn108"><p>پیاده ماند شه و مات شد، ازین فرزین!</p></div>
<div class="b" id="bn109"><div class="m1"><p>بشد سپهدار اول، وزیر صدر پناه</p></div>
<div class="m2"><p>دو باره خلوتیان مظفرالدین شاه</p></div></div>
<div class="b" id="bn110"><div class="m1"><p>شدند مصدر کار و مقرب درگاه</p></div>
<div class="m2"><p>یکی وزیر شد و آن دگر رئیس سپاه</p></div></div>
<div class="b2" id="bn111"><p>شد این چنین چو سپهدار گشت رکن رکین</p></div>
<div class="b" id="bn112"><div class="m1"><p>منی که کنده بدم، جان به پای مشروطه</p></div>
<div class="m2"><p>ز پا فتاده بدم، از برای مشروطه</p></div></div>
<div class="b" id="bn113"><div class="m1"><p>بشد دو میوه عمرم، فدای مشروطه</p></div>
<div class="m2"><p>عریضه دادم بر اولیای مشروطه</p></div></div>
<div class="b2" id="bn114"><p>که من که بودم و اکنون شدست حالم این؟</p></div>
<div class="b" id="bn115"><div class="m1"><p>سپس برفتم، هر روز هیئت وزرا</p></div>
<div class="m2"><p>جواب نامه خود را نمودم استدعا</p></div></div>
<div class="b" id="bn116"><div class="m1"><p>ز بعد شش مه، هر روز وعده فردا</p></div>
<div class="m2"><p>چنین نوشت سپهدار، عرض حال شما:</p></div></div>
<div class="b2" id="bn117"><p>به من رسید و جوابش به شعر گویم هین:</p></div>
<div class="b" id="bn118"><div class="m1"><p>«هنوز اول عشق است اضطراب مکن</p></div>
<div class="m2"><p>«تو هم به مطلب خود می رسی شتاب مکن »</p></div></div>
<div class="b" id="bn119"><div class="m1"><p>ز من اگر شنوی، خویش را خراب مکن</p></div>
<div class="m2"><p>ز انقلاب تقاضای نان و آب مکن</p></div></div>
<div class="b2" id="bn120"><p>برو ز راه دگر، نان خود نما تأمین!</p></div>
<div class="b" id="bn121"><div class="m1"><p>شد این سخن بدل من چو خنجر کاری</p></div>
<div class="m2"><p>برای اینکه پس از آنهمه فداکاری</p></div></div>
<div class="b" id="bn122"><div class="m1"><p>روا نبود، کنم فکر کار بازاری</p></div>
<div class="m2"><p>چه خواستم من ازین انقلاب ادباری</p></div></div>
<div class="b2" id="bn123"><p>به غیر شغل قدیمی و رتبه دیرین!</p></div>
<div class="b" id="bn124"><div class="m1"><p>زنم برای من، از بس که غصه خورد همی</p></div>
<div class="m2"><p>پس از سه مه تب لازم گرفت و مرد همی</p></div></div>
<div class="b" id="bn125"><div class="m1"><p>یگانه دختر خود را به من سپرد همی</p></div>
<div class="m2"><p>همان هم آخر، از دست من ببرد همی</p></div></div>
<div class="b2" id="bn126"><p>کسی که کام از او برگرفت بی کابین</p></div>
<div class="b" id="bn127"><div class="m1"><p>دگر نمودم، از آنگاه فکر دهقانی</p></div>
<div class="m2"><p>شدم دگر من، از آن دم به بعد شمرانی</p></div></div>
<div class="b" id="bn128"><div class="m1"><p>به من گذشت در اینجا، همانکه میدانی</p></div>
<div class="m2"><p>غرض قناعت کردم به شغل بستانی</p></div></div>
<div class="b2" id="bn129"><p>بسر ببردم در خانه خراب و گلین</p></div>
<div class="b" id="bn130"><div class="m1"><p>چه گویمت من ازین انقلاب بدبنیاد!</p></div>
<div class="m2"><p>که شد وسیله‌ای از بهر دسته‌ای شیاد!</p></div></div>
<div class="b" id="bn131"><div class="m1"><p>چه مردمان خرابی، شدند از آن آباد!</p></div>
<div class="m2"><p>گر انقلاب بد این، زنده باد استبداد</p></div></div>
<div class="b2" id="bn132"><p>که هر چه بود، ازین انقلاب بود بهین!</p></div>
<div class="b" id="bn133"><div class="m1"><p>ز بعد آنهمه زحمت، مرا در این پیری</p></div>
<div class="m2"><p>شد از نتیجه این انقلاب تزویری</p></div></div>
<div class="b" id="bn134"><div class="m1"><p>نصیب بیل زدن، روزی از زمین گیری</p></div>
<div class="m2"><p>پی نکوهش این انقلاب اکبیری</p></div></div>
<div class="b2" id="bn135"><p>شنو حکایت آن مرده شوی دل چرکین</p></div>
<div class="b" id="bn136"><div class="m1"><p>چو توپ بست محمد علی شه منفور</p></div>
<div class="m2"><p>به کاخ مجلس و رو گشت ملتی مقهور</p></div></div>
<div class="b" id="bn137"><div class="m1"><p>به شهر کرمان آن مرده شوی بد مأمور</p></div>
<div class="m2"><p>بسی ز ملتیان زنده زنده کرد به گور</p></div></div>
<div class="b2" id="bn138"><p>ببین که عاقبت آن کهنه مرده شوی لعین</p></div>
<div class="b" id="bn139"><div class="m1"><p>همین که دید شه از تخت گشت افکنده</p></div>
<div class="m2"><p>هزار مرتبه مشروطه تر شد از بنده!</p></div></div>
<div class="b" id="bn140"><div class="m1"><p>ز بس که گفت که مشروطه باد پاینده</p></div>
<div class="m2"><p>فلان دوله شد، آن دل ز آبرو کنده!</p></div></div>
<div class="b2" id="bn141"><p>کنون شدست ز اشراف نامدار مهین!</p></div>
<div class="b" id="bn142"><div class="m1"><p>چو صحبت از لقب او بشد کشیدم آه</p></div>
<div class="m2"><p>(من) شناختم چه کس است آن پلید نامه سیاه</p></div></div>
<div class="b" id="bn143"><div class="m1"><p>عجب که خواندم در نامه ای تجدد خواه</p></div>
<div class="m2"><p>«فلان که هست ز اشراف جدی و آگاه!</p></div></div>
<div class="b2" id="bn144"><p>به حکمرانی شهر فلان شده تعیین!</p></div>
<div class="b" id="bn145"><div class="m1"><p>(پیرمرد): مگر که ذهن تو از این محیط بیگانه است</p></div>
<div class="m2"><p>گمان مدار که این مرده شوی یک دانه است؟</p></div></div>
<div class="b" id="bn146"><div class="m1"><p>عمو! تمام ادارات، مرده شو خانه است</p></div>
<div class="m2"><p>وزین ره است که این کهنه ملک ویرانه است</p></div></div>
<div class="b2" id="bn147"><p>ز من نمی شنوی رو به چشم خویش ببین</p></div>
<div class="b" id="bn148"><div class="m1"><p>برو به مالیه تا آنکه چیزها بینی</p></div>
<div class="m2"><p>برو به نظمیه تا آنکه چیزها بینی</p></div></div>
<div class="b" id="bn149"><div class="m1"><p>برو به عدلیه تا بی تمیزها بینی</p></div>
<div class="m2"><p>که مرده شوها در پشت میزها بینی</p></div></div>
<div class="b2" id="bn150"><p>چه بی تمیز کسانی شدند میز نشین . . . !</p></div>
<div class="b" id="bn151"><div class="m1"><p>به پشت میز کس ار مرده شو نباشد نیست!</p></div>
<div class="m2"><p>کسی که با او هم رنگ و بو نباشد نیست!</p></div></div>
<div class="b" id="bn152"><div class="m1"><p>کسی که همسر و همکار او نباشد نیست!</p></div>
<div class="m2"><p>کسی که بی شرف و آبرو نباشد نیست!</p></div></div>
<div class="b2" id="bn153"><p>همی ز بالا بگرفته است تا پائین!</p></div>
<div class="b" id="bn154"><div class="m1"><p>چرا نگردد آئین مرده شوئی باب؟</p></div>
<div class="m2"><p>چو نیست هیچ درین مملکت حساب و کتاب!</p></div></div>
<div class="b" id="bn155"><div class="m1"><p>کدام دوره تو دیدی که این رجال خراب</p></div>
<div class="m2"><p>پی محاکمه دعوت شوند پای حساب؟</p></div></div>
<div class="b2" id="bn156"><p>به جز سه ماهه زمان مهین ضیاء الدین</p></div>
<div class="b" id="bn157"><div class="m1"><p>در این زمانه، هر آنکس گذشت از انصاف</p></div>
<div class="m2"><p>ز هیچ بی شرفی، می نکرد استنکاف</p></div></div>
<div class="b" id="bn158"><div class="m1"><p>شرف ورا شود آنگاه کمترین اوصاف</p></div>
<div class="m2"><p>ازین ره است که آن مرده شو شد از اشراف</p></div></div>
<div class="b2" id="bn159"><p>که مرده شو ببرد این شرافت ننگین!</p></div>
<div class="b" id="bn160"><div class="m1"><p>چرا نباید این مملکت ذلیل شود</p></div>
<div class="m2"><p>در انقلاب «سپهدار» چون دخیل شود</p></div></div>
<div class="b" id="bn161"><div class="m1"><p>رجال دوره او هم از این قبیل شود!</p></div>
<div class="m2"><p>یقین بدان تو که این مرده شو وکیل شود</p></div></div>
<div class="b2" id="bn162"><p>کند رسوم و قوانین برای ما تدوین!</p></div>
<div class="b" id="bn163"><div class="m1"><p>شود زمانی ار این مرده شوی از وزرا!</p></div>
<div class="m2"><p>عجب مدار ز دیوانه بازی دنیا!</p></div></div>
<div class="b" id="bn164"><div class="m1"><p>که این زمانه نااصل و دهر بی سر و پا!</p></div>
<div class="m2"><p>زمان موسی، گوساله را نمود خدا!!</p></div></div>
<div class="b2" id="bn165"><p>ولی نداشت، جهان پاس خدمت داروین</p></div>
<div class="b" id="bn166"><div class="m1"><p>به چشم عشقی دنیا چنان نماید پست</p></div>
<div class="m2"><p>که هرزه بازی شش ساله طفل دائم مست</p></div></div>
<div class="b" id="bn167"><div class="m1"><p>به چشم پیر حکیمی رسانده سال به شصت</p></div>
<div class="m2"><p>به اعتقاد من: این کائنات بازیچه است</p></div></div>
<div class="b2" id="bn168"><p>به حیرتم من از این بچه بازی تکوین!</p></div>
<div class="b" id="bn169"><div class="m1"><p>(من):کنون که گشت مبرهن به من که حال تو چیست</p></div>
<div class="m2"><p>به عمر سفله، از این بیش اتصال تو چیست؟</p></div></div>
<div class="b" id="bn170"><div class="m1"><p>دگر ز ماندن در این جهان، خیال تو چیست</p></div>
<div class="m2"><p>به قول مردم امروزه، ایدآل تو چیست؟</p></div></div>
<div class="b2" id="bn171"><p>ز زندگی برهان خویش ز اندکی مرفین</p></div>
<div class="b" id="bn172"><div class="m1"><p>(پیرمرد): کنون که دم زدی از ایدآل، گویم راست:</p></div>
<div class="m2"><p>برای من دگر اینگونه زندگی بیجاست!</p></div></div>
<div class="b" id="bn173"><div class="m1"><p>که گر بمیرم امروز، بهتر از فرداست</p></div>
<div class="m2"><p>مرا ولیک یکی ایدآل در دنیاست</p></div></div>
<div class="b2" id="bn174"><p>که سالها پی وصلش نشسته ام به کمین:</p></div>
<div class="b" id="bn175"><div class="m1"><p>مراست مد نظر، مقصدی که مستورش</p></div>
<div class="m2"><p>مدام دارم و سازم بر تو مذکورش</p></div></div>
<div class="b" id="bn176"><div class="m1"><p>همین که خواست بگوید که چیست منظورش</p></div>
<div class="m2"><p>بگشت منقلب، آنسان دو چشم پرنورش</p></div></div>
<div class="b2" id="bn177"><p>که انقلاب نماید چو چشم های لنین</p></div>
<div class="b" id="bn178"><div class="m1"><p>زبان میان دهانش، به جنبش آمد چون</p></div>
<div class="m2"><p>زبان نبود بدآن سرخ گوشت، بیرق خون</p></div></div>
<div class="b" id="bn179"><div class="m1"><p>بشد سپس سخنانی، از آن دهان بیرون</p></div>
<div class="m2"><p>که دیدم آتیه سرزمین افریدون:</p></div></div>
<div class="b2" id="bn180"><p>شود سراسر، یک قطعه آتش خونین</p></div>
<div class="b" id="bn181"><div class="m1"><p>ز ایدآل خود او چیزها نمود اظهار</p></div>
<div class="m2"><p>از آن میان بشد این جمله ها بسی تکرار:</p></div></div>
<div class="b" id="bn182"><div class="m1"><p>در این محیط چو من بینوا بود بسیار!</p></div>
<div class="m2"><p>که دیده اند، چو من ظلم و زور و رنج و فشار</p></div></div>
<div class="b2" id="bn183"><p>که دیده اند چو من، بس مصیبت سنگین!</p></div>
<div class="b" id="bn184"><div class="m1"><p>به غیر من چه بسا کس که مرده شو دارد؟</p></div>
<div class="m2"><p>که تیره بختی خود را، همه از او دارد</p></div></div>
<div class="b" id="bn185"><div class="m1"><p>تو هر که را که ببینی، یک آرزو دارد:</p></div>
<div class="m2"><p>به این خوش است که دنیا هزار رو دارد</p></div></div>
<div class="b2" id="bn186"><p>شود که گردد، یک روز، روز کیفر و کین</p></div>
<div class="b" id="bn187"><div class="m1"><p>چه خوب روزی آن روز، روز کشتار است</p></div>
<div class="m2"><p>گر آن زمان برسد، مرده شوی بسیار است</p></div></div>
<div class="b" id="bn188"><div class="m1"><p>حواله همه این رجال، بر دار است</p></div>
<div class="m2"><p>برای خائن، چوب و طناب در کار است</p></div></div>
<div class="b2" id="bn189"><p>سزای جمله شود داده از یسار و یمین</p></div>
<div class="b" id="bn190"><div class="m1"><p>تمام مملکت آن روز زیر و رو گردد</p></div>
<div class="m2"><p>که قهر ملت با ظلم روبرو گردد</p></div></div>
<div class="b" id="bn191"><div class="m1"><p>به خائنین زمین، آسمان عدو گردد</p></div>
<div class="m2"><p>زمان کشتن افواج مرده شو گردد</p></div></div>
<div class="b2" id="bn192"><p>بسیط خاک ز خون پلیدشان رنگین</p></div>
<div class="b" id="bn193"><div class="m1"><p>وزیر عدلیه ها، بر فراز دار روند</p></div>
<div class="m2"><p>رئیس نظمیه ها، سوی آن دیار روند</p></div></div>
<div class="b" id="bn194"><div class="m1"><p>کفیل مالیه ها، زنده در مزار روند</p></div>
<div class="m2"><p>وزیر خارجه ها، از جهان کنار روند</p></div></div>
<div class="b2" id="bn195"><p>که تا نماند از ایشان نشان، بروی زمین</p></div>
<div class="b" id="bn196"><div class="m1"><p>بساط بی شرفی، ز آن سپس خورد بر هم</p></div>
<div class="m2"><p>رسد به کیفر خود، نیز قاتل مریم</p></div></div>
<div class="b" id="bn197"><div class="m1"><p>سپس چو گشت خریدار مرده شویان کم</p></div>
<div class="m2"><p>دگر نماند در این ملک از این قبیل آدم</p></div></div>
<div class="b2" id="bn198"><p>همی شود دگر ایران زمین، بهشت برین</p></div>
<div class="b" id="bn199"><div class="m1"><p>دگر در آنکه وجدان کشی هنر نبود</p></div>
<div class="m2"><p>شرف به اشرفی و سکه های زر نبود</p></div></div>
<div class="b" id="bn200"><div class="m1"><p>شرف بدزدی کف رنج رنجبر نبود</p></div>
<div class="m2"><p>شرف به داشتن قصر معتبر نبود</p></div></div>
<div class="b2" id="bn201"><p>شرف نه هست درشکه، نه چرخهای زرین</p></div>
<div class="b" id="bn202"><div class="m1"><p>همی نگردد، آباد این محیط خراب</p></div>
<div class="m2"><p>اگر نگردد از خون خائنین سیراب</p></div></div>
<div class="b" id="bn203"><div class="m1"><p>گمان مدار که این حرفهاست، نقش بر آب</p></div>
<div class="m2"><p>یقین بدان تو که تعبیر می شود این خواب</p></div></div>
<div class="b2" id="bn204"><p>مدان تو این پدر انقلاب را عنین</p></div>
<div class="b" id="bn205"><div class="m1"><p>گرفتم آنکه نباشد مرا، از این پس زیست</p></div>
<div class="m2"><p>بماند از من این فکر، پس مرا غم چیست؟</p></div></div>
<div class="b" id="bn206"><div class="m1"><p>چرا که فکر من صدمه دیده ای مسریست</p></div>
<div class="m2"><p>چو گشت مسری فکری، زمانه ول کن نیست</p></div></div>
<div class="b2" id="bn207"><p>سر ورا نهد آخر، بروی یک بالین</p></div>