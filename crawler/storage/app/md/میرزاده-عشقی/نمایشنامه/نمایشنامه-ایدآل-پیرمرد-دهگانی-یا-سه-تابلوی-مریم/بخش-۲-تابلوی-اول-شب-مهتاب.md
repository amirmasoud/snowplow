---
title: >-
    بخش ۲ - تابلوی اول: شب مهتاب
---
# بخش ۲ - تابلوی اول: شب مهتاب

<div class="b" id="bn1"><div class="m1"><p>اوائل گل سرخ است و انتهای بهار</p></div>
<div class="m2"><p>نشسته ام سر سنگی کنار یک دیوار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جوار دره دربند و دامن کهسار</p></div>
<div class="m2"><p>فضای شمران اندک ز قرب مغرب تار</p></div></div>
<div class="b2" id="bn3"><p>هنوز بد اثر روز، بر فراز «اوین »</p></div>
<div class="b" id="bn4"><div class="m1"><p>نموده در پس که آفتاب تازه غروب</p></div>
<div class="m2"><p>سواد شهرری از دور نیست پیدا خوب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جهان نه روز بود در شمر نه شب محسوب</p></div>
<div class="m2"><p>شفق ز سرخی نیمیش بیرق آشوب</p></div></div>
<div class="b2" id="bn6"><p>سپس ز زردی نیمیش، پرده زرین</p></div>
<div class="b" id="bn7"><div class="m1"><p>چو آفتاب پس کوهسار، پنهان شد</p></div>
<div class="m2"><p>ز شرق از پس اشجار، مه نمایان شد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هنوز شب نشده، آسمان چراغان شد</p></div>
<div class="m2"><p>جهان ز پرتو مهتاب نورباران شد</p></div></div>
<div class="b2" id="bn9"><p>چو نوعروس، سفیداب کرد روی زمین</p></div>
<div class="b" id="bn10"><div class="m1"><p>اگر چه قاعدتا ، شب سیاهی است پدید</p></div>
<div class="m2"><p>خلاف هر شبه، امشب دگر شبیست سپید</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شما بهر چه که خوبست، ماه می گوئید</p></div>
<div class="m2"><p>بیا که امشب، ماهست و دهر، رنگ امید</p></div></div>
<div class="b2" id="bn12"><p>بخود گرفته همانا در این شب سیمین</p></div>
<div class="b" id="bn13"><div class="m1"><p>جهان سپیدتر از فکرهای عرفانیست</p></div>
<div class="m2"><p>رفیق روح من، آن عشق های پنهانیست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>درون مغزم از افکار خوش، چراغانیست</p></div>
<div class="m2"><p>چرا که در شب مه، فکر نیز نورانیست</p></div></div>
<div class="b2" id="bn15"><p>چنانکه دل شب تاریک تیره است و حزین</p></div>
<div class="b" id="bn16"><div class="m1"><p>نشسته ام به بلندی و پیش چشمم باز</p></div>
<div class="m2"><p>به هر کجا که کند چشم کار، چشم انداز</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>فتاده بر سر من فکرهای دور و دراز</p></div>
<div class="m2"><p>بر آن سرم که کنم سوی آسمان پرواز</p></div></div>
<div class="b2" id="bn18"><p>فغان که دهر به من پر نداده چون شاهین</p></div>
<div class="b" id="bn19"><div class="m1"><p>فکنده نور مه از لابلای شاخه بید</p></div>
<div class="m2"><p>به جویبار و چمنزار خالهای سفید</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بسان قلب پر از یأس و نقطه های امید</p></div>
<div class="m2"><p>خوش آنکه دور جوانی من شود تجدید</p></div></div>
<div class="b2" id="bn21"><p>ز سی عقب بنهم پا به سال بیستمین</p></div>
<div class="b" id="bn22"><div class="m1"><p>درون بیشه سیاه و سپید دشت و دمن</p></div>
<div class="m2"><p>تمام خطه تجریش سایه و روشن</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ز سایه روشن عمرم رسید خاطر من</p></div>
<div class="m2"><p>گذشته های سپید و سیه ز سوز و محن</p></div></div>
<div class="b2" id="bn24"><p>که روزگار گهی تلخ بود و گه شیرین</p></div>
<div class="b" id="bn25"><div class="m1"><p>به ابر پاره چو مه نور خویش افشاند</p></div>
<div class="m2"><p>بسان پنبه آتش گرفته می ماند</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ز من مپرس که کبکم خروس می خواند</p></div>
<div class="m2"><p>چو من ز حسن طبیعت که قدر می داند</p></div></div>
<div class="b2" id="bn27"><p>مگر کسان چو من موشکاف و نازک بین</p></div>
<div class="b" id="bn28"><div class="m1"><p>حباب سبز چه رنگست شب ز نور چراغ؟</p></div>
<div class="m2"><p>نموده است همان رنگ ماه منظر باغ</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>نشان آرزوی خویش، این دل پر داغ</p></div>
<div class="m2"><p>ز لابلای درختان، همی گرفت سراغ</p></div></div>
<div class="b2" id="bn30"><p>کجاست آنکه بیاید مرا دهد تسکین</p></div>
<div class="b" id="bn31"><div class="m1"><p>چو زین سیاحت من یک دو ساعتی بگذشت</p></div>
<div class="m2"><p>ز دور دختر دهقانه‌ای هویدا گشت</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>قدم به ناز (بکافوروش) زمین می‌هشت</p></div>
<div class="m2"><p>نظرکنان همه سو، بیمناک بر در و دشت</p></div></div>
<div class="b2" id="bn33"><p>چو فکر از همه مظنون مردمان ظنین</p></div>
<div class="b" id="bn34"><div class="m1"><p>تنش نهفته به چادر نماز آبیگون</p></div>
<div class="m2"><p>برون فتاده از آن پرده، چهره گلگون</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>در آن قیافه گهی شادمان و گه محزون</p></div>
<div class="m2"><p>به صد دلیل به آثار عاشقی مشحون</p></div></div>
<div class="b2" id="bn36"><p>ز سوز عشق نشانها در آن لب نمکین</p></div>
<div class="b" id="bn37"><div class="m1"><p>به رسم پوشش دوشیزگان شمرانی</p></div>
<div class="m2"><p>ز حیث جامه نه شهری بد و نه دهقانی</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>بر او تمام مزایای حسن ارزانی</p></div>
<div class="m2"><p>شبیه تر به فرشته است تا به انسانی</p></div></div>
<div class="b2" id="bn39"><p>مرددم که بشر بود یا که حورالعین</p></div>
<div class="b" id="bn40"><div class="m1"><p>چو روی سبزه لب جو نشست آهسته</p></div>
<div class="m2"><p>بد او چو شاخ گلی روی سبزه ها رسته</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>شد آن فرشته در آن سبزه زار گلدسته</p></div>
<div class="m2"><p>گل ار چه بود، شد از سبزه نیز آرسته</p></div></div>
<div class="b2" id="bn42"><p>هم او ز سبزه و هم سبزه یافت زو تزئین</p></div>
<div class="b" id="bn43"><div class="m1"><p>فکنده زلف ز دو سوی بر جبین سفید</p></div>
<div class="m2"><p>تلالوئی به عذارش ز ماهتاب پدید</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>بسان آینه ای در مقابل خورشید</p></div>
<div class="m2"><p>نه هیچ عضو مر او راست در خور تنقید</p></div></div>
<div class="b2" id="bn45"><p>که هست درخور تمجید و قابل تحسین</p>
<p>نه هیچ وصف مر او را نه درخور تحسین</p></div>
<div class="b" id="bn46"><div class="m1"><p>نگاه مردمک دیده اش سوی بالاست</p></div>
<div class="m2"><p>عیان از این حرکت، گو توجهش به خداست</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>و یا در این حرکت چیزی از خدا می خواست</p></div>
<div class="m2"><p>گهی نظر کند از زیر چشم بر چپ و راست</p></div></div>
<div class="b2" id="bn48"><p>چنانکه در اثر انتظار، منتظرین</p></div>
<div class="b" id="bn49"><div class="m1"><p>سیاهئی به همین دم ز دور پیدا بود</p></div>
<div class="m2"><p>رسید پیش، جوانی بلند بالا بود</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>ز آب و رنگ، همی بد نبود زیبا بود</p></div>
<div class="m2"><p>ز حیث جامه هم، از مردمان حالا بود</p></div></div>
<div class="b2" id="bn51"><p>کلاه ساده و شلوار و ژاکت و پوتین</p></div>
<div class="b" id="bn52"><div class="m1"><p>(جوان) سلام مریم مهپاره (مریم): کیست ایوائی!</p></div>
<div class="m2"><p>(جوان): منم نترس عزیز، از چه وقت اینجائی؟</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>(مریم) توئی عزیز دلم، به چه دیر می آئی</p></div>
<div class="m2"><p>سپس در آن شب مه، آن شب تماشائی!</p></div></div>
<div class="b2" id="bn54"><p>شد آن جوان بر آن ماهپاره جایگزین</p></div>
<div class="b" id="bn55"><div class="m1"><p>دگر بقیه احوال پرسی و آداب</p></div>
<div class="m2"><p>به ماچ و بوسه بجا آمد، اندر آن مهتاب</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>خوش آنکه بر رخ یارش نظر کند شاداب</p></div>
<div class="m2"><p>لبش نجنبد و قلبش کند: سئوال و جواب</p></div></div>
<div class="b2" id="bn57"><p>(عشقی) برای من به خدا، بارها شدست چنین</p></div>
<div class="b" id="bn58"><div class="m1"><p>پس از سه چار دقیقه، ببرد دست آن مرد</p></div>
<div class="m2"><p>دو شیشه سرخ، ز جیب بغل برون آورد</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>از آن دوای که، آن شب به دردشان می خورد</p></div>
<div class="m2"><p>نخست جام به آن ماهرو تعارف کرد</p></div></div>
<div class="b2" id="bn60"><p>(مریم): هزار مرتبه گفتم نمی خورم من ازین</p></div>
<div class="b" id="bn61"><div class="m1"><p>(جوان) بخور که نیست به از این شراب اندر دهر</p></div>
<div class="m2"><p>(مریم): برای من که نخوردم بتر بود از زهر:</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>شراب خوب است اما برای مردم شهر:</p></div>
<div class="m2"><p>که هست خوردن نان از تنور و آب از نهر:</p></div></div>
<div class="b2" id="bn63"><p>نشاط و عشرت ما مردمان کوه نشین</p></div>
<div class="b" id="bn64"><div class="m1"><p>(جوان) ولم بکن، کم از این حرفها بزن، ده بیا:</p></div>
<div class="m2"><p>بخور عزیز دل من، (مریم): نمی خورم والله</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>(جوان): بخور ترا به خدا (مریم): نه نمی خورم به خدا</p></div>
<div class="m2"><p>(جوان): بخور، بخور، ده بخور (مریم): ای ولم بکن آقا</p></div></div>
<div class="b2" id="bn66"><p>خودت بنوش ازین تلخ باده ننگین</p></div>
<div class="b" id="bn67"><div class="m1"><p>(جوان): بخور تصدق بادام چشمهات بخور:</p></div>
<div class="m2"><p>فدای آن لب شیرین تر از نبات بخور:</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>ترا قسم به تمام مقدسات بخور:</p></div>
<div class="m2"><p>ترا قسم به خداوند کائنات بخور:</p></div></div>
<div class="b2" id="bn69"><p>(مریم): پی شراب، کم اسم خدا ببر بی دین!</p></div>
<div class="b" id="bn70"><div class="m1"><p>(جوان): ترا قسم به دل عاشقان افسرده:</p></div>
<div class="m2"><p>به غنچه های سحر ناشکفته پژمرده:</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>به مرگ عاشق ناکام نوجوان مرده:</p></div>
<div class="m2"><p>بخور، بخور، ده بخور نیم جرعه، یک خورده</p></div></div>
<div class="b2" id="bn72"><p>چو دید رام نگردد به حرف، ماه جبین:</p></div>
<div class="b" id="bn73"><div class="m1"><p>همی نمود پر از می پیاله را وان پس</p></div>
<div class="m2"><p>همی نهاد به لبهاش، او همی زد پس</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>(عشقی) دل من از تو چه پنهان، نموده بود هوس</p></div>
<div class="m2"><p>که کاش زین همه اصرار، قدر بال مگس:</p></div></div>
<div class="b2" id="bn75"><p>به من شدی که به زودی نمودمی تمکین</p></div>
<div class="b" id="bn76"><div class="m1"><p>خلاصه کرد به اصرار، نرم یارو را</p></div>
<div class="m2"><p>به زور روی، ز رو برد نازنین رو را</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>نمود با لب وی آشنای، دارو را</p></div>
<div class="m2"><p>خوراند آخر کار، آن «نمی خورم گو» را</p></div></div>
<div class="b2" id="bn78"><p>نه دو پیاله، نه سه، نه چهار، بل چندین</p></div>
<div class="b" id="bn79"><div class="m1"><p>پس از سه چار دقیقه، ز روی شنگولی</p></div>
<div class="m2"><p>شروع شد به سخن های عشق معمولی</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>«تصدقت بروم به، چقدر مقبولی:</p></div>
<div class="m2"><p>تو از تمام دواهای حسن کبسولی:</p></div></div>
<div class="b2" id="bn81"><p>قسم به عشق، تو شیرین تری ز ساخارین »</p></div>
<div class="b" id="bn82"><div class="m1"><p>سخن گهی هم در ضمن شوخی و خنده</p></div>
<div class="m2"><p>بُد از عروسی و عقد و نکاح زیبنده</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>شریک بودن در زندگی آینده</p></div>
<div class="m2"><p>پس آن جوان پی تفریح، پنجه افکنده</p></div></div>
<div class="b2" id="bn84"><p>گرفت در کف، از آن ماه گیسوی پرچین</p></div>
<div class="b" id="bn85"><div class="m1"><p>کشیده نعره که امشب بهشت : «دربند» است</p></div>
<div class="m2"><p>رسد به آرزویش، هر که آرزومند است</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>دو دست من به سر زلف یار پیوند است</p></div>
<div class="m2"><p>بریز باده به حلقم که دست من بند است</p></div></div>
<div class="b2" id="bn87"><p>به جای نقل، بنه بر لبم لب شکرین</p></div>
<div class="b" id="bn88"><div class="m1"><p>به روی دشت و دمن ماهتاب با مه جفت</p></div>
<div class="m2"><p>«بیار باده که شکر خدای باید گفت »</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>ز بعد آن که مر، این نکته چو در را سفت</p></div>
<div class="m2"><p>ز بس که، جام به هم خورد، گوش من بشنفت</p></div></div>
<div class="b2" id="bn90"><p>به نام شکر پیاپی، صدای جین جین جین</p></div>
<div class="b" id="bn91"><div class="m1"><p>از آن به بعد بدیدم که هر دو خوابیدند</p></div>
<div class="m2"><p>خدای شکر که آنها مرا نمی دیدند</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>به هم چون شهد و شکر آن دو یار چسبیدند</p></div>
<div class="m2"><p>به روی سبزه، بسی روی هم بغلطیدند</p></div></div>
<div class="b2" id="bn93"><p>دگر زیاده بر این را نمی کنم تبیین</p></div>
<div class="b" id="bn94"><div class="m1"><p>به روی دشت و دمن ماهتاب تابیده</p></div>
<div class="m2"><p>به هر کجا نگری نقره گرد پاشیده</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>به روی سبزه چمن، آن دو یار خوابیده</p></div>
<div class="m2"><p>مرا ز دیدنشان، لذتیست در دیده</p></div></div>
<div class="b2" id="bn96"><p>چه گویمت که طبیعت چگونه باشد حین؟</p></div>
<div class="b" id="bn97"><div class="m1"><p>صدای قهقهه کبکی ز کوهسار آید</p></div>
<div class="m2"><p>غریو ریختن آب، از آبشار آید</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>ز دور زمزمه سوزناک تار آید</p></div>
<div class="m2"><p>در این میانه صدائی از آن دو یار آید</p></div></div>
<div class="b2" id="bn99"><p>ز فرط خوردن لبهای زیر بر زیرین</p></div>
<div class="b" id="bn100"><div class="m1"><p>وزان ز جانب «توچال » بادی اندک سرد</p></div>
<div class="m2"><p>که شاخه های درختان از آن به هم می خورد</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>همی گذشت چو از خوابگاه آن زن و مرد</p></div>
<div class="m2"><p>برای شامه ها، بوی عشق می آورد</p></div></div>
<div class="b2" id="bn102"><p>هزار بار به از بوی سنبل و نسرین</p></div>
<div class="b" id="bn103"><div class="m1"><p>در آن دقیقه که آنها جدا شدند از هم</p></div>
<div class="m2"><p>به عضو پردگی و محرمانه مریم</p></div></div>
<div class="b" id="bn104"><div class="m1"><p>فتاد دیده پروین و ماه نامحرم</p></div>
<div class="m2"><p>ستاره ها همه دیدند آسمان ها هم</p></div></div>
<div class="b2" id="bn105"><p>که نیمی از تن مریم برون بد از پاچین</p></div>