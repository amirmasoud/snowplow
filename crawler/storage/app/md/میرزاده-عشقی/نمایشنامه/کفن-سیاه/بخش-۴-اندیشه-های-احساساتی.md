---
title: >-
    بخش ۴ - اندیشه‌های احساساتی
---
# بخش ۴ - اندیشه‌های احساساتی

<div class="b" id="bn1"><div class="m1"><p>بوی این درد دل خسرو، از آن باد آمد!</p></div>
<div class="m2"><p>بعد من، بر تو چه ای قصر مه آباد آمد؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که ز غم اشک تو تا دجله بغداد آمد</p></div>
<div class="m2"><p>من چو از خسروم این شکوه همی یاد آمد</p></div></div>
<div class="b2" id="bn3"><p>در و دیوار مه آباد، به فریاد آمد</p></div>
<div class="b" id="bn4"><div class="m1"><p>کای: شهنشاه برون شو ز مغاک</p></div>
<div class="m2"><p>خسروا سر بدر آر، از دل خاک</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حال این خطه، به عهد تو چنین بود؟ ببین</p></div>
<div class="m2"><p>حجله مهر تو، ویرانه کین بود؟ ببین</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پیکرش همسر با خاک زمین بود؟ ببین</p></div>
<div class="m2"><p>خسروا کاخ «مه آباد» تو این بود؟ ببین</p></div></div>
<div class="b2" id="bn7"><p>قصر شیرین تو، این جغدنشین بود؟ ببین</p></div>
<div class="b" id="bn8"><div class="m1"><p>ای خجسته ملک عالمگیر</p></div>
<div class="m2"><p>ملک چندین ملک در تسخیر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در خور تاج سرت، از همه جا باج رسید!</p></div>
<div class="m2"><p>سر برآور، چه ببین بر سر آن تاج رسید؟</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>که همان با همه ملک تو به تاراج رسید!</p></div>
<div class="m2"><p>حرمتت در حرم کعبه به حجاج رسید!!</p></div></div>
<div class="b2" id="bn11"><p>کار دخت تو در آن وهله به حراج رسید!!</p></div>
<div class="b" id="bn12"><div class="m1"><p>بر خلاف این چه خلافت بدو شد؟</p></div>
<div class="m2"><p>این چه طغیان خرافت بد و شد</p></div></div>