---
title: >-
    بخش ۶ - در قلعه خرابه
---
# بخش ۶ - در قلعه خرابه

<div class="b" id="bn1"><div class="m1"><p>برسیدم به یکی قلعه کهسان و کهن</p></div>
<div class="m2"><p>که در و بامش به هم ریخته، دامن دامن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زیر هر دامنه، غاری شده بگشوده دهن</p></div>
<div class="m2"><p>سر شب هر چه سخن گفته بد، آن پیر به من</p></div></div>
<div class="b2" id="bn3"><p>آن دهنها همه بنموده، به تصدیق سخن</p></div>
<div class="b" id="bn4"><div class="m1"><p>باری آن قلعه، حکایتها داشت</p></div>
<div class="m2"><p>ز آفت دهر، شکایتها داشت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چه سرائی؟ که سر و روش سراسر خاک است</p></div>
<div class="m2"><p>چه سرائی؟ که سرش همسر با افلاک است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چه سرائی؟ که حساب فلک آنجا پاک است</p></div>
<div class="m2"><p>بس که معظم بود، اما در و پیکر چاک است</p></div></div>
<div class="b2" id="bn7"><p>زین عیان است که تاریخ در آن غمناک است</p></div>
<div class="b" id="bn8"><div class="m1"><p>هیئتش تپه انبوهی بود</p></div>
<div class="m2"><p>روی هم رفته تو گو، کوهی بود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>یک بنائیش که از خاک، برون پیدا بود</p></div>
<div class="m2"><p>سطح بامش، سر یک دسته ستون پیدا بود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز آن ستونها، چه بسی راز درون پیدا بود</p></div>
<div class="m2"><p>هر ستونی چو یکی بیرق خون پیدا بود</p></div></div>
<div class="b2" id="bn11"><p>گو تو یک صفحه ز تاریخ قرون پیدا بود</p></div>
<div class="b" id="bn12"><div class="m1"><p>رفتم اندرش که تا جای کنم</p></div>
<div class="m2"><p>هم ز نزدیک تماشای کنم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>دیدم آن مهد بسی سلسله شاهان عجم</p></div>
<div class="m2"><p>بامش بس خورده لگد، طاقش برآورده شکم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بالش خسرو و آرامگه کله جم</p></div>
<div class="m2"><p>دست ایام فرو ریختشان بر سر هم</p></div></div>
<div class="b2" id="bn15"><p>ز آن میان حجره آکنده به آثار قدم</p></div>
<div class="b" id="bn16"><div class="m1"><p>وندر آن جایگه تاج عیان</p></div>
<div class="m2"><p>سر آن جایگه تاج کیان</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>جای پای عرب برهنه پائی دیدم</p></div>
<div class="m2"><p>نسبت تاج شه و پای عرب سنجیدم!</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>آنچه بایست بفهمم، ز جهان فهمیدم!</p></div>
<div class="m2"><p>بعد از آن هر چه که دیدم ز فلک خندیدم!</p></div></div>
<div class="b2" id="bn19"><p>باری اینگونه بنا هر چه که بدگردیدم</p></div>
<div class="b" id="bn20"><div class="m1"><p>خسته از گشتن، دیگر گشتم</p></div>
<div class="m2"><p>پای از قلعه به بیرون هشتم</p></div></div>