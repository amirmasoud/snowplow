---
title: >-
    عمر نرگس
---
# عمر نرگس

<div class="b" id="bn1"><div class="m1"><p>آتشین‌خوی مرا پاس دل من نیست نیست</p></div>
<div class="m2"><p>برق عالم‌سوز را پروای خرمن نیست نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مشت خاشاکی کجا بندد ره سیلاب را؟</p></div>
<div class="m2"><p>پایداری پیش اشکم کار دامن نیست نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آنقدر بنشین که برخیزد غبار از خاطرم</p></div>
<div class="m2"><p>پای تا سر ناز من هنگام رفتن نیست نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>قصه امواج دریا را ز دریادیده پرس</p></div>
<div class="m2"><p>هر دلی آگه ز طوفان دل من نیست نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همچو نرگس تا گشودم چشم پیوستم به خاک</p></div>
<div class="m2"><p>گل دو روزی بیشتر مهمان گلشن نیست نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ناگزیر از ناله‌ام در ماتم دل چون کنم؟</p></div>
<div class="m2"><p>مرهم داغ عزیزان غیر شیون نیست نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در پناه می ز عقل مصلحت بین فارغیم</p></div>
<div class="m2"><p>در کنار دوست بیم از طعن دشمن نیست نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بر دل پاکان نیفتد سایه آلودگی</p></div>
<div class="m2"><p>داغ ظلمت بر جبینم صبح روشن نیست نیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نیست در خاطر مرا اندیشه از گردون رهی</p></div>
<div class="m2"><p>رهرو آزاده را پروای رهزن نیست نیست</p></div></div>