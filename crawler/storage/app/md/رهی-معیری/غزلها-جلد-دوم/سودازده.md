---
title: >-
    سودازده
---
# سودازده

<div class="b" id="bn1"><div class="m1"><p>آن که سودازده چشم تو بوده است منم</p></div>
<div class="m2"><p>وآن که از هر مژه صد چشمه گشوده است منم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن ز ره مانده سرگشته که ناسازی بخت</p></div>
<div class="m2"><p>ره به سرمنزل وصلش ننموده است منم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آن که پیش لب شیرین تو ای چشمه نوش</p></div>
<div class="m2"><p>آفرین گفته و دشنام شنوده است منم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آن که خواب خوشم از دیده ربوده است تویی</p></div>
<div class="m2"><p>وآن که یک بوسه از آن لب نربوده است منم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای که از چشم رهی پای کشیدی چون اشک</p></div>
<div class="m2"><p>آن که چون آه به دنبال تو بوده است منم</p></div></div>