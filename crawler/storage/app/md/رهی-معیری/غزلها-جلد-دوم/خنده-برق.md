---
title: >-
    خنده برق
---
# خنده برق

<div class="b" id="bn1"><div class="m1"><p>سزای چون تو گلی گرچه نیست خانه ما</p></div>
<div class="m2"><p>بیا چو بوی گل امشب به آشیانه ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو ای ستاره خندان کجا خبر داری؟</p></div>
<div class="m2"><p>ز ناله سحر و گریه شبانه ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو بانگ رعد خروشان که پیچد اندر کوه</p></div>
<div class="m2"><p>جهان پر است ز گلبانگ عاشقانه ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نوای گرم نی از فیض آتشین‌نفسی است</p></div>
<div class="m2"><p>ز سوز سینه بود گرمی ترانه ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چنان ز خاطر اهل جهان فراموشیم</p></div>
<div class="m2"><p>که سیل نیز نگیرد سراغ خانه ما</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به خنده‌رویی دشمن مخور فریب رهی</p></div>
<div class="m2"><p>که برق خنده‌زنان سوخت آشیانه ما</p></div></div>