---
title: >-
    لبخند صبحدم
---
# لبخند صبحدم

<div class="b" id="bn1"><div class="m1"><p>گر شود آن روی روشن جلوه‌گر هنگام صبح</p></div>
<div class="m2"><p>پیش رخسارت کسی بر لب نیارد نام صبح</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از بناگوش تو و زلف توام آمد به یاد</p></div>
<div class="m2"><p>چون دمید از پرده شب روی سیمین‌فام صبح</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نیم‌شب با گریه مستانه حالی داشتم</p></div>
<div class="m2"><p>تلخ شد عیش من از لبخند بی‌هنگام صبح</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خواب را بدرود کن کز سیمگون ساغر دمید</p></div>
<div class="m2"><p>پرتو می چون فروغ آفتاب از جام صبح</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شست‌وشو در چشمه خورشید کرد از آن سبب</p></div>
<div class="m2"><p>نور هستی بخش می‌بارد ز هفت اندام صبح</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر ننوشیده است در خلوت نبید مشک بوی</p></div>
<div class="m2"><p>از چه آید هر نفس بوی بهشت از کام صبح ؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>می‌دود هرسو گریبان چاک از بی‌طاقتی</p></div>
<div class="m2"><p>تا کجا آرام گیرد جان بی‌آرام صبح ؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>معنی مرگ و حیات ای نفس کوته‌بین یکیست</p></div>
<div class="m2"><p>نیست فرقی بین آغاز شب و انجام صبح</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>این منم کز ناله و زاری نیاسایم دمی</p></div>
<div class="m2"><p>ورنه آرامش پذیرد مرغ شب هنگام صبح</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>جلوه من یک نفس چون صبح روشن بیش نیست</p></div>
<div class="m2"><p>در شکرخندی است فرجام من و فرجام صبح</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>عمر کوتاهم رهی در شام تنهایی گذشت</p></div>
<div class="m2"><p>مردم و نشنیدم از خورشیدرویی نام صبح</p></div></div>