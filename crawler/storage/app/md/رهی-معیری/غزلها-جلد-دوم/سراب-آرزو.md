---
title: >-
    سراب آرزو
---
# سراب آرزو

<div class="b" id="bn1"><div class="m1"><p>دل من ز تابناکی به شراب ناب ماند</p></div>
<div class="m2"><p>نکند سیاهکاری که به آفتاب ماند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نه ز پای می‌نشیند نه قرار می‌پذیرد</p></div>
<div class="m2"><p>دل آتشین من بین که به موج آب ماند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز شب سیه چه نالم؟ که فروغ صبح رویت</p></div>
<div class="m2"><p>به سپیده سحرگاه و به ماهتاب ماند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نفس حیات بخشت به هوای بامدادی</p></div>
<div class="m2"><p>لب مستی آفرینت به شراب ناب ماند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نه عجب اگر به عالم اثری نماند از ما</p></div>
<div class="m2"><p>که بر آسمان نبینی اثر از شهاب ماند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>رهی از امید باطل ره آرزو چه پویی؟</p></div>
<div class="m2"><p>که سراب زندگانی به خیال و خواب ماند</p></div></div>