---
title: >-
    باران صبحگاهی
---
# باران صبحگاهی

<div class="b" id="bn1"><div class="m1"><p>اشک سحر زداید از لوح دل سیاهی</p></div>
<div class="m2"><p>خرم کند چمن را باران صبحگاهی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عمری ز مهرت ای مه شب تا سحر نخفتم</p></div>
<div class="m2"><p>دعوی ز دیده من و ز اختران گواهی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون زلف و عارض او چشمی ندیده هرگز</p></div>
<div class="m2"><p>صبحی بدین سپیدی شامی بدان سیاهی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>داغم چو لاله ای گل از درد من چه پرسی؟</p></div>
<div class="m2"><p>مردم ز محنت ای غم از جان من چه خواهی؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای گریه در هلاکم هم عهد رنج و دردی</p></div>
<div class="m2"><p>وی ناله در عذابم هم‌راز اشک و آهی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چندین رهی چه نالی از داغ بی‌نصیبی؟</p></div>
<div class="m2"><p>در پای لاله‌رویان این بس که خاک راهی</p></div></div>