---
title: >-
    از خود رمیده
---
# از خود رمیده

<div class="b" id="bn1"><div class="m1"><p>چو گل ز دست تو جیب دریده‌ای دارم</p></div>
<div class="m2"><p>چو لاله دامن در خون کشیده‌ای دارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به حفظ جان بلا دیده سعی من بی‌جاست</p></div>
<div class="m2"><p>که پاس خرمن آفت رسیده‌ای دارم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز سردمهری آن گل چو برگ‌های خزان</p></div>
<div class="m2"><p>رخ شکسته و رنگ پریده‌ای دارم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نسیم عشق کجا بشکفد بهار مرا؟</p></div>
<div class="m2"><p>که همچو لاله دل داغ‌دیده‌ای دارم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مرا ز مردم نااهل چشم مردمی است</p></div>
<div class="m2"><p>امید میوه ز شاخ بریده‌ای دارم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کجاست عشق جگرسوز اضطراب‌انگیز؟</p></div>
<div class="m2"><p>که من به سینه دل آرمیده‌ای دارم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>صفا و گرمی جانم از آن بود که چو شمع</p></div>
<div class="m2"><p>شرار آهی و خوناب دیده‌ای دارم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مرا چگونه بود تاب آشنایی خلق؟</p></div>
<div class="m2"><p>که چون رهی دل از خود رمیده‌ای دارم</p></div></div>