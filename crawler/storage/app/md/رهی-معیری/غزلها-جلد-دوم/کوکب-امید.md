---
title: >-
    کوکب امید
---
# کوکب امید

<div class="b" id="bn1"><div class="m1"><p>ای صبح نودمیده! بناگوش کیستی؟</p></div>
<div class="m2"><p>وی چشمه حیات لب نوش کیستی؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از جلوهٔ تو سینه چو گل چاک شد مرا</p></div>
<div class="m2"><p>ای خرمن شکوفه! بر و دوش کیستی؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همچون هلال بهر تو آغوش من تهی است</p></div>
<div class="m2"><p>ای کوکب امید در آغوش کیستی؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مهر منیر را نبود جامهٔ سیاه</p></div>
<div class="m2"><p>ای آفتاب حسن سیه پوش کیستی؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>امشب کمند زلف ترا تاب دیگری است</p></div>
<div class="m2"><p>ای فتنه در کمین دل و هوش کیستی؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ما لاله‌سان ز داغ تو نوشیم خون دل</p></div>
<div class="m2"><p>تو همچو گل حریف قدح‌نوش کیستی؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای عندلیب گلشن شعر و ادب رهی</p></div>
<div class="m2"><p>نالان به یاد غنچه خاموش کیستی؟</p></div></div>