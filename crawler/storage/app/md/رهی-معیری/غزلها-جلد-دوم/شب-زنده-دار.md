---
title: >-
    شب‌زنده‌دار
---
# شب‌زنده‌دار

<div class="b" id="bn1"><div class="m1"><p>خاطر بی‌آرزو از رنج یار آسوده است</p></div>
<div class="m2"><p>خار خشک از منت ابر بهار آسوده است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر به دست عشق نسپاری عنان اختیار</p></div>
<div class="m2"><p>خاطرت از گریه بی‌اختیار آسوده است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هرزه‌گردان از هوای نفس خود سرگشته‌اند</p></div>
<div class="m2"><p>گر نخیزد باد غوغا گر غبار آسوده است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پای در دامن کشیدن فتنه از خود راندن است</p></div>
<div class="m2"><p>گر زمین را سیل گیرد کوهسار آسوده است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کج‌نهادی پیشه کن تا وارهی از دست خلق</p></div>
<div class="m2"><p>غنچه را صد گونه آسیب است و خار آسوده است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هرکه دارد شیوه نامردمی چون روزگار</p></div>
<div class="m2"><p>از جفای مردمان در روزگار آسوده است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا بود اشک روان از آتش غم باک نیست</p></div>
<div class="m2"><p>برق اگر سوزد چمن را جویبار آسوده است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شب سرآمد یک دم آخر دیده بر هم نه رهی</p></div>
<div class="m2"><p>صبحگاهان اختر شب‌زنده‌دار آسوده است</p></div></div>