---
title: >-
    اندام او
---
# اندام او

<div class="b" id="bn1"><div class="m1"><p>به مهر و ماه چه نسبت فرشته روی مرا؟</p></div>
<div class="m2"><p>سخن مگو که مرا نیست تاب گفت و شنید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کجا به نرمی اندام او بود مهتاب؟</p></div>
<div class="m2"><p>کجا به گرمی آغوش او بود خورشید؟</p></div></div>