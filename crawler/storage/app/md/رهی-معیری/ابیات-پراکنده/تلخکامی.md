---
title: >-
    تلخکامی
---
# تلخکامی

<div class="b" id="bn1"><div class="m1"><p>داغ حسرت سوخت جان آرزومند مرا</p></div>
<div class="m2"><p>آسمان با اشک غم آمیخت لبخند مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در هوای دوستداران دشمن خویشم رهی</p></div>
<div class="m2"><p>در همه عالم نخواهی یافت مانند مرا</p></div></div>