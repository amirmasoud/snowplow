---
title: >-
    فریب
---
# فریب

<div class="b" id="bn1"><div class="m1"><p>چاره من نمی‌کنی چون کنم و کجا برم؟</p></div>
<div class="m2"><p>شکوه بی‌نهایت و خاطر ناشکیب را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر به دروغ هم بود شیوه مهر ساز کن</p></div>
<div class="m2"><p>دیده عقل بسته‌ام کز تو خورم فریب را</p></div></div>