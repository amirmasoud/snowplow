---
title: >-
    نیش و نوش
---
# نیش و نوش

<div class="b" id="bn1"><div class="m1"><p>کس بهره از آن تازه بر و دوش ندارد</p></div>
<div class="m2"><p>کاین شاخه گل طاقت آغوش ندارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از عشق نرنجیم و گر مایه رنج است</p></div>
<div class="m2"><p>با نیش بسازیم اگر نوش ندارد</p></div></div>