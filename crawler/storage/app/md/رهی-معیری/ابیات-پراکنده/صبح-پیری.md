---
title: >-
    صبح پیری
---
# صبح پیری

<div class="b" id="bn1"><div class="m1"><p>تا بر آمد صبح پیری پایم از رفتار ماند</p></div>
<div class="m2"><p>کیست تا برگیرد و در سایه تاکم برد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ذره‌ام سودای وصل آفتابم در سر است</p></div>
<div class="m2"><p>بال همت می‌گشایم تا بر افلاکم برد</p></div></div>