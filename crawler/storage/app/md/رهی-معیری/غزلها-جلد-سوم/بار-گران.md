---
title: >-
    بار گران
---
# بار گران

<div class="b" id="bn1"><div class="m1"><p>زندگی بر دوش ما بار گرانی بیش نیست</p></div>
<div class="m2"><p>عمر جاویدان عذاب جاودانی بیش نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>لاله بزم‌آرای گلچین گشت و گل دمساز خار</p></div>
<div class="m2"><p>زین گلستان بهره بلبل فغانی بیش نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>می‌کند هر قطره اشکی ز داغی داستان</p></div>
<div class="m2"><p>گرچه شمعم شکوه دل را زبانی بیش نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آنچنان دور از لبش بگداختم کز تاب درد</p></div>
<div class="m2"><p>چون نی اندام نحیفم استخوانی بیش نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>من اسیرم در کف مهر و وفای خویشتن</p></div>
<div class="m2"><p>ورنه او سنگین دل نامهربانی بیش نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تکیه بر تاب و توان کم کن که در میدانِ عشق</p></div>
<div class="m2"><p>آن ز پا افتاده‌ای وین ناتوانی بیش نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>قوت بازو سلاح مرد باشد کآسمان</p></div>
<div class="m2"><p>آفت خلق است و در دستش کمانی بیش نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هر خس و خاری درین صحرا بهاری داشت لیک</p></div>
<div class="m2"><p>سربه‌سر دوران عمر ما خزانی بیش نیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ای گل از خون رهی پروا چه داری؟ کان ضعیف</p></div>
<div class="m2"><p>پر شکسته طایر بی‌آشیانی بیش نیست</p></div></div>