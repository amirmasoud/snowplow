---
title: >-
    بوسه نسیم
---
# بوسه نسیم

<div class="b" id="bn1"><div class="m1"><p>همراه خود نسیم صبا می‌برد مرا</p></div>
<div class="m2"><p>یا رب چو بوی گل به کجا می‌برد مرا؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سوی دیار صبح رود کاروان شب</p></div>
<div class="m2"><p>باد فنا به ملک بقا می‌برد مرا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با بال شوق ذره به خورشید می‌رسد</p></div>
<div class="m2"><p>پرواز دل به سوی خدا می‌برد مرا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفتم که بوی عشق که را می‌برد ز خویش؟</p></div>
<div class="m2"><p>مستانه گفت دل که مرا می‌برد مرا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>برگ خزان رسیده بی‌طاقتم رهی</p></div>
<div class="m2"><p>یک بوسه نسیم ز جا می‌برد مرا</p></div></div>