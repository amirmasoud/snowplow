---
title: >-
    آشیانهٔ تهی
---
# آشیانهٔ تهی

<div class="b" id="bn1"><div class="m1"><p>همچو مجنون گفتگو با خویشتن باید مرا</p></div>
<div class="m2"><p>بی‌زبانم هم‌زبانی همچو من باید مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا شوم روشنگر دل‌ها به آه آتشین</p></div>
<div class="m2"><p>گرم‌خویی‌های شمع انجمن باید مرا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رشک می‌آید مرا از جامه بر اندام تو</p></div>
<div class="m2"><p>با تو ای گل جای در یک پیرهن باید مرا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آشیان بی‌طایر دستانسرا ویرانه به</p></div>
<div class="m2"><p>چند با دل‌مردگی‌ها پاس تن باید مرا؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا ز خاطر کوه محنت را براندازم رهی</p></div>
<div class="m2"><p>همت مردانه‌ای چون کوهکن باید مرا</p></div></div>