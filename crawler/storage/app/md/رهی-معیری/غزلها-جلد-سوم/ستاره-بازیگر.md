---
title: >-
    ستاره بازیگر
---
# ستاره بازیگر

<div class="b" id="bn1"><div class="m1"><p>تا گریزان گشتی ای نیلوفری‌چشم از برم</p></div>
<div class="m2"><p>در غمت از لاغری چون شاخه نیلوفرم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا گرفتی از حریفان جام سیمین چون هلال</p></div>
<div class="m2"><p>چون شفق خونابهٔ دل می‌چکد از ساغرم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خفته‌ام امشب ولی جای من دل‌سوخته</p></div>
<div class="m2"><p>صبحدم بینی که خیزد دود آه از بسترم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تار و پود هستی‌ام بر باد رفت اما نرفت</p></div>
<div class="m2"><p>عاشقی‌ها از دلم، دیوانگی‌ها از سرم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شمع لرزان نیستم تا ماند از من اشک سرد</p></div>
<div class="m2"><p>آتشی جاوید باشد در دل خاکسترم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سرکشی آموخت بخت از یار یا آموخت یار</p></div>
<div class="m2"><p>شیوه بازیگری از طالع بازیگرم؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خاطرم را الفتی با اهل عالم نیست نیست</p></div>
<div class="m2"><p>کز جهانی دیگرند و از جهانی دیگرم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گرچه ما را کار دل محروم از دنیا کند</p></div>
<div class="m2"><p>نگذرم از کار دل وز کار دنیا بگذرم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شعر من رنگ شب و آهنگ غم دارد رهی</p></div>
<div class="m2"><p>زآنکه دارد نسبتی با خاطر غم‌پرورم</p></div></div>