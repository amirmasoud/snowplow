---
title: >-
    صفای شبنم
---
# صفای شبنم

<div class="b" id="bn1"><div class="m1"><p>او را به رنگ و بوی نگویم نظیر نیست</p></div>
<div class="m2"><p>گلبن نظیر اوست ولی دل‌پذیر نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ما را نسیم کوی تو از خاک بر گرفت</p></div>
<div class="m2"><p>خاشاک را به غیر صبا دستگیر نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گلبانگ نی اگر چه بود دل‌نشین ولی</p></div>
<div class="m2"><p>آتش اثر چو ناله مرغ اسیر نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>غافل مشو ز عمر که ساکن نمی‌شود</p></div>
<div class="m2"><p>سیل عنان‌گسسته اقامت‌پذیر نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>روی نکو به طینت ساقی نمی‌رسد</p></div>
<div class="m2"><p>گل را صفای شبنم روشن‌ضمیر نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>با عمر ساختیم ز دل‌مردگی رهی</p></div>
<div class="m2"><p>ماتم‌رسیده را ز تحمل گزیر نیست</p></div></div>