---
title: >-
    قطعهٔ ۹ - رازداری
---
# قطعهٔ ۹ - رازداری

<div class="b" id="bn1"><div class="m1"><p>خویشتن‌داری و خموشی را</p></div>
<div class="m2"><p>هوشمندان حصار جان دانند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر زیان بینی از بیان بینی</p></div>
<div class="m2"><p>ور زبون گردی از زبان دانند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>راز دل پیش دوستان مگشای</p></div>
<div class="m2"><p>گر نخواهی که دشمنان دانند</p></div></div>