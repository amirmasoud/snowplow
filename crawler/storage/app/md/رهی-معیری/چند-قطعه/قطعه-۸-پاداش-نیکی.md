---
title: >-
    قطعهٔ ۸ - پاداش نیکی
---
# قطعهٔ ۸ - پاداش نیکی

<div class="b" id="bn1"><div class="m1"><p>من نگویم ترک آیین مروت کن ولی</p></div>
<div class="m2"><p>این فضیلت با تو خلق سفله را دشمن کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تار و پودش را ز کین‌توزی همی‌خواهند سوخت</p></div>
<div class="m2"><p>هرکه همچون شمع بزم دیگران روشن کند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفت با صاحبدلی مردی که بهمان در نهفت</p></div>
<div class="m2"><p>قصد دارد تا به تیغت سر جدا از تن کند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نیک‌مردش گفت باور نایدم این گفته ز آنک</p></div>
<div class="m2"><p>من به او نیکی نکردم تا بدی با من کند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>می‌کنند از دشمنی نادوستان با دوستان</p></div>
<div class="m2"><p>آنچه آتش با گیاه و برق با خرمن کند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دور شو زین مردم نااهل دور از مردمی</p></div>
<div class="m2"><p>دیو گردد هرکه آمیزش به اهریمن کند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>منزلت خواهی مکان در کنج تنهایی گزین</p></div>
<div class="m2"><p>گنج گوهر بین که در ویرانه‌ها مسکن کند</p></div></div>