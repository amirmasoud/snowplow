---
title: >-
    قطعهٔ ۳۲ - دل من
---
# قطعهٔ ۳۲ - دل من

<div class="b" id="bn1"><div class="m1"><p>درون کلبه تنگی شبانگاه</p></div>
<div class="m2"><p>ز آتشدان، به هرسو شعله خیزد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در آتش چوب تر همچون دل من</p></div>
<div class="m2"><p>«سری سوزد، سری خونابه ریزد»</p></div></div>
<div class="b2" id="bn3"><p>سراید ساز، از سوز جدایی</p>
<p>به گوشم نغمه‌های آشنایی</p></div>
<div class="b" id="bn4"><div class="m1"><p>ز برف بهمنی پوشیده هامون</p></div>
<div class="m2"><p>پرند سیمگون بر پیکر خویش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>من از سیمینه هامون باز یابم</p></div>
<div class="m2"><p>نشان دلبر سیمین‌بر خویش</p></div></div>
<div class="b2" id="bn6"><p>صبا در گوش من نام تو گوید</p>
<p>نسیم آهسته پیغام تو گوید</p></div>
<div class="b" id="bn7"><div class="m1"><p>کجایی؟ کز نوای آتشینم</p></div>
<div class="m2"><p>دلت در سینه گردد آتش‌انگیز</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>میان برف و یخ در آتشستم</p></div>
<div class="m2"><p>به برف اندر شگفت است آتش تیز</p></div></div>
<div class="b2" id="bn9"><p>جهان در دیده من محو و تاریک</p>
<p>تو از من دور و من با مرگ نزدیک</p></div>
<div class="b" id="bn10"><div class="m1"><p>برآر ای ساز، آوازی که گردون</p></div>
<div class="m2"><p>طریق سازگاری پیش گیرد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>فراخوان بخت ره گم کرده‌ام را</p></div>
<div class="m2"><p>که راه آشیان خویش گیرد</p></div></div>
<div class="b2" id="bn12"><p>به سردی گر فلک بیداد کیش است</p>
<p>دل من، گرم از سودای خویش است</p></div>