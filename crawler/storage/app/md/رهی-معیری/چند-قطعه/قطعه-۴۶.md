---
title: >-
    قطعهٔ ۴۶
---
# قطعهٔ ۴۶

<div class="b" id="bn1"><div class="m1"><p>حمید، تازه‌جوان و شکفته‌رو شده است</p></div>
<div class="m2"><p>مگر به چشمه حیوان تنش فرو شده است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سرشک بیوه‌زنان سهم ساعد است ولی</p></div>
<div class="m2"><p>گشایش پل دختر نصیب او شده است</p></div></div>