---
title: >-
    قطعهٔ ۶ - موی سپید
---
# قطعهٔ ۶ - موی سپید

<div class="b" id="bn1"><div class="m1"><p>رهی به گونه چون لاله برگ غره مباش</p></div>
<div class="m2"><p>که روزگارش چون شنبلید گرداند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گرت به فر جوانی امیدواری‌هاست</p></div>
<div class="m2"><p>جهان پیر ترا ناامید گرداند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر از دمیدن موی سپید بر سر خلق</p></div>
<div class="m2"><p>زمانه آیت پیری پدید گرداند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دریغ و درد که مویی نماند بر سر من</p></div>
<div class="m2"><p>که روزگار به پیری سپید گرداند</p></div></div>