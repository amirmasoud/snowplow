---
title: >-
    قطعهٔ ۱۴ - پاس ادب
---
# قطعهٔ ۱۴ - پاس ادب

<div class="b" id="bn1"><div class="m1"><p>پاس ادب به حد کفایت نگاه دار</p></div>
<div class="m2"><p>خواهی اگر ز بی‌ادبان یابی ایمنی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با کم ز خویش هرکه نشیند به دوستی</p></div>
<div class="m2"><p>با عز و حرمت خود خیزد به دشمنی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در خون نشست غنچه که شد هم‌نشین خار</p></div>
<div class="m2"><p>گردن فراخت سرو ز بر چیده دامنی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>افتاده باش لیک نه چندان که همچو خاک</p></div>
<div class="m2"><p>پامال هر نبهره شوی از فروتنی</p></div></div>