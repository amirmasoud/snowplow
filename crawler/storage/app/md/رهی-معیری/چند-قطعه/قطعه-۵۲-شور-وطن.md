---
title: >-
    قطعهٔ ۵۲ - شور وطن
---
# قطعهٔ ۵۲ - شور وطن

<div class="b" id="bn1"><div class="m1"><p>هرکه را بر سر ز سودای وطن افسر بود</p></div>
<div class="m2"><p>هرکجا باشد تنی اهل وطن را سر بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هرکه از میهن سخن گوید کلامش دلرباست</p></div>
<div class="m2"><p>نغمه‌های بلبل این باغ رنگین‌تر بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هرکه از نام وطن دارد کلام او نشان</p></div>
<div class="m2"><p>نامش آخر زینت اوراق هر دفتر بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هرکه بهر زیب و زیور رو نتابد از وطن</p></div>
<div class="m2"><p>چهره مام وطن را زینت و زیور بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آنکه از راه خیانت سرور جمعی شده است</p></div>
<div class="m2"><p>زان بود ارباب، کان ارباب را نوکر بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آنکه در هر کار می‌رقصد به ساز اجنبی</p></div>
<div class="m2"><p>تازه گر شیرین برقصد لنگه عنتر بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مهر میهن، پرتو مردانگی، عزمی قوی</p></div>
<div class="m2"><p>این سه‌تا تنها دوای درد این کشور بود</p></div></div>