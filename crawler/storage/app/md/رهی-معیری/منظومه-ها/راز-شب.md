---
title: >-
    راز شب
---
# راز شب

<div class="b" id="bn1"><div class="m1"><p>شب چو بوسیدم لب گلگون او</p></div>
<div class="m2"><p>گشت لرزان قامت موزون او</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زیر گیسو کرد پنهان روی خویش</p></div>
<div class="m2"><p>ماه را پوشید با گیسوی خویش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفتمش: ای روی تو صبح امید</p></div>
<div class="m2"><p>در دل شب بوسه ما را که دید؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>قصه‌پردازی در این صحرا نبود</p></div>
<div class="m2"><p>چشم غمازی به سوی ما نبود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>غنچهٔ خاموش او چون گل شکفت</p></div>
<div class="m2"><p>بر من از حیرت نگاهی کرد و گفت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>با خبر از راز ما گردید شب</p></div>
<div class="m2"><p>بوسه‌ای دادیم و آن را دید شب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بوسه را شب دید و با مهتاب گفت</p></div>
<div class="m2"><p>ماه خندید و به موج آب گفت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>موج دریا جانب پارو شتافت</p></div>
<div class="m2"><p>راز ما گفت و به دیگر سو شتافت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>قصه را پارو به قایق باز گفت</p></div>
<div class="m2"><p>داستان دلکشی ز آن راز گفت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گفت قایق هم به قایقبان خویش</p></div>
<div class="m2"><p>آنچه را بشنید از یاران خویش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مانده بود این راز اگر در پیش او</p></div>
<div class="m2"><p>دل نبود آشفته از تشویش او</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>لیک درد اینجاست کان ناپخته مرد</p></div>
<div class="m2"><p>با زنی آن راز را ابراز کرد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گفت با زن مرد غافل راز را</p></div>
<div class="m2"><p>آن تهی‌طبل بلندآواز را</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>لاجرم فردا از آن راز نهفت</p></div>
<div class="m2"><p>قصه‌گویان قصه‌ها خواهند گفت</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>زن به غمازی دهان وا می‌کند</p></div>
<div class="m2"><p>راز را چون روز افشا می‌کند</p></div></div>