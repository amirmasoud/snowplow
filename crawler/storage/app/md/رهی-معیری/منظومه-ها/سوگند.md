---
title: >-
    سوگند
---
# سوگند

<div class="b" id="bn1"><div class="m1"><p>لاله‌رویی بر گل سرخی نگاشت</p></div>
<div class="m2"><p>کز سیه‌چشمان نگیرم دلبری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از لب من کس نیابد بوسه‌ای</p></div>
<div class="m2"><p>وز کف من کس ننوشد ساغری</p></div></div>
<div class="b2" id="bn3"><p>تا نیفتد پایش اندر بندها</p>
<p>یاد کرد آن تازه گل سوگندها</p></div>
<div class="b" id="bn4"><div class="m1"><p>ناگهان باد صبا دامن‌کشان</p></div>
<div class="m2"><p>سوی سرو و لاله و شمشاد رفت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فارغ از پیمان نگشته نازنین</p></div>
<div class="m2"><p>کز نسیمی برگ گل بر باد رفت</p></div></div>
<div class="b2" id="bn6"><p>خنده زد گل بر رخ دلبند او</p>
<p>کآن چنان بر باد شد سوگند او</p></div>