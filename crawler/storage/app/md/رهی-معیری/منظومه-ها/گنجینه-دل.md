---
title: >-
    گنجینهٔ دل
---
# گنجینهٔ دل

<div class="b" id="bn1"><div class="m1"><p>چشم فروبسته اگر وا کنی</p></div>
<div class="m2"><p>در تو بود هرچه تمنا کنی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عافیت از غیر نصیب تو نیست</p></div>
<div class="m2"><p>غیر تو ای خسته طبیب تو نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از تو بود راحت بیمار تو</p></div>
<div class="m2"><p>نیست به غیر از تو پرستار تو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همدم خود شو که حبیب خودی</p></div>
<div class="m2"><p>چارهٔ خود کن که طبیب خودی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>غیر که غافل ز دل زار توست</p></div>
<div class="m2"><p>بی‌خبر از مصلحت کار توست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر حذر از مصلحت‌اندیش باش</p></div>
<div class="m2"><p>مصلحت‌اندیش دل خویش باش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چشم بصیرت نگشایی چرا؟</p></div>
<div class="m2"><p>بی‌خبر از خویش چرایی؟ چرا؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>صید که درمانده ز هرسو شده‌ست</p></div>
<div class="m2"><p>غفلت او دام ره او شده‌ست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تا ره غفلت سپرد پای تو</p></div>
<div class="m2"><p>دام بود جای تو ای وای تو</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خواجهٔ مقبل که ز خود غافلی</p></div>
<div class="m2"><p>خواجه نه‌ای بندهٔ نامقبلی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از ره غفلت به گدایی رسی</p></div>
<div class="m2"><p>ور به خود آیی به خدایی رسی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>پیر تهی کیسهٔ بی‌خانه‌ای</p></div>
<div class="m2"><p>داشت مکان در دل ویرانه‌ای</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>روز به دریوزگی از بخت شوم</p></div>
<div class="m2"><p>شام به ویرانه درون همچو بوم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گنج زری بود در آن خاکدان</p></div>
<div class="m2"><p>چون پری از دیدهٔ مردم نهان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>پای گدا بر سر آن گنج بود</p></div>
<div class="m2"><p>لیک ز غفلت به غم و رنج بود</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گنج‌صفت خانه به ویرانه داشت</p></div>
<div class="m2"><p>غافل از آن گنج که در خانه داشت</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>عاقبت از فاقه و اندوه و رنج</p></div>
<div class="m2"><p>مرد گدا مرد و نهان ماند گنج</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ای شده نالان ز غم و رنج خویش</p></div>
<div class="m2"><p>چند نداری خبر از گنج خویش؟</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>گنج تو باشد دل آگاه تو</p></div>
<div class="m2"><p>گوهر تو اشک سحرگاه تو</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>مایهٔ امید مدان غیر را</p></div>
<div class="m2"><p>کعبهٔ حاجات مخوان دیر را</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>غیر ز دلخواه تو آگاه نیست</p></div>
<div class="m2"><p>زآن که دلی را به دلی راه نیست</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>خواهش مرهم ز دل ریش کن</p></div>
<div class="m2"><p>هرچه طلب می‌کنی از خویش کن</p></div></div>