---
title: >-
    ساز محجوبی
---
# ساز محجوبی

<div class="b" id="bn1"><div class="m1"><p>آنکه جانم شد نواپرداز او</p></div>
<div class="m2"><p>می‌سرایم قصه‌ای از ساز او</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ساز او در پرده گوید رازها</p></div>
<div class="m2"><p>سر کند در گوش جان آوازها</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بانگی از آوای بلبل گرم‌تر</p></div>
<div class="m2"><p>وز نوای جویباران نرم‌تر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نغمهٔ مرغ چمن جان‌پرور است</p></div>
<div class="m2"><p>لیک دراین ساز سوزی دیگر است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آنچه آتش با نیستان می‌کند</p></div>
<div class="m2"><p>ناله او با دلم آن می‌کند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خسته دل داند بهای ناله را</p></div>
<div class="m2"><p>شمع داند قدر داغ لاله را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر دلی از سوز ما آگاه نیست</p></div>
<div class="m2"><p>غیر را در خلوت ما راه نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دیگران دل بسته جان و سرند</p></div>
<div class="m2"><p>مردم عاشق گروهی دیگرند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شرح این معنی ز من باید شنید</p></div>
<div class="m2"><p>راز عشق از کوهکن باید شنید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>حال بلبل از دل پروانه پرس</p></div>
<div class="m2"><p>قصه دیوانه از دیوانه پرس</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>من شناسم آه آتشناک را</p></div>
<div class="m2"><p>بانگ مستان گریبان‌چاک را</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چیستم من؟ آتشی افروخته</p></div>
<div class="m2"><p>لاله‌ای از داغ حسرت سوخته</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>شمع را در سینه سوز من مباد</p></div>
<div class="m2"><p>در محبت کس به روز من مباد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>سودم از سودای دل جز درد نیست</p></div>
<div class="m2"><p>غیر اشک گرم و آه سرد نیست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>خسته از پیکان محرومی پرم</p></div>
<div class="m2"><p>مانده بر زانوی خاموشی سرم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>عمر کوتاهم چو گل بر باد رفت</p></div>
<div class="m2"><p>نغمه شادی مرا از یاد رفت</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>گرچه غم در سینه خاکم برد</p></div>
<div class="m2"><p>ساز محجوبی بر افلاکم برد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>شعله‌ای چون وی جهان‌افروز نیست</p></div>
<div class="m2"><p>مرتضی از مردم امروز نیست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>جان من با جان او پیوسته است</p></div>
<div class="m2"><p>زآنکه چون من از دو عالم رسته است</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ما دوتن در عاشقی پاینده‌ایم</p></div>
<div class="m2"><p>همچو شمع از آتش دل زنده‌ایم</p></div></div>