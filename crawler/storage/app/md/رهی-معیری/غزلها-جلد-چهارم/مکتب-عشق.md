---
title: >-
    مکتب عشق
---
# مکتب عشق

<div class="b" id="bn1"><div class="m1"><p>هرشب فزاید تاب و تب من</p></div>
<div class="m2"><p>وای از شب من وای از شب من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یا من رسانم لب بر لب او</p></div>
<div class="m2"><p>یا او رساند جان بر لب من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>استاد عشقم بنشین و برخوان</p></div>
<div class="m2"><p>درس محبت در مکتب من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رسم دورنگی آیین ما نیست</p></div>
<div class="m2"><p>یکرنگ باشد روز و شب من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفتم رهی را کامشب چه خواهی؟</p></div>
<div class="m2"><p>گفت آنچه خواهد نوشین‌لب من</p></div></div>