---
title: >-
    حاصل عمر
---
# حاصل عمر

<div class="b" id="bn1"><div class="m1"><p>بس که جفا ز خار و گل دید دل رمیده‌ام</p></div>
<div class="m2"><p>همچو نسیم از این چمن پای برون کشیده‌ام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شمع طرب ز بخت ما آتش خانه‌سوز شد</p></div>
<div class="m2"><p>گشت بلای جان من عشق به جان خریده‌ام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حاصل دور زندگی صحبت آشنا بود</p></div>
<div class="m2"><p>تا تو ز من بریده‌ای من ز جهان بریده‌ام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا به کنار بودیَم بود به جا قرار دل</p></div>
<div class="m2"><p>رفتی و رفت راحت از خاطر آرمیده‌ام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا تو مراد من دهی کشته مرا فراق تو</p></div>
<div class="m2"><p>تا تو به داد من رسی من به خدا رسیده‌ام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون به بهار سر کند لاله ز خاک من برون</p></div>
<div class="m2"><p>ای گل تازه یاد کن از دل داغ دیده‌ام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یا ز ره وفا بیا یا ز دل رهی برو</p></div>
<div class="m2"><p>سوخت در انتظار تو جان به لب رسیده‌ام</p></div></div>