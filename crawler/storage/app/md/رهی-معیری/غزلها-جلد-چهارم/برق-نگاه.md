---
title: >-
    برق نگاه
---
# برق نگاه

<div class="b" id="bn1"><div class="m1"><p>به روی سیل گشادیم راه خانهٔ خویش</p></div>
<div class="m2"><p>به دست برق سپردیم آشیانهٔ خویش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مرا چه حد که زنم بوسه آستین ترا</p></div>
<div class="m2"><p>همین قدر تو مرانم ز آستانهٔ خویش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به جز تو کز نگهی سوختی دل ما را</p></div>
<div class="m2"><p>به دست خویش که آتش زند به خانهٔ خویش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مخوان حدیث رهایی که الفتی است مرا</p></div>
<div class="m2"><p>به ناله سحر و گریه شبانهٔ خویش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز رشک تا که هلاکم کند به دامن غیر</p></div>
<div class="m2"><p>چو گل نهد سر و مستی کند بهانهٔ خویش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>رهی به ناله دهی چند دردسر ما را؟</p></div>
<div class="m2"><p>بمیر از غم و کوتاه کن فسانهٔ خویش</p></div></div>