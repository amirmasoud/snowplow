---
title: >-
    سرگشته
---
# سرگشته

<div class="b" id="bn1"><div class="m1"><p>بی‌روی تو راحت ز دل زار گریزد</p></div>
<div class="m2"><p>چون خواب که از دیده بیمار گریزد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در دام تو یک شب دلم از ناله نیاسود</p></div>
<div class="m2"><p>آسودگی از مرغ گرفتار گریزد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از دشمن و از دوست گریزیم و عجب نیست</p></div>
<div class="m2"><p>سرگشته نسیم از گل و از خار گریزد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شب تا سحر از ناله دل خواب ندارم</p></div>
<div class="m2"><p>راحت به شب از چشم پرستار گریزد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای دوست بیازار مرا هرچه توانی</p></div>
<div class="m2"><p>دل نیست اسیری که ز آزار گریزد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زین بیش رهی ناله مکن در بر آن شوخ</p></div>
<div class="m2"><p>ترسم که ز نالیدن بسیار گریزد</p></div></div>