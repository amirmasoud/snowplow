---
title: >-
    بوسه جام
---
# بوسه جام

<div class="b" id="bn1"><div class="m1"><p>تو سوز آه من ای مرغ شب چه می‌دانی؟</p></div>
<div class="m2"><p>ندیده‌ای شب من تاب و تب چه می‌دانی؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به من گذار که لب بر لبش نهم ای جام</p></div>
<div class="m2"><p>تو قدر بوسه آن نوش لب چه می‌دانی؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو شمع و گل شب و روزت به خنده می‌گذرد</p></div>
<div class="m2"><p>تو گریه سحر و آه شب چه می‌دانی؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بلای هجر ز هر درد جانگدازتر است</p></div>
<div class="m2"><p>ندیده داغ جدایی تعب چه می‌دانی؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رهی به محفل عشرت به نغمه لب مگشای</p></div>
<div class="m2"><p>تو دل‌شکسته نوای طرب چه می‌دانی؟</p></div></div>