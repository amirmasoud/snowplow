---
title: >-
    ناله جویبار
---
# ناله جویبار

<div class="b" id="bn1"><div class="m1"><p>گرچه روزی تیره‌تر از شام غم باشد مرا</p></div>
<div class="m2"><p>در دل روشن صفای صبحدم باشد مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زرپرستی خواب راحت را ز نرگس دور کرد</p></div>
<div class="m2"><p>صرف عشرت می‌کنم گر یک درم باشد مرا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خواهش دل هرچه کمتر شادی جان بیشتر</p></div>
<div class="m2"><p>تا دلی بی‌آرزو باشد چه غم باشد مرا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در کنار من ز گرمی بر کناری ای دریغ</p></div>
<div class="m2"><p>وصل و هجران غم و شادی به هم باشد مرا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در خروش آیم چو بینم کج‌نهادی‌های خلق</p></div>
<div class="m2"><p>جویبارم ناله از هر پیچ و خم باشد مرا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گرچه در کارم چو انجم عقده‌ها باشد رهی</p></div>
<div class="m2"><p>چهره بگشاده‌ای چون صبحدم باشد مرا</p></div></div>