---
title: >-
    گلبانگ رود
---
# گلبانگ رود

<div class="b" id="bn1"><div class="m1"><p>نوای آسمانی آید از گلبانگ رود امشب</p></div>
<div class="m2"><p>بیا ساقی که رفت از دل غم بود و نبود امشب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فراز چرخ نیلی ناله مستانه‌ای دارد</p></div>
<div class="m2"><p>دل از بام فلک دیگر نمی‌آید فرود امشب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>که بود آن آهوی وحشی چه بود آن سایه مژگان؟</p></div>
<div class="m2"><p>که تاب از من ستاند امروز و خواب از من ربود امشب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به یاد غنچه خاموش او سر در گریبانم</p></div>
<div class="m2"><p>ندارم با نسیم گل سر گفت و شنود امشب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز بس بر تربت صائب عنان گریه سر دادم</p></div>
<div class="m2"><p>رهی از چشمهٔ چشمم خجل شد زنده‌رود امشب</p></div></div>