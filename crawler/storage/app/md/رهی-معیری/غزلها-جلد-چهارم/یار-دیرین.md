---
title: >-
    یار دیرین
---
# یار دیرین

<div class="b" id="bn1"><div class="m1"><p>به سوی ما گذار مردم دنیا نمی‌افتد</p></div>
<div class="m2"><p>کسی غیر از غم دیرین به یاد ما نمی‌افتد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>منم مرغی که جز در خلوت شب‌ها نمی‌نالد</p></div>
<div class="m2"><p>منم اشکی که جز بر خرمن دل‌ها نمی‌افتد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز بس چون غنچه از پاس حیا سر در گریبانم</p></div>
<div class="m2"><p>نگاه من به چشم آن سهی بالا نمی‌افتد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به پای گلبنی جان داده‌ام اما نمی‌دانم</p></div>
<div class="m2"><p>که می‌افتد به خاکم سایهٔ گل یا نمی‌افتد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رود هر ذرهٔ خاکم به دنبال پری‌رویی</p></div>
<div class="m2"><p>غبار من به صحرای طلب از پا نمی‌افتد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مراد آسان به دست آید ولی نوشین لبی جز او</p></div>
<div class="m2"><p>پسند خاطر مشکل پسند ما نمی‌افتد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تو هم با سروبالایی سری داری و سودایی</p></div>
<div class="m2"><p>کمند آرزو برجان من تنها نمی‌افتد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نصیب ساغر می شد لب جانانه بوسیدن</p></div>
<div class="m2"><p>رهی دامان این دولت به دست ما نمی‌افتد</p></div></div>