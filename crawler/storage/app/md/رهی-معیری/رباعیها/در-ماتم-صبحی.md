---
title: >-
    در ماتم صبحی
---
# در ماتم صبحی

<div class="b" id="bn1"><div class="m1"><p>دردا که بهار عیش ما آخر شد</p></div>
<div class="m2"><p>دوران گل از باد فنا آخر شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شب طی شد و رفت صبحی از محفل ما</p></div>
<div class="m2"><p>افسانه افسانه‌سرا آخر شد</p></div></div>