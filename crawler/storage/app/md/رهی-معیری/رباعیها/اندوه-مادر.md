---
title: >-
    اندوه مادر
---
# اندوه مادر

<div class="b" id="bn1"><div class="m1"><p>آسودگی از محن ندارد مادر</p></div>
<div class="m2"><p>آسایش جان و تن ندارد مادر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دارد غم و اندوه جگرگوشه خویش</p></div>
<div class="m2"><p>ورنه غم خویشتن ندارد مادر</p></div></div>