---
title: >-
    مردم چشم
---
# مردم چشم

<div class="b" id="bn1"><div class="m1"><p>بی‌روی تو گشت لاله‌گون مردم چشم</p></div>
<div class="m2"><p>بنشست ز دوریت به خون مردم چشم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>افتادی اگر ز چشم مردم چون اشک</p></div>
<div class="m2"><p>در چشم منی عزیز چون مردم چشم</p></div></div>