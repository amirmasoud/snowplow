---
title: >-
    بیدادگری
---
# بیدادگری

<div class="b" id="bn1"><div class="m1"><p>از ظلم حذر کن اگرت باید ملک</p></div>
<div class="m2"><p>در سایهٔ معدلت بیاساید ملک</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با کفر توان ملک نگه داشت ولی</p></div>
<div class="m2"><p>با ظلم و ستمگری نمی‌پاید ملک</p></div></div>