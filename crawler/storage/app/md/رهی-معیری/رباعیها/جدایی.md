---
title: >-
    جدایی
---
# جدایی

<div class="b" id="bn1"><div class="m1"><p>ای بی‌خبر از محنت روزافزونم</p></div>
<div class="m2"><p>دانم که ندانی از جدایی چونم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>باز آی که سرگشته‌تر از فرهادم</p></div>
<div class="m2"><p>دریاب که دیوانه‌تر از مجنونم</p></div></div>