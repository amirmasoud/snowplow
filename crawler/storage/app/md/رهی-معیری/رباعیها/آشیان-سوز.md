---
title: >-
    آشیان‌سوز
---
# آشیان‌سوز

<div class="b" id="bn1"><div class="m1"><p>ای جلوهٔ برق آشیان‌سوز تو را</p></div>
<div class="m2"><p>ای روشنی شمع شب‌افروز تو را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زآن روز که دیدمت شبی خوابم نیست</p></div>
<div class="m2"><p>ای کاش ندیده بودم آن روز تو را</p></div></div>