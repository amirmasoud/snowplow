---
title: >-
    آتش خاموش
---
# آتش خاموش

<div class="b" id="bn1"><div class="m1"><p>نه دل مفتون دلبندی نه جان مدهوش دلخواهی</p></div>
<div class="m2"><p>نه بر مژگان من اشکی نه بر لب‌های من آهی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نه جان بی‌نصیبم را پیامی از دلارامی</p></div>
<div class="m2"><p>نه شام بی‌فروغم را نشانی از سحرگاهی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نیابد محفلم گرمی نه از شمعی نه از جمعی</p></div>
<div class="m2"><p>ندارد خاطرم الفت نه با مهری نه با ماهی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به دیدار اجل باشد اگر شادی کنم روزی</p></div>
<div class="m2"><p>به بخت واژگون باشد اگر خندان شوم گاهی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کیم من؟ آرزو گم کرده‌ای تنها و سرگردان</p></div>
<div class="m2"><p>نه آرامی نه امیدی نه همدردی نه همراهی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گهی افتان و خیزان چون غباری در بیابانی</p></div>
<div class="m2"><p>گهی خاموش و حیران چون نگاهی بر نظرگاهی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>رهی تا چند سوزم در دل شب‌ها چو کوکب‌ها</p></div>
<div class="m2"><p>به اقبال شرر نازم که دارد عمر کوتاهی</p></div></div>