---
title: >-
    بهشت آرزو
---
# بهشت آرزو

<div class="b" id="bn1"><div class="m1"><p>بر جگر داغی ز عشق لاله‌رویی یافتم</p></div>
<div class="m2"><p>در سرای دل بهشت آرزویی یافتم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عمری از سنگ حوادث سوده گشتم چون غبار</p></div>
<div class="m2"><p>تا به امداد نسیمی ره به کویی یافتم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خاطر از آیینه صبح است روشن‌تر مرا</p></div>
<div class="m2"><p>این صفا از صحبت پاکیزه‌رویی یافتم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گرمی شمع شب‌افروز آفت پروانه شد</p></div>
<div class="m2"><p>سوخت جانم تا حریف گرم‌خویی یافتم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بی‌تلاش من غم عشق توام در دل نشست</p></div>
<div class="m2"><p>گنج را در زیر پا بی‌جست‌و‌جویی یافتم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تلخ‌کامی بین که در میخانه دلدادگی</p></div>
<div class="m2"><p>بود پر خون جگر هرجا سبویی یافتم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون صبا در زیر زلفش هرکجا کردم گذار</p></div>
<div class="m2"><p>یک جهان دل‌بسته بر هر تار مویی یافتم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ننگ رسوایی رهی نامم بلندآوازه کرد</p></div>
<div class="m2"><p>خاک راه عشق گشتم آبرویی یافتم</p></div></div>