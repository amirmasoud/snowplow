---
title: >-
    زندان خاک
---
# زندان خاک

<div class="b" id="bn1"><div class="m1"><p>با دل روشن در این ظلمت‌سرا افتاده‌ام</p></div>
<div class="m2"><p>نور مهتابم که در ویرانه‌ها افتاده‌ام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سایه پرورد بهشتم از چه گشتم صید خاک ؟</p></div>
<div class="m2"><p>تیره‌بختی بین کجا بودم کجا افتاده‌ام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جای در بستان‌سرای عشق می‌باید مرا</p></div>
<div class="m2"><p>عندلیبم از چه در ماتم‌سرا افتاده‌ام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پایمال مردمم از نارسایی‌های بخت</p></div>
<div class="m2"><p>سبزهٔ بی‌طالعم در زیر پا افتاده‌ام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خار ناچیزم مرا در بوستان مقدار نیست</p></div>
<div class="m2"><p>اشک بی‌قدرم ز چشم آشنا افتاده‌ام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا کجا راحت پذیرم یا کجا یابم قرار ؟</p></div>
<div class="m2"><p>برگ خشکم در کف باد صبا افتاده‌ام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر من ای صاحبدلان رحمی که از غم‌های عشق</p></div>
<div class="m2"><p>تا جدا افتاده‌ام از دل جدا افتاده‌ام</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>لب فرو بستم رهی بی‌روی گلچین و امیر</p></div>
<div class="m2"><p>در فراق هم‌نوایان از نوا افتاده‌ام</p></div></div>