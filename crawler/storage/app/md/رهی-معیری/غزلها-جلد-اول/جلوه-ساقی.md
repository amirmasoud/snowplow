---
title: >-
    جلوهٔ ساقی
---
# جلوهٔ ساقی

<div class="b" id="bn1"><div class="m1"><p>در قدح عکس تو یا گل در گلاب افتاده است؟</p></div>
<div class="m2"><p>مهر در آیینه یا آتش در آب افتاده است؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بادهٔ روشن دمی از دست ساقی دور نیست</p></div>
<div class="m2"><p>ماه امشب همنشین با آفتاب افتاده است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خفته از مستی به دامان ترم آن لاله‌روی</p></div>
<div class="m2"><p>برق از گرمی در آغوش سحاب افتاده است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در هوای مردمی از کید مردم سوختیم</p></div>
<div class="m2"><p>در دل ما آتش از موج سراب افتاده است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>طی نگشته روزگار کودکی پیری رسید</p></div>
<div class="m2"><p>از کتاب عمر ما فصل شباب افتاده است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آسمان در حیرت از بالانشینی‌های ماست</p></div>
<div class="m2"><p>بحر در اندیشه از کار حباب افتاده است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گوشهٔ عزلت بود سرمنزل عزت رهی</p></div>
<div class="m2"><p>گنج گوهر بین که در کنج خراب افتاده است</p></div></div>