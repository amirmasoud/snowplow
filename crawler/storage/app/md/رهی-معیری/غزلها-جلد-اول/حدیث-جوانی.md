---
title: >-
    حدیث جوانی
---
# حدیث جوانی

<div class="b" id="bn1"><div class="m1"><p>اشکم ولی به پای عزیزان چکیده‌ام</p></div>
<div class="m2"><p>خارم ولی به سایهٔ گل آرمیده‌ام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با یاد رنگ و بوی تو ای نو بهار عشق</p></div>
<div class="m2"><p>همچون بنفشه سر به گریبان کشیده‌ام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون خاک در هوای تو از پا فتاده‌ام</p></div>
<div class="m2"><p>چون اشک در قفای تو با سر دویده‌ام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>من جلوهٔ شباب ندیدم به عمر خویش</p></div>
<div class="m2"><p>از دیگران حدیث جوانی شنیده‌ام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از جام عافیت می نابی نخورده‌ام</p></div>
<div class="m2"><p>وز شاخ آرزو گل عیشی نچیده‌ام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>موی سپید را فلکم رایگان نداد</p></div>
<div class="m2"><p>این رشته را به نقد جوانی خریده‌ام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای سرو پای بسته به آزادگی مناز</p></div>
<div class="m2"><p>آزاده من که از همه عالم بریده‌ام</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر می‌گریزم از نظر مردمان رهی</p></div>
<div class="m2"><p>عیبم مکن که آهوی مردم‌ندیده‌ام</p></div></div>