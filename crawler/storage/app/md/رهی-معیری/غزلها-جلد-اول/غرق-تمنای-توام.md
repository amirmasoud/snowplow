---
title: >-
    غرق تمنای توام
---
# غرق تمنای توام

<div class="b" id="bn1"><div class="m1"><p>در پیش بی‌دردان چرا فریاد بی‌حاصل کنم</p></div>
<div class="m2"><p>گر شکوه‌ای دارم ز دل با یار صاحبدل کنم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در پرده سوزم همچو گل در سینه جوشم همچو مل</p></div>
<div class="m2"><p>من شمع رسوا نیستم تا گریه در محفل کنم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اول کنم اندیشه‌ای تا برگزینم پیشه‌ای</p></div>
<div class="m2"><p>آخر به یک پیمانه می اندیشه را باطل کنم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زآن رو ستانم جام را آن مایه آرام را</p></div>
<div class="m2"><p>تا خویشتن را لحظه‌ای از خویشتن غافل کنم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از گل شنیدم بوی او مستانه رفتم سوی او</p></div>
<div class="m2"><p>تا چون غبار کوی او در کوی جان منزل کنم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>روشنگری افلاکیم چون آفتاب از پاکیم</p></div>
<div class="m2"><p>خاکی نیم تا خویش را سرگرم آب و گل کنم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>غرق تمنای توام موجی ز دریای توام</p></div>
<div class="m2"><p>من نخل سرکش نیستم تا خانه در ساحل کنم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دانم که آن سرو سهی از دل ندارد آگهی</p></div>
<div class="m2"><p>چند از غم دل چون رهی فریاد بی‌حاصل کنم</p></div></div>