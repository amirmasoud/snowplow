---
title: >-
    شمارهٔ ۸۶
---
# شمارهٔ ۸۶

<div class="b" id="bn1"><div class="m1"><p>ساقی تو بمستیی گواه دل من</p></div>
<div class="m2"><p>کان دم که ز خود رود دل غافل من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جز آرزوی تو در دلم حاصل نیست</p></div>
<div class="m2"><p>این بس بود از هر دو جهان حاصل من</p></div></div>