---
title: >-
    شمارهٔ ۶۷
---
# شمارهٔ ۶۷

<div class="b" id="bn1"><div class="m1"><p>ساقی قدحی ده بمن سوخته حال</p></div>
<div class="m2"><p>وز من بنشان بآب می گرد ملال</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر برق وصال خرمن جمله بسوخت</p></div>
<div class="m2"><p>من سوخته ام به حسرت برق وصال</p></div></div>