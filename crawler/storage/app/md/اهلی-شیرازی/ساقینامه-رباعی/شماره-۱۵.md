---
title: >-
    شمارهٔ ۱۵
---
# شمارهٔ ۱۵

<div class="b" id="bn1"><div class="m1"><p>ساقی دل ما که شادی از غم نشناخت</p></div>
<div class="m2"><p>جز جام می از نعیم عالم نشناخت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می ده که دم صبوح جانبخش دمیست</p></div>
<div class="m2"><p>کس غیر مسیح قدر این دم نشناخت</p></div></div>