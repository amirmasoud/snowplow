---
title: >-
    شمارهٔ ۴
---
# شمارهٔ ۴

<div class="b" id="bn1"><div class="m1"><p>ساقی بکرم تو میکنی یاد مرا</p></div>
<div class="m2"><p>غیر از تو که میرسد بفریاد مرا؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر در غم دل تو دستگیرم نشوی</p></div>
<div class="m2"><p>سوی که روم که میکند شاد مرا؟</p></div></div>