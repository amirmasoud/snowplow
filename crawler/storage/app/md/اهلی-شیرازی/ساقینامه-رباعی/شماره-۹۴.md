---
title: >-
    شمارهٔ ۹۴
---
# شمارهٔ ۹۴

<div class="b" id="bn1"><div class="m1"><p>ساقی اگرت ستم مرادست بگو</p></div>
<div class="m2"><p>ور باده به جام عدل و دادست بگو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بدمستی اگر گناه خوی بد ماست</p></div>
<div class="m2"><p>بدخویی ما بما که دادست بگو</p></div></div>