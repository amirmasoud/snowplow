---
title: >-
    شمارهٔ ۷۱
---
# شمارهٔ ۷۱

<div class="b" id="bn1"><div class="m1"><p>ساقی نظری که همدم غم ماییم</p></div>
<div class="m2"><p>محروم ز خورشید چو شبنم ماییم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر چند که عالمیست محروم از تو</p></div>
<div class="m2"><p>محروم ترین خلق عالم ماییم</p></div></div>