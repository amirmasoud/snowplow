---
title: >-
    شمارهٔ ۶۰
---
# شمارهٔ ۶۰

<div class="b" id="bn1"><div class="m1"><p>ساقی نظری که دردی از جام تو بس</p></div>
<div class="m2"><p>ورمی نبود عارض گلفام تو بس</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جان مست شود چو نام ساقی شنود</p></div>
<div class="m2"><p>ایراحت جان مرا همی نام تو بس</p></div></div>