---
title: >-
    شمارهٔ ۶۳
---
# شمارهٔ ۶۳

<div class="b" id="bn1"><div class="m1"><p>ساقی تو مهی ز روی فرخنده خویش</p></div>
<div class="m2"><p>حسن تو فرشته کرد شرمنده خویش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر خنده زنان صبح به بیند چو گلت</p></div>
<div class="m2"><p>گرید بهزار دیده از خنده خویش</p></div></div>