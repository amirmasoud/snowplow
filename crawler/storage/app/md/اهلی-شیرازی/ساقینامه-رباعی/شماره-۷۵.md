---
title: >-
    شمارهٔ ۷۵
---
# شمارهٔ ۷۵

<div class="b" id="bn1"><div class="m1"><p>ساقی قدحی که دل بدریا فکنم</p></div>
<div class="m2"><p>چشمی سوی آن نرگس شهلا فکنم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ما را سر و تن گر نشود خاک رهت</p></div>
<div class="m2"><p>سر پیش سگان و تن بصحرا فکنم</p></div></div>