---
title: >-
    شمارهٔ ۲۷
---
# شمارهٔ ۲۷

<div class="b" id="bn1"><div class="m1"><p>ساقی که لبش مفرح یاقوت است</p></div>
<div class="m2"><p>دلرا غم او قوت و جان را قوت است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر کس که نشد کشته طوفان غمش</p></div>
<div class="m2"><p>در کشتی نوح زنده در تابوت است</p></div></div>