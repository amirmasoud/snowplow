---
title: >-
    شمارهٔ ۹۰
---
# شمارهٔ ۹۰

<div class="b" id="bn1"><div class="m1"><p>ساقی غم دل کجا خورد جان حزین</p></div>
<div class="m2"><p>می ده که بریده ام دل از خلد برین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل یا غم جانان بودش یا غم دین</p></div>
<div class="m2"><p>گر هر دو طلب کند نه آنست و نه این</p></div></div>