---
title: >-
    شمارهٔ ۳۳
---
# شمارهٔ ۳۳

<div class="b" id="bn1"><div class="m1"><p>ساقی که هلاکم ز غم هجرانت</p></div>
<div class="m2"><p>هر جا که روی دست من و دامانت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رفتی هزار دل هلاک از غم تست</p></div>
<div class="m2"><p>باز آی که صد هزار جان قربانت</p></div></div>