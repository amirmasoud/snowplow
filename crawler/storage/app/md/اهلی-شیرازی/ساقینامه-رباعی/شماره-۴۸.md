---
title: >-
    شمارهٔ ۴۸
---
# شمارهٔ ۴۸

<div class="b" id="bn1"><div class="m1"><p>ساقی فرح از ساغر می میباشد</p></div>
<div class="m2"><p>عیش و طرب از نوای نی میباشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دیوانه من از هجر توام عیب مکن</p></div>
<div class="m2"><p>دیوانگی از برای کی میباشد</p></div></div>