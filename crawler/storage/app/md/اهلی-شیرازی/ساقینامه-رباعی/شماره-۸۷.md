---
title: >-
    شمارهٔ ۸۷
---
# شمارهٔ ۸۷

<div class="b" id="bn1"><div class="m1"><p>ساقی قدحی ده و دل از غم برهان</p></div>
<div class="m2"><p>جان را ز خیال هر دو عالم برهان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وارسته چو خضریم زهر قید که هست</p></div>
<div class="m2"><p>در قید حیاتیم ازین هم برهان</p></div></div>