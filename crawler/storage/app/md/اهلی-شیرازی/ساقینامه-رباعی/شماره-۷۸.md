---
title: >-
    شمارهٔ ۷۸
---
# شمارهٔ ۷۸

<div class="b" id="bn1"><div class="m1"><p>ساقی قدحی که من ببستان نروم</p></div>
<div class="m2"><p>بی روی تو در روضه رضوان نروم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا سر بودم قدم در این راه نهم</p></div>
<div class="m2"><p>تا جان بودم ز کوی جانان نروم</p></div></div>