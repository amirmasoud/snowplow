---
title: >-
    شمارهٔ ۷۴
---
# شمارهٔ ۷۴

<div class="b" id="bn1"><div class="m1"><p>ساقی نظر از تو گر سوی باغ کنم</p></div>
<div class="m2"><p>باغ از تف دل سیه ترا از داغ کنم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر آتش حسرتت برم زیر زمین</p></div>
<div class="m2"><p>چون لاله همه زیر زمین داغ کنم</p></div></div>