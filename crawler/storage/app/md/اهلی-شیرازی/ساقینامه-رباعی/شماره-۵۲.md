---
title: >-
    شمارهٔ ۵۲
---
# شمارهٔ ۵۲

<div class="b" id="bn1"><div class="m1"><p>ساقی چه صلاح از من مجنون آید</p></div>
<div class="m2"><p>فکر از تو مگر باز بقانون آید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پر کن قدحی که پر تهیدست ودلیم</p></div>
<div class="m2"><p>از دست و دل تهی چه بیرون آید</p></div></div>