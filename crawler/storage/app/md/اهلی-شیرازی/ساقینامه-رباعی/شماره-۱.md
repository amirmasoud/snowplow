---
title: >-
    شمارهٔ ۱
---
# شمارهٔ ۱

<div class="b" id="bn1"><div class="m1"><p>ساقی قدحی که کار سازست خدا</p></div>
<div class="m2"><p>وز رحمت خود بنده نوازست خدا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می خور بنیاز و ناز طاعت مفروش</p></div>
<div class="m2"><p>کز طاعت خلق بی نیازست خدا</p></div></div>