---
title: >-
    شمارهٔ ۸
---
# شمارهٔ ۸

<div class="b" id="bn1"><div class="m1"><p>ساقی نظری که دل خوش از دیدن تست</p></div>
<div class="m2"><p>جان شاد ز خوشه چینی خرمن تست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ناگفته دلت ضمیر ما میداند</p></div>
<div class="m2"><p>جام جم عاشقان دل روشن تست</p></div></div>