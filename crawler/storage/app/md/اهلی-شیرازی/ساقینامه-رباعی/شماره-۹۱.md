---
title: >-
    شمارهٔ ۹۱
---
# شمارهٔ ۹۱

<div class="b" id="bn1"><div class="m1"><p>ساقی دل من سوخت نظر با من کن</p></div>
<div class="m2"><p>چشمی فکن و گلخن من گلشن کن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مردم چو چراغ سحر شمع مراد</p></div>
<div class="m2"><p>یکبار دگر چراغ من روشن کن</p></div></div>