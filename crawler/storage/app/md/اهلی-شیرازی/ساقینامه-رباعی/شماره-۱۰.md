---
title: >-
    شمارهٔ ۱۰
---
# شمارهٔ ۱۰

<div class="b" id="bn1"><div class="m1"><p>ساقی قدحی که کار عالم نفسی است</p></div>
<div class="m2"><p>گر یک نفست فراغتی هست بسی است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خوشباش به هر چه پیشت آید ز جهان</p></div>
<div class="m2"><p>هرگز نشود چنانکه دلخواه کسیست</p></div></div>