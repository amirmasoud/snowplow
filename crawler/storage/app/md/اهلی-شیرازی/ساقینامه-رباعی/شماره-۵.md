---
title: >-
    شمارهٔ ۵
---
# شمارهٔ ۵

<div class="b" id="bn1"><div class="m1"><p>ساقی می لعل قوت روح است مرا</p></div>
<div class="m2"><p>دیدار تو خورشید صبوح است مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>برخیز که در پای تو مردن نفسی</p></div>
<div class="m2"><p>بهتر ز هزار عمر نوح است مرا</p></div></div>