---
title: >-
    شمارهٔ ۹۲
---
# شمارهٔ ۹۲

<div class="b" id="bn1"><div class="m1"><p>ساقی هم زخم طعنه شد مستی من</p></div>
<div class="m2"><p>در خاک فرو رفت دل از پستی من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خواهم که چنان گم شوم از ملک وجود</p></div>
<div class="m2"><p>کز هر دو جهان محو شود هستی من</p></div></div>