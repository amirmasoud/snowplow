---
title: >-
    شمارهٔ ۵۳
---
# شمارهٔ ۵۳

<div class="b" id="bn1"><div class="m1"><p>ساقی ز زمانه چند بیداد رسد</p></div>
<div class="m2"><p>تا چند ستم بر دل ناشاد رسد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فریاد چه سود چون بود بخت بخواب</p></div>
<div class="m2"><p>بیداری دل مگر بفریاد رسد</p></div></div>