---
title: >-
    شمارهٔ ۸۵
---
# شمارهٔ ۸۵

<div class="b" id="bn1"><div class="m1"><p>ساقی نظری که مستم و شیدا هم</p></div>
<div class="m2"><p>دنیا بدو جو پیش من و عقباهم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مست تو بسوی جنت و کوثر و حور</p></div>
<div class="m2"><p>امروز نظر نمیکند فردا هم</p></div></div>