---
title: >-
    شمارهٔ ۴۳
---
# شمارهٔ ۴۳

<div class="b" id="bn1"><div class="m1"><p>ساقی قدحی که هر که بیدار بود</p></div>
<div class="m2"><p>امید حیاتش از لب یار بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر کس که حیات جوید از ظلمت دهر</p></div>
<div class="m2"><p>آخر ز حیات خویش بیزار بود</p></div></div>