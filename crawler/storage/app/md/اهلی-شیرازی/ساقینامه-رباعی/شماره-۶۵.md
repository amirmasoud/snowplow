---
title: >-
    شمارهٔ ۶۵
---
# شمارهٔ ۶۵

<div class="b" id="bn1"><div class="m1"><p>ساقی که رسد بوصلت از یاری عقل</p></div>
<div class="m2"><p>در خواب که بنیدت ز بیداری عقل؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از باده عشق اگر چه بدمستی زاد</p></div>
<div class="m2"><p>بد مستی عشق به ز هشیاری عقل</p></div></div>