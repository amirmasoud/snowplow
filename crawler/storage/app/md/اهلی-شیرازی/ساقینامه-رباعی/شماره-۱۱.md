---
title: >-
    شمارهٔ ۱۱
---
# شمارهٔ ۱۱

<div class="b" id="bn1"><div class="m1"><p>ساقی دل ما سوخته از مشتاقیست</p></div>
<div class="m2"><p>بازآ که طبیب درد مستان ساقیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جان دادن امیدست مرا در قدمت</p></div>
<div class="m2"><p>تا جان بودم امیدواری باقیست</p></div></div>