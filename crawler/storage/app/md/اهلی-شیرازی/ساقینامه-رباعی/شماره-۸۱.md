---
title: >-
    شمارهٔ ۸۱
---
# شمارهٔ ۸۱

<div class="b" id="bn1"><div class="m1"><p>ساقی قدحی که از غم دل پیرم</p></div>
<div class="m2"><p>بی می چو چراغ صبحدم میمیرم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بازم بچراغ روغنی ریز ز می</p></div>
<div class="m2"><p>تا بار دگر زندگی از سر میگیرم</p></div></div>