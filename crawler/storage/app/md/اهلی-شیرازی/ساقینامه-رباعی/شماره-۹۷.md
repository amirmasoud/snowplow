---
title: >-
    شمارهٔ ۹۷
---
# شمارهٔ ۹۷

<div class="b" id="bn1"><div class="m1"><p>ای ساقی جان و سرو آزاد کسی</p></div>
<div class="m2"><p>چونست که هرگز نکنی یاد کسی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دست که به دامن تو ای سرو رسد؟</p></div>
<div class="m2"><p>هم برسد که می‌دهد داد کسی؟</p></div></div>