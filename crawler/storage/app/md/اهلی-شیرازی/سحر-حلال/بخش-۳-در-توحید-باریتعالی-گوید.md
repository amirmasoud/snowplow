---
title: >-
    بخش ۳ - در توحید باریتعالی گوید
---
# بخش ۳ - در توحید باریتعالی گوید

<div class="b" id="bn1"><div class="m1"><p>ای که بر اسرار تو دانا کمند</p></div>
<div class="m2"><p>کی رسد از عقل کس آنجا کمند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کیست درین مرحله تا آخرت</p></div>
<div class="m2"><p>رهبر اول شده یا آخرت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون همه ز اندیشه خود واپسند</p></div>
<div class="m2"><p>کی بود اندیشه‌ات از ما پسند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کی کند ادراک تو حاصل خرد</p></div>
<div class="m2"><p>فهم کی این عشوه باطل خرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در کف داود تو جان جبه چیست</p></div>
<div class="m2"><p>علم تو داند که در آن جبه چیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>لطف تو بخشنده تخت از نواخت</p></div>
<div class="m2"><p>یوسف جان رایت بخت از نو آخت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یافته از لطف تو جنت نعم</p></div>
<div class="m2"><p>قهر تو لا گفته و رحمت نعم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بخشش تو نعمت و گنج روان</p></div>
<div class="m2"><p>رنجش تو علت و رنج روان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تا شدی از بنده دین رنج‌کاه</p></div>
<div class="m2"><p>یافته صد راحت ازین رنجگاه</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گلبن تن را دهی از جان نوا</p></div>
<div class="m2"><p>بلبل دل را رسد از آن نوا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نغمه شوقت دل عشاق راست</p></div>
<div class="m2"><p>آمد از آن نغمه عشاق راست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بنده بی‌عشق تو مرد ار زنست</p></div>
<div class="m2"><p>بهتر از آن بی‌غم و درد ارزنست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>در مکش از کرده بد روز ما</p></div>
<div class="m2"><p>شب مکن از هیبت خود روز ما</p></div></div>