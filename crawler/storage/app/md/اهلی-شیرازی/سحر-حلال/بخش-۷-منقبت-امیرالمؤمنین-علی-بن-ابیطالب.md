---
title: >-
    بخش ۷ - منقبت امیرالمؤمنین علی بن ابیطالب
---
# بخش ۷ - منقبت امیرالمؤمنین علی بن ابیطالب

<div class="b" id="bn1"><div class="m1"><p>پیر و حیدر شو و همرنگ آل</p></div>
<div class="m2"><p>تا دمد از روی تو هم رنگ آل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حیدر والا گهر آن سرفراز</p></div>
<div class="m2"><p>کامده نور حقش از در فراز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رهرو حق آمد و همراه حق</p></div>
<div class="m2"><p>هم حق از او ظاهر و هم راه حق</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تیغ وی آن رهبر جان بر قدم</p></div>
<div class="m2"><p>آتش قهر آمده زان برق دم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سرور و شاه همه کو صفدرست</p></div>
<div class="m2"><p>در صف جنگ از همه او صفدرست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جوهر او گوهر حق آفرین</p></div>
<div class="m2"><p>باد بر آن مظهر حق آفرین</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مردم نورانی او عرض عین</p></div>
<div class="m2"><p>بر همه شان سجده او فرض عین</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>یافته عزت فلک از شاه دین</p></div>
<div class="m2"><p>دعوی او را ملک از شاهدین</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گوهر او یافته درج شرف</p></div>
<div class="m2"><p>اختر او تافته برج شرف</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>واقف جود آن شه دین در سجود</p></div>
<div class="m2"><p>شد همه جا حافظ این درس جود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مرغ دل از خرمن او دانه چید</p></div>
<div class="m2"><p>بلبل جان هم گل از آن خانه چید</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>با سک او تا شده دشمن مزید</p></div>
<div class="m2"><p>دوزخش انداخته هل من مزید</p></div></div>