---
title: >-
    بخش ۲۴ - رفتن جم بگوی بازی و افتادن از اسب و هلاک شدن
---
# بخش ۲۴ - رفتن جم بگوی بازی و افتادن از اسب و هلاک شدن

<div class="b" id="bn1"><div class="m1"><p>ساقی ازین چنبر غم داد داد</p></div>
<div class="m2"><p>کشت بس آزاده و کم داد داد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر سر ما تا مه و گردون بود</p></div>
<div class="m2"><p>سوزد ازو گر شه و گردون بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>روزی از آسایش آن خوش هوا</p></div>
<div class="m2"><p>خاطر جم را تک ابرش هوا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خورد دو جام می گلبو زدن</p></div>
<div class="m2"><p>جانب میدان شده در گوزدن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر سر گردون تک ابرش رساند</p></div>
<div class="m2"><p>گوزد و بر تارک ابرش رساند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زان سر چوگان شده گو شهر شهر</p></div>
<div class="m2"><p>چون مه نوز ابروی او شهر شهر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یکدم از او چون دم بیهوده گوی</p></div>
<div class="m2"><p>از خم چوگان نشد آسوده گوی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تاخته اسب از حد چین تاختن</p></div>
<div class="m2"><p>مرگ هم آماده بر این تاختن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>رد شد از اسب آنمه و خون خورد و مرد</p></div>
<div class="m2"><p>ساغر جم گشت از آن خرد و مرد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سیلی مرگ آنهمه اسباب خورد</p></div>
<div class="m2"><p>زیروی از خون وی اسب آب خورد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خورد شد از حادثه آن جام جم</p></div>
<div class="m2"><p>مرد وشد این عاقبت انجام جم</p></div></div>