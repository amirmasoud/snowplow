---
title: >-
    بخش ۶ - در خطاب زمین بوس گوید
---
# بخش ۶ - در خطاب زمین بوس گوید

<div class="b" id="bn1"><div class="m1"><p>ای شده در خانه جان منزلت</p></div>
<div class="m2"><p>خانه جان یافته زان منزلت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای شده مهر رخ تو زین چرخ</p></div>
<div class="m2"><p>چرخ ازان آمده در عین چرخ</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مهر تو ارزنده بیعت بود</p></div>
<div class="m2"><p>یوسف از آن بنده بیعت بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چشمه خور طلعت رخشان تو</p></div>
<div class="m2"><p>یوسفی و صفوت رخ شان تو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>روی تو آیینه خورشید تاب</p></div>
<div class="m2"><p>می برد از ذره نومید تاب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>طلعت تو صورت مهدی گراست</p></div>
<div class="m2"><p>خوبی تو دیگر و مه دیگر است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دورم از آن آینه تابنده ام</p></div>
<div class="m2"><p>گرچه از آن آینه تابنده ام</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بردرت این بنده مسکین نهاد</p></div>
<div class="m2"><p>خشت دراز شوق تو بالین نهاد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اهلی شیرینی سخن از مدحت اوست</p></div>
<div class="m2"><p>طوطی شکر شکن از مدحت اوست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از ره مدحت چو شکر خواست</p></div>
<div class="m2"><p>دایم از آن مرغ شکر خاست او</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نامه مدحت همه یکسر نوشت</p></div>
<div class="m2"><p>مدح تو گفت و غم دل در نوشت</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>در کف تو چامه او یا رسول</p></div>
<div class="m2"><p>خود نهد این نامه او یا رسول</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>هم شه امروزی و هم شاه دی</p></div>
<div class="m2"><p>بر همه عالم همه دم شاهدی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>قرب تو گر از ره آلت بود</p></div>
<div class="m2"><p>آلت آن مدحت آلت بود</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>هر که بر آلت دهد از جان درود</p></div>
<div class="m2"><p>کشته آمرزش و غفران درود</p></div></div>