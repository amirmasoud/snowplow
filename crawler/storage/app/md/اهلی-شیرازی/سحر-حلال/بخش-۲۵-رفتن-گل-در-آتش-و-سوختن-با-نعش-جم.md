---
title: >-
    بخش ۲۵ - رفتن گل در آتش و سوختن با نعش جم
---
# بخش ۲۵ - رفتن گل در آتش و سوختن با نعش جم

<div class="b" id="bn1"><div class="m1"><p>ساقی ازین کاسه و خوان کبود</p></div>
<div class="m2"><p>خرمی اندر دل و جان که بود؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چشمه نوشیست پر از گرد مهر</p></div>
<div class="m2"><p>گرمی رقصیست در آن سرد مهر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>قصه دختر شنو القصه باز</p></div>
<div class="m2"><p>کرد بر او جم دری از غصه باز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آمده این فرض بر آتش زنان</p></div>
<div class="m2"><p>کز پی نعشند در آتش زنان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شخص جم ار مرده وار زنده بود</p></div>
<div class="m2"><p>بر سر آتش شدن ارزنده بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جم که پر از ناوک کین کیش داشت</p></div>
<div class="m2"><p>مرد و در آتش شدو و آن کیش داشت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سخت شد از عالم فرمان بری</p></div>
<div class="m2"><p>زنده در آتش شدن از آن پری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ماه رخ آراسته چون مشتری</p></div>
<div class="m2"><p>وز غم او غرقه خون مشتری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از پی رقص از غم جم کف زنان</p></div>
<div class="m2"><p>غرقه خون هم رخ و هم کف زنان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سرو قدش بر زده دامان شده</p></div>
<div class="m2"><p>مو همه رام دل و دام آن شده</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>باد برافروخته آتش به چرخ</p></div>
<div class="m2"><p>بر سر آتش زده پا خوش به چرخ</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>او همه هیزم شده گوگرد باد</p></div>
<div class="m2"><p>خاک ره آتش شد و او گرد باد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>عاشق و سرمست نه پروانه رنگ</p></div>
<div class="m2"><p>چرخ بر آتش زده پروانه رنگ</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>مست شد آن مهوش و گلنار گشت</p></div>
<div class="m2"><p>رفت در آن آتش و گل نار گشت</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>دانه وش افتاد در آتش روان</p></div>
<div class="m2"><p>طعنه زد آن شمع بر آتش روان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>زو نشد اندر غم جان گونه کم</p></div>
<div class="m2"><p>دانه در آتش رود آن گونه کم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>آتش شوقش دل پروانه سوخت</p></div>
<div class="m2"><p>زن نگر آخر که چه مردانه سوخت</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ایدل ازین واقعه بیدار شو</p></div>
<div class="m2"><p>کشته درین معرکه بی دار شو</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>جسته ازین معرکه که گردان همه</p></div>
<div class="m2"><p>کرده رخ از مهلکه گردان همه</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>غیرت عشق از همه کس برنخاست</p></div>
<div class="m2"><p>عشق هم از طینت خس برنخاست</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بید شد ار بیدل و بیدین ز عشق</p></div>
<div class="m2"><p>میکشد او خنجر بیدین ز عشق</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>گر همه بر خود زده خنجر خلاف</p></div>
<div class="m2"><p>دوستی این آمد و دیگر خلاف</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>باغ در آرایش و آیین گل</p></div>
<div class="m2"><p>سوختن آسایش و آیین گل</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چون تن گل را رود از سر گل آب</p></div>
<div class="m2"><p>گل چه در آتش چه خود اندر گلاب</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>کاین شه عرش آمده وانکه گلی است</p></div>
<div class="m2"><p>خانکه که زان شه رود آنگه گلی است</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>معدن گنج و گهر این خاک دان</p></div>
<div class="m2"><p>در شو و منگر دگر این خاکدان</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>قطره کزین بحر برآمده درست</p></div>
<div class="m2"><p>در شد و شد قیمت آن صد درست</p></div></div>