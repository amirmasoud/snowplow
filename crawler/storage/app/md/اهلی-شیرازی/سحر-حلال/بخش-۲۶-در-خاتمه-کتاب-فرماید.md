---
title: >-
    بخش ۲۶ - در خاتمه کتاب فرماید
---
# بخش ۲۶ - در خاتمه کتاب فرماید

<div class="b" id="bn1"><div class="m1"><p>ساقی ازین جرعه در انجام کوش</p></div>
<div class="m2"><p>چون همه داریم بر انجام کوش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پر مکن این شیشه و خم کوتهیست</p></div>
<div class="m2"><p>کاخر سررشته گم کوتهیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا بکی این خانه و جام مدام</p></div>
<div class="m2"><p>بگذر ازین دانه و دام مدام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جان که در آتش پرداز از سرخوشی</p></div>
<div class="m2"><p>تلخی مرگش برد از سرخوشی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دام تو شد از طرب آواز چنگ</p></div>
<div class="m2"><p>تا برد آنشمع شب آواز چنگ</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نعره زن از قافله آن خوش درای</p></div>
<div class="m2"><p>کز سر جان خیز و در آتش درآی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>درگذر از این تن چون سرخ روی</p></div>
<div class="m2"><p>زر شو و ساز از تف خون سرخ روی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>میل تو شد گر سوی دارالسلام</p></div>
<div class="m2"><p>من شدم اینک روی دارالسلام</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از سر جان بگذر و دلخوش نشین</p></div>
<div class="m2"><p>باش در آن منزل گل خوش نشین</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ناوک دل را پر دین برنشان</p></div>
<div class="m2"><p>تا خورد این ناوک ازین بر نشان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کعبه دل گر در بتخانه ایست</p></div>
<div class="m2"><p>رو چوبت اندر در بتخانه ایست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>طاعت یزدان کن و بت کم پرست</p></div>
<div class="m2"><p>بر دل طایر صفت آن هم پرست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>طاعت صد قافله هر شام کن</p></div>
<div class="m2"><p>صبح حج و نافله در شام کن</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>از همه کش خواری و چون خارپشت</p></div>
<div class="m2"><p>کم کن ازین وادی خونخوار پشت</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>اهلی ازین بادیه کز خون ترست</p></div>
<div class="m2"><p>دمبدم آشفته و مجنون ترست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>شد ز خود آواره و ثابت نشست</p></div>
<div class="m2"><p>تا بر سیاره و ثابت نشست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>تا که در این کعبه جان گام زد</p></div>
<div class="m2"><p>مدتی از سعی در آن کام زد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ناوک صد جعبه برین بوته ریخت</p></div>
<div class="m2"><p>از همه زر بر دو درین بوته ریخت</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>سکه او بین کم از آن خورده گیر</p></div>
<div class="m2"><p>خورده رشکی هم از آن خورده گیر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>آهوی او گر شده عیبش مبین</p></div>
<div class="m2"><p>نافه او بنگر و عیبش مبین</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>خوش کن ازین گلشن و مأوا گذار</p></div>
<div class="m2"><p>گل بر و خارش بر ما واگذار</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>سوختم از محنت و پر ساختم</p></div>
<div class="m2"><p>تا که من این مخزن در ساختم</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بسته برین سوخته ره بحرها</p></div>
<div class="m2"><p>گه سد ره قافیه گه بحرها</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>معرکه بر مدر که تنگ آمده</p></div>
<div class="m2"><p>رستم ازین معرکه تنگ آمده</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>نوح شد این همت و کشتی گرفت</p></div>
<div class="m2"><p>تر نشد از زحمت و کشتی گرفت</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>تا که خم آمد قد هم کشتیم</p></div>
<div class="m2"><p>رسته شد از ورطه غم کشتیم</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>کس چو من این رشته زیبا نتافت</p></div>
<div class="m2"><p>پرتو فکر کسی اینجا نتافت</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>سودن این لعل و در آسان کجاست</p></div>
<div class="m2"><p>این حق دریاست در آس آن کجاست</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>فکرت من صاحب صد رمز شعر</p></div>
<div class="m2"><p>در همه جا صاحب صدرم ز شعر</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>زهره گر این چنگ من آرد بچنگ</p></div>
<div class="m2"><p>تارک جان سخن آرد به چنگ</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>گو سر مضراب در ابریشم آر</p></div>
<div class="m2"><p>وز نم خون هر مژه ابری شمار</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>باتک من شیر نر از همرهی</p></div>
<div class="m2"><p>ناید ازو تک مگر از هم رهی</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>فارس میدان طلب این فارسیست</p></div>
<div class="m2"><p>وز دم شاه عرب این فارسیست</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>بنده محمودم و سر در قدم</p></div>
<div class="m2"><p>حلقه شد از خدمت این در قدم</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>لطف وی از دجله خون برکنار</p></div>
<div class="m2"><p>کشتیم آورد در اندر کنار</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>هست درین سر هوس شاهیم</p></div>
<div class="m2"><p>نیست سر و مال بجز شاهیم</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>بر لب بحر از همه سو فارغم</p></div>
<div class="m2"><p>رسته ام از ناوک و سوفا رغم</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>شرطه شد از همت محمود باد</p></div>
<div class="m2"><p>آخر کار همه محمود باد</p></div></div>