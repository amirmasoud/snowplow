---
title: >-
    بخش ۱۲ - تعریف مولانا کاتبی و مجمع البحرین او
---
# بخش ۱۲ - تعریف مولانا کاتبی و مجمع البحرین او

<div class="b" id="bn1"><div class="m1"><p>قافیه سنجان همه عیسی دمند</p></div>
<div class="m2"><p>وز دم خود جان پی احیا دمند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>طایر فرخنده معنی پرند</p></div>
<div class="m2"><p>جانب عرش از پر دعوی پرند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پیشرو و از لشگر و پس تاخته</p></div>
<div class="m2"><p>تیغ ببالا و بپست آخته</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کاتبی آویخت دو محکم کمان</p></div>
<div class="m2"><p>کامده در قبضه رستم کم آن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مجمع بحرین در آن دادکار</p></div>
<div class="m2"><p>نسخه تجنیس شد آن یادگار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>فکرت صاحب خرد از هوش کار</p></div>
<div class="m2"><p>کرده از آن هر دو صد آهوشکار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بازوی من ساخت دو آهن کمان</p></div>
<div class="m2"><p>خم شده هر دو به یک آهنگم آن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مجمع بحرین در افشان دو بحر</p></div>
<div class="m2"><p>جامع تجنیس و در اوزان دو بحر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>قافیتین البته گفتن دو زه</p></div>
<div class="m2"><p>با همه کاحسن همه گفتند و زه</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ساختم آن قبضه او دست کش</p></div>
<div class="m2"><p>رستم ازین که گو دست کش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هر یک از آن احسن و جوهر یکی</p></div>
<div class="m2"><p>کی شده بیجاده و گوهر یکی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گر گل او یافته بلبل هزار</p></div>
<div class="m2"><p>گلشن من دارد از آن گل هزار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>راستی آن کین دژ رویینه بود</p></div>
<div class="m2"><p>فتح من از این دژ رویی نه بود</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بازوی من کسوت پشمینه داشت</p></div>
<div class="m2"><p>پنجه من قوت پشمی نداشت</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ماندم و هم تن در خوی برگشاد</p></div>
<div class="m2"><p>همت شاه این در خیبر گشاد</p></div></div>