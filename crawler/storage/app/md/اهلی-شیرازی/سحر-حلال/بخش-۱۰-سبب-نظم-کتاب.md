---
title: >-
    بخش ۱۰ - سبب نظم کتاب
---
# بخش ۱۰ - سبب نظم کتاب

<div class="b" id="bn1"><div class="m1"><p>ساقی از آن مشربه یاقوت ده</p></div>
<div class="m2"><p>قوتم از مرتبه یاقوت ده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یا رهد این دل تن وی از سراب</p></div>
<div class="m2"><p>یا رود از مستی می بر سر آب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یکشب از آنجا که در انجام حال</p></div>
<div class="m2"><p>شده ره بیگانه در آنجا محال</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دل که در آن دجله خون آشناست</p></div>
<div class="m2"><p>گفتمش اینواقعه چون آشناست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خدمت خلق از ره خر بندگیست</p></div>
<div class="m2"><p>خاطر آزاد تو در بند کیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خیز و رخ از ظلمت غفلت بتاب</p></div>
<div class="m2"><p>رشته جهد از پی طاعت بتاب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نیست ره از هیأت و حکمت بدوست</p></div>
<div class="m2"><p>از در دلها ره قربت بدوست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کار نه نحوست درین کو نه صرف</p></div>
<div class="m2"><p>عمر تو تا کی شود این گونه صرف</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هر که حقش نامده راضی ز حال</p></div>
<div class="m2"><p>یافته کم معنی ماضی ز حال</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>رایحه همدم شده با گل وزان</p></div>
<div class="m2"><p>شد هم دم با گل و سنبل وزان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>عاصفه چون بیهده گرد آمده</p></div>
<div class="m2"><p>حاصل کارش همه گرد آمده</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هر که در افسانه و افسون گریست</p></div>
<div class="m2"><p>بس که بر افسانه و افسون گریست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گم مشو اندر پی نالان درای</p></div>
<div class="m2"><p>مرد شو اندر صف مردان درآی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>پا مکش از شه ره تحقیق باز</p></div>
<div class="m2"><p>تا کند این در بتو توفیق باز</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>خیز و در آسایش اصحاب کوش</p></div>
<div class="m2"><p>تا کند اخبار تو احباب کوش</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>نکته سر رشته نظم آوران</p></div>
<div class="m2"><p>در کن و در رشته نظم آور آن</p></div></div>