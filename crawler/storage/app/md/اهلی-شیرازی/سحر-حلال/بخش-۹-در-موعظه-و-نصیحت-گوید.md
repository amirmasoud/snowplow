---
title: >-
    بخش ۹ - در موعظه و نصیحت گوید
---
# بخش ۹ - در موعظه و نصیحت گوید

<div class="b" id="bn1"><div class="m1"><p>ساقی از آن شیشه منصور دم</p></div>
<div class="m2"><p>بر رگ و بر ریشه من صوردم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خواهی از این نادره گو گر مقال</p></div>
<div class="m2"><p>زاتش می کن دم او گرم قال</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آتشی از می فکن اندر روان</p></div>
<div class="m2"><p>تا شود این نکته چون زر روان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یکنفس ای مونس من کوش دار</p></div>
<div class="m2"><p>گوهری از مجلس من گوش دار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مرتبه دان همه شی دانش است</p></div>
<div class="m2"><p>وین سخن اندر دل شیدا نشست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نامه من کامده یکسر بلاغ</p></div>
<div class="m2"><p>حق شمر آن نامه و مشمر بلاغ</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در صف آن طاعت اکثر صفا</p></div>
<div class="m2"><p>پیشتر از عقد صف اندر صف آ</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هر که شد از طاعت حق پیشتر</p></div>
<div class="m2"><p>فیض وی از رحمت حق بیشتر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بنده بی قیمت و میر اجل</p></div>
<div class="m2"><p>هر دو شد افتاده تیر اجل</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>پیشتر از مرگ خود ایخواجه میر</p></div>
<div class="m2"><p>تا شوی از ترک خود ایخواجه میر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از پی گور آمده بهرام گور</p></div>
<div class="m2"><p>پیش دل وحش تو به رام گور</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>خواجه در ابریشم و ما در گلیم</p></div>
<div class="m2"><p>عاقبت ایدل همه یکسر گلیم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>دانه امید در آن خانه کار</p></div>
<div class="m2"><p>کامده جاوید در آن خانه کار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>پر مکن این تخته جان خان گیر</p></div>
<div class="m2"><p>مهره تن واکن و آن خانه گیر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>هر که شد اینجا دم او دیر پای</p></div>
<div class="m2"><p>برکشد از دل غم او دیر پای</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>زودتر این وادی و صحرا نورد</p></div>
<div class="m2"><p>زانکه نه خارش بود از مانه ورد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چرخ کی اندر سر غمخواریست</p></div>
<div class="m2"><p>رحمت او بر سر غم خواری است</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>در ره حق گر شوی از رهروان</p></div>
<div class="m2"><p>یوسف جان بر کشی از چه روان</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بر دل تو نیست تن این جامه ایست</p></div>
<div class="m2"><p>بگسل ازین جامه و اینجامایست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>پیکرت آراسته حق چون پری</p></div>
<div class="m2"><p>تا تو سوی صانع بیچون پری</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بگذر ازین پیکر و بیناییش</p></div>
<div class="m2"><p>غلغل نی منگر و بین ناییش</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>رهزن مردان شده شیطان بمال</p></div>
<div class="m2"><p>گوش وی از کوشش احسان بمال</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>کی بود این مملکت جان بی خدیو</p></div>
<div class="m2"><p>کز دل ما برکند آن بیخ دیو</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>مرد گر آخر کم از آن رهزن است</p></div>
<div class="m2"><p>مرد نه کان ناکس گمره، زن است</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>دور کن از آینه مردود را</p></div>
<div class="m2"><p>ره مده از روزنه مردود را</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>گر تهی آن آینه آید ز دود</p></div>
<div class="m2"><p>زنگ غم از آینه شاید زدود</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>نفس تو چون خر همه سود چراست</p></div>
<div class="m2"><p>آهوی جان در پی این خر چراست</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>با همه این دعوی شهبازیت</p></div>
<div class="m2"><p>میدهد این روی سیه بازیت</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>جان شده از حرص تو پیچان در آز</p></div>
<div class="m2"><p>بگسل ازین رشته دامان دراز</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>سر بسر از لقمه آزی دهان</p></div>
<div class="m2"><p>فکر کن از لقمه بازی دهان</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>مرغ تو تا قوت بازیش هست</p></div>
<div class="m2"><p>وسوسه هم فرصت بازیش هست</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>جای اگر اندر ته غارت بود</p></div>
<div class="m2"><p>وسوسه اندر ره غارت بود</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>شد بد و نیک همه کس درگذر</p></div>
<div class="m2"><p>از بد و نیک همه پس درگذر</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>بر تن بیگانه و بر جان خویش</p></div>
<div class="m2"><p>ناحق و حق دان همه در شأن خویش</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>گرچه شد این ره روی آسان نما</p></div>
<div class="m2"><p>نی تو درین ره روی آسان نه ما</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>میکند اینها همه توفیق راست</p></div>
<div class="m2"><p>دولت عقبی همه توفیق راست</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>اهلی از آن غم که کم آید بدست</p></div>
<div class="m2"><p>ناخوشی حال تو از خود بدست</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>مشتکی از نعمت جان بیحساب</p></div>
<div class="m2"><p>زهر به اندر تن آن بیحس آب</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>شکوه حق زد چو سر از نافقیر</p></div>
<div class="m2"><p>مشک وی آمد بدر از نافه قیر</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>کی شد ازین خوان دل فرو آتش جوی</p></div>
<div class="m2"><p>شکر کن امروزش و فرداش جوی</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>شکر اگر آید ز تو فرد آشکار</p></div>
<div class="m2"><p>کی بود آتش بتو فرداش کار</p></div></div>