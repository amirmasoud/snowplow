---
title: >-
    بخش ۱۱ - نکته در بیان وحی و الهام
---
# بخش ۱۱ - نکته در بیان وحی و الهام

<div class="b" id="bn1"><div class="m1"><p>ساقی از اغیار در امشب ببند</p></div>
<div class="m2"><p>رخنه آزار در امشب ببند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>امشب از آن ساغر می مایه بخش</p></div>
<div class="m2"><p>کش برد از تو دل بیمایه بخش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سر حق از محفل مستان طلب</p></div>
<div class="m2"><p>نزدل شیخ از دل دست آن طلب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>صد محلش پرده وز آن صد محال</p></div>
<div class="m2"><p>جز نبی آنجا ره کس خود محال</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حق پی آن پرده بر آن رخ نکرد</p></div>
<div class="m2"><p>دیده الهام در آن رخنه کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دیدن پیغمبر ازین دیده است</p></div>
<div class="m2"><p>ز آینه آن آینه بین دیده است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر تو ز الهام در آن جانبی</p></div>
<div class="m2"><p>محرم رازست در آنجا نبی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>صاحب وحیش در پیغام باز</p></div>
<div class="m2"><p>میدهد از وی خبر الهام باز</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هر چه از آن پرتو اشعار یافت</p></div>
<div class="m2"><p>عکسی از الهام در اشعار یافت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مه نبی و کوکب دیدن اهل بیت</p></div>
<div class="m2"><p>سایه روحی نبی این اهل بیت</p></div></div>