---
title: >-
    برگ سیم ده شمشیر است
---
# برگ سیم ده شمشیر است

<div class="b" id="bn1"><div class="m1"><p>ای کاهوی تو صید بود صد شیرش</p></div>
<div class="m2"><p>خورشید رخت کسی نه بیند سیرش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر کس که بکوی تو دمی جان گرفت</p></div>
<div class="m2"><p>زانجا نتوان راند به ده شمشیرش</p></div></div>