---
title: >-
    برگ ششم هفت شمشیر است
---
# برگ ششم هفت شمشیر است

<div class="b" id="bn1"><div class="m1"><p>ای آنکه دو آهوی تو بر شیر زنند</p></div>
<div class="m2"><p>چونست که ناوکی بمن دیر زنند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شاهنشه خوبانی و در بندگیت</p></div>
<div class="m2"><p>هفت اختر چرخ هفت شمشیر زنند</p></div></div>