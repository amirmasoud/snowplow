---
title: >-
    برگ نهم چهار چنگ است
---
# برگ نهم چهار چنگ است

<div class="b" id="bn1"><div class="m1"><p>ای کز ستم غم تو یک دل نرهد</p></div>
<div class="m2"><p>کس دل ندهد ترا که جان هم ندهد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از چنگ دو ابرو و دو چشمت نرهم</p></div>
<div class="m2"><p>یک صید ز چار چنگ بیرون نجهد</p></div></div>
<div class="m2"><p>یک صید ز چار چنگ بیرون نجهد</p></div></div>