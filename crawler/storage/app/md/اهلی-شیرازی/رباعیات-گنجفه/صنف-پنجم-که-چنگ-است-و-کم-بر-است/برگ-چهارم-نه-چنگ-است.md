---
title: >-
    برگ چهارم نه چنگ است
---
# برگ چهارم نه چنگ است

<div class="b" id="bn1"><div class="m1"><p>ای برده سبق جمالت از حسن ملک</p></div>
<div class="m2"><p>خطت ز دلم حرف طرب ساخته حک</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا چنگ بدامن وصالت نزنم</p></div>
<div class="m2"><p>خوشدل نشوم ز ساز نه چنگ فلک</p></div></div>
<div class="m2"><p>خوشدل نشوم ز ساز نه چنگ فلک</p></div></div>