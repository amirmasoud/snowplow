---
title: >-
    برگ یازدهم دو غلام است
---
# برگ یازدهم دو غلام است

<div class="b" id="bn1"><div class="m1"><p>ای آنکه بود حسن دل افروز ترا</p></div>
<div class="m2"><p>شاه همه کرد بخت فیروز ترا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نام شب و روز عنبرست و کافور</p></div>
<div class="m2"><p>یعنی دو غلام شب و روز ترا</p></div></div>