---
title: >-
    برگ چهارم نه غلام است
---
# برگ چهارم نه غلام است

<div class="b" id="bn1"><div class="m1"><p>ای آنکه چو من بسی اسیرند ترا</p></div>
<div class="m2"><p>شاهان همه در نظر حقیرند ترا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>برگرد تو گردند که آزاد شوند</p></div>
<div class="m2"><p>افلاک که نه غلام پیرند ترا</p></div></div>