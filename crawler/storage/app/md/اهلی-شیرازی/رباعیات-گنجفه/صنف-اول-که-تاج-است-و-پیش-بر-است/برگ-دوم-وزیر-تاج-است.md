---
title: >-
    برگ دوم وزیر تاج است
---
# برگ دوم وزیر تاج است

<div class="b" id="bn1"><div class="m1"><p>ای کز مه نو رکاب رخش تو بود</p></div>
<div class="m2"><p>خود صورت خوب جمله بخش تو بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>معموری ملک جانکه شاهش غم تست</p></div>
<div class="m2"><p>از لطف وزیر تاج بخش تو بود</p></div></div>