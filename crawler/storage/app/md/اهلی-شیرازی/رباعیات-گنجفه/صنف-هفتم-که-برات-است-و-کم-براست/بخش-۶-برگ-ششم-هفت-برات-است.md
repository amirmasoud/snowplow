---
title: >-
    بخش ۶ - برگ ششم هفت برات است
---
# بخش ۶ - برگ ششم هفت برات است

<div class="b" id="bn1"><div class="m1"><p>ای نامه زندگی خط اشرف تو</p></div>
<div class="m2"><p>دستور سپهر کمترین آصف تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خوشتر ز سپهر و سبعه سیاره</p></div>
<div class="m2"><p>بر هفت برات هفت مهر از کف تو</p></div></div>
<div class="m2"><p>بر هفت برات هفت مهر از کف تو</p></div></div>