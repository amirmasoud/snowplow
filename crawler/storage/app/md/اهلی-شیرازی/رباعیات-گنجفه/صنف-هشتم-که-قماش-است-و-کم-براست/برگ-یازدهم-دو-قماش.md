---
title: >-
    برگ یازدهم دو قماش
---
# برگ یازدهم دو قماش

<div class="b" id="bn1"><div class="m1"><p>ای کشته مرا بناله سینه خراش</p></div>
<div class="m2"><p>زین بیش بکین من دلخسته مباش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با رشته جان کس مباف این سر زلف</p></div>
<div class="m2"><p>خوش نیست که تار و پود باشد دو قماش</p></div></div>