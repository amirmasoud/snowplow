---
title: >-
    برگ هفتم شش قماش
---
# برگ هفتم شش قماش

<div class="b" id="bn1"><div class="m1"><p>ای از غم دهر همچو سرو آزاده</p></div>
<div class="m2"><p>کم چون تو پسر مادر گیتی زاده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در دست بتان ز بهر پای اندازت</p></div>
<div class="m2"><p>نرگس صفتست شش قماش آماده</p></div></div>