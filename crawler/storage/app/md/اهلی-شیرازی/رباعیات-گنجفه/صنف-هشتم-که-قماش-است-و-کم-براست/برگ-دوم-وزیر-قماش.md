---
title: >-
    برگ دوم وزیر قماش
---
# برگ دوم وزیر قماش

<div class="b" id="bn1"><div class="m1"><p>ای آنکه لب تو پرده غنچه درد</p></div>
<div class="m2"><p>رخسار تو آب صورت چین ببرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آنجا که زند شنهشه عشق تو تخت</p></div>
<div class="m2"><p>کی عقل وزیر را قماشی شمرد</p></div></div>