---
title: >-
    شمارهٔ ۲۲۱
---
# شمارهٔ ۲۲۱

<div class="b" id="bn1"><div class="m1"><p>دیوانه یارم من و با کس نظرم نیست</p></div>
<div class="m2"><p>مشغول خودم وز همه عالم خبرم نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من مست دل آشفته ام ای همدم مشفق</p></div>
<div class="m2"><p>خارم مکش از پای پروای سرم نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زخمی بود از عشق بهرمو که مرا هست</p></div>
<div class="m2"><p>با آنهمه از عشق نکویان حذرم نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای قبله حاجت زدرت رو بکه آرم؟</p></div>
<div class="m2"><p>زنهار مرانم که ازین در گذرم نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا خون جگر بود فرو ریختم از چشم</p></div>
<div class="m2"><p>رحمی بکن امروز که نم در جگرم نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زاهد دهدم توبه که کار تو صلاح است</p></div>
<div class="m2"><p>پنداشت مگر خواجه که کاری دگرم نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اهلی ز جهان قسمت هر کس زر و سیم است</p></div>
<div class="m2"><p>این قسمت من بس که غم سیم و زرم نیست</p></div></div>