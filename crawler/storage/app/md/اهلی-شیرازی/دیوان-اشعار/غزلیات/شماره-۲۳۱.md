---
title: >-
    شمارهٔ ۲۳۱
---
# شمارهٔ ۲۳۱

<div class="b" id="bn1"><div class="m1"><p>با خوش نفسی می خور اگر ماهوشی نیست</p></div>
<div class="m2"><p>آواز خوشی باری اگر حسن خوشی نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر روسیه از حسن تو گشتیم هم از ماست</p></div>
<div class="m2"><p>در آینه صورتگر چین و حبشی نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زاد منشین در صف میخانه که اینجا</p></div>
<div class="m2"><p>رندی است هنر کار بدستار کشی نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تلخ است مذاق غم و این چاشنی زهر</p></div>
<div class="m2"><p>جز من که شناسد که چو من زهر چشی نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هرچند فلک کینه کش از تلخی مرگ است</p></div>
<div class="m2"><p>هرگز بتر از زهر غمت کینه کشی نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از سینه صد چاک و صفای دل اهلی</p></div>
<div class="m2"><p>پیداست که در آینه اش هیچ غشی نیست</p></div></div>