---
title: >-
    شمارهٔ ۱۵
---
# شمارهٔ ۱۵

<div class="b" id="bn1"><div class="m1"><p>گر نامه بی‌نامِ خوشت بوسد زبان خامه را</p></div>
<div class="m2"><p>آن رو سیاهی بس بود تا روز محشر نامه را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دلدادهٔ نام توام از هرکه نامت بشنوم</p></div>
<div class="m2"><p>بر مژده نام خوشت هم جان دهم هم جامه را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من کشتهٔ خط توام کم ران به خون من قلم</p></div>
<div class="m2"><p>سرخی به خون من مده منقار زاغ خامه را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از یک نظر در کوی تو صد شیخ صنعان بت‌پرست</p></div>
<div class="m2"><p>رسوای عالم می‌کند عشق تو صد علّامه را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از حلقهٔ مستان تو خیل ملک بی‌بهره‌اند</p></div>
<div class="m2"><p>کز صحبت خاصان حق فیضی نباشد عامه را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اهلی سخن کوتاه کن افسانه‌گویی تا به کی؟</p></div>
<div class="m2"><p>هنگام خواب القصه شد بر هم زن این هنگامه را</p></div></div>