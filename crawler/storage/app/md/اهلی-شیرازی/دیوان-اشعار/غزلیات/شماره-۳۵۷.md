---
title: >-
    شمارهٔ ۳۵۷
---
# شمارهٔ ۳۵۷

<div class="b" id="bn1"><div class="m1"><p>حیات تشنه لبان وصل آن مسیح دم است</p></div>
<div class="m2"><p>که با وجود لبش آب زندگی عدم است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گناه کاسه زدن شیخ را چو غنچه نهان</p></div>
<div class="m2"><p>گناه ماست که چون لاله بر سر علم است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گدای پیر مغنم به خانقه چه روم؟</p></div>
<div class="m2"><p>اگرچه می همه جا هست بحث بر کرم است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز دلبران همه عاشق کشی ستم دانند</p></div>
<div class="m2"><p>ولی تو عاشق خود گر نمی کشی ستم است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بناز بر همه عالم ز حسن عالم سوز</p></div>
<div class="m2"><p>که گر بناز کشی عالمی هنوز کم است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز حسن روی تو فرق است تا پری بسیار</p></div>
<div class="m2"><p>اگرچه صورت این هر دو کار یک قلم است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بجان دوست که آزار اهلی است حرام</p></div>
<div class="m2"><p>سگ در تو مگر کم ز آهوی حرم است</p></div></div>