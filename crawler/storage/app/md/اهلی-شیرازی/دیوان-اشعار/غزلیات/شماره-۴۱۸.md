---
title: >-
    شمارهٔ ۴۱۸
---
# شمارهٔ ۴۱۸

<div class="b" id="bn1"><div class="m1"><p>مستان تو گر باغ و بهاری طلبیدند</p></div>
<div class="m2"><p>از درد سر خلق کناری طلبیدند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هرکس که درین بادیه کشته چو مجنون</p></div>
<div class="m2"><p>هر پاره او در سر خاری طلبیدند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>راه همه در معرکه عشق بتان نیست</p></div>
<div class="m2"><p>کاین قوم ز صد خیل سواری طلبیدند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>می باد گران خور که مرا پاس تو کارست</p></div>
<div class="m2"><p>در کوی تو هرکس پی کاری طلبیدند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بی روی تو مشنو که بشمع مه و خورشید</p></div>
<div class="m2"><p>از اهلی گمگشته غباری طلبیدند</p></div></div>