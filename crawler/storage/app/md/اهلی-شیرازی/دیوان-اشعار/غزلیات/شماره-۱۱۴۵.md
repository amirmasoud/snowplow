---
title: >-
    شمارهٔ ۱۱۴۵
---
# شمارهٔ ۱۱۴۵

<div class="b" id="bn1"><div class="m1"><p>گر شوم خاک ره و سبزه دمد از گل من</p></div>
<div class="m2"><p>حسرت سبزه خطت نرود از دل من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای چراغ نظر و شمع شبستان همه</p></div>
<div class="m2"><p>یکشب از شمع رخ افروخته کن محفل من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>قصه درد دل از داغ تو تا کی گویم</p></div>
<div class="m2"><p>آه ازین درد دل و وای ز داغ دل من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دانه خال تو حاصل نشد از خرمن عمر</p></div>
<div class="m2"><p>گو ببر باد فنا خرمن بیحاصل من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مگذر از من که سرم کوی تو زان منزل ساخت</p></div>
<div class="m2"><p>که فتد سایه سرو تو بسر منزل من</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ساقی امشب مه من مست و سر افشان سازش</p></div>
<div class="m2"><p>که بمستی مگر آنسرو شود مایل من</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>رخت در کعبه مقصود کشم چون اهلی</p></div>
<div class="m2"><p>گر ببندد اجل از کوی بتان محمل من</p></div></div>