---
title: >-
    شمارهٔ ۱۳۷۶
---
# شمارهٔ ۱۳۷۶

<div class="b" id="bn1"><div class="m1"><p>سوختم چند چو برق آیی و چون باد روی</p></div>
<div class="m2"><p>من بصد فتنه گرفتار و تو آزادی روی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>میکنی جور و جفا تا نکنم یاد تو لیک</p></div>
<div class="m2"><p>تو نه آنی که بجور و ستم از یاد روی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مردم از ناله مردم بکسان جور مکن</p></div>
<div class="m2"><p>هم مرا سوز اگر از پی بیداد روی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ایخوش آندم که چو بازیچه طفلان گشتم</p></div>
<div class="m2"><p>سوی من خنده زنان آیی و دلشاد روی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از پی پرسش اهلی چه کنی رنجه قدم</p></div>
<div class="m2"><p>ترسم آزرده ز بس ناله و فریاد روی</p></div></div>