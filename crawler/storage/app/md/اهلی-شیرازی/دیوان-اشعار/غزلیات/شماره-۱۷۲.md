---
title: >-
    شمارهٔ ۱۷۲
---
# شمارهٔ ۱۷۲

<div class="b" id="bn1"><div class="m1"><p>گر بسوزد تن زارم بر من خار و خسی است</p></div>
<div class="m2"><p>غم دل می خورم اما که دلم آن کسی است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بسکه شبها من دیوانه بخود در سخنم</p></div>
<div class="m2"><p>هست همسایه گمانش که مرا همنفسی است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای اجل جان مرا بخش بدان زلف سیاه</p></div>
<div class="m2"><p>رانکه هر رشته جان بسته دام هوسی است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>باغبان، مرغ دل من ننشیند بچمن</p></div>
<div class="m2"><p>بگشا در که چمن بر دل تنگم قفسی است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای شکر لب زبر خویش مران اهلی را</p></div>
<div class="m2"><p>زانکه هرجا شکری هست بگردش مگسی است</p></div></div>