---
title: >-
    شمارهٔ ۷۲۹
---
# شمارهٔ ۷۲۹

<div class="b" id="bn1"><div class="m1"><p>شد سگش یار رقیب و جور با خود می‌کند</p></div>
<div class="m2"><p>با بدان هر کو نکویی می‌کند بد می‌کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سرو من در باغ و نگشاید در من باغبان</p></div>
<div class="m2"><p>باغبان هم ناز بر من زان سهی‌قد می‌کند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در بیابان عدم سر می‌نهم دیوانه‌وار</p></div>
<div class="m2"><p>زلف چون زنجیر او بازم مقید می‌کند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>می زدن کی از سفال آن سگ کو حد ماست</p></div>
<div class="m2"><p>ما چه سگ باشیم او خود لطف بی‌حد می‌کند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سر بنه بر آستان او به امید قبول</p></div>
<div class="m2"><p>گر قبولت می‌کند اهلی و گر رد می‌کند</p></div></div>