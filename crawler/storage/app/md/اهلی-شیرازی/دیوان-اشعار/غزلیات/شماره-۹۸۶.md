---
title: >-
    شمارهٔ ۹۸۶
---
# شمارهٔ ۹۸۶

<div class="b" id="bn1"><div class="m1"><p>تا کی از گریه گره بر لب فریاد زنم</p></div>
<div class="m2"><p>سوختم چند گره بیهده بر باد زنم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>منکه از طالع شوریده خود در بدرم</p></div>
<div class="m2"><p>از که نالم چکنم پیش که فریاد زنم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>غم دل چند خورم کی بود آنروز که من</p></div>
<div class="m2"><p>بفراغت نفسی با دل آزاد زنم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر گهم صورت شیرین نفسی پیش آید</p></div>
<div class="m2"><p>ای بسا آه که بر حسرت فرهاد زنم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اهلی آن غنچه دهن کار بمن تنگ گرفت</p></div>
<div class="m2"><p>وقت آن شد که قدم در عدم آباد زنم</p></div></div>