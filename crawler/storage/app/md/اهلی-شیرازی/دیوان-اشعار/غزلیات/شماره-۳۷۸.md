---
title: >-
    شمارهٔ ۳۷۸
---
# شمارهٔ ۳۷۸

<div class="b" id="bn1"><div class="m1"><p>ترا صد خوبی و بر هر یکی صد دیده حیرانت</p></div>
<div class="m2"><p>مرا یک جان و می‌خواهم شوم صد بار قربانت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قیامت در صباح حشر باشد وه چه حال است این</p></div>
<div class="m2"><p>که در هر صبح برخیزد قیامت از گریبانت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به خونخواهی عنانت را که گیرد آفتاب من</p></div>
<div class="m2"><p>عجب گر روز محشر هم رسد دستی به دامانت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>که باشم من که در سر باشدم سودای وصل تو</p></div>
<div class="m2"><p>سگ کویم سری دارم فدای پای دربانت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بود شیرین‌تر از جان تلخی مرگم دم مردن</p></div>
<div class="m2"><p>اگر پیش نظر باشد مرا لب‌های خندانت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سرم بادا فدای خاک پای شهسوار خود</p></div>
<div class="m2"><p>مرا جانی بود آن هم فدای دردمندانت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به چشمت ای کمان‌ابرو نگه گر می‌کند آهو</p></div>
<div class="m2"><p>به هر مو می‌خورد خاری ز ناوک‌های مژگانت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دل اهلی به نور عشق آبادان کن ای گردون</p></div>
<div class="m2"><p>که خاک ره شمارد گنج‌های ملک ویرانت</p></div></div>