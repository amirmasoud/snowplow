---
title: >-
    شمارهٔ ۳۷۲
---
# شمارهٔ ۳۷۲

<div class="b" id="bn1"><div class="m1"><p>نه عاشق است که واله بر وی چون مه تست</p></div>
<div class="m2"><p>که مرده متحرک چو سایه همره تست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>طبیب من بتغافل نپرسی احوالم</p></div>
<div class="m2"><p>وگرنه حال که فوت از ضمیر آگه تست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>برون خرام و برحمت ز خاک ره برگیر</p></div>
<div class="m2"><p>سر سپهر که بر آستان خرگه تست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر آستان تو تنها سر بتان نبود</p></div>
<div class="m2"><p>که تکیه گاه مه و مهر خشت درگه تست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بدیر و صومعه ایدل کسی ندارد خواب</p></div>
<div class="m2"><p>ز بسکه شب همه شب ذکر الله الله تست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تو کی رسی بته کار جام می ای عقل</p></div>
<div class="m2"><p>برو که درد سر ما ز فکر بی ته تست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>امید وصل ببر اهلی از قد آن سرو</p></div>
<div class="m2"><p>که مرد بخت بلندش نه دست کوته تست</p></div></div>