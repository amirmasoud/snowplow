---
title: >-
    شمارهٔ ۱۱۸۸
---
# شمارهٔ ۱۱۸۸

<div class="b" id="bn1"><div class="m1"><p>ایدل به غم بساز و وصالش طلب مکن</p></div>
<div class="m2"><p>ترک مراد خود کن و ترک ادب مکن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ساقی بیا که خواب شب واپسین بس است</p></div>
<div class="m2"><p>هشدار و روز عیش به بیهوده شب مکن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زین آه پر شرر که بر افلاک میرود</p></div>
<div class="m2"><p>آتش اگر ز چرخ ببارد عجب مکن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در بزم غم بمستی و محنت دلا بساز</p></div>
<div class="m2"><p>آلوده خویش را بشراب طرب مکن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اهلی مراد خویش ز رندان دیر خواه</p></div>
<div class="m2"><p>از زاهدان صومعه همت طلب مکن</p></div></div>