---
title: >-
    شمارهٔ ۱۸۵
---
# شمارهٔ ۱۸۵

<div class="b" id="bn1"><div class="m1"><p>هر کرا حسنی بود آیینه دار روی اوست</p></div>
<div class="m2"><p>هرکه دارد بوی عشقی از سگان کوی اوست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فتنه پیران نه تنها شد که طفل مکتبی</p></div>
<div class="m2"><p>چون الف گوید مرادش قامت دلجوی اوست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عشق خود یاری دهد یعنی که کار کوهکن</p></div>
<div class="m2"><p>قوت بازوی عشق است این نه از بازوی اوست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دیدنش جان بخشد اما زهر چشمش میکشد</p></div>
<div class="m2"><p>زهر و تریاکی عجب با نرگش جادوی اوست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گاه گاه از شرم مردم روی ازو پوشم ولی</p></div>
<div class="m2"><p>تا نظر در خود کنم بینم که چشمم سوی اوست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مست آن چشمند اهلی، نوغر الان جهان</p></div>
<div class="m2"><p>وه که هر جا هست صیادی سگ آهوی اوست</p></div></div>