---
title: >-
    شمارهٔ ۱۰۷۲
---
# شمارهٔ ۱۰۷۲

<div class="b" id="bn1"><div class="m1"><p>عاشق مستم و محنت زده از بار دلم</p></div>
<div class="m2"><p>دوستان عفو کنیدم که گرفتار دلم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همه کس در پی تیمار و دل من ز جنون</p></div>
<div class="m2"><p>سنگ بر سینه زنان در پی آزار دلم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خار خار دلم از زخم زبان کم نشود</p></div>
<div class="m2"><p>مگر آن سوزن مژگان بکشد خار دلم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دل خرید از کف غم بازم و بفروخت بدوست</p></div>
<div class="m2"><p>خود فروشی نکنم بنده بازار دلم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عیب من از رخ معشوق نشاید اهلی</p></div>
<div class="m2"><p>که مرا خود هنر اینست که در کار دلم</p></div></div>