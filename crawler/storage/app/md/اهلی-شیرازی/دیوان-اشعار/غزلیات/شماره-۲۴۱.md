---
title: >-
    شمارهٔ ۲۴۱
---
# شمارهٔ ۲۴۱

<div class="b" id="bn1"><div class="m1"><p>چنین که تشنه بخون لعل یاربی سبب است</p></div>
<div class="m2"><p>هلاک ما عجبی نیست زندگی عجب است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من از ادب نشمارم سگ درت خود را</p></div>
<div class="m2"><p>که آدمش نشمارند هرکه بی ادب است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگرچه خاک شدم رخ متاب ای خورشید</p></div>
<div class="m2"><p>که ذره ذره غبارم هنوز در طلب است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به مجلسی که بود هزار دل پرخون</p></div>
<div class="m2"><p>چه جای ساغر عیش و پیاله طرب است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز استخوان چه عجب در رطب کزوما را</p></div>
<div class="m2"><p>بجای مغز ز پیکان در استخوان رطب است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>فکند از آن سخنت شور در عجم اهلی</p></div>
<div class="m2"><p>که چاشنی حدیث تو از شه عرب است</p></div></div>