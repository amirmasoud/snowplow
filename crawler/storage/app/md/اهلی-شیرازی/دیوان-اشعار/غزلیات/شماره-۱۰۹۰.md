---
title: >-
    شمارهٔ ۱۰۹۰
---
# شمارهٔ ۱۰۹۰

<div class="b" id="bn1"><div class="m1"><p>ز خونم سیر کی گردد که با لعلش نظر دارم</p></div>
<div class="m2"><p>ز چشمم می‌کشد تا قطره‌ای خون در جگر دارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مکن منع از سجود خود مرا ای سرو چندانی</p></div>
<div class="m2"><p>که در راهت رخ زردی نهم بر خاک و بردارم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو رو در قبله می‌آرم سجودم بهر آن باشد</p></div>
<div class="m2"><p>که از محراب ابرویت خیالی در نظر دارم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کجایی آفتاب من که شب تا روز در راهت</p></div>
<div class="m2"><p>به جان کندن چراغ دیده در راه سحر دارم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>طبیبا حال اگر پرسی که دردم را کنی درمان</p></div>
<div class="m2"><p>برو درد سرم کم ده که من دردی دگر دارم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو ناصح در زبان آید مرا موی از بدن خیزد</p></div>
<div class="m2"><p>کزان تیغ زبان در دل هزاران نیشتر دارم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نه تنها از خیال زلف او مویی شدم اهلی</p></div>
<div class="m2"><p>که دستی با خیال موی آن هم در کمر دارم</p></div></div>