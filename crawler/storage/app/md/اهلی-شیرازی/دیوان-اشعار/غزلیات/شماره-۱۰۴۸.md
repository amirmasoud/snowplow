---
title: >-
    شمارهٔ ۱۰۴۸
---
# شمارهٔ ۱۰۴۸

<div class="b" id="bn1"><div class="m1"><p>آن عید مشتاقان که من قربان او صد جان کنم</p></div>
<div class="m2"><p>یکبار اگر نامش برم صد بار جان قربان کنم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من بت پرست و عاشقم اما نمی گویم بکس</p></div>
<div class="m2"><p>شرط است کز نامحرمان عشق بتان پنهان کنم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پروانه وارم بی زبان جانرا بخاموشی دهم</p></div>
<div class="m2"><p>گلبانگ شهرت میزنم چون بلبلان افغان کنم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفتم که بیروی توام دشوار باشد زندگی</p></div>
<div class="m2"><p>گفتا بیک تیر نظر دشواریت آسان کنم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با آنکه صد چاکم بود در سینه و لب بسته ام</p></div>
<div class="m2"><p>چون غنچه در خونم کشد هرگه لبی خندان کنم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون دردمندان غمت دردی بصد جان میخرند</p></div>
<div class="m2"><p>بیدرد باشم من اگر درد ترا درمان کنم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پیرم چو اهلی ایجوان امروز میدان زان تست</p></div>
<div class="m2"><p>پیش آ که من چوگان صفت سردر سر میدان کنم</p></div></div>