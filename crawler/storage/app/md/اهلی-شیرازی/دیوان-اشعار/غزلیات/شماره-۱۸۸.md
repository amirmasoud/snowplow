---
title: >-
    شمارهٔ ۱۸۸
---
# شمارهٔ ۱۸۸

<div class="b" id="bn1"><div class="m1"><p>گرنه بر غم عاشقان کار جهان مشوش است</p></div>
<div class="m2"><p>خار چراست یار گل مرغ چرا در آتش است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>داغ درون چو لاله ام هیچ نگه نمیکنی</p></div>
<div class="m2"><p>این نگری که ظاهرم چهره بخون منقش است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جانب کعبه ام مخوان کعبه کجا و من کجا</p></div>
<div class="m2"><p>قبله من که عاشقم روی بتان مهوش است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر محک وفا زدم روی جهانیان همه</p></div>
<div class="m2"><p>نیست بغیر جام می همنفسی که بی غش است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اهلی،از آن نفس که شد بنده کوی نیکویان</p></div>
<div class="m2"><p>از همه غصه شادمان با همه ناخوشی خوش است</p></div></div>