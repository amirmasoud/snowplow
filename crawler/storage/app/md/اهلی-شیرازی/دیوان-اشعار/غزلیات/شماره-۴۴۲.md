---
title: >-
    شمارهٔ ۴۴۲
---
# شمارهٔ ۴۴۲

<div class="b" id="bn1"><div class="m1"><p>من اگر حسرت روی تو چنین خواهم برد</p></div>
<div class="m2"><p>حسرت روی زمین زیر زمین خواهم برد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر به جنت ره من با دل پر آه دهند</p></div>
<div class="m2"><p>دوزخی را بسوی خلد برین خواهم برد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>غیرت دین اگرم پرده ز نار کشد</p></div>
<div class="m2"><p>بس خجالت که من بیدل و دین خواهم برد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کی ازین سودن رخ بر در مسجد من مست</p></div>
<div class="m2"><p>سرنوشت ازل از لوح جبین خواهم برد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>من نه آنم که چو اهلی پی معراج مراد</p></div>
<div class="m2"><p>منت همرهی از روح امین خواهم برد</p></div></div>