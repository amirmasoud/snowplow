---
title: >-
    شمارهٔ ۱۵۹
---
# شمارهٔ ۱۵۹

<div class="b" id="bn1"><div class="m1"><p>دوستان خواب اجل بی وصل یارم آرزوست</p></div>
<div class="m2"><p>بیقرارم دور ازو یکدم قرارم آرزوست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کار او جور است بامن جان بنومیدی کنم</p></div>
<div class="m2"><p>ورنه چون شد مهربان بوس و کنارم آرزوست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زندگی می بایدم چندانکه میرم پیش او</p></div>
<div class="m2"><p>ورنه دیگر بهر چه کارم آرزوست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اختیار از دست از آن دادم بمستی ای حریف</p></div>
<div class="m2"><p>کامشب از می گریه بی اختیارم آرزوست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حال من اهلی نگر کز درد آن نامهربان</p></div>
<div class="m2"><p>آرزو دارم بمرگ و صد هزارم آرزوست</p></div></div>