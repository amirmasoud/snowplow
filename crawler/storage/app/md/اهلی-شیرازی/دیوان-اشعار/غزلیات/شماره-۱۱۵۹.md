---
title: >-
    شمارهٔ ۱۱۵۹
---
# شمارهٔ ۱۱۵۹

<div class="b" id="bn1"><div class="m1"><p>ای تو بروی همچو مه چشم و چراغ عاشقان</p></div>
<div class="m2"><p>راحت جان بیدلان مرهم داغ عاشقان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روشنی دو دیده یی، مونس دل رمیده یی</p></div>
<div class="m2"><p>تازه بهار این چمن نوگل باغ عاشقان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بسکه ز دیدن رخت سیر نمیشود نظر</p></div>
<div class="m2"><p>یکنفس از نظاره ات نیست فراغ عاشقان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گرنه نسیم رحمتت روز جزا رسد بخلق</p></div>
<div class="m2"><p>بوی بهشت کی کند تازه دماغ عاشقان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اهلی از آفت فنا غم نخوریم تا ابد</p></div>
<div class="m2"><p>کز رخ شمع ما بود زنده چراغ عاشقان</p></div></div>