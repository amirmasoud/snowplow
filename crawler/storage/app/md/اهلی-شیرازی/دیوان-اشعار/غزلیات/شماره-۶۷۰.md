---
title: >-
    شمارهٔ ۶۷۰
---
# شمارهٔ ۶۷۰

<div class="b" id="bn1"><div class="m1"><p>از آن در دیده یعقوبش غم یوسف غبار آرد</p></div>
<div class="m2"><p>که عشق آموختن پیرانه‌سر کوری به یار آرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مکش ای مست ناز امروز مارا فکر فردا کن</p></div>
<div class="m2"><p>که درد سر نیرزد مستی کاخر خمار آرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تو خود بگشادی دری ای گل و گرنه باغبان هرگز</p></div>
<div class="m2"><p>نمیخواهد که بویی سوی ما باد بهار آرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بتلخی کوهکن در بیستون گر جان دهد آخر</p></div>
<div class="m2"><p>بیابد جان شیرین باز اگر شیرین گذار آرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تو خورشیدی و ما ذره حساب از ما چه بر گیری</p></div>
<div class="m2"><p>که در جمع هواداران که ما را در شمار آرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز عشقت من وفا جستم نه خار از مدعی خوردن</p></div>
<div class="m2"><p>چه دانستم که تخم مهر کشتن خار بار آرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو اهلی بلبل باغ توام خوارم مکن ای گل</p></div>
<div class="m2"><p>که نادر این چمن مرغی چو من گر صدهزار آرد</p></div></div>