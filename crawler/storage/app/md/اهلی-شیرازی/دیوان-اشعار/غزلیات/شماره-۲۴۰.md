---
title: >-
    شمارهٔ ۲۴۰
---
# شمارهٔ ۲۴۰

<div class="b" id="bn1"><div class="m1"><p>قد طوبی و لب کوثر و خود حور بهشت است</p></div>
<div class="m2"><p>یارب ملک است آدمی این چه بهشت است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جز برق محبت نبود آتش موسی</p></div>
<div class="m2"><p>گر از سر طورست و گر از کنج کنشت است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آن لعل روان بخش بود یا خط نوخیز</p></div>
<div class="m2"><p>یا چشمه آبی که روان از لب کشت است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پیش رخ خوبش که ملک را نمکی نیست</p></div>
<div class="m2"><p>از حسن پری هیچ مگویید که زشت است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خشت سرخم بس بودم بالش راحت</p></div>
<div class="m2"><p>بالین چکنم من که سرم لایق خشت است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>این نیز هم از طینت پاک است که اهلی</p></div>
<div class="m2"><p>آمیخته با مهر تو ای حور سرشت است</p></div></div>