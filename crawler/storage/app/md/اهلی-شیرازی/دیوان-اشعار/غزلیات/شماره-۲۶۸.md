---
title: >-
    شمارهٔ ۲۶۸
---
# شمارهٔ ۲۶۸

<div class="b" id="bn1"><div class="m1"><p>بود از ازلم درد تو وین درد همان است</p></div>
<div class="m2"><p>عشقت ز ازل هرکه رقم کرد همان است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هرچند بهار آمد و ایام خزان رفت</p></div>
<div class="m2"><p>عشاق ترا رنگ رخ زرد همان است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آمد شب و آسوده خوابند جهانی</p></div>
<div class="m2"><p>فریاد و فغان سگ شبگرد همان است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>من زنده و تو سوخته یی مدعیان را</p></div>
<div class="m2"><p>بر آینه خاطر من گرد همان است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اهلی بود از روز ازل مرد غم عشق</p></div>
<div class="m2"><p>گر روز نه آنست ولی مرد همان است</p></div></div>