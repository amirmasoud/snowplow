---
title: >-
    شمارهٔ ۳۵۲
---
# شمارهٔ ۳۵۲

<div class="b" id="bn1"><div class="m1"><p>آن سرو هرگه از نظر من گذشته است</p></div>
<div class="m2"><p>گویی که تیری از جگر من گذشته است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دامان ناز بر زده چون سرو میرود</p></div>
<div class="m2"><p>گویی ز جوی چشم تر من گذشته است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر بیستون ز تیشه فرهاد کی گذشت</p></div>
<div class="m2"><p>آنها که از غمش بسر من گذشته است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون سایه سوخت از اثر آتش دلم</p></div>
<div class="m2"><p>خورشید اگر برهگذر من گذشته است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اهلی مگو حذر کن از آن شوخ سنگدل</p></div>
<div class="m2"><p>اکنون که کار از حذر من گذشته است</p></div></div>