---
title: >-
    شمارهٔ ۴۶۷
---
# شمارهٔ ۴۶۷

<div class="b" id="bn1"><div class="m1"><p>فصل بهار و خلق بعشرت نشسته اند</p></div>
<div class="m2"><p>مارا چو لاله ساغر عشرت شکسته اند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیرون خرم ام ای گل خندان که در چمن</p></div>
<div class="m2"><p>آیین نوبهار بیاد تو بسته اند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از خاک کشتگان غمت لاله می دمد</p></div>
<div class="m2"><p>یعنی هنوز ز آتش داغت نرسته اند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بیچاره عاشقان که ز دست تو روی زرد</p></div>
<div class="m2"><p>همچون گل دو رنگ بخوناب شسته اند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اهلی که مرغ هر چمنی بود شد کنون</p></div>
<div class="m2"><p>ز آنها که پا شکسته بکنجی نشسته اند</p></div></div>