---
title: >-
    شمارهٔ ۱۳۰۲
---
# شمارهٔ ۱۳۰۲

<div class="b" id="bn1"><div class="m1"><p>وقت آن شد که نظر درمن درمانده کنی</p></div>
<div class="m2"><p>تلخی عیش مرا چاره بیک خنده کنی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هیچ نقصان نبود قدر ترا ایشه حسن</p></div>
<div class="m2"><p>گر نگاهی سوی درویش کهن ژنده کنی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من که باشم که بدل کینه من راه دهی</p></div>
<div class="m2"><p>بهر من خاطر خود چند پراکنده کنی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شمع من خنده زنان چهره برافروز دگر</p></div>
<div class="m2"><p>تا چراغ دل صد سوخته را زنده کنی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اهلی از تیغ تو چون سرکشد امید که تو</p></div>
<div class="m2"><p>از خداوندی خود رحم بر این بنده کنی</p></div></div>