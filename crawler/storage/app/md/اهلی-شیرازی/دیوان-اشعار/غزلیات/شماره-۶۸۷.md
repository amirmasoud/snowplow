---
title: >-
    شمارهٔ ۶۸۷
---
# شمارهٔ ۶۸۷

<div class="b" id="bn1"><div class="m1"><p>گر آه کشم آن سگ کویم بسر آید</p></div>
<div class="m2"><p>کز آه من سوخته بوی جگر آید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از ضعف چنانم که اگر ناله کشد دل</p></div>
<div class="m2"><p>بیهوش شوم تا نفسی چند بر آید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دایم برهت پیش صبا دیده گشایم</p></div>
<div class="m2"><p>باشد که دمی گردی از آن رهگذر آید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از درد تو چون ناله کنم خلق بگریند</p></div>
<div class="m2"><p>هر ناله که از درد بود کار گر آید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جایی که کشی تیغ پی قتل محبان</p></div>
<div class="m2"><p>اهلی اگرش ره بود اینجا بسر آید</p></div></div>