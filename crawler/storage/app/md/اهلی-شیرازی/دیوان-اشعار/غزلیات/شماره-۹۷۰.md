---
title: >-
    شمارهٔ ۹۷۰
---
# شمارهٔ ۹۷۰

<div class="b" id="bn1"><div class="m1"><p>من سوخته خال و خط سیمبرانم</p></div>
<div class="m2"><p>پروانه شمع رخ زیبا پسرانم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شرح غم پنهان چه دهم زانکه چو لاله</p></div>
<div class="m2"><p>پیداست که از وادی خونین جگرانم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دارم نظر پاک اگر هیچ ندارم</p></div>
<div class="m2"><p>غافل مشو از من که ز صاحب نظرانم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>این سلطنت و ملک مرا بس که همه عمر</p></div>
<div class="m2"><p>در گوشه میخانه بمستی گذرانم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اهلی اگر آنمه دگران را کشد از ناز</p></div>
<div class="m2"><p>خود را بکشم من که نه کم از دگرانم</p></div></div>