---
title: >-
    شمارهٔ ۱۲۲۷
---
# شمارهٔ ۱۲۲۷

<div class="b" id="bn1"><div class="m1"><p>پیش پای خود ببین شاد از غم مردم مشو</p></div>
<div class="m2"><p>چاه در راه است چون یوسف بخوبی گم مشو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در بهشت آدم بیک گندم چو ارزانی نبود</p></div>
<div class="m2"><p>از جهان گو حاصل ما نیز یک گندم مشو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سرزنش کم کن که نتوان گفت مور خسته را</p></div>
<div class="m2"><p>کز سمند سرکشان فرسوده زیر سم مشو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ساقیا چون نقد هستی میرود آخر ز دست</p></div>
<div class="m2"><p>جان من تا میدهد دستت ز پای خم مشو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خرقه را صوفی به می کن صاف اگر دل‌زنده‌ای</p></div>
<div class="m2"><p>مرده پشمینه‌ای چون کرم ابریشم مشو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حاصل دنیا نباشد هیچ غیر از مردمی</p></div>
<div class="m2"><p>با شک او خوش برآ اهلی و نامردم مشو</p></div></div>