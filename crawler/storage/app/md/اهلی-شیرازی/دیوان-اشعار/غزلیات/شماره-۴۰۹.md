---
title: >-
    شمارهٔ ۴۰۹
---
# شمارهٔ ۴۰۹

<div class="b" id="bn1"><div class="m1"><p>ساقی که یار ما بود اغیار را چه بحث؟</p></div>
<div class="m2"><p>جایی که گل حریف شود خار را چه بحث؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مشتاق دیدنت چکند چشمه حیات</p></div>
<div class="m2"><p>با آب خضر تشنه دیدار را چه بحث؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نتوان خیال روی تو دیدن مگر بخواب</p></div>
<div class="m2"><p>در این خیال دیده بیدار را چه بحث؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خضر و مسیح را ندهند از لب تو می</p></div>
<div class="m2"><p>با این حدیث عاشق بیمار را چه بحث؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اهلی کجا و گشت سر کوی او کجا</p></div>
<div class="m2"><p>با طرف باغ مرغ گرفتار را چه بحث؟</p></div></div>