---
title: >-
    شمارهٔ ۵۶۰
---
# شمارهٔ ۵۶۰

<div class="b" id="bn1"><div class="m1"><p>آن گل چو شمع بر من مدهوش میزند</p></div>
<div class="m2"><p>خونم ز شوق تیغ دگر جوش میزند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون شمع هرکه سوخته شد دل نهد به نیش</p></div>
<div class="m2"><p>خام است عاشقی که دم از نوش میزند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هم خود مگر ز لطف بفریاد من رسد</p></div>
<div class="m2"><p>زین زخمها که بر من خاموش میزند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفتی مرو ز هوش بوصلش، چنان کنم</p></div>
<div class="m2"><p>کاول بیک کرشمه ره هوش میزند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دام ره است خرقه صد پاره هوش دار</p></div>
<div class="m2"><p>کان خرقه پوش راه قبا پوش میزند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اهلی مگو که که کشت مرا در گوش او</p></div>
<div class="m2"><p>کاین نکته پهلویی به بنا گوش میزند</p></div></div>