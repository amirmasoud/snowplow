---
title: >-
    شمارهٔ ۴۱
---
# شمارهٔ ۴۱

<div class="b" id="bn1"><div class="m1"><p>گر التفات بود شمع مجلس مارا</p></div>
<div class="m2"><p>به کیمیای نظر زر کند مس مارا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مرو ز دیده که نقش بنفشه خالت</p></div>
<div class="m2"><p>بجای مردم چشم است نرگس مارا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تو خود بگو صفت حسن خود که دوزخ تو</p></div>
<div class="m2"><p>چه جای فهم بود عقل بی حس مارا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بغیر علم نظر درس ما نگفت استاد</p></div>
<div class="m2"><p>نبود علمی ازین به مدرس مارا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مگو که بتکده از چیست خانه دل تو</p></div>
<div class="m2"><p>که طرح کار چنین شد مهندس مارا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بزرگوار خدایا مراد ما این است</p></div>
<div class="m2"><p>که یار کس نکنی یار و مونس مارا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در آبخلوت اهلی که مجلس انس است</p></div>
<div class="m2"><p>ز شمع چهره برافروز مجلس مارا</p></div></div>