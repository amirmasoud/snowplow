---
title: >-
    شمارهٔ ۳۴۲
---
# شمارهٔ ۳۴۲

<div class="b" id="bn1"><div class="m1"><p>آتشی دردل من شمع رخت درزده است</p></div>
<div class="m2"><p>که بهر مو زتنم درد دلی سر زده است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از پریشانیم ایشوخ چه پرسی که فراق</p></div>
<div class="m2"><p>همچو رلف تو مرا کار بهم بر زده است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نامه شوق تو هر مرغ که آورده بمن</p></div>
<div class="m2"><p>پای در خون دلم همچو کبوتر زده است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نفس باد صبا تا خبری از تو رساند</p></div>
<div class="m2"><p>هر نفس در دل من آتش دیگر زده است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یک نفس سایه فکن بر سرم ای فر همای</p></div>
<div class="m2"><p>کز هوای تو بسی مرغ دلم پر زده است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زر رخساره اهلی برواج است ز عشق</p></div>
<div class="m2"><p>خاصه کز مهر و وفا سکه بر آن زر زده است</p></div></div>