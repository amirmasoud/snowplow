---
title: >-
    شمارهٔ ۲۱۶
---
# شمارهٔ ۲۱۶

<div class="b" id="bn1"><div class="m1"><p>تاخار عشق در جگر من شکسته است</p></div>
<div class="m2"><p>هر گل که هست در نظر من شکسته است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شوخی که بسته بود در از ناز بر همه</p></div>
<div class="m2"><p>مست آمدست دوش و در من شکسته است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هرجا که شیشه دل پر خون عاشقی است</p></div>
<div class="m2"><p>آن سنگدل برهگذر من شکسته است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>غوغای عشق بر در و بامش ز دیگران</p></div>
<div class="m2"><p>سنگ غم از میانه سر من شکسته است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>من چون خمیده پشت نگردم که در فراق</p></div>
<div class="m2"><p>بار چو کوه او کمر من شکسته است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دی خنده کرد آن بت و گفتا شکر فروش</p></div>
<div class="m2"><p>اینست کز لبش شکر من شکسته است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اهلی بشاخ وصل چو بلبل کجا رسم</p></div>
<div class="m2"><p>کز سنگ جور بال و پر من شکسته است</p></div></div>