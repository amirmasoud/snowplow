---
title: >-
    شمارهٔ ۵۲۳
---
# شمارهٔ ۵۲۳

<div class="b" id="bn1"><div class="m1"><p>بر من از دورش به نوبت ساغر می می‌رسد</p></div>
<div class="m2"><p>نوبت ما یارب از دور فلک کی می‌رسد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مرهمی تا کی رسد بر ریش دل از لعل او</p></div>
<div class="m2"><p>حالیا زان غمزه‌ام زخم پیاپی می‌رسد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عاشق لب‌تشنه را از ساقی دور فلک</p></div>
<div class="m2"><p>دیده پرخون می‌شود تا جرعه‌ای می می‌رسد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در تحمل جان سخت من چو سنگ خاره است</p></div>
<div class="m2"><p>زین همه سختی که بر جان من از وی می‌رسد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>باد اگر بر استخوانم می‌وزد از سوز دل</p></div>
<div class="m2"><p>آتش سوزنده را مانَد که بر نی می‌رسد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>صبر کن از زخم دل اهلی که آن ابروکمان</p></div>
<div class="m2"><p>مرهمی گر می‌نهد صد ناوک از پی می‌رسد</p></div></div>