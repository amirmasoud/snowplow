---
title: >-
    شمارهٔ ۱۳۵۲
---
# شمارهٔ ۱۳۵۲

<div class="b" id="bn1"><div class="m1"><p>صوفی اگر حریف ما بهر شراب میشوی</p></div>
<div class="m2"><p>تا نچشیده یی برو ورنه خراب میشوی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بسکه چو باده خون ما شب همه شب همیخوری</p></div>
<div class="m2"><p>چون گل تازه صبحدم مست شراب میشوی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ایکه طبیبی از سرم بگذر و حال من مپرس</p></div>
<div class="m2"><p>آتش من بهم مزن ورنه کباب میشوی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از هوس لبش دلا، تشنه مشو نفس نفس</p></div>
<div class="m2"><p>گرچه ز گریه دمبدم غرقه در آب میشوی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اهلی شوخدیده گو از در نیکویان برو</p></div>
<div class="m2"><p>ورنه چو اشک خود خجل از همه باب میشوی</p></div></div>