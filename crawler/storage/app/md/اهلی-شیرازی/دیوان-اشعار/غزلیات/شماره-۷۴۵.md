---
title: >-
    شمارهٔ ۷۴۵
---
# شمارهٔ ۷۴۵

<div class="b" id="bn1"><div class="m1"><p>نتوان بر ساقی سخن از توبه عیان کرد</p></div>
<div class="m2"><p>پیش محک تجربه قلبی نتوان کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن دل که نیرزد سفال سگ کویی</p></div>
<div class="m2"><p>از جرعه خود جام جمش پیر مغان کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>المنه لله که صبا خاک ره دوست</p></div>
<div class="m2"><p>در دیده من کوری چشم دگران کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پیرانه سرم مست جوانی که به عشقش</p></div>
<div class="m2"><p>صد بار شدم پیر و دگر بار جوان کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بسیار بنومیدی و حسرت دل من سوخت</p></div>
<div class="m2"><p>و آخر ز کرم هرچه دلم خواست چنان کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مستان محبت می عشق تو نهفتند</p></div>
<div class="m2"><p>بدمست تنگ حوصله چون شیشه عیان کرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سر دهنت کس بیقین راه نبردست</p></div>
<div class="m2"><p>اهلی به گمان هم سخنی چند بیان کرد</p></div></div>