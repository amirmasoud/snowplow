---
title: >-
    شمارهٔ ۶۹۳
---
# شمارهٔ ۶۹۳

<div class="b" id="bn1"><div class="m1"><p>چو بهر قتل غیر آن مه به تیغ آبگون خیزد</p></div>
<div class="m2"><p>ز غیرت بر سراپای تنم رگهای خون خیزد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دلم خرم نمی گردد و گر گردد چنان نبود</p></div>
<div class="m2"><p>نخیزد سبزه زین وادی وگر خیزد زبون خیزد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نباشد خوش فغان و گریه بی جایگه لیکن</p></div>
<div class="m2"><p>چه درمان چون مرا اینها ز زخم اندرون خیزد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حذر کن ای رقیب من که گشتم زین پریرویان</p></div>
<div class="m2"><p>سگ دیوانه یی کز ناله ام بوی جنون خیزد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چرا بیدرد خوانی با وجود گریه اهلی را</p></div>
<div class="m2"><p>نمیگویی که گر دردی نباشد گریه چون خیزد</p></div></div>