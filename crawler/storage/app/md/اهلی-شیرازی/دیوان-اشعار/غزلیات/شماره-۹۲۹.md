---
title: >-
    شمارهٔ ۹۲۹
---
# شمارهٔ ۹۲۹

<div class="b" id="bn1"><div class="m1"><p>می‌زدی تیری به غیر و بر جگر خوردم ز رشک</p></div>
<div class="m2"><p>میل قتل دیگری کردی و من مردم ز رشک</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با همه شوقی که دارم دی چه بودی با رقیب</p></div>
<div class="m2"><p>دیدن روی ترا طاقت نیاوردم ز رشک</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفتم از آزار دشمن شادمان گردد دلم</p></div>
<div class="m2"><p>لیک چون آزار او کردی خود آزاردم ز رشک</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با وجود آنکه جان داد از دعای من رقیب</p></div>
<div class="m2"><p>چون گذشتی از پی تابوت او مردم ز رشک</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا ببینم دامن وصلت به دست دیگران</p></div>
<div class="m2"><p>همچو اهلی سر به جیب نیستی بردم ز رشک</p></div></div>