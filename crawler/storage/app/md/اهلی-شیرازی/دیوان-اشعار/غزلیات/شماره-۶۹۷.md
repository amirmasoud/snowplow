---
title: >-
    شمارهٔ ۶۹۷
---
# شمارهٔ ۶۹۷

<div class="b" id="bn1"><div class="m1"><p>دیده هر که از هوس سوی تو سیمتن بود</p></div>
<div class="m2"><p>غرقه بخون دل شود گر همه چشم من بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آه که گیردم نفس راه گلو ز بخت بد</p></div>
<div class="m2"><p>در نفسی که بامنش یار سر سخن بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>می بدهم که بیخودی می نهلد که چون منی</p></div>
<div class="m2"><p>خون خورد و لب تواش پیش لب و دهن بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر بگشایی ام کفن لاله صفت ز بعد مرگ</p></div>
<div class="m2"><p>زآتش داغ غم دلم سوخته در کفن بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مردم روزگار را ماتم اگر ز مردن است</p></div>
<div class="m2"><p>اهلی نا امید را ماتم زیستن بود</p></div></div>