---
title: >-
    شمارهٔ ۱۱۷۶
---
# شمارهٔ ۱۱۷۶

<div class="b" id="bn1"><div class="m1"><p>لبز غم تو خشک شد دیده تر هم آنچنان</p></div>
<div class="m2"><p>سوخت جگر بداغ دل خون جگر هم آنچنان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ایگل خوش نسیم من رفتی و بلبل ترا</p></div>
<div class="m2"><p>گریه شب بحال خود آه سحر هم آنچنان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سبزه دمید و خشک شد لاله شکفت و برگ ریخت</p></div>
<div class="m2"><p>نرگس مست را بگل میل نظر هم آنچنان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عمر گذشت و خاطرم باز نیامد از غمت</p></div>
<div class="m2"><p>خانه جان خراب شد دل بسفر هم آنچنان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نوش تو برد هوش ما سیر نمیشویم از آن</p></div>
<div class="m2"><p>مست شدند طوطیان ذوق شکر هم آنچنان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آب ببرد بحر و بر خاک بخورد گنج زر</p></div>
<div class="m2"><p>گوهر اشک من همان روی چو زر هم آنچنان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اهلی بت پرست اگر گشت بتو به حقپرست</p></div>
<div class="m2"><p>توبه شکست و مست شد مست دگر هم آنچنان</p></div></div>