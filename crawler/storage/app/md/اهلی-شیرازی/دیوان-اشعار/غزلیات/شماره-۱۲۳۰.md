---
title: >-
    شمارهٔ ۱۲۳۰
---
# شمارهٔ ۱۲۳۰

<div class="b" id="bn1"><div class="m1"><p>بسکه حیران گشته ام در جلوه رفتار ازو</p></div>
<div class="m2"><p>خشک برجا مانده ام چون صورت دیوار ازو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>او که از لب زنده سازد مرده را همچون مسیح</p></div>
<div class="m2"><p>ضعف طالع بین که من چون مانده ام بیمار ازو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با چنین عزت که آن سلطان خوبان را بود</p></div>
<div class="m2"><p>میبرد یوسف خجالت بر سر بازار ازو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>قاصدم گفت آنپری در کشتنت گوید سخن</p></div>
<div class="m2"><p>جان دهم صدبار اگر این بشنوم یکبار ازو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مرده را گر جان دهد اهلی نمیباشد عجب</p></div>
<div class="m2"><p>معجز حسن است و من این دیده ام بسیار ازو</p></div></div>