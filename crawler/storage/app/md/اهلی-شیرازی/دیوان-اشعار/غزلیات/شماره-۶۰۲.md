---
title: >-
    شمارهٔ ۶۰۲
---
# شمارهٔ ۶۰۲

<div class="b" id="bn1"><div class="m1"><p>خون شد جگر از خنده که آن رشک ملک زد</p></div>
<div class="m2"><p>تا چند توان بر جگر ریش نمک زد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چند از دل آلوده صفا خرج توان کرد</p></div>
<div class="m2"><p>آخر مس قلبم همه عالم به محک زد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دود دل من دامنت ای ماه بگیراد</p></div>
<div class="m2"><p>هر چند که از جور تو آتش بفلک زد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در عشق مجو وصل که از هجر بسوزی</p></div>
<div class="m2"><p>هر کس که دوشش خواست درین نرد دو یک زد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اهلی سفر از خانقهت هست مبارک</p></div>
<div class="m2"><p>چون پیر مغان نعره الله معک زد</p></div></div>