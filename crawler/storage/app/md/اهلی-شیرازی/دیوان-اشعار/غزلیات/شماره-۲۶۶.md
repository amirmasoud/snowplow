---
title: >-
    شمارهٔ ۲۶۶
---
# شمارهٔ ۲۶۶

<div class="b" id="bn1"><div class="m1"><p>روی تو مصحفی است که در آن خوبی است</p></div>
<div class="m2"><p>خال تو آیتی است که در شان خوبی است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جان جهان، ملاحت روی تو تازه کرد</p></div>
<div class="m2"><p>خوبی است جان عالم و آن جان خوبی است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>طغرای ابرویت چو مه نو تمام کرد</p></div>
<div class="m2"><p>منشور دلبری که ز دیوان خوبی است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آهسته ران که توسن حسن تو پست ساخت</p></div>
<div class="m2"><p>جولان هرکه در سر میدان خوبی است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رخسار دل‌فروز تو زان لعل پرنمک</p></div>
<div class="m2"><p>این معدن ملاحت و آن کان خوبی است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>روی چو آتشت گل باغ جوانی است</p></div>
<div class="m2"><p>قد خوشت نهال گلستان خوبی است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سلطان اگر ربود دلش خوبی غلام</p></div>
<div class="m2"><p>اهلی غلام اوست که سلطان خوبی است</p></div></div>