---
title: >-
    شمارهٔ ۷۶۵
---
# شمارهٔ ۷۶۵

<div class="b" id="bn1"><div class="m1"><p>یاران همه مست و خبر از خویش ندارند</p></div>
<div class="m2"><p>پروای خمار من درویش ندارند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جان کندن فرهاد چه دانند که چون است</p></div>
<div class="m2"><p>آنها که چو من کوه غمی پیش ندارند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>درد دل خود با که بگوییم که یاران</p></div>
<div class="m2"><p>با ما بجز از سرزنشی بیش ندارند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یوسف نکند یاد ز یعقوب که خوبان</p></div>
<div class="m2"><p>بیگانه نهادند و غم خویش ندارند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حق نمک خنده شیرین نشناسند</p></div>
<div class="m2"><p>کافر نمکانی که دل ریش ندارند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>با نوش لبان خرمگسانند رقیبان</p></div>
<div class="m2"><p>اهلی مطلب نوش که جز نیش ندارند</p></div></div>