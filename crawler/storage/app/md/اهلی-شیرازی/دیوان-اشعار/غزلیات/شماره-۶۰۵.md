---
title: >-
    شمارهٔ ۶۰۵
---
# شمارهٔ ۶۰۵

<div class="b" id="bn1"><div class="m1"><p>شوقی دگر امروز دلم سوی تو دارد</p></div>
<div class="m2"><p>گویا کششی از طرف موی تو دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل میبردم سوی تو پیداست که مرغم</p></div>
<div class="m2"><p>آهنگ کمانخانه ابروی تو دارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از زندگیش دل چو صبا جز رمقی نیست</p></div>
<div class="m2"><p>وین زندگی از جان نه که از بوی تو دارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر خاک سر کوی تو خوابد بفراغت</p></div>
<div class="m2"><p>عیشی عجب است اینکه سگ کوی تو دارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اهلی ز پی مردنش اسباب مهیاست</p></div>
<div class="m2"><p>بازآ که همین آرزوی روی تو دارد</p></div></div>