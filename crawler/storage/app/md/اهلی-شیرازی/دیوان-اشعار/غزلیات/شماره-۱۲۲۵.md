---
title: >-
    شمارهٔ ۱۲۲۵
---
# شمارهٔ ۱۲۲۵

<div class="b" id="bn1"><div class="m1"><p>آن بزم عیش ساقی و جام شراب کو</p></div>
<div class="m2"><p>و آن مستی محبت و آن اضطراب کو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گیرم که روی گل نگرم از هوای دوست</p></div>
<div class="m2"><p>آن شیوه و کرشمه و ناز و عتاب کو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گلشن همان و مرغ همان شاخ گل همان</p></div>
<div class="m2"><p>گلبانگ شوق و مستی عهد شباب کو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر مدعی ز عشق زند لاف همچو من</p></div>
<div class="m2"><p>کنج محبت و دل و جان خراب کو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خواب از خیال آن مژه در دیده نیستم</p></div>
<div class="m2"><p>در دیده یی که خار بود جای خواب کو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>من مست و بیخود از بت و نامم خداپرست</p></div>
<div class="m2"><p>زین قصه گر سیوال کنندم جواب کو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در آتشم هنوز از آن شب که آن حریف</p></div>
<div class="m2"><p>شد مست ناز و گفت که اهلی کباب کو</p></div></div>