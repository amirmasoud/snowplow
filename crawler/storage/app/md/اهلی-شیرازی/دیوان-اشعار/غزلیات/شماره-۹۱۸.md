---
title: >-
    شمارهٔ ۹۱۸
---
# شمارهٔ ۹۱۸

<div class="b" id="bn1"><div class="m1"><p>گر تشنه آب خضری از سر اخلاص</p></div>
<div class="m2"><p>برخیز و بمیخانه درآ از سر اخلاص</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>میخانه بود کوثر و اخلاص تو رهبر</p></div>
<div class="m2"><p>آنجا نرسد کس بجز از رهبر اخلاص</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مرغ دل از اخلاص ببام تو گذر کرد</p></div>
<div class="m2"><p>بر عرش توان رفت ببال و پر اخلاص</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بی شوق تو کس حرفی از اخلاص نیابد</p></div>
<div class="m2"><p>شد فاتحه شوق تو سر دفتر اخلاص</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از پرتو اخلاص بظلمت نشوی گم</p></div>
<div class="m2"><p>هشدار که تا گم نکنی گوهر اخلاص</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اهلی به هوای شکرین لعل لب دوست</p></div>
<div class="m2"><p>طوبی صتم فاتحه خوان از سر اخلاص</p></div></div>