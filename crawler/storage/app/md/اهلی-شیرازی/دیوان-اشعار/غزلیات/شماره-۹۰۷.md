---
title: >-
    شمارهٔ ۹۰۷
---
# شمارهٔ ۹۰۷

<div class="b" id="bn1"><div class="m1"><p>چون شیشه پرم از غم پیمان گسل خویش</p></div>
<div class="m2"><p>بگذار که خالی کنم از گریه دل خویش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر سوزش پروانه ز نزدیکی شمع است</p></div>
<div class="m2"><p>من سوختم از دوری شمع چگل خویش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>داغ سگ کویت همه را خط غلامی است</p></div>
<div class="m2"><p>ما نیز نهادیم برین خط سجل خویش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حقا که برابر نکنم عیش دو عالم</p></div>
<div class="m2"><p>با شادی گه گاه و غم متصل خویش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هرچند که اهلی ز جهان مهر گیاجست</p></div>
<div class="m2"><p>بویی نشنیدست مگر ز آب و گل خویش</p></div></div>