---
title: >-
    شمارهٔ ۴۴۵
---
# شمارهٔ ۴۴۵

<div class="b" id="bn1"><div class="m1"><p>گر حسن و دلبری بتو مهپاره داده اند</p></div>
<div class="m2"><p>چشمی بماهم از پی نظاره داده اند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آندم که خورده اند دو لعل تو خون ما</p></div>
<div class="m2"><p>یک جرعه هم بنرگس خونخواره داده اند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ما کشته توایم و ترا از برای ما</p></div>
<div class="m2"><p>این نخل قامت و گل رخساره داده اند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آن ساقیان که باده مقصود میدهند</p></div>
<div class="m2"><p>خون دلی بعاشق بیچاره داده اند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اهلی هلاک نیستی و بی نشانی</p></div>
<div class="m2"><p>کانجا نشانش از دل آواره داده اند</p></div></div>