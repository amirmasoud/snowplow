---
title: >-
    شمارهٔ ۱۱۹۵
---
# شمارهٔ ۱۱۹۵

<div class="b" id="bn1"><div class="m1"><p>بیا ای عشق جان‌سوز آتشم در جان محزون زن</p></div>
<div class="m2"><p>بیا ای آتشین آه و علم بر بام گردون زن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خیالت می‌رسد در دل کجا جان در میان گنجد</p></div>
<div class="m2"><p>بگو سلطان رسید اینک تو ز آنجا خیمه بیرون زن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تنور سینه می‌جوشد گر از طوفان حذر داری</p></div>
<div class="m2"><p>بیا بر آتشم آبی از آن لب‌های میگون زن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ترا صوفی ز دیر ما صدای حلقه در بس</p></div>
<div class="m2"><p>وگر در حلقه می‌آیی صلا بر گنج قارون زن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بیفکن استخوان پیش هما وز وی مجو همت</p></div>
<div class="m2"><p>بیا و قرعه همت بر این روی همایون زن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>درآ ساقی و باقی کن حدیث دجله و جیحون</p></div>
<div class="m2"><p>قدم بر دیده ما نه قلم بر حرف جیحون زن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اسیر لشکر غم تا به کی اهلی بدین روزی</p></div>
<div class="m2"><p>ز تیغ آه خود یک شب بر این لشکر شبیخون زن</p></div></div>