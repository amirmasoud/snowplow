---
title: >-
    شمارهٔ ۶۱۸
---
# شمارهٔ ۶۱۸

<div class="b" id="bn1"><div class="m1"><p>گرچه با روی نکو خوی نکو می باید</p></div>
<div class="m2"><p>عاشقان را ز بتان تندی خو می باید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چند کوشیم و نبخشد لب او آب حیات</p></div>
<div class="m2"><p>کوشش ما چکند بخشش از او می باید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نه که هر سرو قدی دل برد از دست کسی</p></div>
<div class="m2"><p>سرو سیمین بدنی غالیه مو می باید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گوشه چشم من ای سرو سهی مایل توست</p></div>
<div class="m2"><p>منزل سرو سهی بر لب جو می باید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا تو در معرکه یی در پی چوگان بازی</p></div>
<div class="m2"><p>سر ما در پی چوگانت چو گو می باید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>احتراز تو ز من چیست بگفتار رقیب</p></div>
<div class="m2"><p>احتراز از سخن بیهده گو می باید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اهلی از لعل تو گر میطلبد آب حیات</p></div>
<div class="m2"><p>تا بود زنده چو خضرش تک و پو می باید</p></div></div>