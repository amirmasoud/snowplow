---
title: >-
    شمارهٔ ۱۳۷۷
---
# شمارهٔ ۱۳۷۷

<div class="b" id="bn1"><div class="m1"><p>سوخت دل از ناله ام چند خروشد کسی</p></div>
<div class="m2"><p>مرگ بجان میخرم کاش فروشد کسی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیخ من و کوهکن کوشش بی بخت کند</p></div>
<div class="m2"><p>آدمی از سنگ نیست چند بکوشد کسی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آینه حسن تو زنگ ز دل میبرد</p></div>
<div class="m2"><p>حیف بود کز رخت دیده بپوشد کسی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تلخ بود بی تو می زهر بود گر خورم</p></div>
<div class="m2"><p>آب حیات ای پسر بیتو ننوشد کسی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مانع اهلی مشو گر بخروشد ز غم</p></div>
<div class="m2"><p>دل مخراش از ستم تا نخروشد کسی</p></div></div>