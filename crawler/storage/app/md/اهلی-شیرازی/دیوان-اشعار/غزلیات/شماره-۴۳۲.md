---
title: >-
    شمارهٔ ۴۳۲
---
# شمارهٔ ۴۳۲

<div class="b" id="bn1"><div class="m1"><p>گاهم دو آهوی تو سگ خویش خوانده‌اند</p></div>
<div class="m2"><p>گاهم به سنگ تفرقه از پیش رانده‌اند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ما دل نمی‌بریم که شاهان چو باز خود</p></div>
<div class="m2"><p>کس را نرانده‌اند که بازش نخوانده‌اند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ما را چو چشم خویش حریفان بی‌وفا</p></div>
<div class="m2"><p>مخمور و ناتوان به کناری نشانده‌اند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا داده‌اند جرعهٔ جامی ز لعل خویش</p></div>
<div class="m2"><p>خوبانِ عشوه‌ساز به جانم رسانده‌اند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آن را که داده‌اند بتان نوش‌لعل خود</p></div>
<div class="m2"><p>از زهر چشم چاشنیی هم چشانده‌اند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اهلی به درد بی‌دلی و بی‌کسی بساز</p></div>
<div class="m2"><p>درمان طلب نکن که طبیبان نخوانده‌اند</p></div></div>