---
title: >-
    شمارهٔ ۵۹
---
# شمارهٔ ۵۹

<div class="b" id="bn1"><div class="m1"><p>به هم متاب دگر سنبل پریشان را</p></div>
<div class="m2"><p>یکی مساز به قتلم دو نا مسلمان را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بلای پیر و جوان حسن یوسف است ار نه</p></div>
<div class="m2"><p>چه وقت عشق و جوانی است پیر کنعان را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مجوی شربت وصل از بتان که این مردم</p></div>
<div class="m2"><p>همیشه خون جگر داده اند مهمان را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو لاله دامنت از خون ما نشان دارد</p></div>
<div class="m2"><p>اگرچه بر زده‌ای چابکانه دامان را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نسیم گل خبر از یار می‌دهد اهلی</p></div>
<div class="m2"><p>مرو به باغ که بر باد می‌دهی جان را</p></div></div>