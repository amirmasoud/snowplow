---
title: >-
    شمارهٔ ۱۸۲
---
# شمارهٔ ۱۸۲

<div class="b" id="bn1"><div class="m1"><p>هرچند که یوسف بجمال از همه بیش است</p></div>
<div class="m2"><p>حسن نمکین تو بلای دل ریش است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون گل همه تن آب حیاتی تو ولیکن</p></div>
<div class="m2"><p>چون خار دل آزار رقیبت همه نیش است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در دوستی مدعیان نیست جز آزار</p></div>
<div class="m2"><p>هر کو ببدان دوست شود دشمن خویشست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دشمن ببرادر صفتی دوست نگردد</p></div>
<div class="m2"><p>یوسف نگر ای مه که چه آزرده خویش است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مارا چه غم از مهر تو گر کم شد اگر بیش</p></div>
<div class="m2"><p>کوته نظران را نظر اندر کم و بیش است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کام دلم ایکافر بدکیش کرم کن</p></div>
<div class="m2"><p>آخر چو ترا مهر و وفا در همه کیش است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در کوی تو اهلی چه بود از همه واپس</p></div>
<div class="m2"><p>زیرا که بجانبازی و مهر از همه پیش است</p></div></div>