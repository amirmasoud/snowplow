---
title: >-
    شمارهٔ ۳۴۴
---
# شمارهٔ ۳۴۴

<div class="b" id="bn1"><div class="m1"><p>ساقی، جهان سرشک حریفان گرفته است</p></div>
<div class="m2"><p>کو کشتی شراب که طوفان گرفته است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چندان بجان رسید دلم از غم جهان</p></div>
<div class="m2"><p>کز محنت جهان دلم از جان گرفته است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با آنکه دامن از همه عالم کشیده ام</p></div>
<div class="m2"><p>عشق توام هنوز گریبان گرفته است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگه نیی ز آتش دل آه چون کنم</p></div>
<div class="m2"><p>ما بی زبان و آتش پنهان گرفته است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا زد بدامن تو ز مستی رقیب دست</p></div>
<div class="m2"><p>اهلی همیشه دست بدندان گرفته است</p></div></div>