---
title: >-
    شمارهٔ ۵۱۹
---
# شمارهٔ ۵۱۹

<div class="b" id="bn1"><div class="m1"><p>گفتم ار عاشق شوم گاهی غمی خواهم کشید</p></div>
<div class="m2"><p>من چه دانستم که بار عالمی خواهم کشید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زهر پنهان در می عشق است اما دلکش است</p></div>
<div class="m2"><p>کشته هم گر میشوم زین می دمی خواهم کشید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چشم خونریز تو گر مردم کشی زین سان کند</p></div>
<div class="m2"><p>در سر کوی تو هر دم ماتمی خواهم کشید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آتش محرومیم تیزست و نتوان باتو گفت</p></div>
<div class="m2"><p>این سخن باری بگوش محرمی خواهم کشید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چاک شد زان لب دلم اهلی کمال شوق بین</p></div>
<div class="m2"><p>کاینچنین زخمی به یاد مرهمی خواهم کشید</p></div></div>