---
title: >-
    شمارهٔ ۱۲۴۸
---
# شمارهٔ ۱۲۴۸

<div class="b" id="bn1"><div class="m1"><p>تنم که غرقه بخون اشک لاله گون کرده</p></div>
<div class="m2"><p>شهید عشق ترا شست و شو بخون کرده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جنون عشق اگر نیست چرخ مجنون را</p></div>
<div class="m2"><p>بتهمت غلط افسانه در جنون کرده</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به دوش من نهد ایام هر کجا باریست</p></div>
<div class="m2"><p>قضابخانه گردون مرا ستون کرده</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چراغ مجلس مستان شب نشین بودم</p></div>
<div class="m2"><p>مرا غم تو چو شمع سحر زبون کرده</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زبان کلک گرفتم حدیث خطت گفت</p></div>
<div class="m2"><p>حکایت دهنت را ندیده چون کرده</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بیان نامه سیاهی حکایت اهلی است</p></div>
<div class="m2"><p>که عمر در سر افسانه و فسون کرده</p></div></div>