---
title: >-
    شمارهٔ ۱۳۰۴
---
# شمارهٔ ۱۳۰۴

<div class="b" id="bn1"><div class="m1"><p>به نظاره جوانان که کسی نیافت سیری</p></div>
<div class="m2"><p>من پیر بس حریصم چه بلاست حرص و پیری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چکنم که چون تو یوسف نتوان خرید وصلت</p></div>
<div class="m2"><p>نه بنقد سربلندی نه بمایه فقیری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو سر وفا نداری مفکن بدام خویشم</p></div>
<div class="m2"><p>چه کنی شکار مرغی که ز خاک بر نگیری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نروی بدام زلفش بهوای خویش ایدل</p></div>
<div class="m2"><p>که هزار بار خواهی که بمیری و نمیری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چمن جمال خوبان دو سه روز دلپذیرست</p></div>
<div class="m2"><p>تو بهشت عاشقانی همه عمر دلپذیری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>من از آهوی دو چشمت شده ام زبون ولیکن</p></div>
<div class="m2"><p>بخدا که پیش شیران ننهم سر اسیری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز جفای تیغ خشمت سر خود گرفت هر کس</p></div>
<div class="m2"><p>گذر از تو من ندارم چکنم که ناگزیری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بسگان یار اهلی نتوان برابری کرد</p></div>
<div class="m2"><p>بشناس قدر خود را مکن اینچنین دلیری</p></div></div>