---
title: >-
    شمارهٔ ۷۷۳
---
# شمارهٔ ۷۷۳

<div class="b" id="bn1"><div class="m1"><p>گرچه ارباب خرد طایفه یی پرهنرند</p></div>
<div class="m2"><p>عاشقان طایفه دیگر و قومی دگرند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رشگ خوبان برم از مردم دنیا ورنه</p></div>
<div class="m2"><p>دنیی آنقدر ندارد که برو رشگ برند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عدم است آن دهن و در غم آن تنگدلان</p></div>
<div class="m2"><p>با وجود عدم او غم بیهوده خورند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مردم دهر چه دانند ترا قدر مگر</p></div>
<div class="m2"><p>مردم دیده عشاق که اهل نظرند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گرچه بر اهل نظر هر سر مو تیغ بلاست</p></div>
<div class="m2"><p>کافرم گر ز بلا یکسر مو برحذرند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کوی تو کعبه دلهاست چرا بی خبران</p></div>
<div class="m2"><p>چون رسیدند بدین کعبه بر او میگذرند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مردم چشم تو هر گوشه ربایند دلی</p></div>
<div class="m2"><p>مردم گوشه نشین بین که چه بیداد گرند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اهلی سوخته را در شکرستان خیال</p></div>
<div class="m2"><p>طوطیانند که از لعل تو مست شکرند</p></div></div>