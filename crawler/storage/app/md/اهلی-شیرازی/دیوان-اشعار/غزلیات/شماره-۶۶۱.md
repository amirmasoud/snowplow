---
title: >-
    شمارهٔ ۶۶۱
---
# شمارهٔ ۶۶۱

<div class="b" id="bn1"><div class="m1"><p>دوستان چون میرم آن خشت درم بالین کنید</p></div>
<div class="m2"><p>وزلب جانبخش او حرفی مرا تلقین کنید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از شهیدان غم عشقم مرا در زیر خاک</p></div>
<div class="m2"><p>بارخ پر خاک و خون و جامه خونین کنید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون شود سر مست میل قتل مسکینان کند</p></div>
<div class="m2"><p>زینهار ای دوستان یاد من مسکین کنید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مردم ایکافر تلخی و تندی تا بکی</p></div>
<div class="m2"><p>جان شیرین مرا گیرید و لب شیرین کنید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یا رب آزاری نبیند همچو اهلی کس ز عشق</p></div>
<div class="m2"><p>خوش دعایی میکنم ایعاشقان آمین کنید</p></div></div>