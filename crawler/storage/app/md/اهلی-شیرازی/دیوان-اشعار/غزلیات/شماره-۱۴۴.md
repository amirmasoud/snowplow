---
title: >-
    شمارهٔ ۱۴۴
---
# شمارهٔ ۱۴۴

<div class="b" id="bn1"><div class="m1"><p>در کوی تو خون دل ما پاک فرو رفت</p></div>
<div class="m2"><p>بس خون شهیدان که درین خاک فرو رفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با خون جگر سرزند از دیده چو مژگان</p></div>
<div class="m2"><p>خاری که مرا در جگر چاک فرو رفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پیش همه کس زهر بود چاشنی مرگ</p></div>
<div class="m2"><p>در کام من این زهر چو تریاک فرو رفت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شب همچو مه چارده پیدا شدی از بام</p></div>
<div class="m2"><p>وز شرم تو خورشید بر افلاک فرو رفت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اهلی که ره عقل گرفت از ستم عشق</p></div>
<div class="m2"><p>در فکر غلط با همه ادراک فرو رفت</p></div></div>