---
title: >-
    شمارهٔ ۱۸۹
---
# شمارهٔ ۱۸۹

<div class="b" id="bn1"><div class="m1"><p>همه دم ز درد عشقت دل زار من حزین است</p></div>
<div class="m2"><p>همه درد روزیم شد چکنم نصیبم این است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل و دین گر از سجودت همه شد مرا همین بس</p></div>
<div class="m2"><p>که زخاک راهت ای بت اثریم بر جبین است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به رخ تو شهسوارا نه غبار خط برآمد</p></div>
<div class="m2"><p>که بر آفتاب گردی زغبار مشک چین است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تو که خرمن مرادی کرم از تو زیبد اما</p></div>
<div class="m2"><p>کرمت به سر بلندان ستمت به خوشه چین است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سر کویت ای سهی قد بود از بهشت خوشتر</p></div>
<div class="m2"><p>به بهشت نیست عیشی که درین سرا زمین است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>من اگرچه بت پرستم تو مگو نظر بپوشان</p></div>
<div class="m2"><p>رخ خود بپوش ای دل که بلای عقل و دین است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بگشا کمند اهلی به شکار نو غزالی</p></div>
<div class="m2"><p>که سگ رقیب او را همه وقت در کمین است</p></div></div>