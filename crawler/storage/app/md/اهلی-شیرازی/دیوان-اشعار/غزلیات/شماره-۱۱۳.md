---
title: >-
    شمارهٔ ۱۱۳
---
# شمارهٔ ۱۱۳

<div class="b" id="bn1"><div class="m1"><p>جز از وصال تو با کس سر وصولم نیست</p></div>
<div class="m2"><p>به جز قبول تو هیچ از جهان قبولم نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگرچه بی تو به هر سر چو باد می گردم</p></div>
<div class="m2"><p>به هیچ منزل راحت سر نزولم نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زدست طعن چنان سر به جیب از آن دارم</p></div>
<div class="m2"><p>که تاب سرزنش مردم فضولم نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>که نامه ام به تو آرد که برتر از عرشی</p></div>
<div class="m2"><p>بغیر طایر همت کسی رسولم نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بجز خیال دو آهوی تو من محزون</p></div>
<div class="m2"><p>کسی ملال گذار دل ملولم نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر آن سرم که بمیرم بر آستان اهلی</p></div>
<div class="m2"><p>چو در حریم وصالش ره دخولم نیست</p></div></div>