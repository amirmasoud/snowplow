---
title: >-
    شمارهٔ ۸۳۱
---
# شمارهٔ ۸۳۱

<div class="b" id="bn1"><div class="m1"><p>ای همچو گل شکفته و از گل شکفته تر</p></div>
<div class="m2"><p>تا کی نهفته باشی و از من نهفته تر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیدارتر ز دولت حسن تو چشم من</p></div>
<div class="m2"><p>وز بخت من دو نرگس مست تو خفته تر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چندانکه سعی بیش کنم هست پیش تو</p></div>
<div class="m2"><p>ناگفته بر حدیث من و نا شقته تر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>فریاد ازین ستم که ز غارتگر غمم</p></div>
<div class="m2"><p>شد خانه رفته و دلم از خانه رفته تر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اهلی بگوش یار رسان این سخن که هست</p></div>
<div class="m2"><p>از در سفته گوهر نظم تو سفته تر</p></div></div>