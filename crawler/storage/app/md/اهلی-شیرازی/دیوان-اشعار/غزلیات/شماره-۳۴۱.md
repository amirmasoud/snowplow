---
title: >-
    شمارهٔ ۳۴۱
---
# شمارهٔ ۳۴۱

<div class="b" id="bn1"><div class="m1"><p>عیسی دم ما همدم اگر نیست غمی نیست</p></div>
<div class="m2"><p>ما را غم آن کشت که با ماش دمی نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای ابر کرم مرحمتی کن که درین راه</p></div>
<div class="m2"><p>جز اشک من تشنه جگر هیچ نمی نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر وصل تو جویم بگدایی مکنم عیب</p></div>
<div class="m2"><p>درویشم و در ملک تو صاحب کرمی نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با درد تو صد شکر بود صاحب دردش</p></div>
<div class="m2"><p>بی درد کند شکر که او را المی نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>برخیز و دل شحنه و قاضی ببر از راه</p></div>
<div class="m2"><p>وانگاه بکش هرکه تو خواهی که غمی نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اهلی، ره مقصود که در چشم تو دورست</p></div>
<div class="m2"><p>پا بر سر جان نه که بغیر از قدمی نیست</p></div></div>