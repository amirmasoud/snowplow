---
title: >-
    شمارهٔ ۱۲۶۹
---
# شمارهٔ ۱۲۶۹

<div class="b" id="bn1"><div class="m1"><p>روی نیاز بر ره آنسرو ناز به</p></div>
<div class="m2"><p>جانی که دادنی است بروی نیاز به</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر باد فتنه اطلس گل زود میرود</p></div>
<div class="m2"><p>چونسرو ژنده پوشی و عمر دراز به</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جاییکه میتوان بسگ او رفیق شد</p></div>
<div class="m2"><p>آنجا اگر فرشته بود احتراز به</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر مرگ لازم است به خامی چرا رویم</p></div>
<div class="m2"><p>جان سوختن چو شمع بسوز و گداز به</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بهر خدا مبند در ای پیر میفروش</p></div>
<div class="m2"><p>بر روی عاشقان در میخانه باز به</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زاهد تو و نماز که ما را نیاز بس</p></div>
<div class="m2"><p>در حضرت کریم نیاز از نماز به</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اهلی صبور باش که زخم دل ترا</p></div>
<div class="m2"><p>آخر بلطف خویش کند چاره ساز به</p></div></div>