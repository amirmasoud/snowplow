---
title: >-
    شمارهٔ ۴۴۰
---
# شمارهٔ ۴۴۰

<div class="b" id="bn1"><div class="m1"><p>تا جهان بوده است با نیکان بدان در کینه‌اند</p></div>
<div class="m2"><p>میگساران را حسودان دشمن دیرینه‌اند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بگذر از اهل ورع کاین مردم افسرده‌دل</p></div>
<div class="m2"><p>همچو کرم پیله زیر خرقهٔ پشمینه‌اند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>قدسیان گنجینه سر محبت گر شدند</p></div>
<div class="m2"><p>عاشقان با داغ عشقش نقد آن گنجینه‌اند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در خراباتند مستوران به مستی متهم</p></div>
<div class="m2"><p>مفتیان مستند و شیخ مسجد آدینه‌اند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کی غم و دردت برون از دل کنم کاین همدمان</p></div>
<div class="m2"><p>همچو جان در دل درون و همچو دل در سینه‌اند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آن حریفان کز می وصل تو شب بودند مست</p></div>
<div class="m2"><p>تا قیامت در خمار آن می دوشینه‌اند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>غیبت رندان نگنجد در درون اهل دل</p></div>
<div class="m2"><p>دم مزن اهلی که پاکان صفا آیینه‌اند</p></div></div>