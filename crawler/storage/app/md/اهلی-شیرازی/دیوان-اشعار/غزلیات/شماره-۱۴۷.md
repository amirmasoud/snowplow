---
title: >-
    شمارهٔ ۱۴۷
---
# شمارهٔ ۱۴۷

<div class="b" id="bn1"><div class="m1"><p>عشق جانان راحت جان من بیچاره است</p></div>
<div class="m2"><p>آتش دوزخ بهشت مرغ آتشخواره است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا گل رویش شکفت آن سرو دل با کس نماند</p></div>
<div class="m2"><p>ور کسی دارد دلی چون غنچه هم صد پاره است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>این چنین کان سنگدل نرم از دم گرمم نشد</p></div>
<div class="m2"><p>میشود معلوم کان دل نیست سنگ خاره است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر بعشق آواره شد مجنون که پرسد حال او</p></div>
<div class="m2"><p>کاندرین وادی چو مجنون صدهزار آواره است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سامری کز ساحران بیشش همی گیرند گوش</p></div>
<div class="m2"><p>گوشه گیر از نرگس جادوی آه عیاره است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عقل و حکمت چاره کار تو اهلی کی کند</p></div>
<div class="m2"><p>چاره کار تو ای بیچاره ترک چاره است</p></div></div>