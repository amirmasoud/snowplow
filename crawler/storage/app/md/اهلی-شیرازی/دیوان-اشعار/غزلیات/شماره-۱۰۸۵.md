---
title: >-
    شمارهٔ ۱۰۸۵
---
# شمارهٔ ۱۰۸۵

<div class="b" id="bn1"><div class="m1"><p>چو چاره از غم خونخواره نمییابم</p></div>
<div class="m2"><p>جز آنکه جان بدهم چاره یی نمی یابم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بهار عمر خزان کردم از غمت ایگل</p></div>
<div class="m2"><p>هنوز فرصت نظاره یی نمی یابم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز دست جور تو آواره جهان گشتم</p></div>
<div class="m2"><p>ولیک همچو خود آواره یی نمی یابم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هنوز تیره شبم با هزار مشعل آه</p></div>
<div class="m2"><p>شبی که همچو تو مهپاره یی نمی یابم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خوشم بخواری دل با هزار زخم زبان</p></div>
<div class="m2"><p>بسختی دل خود خاره یی نمی یابم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شبم نمیرود از غصه خواب اگر روزی</p></div>
<div class="m2"><p>ملامتی ز ستمکاره یی نمی یابم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بجان دوست که هرگز ز خانقه اهلی</p></div>
<div class="m2"><p>صفای محبت میخواره یی نمی یابم</p></div></div>