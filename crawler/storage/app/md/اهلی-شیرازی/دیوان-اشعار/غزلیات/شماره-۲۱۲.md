---
title: >-
    شمارهٔ ۲۱۲
---
# شمارهٔ ۲۱۲

<div class="b" id="bn1"><div class="m1"><p>گر کوه تحمل کسی از بار ستم نیست</p></div>
<div class="m2"><p>در عشق تو ثابت قدم آن سست قدم نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پیشت فلک از بار غمت خم شده چون ماست</p></div>
<div class="m2"><p>در دور تو آزاده کس از بار ستم نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پیش تو وجود و عدم ما چه تفاوت</p></div>
<div class="m2"><p>در کوی بتان نیست وجودی که عدم نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پروانه اگر سوخت بر او شمع بگرید</p></div>
<div class="m2"><p>آن شمع نگاهش بمن سوخته هم نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جان بنده ساقی که دهد می بتواضع</p></div>
<div class="m2"><p>در مشرب ما هیچ به از خلق و کرم نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر زخم غمی مرهم لطفی ز پی اوست</p></div>
<div class="m2"><p>دل بد مکن از زهر غم یار که غم نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اهلی بجز این کز تو بشد پای بزنجیر</p></div>
<div class="m2"><p>هیچش دگر از دولت سودای تو کم نیست</p></div></div>