---
title: >-
    شمارهٔ ۴۶۱
---
# شمارهٔ ۴۶۱

<div class="b" id="bn1"><div class="m1"><p>شدم هلاک و ز من جز دریغ و درد نماند</p></div>
<div class="m2"><p>بباد رفت غبارم چنانکه گرد نماند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گذشت رقص کنان جان چو کرد از خود یار</p></div>
<div class="m2"><p>بکوی او چه حریفان هرزه گرد نماند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کجا شد از می وصل تو سرخ رویی من</p></div>
<div class="m2"><p>که در خمار غمم غیر روی زرد نماند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>برفت گرمی بازار همدمی بر باد</p></div>
<div class="m2"><p>چنانکه در دل من غیر آه سرد نماند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ربود در صف عشاق از آن زلیخا گوی</p></div>
<div class="m2"><p>که در محبت یوسف ز هیچ مرد نماند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بیاد چشم و لبت مست شد چنان اهلی</p></div>
<div class="m2"><p>که چون فرشته در او ذوق خواب و خورد نماند</p></div></div>