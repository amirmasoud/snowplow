---
title: >-
    شمارهٔ ۴۹۵
---
# شمارهٔ ۴۹۵

<div class="b" id="bn1"><div class="m1"><p>آه گر می ز غمت عاشق غمناک نزد</p></div>
<div class="m2"><p>که ز سوزش دگری جامه بتن چاک نزد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پر گرانجان مشو ایخواجه که روح الله هم</p></div>
<div class="m2"><p>تا سبکروح نشد پای بر افلاک نزد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کشته صید گه عشق سرافراز نشد</p></div>
<div class="m2"><p>تا سرش دست بر آن حلقه فتراک نزد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کس چراغی شب غم بر ره مجنون ننهاد</p></div>
<div class="m2"><p>تا ز سوز دل خود شعله بخاشاک نزد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بی تو اهلی چو بسر خاک کند نیست عجب</p></div>
<div class="m2"><p>عجب آنست که بر دیده چرا خاک نزد</p></div></div>