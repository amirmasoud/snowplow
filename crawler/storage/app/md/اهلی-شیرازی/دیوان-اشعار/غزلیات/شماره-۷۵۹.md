---
title: >-
    شمارهٔ ۷۵۹
---
# شمارهٔ ۷۵۹

<div class="b" id="bn1"><div class="m1"><p>در جان و دل بیک نگه آن شوخ راه کرد</p></div>
<div class="m2"><p>آنشوخ هرچه کرد هم از یک نگاه کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای شاه حسن در دل ویران ز عشق تست</p></div>
<div class="m2"><p>گنجی که صدهزار گدا پادشاه کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من بی تو چون زنم دم خوش؟ کاتش غمت</p></div>
<div class="m2"><p>در سینه هر نفس که زدم دود آه کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جای فرشته نیست چه جای پری وشان</p></div>
<div class="m2"><p>در خلوت دلم که غمت تکیه گاه کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چند از خیال خط تو مشق جنون کنم</p></div>
<div class="m2"><p>سودا ببین که نامه عمرم سیاه کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کافر دلم بعشق دو زنارت ایصنم</p></div>
<div class="m2"><p>یکرنگ کفر گشت و خدا را گواه کرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>منت ز آفتاب نبرد از فرشته هم</p></div>
<div class="m2"><p>اهلی که سایه سگ این در پناه کرد</p></div></div>