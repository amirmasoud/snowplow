---
title: >-
    شمارهٔ ۱۲۴۵
---
# شمارهٔ ۱۲۴۵

<div class="b" id="bn1"><div class="m1"><p>خبرم شدست کانمه هوس شراب کرده</p></div>
<div class="m2"><p>چکنم که این حکایت جگرم کباب کرده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که نمود (می) بجامش که ز غم بسوخت جانم</p></div>
<div class="m2"><p>ز که بود این خرابی که مرا خراب کرده</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز زلال جان شیرین می اوست در دل من</p></div>
<div class="m2"><p>بشراب تلخ مردم بچه رو بتاب کرده</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بکجا کشید ساغر بکه شد حریف یا رب</p></div>
<div class="m2"><p>که دل مرا بصد غم گرو عذاب کرده</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مشنو ز غیر اهلی که شب آنحریف می زد</p></div>
<div class="m2"><p>بکمان آنکه حسنش همه روز خواب کرده</p></div></div>