---
title: >-
    شمارهٔ ۳۷۶
---
# شمارهٔ ۳۷۶

<div class="b" id="bn1"><div class="m1"><p>اگر ز کین‌کشی‌ام غم ز خشم و کینم نیست</p></div>
<div class="m2"><p>ز رشک غیر می‌سوزم که تاب اینم نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یکی به کفر خوش است و یکی به دین شادست</p></div>
<div class="m2"><p>من از خیال تو پروای کفر و دینم نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نشان پای سگان تو گلستان من است</p></div>
<div class="m2"><p>به غیر ازین ز دو عالم گلی زمینم نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کدام شب که نه دست دعاست بر فلکم</p></div>
<div class="m2"><p>کدام روز که بر خاک ره جبینم نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به غیر ناله چو مجنون نماند هم‌نفسم</p></div>
<div class="m2"><p>به جز سگان درت هیچ هم‌نشینم نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز طوف کوی تو پروای جنتم نبود</p></div>
<div class="m2"><p>ز دیدن تو نظر سوی حور عینم نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نظر به گنج زر از گریه کی کنم اهلی</p></div>
<div class="m2"><p>کدام گنج که در کنج آستینم نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آیینه است دوست گرت رشک غیر ازوست</p></div>
<div class="m2"><p>در غیر او مبین که نبیند به غیر دوست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ای شهسوار حسن تو چوگان ز زلف ساز</p></div>
<div class="m2"><p>کافتاده در ره از سرما صدهزار گوست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در قبضه مراد تو باشد اگر مرا</p></div>
<div class="m2"><p>یک مشت استخوان چو کمان است زیر پوست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گاهی نگه کنی به من ای آروزی جان</p></div>
<div class="m2"><p>لیکن نه آن نگه که مرا از تو آرزوست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مژگان خون‌چکان من است این به گرد چشم</p></div>
<div class="m2"><p>یا سرخ بید هر طرفی بر کنار جوست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>اهلی غزال مشک‌فشانی است کلک تو</p></div>
<div class="m2"><p>کز نافه‌های او همه آفاق مشکبوست</p></div></div>