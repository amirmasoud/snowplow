---
title: >-
    شمارهٔ ۳۴
---
# شمارهٔ ۳۴

<div class="b" id="bn1"><div class="m1"><p>آتش رخی کز یاد او آهی زجان خیزد مرا</p></div>
<div class="m2"><p>چون شمع اگر نامش برم دود از زبان خیزد مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در حسرت ماه رخش از یارب شب سوختم</p></div>
<div class="m2"><p>یارب زهجرش تا بکی آه از فغان خیزد مرا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زان ساقی سرمست اگر چون شیشه پرخون شد دلم</p></div>
<div class="m2"><p>بی لعل میگونش نفس کی از دهان خیزد مرا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دل می خرد مهر بتان جان می فروشد رایگان</p></div>
<div class="m2"><p>مشکل کزین سودای دل غیر از زبان خیزد مرا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بس کز هوای گلرخان چون لاله می سوزد دلم</p></div>
<div class="m2"><p>جز داغ حسرت کی گلی زین گلستان خیزد مرا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در خونم از اشک روان آغشته در شب‌های تار</p></div>
<div class="m2"><p>صبحی کز آغوش طرب سروی روان خیزد مرا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اهلی، براه گلرخان چشمم چو نرگس خاک شد</p></div>
<div class="m2"><p>روزی کزین گلشن روم گل زاستخوان خیزد مرا</p></div></div>