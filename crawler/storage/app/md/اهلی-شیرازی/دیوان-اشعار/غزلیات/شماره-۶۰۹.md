---
title: >-
    شمارهٔ ۶۰۹
---
# شمارهٔ ۶۰۹

<div class="b" id="bn1"><div class="m1"><p>گرچه کار دلم از صبر بسامان نشود</p></div>
<div class="m2"><p>هم صبوری که کس از صبر پشیمان نشود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جان ثابت قدم آنست که در راه وفا</p></div>
<div class="m2"><p>خاک ره گردد و یکذره پریشان نشود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آنکه صد سال پرستنده بود لعبت چین</p></div>
<div class="m2"><p>کافرم گر بهوای تو مسلمان نشود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا تو راهی ننهی پیش من ایکعبه وصل</p></div>
<div class="m2"><p>مشکل کار من از سعی خود آسان نشود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اهلی آسوده ز نومیدی خود باش که عشق</p></div>
<div class="m2"><p>کیمیایی است که بی مایه حرمان نشود</p></div></div>