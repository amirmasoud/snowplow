---
title: >-
    شمارهٔ ۹۰۲
---
# شمارهٔ ۹۰۲

<div class="b" id="bn1"><div class="m1"><p>ای اجل مهلت من ده که ببوسم دامنش</p></div>
<div class="m2"><p>دشمنم نیزمکش تا بکشد رشگ منش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون شکایت کنم از دوست که خون ریخت مرا</p></div>
<div class="m2"><p>هرکه شد کشته ز معشوق نباشد سخنش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دهنش گفت که دردت کنم از بوسه دوا</p></div>
<div class="m2"><p>جای آنست که صد بوسه زنم بر دهنش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مبتلای قفس تن نشدی طوطی جان</p></div>
<div class="m2"><p>گر نبودی هوس آن لب شکر شکنش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اهلی آن روز که چون لاله سر از خاک زند</p></div>
<div class="m2"><p>غرقه در خون جگر سوخته بینی کفنش</p></div></div>