---
title: >-
    شمارهٔ ۱۲۴۲
---
# شمارهٔ ۱۲۴۲

<div class="b" id="bn1"><div class="m1"><p>چون شمع دل از داغ تو افروختنش به</p></div>
<div class="m2"><p>عاشق که دل افسرده بود سوختنش به</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر رسم وره عقل ندانم مکنم عیب</p></div>
<div class="m2"><p>کاین دانش بیهوده نیاموختنش به</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بی یوسف خود دیده چو یعقوب ببستم</p></div>
<div class="m2"><p>چشمی که نه بر دوست بود دوختنش به</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر چند عزیزست متاع خرد و صبر</p></div>
<div class="m2"><p>در پای تو افشاندن از اندوختنش به</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اهلی بغلامی تو چون پیر شد آخر</p></div>
<div class="m2"><p>مفروش بدین عیب که نفروختنش به</p></div></div>