---
title: >-
    شمارهٔ ۳۸۳
---
# شمارهٔ ۳۸۳

<div class="b" id="bn1"><div class="m1"><p>چو خط به کشتنم آورد لعل خندانت</p></div>
<div class="m2"><p>بکش وگرنه کند بخت من پشیمانت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گمان برند که یوسف ز چه برون آمد</p></div>
<div class="m2"><p>چو ماه چهره برآرد سر از گریبانت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تو کعبه دل و جانی و عید مشتاقان</p></div>
<div class="m2"><p>زهر کنار بود صد هزار قربانت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زناز سرو بلند تو من عجب دارم</p></div>
<div class="m2"><p>که دست کوته عاشق رسد بدامانت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز گریه دامن ما مفلسان شود پر در</p></div>
<div class="m2"><p>چو درج در بگشاید دهان خندانت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گرم بهجر کشی ور بوصل زنده کنی</p></div>
<div class="m2"><p>از آن چه سود ترا و ازین چه نقصانت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز حسن خط تو شاید که سر برون آرد</p></div>
<div class="m2"><p>هزار یوسف مصر از چه زنخدانت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سگ در تو بامید آن بود اهلی</p></div>
<div class="m2"><p>که خاک راه شود زیر پای دربانت</p></div></div>