---
title: >-
    شمارهٔ ۵۳۲
---
# شمارهٔ ۵۳۲

<div class="b" id="bn1"><div class="m1"><p>نصیب کوهکن از وصل دوست چون باشد</p></div>
<div class="m2"><p>که سنگ تفرقه اش کوه بیستون باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ننالم از تو گرم جای لطف قهر کنی</p></div>
<div class="m2"><p>که بر من این ستم از بخت واژگون باشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کجا ز شوق وصالت سخن کنم با کس</p></div>
<div class="m2"><p>گرم نه سرزنش از تهمت جنون باشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مراد من بده و با رقیب هم منشین</p></div>
<div class="m2"><p>که درد رشک ز نومیدی ام فزون باشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تو آب خضری و لب تشنه چون زید بی تو</p></div>
<div class="m2"><p>وگر زید مگر از گریه غرق خون باشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کسیکه روی تو دید و نشست با تو دمی</p></div>
<div class="m2"><p>تو خود بگو که دگر بی رخ تو چون باشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پر است ساغر چشمم ز خون دل اهلی</p></div>
<div class="m2"><p>اگرچه کاسه درویش سرنگون باشد</p></div></div>