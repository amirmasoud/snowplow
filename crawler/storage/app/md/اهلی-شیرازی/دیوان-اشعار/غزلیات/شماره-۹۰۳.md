---
title: >-
    شمارهٔ ۹۰۳
---
# شمارهٔ ۹۰۳

<div class="b" id="bn1"><div class="m1"><p>نمیخواهد که دست کس رسد بر طاق ابرویش</p></div>
<div class="m2"><p>بود پیوسته آن ابرو بلند از تندی خویش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چنین در خاک و خون افتاده ام مگذرا ای همدم</p></div>
<div class="m2"><p>که میترسم بخون من کشد تهمت سگ کویش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پی تسکین دل خواهم نشینم پهلویش لیکن</p></div>
<div class="m2"><p>دلم افزون طپد هرگه رسد پهلو به پهلویش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همان بهتر که لب بندم زآه گرم پیش او</p></div>
<div class="m2"><p>مبادا در عرق افتد زآه من گل رویش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سگ مشکین غزال خود من از بوی خوشم باری</p></div>
<div class="m2"><p>که هرجا بگذرد خلقی برآسایند از بویش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مبین زنهار ای همدم چو اهلی سرو قد او</p></div>
<div class="m2"><p>که حسرت بار می آرد هوای سرو دلجویش</p></div></div>