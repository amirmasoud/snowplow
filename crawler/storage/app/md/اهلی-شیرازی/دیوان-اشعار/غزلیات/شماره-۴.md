---
title: >-
    شمارهٔ ۴
---
# شمارهٔ ۴

<div class="b" id="bn1"><div class="m1"><p>ای به خاطر صد غبار از رشک تو آیینه را</p></div>
<div class="m2"><p>کرده چاک از دست تو ماه منور سینه را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با تن چون برگ گل پشمینه‌پوشی کرده‌ای</p></div>
<div class="m2"><p>زان بنوت نافه پوشد خرقه پشمینه را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از میان انبیاء مهر نبوت زان تست</p></div>
<div class="m2"><p>گنج علمی لاجرم مهری بود گنجینه را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر نمایی روی روشن کی دگر آیینه گر</p></div>
<div class="m2"><p>پرده زنگاری از رخ بر کشد آیینه را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دیده روح الامین از شرح صدرت نور یافت</p></div>
<div class="m2"><p>روشن آن چشمی که دید آن سینه بی کینه را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شد شب قدری شب آدینه تا مخصوص تست</p></div>
<div class="m2"><p>غایت قدر است ازین نسبت شب آدینه را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آنچه من دوش از قیام قامتت دیدم بخواب</p></div>
<div class="m2"><p>تا قیامت عاشقم خواب شب دوشینه را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>با خیالت از ازل اهلی چو می بازد نظر</p></div>
<div class="m2"><p>روی بنما یک نظر این عاشق دیرینه را</p></div></div>