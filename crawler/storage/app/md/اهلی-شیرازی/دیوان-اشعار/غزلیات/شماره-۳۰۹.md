---
title: >-
    شمارهٔ ۳۰۹
---
# شمارهٔ ۳۰۹

<div class="b" id="bn1"><div class="m1"><p>ای که می‌پرسی که با دل هم‌سخن در خانه کیست</p></div>
<div class="m2"><p>طوطی حیران چه می‌داند که در آیینه کیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مار گنج عشق یار از عقل ما کی دم زند</p></div>
<div class="m2"><p>غیر محرم آگه از نقد دل گنجینه کیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کی زند بر سینه سنگ از دست دل دیوانه وار</p></div>
<div class="m2"><p>هر که دریابد که با دل همنشین در سینه کیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>من نمیگویم که بود از باده شب مست و خراب</p></div>
<div class="m2"><p>نرگست گوید که مست از باده دوشینه کیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چرخ هم در کینه عاشق بود سنگ غمش</p></div>
<div class="m2"><p>جان من در عالم عشق از حسد بی کینه کیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حال پیر می فروش از من مپرس ایشیخ شهر</p></div>
<div class="m2"><p>من چه میدانم که شیخ مسجد آدینه کیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جان من خلقی ز عشقت گر چو اهلی دم زنند</p></div>
<div class="m2"><p>خود تو می‌دانی کز ایشان عاشق دیرینه کیست</p></div></div>