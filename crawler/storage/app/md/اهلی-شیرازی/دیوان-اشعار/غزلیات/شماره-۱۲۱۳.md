---
title: >-
    شمارهٔ ۱۲۱۳
---
# شمارهٔ ۱۲۱۳

<div class="b" id="bn1"><div class="m1"><p>دلش رمیده شد آهو ز چین کاکل او</p></div>
<div class="m2"><p>نهاده رو ببیابان ببوی سنبل او</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز خار خار دلم داغ حسرتست بدست</p></div>
<div class="m2"><p>کسیکه خار نشاند همین بود گل او</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>صدای تیشه فرهاد ذوق عشق دهد</p></div>
<div class="m2"><p>نه صوت صحبت پرویز و بانگ غلغل او</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چرا بهم نزند چشم پیش او نرگس</p></div>
<div class="m2"><p>اگرنه خیره شدش دیده در تامل او</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بجای سوزن عیسی بچشم من خارست</p></div>
<div class="m2"><p>گذشته پایه ترک من از توکل او</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>طبیب من چو مسیحا بمرده جان بخشد</p></div>
<div class="m2"><p>بلاست این که مرا میکشد تغافل او</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بسوخت اهلی و از داغ عشق ناله نکرد</p></div>
<div class="m2"><p>صد آفرین خدا باد بر تحمل او</p></div></div>