---
title: >-
    شمارهٔ ۴۴۳
---
# شمارهٔ ۴۴۳

<div class="b" id="bn1"><div class="m1"><p>یار شد مست و دل ما بفغان باز آورد</p></div>
<div class="m2"><p>گریه بد مستی ماهم بمیان باز آورد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عاقبت بخت سیاه ستم پیشه چنان کرد که یار</p></div>
<div class="m2"><p>جرم بخشیده مارا بزبان باز آورد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آنچنان زد ره عقل و دل و دین شاهد می</p></div>
<div class="m2"><p>که بصد سال ریاضت نتوان باز آورد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رخت بستم ز درش تا ندرد جامه صبر</p></div>
<div class="m2"><p>آخرم نعره زنان جامه دران باز آورد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای بسا آهوی آزاد که در قید دلش</p></div>
<div class="m2"><p>هوس دیدن آن دست و کمان باز آورد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هرکه یک جرعه کشید از می لعل لب او</p></div>
<div class="m2"><p>آب حسرت چو صراحی بدهان باز آورد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اهلی گمشده در فکر دهانش صد بار</p></div>
<div class="m2"><p>از جهان رفت و خیالش بجهان باز آورد</p></div></div>