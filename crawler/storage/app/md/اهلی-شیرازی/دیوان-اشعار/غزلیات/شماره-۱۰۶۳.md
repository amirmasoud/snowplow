---
title: >-
    شمارهٔ ۱۰۶۳
---
# شمارهٔ ۱۰۶۳

<div class="b" id="bn1"><div class="m1"><p>روزه بگذشت و هوای می بیغش دارم</p></div>
<div class="m2"><p>از مه عید و شفق نعل در آتش دارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هرکه بینی خوشی خود طلبد جز من زار</p></div>
<div class="m2"><p>که بدین ناخوشی خویش عجب خوش دارم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خورده ام تیر تو چون آهو از آن بار دگر</p></div>
<div class="m2"><p>از پی تیر دگر چشم به ترکش دارم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مکن ایدوست قیاس طرب از رنگ رخم</p></div>
<div class="m2"><p>که بصد خون جگر چهره منقش دارم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جان جفا چند کشد کز تو بجان رحمت نیست</p></div>
<div class="m2"><p>شرم باری من ازین جان بلاکش دارم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در چنین قحط کرم شاکرم از پیر مغان</p></div>
<div class="m2"><p>که اگر هیچ ندارم می بیغش دارم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گفت اهلی چو طبیب تو منم دل خوشدار</p></div>
<div class="m2"><p>من نه آنم که دل خسته مشوش دارم</p></div></div>