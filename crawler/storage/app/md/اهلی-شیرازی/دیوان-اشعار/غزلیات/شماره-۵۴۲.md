---
title: >-
    شمارهٔ ۵۴۲
---
# شمارهٔ ۵۴۲

<div class="b" id="bn1"><div class="m1"><p>نه آه از جان زار من برآمد</p></div>
<div class="m2"><p>که دود از روزگار من برآمد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ربودند از من مجنون غزالی</p></div>
<div class="m2"><p>که عمری در کنار من برآمد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مگر باد صبا ز آن سو گذر کرد</p></div>
<div class="m2"><p>که مشکین بوی یار من برآمد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عجب نبود که گرد از من برآمد</p></div>
<div class="m2"><p>چنین کز جا سوار من برآمد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خوشم اهلی کزین مژگان خونریز</p></div>
<div class="m2"><p>گلی آخر ز خار من برآمد</p></div></div>