---
title: >-
    شمارهٔ ۱۰۳۴
---
# شمارهٔ ۱۰۳۴

<div class="b" id="bn1"><div class="m1"><p>با چون تو شوخی سرو قد عاشق کش و طناز هم</p></div>
<div class="m2"><p>گل را نزیبد ناز کی سرو سهی را ناز هم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هرچند خونم میخوری هرگه که میبینم ترا</p></div>
<div class="m2"><p>دل میبرد سوی توام جان میکند پرواز هم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>صد خسته بهر پرسشی جان داد و تو عیسی نفس</p></div>
<div class="m2"><p>از ناز نگشودی لبی چشمی نکردی باز هم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر صد هزار از ما کشی چون مور زیر پای خود</p></div>
<div class="m2"><p>از ما نخیزد شکوه‌ای کس نشنود آواز هم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اهلی اگر مست بتی هم سجده شو با برهمن</p></div>
<div class="m2"><p>با شیخ مسجد کم نشین سجاده دور انداز هم</p></div></div>