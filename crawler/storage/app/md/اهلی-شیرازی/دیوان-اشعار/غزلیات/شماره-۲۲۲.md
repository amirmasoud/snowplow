---
title: >-
    شمارهٔ ۲۲۲
---
# شمارهٔ ۲۲۲

<div class="b" id="bn1"><div class="m1"><p>سرومن، صد خارم از دست تو در پا رفته است</p></div>
<div class="m2"><p>دست من گیر از کرم چون پایم از جا رفته است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بعد از این خواهد ز صحراسیل خون آمد بشهر</p></div>
<div class="m2"><p>بسکه خون دل ز چشم ما بصحرار رفته است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هیچکس را خاری از دست غمت بر پا نرفت</p></div>
<div class="m2"><p>هر چه رفت از زخم بیداد تو بر ما رفته است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آفتاب من اگر از روی زین گردد بلند</p></div>
<div class="m2"><p>پست گردد کار مه هر چند بالا رفته است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کس بیوسف ننگرد از گرمی بازار تو</p></div>
<div class="m2"><p>بلکه چون یوسف هزار اینجا بسودار رفته است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خسروان را آرزوی لعل شیرین تو کشت</p></div>
<div class="m2"><p>کوهکن دروادی حسرت نه تنها رفته است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در سر کوی بتان اهلی گر رفت رفت</p></div>
<div class="m2"><p>صد هزاران دین و دل اینجا بیغمار رفته است</p></div></div>