---
title: >-
    شمارهٔ ۱۲۹
---
# شمارهٔ ۱۲۹

<div class="b" id="bn1"><div class="m1"><p>ای نخل آرزو لب لعلت طبیب کیست</p></div>
<div class="m2"><p>پرورده یی عجب رطبی تا نصیب کیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بادی که می وزد خبر هجر می دهد</p></div>
<div class="m2"><p>بازاین سموم غم پی جان غریب کیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هرجا که عاشقی است فلک در کمین اوست</p></div>
<div class="m2"><p>آه این حسود سنگدل آخر رقیب کیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با هر کسی کرشمه خاصی است یار را</p></div>
<div class="m2"><p>کس را یقین نگشت که آن مه حبیب کیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون گلبن مراد به بیگانه داد گل</p></div>
<div class="m2"><p>اهلی که نالد از غم دل عندلیب کیست</p></div></div>