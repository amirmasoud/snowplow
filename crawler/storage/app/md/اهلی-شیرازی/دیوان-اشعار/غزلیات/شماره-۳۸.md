---
title: >-
    شمارهٔ ۳۸
---
# شمارهٔ ۳۸

<div class="b" id="bn1"><div class="m1"><p>گر کند ابر کرم میل تن خاکی ما</p></div>
<div class="m2"><p>چه تفاوت کند آلودگی و پاکی ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ما بدیدار توای ساقی جان دلشادیم</p></div>
<div class="m2"><p>نه که از هستی جانست فرحناکی ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با وجود تو برما همه عالم عدم است</p></div>
<div class="m2"><p>با وجود عدم فهم و کم ادراکی ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دوستان چاک گریبان حریفان دوزند</p></div>
<div class="m2"><p>هیچ رحمی نکند کس به جگر چاکی ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عاشق مردن خویشیم چو پروانه مست</p></div>
<div class="m2"><p>عقل دیوانه شد از مستی و بی باکی ما</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>او که صید دل ما کرد چنان راند سمند</p></div>
<div class="m2"><p>که بگردش نرسد چستی و چالاکی ما</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عاقبت در طلب گوهر وصلش اهلی</p></div>
<div class="m2"><p>غرقه بحر فنا گشت تن خاکی ما</p></div></div>