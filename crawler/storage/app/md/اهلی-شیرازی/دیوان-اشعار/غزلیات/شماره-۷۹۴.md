---
title: >-
    شمارهٔ ۷۹۴
---
# شمارهٔ ۷۹۴

<div class="b" id="bn1"><div class="m1"><p>دور از تو دل من به که پیغام فرستد</p></div>
<div class="m2"><p>پیغام همان به که دلارام فرستد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پیش تو حکایت بزبان دل و جان است</p></div>
<div class="m2"><p>آنجا رقم خامه دل خام فرستد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر نامه نویسد دلم از خون جگر هم</p></div>
<div class="m2"><p>کو طایر همت که بر آن بام فرستد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خواهم بدعا حرفی از آن لب که چو عیسی</p></div>
<div class="m2"><p>جان تازه کند گر همه دشنام فرستد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>لب تشنه آن رشحه کلکم مگرم بخت</p></div>
<div class="m2"><p>این رحمت خاص از کرم عام فرستد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نرگس برهش دیده سپید است چو یعقوب</p></div>
<div class="m2"><p>تا بوی خود آن سرو گلندام فرستد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هرگز نکند یاد من آن آهوی مشکین</p></div>
<div class="m2"><p>هم باد که بویی بهر ایام فرستد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اهلی چکند عرض هنر نزد کسی کو</p></div>
<div class="m2"><p>خاتم بسلیمان و به جم جام فرستد</p></div></div>