---
title: >-
    شمارهٔ ۳۳۷
---
# شمارهٔ ۳۳۷

<div class="b" id="bn1"><div class="m1"><p>در سوختنم آنکه بر افروختن آموخت</p></div>
<div class="m2"><p>شمعی است که پروانه ازو سوختن آموخت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آنروز که تعلیم نظر کرد مرا عشق</p></div>
<div class="m2"><p>اول ز رخ غیر نظر دوختن آموخت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل سوختن آموخت چو پروانه بعاشق</p></div>
<div class="m2"><p>آنکس که بدان شمع برافروختن آموخت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بلبل ز سرشک از غم گل دانه فشان است</p></div>
<div class="m2"><p>مورست که حرصش همه اندوختن آموخت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از فیض ازل علم نظر بازی اهلی</p></div>
<div class="m2"><p>فنی است که بی زحمت آموختن آموخت</p></div></div>