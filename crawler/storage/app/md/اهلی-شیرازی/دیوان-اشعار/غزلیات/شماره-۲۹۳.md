---
title: >-
    شمارهٔ ۲۹۳
---
# شمارهٔ ۲۹۳

<div class="b" id="bn1"><div class="m1"><p>نقد گنج کعبه جایش جز دل ویرانه نیست</p></div>
<div class="m2"><p>حلقه بر در گو مزن زاهد که کس در خانه نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در حریم کعبه و بتخانه عاشق محرم است</p></div>
<div class="m2"><p>هرکه با عاشق آشنا شد هیچ جا بیگانه نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سربسر احوال ما چون کوهکن جان کندن است</p></div>
<div class="m2"><p>قصه صاحبدلان درد دل است افسانه نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همچو مجنون نوغزالی رام من هرگز نشد</p></div>
<div class="m2"><p>چون کنم عقل معاشی با من دیوانه نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مست خون خویشتن چون مردم چشم خوردیم</p></div>
<div class="m2"><p>مستی اهل نظر از ساغر و پیمانه نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا سفال درد باشد جام زمزم گو مده</p></div>
<div class="m2"><p>لذتی درمی پرستی نیست تا مردانه نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کس چو اهلی شمع من در خون گرم خود نکشت</p></div>
<div class="m2"><p>رقص در آتش زدن جز شیوه پروانه نیست</p></div></div>