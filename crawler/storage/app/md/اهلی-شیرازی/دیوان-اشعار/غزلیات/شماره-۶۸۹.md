---
title: >-
    شمارهٔ ۶۸۹
---
# شمارهٔ ۶۸۹

<div class="b" id="bn1"><div class="m1"><p>آگه از شمع رخش هم عاشقی چون من شود</p></div>
<div class="m2"><p>هر کجا در گیرد آتش سوزاو روشن شود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یارمیباید که باشد، خانه گو ویرانه باش</p></div>
<div class="m2"><p>زانکه گر گلخن بود از روی او گلشن شود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عشقبازان را بدل باری که خوانندش بلا</p></div>
<div class="m2"><p>دانه مهری بود پرورش خرمن شود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شوخ خونخواری که هر کس دید آهی میکشد</p></div>
<div class="m2"><p>آه از آنجانی که آن خونخواره را مسکن شود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گرتنت سوزد چو شمع از عشق اهلی غم مخور</p></div>
<div class="m2"><p>عاشق آزاده کی هرگز اسیر تن شود</p></div></div>