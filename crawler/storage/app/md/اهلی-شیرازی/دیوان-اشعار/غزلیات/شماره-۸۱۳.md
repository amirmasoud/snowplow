---
title: >-
    شمارهٔ ۸۱۳
---
# شمارهٔ ۸۱۳

<div class="b" id="bn1"><div class="m1"><p>از بتان دل برکنم گویم مسلمان دگر</p></div>
<div class="m2"><p>ناگهان روی ترا بینم پشیمان دگر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا دل آزاده در زنجیر زلفت بسته ام</p></div>
<div class="m2"><p>با خود از دیوانگی دست و گریبانم دگر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خاطر از نظاره ات صد سال اگر جمع آورم</p></div>
<div class="m2"><p>یک نظر کز دیده ام رفتی پریشانم دگر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر دلم داری نگه دوری نباشد زانکه من</p></div>
<div class="m2"><p>دل بشرط آن ترا دادم که نستان دگر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>منکه عمری میگشودم عقده از کار کسان</p></div>
<div class="m2"><p>حالیا اهلی بحال خویش حیرانم دگر</p></div></div>