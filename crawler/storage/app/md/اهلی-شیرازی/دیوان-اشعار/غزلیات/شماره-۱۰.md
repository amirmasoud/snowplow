---
title: >-
    شمارهٔ ۱۰
---
# شمارهٔ ۱۰

<div class="b" id="bn1"><div class="m1"><p>الا ای ساقی گلرخ که گشتی شمع محفل‌ها</p></div>
<div class="m2"><p>ز غیرت عاشقان کشتی ز حسرت سوختی دل‌ها</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از آن روزست در دل‌ها خیال دانه خالت</p></div>
<div class="m2"><p>که دهقان ازل تخم محبت ریخت در دل‌ها</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فغان ای کعبه جان‌ها که در راه تمنایت</p></div>
<div class="m2"><p>چو مور از ضعف شیران را به هر گام است منزل‌ها</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>درین وادی مکن ای ساربان آرایش محمل</p></div>
<div class="m2"><p>که در خون شهیدان غرقه خواهی دید محمل‌ها</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز اشک گرم ما بحر کرم یارب به جوش آور</p></div>
<div class="m2"><p>وزین گرداب خون‌افکن گرفتاران به ساحل‌ها</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به جز پیر مغان زاهد، که سازد مشکل ما حل؟</p></div>
<div class="m2"><p>تو دانی حال خود مشکل چه دانی حل مشکل‌ها</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پی دنیا و مافیها گران‌جانی مکش اهلی</p></div>
<div class="m2"><p>قدح کش گر سبک‌روحی .....ل‌ها</p></div></div>