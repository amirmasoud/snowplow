---
title: >-
    شمارهٔ ۱۲۵۱
---
# شمارهٔ ۱۲۵۱

<div class="b" id="bn1"><div class="m1"><p>کی دل بی‌دردی از داغ گزندی سوخته</p></div>
<div class="m2"><p>هرکجا دیدیم عشقت دردمندی سوخته</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وه چه شمعست این که پیش عاشقان پروانه‌وار</p></div>
<div class="m2"><p>صدهزار افتاده چندی مرده چندی سوخته</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا بود شمع ایمن از چشم بدان گرد سرش</p></div>
<div class="m2"><p>هر نفس پروانگان مشت سپندی سوخته</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>می‌شود دودی بلند و می‌دمد بویی عجب</p></div>
<div class="m2"><p>تا کجا آن شوخ جان مستمندی سوخته</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آه ازین نازک‌پسندی‌های دل کاین جان زار</p></div>
<div class="m2"><p>هردم از نازک‌دلی مشکل‌پسندی سوخته</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پیش من فرهاد و مجنون مرم آزاده‌اند</p></div>
<div class="m2"><p>کم چو من دیوانه‌ای در قید و بندی سوخته</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر زمین و آسمان دشمن شود اهلی چه باک</p></div>
<div class="m2"><p>آه من بسیار ازین پست و بلندی سوخته</p></div></div>