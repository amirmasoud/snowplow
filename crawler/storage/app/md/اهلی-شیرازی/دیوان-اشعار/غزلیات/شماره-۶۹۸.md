---
title: >-
    شمارهٔ ۶۹۸
---
# شمارهٔ ۶۹۸

<div class="b" id="bn1"><div class="m1"><p>چشم صاحبدل نظر چون بر رخ گل می‌کند</p></div>
<div class="m2"><p>از جمال گل قیاس حال بلبل می‌کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هرکه را چون کوهکن لعل لب شیرین هواست</p></div>
<div class="m2"><p>بار کوه غم به یاد او تحمل می‌کند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عاشقی کز زهره‌رویان دل به وصل آلوده کرد</p></div>
<div class="m2"><p>گر ملک باشد که عشق او تنزل می‌کند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در دل پاکان که از روی صفا آیینه است</p></div>
<div class="m2"><p>مدعی ناپاکی خود را تعقل می‌کند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جز به خاک کوی او اهلی نیارد سر فرو</p></div>
<div class="m2"><p>آسمان بیهوده از وی این تخیل می‌کند</p></div></div>