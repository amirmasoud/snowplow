---
title: >-
    شمارهٔ ۸۰۳
---
# شمارهٔ ۸۰۳

<div class="b" id="bn1"><div class="m1"><p>به تاج عشق سر آدمی عزیز بود</p></div>
<div class="m2"><p>اگرنه عشق بود آدمی چه چیز بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تمیز خدمت اصحاب دل سگ آدم کرد</p></div>
<div class="m2"><p>سگ است بهتر از آن کس که بی تمیز بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به نوبت است درین بزم جام وصل دلا</p></div>
<div class="m2"><p>اگر نصیب شود دولت تو نیز بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو ذره ظن مبر ای دل که پیش آن خورشید</p></div>
<div class="m2"><p>هزار همچو تو را قدر یک پشیز بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تو گر به اهلی بی زر نظر کنی لطف است</p></div>
<div class="m2"><p>وگرنه هرکه تو بینی بزر عزیز بود</p></div></div>