---
title: >-
    شمارهٔ ۱۳۴
---
# شمارهٔ ۱۳۴

<div class="b" id="bn1"><div class="m1"><p>زبخت بد جگرم از جفای اوریش است</p></div>
<div class="m2"><p>بلای بخت بدم از جفای او بیش است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چه طالع است ندانم چه بخت شور است این</p></div>
<div class="m2"><p>گه گر جهان همه نوش است بخت من نیش است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حریف من نشود هیچ مهربان هرگز</p></div>
<div class="m2"><p>همیشه مونس من دلبری جفا کیش است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر چه کوه غمی پیش کوهکن آمد</p></div>
<div class="m2"><p>هزار کوه بلا هر طرف مرا پیش است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز خط آن لب لعلم جگر بود پاره</p></div>
<div class="m2"><p>فغان که نیش من است آنکه مرهم ریش است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سلامت دو جهان بخش سر بلندان است</p></div>
<div class="m2"><p>ملامت همه عالم نصیب درویش است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اگر به خلق غمی می رسد ز بیگانه</p></div>
<div class="m2"><p>بلا و محنت اهلی همیشه از خویش است</p></div></div>