---
title: >-
    شمارهٔ ۱۰۵۳
---
# شمارهٔ ۱۰۵۳

<div class="b" id="bn1"><div class="m1"><p>دل چو کوه از حسرت لعل تو خونشد چون کنم</p></div>
<div class="m2"><p>سال‌ها باید که این حسرت ز دل بیرون کنم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گرنه رسوا سازدم بوی محبت نافه وار</p></div>
<div class="m2"><p>پوست درپوشم چو آهو شیوه مجنون کنم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر بقدر گریه آبم در جگر باشد چو ابر</p></div>
<div class="m2"><p>آنقدر گریم که کوه و دشت را جیحون کنم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گه گهی در کنج غم از گریه سازم دل تهی</p></div>
<div class="m2"><p>بازآیم سوی آن بدخو و دل پر خون کنم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سوختم چون اهلی لب تشنه یارب رحم کن</p></div>
<div class="m2"><p>بیش ازین از چشمه حیوان صبوری چون کنم</p></div></div>