---
title: >-
    شمارهٔ ۱۳۰۳
---
# شمارهٔ ۱۳۰۳

<div class="b" id="bn1"><div class="m1"><p>دی شامگه کز پیش من مرکب بتندی تاختی</p></div>
<div class="m2"><p>رفتی چو خورشید از نظر روز مرا شب ساختی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون خون نریزد چشم من کز بعد عمری یکشبم</p></div>
<div class="m2"><p>ساقی شدی می دادیم بازم ز چشم انداختی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای در میان گلرخان قد تو در خوبی علم</p></div>
<div class="m2"><p>بشکست بازار بتان هرجا علم افراختی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ناگه نظر کردی بمن گفتم طبیب من شدی</p></div>
<div class="m2"><p>تا حال خود گفتم ترا با دیگران پرداختی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کارم چو ماه از مهر تو چون اندک اندک شد درست</p></div>
<div class="m2"><p>رخ تافتی باز از غمم چون ماه نو بگداختی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>من خود چه ارزم پیش تو کز لاله رویان جهان</p></div>
<div class="m2"><p>شد صدهزاران خاک و تو قدر یکی نشناختی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اهللی تو در نرد وفا از نقش وصل آن پری</p></div>
<div class="m2"><p>اول زدی داوی عجب آخر ولی درباختی</p></div></div>