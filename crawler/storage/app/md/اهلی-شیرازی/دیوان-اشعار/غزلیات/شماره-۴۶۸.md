---
title: >-
    شمارهٔ ۴۶۸
---
# شمارهٔ ۴۶۸

<div class="b" id="bn1"><div class="m1"><p>زلف یار از دست رفت و دل ازو دیوانه ماند</p></div>
<div class="m2"><p>مشک رفت از خانه اما بوی او در خانه ماند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خانه ناموس کندم از پی گنج مراد</p></div>
<div class="m2"><p>آن نیامد عاقبت در دست و این ویرانه ماند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شمع چندان سر کشید از ناز کز برق فنا</p></div>
<div class="m2"><p>خود نماند و داغ حسرت بر دل پروانه ماند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ساغر عشرت شکست و هر کسی راهی گرفت</p></div>
<div class="m2"><p>عاشق افتاده دل در گوشه میخانه ماند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گرچه آن شوخ پریوش را نماند آن حسن و ناز</p></div>
<div class="m2"><p>اهلی سرگشته باری از غمش دیوانه ماند</p></div></div>