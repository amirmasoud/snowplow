---
title: >-
    شمارهٔ ۴۳۸
---
# شمارهٔ ۴۳۸

<div class="b" id="bn1"><div class="m1"><p>هرچند آهنین دل ما ناز بیش کرد</p></div>
<div class="m2"><p>آهن ربای همت ما کار خویش کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بسی شیرهای جان بهم آمیخت روزگار</p></div>
<div class="m2"><p>تا لعل یار شربت دلهای ریش کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر جانبی بقصد محبان کشید تیغ</p></div>
<div class="m2"><p>اول مرا زمانه بد مهر پیش کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خارم ز لب بجای رطب نوخطان دهند</p></div>
<div class="m2"><p>بخت سیاه نوش مرا جمله نیش کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اهلی فکند یوسف جان را بچاره غم</p></div>
<div class="m2"><p>بیگانه با کسی نکند آنچه خویش کرد</p></div></div>