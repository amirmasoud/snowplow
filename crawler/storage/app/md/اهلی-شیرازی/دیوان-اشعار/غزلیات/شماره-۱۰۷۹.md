---
title: >-
    شمارهٔ ۱۰۷۹
---
# شمارهٔ ۱۰۷۹

<div class="b" id="bn1"><div class="m1"><p>ما سایه صفت سوخته وصل تو ماهیم</p></div>
<div class="m2"><p>دور از تو ز بیطالعی بخت سیاهیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر زندگی ما نه بدلخواه تو باشد</p></div>
<div class="m2"><p>بالله که ما زندگی خویش نخواهیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ما را کشی از هجر خود و زنده کنی باز</p></div>
<div class="m2"><p>عیسی روانبخشی و ما زنده گواهیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر ما نگهت نیست بجز گوشه چشمی</p></div>
<div class="m2"><p>ای آهوی چین کشته این نیم نگاهیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در کوی تو چون کاه بهر باد نلرزیم</p></div>
<div class="m2"><p>ما کوه غمیم از بد ایام نکاهیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون شمع فروغ دل ما ز آتش آهست</p></div>
<div class="m2"><p>ما زنده ازین دود دل و آتش آهیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز آسوده دلی بر در میخانه چو اهلی</p></div>
<div class="m2"><p>شاهنشه وقتیم و سگ بنده شاهیم</p></div></div>