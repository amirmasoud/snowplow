---
title: >-
    شمارهٔ ۱۶۱
---
# شمارهٔ ۱۶۱

<div class="b" id="bn1"><div class="m1"><p>مست شد صوفی و جنگش بر سر پیمانه است</p></div>
<div class="m2"><p>خانقه بر هم زد اکنون نوبت میخانه است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ساقیا می ده که جنت مستی و دیوانگی است</p></div>
<div class="m2"><p>دوزخی دارد کسی کو عاقل و فرزانه است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خودپرستان جهان آرایشی دارند و بس</p></div>
<div class="m2"><p>در خرابات است عیش و گنج در ویرانه است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عاشقی این است و جان کندن که من دارم از او</p></div>
<div class="m2"><p>قصه فرهاد و کوه بیستون افسانه است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر حذر باش ای رقیب از وی که هرکس پیش یار</p></div>
<div class="m2"><p>آشنایی بیش دارد بیشتر بیگانه است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یار نازک‌خوی، و ما مست، و رقیبان تندخو</p></div>
<div class="m2"><p>دم مزن اهلی چه وقت نعره مستانه است</p></div></div>