---
title: >-
    شمارهٔ ۷۱۰
---
# شمارهٔ ۷۱۰

<div class="b" id="bn1"><div class="m1"><p>فیروزی بخت همه کس لعل تو بخشید</p></div>
<div class="m2"><p>فیروزه ما بود که هرگز ندرخشید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چشم همه بر راه تو بازاست و تو از ناز</p></div>
<div class="m2"><p>یکچشم زدن رو ننمایی چو مه عید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هرچند که دوری ز نظر یاد وصالت</p></div>
<div class="m2"><p>هرگز نظری از دل ما دور نگردید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بی داغ محبت نتوان رفت ز عالم</p></div>
<div class="m2"><p>خوش وقت کسی کز چمن عمر گلی چید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جان بنده خلق خوش ساقی است که هرگز</p></div>
<div class="m2"><p>از بی ادبی های من مست نرنجید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اهلی رسی از عشق مجازی به حقیقت</p></div>
<div class="m2"><p>گر یار به تحقیق شناسی نه بتقلید</p></div></div>