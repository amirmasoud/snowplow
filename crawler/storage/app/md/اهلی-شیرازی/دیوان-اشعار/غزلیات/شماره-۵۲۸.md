---
title: >-
    شمارهٔ ۵۲۸
---
# شمارهٔ ۵۲۸

<div class="b" id="bn1"><div class="m1"><p>اطیفه عجب از غمزه نرگست دادند</p></div>
<div class="m2"><p>که عالمی کشد و خاطری برنجاند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خراب مستی آن عاشقم که در گامش</p></div>
<div class="m2"><p>چو جرعه یی بفشانند جان برافشاند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز باد فتنه میان من و فرج گرد است</p></div>
<div class="m2"><p>سفال باده بیاور که گرد بنشاند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به خاک میدکده تنها نه من فرو ماندم</p></div>
<div class="m2"><p>که صدهزار چو قارون بخود فروماند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ندانم از در دلها چه یافتی اهلی</p></div>
<div class="m2"><p>که همتت دو جهان را بهیچ نستاند</p></div></div>