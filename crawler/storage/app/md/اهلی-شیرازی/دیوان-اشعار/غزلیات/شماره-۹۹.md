---
title: >-
    شمارهٔ ۹۹
---
# شمارهٔ ۹۹

<div class="b" id="bn1"><div class="m1"><p>در عهد تو آسوده کس از داغ ستم نیست</p></div>
<div class="m2"><p>گر سنگ سیاه است که بی آتش غم نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خاک قدمت هرکه شود بگذرد از عرش</p></div>
<div class="m2"><p>ای خاک بر آن سر که ترا خاک قدم نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آزار دل ما مکن ای گل که حرام است</p></div>
<div class="m2"><p>مرغ دل عاشق کم از صید حرم نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر چند که جور تو ز اندیشه زیادست</p></div>
<div class="m2"><p>تا بیش بود مهر من از جور تو کم نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نشکفت دل ما زنسیم کرم کس</p></div>
<div class="m2"><p>افسوس که در باغ جهان بوی کرم نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خوشباش اگر درد و ملالی رسد از دوست</p></div>
<div class="m2"><p>در جام جهان صاف سلامت همه دم نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اهلی مزن از همدمی ماه و شان لاف</p></div>
<div class="m2"><p>کایشان نکنند این کرم و شان توهم نیست</p></div></div>