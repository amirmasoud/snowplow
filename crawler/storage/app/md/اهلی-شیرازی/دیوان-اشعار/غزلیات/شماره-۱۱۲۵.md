---
title: >-
    شمارهٔ ۱۱۲۵
---
# شمارهٔ ۱۱۲۵

<div class="b" id="bn1"><div class="m1"><p>من دردمند و ناتوان او سرکش و خونخواره هم</p></div>
<div class="m2"><p>حالم خراب از جور او کار دل بیچاره هم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هجرش همه محنت بود وصل آتش حسرت بود</p></div>
<div class="m2"><p>نی دوریم طاقت بود نی طاقت نظاره هم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز آهوی چشم آنجوان این پیر زار ناتوان</p></div>
<div class="m2"><p>مجنون صفت شد در جهان سرگشته و آواره هم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رویی چو برگ نسترن چشمی چو آهوی ختن</p></div>
<div class="m2"><p>مردم فریب و راهزن کافر دل و عیاره هم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا کی بقصد جان ما زلفش کشد باد صبا</p></div>
<div class="m2"><p>ایکاش از آن زلف دوتا ما را رسد یکتاره هم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زد عاشق ناشاد او صد جامه چاک از یاد او</p></div>
<div class="m2"><p>گر این بود بیداد او جانها شود صد پاره هم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>با چشم مست دلربا گر بنگرد آن بی‌وفا</p></div>
<div class="m2"><p>خلوت نشین پارسا عاشق شود میخواره هم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر من برم زان سرو قد این داغها زیر لحد</p></div>
<div class="m2"><p>گلهای آتش تا ابد خیزد ز خار از خاره هم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دور از رخ آن نازنین اهلی ز آه آتشین</p></div>
<div class="m2"><p>نگذاشت یک گل در زمین بر آسمان استاره هم</p></div></div>