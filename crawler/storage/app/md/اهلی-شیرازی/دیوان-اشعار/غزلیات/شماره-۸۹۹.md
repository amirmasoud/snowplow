---
title: >-
    شمارهٔ ۸۹۹
---
# شمارهٔ ۸۹۹

<div class="b" id="bn1"><div class="m1"><p>ره ز مستی بزنم باز به ویرانه خویش</p></div>
<div class="m2"><p>چون مرا شوق تو بیرون برد از خانه خویش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سنگ بر سینه زنان زان در دل می کوبم</p></div>
<div class="m2"><p>که ترا یافته ام در دل ویرانه خویش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تو و بانک طرب انگیز نی و جام شراب</p></div>
<div class="m2"><p>من و خون جگر و نعره مستانه خویش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>میدهی عاشق بی خویشتن از خود خبرش</p></div>
<div class="m2"><p>نه ز بیداد ز نی سنگ بدیوانه خویش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حال اهلی برسانید به مجنون که کند</p></div>
<div class="m2"><p>گریه بر حال من و خنده بر افسانه خویش</p></div></div>