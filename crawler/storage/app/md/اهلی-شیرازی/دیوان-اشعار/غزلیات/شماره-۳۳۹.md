---
title: >-
    شمارهٔ ۳۳۹
---
# شمارهٔ ۳۳۹

<div class="b" id="bn1"><div class="m1"><p>مریز بی گنهم خون که جست و جویی</p></div>
<div class="m2"><p>قیامتی و سیوالی و گفت و گویی هست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بزیر پای تو صد سر ز آرزو خاک است</p></div>
<div class="m2"><p>بیا که در ما سرما نیز آرزویی هست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گمان مبر که به بیداری و جگر خواری</p></div>
<div class="m2"><p>میان ما و سگت فرق جز بمویی هست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بخاک جرعه چه ریزی بروی من میریز</p></div>
<div class="m2"><p>ز خاک اگر چه کمم آخر آبرویی هست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تویی که در خم چوگان چو ماه چاردهت</p></div>
<div class="m2"><p>بهر کناره میدان شکسته گویی هست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بغیر من که چو خار از گل تو محرومم</p></div>
<div class="m2"><p>بقدر خود همه را از تو رنگ و بویی هست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بکوی میکده دردی کشان خوشحالیم</p></div>
<div class="m2"><p>خوشیم تا قدری باده در سبویی هست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هنوز با همه آلوده دامنی اهلی</p></div>
<div class="m2"><p>ز ابر رحمت آن یار شست و شویی هست</p></div></div>