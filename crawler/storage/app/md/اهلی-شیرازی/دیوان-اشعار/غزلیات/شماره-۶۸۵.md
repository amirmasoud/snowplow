---
title: >-
    شمارهٔ ۶۸۵
---
# شمارهٔ ۶۸۵

<div class="b" id="bn1"><div class="m1"><p>دنیا همه هیچ است و وفا هیچ ندارد</p></div>
<div class="m2"><p>بر هیچ منه دل که بقا هیچ ندارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ساقی می نوشین منه از دست که دوران</p></div>
<div class="m2"><p>در جام بجز زهر بلا هیچ ندارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خورشید من از لطف سراسر همه مهر است</p></div>
<div class="m2"><p>با من بجز از جور و جفا هیچ ندارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بی شمع رخش نیست صفا خانه دل را</p></div>
<div class="m2"><p>گر کعبه بود هم که صفا هیچ ندارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بحر کرم است آب حیات دهنش لیک</p></div>
<div class="m2"><p>آن بحر کرم بخش گدا هیچ ندارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آراسته شد از خط سبزش چمن حسن</p></div>
<div class="m2"><p>بی سبزه چمن نشو و نما هیچ ندارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اهلی است گدای تو از آن زدبدعاست</p></div>
<div class="m2"><p>در دست گدا غیر دعا هیچ ندارد</p></div></div>