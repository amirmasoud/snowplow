---
title: >-
    شمارهٔ ۳۵۱
---
# شمارهٔ ۳۵۱

<div class="b" id="bn1"><div class="m1"><p>آن سرو ناز کز چمن جان دمیده است</p></div>
<div class="m2"><p>شاخ گلی بصورت او کس ندیده است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن نوغزال با من مجنون انیس بود</p></div>
<div class="m2"><p>اکنون چه دیده است که از من رمیده است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با کس نگفته ام غم عشقش مگر صبا</p></div>
<div class="m2"><p>بوی محبت از نفس من شنده است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تلخ است در مذاق دلش آب زندگی</p></div>
<div class="m2"><p>لب تشنه یی که زهر جدایی چشیده است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آسوده دل فریب کمان ابرویان خورد</p></div>
<div class="m2"><p>ما زخم خورده ایم و دل ما رمیده است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>طوفان فتنه فلک آبیست زیر کاه</p></div>
<div class="m2"><p>ایمن مشو که بحر فلک آرمیده است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اهلی ز دامنش نکشد خار فتنه دست</p></div>
<div class="m2"><p>هرچند دامن از همه عالم کشیده است</p></div></div>