---
title: >-
    شمارهٔ ۱۱۱۱
---
# شمارهٔ ۱۱۱۱

<div class="b" id="bn1"><div class="m1"><p>ترک خوبان گرچه از دست ملامت میکنیم</p></div>
<div class="m2"><p>چون بهشتی صورتی دیدم قیامت میکنم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ذکر دل گر میکنم بر من گمان دل مبر</p></div>
<div class="m2"><p>من حدیثی با تو از روز سلامت میکنم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>میکنم عرض نیازی گر سگت را جان دهم</p></div>
<div class="m2"><p>تا نپنداری که اظهار کرامت میکنم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مستم و دیوانه و حسن پری پیش نظر</p></div>
<div class="m2"><p>لاجرم خود را برسوایی علامت میکنم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سجده بت کردم و آتش زدم در خانقه</p></div>
<div class="m2"><p>کافرم گر هیچ پروا از ندامت میکنم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>غرقه بحر غمم از اضطرابم چاره نیست</p></div>
<div class="m2"><p>من نه بیصبری ز راه استقامت میکنم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گرچه درویشم چو اهلی همتی دارم بلند</p></div>
<div class="m2"><p>زان سبب میل بتان سرو قامت میکنم</p></div></div>