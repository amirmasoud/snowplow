---
title: >-
    شمارهٔ ۱۹۲
---
# شمارهٔ ۱۹۲

<div class="b" id="bn1"><div class="m1"><p>ای سرو روان خاک شوم در ته پایت</p></div>
<div class="m2"><p>جان من و جان همه عالم به فدایت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در خلوت دل شو که چه جای دگران است</p></div>
<div class="m2"><p>بالله که جان هم نتوان دید به جایت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در حشر قیامت بود آن دم که شهیدان</p></div>
<div class="m2"><p>خیزند و سراسیمه درافتند به پایت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر کشته شوم بهر تو یکبار بکو حیف</p></div>
<div class="m2"><p>تا زنده شوم از نفس روح فزایت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بسیار زند بر سر خود سنگ ندامت</p></div>
<div class="m2"><p>آن کو دل و دین باخت به امید وفایت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جایی که زلیخا نکند ناله ز یوسف</p></div>
<div class="m2"><p>زن بهتر از آن مرد که نالد ز جفایت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اهلی، به گدایی ز تو خرسند شد اما</p></div>
<div class="m2"><p>شاهنشه حسنی چه غم از حال گدایت</p></div></div>