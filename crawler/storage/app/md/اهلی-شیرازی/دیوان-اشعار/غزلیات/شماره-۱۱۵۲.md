---
title: >-
    شمارهٔ ۱۱۵۲
---
# شمارهٔ ۱۱۵۲

<div class="b" id="bn1"><div class="m1"><p>ای چرخ پی مجلس او ز آب و گل من</p></div>
<div class="m2"><p>جامی کن و بنویس بر آن حال دل من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر دانه غم کاری و گر تخم ملامت</p></div>
<div class="m2"><p>جز سبزه مهرت ندهد ز آب و گل من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من بنده این داغ غلامی که تو گویی</p></div>
<div class="m2"><p>برنامه آزادی ات اینک سجل من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بیم است که آید پری از شوق و بسوزد</p></div>
<div class="m2"><p>پروانه صفت بر سر شمع چگل من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یک عاشق بیدل نتوان یافت چو اهلی</p></div>
<div class="m2"><p>کش دل نبود جانب پیمان گسل من</p></div></div>