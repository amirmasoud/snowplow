---
title: >-
    شمارهٔ ۲۲۸
---
# شمارهٔ ۲۲۸

<div class="b" id="bn1"><div class="m1"><p>گذشت در هوست عمر و یک نفس باقی است</p></div>
<div class="m2"><p>هنوز تا نفسی هست این هوس باقی است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بهار عمر خزان گشت و گل برفت از باغ</p></div>
<div class="m2"><p>هنوز مرغ مرا ناله در قفس باقی است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بسوخت با تو شکر لب رقیب روسیهم</p></div>
<div class="m2"><p>شکر نماند مرا زحمت مگس باقی است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>برفت صید مرادم ز پیش چشم و هنوز</p></div>
<div class="m2"><p>هزار تیر ملامت ز پیش و پس باقی است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگرچه دین و دل از دست رفت غم نبود</p></div>
<div class="m2"><p>به پای‌بوس تو ما را چو دسترس باقی است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اگرچه سوخت چو شمع و نفس نزد اهلی</p></div>
<div class="m2"><p>هنوز طعنه یاران هم‌نفس باقی است</p></div></div>