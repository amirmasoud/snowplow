---
title: >-
    شمارهٔ ۸۹۶
---
# شمارهٔ ۸۹۶

<div class="b" id="bn1"><div class="m1"><p>چو وقت گریه کردن رو نهم بر سوی دیوارش</p></div>
<div class="m2"><p>شوم بهوش و باز آیم بهوش از بوی دیوارش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون مراغ بسملم گر افکند از کوی خود بیرون</p></div>
<div class="m2"><p>طپم در خاک و خون چندان که افتم سوی دیوارش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون افتاده بیمارم که همچون صورت بیجان</p></div>
<div class="m2"><p>نشستن نیستم قوت مگر پهلوی دیوارش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نوشتم بر در و دیوار محنت خانه غم چندان</p></div>
<div class="m2"><p>که از مشق جنون من سیه شد روی دیوارش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو نگذارد که رو درروی دیوارش نهم اهلی</p></div>
<div class="m2"><p>دهم تسکین دل باری به گفتگوی دیوارش</p></div></div>