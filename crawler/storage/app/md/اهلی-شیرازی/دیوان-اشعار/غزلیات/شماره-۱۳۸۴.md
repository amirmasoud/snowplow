---
title: >-
    شمارهٔ ۱۳۸۴
---
# شمارهٔ ۱۳۸۴

<div class="b" id="bn1"><div class="m1"><p>گر به بتخانه درآیی و موافق باشی</p></div>
<div class="m2"><p>به که در قبله کنی روی و منافق باشی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جیب جان چاک کند صبح و دم از مهر زند</p></div>
<div class="m2"><p>اگر اینکار کنی عاشق صادق باشی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عاشق از خود چه برون رفت بمعشوق رسید</p></div>
<div class="m2"><p>وین نیابی مگر آن وقت که عاشق باشی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پایه دار فنا مرتبه مردان است</p></div>
<div class="m2"><p>سعی آن کن که بدین مرتبه لایق باشی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حسن عذارا نه که با لاله عذاران نبود</p></div>
<div class="m2"><p>همه عذر است هر آنگه که تو وامق باشی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا تو در بند سری از همه کس پست تری</p></div>
<div class="m2"><p>زیر پا سر چو نهی بر همه فایق باشی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اهلی این خرقه بینداز که زنار دروست</p></div>
<div class="m2"><p>پیش از آنروز که رسوای خلایق باشی</p></div></div>