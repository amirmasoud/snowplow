---
title: >-
    شمارهٔ ۱۱۲۴
---
# شمارهٔ ۱۱۲۴

<div class="b" id="bn1"><div class="m1"><p>ایهمه آرزوی تو فکر من و خیال هم</p></div>
<div class="m2"><p>چند بر آرزو زیم آرزوی محال هم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>لعبت چنین مگر تویی کز هوس تو بت پرست</p></div>
<div class="m2"><p>صوفی سالخورده شد کودک خردسال هم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شامگهی ببام خود جلوه ناز اگر کنی</p></div>
<div class="m2"><p>ناز تو ماه بشکند چین جبین هلال هم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای بکمال نیکویی رشگ فرشته و پری</p></div>
<div class="m2"><p>نیست برین جمال کس کیست بدین کمال هم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شد به سپهر دلبری حسن رخ تو مهر و مه</p></div>
<div class="m2"><p>ماه تو بیکمی ولی مهر تو بی زوال هم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ناز تو میکشد مرا نام وصال چون برم</p></div>
<div class="m2"><p>قطع امید کرده ام از خود و از وصال هم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در حرم وصال تو روح قدس نمیرسد</p></div>
<div class="m2"><p>اهلی خاکسار را جا که دهد مجال هم</p></div></div>