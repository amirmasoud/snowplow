---
title: >-
    شمارهٔ ۱۱۷۵
---
# شمارهٔ ۱۱۷۵

<div class="b" id="bn1"><div class="m1"><p>از در کعبه چه حاصل به در یار نشین</p></div>
<div class="m2"><p>روی بر دوست کن و پشت به دیوار نشین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر دلت تیره بود رشته تسبیح چه سود</p></div>
<div class="m2"><p>سینه صافی کن و در حلقه ز نار نشین</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در صف میکده سجاده نگنجد ایشیخ</p></div>
<div class="m2"><p>بفکن این بار غم از دوش و سبکبار نشین</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اینزمان کان قد رعنا به خرامش برخاست</p></div>
<div class="m2"><p>یکدم ای کبک خرامنده ز رفتار نشین</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>منکه با یار نشینم ز جهانم چه خبر</p></div>
<div class="m2"><p>گو بطعنم همه بر خیز و در انکار نشین</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>وصل یوسف طلبی پرده ناموس بدر</p></div>
<div class="m2"><p>چون زلیخا بدر آ بر سر بازار نشین</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کار ما از صف مسجد نگشاید اهلی</p></div>
<div class="m2"><p>یک قدم پیش نه و در صف خمار نشین</p></div></div>