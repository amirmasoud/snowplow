---
title: >-
    شمارهٔ ۳۹۸
---
# شمارهٔ ۳۹۸

<div class="b" id="bn1"><div class="m1"><p>آنکس طلبد کعبه که بیگانه عشق است</p></div>
<div class="m2"><p>گر عاشق و مستی همه جا خانه عشق است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای عقل تو و مسجد معموره تقوی</p></div>
<div class="m2"><p>ماییم و خرابات که ویرانه عشق است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از بحر وجود و صدف سینه آدم</p></div>
<div class="m2"><p>مقصود خدا گوهر یکدانه عشق است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در مدرسه و صومعه و دیر و خرابات</p></div>
<div class="m2"><p>حرفی که شنیدی همه افسانه عشق است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با اینهمه هشیاری و عشقی که ملک راست</p></div>
<div class="m2"><p>دل شیفته شیوه مستانه عشق است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مشکن دل دیوانه ام ای شیشه گردون</p></div>
<div class="m2"><p>از وی بحذر باش که دیوانه عشق است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اهلی اگرش ساغر جم نیست همین بس</p></div>
<div class="m2"><p>کز درد سفالی سک میخانه عشق است</p></div></div>