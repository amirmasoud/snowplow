---
title: >-
    شمارهٔ ۶۳۸
---
# شمارهٔ ۶۳۸

<div class="b" id="bn1"><div class="m1"><p>ای شاه حسن آنکه ترا تخت و تاج داد</p></div>
<div class="m2"><p>ما را بگوشه نظری احتیاج داد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جام جم از حقیقت لعلت خبر نداشت</p></div>
<div class="m2"><p>عشقم نشان بجوهر جام زجاج داد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جانم نماند هیچ تو دانی و تن دگر</p></div>
<div class="m2"><p>تا کی توانم از ده ویران خراج داد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>من خسته حریصم و جویم شراب وصل</p></div>
<div class="m2"><p>دلبر طبیب حاذق و صبرم علاج داد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ساقی بیا که رونق ازین بزم رفته بود</p></div>
<div class="m2"><p>اهلی دگر بگوهر نظمش رواج داد</p></div></div>