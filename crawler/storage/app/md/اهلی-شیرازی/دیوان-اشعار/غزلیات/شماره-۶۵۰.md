---
title: >-
    شمارهٔ ۶۵۰
---
# شمارهٔ ۶۵۰

<div class="b" id="bn1"><div class="m1"><p>تا عشق از آن ما شد بخت از جهان بر افتاد</p></div>
<div class="m2"><p>تا ملک حسن از او شد مهر از میان بر افتاد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حسنش که بود پنهان برقع فکند ناگه</p></div>
<div class="m2"><p>شور از جهان بر آمد عشق نهان بر افتاد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا خانه کرد آن مه در کوچه خرابات</p></div>
<div class="m2"><p>بسس خانمان فروشند بس خاندان بر افتاد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>باران عشق آمد پایم بگل فروشد</p></div>
<div class="m2"><p>سیل بلا بر آمد بنیاد جان بر افتاد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از نام ما نشانی در خاطر که ماند؟</p></div>
<div class="m2"><p>ما را که همچو اهلی نام و نشان بر افتاد</p></div></div>