---
title: >-
    شمارهٔ ۱۲۰۹
---
# شمارهٔ ۱۲۰۹

<div class="b" id="bn1"><div class="m1"><p>از بسکه نازک است چو گل طبع و خوی تو</p></div>
<div class="m2"><p>خورشید ذره ذره درآید بکوی تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ما مست نکهت تو نه امروز ای گلیم</p></div>
<div class="m2"><p>پیش از شکفتن تو شنیدیم بوی تو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تو آتشی گرت بپرستیم عاقبت</p></div>
<div class="m2"><p>آتش زند بخرمن ما شمع روی تو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ما تشنه چون سکندر و تو چشمه حیات</p></div>
<div class="m2"><p>تا زنده ایم کم نشود آرزوی تو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آن شمع روشنی که چو پروانه مرغ دل</p></div>
<div class="m2"><p>گر آتشش زنی نگریزد ز سوی تو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دیوانه ایم و با سر زلفت به پیچ و تاب</p></div>
<div class="m2"><p>کس درنیافت سلسله مار موی تو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اهلی بجستجوی تو در عالم آمدست</p></div>
<div class="m2"><p>خواهد شد از جهان بهمین جستجوی تو</p></div></div>