---
title: >-
    شمارهٔ ۷۲۳
---
# شمارهٔ ۷۲۳

<div class="b" id="bn1"><div class="m1"><p>آمد آن عیسی نفس کز عشوه و نازم کشد</p></div>
<div class="m2"><p>زنده ام سازد به مهر و از جفا بازم کشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روز وصل آمد ولی ترسم که در روز چنین</p></div>
<div class="m2"><p>طالع ناسازگار و بخت ناسازم کشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گو: بدارم بر کش و بر بستر هجرم مکش</p></div>
<div class="m2"><p>کشتنی گر کشته ام باری سرافرازم کشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مادر دهرم چه می پرورد عمری در کنار؟</p></div>
<div class="m2"><p>کانچنین در خاک و خون آنترک طنازم کشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همچو اهلی دارم از راز غمش گنجی نهان</p></div>
<div class="m2"><p>گر کشد عشق آخر از افشای اینرازم کشد</p></div></div>