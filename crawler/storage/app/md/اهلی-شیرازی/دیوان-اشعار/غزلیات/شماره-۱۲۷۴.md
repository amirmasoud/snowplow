---
title: >-
    شمارهٔ ۱۲۷۴
---
# شمارهٔ ۱۲۷۴

<div class="b" id="bn1"><div class="m1"><p>آدم و گندم، من و خال لب جانانه‌ای</p></div>
<div class="m2"><p>من نه آن مرغم که در دام آردم هر دانه‌ای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>درنگیرد صحبت من با دم ارباب عقل</p></div>
<div class="m2"><p>هم مگر در جوشم آرد آتش دیوانه‌ای</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سوختم از صلح و جنگت همچو آتش تا به کی</p></div>
<div class="m2"><p>گه برافروزی چراغی گه بسوزی خانه‌ای</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>باده می باید که صافی باشد و ساقی لطیف</p></div>
<div class="m2"><p>بزم شاهی گر نباشد گوشهٔ ویرانه‌ای</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پیش از آن اهلی که خواب واپسین گیرد ترا</p></div>
<div class="m2"><p>حالیا از عشق او فرصت شمار افسانه‌ای</p></div></div>