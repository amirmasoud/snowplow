---
title: >-
    شمارهٔ ۸۱۵
---
# شمارهٔ ۸۱۵

<div class="b" id="bn1"><div class="m1"><p>چند بود دمبدم چشم تو خونریز تر</p></div>
<div class="m2"><p>یا نظر رحمتی یا نگهی تیزتر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل ز خزان غمت پیر شد و همچنان</p></div>
<div class="m2"><p>لعل تو سیراب تر نخل تو نو خیزتر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>روی تو از گل بود تازه تر ای نوبهار</p></div>
<div class="m2"><p>خط تو از سنبل است غالیه آمیزتر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زآب حیات لبت سبزه خط بردمید</p></div>
<div class="m2"><p>لعل دلاویز تو گشت دلاویزتر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اهلی اگر گلرخان در پی خونریزی اند</p></div>
<div class="m2"><p>دیده گستاخ تست از همه خونریزتر</p></div></div>