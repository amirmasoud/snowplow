---
title: >-
    شمارهٔ ۲۳۵
---
# شمارهٔ ۲۳۵

<div class="b" id="bn1"><div class="m1"><p>از لطف اگرچه با سگ خویشت عجب خوش است</p></div>
<div class="m2"><p>من کیستم که با تو نشینم ادب خوش است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر کعبه وصال تو نتوان بسعی یافت</p></div>
<div class="m2"><p>ننشینم از طلب که درین ره طلب خوش است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر بر تو خوش بود غم و اندوه عاشقی</p></div>
<div class="m2"><p>ورنی بهر که هست نشاط و طرب خوش است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>من منع صوفی از می و شاهد نمیکنم</p></div>
<div class="m2"><p>با لعل یار ساغر می لب بلب خوش است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>صاحب خرد خمار بلا سال و مه کشد</p></div>
<div class="m2"><p>اهلی که مست عشق بود روز و شب خوش است</p></div></div>