---
title: >-
    شمارهٔ ۹۸۲
---
# شمارهٔ ۹۸۲

<div class="b" id="bn1"><div class="m1"><p>نیستم طاقت که آه داد خواهی بشنوم</p></div>
<div class="m2"><p>کآتشم در جان فتد هرگه که آهی بشنوم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پیش ازین راضی نمیگشتم بدیداری ز دود</p></div>
<div class="m2"><p>راضیم اکنون که نامش گاهگاهی بشنوم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گریه ام یاد آورد چون مردم ماتمزده</p></div>
<div class="m2"><p>ناله افتاده یی هرگه براهی بشنوم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شوق آن چابک سوار از خانه ام بیرون کشد</p></div>
<div class="m2"><p>در هر آن منزل که آواز سپاهی بشنوم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>داد بر خونم گواهی خونبها این بس مرا</p></div>
<div class="m2"><p>کاین گواهی از لب چون او گواهی بشنوم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز آتش غیرت رود دودم به سر اهلی چو شمع</p></div>
<div class="m2"><p>از زبان هر که نام کج کلاهی بشنوم</p></div></div>