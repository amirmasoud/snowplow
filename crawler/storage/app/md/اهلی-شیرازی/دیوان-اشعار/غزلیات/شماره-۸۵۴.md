---
title: >-
    شمارهٔ ۸۵۴
---
# شمارهٔ ۸۵۴

<div class="b" id="bn1"><div class="m1"><p>دیده بوصل آرمید دل نشود خوش هنوز</p></div>
<div class="m2"><p>آب ز سر برگذشت در جگر آتش هنوز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آ]وی سرگشته شد کشته به تیر نظر</p></div>
<div class="m2"><p>گر نبرد ترک مست دست بترکش هنوز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از گذر سیل اشک نقش بصر شسته شد</p></div>
<div class="m2"><p>و از اثر خون دل چهره منقش هنوز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کار دل آشفتگان راست شد از نوخطان</p></div>
<div class="m2"><p>خاطر مجموع ما از تو مشوش هنوز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گرچه بروی چو روز زلف چو شب بسته یی</p></div>
<div class="m2"><p>روز کنی شام ما از رخ مهوش هنوز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سبزه تر خشک شد غنچه گل باد برد</p></div>
<div class="m2"><p>نرگس ما خاک شد سرو سهی خوش هنوز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خاک تن کوهکن باد بهر گوشه برد</p></div>
<div class="m2"><p>صورت شیرین زناز مانده برابرش هنوز</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هر خس و خاری که بود گشت بکویت عزیز</p></div>
<div class="m2"><p>اهلی شوریده بخت خوار و ستم کش هنوز</p></div></div>