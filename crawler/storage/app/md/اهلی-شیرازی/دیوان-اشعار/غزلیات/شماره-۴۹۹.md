---
title: >-
    شمارهٔ ۴۹۹
---
# شمارهٔ ۴۹۹

<div class="b" id="bn1"><div class="m1"><p>شوخی که خون من چو می ناب می‌خورد</p></div>
<div class="m2"><p>شاخ گلی است کز دل من آب می‌خورد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هرگه فکند بر گل رخ زلف تابدار</p></div>
<div class="m2"><p>ما را چو شمع رشته جان تاب می‌خورد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل با خراش ناوک او خوش بود مرا</p></div>
<div class="m2"><p>عود آن زمان خوش است که مضراب می‌خورد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>باور مکن که گوشه‌نشین گشت چشم او</p></div>
<div class="m2"><p>مست است و می به گوشه محراب می‌خورد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دل صید زخم دار وز دست و زبان خلق</p></div>
<div class="m2"><p>هرجا که رفت ناوک پرتاب می‌خورد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بهر خدا مگوی به دشمن حدیث تلخ</p></div>
<div class="m2"><p>کاین نیش زهر بر دل احباب می‌خورد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اهلی ز اضطراب تو دانی که آگه است</p></div>
<div class="m2"><p>صیدی که تشنه خنجر قصاب می‌خورد</p></div></div>