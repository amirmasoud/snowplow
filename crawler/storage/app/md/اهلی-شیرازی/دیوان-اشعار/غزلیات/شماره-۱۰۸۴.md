---
title: >-
    شمارهٔ ۱۰۸۴
---
# شمارهٔ ۱۰۸۴

<div class="b" id="bn1"><div class="m1"><p>غم چون تو آفتابی ز جهان پسند دارم</p></div>
<div class="m2"><p>من اگر چو ذره پستم نظری بلند دارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل ریش من متاعی نبود ولی مکن رد</p></div>
<div class="m2"><p>که من از متاع عالم دل دردمند دارم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بفراق خو گرفتم ز هلاک خود چه باکم</p></div>
<div class="m2"><p>چو بمگر دل نهادم چه غم از گزند دارم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شکرم مده چو طوطی سخنی بگو از آن لب</p></div>
<div class="m2"><p>که من از لب تو مستم چه مذاق قند دارم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو گلم خجل ندانم که کدام زهر نوشتم</p></div>
<div class="m2"><p>چکنم که صد جراحت من مستمند دارم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز کمند زلف هر سو چه نهی براه دامم</p></div>
<div class="m2"><p>تو تعب مکش که منهم سر این کمند دارم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نه دل است اینکه دارم بتن ضعیف اهلی</p></div>
<div class="m2"><p>که بتار عنکبوتی مگسی ببند دارم</p></div></div>