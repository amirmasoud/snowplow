---
title: >-
    شمارهٔ ۲۹۶
---
# شمارهٔ ۲۹۶

<div class="b" id="bn1"><div class="m1"><p>غیر سوز و گریه کار عاشق درمانده چیست</p></div>
<div class="m2"><p>ای گل رعنا ترا بر گریه ما خنده چیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>باغبان گل زد در گلزار را در عهد او</p></div>
<div class="m2"><p>گر گل از رویت خجل شد باغبان شرمنده چیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کشتن تخم محبت بنده را کار دل است</p></div>
<div class="m2"><p>ابر رحمت گر نمیبارد گناه بنده چیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا منم در خون خود غرقم ز بخت بد مدام</p></div>
<div class="m2"><p>من چه میدانم که بخت و طالع فرخنده چیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یار چون خواهد هلاکم مردنم از بهر کیست</p></div>
<div class="m2"><p>چون دل معشوق کشتن خواست عاشق زنده چیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>فرق بسیاری زقدر پادشه تا بنده است</p></div>
<div class="m2"><p>پیش خورشید جمال او مه تابنده چیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گلرخان سرمست عیش و همدمان را حال خوش</p></div>
<div class="m2"><p>کس نمی پرسد که حال اهلی درمانده چیست</p></div></div>