---
title: >-
    شمارهٔ ۶۷۲
---
# شمارهٔ ۶۷۲

<div class="b" id="bn1"><div class="m1"><p>چون مرغ بسملم خبر ار ترک سر نشد</p></div>
<div class="m2"><p>تیغ تو ریخت خونم و هیچم خبر شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یعقوب هم بباخت دل و چشم بهر دوست</p></div>
<div class="m2"><p>چشم و دلی که در سر کار نظر نشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا بخت ره بکعبه وصلت کرا دهد؟</p></div>
<div class="m2"><p>ورنه کسی بسعی ز من بیشتر نشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بیچاره من که در طلب بوی زلف تو</p></div>
<div class="m2"><p>کار دلم چونافه بخون جگر نشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اهلی ببزم یار کسی یاد ما نکرد</p></div>
<div class="m2"><p>بس چون شود حکایت ما کاینقدر نشد</p></div></div>