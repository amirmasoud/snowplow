---
title: >-
    شمارهٔ ۶۰
---
# شمارهٔ ۶۰

<div class="b" id="bn1"><div class="m1"><p>زلف چو مار او کشد در دهن بلا مرا</p></div>
<div class="m2"><p>چون نروم که مو کشان می کشد اژدها مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همچو مگس در انگبین کوشش هرزه میکنم</p></div>
<div class="m2"><p>دست ز خویش مگسلم تا تو کنی رها مرا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زخم دل از تو تابکی؟ صبر نماند و طاقتم</p></div>
<div class="m2"><p>وه چه کنم مگر دهد صبر و دلی خدا مرا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با همه آفتاب من از سر مهر طالع است</p></div>
<div class="m2"><p>هیچ وفا نمی کند طالع بی وفا مرا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کوه بلا چو اهلیم گر نه ز غم بیفکند</p></div>
<div class="m2"><p>دست اجل به زور خود کی فکند ز پا مرا</p></div></div>