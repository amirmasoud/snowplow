---
title: >-
    شمارهٔ ۱۳۷
---
# شمارهٔ ۱۳۷

<div class="b" id="bn1"><div class="m1"><p>هر کو ندید خواب و نداند که خورد چیست</p></div>
<div class="m2"><p>داند چو من که عشق چه چیزست و درد چیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای شاخ گل بسوختم از غیرت صبا</p></div>
<div class="m2"><p>گر نیست عاشق این آه سرد چیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>این بس نوازشم که چو داد از غمت زنم</p></div>
<div class="m2"><p>گویی فغان این سگ بیهوده گرد چیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در بیخودی اگر نه بپایت فتاده ام</p></div>
<div class="m2"><p>در چشمم آب حسرت و بر چهره گرد چیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مردی که زخم ناوک عشق ترا نخورد</p></div>
<div class="m2"><p>واقف نشد که ناله مردان مرد چیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ما خود شکسته ایم ترا با شکستگان</p></div>
<div class="m2"><p>ای ترک جنگجوی نزاع و نبرد چیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جایی که نقش هستی خود شسته ایم ما</p></div>
<div class="m2"><p>اهلی سرشک لعل تو و رویی زرد چیست</p></div></div>