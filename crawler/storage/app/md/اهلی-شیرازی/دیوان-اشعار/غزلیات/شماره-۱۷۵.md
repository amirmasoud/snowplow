---
title: >-
    شمارهٔ ۱۷۵
---
# شمارهٔ ۱۷۵

<div class="b" id="bn1"><div class="m1"><p>مجال حلقه صحبت کرابخانه تست؟</p></div>
<div class="m2"><p>که کعبه حلقه چشمش بر آستانه تست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>میانه تن و جان باوجود مهر ازل</p></div>
<div class="m2"><p>کجاست آنچه میان من و میانه تست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بهانه جویی و کامم نمیدهی دانم</p></div>
<div class="m2"><p>غرض ز خواهش من عشوه و بهانه تست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرا به خازن جنت چکار و نسیه خلد</p></div>
<div class="m2"><p>که آنچه نقد مرا دست در خزانه تست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نشان نماند ز تیرت جز استخوان از ما</p></div>
<div class="m2"><p>ولی خوشیم که این استخوان نشانه تست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>من از زبانه آتش چه ترسم ای دوزخ</p></div>
<div class="m2"><p>مگر زبان ملامت کم از زبانه تست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بیا که صحبت مارا صفا تویی اهلی</p></div>
<div class="m2"><p>چراغ مجلس ما آه عاشقانه تست</p></div></div>