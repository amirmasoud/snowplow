---
title: >-
    شمارهٔ ۸۴۸
---
# شمارهٔ ۸۴۸

<div class="b" id="bn1"><div class="m1"><p>گرچه زارم سوختی من عاشق زارم هنوز</p></div>
<div class="m2"><p>روشن است از دود آهم کاتشی دارم هنوز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گرچه نقد دین و دل همچون زلیخا باختم</p></div>
<div class="m2"><p>یوسف ار سودای من دارد خریدارم هنوز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بسته فتراک عشقم کی بمردن وارهم</p></div>
<div class="m2"><p>با وجود آنکه جان دادم گرفتارم هنوز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در گلستان صد هزاران گل شکفت و باد برد</p></div>
<div class="m2"><p>من ز جور باغبان آزرده و خورام هنوز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یوسف از زندان برآمد یونس از ماهی برست</p></div>
<div class="m2"><p>صبر ایوبی سرآمد من دل افکارم هنوز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در خرابات مغان اهلی ندارد هیچ کم</p></div>
<div class="m2"><p>گرچه پیرم بامی و معشوق در کارم هنوز</p></div></div>