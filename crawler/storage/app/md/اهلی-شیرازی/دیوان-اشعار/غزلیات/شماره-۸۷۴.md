---
title: >-
    شمارهٔ ۸۷۴
---
# شمارهٔ ۸۷۴

<div class="b" id="bn1"><div class="m1"><p>امشب که چراغ نظری چرب زبان باش</p></div>
<div class="m2"><p>دل نرم کن ای شمع و مرا مرهم جان باش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حسنت بصفا بهتر از آن گشت که بودست</p></div>
<div class="m2"><p>زنهار که در حسن وفا هم به از آن باش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خواهی که سرافراز شوی در همه عالم</p></div>
<div class="m2"><p>چون سرو سهی راست دل و راست زبان باش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بیگانه شو از مردم و گر هم بتوانی</p></div>
<div class="m2"><p>چون مردم چشم از نظر خویش نهان باش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تعلیم فراغت دهم ایدل به دو حرفت</p></div>
<div class="m2"><p>عاشق شو و در سایه آن سرو روان باش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اکسیر مرادی که کند خاک زر سرخ</p></div>
<div class="m2"><p>گر میطلبی خاک ره پیر مغان باش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون صید غزالان همه مجنون صفتانند</p></div>
<div class="m2"><p>اهلی پی خود گم کن و بی نام و نشان باش</p></div></div>