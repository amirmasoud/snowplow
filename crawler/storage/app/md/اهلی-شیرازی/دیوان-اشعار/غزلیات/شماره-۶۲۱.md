---
title: >-
    شمارهٔ ۶۲۱
---
# شمارهٔ ۶۲۱

<div class="b" id="bn1"><div class="m1"><p>گر صد هزار رنج و تعب باغبان برد</p></div>
<div class="m2"><p>گل چون شکفت باد صبا از میان برد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آب بقا مجوی که ظلمات روزگار</p></div>
<div class="m2"><p>مشکل که خضر هم بگذارد که جان برد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر سنگ اگر کنیم نشان نام خود چه سود</p></div>
<div class="m2"><p>ما را که سیل حادثه نام و نشان برد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عمری بخون دیده چه پروردم آن غزال؟</p></div>
<div class="m2"><p>کز چمگ من زمانه چنان رایگان برد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اهلی تو را که قبله دو باشد نماز تو</p></div>
<div class="m2"><p>شرمنده آن فرشته که بر آسمان برد</p></div></div>