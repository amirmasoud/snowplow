---
title: >-
    شمارهٔ ۱۲۳۹
---
# شمارهٔ ۱۲۳۹

<div class="b" id="bn1"><div class="m1"><p>مدعی در جوش و ما بیهوش از آن نو گل شده</p></div>
<div class="m2"><p>بلبلان خاموش و مرغ هرزه گو بلبل شده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آفتاب من که سوزد برق حسنش خشک و تر</p></div>
<div class="m2"><p>سبزه تر بین که گرد عارضش سنبل شده</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در کمین صید دل از زلف و خال و چشم مست</p></div>
<div class="m2"><p>فتنه ها دارد سرفتنه آن کاکل شده</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دیگرانرا جزوییی گر سوخت برق حسن او</p></div>
<div class="m2"><p>خرمن صاحبدلان بر باد جزو و کل شده</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همنشین آسان نشد اهلی بدان سر و سهی</p></div>
<div class="m2"><p>باغبان خون خورده عمری تا قرین گل شده</p></div></div>