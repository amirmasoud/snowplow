---
title: >-
    شمارهٔ ۸۰۷
---
# شمارهٔ ۸۰۷

<div class="b" id="bn1"><div class="m1"><p>ناخوش آن عمر که دور از رخ آن ماه رود</p></div>
<div class="m2"><p>عمر هم خوش نبود گرنه بدلخواه رود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سوختم در ره خوبان ز پریشانی دل</p></div>
<div class="m2"><p>چو پریشان نشود دل که بصد راه رود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یوسف از چاه زنخدان تو هرگز نرهد</p></div>
<div class="m2"><p>وای آن دل که بامید تو در چاه رود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>غیرت عشق زبان همه چون شمع بسوخت</p></div>
<div class="m2"><p>که مبادا بزبان نام تو ناگاه رود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>برق را زهره شبگردی آن کوی کجاست</p></div>
<div class="m2"><p>جان اهلی است که با مشعله آه رود</p></div></div>