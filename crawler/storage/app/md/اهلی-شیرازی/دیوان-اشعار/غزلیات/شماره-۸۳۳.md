---
title: >-
    شمارهٔ ۸۳۳
---
# شمارهٔ ۸۳۳

<div class="b" id="bn1"><div class="m1"><p>بسنگی دوستان را یاد میدار</p></div>
<div class="m2"><p>بگو دشمن چو سگ فریاد میدار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گرفتار تو ای گل صد هزارند</p></div>
<div class="m2"><p>تو خود را همچو سرو آزاد میدار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به خسرو گوی ای شیرین خدا را</p></div>
<div class="m2"><p>که شرم از کشتن فرهاد میدار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تن ویران عمارت کی پذیرد</p></div>
<div class="m2"><p>دل ویران بعشق آباد میدار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بیاد دیگران صد جام خوردی</p></div>
<div class="m2"><p>نکردی یاد اهلی یاد میدار</p></div></div>