---
title: >-
    شمارهٔ ۱۲۸۹
---
# شمارهٔ ۱۲۸۹

<div class="b" id="bn1"><div class="m1"><p>با دیگران بعشوه سخن هر نفس کنی</p></div>
<div class="m2"><p>بیچاره من که چون رسم از دور بس کنی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دانم که هست با همه جورت نظر بمن</p></div>
<div class="m2"><p>کز من چو بگذری نظری بازپس کنی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هرگز هوس بصحبت من نیستت ولی</p></div>
<div class="m2"><p>کی مدعی گذارد اگر هم هوس کنی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زینسانکه شیوه تو همه ناز و سر کشی است</p></div>
<div class="m2"><p>ایسرو ناز مشکل اگر میل کس کنی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اهلی بهوش باش که خواب غم آورد</p></div>
<div class="m2"><p>گر گوش بر فسانه من یکنفس کنی</p></div></div>