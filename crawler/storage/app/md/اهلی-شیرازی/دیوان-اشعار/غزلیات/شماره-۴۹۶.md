---
title: >-
    شمارهٔ ۴۹۶
---
# شمارهٔ ۴۹۶

<div class="b" id="bn1"><div class="m1"><p>هرکس که چشم مست تو نظاره می‌کند</p></div>
<div class="m2"><p>مژگان به صد سنان جگرش پاره می‌کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جایی که صدهزار سر افتاده هر طرف</p></div>
<div class="m2"><p>در آن میان که زخم مرا چاره می‌کند؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رحمی بکن به مردم عالم که هرکه هست</p></div>
<div class="m2"><p>فریاد از آن دو نرگس خونخواره می‌کند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا ننگرند روی تو چاووش غیرتت</p></div>
<div class="m2"><p>خلق از وجود در عدم آواره می‌کند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>من خود ننالم از تو به جور و جفا ولی</p></div>
<div class="m2"><p>داد از ستم که بخت ستمکاره می‌کند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شیرین برفت از نظر و کوهکن هنوز</p></div>
<div class="m2"><p>چون صورت ایستاده و نظاره می‌کند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اهلی که پاره‌پاره دل از خویش می‌برید</p></div>
<div class="m2"><p>این بار ترک خویش به یکباره می‌کند</p></div></div>