---
title: >-
    شمارهٔ ۳۲۸
---
# شمارهٔ ۳۲۸

<div class="b" id="bn1"><div class="m1"><p>از بسکه جان به تشنه لبی درد کرده است</p></div>
<div class="m2"><p>آب حیات بر دل من سرد کرده است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>امروز یافتم که چه درد از میان خلق</p></div>
<div class="m2"><p>مجنون و شان گمشده را فرد کرده است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گیرم که نیست ناله ام از غم نه عاقبت</p></div>
<div class="m2"><p>دردی است در دلم که رخم زرد کرده است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>فریاد از آن غزاله مشکین که بوی او</p></div>
<div class="m2"><p>ماراچو باد صبح جهانگرد کرده است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اهلی بکنج صومعه سلطان وقت بود</p></div>
<div class="m2"><p>عشقش گدای میکده پرورد کرده است</p></div></div>