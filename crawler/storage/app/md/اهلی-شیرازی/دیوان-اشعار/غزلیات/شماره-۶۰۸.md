---
title: >-
    شمارهٔ ۶۰۸
---
# شمارهٔ ۶۰۸

<div class="b" id="bn1"><div class="m1"><p>تا یار ز شوخی بکناری ننشیند</p></div>
<div class="m2"><p>دل در بر ما هم بقراری ننشیند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در گوشه میخانه بمستی دلم افتاد</p></div>
<div class="m2"><p>تا مست نیفتد بکناری ننشیند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر خاک شود چون من آلوده جهانی</p></div>
<div class="m2"><p>بر دامن پاک تو غباری ننشیند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ایکاش چو نایی ندهی وعده که عاشق</p></div>
<div class="m2"><p>در خون همه دم بهر تو باری ننشیند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر خار غمم رخنه بجان کرد غمی نیست</p></div>
<div class="m2"><p>امید که در پای تو خاری ننشیند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>با کار جهان اهلی دیوانه چکارش</p></div>
<div class="m2"><p>سودا زده هر کز پی کاری ننشیند</p></div></div>