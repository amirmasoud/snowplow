---
title: >-
    شمارهٔ ۶۶۲
---
# شمارهٔ ۶۶۲

<div class="b" id="bn1"><div class="m1"><p>لعلت به تلخ گویی دل در خروش آرد</p></div>
<div class="m2"><p>می تلخ و تند باید تا خون بجوش آرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ساقی ز فکر و عقلم هوشی که بود کم شد</p></div>
<div class="m2"><p>می ده که مستی می بازم بهوش آرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در کوی میفرشان بی خانمان شدم من</p></div>
<div class="m2"><p>باشد که رحم بر من هم میفروش آرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گرشحنه محبت مستان عشق گیرد</p></div>
<div class="m2"><p>رسوا ز خانه بیرون صد حرقه پوش آرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گل رارخ تو سوزد چون شمع رشته جان</p></div>
<div class="m2"><p>گولاله این فتیله بیرون ز گوش آرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ساقی زلال کوثر چون وعده در بهشت</p></div>
<div class="m2"><p>می ده کزین تحمل کی درد نوش آرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اهلی چو مست میرد در کوی میفروشان</p></div>
<div class="m2"><p>تا قصر حور عینش رضوان بدوش آرد</p></div></div>