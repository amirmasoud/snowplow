---
title: >-
    شمارهٔ ۱۳۲۲
---
# شمارهٔ ۱۳۲۲

<div class="b" id="bn1"><div class="m1"><p>از رشگ رخت گر دل گل درد نکردی</p></div>
<div class="m2"><p>گل در تب گرم این عرق سرد نکردی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر عشق خریدار رخ زرد نبودی</p></div>
<div class="m2"><p>اکسیر محبت رخ ما زرد نکردی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ایکاش نم از خون دلم خاک گرفتی</p></div>
<div class="m2"><p>تا گرد تو خیل و سپهت گرد نکردی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر آهوی چشم تو نبردی دلم از دست</p></div>
<div class="m2"><p>مجنون صفتم از دو جهان فرد نکردی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دورست سگ کوی تو ار مردمی ارنه</p></div>
<div class="m2"><p>شب عربده با عاشق شبگرد نکردی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اهلی سگ مرد ره عشقست بمردی</p></div>
<div class="m2"><p>گر مرد نبودی سخن از مرد نکردی</p></div></div>