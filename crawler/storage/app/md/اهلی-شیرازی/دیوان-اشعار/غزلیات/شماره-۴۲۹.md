---
title: >-
    شمارهٔ ۴۲۹
---
# شمارهٔ ۴۲۹

<div class="b" id="bn1"><div class="m1"><p>سوز حدیث شمع زبان را خبر نکرد</p></div>
<div class="m2"><p>حرف سر زبان بدل کی اثر نکرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غوغای رستخیز برآید ز عاشقان</p></div>
<div class="m2"><p>آن مست نازنین چه سر از خواب بر نکرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>طوبی که سرفرازی باغ بهشت یافت</p></div>
<div class="m2"><p>هرگز ز شرم قامت او سربدر نکرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کردم طمع بجرعه جامش قضا نهشت</p></div>
<div class="m2"><p>آه از قضا که رحم بما آنقدر نکرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زان آفتاب حسن مه بخت نو نشد</p></div>
<div class="m2"><p>تاسوی من بگوشه چشمی نظر نکرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عیبش مکن حسود اگر از رشک ما بمرد</p></div>
<div class="m2"><p>در عمر خویش بهتر از این یک هنر نکرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هرگز نکرد سوی من آن عشوه گر نگاه</p></div>
<div class="m2"><p>کز عشوه رخنه دگرم در جگر نکرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اهلی نظر نسبت به تیغ از جمال دوست</p></div>
<div class="m2"><p>این شوخ دیده بین که ز کشتن حذر نکرد</p></div></div>