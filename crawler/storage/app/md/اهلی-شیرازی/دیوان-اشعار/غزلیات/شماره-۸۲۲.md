---
title: >-
    شمارهٔ ۸۲۲
---
# شمارهٔ ۸۲۲

<div class="b" id="bn1"><div class="m1"><p>سنبل بگشا بر گل و سروت به خرام آر</p></div>
<div class="m2"><p>مرغ دل ما را ز عدم باز بدام آر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا کی بمراد دل خود صبح کنی شام</p></div>
<div class="m2"><p>صبحی بمراد دل ما نیز بشام آر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>لب بر لب جامی و مرا جان بلب از غم</p></div>
<div class="m2"><p>جانم بلب خویش بجای لب جام آر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یکبار لب آور که ببوسم بهمه عمر</p></div>
<div class="m2"><p>این باده جانبخش نگویم که مدام آر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با یار جفا پیشه ز فریاد چه حاصل</p></div>
<div class="m2"><p>گو آتش دل هم علم داد ببام آر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای جان، که بود غیر تو محرم بر جانان؟</p></div>
<div class="m2"><p>برخیز و پیامی ببر و هم تو پیام آر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گو یاد کن از کشته هجران بسلامی</p></div>
<div class="m2"><p>در کلبدش بار دگر جان بسلام آر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اهلی است غلام تو چو شد پیر مرانش</p></div>
<div class="m2"><p>ای تازه جوان رحم بر این پیر غلام آر</p></div></div>