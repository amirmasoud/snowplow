---
title: >-
    شمارهٔ ۸۸۲
---
# شمارهٔ ۸۸۲

<div class="b" id="bn1"><div class="m1"><p>بصبح وصل کشد این شب ستم خوشباش</p></div>
<div class="m2"><p>رسد بخانه ما آفتاب هم خوشباش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غمی که میرسد از دوست عاشقانه بکش</p></div>
<div class="m2"><p>کسی همیشه نماند اسیر غم خوشباش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تو مرغ زیرکی از خار و گل منال ایدل</p></div>
<div class="m2"><p>چو خار و گل همه خواهد شدن عدم خوشباش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز بهر جام جم و آب خضر غصه مخور</p></div>
<div class="m2"><p>نه آب خضر بماند نه جام جم خوشباش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز آفتاب به خیبت متاب رخ اهلی</p></div>
<div class="m2"><p>که بر تو نیز فتد سایه کرم خوشباش</p></div></div>