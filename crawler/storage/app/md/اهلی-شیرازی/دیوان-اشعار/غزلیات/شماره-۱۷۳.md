---
title: >-
    شمارهٔ ۱۷۳
---
# شمارهٔ ۱۷۳

<div class="b" id="bn1"><div class="m1"><p>گوهر دل گم شد و وقت فراغ از دست رفت</p></div>
<div class="m2"><p>پیش پای خود ندیدیم و چراغ از دست رفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سوختم از درد و داغش بیش از این طاقت نماند</p></div>
<div class="m2"><p>من بدرد از پا فتادم دل بداغ از دست رفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>میرود دوران گل چون باد ساقی فکر چیست</p></div>
<div class="m2"><p>تا تو در اندیشه یی گلگشت باغ از دست رفت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر سبو در میکشم عیبم مکن ای همنفس</p></div>
<div class="m2"><p>مستم و اندازه جام و ایاغ از دست رفت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا بکی اندیشه زلف دراز او کنم</p></div>
<div class="m2"><p>اهلی از فکر پریشانم دماغ از دست رفت</p></div></div>