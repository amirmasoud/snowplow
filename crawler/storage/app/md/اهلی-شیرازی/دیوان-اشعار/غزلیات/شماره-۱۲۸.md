---
title: >-
    شمارهٔ ۱۲۸
---
# شمارهٔ ۱۲۸

<div class="b" id="bn1"><div class="m1"><p>زهی ملاحت و خوبی که با تو محبوب است</p></div>
<div class="m2"><p>که خشم و ناز و وفا هرچه می کنی خوب است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بکن هر آنچه تو خواهی که گر وفا نبود</p></div>
<div class="m2"><p>کرشمه های جفا نیز از تو مطلوب است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تو آفتاب جهانی ازین مشو در تاب</p></div>
<div class="m2"><p>که ذره یی بهوا داری تو منسوب است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>میان ما و تو ای مه به نامه حاجت نیست</p></div>
<div class="m2"><p>که هر اشارت ابرو هزار مکتوب است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کنون که سنبل تر حلقه می زند بر گل</p></div>
<div class="m2"><p>مپوش زلف که مرغوله تو مرغوب است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به خاکروبی میخانه سرخوشم کانجا</p></div>
<div class="m2"><p>ز آستین مرقع صد آستین روب است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به بوسه گر دل مسکین نوازیی داری</p></div>
<div class="m2"><p>تو خود ببخش که اهلی گدای محجوب است</p></div></div>