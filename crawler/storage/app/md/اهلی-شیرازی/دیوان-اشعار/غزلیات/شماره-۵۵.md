---
title: >-
    شمارهٔ ۵۵
---
# شمارهٔ ۵۵

<div class="b" id="bn1"><div class="m1"><p>به خلوت بهل شیخ دل مرده را</p></div>
<div class="m2"><p>که دوزخ بهشت است افسرده را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل زاهد از می کجا بشکفد</p></div>
<div class="m2"><p>شکفتن محال است پژمره را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگر درد جامی دهد لعل تو</p></div>
<div class="m2"><p>زتریاک به زهر غم خورده را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز خاکم چو بر داشتی مفکنم</p></div>
<div class="m2"><p>میفکن نهال بر آورده را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مکن وعده از انتظارم مکش</p></div>
<div class="m2"><p>میازار دیگر دل آزرده را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از آن لاله در خون خود غرقه است</p></div>
<div class="m2"><p>که بر داغ دل می درد پرده را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دل اهلی از توبه بی چاره شد</p></div>
<div class="m2"><p>بلی چاره یی نیست خود کرده را</p></div></div>