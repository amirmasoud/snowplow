---
title: >-
    شمارهٔ ۸۶۰
---
# شمارهٔ ۸۶۰

<div class="b" id="bn1"><div class="m1"><p>اشارت تو بجان دادنم بشارت بس</p></div>
<div class="m2"><p>مرا به تیغ چه حاجت همین اشارت بس</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چنان بشارت وصل تو شادمانم کرد</p></div>
<div class="m2"><p>که گر تو نیز نیایی همین بشارت بس</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو کشته تو شوم بر مزار من بخرام</p></div>
<div class="m2"><p>که خونبهای من ایدوست این زیارت بس</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شهید خانه خراب تو خاک منزل ساخت</p></div>
<div class="m2"><p>درین سراچه فانی همین عمارت بس</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بخون دیده کنم سجده بتان اهلی</p></div>
<div class="m2"><p>نماز همچو منی را همین طهارت بس</p></div></div>