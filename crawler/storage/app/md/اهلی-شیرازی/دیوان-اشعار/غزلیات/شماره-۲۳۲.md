---
title: >-
    شمارهٔ ۲۳۲
---
# شمارهٔ ۲۳۲

<div class="b" id="bn1"><div class="m1"><p>نرگس رعنا اگر چشم و چراغ گلشن است</p></div>
<div class="m2"><p>لاله یی کز خون دل دارد نشان چشم من است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حاجت گفتن ندارد حال من ای شمع حسن</p></div>
<div class="m2"><p>قصه جانسوزی پروانه حرفی روشن است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دست چون در خون من دارد! اگر با دیگری</p></div>
<div class="m2"><p>دست در گردن کند خون منش در گردن است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آتش غم ساقیا ننشاند الا آب می</p></div>
<div class="m2"><p>رحم کن ای ابر رحمت کاتشم در خرمن است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جیب آن دارد از پیراهن یوسف نشان</p></div>
<div class="m2"><p>گکوری ایبرا در اینهمان پیراهن است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حال سوز دوستان ای شمع میدانی ولی</p></div>
<div class="m2"><p>من از آن سوزم که چشمت بر زبان دشمن است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نگسلم اهلی به جور از دامن آن گل چو خار</p></div>
<div class="m2"><p>هر کجا خواهد شدن دست منش در دامن است</p></div></div>