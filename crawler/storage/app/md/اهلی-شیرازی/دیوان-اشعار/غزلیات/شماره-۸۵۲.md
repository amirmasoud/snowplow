---
title: >-
    شمارهٔ ۸۵۲
---
# شمارهٔ ۸۵۲

<div class="b" id="bn1"><div class="m1"><p>شکر خدا که چشم تو بر ما فتاد باز</p></div>
<div class="m2"><p>دست دعای ما در دولت گشاد باز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گویی ببرد از دل ما هر غمی که بود</p></div>
<div class="m2"><p>لطفت بیک جواب سلامی که داد باز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چشمت بحال ما نظر مرحمت فکند</p></div>
<div class="m2"><p>و آن عشوه ها که داشت بیکسو نهاد باز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تاشد زیاده مهر تو ای آفتاب حسن</p></div>
<div class="m2"><p>شد سوز عاشقانه مراهم زیاد باز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بس نامرادیی که کشید از خمار هجر</p></div>
<div class="m2"><p>اهلی که شد ز وصل تو مست مراد باز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>باز بشکت گل دولت و یار آمد باز</p></div>
<div class="m2"><p>مژدگانی که خزان رفت و بهار آمد باز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دیده ام بسکه بخون موج زد از گریه چو بحر</p></div>
<div class="m2"><p>گوهری کز نظرم شد بکنار آمد باز</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نقد دل بردم و در کار نثارش کردم</p></div>
<div class="m2"><p>عاقبت این درم قلب بکار آمد باز</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>یار اینطایر فرخ چه سبکروح کسی است</p></div>
<div class="m2"><p>که برای دل موری بشکار آمد باز</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مرده بودم بسر رهگذر از رفتن دوست</p></div>
<div class="m2"><p>شکر ایزد که مسیحا بگذار آمد باز</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>اهلی از رشگ تو خاری بجگر خورد چه سود</p></div>
<div class="m2"><p>زین گل نو که ز باغ تو ببار آمد باز</p></div></div>