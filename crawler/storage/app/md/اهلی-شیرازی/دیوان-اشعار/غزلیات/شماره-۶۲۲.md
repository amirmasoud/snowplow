---
title: >-
    شمارهٔ ۶۲۲
---
# شمارهٔ ۶۲۲

<div class="b" id="bn1"><div class="m1"><p>بی تو چو شمع کرده ام گریه و خنده کار خود</p></div>
<div class="m2"><p>خنده بروز دل کنم گریه بروزگار خود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای چو غزال مشگبو صید تو صد هزار دل</p></div>
<div class="m2"><p>من چه سگم که آهویی چون تو کنم شکار خود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در دل پر غبار من گر گذری بهل که من</p></div>
<div class="m2"><p>پاک کنم ز دود دل سینه بی قرار خود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بی تو بهیچ صحبتی نیست سکون دل مرا</p></div>
<div class="m2"><p>هم تو بگو که چون کنم با دل بی قرار خود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اهلی اگرچه سالها داشت امید وصل تو</p></div>
<div class="m2"><p>شاد ندید یکنفس جان امیدوار خود</p></div></div>