---
title: >-
    شمارهٔ ۱۹۵
---
# شمارهٔ ۱۹۵

<div class="b" id="bn1"><div class="m1"><p>جز داغ دلم چراغ شب نیست</p></div>
<div class="m2"><p>وز طالع تیره این عجب نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر با سگت از ادب زنم دم</p></div>
<div class="m2"><p>او عفو کند ولی ادب نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تو نخل مرادی اینقدر هست</p></div>
<div class="m2"><p>کامید کس از تو یک رطب نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در عالمی ام ز مستی عشق</p></div>
<div class="m2"><p>کآنجا مه و سال و روز و شب نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اهلی ره عقلت آن پری زد</p></div>
<div class="m2"><p>دیوانگی تو بی سبب نیست</p></div></div>