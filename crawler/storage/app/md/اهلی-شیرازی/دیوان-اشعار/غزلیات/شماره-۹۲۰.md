---
title: >-
    شمارهٔ ۹۲۰
---
# شمارهٔ ۹۲۰

<div class="b" id="bn1"><div class="m1"><p>ز بهر کلبه عاشق دلا مجوی چراغ</p></div>
<div class="m2"><p>که شمع مجلس او بس بود فتیله داغ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل از چمن نگشاید اسیر عشق ترا</p></div>
<div class="m2"><p>که پیش بلبل عاشق قفس نماید باغ</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به جرعه یی بزن آبی بر آتشم که رسید</p></div>
<div class="m2"><p>ز سینه بوی کبابم ز شوق می بدماغ</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رقیب اگرچه عزیزست و ما چنین خواریم</p></div>
<div class="m2"><p>روا مدار که بلبل جفا کشد از زاغ</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بنه به کنج لحد دل ز سوز غم اهلی</p></div>
<div class="m2"><p>گرت هوس کند آسودگی و کنج فراغ</p></div></div>