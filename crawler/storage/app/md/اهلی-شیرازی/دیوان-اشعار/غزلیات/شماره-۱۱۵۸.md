---
title: >-
    شمارهٔ ۱۱۵۸
---
# شمارهٔ ۱۱۵۸

<div class="b" id="bn1"><div class="m1"><p>دارد رقیب بهر تو چشم حسد به من</p></div>
<div class="m2"><p>کاری مکن که کار کند چشم بد به من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گردون ز آستان توام گو مکن جدا</p></div>
<div class="m2"><p>زان روز که اینقدر ز جهان می‌رسد به من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سر در سر خیال تو خواهد شدن مرا</p></div>
<div class="m2"><p>این قصه گفته است خیال تو خود به من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>من قانعم به یک نگه از سرو قامتت</p></div>
<div class="m2"><p>بنما ز دور یک نظر ای سرو قد به من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اهلی ز بی‌کسی چه غم ار دشمنم بسی است</p></div>
<div class="m2"><p>غمخوار بی‌کسان برساند مدد به من</p></div></div>