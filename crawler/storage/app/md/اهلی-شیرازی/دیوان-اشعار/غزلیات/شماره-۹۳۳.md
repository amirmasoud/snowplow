---
title: >-
    شمارهٔ ۹۳۳
---
# شمارهٔ ۹۳۳

<div class="b" id="bn1"><div class="m1"><p>صد بار اگر از جور توام خون رود از دل</p></div>
<div class="m2"><p>از در چو درآیی همه بیرون رود از دل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بس خون دلی بایدم از دیده فرو ریخت</p></div>
<div class="m2"><p>تا آرزوی آن لب میگون رود از دل</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>لیلی همه در خنده و بازی است چه داند</p></div>
<div class="m2"><p>کز گریه چها بر سر مجنون رود از دل</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفتی که برون کن غم من از دل و خوشباش</p></div>
<div class="m2"><p>آه این سخن سخت مرا چون رود از دل</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اهلی مدم افسون که دوا لعل حبیب است</p></div>
<div class="m2"><p>کی درد و غم عشق به افسون رود از دل</p></div></div>