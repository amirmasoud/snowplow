---
title: >-
    شمارهٔ ۱۱۵۳
---
# شمارهٔ ۱۱۵۳

<div class="b" id="bn1"><div class="m1"><p>نشاید با لبت غیری چو طوطی هم‌نفس دیدن</p></div>
<div class="m2"><p>که از خال تو هم نتوان برین شکر مگس دیدن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بهشت وصلت از جور رقیبان دوزخ من شد</p></div>
<div class="m2"><p>که در یک دیدنم صد بار باید پیش و پس دیدن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>قفس بر مرغ جانم ای اجل بشکن که مشکل شد</p></div>
<div class="m2"><p>حریفان را به گل در حرف و خود را در قفس دیدن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرا از دیدنت گر دین و دنیا شد چه غم باشد</p></div>
<div class="m2"><p>که می‌ارزد دو عالم بر جمالت یک نفس دیدن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگر بخت زبون دارد چنین دست مرا کوته</p></div>
<div class="m2"><p>به شاخ آرزو هرگز نخواهم دسترس دیدن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو یعقوب از غم یوسف ز عالم دیده پوشیدم</p></div>
<div class="m2"><p>که من بی‌دوست در عالم نخواهم هیچکس دیدن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نظر بر غیر اهلی را دو جر مست ای گل خندان</p></div>
<div class="m2"><p>یکی از گل نظر بستن یکی بر خار و خس دیدن</p></div></div>