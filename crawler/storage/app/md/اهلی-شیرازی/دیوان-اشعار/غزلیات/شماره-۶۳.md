---
title: >-
    شمارهٔ ۶۳
---
# شمارهٔ ۶۳

<div class="b" id="bn1"><div class="m1"><p>دل اگر زغم خروشد چه غم از خروش اورا</p></div>
<div class="m2"><p>که فغان دردمندان نرسد بگوش اورا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل من زنوش آن لب نرسد بکام هرگز</p></div>
<div class="m2"><p>مگر از کسی که داد این لب همچو نوش اورا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگر ای عزیز یوسف به منش نمی فروشی</p></div>
<div class="m2"><p>به زکات حسن باری ز نظر مپوش او را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دل من چو دجله خون نکند چگونه طوفان</p></div>
<div class="m2"><p>که هوای دوست آرد همه دم به جوش اورا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زفغان این پریشان چه شو چو زلف درهم</p></div>
<div class="m2"><p>که زخ تو کرد غارت دل و صبر و هوش اورا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دل زار من چو بلبل به فغان میار ای گل</p></div>
<div class="m2"><p>که زبان اگر گشاید، که کند خموش اورا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو غلام تست اهلی مفروش و گر فروشی</p></div>
<div class="m2"><p>پی پاسبانی در به سگان فروش او را</p></div></div>