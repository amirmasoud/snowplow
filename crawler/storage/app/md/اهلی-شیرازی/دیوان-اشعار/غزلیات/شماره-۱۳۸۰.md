---
title: >-
    شمارهٔ ۱۳۸۰
---
# شمارهٔ ۱۳۸۰

<div class="b" id="bn1"><div class="m1"><p>گشاد خنده زنان چشم برمن آفت جانی</p></div>
<div class="m2"><p>چه خنده یی چه نگاهی چه نرگسی چه دهانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بچهره چشم فروزی بعشوه مرد گدازی</p></div>
<div class="m2"><p>عجب ملیح بیانی غریب چرب زبانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رخی چه نازک و دلجو قدی چه چابک و رعنا</p></div>
<div class="m2"><p>نه رخ که ماه تمامی نه قد که سرو روانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کجا ملاحت لیل کجا حلاوت شیرین</p></div>
<div class="m2"><p>ازین شکر دهنی فتنه یی بلای جهانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خموش اهلی ازین نکته تا کسی نبرد پی</p></div>
<div class="m2"><p>که داد این سخن آشنا غریب نشانی</p></div></div>