---
title: >-
    شمارهٔ ۱۱۵۷
---
# شمارهٔ ۱۱۵۷

<div class="b" id="bn1"><div class="m1"><p>گر بوسه دهی زان لب خندان که دهم جان</p></div>
<div class="m2"><p>بوسم دهن تنگ تو چندان که دهم جان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ارزان بود از بوسه خرم از تو بجان لیک</p></div>
<div class="m2"><p>هرگز ندهی بوسه ام ارزان که دهم جان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خضر ره من شو که درین ظلمت دوری</p></div>
<div class="m2"><p>نزدیک شد ایچشمه حیوان که دهم جان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هرچند بعیسی نفسی جان دهیم باز</p></div>
<div class="m2"><p>بازآ نفسی پیشتر از آن که دهم جان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر باد دهم جان بهوای تو چو اهلی</p></div>
<div class="m2"><p>گر باد رساند ز تو فرمان که دهم جان</p></div></div>