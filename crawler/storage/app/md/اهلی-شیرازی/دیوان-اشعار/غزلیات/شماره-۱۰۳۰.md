---
title: >-
    شمارهٔ ۱۰۳۰
---
# شمارهٔ ۱۰۳۰

<div class="b" id="bn1"><div class="m1"><p>چند از هوس آن لب چون قند بسوزم</p></div>
<div class="m2"><p>گر سوختنی هم شده ام چند بسوزم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مگذار که از تلخی آن گریه جانسوز</p></div>
<div class="m2"><p>در حسرت آن لعل شکر خند بسوزم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل نامزد عشق کسان ساخته جانرا</p></div>
<div class="m2"><p>در آتش تو بهر زبان بند بسوزم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دور از تو همان به که جنونم برد از هوش</p></div>
<div class="m2"><p>ورنه چو شوم بی تو خردمند بسوزم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اهلی دهدم پند که کم سوز ز مهرش</p></div>
<div class="m2"><p>ترسم که اگر سوزم ازین پند بسوزم</p></div></div>