---
title: >-
    شمارهٔ ۱۰۸
---
# شمارهٔ ۱۰۸

<div class="b" id="bn1"><div class="m1"><p>بسکه به دل می زنم سنگ که دلدار رفت</p></div>
<div class="m2"><p>کار دل از دست شد دست هم از کار رفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حال چه پرسی زمن از هجر سوخت</p></div>
<div class="m2"><p>کار به مردن کشید قصه ز گفتار رفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بود مرادم که زود شب بشود روز وصل</p></div>
<div class="m2"><p>کی به مراد کسی چرخ ستمکار رفت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عمر گرانمایه رفت هجر در آمد زدر</p></div>
<div class="m2"><p>پای گریزم نماند قوت رفتار رفت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ساقی مجلس برفت بزم بر آمد بهم</p></div>
<div class="m2"><p>گل زچمن شد برون رونق گلزار رفت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اهلی اگر جان کنی صرف سگانش رواست</p></div>
<div class="m2"><p>زانکه در آن حلقه دوش ذکر تو بسیار رفت</p></div></div>