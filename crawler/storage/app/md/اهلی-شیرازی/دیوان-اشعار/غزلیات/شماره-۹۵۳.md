---
title: >-
    شمارهٔ ۹۵۳
---
# شمارهٔ ۹۵۳

<div class="b" id="bn1"><div class="m1"><p>عالمی گر خون خورند از عشق ما هم می‌خوریم</p></div>
<div class="m2"><p>بخش خود ما نیز خون دل ز عالم می‌خوریم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پیش ما از ذوق شادی لذت غم خوش‌تر است</p></div>
<div class="m2"><p>تا نگویی دیگران شادند و ما غم می‌خوریم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جرعه نوشانیم فارغ از مرگ و حیات</p></div>
<div class="m2"><p>زان که آب زندگی از ساغر جم می‌خوریم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ما که خود لب بسته‌ایم از سرمستی همچو جم</p></div>
<div class="m2"><p>گر صراحی دم زند خونش به یک دم می‌خوریم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در سر کار بتان ساقی دلی گر رفت رفت</p></div>
<div class="m2"><p>جام می پر کن که ما اندوه دل کم می‌خوریم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ما ز گرد غیر چون آب روان دل شسته‌ایم</p></div>
<div class="m2"><p>از کدورت دم مزن با ما که بر هم می‌خوریم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>با رقیبان گر نداریم الفتی اهلی چه عیب</p></div>
<div class="m2"><p>آهوی صحرای عشقیم از سگان رم می‌خوریم</p></div></div>