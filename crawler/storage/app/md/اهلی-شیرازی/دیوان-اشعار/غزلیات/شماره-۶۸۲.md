---
title: >-
    شمارهٔ ۶۸۲
---
# شمارهٔ ۶۸۲

<div class="b" id="bn1"><div class="m1"><p>میرود از برم دگر، تا ببر که میرود؟</p></div>
<div class="m2"><p>تازه تر چه نخل گل در نظر که میرود؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>توسن خشم کرده زین، دامن ناز بر زده</p></div>
<div class="m2"><p>طرف کلاه کرده کج تا بسر که میرود؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بسته میان بچابکی رهزن دین عاشقان</p></div>
<div class="m2"><p>راه میزند دگر بر گذر که میرود؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در بدریم در طلب ما و دل از پیش ولی</p></div>
<div class="m2"><p>من بدر دل آمدم دل بدر که میرود؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر همه با حریف خود دست زنند در کمر</p></div>
<div class="m2"><p>دست ضعیف چون منی در کمر که میرود؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بی لب شکرین او من چه کنم شکر لبان</p></div>
<div class="m2"><p>تلخی زهر حسرتم از شکر که میرود؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زخم فراق بر جگر اهلی زار اگر نخورد</p></div>
<div class="m2"><p>اینهمه سیل خون بگو کز جگر که میرود؟</p></div></div>