---
title: >-
    شمارهٔ ۵۸۱
---
# شمارهٔ ۵۸۱

<div class="b" id="bn1"><div class="m1"><p>بقرار یک نظر دل ز تو چون کنار گیرد</p></div>
<div class="m2"><p>تو مگر بجان در آیی که کسی قرار گیرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو چنین بخون عاشق چو می شبانه نوشی</p></div>
<div class="m2"><p>بصباح حشر ترسم که ترا خمار گیرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بکشی به جورم آنگه بکشی به دار عبرت</p></div>
<div class="m2"><p>بتوهر که عشق ورزد زمن اعتبار گیرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تو بلطف آب خضری من اگر چه تشنه سوزم</p></div>
<div class="m2"><p>نزنم نفس مبادا که دلت غبار گیرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مژه را گشاد اهلی ندهد بگریه لیکن</p></div>
<div class="m2"><p>چکند که سیل خون را سر ره بخار گیرد</p></div></div>