---
title: >-
    شمارهٔ ۹۴۴
---
# شمارهٔ ۹۴۴

<div class="b" id="bn1"><div class="m1"><p>چون مرغ نیم بسمل چندانکه می‌تپیدم</p></div>
<div class="m2"><p>تسلیم تا نگشتم آسودگی ندیدم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خاک سیه کشیدم در دیده بیجمالش</p></div>
<div class="m2"><p>پیرانه سر چو طفلان خوش سرمه یی کشیدم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آخر به سنگ جورم بشکست شیشه دل</p></div>
<div class="m2"><p>دردا که در پی دل بیهوده میدویدم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خواهم بخاک بردن چون لاله داغ حسرت</p></div>
<div class="m2"><p>کز گلشن وصالش هرگز گلی نچیدم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اهلی اگرچه گشتم دیوانه لیک شادم</p></div>
<div class="m2"><p>کز قید خودپسندی وحشی صفت رمیدم</p></div></div>