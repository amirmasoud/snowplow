---
title: >-
    شمارهٔ ۱۲۷۳
---
# شمارهٔ ۱۲۷۳

<div class="b" id="bn1"><div class="m1"><p>بیا و درد هجران را دوا ده</p></div>
<div class="m2"><p>ز شربتخانه وصلم شفا ده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غم عاشق کشت داد جفا داد</p></div>
<div class="m2"><p>تو بر عکس ایپری داد وفا ده</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تراکان ملاحت آفریدند</p></div>
<div class="m2"><p>از آن کان نمک بخشی بما ده</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همه حسن و جوانی حق ترا داد</p></div>
<div class="m2"><p>زکوه آن بدین پیر گدا ده</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دلا، از گل هوا داری بیاموز</p></div>
<div class="m2"><p>تن و جان جمله بر باد هوا ده</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>صفای کعبه را با کعبه بگذار</p></div>
<div class="m2"><p>بیا و خانه دل را صفا ده</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بتلخی کوهکن میمرد و میگفت</p></div>
<div class="m2"><p>الهی جان شیرین را بقا ده</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بخوبان همعنان اهلی چه گردی</p></div>
<div class="m2"><p>عنان در دست تسلیم و رضا ده</p></div></div>