---
title: >-
    شمارهٔ ۱۱۸۶
---
# شمارهٔ ۱۱۸۶

<div class="b" id="bn1"><div class="m1"><p>باده می‌نوش و بدین مشغله می‌گذران</p></div>
<div class="m2"><p>کار عالم گذرانست تو هم می‌گذران</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو پسندیده‌خصالی و کرم شیوه تست</p></div>
<div class="m2"><p>هرچه از ما نپسندی به کرم می‌گذران</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جام جم گر نبود همت خود دار بلند</p></div>
<div class="m2"><p>باده می‌نوش و بصد حشمت جم می‌گذران</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>فکر فردا مکن امروز دل خود خوش دار</p></div>
<div class="m2"><p>در دل اندیشه ناآمده کم می‌گذران</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ساقی امروز که دریای کرم در جوش است</p></div>
<div class="m2"><p>کشتی غرقه ز گرداب عدم می‌گذران</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گه گهی دست من پیر به جامی می گیر</p></div>
<div class="m2"><p>وز گذر گاه غمم یک دو قدم می‌گذران</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اهلی از دوست به جز زخم ستم چون نرسد</p></div>
<div class="m2"><p>بگذر از مرهم و با زخم ستم می‌گذران</p></div></div>