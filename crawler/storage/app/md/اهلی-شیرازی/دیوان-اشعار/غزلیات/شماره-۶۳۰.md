---
title: >-
    شمارهٔ ۶۳۰
---
# شمارهٔ ۶۳۰

<div class="b" id="bn1"><div class="m1"><p>بر شمع فلک حسنت آن لحظه که ناز آرد</p></div>
<div class="m2"><p>از جلوه گه نازش با خاک نیاز آرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر طایر قدسی را بر خال تو چشم افتد</p></div>
<div class="m2"><p>از سیر حقیقت رو در دام مجاز آرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جانبخش لبی داری کاندم که سخن گوید</p></div>
<div class="m2"><p>از وادی خاموشی صد گمشده باز آرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای ظالم کافر دل وی کافر ظالم خو</p></div>
<div class="m2"><p>داغ تو مرا تا کی در سوز و گداز آرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هرچند ز ناز آن مه آزرده کند دلها</p></div>
<div class="m2"><p>اهلی تو نیاز آورد شاید که بناز آرد</p></div></div>