---
title: >-
    شمارهٔ ۱۰۴۲
---
# شمارهٔ ۱۰۴۲

<div class="b" id="bn1"><div class="m1"><p>شب وعده داد مست و بره دیده دوختم</p></div>
<div class="m2"><p>دل ساختم کباب و نیامد بسوختم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عیبم مکن ز سجده آن روی آتشین</p></div>
<div class="m2"><p>کز وی چراغ دولت و دین برفروختم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دیوانه گشتم از ستم این پریوشان</p></div>
<div class="m2"><p>از بسکه چاک خرقه به صد پاره دوختم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بختم فشرد پا بدرت کوری رقیب</p></div>
<div class="m2"><p>میخی عجب بدیده دشمن سپوختم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اهلی ز کشت دهر چو عنقابری نخورد</p></div>
<div class="m2"><p>من مرغ زیرکم که بیک جو فروختم</p></div></div>