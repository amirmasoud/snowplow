---
title: >-
    شمارهٔ ۸۶۱
---
# شمارهٔ ۸۶۱

<div class="b" id="bn1"><div class="m1"><p>هرگز گلی نچید دل من ز باغ کس</p></div>
<div class="m2"><p>روشن نگشت خانه من از چراغ کس</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر مژده وصال دگر دل نمی نهم</p></div>
<div class="m2"><p>بازی نمیخورم دگر از لهو و لاغ کس</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در عشق اگرچه هیچ فراغت دلم نیافت</p></div>
<div class="m2"><p>هرگز حسد نبرد به عیش و فراغ کس</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دایم اگر چه سوخته عشق بوده ام</p></div>
<div class="m2"><p>هرگز چنین نسوخت دل من ز داغ کس</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا رشته ی زجان و رگی باقی از تن است</p></div>
<div class="m2"><p>سودای زلف او نرود از دماغ کس</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اهلی وفا ز مردم عالم طمع مدار</p></div>
<div class="m2"><p>کاین جرعه کرم نبود در ایاغ کس</p></div></div>