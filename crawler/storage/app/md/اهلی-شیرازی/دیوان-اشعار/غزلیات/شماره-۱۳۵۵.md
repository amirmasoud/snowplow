---
title: >-
    شمارهٔ ۱۳۵۵
---
# شمارهٔ ۱۳۵۵

<div class="b" id="bn1"><div class="m1"><p>کاشکی فرق سرم فرش سرایت بودی</p></div>
<div class="m2"><p>تا شب و روز رخم در کف پایت بودی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>لذت زخم تو از راحت مرهم بیش است</p></div>
<div class="m2"><p>کاش بر من همه دم زخم جفایت بودی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کاش جان بلبل باغ تو شدی تا همه وقت</p></div>
<div class="m2"><p>در تماشای رخ روح فزایت بودی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای پریرخ مه نو ابروی شوخ تو ندید</p></div>
<div class="m2"><p>ورنه دیوانه و انگشت نمایت بودی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آنچه من یافتم از حسن تو گر دیدی خلق</p></div>
<div class="m2"><p>هر دو عالم همه یکبار فدایت بودی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اینهمه حشمت و خوبی که خدا داد بتو</p></div>
<div class="m2"><p>جای آنست که رحمی بگدایت بودی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اهلی از دعوی یاری بدرش جای نبود</p></div>
<div class="m2"><p>گر سگش میشدی آنجا همه جایت بودی</p></div></div>