---
title: >-
    شمارهٔ ۹۹۲
---
# شمارهٔ ۹۹۲

<div class="b" id="bn1"><div class="m1"><p>تا کی بغیر گویی من چشم و گوش باشم</p></div>
<div class="m2"><p>من هم زبان برآرم تا کی خموش باشم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من خاک آستانم گر مدعی غلامست</p></div>
<div class="m2"><p>این خاکساریم به کان خود فروش باشم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر زیر خاک چون خم خشت از سرم بر آری</p></div>
<div class="m2"><p>بینی کز آتش دل چون می بجوش باشم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خواهم که پیش خلقت مستانه ره بگیرم</p></div>
<div class="m2"><p>لیکن تو چون بیایی من کی بهوش باشم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>این بس که از تو گاهی نیشی خورم چو اهلی</p></div>
<div class="m2"><p>من کیستم کز آن لب در بند نوش باشم</p></div></div>