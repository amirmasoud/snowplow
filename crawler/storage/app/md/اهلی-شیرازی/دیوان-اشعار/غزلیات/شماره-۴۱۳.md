---
title: >-
    شمارهٔ ۴۱۳
---
# شمارهٔ ۴۱۳

<div class="b" id="bn1"><div class="m1"><p>دوش از دیرم ملک در حلقه او را برد</p></div>
<div class="m2"><p>ناگه آن بت پیش چشمم آمد آنها باد برد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عشق او بازی ببازی دست عقلم تاب داد</p></div>
<div class="m2"><p>شوخی شاگرد آخر پنجه استاد برد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سالها پرورد یوسف را بجان یعقوب زار</p></div>
<div class="m2"><p>چون عزیز مصر گشت اورا تمام از یاد برد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>من غلط کردم که اول ره ببستم بر سرشک</p></div>
<div class="m2"><p>قطره قطره سیل گشت و خانه از بنیاد برد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خسرو از بخت است شیرین کام آه از دست بخت</p></div>
<div class="m2"><p>رنج ضایع در جهان از طالعش فرهاد برد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عندلیب عاشق از بیداد گل هرچند سوخت</p></div>
<div class="m2"><p>کافرم گر غیر او پیش کسی فریاد برد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اهلی، آن بازیچه دیدی کان تذرو خوش خرام</p></div>
<div class="m2"><p>چون بدام افتاد و چون رفت و دل از صیاد برد</p></div></div>