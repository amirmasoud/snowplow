---
title: >-
    شمارهٔ ۱۱۰۶
---
# شمارهٔ ۱۱۰۶

<div class="b" id="bn1"><div class="m1"><p>ز رقیب او چه سازم که کند نظر بکین هم</p></div>
<div class="m2"><p>چه رخی گشاده دارد که کند گره جبین هم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز غم بهشت رویی من خسته را چه دوزخ</p></div>
<div class="m2"><p>جگریست پر ز آتش نفسی است آتشین هم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بکشد هزار عاشق نکشیده تیغ و شاید</p></div>
<div class="m2"><p>که برون چو غنچه نارد سر دست و آستین هم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سگ آهوان چشمت به نیاز صد چو مجنون</p></div>
<div class="m2"><p>نه همین نیازمندان که هزار نازنین هم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چه کنم کجا گریزم ز کمان ابروی او</p></div>
<div class="m2"><p>گر ازین کمان گریزم اجلست در کمین هم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بنشاط و ناز خلقی گل وصل باز چیدند</p></div>
<div class="m2"><p>من و جور باغبانان نه همان گل و همین هم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>همه عمر چشم اهلی بجمال یار بازست</p></div>
<div class="m2"><p>نه نظر بر آسمانش نه نگاه بر زمین هم</p></div></div>