---
title: >-
    شمارهٔ ۷۹۰
---
# شمارهٔ ۷۹۰

<div class="b" id="bn1"><div class="m1"><p>بتر ز محنت هجر ای پسر چه خواهد بود</p></div>
<div class="m2"><p>جدا ز وصل توام زین بتر چه خواهد بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به حلق تشنه ام از جرعه لب تو چه کم</p></div>
<div class="m2"><p>که قطره یی نچکد اینقدر چه خواهد بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سرشک گرم که چشمم چو لاله داغ از اوست</p></div>
<div class="m2"><p>چو دیده داغ کند در جگر چه خواهد بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز عشق بس نکنم گر سرم چو شمع برند</p></div>
<div class="m2"><p>به پیش اهل نظر ترک سر چه خواهد بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حکایت لب شیرین بجان دهد لذت</p></div>
<div class="m2"><p>وگرنه لذت قند و شکر چه خواهد بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز سیم اشک و رخ زرد من چه میخواهی</p></div>
<div class="m2"><p>تو جان و سر بطلب سیم و زر چه خواهد بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>حسود بی هنرت عیب اگر کند اهلی</p></div>
<div class="m2"><p>حزین مشو سخن بی هنر چه خواهد بود</p></div></div>