---
title: >-
    شمارهٔ ۱۰۶
---
# شمارهٔ ۱۰۶

<div class="b" id="bn1"><div class="m1"><p>نیازمند ترا رسم خود پسندی نیست</p></div>
<div class="m2"><p>طریق اهل دلان غیر دردمندی نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به خلق و لطف تو نازیم ای سهی بالا</p></div>
<div class="m2"><p>که سرو قد تورا ناز سر بلندی نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مریض عشق تورا شربت طبیب چه سود</p></div>
<div class="m2"><p>دوای خسته دلان جز لبان قندی نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرا سجود نیاز از نماز شیخ به است</p></div>
<div class="m2"><p>نماز زنده دلان جز نیازمندی نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خروش مرغ سحر زارتر بود اهلی</p></div>
<div class="m2"><p>جراحتی بتر از زخم مستمندی نیست</p></div></div>