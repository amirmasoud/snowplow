---
title: >-
    شمارهٔ ۸۲۶
---
# شمارهٔ ۸۲۶

<div class="b" id="bn1"><div class="m1"><p>از بهر قتل عاشق دلخسته چشم یار</p></div>
<div class="m2"><p>تیغی کشیده از مژه همچون زبان مار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پاس دل است مایه هر دولتی که هست</p></div>
<div class="m2"><p>هان ای پسر گر اهل دلی دل نگاهدار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ساقی بجرعه یی بنشان گرد هستی ام</p></div>
<div class="m2"><p>تا از میان ما و تو برخیزد این غبار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دست بتی بگیر و بگو عالم آب بر</p></div>
<div class="m2"><p>جامی شراب درکش و گو سیل غم ببار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اهلی چه گوشه گیر شوی هیچکاره یی</p></div>
<div class="m2"><p>با مردم زمانه بسر بردن است کار</p></div></div>