---
title: >-
    شمارهٔ ۱۱۸۰
---
# شمارهٔ ۱۱۸۰

<div class="b" id="bn1"><div class="m1"><p>گر با من مستی حذر از بیهده گو کن</p></div>
<div class="m2"><p>در سینه من آی و در فتنه فرو کن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون غنچه دمد بوی ترا ایگل ز دل من</p></div>
<div class="m2"><p>زنهار که بشکاف دل ریشم و بو کن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای قبله پرست از بت ما شرم نداری</p></div>
<div class="m2"><p>تا چند کنی سجده گل روی باو کن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کاری نکنی دیده زهاد که شورست</p></div>
<div class="m2"><p>ساقی می ازین طایفه پنهان بسبو کن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چوگان سر زلف بر آور بسر دوش</p></div>
<div class="m2"><p>سرهای عزیزان همه در معرکه گو کن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای تشنه می شیره رز آب حیاتست</p></div>
<div class="m2"><p>تا زهر نگردد حذر از عربده جو کن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>رخسار نکو آینه صنع الهی است</p></div>
<div class="m2"><p>اهلی به ادب سجده آن روی نکو کن</p></div></div>