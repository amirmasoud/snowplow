---
title: >-
    شمارهٔ ۵۳۹
---
# شمارهٔ ۵۳۹

<div class="b" id="bn1"><div class="m1"><p>چشم صاحبدل نظر چون بر رخ گل می‌کند</p></div>
<div class="m2"><p>از جمال گل قیاس حال بلبل می‌کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر کرا چون کوهکن بار غم از شیرین‌لبی است</p></div>
<div class="m2"><p>لاجرم گر کوه غم باشد تحمل می‌کند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پاکبازان از صفا آیینه‌اند و مدعی</p></div>
<div class="m2"><p>صورت آلودهٔ خود را تخیل می‌کند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عاشقی کز زُهره‌رویان دل به وصل آلوده کرد</p></div>
<div class="m2"><p>گر ملک باشد که عشق او تنزل می‌کند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از فریب آهوی چشمش مشو غافل که او</p></div>
<div class="m2"><p>حال ما می‌داند و عمدا تغافل می‌کند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جز به خاک کوی او اهلی نیارد سر فرو</p></div>
<div class="m2"><p>آسمان بیهوده این عرض تجمل می‌کند</p></div></div>