---
title: >-
    شمارهٔ ۵۶۳
---
# شمارهٔ ۵۶۳

<div class="b" id="bn1"><div class="m1"><p>چشمه نوش نوخطان مهر گیابر آورد</p></div>
<div class="m2"><p>چشمه چشم عاشقان خار بلا بر آورد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چند نهیم بر زمین روی نیاز بهر او</p></div>
<div class="m2"><p>چند بر آسمان کسی دست دعا بر آورد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گرچه ز نوگل رخش بوی وفا نمی دهد</p></div>
<div class="m2"><p>باشد از آب چشم ما رنگ وفا بر آورد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر بکشد ز غمزه ام غم نبود ز سر مرا</p></div>
<div class="m2"><p>ترسم از آنکه تیغ او سر بجفا بر آورد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ایکه رقیب ما شدی گر همه بیستون شوی</p></div>
<div class="m2"><p>تکیه مکن که آه ما کوه ز جا بر آورد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اهلی اگرچه کار دل نیست بکام از آن دهان</p></div>
<div class="m2"><p>صبر که کار به شود کام خدا بر آورد</p></div></div>