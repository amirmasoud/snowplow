---
title: >-
    شمارهٔ ۳۶۹
---
# شمارهٔ ۳۶۹

<div class="b" id="bn1"><div class="m1"><p>گرچه ز گلهای می دامن ما شستنی است</p></div>
<div class="m2"><p>شستن دامن ز می غایت تر از دامنی است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به که نگردد رقیب دوست که این دیو ره</p></div>
<div class="m2"><p>دشمنی اش دوستی دوستی اش دشمنی است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سوختگان را مبین خوار که آن شاه حسن</p></div>
<div class="m2"><p>شب همه شب تا سحر همنفس گلخنی است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خیز و به میخانه کش رخت که در خانقه</p></div>
<div class="m2"><p>در سر پیران ره وسوسه رهزنی است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اهلی اگر عاشقی فاش مکن سر عشق</p></div>
<div class="m2"><p>قصه معراج دل نکته ناگفتنی است</p></div></div>