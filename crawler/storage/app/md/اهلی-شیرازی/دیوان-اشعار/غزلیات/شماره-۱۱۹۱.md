---
title: >-
    شمارهٔ ۱۱۹۱
---
# شمارهٔ ۱۱۹۱

<div class="b" id="bn1"><div class="m1"><p>من اگر شکسته عهدم تو وفای خود نگه کن</p></div>
<div class="m2"><p>به خطای من چه بینی به عطای خود نگه کن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به ره تو شهسوارا ز فرشته ره نباشد</p></div>
<div class="m2"><p>به سر خودت که گاهی ته پای خود نگه کن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به درون نامرادان منگر به تیره‌بختی</p></div>
<div class="m2"><p>تو که کعبه مرادی به صفای خود نگه کن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به وفا که در قیامت چو برآورم سر از گل</p></div>
<div class="m2"><p>چو گلم کفن به خون تر ز جفای خود نگه کن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تو در آینه نگنجی که جهان حسن و نازی</p></div>
<div class="m2"><p>به دو چشم عاشقان آ همه‌جای خود نگه کن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همه روز چند اهلی گله از جفای آن مه</p></div>
<div class="m2"><p>همه جور او چه بینی گله‌های خود نگه کن</p></div></div>