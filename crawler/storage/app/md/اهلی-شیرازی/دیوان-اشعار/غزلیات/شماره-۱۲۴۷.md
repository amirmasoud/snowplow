---
title: >-
    شمارهٔ ۱۲۴۷
---
# شمارهٔ ۱۲۴۷

<div class="b" id="bn1"><div class="m1"><p>ای مصور ز کف این تخته تعلیم بنه</p></div>
<div class="m2"><p>سجده کن پیش بت ما سر تسلیم بنه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر سر و دست به میزان محبت داری</p></div>
<div class="m2"><p>جان بیک نیم و جهان جمله بیک نیم بنه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ملک راحت طلبی رند و گدا باش چو من</p></div>
<div class="m2"><p>ورنه سلطان شو و دل بر خطر و بیم بنه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آخر ای چرخ منداینهمه غم بر دل من</p></div>
<div class="m2"><p>بار غم بر دل عشاق به تقسیم بنه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا مرا بنده خود خواند ز غم آزادم</p></div>
<div class="m2"><p>خسرو وقتم و گو خط بغلامیم بنه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همنشینان چو به خشت در او جان بدهم</p></div>
<div class="m2"><p>بر سر خاک من آن خشت بتعظیم بنه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بنده خاک در سیمتنی شو اهلی</p></div>
<div class="m2"><p>بت پرستی مکن و دل ز زر و سیم بنه</p></div></div>