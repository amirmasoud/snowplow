---
title: >-
    شمارهٔ ۳۳۱
---
# شمارهٔ ۳۳۱

<div class="b" id="bn1"><div class="m1"><p>آن شمع گلرخان که رخش لاله زار ماست</p></div>
<div class="m2"><p>طوفان آتشی است که در روزگار ماست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا خوشه چین خرمن صاحبدلان شدیم</p></div>
<div class="m2"><p>تخم محبت همه در کشت و کار ماست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زین آتش نهفته که در خاک میبریم</p></div>
<div class="m2"><p>تا حشر لاله یی که دمد داغدار ماست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در دام عشق ما ز سر شوق میرویم</p></div>
<div class="m2"><p>کانکس که صید ما کند اول شکار ماست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ما تشنه لب به مسجد و ساقی بمیکده</p></div>
<div class="m2"><p>بر کف شراب کوثر و در انتظار ماست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اهلی بمال چهره زردی که زر شوی</p></div>
<div class="m2"><p>زان کیمیای عشق که خاک مزار ماست</p></div></div>