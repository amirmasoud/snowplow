---
title: >-
    شمارهٔ ۱۳۵۳
---
# شمارهٔ ۱۳۵۳

<div class="b" id="bn1"><div class="m1"><p>گر به غم شادی ز عشقش شادمانی‌ها کنی</p></div>
<div class="m2"><p>ور به ناکامی بسازی کامرانی‌ها کنی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سر نهی بر آستان دوست هرگه کز وفا</p></div>
<div class="m2"><p>چون سگان عمری به کویش پاسبانی‌ها کنی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پیش آن شاه بتان گر لاف نادانی زنی</p></div>
<div class="m2"><p>به که اظهار کمال و نکته‌دانی‌ها کنی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>من نرنجم هرگز از بی‌مهریت ای آفتاب</p></div>
<div class="m2"><p>گرچه با دشمن بر عمم مهربانی‌ها کنی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>طوطی جان را کنی ز آیینه رخ صید خود</p></div>
<div class="m2"><p>خاصه کز شکرلبی شیرین‌زبانی‌ها کنی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>قصه کوته کن شهید عشق شو اهلی چو من</p></div>
<div class="m2"><p>تا کی از فرهاد و مجنون قصه‌خوانی‌ها کنی</p></div></div>