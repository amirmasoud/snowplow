---
title: >-
    شمارهٔ ۷۳۹
---
# شمارهٔ ۷۳۹

<div class="b" id="bn1"><div class="m1"><p>مپرس یوسف من کز تو گلرخان چونند</p></div>
<div class="m2"><p>چو گل بریده کف از رشک و غرقه در خونند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فتاده سلسله مو بپای مهرویان</p></div>
<div class="m2"><p>کنایتی است که بهر تو جمله مجنونند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از آن حساب نپرسند از شهیدانت</p></div>
<div class="m2"><p>که گر حساب کنند از حساب بیرونند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بهانه جام شراب است ورنه سرمستان</p></div>
<div class="m2"><p>خراب نرگس ساقی و لعل میگونند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>غمین مبین دل ناشاد عاشقان اهلی</p></div>
<div class="m2"><p>که محض دل خوشی اند آنزمان که محزونند</p></div></div>