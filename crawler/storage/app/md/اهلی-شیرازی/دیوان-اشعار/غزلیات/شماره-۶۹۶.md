---
title: >-
    شمارهٔ ۶۹۶
---
# شمارهٔ ۶۹۶

<div class="b" id="bn1"><div class="m1"><p>هرچند بیخم می‌کَنی، وصلت گرم خرم کند</p></div>
<div class="m2"><p>بازم نهال زندگی پا در زمین محکم کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شد با من مجنون صفت آهوی چشمت آشنا</p></div>
<div class="m2"><p>این آشنایی آخرم بیگانه از عالم کند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در کعبه کویش دلا از راه نومیدی درآ</p></div>
<div class="m2"><p>گر در حریم حرمتش محرومیت محرم کند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مخمور غم را دردسر از پند خلق افزون شود</p></div>
<div class="m2"><p>ساقی سر خم برگشا تا درد سرها کم کند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای غافل از عشق بتان کز خورد و خواب آسوده‌ای</p></div>
<div class="m2"><p>حیوان‌صفت می‌بینمت عشقت مگر آدم کند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زخم دل عشاق را خاک لحد مرهم بود</p></div>
<div class="m2"><p>خاکش به سر هر دل که او زخم ترا مرهم کند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ترک دل و جان ای صنم اهلی به عشقت کرده است</p></div>
<div class="m2"><p>او مانده است و ترک دین گر بت تویی آن هم کند</p></div></div>