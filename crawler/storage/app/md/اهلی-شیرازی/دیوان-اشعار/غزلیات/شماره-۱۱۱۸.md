---
title: >-
    شمارهٔ ۱۱۱۸
---
# شمارهٔ ۱۱۱۸

<div class="b" id="bn1"><div class="m1"><p>آنکه شب روز طرب کرد ز روی چو مهم</p></div>
<div class="m2"><p>گر بتابد رخ خود وای بروز سیهم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آفتابی که من از یک نگهش زنده شدم</p></div>
<div class="m2"><p>چکنم گر نکند از کرم خود نگهم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زان ز نخدان من مسکین چو بچاه افتادم</p></div>
<div class="m2"><p>دوست گر دست نگیرد که برآرد ز چهم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گرچه ره داد بخویشم مرو ایدل گستاخ</p></div>
<div class="m2"><p>که غیورست و ز غیرت بدهد باز رهم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>من چو زلف تو پریشان نه ز خویشم شب و روز</p></div>
<div class="m2"><p>که پریشان تو کنی روز و شب و سال و مهم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دم نیارم زدن از خرمن طاعت ترسم</p></div>
<div class="m2"><p>که کند خاک سیه آتش برق گنهم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون گدای در آن شاه بتانم اهلی</p></div>
<div class="m2"><p>نا امید از در بخشش نکند میل شهم</p></div></div>