---
title: >-
    شمارهٔ ۱۲
---
# شمارهٔ ۱۲

<div class="b" id="bn1"><div class="m1"><p>تا چند خشم و ناز و کین برخیز و باز از در درآ</p></div>
<div class="m2"><p>عاشق کشی از سر بنه عاشق نو از در درآ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بی صورت زیبای تو صحبت صفایی نیستش</p></div>
<div class="m2"><p>مجلس گلستان کم دگر چون سرو ناز از در درآ</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در مسجد ارباب ورع خوانند ما را بت پرست</p></div>
<div class="m2"><p>یکبار از بهر خدا وقت نماز از در درآ</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جز دیده من ای پری چشمی نبیند روی تو</p></div>
<div class="m2"><p>وحشت مکن از مردمان ای دلنواز از در درآ</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رحمی بکن بر حال من کافتاده ام دور از درت</p></div>
<div class="m2"><p>بیچاره و درمانده ام چاره ساز از در درآ</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا کی ز رشک دشمنان سوزی دل اهلی بغم</p></div>
<div class="m2"><p>یک ره به کام دوستان دشمن‌گداز از در درآ</p></div></div>