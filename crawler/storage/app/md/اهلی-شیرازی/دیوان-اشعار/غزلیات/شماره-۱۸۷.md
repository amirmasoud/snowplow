---
title: >-
    شمارهٔ ۱۸۷
---
# شمارهٔ ۱۸۷

<div class="b" id="bn1"><div class="m1"><p>مجنون عشق را هوس تخت و تاج نیست</p></div>
<div class="m2"><p>اورا که عقل نیست بهیچ احتیاج نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گیتی نما چه حاجت؟ اگر جم بصیرتی</p></div>
<div class="m2"><p>آیینه یی به ازمی و جام زجاج نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر کس که بود از هنر خود رواج یافت</p></div>
<div class="m2"><p>کار محبت است که هیچش رواج نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هان ای حکیم زحمت مجنون چه میدهی</p></div>
<div class="m2"><p>داغ ستاره سوختگی را علاج نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اهلی، کنون که صبر و دل و دین بباد رفت</p></div>
<div class="m2"><p>آسوده شو که بر ده ویران خراج نیست</p></div></div>