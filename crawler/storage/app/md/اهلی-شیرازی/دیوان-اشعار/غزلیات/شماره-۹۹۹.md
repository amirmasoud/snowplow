---
title: >-
    شمارهٔ ۹۹۹
---
# شمارهٔ ۹۹۹

<div class="b" id="bn1"><div class="m1"><p>خوش آنکه پیش تو دور از دیار خود گریم</p></div>
<div class="m2"><p>بهانه غربت و بر حال زار خود گریم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گهی که خاک شوم خیزم از مزار چو گرد</p></div>
<div class="m2"><p>بچشم خلق روم بر مزار خود گریم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو شمع گریه بپروانه کی کنم گر سوخت</p></div>
<div class="m2"><p>که گر توان بغم روزگار خود گریم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز آب دیده خورد کشته امیدم آب</p></div>
<div class="m2"><p>کجاست اشک که بر کشتکار خود گریم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بس است زخم درون چشم خون فشان اهلی</p></div>
<div class="m2"><p>دمی که بر دل و جان فکار خود گریم</p></div></div>