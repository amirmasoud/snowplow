---
title: >-
    شمارهٔ ۶۷۵
---
# شمارهٔ ۶۷۵

<div class="b" id="bn1"><div class="m1"><p>شوخی که می نخورده دل خلق خون کند</p></div>
<div class="m2"><p>وای آنزمان که چهره ز خون لاله گون کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گوید مجوی وصلم و نظاره هم مکن</p></div>
<div class="m2"><p>پس عاشقی که دل بکسی داد چون کند؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خواهد به خشم و ناز کند کم محبتم</p></div>
<div class="m2"><p>غافل که این کرشمه محبت فزون کند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آه از بتی که در دل سختش اثر نکرد</p></div>
<div class="m2"><p>آهی که رخنه در جگر بیستون کند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اهلی اگر نمیکند آن مه دوای دل</p></div>
<div class="m2"><p>گاهم به پرسشی غمی از دل برون کند</p></div></div>