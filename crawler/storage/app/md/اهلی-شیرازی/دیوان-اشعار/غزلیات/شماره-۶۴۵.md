---
title: >-
    شمارهٔ ۶۴۵
---
# شمارهٔ ۶۴۵

<div class="b" id="bn1"><div class="m1"><p>سایه کی بر خاک من آن سر و چالاک افکند</p></div>
<div class="m2"><p>شمع از آن نبود که هرگز سایه بر خاک افکند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من در عمری ز دلتنگی ندارم خنده یی</p></div>
<div class="m2"><p>عاقبت چون پسته این غم بر دل چاک افکند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا بکی گرد رهت باد از جبین من برد</p></div>
<div class="m2"><p>کی بود کاین طوطیا در چشم نمناک افکند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دوزخی گردد هر آن منزل که من منزل کنم</p></div>
<div class="m2"><p>بس که آهم آتشی در خاک و خاشاک افکند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با چنین آه جر سوزی که اهلی می‌کشد</p></div>
<div class="m2"><p>گر نیابد کام خود آتش با افلاک افکند</p></div></div>