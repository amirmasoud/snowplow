---
title: >-
    شمارهٔ ۱۲۸۰
---
# شمارهٔ ۱۲۸۰

<div class="b" id="bn1"><div class="m1"><p>عالمی را تو به شوخی دل و جان سوخته‌ای</p></div>
<div class="m2"><p>ای پسر این همه شوخی ز که آموخته‌ای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با دل زار مرا باز بسوزی بچه رنگ</p></div>
<div class="m2"><p>که چو گل چهره به صد رنگ بر افروخته‌ای</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چشم من چون نگرد بی‌تو جمال دگران</p></div>
<div class="m2"><p>که از آن سوزن مژگان نظرم دوخته‌ای</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همه آفاق خریدار تو زانند که تو</p></div>
<div class="m2"><p>یوسف خود به زر ناسره بفروخته‌ای</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عاشق سوخته پروانه شد ای بلبل مست</p></div>
<div class="m2"><p>تو چو سوسن به زبان عاشق دل‌سوخته‌ای</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اهلی از دانه اشکی چه غم از دل برود</p></div>
<div class="m2"><p>با چنین خرمن غم‌ها که تو اندوخته‌ای</p></div></div>