---
title: >-
    شمارهٔ ۳۹۷
---
# شمارهٔ ۳۹۷

<div class="b" id="bn1"><div class="m1"><p>آنکه فرشته را ازو دست امید کوته است</p></div>
<div class="m2"><p>با دل ما بود ولی از دل خود کی آگه است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از بر ما چه میروی بهر خدا که باز گرد</p></div>
<div class="m2"><p>جان بفدای مرکبت چشم امید بر ره است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دست بلند همتان عشق چو آفتاب کرد</p></div>
<div class="m2"><p>سایه ز پست همتی گمشده در ته چه است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر من پیر ای جوان طعنه ز عاشقی مزن</p></div>
<div class="m2"><p>باده چو سالخورده شد مستی و سوز آنگه است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جامه زهد دوختن بر تن پیر سینه چاک</p></div>
<div class="m2"><p>عقل هوس کند ولی رشته عمر کوته است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز آتش دوزخش چه غم اهلی از ابر لطف تو</p></div>
<div class="m2"><p>سایه رحمت تواش در همه حال همره است</p></div></div>