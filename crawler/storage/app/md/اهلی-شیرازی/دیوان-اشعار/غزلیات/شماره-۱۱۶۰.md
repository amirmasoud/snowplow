---
title: >-
    شمارهٔ ۱۱۶۰
---
# شمارهٔ ۱۱۶۰

<div class="b" id="bn1"><div class="m1"><p>خوشا سنگ جفایت خوردن و هم در نفس مردن</p></div>
<div class="m2"><p>ولی من بخت اینم نیست خواهم زین هوس مردن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من آنمرغ دل افکارم که محرومم ز وصل گل</p></div>
<div class="m2"><p>نخواهم روی بستان دید خواهم در قفس مردن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل مجنون کجا یابد نشان از محمل لیلی</p></div>
<div class="m2"><p>که میباید در این وادی بآوای جرس مردن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز طعن همنفس نتوان که نالم از بلای او</p></div>
<div class="m2"><p>بلا این شد که می باید ز طعن همنفس مردن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز عشق آسان شود مردن اگرنه زین بود مردن</p></div>
<div class="m2"><p>نخواهد کس درین عالم برای هیچکس مردن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چنینم گفت جانت را بیک دیدار بستانم</p></div>
<div class="m2"><p>اگر باشد چنین اهلی خوش آسانست پس مردن</p></div></div>