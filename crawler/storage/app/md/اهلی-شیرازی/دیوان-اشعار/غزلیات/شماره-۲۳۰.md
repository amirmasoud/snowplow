---
title: >-
    شمارهٔ ۲۳۰
---
# شمارهٔ ۲۳۰

<div class="b" id="bn1"><div class="m1"><p>حسن تو گرچه با همه کس در تجلی است</p></div>
<div class="m2"><p>با دیگران به صورت و باما بمعنی است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خاک ره از فروغ تو ای آفتاب حسن</p></div>
<div class="m2"><p>هر ذره را به چشمه خورشید دعوی است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عیسی دهد بمرده ز گفتار جان ولی</p></div>
<div class="m2"><p>مسکین کسی که کشته گفتار عیسی است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حال من و سگت زعنایت گذشته است</p></div>
<div class="m2"><p>احوال ما حکایت مجنون و لیلی است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اهلی ز اهل دولت اگر بر کناره ماند</p></div>
<div class="m2"><p>دولت همین بسش که نه از اهل دنیی است</p></div></div>