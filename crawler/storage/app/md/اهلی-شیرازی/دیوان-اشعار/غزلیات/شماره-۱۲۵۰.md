---
title: >-
    شمارهٔ ۱۲۵۰
---
# شمارهٔ ۱۲۵۰

<div class="b" id="bn1"><div class="m1"><p>گهی که زلف بر آن روی مهوش افتاده</p></div>
<div class="m2"><p>هزار سوخته را جان در آتش افتاده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بصد هزار جفا ای غزال مشکین بو</p></div>
<div class="m2"><p>خوشم که با من مسکین ترا خوش افتاده</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به گوشه نظری شهسوار من بنگر</p></div>
<div class="m2"><p>سری که بهر تو در پای ابرش افتاده</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نشان تیر تو خلق استخوان خود سازند</p></div>
<div class="m2"><p>ولیک قرعه بنام بلاکش افتاده</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جمال یار کند جلوه در دل پاکان</p></div>
<div class="m2"><p>خوشا دلی که چو آیینه بیغش افتاده</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مرا چه چاره ز دیوانگی بود اهلی</p></div>
<div class="m2"><p>که کار من بحریفی پریوش افتاده</p></div></div>