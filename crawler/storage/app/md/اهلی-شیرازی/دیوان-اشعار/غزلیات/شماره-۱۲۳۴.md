---
title: >-
    شمارهٔ ۱۲۳۴
---
# شمارهٔ ۱۲۳۴

<div class="b" id="bn1"><div class="m1"><p>ای مرا داغی به دل از لعل آتش رنگ تو</p></div>
<div class="m2"><p>بسته راه زندگی بر من دهان تنگ تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نیمه جانی کز کف خوبان برون آورده ام</p></div>
<div class="m2"><p>چون کنم دیگر که بیرون آورم از چنگ تو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در عتابی با من و آهسته می خندد لبت</p></div>
<div class="m2"><p>وه چه دلجویی بمیرم پیش صلح و جنگ تو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سوختم از شوق تو بر من دلت رحمی نکرد</p></div>
<div class="m2"><p>آه از این بیرحمی و وای از دل چون سنگ تو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر قد خوبان نظر هر چند اهلی میکند</p></div>
<div class="m2"><p>کس ندارد اعتدال قد شوخ و شنگ تو</p></div></div>