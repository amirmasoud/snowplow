---
title: >-
    شمارهٔ ۱۳۹
---
# شمارهٔ ۱۳۹

<div class="b" id="bn1"><div class="m1"><p>تا نمردیم دل از درد تو بهبود نداشت</p></div>
<div class="m2"><p>تا ندادیم به سودای تو جان سود نداشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حقه سبز فلک داشت دوای همه کس</p></div>
<div class="m2"><p>آنچه درمان دل خسته ما بود نداشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هم ز خاکستر ما گشت چراغی روشن</p></div>
<div class="m2"><p>کاتش مرده دلان فایده جز دود نداشت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بود مقصود تو دل سوختن عاشق زار</p></div>
<div class="m2"><p>غیر مقصود تو هم دلشده مقصود نداشت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سصر محمود از آن بود کف پای ایاز</p></div>
<div class="m2"><p>که ایاز از دو جهان جز سر محمود نداشت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>برق حسن تو عجب خرمن ما زود بسوخت</p></div>
<div class="m2"><p>این گمان گرچه دلم داشت چنین زود نداشت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>طمع بوس و کنار تو که اهلی بودش</p></div>
<div class="m2"><p>پیش ازین بود که دامان می آلود نداشت</p></div></div>