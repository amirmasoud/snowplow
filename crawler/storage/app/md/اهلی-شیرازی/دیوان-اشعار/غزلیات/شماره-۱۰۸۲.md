---
title: >-
    شمارهٔ ۱۰۸۲
---
# شمارهٔ ۱۰۸۲

<div class="b" id="bn1"><div class="m1"><p>تشنه درد توام وز پی درمان نروم</p></div>
<div class="m2"><p>گر بمیرم بسر چشمه حیوان نروم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چه بهار و چه خزان بیخبرم از گل و خار</p></div>
<div class="m2"><p>زانکه بی روی تو هرگز بگلستان نروم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بسکه از غیر تو ای گل چو صبا خار خورم</p></div>
<div class="m2"><p>هیچگه سوی تو نایم که پریشان نروم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر در آتش طلبد عشق توام شمع صفت</p></div>
<div class="m2"><p>کشتنی باشم اگر خرم و خندان نروم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بلبل مستم اگر بر سر خارم چه عجب</p></div>
<div class="m2"><p>خار در چشمم اگر بر سر پیکان نروم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بامید لب شیرین تو فرهاد صفت</p></div>
<div class="m2"><p>میکنم جانی و باشد که بحرمان نروم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>غرقه در خون دلم پیش سگانش اهلی</p></div>
<div class="m2"><p>باشد آلوده ازین منزل پاکان نروم</p></div></div>