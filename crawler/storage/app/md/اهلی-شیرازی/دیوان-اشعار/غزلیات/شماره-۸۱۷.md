---
title: >-
    شمارهٔ ۸۱۷
---
# شمارهٔ ۸۱۷

<div class="b" id="bn1"><div class="m1"><p>ساقیا مستم لب خود از لب من دور دار</p></div>
<div class="m2"><p>ورنه گر گستاخیی آید ز من معذور دار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دردمند عشق را راحت گر از بیداد تست</p></div>
<div class="m2"><p>یارب این راحت ز جان دردمندان دور دار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر کرا در ظلمت غم چشم بیرون رفتن است</p></div>
<div class="m2"><p>گو چراغ می براه دیده پرنور دار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای حریف بزم وصل از غیرت جانان بترس</p></div>
<div class="m2"><p>یا طمع از جان ببر یا راز ازو مستور دار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خانه تن عاقبت اهلی ز پا خواهد فتاد</p></div>
<div class="m2"><p>تا توانی خانه تن را بمی معمور دار</p></div></div>