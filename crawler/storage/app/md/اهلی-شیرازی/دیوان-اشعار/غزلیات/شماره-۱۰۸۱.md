---
title: >-
    شمارهٔ ۱۰۸۱
---
# شمارهٔ ۱۰۸۱

<div class="b" id="bn1"><div class="m1"><p>دمی که همنفسان گرم گفتگو بینم</p></div>
<div class="m2"><p>من از کناره به دزدیده روی او بینم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>لطیفه هاست....</p></div>
<div class="m2"><p>گشایم از رخ تو زلف و مو بمو بینم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چنین کز آینه ام زرد رو من بیمار</p></div>
<div class="m2"><p>مگر که خویشتن از باده سرخ رو بینم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رقیب حمل جفا میکند ولی لطفست</p></div>
<div class="m2"><p>هر آن ستم که از آن شوخ تند خو بینم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به آرزوی دل خود عجب رسد اهلی</p></div>
<div class="m2"><p>که در دلش ز تو هردم صد آرزو بینم</p></div></div>