---
title: >-
    شمارهٔ ۴۳۰
---
# شمارهٔ ۴۳۰

<div class="b" id="bn1"><div class="m1"><p>لعلت بخنده هرچه دلم از تو خواست کرد</p></div>
<div class="m2"><p>صد وعده دروغ بیک خنده راست کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مقصود ما هلک شدن بود غایتش</p></div>
<div class="m2"><p>عشق تو آنچه غایت مقصود ماست کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>برقی ز آفتاب رخت درچمن فتاد</p></div>
<div class="m2"><p>بازار گل چو خرمن مه روبکاست کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دیدی که چشم مست تو چون خواست قتل ما</p></div>
<div class="m2"><p>نگذاشت جای آشتی و هرچه خواست کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>باور مکن که چرخ جفا کار کج نهاد</p></div>
<div class="m2"><p>کاری چنانکه خاطر ما خواست راست کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اهلی برید دل ز طمع ای شکر لبان</p></div>
<div class="m2"><p>از جان گذشت و آنچه مراد شماست کرد</p></div></div>