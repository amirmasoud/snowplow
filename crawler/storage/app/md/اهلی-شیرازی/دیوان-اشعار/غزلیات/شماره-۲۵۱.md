---
title: >-
    شمارهٔ ۲۵۱
---
# شمارهٔ ۲۵۱

<div class="b" id="bn1"><div class="m1"><p>جان دادن از وفا هنر کوهکن بس است</p></div>
<div class="m2"><p>حاجت بقصه نیست همین یکسخن بس است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ساقی رواج مدرسه و خانقه شکست</p></div>
<div class="m2"><p>درصد هزار بتکده یک بت شکن بس است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا زنده ام پلاس سگت پیرهن کنم</p></div>
<div class="m2"><p>چو میرم از غم تو همینم کفن بس است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بوی گل وصال کجا میرسد بمن</p></div>
<div class="m2"><p>بگشا قبا که نکهت ازین پیرهن بس است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>من کشته توام چکنم معجز مسیح</p></div>
<div class="m2"><p>حرفی بگو که یک سخنم زان دهن بس است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ما را بس این که در سر سرو تو رفت دل</p></div>
<div class="m2"><p>معراج بلبلان سر سرو چمن بس است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اهلی اگر حکایت مجنون ز یاد رفت</p></div>
<div class="m2"><p>حرفی ز داستان تو در انجمن بس است</p></div></div>