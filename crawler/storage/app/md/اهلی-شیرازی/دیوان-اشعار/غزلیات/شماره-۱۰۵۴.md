---
title: >-
    شمارهٔ ۱۰۵۴
---
# شمارهٔ ۱۰۵۴

<div class="b" id="bn1"><div class="m1"><p>من کز غمت ز دیده می لاله گون خورم</p></div>
<div class="m2"><p>گر بیتو لب بلب جام خون خورم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تلخ است زهر مرگ و ز هجر تو تلخ تر</p></div>
<div class="m2"><p>بی شربت وصال تو این زهر چون خورم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عشقم چنان گداخت که هر کسل نگاه کرد</p></div>
<div class="m2"><p>دید از برون چو شیشه که خون از درون خورم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون بیم سر نماند ز دیوانگی چه باک</p></div>
<div class="m2"><p>کارم از آن گذشت که خوف از جنون خورم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اهلی جفای چرخ ز خونخواریم بکشت</p></div>
<div class="m2"><p>تا چند خون ازین قدح واژگون خورم</p></div></div>