---
title: >-
    شمارهٔ ۳۳۶
---
# شمارهٔ ۳۳۶

<div class="b" id="bn1"><div class="m1"><p>همه راست میل خالی که بر آن رخ جمیلست</p></div>
<div class="m2"><p>چه رسد بما ازین خوان عدسی و صد خلیل است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به بهشت و سلسبیلم چه دهی تو وعده، مارا</p></div>
<div class="m2"><p>تو بهشت حسنی و می زکف تو سلسبیل است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پی دفع چشم زخمت نبود به نیل حاجت</p></div>
<div class="m2"><p>که بمصر حسن رویت خط سبز به زنیل است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سرم ار ربود تیغت بقیامتت نگیرم</p></div>
<div class="m2"><p>که بشرع عشقبازی سر و خون ما سبیل است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نشنیده یی که مجنون بقبیله تیغ چون زد</p></div>
<div class="m2"><p>بمحبتت که اهلی بوفا از آن قبیل است</p></div></div>