---
title: >-
    شمارهٔ ۱۲۰۴
---
# شمارهٔ ۱۲۰۴

<div class="b" id="bn1"><div class="m1"><p>هرچند که دیدم همه جور و ستم از تو</p></div>
<div class="m2"><p>بازآ که گناه از من و لطف و کرم از تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پیش آ قدمی تا بسپارم بتو جان را</p></div>
<div class="m2"><p>جان باختن از جانب من یک قدم از تو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خورشید جهانی تو و ما سوخته حالان</p></div>
<div class="m2"><p>آن ذره، که داریم وجود از عدم تو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هرگز بجز از شکر عطای تو نگفتیم</p></div>
<div class="m2"><p>با آنکه هزاران گله داریم هم از تو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر تشنه لبی را بدهی جرعه آبی</p></div>
<div class="m2"><p>دریای حیاتی، نشود هیچ کم از تو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اهلی غم خود خور که بتانرا غم کس نیست</p></div>
<div class="m2"><p>گر کشته شوی نیز کسی را چه غم از تو</p></div></div>