---
title: >-
    شمارهٔ ۹۶۵
---
# شمارهٔ ۹۶۵

<div class="b" id="bn1"><div class="m1"><p>من خسته ایفلک کی دمی از تو شاد گشتم</p></div>
<div class="m2"><p>چه مراد جستم از تو که نامراد گشتم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من زار را بمجنون مکن ایحکیم نسبت</p></div>
<div class="m2"><p>که بدرود و داغ حسرت من از او زیاد گشتم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بغبار خاک راهش نرسیدم ارچه عمری</p></div>
<div class="m2"><p>همه سو چو آب رفتم همه جا چو باد گشتم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دل من ز ششدر غم نشدش گشاد هرگز</p></div>
<div class="m2"><p>که چو کعبتین هر سو ز پی مراد گشتم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز در بتان چو اهلی که مرا بسنگ راند؟</p></div>
<div class="m2"><p>که بکوی نوغزالان سگ خانه زاد گشتم</p></div></div>