---
title: >-
    شمارهٔ ۱۳۳۴
---
# شمارهٔ ۱۳۳۴

<div class="b" id="bn1"><div class="m1"><p>دگرم ز چشم گریان بهزار دلستانی</p></div>
<div class="m2"><p>چو فرشته میخرامی که چو ماه آسمانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز قبای نیلگونت بگمان فتاده خلقی</p></div>
<div class="m2"><p>که مگر به نیل چشمم گذری کنی نهانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همه روز مهر خوبان ز حسود می نهفتم</p></div>
<div class="m2"><p>تو ولیکن آفتابی ز کسی نهان نمانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نه تو بودی ای پریرخ که زدی بتیغ خلقی</p></div>
<div class="m2"><p>ز هلاک من چه ترسی بکشم چنانکه دانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز فسون چشم مستت که فسانه گشت اهلی</p></div>
<div class="m2"><p>بجهان فتاده فتنه تو چه فتنه جهانی</p></div></div>