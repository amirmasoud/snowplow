---
title: >-
    شمارهٔ ۶۸۱
---
# شمارهٔ ۶۸۱

<div class="b" id="bn1"><div class="m1"><p>دفع غم دور از تو میل باده‌ام چون می‌شود</p></div>
<div class="m2"><p>در دهان چون زهر تلخ و در جگر خون می‌شود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مست و خندان بر سر عاشق مران بهر خدا</p></div>
<div class="m2"><p>کش عنان اختیار از دست بیرون می‌شود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همچو شمعم آتشی کز مهر رویت در گرفت</p></div>
<div class="m2"><p>گریه آن را کی نشاند بلکه افزون می‌شود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آن همه پیکان که لیلی بر دل اغیار زد</p></div>
<div class="m2"><p>یک به یک پیدا کنون از خاک مجنون می‌شود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کوکب بخت کرا تا خواهد امشب سوختن</p></div>
<div class="m2"><p>آه اهلی کز دل سوزان به گردون می‌شود</p></div></div>