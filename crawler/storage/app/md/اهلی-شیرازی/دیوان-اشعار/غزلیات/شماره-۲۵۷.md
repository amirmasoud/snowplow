---
title: >-
    شمارهٔ ۲۵۷
---
# شمارهٔ ۲۵۷

<div class="b" id="bn1"><div class="m1"><p>ناخورده می ز نرگس او دل خمار یافت</p></div>
<div class="m2"><p>تا چیده یک گل از مژه صد زخم خار یافت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>میکرد شمع از آتش دل بی قرار سعی</p></div>
<div class="m2"><p>بعد از هزار سعی بکشتن قرار یافت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آنرا چو من رواست که گشت چمن کند</p></div>
<div class="m2"><p>کز خار و گل مشام دلش بوی یار یافت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>من ذره حقیرم و آن آفتاب حسن</p></div>
<div class="m2"><p>هرجا نظر فکند چو من صدهزار یافت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چندان ز مهر او بفلک رفت دود دل</p></div>
<div class="m2"><p>کایینه جمال بآخر غبار یافت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>با انس آن غزال کنار از جهان گرفت</p></div>
<div class="m2"><p>مجنون که آرزوی دل اندر کنار یافت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سرچشمه حیات اگر شد نصیب خضر</p></div>
<div class="m2"><p>اهلی نمی هم از مژه اشگبار یافت</p></div></div>