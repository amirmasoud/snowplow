---
title: >-
    شمارهٔ ۳۴۶
---
# شمارهٔ ۳۴۶

<div class="b" id="bn1"><div class="m1"><p>تن عاشق همای لامکان است</p></div>
<div class="m2"><p>تو پنداری ک مشتی استخوان است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از آن هر موی ما ماری است بر تن</p></div>
<div class="m2"><p>که با هر موی ما گنجی نهان است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بهار عمر دریاب از صبوحی</p></div>
<div class="m2"><p>که تا خورشید سربرزد خزان است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در آتش گر بود پروانه با شمع</p></div>
<div class="m2"><p>بر او آتش بهشت جاودان است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بصورت از ملک بگذشت اهلی</p></div>
<div class="m2"><p>بمعنی خود سگ این آستان است</p></div></div>