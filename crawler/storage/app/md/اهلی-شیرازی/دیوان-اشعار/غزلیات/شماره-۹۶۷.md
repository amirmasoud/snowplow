---
title: >-
    شمارهٔ ۹۶۷
---
# شمارهٔ ۹۶۷

<div class="b" id="bn1"><div class="m1"><p>چون سایه گذر زان مه رخساره نداریم</p></div>
<div class="m2"><p>و ز سوختگی طاقت نظاره نداریم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون ما دل صد پاره ز مهر تو که دارد؟</p></div>
<div class="m2"><p>اینست که طالع ز تو یکباره نداریم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عیش دو جهان سهل بود بر دل ما لیک</p></div>
<div class="m2"><p>با حسرت شیرین دهنان چاره نداریم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حال دل گمگشته ما شهره شهرست</p></div>
<div class="m2"><p>ما خود خبری از دل آواره نداریم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هرچند که سرگشته ز هجریم چو اهلی</p></div>
<div class="m2"><p>خوشباش که رشک مه و سیاره نداریم</p></div></div>