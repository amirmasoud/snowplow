---
title: >-
    شمارهٔ ۱۱۰۹
---
# شمارهٔ ۱۱۰۹

<div class="b" id="bn1"><div class="m1"><p>چند این دل سودازده را پند بگویم</p></div>
<div class="m2"><p>دیوانه شدم بیهده تا چند بگویم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سوگند دهندم که کنم ترک تو ای بت</p></div>
<div class="m2"><p>کفرست که ترک تو بسوگند بگویم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>درد دل دیوانه بدیوانه توان گفت</p></div>
<div class="m2"><p>سودی ندهد گر بخردمند بگویم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هرگز نرود از دل من تلخی حسرت</p></div>
<div class="m2"><p>هر چند کزان لعل شکر خند بگویم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شیرین نشود جز بگزیدن لبم از قند</p></div>
<div class="m2"><p>تا کی بزبان من سخن قند بگویم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هرگز که غم بنده خورد؟ به ز خداوند</p></div>
<div class="m2"><p>آن به که غم خود بخداوند بگویم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اهلی لب او کی دلم از بند گشاید</p></div>
<div class="m2"><p>هرچند که صد نکته دلبند بگویم</p></div></div>