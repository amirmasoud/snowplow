---
title: >-
    شمارهٔ ۱۵۲
---
# شمارهٔ ۱۵۲

<div class="b" id="bn1"><div class="m1"><p>مصر شرف از حسن ادب یوسف جان یافت</p></div>
<div class="m2"><p>دولت بعبث جان برادر نتوان یافت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>داریم گمانی که ترا هست دهان لیک</p></div>
<div class="m2"><p>کس گنج یقین را نتواند بگمان یافت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آنی که تو داری بصفت راست نیاید</p></div>
<div class="m2"><p>آن مست می تست که کیفیت آن یافت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آن زخم جفا خورد شکاریم که از ما</p></div>
<div class="m2"><p>بی خون جگر کس نتوانست نشان یافت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دشنام ترا قدر دعا گوی تو داند</p></div>
<div class="m2"><p>کز تلخی دشنام تو شیرینی جان یافت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یکدم بوصال تو امانم ندهد هجر</p></div>
<div class="m2"><p>هرگز نتواند کسی از مرگ امان یافت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آیینه اسکندر و جام جم اگر هست</p></div>
<div class="m2"><p>اینست که اهلی ز کف پیر مغان یافت</p></div></div>