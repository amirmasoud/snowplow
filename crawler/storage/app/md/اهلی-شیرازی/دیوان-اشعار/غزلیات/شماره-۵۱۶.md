---
title: >-
    شمارهٔ ۵۱۶
---
# شمارهٔ ۵۱۶

<div class="b" id="bn1"><div class="m1"><p>گرم چو شمع برد یار سر چه خواهد شد</p></div>
<div class="m2"><p>بغیر از آن که بمیرم دگر چه خواهد شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیک نگاه تو از قید جان توان رستن</p></div>
<div class="m2"><p>اگر نگاه کنی اینقدرچه خواهد شد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مبند در بمن ای گلشن جمال که من</p></div>
<div class="m2"><p>گدای یک نظرم یک نظر چه خواهد شد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>من از تو روی نتابم ز بیم رسوایی</p></div>
<div class="m2"><p>چنانکه من شده ام زین بتان بتر چه خواهد شد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مدار دست ز زنار زلف او اهلی</p></div>
<div class="m2"><p>اگرچه دین ببرد گو ببر چه خواهد شد</p></div></div>