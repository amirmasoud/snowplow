---
title: >-
    شمارهٔ ۱۰۳۸
---
# شمارهٔ ۱۰۳۸

<div class="b" id="bn1"><div class="m1"><p>دل از آرزو چه خون شد بدعا چه کام خواهم</p></div>
<div class="m2"><p>همه آرزو و کامم ز خدا کدام خواهم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سر وصلت ای پریوش نبود مرا ولیکن</p></div>
<div class="m2"><p>سر خویش زیر پایت بگه خرام خواهم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هوسم بود که طوبی نگرم ولی نه چندان</p></div>
<div class="m2"><p>که خرام سرو قدت بکنار بام خواهم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگرم بخلد ساقی تو بهشت رو نباشی</p></div>
<div class="m2"><p>نه جمال حور و غلمان نه می مدام خواهم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همه صاف عیش جویان من خاکسار از تو</p></div>
<div class="m2"><p>همه درد و داغ جویم همه درد جام خواهم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نبری ز ننگ نامم من خسته پس وصالت</p></div>
<div class="m2"><p>بچه آبرو بجویم بچه ننگ و نام خواهم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به در بهشت رضوان چو فرشته ام چه خواند</p></div>
<div class="m2"><p>که بکوی یار اهلی چو سگان مقام خواهم</p></div></div>