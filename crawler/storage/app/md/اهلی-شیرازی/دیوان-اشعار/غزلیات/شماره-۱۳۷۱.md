---
title: >-
    شمارهٔ ۱۳۷۱
---
# شمارهٔ ۱۳۷۱

<div class="b" id="bn1"><div class="m1"><p>دولت اگر مدد کند گلبن باغ من شوی</p></div>
<div class="m2"><p>پای بچشم من نهی چشم و چراغ من شوی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پنبه داغ سینه ام مرهم دل نشد مگر</p></div>
<div class="m2"><p>با تن همچو برگ گل مرهم داغ من شوی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بوی نسیم گل کجا تازه کند دماغ ما</p></div>
<div class="m2"><p>هم مگر از شمیم خود عطر دماغ من شوی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بهر فراغ دل ترا بینم و این ملال تست</p></div>
<div class="m2"><p>وه تو ملول تا بکی بهر فراغ من شوی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفت به خنده آن گلم اهلی اگرچه عاشقی</p></div>
<div class="m2"><p>گر بپری از این چمن بلبل باغ من شوی</p></div></div>