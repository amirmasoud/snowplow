---
title: >-
    شمارهٔ ۸۲۰
---
# شمارهٔ ۸۲۰

<div class="b" id="bn1"><div class="m1"><p>پایه معراج جان خواهی ز دنیا درگذر</p></div>
<div class="m2"><p>پای در هفتم فلک نه و زمسیحا درگذر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دوش در مجلس چه خوش میگفت با پروانه شمع</p></div>
<div class="m2"><p>گر سر مردن نداری از سر ما درگذر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا نبرد غیرت یوسف ترا انگشت عیب</p></div>
<div class="m2"><p>بازگیر انگشت و از عیب زلیخا درگذر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مردن اندر هجر خوش باشد نه در روز وصال</p></div>
<div class="m2"><p>ای اجل کار است مارا با تو از ما درگذر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای شب هجران مگر روز قیامت بگذری</p></div>
<div class="m2"><p>سوختم از ظلمت آخر یا بکش یا درگذر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کی شود عاقل دل مجنون بکوشش ای حکیم</p></div>
<div class="m2"><p>گر تو باری عاقلی از فکر سودا درگذر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>لاف عشق، آنگه غم دنیا و دین نامردی است</p></div>
<div class="m2"><p>مرد شو اهلی و از دنیا و عقبی درگذر</p></div></div>