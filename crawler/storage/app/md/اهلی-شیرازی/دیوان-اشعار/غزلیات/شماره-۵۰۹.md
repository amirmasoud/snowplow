---
title: >-
    شمارهٔ ۵۰۹
---
# شمارهٔ ۵۰۹

<div class="b" id="bn1"><div class="m1"><p>آن تقوی ام که از همه کس احتراز بود</p></div>
<div class="m2"><p>تقوی نبود مایه صد کبر و ناز بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ساقی که می بخرقه این توبه کار ریخت</p></div>
<div class="m2"><p>خوش کرد اگرنه قصه تقوی دراز بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>روشن بود چراغ تو ای پیر زانکه من</p></div>
<div class="m2"><p>هر وقت کآمدم در میخانه باز بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از جنگ و بحث مدرسه رفتم بمیکده</p></div>
<div class="m2"><p>دیدم میان درد کشان صد نیاز بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اهلی شکست کار تو میخواست مدعی</p></div>
<div class="m2"><p>بیچاره غافل از کرم کارساز بود</p></div></div>