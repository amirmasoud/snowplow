---
title: >-
    شمارهٔ ۱۳۲۹
---
# شمارهٔ ۱۳۲۹

<div class="b" id="bn1"><div class="m1"><p>کی دل سبک باشک جگرگون کند کسی</p></div>
<div class="m2"><p>دریا بقطره قطره تهی چون کند کسی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو نوبهار حسن نه آنی که دل دهد</p></div>
<div class="m2"><p>کز سینه خار خار تو بیرون کند کسی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا کی سخن ز چشمه حیوان کند خضر</p></div>
<div class="m2"><p>باوی حدیث آن لب میگون کند کسی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عاشق بوصل یار کجا میرسد مگر</p></div>
<div class="m2"><p>آیین روزگار دگرگون کند کسی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در دیده خواب مردم چشمم ز گریه نیست</p></div>
<div class="m2"><p>چون خواب در میانه جیحون کند کسی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای همنفس تفحص حالم چه میکنی</p></div>
<div class="m2"><p>خودرا چرا به بیهده محزون کند کسی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>انصاف نیست اهلی اگر با چنین غمی</p></div>
<div class="m2"><p>در دور من حکایت مجنون کند کسی</p></div></div>