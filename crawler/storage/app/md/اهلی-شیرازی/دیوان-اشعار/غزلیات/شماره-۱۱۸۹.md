---
title: >-
    شمارهٔ ۱۱۸۹
---
# شمارهٔ ۱۱۸۹

<div class="b" id="bn1"><div class="m1"><p>سوختم چون لاله چشمی بر دل چاکم فکن</p></div>
<div class="m2"><p>یا برآر از خاک یا یکباره در خاکم فکن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون بخاکم گر زنده خواهی دیگرم</p></div>
<div class="m2"><p>قطره یی بر خاک از آنروزی عرقناکم فکن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پیش از آنساعت که گردد کار گر زهر اجل</p></div>
<div class="m2"><p>جرعه یی در کام از آن لعل چو تریاکم فکن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چشم من لایق کجا باشد بخورشید رخت</p></div>
<div class="m2"><p>پرتو اندیشه یی در چشم ادراکم فکن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اهلی آن بدمهر اگر پرسد ز احوال درون</p></div>
<div class="m2"><p>گو نظر بر حال دل از سینه چاکم فکن</p></div></div>