---
title: >-
    شمارهٔ ۹۲۱
---
# شمارهٔ ۹۲۱

<div class="b" id="bn1"><div class="m1"><p>چند فتد به کشتنم زلف تو ماه برطرف</p></div>
<div class="m2"><p>روی تو میکشد مرا زلف سیاه برطرف</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چشم خوش تو برد دل تهمت او بخط فکند</p></div>
<div class="m2"><p>چون دل برده خواستم کرد نگاه برطرف</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا نرسد غبار دل بر تو ز رهگذار من</p></div>
<div class="m2"><p>سیل سرشک گرد من کرد ز راه برطرف</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پیش قد تو کی بود یاد بهشت و طوبیم</p></div>
<div class="m2"><p>نخل گل است در میان شاخ گیاه برطرف</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اهلی اگر ترا کشد یار فرشته خوی تو</p></div>
<div class="m2"><p>لاف وفا بهانه بس جرم و گناه برطرف</p></div></div>