---
title: >-
    شمارهٔ ۱۰۷۳
---
# شمارهٔ ۱۰۷۳

<div class="b" id="bn1"><div class="m1"><p>گرچه بی بختم و دور از رخ گلفام توام</p></div>
<div class="m2"><p>شکر این بخت چگویم که در ایام توام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مرغ هرگز نپرد سوی من و من همه عمر</p></div>
<div class="m2"><p>چشم در راه خبر گوش به پیغام توام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ساقیا تشنه لم سوز مرا عیب مکن</p></div>
<div class="m2"><p>چکنم سوخته جرعه یی از جان توام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تو ملک خویی و با من سخن از لطف کنی</p></div>
<div class="m2"><p>من خراب از ستم و کشته دشنام توام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نظری کن که گدای نظرم چون اهلی</p></div>
<div class="m2"><p>نه چو کوته نظران در پی انعام توام</p></div></div>