---
title: >-
    شمارهٔ ۵۷۲
---
# شمارهٔ ۵۷۲

<div class="b" id="bn1"><div class="m1"><p>جان هلاک از شوق و یار از دیه ما می‌رود</p></div>
<div class="m2"><p>تشنه مردم چشمه حیوان به صحرا می‌رود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گرد و دین در فراقش از ملاحت سوخت سوخت</p></div>
<div class="m2"><p>یار من یا رب سلامت باد هرجا می‌رود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در دو عالم هر کجا صیدی بود نخجیر اوست</p></div>
<div class="m2"><p>او به قصد دیدن غمدیده عمدا می‌رود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ناصحم گوید که بنشین در پِیَش صحرا مگیر</p></div>
<div class="m2"><p>چون کنم؟ گرمی نشینم جان شیدا می‌رود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>می‌رود آن شوخ و فریاد اسیران در پیش</p></div>
<div class="m2"><p>شاه خوبان است و با صد شور و غوغا می‌رود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بازش آرای هم‌نشین و فتنه بنشان کان سوار</p></div>
<div class="m2"><p>پای اگر در زین درآرد فتنه بالا می‌رود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در بر بازار حسن او که صد یوسف کم است</p></div>
<div class="m2"><p>اهلی از جوش خریداران به سودا می‌رود</p></div></div>