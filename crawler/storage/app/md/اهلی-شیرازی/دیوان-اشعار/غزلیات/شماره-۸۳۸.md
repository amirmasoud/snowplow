---
title: >-
    شمارهٔ ۸۳۸
---
# شمارهٔ ۸۳۸

<div class="b" id="bn1"><div class="m1"><p>در دل آمد یار و گشت از دیده غمدیده دور</p></div>
<div class="m2"><p>بیش از آن نزدیک شد در دل که گشت از دیده دور</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من به بویی زنده ام جانی ندارم دور ازو</p></div>
<div class="m2"><p>مدتی دیرست کز من جان من گردیده دور</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر کشد آن مه مرا دستش مگیر ای همنفس</p></div>
<div class="m2"><p>مردنم بهتر که یار از من شود رنجیده دور</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مستم و شوریده و سرگرم آغوش توام</p></div>
<div class="m2"><p>ای پری گر عاقلی باش از من شوریده دور</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای نصیحتگو میا نزدیک کز سودای عشق</p></div>
<div class="m2"><p>اهلی شوریده خود را از دو عالم دیده دور</p></div></div>