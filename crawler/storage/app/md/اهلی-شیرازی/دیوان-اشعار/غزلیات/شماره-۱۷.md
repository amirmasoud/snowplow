---
title: >-
    شمارهٔ ۱۷
---
# شمارهٔ ۱۷

<div class="b" id="bn1"><div class="m1"><p>خط چو مهر گیا تا نموده‌ای ما را</p></div>
<div class="m2"><p>هزار مهر و محبت فزوده‌ای ما را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جفا نموده‌ات اکنون طریق رندی نیست</p></div>
<div class="m2"><p>چنان نما که در اول نموده‌ای ما را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگر حدیث پری بر زبان ما بگذشت</p></div>
<div class="m2"><p>به جان دوست که در دل تو بوده‌ای ما را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به خنده‌ای که از آن پسته‌دهن کردی</p></div>
<div class="m2"><p>دری ز عالم معنی گشوده‌ای ما را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چه حاجت است که لافیم از وفا چون تو</p></div>
<div class="m2"><p>به صدهزار جفا آزموده‌ای ما را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سگ توییم که از لطف و پاکدامانی</p></div>
<div class="m2"><p>غبار از آینه دل زدوده‌ای ما را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به عشوه گفت که اهلی گر از تو دل بردیم</p></div>
<div class="m2"><p>تو هم به لطف سخن دل ربوده‌ای ما را</p></div></div>