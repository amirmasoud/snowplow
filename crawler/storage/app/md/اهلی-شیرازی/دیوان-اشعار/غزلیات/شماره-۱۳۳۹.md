---
title: >-
    شمارهٔ ۱۳۳۹
---
# شمارهٔ ۱۳۳۹

<div class="b" id="bn1"><div class="m1"><p>ای عاشق بی دیده که در عیب منستی</p></div>
<div class="m2"><p>گر ساقی ما را تو ببینی بپرستی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فردا ز تو چون لاله اثر نیست درین باغ</p></div>
<div class="m2"><p>امروز قدح گیر به شکرانه که هستی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون مرغ چمن چند کنی ناله ز حسرت؟</p></div>
<div class="m2"><p>بشکن قفس ای مرغ گرفتار که رستی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا میل تو یوسف نکند سود ندارد</p></div>
<div class="m2"><p>هر چند که جان بر لب و دل بر کف دستی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پیش کرم عشق چو خورشید و چه ذره</p></div>
<div class="m2"><p>محروم از آنی تو که با همت پستی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شادم ز تو ای ساقی بدمست که هرگز</p></div>
<div class="m2"><p>جز کاسه عیش من و مجنون نشکستی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زنهار گرانی مکن ای شوخ پریزاد</p></div>
<div class="m2"><p>یک لحظه که با مردم دیوانه نشستی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دیوانه نیم من که شدم بسته آن زلف</p></div>
<div class="m2"><p>دیوانه تویی خواجه کزین سلسله جستی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اهلی ز سگان تو خجل گشت که هرگز</p></div>
<div class="m2"><p>لاغرتر از این صید به فتراک نبستی</p></div></div>