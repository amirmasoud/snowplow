---
title: >-
    شمارهٔ ۸۲۷
---
# شمارهٔ ۸۲۷

<div class="b" id="bn1"><div class="m1"><p>هزار شکر که عالم بکام ماست دگر</p></div>
<div class="m2"><p>می مراد حریفان بجام ماست دگر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مه امید چو نوشد بر آسمان مراد</p></div>
<div class="m2"><p>فلک چو حلقه بگوشان غلام ماست دگر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شهان بسایه دیوار ما پناه آرند</p></div>
<div class="m2"><p>بلی همای سعادت بدام ماست دگر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کسیکه می نشنیدی سلام ما از کبر</p></div>
<div class="m2"><p>در انتظار جواب سلام ماست دگر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فروغ بم و فراغ صبوحی ات اهلی</p></div>
<div class="m2"><p>بیمن فاتحه صبح و شام ماست دگر</p></div></div>