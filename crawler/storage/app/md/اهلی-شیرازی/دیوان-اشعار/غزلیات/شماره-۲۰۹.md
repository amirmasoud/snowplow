---
title: >-
    شمارهٔ ۲۰۹
---
# شمارهٔ ۲۰۹

<div class="b" id="bn1"><div class="m1"><p>من که چون شمع ز داغ تو صدم روشنی است</p></div>
<div class="m2"><p>گر بگریم ز غمت غایت تر دامنی است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شب چراغ غم دل از در دلها طلبند</p></div>
<div class="m2"><p>ورنه محمود چکارش به در گلخنی است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>این چه سحرست که آن آهوی صید افکن تو</p></div>
<div class="m2"><p>خود بخواب است و صدش غمزه بصید افکنی است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آفتابی تو و کس را نبود تاب نظر</p></div>
<div class="m2"><p>مگر آن کس که چو آیینه دلش آهنی است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای ز جان پاک تر آن لعل می آلود بنوش</p></div>
<div class="m2"><p>کز کمینگاه دلم وسوسه در رهزنی است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همه کس درد دل خود به طبیبان گفتند</p></div>
<div class="m2"><p>قصه درد دل ماست که نا گفتنی است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اهلی از زخم رقیب است ز رحمت محروم</p></div>
<div class="m2"><p>غایت دوستی مدعیان دشمنی است</p></div></div>