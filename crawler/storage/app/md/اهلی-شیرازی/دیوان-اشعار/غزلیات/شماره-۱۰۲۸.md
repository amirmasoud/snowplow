---
title: >-
    شمارهٔ ۱۰۲۸
---
# شمارهٔ ۱۰۲۸

<div class="b" id="bn1"><div class="m1"><p>پیش تو ایطبیب جان بسکه ز خویش میروم</p></div>
<div class="m2"><p>حال نگفته از درت بادل ریش میروم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از همه کس چو ذره ام پیش تو آفتاب پس</p></div>
<div class="m2"><p>گر بمنت نظر بود از همه پیش میروم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پیش زبان مدعی چون مگسم چه غم بود</p></div>
<div class="m2"><p>من که بباد نوش تو بر سر نیش میروم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پیش تو آفتاب اگر ذره صفت ز جا روم</p></div>
<div class="m2"><p>مهر تو میکشد مرا من نه بخویش میروم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عشق بتان شد از ازل کیش من ایخدا پرست</p></div>
<div class="m2"><p>عیب چه میکنی مرا کز پی کیش میروم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>منکه چو طفل اشک خود زاده کوی دوستم</p></div>
<div class="m2"><p>اهلی از آن درم مکن منع که بیش میروم</p></div></div>