---
title: >-
    شمارهٔ ۹۸۱
---
# شمارهٔ ۹۸۱

<div class="b" id="bn1"><div class="m1"><p>کنون که جامه چو من میدری که سر مستم</p></div>
<div class="m2"><p>خوش است سینه صفایی اگر دهد دستم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زلاف مردمی ام قید خود پسندی بود</p></div>
<div class="m2"><p>سگ تو گشتم و از بند خویش وارستم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بدان هوس که چو دیو انگان خورم سنگت</p></div>
<div class="m2"><p>هزار شیشه ناموس و ننگ بشکستم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تو آفتابی و من همچو عیسی از تجرید</p></div>
<div class="m2"><p>بریدم از همه اشیا و با تو پیوستم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به هیچ راه چو اهلی ندیدمت روزی</p></div>
<div class="m2"><p>که سالها بامید تو باز ننشستم</p></div></div>