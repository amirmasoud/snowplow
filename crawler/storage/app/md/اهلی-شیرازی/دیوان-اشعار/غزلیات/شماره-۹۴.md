---
title: >-
    شمارهٔ ۹۴
---
# شمارهٔ ۹۴

<div class="b" id="bn1"><div class="m1"><p>به قتلم اینهمه خشم و عتاب حاجت نیست</p></div>
<div class="m2"><p>چو می کشد غم عشقم شتاب حاجت نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو شمع بزمی و صد خانه روشن از رویت</p></div>
<div class="m2"><p>بهر کجا که تویی آفتاب حاجت نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگر شراب نباشد تو باشی ای ساقی</p></div>
<div class="m2"><p>بیا که صحبت ما را شراب حاجت نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ما که قبله حاجت شد آستانه تو</p></div>
<div class="m2"><p>بکعبه رفتنم از هیچ باب حاجت نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دلا چو مرغ قفص چاره تو تسلیم است</p></div>
<div class="m2"><p>چه میطپی بنشین اضطراب حاجت نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عذاب ظلمتت غم بهر آب خضر مکش</p></div>
<div class="m2"><p>برای یکدم آب این عذاب حاجت نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خموش اهلی اگر توبه ات دهند از عشق</p></div>
<div class="m2"><p>حدیث بیهده گو را جواب حاجت نیست</p></div></div>