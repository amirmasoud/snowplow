---
title: >-
    شمارهٔ ۳۱۸
---
# شمارهٔ ۳۱۸

<div class="b" id="bn1"><div class="m1"><p>لعل جانبخشش که آرام دل شیدا ازوست</p></div>
<div class="m2"><p>راحت جان است ما را بلکه جان ما ازوست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از گلستان جهان هرگز مبادا کم چو سرو</p></div>
<div class="m2"><p>آن گل جنت که حسن گلشن دنیا ازوست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من ز رنگ آمیزی مشاطه او سوختم</p></div>
<div class="m2"><p>کاین دورنگی بامنش آن نوگل زیبا ازوست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چشم مست او که بینی گوشه گیر از هر طرف</p></div>
<div class="m2"><p>در جهان هرجا که گردد فتنه یی پیدا ازوست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زاهد از غوغای ما مشکن سبومی را چه جرم</p></div>
<div class="m2"><p>گر توانی منع او کن کاین همه غوغا ازوست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>لیلی و شیرین خریداران آن یوسف رخ اند</p></div>
<div class="m2"><p>در سر مجنون و شان هم مایه سودا ازوست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>وصف آن گل گوی اهلی کز نسیم روح بخش</p></div>
<div class="m2"><p>عندلیب گلشن روحانیان گویا ازوست</p></div></div>