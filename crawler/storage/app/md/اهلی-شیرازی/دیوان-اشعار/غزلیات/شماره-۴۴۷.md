---
title: >-
    شمارهٔ ۴۴۷
---
# شمارهٔ ۴۴۷

<div class="b" id="bn1"><div class="m1"><p>در عرق شد چو رخش ز آتش می تابی خورد</p></div>
<div class="m2"><p>وه که زان روی عرقناک دلم آبی خورد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در خیال خم آن طاق دو ابرو دل من</p></div>
<div class="m2"><p>ای بسا می که بهر گوشه محرابی خورد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون بپوشم ز کس این قصه که با همچو منی</p></div>
<div class="m2"><p>آفتابی چو تو می در شب مهتابی خورد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>فکر روزی چکند کس که دلم آب حیات</p></div>
<div class="m2"><p>از خضر جستی و از خنجر قصابی خورد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کی دل اهلی مسکین بسلامت باشد</p></div>
<div class="m2"><p>زینهمه سنگ ملامت که بهر بابی خورد</p></div></div>