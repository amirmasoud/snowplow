---
title: >-
    شمارهٔ ۳۱۷
---
# شمارهٔ ۳۱۷

<div class="b" id="bn1"><div class="m1"><p>هوا خوش است و مرا بی رخ تو دل خوش نیست</p></div>
<div class="m2"><p>هوای خوش چکند هرکه خاطرش خوش نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گل جمال تو شد تا چراغ بزم افروز</p></div>
<div class="m2"><p>چو لاله نیست دلی کز غمت در آتش نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگر پری طلبم چون تو آدمی نبود</p></div>
<div class="m2"><p>در آدمی نگرم چون تو کس پریوش نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بپوش بهر خدا زلف همچو ز نارت</p></div>
<div class="m2"><p>که نیست هیچ مسلمان کزو مشوش نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بسر نرفت بهاری که بی تو اهلی را</p></div>
<div class="m2"><p>خزان چهره بخون جگر منقش نیست</p></div></div>