---
title: >-
    شمارهٔ ۱۳۳۲
---
# شمارهٔ ۱۳۳۲

<div class="b" id="bn1"><div class="m1"><p>عاشق بیخانمان گر اختیاری داشتی</p></div>
<div class="m2"><p>بر مراد خاطر خود روزگاری داشتی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قسمت گردون اگر بودی بقانون مراد</p></div>
<div class="m2"><p>عاشق بیچاره هم وصلی زیاری داشتی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عالمی نخجیر بازویار از ایشان بی نیاز</p></div>
<div class="m2"><p>کاشکی باری سگش میل شکاری داشتی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اینهمه غوغای مجنون از غم لیل که بود</p></div>
<div class="m2"><p>کاش ازینسان سرو قدی گلعذاری داشتی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اعتبار یکجوم در کار عالم کس نکرد</p></div>
<div class="m2"><p>ای دریغا کار عالم اعتباری داشتی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عیب رندان میکند کاری ندارد شیخ شهر</p></div>
<div class="m2"><p>کی بما پرداختی گر زانکه کاری داشتی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>روی زرد اهلی از مهرت شدی روزی سفید</p></div>
<div class="m2"><p>گر خزان عاشقان روزی بهاری داشتی</p></div></div>