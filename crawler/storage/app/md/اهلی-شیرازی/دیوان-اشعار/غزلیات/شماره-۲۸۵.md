---
title: >-
    شمارهٔ ۲۸۵
---
# شمارهٔ ۲۸۵

<div class="b" id="bn1"><div class="m1"><p>چشم همه را بر گل روی تو نگاه است</p></div>
<div class="m2"><p>انصاف بده دیده مارا چه گناه است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>طوبی چکنم من که خرابم ز قد تو</p></div>
<div class="m2"><p>سرو تو مگر کمتر از آن شاخ گیاه است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مگذر ز وداع من بیمار که امروز</p></div>
<div class="m2"><p>دل عزم سفر دارد و جان بر سر راه است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آن خال ز نخدان که دل من به چه افکند</p></div>
<div class="m2"><p>گر چاه دلم کند خود اندر ته چاه است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گیرم که بر افالک زند خیمه رقیبت</p></div>
<div class="m2"><p>موقوف بیک شعله از این آتش و آه است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر خضر بسر چشمه حیوان رسدش دست</p></div>
<div class="m2"><p>مارا می کوثر بکف از دولت شاه است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اهلی که غلام تو بود زان خودش دان</p></div>
<div class="m2"><p>گر نامه سفیدست و گر نامه سیاه است</p></div></div>