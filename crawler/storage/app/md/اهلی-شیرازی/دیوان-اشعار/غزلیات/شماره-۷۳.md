---
title: >-
    شمارهٔ ۷۳
---
# شمارهٔ ۷۳

<div class="b" id="bn1"><div class="m1"><p>نیست آن در که ز گوش آمده تا دوش ترا</p></div>
<div class="m2"><p>می چکد آب لطافت زبنا گوش ترا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یک نگاه از رخ زیبات مرا هست طمع</p></div>
<div class="m2"><p>دین ترا عقل ترا صبر ترا هوش ترا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آه من سوخت جهانی تو عجب سنگدلی</p></div>
<div class="m2"><p>که نیارد نفس سوخته درجوش ترا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گرنه در خون دل خود شوی آغشته چو صید</p></div>
<div class="m2"><p>کی سک یار کند دست در آغوش ترا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یار آنگاه کند با تو سخن از سر لطف</p></div>
<div class="m2"><p>که ببیند ز حدیث همه خاموش ترا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر براند زرهی از ره دیگر باز آی</p></div>
<div class="m2"><p>که بیک ره نکند یار فراموش ترا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خرقه پوشان همه از رشک تو اهلی سوزند</p></div>
<div class="m2"><p>که مریدند جوانان قبا پوش ترا</p></div></div>