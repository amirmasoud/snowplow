---
title: >-
    شمارهٔ ۴۱۰
---
# شمارهٔ ۴۱۰

<div class="b" id="bn1"><div class="m1"><p>سوختیم از غم و این آتش پنهان همه هیچ</p></div>
<div class="m2"><p>همه داغیم ز خوبان و بر ایشان همه هیچ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غنچه بخت مرا هیچ گل آخر نشکفت</p></div>
<div class="m2"><p>زخم‌های جگر و چاک گریبان همه هیچ</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با حریفان دگر یار شد آن گل چه شگفت</p></div>
<div class="m2"><p>سعی باد و نفس مرغ سحرخوان همه هیچ</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در سرم بود که در پای تو ریزم دل و جان</p></div>
<div class="m2"><p>وه که از داغ تو شد کار دل و جان همه هیچ</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به جز از مهر و وفا هیچ نماند اهلی</p></div>
<div class="m2"><p>تخت و بخت و زر و مال و سر و سامان همه هیچ</p></div></div>