---
title: >-
    شمارهٔ ۴۰۰
---
# شمارهٔ ۴۰۰

<div class="b" id="bn1"><div class="m1"><p>از جلوه محبت خورشید حسن دوست</p></div>
<div class="m2"><p>هر ذره را تصور آن کافتاب اوست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ظل همای عشق گدا شاه میکند</p></div>
<div class="m2"><p>شاهی که چرخ در خم چوگان او چو گوست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جز در دل خراب اسیران غم مجو</p></div>
<div class="m2"><p>گنجی که خلق هردو جهانش به جستجوست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تحقیق من ز هر دو جهان این بود که عشق</p></div>
<div class="m2"><p>مغز حقیقت است و دگر هرچه هست پوست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کلک قضا که اینهمه صورت کشیده است</p></div>
<div class="m2"><p>مقصود کارخانه او صورت نکوست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از کلک مشکبار تو اهلی که دم زند؟</p></div>
<div class="m2"><p>در گلشنی که خار و گلش جمله مشکبوست</p></div></div>