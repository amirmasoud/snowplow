---
title: >-
    شمارهٔ ۴۶۶
---
# شمارهٔ ۴۶۶

<div class="b" id="bn1"><div class="m1"><p>باز شمعم خانه روشن، کوری اغیار کرد</p></div>
<div class="m2"><p>عاقبت دود چراغ شب نشینان کار کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صد هزاران عاشق گل گشت و کس آهی نزد</p></div>
<div class="m2"><p>مدعی از زخم خاری شکوه بسیار کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در سر بازار حسنش گل خریداری نیافت</p></div>
<div class="m2"><p>گرچه چندین گل فروشی گل درین بازار کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دور ازان لبها نمی خواهم حیات و زندگی</p></div>
<div class="m2"><p>زانکه هجر او مرا از زندگی بیزار کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پرده بگشود آن گل و اهلی ز حیرت دم نزد</p></div>
<div class="m2"><p>گرچه با خود همچو بلبل صد سخن تکرار کرد</p></div></div>