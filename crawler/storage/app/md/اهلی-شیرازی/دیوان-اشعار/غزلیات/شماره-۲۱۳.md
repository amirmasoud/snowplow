---
title: >-
    شمارهٔ ۲۱۳
---
# شمارهٔ ۲۱۳

<div class="b" id="bn1"><div class="m1"><p>سکون خاطر من بی تو سرو قامت نیست</p></div>
<div class="m2"><p>چو باد یک نفسم بی تو استقامت نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به جرم عشق اگر من سزای سوختنم</p></div>
<div class="m2"><p>تو خود بسوز مرا حاجت قیامت نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بجان دوست که گر صد هزار سرو بود</p></div>
<div class="m2"><p>بناز و شیوه یکی چون تو سرو قامت نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>درین خرابه که خار ملامت است همه</p></div>
<div class="m2"><p>بجز طریق محبت ره سلامت نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ندانمت ز چه کیشی چه دینت آیین است</p></div>
<div class="m2"><p>که چشم مست تو از کشتنش ندامت نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز پیر میکده اهلی، طلب جوانمردی</p></div>
<div class="m2"><p>که شیخ صومعه را هرگز این کرامت نیست</p></div></div>