---
title: >-
    شمارهٔ ۱۳۳۳
---
# شمارهٔ ۱۳۳۳

<div class="b" id="bn1"><div class="m1"><p>بسکه دل در سینه من سوخت داغ دلبری</p></div>
<div class="m2"><p>هر نفس کز دل بر آرم نیست بی خاکستری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آه از آن دلشادی اول که خنجر برکشید</p></div>
<div class="m2"><p>وای از آن نومیدی آخر که ز در بر دیگری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نیست جای کشتگان تیغ تو صحرای حشر</p></div>
<div class="m2"><p>باید از بهر شهیدان تو دیگر محشری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گو مباش اسباب عشرت زانکه مارا بس بود</p></div>
<div class="m2"><p>سینه با صد ناله چنگی، دل پر از خون ساغری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عاقبت اهلی گلی از خار غم خواهد دمید</p></div>
<div class="m2"><p>دردمندی را که از شاخ طرب نبود بری</p></div></div>