---
title: >-
    شمارهٔ ۵۶۸
---
# شمارهٔ ۵۶۸

<div class="b" id="bn1"><div class="m1"><p>چشم مجنون هرچه بیند صورت لیلی بود</p></div>
<div class="m2"><p>خوش بود صورت پرستی گر بدین معنی بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من ز آب و رنگ حسن ساقی خود یافتم</p></div>
<div class="m2"><p>آنچه فیض آب خضر و آتش موسی بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گو بمیر افسرده دل کاین نکته با من شمع گفت</p></div>
<div class="m2"><p>هر که از داغی نسوزد مردنش اولی بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زان خوشم با کنج غم کز رشک غیر آسوده ام</p></div>
<div class="m2"><p>دوزخی کاسوده باشم جنت اعلی بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر که با مشکین غزالان همچو مجنون خو گرفت</p></div>
<div class="m2"><p>میرمد ز آنجا که بوی مردم دنیی بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چاک دل نتواندم بی غمزه یی زلف تو دوخت</p></div>
<div class="m2"><p>رشته مریم اگر با سوزن عیسی بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در دو عالم گر نباشد مایه شادی غمت</p></div>
<div class="m2"><p>خاطر اهلی ملول از دنیی و عقبی بود</p></div></div>