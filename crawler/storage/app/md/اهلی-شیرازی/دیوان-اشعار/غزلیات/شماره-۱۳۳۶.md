---
title: >-
    شمارهٔ ۱۳۳۶
---
# شمارهٔ ۱۳۳۶

<div class="b" id="bn1"><div class="m1"><p>مارا کشی و زنده جاوید میکنی</p></div>
<div class="m2"><p>عیسی نکرد آنچه تو خورشید میکنی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>امید هرکه هست ز تیغ تو حاصلست</p></div>
<div class="m2"><p>مارا گناه چیست که نومید میکنی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جامی ببخش و همت رندان مست بین</p></div>
<div class="m2"><p>تا کی سخن ز حشمت جمشید میکنی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ساقی شدست مطرب دل خیز ایحریف</p></div>
<div class="m2"><p>گر رقص بر ترانه ناهید میکنی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اهلی ز شاخ بخت چه جویی بر مراد</p></div>
<div class="m2"><p>بیهوده میوه یی طلب از بید میکنی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تو ایسوار چرا سایه همعنان سازی</p></div>
<div class="m2"><p>مگر که بر سر عاشق دو اسبه می تازی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خوش آندمیکه دل از غصه با تو پردازم</p></div>
<div class="m2"><p>تو هم دمی بمن دردمند پردازی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بجز هلاک خودش آرزو نباشد هیچ</p></div>
<div class="m2"><p>کسیکه یافت چو پروانه ذوق جانبازی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>من ایهمای شرف کی رسم بطالع تو</p></div>
<div class="m2"><p>مگر که هم تو مرا سایه بر سر اندازی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بسوز سینه اهلی اگر رسی شاید</p></div>
<div class="m2"><p>که ناز حسن گذاری بعشق او نازی</p></div></div>