---
title: >-
    شمارهٔ ۸۰۸
---
# شمارهٔ ۸۰۸

<div class="b" id="bn1"><div class="m1"><p>شمشاد تو پروای کس از ناز ندارد</p></div>
<div class="m2"><p>با چشم سیه گو که نظر باز ندارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از عشق ننالیم که این زخم نهانی است</p></div>
<div class="m2"><p>یعنی که قضا میزند آواز ندارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مرغ دل ما در قفس دهر از آن ماند</p></div>
<div class="m2"><p>کز ضعف درون قوت پرواز ندارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر خاک بسر میکنم از خانه خرابی</p></div>
<div class="m2"><p>پروای من آن خانه برانداز ندارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خوشباش که کس محرم راز دل ما نیست</p></div>
<div class="m2"><p>مجنون تو کس همدم و همراز ندارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در پای خسان چون نشوم همچو گیاپست</p></div>
<div class="m2"><p>گر سرو توام بخت سرافراز ندارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اهلی نظرش جانب آن ترک ختایی است</p></div>
<div class="m2"><p>چشم کرم از مردم شیراز ندارد</p></div></div>