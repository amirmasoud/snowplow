---
title: >-
    شمارهٔ ۳۰
---
# شمارهٔ ۳۰

<div class="b" id="bn1"><div class="m1"><p>کوثر کجا و لعل روان بخش او کجا</p></div>
<div class="m2"><p>سرچشمه حیات کجا آب جو کجا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر کو نشد لاله جگر خون از آن غزال</p></div>
<div class="m2"><p>از وادی محبت او یافت بو کجا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سودای آن پری منه ای دل ز فکر خام</p></div>
<div class="m2"><p>دیوانه یی مگر، تو کجایی و او کجا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر روی زرد ما نکند آب دیده تر</p></div>
<div class="m2"><p>ما را میان خلق بود آب رو کجا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>من گندم بهشت بیک جو نمی خرم</p></div>
<div class="m2"><p>سیمرغ من بدانه سر آرد فرو کجا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مستم ز خون دیده و ساغر بغیرده</p></div>
<div class="m2"><p>دریا کشان کجا می جام و سبو کجا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اهلی ز گفتگوی بتان بس نمی کنی</p></div>
<div class="m2"><p>آخر ببین که میکشد این گفتگو کجا</p></div></div>