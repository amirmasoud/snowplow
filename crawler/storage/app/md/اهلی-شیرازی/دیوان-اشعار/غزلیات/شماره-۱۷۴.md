---
title: >-
    شمارهٔ ۱۷۴
---
# شمارهٔ ۱۷۴

<div class="b" id="bn1"><div class="m1"><p>دی از نظر چو سرو و خرامان من گذشت</p></div>
<div class="m2"><p>من دانم از غمش که چه بر جان من گذشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سوزم چو شمع و بر همه این سوز روشن است</p></div>
<div class="m2"><p>زین اشگ آتشین که بدامان من گذشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>صد دل کباب همچو من از گریه مست شد</p></div>
<div class="m2"><p>هرجا حکایت دل بریان من گذشت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کاری نکرد ناله من هم تو رحم کن</p></div>
<div class="m2"><p>فریاد رس که کار ز افغان من گذشت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اهلی، بجای سبزه و گل خار غم دمید</p></div>
<div class="m2"><p>هرجا که ابر دیده گریان من گذشت</p></div></div>