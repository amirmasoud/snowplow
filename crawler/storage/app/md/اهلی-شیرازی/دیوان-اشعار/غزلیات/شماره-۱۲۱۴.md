---
title: >-
    شمارهٔ ۱۲۱۴
---
# شمارهٔ ۱۲۱۴

<div class="b" id="bn1"><div class="m1"><p>همچو رخسار تو در عالم گلی بیخار کو</p></div>
<div class="m2"><p>همچو قدت سرونازی نازنین رفتار کو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای چو گل در پرده ما را قوت صبر از کجا</p></div>
<div class="m2"><p>ور گشایی پرده از رخ طاقت دیدار کو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زاهدان بیخبر منصور را بردار عشق</p></div>
<div class="m2"><p>سنگباران میکنند اما کسی بردار کو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در شب تاریک خورشیدست شمع روی تو</p></div>
<div class="m2"><p>بهر بیداران ولیکن دیده بیدار کو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>من که از سودای زلفت کافری دیوانه ام</p></div>
<div class="m2"><p>عارم از زنجیر آید حلقه زناز کو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سرو من گر نسبت قدت به نیشکر کنند</p></div>
<div class="m2"><p>نیشکر را در زبان شیرینی گفتار کو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اهلی از بیداد خوبان چند میگویی سخن</p></div>
<div class="m2"><p>سودت ای بسیار گو زین گفتن بسیار کو</p></div></div>