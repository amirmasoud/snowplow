---
title: >-
    شمارهٔ ۷۲۰
---
# شمارهٔ ۷۲۰

<div class="b" id="bn1"><div class="m1"><p>هرگز از کوی تو کس دور بشمشیر نشد</p></div>
<div class="m2"><p>هرگز از دیدن دیدار تو کس سیر نشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گام بر سبزه باغ طربش باد حرام</p></div>
<div class="m2"><p>هرکه در راه وفا بر سر شمشیر نشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>صید نخجیرگه عشق به کامی نرسید</p></div>
<div class="m2"><p>تا به چابک صفتی در دهن شیر نشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دست ساقی بکرم در همه وقت است بلند</p></div>
<div class="m2"><p>هرگز از دور فلک دست کرم زیر نشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زود رفت از بر ما یار و دل اهلی زار</p></div>
<div class="m2"><p>سوخت از دوری او گرچه بسی دیر نشد</p></div></div>