---
title: >-
    شمارهٔ ۱۰۹۷
---
# شمارهٔ ۱۰۹۷

<div class="b" id="bn1"><div class="m1"><p>همدمان رفتند و من از همرهان وامانده ام</p></div>
<div class="m2"><p>میرم از این غم که بی یاران چرا من زنده ام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تاب وصلم نیست ایمه چون زیم در هجر تو</p></div>
<div class="m2"><p>وای بر مردن چو من در زندگی وامانده ام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>داغ سودای غمت دیوانه کردم ای پری</p></div>
<div class="m2"><p>زانسبب چونشمع گه در گریه گه در خنده ام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گرچه آزاد جهانم همچو سرو ای ابر لطف</p></div>
<div class="m2"><p>رحمتی فرما که از دست تهی شرمنده ام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همت من در نظر نارد جز آنخورشید رخ</p></div>
<div class="m2"><p>گرچه درویشم نظر جای بلند افکنده ام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نازنینان گر کشندم سر نمی تابم ز حکم</p></div>
<div class="m2"><p>پادشاهانند ایشان من فقیر و بنده ام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زین چمن اهلی مرا دیگر بهیچ امید نیست</p></div>
<div class="m2"><p>زانکه از شاخ بقا چونغنچه دل برکنده ام</p></div></div>