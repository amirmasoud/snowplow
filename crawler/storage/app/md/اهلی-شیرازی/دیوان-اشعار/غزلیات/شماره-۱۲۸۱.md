---
title: >-
    شمارهٔ ۱۲۸۱
---
# شمارهٔ ۱۲۸۱

<div class="b" id="bn1"><div class="m1"><p>نیست از همت خود با دو جهانم کاری</p></div>
<div class="m2"><p>همتی دارم اگر هیچ ندارم باری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قیمت عمر ندانست و نداند هرگز</p></div>
<div class="m2"><p>هرکرا نیست بیوسف صفتی بازاری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نیست غم گر دلم از شوق تو بسیار نماند</p></div>
<div class="m2"><p>پیش رندان تو هم جان نبود بسیاری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مشکل عشق تو از مدرسه نگشود مرا</p></div>
<div class="m2"><p>مگرم از در میخانه گشاید کاری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تو که در میکده با پیر مغانی اهلی</p></div>
<div class="m2"><p>کعبه بگذار چه حاصل ز در و دیواری</p></div></div>