---
title: >-
    شمارهٔ ۶۸۶
---
# شمارهٔ ۶۸۶

<div class="b" id="bn1"><div class="m1"><p>زبسکه روزم از آن مه ملال می خیزد</p></div>
<div class="m2"><p>شبم بکشتن خود صد خیال می خیزد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نهال قد بتان جمله فتنه انگیزند</p></div>
<div class="m2"><p>ندانم از چه زمین این نهال می خیزد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بسوز کوکب بخت زبون من ایماه</p></div>
<div class="m2"><p>کزین ستاره مدامم و بال می خیزد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رسید وصل مرا چشم زخم هجر آری</p></div>
<div class="m2"><p>شرار فتنه ز شمع وصال می خیزد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چه گلشنی است جمالت که از عرق دروی</p></div>
<div class="m2"><p>هزار چشمه آب زلال می خیزد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نمیدمید گل و سر و اگر بدانستی</p></div>
<div class="m2"><p>که از قد تو صدش انفعال می خیزد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مگر پیام ترا مرغ جان برد اهلی</p></div>
<div class="m2"><p>که باد صبح بسی خسته حال می خیزد</p></div></div>