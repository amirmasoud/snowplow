---
title: >-
    شمارهٔ ۱۱۱۳
---
# شمارهٔ ۱۱۱۳

<div class="b" id="bn1"><div class="m1"><p>اگر تو دور کنی از درم صبور شوم</p></div>
<div class="m2"><p>ولی خدا نگذارد که از تو دور شوم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سرم ز سجده این در چه خوش حضوری یافت</p></div>
<div class="m2"><p>خوش آنکه خاک درت از سر حضور شوم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شبی چو آبحیات از درم درآ ای شمع</p></div>
<div class="m2"><p>که گرچه ظلمت محضم تمام نور شوم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>باختیار چرا در رهت نگردم خاک</p></div>
<div class="m2"><p>که در فراق تو خاک از سر سرور شوم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تو بدگمان مشو ایگل که من نه آنمرغم</p></div>
<div class="m2"><p>که گر بخلد روم بیتو صید حور شوم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سگ رقیب تو غافل ز حلم شیران است</p></div>
<div class="m2"><p>مباد آنکه ز غیرت بر او غیور شوم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>برغم کج نظران جرعه یی به اهلی بخش</p></div>
<div class="m2"><p>که از شراب تو مست می طهور شوم</p></div></div>