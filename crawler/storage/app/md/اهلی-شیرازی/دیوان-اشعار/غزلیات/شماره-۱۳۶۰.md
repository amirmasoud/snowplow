---
title: >-
    شمارهٔ ۱۳۶۰
---
# شمارهٔ ۱۳۶۰

<div class="b" id="bn1"><div class="m1"><p>از خون دیدها شد کوی تو لاله‌زاری</p></div>
<div class="m2"><p>بس دیده خاک ره شد کو چشم اعتباری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از جلوه‌های امکان چندان که نقش بستم</p></div>
<div class="m2"><p>صورت نبست در دل شکل تو گلعذاری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آیینه جمالت هر چند سوخت جانم</p></div>
<div class="m2"><p>هرگر مباد او را بر دل ز من غباری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شادم که رشته جان، شد دام صحبت تو</p></div>
<div class="m2"><p>کی بهتر از تو افتد در دام من شکاری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر چند بار جورت جان سوخت عاشقان را</p></div>
<div class="m2"><p>گر بر کسست ناخوش ما را خوشست باری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خلق از هوای عالم در دست باد دارند</p></div>
<div class="m2"><p>خوش وقت آنکه دارد در دست زلف یاری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اهلی ز سنگ خوبان کنج لحد حصارست</p></div>
<div class="m2"><p>آسوده شو که نبود زین خوبتر حصاری</p></div></div>