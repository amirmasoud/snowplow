---
title: >-
    شمارهٔ ۵۴۴
---
# شمارهٔ ۵۴۴

<div class="b" id="bn1"><div class="m1"><p>چشم زناز یوسفش سوی پدر نمی فتد</p></div>
<div class="m2"><p>ناز ببین که بر پدر چشم پسر نمی فتد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>منتظرم ولی تو کی چشم بچشم من کنی</p></div>
<div class="m2"><p>کاین دو ستاره را بهم هیچ نظر نمی فتد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون تو ز در درآمدی باش که جان فدا کنم</p></div>
<div class="m2"><p>زانکه بجان ازین به ام کار دگر نمی فتد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از کف ساقیی چو تو باده مستی این چنین</p></div>
<div class="m2"><p>سنگ بود نه آدمی هرکه بسر نمی فتد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دام فسون نهاده ام در ره آرزو ولی</p></div>
<div class="m2"><p>صید مراد را بمن هیچ گذر نمی فتد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همدم اهل راز شو بند قبای ناز را</p></div>
<div class="m2"><p>باز گشا که از میان راز بدر نمی فتد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اهلی اگر برافکند خانه عمر سیل غم</p></div>
<div class="m2"><p>چون تو بعشق زنده یی نام تو بر نمی فتد</p></div></div>