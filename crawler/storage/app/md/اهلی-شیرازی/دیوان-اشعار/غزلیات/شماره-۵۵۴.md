---
title: >-
    شمارهٔ ۵۵۴
---
# شمارهٔ ۵۵۴

<div class="b" id="bn1"><div class="m1"><p>هر که آگه ز دل سوخته من باشد</p></div>
<div class="m2"><p>بخدا رحم کند گر همه دشمن باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آفتابا، نفسی خانه من روشن کن</p></div>
<div class="m2"><p>چند گوشم به در و چشم به روزن باشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>برق رخسار بتان گر همه عالم سوزد</p></div>
<div class="m2"><p>چه غم آنرا که چو من سوخته خرمن باشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر بود ساقی گلچهره چه حاجت بچمن</p></div>
<div class="m2"><p>گل بدست ر که عالم همه گلشن باشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عالم از سرو قدان گر همه گلشن گردد</p></div>
<div class="m2"><p>مرغ عاشق نظرش بر گل و سوسن باشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا چو گردم نکند از در او دور رقیب</p></div>
<div class="m2"><p>مهل ای گریه که گردیش بدامن باشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بزم ما تیره کند اهلی شوریده به آه</p></div>
<div class="m2"><p>جای دیوانه همان به که بگلخن باشد</p></div></div>