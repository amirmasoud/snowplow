---
title: >-
    شمارهٔ ۱۰۰
---
# شمارهٔ ۱۰۰

<div class="b" id="bn1"><div class="m1"><p>به عهد یوسف من کز فرشته افزون است</p></div>
<div class="m2"><p>کسی حکایت لیلی کند که مجنون است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگر چه جام جمی آه از آن دل نازک</p></div>
<div class="m2"><p>که تا نفس زده ام خاطرت دگرگون است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سگ تو واقف بیمار دل ز بیداری است</p></div>
<div class="m2"><p>تو مست خواب چه دانی که حال ما چون است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو غنچه سینه ریشم به بین و حال مپرس</p></div>
<div class="m2"><p>که شرح زخم درونم ز وصف بیرون است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نواله ستمت قسمت از ازل داریم</p></div>
<div class="m2"><p>نصیبه ازل است این ستم نه اکنون است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زرطل عشق گدا شاه اگر شود چه عجب</p></div>
<div class="m2"><p>که ظل عشق عجب سایه همایون است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو شیشه گریه تلخی که می کنی اهلی</p></div>
<div class="m2"><p>هزار کاسه چشم از غم تو پر خون است</p></div></div>