---
title: >-
    شمارهٔ ۸۴۰
---
# شمارهٔ ۸۴۰

<div class="b" id="bn1"><div class="m1"><p>جان رفت و دل مقید صد غم بود هنوز</p></div>
<div class="m2"><p>تن خاک گشت و دیده پر از نم بود هنوز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هرگز نرفت از دل ما خارخار وصل</p></div>
<div class="m2"><p>داغ بهشت بر دل آدم بود هنوز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>صد شاخ گل دمید و ورق کرد زرد و ریخت</p></div>
<div class="m2"><p>نخل قد تو فتنه عالم بود هنوز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>من خاک راه گر شدم ای نوبهار حسن</p></div>
<div class="m2"><p>گلزار جان ببوی تو خرم بود هنوز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>صد بیستون چو صورت شیرین بباد رفت</p></div>
<div class="m2"><p>بنیاد عشق ماست که محکم بود هنوز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آیین نیکویی سبب نام نیک شد</p></div>
<div class="m2"><p>جم رفت و وصف آینه جم بود هنوز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اهلی بهر قدم که بود در ره بتان</p></div>
<div class="m2"><p>گر صد هزار سجده کند کم بود هنوز</p></div></div>