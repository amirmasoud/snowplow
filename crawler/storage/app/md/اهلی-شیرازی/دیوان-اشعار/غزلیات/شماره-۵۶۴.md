---
title: >-
    شمارهٔ ۵۶۴
---
# شمارهٔ ۵۶۴

<div class="b" id="bn1"><div class="m1"><p>وجود ما زغمت تا عدم نخواهد شد</p></div>
<div class="m2"><p>غم تو از دل ما هیچ کم نخواهد شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کسیکه سیر نگشت از غم تو در همه عمر</p></div>
<div class="m2"><p>بیک دو روز دگر سیر غم نخواهد شد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به کوی مغبچه محرم کن ای فلک ما را</p></div>
<div class="m2"><p>که کار ما بطواف حرم نخواهد شد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به هرزه چند گدازد دل رقیب از عشق</p></div>
<div class="m2"><p>یقین که سنگ سیه جام جم نخواهد شد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چنان نهاد برین آستانه سر اهلی</p></div>
<div class="m2"><p>که گر سرش برود یکقدم نخواهد شد</p></div></div>