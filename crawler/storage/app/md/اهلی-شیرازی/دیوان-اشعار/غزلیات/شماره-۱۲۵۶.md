---
title: >-
    شمارهٔ ۱۲۵۶
---
# شمارهٔ ۱۲۵۶

<div class="b" id="bn1"><div class="m1"><p>تا ساقی گلهذار رفته</p></div>
<div class="m2"><p>دست و دل ما ز کار رفته</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>منت چه نهم که شد فدایش</p></div>
<div class="m2"><p>جانی که هزار بار رفته</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عالم همه صید خویش چون کرد</p></div>
<div class="m2"><p>دیگر چه پی شکار رفته</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا عشق بباد داد خاکم</p></div>
<div class="m2"><p>از آینه ام غبار رفته</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر چند که کارم انتظارست</p></div>
<div class="m2"><p>کار من از انتظار رفته</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا در دل ما قرار بگرفت</p></div>
<div class="m2"><p>از جان و دلم قرار رفته</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یاران پی کار خویش رفتند</p></div>
<div class="m2"><p>اهلی بفدای یار رفته</p></div></div>