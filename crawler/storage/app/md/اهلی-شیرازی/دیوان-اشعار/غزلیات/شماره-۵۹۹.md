---
title: >-
    شمارهٔ ۵۹۹
---
# شمارهٔ ۵۹۹

<div class="b" id="bn1"><div class="m1"><p>تا یوسفش چاکی چو گل در جیب پیراهن نشد</p></div>
<div class="m2"><p>از نکهت پیراهنش چشم پدر روشن نشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اول زرشک دوستی فرزند آدم کشته شد</p></div>
<div class="m2"><p>تا تیغ غیرت سر نزد کس قابل کشتن نشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سرو از سجود قامتت سجاده بر آب افکند</p></div>
<div class="m2"><p>کاین طاعتش حاصل کسی بی پاکی دامن نشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ننهاد گنج دوستی جز در دل ما عشق تو</p></div>
<div class="m2"><p>آری فلک هم بی غرض با اهل دل دشمن نشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پیدا نشد مغز طرب در استخوان هر گر مرا</p></div>
<div class="m2"><p>تا مرهم پیکان او در استخوان من نشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نگذاردم غیرت که دم چون غنچه از داغت زنم</p></div>
<div class="m2"><p>زان آتش پنهان من بر هیچکس روشن نشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از آه اهلی کس نشد واقف ز چاک سینه اش</p></div>
<div class="m2"><p>تا از درونش دودِ دل بیرون به صد روزن نشد</p></div></div>