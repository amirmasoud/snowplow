---
title: >-
    شمارهٔ ۹۱۱
---
# شمارهٔ ۹۱۱

<div class="b" id="bn1"><div class="m1"><p>خسته یی کان عنبرین مو بگذرد زود از سرش</p></div>
<div class="m2"><p>همچو شمع از آتش حسرت رود دود از سرش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون نمیرد خسته مسکین که بر بالین او</p></div>
<div class="m2"><p>دیر آمد آن طبیب و زود فرمود از سرش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>غم ز درویشی ندارد رند دیر ای مغبچه</p></div>
<div class="m2"><p>زانکه خم گنج است و ساقی مهر بگشود از سرش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر شهی کز فر تاج زر نشد خاک رهت</p></div>
<div class="m2"><p>باد غیرت تاج زر چون لاله بر بود از سرش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اهلی از داغ تو خواهد سوختن تا زنده است</p></div>
<div class="m2"><p>کی چو شمع این آتش سودا رود زود از سرش</p></div></div>