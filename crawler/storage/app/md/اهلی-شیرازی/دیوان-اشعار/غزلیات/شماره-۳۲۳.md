---
title: >-
    شمارهٔ ۳۲۳
---
# شمارهٔ ۳۲۳

<div class="b" id="bn1"><div class="m1"><p>گردون که کین اهل نظر در نهاد اوست</p></div>
<div class="m2"><p>اسباب نامرادی ما بر مراد اوست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ساغر بغیر داد مرا زهر غم دهد</p></div>
<div class="m2"><p>پیش که داد یار برم دار داد ازوست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>می خوردنم بیاد لبش بود وین زمان</p></div>
<div class="m2"><p>خون دلی که میخورم آنهم بیاد اوست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>امروز در دلم گرهی از غم است سخت</p></div>
<div class="m2"><p>کو ناو کی که چشم دلم بر گشاد اوست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اهلی متاع او سخن است و درین زمان</p></div>
<div class="m2"><p>چیزی که نارواست متاع کساد اوست</p></div></div>