---
title: >-
    شمارهٔ ۴۵۴
---
# شمارهٔ ۴۵۴

<div class="b" id="bn1"><div class="m1"><p>خوبان دل گرم و نفس سرد چه دانند؟</p></div>
<div class="m2"><p>باروی چو گل قدر رخ زرد چه دانند؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آسوده دلانی که بخوابند همه شب</p></div>
<div class="m2"><p>سرگشتگی عاشق شبگرد چه دانند؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گویند حریفان که چرا دل بتو دادم</p></div>
<div class="m2"><p>من دانم و دل مردم بیدرد چه دانند؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خلقی همه را چشم حسد بر گل وصل است</p></div>
<div class="m2"><p>خاری که بود بر جگر مرد چه دانند؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اهلی، سخن صومعه بگذار که زهاد</p></div>
<div class="m2"><p>راه و روش میکده پرورد چه دانند؟</p></div></div>