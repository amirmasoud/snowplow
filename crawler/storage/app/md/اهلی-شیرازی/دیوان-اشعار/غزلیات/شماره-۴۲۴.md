---
title: >-
    شمارهٔ ۴۲۴
---
# شمارهٔ ۴۲۴

<div class="b" id="bn1"><div class="m1"><p>خلق از سگت نه از بد دشمن فغان کنند</p></div>
<div class="m2"><p>دشمن چه سگ بود گله از دوستان کنند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز نار عشق رشته جان شد مرا چو شمع</p></div>
<div class="m2"><p>بر خود نبسته ام که مرا منع از آن کنند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دانم یقین نه مستی و از عشوه آندو چشم</p></div>
<div class="m2"><p>ترسم که با یقین خودم بد گمان کنند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خوبان بروی ما نگشایند در مگر</p></div>
<div class="m2"><p>روزی که نوبهار جوانی خزان کنند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مرغ دل از بتان نبرد ره بزیرکی</p></div>
<div class="m2"><p>کاین قوم صید دل نه به تیر و کمان کنند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اهلی ز وصف آن شکرین لب غریب نیست</p></div>
<div class="m2"><p>گر طوطیان حدیث تو ورد زبان کنند</p></div></div>