---
title: >-
    شمارهٔ ۸۰۴
---
# شمارهٔ ۸۰۴

<div class="b" id="bn1"><div class="m1"><p>یارم وداع کرد و ز آغوش میرود</p></div>
<div class="m2"><p>نام وداع می برم و هوش میرود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زان خون دل ز دیده روانست کز نظر</p></div>
<div class="m2"><p>آن نورسیده سرو قباپوش میرود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دوش آن نشاط خنده و امروز گریه یی</p></div>
<div class="m2"><p>کز خاطرم نشاط شب دوش میرود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بی دوست دیک سینه غم جوش میزند</p></div>
<div class="m2"><p>وین خون دیده بر رخ از آن دوش میرود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مشکل حکایتی است که رنجد ز ناله یار</p></div>
<div class="m2"><p>وزناله کار عاشق خاموش میرود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای باد یاد سنبل و گل پیش ما مکن</p></div>
<div class="m2"><p>چون حرف از آن دو زلف و بناگوش میرود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اهلی زجور یار غباری شد و هنوز</p></div>
<div class="m2"><p>دنبال آن سوار قباپوش میرود</p></div></div>