---
title: >-
    شمارهٔ ۵۷۴
---
# شمارهٔ ۵۷۴

<div class="b" id="bn1"><div class="m1"><p>چند چراغ آه من عمر مرا تبه کند</p></div>
<div class="m2"><p>روشنی جهان شود خانه من سیه کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر چه بتان سنگدل رحم بگریه کم کنند</p></div>
<div class="m2"><p>چشمه اشک عاقبت در دل سنگ ره کند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دانه خال نیکوان تخم گنه شود ولی</p></div>
<div class="m2"><p>مستی شوق آدمی کی حذراز گنه کند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در ره عشق میرود کعبه بباد نیستی</p></div>
<div class="m2"><p>مست غروربین که چون تکیه بخانقه کند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خواری گلرخان مرا عبرت خلق کرده است</p></div>
<div class="m2"><p>دم نزد ز عاشقی هر که مرا نگه کند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اهلی شب نشین نفس بی رخ دوست کی زند</p></div>
<div class="m2"><p>مرغ سحر ببوی گل ناله صبحگه کند</p></div></div>