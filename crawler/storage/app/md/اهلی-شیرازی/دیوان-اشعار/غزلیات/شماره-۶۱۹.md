---
title: >-
    شمارهٔ ۶۱۹
---
# شمارهٔ ۶۱۹

<div class="b" id="bn1"><div class="m1"><p>جان به فکر جهان نمی‌ارزد</p></div>
<div class="m2"><p>این جهان هم به آن نمی‌ارزد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر زمین یک زمان چو دلتنگی</p></div>
<div class="m2"><p>به زمین و زمان نمی‌ارزد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سود عالم زیان عاقبت است</p></div>
<div class="m2"><p>هیچ سود این زیان نمی‌ارزد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>صحبت باغ اگرچه روح‌افزاست</p></div>
<div class="m2"><p>منت باغبان نمی‌ارزد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پیش ما عاشقان ناپروا</p></div>
<div class="m2"><p>زندگی رایگان نمی‌ارزد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ذوق مستی و می‌ْپرستی هم</p></div>
<div class="m2"><p>طعنه ناکسان نمی‌ارزد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اهلی از کس مخواه مرهم دل</p></div>
<div class="m2"><p>که به زخم زبان نمی‌ارزد</p></div></div>