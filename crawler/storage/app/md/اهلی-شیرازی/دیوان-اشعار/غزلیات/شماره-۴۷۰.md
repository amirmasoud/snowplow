---
title: >-
    شمارهٔ ۴۷۰
---
# شمارهٔ ۴۷۰

<div class="b" id="bn1"><div class="m1"><p>فلک بدور تو طفلی که در وجود آورد</p></div>
<div class="m2"><p>کجا به مسجد و محراب سر فرود آورد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کسی که قبله گهش آن دو طاق ابرو شد</p></div>
<div class="m2"><p>نهاد سر بزمین و ترا سجود آورد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دلم که کرد ز سودای عقل جمله زیان</p></div>
<div class="m2"><p>زیان او همه از عشق رو بسود آورد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تو شاه حسنی و مهمان عاشق درویش</p></div>
<div class="m2"><p>مرنج از آنکه می و نقل دیر و زود آورد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کباب کن دل و خونش تو می برغبت خور</p></div>
<div class="m2"><p>که ناتوان تو در خانه آنچه بود آورد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>فغان که مطرب مجلس ز نشتر مضراب</p></div>
<div class="m2"><p>خراش در رگ و جان از خروش عود آورد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مرا چو صورت آیینه روی او اهلی</p></div>
<div class="m2"><p>هزار بار عدم کرد و در وجود آورد</p></div></div>