---
title: >-
    شمارهٔ ۵۹۲
---
# شمارهٔ ۵۹۲

<div class="b" id="bn1"><div class="m1"><p>تا من از مادر نزادم غم که زاینده شد</p></div>
<div class="m2"><p>تا نمیرم آتش دوزخ نخواهد زنده شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کی ببوی وصلت از باد هوا خواهد شکفت</p></div>
<div class="m2"><p>غنچه دل کز نهال زندگی بر کنده شد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عیب من کرد آنکه حسنت پرده جانش درید</p></div>
<div class="m2"><p>پیش یوسف مدعی از دست خود شرمنده شد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا نیفتد پرتو خورشید همت بر یکی</p></div>
<div class="m2"><p>سایه اش بر کس نخواهد چون هما فرخنده شد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شد بدرویشی قبول بندگی اهلی ز دوست</p></div>
<div class="m2"><p>نیکبخت اینجا کسی باشد که بی زر بنده شد</p></div></div>