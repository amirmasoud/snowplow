---
title: >-
    شمارهٔ ۴۸۰
---
# شمارهٔ ۴۸۰

<div class="b" id="bn1"><div class="m1"><p>هرگز غبار حسرت دور از دلم نشد</p></div>
<div class="m2"><p>هرگز ز صد مراد یکی حاصلم نشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر خار پای گشتم و گر خاک ره شدم</p></div>
<div class="m2"><p>آن شاخ گل بهیچ صفت مایلم نشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفتم که تحفه سگ او دل کنم ولی</p></div>
<div class="m2"><p>آنهم قبول از دل نا مقبلم نشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هرچند بخت تیره بتاریکی ام بسوخت</p></div>
<div class="m2"><p>جز برق آه روشنی محفلم نشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خار ستم که از غم او بود بر دلم</p></div>
<div class="m2"><p>تا بر نرست گل ز گلم از دلم نشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>روی زمین بشست دو چشمم ز اشک شور</p></div>
<div class="m2"><p>وین شوری غم از دل و آب و گلم نشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آسان نگشت کوی تو منزل چو اهلی ام</p></div>
<div class="m2"><p>تا سر نرفت کوی تو سر منزلم نشد</p></div></div>