---
title: >-
    شمارهٔ ۱۳۶۲
---
# شمارهٔ ۱۳۶۲

<div class="b" id="bn1"><div class="m1"><p>باز دل میبردم عشوه سرو نازی</p></div>
<div class="m2"><p>ناخنی باز بدل میزندم شهبازی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مرغ دل سر و قدی جست و ز طوبی بگذشت</p></div>
<div class="m2"><p>طایر همت ما کرد عجب پروازی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کی شود همنفس و همدم ارباب وفا</p></div>
<div class="m2"><p>سرو نازی که نگاهی نکند بی نازی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ایکه چشم تو زند مرغ دل صید به تیر</p></div>
<div class="m2"><p>کس چو چشم تو ندید آهوی تیر اندازی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عاشقان در قدمت مور صفت کشته شدند</p></div>
<div class="m2"><p>صد هزاران که نیامد ز یکی آوازی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اهلی از شمع محبت کسی افروخت چراغ</p></div>
<div class="m2"><p>که چو پروانه سر مست بود جانبازی</p></div></div>