---
title: >-
    شمارهٔ ۱۳۲۶
---
# شمارهٔ ۱۳۲۶

<div class="b" id="bn1"><div class="m1"><p>ای یوسف عزیز چه از ما نهان شوی</p></div>
<div class="m2"><p>از ما مپوش رخ که عزیز جهان شوی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ماه تمام من که کمی چون مهت مباد</p></div>
<div class="m2"><p>یارب همیشه پیر شوی و جوان شوی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من پیر روشن آینه ام ای شکر دهن</p></div>
<div class="m2"><p>طوطی سخن شوی چو بمن همزبان شوی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بشنو سخن که حسن قبولی دلست و بس</p></div>
<div class="m2"><p>این نکته گر قبول کنی نکته دان شوی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یوسف شنیده یی که عزیزی بخلق یافت</p></div>
<div class="m2"><p>گر خلق و لطف پیشه کنی بیش از آن شوی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اهلی مگیر گوشه ز ابرو کمان خویش</p></div>
<div class="m2"><p>گر هم ز تیر طعنه بعالم نشان شوی</p></div></div>