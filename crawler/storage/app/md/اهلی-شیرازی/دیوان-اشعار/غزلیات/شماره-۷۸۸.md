---
title: >-
    شمارهٔ ۷۸۸
---
# شمارهٔ ۷۸۸

<div class="b" id="bn1"><div class="m1"><p>گر من از درد تو مردم هرگزت دردی مباد</p></div>
<div class="m2"><p>جان من گر خاک شد بر خاطرت گردی مباد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>میرم از درد و نپرسی وه چه بی دردیست این</p></div>
<div class="m2"><p>کس چو من هرگز اسیر چون تو بی دردی مباد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یارب ای سرو سهی عاشق شوی اما دلت</p></div>
<div class="m2"><p>مبتلای عشق چون خود ناز پروردی مباد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر دل افکاری که شد مست از بهار عشق تو</p></div>
<div class="m2"><p>چون خزان بی اشک سرخ و چهره زردی مباد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عیش پیران گر همه خضرند بی خوبان چه سود</p></div>
<div class="m2"><p>بزم خوبان نیز بی پیر جهانگردی مباد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عمر جاویدان اهلی صحبت صاحبدل است</p></div>
<div class="m2"><p>آفتاب عمر هم بی سایه مردی مباد</p></div></div>