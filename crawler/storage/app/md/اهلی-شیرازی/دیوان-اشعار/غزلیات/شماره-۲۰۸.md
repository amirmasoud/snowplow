---
title: >-
    شمارهٔ ۲۰۸
---
# شمارهٔ ۲۰۸

<div class="b" id="bn1"><div class="m1"><p>گنج قارون بزمین رفت و ملامت باقی است</p></div>
<div class="m2"><p>عشق گنجی است که تا روز قیامت باقی است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پیش تابوت من ای نخل خرامان بگذر</p></div>
<div class="m2"><p>که هنوزم هوس این قد و قامت باقی است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر که در پای سگت کشته نشد روز وصال</p></div>
<div class="m2"><p>گرچه جان داد به هجر تو غرامت باقی است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر کشندت بملامت نکنی توبه ز عشق</p></div>
<div class="m2"><p>سخن من بشنو ورنه ملامت باقی است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>من سگ کوی مغانم که درین قحط کرم</p></div>
<div class="m2"><p>نیست جز میکده جایی که کرامت باقی است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اهلی از سر بگذر یا بکناری بنشین</p></div>
<div class="m2"><p>گر نیی مرد بلا کنج سلامت باقی است</p></div></div>