---
title: >-
    شمارهٔ ۲۷۱
---
# شمارهٔ ۲۷۱

<div class="b" id="bn1"><div class="m1"><p>خوش آنکه دور فلک بر مراد من میگشت</p></div>
<div class="m2"><p>که خار گلشن بختم گل و سمن می گشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خوش آنکه بر من لب تشنه خون ترش میشد</p></div>
<div class="m2"><p>مرا از آن ترشی آب در دهن می گشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خوش آنکه از سخنم لب چو غنچه گر بستی</p></div>
<div class="m2"><p>دگر شکفته تر از گل بیک سخن می گشت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خوش آنکه گر گرهی می فتاد در کارم</p></div>
<div class="m2"><p>گره گشای من آن زلف پرشکن می گشت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خوش آنکه در ظلمات غم آن لب شیرین</p></div>
<div class="m2"><p>ز خنده چشمه حیوان به چشم من می گشت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خوش از آنکه از رخ آن بت هزار عاشق مست</p></div>
<div class="m2"><p>بیک نظاره که می کرد برهمن می گشت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خوش آنکه گاه تبسم هزار چون اهلی</p></div>
<div class="m2"><p>فدای آن لب شیرین و آن دهن می گشت</p></div></div>