---
title: >-
    شمارهٔ ۱۹۷
---
# شمارهٔ ۱۹۷

<div class="b" id="bn1"><div class="m1"><p>دل را نمک از گریه گرم است شکی نیست</p></div>
<div class="m2"><p>بی چاشنی گریه کبابش نمکی نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای گل ز غم خویش یکی با تو چگوید</p></div>
<div class="m2"><p>درد دل عشاق هزارست یکی نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از قصه مجنون که بصد رنگ شنیدی</p></div>
<div class="m2"><p>بیش است غم ما و درین قصه شکی نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خوبان دل پاک از دل آلوده شناسد</p></div>
<div class="m2"><p>در قلب شناسی به ازیشان محکی نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اهلی سگ خوبان شد و از زهد بری گشت</p></div>
<div class="m2"><p>باری نبود دیو رهی گر ملکی نیست</p></div></div>