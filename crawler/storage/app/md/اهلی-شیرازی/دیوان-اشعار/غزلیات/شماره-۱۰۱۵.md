---
title: >-
    شمارهٔ ۱۰۱۵
---
# شمارهٔ ۱۰۱۵

<div class="b" id="bn1"><div class="m1"><p>سگ توام من و عمری بغم اسیر شدم</p></div>
<div class="m2"><p>مرانم از در خود این زمان که پیر شدم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>امید از تو نگاهی بگوشه چشم است</p></div>
<div class="m2"><p>من از جهان بامید تو گوشه گیر شدم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بجان هنوز خریدار یوسفم هر چند</p></div>
<div class="m2"><p>که او عزیز جهان گشت و من فقیر شدم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نداشت زخم من از مرهم طبیبان سود</p></div>
<div class="m2"><p>هم از خدنگ تو آخر دوا پذیر شدم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تو آفتابی از آن خوارم از تو یکذره</p></div>
<div class="m2"><p>که من بطالع کم دره حقیر شدم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شدم به تیر ملامت نشانه در عالم</p></div>
<div class="m2"><p>ز بسکه سینه کرده پیش تیر شدم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اسیر عشق تو چون اهلیم مکش زارم</p></div>
<div class="m2"><p>که کشتنی نشدم گر ترا اسیر شدم</p></div></div>