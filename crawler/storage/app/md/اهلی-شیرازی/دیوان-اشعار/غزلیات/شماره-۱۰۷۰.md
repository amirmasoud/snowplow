---
title: >-
    شمارهٔ ۱۰۷۰
---
# شمارهٔ ۱۰۷۰

<div class="b" id="bn1"><div class="m1"><p>ما وصل تو با بوالهوسان دوست نداریم</p></div>
<div class="m2"><p>نوش تو بجوش مگسان دوست نداریم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رفتیم ز گلزار تو از دست رقیبان</p></div>
<div class="m2"><p>بر دامن گل دست خسان دوست نداریم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مردانه گذشتیم ز جان وز همه پیشیم</p></div>
<div class="m2"><p>واماندگی بازپسان دوست نداریم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>غیرت نگذارد که بگیریم کناری</p></div>
<div class="m2"><p>معشوقه هم آغوش کسان دوست نداریم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فریاد مکن اهلی اگر میکشدت هجر</p></div>
<div class="m2"><p>ما منت فریاد رسان دوست نداریم</p></div></div>