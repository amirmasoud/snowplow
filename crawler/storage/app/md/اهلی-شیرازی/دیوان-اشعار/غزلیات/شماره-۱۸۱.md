---
title: >-
    شمارهٔ ۱۸۱
---
# شمارهٔ ۱۸۱

<div class="b" id="bn1"><div class="m1"><p>عشق آتش است و در دل دیوانه در گرفت</p></div>
<div class="m2"><p>برق قبول شمع به پروانه در گرفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از چشم سرخوش تو که مست است بی شراب</p></div>
<div class="m2"><p>صحبت بیک کرشمه مستانه درگرفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اندک مبین سرشک دل گرم عاشقان</p></div>
<div class="m2"><p>کاتش زیک شراره بیک خانه درگرفت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفتم خوش است سوز درون شمع بر فروخت</p></div>
<div class="m2"><p>این حرف آشنا چه به بیگانه درگرفت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اهلی فروغ دل ز خرابات عشق یافت</p></div>
<div class="m2"><p>دیوانه را چراغ به پروانه در گرفت</p></div></div>