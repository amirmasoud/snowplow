---
title: >-
    شمارهٔ ۴۴۹
---
# شمارهٔ ۴۴۹

<div class="b" id="bn1"><div class="m1"><p>بجای آب حیاتم شکر لبی دادند</p></div>
<div class="m2"><p>حیات خضر بهر کس ز مشربی دادند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز بت پرستی ام ای شیخ خود پرست مرنج</p></div>
<div class="m2"><p>مرا چنانکه تویی نیز مذهبی دادند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هدایت است نه کسبی رموز عشق ارنه</p></div>
<div class="m2"><p>هر آنکه هست دو روزش بمکتبی دادند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تو نیز عاشقی ای مدعی ولی فرق است</p></div>
<div class="m2"><p>بسوختند مرا گر ترا تبی دادند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همان چراغ محبت که اهل دل را بود</p></div>
<div class="m2"><p>بدست اهلی بد روز یک شبی دادند</p></div></div>