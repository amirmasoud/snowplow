---
title: >-
    شمارهٔ ۹۱۵
---
# شمارهٔ ۹۱۵

<div class="b" id="bn1"><div class="m1"><p>گر فراغت طلبی همنفس رندان باش</p></div>
<div class="m2"><p>سر و سامان بهل و بیسر و بیسامان باش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بارخ مغبچکان کنج خرابات خوشست</p></div>
<div class="m2"><p>گنج اگر با تو بود خانه بگو ویران باش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هرکه عشقت بخرد هر دو جهان بنده اوست</p></div>
<div class="m2"><p>بنده عشق شو و بر دو جهان سلطان باش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چند حسرت خورد ایشاه جهان درویشی</p></div>
<div class="m2"><p>بطفیل همه یکبار بگو مهمان باش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چشم آلوده دلان لایق دیدار تو نیست</p></div>
<div class="m2"><p>ای بهشتی صفت از مدعیان پنهان باش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شربت وصل تو صد درد مرا درمان است</p></div>
<div class="m2"><p>گو یکی زخم دل از لعل تو بیدرمان باش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نوجوانان همه سرمست می و بوس و کنار</p></div>
<div class="m2"><p>اهلی پیر به این معرکه گوترخان باش</p></div></div>