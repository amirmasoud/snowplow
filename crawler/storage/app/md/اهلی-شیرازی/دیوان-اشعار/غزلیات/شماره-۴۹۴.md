---
title: >-
    شمارهٔ ۴۹۴
---
# شمارهٔ ۴۹۴

<div class="b" id="bn1"><div class="m1"><p>هرکه چون صورت چین دیده بروی تو گشاد</p></div>
<div class="m2"><p>چشم دیگر ز تماشای تو بر هم ننهاد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مگر آن لحظه رقیب تو زمن پوشد چشم</p></div>
<div class="m2"><p>که رود خاک وجود من دلخسته بباد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آنچنان شادم از آنشب که بخوابت دیدم</p></div>
<div class="m2"><p>که نمیآیدم از شادی آن خواب بیاد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یارب آن آهوی مشکین بیابان امید</p></div>
<div class="m2"><p>کس چو مجنون تو گمگشته ببوی تو مباد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر حریفان تو ساقی، بمی از دست شدند</p></div>
<div class="m2"><p>اهلی دلشده ناخورده می از پای افتاد</p></div></div>