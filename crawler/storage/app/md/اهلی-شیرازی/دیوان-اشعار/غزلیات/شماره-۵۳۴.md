---
title: >-
    شمارهٔ ۵۳۴
---
# شمارهٔ ۵۳۴

<div class="b" id="bn1"><div class="m1"><p>چون گل روی تو را آتش می تاب دهد</p></div>
<div class="m2"><p>عارض پر عرقت چشم مرا آب دهد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سوی مسجد مرو ای قبله من ورنه امام</p></div>
<div class="m2"><p>رو بسوی تو کند پشت به محراب دهد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نرگس مست تو ای شوح چه مشرب دارد</p></div>
<div class="m2"><p>که به بیگانه می و زهر با حباب دهد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر چنین سیل دمادم رود از دیده ما</p></div>
<div class="m2"><p>عاقبت گریه ما خانه بسیلاب دهد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اهلی ایمن نشود از تو گرش جام دهی</p></div>
<div class="m2"><p>کاین شراب آن دم آبی است که قصاب دهد</p></div></div>