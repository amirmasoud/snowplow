---
title: >-
    شمارهٔ ۷۵۴
---
# شمارهٔ ۷۵۴

<div class="b" id="bn1"><div class="m1"><p>سرو قدان که دل از عاشق محتاج برند</p></div>
<div class="m2"><p>یوسف از چاه برآرند و بمعراج برند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گلرخانی که برند از دل ما صبر و قرار</p></div>
<div class="m2"><p>جان بی صبر مرا همره خود کاج برند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>صبر و هوش و دل و دین رفت همین حالی ماند</p></div>
<div class="m2"><p>که بیک عشوه مستانه بتاراج برند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دیده کز هجر کمان ابروی خود گشت سپید</p></div>
<div class="m2"><p>کی بود کز پی تیرش سوی آماج برند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مرد منصوبه شطرنج جهان اهلی نیست</p></div>
<div class="m2"><p>کاین حریفان دغل دست ز لیلاج برند</p></div></div>