---
title: >-
    شمارهٔ ۱۳۸۱
---
# شمارهٔ ۱۳۸۱

<div class="b" id="bn1"><div class="m1"><p>چه سر پیش آری و با خود مرا همراز گردانی</p></div>
<div class="m2"><p>بلب چندک رسانی جان و دیگر باز گردانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بخون من چه دشمن را اشارت میکند چشمت</p></div>
<div class="m2"><p>هلاک صد چو من این بس که چشم از ناز گردانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز مستی دوست از دشمن نمیدانی از آن با غیر</p></div>
<div class="m2"><p>سخنگویی و روی از عاشق جانباز گردانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر افسانه عشقت بپایان آورم صد ره</p></div>
<div class="m2"><p>چو گویی یکسخن دیگر همان آغاز گردانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو بلبل از خزان هجر اهلی بسته لب تا کی</p></div>
<div class="m2"><p>بیا ای گل که تا بازش سخن پرداز گردانی</p></div></div>