---
title: >-
    شمارهٔ ۱۱۸
---
# شمارهٔ ۱۱۸

<div class="b" id="bn1"><div class="m1"><p>آن شوخ سوارست و سوی ما گذرش نیست</p></div>
<div class="m2"><p>خورشید بلندست و شب ما سحرش نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا کار دل ما بکجا میرسد آخر</p></div>
<div class="m2"><p>کز غمزه خون ریز جوانان حذرش نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اکنون که به عاشق کشی آن شوخ خبر داد</p></div>
<div class="m2"><p>عاشق چه نشیند مگر از خود خبرش نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بیچاره اسیری که گرفتار بتان شد</p></div>
<div class="m2"><p>جز کشته شدن هیچ دوای دگرش نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر چرخ دهد عرض تحمل زمه و مهر</p></div>
<div class="m2"><p>اهلی سگ یارست بدینها ...رش نیست</p></div></div>