---
title: >-
    شمارهٔ ۴۷۴
---
# شمارهٔ ۴۷۴

<div class="b" id="bn1"><div class="m1"><p>عرش و معراج نه در خورد من زار بود</p></div>
<div class="m2"><p>عرش من کرسی و معراج سردار بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سینه گر زخم تو دوزد سپر طعنه شود</p></div>
<div class="m2"><p>رشته کاین کار کند رشته زنار بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زین چمن هرچه نه چون لاله درو داغ دلیست</p></div>
<div class="m2"><p>گر همه شاخ گل تازه بود خار بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آن زمان بر تو وزد بوی گل باغ بهشت</p></div>
<div class="m2"><p>که گل باغ جهان در نظرت خوار بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مستی و جامه دریدن صفت انسانست</p></div>
<div class="m2"><p>هرکه اینها نکند صورت دیوار بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جان بخواری کشد از سینه اهلی غم عشق</p></div>
<div class="m2"><p>همچو آن رشته که در خاک گرفتار بود</p></div></div>