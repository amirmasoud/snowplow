---
title: >-
    شمارهٔ ۱۱۳۰
---
# شمارهٔ ۱۱۳۰

<div class="b" id="bn1"><div class="m1"><p>ایکه دین و دلم ایثار تو خواهد بودن</p></div>
<div class="m2"><p>یار من شو که خدا یار تو خواهد بودن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیتو جایی که قراری بودم ای خورشید</p></div>
<div class="m2"><p>هم مگر سایه دیوار تو خواهد بودن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آن حلاوت دل من از مگس شهد تو دید</p></div>
<div class="m2"><p>که همه عمر گرفتار تو خواهد بودن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کشتن من که هم از جرم خریداری نیست</p></div>
<div class="m2"><p>عاقبت در سر بازار تو خواهد بودن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اهلی آنروز که در بحر فنا غرقه بود</p></div>
<div class="m2"><p>همچنان تشنه دیدار تو خواهد بودن</p></div></div>