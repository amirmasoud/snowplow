---
title: >-
    شمارهٔ ۵۵۷
---
# شمارهٔ ۵۵۷

<div class="b" id="bn1"><div class="m1"><p>یار برخاست برقص آن قد و قامت نگرید</p></div>
<div class="m2"><p>رستخیز است درین خانه قیامت نگرید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زد علم آتشم از سینه و صد خانه بسوخت</p></div>
<div class="m2"><p>علم داد ببینید و علامت نگرید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگر این گلشن حسن است چه حاجت گل و سرو</p></div>
<div class="m2"><p>رخ ببینید خدا را قد و قامت نگرید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نه چنان نامه سیاهم ز خطش کرد رقم</p></div>
<div class="m2"><p>که ز لوح دل من حرف سلامت نگرید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آتشی در جگر افکنده ام از عشق چو شمع</p></div>
<div class="m2"><p>داغها بر رخم از اشک ندامت نگرید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کوهکن بر دل او گرد ملامت بار است</p></div>
<div class="m2"><p>گرد من کوه غم از سنگ ملامت نگرید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نیست در میکده اهلی نفسی بی می و جام</p></div>
<div class="m2"><p>کرم پیر به بینید و کرامت نگرید</p></div></div>