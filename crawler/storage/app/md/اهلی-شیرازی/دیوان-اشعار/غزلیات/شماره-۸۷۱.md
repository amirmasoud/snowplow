---
title: >-
    شمارهٔ ۸۷۱
---
# شمارهٔ ۸۷۱

<div class="b" id="bn1"><div class="m1"><p>آن پریرو هرکه خواهد دست دل در گردنش</p></div>
<div class="m2"><p>تا ندارد دست از عالم نگیرد دامنش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>باشد از گمگشتگی یابی چو مجنون ره بدوست</p></div>
<div class="m2"><p>ورنه آن آهوی مشکین کس نداند مسکنش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>برق حسن او نخواهد از درخشیدن نشست</p></div>
<div class="m2"><p>تا نخیزد عاشق بیچاره دود از خرمنش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کشته آن نرگس مستم که در هر گوشه یی</p></div>
<div class="m2"><p>صد چو من افتاده شد از غمزه صیدافکنش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اهلی دیوانه را گر همچنین سوزد جگر</p></div>
<div class="m2"><p>عاقبت خاکستری یابی به کنج گلخنش</p></div></div>