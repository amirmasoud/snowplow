---
title: >-
    شمارهٔ ۹۸۴
---
# شمارهٔ ۹۸۴

<div class="b" id="bn1"><div class="m1"><p>می ده که بی غبار غمی از جهان روم</p></div>
<div class="m2"><p>آلوده دل مباد کزین خاکدان روم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خلق جهان بگریه من خنده گر زنند</p></div>
<div class="m2"><p>طوفان شود ز گریه گهی کز جهان روم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا زنده ام چو شمع ندانند قدر من</p></div>
<div class="m2"><p>قدر من آنگه است که من از میان روم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا دل ز من ربود سگ آستان او</p></div>
<div class="m2"><p>هرگز دلم نداد کز آن آستان روم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آن آفتاب حسن کجا سر نهد بمن</p></div>
<div class="m2"><p>گر زیر پا نهم سر و بر آسمان روم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گشتم غبار ره که به شبگیر تا صبا</p></div>
<div class="m2"><p>شبها بکوی دوست ز دشمن نهان روم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اهلی اگر بباغ جنان ساقیم نه اوست</p></div>
<div class="m2"><p>دوزخ از آن به ام که بباغ جنان روم</p></div></div>