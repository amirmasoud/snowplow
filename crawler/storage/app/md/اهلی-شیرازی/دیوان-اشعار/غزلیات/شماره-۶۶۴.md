---
title: >-
    شمارهٔ ۶۶۴
---
# شمارهٔ ۶۶۴

<div class="b" id="bn1"><div class="m1"><p>گرنه ابرو ترش آن غمزه خونریز کند</p></div>
<div class="m2"><p>بر لب لعل تو دندان همه کس تیز کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا فلک دیدترا ز یوسف بگذاشت</p></div>
<div class="m2"><p>باغبان تربیت گلبن نو خیز کند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نرگس مست نو گر رهزن مردم نشود</p></div>
<div class="m2"><p>بر من گوشه نشین فتنه که انگیز کند؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سخن تلخ به بیمار غمت لایق نیست</p></div>
<div class="m2"><p>مگرش خنده شیرین شکرآمیز کند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اهلی از پرتو زنار نبیند کافر</p></div>
<div class="m2"><p>آنچه بادین من آن زلف دلاویز کند</p></div></div>