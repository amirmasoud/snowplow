---
title: >-
    شمارهٔ ۹۶۶
---
# شمارهٔ ۹۶۶

<div class="b" id="bn1"><div class="m1"><p>آنچنانم ز فراقت که ندانم چکنم</p></div>
<div class="m2"><p>جان دهم یا بامید تو بمانم چکنم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بی لبت بر دل خود خنجر چون آب زدم</p></div>
<div class="m2"><p>گر بدین آتش دل را ننشانم چکنم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رخ چرا تا بد از افغان من خسته طبیب</p></div>
<div class="m2"><p>گر باو درد دل خود نرسان چکنم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شربت صبر اگر چاره بیمار دل است</p></div>
<div class="m2"><p>من که صبر از لب شیرین نتوانم چکنم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>منم و جان خرابم ز غم او اهلی</p></div>
<div class="m2"><p>وان هم اندر قدمش گر نفشانم چکنم</p></div></div>