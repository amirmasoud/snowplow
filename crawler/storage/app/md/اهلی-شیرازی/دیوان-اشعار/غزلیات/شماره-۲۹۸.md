---
title: >-
    شمارهٔ ۲۹۸
---
# شمارهٔ ۲۹۸

<div class="b" id="bn1"><div class="m1"><p>پیش سرمستان عشقت گلشن و گلخن یکیست</p></div>
<div class="m2"><p>دار منصور و درخت وادی ایمن یکیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حسن او در دل زجام دیده صد عکس افکند</p></div>
<div class="m2"><p>در درون خانه صد خورشید و در روزن یکیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خوبرویان همچو پروین اند و او ماه تمام</p></div>
<div class="m2"><p>خوشه چین بسیار می بینم ولی خرمن یکیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در گلستان جهان جز وی نمی بینم گلی</p></div>
<div class="m2"><p>گر سراسر گل شود عالم بچشم من یکیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دیده یعقوب روشن گشت و دشمن کور شد</p></div>
<div class="m2"><p>در محل فرق است ورنه بوی پیراهن یکیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جنگ و بحث خانقه بهم دشمن کند</p></div>
<div class="m2"><p>ای خوشا میخانه کانجا دوست با دشمن یکیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر نظر بر غیر دارم غیر او مقصود نیست</p></div>
<div class="m2"><p>دیده در صورت دو شد اهلی ولی دیدن یکیست</p></div></div>