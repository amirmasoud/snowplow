---
title: >-
    شمارهٔ ۵۳۵
---
# شمارهٔ ۵۳۵

<div class="b" id="bn1"><div class="m1"><p>مرا حیات ابد از لبت هوس باشد</p></div>
<div class="m2"><p>که می گر آب حیات است یک نفس باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بزیر تیغ تو ای شوخ در صف عشاق</p></div>
<div class="m2"><p>کسی که پیش نیامد همیشه پس باشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تو آفتابی و با ذره کی شوی نزدیک</p></div>
<div class="m2"><p>مرا ز دور همین دیدن تو بس باشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دلم به سینه صد چاک بی تو محزون است</p></div>
<div class="m2"><p>حزین بود دل مرغی که در قفس باشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ببخش ساقی اگر جرعه یی دهد دستت</p></div>
<div class="m2"><p>که غایت کرم است آنچه دسترس باشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بغیر پیر مغان از که التماس کنم</p></div>
<div class="m2"><p>که بی طلب ندهد آنچه ملتمس باشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بکوی دوست ز طوفان گریه اهلی</p></div>
<div class="m2"><p>کسی نماند و گر ماند تا چه کس باشد</p></div></div>