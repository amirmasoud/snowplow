---
title: >-
    شمارهٔ ۱۰۶۲
---
# شمارهٔ ۱۰۶۲

<div class="b" id="bn1"><div class="m1"><p>تا بکی توبه کنیم از می و دردم شکنیم</p></div>
<div class="m2"><p>توبه کردیم که از توبه دگر دم نزنیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چشم صاحب نظرانست بر آنگونه چشم</p></div>
<div class="m2"><p>ما هم از گوشه کناری نظری می فکنیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نیست ما را خبر از گفت و شنید دو جهان</p></div>
<div class="m2"><p>بسکه از شوق بتان با دل خود در سخنیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زاهدا، قبله پرستی تو و ما یار پرست</p></div>
<div class="m2"><p>تو بدین عقل مسلمانی و ما برهمنیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اهلی از ما سفر کعبه بعیدست بسی</p></div>
<div class="m2"><p>زانکه در میکده افتاده حب الوطنیم</p></div></div>