---
title: >-
    شمارهٔ ۱۳۴۰
---
# شمارهٔ ۱۳۴۰

<div class="b" id="bn1"><div class="m1"><p>اگرچه چشم منی همنشین اغیاری</p></div>
<div class="m2"><p>چو نور دیده گلی در میان صد خاری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نگویم آنکه خریدار یوسفم حاشا</p></div>
<div class="m2"><p>که خودفروش زند لاف این خریداری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هوای صحبت خورشید کم کن ایشبنم</p></div>
<div class="m2"><p>که تاب یکنظر از روی او نمی آری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر بصدق روی همچو کوهکن در عشق</p></div>
<div class="m2"><p>نه بیستون که فلک را ز پیش برداری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دلا، بسوز که ننشیند آتشت چونشمع</p></div>
<div class="m2"><p>بیکدو قطره که گاهی ز دیده میباری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تو گر بدیدن خوبان نمیروی از جا</p></div>
<div class="m2"><p>نه آدمی بحقیقت که نقش دیواری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بوصل دوست رسی آنچنانکه دل خواهد</p></div>
<div class="m2"><p>گر اختیار چو اهلی ز دست بگذاری</p></div></div>