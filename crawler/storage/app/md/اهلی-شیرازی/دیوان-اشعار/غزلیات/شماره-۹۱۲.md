---
title: >-
    شمارهٔ ۹۱۲
---
# شمارهٔ ۹۱۲

<div class="b" id="bn1"><div class="m1"><p>نبود کسی که نبود بلب تو اشتیاقش</p></div>
<div class="m2"><p>مگر آدمی نباشد؟ که نباشد این مذاقش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می صاف و لعل نوشین که نخواهد ای پری وش</p></div>
<div class="m2"><p>مگر آن حریف مسکین که نیفتد اتفاقش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز فراق خاک گشتم ببر ایصبا غبارم</p></div>
<div class="m2"><p>سوی آن مسیح جانها که هلاکم از فراقش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>منم آن شکسته خاطر که ز بهر آن مه نو</p></div>
<div class="m2"><p>همه زود در تفحص همه شب کنم سراغش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دو دل است بخت با من بگذار کاین ستاره</p></div>
<div class="m2"><p>به شرار دل بسوزد که بجانم از نفاقش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز شراب توبه اهلی نکنی که دختر رز</p></div>
<div class="m2"><p>بکسی حلال باشد که نمیدهد طلاقش</p></div></div>