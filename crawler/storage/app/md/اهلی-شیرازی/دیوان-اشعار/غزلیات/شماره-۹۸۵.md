---
title: >-
    شمارهٔ ۹۸۵
---
# شمارهٔ ۹۸۵

<div class="b" id="bn1"><div class="m1"><p>یاد من کردی من از بخت این زیادت یافتم</p></div>
<div class="m2"><p>تا چه نیکی کرده بودم کاین سعادت یافتم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یکسخن گفتی و صد امید پیدا شد مرا</p></div>
<div class="m2"><p>کز نسیم این سخن بوی ارادت یافتم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ناله من کرد گویا این که پرسیدی زمن</p></div>
<div class="m2"><p>کز تو این پرسش خلاف رسم و عادت یافتم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر سجودت میکنم محراب ابرو خم مکن</p></div>
<div class="m2"><p>زانکه من این بت پرستی از عبادت یافتم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر چه اهلی آنلب شیرین چو فرهادم بکشت</p></div>
<div class="m2"><p>تلخیی بردم ولی شهد شهادت یافتم</p></div></div>