---
title: >-
    شمارهٔ ۱۳۰۵
---
# شمارهٔ ۱۳۰۵

<div class="b" id="bn1"><div class="m1"><p>چند سازم که شوم خاک و تو بر من گذری</p></div>
<div class="m2"><p>سوختم تا بمن سوخته خرمن گذری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دست افتاده نخواهی که بدامن رسدت</p></div>
<div class="m2"><p>نیست از شوخی اگر برزده دامن گذری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون خیال تو کنم خون دل از دیده چکد</p></div>
<div class="m2"><p>که بصد نشتر مژگان بدل من گذری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پای بوست چه بود آرزوی مردم چشم</p></div>
<div class="m2"><p>چشم داریم که بر دیده روشن گذری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا نسوزی همه تن شمع صفت اهلی را</p></div>
<div class="m2"><p>از سر او عجبست ارتو بکشتن گذری</p></div></div>