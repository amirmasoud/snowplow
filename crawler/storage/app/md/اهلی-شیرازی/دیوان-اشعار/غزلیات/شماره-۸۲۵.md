---
title: >-
    شمارهٔ ۸۲۵
---
# شمارهٔ ۸۲۵

<div class="b" id="bn1"><div class="m1"><p>ای بچشم جان من حسن تو شورانگیزتر</p></div>
<div class="m2"><p>آفتابت در دل پر آتش ما تیزتر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شد بهار عمرم از پیری خزان در عشق تو</p></div>
<div class="m2"><p>همچنان بینم پر از شاخ گل نو خیزتر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عالمی کشتی و خواهی زارتر کشتن مرا</p></div>
<div class="m2"><p>زانکه می بینم ببخت خود ترا خونریزتر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر کجا ای شاخ گل باشی ببویت پی برم</p></div>
<div class="m2"><p>بسکه می یابم ز گل بوی تو مشک آمیزتر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر کرا در دوزخ هجران چو اهلی سوختی</p></div>
<div class="m2"><p>خاک او شد از نسیم خلد عنبر بیزتر</p></div></div>