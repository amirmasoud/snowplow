---
title: >-
    شمارهٔ ۱۱۳۸
---
# شمارهٔ ۱۱۳۸

<div class="b" id="bn1"><div class="m1"><p>دو ضعیفیم من و سایه در آنراه شدن</p></div>
<div class="m2"><p>گه منم باز پس از سایه و گه سایه ز من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ایکه چون سایه مرا مهر تو برداشت ز خاک</p></div>
<div class="m2"><p>چونکه بر داشتیم باز بخاکم مفکن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رچه خواهی بکن آزار دل زار مکن</p></div>
<div class="m2"><p>جام جم گر شکنی غم نبود دل مشکن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>باغبان جامه درد همچو گل از دست رخت</p></div>
<div class="m2"><p>که بدور تو نگه کس نکند سرو و سمن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بقلم هم نکشد مثل تو صورتگر چین</p></div>
<div class="m2"><p>سرو نازی که بود لاله رخ و غنچه دهن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آفتابا بچمن گر گذری سرو صفت</p></div>
<div class="m2"><p>سجده قد تو چون سایه کند سرو چمن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اهلی از داغ بتان شرط وفا سوختن است</p></div>
<div class="m2"><p>یا بسوز از غم او یا ز وفا لاف مزن</p></div></div>