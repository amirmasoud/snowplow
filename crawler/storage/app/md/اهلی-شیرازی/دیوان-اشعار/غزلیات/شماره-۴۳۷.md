---
title: >-
    شمارهٔ ۴۳۷
---
# شمارهٔ ۴۳۷

<div class="b" id="bn1"><div class="m1"><p>گر سگان تو انیس من محزون شده اند</p></div>
<div class="m2"><p>آهوانند که همصحبت مجنون شده اند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ایکه در حلقه بزم طربی یاد آور</p></div>
<div class="m2"><p>زان اسیران که درین دایره بیرون شده اند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خاک راهند ز جور تو بتان با همه ناز</p></div>
<div class="m2"><p>نازنینان بنگر کز ستمت چون شده اند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آفتابی تو و عیسی صفتان ذره تو</p></div>
<div class="m2"><p>خاک راهند اگر بر سر گردون شده اند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تو خطان با همه جان بخشی لعل لب خوش</p></div>
<div class="m2"><p>فتنه آن خط سبز و لب میگون شده اند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دل پریشانی عشاق چو اهلی ز لب است</p></div>
<div class="m2"><p>دردمندان تو آشفته نه اکنون شده اند</p></div></div>