---
title: >-
    شمارهٔ ۱۰۹۵
---
# شمارهٔ ۱۰۹۵

<div class="b" id="bn1"><div class="m1"><p>ساقی بیا که دست ارادت بهم دهیم</p></div>
<div class="m2"><p>باشد که دست محنت ایام خم دهیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گنج دل از خرابه می در کف آوریم</p></div>
<div class="m2"><p>وین خاک تیره باز بباد عدم دهیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ما را کهروی یار نماید ز جام دل</p></div>
<div class="m2"><p>تا چند جان بهر زه پی جام جم دهیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>روزیکه شب کنیم بیوسف رخی چو تو</p></div>
<div class="m2"><p>گر جان دهیم قیمت یکلحظه کم دهیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اهلی سیه کنیم جهانی چو روز خود</p></div>
<div class="m2"><p>گر شرح دود دل بزبان قلم دهیم</p></div></div>