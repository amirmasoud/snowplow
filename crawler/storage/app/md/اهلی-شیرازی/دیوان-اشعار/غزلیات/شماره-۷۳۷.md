---
title: >-
    شمارهٔ ۷۳۷
---
# شمارهٔ ۷۳۷

<div class="b" id="bn1"><div class="m1"><p>گرمی کوثر بدان لبهای خندان داده اند</p></div>
<div class="m2"><p>جرعه دردی بما هم دردمندان داده اند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آنهمه خوبی که یوسف داشت در مصر جمال</p></div>
<div class="m2"><p>از ملاحت دلبر مارا دو چندان داده اند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شاخ گل میافتد از رشک سهی قدان بخاک</p></div>
<div class="m2"><p>کاینهمه شوخی باین بالا بلندان داده اند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پیش آن مژگان ساحر سامری گوساله است</p></div>
<div class="m2"><p>بازی صد سامری آن چشم بندان داده اند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آب چشم و آتش آ]م نگیرد در دلش</p></div>
<div class="m2"><p>چون کنم کاو را دل سختی چو سندان داده اند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ازغم هجران بمردن رست جان و خرم است</p></div>
<div class="m2"><p>چون کسی کاو را خلاص از بند و زندان داده اند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>رفت اهلی از جهان چونشمع خندان لب برون</p></div>
<div class="m2"><p>جان بجانان عاشقان سرمست و خندان داده اند</p></div></div>