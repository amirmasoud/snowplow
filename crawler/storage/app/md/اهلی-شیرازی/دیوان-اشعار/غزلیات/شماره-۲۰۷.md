---
title: >-
    شمارهٔ ۲۰۷
---
# شمارهٔ ۲۰۷

<div class="b" id="bn1"><div class="m1"><p>یار اگر میکشدت دل بنه ایدل گله چیست؟</p></div>
<div class="m2"><p>غیر تسلیم و رضا چاره درین مسیله چیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خرد سالی که هنوز آبله نشناخته است</p></div>
<div class="m2"><p>او چه داند که درون دل ما آبله چیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نقد جانم همه در قافله اهل وفاست</p></div>
<div class="m2"><p>رهزن عقل چه داند که در این قافله چیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پیش مرغان چمن تخت سلیمان با دست</p></div>
<div class="m2"><p>نازش از افسر زر هدهد کم حوصله چیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همرهان در حرم کعبه مقصود شدند</p></div>
<div class="m2"><p>ماندن اهلی بیچاره در این مرحله چیست</p></div></div>