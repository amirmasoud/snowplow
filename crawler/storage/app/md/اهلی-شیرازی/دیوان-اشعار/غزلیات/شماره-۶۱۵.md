---
title: >-
    شمارهٔ ۶۱۵
---
# شمارهٔ ۶۱۵

<div class="b" id="bn1"><div class="m1"><p>هر گرد بلایی که خدا خواسته باشد</p></div>
<div class="m2"><p>چون بنگرم از کوی تو برخاسته باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مجلس بتو نازد که تو آرایش بزمی</p></div>
<div class="m2"><p>هر جا که تو باشی بتو آراسته باشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یکروز اگر روی تو خورشید به بیند</p></div>
<div class="m2"><p>روزی دگرش چهره چو مه کاسته باشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با اطلس شاهی نتوانیم که رقصیم</p></div>
<div class="m2"><p>ما را که کهن خرقه پیراسته باشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اهلی نه بسعی است مراد از دل معشوق</p></div>
<div class="m2"><p>باشد که مراد تو خدا خواسته باشد</p></div></div>