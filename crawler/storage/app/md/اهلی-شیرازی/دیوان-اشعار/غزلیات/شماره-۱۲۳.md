---
title: >-
    شمارهٔ ۱۲۳
---
# شمارهٔ ۱۲۳

<div class="b" id="bn1"><div class="m1"><p>ما گرچه گداییم وفا در خور ما نیست</p></div>
<div class="m2"><p>حیف است که یاری چو ترا هیچ وفا نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صاف می عشرت همه را داد وصالت</p></div>
<div class="m2"><p>چون دور من آمد بجز از درد جفا نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هرچند به خشمی مکشم تیر خود از دل</p></div>
<div class="m2"><p>بگذار که آزار دل خسته روا نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عمرم همه در تیرگی هجر به سر رفت</p></div>
<div class="m2"><p>داد از شب هجر تو مگر روز جزا نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ذرات جهان مهر رخ خوب تو دارند</p></div>
<div class="m2"><p>این پرتو خورشید جهان تاب کجا نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تنها نه ترا نیست سر کشته محنت</p></div>
<div class="m2"><p>فتراک ترا هم سر این بی سر و پا نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر سگ که درآمد به سر کوی تو جا کرد</p></div>
<div class="m2"><p>اهلی است که تورا به سر کوی تو جانیست</p></div></div>