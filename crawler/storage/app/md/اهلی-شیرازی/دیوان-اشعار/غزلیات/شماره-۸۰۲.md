---
title: >-
    شمارهٔ ۸۰۲
---
# شمارهٔ ۸۰۲

<div class="b" id="bn1"><div class="m1"><p>اگر آن پری بعاشق نظر از وصال دارد</p></div>
<div class="m2"><p>ز کرشمه اش که داند که چه در خیال دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل تیره بخت مارا چه سعادتی ازین به</p></div>
<div class="m2"><p>که ز ما برید و اکنون بتو اتصال دارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مرو ای پری ببستان دل گل بخون مگردان</p></div>
<div class="m2"><p>که بسینه خارخاری گل از آن جمال دارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>غم هجر اگر رساند همه را بحال مردن</p></div>
<div class="m2"><p>تو کجا ز ناز پرسی که کسی چه حال دارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به کسی نمی نماید خضر آب زندگانی</p></div>
<div class="m2"><p>ز لب تو آب حیوان مگر انفعال دارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بزمین رقیب ترسم که فرو رود چو قارون</p></div>
<div class="m2"><p>که کمال سر گرانی ز غرور مال دارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به سگش چگونه اهلی ز کمال خود زند دم</p></div>
<div class="m2"><p>که به نسبت سگ او صفت کمال دارد</p></div></div>