---
title: >-
    شمارهٔ ۱۱۴
---
# شمارهٔ ۱۱۴

<div class="b" id="bn1"><div class="m1"><p>من سری دارم که بر خاک ره از جولان اوست</p></div>
<div class="m2"><p>هرکه بردارد زخاک ره سر من زان اوست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>او که خواهد در خم چوگان سرماهمچو گوی</p></div>
<div class="m2"><p>گوییا کاینک سرما و سر میدان اوست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر بخون غلطان نشد زان زلف چون چوگان دلم</p></div>
<div class="m2"><p>این گنه از گو نبود از جانب چوگان اوست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پیش آهوی حرم صاحبدلان قربان شوند</p></div>
<div class="m2"><p>من سگ یارم که آهوی حرم قربان اوست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در هوا هر ذره خاکی مردم چشمی بود</p></div>
<div class="m2"><p>بسکه چشم عاشقان خاک ره از جولان اوست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دامن اهلی که چاک از عشق شد چون دوزیش</p></div>
<div class="m2"><p>تا بدامان قیامت چاک در دامان اوست</p></div></div>