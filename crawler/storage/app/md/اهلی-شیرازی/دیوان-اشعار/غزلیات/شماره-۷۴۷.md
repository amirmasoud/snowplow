---
title: >-
    شمارهٔ ۷۴۷
---
# شمارهٔ ۷۴۷

<div class="b" id="bn1"><div class="m1"><p>سرو من تا چو مه از خانه زین گشت بلند</p></div>
<div class="m2"><p>آفتابی دگر از روی زمین گشت بلند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>برحذر باش دلازان بت خونخواره که باز</p></div>
<div class="m2"><p>رنگش افروخت می و چین جبین گشت بلند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عاشق آنست که پروا نکند بد نامی</p></div>
<div class="m2"><p>در جهان نام زلیخا بهمین گشت بلند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>منم آن صید که تا چشم فکندم به بتان</p></div>
<div class="m2"><p>هر طرف دست و کمانی ز کمین گشت بلند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اهلی از دار فنا سرنکشی چون منصور</p></div>
<div class="m2"><p>که مقام همه در عشق ازین گشت بلند</p></div></div>