---
title: >-
    شمارهٔ ۱۱۵۶
---
# شمارهٔ ۱۱۵۶

<div class="b" id="bn1"><div class="m1"><p>اکنون که تنها دیدمت لطف ارنه آزاری بکن</p></div>
<div class="m2"><p>سنگی بزن تلخی بگو تیغی بکش کاری بکن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گیرم نداری میل من ایمردم چشم کسی</p></div>
<div class="m2"><p>از گوشه چشمی بما نظاره یی باری بکن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای یوسف جان میخرد خلقی بجان وصل ترا</p></div>
<div class="m2"><p>رسم گرانجانی بهل میل خریداری بکن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مردیم دور از روی تو در خانه مانی تا به کی</p></div>
<div class="m2"><p>بیرون خرام آخر گهی گلگشت بازاری بکن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا کی طبیب عاشقان غافل ز حالت بگذرد</p></div>
<div class="m2"><p>اهلی بکش آهی ز دل یا ناله زاری بکن</p></div></div>