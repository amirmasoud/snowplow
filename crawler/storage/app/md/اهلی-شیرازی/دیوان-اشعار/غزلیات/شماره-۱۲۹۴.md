---
title: >-
    شمارهٔ ۱۲۹۴
---
# شمارهٔ ۱۲۹۴

<div class="b" id="bn1"><div class="m1"><p>تا کی ای رشگ پری عالمی از غم بکشی</p></div>
<div class="m2"><p>در غم آن من دل خسته بماتم بکشی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مردم از دست تو تا چند ترا جور بود</p></div>
<div class="m2"><p>نه که خوبی بتو دادند که آدم بکشی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر بعیسی نفسی جان بدهد باز لبت</p></div>
<div class="m2"><p>جان من خلق جهان را بتو یکدم بکشی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چونهمه عالم از انفاس تو جان یافته اند</p></div>
<div class="m2"><p>نیست کس را سخنی گر همه عالم بکشی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اهلی سوخته چونشمع چه کردست که تو</p></div>
<div class="m2"><p>هم بسوزی ز غم خویشتن هم بکشی</p></div></div>