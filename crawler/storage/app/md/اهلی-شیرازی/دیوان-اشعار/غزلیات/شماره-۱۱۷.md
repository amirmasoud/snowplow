---
title: >-
    شمارهٔ ۱۱۷
---
# شمارهٔ ۱۱۷

<div class="b" id="bn1"><div class="m1"><p>در بت پرستی قبله ام چون آن بت روحانی است</p></div>
<div class="m2"><p>در سجده شکر حقم کاین دولتم ارزانی است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دلدار اگر جوری کند از غایت یاری بود</p></div>
<div class="m2"><p>جور بتان بر عاشقان دلداری پنهانی است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من بی رخ روحانیش در ظلمت غم چون زیم</p></div>
<div class="m2"><p>جایی که روز روشنم بی او شب ظلمانی است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>لب بسته شد هرکس که او دانا شد از علم نظر</p></div>
<div class="m2"><p>غوغای بحث مدعی از غایت نادانی است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از ملک عالم در گذر آباد کن دل را به عشق</p></div>
<div class="m2"><p>کاین ملکش از روز ازل بنیاد بر ویرانی است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اهلی براه وصف او تا می توانی پامنه</p></div>
<div class="m2"><p>صد سال اگر این ره روی آخر قدم ویرانی است</p></div></div>