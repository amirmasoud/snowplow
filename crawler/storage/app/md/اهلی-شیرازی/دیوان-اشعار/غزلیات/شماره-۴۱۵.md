---
title: >-
    شمارهٔ ۴۱۵
---
# شمارهٔ ۴۱۵

<div class="b" id="bn1"><div class="m1"><p>آن آفتاب حسن سحر میل باده کرد</p></div>
<div class="m2"><p>صبح سعادتم در دولت گشاده کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتم سگ توام نظر از من بخشم تافت</p></div>
<div class="m2"><p>خشمی که صدهزار محبت زیاده کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفت آدمی نیی سگ من چون شوی بین</p></div>
<div class="m2"><p>این مردمی که آن صنم حورزاده کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در گلشن امید از آن غنچه لب دمید</p></div>
<div class="m2"><p>بویی که خاور گل همه را مست باده کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفتم که ساغری به کفم نه بناز گفت</p></div>
<div class="m2"><p>بیهوده کی توان طلب نا نهاده کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سیلاب شوق آمد و نقش خیال فهم</p></div>
<div class="m2"><p>شست از دل آنچنان که مرا لوح ساده کرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اهلی که همتش بفلک سر نمی نهاد</p></div>
<div class="m2"><p>سیلی عشق عاقبتش سر فتاده کرد</p></div></div>