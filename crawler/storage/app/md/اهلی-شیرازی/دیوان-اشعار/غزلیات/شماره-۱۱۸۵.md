---
title: >-
    شمارهٔ ۱۱۸۵
---
# شمارهٔ ۱۱۸۵

<div class="b" id="bn1"><div class="m1"><p>آن جوان عاجز کش و من ناتوانی اینچنین</p></div>
<div class="m2"><p>چون کشم پیرانه سر جور جوانی اینچنین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گه کند خندان چو شمع از وصل و گه گریان ز هجر</p></div>
<div class="m2"><p>تا زمانی آنچنان سوزم زمانی اینچنین</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ابرویش با من بقصد جان کمان زه کرده است</p></div>
<div class="m2"><p>چون کشد بازوی بی زورم کمانی اینچنین</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جان بیمارم که دایم بسته درد و غم است</p></div>
<div class="m2"><p>گر رود گور و چه میخواهم ز جانی اینچنین</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آنمیان کز ناز کی کس را نگنجد در خیال</p></div>
<div class="m2"><p>چون کشد بار کمر نازک میانی اینچنین</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تلخ گوی و شکرش زیر زبان چون نیشکر</p></div>
<div class="m2"><p>جان فدای تلخی شیرین زبانی اینچنین</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اهلی آنسرو روان جا در دل من کرده است</p></div>
<div class="m2"><p>کی رود از دل برون سرو روانی اینچنین</p></div></div>