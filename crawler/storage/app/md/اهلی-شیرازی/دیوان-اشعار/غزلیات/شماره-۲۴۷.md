---
title: >-
    شمارهٔ ۲۴۷
---
# شمارهٔ ۲۴۷

<div class="b" id="bn1"><div class="m1"><p>ذره چون خورشید گردد طالع و لامع خوش است</p></div>
<div class="m2"><p>آفتاب بخت من لامع نشد طالع خوش است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>میزند تیغ آفتاب من که میرم پیش او</p></div>
<div class="m2"><p>گر شفاعتخواه نبود در میان مانع خوش است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>میرسد گاهی بسمع محرمان حالم ولی</p></div>
<div class="m2"><p>حال مجنون را اگر لیلی بود سامع خوش است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سایه ام بر سر سگش همچون همامی افکند</p></div>
<div class="m2"><p>گر بمشتی استخوان من شود قانع خوش است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ناخوشیهایی که ما از ظلمت هجران کشیم</p></div>
<div class="m2"><p>گاه گاهی برق وصلی گر شود لامع خوش است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دوش دیدم اهلی آنمه همنشین در واقعه</p></div>
<div class="m2"><p>گر بخواب این قصه هم واقع شود واقع خوش است</p></div></div>