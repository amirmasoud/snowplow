---
title: >-
    شمارهٔ ۱۳۰۶
---
# شمارهٔ ۱۳۰۶

<div class="b" id="bn1"><div class="m1"><p>ای اشک جگر سوز که در چشم پر آیی</p></div>
<div class="m2"><p>بنشین که نمک ریزه دلهای کبابی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یارب که کف پای تو بر دیده که مالد</p></div>
<div class="m2"><p>شبها که تو افتاده ز می مست و خرابی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دریاب کزین همنفسان زود برنجی</p></div>
<div class="m2"><p>و آنگاه چو من سوخته جویی و نیابی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پروای که داری تو که با حشمت شاهی</p></div>
<div class="m2"><p>سرخوش ز می حسنی و مستان ز شرابی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بیداری اهلی بامیدی است که یکشب</p></div>
<div class="m2"><p>بوسد کف پای تو نه بیند که بخوابی</p></div></div>