---
title: >-
    شمارهٔ ۷۵۵
---
# شمارهٔ ۷۵۵

<div class="b" id="bn1"><div class="m1"><p>نفی خوبان کردم از خاطر که بار خاطرند</p></div>
<div class="m2"><p>تا زخود غایب شدم دیدم که در دل حاضرند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از فریب مردم چشم بتان ایمن مباش</p></div>
<div class="m2"><p>کاین سیه دل مردمان هم کافر و هم ساحرند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جز سر تسلیم در پای بتان نتوان نهاد</p></div>
<div class="m2"><p>چون بکشتن حاکم و برزنده کردن قادرند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>صبر بی شیرین لبان تلخ است اما چاره نیست</p></div>
<div class="m2"><p>دردمندان بلا خواهی نخواهی صابرند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پیش ما نادیده زاهد وصف حورعین مکن</p></div>
<div class="m2"><p>گرچه پنهانند از چشم تو بر ما ظاهرند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اهلی از هجر پری رویان نشاید ناله کرد</p></div>
<div class="m2"><p>هر کجا هستند بر حال دل ما ناظرند</p></div></div>