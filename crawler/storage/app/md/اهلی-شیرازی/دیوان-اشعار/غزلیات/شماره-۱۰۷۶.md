---
title: >-
    شمارهٔ ۱۰۷۶
---
# شمارهٔ ۱۰۷۶

<div class="b" id="bn1"><div class="m1"><p>تا نیست می از خود خبرم نیست که هستم</p></div>
<div class="m2"><p>من هوش ندارم مگر آن لحظه که مستم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز نار مرا کافر و مومن همه دانند</p></div>
<div class="m2"><p>پنهان چکنم عاشق معشوق پرستم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در کوی تو آنم که چو گرد از سر عالم</p></div>
<div class="m2"><p>برخاستم و بر سر راه تو نشستم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون خویش پرستی بتر از باده پرستی است</p></div>
<div class="m2"><p>المنه لله که بت خویش شکستم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا در سر زلفش نزدم دست چو اهلی</p></div>
<div class="m2"><p>سررشته مقصود نیفتاد بدستم</p></div></div>