---
title: >-
    شمارهٔ ۲۹۵
---
# شمارهٔ ۲۹۵

<div class="b" id="bn1"><div class="m1"><p>هرکه برخاست ز سودای تو بر جا ننشست</p></div>
<div class="m2"><p>تا نیفکند بپای تو سر از پا ننشست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عرصه کوی تو صحرای قیامت باشد</p></div>
<div class="m2"><p>زانکه هرگز ز سر کوی تو غوغا ننشست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گرچه عمری بنشستم بسر راه امید</p></div>
<div class="m2"><p>جز سگ کوی تو کس با من شیدا ننشست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دل مجنون نه بدیوانگی از خلق رمید</p></div>
<div class="m2"><p>عقل او بود که با مردم دانا ننشست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>داد طوفان سرشکم همه عالم بر باد</p></div>
<div class="m2"><p>در تنور دل من آتش سودا ننشست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گرد راهت به سر سرو سهی باد نشاند</p></div>
<div class="m2"><p>خاک پای تو کجا رفت که بالا ننشست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پهلوی خار کسی سرو سهی را ننشاند</p></div>
<div class="m2"><p>اهلی از یار مشو رنجه که با ما ننشست</p></div></div>