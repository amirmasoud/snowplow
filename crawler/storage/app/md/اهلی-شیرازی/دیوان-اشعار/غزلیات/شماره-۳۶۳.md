---
title: >-
    شمارهٔ ۳۶۳
---
# شمارهٔ ۳۶۳

<div class="b" id="bn1"><div class="m1"><p>باغبان، آنسرو اگر برطرف جو خواهد نشست</p></div>
<div class="m2"><p>آتش مغروری گلها فرو خواهد نشست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از رخ ساقی گل عیشی بچین کاینک ز باغ</p></div>
<div class="m2"><p>گل بخواهد رفت و مرغ از گفتگو خواهد نشست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از جهان برخاستن سهل است بر مجنون عشق</p></div>
<div class="m2"><p>گر غزالی چون تو یکدم پیش او خواهد نشست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر چو شمع آتش زنی ای آرزوی جان مرا</p></div>
<div class="m2"><p>کی ز جانم آتش این آرزو خواهد نشست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>میدمد بوی محبت از دلم کز آب چشم</p></div>
<div class="m2"><p>در کنارم چون تو سروی مشکبو خواهد نشست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عاشق دیوانه را از صحبت خود ای پری</p></div>
<div class="m2"><p>گر برانی همچو سگ بر خاک کو خواهد نشست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر نبخشد صاف می ساقی بما دردی کشان</p></div>
<div class="m2"><p>غم مخور اهلی که دردی در سبو خواهد نشست</p></div></div>