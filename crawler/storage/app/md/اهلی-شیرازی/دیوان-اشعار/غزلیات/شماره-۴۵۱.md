---
title: >-
    شمارهٔ ۴۵۱
---
# شمارهٔ ۴۵۱

<div class="b" id="bn1"><div class="m1"><p>یاران چه شد که پرسش یاری نمیکند</p></div>
<div class="m2"><p>بر دردمند خویش گذاری نمیکنند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از غصه بر کنار و چو گل در کنار هم</p></div>
<div class="m2"><p>برما نظر ز گوشه کناری نمیکنند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مارا بسوخت یاد حریفان و این گروه</p></div>
<div class="m2"><p>یادی ز حال سوخته باری نمیکنند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مردیم از انتظار و رفیقان بیوفا</p></div>
<div class="m2"><p>آخر گذار هم بمزاری نمیکنند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اهلی ترا بناله که فریاد رس شود؟</p></div>
<div class="m2"><p>کاین ناله های سرد تو کاری نمیکنند</p></div></div>