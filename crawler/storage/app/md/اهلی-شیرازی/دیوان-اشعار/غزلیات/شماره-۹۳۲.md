---
title: >-
    شمارهٔ ۹۳۲
---
# شمارهٔ ۹۳۲

<div class="b" id="bn1"><div class="m1"><p>پرتوی گر افکنی در چشم از شمع جمال</p></div>
<div class="m2"><p>چشم من بتاخنه‌ای گردد چو فانوس خیال</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کاش چشمم را بر آری وقت کشتن تا شود</p></div>
<div class="m2"><p>کاسه چشم پر از خونم سگانت را سفال</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>وعده دیدار لیلی چون به عید افتاده است</p></div>
<div class="m2"><p>جای آن دارد که گردد قامت مجنون هلال</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مست وصلش پر مشو ایدل که می باید کشید</p></div>
<div class="m2"><p>در شب هجران خمار مستی روز وصال</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حسن او کز دست دانایان عنان دل ربود</p></div>
<div class="m2"><p>چون نگه دارد دل از وی اهلی شوریده‌حال</p></div></div>