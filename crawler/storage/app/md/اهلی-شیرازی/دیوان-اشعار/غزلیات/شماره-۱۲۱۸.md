---
title: >-
    شمارهٔ ۱۲۱۸
---
# شمارهٔ ۱۲۱۸

<div class="b" id="bn1"><div class="m1"><p>هرگز دلم ملول نگشت از جفای او</p></div>
<div class="m2"><p>تا زنده ام خوشم چو بمیرم فدای او</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن گل نهاده در ره من صد هزار خار</p></div>
<div class="m2"><p>من خار راه رفته بمژگان برای او</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از عمر بیوفای خودم در عجب که چون</p></div>
<div class="m2"><p>کرد اینوفا که کشته شدم در وفای او</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ما را طواف کعبه چه حاجت که میکده</p></div>
<div class="m2"><p>صد کعبه در طواف در آرد صفای او</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عالم خراب گنج زر و رند می پرست</p></div>
<div class="m2"><p>در عالمی که گنج زرست اژدهای او</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دانی که مرغ دل همه در اضطراب چیست</p></div>
<div class="m2"><p>در اضطراب آنکه بمیرد بپای او</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اهلی در آشنایی و یاری ز جور اوست</p></div>
<div class="m2"><p>بیگانه یی که کس نشود آشنای او</p></div></div>