---
title: >-
    شمارهٔ ۱۳۶۶
---
# شمارهٔ ۱۳۶۶

<div class="b" id="bn1"><div class="m1"><p>نظری فکن که دارم تن زار و روی زردی</p></div>
<div class="m2"><p>لب خشک و دیده تر دل گرم و آه سردی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو که آفتاب حسنی چه غمت ز خاکساران</p></div>
<div class="m2"><p>که بدامن تو هرگز ننشسته است گردی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>غم جان خسته ما نخورد طبیب جانها</p></div>
<div class="m2"><p>مگر این طبیب مارا بدهد خدای دردی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دل خسته زخم هجران به هلاک کرد درمان</p></div>
<div class="m2"><p>اگر این دوا نبودی دل ریش ما چه کردی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سر آنحریف گردم که بخون خویش رقصد</p></div>
<div class="m2"><p>نه چو گرد باشد بره تو هرزه گردی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>که رسد بخضر و عیسی برسان بما خدایا</p></div>
<div class="m2"><p>نفس حیات بخشی قدم جهان نوردی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تو بسعی خویش اهلی نشوی قبول دلها</p></div>
<div class="m2"><p>مگر آنکه بر تو افتد نظر قبول مردی</p></div></div>