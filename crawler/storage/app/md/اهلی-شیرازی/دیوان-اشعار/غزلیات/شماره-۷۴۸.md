---
title: >-
    شمارهٔ ۷۴۸
---
# شمارهٔ ۷۴۸

<div class="b" id="bn1"><div class="m1"><p>خوبان که فرق تاقدم از جان سرشته اند</p></div>
<div class="m2"><p>مردم کشند اگرچه بصورت فرشته اند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در کوی گلرخان پی خواری کشان عشق</p></div>
<div class="m2"><p>یک گل زمین نماند که خاری نکشته اند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زخم بتی است هر سر مویم که بر تن است</p></div>
<div class="m2"><p>بی زخم خویش یکسر مویم نهشته اند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از تیغ نو خطان سر ما را گریز نیست</p></div>
<div class="m2"><p>کاین حرف از ازل بسر ما نوشته اند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اهلی قبای عافیت کارزو بود</p></div>
<div class="m2"><p>چون پوشی این قبا که هنوزش نرشته اند</p></div></div>