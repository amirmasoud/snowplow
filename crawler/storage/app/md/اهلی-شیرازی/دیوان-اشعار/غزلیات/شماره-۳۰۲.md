---
title: >-
    شمارهٔ ۳۰۲
---
# شمارهٔ ۳۰۲

<div class="b" id="bn1"><div class="m1"><p>سر رشته غم دل چون شمع جانگدازست</p></div>
<div class="m2"><p>کوته کنم حکایت کاین قصه بس درازست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>او همچو سرو سرکش من خاک ره چو سایه</p></div>
<div class="m2"><p>آن رسم ناز و شوخی وین شیوه نیازست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آن سرو ناز، کاری نگشاید از نیازم</p></div>
<div class="m2"><p>تا من نیاز دارم او در مقام نازست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در کوی بت پرستان خاموش باش زاهد</p></div>
<div class="m2"><p>کانجا می حقیقت در ساغر مجازست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مارا بحسن صورت مجنون نساخت آن مه</p></div>
<div class="m2"><p>محمود را محبت با سیرت ایازست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گردون گرت نماید از حقه مهره مهر</p></div>
<div class="m2"><p>بازی مخور که گردون تا هست حقه بازاست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر گنج وصل خواهی لب تشنه دار اهلی</p></div>
<div class="m2"><p>یعنی کلید این در دردست اهل رازست</p></div></div>