---
title: >-
    شمارهٔ ۱۰۹۴
---
# شمارهٔ ۱۰۹۴

<div class="b" id="bn1"><div class="m1"><p>من سوخته داغ درون پرور عشقم</p></div>
<div class="m2"><p>گر کعبه شوم حلقه بگوش در عشقم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز آن آتش رسوایی من صد علم افراخت</p></div>
<div class="m2"><p>تا عقل بداند که من از لشکر عشقم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون ذره به مه دل ندهم مهر پرستم</p></div>
<div class="m2"><p>آری همه حسنی نبود در خور عشقم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>فرزند غمم از ازل آورده ام این درد</p></div>
<div class="m2"><p>پرورده درد از پدر و مادر عشقم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر کسکه چو اهلی ورق عشق مرا خواند</p></div>
<div class="m2"><p>دانست که من آیتی از دفتر عشقم</p></div></div>