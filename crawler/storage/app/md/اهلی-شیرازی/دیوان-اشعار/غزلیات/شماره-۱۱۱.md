---
title: >-
    شمارهٔ ۱۱۱
---
# شمارهٔ ۱۱۱

<div class="b" id="bn1"><div class="m1"><p>گمره عشقم و نبود به من مست گرفت</p></div>
<div class="m2"><p>که چنین می بردم او که مرا دست گرفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خاک ره گشتن ما عین سرافرازی بود</p></div>
<div class="m2"><p>چشم کوتاه نظر همت ما پست گرفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>موج بحر کرم افکند مرادم به کنار</p></div>
<div class="m2"><p>ورنه این صید نشاید به دو صد شست گرفت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از وجود و عدم یار فراغت دارد</p></div>
<div class="m2"><p>هستی ام نیستی و نیستی ام هست گرفت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اهلی از عشق تو شد کافر و زنار پرست</p></div>
<div class="m2"><p>نام ایمان به زبان بهر زبان بست گرفت</p></div></div>