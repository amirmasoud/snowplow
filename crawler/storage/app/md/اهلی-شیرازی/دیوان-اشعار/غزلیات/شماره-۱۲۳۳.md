---
title: >-
    شمارهٔ ۱۲۳۳
---
# شمارهٔ ۱۲۳۳

<div class="b" id="bn1"><div class="m1"><p>هر که آن گل پا نهد در دیده نمناک او</p></div>
<div class="m2"><p>عاقبت شاخ گلی سر بر زند از خاک او</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همچو آهو باز خواهم دیده را بعد از هلاک</p></div>
<div class="m2"><p>تا سر خود را بینم بسته فتراک او</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آتش دوزخ ز آب کوثرم خوشتر بود</p></div>
<div class="m2"><p>گر بود در پیش چشمم روی آتشناک او</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جان آنصاحب نظر آیینه عین دلست</p></div>
<div class="m2"><p>کز غبار دیگران دورست چشم پاک او</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>میشود دود دلم از سینه چونسروی بلند</p></div>
<div class="m2"><p>هر گه آرم در نظر سر و قد چالاک او</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر که در دل تلخی عشق است او را خوشگوار</p></div>
<div class="m2"><p>زهر باشد از لب نوشین لبان تریاک او</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کس سخن نزدیک اهلی چونکند از ترک عشق</p></div>
<div class="m2"><p>زانکه دورست اینسخن بسیار از دراک او</p></div></div>