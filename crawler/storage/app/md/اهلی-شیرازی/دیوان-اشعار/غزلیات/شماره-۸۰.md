---
title: >-
    شمارهٔ ۸۰
---
# شمارهٔ ۸۰

<div class="b" id="bn1"><div class="m1"><p>یامن ناصبور را سوی خود از وفا طلب</p></div>
<div class="m2"><p>یاتو که پاکدامنی صبر من از خدا طلب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روزشکار چون خورد بر دل صید تیر تو</p></div>
<div class="m2"><p>گر طلبی خدنگ خود از دل ریش ما طلب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>درد تو می کشد مرایا به کرم دوا کنش</p></div>
<div class="m2"><p>یا قدری فزون ازین تا نکنم دوا طلب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آه چه پوشم این سخن وه که بکام غیر شد</p></div>
<div class="m2"><p>آنچه دل من از خدا کرد بصد دعا طلب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خواب و خیال می برد در پی وصل تو مرا</p></div>
<div class="m2"><p>فکر محال می کند مفلس کیمیا طلب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای دلم آشنای تو همدم غیر من مشو</p></div>
<div class="m2"><p>غیر کی آشنا شود، هم دل آشنا طلب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>همنفسان دوست را مستی وصل بس بود</p></div>
<div class="m2"><p>ساقی اگر کرم کنی اهلی بی نوا طلب</p></div></div>