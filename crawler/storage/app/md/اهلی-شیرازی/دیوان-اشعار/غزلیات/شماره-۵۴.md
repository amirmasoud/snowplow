---
title: >-
    شمارهٔ ۵۴
---
# شمارهٔ ۵۴

<div class="b" id="bn1"><div class="m1"><p>کوی تو که من از همه کس واپسم آنجا</p></div>
<div class="m2"><p>معراج مراد است کجا می رسم آنجا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر کس سخن همنفسی پیش تو گوید</p></div>
<div class="m2"><p>ازمن که کند یاد که من بی کسم آنجا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در گلشن کوی تو من خار چه ارزم</p></div>
<div class="m2"><p>آتش به من انداز که خار و خسم آنجا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آه از ستم بخت که در کوی محبت</p></div>
<div class="m2"><p>بیش از همه ام وز همه کس واپسم آنجا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اهلی سگ آن در چه کند پاس رقیبان</p></div>
<div class="m2"><p>حاجت به سگان هم نبود من بسم آنجا</p></div></div>