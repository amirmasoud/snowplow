---
title: >-
    شمارهٔ ۹۴۵
---
# شمارهٔ ۹۴۵

<div class="b" id="bn1"><div class="m1"><p>سجده بردم بدرش دوش چو تنها گشتم</p></div>
<div class="m2"><p>آمد آنشمع برون ناگه و رسوا گشتم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عاقبت نقد مرا دل خود داد بدست</p></div>
<div class="m2"><p>سالها گرچه بدر یوزه دلها گشتم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>منم آن ذره که بی نام و نشان میباشم</p></div>
<div class="m2"><p>بهوا داری خورشید تو پیدا گشتم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>وقت آنست که در میکده افتاده شوم</p></div>
<div class="m2"><p>جبر آن عمر که بیهوده بهر جا گشتم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اهلی از کعبه چو حاجی نزنم لاف که من</p></div>
<div class="m2"><p>خادم میکده بودم چه بصحرا گشتم</p></div></div>