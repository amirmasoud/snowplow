---
title: >-
    شمارهٔ ۷۴۶
---
# شمارهٔ ۷۴۶

<div class="b" id="bn1"><div class="m1"><p>چون لاله جهانی بغمت غرقه بخونند</p></div>
<div class="m2"><p>یکبار نگه کن که اسیران تو چونند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عشاق ترا در خط فرمان نکشد عقل</p></div>
<div class="m2"><p>کاینطایفه از دایره عقل برونند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون سلسله عشق سراسر همه قیدست</p></div>
<div class="m2"><p>آزاده دل آن قوم که در قید جنونند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر خاک شهیدان گذر ای سرو بعزت</p></div>
<div class="m2"><p>کز خاک رهت کمتر و از عرش فزونند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ثابت قدمانرا منگر در ره خود سهل</p></div>
<div class="m2"><p>کاین طایفه در خیمه افلاک ستونند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای من سگ آن قوم که از آهوی چشمت</p></div>
<div class="m2"><p>چون نافه دهان دوخته باداغ جنونند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بی صبری اهلی نبود عیب ز شوقت</p></div>
<div class="m2"><p>مستان محبت همه بی صبر و سکونند</p></div></div>