---
title: >-
    شمارهٔ ۷۰۳
---
# شمارهٔ ۷۰۳

<div class="b" id="bn1"><div class="m1"><p>بذات حق که مرا تا وجود خواهد بود</p></div>
<div class="m2"><p>سرم بپای بتان در سجود خواهد بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو گر زمهر پشیمان شدی مرا باری</p></div>
<div class="m2"><p>همان محبت دیرین که بود خواهد بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چرا ز دود دل من به تنگ می آیی</p></div>
<div class="m2"><p>چو آتشم زدی البته دود خواهد بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تو خود بگو که رخ زرد عاشقان تا کی</p></div>
<div class="m2"><p>ز دست سیلی عشقت کبود خواهد بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زیان دین و دلم نیست غم که در ره دوست</p></div>
<div class="m2"><p>هر آن زیان که شود عین سود خواهد بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مباش دور ز اهلی که مرگ نزدیک است</p></div>
<div class="m2"><p>وگر چه دیر بود دیر زود خواهد بود</p></div></div>