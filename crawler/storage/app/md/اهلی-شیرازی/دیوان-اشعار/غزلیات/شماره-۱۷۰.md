---
title: >-
    شمارهٔ ۱۷۰
---
# شمارهٔ ۱۷۰

<div class="b" id="bn1"><div class="m1"><p>مارا تنی چو صورت دیوار مانده است</p></div>
<div class="m2"><p>چشم و زبان و دست و دل از کار مانده است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خواهم که بشکنم قفس تن که دور ازو</p></div>
<div class="m2"><p>بیهوده مرغ روح گرفتار مانده است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از دیده یار رفت وزخون خشک شد مژه</p></div>
<div class="m2"><p>زان گل که بود در نظرم خار مانده است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از زخم تیر غمزه او زنده نیست کس</p></div>
<div class="m2"><p>وان هم که زنده است دل افکار مانده است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در عشق هرکه رشته جان بگسلد ز شوق</p></div>
<div class="m2"><p>آن خود بدست بسته زنار مانده است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اهلی اسیر ششدر غم گشت چاره نیست</p></div>
<div class="m2"><p>بیچاره نامراد بناچار مانده است</p></div></div>