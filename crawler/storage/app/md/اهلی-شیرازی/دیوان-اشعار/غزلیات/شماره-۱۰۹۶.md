---
title: >-
    شمارهٔ ۱۰۹۶
---
# شمارهٔ ۱۰۹۶

<div class="b" id="bn1"><div class="m1"><p>از بسکه پیش روی بتان سجدها کنم</p></div>
<div class="m2"><p>شرم آیدم که روی دعا با خدا کنم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اکنون که دین نماند بتان گر جفا کنند</p></div>
<div class="m2"><p>باری به بت پرستی خود من وفا کنم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من از دو کون دامن یاری گرفته ام</p></div>
<div class="m2"><p>دیوانگی بود اگر آنهم رها کنم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نامردمی است داد ز زخم کسی زدن</p></div>
<div class="m2"><p>اینک بیک مشاهده او دوا کنم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اهلی گواه مهر و محبت همین بسست</p></div>
<div class="m2"><p>گر خون من بریزد من صد دعا کنم</p></div></div>