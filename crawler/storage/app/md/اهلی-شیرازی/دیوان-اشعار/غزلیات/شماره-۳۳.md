---
title: >-
    شمارهٔ ۳۳
---
# شمارهٔ ۳۳

<div class="b" id="bn1"><div class="m1"><p>از که نالم که فغان از دل ریش است مرا</p></div>
<div class="m2"><p>هر بلایی که بود از دل خویش است مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شربت وصل تو بی زخم فراقی نبود</p></div>
<div class="m2"><p>لذت نوش پس از تلخی نیش است مرا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من که بیگانه ام از خویش و محبت سوزم</p></div>
<div class="m2"><p>چه غم از محنت بیگانه و خویش است مرا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مگرم کعبه امید پس از مرگ دهند</p></div>
<div class="m2"><p>کاین ره دور و درازی است که پیش است مرا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر دل از زخم جفای تو شود ریش چه غم</p></div>
<div class="m2"><p>هرحمت های غمت مرحم ریش است مرا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گرچه خوبان بمن خسته جفا کم نکنند</p></div>
<div class="m2"><p>شکر ایزد که وفا از همه بیش است مرا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سجده روی نکو اهلی اگر بد کیشی است</p></div>
<div class="m2"><p>بت پرستم چه غم از ملت و کیش است مرا</p></div></div>