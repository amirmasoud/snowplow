---
title: >-
    شمارهٔ ۸۳۰
---
# شمارهٔ ۸۳۰

<div class="b" id="bn1"><div class="m1"><p>ای ز خورشید رخت هر ساعتم سوزی دگر</p></div>
<div class="m2"><p>مهر تو سوزنده تر هر روز از روزی دگر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بس نبودت خوی بدجانان که بهر سوز من</p></div>
<div class="m2"><p>می نشینی هر دم از نو با بدآموزی دگر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل که در دست تو دادم به که نستانم ز تو</p></div>
<div class="m2"><p>سینه پر حسرت چه جویم محنت اندوزی دگر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای ملول از دیدن من کی ملامت دادمی</p></div>
<div class="m2"><p>گر دلم خرسند گشتی از دل افروزی دگر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بی تو آهی زد دل شوریده و خلقی بسوخت</p></div>
<div class="m2"><p>آه اگر اهلی کشد آه جگر سوزی دگر</p></div></div>