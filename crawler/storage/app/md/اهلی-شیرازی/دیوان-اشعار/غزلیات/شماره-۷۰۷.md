---
title: >-
    شمارهٔ ۷۰۷
---
# شمارهٔ ۷۰۷

<div class="b" id="bn1"><div class="m1"><p>یار از سر کوچه چو مه برآمد</p></div>
<div class="m2"><p>هر گوشه هزار وه برآمد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>برخاست قیامتی که در صبح</p></div>
<div class="m2"><p>ماه شب چارده برآمد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سرمست به خانقه گذر کرد</p></div>
<div class="m2"><p>فریاد ز خانقه برآمد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مقصودم از او هلاک خود بود</p></div>
<div class="m2"><p>مقصود بیک نگه برآمد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یعقوب صفت بسوخت اهلی</p></div>
<div class="m2"><p>تا یوسف او زچه برآمد</p></div></div>