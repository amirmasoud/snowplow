---
title: >-
    شمارهٔ ۱۰۰۷
---
# شمارهٔ ۱۰۰۷

<div class="b" id="bn1"><div class="m1"><p>ای جگر از تو پر نمک دیده شور بخت هم</p></div>
<div class="m2"><p>چاک شد از تو جیب جان سینه لخت لخت هم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر چه رسید نخل تو خوش بکمال نیکویی</p></div>
<div class="m2"><p>کی رطبی رسد بما گرفتد از درخت هم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پرتو آفتاب تو سوخت ز تاب یکنظر</p></div>
<div class="m2"><p>کوکب تیره بخت ما اختر نیکبخت هم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آه که باغبان گل از پی یکنظر بمن</p></div>
<div class="m2"><p>کرد هزار نازکی گفت هزار سخت هم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اهلی خسته کی کشد منت تاج و تخت کی</p></div>
<div class="m2"><p>سایه رحمت تواش تاج بسست و تخت هم</p></div></div>