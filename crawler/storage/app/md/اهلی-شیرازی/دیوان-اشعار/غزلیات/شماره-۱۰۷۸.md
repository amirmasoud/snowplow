---
title: >-
    شمارهٔ ۱۰۷۸
---
# شمارهٔ ۱۰۷۸

<div class="b" id="bn1"><div class="m1"><p>مست آنم که ز دستت قدحی نوش کنم</p></div>
<div class="m2"><p>هرچه غیر تو بود جمله فراموش کنم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نایم از شوق تو تا روز قیامت باهوش</p></div>
<div class="m2"><p>مست اگر با تو شبی دست در آغوش کنم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گوش بر قول تو دارم نه به پند دگران</p></div>
<div class="m2"><p>من نه آنم که حدیث دگران گوش کنم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خنده رویم همه چون جام و دلم پرخونست</p></div>
<div class="m2"><p>پرنشاطم چوخم وز آتش دل جوش کنم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>روی اهلی سوی پیران مرقعپوش است</p></div>
<div class="m2"><p>من نظر سوی جوانان قباپوش کنم</p></div></div>