---
title: >-
    شمارهٔ ۱۱۰۴
---
# شمارهٔ ۱۱۰۴

<div class="b" id="bn1"><div class="m1"><p>زان مرهم دل غیر دل ریش ندیدیم</p></div>
<div class="m2"><p>هرچند که دیدیم ازین پیش ندیدیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>منصور هم از دار نمایش غرضش بود</p></div>
<div class="m2"><p>مردی که بپوشد هنر خویش ندیدیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هرچند که گشتیم چو مجنون ز پی یار</p></div>
<div class="m2"><p>یاری بجز از سایه خود بیش ندیدیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر چرخ منه نام مروت که ز خوانش</p></div>
<div class="m2"><p>جز خون جگر قسمت درویش ندیدیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>باور که کند درد تو اهلی که توگویی</p></div>
<div class="m2"><p>از نوش لبان فایده جز نیش ندیدیم</p></div></div>