---
title: >-
    شماره ۱ - تاریخ وفات پهلوان یار علی
---
# شماره ۱ - تاریخ وفات پهلوان یار علی

<div class="b" id="bn1"><div class="m1"><p>پهلوان زمانه یار علی</p></div>
<div class="m2"><p>که ز مرگش زمانه پر شورست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با همه زور از اجل افتاد</p></div>
<div class="m2"><p>تا نگویی که کار بر زورست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پیر ما گفت بهر تاریخش</p></div>
<div class="m2"><p>جای اصلی آدمی گورست</p></div></div>