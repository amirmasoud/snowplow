---
title: >-
    شماره ۲۳ - تاریخ وفات تاج الدین
---
# شماره ۲۳ - تاریخ وفات تاج الدین

<div class="b" id="bn1"><div class="m1"><p>تاج دین خواجه زاده اسماعیل</p></div>
<div class="m2"><p>که گل از شاخسار عمر نچید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مرغ روحش بلند همت بود</p></div>
<div class="m2"><p>روضه قدس جان خویش گزید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شد بخلد برین درین تاریخ</p></div>
<div class="m2"><p>ز استان امام زاده شهید</p></div></div>