---
title: >-
    شماره ۳۰ - تاریخ وفات خواجه زین الدین
---
# شماره ۳۰ - تاریخ وفات خواجه زین الدین

<div class="b" id="bn1"><div class="m1"><p>مظهر لطف خواجه زین الدین</p></div>
<div class="m2"><p>کرد در خلد جاودان منزل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بود شمعی میان اهل دلان</p></div>
<div class="m2"><p>ذات پاکش بنور حق واصل</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زان سبب گشت سال تاریخش</p></div>
<div class="m2"><p>شمع بمجلس فروز اهل دل</p></div></div>