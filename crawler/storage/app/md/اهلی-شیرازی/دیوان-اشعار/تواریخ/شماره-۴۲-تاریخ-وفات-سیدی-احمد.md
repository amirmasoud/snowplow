---
title: >-
    شماره ۴۲ - تاریخ وفات سیدی احمد
---
# شماره ۴۲ - تاریخ وفات سیدی احمد

<div class="b" id="bn1"><div class="m1"><p>آه ازین چرخ بیوفا کز وی</p></div>
<div class="m2"><p>دایم آزرده است بنده و شاه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شد بباد فنا درین تاریخ</p></div>
<div class="m2"><p>سیدی احمد گل بهشت اله</p></div></div>