---
title: >-
    شماره ۲ - در ولادت ملک منصور گوید
---
# شماره ۲ - در ولادت ملک منصور گوید

<div class="b" id="bn1"><div class="m1"><p>بخت ملک قاسم از الطاف حق</p></div>
<div class="m2"><p>چرخ بحکمش شد و او حکم است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>داد خدایش خلفی کز شرف</p></div>
<div class="m2"><p>مشتریش چاکر و مه خادم است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اختر فرخنده منصور نام</p></div>
<div class="m2"><p>کش علم فتح و ظفر قایم است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نخل مرادست و بتاریخ سال</p></div>
<div class="m2"><p>گلبن بستان ملک قاسم است</p></div></div>