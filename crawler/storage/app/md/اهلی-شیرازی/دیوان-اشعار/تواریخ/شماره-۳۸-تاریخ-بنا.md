---
title: >-
    شماره ۳۸ - تاریخ بنا
---
# شماره ۳۸ - تاریخ بنا

<div class="b" id="bn1"><div class="m1"><p>بیمن دولت و اقبال شاه اسماعیل</p></div>
<div class="m2"><p>که هست بنده درگاه او علی سلطان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>درین مزار مبارک کمینه عبدالله</p></div>
<div class="m2"><p>بنای خیر نهاد از عنایت یزدان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چهار طاق سرچشمه ساخت و ز توفیق</p></div>
<div class="m2"><p>چهار طاق سرچشمه سال هجرت دان</p></div></div>