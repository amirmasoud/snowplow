---
title: >-
    شماره ۶ - تاریخ وفات سلطان خلیل
---
# شماره ۶ - تاریخ وفات سلطان خلیل

<div class="b" id="bn1"><div class="m1"><p>مکن تکیه بر مسند عمر فانی</p></div>
<div class="m2"><p>که سر رشته عمر بر بیوفایی است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نگه کن بسلطان خلیل سرافراز</p></div>
<div class="m2"><p>که چون غرقه در خون ز تیغ جدایی است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بفرمان شاهی فدا کرد جان را</p></div>
<div class="m2"><p>که فرمان شاهی چو حکم خدایی است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>فدایی صفت رفت و هست این گواهش</p></div>
<div class="m2"><p>که تاریخ سلطان خلیل فدایی است</p></div></div>