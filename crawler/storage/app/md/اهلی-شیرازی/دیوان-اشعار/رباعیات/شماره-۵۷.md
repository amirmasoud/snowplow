---
title: >-
    شمارهٔ ۵۷
---
# شمارهٔ ۵۷

<div class="b" id="bn1"><div class="m1"><p>واقف ز شب سیاه این سوخته کیست</p></div>
<div class="m2"><p>جز شمع سحر که دوش تا روز گریست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر کس که شبی در آتش هجر تو زیست</p></div>
<div class="m2"><p>دانست که سوز آتش دوزخ چیست</p></div></div>