---
title: >-
    شمارهٔ ۶۴
---
# شمارهٔ ۶۴

<div class="b" id="bn1"><div class="m1"><p>بی بختم و دست و دل ز اندیشه تهیست</p></div>
<div class="m2"><p>شیران همه رفتند و سر بیشه تهیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دوش اینهمه جام می فلک داد بغیر</p></div>
<div class="m2"><p>امروز که دور من بود شیشه تهیست</p></div></div>