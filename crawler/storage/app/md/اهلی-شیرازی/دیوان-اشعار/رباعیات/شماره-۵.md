---
title: >-
    شمارهٔ ۵
---
# شمارهٔ ۵

<div class="b" id="bn1"><div class="m1"><p>یارب نظر لطف بسویم بگشا</p></div>
<div class="m2"><p>وز دست دعا دری بروی بگشا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یا موی تنم با گره دل خوشدار</p></div>
<div class="m2"><p>یا این گره از تن چو مویم بگشا</p></div></div>