---
title: >-
    شمارهٔ ۲۲۸
---
# شمارهٔ ۲۲۸

<div class="b" id="bn1"><div class="m1"><p>در عشق تو کس جز رخ زردش نرسد</p></div>
<div class="m2"><p>درمان نبرد دلی که دردش نرسد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من مرده خاک پای تو کآب حیات</p></div>
<div class="m2"><p>هر چند که جان دهد بگردش نرسد</p></div></div>