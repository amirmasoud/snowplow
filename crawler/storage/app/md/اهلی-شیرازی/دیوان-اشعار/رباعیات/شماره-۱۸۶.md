---
title: >-
    شمارهٔ ۱۸۶
---
# شمارهٔ ۱۸۶

<div class="b" id="bn1"><div class="m1"><p>گردر تر و خشک دهر گشتی همه هیچ</p></div>
<div class="m2"><p>ور خار و گلی چو در گذشتی همه هیچ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر ساده دلی ورت هزاران نقشست</p></div>
<div class="m2"><p>در بحر فنا چو غرقه گشتی همه هیچ</p></div></div>