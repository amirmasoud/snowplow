---
title: >-
    شمارهٔ ۵۳
---
# شمارهٔ ۵۳

<div class="b" id="bn1"><div class="m1"><p>باز آتش من بلند آوازه شدست</p></div>
<div class="m2"><p>سرمستی من برون ز اندازه شدست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با موی سفید سرخوشم کز خط تو</p></div>
<div class="m2"><p>پیرانه سرم بهار دل تازه شدست</p></div></div>