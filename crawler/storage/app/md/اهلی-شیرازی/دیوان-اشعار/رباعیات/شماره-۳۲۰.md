---
title: >-
    شمارهٔ ۳۲۰
---
# شمارهٔ ۳۲۰

<div class="b" id="bn1"><div class="m1"><p>آنانکه بخوبی زنان مینازند</p></div>
<div class="m2"><p>با صورت دیوار نظر میبازند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جز صورت دیوار چه خواهد بودن</p></div>
<div class="m2"><p>شکلی که بسرخی و سفیدی سازند</p></div></div>