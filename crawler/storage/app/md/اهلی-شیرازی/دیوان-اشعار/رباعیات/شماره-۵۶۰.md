---
title: >-
    شمارهٔ ۵۶۰
---
# شمارهٔ ۵۶۰

<div class="b" id="bn1"><div class="m1"><p>آن دل که نگشته ام دمی خرم ازو</p></div>
<div class="m2"><p>هر روز بتازه دیده ام صد غم ازو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خشنود نه من ازو نه او نیز ز من</p></div>
<div class="m2"><p>بیزار شدست دل ز من منهم ازو</p></div></div>