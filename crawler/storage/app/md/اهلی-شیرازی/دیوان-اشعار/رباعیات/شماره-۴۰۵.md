---
title: >-
    شمارهٔ ۴۰۵
---
# شمارهٔ ۴۰۵

<div class="b" id="bn1"><div class="m1"><p>گه بر در کعبه ایم گه بر در دیر</p></div>
<div class="m2"><p>گه همدم خویشیم و گهی محرم غیر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خیر و شر کار حالیا پیدا نیست</p></div>
<div class="m2"><p>باشد که بود خاتمت کار بخیر</p></div></div>