---
title: >-
    شمارهٔ ۶۵
---
# شمارهٔ ۶۵

<div class="b" id="bn1"><div class="m1"><p>ای برهمن آن عارض چون لاله پرست</p></div>
<div class="m2"><p>رخسار بتان چارده ساله پرست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر چشم خدای بین نباشد باری</p></div>
<div class="m2"><p>خورشید پرست به که گوساله پرست</p></div></div>