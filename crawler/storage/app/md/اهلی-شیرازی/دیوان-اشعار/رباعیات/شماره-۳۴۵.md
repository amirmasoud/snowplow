---
title: >-
    شمارهٔ ۳۴۵
---
# شمارهٔ ۳۴۵

<div class="b" id="bn1"><div class="m1"><p>از آل علی هر چه ترا دل باشد</p></div>
<div class="m2"><p>از شاه خراسان همه حاصل باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در موسی و سی و نه خلف فرقی نیست</p></div>
<div class="m2"><p>ایشان چلشان یکی یکی چل باشد</p></div></div>