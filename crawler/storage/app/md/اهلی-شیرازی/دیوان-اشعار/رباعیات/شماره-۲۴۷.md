---
title: >-
    شمارهٔ ۲۴۷
---
# شمارهٔ ۲۴۷

<div class="b" id="bn1"><div class="m1"><p>می خور که فلک نقد بقا میدزدد</p></div>
<div class="m2"><p>نور از دل و از چهره صفا میدزدد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یکدم نبود که نیست در غارت ما</p></div>
<div class="m2"><p>سال و مه و روز و شب ز ما میدزدد</p></div></div>