---
title: >-
    شمارهٔ ۴۴۷
---
# شمارهٔ ۴۴۷

<div class="b" id="bn1"><div class="m1"><p>شهوت چه حرام و چه حلال است و بال</p></div>
<div class="m2"><p>کان یکدمه لذتست و صد ساله ملال</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خون تو حلال است اگر هست حرام</p></div>
<div class="m2"><p>مال تو حرام است اگر هست حلال</p></div></div>