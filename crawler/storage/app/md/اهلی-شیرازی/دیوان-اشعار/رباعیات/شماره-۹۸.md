---
title: >-
    شمارهٔ ۹۸
---
# شمارهٔ ۹۸

<div class="b" id="bn1"><div class="m1"><p>در آینه خاطرت از هر که شکی است</p></div>
<div class="m2"><p>او هم بشک است رانکه خاطر محکی است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا شیشه دو رنگست بود آب دو رنگ</p></div>
<div class="m2"><p>یکرنگ بر آکه دشمن و دوست یکی است</p></div></div>