---
title: >-
    شمارهٔ ۱۹۸
---
# شمارهٔ ۱۹۸

<div class="b" id="bn1"><div class="m1"><p>کی ره سوی عشق عقل آگاه برد</p></div>
<div class="m2"><p>وان دلشده هم نه ره بدلخواه برد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این کعبه نه رهنما نه رهبر دارد</p></div>
<div class="m2"><p>حیران شده یی که گم شود راه برد</p></div></div>