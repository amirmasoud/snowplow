---
title: >-
    شمارهٔ ۴۵۲
---
# شمارهٔ ۴۵۲

<div class="b" id="bn1"><div class="m1"><p>ایسرو سهی درآ در آغوش چو گل</p></div>
<div class="m2"><p>با ما بحضرو باده مینوش چو گل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اندیشه مکن ز غیبت غیر که ما</p></div>
<div class="m2"><p>دو چشم چو نرگسیم و صد گوش چو گل</p></div></div>