---
title: >-
    شمارهٔ ۱۶
---
# شمارهٔ ۱۶

<div class="b" id="bn1"><div class="m1"><p>ای رفته و در دام بلا هشته مرا</p></div>
<div class="m2"><p>آتش زده در کشته و نا کشته مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کی رشته عافیت بدست آرم باز</p></div>
<div class="m2"><p>کز زلف تو گم شدست سررشته مرا</p></div></div>