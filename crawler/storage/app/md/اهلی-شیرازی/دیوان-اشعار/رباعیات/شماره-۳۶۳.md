---
title: >-
    شمارهٔ ۳۶۳
---
# شمارهٔ ۳۶۳

<div class="b" id="bn1"><div class="m1"><p>تا پیر و معلم از تو خرم نشود</p></div>
<div class="m2"><p>کار تو قبول خلق عالم نشود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بشناس حق پیر که بی علم و ادب</p></div>
<div class="m2"><p>گر شخص فرشته باشد آدم نشود</p></div></div>