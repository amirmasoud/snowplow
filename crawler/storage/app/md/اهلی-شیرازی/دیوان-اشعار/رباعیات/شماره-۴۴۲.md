---
title: >-
    شمارهٔ ۴۴۲
---
# شمارهٔ ۴۴۲

<div class="b" id="bn1"><div class="m1"><p>گر بگسلی از لذت تن همچو ملک</p></div>
<div class="m2"><p>پرواز کند طایر روحت بفلک</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از عرش فتاده یی بفرش از پی نفس</p></div>
<div class="m2"><p>زنهار که تا نیفتی آخر بدرک</p></div></div>