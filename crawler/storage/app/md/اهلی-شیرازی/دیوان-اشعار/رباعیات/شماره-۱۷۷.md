---
title: >-
    شمارهٔ ۱۷۷
---
# شمارهٔ ۱۷۷

<div class="b" id="bn1"><div class="m1"><p>فرزند که بی حیا و شرم و ادب است</p></div>
<div class="m2"><p>زان است که بی نسبتیی در نسب است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عقدی که بنسبت نبود نیست مباح</p></div>
<div class="m2"><p>استر که حرامزاده شد زان سبب است</p></div></div>