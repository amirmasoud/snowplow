---
title: >-
    شمارهٔ ۵۵۰
---
# شمارهٔ ۵۵۰

<div class="b" id="bn1"><div class="m1"><p>بسرشت بچل صباح ایزد گل تو</p></div>
<div class="m2"><p>کز صبر خمیر مایه گیرد دل تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جایی که بدین صبر تو حاصل شده یی</p></div>
<div class="m2"><p>بی صبر مراد کی شود حاصل تو</p></div></div>