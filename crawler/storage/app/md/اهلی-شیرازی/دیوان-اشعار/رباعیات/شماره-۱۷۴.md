---
title: >-
    شمارهٔ ۱۷۴
---
# شمارهٔ ۱۷۴

<div class="b" id="bn1"><div class="m1"><p>ایزد که برحمت نظری سوی تو داشت</p></div>
<div class="m2"><p>بر خاتم تو قباله ملک نگاشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غافل نشوی که ملک محبوب تو است</p></div>
<div class="m2"><p>محبوب بدست دگران کس نگذاشت</p></div></div>