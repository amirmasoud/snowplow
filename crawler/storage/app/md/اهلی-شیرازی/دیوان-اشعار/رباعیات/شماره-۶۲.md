---
title: >-
    شمارهٔ ۶۲
---
# شمارهٔ ۶۲

<div class="b" id="bn1"><div class="m1"><p>در ملک جهان حیات جاوید یکیست</p></div>
<div class="m2"><p>در دور فلک ساغر جمشید یکیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کس نسبت یار ما بخوبان نکند</p></div>
<div class="m2"><p>خوبان همه ذره اند و خورشید یکیست</p></div></div>