---
title: >-
    شمارهٔ ۱۱۵
---
# شمارهٔ ۱۱۵

<div class="b" id="bn1"><div class="m1"><p>ما صاف دلیم و کار دشمن دغلیست</p></div>
<div class="m2"><p>هر بد که کند چاره ما کم محلیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در مهر تو دشمنم رقیب از ازلست</p></div>
<div class="m2"><p>مهر از ازلست و دشمنی هم ازلیست</p></div></div>