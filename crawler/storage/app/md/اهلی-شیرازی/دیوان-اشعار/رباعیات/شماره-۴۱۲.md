---
title: >-
    شمارهٔ ۴۱۲
---
# شمارهٔ ۴۱۲

<div class="b" id="bn1"><div class="m1"><p>در ملک جهان چه دیدی از عمر دراز</p></div>
<div class="m2"><p>کانرا نشکست این فلک شعبده باز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون هر چه دلت خواست بدلخواه نماند</p></div>
<div class="m2"><p>دل بر چه نهی دگر چه میخواهی باز</p></div></div>