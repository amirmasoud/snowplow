---
title: >-
    شمارهٔ ۹۶
---
# شمارهٔ ۹۶

<div class="b" id="bn1"><div class="m1"><p>در مذهب عشق مست و مستوره یکیست</p></div>
<div class="m2"><p>انگور و شراب و سرکه و غوره یکیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یک نقطه حباب کوزه و کوره بود</p></div>
<div class="m2"><p>آن نقطه چو سوخت کوزه و کوره یکیست</p></div></div>