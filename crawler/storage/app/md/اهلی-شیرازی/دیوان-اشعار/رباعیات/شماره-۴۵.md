---
title: >-
    شمارهٔ ۴۵
---
# شمارهٔ ۴۵

<div class="b" id="bn1"><div class="m1"><p>گر در چمن از تو گفتگو خواهد رفت</p></div>
<div class="m2"><p>آب رخ گل چو آب جو خواهد رفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر بام میا چو مه که خورشید فلک</p></div>
<div class="m2"><p>از شرم تو در زمین فرو خواهد رفت</p></div></div>