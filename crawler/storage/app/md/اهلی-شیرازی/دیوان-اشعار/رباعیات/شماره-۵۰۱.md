---
title: >-
    شمارهٔ ۵۰۱
---
# شمارهٔ ۵۰۱

<div class="b" id="bn1"><div class="m1"><p>من غیر تو ای مایه راحت چکنم</p></div>
<div class="m2"><p>بی لعل تو مرهم جراحت چکنم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر مه نظر ای کان ملاحت چکنم</p></div>
<div class="m2"><p>من مست ملامتم صباحت چکنم</p></div></div>