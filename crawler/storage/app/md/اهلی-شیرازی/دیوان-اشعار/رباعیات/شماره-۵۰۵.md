---
title: >-
    شمارهٔ ۵۰۵
---
# شمارهٔ ۵۰۵

<div class="b" id="bn1"><div class="m1"><p>گر سر ننهم بجور دشمن چکنم؟</p></div>
<div class="m2"><p>ور پای نیاورم بدامن چکنم؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خورشید تو طالع است و من محرومم</p></div>
<div class="m2"><p>طالع چو مدد نمیکند من چکنم؟</p></div></div>