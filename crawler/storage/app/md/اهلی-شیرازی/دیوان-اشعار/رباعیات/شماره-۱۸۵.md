---
title: >-
    شمارهٔ ۱۸۵
---
# شمارهٔ ۱۸۵

<div class="b" id="bn1"><div class="m1"><p>جز هجر نصیب عاشق بیدل چیست</p></div>
<div class="m2"><p>جز حسرت وصل او بمن واصل چیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر من نگرم سیاهه دفتر عمر</p></div>
<div class="m2"><p>جز نامه سیاهی دگرم حاصل چیست</p></div></div>