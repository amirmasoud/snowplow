---
title: >-
    شمارهٔ ۱۴
---
# شمارهٔ ۱۴

<div class="b" id="bn1"><div class="m1"><p>ایمرغ چمن که کار عشقست ترا</p></div>
<div class="m2"><p>مقصود ز حسن یار عشق است ترا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با هر گل این باغ هزارت عشقست</p></div>
<div class="m2"><p>عشق است ترا هزار عشق است ترا</p></div></div>