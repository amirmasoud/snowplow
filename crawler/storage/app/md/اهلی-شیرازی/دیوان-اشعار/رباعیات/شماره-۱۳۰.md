---
title: >-
    شمارهٔ ۱۳۰
---
# شمارهٔ ۱۳۰

<div class="b" id="bn1"><div class="m1"><p>آنکس نعمش زیاده باشد همه وقت</p></div>
<div class="m2"><p>کو خوان کرم نهاده باشد همه وقت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در سفره چو غنچه گر همه نان تهیست</p></div>
<div class="m2"><p>باید که چو گل گشاده باشد همه وقت</p></div></div>