---
title: >-
    شمارهٔ ۲۲۲
---
# شمارهٔ ۲۲۲

<div class="b" id="bn1"><div class="m1"><p>خوش آنکه دمی زمانه یارم باشد</p></div>
<div class="m2"><p>معشوقه چو سرو و در کنارم باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر توسن بخت از آن ز زلف چو کمند</p></div>
<div class="m2"><p>در دست عنان اختیارم باشد</p></div></div>