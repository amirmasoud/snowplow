---
title: >-
    شمارهٔ ۵۳۹
---
# شمارهٔ ۵۳۹

<div class="b" id="bn1"><div class="m1"><p>هرگز نکنی میل من ایعهد شکن</p></div>
<div class="m2"><p>سنگ است دل تو ای بت سیم ذقن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من نرم چو موم کی کنم آندل سخت</p></div>
<div class="m2"><p>این کار مقلب القلوب است نه من</p></div></div>