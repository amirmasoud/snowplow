---
title: >-
    شمارهٔ ۵۳۲
---
# شمارهٔ ۵۳۲

<div class="b" id="bn1"><div class="m1"><p>یا رب بکرم درد مرا درمان کن</p></div>
<div class="m2"><p>رحمی بمن سوخته هجران کن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یا راه بمومیایی وصل نمای</p></div>
<div class="m2"><p>یا بر من دلشکسته کار آسان کن</p></div></div>