---
title: >-
    شمارهٔ ۶۳
---
# شمارهٔ ۶۳

<div class="b" id="bn1"><div class="m1"><p>عمریست که میرود دل اندر ره دوست</p></div>
<div class="m2"><p>ذره نبرد راه به منزلگه دوست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اهلی بدل مست تو گر نیست امید</p></div>
<div class="m2"><p>نومید مباشی از دل آگه دوست</p></div></div>