---
title: >-
    شمارهٔ ۲۳۷
---
# شمارهٔ ۲۳۷

<div class="b" id="bn1"><div class="m1"><p>گاهیم بعشق و مستی آموخته اند</p></div>
<div class="m2"><p>گاهی نظر از مراد دل دوخته اند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا کی مس من بکیمیایی نرسد</p></div>
<div class="m2"><p>باری بهزار کوره ام سوخته اند</p></div></div>