---
title: >-
    شمارهٔ ۵۷۴
---
# شمارهٔ ۵۷۴

<div class="b" id="bn1"><div class="m1"><p>اهلی ز جهان چو قسمت خویش خوری</p></div>
<div class="m2"><p>گر کوشی وگرنه کی کم و بیش خوری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بگریز ز خلق زانکه زنبور و شند</p></div>
<div class="m2"><p>گر نوش طلب کنی همه نیش خوری</p></div></div>