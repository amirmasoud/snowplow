---
title: >-
    شمارهٔ ۳۰۷
---
# شمارهٔ ۳۰۷

<div class="b" id="bn1"><div class="m1"><p>آنانکه به بند زینت و زیب تنند</p></div>
<div class="m2"><p>خود را بزبان مردمان می فکنند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آنکس که خود آراست بود دشمن خود</p></div>
<div class="m2"><p>گر شاخ گل است کاخرش می شکنند</p></div></div>