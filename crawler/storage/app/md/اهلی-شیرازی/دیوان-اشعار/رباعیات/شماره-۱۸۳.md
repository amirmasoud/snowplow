---
title: >-
    شمارهٔ ۱۸۳
---
# شمارهٔ ۱۸۳

<div class="b" id="bn1"><div class="m1"><p>گر کار جهان ز غصه بی تاری نیست</p></div>
<div class="m2"><p>ما را سر و سودای جهان باری نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون کار باختیار کس می نشود</p></div>
<div class="m2"><p>بهتر ز طریق بیخودی کاری نیست</p></div></div>