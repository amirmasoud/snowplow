---
title: >-
    شمارهٔ ۴۹۶
---
# شمارهٔ ۴۹۶

<div class="b" id="bn1"><div class="m1"><p>ای کز ستم تو راحتی می یابم</p></div>
<div class="m2"><p>وز خون لبت ملاحتی می یابم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در گلشن حسن لاله زار رخ تو</p></div>
<div class="m2"><p>بر هر جگری جراحتی می یابم</p></div></div>