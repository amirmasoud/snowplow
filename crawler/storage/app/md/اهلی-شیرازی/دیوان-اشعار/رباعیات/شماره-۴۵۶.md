---
title: >-
    شمارهٔ ۴۵۶
---
# شمارهٔ ۴۵۶

<div class="b" id="bn1"><div class="m1"><p>من خود نرسم بوصل آنطرفه غزال</p></div>
<div class="m2"><p>ور هم برسم کجا رسم باری حال</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صد ساله حیات من شد ازهجر تلف</p></div>
<div class="m2"><p>جبر همه چون شود بیکروز وصال</p></div></div>