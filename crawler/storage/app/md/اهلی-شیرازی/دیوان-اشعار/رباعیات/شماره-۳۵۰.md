---
title: >-
    شمارهٔ ۳۵۰
---
# شمارهٔ ۳۵۰

<div class="b" id="bn1"><div class="m1"><p>گر خرقه عارفان بصد چاک بود</p></div>
<div class="m2"><p>باکی نبود ز چرکنی پاک بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر جامه چو گل پاره بود عیبی نیست</p></div>
<div class="m2"><p>شرطست دلی که همچو گل پاک بود</p></div></div>