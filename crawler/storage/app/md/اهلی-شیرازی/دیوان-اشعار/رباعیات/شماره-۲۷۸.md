---
title: >-
    شمارهٔ ۲۷۸
---
# شمارهٔ ۲۷۸

<div class="b" id="bn1"><div class="m1"><p>از هر چه در این دایره موجود بود</p></div>
<div class="m2"><p>مقصود شناسایی معبود بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مقصود حق از وجود ما معرفتست</p></div>
<div class="m2"><p>ور نی ز وجود ما چه مقصود بود</p></div></div>