---
title: >-
    شمارهٔ ۱۰۴
---
# شمارهٔ ۱۰۴

<div class="b" id="bn1"><div class="m1"><p>عالم که چو چشم باز کردی هیچست</p></div>
<div class="m2"><p>هر کار کزو بساز کردی هیچست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون صورت آینه تماشاش خوشست</p></div>
<div class="m2"><p>گر دست طمع دراز کردی هیچست</p></div></div>