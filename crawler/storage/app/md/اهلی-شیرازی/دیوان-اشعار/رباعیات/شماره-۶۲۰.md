---
title: >-
    شمارهٔ ۶۲۰
---
# شمارهٔ ۶۲۰

<div class="b" id="bn1"><div class="m1"><p>ای کز مه خود چو سایه بی نور تری</p></div>
<div class="m2"><p>نزدیک تری و ز همه مهجور تری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از کعبه مقصود چو رو تافته یی</p></div>
<div class="m2"><p>هر چند که میروی ازو دور تری</p></div></div>