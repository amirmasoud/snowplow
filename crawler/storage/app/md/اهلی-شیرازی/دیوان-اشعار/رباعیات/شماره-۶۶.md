---
title: >-
    شمارهٔ ۶۶
---
# شمارهٔ ۶۶

<div class="b" id="bn1"><div class="m1"><p>می نوش که آنکه کعبه کردست و کنشت</p></div>
<div class="m2"><p>خاک همه را مستی عشق تو سرشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>معمور بود بشاهد و باده جهان</p></div>
<div class="m2"><p>موعود بود به کوثر و حور بهشت</p></div></div>