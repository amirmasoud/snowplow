---
title: >-
    شمارهٔ ۲۳۹
---
# شمارهٔ ۲۳۹

<div class="b" id="bn1"><div class="m1"><p>تا کی غم دل عاشق بی بخت خورد</p></div>
<div class="m2"><p>خواهد ز دلم سگ و صد لخت خورد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من رحم بدل کردم و خون ریخت دلم</p></div>
<div class="m2"><p>در عشق کسی که سست زد سخت خورد</p></div></div>