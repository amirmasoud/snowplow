---
title: >-
    شمارهٔ ۵۲۷
---
# شمارهٔ ۵۲۷

<div class="b" id="bn1"><div class="m1"><p>با حق پدی نا حق در جنگ مزن</p></div>
<div class="m2"><p>یعنی بزنا در زدن کس چنگ مزن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خواهی که بسنگسار لایق نشوی</p></div>
<div class="m2"><p>بر شیشه ناموس کسان سنگ مزن</p></div></div>