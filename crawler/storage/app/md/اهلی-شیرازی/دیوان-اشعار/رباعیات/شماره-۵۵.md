---
title: >-
    شمارهٔ ۵۵
---
# شمارهٔ ۵۵

<div class="b" id="bn1"><div class="m1"><p>آن بت که ز فرق تا قدم صنع خداست</p></div>
<div class="m2"><p>آن آب حیات و جان لب تشنه ماست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سیراب کجا شوم از آن آب حیات</p></div>
<div class="m2"><p>یک دیدن سیر خواهم او نیز کجاست</p></div></div>