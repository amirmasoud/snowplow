---
title: >-
    شمارهٔ ۵۷۱
---
# شمارهٔ ۵۷۱

<div class="b" id="bn1"><div class="m1"><p>یا رب بکرم نگاهدارنده تویی</p></div>
<div class="m2"><p>نقش همه را رقم نگارنده تویی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر چند که من تخم اهل میکارم</p></div>
<div class="m2"><p>کارنده منم ولی برآرنده تویی</p></div></div>