---
title: >-
    شمارهٔ ۶۱۲
---
# شمارهٔ ۶۱۲

<div class="b" id="bn1"><div class="m1"><p>گر طالب فیض آب انگور شوی</p></div>
<div class="m2"><p>می نوش نه خمر کز صفا دور شوی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می یک دو دم است بعد از آن مست شراب</p></div>
<div class="m2"><p>خمرست گهی که مست و مخمور شوی</p></div></div>