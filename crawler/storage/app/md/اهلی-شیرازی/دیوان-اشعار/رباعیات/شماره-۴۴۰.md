---
title: >-
    شمارهٔ ۴۴۰
---
# شمارهٔ ۴۴۰

<div class="b" id="bn1"><div class="m1"><p>دهقان چو بخاک افکند تخم ضعیف</p></div>
<div class="m2"><p>یک صد شود از لطف خداوند لطیف</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ده یک چونمیدهد که تخم افزون به « کذا »</p></div>
<div class="m2"><p>طوفان فنا برافکند تخم خریف</p></div></div>