---
title: >-
    شمارهٔ ۵۲۹
---
# شمارهٔ ۵۲۹

<div class="b" id="bn1"><div class="m1"><p>ما گریه کنان بر سر خاک پدران</p></div>
<div class="m2"><p>زین غم که شدند ازین جهان گذران</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در زیر زمین هم پدران میگریند</p></div>
<div class="m2"><p>بر تلخی این زندگی ما پسران</p></div></div>