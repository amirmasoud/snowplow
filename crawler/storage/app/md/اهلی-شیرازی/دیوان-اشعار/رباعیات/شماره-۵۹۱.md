---
title: >-
    شمارهٔ ۵۹۱
---
# شمارهٔ ۵۹۱

<div class="b" id="bn1"><div class="m1"><p>گر حج نکنی کجا بجایی برسی</p></div>
<div class="m2"><p>کی تا نروی برهنمایی برسی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حق در همه جاست کعبه زانست که تو</p></div>
<div class="m2"><p>سعیی ببری تا بصفایی برسی</p></div></div>