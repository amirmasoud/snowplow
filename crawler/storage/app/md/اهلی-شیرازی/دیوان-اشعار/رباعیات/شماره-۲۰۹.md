---
title: >-
    شمارهٔ ۲۰۹
---
# شمارهٔ ۲۰۹

<div class="b" id="bn1"><div class="m1"><p>اهلی که ز سودای بتان سود ندید</p></div>
<div class="m2"><p>از آتش سودا بجز از دود ندید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر سرو که پرورد بخون دل خون</p></div>
<div class="m2"><p>از نخل قدش میوه مقصود ندید</p></div></div>