---
title: >-
    شمارهٔ ۴۶۹
---
# شمارهٔ ۴۶۹

<div class="b" id="bn1"><div class="m1"><p>چل سال بوادی طلب افتادم</p></div>
<div class="m2"><p>بستم کمر شوق و قدم بگشادم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از هر طرفی که ره بجایی بردم</p></div>
<div class="m2"><p>بهر دگران نشانه یی بنهادم</p></div></div>