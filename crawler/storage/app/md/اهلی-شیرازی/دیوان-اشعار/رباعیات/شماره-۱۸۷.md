---
title: >-
    شمارهٔ ۱۸۷
---
# شمارهٔ ۱۸۷

<div class="b" id="bn1"><div class="m1"><p>عیش و طرب جهان فانی همه هیچ</p></div>
<div class="m2"><p>وین گفت و شنید و نکته دانی همه هیچ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گیرم که هزار سال مانی بمراد</p></div>
<div class="m2"><p>یکروز که نامراد مانی همه هیچ</p></div></div>