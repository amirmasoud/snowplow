---
title: >-
    شمارهٔ ۳۳۲
---
# شمارهٔ ۳۳۲

<div class="b" id="bn1"><div class="m1"><p>بیگانه وشی که دیر دیرش بینند</p></div>
<div class="m2"><p>به زانکه بدوستی دلیرش بینند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دریاب که گل بتازه روییست عزیز</p></div>
<div class="m2"><p>خوارست بهفته یی که سیرش بینند</p></div></div>