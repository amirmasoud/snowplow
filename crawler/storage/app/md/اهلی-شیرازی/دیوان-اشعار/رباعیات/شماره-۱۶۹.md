---
title: >-
    شمارهٔ ۱۶۹
---
# شمارهٔ ۱۶۹

<div class="b" id="bn1"><div class="m1"><p>ایطالب کعبه گر ترا میل صفاست</p></div>
<div class="m2"><p>دریاب که استطاعت پای رواست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در خانه چه پا شکسته مانی برخیز</p></div>
<div class="m2"><p>کز کعبه بعد ز لنگ ماندن نه رواست</p></div></div>