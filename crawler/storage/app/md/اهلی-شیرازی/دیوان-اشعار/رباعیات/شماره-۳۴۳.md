---
title: >-
    شمارهٔ ۳۴۳
---
# شمارهٔ ۳۴۳

<div class="b" id="bn1"><div class="m1"><p>در راه حق انبیا چو انجم گشتند</p></div>
<div class="m2"><p>یعنی همه رهنمای مردم گشتند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خورشید عرب چو سر زد از مشرق غیب</p></div>
<div class="m2"><p>ایشان همه در ظهور او گم گشتند</p></div></div>