---
title: >-
    شمارهٔ ۱۳۷
---
# شمارهٔ ۱۳۷

<div class="b" id="bn1"><div class="m1"><p>ای خفته که راه بیخودی پیشه تست</p></div>
<div class="m2"><p>شیطان غرور در رگ و ریشه تست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>برخیز که آنکه در بر تست بخواب</p></div>
<div class="m2"><p>هیچست همین صورت اندیشه تست</p></div></div>