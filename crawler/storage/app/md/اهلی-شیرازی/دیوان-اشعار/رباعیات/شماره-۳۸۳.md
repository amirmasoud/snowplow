---
title: >-
    شمارهٔ ۳۸۳
---
# شمارهٔ ۳۸۳

<div class="b" id="bn1"><div class="m1"><p>هر کس که بوقت عقد زن نار خرید</p></div>
<div class="m2"><p>بیچاره بزر خانه برانداز خرید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کابین زن ایخواجه چنان کن که بزر</p></div>
<div class="m2"><p>خود را بتوان ز بندگی باز خرید</p></div></div>