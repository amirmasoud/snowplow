---
title: >-
    شمارهٔ ۲۰۵
---
# شمارهٔ ۲۰۵

<div class="b" id="bn1"><div class="m1"><p>با همچو منی که همسخن خواهد بود</p></div>
<div class="m2"><p>من خاک رهم که یار من خواهد بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اهلی مطلب پنبه داغ دل خویش</p></div>
<div class="m2"><p>کاین پنبه نصیب در کفن خواهد بود</p></div></div>