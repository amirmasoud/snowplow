---
title: >-
    شمارهٔ ۱۹۲
---
# شمارهٔ ۱۹۲

<div class="b" id="bn1"><div class="m1"><p>از کوشش من بمن نوایی نرسد</p></div>
<div class="m2"><p>وز نخل تو ام بر وفایی نرسد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دستی که ز شاخ بخت کوتاه بود</p></div>
<div class="m2"><p>هر چند که بر جهد بجایی نرسد</p></div></div>