---
title: >-
    شمارهٔ ۳۶۰
---
# شمارهٔ ۳۶۰

<div class="b" id="bn1"><div class="m1"><p>آنانکه ز شاخ بخت برخوردارند</p></div>
<div class="m2"><p>پیوسته بعذر دوستان درکارند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سروی ز برای آن نشانند کسان</p></div>
<div class="m2"><p>کز سایه آن تمتعی بردارند</p></div></div>