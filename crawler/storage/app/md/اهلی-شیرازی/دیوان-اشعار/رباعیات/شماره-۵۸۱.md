---
title: >-
    شمارهٔ ۵۸۱
---
# شمارهٔ ۵۸۱

<div class="b" id="bn1"><div class="m1"><p>از عامه مشو ملول اگر با درکی</p></div>
<div class="m2"><p>کو واسطه گشت تا تو صاحب ترکی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر حاصل شاخ جمله میبود ثمر</p></div>
<div class="m2"><p>میسوخت شجر ز غایت بی برگی</p></div></div>