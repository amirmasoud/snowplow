---
title: >-
    شمارهٔ ۴۴۵
---
# شمارهٔ ۴۴۵

<div class="b" id="bn1"><div class="m1"><p>فرزند بخوی و بوی خودساز بزرگ</p></div>
<div class="m2"><p>مگذار بصحبتش چو تاجیک چه ترک</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در ظاهر کس مبین که بسیار کسی</p></div>
<div class="m2"><p>با صورت یوسف است و با سیرت گرگ</p></div></div>