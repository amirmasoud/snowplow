---
title: >-
    شمارهٔ ۱۱۱
---
# شمارهٔ ۱۱۱

<div class="b" id="bn1"><div class="m1"><p>عمرم که بگفتگو درین خانه گذشت</p></div>
<div class="m2"><p>یکچند بوصف چشم مستانه گذشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یکچند بذکر جام و پیمانه گذشت</p></div>
<div class="m2"><p>القصه شب عمر بافسانه گذشت</p></div></div>