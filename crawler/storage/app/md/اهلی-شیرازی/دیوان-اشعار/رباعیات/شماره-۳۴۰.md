---
title: >-
    شمارهٔ ۳۴۰
---
# شمارهٔ ۳۴۰

<div class="b" id="bn1"><div class="m1"><p>پروانه تر از شمع روشن طلبد</p></div>
<div class="m2"><p>موسی ز چراغ نخل ایمن طلبد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نقد تو برون ز گنج دل نیست ولی</p></div>
<div class="m2"><p>از دل طلبم من و دل از من طلبد</p></div></div>