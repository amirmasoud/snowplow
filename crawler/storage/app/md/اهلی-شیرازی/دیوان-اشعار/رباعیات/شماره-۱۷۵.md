---
title: >-
    شمارهٔ ۱۷۵
---
# شمارهٔ ۱۷۵

<div class="b" id="bn1"><div class="m1"><p>جوری ز زمانه بر بد اندیش نرفت</p></div>
<div class="m2"><p>تا ظلمی از او بر سر درویش نرفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر کس که جفا کرد جفا دید جزا</p></div>
<div class="m2"><p>باعدل خدا ظلم کس از پیش نرفت</p></div></div>