---
title: >-
    شمارهٔ ۲۹۳
---
# شمارهٔ ۲۹۳

<div class="b" id="bn1"><div class="m1"><p>هر صحبت مستیی خماری دارد</p></div>
<div class="m2"><p>هر باغ خزانی و بهاری دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گنجیست چمن ز گل پر از زر لیکن</p></div>
<div class="m2"><p>در هر بن خار خفته ماری دارد</p></div></div>