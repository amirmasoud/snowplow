---
title: >-
    شمارهٔ ۲۷۶
---
# شمارهٔ ۲۷۶

<div class="b" id="bn1"><div class="m1"><p>در عشق اساس چاره کردن نبود</p></div>
<div class="m2"><p>کس سیریش از نظاره کردن نبود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در بحر غم تو دست و پا چند زنم</p></div>
<div class="m2"><p>چون چاره بجز کناره کردن نبود</p></div></div>