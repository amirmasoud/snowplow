---
title: >-
    شمارهٔ ۴۱۰
---
# شمارهٔ ۴۱۰

<div class="b" id="bn1"><div class="m1"><p>ای نخل جوان درین چمن با گل و خار</p></div>
<div class="m2"><p>چون سبزه مکن زبان درازی زنهار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سر نه چون سمن در قدم سرو و چنار</p></div>
<div class="m2"><p>تا پیر شوی حرمت پیران میدار</p></div></div>