---
title: >-
    شمارهٔ ۵۷۲
---
# شمارهٔ ۵۷۲

<div class="b" id="bn1"><div class="m1"><p>ای محرم راز دل چه بیگانه وشی</p></div>
<div class="m2"><p>تا چند برنگ و بوی این باغ خوشی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در گوش تو معرفت شود ناله مرغ</p></div>
<div class="m2"><p>چون خطی اگر فتیله از گوش کشی</p></div></div>