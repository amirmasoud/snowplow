---
title: >-
    شمارهٔ ۴۴۸
---
# شمارهٔ ۴۴۸

<div class="b" id="bn1"><div class="m1"><p>اهلی ز جهانیان چو مجنون بگسل</p></div>
<div class="m2"><p>وین دام و ددان بهم در افتاده بهل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با طرفه غزالان سیه چشم نشین</p></div>
<div class="m2"><p>کارایش عالمند و آسایش دل</p></div></div>