---
title: >-
    شمارهٔ ۱۲۸
---
# شمارهٔ ۱۲۸

<div class="b" id="bn1"><div class="m1"><p>بخشش نبود جز صفت حضرت دوست</p></div>
<div class="m2"><p>آزاده کسی که بخشش عادت و خوست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در صورت بندگی خداوند بود</p></div>
<div class="m2"><p>هر بنده که سیرت خداوند در اوست</p></div></div>