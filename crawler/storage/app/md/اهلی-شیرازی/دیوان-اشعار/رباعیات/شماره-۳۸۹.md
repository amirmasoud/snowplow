---
title: >-
    شمارهٔ ۳۸۹
---
# شمارهٔ ۳۸۹

<div class="b" id="bn1"><div class="m1"><p>ایخواجه ز کف رسوم حکمت مگذار</p></div>
<div class="m2"><p>تا باده بود ز دست فرصت مگذار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با علت پیریت طبیعت چکند</p></div>
<div class="m2"><p>می در کش و علت طبیعت مگذار</p></div></div>