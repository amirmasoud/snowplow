---
title: >-
    شمارهٔ ۱۶۸
---
# شمارهٔ ۱۶۸

<div class="b" id="bn1"><div class="m1"><p>شیریست درنده نفس سرکش که تراست</p></div>
<div class="m2"><p>گیر سیر بود ترا بر او حکم کجاست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون گرسنه شد زروزه فرمانبر تست</p></div>
<div class="m2"><p>پس سیر مشو که سیری نفس خطاست</p></div></div>