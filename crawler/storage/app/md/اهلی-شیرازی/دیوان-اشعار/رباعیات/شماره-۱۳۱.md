---
title: >-
    شمارهٔ ۱۳۱
---
# شمارهٔ ۱۳۱

<div class="b" id="bn1"><div class="m1"><p>ممسک بجهان به بیش و کم محرومست</p></div>
<div class="m2"><p>با گنج زر از فیض درم محروم است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در شیره جان او بود لذت بخل</p></div>
<div class="m2"><p>بیچاره ز لذت کرم محروم است</p></div></div>