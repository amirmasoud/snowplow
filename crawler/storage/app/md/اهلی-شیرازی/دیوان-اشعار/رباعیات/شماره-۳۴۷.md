---
title: >-
    شمارهٔ ۳۴۷
---
# شمارهٔ ۳۴۷

<div class="b" id="bn1"><div class="m1"><p>ما را ز حسین تشنه چون یاد</p></div>
<div class="m2"><p>از هر بن مو هزار فریاد آید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آیا فلک این تحملش بود کزو</p></div>
<div class="m2"><p>بر آل علی اینهمه بیداد آید</p></div></div>