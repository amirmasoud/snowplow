---
title: >-
    شمارهٔ ۳۷۴
---
# شمارهٔ ۳۷۴

<div class="b" id="bn1"><div class="m1"><p>ظالم بقیامت اعتقادش نبود</p></div>
<div class="m2"><p>بیدار کند که بیم دادش نبود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر کس که ز عدل داور اندیشه کند</p></div>
<div class="m2"><p>اندیشه ظلم در نهادش نبود</p></div></div>