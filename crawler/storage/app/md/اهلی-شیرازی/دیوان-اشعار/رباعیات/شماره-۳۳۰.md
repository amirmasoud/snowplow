---
title: >-
    شمارهٔ ۳۳۰
---
# شمارهٔ ۳۳۰

<div class="b" id="bn1"><div class="m1"><p>گردون تن ما چه خانه جان میکرد</p></div>
<div class="m2"><p>از بهر چه ساخت چونکه ویران میکرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اینخاک چه گل کرد؟ که هم خاک شود</p></div>
<div class="m2"><p>وین جمع چه ساخت چون پریشان میکرد</p></div></div>