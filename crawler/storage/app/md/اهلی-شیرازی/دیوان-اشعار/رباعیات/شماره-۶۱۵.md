---
title: >-
    شمارهٔ ۶۱۵
---
# شمارهٔ ۶۱۵

<div class="b" id="bn1"><div class="m1"><p>جان در غم یوسف چو زلیخا نکنی</p></div>
<div class="m2"><p>یعقوب صفت نه مرد بیت الحزنی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در معرکه عشق چه نامند ترا</p></div>
<div class="m2"><p>کاندر صف عشاق نه مردی نه زنی</p></div></div>