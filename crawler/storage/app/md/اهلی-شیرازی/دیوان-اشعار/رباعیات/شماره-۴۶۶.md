---
title: >-
    شمارهٔ ۴۶۶
---
# شمارهٔ ۴۶۶

<div class="b" id="bn1"><div class="m1"><p>از بسکه زناکسان پریشان شده ام</p></div>
<div class="m2"><p>از صحبت نیک و بد گریزان شده ام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من بعد هوای صحبت کس نکنم</p></div>
<div class="m2"><p>کز آنچه گذشت هم پشیمان شده ام</p></div></div>