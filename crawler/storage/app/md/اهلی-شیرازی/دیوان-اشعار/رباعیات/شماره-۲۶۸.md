---
title: >-
    شمارهٔ ۲۶۸
---
# شمارهٔ ۲۶۸

<div class="b" id="bn1"><div class="m1"><p>عیش و طرب از مایه هستی خیزد</p></div>
<div class="m2"><p>هستی گل از فراخ دستی خیزد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در ساغر لاله جرعه خون دلست</p></div>
<div class="m2"><p>پیداست کزین جرعه چه مستی خیزد</p></div></div>