---
title: >-
    شمارهٔ ۷۷
---
# شمارهٔ ۷۷

<div class="b" id="bn1"><div class="m1"><p>ای غایت هر هنر که در آدم هست</p></div>
<div class="m2"><p>مقصود منی ز هر چه در عالم هست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من خسته دلم ز خدمت گر دورم</p></div>
<div class="m2"><p>از ضعف تنست و ضعف طالع هم هست</p></div></div>