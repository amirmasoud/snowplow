---
title: >-
    شمارهٔ ۶
---
# شمارهٔ ۶

<div class="b" id="bn1"><div class="m1"><p>گر درد دل از غم حبیب است مرا</p></div>
<div class="m2"><p>غم نیست که هم غمش طبیب است مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از فخر جهانیان مرا عار بود</p></div>
<div class="m2"><p>گر فقر محمدی نصیب است مرا</p></div></div>