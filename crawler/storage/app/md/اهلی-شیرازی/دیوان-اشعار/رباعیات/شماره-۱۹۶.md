---
title: >-
    شمارهٔ ۱۹۶
---
# شمارهٔ ۱۹۶

<div class="b" id="bn1"><div class="m1"><p>زاهد که اساس دین برونق طلبد</p></div>
<div class="m2"><p>بر مست بتان خواری مطلق طلبد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر بنگرد آن صنم که مستان دارند</p></div>
<div class="m2"><p>آنرا بنماز و حاجت از حق طلبد</p></div></div>