---
title: >-
    شمارهٔ ۴۸۳
---
# شمارهٔ ۴۸۳

<div class="b" id="bn1"><div class="m1"><p>گر از دهنت روایتی میگویم</p></div>
<div class="m2"><p>زان لب سخن از عنایتی میگویم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>منعم مکن ای پری که من با دل خود</p></div>
<div class="m2"><p>دیوانه صفت حکایتی میگویم</p></div></div>