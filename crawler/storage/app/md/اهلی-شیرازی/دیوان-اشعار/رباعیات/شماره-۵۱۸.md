---
title: >-
    شمارهٔ ۵۱۸
---
# شمارهٔ ۵۱۸

<div class="b" id="bn1"><div class="m1"><p>زنهار که بد گفتن کس ورد مکن</p></div>
<div class="m2"><p>وین آتش شر قرین گوگرد مکن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دانی که همیشه میجهد برق بلا</p></div>
<div class="m2"><p>پس خر من فتنه گرد خود گردم مکن</p></div></div>