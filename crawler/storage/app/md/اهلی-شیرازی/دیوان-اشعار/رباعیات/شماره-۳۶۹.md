---
title: >-
    شمارهٔ ۳۶۹
---
# شمارهٔ ۳۶۹

<div class="b" id="bn1"><div class="m1"><p>سلطان که خراج بر رعیت فکند</p></div>
<div class="m2"><p>باید که بظلم بیخ مردم نکند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر چند که شاخ بایدش بار کشید</p></div>
<div class="m2"><p>چون بارگران بود بر آن میشکند</p></div></div>