---
title: >-
    شمارهٔ ۱۷۶
---
# شمارهٔ ۱۷۶

<div class="b" id="bn1"><div class="m1"><p>کج دست که در مال کسانش نظر است</p></div>
<div class="m2"><p>دستش ببرند اگر چه با صد هنرست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دستی که شد از کجی در این باغ دراز</p></div>
<div class="m2"><p>چون شاخ رزش برند اگر شاخ ز دست</p></div></div>