---
title: >-
    شمارهٔ ۴۶۱
---
# شمارهٔ ۴۶۱

<div class="b" id="bn1"><div class="m1"><p>گر در همه آفاق بگردی چو نسیم</p></div>
<div class="m2"><p>کار همه کس بسنجی از طبع سلیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دشوار تر از سؤال کاری نبود</p></div>
<div class="m2"><p>خواهی ز کریم خواه و خواهی ز لییم</p></div></div>