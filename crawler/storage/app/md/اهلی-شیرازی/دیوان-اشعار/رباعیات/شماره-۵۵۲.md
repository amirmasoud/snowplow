---
title: >-
    شمارهٔ ۵۵۲
---
# شمارهٔ ۵۵۲

<div class="b" id="bn1"><div class="m1"><p>هرکس که هوای نفس شد پیشه او</p></div>
<div class="m2"><p>صد خار بلا روید از اندیشه او</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مگذار که این خار بلا پهن شود</p></div>
<div class="m2"><p>برکن ز زمین دل رگ و ریشه او</p></div></div>