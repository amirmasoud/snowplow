---
title: >-
    شمارهٔ ۴۲۵
---
# شمارهٔ ۴۲۵

<div class="b" id="bn1"><div class="m1"><p>یارب خجلم چنان ز آلایش خویش</p></div>
<div class="m2"><p>کز خلد طمع ندارم آسایش خویش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>باشد که تو از کمال رحمت شویی</p></div>
<div class="m2"><p>آلایش ما ز ابر بخشایش خویش</p></div></div>