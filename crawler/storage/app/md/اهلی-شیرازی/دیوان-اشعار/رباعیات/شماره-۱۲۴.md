---
title: >-
    شمارهٔ ۱۲۴
---
# شمارهٔ ۱۲۴

<div class="b" id="bn1"><div class="m1"><p>کشتیست دو کون و ما تماشا گر کشت</p></div>
<div class="m2"><p>از بهر تماشاست چه زیبا و چه زشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در دانه اگر طمع نکردی آدم</p></div>
<div class="m2"><p>کی رانده شدی بخواری از باغ بهشت</p></div></div>