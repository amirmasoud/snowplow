---
title: >-
    شمارهٔ ۲۹۷
---
# شمارهٔ ۲۹۷

<div class="b" id="bn1"><div class="m1"><p>خاکی که چو گرد گرد درها گردد</p></div>
<div class="m2"><p>هیهات که سرمه نظرها گردد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سنگ سیهی که زیر پا خاک ره است</p></div>
<div class="m2"><p>لعلی بشود که تاج سرها گردد</p></div></div>