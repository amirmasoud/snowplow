---
title: >-
    شمارهٔ ۸۲
---
# شمارهٔ ۸۲

<div class="b" id="bn1"><div class="m1"><p>گردون طبق از بلندی همت بست</p></div>
<div class="m2"><p>شد خاک زبون پستی از همت پست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پستی و بلندی همه از همت ماست</p></div>
<div class="m2"><p>وابسته به همت است هر چیز که هست</p></div></div>