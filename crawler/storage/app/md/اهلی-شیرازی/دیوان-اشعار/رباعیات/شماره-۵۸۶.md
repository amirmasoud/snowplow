---
title: >-
    شمارهٔ ۵۸۶
---
# شمارهٔ ۵۸۶

<div class="b" id="bn1"><div class="m1"><p>من گر چه نمیزنم دم از دانایی</p></div>
<div class="m2"><p>صاحبنظرم بدیده بینایی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عیش و مزه جهان چشیدم همه عمر</p></div>
<div class="m2"><p>در هیچ نبود لذت تنهایی</p></div></div>