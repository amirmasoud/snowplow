---
title: >-
    شمارهٔ ۱۲۲
---
# شمارهٔ ۱۲۲

<div class="b" id="bn1"><div class="m1"><p>یک گربه ده و بصد « همی » منصرف است</p></div>
<div class="m2"><p>در اصل یکیست این سخن منکشف است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از کثرت صفر اگر الف گشت هزار</p></div>
<div class="m2"><p>الف راست بصورت و بمعنی الف است</p></div></div>