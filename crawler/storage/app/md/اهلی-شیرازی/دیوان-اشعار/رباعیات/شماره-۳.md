---
title: >-
    شمارهٔ ۳
---
# شمارهٔ ۳

<div class="b" id="bn1"><div class="m1"><p>ای همچو گهر در صدف سینه ما</p></div>
<div class="m2"><p>هر گوهر راز تست گنجینه ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حقا که غرض ز هستی ما نبود</p></div>
<div class="m2"><p>جز جلوه حسن تو در آیینه ما</p></div></div>