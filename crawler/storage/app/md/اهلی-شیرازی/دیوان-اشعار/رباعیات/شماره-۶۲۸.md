---
title: >-
    شمارهٔ ۶۲۸
---
# شمارهٔ ۶۲۸

<div class="b" id="bn1"><div class="m1"><p>گر با همه کس راست روی پیشه کنی</p></div>
<div class="m2"><p>فردوس چو شیران خدا بیشه کنی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر نیک و بدی که با کسی خواهی کرد</p></div>
<div class="m2"><p>باید همه در شان خود اندیشه کنی</p></div></div>