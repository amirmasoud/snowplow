---
title: >-
    شمارهٔ ۱۹
---
# شمارهٔ ۱۹

<div class="b" id="bn1"><div class="m1"><p>عذرا بجفا اگر کشد وامق را</p></div>
<div class="m2"><p>جان نیست دریغ عاشق صادق را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گو یار ز عاشقان وفا جوی بسر</p></div>
<div class="m2"><p>کان خود به سر نیزه بود عاشق را</p></div></div>