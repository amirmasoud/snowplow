---
title: >-
    شمارهٔ ۴۸۰
---
# شمارهٔ ۴۸۰

<div class="b" id="bn1"><div class="m1"><p>دستم نرسد بوصل آنمه چکنم؟</p></div>
<div class="m2"><p>با نخل بلند و دست کوته چکنم؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>درمانده دام محنت از دست دلم</p></div>
<div class="m2"><p>آزادی خود نمیبرم ره چکنم؟</p></div></div>