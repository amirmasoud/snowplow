---
title: >-
    شمارهٔ ۴۰۸
---
# شمارهٔ ۴۰۸

<div class="b" id="bn1"><div class="m1"><p>از پرسش دوستان قدم باز مگیر</p></div>
<div class="m2"><p>وندر خور دسترس کرم باز مگیر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا خورده زر غنچه صفت در کف تست</p></div>
<div class="m2"><p>از خلق جهان ز بیش و کم باز مگیر</p></div></div>