---
title: >-
    شمارهٔ ۵۵۱
---
# شمارهٔ ۵۵۱

<div class="b" id="bn1"><div class="m1"><p>گر سفله غنی شود چه جود آید ازو</p></div>
<div class="m2"><p>او جمله زیان است چه سود آید ازو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گویی ز وجود آن خسیس این غرضست</p></div>
<div class="m2"><p>کافعال خسیسه در وجود آید ازو</p></div></div>