---
title: >-
    شمارهٔ ۳۵۲
---
# شمارهٔ ۳۵۲

<div class="b" id="bn1"><div class="m1"><p>گر مال خدا دهی تباهی ببرد</p></div>
<div class="m2"><p>ور نی همه را قهر الهی ببرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر یک بره قربان نکنی صد بره ات</p></div>
<div class="m2"><p>گرگ از گله ات خواهی نخواهی ببرد</p></div></div>