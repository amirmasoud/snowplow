---
title: >-
    شمارهٔ ۵۴
---
# شمارهٔ ۵۴

<div class="b" id="bn1"><div class="m1"><p>مجنون که ز بیم طعنه از خلق جداست</p></div>
<div class="m2"><p>حال که بود میانه او و خداست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رسوایی او ز چشم خلق است نهان</p></div>
<div class="m2"><p>رسوایی من میانه خلق بلاست</p></div></div>