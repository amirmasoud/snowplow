---
title: >-
    شمارهٔ ۵۶
---
# شمارهٔ ۵۶

<div class="b" id="bn1"><div class="m1"><p>در مستی اگر کسی نکویا نه نکوست</p></div>
<div class="m2"><p>از می نبود نیک و بد از عادت و خوست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می آتش محض است و چون آتش افروخت</p></div>
<div class="m2"><p>در هر چه فتد همان دهد بو که دروست</p></div></div>