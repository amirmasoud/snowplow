---
title: >-
    شمارهٔ ۲۳۴
---
# شمارهٔ ۲۳۴

<div class="b" id="bn1"><div class="m1"><p>هر چند دلم بوصل فایز نشود</p></div>
<div class="m2"><p>در داغ فراق دوست عاجز نشود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر چند که سوزمش بغم خامترست</p></div>
<div class="m2"><p>خامیست دلم که پخته هرگز نشود</p></div></div>