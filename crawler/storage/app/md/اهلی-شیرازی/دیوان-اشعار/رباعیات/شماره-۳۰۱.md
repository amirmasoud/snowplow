---
title: >-
    شمارهٔ ۳۰۱
---
# شمارهٔ ۳۰۱

<div class="b" id="bn1"><div class="m1"><p>آن خواجه گرش گاه بخرمن نقتد</p></div>
<div class="m2"><p>کس را ز کفش جوی بدامن نفتد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر افکنیش ز شاخ طوبی بزمین</p></div>
<div class="m2"><p>چون غنچه ز مشت او یک ارزن نفتد</p></div></div>