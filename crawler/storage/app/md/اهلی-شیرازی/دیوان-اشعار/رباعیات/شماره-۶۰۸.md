---
title: >-
    شمارهٔ ۶۰۸
---
# شمارهٔ ۶۰۸

<div class="b" id="bn1"><div class="m1"><p>آن گل که بنازکی ندارد بدلی</p></div>
<div class="m2"><p>بر دامن او مباد خار و خللی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روزی سوی من آید و روزی سوی غیر</p></div>
<div class="m2"><p>هر روز برآید آفتاب از محلی</p></div></div>