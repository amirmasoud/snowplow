---
title: >-
    شمارهٔ ۲۲۹
---
# شمارهٔ ۲۲۹

<div class="b" id="bn1"><div class="m1"><p>می خور که حیات جاودانت بدهند</p></div>
<div class="m2"><p>وز صد چو سکندری امانت بدهند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دریاب حیات نقد و ضایع مگذار</p></div>
<div class="m2"><p>کاینت برود ز دست و آنت ندهند</p></div></div>