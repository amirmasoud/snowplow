---
title: >-
    شمارهٔ ۵۱۴
---
# شمارهٔ ۵۱۴

<div class="b" id="bn1"><div class="m1"><p>در خانه تن مشو دلا خانه نشین</p></div>
<div class="m2"><p>گر خانه پر از گل است چون خلد برین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گلریزی جام خانه از خورشیدست</p></div>
<div class="m2"><p>از خان برون خرام و خورشید ببین</p></div></div>