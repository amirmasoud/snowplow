---
title: >-
    شمارهٔ ۱۹۵
---
# شمارهٔ ۱۹۵

<div class="b" id="bn1"><div class="m1"><p>تا کی رطب لبت باغیار رسد</p></div>
<div class="m2"><p>وز نخل قدت نصیب ما خار رسد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>لعل تو که خورد خون من حق منست</p></div>
<div class="m2"><p>روزی برسد که حق بحقدار رسد</p></div></div>