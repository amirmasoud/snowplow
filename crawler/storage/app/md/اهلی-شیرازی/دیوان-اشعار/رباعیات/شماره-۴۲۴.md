---
title: >-
    شمارهٔ ۴۲۴
---
# شمارهٔ ۴۲۴

<div class="b" id="bn1"><div class="m1"><p>بیمار و جز تو یاریم نیست ز کس</p></div>
<div class="m2"><p>در زندگیم نمانده جز یکدو نفس</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با این دو نفس که باقی از عمر منست</p></div>
<div class="m2"><p>در هر نفسم هزار مرگست هوس</p></div></div>