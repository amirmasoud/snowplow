---
title: >-
    شمارهٔ ۱۶۷
---
# شمارهٔ ۱۶۷

<div class="b" id="bn1"><div class="m1"><p>هر سجده که میکنی پی خدمت دوست</p></div>
<div class="m2"><p>گاهی که بترتیب کنی لایق اوست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تنها نه که ترتیب بود شرط نماز</p></div>
<div class="m2"><p>هر کار که میکنی به ترتیب نکوست</p></div></div>