---
title: >-
    شمارهٔ ۳۹۸
---
# شمارهٔ ۳۹۸

<div class="b" id="bn1"><div class="m1"><p>گر راز دلت نگفته باشد بهتر</p></div>
<div class="m2"><p>کس حال تو کم شنفته باشد بهتر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زنهار که هر چه میکنی پنهان کن</p></div>
<div class="m2"><p>طاعت هم اگر نهفته باشد بهتر</p></div></div>