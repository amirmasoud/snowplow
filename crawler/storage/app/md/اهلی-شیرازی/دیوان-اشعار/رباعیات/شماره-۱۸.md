---
title: >-
    شمارهٔ ۱۸
---
# شمارهٔ ۱۸

<div class="b" id="bn1"><div class="m1"><p>ای تنگ شکر، دهن بگفتن بگشا</p></div>
<div class="m2"><p>وانگه بگدا بگو که دامن بگشا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا چند بود قفل غمم بر در دل</p></div>
<div class="m2"><p>این قفل غم از در دل من بگشا</p></div></div>