---
title: >-
    شمارهٔ ۱۴۴
---
# شمارهٔ ۱۴۴

<div class="b" id="bn1"><div class="m1"><p>برخاست میان بسته دلم چابک و چست</p></div>
<div class="m2"><p>میخواست که کار خود کند جمله درست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون کرد به بیوفایی عمر نگاه</p></div>
<div class="m2"><p>کنجی بنشست و از جهان دست بشست</p></div></div>