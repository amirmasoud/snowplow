---
title: >-
    شمارهٔ ۶۲۱
---
# شمارهٔ ۶۲۱

<div class="b" id="bn1"><div class="m1"><p>ساقی نظری به بینوایی باری</p></div>
<div class="m2"><p>گر باده نمیدهی صلایی باری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>درمان منست یک نگه چون نکنی</p></div>
<div class="m2"><p>از نیم نگه نیم دوایی باری</p></div></div>