---
title: >-
    شمارهٔ ۵۶۶
---
# شمارهٔ ۵۶۶

<div class="b" id="bn1"><div class="m1"><p>ما میوه این چمن چشیدیم همه</p></div>
<div class="m2"><p>ور بار دلی بود کشیدیم همه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دیدیم هر آنچه دیدنی بود دلا</p></div>
<div class="m2"><p>نادیده همان گیر که دیدیم همه</p></div></div>