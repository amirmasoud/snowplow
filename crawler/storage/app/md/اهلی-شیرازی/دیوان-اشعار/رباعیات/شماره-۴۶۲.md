---
title: >-
    شمارهٔ ۴۶۲
---
# شمارهٔ ۴۶۲

<div class="b" id="bn1"><div class="m1"><p>صاحب هنری را که بود طبع سلیم</p></div>
<div class="m2"><p>خاک ره خلق باشد از خلق کریم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر مشک شوی دماغ خشکی مفروش</p></div>
<div class="m2"><p>مانند بنفشه باش مسکین و حلیم</p></div></div>