---
title: >-
    شمارهٔ ۵۴۷
---
# شمارهٔ ۵۴۷

<div class="b" id="bn1"><div class="m1"><p>ساقی، چو چراغ سحرم زار و زر بون</p></div>
<div class="m2"><p>چشمم بعنایت تو بازست کنون</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خیز از می کهنه روغنی کن به چراغ</p></div>
<div class="m2"><p>دریاب و گر نه رفتم از دست برون</p></div></div>