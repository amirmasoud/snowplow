---
title: >-
    شمارهٔ ۲۱۷
---
# شمارهٔ ۲۱۷

<div class="b" id="bn1"><div class="m1"><p>گر پیش تو سر ز کشتن افتد به سجود</p></div>
<div class="m2"><p>رنجیده مشو ز دامن خون آلود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر کس که بخون در افکند بیگنهی</p></div>
<div class="m2"><p>بر دامن او ترشحی خواهد بود</p></div></div>