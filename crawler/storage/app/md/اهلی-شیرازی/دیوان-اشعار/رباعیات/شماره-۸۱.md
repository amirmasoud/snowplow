---
title: >-
    شمارهٔ ۸۱
---
# شمارهٔ ۸۱

<div class="b" id="bn1"><div class="m1"><p>در وادی عشق کز صفت بیرونست</p></div>
<div class="m2"><p>حال دل عاشقان چه پرسی خونست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در هر پس سنگ کشته صد کوهکن است</p></div>
<div class="m2"><p>در هر بن خار مرده صد مجنونست</p></div></div>