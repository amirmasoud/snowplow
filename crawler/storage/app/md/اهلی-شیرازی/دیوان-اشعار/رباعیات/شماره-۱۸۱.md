---
title: >-
    شمارهٔ ۱۸۱
---
# شمارهٔ ۱۸۱

<div class="b" id="bn1"><div class="m1"><p>پیش تو مرا شرم و حیا خواهد کشت</p></div>
<div class="m2"><p>خاموشم و خصمم بجفا خواهد کشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از خاموشیم کار رسیدست بجان</p></div>
<div class="m2"><p>فریاد که خامشی مرا خواهد کشت</p></div></div>