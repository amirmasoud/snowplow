---
title: >-
    شمارهٔ ۴۴۱
---
# شمارهٔ ۴۴۱

<div class="b" id="bn1"><div class="m1"><p>برخیز و طواف کعبه مشمار گزاف</p></div>
<div class="m2"><p>کین رمز کسی نیابد الا دل صاف</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن نور که طوف کعبه را شوق دلست</p></div>
<div class="m2"><p>گر با تو بود کند تو را کعبه طواف</p></div></div>