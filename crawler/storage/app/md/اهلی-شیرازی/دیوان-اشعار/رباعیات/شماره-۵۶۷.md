---
title: >-
    شمارهٔ ۵۶۷
---
# شمارهٔ ۵۶۷

<div class="b" id="bn1"><div class="m1"><p>آن گل که بود رخش گلستان همه</p></div>
<div class="m2"><p>وز دیدن او تازه بود جان همه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر گفت رقیب وصل او حق منست</p></div>
<div class="m2"><p>نومید مشو که حق بود زان همه</p></div></div>