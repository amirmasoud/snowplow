---
title: >-
    شمارهٔ ۲۸۸
---
# شمارهٔ ۲۸۸

<div class="b" id="bn1"><div class="m1"><p>آنان که اساس کار بر خلق نهند</p></div>
<div class="m2"><p>باید که جواب کس بتندی ندهند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خورشید که شمع عالم افروز بود</p></div>
<div class="m2"><p>آن لحظه که گرمی کند از وی بجهند</p></div></div>