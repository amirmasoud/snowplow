---
title: >-
    شمارهٔ ۲۴۸
---
# شمارهٔ ۲۴۸

<div class="b" id="bn1"><div class="m1"><p>هر که سگ تست بر دری ننشیند</p></div>
<div class="m2"><p>غیر از تو بهیچ دلبری ننشیند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر روز چو بلبل نسراید به گلی</p></div>
<div class="m2"><p>هر لحظه بشاخ دیگری ننشیند</p></div></div>