---
title: >-
    شمارهٔ ۱۸۹
---
# شمارهٔ ۱۸۹

<div class="b" id="bn1"><div class="m1"><p>منعم همه مال و کسب زر میداند</p></div>
<div class="m2"><p>زاهد همه اوراد سحر میداند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عارف هنر و معرفت آموخته است</p></div>
<div class="m2"><p>خوش وقت کسی که این هنر میداند</p></div></div>