---
title: >-
    شمارهٔ ۲۲۷
---
# شمارهٔ ۲۲۷

<div class="b" id="bn1"><div class="m1"><p>گر دل ز غبار غم درون پاک کند</p></div>
<div class="m2"><p>وین گرد بلا روی بر افلاک کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در کاخ فلک نگنجد از بسیاری</p></div>
<div class="m2"><p>شاید که دل چرخ فلک چاک کند</p></div></div>