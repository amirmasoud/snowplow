---
title: >-
    شمارهٔ ۵۰۲
---
# شمارهٔ ۵۰۲

<div class="b" id="bn1"><div class="m1"><p>گاهی ز در کعب صفا میجستیم</p></div>
<div class="m2"><p>گه جنت و کوثر از خدا میجستیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مقصود دل از عشق بتان حاصل شد</p></div>
<div class="m2"><p>مقصود چه بود و ما چها میجستیم</p></div></div>