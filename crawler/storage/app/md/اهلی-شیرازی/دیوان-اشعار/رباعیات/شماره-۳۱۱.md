---
title: >-
    شمارهٔ ۳۱۱
---
# شمارهٔ ۳۱۱

<div class="b" id="bn1"><div class="m1"><p>می باده‌فروش داد و زر می‌طلبد</p></div>
<div class="m2"><p>معشوقه نشست و جان و سر می‌طلبد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>برخیز که کام دل ز معشوقه و می</p></div>
<div class="m2"><p>هر چند که می‌دهی دگر می‌طلبد</p></div></div>