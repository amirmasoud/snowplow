---
title: >-
    شمارهٔ ۳۰۹
---
# شمارهٔ ۳۰۹

<div class="b" id="bn1"><div class="m1"><p>می را که خمار در کمین است چه سود</p></div>
<div class="m2"><p>خیرش بهزار شر قرین است چه سود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر سود تنست عقل و دین راست زیان</p></div>
<div class="m2"><p>سودی که زیان عقل و دین است چه سود</p></div></div>