---
title: >-
    شمارهٔ ۳۲۶
---
# شمارهٔ ۳۲۶

<div class="b" id="bn1"><div class="m1"><p>هر چند که خصم کینه ور میباشد</p></div>
<div class="m2"><p>وز عالم مهر بی خبر میباشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در کشتن او حریص تر پیر فلک</p></div>
<div class="m2"><p>پیر از همه کس حریص تر میباشد</p></div></div>