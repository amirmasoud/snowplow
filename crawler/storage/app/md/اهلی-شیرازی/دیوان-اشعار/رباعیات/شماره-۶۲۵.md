---
title: >-
    شمارهٔ ۶۲۵
---
# شمارهٔ ۶۲۵

<div class="b" id="bn1"><div class="m1"><p>اهلی تو که با اهل ریا نزدیکی</p></div>
<div class="m2"><p>از پستی خود بحق کجا نزدیکی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>منصور صفت بلندی از عشق طلب</p></div>
<div class="m2"><p>بردار برآ که با خدا نزدیکی</p></div></div>