---
title: >-
    شمارهٔ ۲۰۲
---
# شمارهٔ ۲۰۲

<div class="b" id="bn1"><div class="m1"><p>هر چند لب تو شربت نوش بود</p></div>
<div class="m2"><p>خامش ز طلب عاشق مدهوش بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون خسته نه درد خود شناسد نه دوا</p></div>
<div class="m2"><p>در پیش طبیب به که خاموش بود</p></div></div>