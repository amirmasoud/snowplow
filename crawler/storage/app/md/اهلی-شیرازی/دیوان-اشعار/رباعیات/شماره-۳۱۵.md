---
title: >-
    شمارهٔ ۳۱۵
---
# شمارهٔ ۳۱۵

<div class="b" id="bn1"><div class="m1"><p>شهوت نهلی که چون شرابت ببرد</p></div>
<div class="m2"><p>برتاب عنان و گرنه تابت ببرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اندازه چو جام عدل از دست مده</p></div>
<div class="m2"><p>افراط مکن و گرنه آبت ببرد</p></div></div>