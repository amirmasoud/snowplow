---
title: >-
    شمارهٔ ۵۹۹
---
# شمارهٔ ۵۹۹

<div class="b" id="bn1"><div class="m1"><p>خوش زی بکسان که بینی از دهر خوشی</p></div>
<div class="m2"><p>ور جور کنی همانقدر جور کشی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نیشی که زنی ترا همان نیش زنند</p></div>
<div class="m2"><p>کان زهر که خود چشانده یی باز چشی</p></div></div>