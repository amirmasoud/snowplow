---
title: >-
    شمارهٔ ۴۶۵
---
# شمارهٔ ۴۶۵

<div class="b" id="bn1"><div class="m1"><p>تا چند بعالم مکرر نگریم</p></div>
<div class="m2"><p>سال و مه و روز و هفته و شب نگریم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا چند خوریم خون دل کین خور شست</p></div>
<div class="m2"><p>گر آب حیات هم بود چند خوریم</p></div></div>