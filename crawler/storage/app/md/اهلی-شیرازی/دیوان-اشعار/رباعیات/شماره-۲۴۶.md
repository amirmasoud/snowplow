---
title: >-
    شمارهٔ ۲۴۶
---
# شمارهٔ ۲۴۶

<div class="b" id="bn1"><div class="m1"><p>مشتاق تو فردوس برین را چکند</p></div>
<div class="m2"><p>وانهار نعیم و حور عین را چکند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سرمست لبت کجا برد کوثر را</p></div>
<div class="m2"><p>مخمور تو شیر و انگبین را چکند</p></div></div>