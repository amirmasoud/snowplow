---
title: >-
    شمارهٔ ۸۷
---
# شمارهٔ ۸۷

<div class="b" id="bn1"><div class="m1"><p>خورشید مرا بنیکبختان نگه است</p></div>
<div class="m2"><p>بخش من از او چو سایه بخت سیه است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خورشید و شان سزای اوج شرفند</p></div>
<div class="m2"><p>گر ذره بآسمان رود خاک ره است</p></div></div>