---
title: >-
    شمارهٔ ۴۰۲
---
# شمارهٔ ۴۰۲

<div class="b" id="bn1"><div class="m1"><p>ای مست حق از می مجازی بگذر</p></div>
<div class="m2"><p>وزعشق بتان و عشوه سازی بگذر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با مغبچگان عشق و جوانی خوبست</p></div>
<div class="m2"><p>چون پیر شدی ز بچه بازی بگذر</p></div></div>