---
title: >-
    شمارهٔ ۲۶۶
---
# شمارهٔ ۲۶۶

<div class="b" id="bn1"><div class="m1"><p>هرگز دلم از عشق پشیمان نشود</p></div>
<div class="m2"><p>با آنکه دمی زعشق شادان نشود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا نقش بتان بتخته هستی هست</p></div>
<div class="m2"><p>این کافر دل سیه مسلمان نشود</p></div></div>