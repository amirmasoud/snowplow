---
title: >-
    شمارهٔ ۳۸۷
---
# شمارهٔ ۳۸۷

<div class="b" id="bn1"><div class="m1"><p>ای تازه جوان بیا و پیری بنگر</p></div>
<div class="m2"><p>در خاک نشسته ام فقیری بنگر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در چشم رقیب از حقیری نایم</p></div>
<div class="m2"><p>پیری و فقیری و حقیری بنگر</p></div></div>