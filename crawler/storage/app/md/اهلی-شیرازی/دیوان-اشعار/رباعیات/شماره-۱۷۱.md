---
title: >-
    شمارهٔ ۱۷۱
---
# شمارهٔ ۱۷۱

<div class="b" id="bn1"><div class="m1"><p>آن چار کتاب حق که آمد ز نخست</p></div>
<div class="m2"><p>بیشک همه حجت الهی است درست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قرآن که جوامع الکلم آمده است</p></div>
<div class="m2"><p>لب همه جمع کرد و رق همه شست</p></div></div>