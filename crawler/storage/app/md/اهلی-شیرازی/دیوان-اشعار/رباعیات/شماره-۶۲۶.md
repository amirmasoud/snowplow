---
title: >-
    شمارهٔ ۶۲۶
---
# شمارهٔ ۶۲۶

<div class="b" id="bn1"><div class="m1"><p>می نوش نه آنچنان که از دست شوی</p></div>
<div class="m2"><p>در پای خسان چون خاک ره پست شوی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیداری عیش شب بفریاد چه سود</p></div>
<div class="m2"><p>کز خواب صبوحی چو سگان مست شوی</p></div></div>