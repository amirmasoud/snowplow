---
title: >-
    شمارهٔ ۵۳۶
---
# شمارهٔ ۵۳۶

<div class="b" id="bn1"><div class="m1"><p>هر چند ز جور بخت نامقبل من</p></div>
<div class="m2"><p>معشوقه مهربان شود قاتل من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>باور نکنی که در دل غافل من</p></div>
<div class="m2"><p>یک جو حذرست و الحذر از دل من</p></div></div>