---
title: >-
    شمارهٔ ۵۷۵
---
# شمارهٔ ۵۷۵

<div class="b" id="bn1"><div class="m1"><p>اهلی بسخن کجا رسیدی باری</p></div>
<div class="m2"><p>زان حسن نهانی چه شنیدی باری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سیمرغ ستایی تو و سیمرغ که دید؟</p></div>
<div class="m2"><p>ور دید کسی تو خود ندیدی باری</p></div></div>