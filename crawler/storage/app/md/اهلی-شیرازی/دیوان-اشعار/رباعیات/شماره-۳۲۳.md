---
title: >-
    شمارهٔ ۳۲۳
---
# شمارهٔ ۳۲۳

<div class="b" id="bn1"><div class="m1"><p>دردا که رسید پیری و داد نوید</p></div>
<div class="m2"><p>کز صفحه عمر شسته شد خط امید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر آتش دلکش جوانی بنشست</p></div>
<div class="m2"><p>خاکستر ناامیدی از موی سپید</p></div></div>