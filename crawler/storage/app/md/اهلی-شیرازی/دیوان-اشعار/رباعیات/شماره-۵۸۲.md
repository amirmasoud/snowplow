---
title: >-
    شمارهٔ ۵۸۲
---
# شمارهٔ ۵۸۲

<div class="b" id="bn1"><div class="m1"><p>رحمان طلبی ز اهل عرفان گردی</p></div>
<div class="m2"><p>شیطان طلبی غرقه عصیان گردی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای کور بصر شرم نداری که ز جهل</p></div>
<div class="m2"><p>رحمان بلی پیرو شیطان گردی</p></div></div>