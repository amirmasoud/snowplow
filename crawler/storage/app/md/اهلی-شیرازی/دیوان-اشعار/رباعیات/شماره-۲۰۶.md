---
title: >-
    شمارهٔ ۲۰۶
---
# شمارهٔ ۲۰۶

<div class="b" id="bn1"><div class="m1"><p>ای کز گل روی تو رخم زرد بود</p></div>
<div class="m2"><p>دل از دهنت چو غنچه پر درد بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بوسی دهنت نداد و صد وعده دهد</p></div>
<div class="m2"><p>کم حوصله را زبان جوانمرد بود</p></div></div>