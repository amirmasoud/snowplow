---
title: >-
    لغز قفل
---
# لغز قفل

<div class="b" id="bn1"><div class="m1"><p>آن چیست که همچو مرغ پی بسته بود</p></div>
<div class="m2"><p>چون مار فراز گنج پیوسته بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مرغان همه را پر ز برون میروید</p></div>
<div class="m2"><p>آن مرغ پرش ز اندرون رسته بود</p></div></div>