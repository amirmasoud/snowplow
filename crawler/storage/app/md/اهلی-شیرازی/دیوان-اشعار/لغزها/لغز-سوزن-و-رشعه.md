---
title: >-
    لغز سوزن و رشعه
---
# لغز سوزن و رشعه

<div class="b" id="bn1"><div class="m1"><p>چیست آنمرغ مار هیبت کو</p></div>
<div class="m2"><p>راست ماند بناوک دلدوز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رشته بر پا زوری دست پرد</p></div>
<div class="m2"><p>باز آید چو مرغ دست آموز</p></div></div>