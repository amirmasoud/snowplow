---
title: >-
    شمارهٔ  ۱۱ - رباعی مستزاد
---
# شمارهٔ  ۱۱ - رباعی مستزاد

<div class="b" id="bn1"><div class="m1"><p>این مغبچگان بما ملامت چه کنند</p></div>
<div class="m2"><p>بی جرم و خطا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ما را بدر از کوی سلامت چکنند</p></div>
<div class="m2"><p>از راه جفا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>امروز اگر کشند ما را بستم</p></div>
<div class="m2"><p>وز پیش برند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>فردا که بود روز قیامت چکنند</p></div>
<div class="m2"><p>ماییم و شما</p></div></div>