---
title: >-
    شمارهٔ  ۱ - ترکیب بند در نعت حضرت رسالت و ایمه اثنا عشر
---
# شمارهٔ  ۱ - ترکیب بند در نعت حضرت رسالت و ایمه اثنا عشر

<div class="b" id="bn1"><div class="m1"><p>کس عزیز من نشد واقف بر اسرار خدا</p></div>
<div class="m2"><p>یوسف مصری بود حیران بازار خدا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نور خورشیدی از شراری بنگری گر واقفی</p></div>
<div class="m2"><p>زانکه از هر ذره تابان است انوار خدا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بگذر از رنگ یقین و چون صبا بیرنگ شو</p></div>
<div class="m2"><p>گر گل توحید میجویی ز گلزار خدا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زامتحان لطف حق اندیشه کن و زغم منال</p></div>
<div class="m2"><p>در مباز از اندکی غم لطف بسیار خدا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ما چه دریابیم ازو گر در میان نبود نبی</p></div>
<div class="m2"><p>طوطی هم صوت ما گویا بگفتار خدا</p></div></div>
<div class="b2" id="bn6"><p>خاک ما را کی بود با بحر عزت رابطه</p>
<p>گر نباشد مصطفی چون ابر رحمت واسطه</p></div>
<div class="b" id="bn7"><div class="m1"><p>آنکه ذاتش شد سبب در نظم عالم مصطفاست</p></div>
<div class="m2"><p>فخر عالم آدم آمد فخر آدم مصطفاست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>حرف حرف آمد رسل تا نامه پیغمبری</p></div>
<div class="m2"><p>ختم بر مهر نبوت شد که خاتم مصطفاست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>قصه جان بخشی عیسی بهل با مردگان</p></div>
<div class="m2"><p>کاین نفس جانبخش بر عیسی مریم مصطفاست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>جز به انوار ولایت در نمی یابد کسی</p></div>
<div class="m2"><p>راز سر پوشیده حق را که محرم مصطفاست</p></div></div>
<div class="b2" id="bn11"><p>شهر علم مصطفی را جز علی کس در نیافت</p>
<p>کی چنان شهری کسی دریافت تا آن در نیافت</p></div>
<div class="b" id="bn12"><div class="m1"><p>دیده عقل است محروم از کمال مرتضی</p></div>
<div class="m2"><p>کی درین آیینه میگنجد مثال مرتضی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>جلوه کامل صفات الله را در ذات اوست</p></div>
<div class="m2"><p>شاهد خلق جمیل است و جمال مرتضی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>دشمن ساقی کوثر را ز دوزخ بدترست</p></div>
<div class="m2"><p>این که دارد داغ حسرت از زلال مرتضی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>هم ملک هم شیر حق هم بحر دین هم فتح علم</p></div>
<div class="m2"><p>آدمی صورت نمی بندد مثال مرتضی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>نامه اعمال ما ختم است بر توقیع لطف</p></div>
<div class="m2"><p>زانکه مهر مرتضی داریم و آل مرتضی</p></div></div>
<div class="b2" id="bn17"><p>سینه پر علم حیدر بحر مالامال بود</p>
<p>گوهر شبیر و شبر شاهد احوال بود</p></div>
<div class="b" id="bn18"><div class="m1"><p>گر فلک واقف شدی از تلخی کام حسن</p></div>
<div class="m2"><p>آنچنان زهری کجا میریخت در کام حسن</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>جز نکویی از نکو چیزی نمیآید پدید</p></div>
<div class="m2"><p>لاجرم خلق حسن ظاهر کند نام حسن</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>خرقه ظاهر مبین چون گل که زیر جامه داشت</p></div>
<div class="m2"><p>خار خار از خرقه پشمینه اندام حسن</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>آستانش کعبه قدس است زان خیل ملک</p></div>
<div class="m2"><p>میپرد همچون کبوتر بر دو بام حسن</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>کس بگفتن در نیابد تلخی از آشام زهر</p></div>
<div class="m2"><p>هم حسین تشنه لب داند در آشام حسن</p></div></div>
<div class="b2" id="bn23"><p>محنت درد حسن هر چند دلها خون ازوست</p>
<p>تلخی لب تشنگیهای حسین افزون ازوست</p></div>
<div class="b" id="bn24"><div class="m1"><p>چون ز عالم تشنه لب شد سرو آزاد حسین</p></div>
<div class="m2"><p>کار ما از گریه سقایی است بریاد حسین</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>گر بجز تسلیم بودی چاره تقدیر حق</p></div>
<div class="m2"><p>چشمها در تیغ کردی تیغ فولاد حسین</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>آفتابی چون نبی ماه تمامی چون علی</p></div>
<div class="m2"><p>شد قران هر دو یکجا بهر ایجاد حسین</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>آبروی چون حسینی بهر آبی ریختند</p></div>
<div class="m2"><p>آه اگر نستاند از ایشان فلک داد حسین</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>کوری آنکس که تیغ ظلم زد بر خاندان</p></div>
<div class="m2"><p>تا قیامت خواهد افزون گشت اولاد حسین</p></div></div>
<div class="b2" id="bn29"><p>گر حسین تشنه لب را جان ز محنت سوختند</p>
<p>ماند ازو شمعی که صد عالم چراغ افروختند</p></div>
<div class="b" id="bn30"><div class="m1"><p>بسکه پر شد اشک چون باران زین العابدین</p></div>
<div class="m2"><p>ناودان خون شد از دامان زین العابدین</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>کشتی نوح است لطف خاندان ورنی جهان</p></div>
<div class="m2"><p>گم شدی در اشک چون طوفان زین العابدین</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>آدم آل عبا خوانند او را زانکه هست</p></div>
<div class="m2"><p>رونق آدم ز فرزندان، زین العابدین</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>دیده یعقوب کز هجران یوسف بسته شد</p></div>
<div class="m2"><p>هجر او وصلی است از هجران زین العابدین</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>آتشش در جان فتد هر کس که خندد همچو شمع</p></div>
<div class="m2"><p>با وجود دیده گریان زین العابدین</p></div></div>
<div class="b2" id="bn35"><p>چشم زین العابدین را گر فراق و گریه کاست</p>
<p>قره العینی که دارد او چراغ دیده هاست</p></div>
<div class="b" id="bn36"><div class="m1"><p>روشنی بخش فلک روی چو ماه باقرست</p></div>
<div class="m2"><p>سرمه چشم ملک گردی ز راه باقرست</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>آنچنان بحری کجا گنجه درین میدان تنگ</p></div>
<div class="m2"><p>سینه اهل یقین آرامگاه باقرست</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>باوجود بارگاه اوست قندیلی فلک</p></div>
<div class="m2"><p>بلکه قندیلی ازو در بارگاه باقرست</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>نور فرزندان او باز از جهان ظلمت ز دود</p></div>
<div class="m2"><p>در حقیقت نور ایمان در پناه باقرست</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>بحربی پایان باقررا دو عالم قطره ییست</p></div>
<div class="m2"><p>جعفر صادق درین دعوی گواه باقرست</p></div></div>
<div class="b2" id="bn41"><p>چشم باقر نور حق از مطلع غیبش دمید</p>
<p>در شب قدری که نور صادق از جیبش دمید</p></div>
<div class="b" id="bn42"><div class="m1"><p>قبله اهل حقیقت جعفر صادق بود</p></div>
<div class="m2"><p>مفتی شرع و طریقت جعفر صادق بود</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>ایکه از فر هما عزت طلب داری بیا</p></div>
<div class="m2"><p>کان همای اوج عزت جعفر صادق بود</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>بررخ دنیا که میخوانند خلقش سوی خود</p></div>
<div class="m2"><p>آنکه برزد دست همت جعفر صادق بود</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>موسی کاظم دلیل آمد که بر خلق خدا</p></div>
<div class="m2"><p>آیتی از فضل و رحمت جعفر صادق بود</p></div></div>
<div class="b2" id="bn46"><p>گر چه جعفر برد ازین گرداب غم چشم تری</p>
<p>ماند ازو بحری که در هر گوشه دارد گوهری</p></div>
<div class="b" id="bn47"><div class="m1"><p>صبح انوار هدایت موسی کاظم بود</p></div>
<div class="m2"><p>مخزن سر ولایت موسی کاظم بود</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>ظلمها بردی و خوردی خشم و کردی مردمی</p></div>
<div class="m2"><p>اینچنین محض عنایت موسی کاظم بود</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>آن گلستان ولایت کز نسیم او دمید</p></div>
<div class="m2"><p>گلبنی در هر ولایت موسی کاظم بود</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>عقل در تفسیر آیات کمالش کی رسید</p></div>
<div class="m2"><p>کایتی در صد روایت موسی کاظم بود</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>آفتابی چون علی موسی الرضا را در وجود</p></div>
<div class="m2"><p>مشرق صبح سعادت موسی کاظم بود</p></div></div>
<div class="b2" id="bn52"><p>گرچه گوهرها ز صلب موسی کاظم چکید</p>
<p>گوهری آمد که صد دریا از آن شد ناپدید</p></div>
<div class="b" id="bn53"><div class="m1"><p>کعبه اهل صفا روی علی موسی الرضاست</p></div>
<div class="m2"><p>کعبه راهم روی دل سوی علی موسی الرضاست</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>یک طواف کوی او هفتاد حج آمد ولی</p></div>
<div class="m2"><p>هر قدم یک معبه در کوی علی موسی الرضاست</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>تحفه صوری که میگویند جان خواهد دمید</p></div>
<div class="m2"><p>در صباح حشر یک موی علی موسی الرضاست</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>آنکه دروی میزند صد کعبه دست اعتصام</p></div>
<div class="m2"><p>حلقه های جعد گیسوی علی موسی الرضاست</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>هم تقی سر دلش داند کز آن سرچشمه است</p></div>
<div class="m2"><p>کان نه دل بحری بپهلوی علی موسی الرضاست</p></div></div>
<div class="b2" id="bn58"><p>گر علی موسی الرضا نورش بود و اصل زدوست</p>
<p>شبچراغی چون تقی دارد که نور روز ازوست</p></div>
<div class="b" id="bn59"><div class="m1"><p>قاف سیمرغ حقیقت هستی و بود تقی است</p></div>
<div class="m2"><p>قاف تا قاف جهان در سایه جود تقی است</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>کعبه سودش کی کند هر کس کزو درمانده است</p></div>
<div class="m2"><p>کی شود مقبول حق هر کس که مردود تقی است</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>نیست مقصود تقی جز آنکه مقصود حق است</p></div>
<div class="m2"><p>لاجرم حق میکند آنرا که مقصود تقی است</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>گشت طالع آفتاب دولت از دامان او</p></div>
<div class="m2"><p>دولت جاوید گویا بخت مسعود تقی است</p></div></div>
<div class="b2" id="bn63"><p>شد تقی زین ظلمت و دامان جان افشاند ازو</p>
<p>وز نقی سرچشمه آب حیاتی ماند ازو</p></div>
<div class="b" id="bn64"><div class="m1"><p>چون برآمد اختر خورشید تاثیر نقی</p></div>
<div class="m2"><p>صد جهان بگرفت انوار جهانگیر نقی</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>کرد خواب واپسین ناکرده تعبیرش درست</p></div>
<div class="m2"><p>یوسف اندر خواب کی دیده ست تعبیر نقی</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>کی توانستی چنان آزاد و فارغ زیستن</p></div>
<div class="m2"><p>گر نبودی مهر گردون بنده پیر نقی</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>غلغل افتد در فلک از بسکه در جوش آورد</p></div>
<div class="m2"><p>حلقه ذکر ملک تسبیح و تکبیر نقی</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>کی عیان میشد چو سر کنت کنزا مخفیا</p></div>
<div class="m2"><p>گر نه ذات عسگری میداد تشهیر نقی</p></div></div>
<div class="b2" id="bn69"><p>گر نقی را پایه شاهی جدا از لشگرست</p>
<p>عسگری لشکر کش اقبال او تا محشرست</p></div>
<div class="b" id="bn70"><div class="m1"><p>چهره مقصود چون زیر نقاب عسکری است</p></div>
<div class="m2"><p>مشرق و مغرب منور ز آفتاب عسکری است</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>با وجود عسکری گو چشمه حیوان مباش</p></div>
<div class="m2"><p>بحر او یک قطره از چشم پر آب عسکری است</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>کی تواند با رکابش ماه نو پهلو زند</p></div>
<div class="m2"><p>زانکه شاه تخت گردون در رکاب عسکری است</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>گر چه بر گردون نیفکند از کرم چشم عتاب</p></div>
<div class="m2"><p>لرزه بر خورشید از بیم عتاب عسکری است</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>یوسف جان عاقبت خواهد ز جیبی سر زدن</p></div>
<div class="m2"><p>وانکه میزیبد بدین دولت جناب عسکری است</p></div></div>
<div class="b2" id="bn75"><p>عسکری در راه حق ضایع نماند رنج او</p>
<p>عاقبت نقد دو عالم سر زند از گنج او</p></div>
<div class="b" id="bn76"><div class="m1"><p>مژده باد ای اهل دل کاینک ظهور مهدی است</p></div>
<div class="m2"><p>ظلمت عالم ز حد شد وقت نور مهدی است</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>در چنین ظلمی که عالم سر بسر ظلمت گرفت</p></div>
<div class="m2"><p>آنکه آتش در زند تیغ غیور مهدی است</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>داد مظلومان ز جور ظالمان گرشه نداد</p></div>
<div class="m2"><p>ماجرای ما و ایشان در ظهور مهدی است</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>مرکب اندر زین و خلق استاده او در صبر وقت</p></div>
<div class="m2"><p>عقل حیران مانده در ذات صبور مهدی است</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>نامه فرمان که حکم آدم و خاتم دروست</p></div>
<div class="m2"><p>حکم آن منشور در حکم امور مهدی است</p></div></div>
<div class="b2" id="bn81"><p>اینچنین نوری که بر افلاک سر خواهد کشید</p>
<p>هم ز جیب اهل بیت مصطفی خواهد دمید</p></div>
<div class="b" id="bn82"><div class="m1"><p>بر گدایان چون فتد فرهمای اهل بیت</p></div>
<div class="m2"><p>پادشاه است آنکه میگردد گدای اهل بیت</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>ذره بردن بر فلک خورشید راد انی که چیست</p></div>
<div class="m2"><p>می کشد در چشم خاک پاک پای اهل بیت</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>عقل قدر سنگ و گوهر هر دو داند بیش و کم</p></div>
<div class="m2"><p>کعبه گر لاف صفا زد با صفای اهل بیت</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>اهل بیت مصطفا گر قدر خود ظاهر کنند</p></div>
<div class="m2"><p>عالمی دیگر بباید از برای اهل بیت</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>جنبش باد صبا هر لحظه میدانی که چیست</p></div>
<div class="m2"><p>طایر جان میزند پر در هوای اهل بیت</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>چشمه آب بقا یابی ز ظلمات فنا</p></div>
<div class="m2"><p>گر فنای خویش جویی در بقای اهل بیت</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>عاقبت چون بیوفایی با تو خواهد کرد عمر</p></div>
<div class="m2"><p>بهتر آن باشد که میری در وفای اهل بیت</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>یارب از انعام عام اهل بیتش شاد کن</p></div>
<div class="m2"><p>اهلی مسکین که میگوید دعای اهل بیت</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>نظم او کز محکمی حبل المتین خواندش خرد</p></div>
<div class="m2"><p>ختم آن حبل المتین شد بر دعای اهل بیت</p></div></div>
<div class="b2" id="bn91"><p>تا ابد نور علی چون مهر و مه تابنده باد</p>
<p>بر سر ما سایه آل علی پاینده باد</p></div>