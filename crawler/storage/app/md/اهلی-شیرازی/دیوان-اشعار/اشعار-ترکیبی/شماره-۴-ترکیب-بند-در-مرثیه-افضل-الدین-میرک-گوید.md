---
title: >-
    شمارهٔ  ۴ - ترکیب بند در مرثیه افضل الدین میرک گوید
---
# شمارهٔ  ۴ - ترکیب بند در مرثیه افضل الدین میرک گوید

<div class="b" id="bn1"><div class="m1"><p>آنان که ره بمنزل مقصود برده اند</p></div>
<div class="m2"><p>روزیکه زاده اند همانروز مرده اند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون نقش تن ز خانه هستی ستردنی است</p></div>
<div class="m2"><p>ایشان بدست خود رقم خود سترده اند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خوش وقت عارفان که نمیرند همچو خضر</p></div>
<div class="m2"><p>کاب بقا ز مشرب توحید خورده اند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آزاده همچو سر و ز عالم نه چون خسان</p></div>
<div class="m2"><p>دل بسته همچو غنچه بیک مشت خرده اند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آنان که زنده دل نه بنور هدایتند</p></div>
<div class="m2"><p>گر آتشند جمله که چون یخ فسرده اند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سیل اجل در آمد و شیران ز جای برد</p></div>
<div class="m2"><p>روبه وشان خام طمع پا فشرده اند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا هست و بود دشمن جان است روزگار</p></div>
<div class="m2"><p>بیچاره مردمی که بیارش شمرده اند</p></div></div>
<div class="b2" id="bn8"><p>گریاریی زمانه بدمهر داشتی</p>
<p>مهر جهانیان بجهان واگذاشتی</p></div>
<div class="b" id="bn9"><div class="m1"><p>دنیا وفا بمردم دانا نمیکند</p></div>
<div class="m2"><p>خوش وقت آنکه میل بدنیا نمیکند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دنیا و هر چه هست در او دام و دانه است</p></div>
<div class="m2"><p>او مرغ زیرک است که پروا نمیکند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آندم که یار گشت فلک تیغ میزند</p></div>
<div class="m2"><p>هرگز حیا نکرده و قطعا نمیکند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>معشوقه ایست ناکس و بر جای اینجهان</p></div>
<div class="m2"><p>مرد آن بود که دل او جا نمیکند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>زحمت مکش که کس نگشاید در بقا</p></div>
<div class="m2"><p>وین در بزور دست کسی وا نمیکند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گردهر ز هر جور به نیکان نمی دهد</p></div>
<div class="m2"><p>ور چرخ قصد مردم دانا نمیکند</p></div></div>
<div class="b2" id="bn15"><p>آن گنج رشد و مرشد حکمت پناه کو</p>
<p>وان بحر علم و مظهر لطف اله کو</p></div>
<div class="b" id="bn16"><div class="m1"><p>سر رشته وجود کسی را بدست نیست</p></div>
<div class="m2"><p>جز واجب الوجود دگر چه هست نیست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ما شیشه ایم و سنگ اجل در کمین ماست</p></div>
<div class="m2"><p>واحسرتا که چاره بغیر از شکست نیست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>در عالم خرابه نشستن هوس مکن</p></div>
<div class="m2"><p>بگذر ازین خرابه که جای نشست نیست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>گردنکشی مکن که سری کی بلند شد؟</p></div>
<div class="m2"><p>کامروز زیر پای تو چون خاک پست نیست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>شستی گشاده چرخ مشبک بگرد ما</p></div>
<div class="m2"><p>یعنی تو ماهیی و گزیرت زشست نیست</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>آسوده از زمانه نه عاقل نه جاهل است</p></div>
<div class="m2"><p>جام مراد در کف هشیار و مست نیست</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>گردن بنه بهر چه رسید از زمانه ات</p></div>
<div class="m2"><p>کاینها بجز نصیبه روز الست نیست</p></div></div>
<div class="b2" id="bn23"><p>گر موجب حیات دل زیرک آمدی</p>
<p>کی این جفا به افضل دین میرک آمدی</p></div>
<div class="b" id="bn24"><div class="m1"><p>جان جهانیان ز جهان شد دریغ ازو</p></div>
<div class="m2"><p>گنج وفا بخاک نهان شد دریغ ازو</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>آن روشنی دیده که بودی چراغ جمع</p></div>
<div class="m2"><p>از دیده همچو برق یمان شد دریغ ازو</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>مادر میان تیرگی غم چنین اسیر</p></div>
<div class="m2"><p>شمع مراد ما ز میان شد دریغ ازو</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>عیسی دمی که بود روانبخش مردمان</p></div>
<div class="m2"><p>در زیر گل چو آب روان شد دریغ ازو</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>صاحبدلی که چشم وچراغ زمانه بود</p></div>
<div class="m2"><p>آخر بچشم زخم زمان شد دریغ ازو</p></div></div>
<div class="b2" id="bn29"><p>آن یار محترم چو روان شد سوی عدم</p>
<p>تاریخ رحلتش طلب از از یار محترم</p></div>
<div class="b" id="bn30"><div class="m1"><p>یارب بفضل خود که بپاکان قرین کنش</p></div>
<div class="m2"><p>با محرمان صدر نشین همنشین کنش</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>از بزم عیش همنفسان چون جدا افتاد</p></div>
<div class="m2"><p>از لطف خویش همنفس حور عین کنش</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>چون مرغ روحش از قفس تن پریده شد</p></div>
<div class="m2"><p>آسوده در حدیقه خلد برین کنش</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>در سایه لوای محمد در آورش</p></div>
<div class="m2"><p>یارب بآبروی نبی کاینچنین کنش</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>در روز حشر خرده مگیر از کرم بر او</p></div>
<div class="m2"><p>حشری که میکنی ببزرگان دین کنش</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>کاری که کرده است نه در خورد آفرین</p></div>
<div class="m2"><p>عفو از کمال لطف جهان آفرین کنش</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>جز رحمت تو هیچ هنر دستگیر نیست</p></div>
<div class="m2"><p>رحمی نما و ختم عمل برهمین کنش</p></div></div>
<div class="b2" id="bn37"><p>روحش همیشه از کرم خویش شاد کن</p>
<p>عمر برادر و پسرانش زیاد کن</p></div>