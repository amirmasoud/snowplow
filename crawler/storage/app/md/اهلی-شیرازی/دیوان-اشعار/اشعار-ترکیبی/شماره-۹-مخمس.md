---
title: >-
    شمارهٔ  ۹ - مخمس
---
# شمارهٔ  ۹ - مخمس

<div class="b" id="bn1"><div class="m1"><p>این همه خشم تو ای عاشقکش بی باک چیست</p></div>
<div class="m2"><p>دل ز خشمت خاک شد این زهر بی تریاک چیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جر دل غیر از نگاه دیده بی باک چیست</p></div>
<div class="m2"><p>دیده را گر با تو کار افتاد دل غمناک چیست</p></div></div>
<div class="b2" id="bn3"><p>مرغ عاشق میشود پیراهن گل چاک چیست</p></div>
<div class="b" id="bn4"><div class="m1"><p>ایکه در خوبی نباشد همعنانت هیچکس</p></div>
<div class="m2"><p>کی ز فتراک تو عاشق بگسلددست هوس</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>میروی شاهانه و غوغای خلق از پیش و پس</p></div>
<div class="m2"><p>ابلق حسن ار بزیر زین یوسف بود بس</p></div></div>
<div class="b2" id="bn6"><p>عالمی گرد سمندت دست بر فتراک چیست</p></div>
<div class="b" id="bn7"><div class="m1"><p>شمع رویت کز وجود ما اثر باما نهشت</p></div>
<div class="m2"><p>او بود مقصود خواه از کعبه و خواه از کنشت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آتشین رویت بهشت ماست ای حوری سرشت</p></div>
<div class="m2"><p>خلق میگویند آتش ره ندارد در بهشت</p></div></div>
<div class="b2" id="bn9"><p>ای بهشت عاشقان این روی آتشناک چیست</p></div>
<div class="b" id="bn10"><div class="m1"><p>آن دولب کز آب خضر و چشمه کوثر نهند</p></div>
<div class="m2"><p>تا بکی بگشایی و خلقی ز حسرت جان دهند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خلق اگر در فکر آن کز زهر مردن و ارهند</p></div>
<div class="m2"><p>مرده تریاک را بسیار عزت می نهند</p></div></div>
<div class="b2" id="bn12"><p>تو ازین لب مهره بگشاد مهره تریاک چیست</p></div>
<div class="b" id="bn13"><div class="m1"><p>عالمی را کرده یی از حسن خود پر شور و شر</p></div>
<div class="m2"><p>بسکه خون عاشقان میریزی ای رشک قمر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چرخ در خون از شفق دامان کشد شام و سحر</p></div>
<div class="m2"><p>گر ز رشک روی تو مه را شده پاره جگر</p></div></div>
<div class="b2" id="bn15"><p>این نشانیهای خون بر دامن افلاک چیست</p></div>
<div class="b" id="bn16"><div class="m1"><p>گر بجرم عشق با اهلی نمیگویی سخن</p></div>
<div class="m2"><p>ای گل نو رسته بشنو پند پیران کهن</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>گلبن امید غمخواران مکن از بیخ و بن</p></div>
<div class="m2"><p>گر حسن قدر تو را نشناخت او را عفو کن</p></div></div>
<div class="b2" id="bn18"><p>پیش عفو شامل تو جرم مشتی خاک چیست</p></div>