---
title: >-
    شمارهٔ ۴۹ - در مدح معز الدوله گوید
---
# شمارهٔ ۴۹ - در مدح معز الدوله گوید

<div class="b" id="bn1"><div class="m1"><p>سوار من که سرم باد گوی میدانش</p></div>
<div class="m2"><p>سر منست و سر زلف همچو گانش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هزار یوسف مصری کمست اگر هردم</p></div>
<div class="m2"><p>فرو روند بفکر چه زنخدانش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از آن همیشه گریبان درم که در کارم</p></div>
<div class="m2"><p>گره همی فکند تکمه گریبانش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کمال صورت او از که باز پرسم من</p></div>
<div class="m2"><p>که هرکه می نگرم چون منست حیرانش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز دست چشم بلاجو دلم بجان آمد</p></div>
<div class="m2"><p>که هرچه دیده بر او بست داد تاوانش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بباغ عارض او هر طرف که می بینم</p></div>
<div class="m2"><p>نهاده دام دلی طره پریشانش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هزار نقد دل و صد هزار جوهر جان</p></div>
<div class="m2"><p>فدای حقه یاقوت و لعل خندانش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کسی که اینهمه خوبی و نیکویی دارد</p></div>
<div class="m2"><p>چگونه زار نباشد اسیر هجرانش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دوای مفلسی و عاشقی نمی باشد</p></div>
<div class="m2"><p>وگر بود کرم آصف است درمانش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>معز دولت و دستور ملک عبدالله</p></div>
<div class="m2"><p>که هست زیر نگین ملکت سلیمانش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>قضا چو دید ضمیر منیر او بگذاشت</p></div>
<div class="m2"><p>حساب روز قیامت بروز دیوانش</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>زهی مراتب حشمت که از کرم دایم</p></div>
<div class="m2"><p>گدای ریزه خوان است خان بن خانش</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>سزد که بر ورق مشکبار نافه چین</p></div>
<div class="m2"><p>خط خطا بکشد کلک عنبر افشانش</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بروز نامچه عمر جاودان مجراست</p></div>
<div class="m2"><p>برات خضر که طغرای اوست عنوانش</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چو مور تشنه و دریای بیکران باشد</p></div>
<div class="m2"><p>اگر خورند دو عالم ز نعمت خوانش</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چو روز بر همه کس روشن است اینمعنی</p></div>
<div class="m2"><p>که شمع ماه چراغیست از شبستانش</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ایا بلند جنابی که خانه قدرت</p></div>
<div class="m2"><p>گذشته است ز طاق سپهر ایوانش</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>عنایت ازلی همره سعادت تست</p></div>
<div class="m2"><p>که باد تا بابد لطف حق نکهبانش</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>عدو که با تو زبان آوری کند چو نشمع</p></div>
<div class="m2"><p>ببر زبانش و بر جای خویش بنشانش</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>اگر نه مهر تو ایمان خود کند دشمن</p></div>
<div class="m2"><p>زمان مرگ بروزی نگردد ایمانش</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>هر آنکه با تو چو سوسن دراز کرد زبان</p></div>
<div class="m2"><p>نشانده باد چو نرگس بفرق، دندانش</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>عدوی جاه ترا باد آنمرض کزتن</p></div>
<div class="m2"><p>برای رشته بر آرند رشته جانش</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بنای خاصه هستی کجا شود ویران</p></div>
<div class="m2"><p>اگر ز ضبط تو بودی اساس و ارکانش</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ز حق شناسی نعمت غلام شد اهلی</p></div>
<div class="m2"><p>وظیفه نیست که غیر از تو کس دهد نانش</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بجان آصف دوران که این گدا چون مور</p></div>
<div class="m2"><p>بخانه نیست جوی آذق زمستانش</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بروزگار بگو کاین فقیر کاین سر گردان</p></div>
<div class="m2"><p>غلام ماست ازین بیشتر مرنجانش</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>مبین خرابی حالش نظر بمعنی کن</p></div>
<div class="m2"><p>که جای گنج معانی است جان ویرانش</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>همیشه تا فلک از فیض ماه و پر تو مهر</p></div>
<div class="m2"><p>گهی بهار شود گه خزان گلستانش</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>نهال عیش تو یارب بلند باد چنان</p></div>
<div class="m2"><p>که دست غم نرسد بر طراز دامانش</p></div></div>