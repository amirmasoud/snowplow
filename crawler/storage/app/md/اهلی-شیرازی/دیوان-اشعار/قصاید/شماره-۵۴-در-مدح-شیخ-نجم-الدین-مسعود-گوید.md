---
title: >-
    شمارهٔ ۵۴ - در مدح شیخ نجم الدین مسعود گوید
---
# شمارهٔ ۵۴ - در مدح شیخ نجم الدین مسعود گوید

<div class="b" id="bn1"><div class="m1"><p>الله الله مگر اینواقعه خواب است و خیال</p></div>
<div class="m2"><p>که مرا بخت رسانید به معراج وصال</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یار خود آمد و احوال دلم دید که چیست</p></div>
<div class="m2"><p>که پیامم نه صبا برد بسویشش نه شمال</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گرنه خود خضر بسروقت اسیران آید</p></div>
<div class="m2"><p>تشنه بادیه میرد بتمنای زلال</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نه همین چهره من شسته شد از ابر کرم</p></div>
<div class="m2"><p>که بشست از رخ امید جهان گرد ملال</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در دل چرخ فلک گشت مگر زینت چرخ</p></div>
<div class="m2"><p>که مرا قرعه اقبال در افتاد بفال</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر من این فیض سعادت همه دانی که ز چیست</p></div>
<div class="m2"><p>اثر سایه خورشید زمان بحر کمال</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شیخ نجم الحق و الدوله والدین مسعود</p></div>
<div class="m2"><p>اختر برج سعادت فلک عز و جلال</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>رهبر راهروان ره حق از همه ره</p></div>
<div class="m2"><p>واقف حال فقیران جهان در همه حال</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>حلم او گر ز ازل سایه فکندی بزمین</p></div>
<div class="m2"><p>خاک محتاج نبودی بگرانی جبال</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بسکه از معتدلش بازوی مظلوم قویست</p></div>
<div class="m2"><p>زیر چنگال کبوتر شده شاهین چنگال</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ناورد ما در ایام دگر فرزندی</p></div>
<div class="m2"><p>بچنین صورت خوب و بچنین حسن و جمال</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>آفتاب کرمش گر ز افق تیغ زند</p></div>
<div class="m2"><p>زر کند خاک ره و لعل شود سنگ سفال</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ای بلند اختر، از اندیشه ما بیشتری</p></div>
<div class="m2"><p>فهم ما را نبود در سر و کار تو مجال</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>سر خط عقل بود این که محالست چو تو</p></div>
<div class="m2"><p>مشق ما نیز همین شد که محالست محال</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>مدعی کی ز ترقی بکمال تو رسد</p></div>
<div class="m2"><p>سبزه شوخ است ولی نگذرد از شاخ نهال</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>در ترقی است جمال هنرت روز بروز</p></div>
<div class="m2"><p>حسن بختست مآل تو زهی حسن مآل</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>آنچنان در پی خوشنودی خلقی که بود</p></div>
<div class="m2"><p>راضی از جاه و جلال تو خدا جل جلال</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>هرکه یابد ز سحاب کرمت یکقطره</p></div>
<div class="m2"><p>همچو دریا شود از مال جهان مالا مال</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>آن کریمی تو که در عدل و کرم مثل تو نیست</p></div>
<div class="m2"><p>خلق عالم همه خواهان که کنند از تو سوال</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>کمترین بنده درگاه تو بر قیصر روم</p></div>
<div class="m2"><p>مینویسد بتحکم که بده مال و منال</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>تو ببزم طرب اسوده دل و همت تو</p></div>
<div class="m2"><p>دشمنان را همه خون ریخته بی جنگ و جدال</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>دشمن و دوست کمر بست بخدمت برتو</p></div>
<div class="m2"><p>همه در حلقه فرمان تو زنجیر مثال</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>گر ترازوی خرد قدر تو سنجد باطور</p></div>
<div class="m2"><p>صد بود قاف و قار تو و او یک مثقال</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>هرکه زد در ره مهر تو بعلت قدمی</p></div>
<div class="m2"><p>رشته سر بر زند اندر قلم پاش چو نال</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چهره بخت تو مستغنی از این مدح منست</p></div>
<div class="m2"><p>هیچ منت نبرد صورت خوب از خط و خال</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>من که در فکر سخن خواب شبم هست حرام</p></div>
<div class="m2"><p>بکر فکرم همه سحرست زهی سحر حلال</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>اهلی خاک نشین بنده درگاه تو شد</p></div>
<div class="m2"><p>بامید نظری قطع نظر از زر و مال</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>گر تو را چشم عنایت سوی این بنده بود</p></div>
<div class="m2"><p>صدر بزمش چه تفاوت کند از صف نعال</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بدعا ختم کنم قصه خود تا نشود</p></div>
<div class="m2"><p>خاطر نازکت آزرده این قال و مقال</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>تا فلک گاه سپر پیش نهد از خورشید</p></div>
<div class="m2"><p>گه کمان بر سر چنگ آورد از جرم هلال</p></div></div>