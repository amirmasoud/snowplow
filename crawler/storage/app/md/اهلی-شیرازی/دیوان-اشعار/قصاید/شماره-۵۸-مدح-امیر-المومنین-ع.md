---
title: >-
    شمارهٔ ۵۸ - مدح امیر المومنین (ع)
---
# شمارهٔ ۵۸ - مدح امیر المومنین (ع)

<div class="b" id="bn1"><div class="m1"><p>سرزدم از خواب صبحی کز نسیم عنبرین</p></div>
<div class="m2"><p>شبنمی از عنبر اشهب نشستی بر زمین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بادمیبردی بعالم فوج فوج و موج موج</p></div>
<div class="m2"><p>کاروان در کاروان منزل بمنزل مشک چین</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نکهت جان عالم اندر عالم و پنداشتم</p></div>
<div class="m2"><p>عالم جان آفرید آن صبحدم جان آفرین</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفتم این بوی ملک باشد که اید از فلک</p></div>
<div class="m2"><p>عنبر افشان از کنار و مشگریزان راستین</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یا فلک را از نسیم مشگبوی صبحدم</p></div>
<div class="m2"><p>گل شکفت از غنچه مهروز کوکب یاسمین</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یا مگر مشکین دم عیسی ز روی طلف باز</p></div>
<div class="m2"><p>جان فشان روی زمین آمد ز چرخ چارمین</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یا مگر بند قبای ناز یوسف برگشاد</p></div>
<div class="m2"><p>میدمد بوی خوش پیراهن آن نازنین</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>یا مگر مشاطه گیسوی خوبان برگشاد</p></div>
<div class="m2"><p>صد هزاران نافه مشک آمد ز زلف حور عین</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>عقل گفت اینها که میگویی همه بادست باد</p></div>
<div class="m2"><p>بوی جانست اینکه بر میخیزد از خلد برین</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گفتمش ممکن نباشد در جهان بوی بهشت</p></div>
<div class="m2"><p>جز نسیم روضه پاک امیر المومنین</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سرور مردان علی بن ابیطالب که هست</p></div>
<div class="m2"><p>هم امیر المومنین و هم امام المتقین</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>کاشف علم الله آن گیتی نمای لو کشف</p></div>
<div class="m2"><p>دیده راز هر دو کون از دیده علم الیقین</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>کعبه زان شد سجده گاه انبیا و اولیا</p></div>
<div class="m2"><p>کآمد آنجا در وجود آن قبله اصحاب دین</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>شهر علم مصطفا جوی از «علی بابها»</p></div>
<div class="m2"><p>از در علم آ و شهرستان علم الله بین</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>مصطفی را نیست داماد از علی شایسته تر</p></div>
<div class="m2"><p>شاه مردان را سزد خیر نسای العالمین</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>با نبی وصلت ولی را مجمع البحرین شد</p></div>
<div class="m2"><p>تا جهان اندر جهان پر گردد از در ثمین</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>در حریم خاص حق چونمصطفی معراج یافت</p></div>
<div class="m2"><p>محرمش او بوده و نامحرمش روح الامین</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>با علو آنشه قدرست یکسان گر بود</p></div>
<div class="m2"><p>طوطی قدسی سخن یا از مگس خیزد طنین</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چرخ اطلس پیش علم اوست طفل ساده لوح</p></div>
<div class="m2"><p>بلکه نشناسد یسار خویشتن را از یمین</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>رشته مهرش کمند جان بود بر بام عرش</p></div>
<div class="m2"><p>ذره را خط شعاع مهر شد حبل المتین</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>همچو گردون بود راکع در نماز و لطف کرد</p></div>
<div class="m2"><p>خاتم فیروزه زیبا تر از چرخش نگین</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>در کرم شاه ولایت بحر بی پایان بود</p></div>
<div class="m2"><p>شبنمی از موج بحرش حاتم صحرا نشین</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>خود گرفتی روزه و دادی بسایل نان بلی</p></div>
<div class="m2"><p>لذت بخشش غذای جان به از نان جوین</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>اسمان را اینهمه در از کجا آمد بکف</p></div>
<div class="m2"><p>گر نبود از خرمن او همچو پروین خوشه چین</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>هرکه نشناسد امیر نحل را مولای خود</p></div>
<div class="m2"><p>زهر بادش در دهان آن نا شناس انگبین</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ذوالفقارش هر کجا بر جان کافر زخم زد</p></div>
<div class="m2"><p>آید از جان آفرینش صد هزاران آفرین</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>خواند سلمان کودکش گفتا که از شیرت رهاند</p></div>
<div class="m2"><p>زو نشان جست از پس صد سال و کل دادش که بین</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>این شگفت از شاه نبود بلکه در هر دم شکفت</p></div>
<div class="m2"><p>در گلستان ولایت صد هزاران گل ازین</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>یا علی نام تو بر مهر سلیمان نقش بود</p></div>
<div class="m2"><p>آنهمه حشمت سلیمان راند زان نام مهین</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>گرنه از جنس تو فرزند آدمش بودی نشان</p></div>
<div class="m2"><p>کی ملک را قبله گشتی قالبی از ماء و طین</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>پیش بازوی تو رستم همچو بیژن در چه است</p></div>
<div class="m2"><p>بلکه بیجان تر از آن کاندر رحم باشد جنین</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>خورد از دست تو یک سیلی برخ چرخ کبود</p></div>
<div class="m2"><p>تا قیامت همچو گو سرگشته گردد بر زمین</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>هرکه جا بستد ز مکر و مشورت ناحق زتو</p></div>
<div class="m2"><p>مکر حق جانش ستد والله خیرالماکرین</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>کی بود همچون تو از بهر خلافت عمرو و بکر</p></div>
<div class="m2"><p>کی سلیمان گردد از انگشتری دیو لعین</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>هرکه با خصم تو در جنگست جنگ او غزاست</p></div>
<div class="m2"><p>نصرتش یاریست و فتحش یاور و بختش قرین</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>یا علی چشمی فکن بر بنده دیرین که تو</p></div>
<div class="m2"><p>بهترین عالمی اهلی غلام کمترین</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>شیر یزدانی بهل کز زخم غم شیر فلک</p></div>
<div class="m2"><p>همچو یوسف خسته ام دارد چو یعقوب حزین</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>شصت سالم جان چو زر در کوره مهرت گداخت</p></div>
<div class="m2"><p>تا شدم ز آلودگی صافی تر از ماء معین</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>این زمان از سکه لطفم چو زر کن سرخ رو</p></div>
<div class="m2"><p>تا شود نقدم روان در روز بازار حنین</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>تا فلک باشد جهان بادا بکام آل تو</p></div>
<div class="m2"><p>دوستانت شاد کام و دشمنان اندوهگین</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>عالمی دست دعا دارند بامن یا آله</p></div>
<div class="m2"><p>استجب عنی دعایی یا اله العالمین</p></div></div>