---
title: >-
    شمارهٔ ۱۰ - در منقبت شاه ولایت علی (ع) گوید
---
# شمارهٔ ۱۰ - در منقبت شاه ولایت علی (ع) گوید

<div class="b" id="bn1"><div class="m1"><p>آن شهنشاهی که بحر لافتی را گوهر است</p></div>
<div class="m2"><p>شحنه دشت نجف شاه ولایت حیدرست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جویبار ذوالفقار از آب رحمت پرورید</p></div>
<div class="m2"><p>گلشن هر دو سر ازان صاحب هر دو سرست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ذات پاک مرتضی را با کسی نسبت مکن</p></div>
<div class="m2"><p>زانکه این آب حیات از چشمه سار دیگرست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>معنی قول «علی بابها» آسان مدان</p></div>
<div class="m2"><p>کاین سخن را صد جهان معنی بهر بابی درست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سر سبحانی که پنهانست در ناد علی</p></div>
<div class="m2"><p>هم بمعنی مظهرش او هم بمعنی مظهرست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در ارادت اولیا را او موردست</p></div>
<div class="m2"><p>معجزات انبیا را مظهر او مصدرست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از فروع روی او خورشید ذرات جهان</p></div>
<div class="m2"><p>هر یکی جام جم و آیینه اسکندرست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هم شراب کوثر و هم آب خضر از لطف اوست</p></div>
<div class="m2"><p>آری آن نخل کرم هر جا بود بار آورست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>پیر و شاه نجف شو گر بکوثر مایلی</p></div>
<div class="m2"><p>زانکه آن آب بقا را خضر راهش رهبرست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>لحمک لحمی بدان و جسمک جسمی بخوان</p></div>
<div class="m2"><p>تا بدانی ذات حیدر از کدامین جوهرست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هر کرا باشد حصاری جز پناه مرتضی</p></div>
<div class="m2"><p>بشکند دست ولایت گر حصار خبیر است</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>پا به دوش مصطفی بهر شکست بت نهاد</p></div>
<div class="m2"><p>پایه قدرش نگر هر دو عالم برترست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>در شب جان باختن بر جای احمد تکیه کرد</p></div>
<div class="m2"><p>زانکه جای مصطفی هم مرتضی را در خورست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>شاهی ملک جهان او را سزد کز روی قدر</p></div>
<div class="m2"><p>هر گدای آستانش صد عزیز و قیصرست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>پیش لطفش هشت جنت وادیی باشد سراب</p></div>
<div class="m2"><p>نزد قهرش هفت دوزخ توده خاکسترست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>عالم علم نهان کاندر دبیرستان او</p></div>
<div class="m2"><p>تخته تعلیم طفلان قصه خیر و شرست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>تا جهان بودست بود و تا جهان باشد بود</p></div>
<div class="m2"><p>زانکه نفس کاملش واصل به وحی اکبرست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>از خطا کاری کز مهر او بویی نبرد</p></div>
<div class="m2"><p>گر همه آهوی مشکین است از سگ کمترست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>سینه نا پاک بد مهری که در جان و دلش</p></div>
<div class="m2"><p>جای شاهنشاه مردان نیست جای خنجرست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گر ز دشت ارژن و سلمان کسی یاد آورد</p></div>
<div class="m2"><p>آب گردد زهره او گر همه شیر نر است</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>هر کراکین غلامان علی در دل بود</p></div>
<div class="m2"><p>گر برادر باشدم گویم گناه مادر است</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>گر ز روی مقبلی قنبر غلام شاه بود</p></div>
<div class="m2"><p>من سگ آن بختیارم کو غلام قنبر است</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>یا امیر المونین آنی که گر گوید کسی</p></div>
<div class="m2"><p>نیست جز حب تو ایمان مومنان را باور است</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>هر که نبود میوه حب تواش چون خشک</p></div>
<div class="m2"><p>آتشش باید زدن گر خود همه عودتر است</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چون نسوزد دشمنت از داغ دل کاندر تنش</p></div>
<div class="m2"><p>چون انار از نازکی هر قطره خون یک اخگرست</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>خطبه برنامت چو خواند بلبل روح القدس</p></div>
<div class="m2"><p>گلبن طوبی ز روی پایه چوب منبر است</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بحر الطاف ترا دریای اخضر نیم موج</p></div>
<div class="m2"><p>بلکه هر یک قطره یی از آن چو بحر اخضرست</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>ای چراغ شرع و شمع دین دلیل راه شو</p></div>
<div class="m2"><p>کاندرین ظلمت سرا نور تو ما را رهبرست</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>کرد اهلی جان فدا بهر شهید کربلا</p></div>
<div class="m2"><p>وز «سقاهم ربهم» مزدش شراب کوثرست</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>گر حسودان همچو روبه دشمن جان ویند</p></div>
<div class="m2"><p>غم ندارد آنکه او را شیر یزدان رهبرست</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>سایه آل علی پاینده بادا کاین پناه</p></div>
<div class="m2"><p>سایبانی از برای آفتاب محشرست</p></div></div>