---
title: >-
    شمارهٔ ۶۳ - در منقبت حضرت امیرالمومنین
---
# شمارهٔ ۶۳ - در منقبت حضرت امیرالمومنین

<div class="b" id="bn1"><div class="m1"><p>تو شیر خدایی به یقین یا اسدالله</p></div>
<div class="m2"><p>سر بیشه تو عرش برین یا اسدالله</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شیران جهان صید تو اند از ره معنی</p></div>
<div class="m2"><p>شیر فلکت صید کمین یا اسدالله</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در عرش نگین داد رسولت شب معراج</p></div>
<div class="m2"><p>عرش است ترا زیر نگین یا اسدالله</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در ارژنه سلمان تو رهاندی ز کف شیر</p></div>
<div class="m2"><p>فریاد رسی در همه حین یا اسدالله</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بسم الله اگر بر سر آنی که چو مهدی</p></div>
<div class="m2"><p>ظاهر شوی از بیشه دین یا اسدالله</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>وقت است که از خون پلنگان</p></div>
<div class="m2"><p>یک رنگ کنی روی زمین یا اسدالله</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شیر علمت چون فکند سایه به محشر</p></div>
<div class="m2"><p>خورشید شود سایه نشین یا اسدالله</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در روضه روی با علم شیر سیاهت</p></div>
<div class="m2"><p>پیش از همه کس روز پسین یا اسدالله</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جز نور جهانگیر تو با نور محمد</p></div>
<div class="m2"><p>کی بود دو خورشید قرین یا اسدالله</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از شیر قیامت نبود به بفراست</p></div>
<div class="m2"><p>زان از همه ای خوبترین یا اسدالله</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بر گاو سخنگو ید بیضا تو نمودی</p></div>
<div class="m2"><p>در معرکه ی سحر مبین یا اسدالله</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>با قوت بازوی تو رستم چه شغالی است</p></div>
<div class="m2"><p>گر باز کنی پنجه کین یا اسدالله</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>در معرکه گاهی که غریو افکنی از جنگ</p></div>
<div class="m2"><p>در بیشه جَهَد شیر عرین یا اسدالله</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>در موج هلاکند نهنگان همه آندم</p></div>
<div class="m2"><p>کآرد به جبین خشم تو چین یا اسدالله</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>هم کعبه عزیز از تو و هم کعبه نشینان</p></div>
<div class="m2"><p>تو فخر مکانی و مکین یا یا اسدالله</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>در سلسله هر که بود حلقه ذکرت</p></div>
<div class="m2"><p>سر حلقه بود روح امین یا اسدالله</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>با اسم علی وصف عظیم آمده یعنی</p></div>
<div class="m2"><p>نام تو بود نام مهین یا اسدالله</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>از تیر بلانیست محبان ترا غم</p></div>
<div class="m2"><p>حب تو بود حصن حصین یا اسدالله</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>پایی به سر تشنه لبان نه که ز لطف است</p></div>
<div class="m2"><p>خاک قدمت ماه معین یا اسدالله</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چون بوی وفا میدمد از خیل سگانت</p></div>
<div class="m2"><p>صد صید سگت آهوی چین یا اسدالله</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>امید من آنست که خوانی سگ خویشم</p></div>
<div class="m2"><p>دارم ز تو امید همین یا اسدالله</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>فریاد رسی را که تویی در دم آخر</p></div>
<div class="m2"><p>فریاد رس جان حزین یا اسدالله</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>شادند جهانی بنوال کرم تو</p></div>
<div class="m2"><p>مگذار من خسته غمین یا اسدالله</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ما صید ضعیفیم تنابیم عتابت</p></div>
<div class="m2"><p>بر ما زره خشم مبین یا اسدالله</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>باشد که بلطف تو شود خاتمت خیر</p></div>
<div class="m2"><p>کردیم برین ختم سخن یا اسدالله</p></div></div>