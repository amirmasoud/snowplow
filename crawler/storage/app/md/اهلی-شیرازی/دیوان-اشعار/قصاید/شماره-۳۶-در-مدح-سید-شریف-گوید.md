---
title: >-
    شمارهٔ ۳۶ - در مدح سید شریف گوید
---
# شمارهٔ ۳۶ - در مدح سید شریف گوید

<div class="b" id="bn1"><div class="m1"><p>شکر خدا که مژده راحت فرا رسید</p></div>
<div class="m2"><p>آن ارزو که داشت دل ما بما رسید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آمد بهار زندگی و سبزه و نشاط</p></div>
<div class="m2"><p>گو خرش برآ که موسم نشو و نما رسید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از عزتش بخاک رسید آیت امان</p></div>
<div class="m2"><p>وز خاکیان بعرش خروش دعا رسید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>احرام کعبه بست دلم در صفای صدق</p></div>
<div class="m2"><p>بی سعی ره بکعبه صدق و صفا رسید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بیمار غم رسید به بزم وصال یار</p></div>
<div class="m2"><p>دیگر چه غم که خسته بدار الشفا رسید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یعقوب وار نرگس چشمم شکفته شد</p></div>
<div class="m2"><p>زان بوی پیرهن که ز باد صبا رسید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اینک رخم رسید بخاک رهش دگر</p></div>
<div class="m2"><p>مس پاره امید مرا کیمیا رسید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بازم ز دست جذبه خورشید وصل تو</p></div>
<div class="m2"><p>کاه ضعیف را کشش کهربا رسید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دردش بمن رسید و دوا شد نصیب غیر</p></div>
<div class="m2"><p>شکر خدا به هر چه مرا ار خدا رسید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>حقا که خوشگوار تر از صاف عشرتست</p></div>
<div class="m2"><p>درد جفای او که باهل وفا رسید</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آسوده بود مدتی از برق آه، دل</p></div>
<div class="m2"><p>یا آتشی بخرمنم افکند یا رسید</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تا کی ز دست هجر خمار بلا کشم</p></div>
<div class="m2"><p>ساقی بیا که رفع خمار بلا رسید</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>مجلس ز نور دم زند امروز کز سفر</p></div>
<div class="m2"><p>سید سریف بن علی مرتضا رسید</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>آن افتاب عهد که از آستان او</p></div>
<div class="m2"><p>هر ذره یی که تافت باوج علا رسید</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>از عرش بر گذشت سریر فضایلش</p></div>
<div class="m2"><p>آخر ببین که پایه دانش کجا رسید</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بر منتهای سدره نهال عدالتش</p></div>
<div class="m2"><p>طوبی صفت رسید و عجب منتها رسید</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>رخش قضا نکرد دگر تر کتازیی</p></div>
<div class="m2"><p>تا دست حکم او به عنان قضا رسید</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>تسبیح قدسیان فلک ذکر خیر اوست</p></div>
<div class="m2"><p>وز گنبد سپهر بگوش این صدا رسید</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بخشد دو کون و میرسدش اینسخا از آنک</p></div>
<div class="m2"><p>میراث بخشش ز شه لافتی رسید</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>هر بینوا که یافت ز خوانش نواله یی</p></div>
<div class="m2"><p>از آن نواله فیض بصد بینوا رسید</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>انس و پری چو مور و ملخ جوش میزنند</p></div>
<div class="m2"><p>بر خوان او همین که صدای صلا رسید</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ای آفتاب، ظل تو بر خاک اگر فتاد</p></div>
<div class="m2"><p>آن خاک ذره ذره باوج سما رسید</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>وانکسکه تافت روی ز خورشید رای تو</p></div>
<div class="m2"><p>چون سایه اش بلای سیاه از قفا رسید</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بر هر عدو که خشم تو چین بر جبین نمود</p></div>
<div class="m2"><p>کشتی زندگیش بموج فنا رسید</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>هرجا رسید لمعه یی از برق تیغ تو</p></div>
<div class="m2"><p>گوییکه آتش از نفس اژدها رسید</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>نشوو نمای خصم کجا میرسد به تو</p></div>
<div class="m2"><p>مشکل بشاخ سدره ز شوخی گیا رسید</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>کسرا نمیرسد چو تو لاف کرم زدن</p></div>
<div class="m2"><p>این موهبت ز گنج الهی ترا رسید</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>چونگل نماند دامن کس خالی از زری</p></div>
<div class="m2"><p>تادست بخشش تو بشاخ سخا رسید</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>در هر محل که کرد گدایی سوال فیض</p></div>
<div class="m2"><p>کس غیر بخشش تو نگفت این گدا رسید</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>چونسبزه صد هزار زبان شکر گوی گشت</p></div>
<div class="m2"><p>هرجا که رحمت تو ز ابر عطا رسید</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>عیسی دمی ز باد هوا زاد روح بخش</p></div>
<div class="m2"><p>زان ذره کز ره تو بباد هوا رسید</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>در سایه کسی که بپای تو سر نهد</p></div>
<div class="m2"><p>هرکو رسد بسایه فر هما رسید</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>من بنده حقیرم و نظم بلند من</p></div>
<div class="m2"><p>گر بر فلک رسید به یمن شما رسید</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>هر کس بقدر دستگه آورد تحفه یی</p></div>
<div class="m2"><p>دست مدیح خوان به در بی بها رسید</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>اهلی بآرزوی تو جان داد عاقبت</p></div>
<div class="m2"><p>راه عدم گرفت و بملک بقا رسید</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>چون در کمال وصف تو دستم نمیرسد</p></div>
<div class="m2"><p>کوته کنم حدیث که وقت دعا رسید</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>تا روزگار هست بمانی که روزگار</p></div>
<div class="m2"><p>خواهد ز دولت تو بامیدها رسید</p></div></div>