---
title: >-
    شمارهٔ ۳۰ - در تعریف جام شاه گوید
---
# شمارهٔ ۳۰ - در تعریف جام شاه گوید

<div class="b" id="bn1"><div class="m1"><p>این چه روح افزا شراب و این چه سیمین ساغرست</p></div>
<div class="m2"><p>چشمه خضرست یا آیینه اسکندرست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جوهر روح است یا گیتی نما بگداختند</p></div>
<div class="m2"><p>شیره جان است در وی یا می جان پرورست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نقش خط گرد لبش دل میبرد از دست خلق</p></div>
<div class="m2"><p>همچو آن خطی که بر گرد دهان دلبرست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر سواد دیده دارد چون سواد نام شاه</p></div>
<div class="m2"><p>از سواد دیده روشندلان روشن ترست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گرچه جام می نماید در کف شاه از صفا</p></div>
<div class="m2"><p>گر بمعنی بنگری جام شراب کوثرست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا دم محشر تهی یارب مباد این جام می</p></div>
<div class="m2"><p>ازدم شاهی که ساغر بخش روز محشرست</p></div></div>