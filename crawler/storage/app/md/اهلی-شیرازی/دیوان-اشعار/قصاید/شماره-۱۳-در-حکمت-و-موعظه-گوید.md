---
title: >-
    شمارهٔ ۱۳ - در حکمت و موعظه گوید
---
# شمارهٔ ۱۳ - در حکمت و موعظه گوید

<div class="b" id="bn1"><div class="m1"><p>آدمی مجموعه علم و حقیقت پروری است</p></div>
<div class="m2"><p>صورت زیبای او دیباچه صورتگری است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حشمت خیل ملک با قدر آدم هیچ نیست</p></div>
<div class="m2"><p>شوکت شاهنشهی بیش از علو لشکری است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با وجود بحر کشتی تخته اش بر سر زند</p></div>
<div class="m2"><p>با کمال عرش اعظم خاکیان را مهتری است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>قبله ملک و ملک آدم بحسن سیرت است</p></div>
<div class="m2"><p>آفتاب آیینه دلها ز نیکو اختری است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آدمی شو از تواضع خاک شو زیرا که دیو</p></div>
<div class="m2"><p>گردنش در طوق لعنت از غرور سروری است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پیش از آن کآتش شود ریحان خلیل آگاه بود</p></div>
<div class="m2"><p>کآتش نمرود از چوب بتان ازری است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چشم آخر بین ز گلبن خار می بیند نه گل</p></div>
<div class="m2"><p>عین خارست آنرخی کامروز گلبرگ طری است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عاقبت پیریست آنکو همچو نیلوفر در آب</p></div>
<div class="m2"><p>در رخش نارسته پیدا جعد زلف عنبری است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>پر ز مغروری مخند از گریه آخر بترس</p></div>
<div class="m2"><p>قهر شاهین در قفای قهقه کبک دری است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بسکه گردون بیخت خاک ما و گو هر بار کرد</p></div>
<div class="m2"><p>ما کنون با خاک میبازیم و گردون گوهری است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>لب ببند از فیض این بستان که هر کو چون انار</p></div>
<div class="m2"><p>قطره یی خورد آب او صد قطره خون دل گریست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>روح پرور همچو عیسی تن نمی پرور چو خر</p></div>
<div class="m2"><p>کآدمی جان است و جان را فربهی در لاغری است</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>حسن معنی جوی گو آرایش صورت مباش</p></div>
<div class="m2"><p>بی تکلف حسن را در حسن، دیگر زیوری است</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>عقل میخندد بر آنکس کو غم دنیا خورد</p></div>
<div class="m2"><p>دیده میگرید بر آنروییکه زرد از بیزریست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بنده حلق است دنیا دار و گوید خواجه ام</p></div>
<div class="m2"><p>عاشق خربندگی جاهل ز نام مهتری است</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>دب لا گر صبر داری همچو نوح اندوه نیست</p></div>
<div class="m2"><p>کشتی بیطاقتان سرگشته از بیلنگری است</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>قصر سلطان را حصار از سنگ و از آهن بود</p></div>
<div class="m2"><p>خانه صاحب توکل در حصار بی دری است</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>از تکلف در گذر تا نفس شوخی کم کند</p></div>
<div class="m2"><p>سر برون از غرفه شاهد را ز زیبا معجزی است</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>نفس فاسق توبه از عصیان ز بی برگی کند</p></div>
<div class="m2"><p>مهستی مستوریش از غایت بی چادری است</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>دامن از گرد جهان عارف فشاند آزاد شد</p></div>
<div class="m2"><p>گر ترا تر دامنی آلوده دارد از تری است</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>هرکه پاس ملک دارد ره کجا گردد خراب</p></div>
<div class="m2"><p>هر خرابی کآیدش سلطان ز ویران کشوری است</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>گر صدایی خیزد از ذرات عالم ذکر اوست</p></div>
<div class="m2"><p>سفله پندارد که چنگ زهره در خنیاگری است</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>یوسف جان با خریداران خود یکسان زند</p></div>
<div class="m2"><p>در چنین بازار اگر عیبی بود از مشتری است</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>معرفت مقصود حق آمد ز هر علمی که هست</p></div>
<div class="m2"><p>علم یکحرفست و دیگرها عبارت گستری است</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>عارف انسانست و باقی گاو و خر در شکل خلق</p></div>
<div class="m2"><p>کز برای نظم دهر ار هفت تار زرگری است</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>نقش هر صورت که آموزی بغیر از علم دین</p></div>
<div class="m2"><p>جمله رنگ آمیزی و افسانه و افسونگری است</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>علم علم دین بود در هر دو عالم کز شرف</p></div>
<div class="m2"><p>عقلش آنجا رهنمای و روحش اینجا رهبری است</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بینش صد سال کاهن یک نگاه کامل است</p></div>
<div class="m2"><p>چشم صاحبدل رصدگاه سپهر چنبری است</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>چشم سر را حکمت یونانیان روشن کند</p></div>
<div class="m2"><p>چشم سر را روشنی از حکمت پیغمبری است</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>قوت بازوی حکمت پیش ما معجز ماجراست</p></div>
<div class="m2"><p>با ید بیضا که او سرپنجه زور آوری است</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>در میان ما و حق دات نبی دانی که چیست</p></div>
<div class="m2"><p>روغن قندیل کش با آب و آتش همسری است</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>بی چراغ نور احمد کس نیابد راز حق</p></div>
<div class="m2"><p>ورنه روی از حق نتابد گر جهود خیبری است</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>در ره شرع نبی کز مو بود باریک تر</p></div>
<div class="m2"><p>کی فتد اورا که از دست ولایت یاوری است</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>فارغ از دوزخ بود صافی دل پاکیزه دین</p></div>
<div class="m2"><p>شد خلاص از داغ آتش هر زری کو جعفری است</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>هرکه رو در خدمت حق کرد و دست از کار شست</p></div>
<div class="m2"><p>در ادای خدمتش ترک ادب از خاوری است</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>ناله از گردون به نیک و بد مکن کش نیک و بد</p></div>
<div class="m2"><p>فاعل مختار داند چرخ از تهمت بری است</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>ما همه حیران مهر و مه ز منظرهای چرخ</p></div>
<div class="m2"><p>مهر و مه حیران ما هم زانکه غوغا این سری است</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>چهره مقصود از ظلمات عالم کس ندید</p></div>
<div class="m2"><p>رو بفقر آور که فقر آیینه اسکندری است</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>حرف درویشان چو نشنیدی ز حق چون دم زنی</p></div>
<div class="m2"><p>پیش دانا بیشتر خاموشی گنگ از کری است</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>هرکه آزار دلی جوید به هر صورت که هست</p></div>
<div class="m2"><p>گرچه باشد تیغ صاحب گوهر از بد گوهری است</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>نقش مدبر گرچه مقبل زاده باشد عاقبت</p></div>
<div class="m2"><p>موکشان بخت سیاهش در بلای ابتری است</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>ظالم از روسرخی امروز فردا زرد روست</p></div>
<div class="m2"><p>خجلت بدمست در روز خمار داوری است</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>بهر یوسف صد هزاران پیرهن شد پاره لیک</p></div>
<div class="m2"><p>تا قیامت بر زلیخا جرم پیراهن دری است</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>با سبکروحان قرین شو کز گرانجانان زهم</p></div>
<div class="m2"><p>بر دل مردم گران آید گر از حور و پری است</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>گر بصورت ناکسی پیش است در معنی پس است</p></div>
<div class="m2"><p>موی بر فرق سر و کیوان فراز مشتری است</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>گفتگو بسیار شد اهلی سخن کوتاه کن</p></div>
<div class="m2"><p>زانکه پر گفتن بر دانا نه از دانشوری است</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>فکر بکر من که زیر پرده لفظ است غرق</p></div>
<div class="m2"><p>زیر هر حرفی که بینی شاهدی در دلبری است</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>با وجود این کمال خود نمیدانم که شعر</p></div>
<div class="m2"><p>با کمال فضل، نقصان کمال انوری است</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>یارب از لطف خود و نور محمد آن همای</p></div>
<div class="m2"><p>کش ظهور از کعبه بد اکنون ز نام عسگری است</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>کز کرم در کار مسکینان نگاهی کن ز لطف</p></div>
<div class="m2"><p>کار ما مسکینی و شان تو مسکین پروری است</p></div></div>