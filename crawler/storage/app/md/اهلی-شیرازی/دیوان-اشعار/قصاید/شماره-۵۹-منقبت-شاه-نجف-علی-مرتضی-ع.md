---
title: >-
    شمارهٔ ۵۹ - منقبت شاه نجف علی مرتضی (ع)
---
# شمارهٔ ۵۹ - منقبت شاه نجف علی مرتضی (ع)

<div class="b" id="bn1"><div class="m1"><p>هرگز کجا پیدا شود چون شکل قد یار من</p></div>
<div class="m2"><p>نخل از عرب سرو از عجم ترک از ختا ماه از ختن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن زلف و روی لاله گون آنعارض و رنگ چو گل</p></div>
<div class="m2"><p>با مشگ خون با شب شفق با خور سحر با گل سمن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>روی و لب آن حوروش گوید بصد هشیار و مست</p></div>
<div class="m2"><p>اینک بهشت اینک شراب اینک عسل اینک لبن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با آب و تاب و رنگ و بو باشد ز عکس روی او</p></div>
<div class="m2"><p>گر لاله روید از چمن گر ارغوان گر نسترن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون من ضعیفی ناتوان دارد حریفی د جهان</p></div>
<div class="m2"><p>بس تند خوبس جنگجو بس عشوه گر بس غمزه زن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دارم هزاران داد از او باشد بفریادم رسد</p></div>
<div class="m2"><p>سلطان دین برهان حق شاه زمین میر زمن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شاه نجف گنج شرف بحر عطا کان سخا</p></div>
<div class="m2"><p>هم مرتضی هم مجتبی هم بوالعلا هم بوالحسن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شاهی که در روز غزا با صد هزاران اژدها</p></div>
<div class="m2"><p>یک اسبه شد یک مرده زد یک تو قبا یک پیرهن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ایینه شمشیر او گیتی ز دود از زنگ کفر</p></div>
<div class="m2"><p>حتی ختا حتی ختن حتی حبش حتی یمن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>جان از غم دین در غزا کردی فدا بهر خدا</p></div>
<div class="m2"><p>آن شاه دین آن شیر حق آن رهنما آن صف شکن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>زان شیر حق خواندش خدا کورا نبودی در وغا</p></div>
<div class="m2"><p>هرگز کمین هرگز کمان هرگز ز ره هرگز مجن</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>در طینت آن پاک دل در خاطر آن پاکدین</p></div>
<div class="m2"><p>حاشا خطا حاشا خلل حاشا شرر حاشا فتن</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>خواهم که از تیر قضا پیوسته باشد دشمنش</p></div>
<div class="m2"><p>تن در هدف جان در تلف پا در لحد سر در کفن</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>از ذوالفقار مرتضی افتاده صد دشمن ز جا</p></div>
<div class="m2"><p>لب بی زبان، کف بی بنان، تن بی روان، سر بی بدن</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چون خوان احسان می نهد لطفش صلا در میدهد</p></div>
<div class="m2"><p>کودیوودد، کو نیک و بد، کورندوشه، کو مرد و زن</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>او حرب جان و دل کند حاسد فریب بیهده</p></div>
<div class="m2"><p>دلها بدر جانها به زر اینها بمکر آنها بفن</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>یارش چوگل با آبرو خصمش چو خس زار و زبون</p></div>
<div class="m2"><p>اندک ثبات اندک زمان اندک وقار اندک ثمن</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>جز مهر او روز بلا دست که می گیرد ترا</p></div>
<div class="m2"><p>والله نه این والله نه آن والله نه تو والله نه من</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>هرجا عدوی خاندان در خاک بینی چون سگان</p></div>
<div class="m2"><p>حقا بکش حقا بران حقا بکش حقا بزن</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گر آب کوثر در گلو بی او رود آبش مگو</p></div>
<div class="m2"><p>گو زهر و گو تلخ آب غم گو صاف خون گو درد دن</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>گر شعله نار غضب ننشاند آب لطف او</p></div>
<div class="m2"><p>وای ایفلک وای ای ملک وای انجم و وای انجمن</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>آن گنج سر لو کشف جان گوید انذر حضرتش</p></div>
<div class="m2"><p>ای راستگو، ای راسترو، ای راست بین، ای راست زن</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>زان مقتدای عالمی کز علم و حلم و مردمی</p></div>
<div class="m2"><p>رایت ادب کارت حیا خویت سخا خلقت حسن</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>دزدید نورت شمع از ن اشکنجه و بندش کنند</p></div>
<div class="m2"><p>غل آتشش گاز انبرش دودش کمند کندش لگن</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>هرکس که روی از طاعتت برتافت نتوان گفتنش</p></div>
<div class="m2"><p>جز کجروش جز بدمنش جز بتگر و جز برهمن</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>کمتر شتربانت بود ویس قرن کورا قرین</p></div>
<div class="m2"><p>نارد زمین نارد زمان نارد قران نارن قرن</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>در خاک و خون چون کشتگان بدخواهت افتد در جهان</p></div>
<div class="m2"><p>بر چهره خون، بر سینه سر، بر سر سگان، بر تن زغن</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>شاید که در خلد برین گردد به رغم دشمنت</p></div>
<div class="m2"><p>شهد الشفا سم الفنا دارالامان بیت الحزن</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>پیدا و پنهان را همه جمعیت از نظم تو شد</p></div>
<div class="m2"><p>خواهی فلک، خواهی ملک، خواهی پری خواهی پرن</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>مداح اهل بیت شد اهلی، درین سنت بود</p></div>
<div class="m2"><p>خاکی نهاد آبی زمان قدسی مکان عرشی وطن</p></div></div>