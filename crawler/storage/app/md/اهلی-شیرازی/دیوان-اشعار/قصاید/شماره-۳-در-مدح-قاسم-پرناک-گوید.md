---
title: >-
    شمارهٔ ۳ - در مدح قاسم پرناک گوید
---
# شمارهٔ ۳ - در مدح قاسم پرناک گوید

<div class="b" id="bn1"><div class="m1"><p>زهی به تیغ شجاعت گرفته عالم را</p></div>
<div class="m2"><p>حدیث جنک تو جان تازه کرد ستم را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ابولمظفر منصور قاسم پرناک</p></div>
<div class="m2"><p>که جرعه نوشی جامت نمیسزد جم را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>غمی نماند جهان را بیمن دولت تو</p></div>
<div class="m2"><p>شکست شادی فتح تو لشگر غم را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هنوز اینهمه آثار صبح دولت توست</p></div>
<div class="m2"><p>تو آفتابی و خواهی گرفت عالم را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ببار گاه تو سر بر زمین نهد ورنه</p></div>
<div class="m2"><p>چه جای بر سر کرسی است عرش اعظم را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دمیکه قهر کنی چون قضای کن فیکون</p></div>
<div class="m2"><p>مجال نطق نماند مسیح مریم را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تو شیر معرکه و اژدهای رزم گهی</p></div>
<div class="m2"><p>صلابت تو جگر پاره کرد آدم را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز برق تیغ تو بگداخت لشگر دشمن</p></div>
<div class="m2"><p>چو آفتاب برآید چه تاب شبنم را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>محیط قهر تو جوشید و بیم طوفان شد</p></div>
<div class="m2"><p>ز بسکه زلزله در دل فتاد زمزم را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>حدیث رمح تو تا گفت باد در بیشه</p></div>
<div class="m2"><p>گرفت لرزه چونی استخوان ضیغم را</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مبارزان ترا روز جنگ و سرمستی</p></div>
<div class="m2"><p>زره بس اینکه پریشان کنند پرچم را</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مریض قهر تو جایی رسید تلخی او</p></div>
<div class="m2"><p>که همچو شهد خورد زهر ناب ارقم را</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بر.زگار تو ماتم نبود در عالم</p></div>
<div class="m2"><p>بمرگ خویش عدو زنده کرد ماتم را</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>دل حسودترا زخم بهتر از رحم است</p></div>
<div class="m2"><p>که مرده دل نشناسد ز نیش مرهم را</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بدولت تو خلل کی رسد ز گریه خصم</p></div>
<div class="m2"><p>بکوه و دشت خرابی نمیرسد نم را</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>اساس قهر ترا گوهر عدو بشکست (کذا)</p></div>
<div class="m2"><p>زسنگ ژاله چه نقصان بنای محکم را</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>هر آن گدا که برد بهره یی ز نعمت تو</p></div>
<div class="m2"><p>گدای خویش کند صد شه معظم را</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>قیاس قدر تو کردن چه کنه باری شد</p></div>
<div class="m2"><p>که راه نیست در آن پرده هیچ محرم را</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>نگین ملک سلیمان نشان قدرت بس</p></div>
<div class="m2"><p>گواه مهر نبوت بس است خاتم را</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بظاهر ارچه در آیین عیش میکوشی</p></div>
<div class="m2"><p>بباطن آیینه یی بایزید و ادهم را</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>سپهر مرتبتا، گوش کن غم اهلی</p></div>
<div class="m2"><p>تو فهم اگر نکنی اینحدیث مبهم را</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بخدمت از همه بیشم بقدر از همه پس</p></div>
<div class="m2"><p>چرا زیاد بری خدمت مقدم را</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>اگر نه لطف تو باشد طمع ز کس نکنم</p></div>
<div class="m2"><p>به نیم جو نخرم صد هزار حاتم را</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>همیشه تا چو رخ و موی نو خطای سنبل</p></div>
<div class="m2"><p>نقاب لاله کند جعد زلف پر خم را</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بهار عمر تو بادا شکفته تر هم دم</p></div>
<div class="m2"><p>خزان غم نرسد این بهار خرم را</p></div></div>