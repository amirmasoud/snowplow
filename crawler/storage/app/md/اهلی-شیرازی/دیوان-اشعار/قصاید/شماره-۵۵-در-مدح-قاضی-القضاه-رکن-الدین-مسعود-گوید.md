---
title: >-
    شمارهٔ ۵۵ - در مدح قاضی القضاه رکن الدین مسعود گوید
---
# شمارهٔ ۵۵ - در مدح قاضی القضاه رکن الدین مسعود گوید

<div class="b" id="bn1"><div class="m1"><p>در خاک و خونم از غم چون لاله داغ بردل</p></div>
<div class="m2"><p>دستم بگیر پایم بر آر از گل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خالت میان ابرو هندوی مست کرده</p></div>
<div class="m2"><p>دستی بدوش ترکی از هر طرف حمایل</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سرو تو میخرامد چون لعبت بهشتی</p></div>
<div class="m2"><p>وز سدره شاخ طوبی در سجده تو مایل</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شیرین کجا که یوسف در خواب هم نبیند</p></div>
<div class="m2"><p>آن حسن و آن لطافت آن شکل و آنشمایل</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آن کز بتان بلا شد حسن است یا ملاحت</p></div>
<div class="m2"><p>وان هردو معنی آمد بر صورت تو شامل</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در اشک من نگنجد کشتی نوح ای مه</p></div>
<div class="m2"><p>طوفان گریه ام بین در سینه ساز منزل</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بی خال تو چو موران در خاک هم نگنجم</p></div>
<div class="m2"><p>کز خرمن حیاتم یکدانه نیست حاصل</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ای آب زندگانی ما را دمیست باقی</p></div>
<div class="m2"><p>زنهار تا توانی از ما مباش غافل</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شوقم چو پرده در شد عیبم مکن بمستی</p></div>
<div class="m2"><p>ای جرم پوش دامن بر عیب ما فروهل</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هوشم ربود چشمت ترسم نگیردم دست</p></div>
<div class="m2"><p>قاضی قضات عالم سرچشمه فضایل</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>رکن سعادت و دین مسعود آنکه گویی</p></div>
<div class="m2"><p>کز غایت سعادت اقبال ازوست مقبل</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>سر سبزیی ز باغش فیروزی اکابر</p></div>
<div class="m2"><p>در یوزه یی ز فضلش مجموعه افاضل</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>از هشت خلد بروی حور و قصور ظاهر</p></div>
<div class="m2"><p>با آنکه در میان نه پرده است حایل</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>طرح بنای قدرش دست قدرت افکند</p></div>
<div class="m2"><p>آنجا اساس کعبه گرد آمد از فواصل</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چندان نکرد لیلی رحمت باشک مجنون</p></div>
<div class="m2"><p>کش ساربان دیده در آب راند محمل</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>از سنگ کودکانم دیوانه در ره تو</p></div>
<div class="m2"><p>مجنون که شد بصحرا دیوانه ایست عاقل</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>در مکتب علومش چرخ است ساده لوحی</p></div>
<div class="m2"><p>خیل ملک چو طفلان خوانند رب سهل</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>او چون مگس مقید در شهد لعل شیرین</p></div>
<div class="m2"><p>وارستگی چه جویی دست از حیات بگسل</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>با اشتران خیلش در روز عرض شوکت</p></div>
<div class="m2"><p>نه چرخ و طمطراقش یک چنبر جلاجل</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گر لنگری ز حملش با موج فتنه نبود</p></div>
<div class="m2"><p>کی نوح ز آب طوفان کشتی برد بساحل</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>گر آتشی زخشمش بر ابروی مه افتد</p></div>
<div class="m2"><p>از دود دل بر ارد هر ژاله رنگ فلفل</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>در نخل موم گیتی صد مسیله است پنهان</p></div>
<div class="m2"><p>خورشید فهم تیزش حل سازد این مسایل</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>صد ساله دخل عالم یکروزه خرج خوانش</p></div>
<div class="m2"><p>باقی ز فضل و رحمت بر کاینات فاضل</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بس کز خواص جودش دارد اثر طبایع</p></div>
<div class="m2"><p>چون ابر گوهر افشان مشرف شدست مدخل</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ای از جمال رویت آرایش مجالس</p></div>
<div class="m2"><p>وی از کمال خلقت آسایش محافل</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>زان حلقه ها که کردی در گوش ظالمانرا</p></div>
<div class="m2"><p>زنجیر عدل بسته نو شیروان عادل</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بی مهر خاتم تو صد بار مهر گردون</p></div>
<div class="m2"><p>مه را سجل دریده کاین حجتی است باطل</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>خط تو بر سفیدی نورست و دست موسی</p></div>
<div class="m2"><p>کلک تو در سیاهی هاروت و چاه بابل</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>چوبک زن سرایت بر پاسبان گردون</p></div>
<div class="m2"><p>در مهر چرخ خرواند یا ایها المزمل</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>نوریست در دو محمل انسان عین با تو</p></div>
<div class="m2"><p>احول دو دیده بیند این را دو بود محمل</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>چون خشم تو بجنبد چون فرش گل نلرزد</p></div>
<div class="m2"><p>کافتد بلرزه ماهی در آب از آن زلازل</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>تا از سموم قهرت خصم تو شربتی خورد</p></div>
<div class="m2"><p>پیوند استخوانش وارست از مفاصل</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>در زحمتیست خصمت جانش چو رشته در خار</p></div>
<div class="m2"><p>کز وی گسسته دق کرده علت سل</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>مرغی که آشیانش بام تو شد هما شد</p></div>
<div class="m2"><p>سلطان وقت گردد بر هر که افکند ظل</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>زر پاش آنچنان شد دستت که همچو قارون</p></div>
<div class="m2"><p>پایش بگل فروشد از بار سیم سایل</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>در طور همت توصد مشعل است روشن</p></div>
<div class="m2"><p>و انجا درختایمن جو بیست زان مشاعل</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>باز این غزلسرایی بشنو ز مزغ کلکم</p></div>
<div class="m2"><p>کز شوق او بر آید فریاد از عنادل</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>ای بسته لب بعاشق مشکلتر از در دل</p></div>
<div class="m2"><p>حرفی بگو و بگشا در یکنفس دو مشکل</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>تا سوی گشت باغت ساغر شود وسیله</p></div>
<div class="m2"><p>انگیخت لاله در سنگ در راه تو وسایل</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>عشرت بگلستان کن از جام لاله و می</p></div>
<div class="m2"><p>وز شاخ ارغوان زن برسیخ مرغ بسمل</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>بستان ز سبزه و گل دیبای رومی افکند</p></div>
<div class="m2"><p>کز سنبل و بفشه آمد ز چین قوافل</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>بادش دلیل عیسی کو مرده زنده سازد</p></div>
<div class="m2"><p>نخلش گواه مریم کز باد گشت حامل</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>در سایه درختان گل گل فتاره خورشید</p></div>
<div class="m2"><p>چون شامی و عراقی طرحی نهرمنداخل</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>گر صد دلیل خوبی گوید نسیم بر گل</p></div>
<div class="m2"><p>در مبحث جمالت نسخ است این دلایل</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>دیوانه وار چون من سرو از پیت دویدی</p></div>
<div class="m2"><p>گر پای او نبودی از آب دز سلاسل</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>در راه عشقبازی صد مرحله است ایدل</p></div>
<div class="m2"><p>در کعبه مراد آی بگذر ازین مراحل</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>ای آفتاب چون مه آسان نرفتم این ره</p></div>
<div class="m2"><p>تافرق سود پایم در قطع این منازل</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>مهجز نماست اهلی در شعر و از کمالش</p></div>
<div class="m2"><p>نازل مبین که دوری این آیتی است نازل</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>او مرغ گلشنی شد کز آفتاب رحمت</p></div>
<div class="m2"><p>در سایه کمالش صد ناقص است کامل</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>تادر بنای گیتی آب بقاست باقی</p></div>
<div class="m2"><p>تا برمدار گردون حکم قضاست عامل</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>از ابر رحمت تو گیتی مباد خالی</p></div>
<div class="m2"><p>در شغل خدمت تو گردون مباد عاطل</p></div></div>