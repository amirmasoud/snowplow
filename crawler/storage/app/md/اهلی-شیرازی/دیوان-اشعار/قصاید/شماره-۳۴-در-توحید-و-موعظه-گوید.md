---
title: >-
    شمارهٔ ۳۴ - در توحید و موعظه گوید
---
# شمارهٔ ۳۴ - در توحید و موعظه گوید

<div class="b" id="bn1"><div class="m1"><p>منت ایزد را که صنع او ز گل خار آورد</p></div>
<div class="m2"><p>خاک ما از قطره آبی پدیدار آورد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از هوا در گنبد سرها صدایی افکند</p></div>
<div class="m2"><p>تا به حکمت مشت خاکی را بگفتار آورد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>قدرت او ساخت در ترکیب تن هر گوشه یی</p></div>
<div class="m2"><p>مفصلی گردان که چونگردون برفتار آورد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در خیال صورتی کز قطره آب آفرید</p></div>
<div class="m2"><p>نقشبندان خرد را رو بدیوار آورد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نیست از صنعش عجب گر مریم از باد هوا</p></div>
<div class="m2"><p>همچو عیسی نشاه خورشید رخسار آورد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شربت آبی کز کرم بخشد نبات تشنه را</p></div>
<div class="m2"><p>چون رسد در کام نیشکر بار آورد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا نسازد روشن از کحل هدایت چشم دل</p></div>
<div class="m2"><p>بوالحکم بر مهجز پیغمبر انکار آورد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>غیرت او گر کشد تیغ مهابت از غلاف</p></div>
<div class="m2"><p>گردن فرعون را دربند اقرار آورد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر بدرد پرده ناموس بر اهل صلاح</p></div>
<div class="m2"><p>بایزیدان را برون از خرقه زنار آورد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خود فروشی گر کند یوسف بحسن خویشتن</p></div>
<div class="m2"><p>او چون غلامانش ببازار آورد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>عارف خود را کند مستغنی از نقش کتاب</p></div>
<div class="m2"><p>جوهر صافی دلش آیینه کردار آورد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هر یکی را نوبتی داد ارچه باهم آفرید</p></div>
<div class="m2"><p>احمد مختار را در وقت مختار آورد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>لایق هر کس بهر کس داد چیزی لاجرم</p></div>
<div class="m2"><p>ذوالفقاری بایدش جنگی که کرار آورد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نشکند غیر از دلی کو را خریداری کند</p></div>
<div class="m2"><p>گوهر قابل شکستن هم خریدار آورد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گر رود افتاده یی را پاز جای خویشتن</p></div>
<div class="m2"><p>کیست تا بر جای خود جز لطف جبار آورد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>پرده قاتل برای مصلحت قهرش درد</p></div>
<div class="m2"><p>ورنه لطفش جمله را در ظل ستار آورد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>دشمنان دوزخی را گر بسوزد در جهان</p></div>
<div class="m2"><p>مرغ را فرمان دهد کاتش بمنقار آورد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>جای حیرت نیست کر نمرود را سوزد بنار</p></div>
<div class="m2"><p>چون خلیل الله برون بی زحمت از نار آورد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>وای برما زینهمه زهری که شیطان میدهد</p></div>
<div class="m2"><p>گرنه تریاک کرم غفران غفار آورد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>نون رحمن کشتی نوح است و رنی چونکس</p></div>
<div class="m2"><p>طاقت یک موج قهر از بهر قهار آورد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>گر سرو زر مومنان دارند در راهش دریغ</p></div>
<div class="m2"><p>از پی تاراجشان کافر ز تاتار آورد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چرخ هم سر گشته چون پرگار در فرمان اوست</p></div>
<div class="m2"><p>چون تواند کسکه کار کس بپرگار آورد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>راستی را نیک و بد وابسته تقدیر اوست</p></div>
<div class="m2"><p>چونکند مدبر که خود در سلک ابرار آورد</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>جمله یکذاتند در اصل وجود ایشان ولی</p></div>
<div class="m2"><p>گه عزیز آرد برون از بطن و گخ خوار آورد</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>زر که یکجوهر بود گه قفل فرج استرست</p></div>
<div class="m2"><p>گه چو میل سرمه ره در چشم دلدار آورد</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>اوکه بندد در مشیمت زشت و زیبا قادرست</p></div>
<div class="m2"><p>که ز فاجر صالح و صالح ز فجار آورد</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>هیچ بیخیری مبین یعنی که شر محض نیست</p></div>
<div class="m2"><p>زهر دیدی مهره را بنگر که هم مار آورد</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>هر چه جزوی بینی اش خاصیت کلی در اوست</p></div>
<div class="m2"><p>عنکبوت ایزد برای پرده غار آرود</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>ای بسا زشتی که باشد خوبیی را واسطه</p></div>
<div class="m2"><p>دیو نفس است آنکه اینشکل پریوار آورد</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>رحمت حق از پی لب تشنگان محنت است</p></div>
<div class="m2"><p>لاجرم شربت طبیب از بهر بیمار آورد</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>خدمت حقکن غم روزی مخور کز خوان رزق</p></div>
<div class="m2"><p>قسمت هرکس وکیل رزق ناچار آورد</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>در سوار فقر باشد روشناییها بسی</p></div>
<div class="m2"><p>مردمان دیده را ظلمت در انوار آورد</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>گر نباشد نیستی در حضرت هستی متاع</p></div>
<div class="m2"><p>مالک دینار باشد هر که دینار آورد</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>پیر معنی مرد را باید چو فانوس خیال</p></div>
<div class="m2"><p>کز چراغ دل درون در سیراطور آورد</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>نگذرد از جای خود سالک که چشمش بسته اند</p></div>
<div class="m2"><p>گرد و صددوران بسر چون گاو عصار آورد</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>نربیت جایی اثر دارد که جوهر قابل است</p></div>
<div class="m2"><p>کی بکوشش نارون چون ناربن بار آورد</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>عاشقان مست را دست از تکلیف بسته اند</p></div>
<div class="m2"><p>کی سر مجنون کسی در بند دستار آرود</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>تا وبال جان بود نفست بلا بینی ز جان</p></div>
<div class="m2"><p>گنج را تامار باشد رنج و تیمار آورد</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>دل که غیر از دوست خواهد دوست نتوان داشتن</p></div>
<div class="m2"><p>دشمنست آندوستی کو رو به اغیار آورد</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>ز آرزوی شاهدان خود رامیفکن در عذاب</p></div>
<div class="m2"><p>کآخرت این آرزو بیزار بیزار آورد</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>چون دلت می خواهد و معشوقه و آواز چنگ</p></div>
<div class="m2"><p>گر ملک باشی که فی الحالت نگونسار آورد</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>لذت خورد و خورش جانت اسیر نفس کرد</p></div>
<div class="m2"><p>دانه خوردن مرغ را در دام طرار آورد</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>شهوت از سیر حقیقت روح باز آرد که مرغ</p></div>
<div class="m2"><p>ز آرزوی دانه اندر دام طرار آورد</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>از لجام نفس و شهوت مرد ره آزاده است</p></div>
<div class="m2"><p>شیر از آن نبود که سر در قید افسار آورد</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>کی بمردار جهان شیران حق رغبت کنند</p></div>
<div class="m2"><p>هم سگ نفس خسیسان رو به مردار آورد</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>نفس کافر دشمنست از وی حذر کن زینهار</p></div>
<div class="m2"><p>یا بکش یا جهد چندانکن که زنهار آورد</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>زاری نادان بغیر از آرزوی نفس نیست</p></div>
<div class="m2"><p>شیر جستن طفل را در گریه زار آورد</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>گر مجرد همچو عیسی نیستی زحمت مکش</p></div>
<div class="m2"><p>هرکه بار دل برد همچون خران بار آورد</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>راحت از هر یار نتوان دید یار خویش باش</p></div>
<div class="m2"><p>ای بسا محنت که یاری بر سر یار آورد</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>غایت حکمت بود وحشت ز خلق روزگار</p></div>
<div class="m2"><p>ورنه افلاطون چرا رو سوی کهسکار آورد</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>طبع را کم کن بد آموزی بهر جزوی، مباد</p></div>
<div class="m2"><p>ناگهان در کلیی این شیوه در کار آورد</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>از طریق شرع بیرون رفتن از گمراهی است</p></div>
<div class="m2"><p>کمتر از موری مشو کو ره بهنجار آورد</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>جز کلام حق مکن تعویذ خود نا دیو نفس</p></div>
<div class="m2"><p>زیر فرمان تو این تعویذ و طومار آورد</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>گر شوی شب زنده دار از مکر شیطان ایمنی</p></div>
<div class="m2"><p>دزد را کی زهره کو رو سوی بیدار آورد</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>پیرو حق شو که دست سامری کوته شود</p></div>
<div class="m2"><p>چون کلیم الله دست معجز آثار آورد</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>همچو آدم خاک شو پندار شیطانی بهل</p></div>
<div class="m2"><p>تانه چون او گردنت در طوق پندار آورد</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>نقش هستی را بدست خود فروشو تاترا</p></div>
<div class="m2"><p>جبرییل عشق در معراج اسرار آورد</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>کعبه جان جای تست آنجا به هشیاری رسی</p></div>
<div class="m2"><p>مست ره گمکرده اندر خانه هشیار آورد</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>در طواف کعبه دلهای ارباب صفا</p></div>
<div class="m2"><p>گر کنی سعیی طوافت چرخ دوار آورد</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>جان به رقص آورز و جددل که تن کز پا فتاد</p></div>
<div class="m2"><p>در سماعش بار دیگر چرخ فخار آورد</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>ایمن از عالم مشو گر شد بسر طوفان نوح</p></div>
<div class="m2"><p>ای بسا موجی چنان کان بحر خونخوار آورد</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>گرچه حلاجی برون کن پنبه غفلت ز گوش</p></div>
<div class="m2"><p>زانکه این همکاسه سر را بر سر دار آورد</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>راست شو با حق که حق با راستان در راستیست</p></div>
<div class="m2"><p>مکر او کجبازی اندر کار مکار آورد</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>راستی را قلب بازی، بازی خود دادن است</p></div>
<div class="m2"><p>کآسمان دربند دایم دست عیار آورد</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>سایه داری از خسان کم جو که کی گردد شجر</p></div>
<div class="m2"><p>گر گیاهی خویش را بالا به اشجار آورد</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>در کمال ناکسان کی بوی خاصیت بود</p></div>
<div class="m2"><p>خار صحرا هم گل بی نفع بسیار آورد</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>شرکت معنی بصورت نیست کز انجم سهیل</p></div>
<div class="m2"><p>فیض او رنگ عقیق و رنگ بلغار آورد</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>بی غرض گوید سخن اعلی از آن قدرش بود</p></div>
<div class="m2"><p>ورنه باقدر سخن سنجان چه مقدار آورد</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>شعرا گر باشد لباس شاهد معنی نکوست</p></div>
<div class="m2"><p>ورنه عاقل فخر کی هرگز به اشعار آورد</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>شعر رنگین از تکلیف نقش چین و آب جوست</p></div>
<div class="m2"><p>رو بدریا کن که گوهر بحر ذخار آورد</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>اینچنین نقد روانی را که در میزان نظم</p></div>
<div class="m2"><p>هز یک از روی نکته معنا بمعیار آورد</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>مخزن المعنیش خوان بلکه معانی، با خرد</p></div>
<div class="m2"><p>هم زنام اعداد تاریخش بتذکار آورد</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>خواهم از عین کرم آنکسکه چون آبحیات</p></div>
<div class="m2"><p>چشمه خود را بظلمات شب تار آورد</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>قابل روشندلان گر داند این نظم حقیر</p></div>
<div class="m2"><p>قایلش را از کرم در سلک اخیار آورد</p></div></div>