---
title: >-
    شمارهٔ ۲۰ - در ماتم و عزای حسین (ع) گوید
---
# شمارهٔ ۲۰ - در ماتم و عزای حسین (ع) گوید

<div class="b" id="bn1"><div class="m1"><p>چرخ از شفق نه صاعقه در خرمنش گرفت</p></div>
<div class="m2"><p>خون حسین تازه شد و دامنش گرفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گردون که سوخت ز آتش لب تشنگی حسین</p></div>
<div class="m2"><p>آن آتش بلاست که پیرامنش گرفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بود از خطای چرخ که آهوی مشگبار</p></div>
<div class="m2"><p>در صیدگاه عمر سگ دشمنش گرفت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>باد اجل بکشت چراغی که بر فلک</p></div>
<div class="m2"><p>قندیل مهر و مه ز دل روشنش گرفت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شب در عزای اوست سیه پوش صبحدم</p></div>
<div class="m2"><p>گویی که در گلو نفس از شیونش گرفت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در خون نشست ساکن نه مسکن فلک</p></div>
<div class="m2"><p>از رستخیز گریه که در مسکنش گرفت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از داغ لاله بسوخت چنان لاله زین عزا</p></div>
<div class="m2"><p>کاتش ز داغ سینه به پیراهنش گرفت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>یکقطره آب ابر بر آن تشنگان نریخت</p></div>
<div class="m2"><p>زان بر زبان طعنه زدن سوسنش گرفت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>روزم شب از عزای حسین است و روزگار</p></div>
<div class="m2"><p>زان است تیره روز که آه منش گرفت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آهم بسوخت خانه دل وین گواه بس</p></div>
<div class="m2"><p>دود سواد دیده که در روزنش گرفت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>پر شد ز خون دیده من آنچنان فلک</p></div>
<div class="m2"><p>کز خون هزار چشمه چو پرویزنش گرفت</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بر اهل بیت و آل علی مرحمت نکرد</p></div>
<div class="m2"><p>شمر لعین که لعنت مرد و زنش گرفت</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>سگ در قلاده شمر لعین است در جهان</p></div>
<div class="m2"><p>وین طوق لعنت است که در گردنش گرفت</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>از تاب برق قهر تن شمر کی رهد</p></div>
<div class="m2"><p>گر همچو نقش آینه در آهش گرفت</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ای وای او که آتش خشم خدا چو شمع</p></div>
<div class="m2"><p>از پای تا بسر همه جان و تنش گرفت</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بد بخت هر دو کون شد از کودنی یزید</p></div>
<div class="m2"><p>کاین راه ناصواب دل کودنش گرفت</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>زیر زمین ز مکمن غیبش عذابهاست</p></div>
<div class="m2"><p>تنه نه دست مرگ درین مکمنش گرفت</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>همسایه هم ز پهلوی او سوخت زیر خاک</p></div>
<div class="m2"><p>زان آتش عذاب که در مدفنش گرفت</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>خون حسین آنکه پی لعل و در بریخت</p></div>
<div class="m2"><p>آن لعل و در شد آتش و در مخزنش گرفت</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>آن کو امان نداد بخون حسین و آل</p></div>
<div class="m2"><p>فریاد الامان همه در مامنش گرفت</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>این نور چشم شاهسواریست کآسمان</p></div>
<div class="m2"><p>کحل نظر ز گرد سم توسنش گرفت</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>شاهی که خرمن فلکش در کرم جوی</p></div>
<div class="m2"><p>ارزنده نیست بلکه کم از ارزنش گرفت</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>جای یزید در چه ویل است یا علی</p></div>
<div class="m2"><p>کافراسیاب خشم تو چون بیژنش گرفت</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>هرگز نخفت خون شهدان کربلا</p></div>
<div class="m2"><p>پرویز خون کوهکن جان کش گرفت</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>تن پیش تیر مرگ نهنگان بجان نهد</p></div>
<div class="m2"><p>ماهی جگر نداشت که در جوشنش گرفت</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>او مرغ سدره بود نکرد التفات هیچ</p></div>
<div class="m2"><p>بر گلشن جهان و کم از گلخنش گرفت</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>دنیا بهر صفت که بود آخرش فناست</p></div>
<div class="m2"><p>خوش آنکه چون حسن صفت احسنش گرفت</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>اهلی طمع بخرمن گردون چه میکنی</p></div>
<div class="m2"><p>بی خون دل دو دانه که از خرمنش گرفت</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>دل از متاع دهر ببر گر مجردی</p></div>
<div class="m2"><p>عیسی شنیده یی که بیک سوزنش گرفت</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بشناس خویش را که بری ره بسوی دوست</p></div>
<div class="m2"><p>هرکس که در شناخت ره معدنش گرفت</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>یارب تو دستگیر که شخص ضعیف ما</p></div>
<div class="m2"><p>شیطان نفس در نفس مردنش گرفت</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>فریادرس تو باش که گر فتنه ره زند</p></div>
<div class="m2"><p>کی ره توان برستم و رویین تنش گرفت</p></div></div>