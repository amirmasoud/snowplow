---
title: >-
    شمارهٔ ۱۶ - ایضا در مدح یعقوب خان گوید
---
# شمارهٔ ۱۶ - ایضا در مدح یعقوب خان گوید

<div class="b" id="bn1"><div class="m1"><p>خط تو چون بردمید رونق عنبر شکست</p></div>
<div class="m2"><p>سرو تو چون قد کشید قدر صنوبر شکست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>طره پرچین چرا مالش گوش تو داد</p></div>
<div class="m2"><p>نسترنت از چه رو برگ گل تر شکست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آه که تا دم زدم سنبل مشکین او</p></div>
<div class="m2"><p>موی بمو جعد یافت خم بخم اندر شکست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اینهمه شبنم چراست در رهت ای گل مگر</p></div>
<div class="m2"><p>چشم گهربار من حقه گوهر شکست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>باد صبا کز رهت برد گران گل فشاند</p></div>
<div class="m2"><p>عاشق دیوانه را خار ببستر شکست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عشوه شیرین تو لب چو سوی من گزید</p></div>
<div class="m2"><p>آه که در چشم من رونق شکر شکست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آب رخ کاه من اشک صراحی بریخت</p></div>
<div class="m2"><p>شیشه ناموس من خنده ساغر شکست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بسکه ضعیفم بخواب شب چو شدی همبرم</p></div>
<div class="m2"><p>دست خیالت مرا پهلوی لاغر شکست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دور شه متقی است نرگس مستت چرا</p></div>
<div class="m2"><p>میکده پرشور ساخت صومعه را در شکست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سایه لطف خدا حضرت یعقوب خان</p></div>
<div class="m2"><p>آنکه سلیمان صفت قدر سکندر شکست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>حامی دین نبی آنکه بعهدش صبا</p></div>
<div class="m2"><p>شیشه و جام شراب بر سر عبهر شکست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>باد چو با لاله گفت قصه پرهیز او</p></div>
<div class="m2"><p>ساغر خود را بسنگ با می احمر شکست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نیست عجب گر شود تازه ز انفاس او</p></div>
<div class="m2"><p>خشک گلی کش ورق در ته دفتر شکست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>تاجوری را که نیتس داغ غلامی ز وی</p></div>
<div class="m2"><p>دست قضا لاله وار بر سرش افسر شکست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>هرکه نه بر نام او سکه زد آن زر بسوخت</p></div>
<div class="m2"><p>هرکه نه زو خطبه خواند پایه منبر شکست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بسکه بخلق و کرم گوهر و زر پخش کرد</p></div>
<div class="m2"><p>قیمت گوهر نماند مرتبه زر شکست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>شوکت بختش ببزم حشمت جم پست ساخت</p></div>
<div class="m2"><p>رایت فتحش برزم سنجق سنجر شکست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>زهره شیر فلک صولت خیلش درید</p></div>
<div class="m2"><p>مهره گاو زمین انبه لشکر شکست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>غیرت دین خلیل دارد از آن هر کجا</p></div>
<div class="m2"><p>معنی او جلوه کرد صورت آزر شکست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ایکه برد زنگ دل روشنی روی تو</p></div>
<div class="m2"><p>جام جم رای تو آینه خور شکست</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>روی زمین بخت تو از کرم حق گرفت</p></div>
<div class="m2"><p>پشت عدو تیغ تو از دم حیدر شکست</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>سنگی اگر روزرزم از سم رخش تو جست</p></div>
<div class="m2"><p>از سر خاقان گذشت افسر قیصر شکست</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>تیغ چو آبت بجنگ بسکه قیامت کند</p></div>
<div class="m2"><p>آتش دوزخ نشاند صولت محشر شکست</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>گرچه عنان گیریت آرزوی باد بود</p></div>
<div class="m2"><p>رخش تو این آرزو در دل صرصر شکست</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>تیر تو مانند کاف سینه گردون شکافت</p></div>
<div class="m2"><p>تیغ تو چون لاله الف قد دو پیکر شکست</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>چتر مه از بسکه دید سرزنش از چتر تو</p></div>
<div class="m2"><p>چون مه نو آخرش حلقه چنبر شکست</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>نقش نگین ترا مه بحلیل بر گرفت</p></div>
<div class="m2"><p>مهر از آن غیرتش مهر مزور شکست</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>حاکم عدلت بلطف دست ستمکش گرفت</p></div>
<div class="m2"><p>شحنه امرت بقهر فرق ستمگر شکست</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>لطف تو بحری کزو یافته ماهی قرار</p></div>
<div class="m2"><p>قهر تو برقی کزان قلب سمندر شکست</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>دشمن خارت چو گل با همه خفتان چو لعل</p></div>
<div class="m2"><p>ژاله قهرش بفرق غنچه مغفر شکست</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>برق قضا گرم شد دشمن بی نفع سوخت</p></div>
<div class="m2"><p>باد اجل تند گشت گلبن بی بر شکست</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>خصم تو در روز جنگ زه ز کمانش گسست</p></div>
<div class="m2"><p>دست چو بر تیغ زد دسته خنجر شکست</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>گلشن قدرت ندید گلبن جاهت نیافت</p></div>
<div class="m2"><p>مرغ دلم کاینهمه بال زد و پر شکست</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>باز شنو اینغزل کز شکرستان طبع</p></div>
<div class="m2"><p>طوطی شکر شکن شکر دیگر شکست</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>دست و دل دشمنم دوش بهم بر شکست</p></div>
<div class="m2"><p>همت ما عاقبت این در خیبر شکست</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>تا دگر آن مست ناز قصد که دارد که باز</p></div>
<div class="m2"><p>بند قبا چست کرد طرف کله برشکست</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>بختم ازین بیشتر ره بوصالت نداد</p></div>
<div class="m2"><p>خار رهم زان بپا اینهمه نشتر شکست</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>آه، که بنیاد زهد عشوه شوخت بکند</p></div>
<div class="m2"><p>وای که ناموس دین غمزه کافر شکست</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>من بجفای توام شاد که لیلی بلطف</p></div>
<div class="m2"><p>گرهمه را داد دل دلشده را سر شکست</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>بخت نگر کآخرم آن لب جانبخش کشت</p></div>
<div class="m2"><p>جام حیات مرا چشمه کوثر شکست</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>دل ز تو آباد بود گشت خراب از تو باز</p></div>
<div class="m2"><p>کشور ما را بنا بانی کشور شکست</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>روی تو ژولیده مو موی تو آشفته رو</p></div>
<div class="m2"><p>با سپه روم و زنگ شاه مظفر شکست</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>جم صفتا، روشنست پیش تو چون آفتاب</p></div>
<div class="m2"><p>کاین گهر افلاک را زینت و زیور شکست</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>در خور سلمان نبود پیروی انوری</p></div>
<div class="m2"><p>کز گذر قافیه قافله را برشکست</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>من چو برابر زدم رایت خورشید نظم</p></div>
<div class="m2"><p>کوکبه انوری چون مه انور شکست</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>قیمت اهلی مگر از تو فزاید که دهر</p></div>
<div class="m2"><p>رونق دانش ببرد قدر سخنور شکست</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>تا نه مکرر شود قصه دعایت کنم</p></div>
<div class="m2"><p>گرچه ازین شکرم قند مکرر شکست</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>تا رخ هر روزه دار در هوس شام عید</p></div>
<div class="m2"><p>روز بروز از گداز هست چو مه بر شکست</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>روزه و عید تو باد فرخ و در کار تو</p></div>
<div class="m2"><p>یکسر مو ناورد گردش اختر شکست</p></div></div>