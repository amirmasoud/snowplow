---
title: >-
    شمارهٔ ۴۵ - در مرثیه نجم السعادت گوید
---
# شمارهٔ ۴۵ - در مرثیه نجم السعادت گوید

<div class="b" id="bn1"><div class="m1"><p>از جهان رفت آنکه مانندش درین عالم نبود</p></div>
<div class="m2"><p>شاه میداند که هرگز مثل او آدم نبود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>علم و حلم و دانش و لطف و مروت جمله داشت</p></div>
<div class="m2"><p>غیر عمر از آنچه می بایست هنچش کم نبود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون مسیحا گر چه می بخشید جان مرده را</p></div>
<div class="m2"><p>کار چون با خود فتادش مهلت یکدم نبود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رفت تا در ملک جان سازد بنای جاودان</p></div>
<div class="m2"><p>زانکه دید این خانه گل را بقا محکم نبود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زخم هر کس را که می بینیم دارد مرهمی</p></div>
<div class="m2"><p>آه ازین زخمیکه هیچش در جهان مرهم نبود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دشمن ناکس که آخر چشم او اینکار کرد</p></div>
<div class="m2"><p>زهر چشمش کم ز زهر افعی و ارقم نبود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یوسف گمگشته پیدا گشت و آنمحبوب جان</p></div>
<div class="m2"><p>آنچنان گم شد که پنداری درین عالم نبود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>حضرت نجم السعادت آفتاب مرحمت</p></div>
<div class="m2"><p>آن بلند اختر که عرش از ذات او اعظم نبود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>پیش خورشید ضمیرش هرگز از ذرات کون</p></div>
<div class="m2"><p>ذره یی پنهان نگشت و نکته یی مبهم نبود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ذره یی کز خاکپای او بگردون میرسید</p></div>
<div class="m2"><p>گر نبود افزون ز ماه آسمان کم هم نبود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>صد هنر انگیخت چون جمشید طبع روشنش</p></div>
<div class="m2"><p>آنچه پیدا میشد از وی کار جام جم نبود</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>پاس دلها آنچنان میداشت کاندر عهد او</p></div>
<div class="m2"><p>غیر زلف گلرخان اشفته و درهم نبود</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>تا حکیم او بود افلاطون نبود آوازه اش</p></div>
<div class="m2"><p>تا کریم او بود نام بخشش حاتم نبود</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بسکه می بارید در عالم زر از ابر کفش</p></div>
<div class="m2"><p>دست کس چونگل بعهدش خالی از درهم نبود</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بر ره او کس شبی ننهاد سر، کش بامداد</p></div>
<div class="m2"><p>دامن از گوهر گران چونلولو از شبنم نبود</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>آسمان نقص هنر کردی و او دادی رواج</p></div>
<div class="m2"><p>با فلک بد شد از آن شان دوستی با هم نبود</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>در هنرمندی و دانش از که بالاتر نرفت</p></div>
<div class="m2"><p>درچه علم استاد شد شخصی که او اعلم نبود</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>سنبل زلف کدامین گل نیاشفت از غمش</p></div>
<div class="m2"><p>نرگس چشم که خونپالا درین ماتم نبود</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بسکه بود از گریه مردم بکویش زمزمه</p></div>
<div class="m2"><p>کس نبود آنجا که چشمش چشمه زمزم نبود</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>دود آه دوستان آتش بعالم می فکند</p></div>
<div class="m2"><p>دشمنان را دیده هم از خون دل بی نم نبود</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بی خراش غم ندیدم سینه یی همچون نگین</p></div>
<div class="m2"><p>بی عقیق خون دل یک دیده چون خاتم نبود</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>آه از این نیلی خم گردون که از وی هر که را</p></div>
<div class="m2"><p>جرعه شادی برآمد بی غبار غم نبود</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>دل منه چونغنچه بر عیش جهان کز این چمن</p></div>
<div class="m2"><p>هیچکس چونسبزه بیش از هفته یی خرم نبود</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>سوسنی آزاده نامد در جهان کاخر چو رفت</p></div>
<div class="m2"><p>جامه نیلی کرده از این نیلگون طارم نبود</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>کس ترقی همچو ماه نو نکرد از مهر چرخ</p></div>
<div class="m2"><p>کاندران افزودنش کسری در آن مدغم نبود</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>خاک باد اینکاسه گردون که کس از وی نیافت</p></div>
<div class="m2"><p>شربت نوشی که با وی زهر حسرت ضم نبود</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>سر پنهان اجل ظاهر نمیگردد بکس</p></div>
<div class="m2"><p>دم مزن این راز را اهلی که کس محرم نبود</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>ختم کن یارب برحمت حال او کانجام کار</p></div>
<div class="m2"><p>کس خلاص از چنگ مردن ز آدم و خاتم نبود</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>از جهان این سایه گر کم گشت عمر شاه باد</p></div>
<div class="m2"><p>شکر حق باری که مویی از سر او کم نبود</p></div></div>