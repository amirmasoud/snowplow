---
title: >-
    شمارهٔ ۹ - در منقبت شاه نجف (ع)گوید
---
# شمارهٔ ۹ - در منقبت شاه نجف (ع)گوید

<div class="b" id="bn1"><div class="m1"><p>شاه نجف که هر دو جهان در پناه اوست</p></div>
<div class="m2"><p>هرجا سری که هست همه خاک راه اوست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با مهر شاه هست نشانی که چون سهیل</p></div>
<div class="m2"><p>بر هر که تافت رنگ عقیقی گواه اوست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بغض علی نشان بناگوش زردی است</p></div>
<div class="m2"><p>با هرکه هست فاش زرنگ چو کاه اوست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>برهم زند چو عرصه شطرنج باد قهر</p></div>
<div class="m2"><p>هر عرصه یی که جز اسدالله شاه اوست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زان گفت (او کشف) که دو گیتی نگاه کرد</p></div>
<div class="m2"><p>گیتی نمای هر دو جهان یک نگاه اوست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تنها چراغ مه نبود شمع بر رهش</p></div>
<div class="m2"><p>قندیل عرش سوخته بارگاه اوست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هرجا که هست سفره شاه است در میان</p></div>
<div class="m2"><p>او پیر عالم است و جهان خانقاه اوست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نان جوین این خلف، آدم خرید باز</p></div>
<div class="m2"><p>زان گندمی که این همه تخم گناه ازوست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>طوفان چو نوح دید بکشتی پناه برد</p></div>
<div class="m2"><p>او شد به منجنیق که طوفان براه اوست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نور علی اگر نشود رهبر یقین</p></div>
<div class="m2"><p>صد چون خلیل راهزن اشتباه اوست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>زان حسن و خال سبز و رگ هاشمی فتاد</p></div>
<div class="m2"><p>یوسف دلش بچاه که دلها بچاه اوست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>فقر علی طلب که سلیمان به تخت داشت</p></div>
<div class="m2"><p>باد است هر سخن که نه تخت و کلاه اوست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>امروز گر سپاه سکندر جهان گرفت</p></div>
<div class="m2"><p>فردا به زیر سایه خیل و سپاه اوست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>جمشید اگر بشاه و گدا پادشاه شد</p></div>
<div class="m2"><p>کمتر گدای شاه نجف پادشاه اوست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گر عمر خضر یافت کسی بی ولای شاه</p></div>
<div class="m2"><p>مردن به از درازی عمر تباه اوست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>کشت اژدها چو رستم و عمرش دو ماه بود</p></div>
<div class="m2"><p>صد سال عمر رستم دستان دو ماه اوست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>جانبخش و جان ستان همه شاه ولایت است</p></div>
<div class="m2"><p>جانبخشی مسیح هم از دستگاه اوست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>زان برگزیده ساخت بدامادی خودش</p></div>
<div class="m2"><p>فخر رسل که سلطنت فقر جاه اوست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>زوج مطهرش که به زهرا بود علم</p></div>
<div class="m2"><p>آنرا که نام زهره زهراست داه اوست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گردون دو گوشواره عرش آورد گواه</p></div>
<div class="m2"><p>کو هم یکی ز حلقه بگوشان راه اوست</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>برج دوازده مه تابان ازو تمام</p></div>
<div class="m2"><p>وین پرتو شرف همه را هم ز ماه اوست</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>در سایه عنایت او اولیا همه</p></div>
<div class="m2"><p>خوش گلشنی که اینهمه گلها گیاه اوست</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>یا بوالحسن کی است که از خاندان تو</p></div>
<div class="m2"><p>خورشید دین پناه بر آید که گاه اوست</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>عالم شبست و مهر تو چون روز روشن است</p></div>
<div class="m2"><p>و آن را که نیست وای بروز سیاه اوست</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>از آه آتشین جگر دشمن تو سوخت</p></div>
<div class="m2"><p>ای وای او که دوزخ او هم ز آه اوست</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>جان یزید سگ که بسگ باد حشر او</p></div>
<div class="m2"><p>بانگ شغال هر طرف از آه آه اوست</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>تیغت دمی مسیح و دمی اژدهاست لیک</p></div>
<div class="m2"><p>صد اژدها هلاک دم عمر کاه اوست</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>با دلدل تو توسن گردون جنیبت است</p></div>
<div class="m2"><p>زی ات ماه نو که به پشت دوتاه اوست</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>اهلی که یافت گنج سعادت به لطف تست</p></div>
<div class="m2"><p>از فیض مهر حیدر و فضل اله اوست</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>یارب بحق شاه ولایت که عفو کن</p></div>
<div class="m2"><p>جرم گدا که هم کرمت عذر خواه اوست</p></div></div>