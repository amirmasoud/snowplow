---
title: >-
    شمارهٔ ۶۷ - در مدح گوید
---
# شمارهٔ ۶۷ - در مدح گوید

<div class="b" id="bn1"><div class="m1"><p>ای عکس از آفتاب ترا همبر آینه</p></div>
<div class="m2"><p>کس را چه حد که تیز بیند در آینه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون عکس خود در آینه بینی و لب گزی</p></div>
<div class="m2"><p>او هم ز دیدن تو گزد لب در آینه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عیسی نماید از فلک آبگینه رنگ</p></div>
<div class="m2"><p>چون پرتو جمال تو افتد در آینه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تسخیر عکس روی تو آیینه کرد از آن</p></div>
<div class="m2"><p>در شیشه کرده است پری راهر آینه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آیینه شد ز عکس تو بتخانه ایصنم</p></div>
<div class="m2"><p>گر باورت زمن نشود بنگر آینه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عکس رخت چو شمع برو افکند شود</p></div>
<div class="m2"><p>در وصف روت شمع زبان آور آینه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سخنش گلو مگیر که ترسم ز عکس تو</p></div>
<div class="m2"><p>خونش برو فتد چو گل احمر آینه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون بوسه خواهم از تو جوابت بجز نه نیست</p></div>
<div class="m2"><p>یکبار هم بگوی جوابی ور آینه</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از عکس آتشین رخ و از برق حسن تو</p></div>
<div class="m2"><p>گویی که مشعلی است پر از اخگر آینه</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چون قصه مصور چین و فرنگی است</p></div>
<div class="m2"><p>دعوی چه میکند بتو ای دلبر آینه</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از نقش خط و لب همچو شکرت</p></div>
<div class="m2"><p>چون طوطی است در دهنش شکر آینه</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ز آن روی آتشین و سر زلف عنبرین</p></div>
<div class="m2"><p>دود دلش بسر شده چون مجمر آینه</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>داغ ترا بناخن خود تازه تر کنم</p></div>
<div class="m2"><p>زین به که دید صیقله را در خور آینه</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>دایم چراغ دیده ز روی تو روشن است</p></div>
<div class="m2"><p>چون از فروغ سرور دین پرور آینه</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>فرخنده باد طلعت شه کز جمال او</p></div>
<div class="m2"><p>شد نور بخش همچو شه خاور آینه</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>رایش نظر بچرخ فکنده است از آنحکیم</p></div>
<div class="m2"><p>سازد برای حال تو خود اختر آینه</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چون او نزاد مادر گیتی و مثل او</p></div>
<div class="m2"><p>زاییده هم نمی شود الا در آینه</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>تا در دیار دشمنش آتش زند فلک</p></div>
<div class="m2"><p>تابد چو آفتاب ز اسکندر آینه</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>از تف خون دشمن و گرد سمند اوست</p></div>
<div class="m2"><p>گردون اگر ز زنگ شدش اخضر آینه</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بسیار کوفت آهن سرد و نشد چو نعل</p></div>
<div class="m2"><p>کش رو نهد بزیر سم اشقر آینه</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ای از جمالت آینه صورت آشکار</p></div>
<div class="m2"><p>وی صورت جمال ترا مظهر آینه</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>با روشنی رای تو از شرم میرود</p></div>
<div class="m2"><p>اخگر صفت بپرده خاکستر آینه</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>گر طوطیی ز خیل تو پروا کند برو</p></div>
<div class="m2"><p>زان خرمی سزد که برآرد پر آینه</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>گر بت نما بعد تو گردد شود خراب</p></div>
<div class="m2"><p>در یکنفس چو بتکده خاور آینه</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>آید بچشم خصم تو خورشید تاب دار</p></div>
<div class="m2"><p>چون با شرر ز کوره آهنگر آینه</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>پیش تو زرد رو و زبون همچنان بود</p></div>
<div class="m2"><p>خصم تو گر کند بمثل از زر آینه</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>مثلت ندید چرخ کهن گرچه پیش چشم</p></div>
<div class="m2"><p>عینک صفت نهاده ز ماه و خور آینه</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>با آفتاب روی تو گر رو برو شود</p></div>
<div class="m2"><p>گردد چو مه گداخته سرتاسر آینه</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>در یا دلا گداخته شد گوهر دلم</p></div>
<div class="m2"><p>تا ساخام ز بهر تو از گوهر آینه</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>ز آیینه پرس صورت حالم که روشن است</p></div>
<div class="m2"><p>گر چه چو نامه نیست سخن گستر آینه</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>از گوهرست آینه بکر شعر من</p></div>
<div class="m2"><p>کس را نداد دست ازین خوشتر آینه</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>آیینه رو سفید ازین شعر گشت و کس</p></div>
<div class="m2"><p>چون من سفید رو نکند دیگر آینه</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>خصم این سواد شعر نیارد بچشم از آن</p></div>
<div class="m2"><p>کش میخ دیده هاست درویکسر آینه</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>اهلی که شعر روشنش آیینه دل است</p></div>
<div class="m2"><p>شد بر دعای جان توانش رهبر آینه</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>تا نو عروس مهر بود بر کنار چرخ</p></div>
<div class="m2"><p>تا چرخ را بود زمه انوار آینه</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>یا رب که باد در کف مشاطه قضا</p></div>
<div class="m2"><p>از آفتاب ذات تو تا محشر آینه</p></div></div>