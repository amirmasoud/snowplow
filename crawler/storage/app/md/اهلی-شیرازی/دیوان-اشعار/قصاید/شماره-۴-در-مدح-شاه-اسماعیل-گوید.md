---
title: >-
    شمارهٔ ۴ - در مدح شاه اسماعیل گوید
---
# شمارهٔ ۴ - در مدح شاه اسماعیل گوید

<div class="b" id="bn1"><div class="m1"><p>رسید آن گل که می بخشد طراوت گلشن جان را</p></div>
<div class="m2"><p>نسیمش برگرفت از خاک ره تخت سلیمان را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عزیزان هر طرف پویان که یوسف سوی مصر آمد</p></div>
<div class="m2"><p>جوانان تهنین گویان ز هر سو پیر کنعان را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گذشت ایام بی برگی رسید آن نو بهار اکنون</p></div>
<div class="m2"><p>که رشک از گلشن شیراز باشد باغ رضوان را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شه ایران و توران زان سوی شیراز رو آرد</p></div>
<div class="m2"><p>که خاکش قبله حاجت بود ایران و توران را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگر هر ذره این خاک خورشیدی شود شاید</p></div>
<div class="m2"><p>که چشم التفات افتاد بروی ظل یزدان را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سپهر سلطنت خورشید عالم شاه اسماعیل</p></div>
<div class="m2"><p>که چترش سایه بر سر افکند خورشید تابان را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سمند عرش فرسایش گذر بر ساحتی دارد</p></div>
<div class="m2"><p>که زیر سم چوپای مور مالد فرق کیوان را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>فضای کشور عالم چو عرض فسحتش نبود</p></div>
<div class="m2"><p>کجا گنجایش خیلش بود این تنگ میدان را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو زهرآلود زنبوری است تیر آهنین نیشش</p></div>
<div class="m2"><p>که همچون خانه زنبور کرد از رخنه سندان را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز پاس شحنه عدلش چو دست ظلم کوته شد</p></div>
<div class="m2"><p>غم کاهی زباد فتنه نبود کشت دهقان را</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>اگر در گلشن عالم نسیم لطف او نبود</p></div>
<div class="m2"><p>کجا سر سبزی جاوید باشد سرو بستان را</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چنان پیرانه سر حد شریعت بسته در دلها</p></div>
<div class="m2"><p>که راه فتنه در طبع جوانان نیست شیطان را</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>الا ای آفتاب دین تویی آن مرکز دولت</p></div>
<div class="m2"><p>که چون پرگار در گردش در آری چرخ گردان را</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نهنگی چون تو در هیبت اگر چین بر جین آرد</p></div>
<div class="m2"><p>مجال دم زدن دیگر نماند موج طوفان را</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ز خیل بند گانت گر کهن زالی برون تازد</p></div>
<div class="m2"><p>به مردی دست بر بندد هزاران پور دستان را</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>عدو کی پنجه خورشید اقبال تو تاب آرد</p></div>
<div class="m2"><p>کجا فرعون تاب آرد کف موسی عمران را</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>تو آن نو شیروان عدلی که در گلشن سرای دهر</p></div>
<div class="m2"><p>فکندی طرح معموری بکندی بیخ طغیان را</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>پس از معماری عدل تو در این عالم ویران</p></div>
<div class="m2"><p>نیابد کس بصد گنج گهر یک کنج ویران را</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>در ایام تو باغ دهر دارد فیروزی</p></div>
<div class="m2"><p>بعهد گل کمال خرمی باشد گلستان را</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>زرشگ کلک زرپاشت گرهها در دل دریا</p></div>
<div class="m2"><p>ز دست زرفشانت رخنه ها در جان بود کان را</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بقدر نعمت خوان تو نعمت خواره میبود</p></div>
<div class="m2"><p>ز مشرق تا به مغرب میکشیدی خوان احسان را</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>فرو تر می نماید پیش تو چون عکس در دریا</p></div>
<div class="m2"><p>فلک هر چند بالاتر برد فیروزه ایوان را</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>شها، گوش رضا چون گل بمن کن تا درین گلشن</p></div>
<div class="m2"><p>غزل گویی بیاموزم زنو مرغ خوش الحان را</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>که گفت ای چرخ دور افکن رما آن آب حیوان را</p></div>
<div class="m2"><p>بخون ما شهیدان تشنه کن ریگ بیابان را</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ز شوق کعبه وصلت چنان در خاک می غلطم</p></div>
<div class="m2"><p>که از گل باز نشناسد تنم خار مغیلان را</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>مبند آن زلف کز آشفتگی حال دگر دارد</p></div>
<div class="m2"><p>بحال خویشتن بگذار زلف عنبر افشان را</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>شود صد خرمن گل گر دو سنبل خوشه چین باشد</p></div>
<div class="m2"><p>اگر جمع آورد زلف تو دلهای پریشان را</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>دل بی درد هر غیری چه ذوق از شیوه ات یابد</p></div>
<div class="m2"><p>مرا جان رخنه شد هر گه که دیدم لعل خندان را</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>ز حسرت خشگ بر جاسر و چون صورت فروماند</p></div>
<div class="m2"><p>اگر در جلوه ناز آوری سرو خرامان را</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>مرا از تاب خون خوردن گریبان چاک و بی دردش</p></div>
<div class="m2"><p>گمان کز مستی می پاره می سازم گریبان را</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>تو چون فریاد رس کس را بخواهی کشتن ای بدخو</p></div>
<div class="m2"><p>بگوش شه رسانم من ز بیداد تو افغان را</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>فلک قدرا، برفعت پایه قدر توزان بیش است</p></div>
<div class="m2"><p>که ذیل مدحتت در چنگ عقل افتد ثناخوان را</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>سخن پیش تو چون گویم که نطق معجز آثارت</p></div>
<div class="m2"><p>بعجز افکنده هنگام فصاحت صد سخندان را</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>تو آن شاهی که از مدح تو اهلی جای آن دارد</p></div>
<div class="m2"><p>که خاقانی گدای خویش سازد بلکه خاقان را</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>همیشه تا سخن سنجان دعا گوی شهان باشند</p></div>
<div class="m2"><p>همیشه تا سخن گویند شاهان سخندان را</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>جهانبانی ترا باشد ثنا گویی مرا باشد</p></div>
<div class="m2"><p>که همچون من ثنا گویی سزد سان جهانبان را</p></div></div>