---
title: >-
    شمارهٔ ۳۱ - ایضا در جام شاه گوید
---
# شمارهٔ ۳۱ - ایضا در جام شاه گوید

<div class="b" id="bn1"><div class="m1"><p>ساقیا جام تو از آب حیاتش چه کم است</p></div>
<div class="m2"><p>می حیات ابد و ساغر می جام جم است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اینچه جام است و می صاف که از پرتو اوست</p></div>
<div class="m2"><p>عکس خورشید که در آینه صبحدم است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اینچه نقشست و رقم اینچه سوادست و بیاض</p></div>
<div class="m2"><p>که سواد نظرم سوخته این رقم است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اینچه خط است که سرچشمه حرفیکه دروست</p></div>
<div class="m2"><p>چشمه آب حیاتی ز صفای قلم است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دوستکامی به ازین نیست که می نوش کنی</p></div>
<div class="m2"><p>کوری دیده دشمن که گرفتار غم است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یارب این جام فرحناک مبادا خالی</p></div>
<div class="m2"><p>از کف شاه که سرچشمه فیض کرم است</p></div></div>