---
title: >-
    شمارهٔ ۷۳ - در مرگ فرزند خود گوید
---
# شمارهٔ ۷۳ - در مرگ فرزند خود گوید

<div class="b" id="bn1"><div class="m1"><p>ای جگر گوشه که پاک آمدی و پاک شدی</p></div>
<div class="m2"><p>چشم من بودی و از چشم بدان خاک شدی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بود پرواز بلندت هوس ایمرغ بهشت</p></div>
<div class="m2"><p>عالم خاک بهشتی و بر افلاک شدی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دلت از تلخی ز هر غم ایام گرفت</p></div>
<div class="m2"><p>به شفا خانه غیب از پی تریاک شدی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ساحت کوی فنا معرکه شیران است</p></div>
<div class="m2"><p>طفل بودی و درین معرکه بی باک شدی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>میدویدی پی بازی چو غزالان ز نشاط</p></div>
<div class="m2"><p>صید کردت اجل و بسته فتراک شدی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پای ننهاده برون یک قدم از خانه خویش</p></div>
<div class="m2"><p>سوی آن خانه عجب چابک و چالاک شدی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نو بهار دل من بودی و نشکفته هنوز</p></div>
<div class="m2"><p>برگ تاراج خزان چون ورق تاک شدی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دامن افشاندی ازین خاک غم آلوده دهر</p></div>
<div class="m2"><p>آفرین باد ترا کز همه غم پاک شدی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>لاف عرفان نزنم پیش توای جان پدر</p></div>
<div class="m2"><p>که تو رهبر بسر گنج عرفناک شدی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>من پس مانده طلبکار و تو ایجوهر پاک</p></div>
<div class="m2"><p>پیشتر رهبر این پرده ز ادراک شدی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مردم از زخم فراقت که توای نخل جوان</p></div>
<div class="m2"><p>همچو تیراز بر این پیر چگر چاک شدی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>صبر کن اهلی اگر زخم غمی واقع شد</p></div>
<div class="m2"><p>که نه تنها تو درین واقعه غمناک شدی</p></div></div>