---
title: >-
    شمارهٔ ۵۷ - در موعظه گوید
---
# شمارهٔ ۵۷ - در موعظه گوید

<div class="b" id="bn1"><div class="m1"><p>ایدل گدایی از کرم کار ساز کن</p></div>
<div class="m2"><p>خود را زمنت همه کس بی نیاز کن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>توحید چیست ترک تعلق ز هر چه هست</p></div>
<div class="m2"><p>یعنی بروی غیر در دل فراز کن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گوهر بزهر چشم نیز زد ز ناکسان</p></div>
<div class="m2"><p>بگذار مار مهره ز زهر احتراز کن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مور لییم چند شوی ما گنج باش</p></div>
<div class="m2"><p>یعنی که خاک در دهن حرص و از کن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جان را که تیره ساخته یی از هوای نفس</p></div>
<div class="m2"><p>چون شمع روشن از نفس جانگداز کن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خیز ای پسر که قافله عمر میرود</p></div>
<div class="m2"><p>در خواب ناز تا به کیی چشم باز کن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ناز پریوشان همه دیوانگان خرند</p></div>
<div class="m2"><p>در حسن عقل کو و بر ایشان تو ناز کن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>رحمان ذاشتن پی شیطان شدن خطاست</p></div>
<div class="m2"><p>ما خود نهفته ایم تو خود امتیاز کن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ای شیخ شهر سجده دیوار تا بکی</p></div>
<div class="m2"><p>بشناس قبله اول وآنگه نماز کن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کس دل بکس نداد که دلداریی ندید</p></div>
<div class="m2"><p>رو در حقیقت آر و قیاس مجاز کن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>طبل نهی است از عمل خلق گفتگو</p></div>
<div class="m2"><p>چنگ امل هم از عمل خویش ساز کن</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ای خفته، روز عمر ترا رو بکوتهی است</p></div>
<div class="m2"><p>این روز کوته از شب طاعت دراز کن</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>اینراه کعبه نیست که زادش توکل است</p></div>
<div class="m2"><p>این راه محشرست برو توشه ساز کن</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>حاجت بترک تاج ندارد طریق عشق</p></div>
<div class="m2"><p>محمود باش و بندگی چون ایاز کن</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>تن را مکن بدار چو منصور سر فراز</p></div>
<div class="m2"><p>جان را بدار ملک بقا سرفراز کن</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گر خلعت حقیقت او نیست در برت</p></div>
<div class="m2"><p>آن جامه را ز طرز شریعت طراز کن</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>هر کش چراغ دل نه زنور محمدی است</p></div>
<div class="m2"><p>چون شمع سر جدا ز تن او به گاز کن</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>قانون بوعلی مرض تن دوا کند</p></div>
<div class="m2"><p>جان را دوا ز حکمت شاه حجاز کن</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>شاه از عرض نوازش مستغنیان کند</p></div>
<div class="m2"><p>رو در ناب آن شه مسکین نواز کن</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بازی مخور یجلوه طاووس بیهنر</p></div>
<div class="m2"><p>بازآی و صید دولت این شاهباز کن</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>معراج شهسوار عرب از علی بپرس</p></div>
<div class="m2"><p>با عقل بوعلی سخن از ترکتاز کن</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>آنجا که معجزست ره عقل و فهم نیست</p></div>
<div class="m2"><p>از عقل فهم شعبده حقه باز کن</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ارباب صدق میوه ز باغ نبی خورند</p></div>
<div class="m2"><p>ای ژاژخای همچو شتر رو به ژاز کن</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>عاشق نیی که بوی گلت گریه آورد</p></div>
<div class="m2"><p>هان ای فسرده گریه ببوی پیاز کن</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>اهلی اگر ز اعهل دلانی زبان ببند</p></div>
<div class="m2"><p>خاموش باش و همدمی اهل راز کن</p></div></div>