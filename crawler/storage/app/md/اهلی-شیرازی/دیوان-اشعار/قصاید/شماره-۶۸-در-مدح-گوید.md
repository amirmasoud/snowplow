---
title: >-
    شمارهٔ ۶۸ - در مدح گوید
---
# شمارهٔ ۶۸ - در مدح گوید

<div class="b" id="bn1"><div class="m1"><p>کهن داغ جگر را تازه می‌سازد مگر لاله</p></div>
<div class="m2"><p>که از داغش دمادم می‌رود آتش به سر لاله</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز بهر داغ سواد میکند فصدش حکیم دهر</p></div>
<div class="m2"><p>از آن خون سیه ریز در درون طشت زر لاله</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چه خون دل مجنون بجای باده در صحرا</p></div>
<div class="m2"><p>درون آتش اندازد ز بهر او جگر لاله</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز فعل چرخ پر حیلت هرآنکس تو عجب دارد</p></div>
<div class="m2"><p>که مه چون بر شفق گیرد گشاید چشم بر لاله</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نهانست آفتاب گل مگر در غنچه کز هر سو</p></div>
<div class="m2"><p>برافکندست طرف آفتابی را ز سر لاله</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تو گویی جام مینایی بمیل از کوره آتش</p></div>
<div class="m2"><p>برون می آرد از باد هوا چون شیشه گر لاله</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بدان ماند که آتش در میان آتش افتاد</p></div>
<div class="m2"><p>صبات میسازد از هر گوشه اش زوشن دگر لاله</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو مجنون آتش شیقش بموی سر در افتاد</p></div>
<div class="m2"><p>از آن آتش ندارد از سر خودهم خبر لاله</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خم زر بر سر مینا نهد نرگس که پر سازد</p></div>
<div class="m2"><p>قدح بهر چراغ دیده اهل هنر لاله</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نسیم نو بهار از باغ عیشش تاوزد دایم</p></div>
<div class="m2"><p>فلک شد سبزه و اختر شکوفه شمع خور لاله</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گهی از باد قهر او بخاک افتاده چتر گل</p></div>
<div class="m2"><p>گه از فیض نسیم لطف او شد تا جور لاله</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چو طفل مکتبی تعلیم تا گیرد ز کلک او</p></div>
<div class="m2"><p>بآب ژاله شوید صبحدم لوح بصر لاله</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>دلیلش دود دل آمد که در تکرار درس او</p></div>
<div class="m2"><p>بزیر کاسه شب دارد چراغی تا سحر لاله</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>اگر بی امر او از سر بپا برگی بیندازد</p></div>
<div class="m2"><p>زند از دست نادانی بپای خود تبر لاله</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>و گر پا از حصار گلشن لطفش نهد بیرون</p></div>
<div class="m2"><p>خورد بر پهلو از خار بیابان نیشتر لاله</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>دل کوه از نهیب قهر او خونست از آن دایم</p></div>
<div class="m2"><p>بجای خون برون میآمد از کوه کمر لاله</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>الا ای دیده تابان تو آن خورشید تابانی</p></div>
<div class="m2"><p>که از فیضت بر آرد لعل سیراب از حجر لاله</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چمن اسباب بزمت تامهیا کرده گلها را</p></div>
<div class="m2"><p>ز بهر عطر در مجمر فکنده عود تر لاله</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چو در بستان نگون از باد گردد لاله ها را سر</p></div>
<div class="m2"><p>ز بهر مجلس عیش تو فانوسی است هر لاله</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>برنگ گردن اسب تو خواهد دوخت ز انجامه</p></div>
<div class="m2"><p>سقر لاط سیاه و شرخ را در یکدگر لاله</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ز بس کز خون خصمت لاله گون گشتست فرش خاک</p></div>
<div class="m2"><p>برآرد گرد باد اکنون ز خاک دشت و در لاله</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ز بهر جان بد خواه تو همچون مالک دوزخ</p></div>
<div class="m2"><p>بگرز آهنین گویا برون جست از سقر لاله</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>چو خواهد سوی ملک عدم با دشمنت آخر</p></div>
<div class="m2"><p>نگه میدارد آبش برگ را بهر سفر لاله</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بدفع چشم زخمت از سواد چشم و تخم مهر</p></div>
<div class="m2"><p>سپند کشته سوزد بر سر هر رهگذر لاله</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>جهاندارا، او صاف تو اهلی لاله زاری گفت</p></div>
<div class="m2"><p>که پروردش بآب دیده و خون جگر لاله</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>از آن پر گل کنار گلشن مدح تواش کردم</p></div>
<div class="m2"><p>که دایم بر کنار باغ روید بیشتر لاله</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>چو من دارد کنار چاک لاله ودنه بایستی</p></div>
<div class="m2"><p>که بودی دامنش از ابر لطفت پر گهر لاله</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>مگر همطالع من شد که از بخت سیه چون من</p></div>
<div class="m2"><p>تنور افتاده نانی یافت قسمت از قدر لاله</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>همیشه تا فروغ مهر گردد بر شفق پیدا</p></div>
<div class="m2"><p>فروزان تر از آن شبنم که باشد صبح در لاله</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>چراغ دولتت باد از دم روشندلان پیدا</p></div>
<div class="m2"><p>کزین بهتر نیابد گلشن اهل نظر لاله</p></div></div>