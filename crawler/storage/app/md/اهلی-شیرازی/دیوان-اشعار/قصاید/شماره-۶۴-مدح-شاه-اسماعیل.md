---
title: >-
    شمارهٔ ۶۴ - مدح شاه اسماعیل
---
# شمارهٔ ۶۴ - مدح شاه اسماعیل

<div class="b" id="bn1"><div class="m1"><p>آن شهنشاهی که ملک دین مسخر ساخته</p></div>
<div class="m2"><p>آفتاب روی او عالم منور ساخته</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دست قدرت صیقل روی زمین آراسته</p></div>
<div class="m2"><p>تبع سلطان شاه اسماعیل حیدر ساخته</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ملک هر دو عالم از لطف و کرامت لطف اوست (کذا)</p></div>
<div class="m2"><p>تا نپنداری بدین ملک محقر ساخته</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تند باد غیرت او همچو اوراق بهار</p></div>
<div class="m2"><p>صد هزاران نسخه بدعت مبتر ساخته</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با کمال هر دو عالم همچو شاه اولیه</p></div>
<div class="m2"><p>خویشتن را پیرو دین پیغمبر ساخته</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نام شاه اسماعیل را از اتحاد معنوی</p></div>
<div class="m2"><p>هم باسما علی ایزد مصور ساخته</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا بملک دین نبندد رخنه یا جوج کفر</p></div>
<div class="m2"><p>دیر مینا گنبد سد سکندر ساخته</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آفتاب ملک او تا سایه بر عالم فکند</p></div>
<div class="m2"><p>سنگ و خاک از فیض رحمت لعل و گوهر ساخته</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آن سعادت بخش شاهان بنده یی را چون زحل</p></div>
<div class="m2"><p>بر سر تخت هفت افلاک کشور ساخته</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>لطف و قهر او بیکدم صد حال را</p></div>
<div class="m2"><p>مرده قارون و دو صد قارون قلندر ساخته</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خوان لطفش هر کرا بخشی مقرر کرده است</p></div>
<div class="m2"><p>جاودان آن بخش چون رزق مقدر ساخته</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دست حکمش کرده دست کهر با از کاه دور</p></div>
<div class="m2"><p>تیغ عدلش آب و آتش هر دو همسر ساخته</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>عدل نو شروانی او حلقه زنجیر داد</p></div>
<div class="m2"><p>هریکی را طوقی از بهر ستمگر ساخته</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>غازیان مست او روز غزا صد دیو را</p></div>
<div class="m2"><p>کاسه سرها شکسته جام و ساغر ساخته</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>خطبه اثنا عشر تا در جهان سازد بلند</p></div>
<div class="m2"><p>عرشی و کرسی را ز روی پایه منبر ساخته</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گویی اسماعیل قربان است کز ناموس دین</p></div>
<div class="m2"><p>جان خود قربان برای امر داور ساخته</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>محو گردد ز آفتاب تیغ او جان عدو</p></div>
<div class="m2"><p>گر چو گردون ثابت و سیاره لشکر ساخته</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>زر که میل سرمه اش سازند و در چشمان کشند</p></div>
<div class="m2"><p>حشمت او بین که قفل فر استر ساخته</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ای جهان لطف و بحر جود و کان مرحمت</p></div>
<div class="m2"><p>اسمان کی آدمی مثل تو دیگر ساخته</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>یک اشارت هر کجا خورشید تیغت کرده است</p></div>
<div class="m2"><p>پیکر شیر فلک در دم دو پیکر ساخته</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>انبیا و اولیا و غیرت دین امام</p></div>
<div class="m2"><p>همت خود را بمعنی با تو یاور ساخته</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>آن شقی خاندان کرد از پی اثبات خصم</p></div>
<div class="m2"><p>کعبه ویران کرده است از جهل و خیبر ساخته</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>معنیی کز علم الاسماء بآدم شد عیان</p></div>
<div class="m2"><p>فیض حق در هیکل پاک تو مضمر ساخته</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>خر گه قدرت به عمر خود کجا یابد فلک</p></div>
<div class="m2"><p>کو ازین خرگه هنوز امروز چنبر ساخته</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>حمزه قدر تو در میدان شوکت روز عرض</p></div>
<div class="m2"><p>تاج زرین تهمتن نعل استر ساخته</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>مهر مهر آل حیدر سکه بر زر میزند</p></div>
<div class="m2"><p>زان سبب نظم مرا مدح تو چون زر ساخته</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بنده دیرین شاهم روزگارم زنده باز</p></div>
<div class="m2"><p>در ادای خدمت سلمان و بوذر ساخته</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>گرچه درویشم چو اهلی گنج مهر اهل بیت</p></div>
<div class="m2"><p>با وجود فقرم از عالم توانگر ساخته</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بر دعای دولتت ختم سخن شد آخرم</p></div>
<div class="m2"><p>بلکه حق این دولت از اول مقرر ساخته</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>تا قیامت سایه ات پاینده بادا کاسمان</p></div>
<div class="m2"><p>ظل لطفت سایه بان روز محشر ساخته</p></div></div>