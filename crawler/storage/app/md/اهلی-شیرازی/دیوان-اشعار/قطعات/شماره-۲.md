---
title: >-
    شمارهٔ ۲
---
# شمارهٔ ۲

<div class="b" id="bn1"><div class="m1"><p>ای لییمان چه میگویید عیب مزبله</p></div>
<div class="m2"><p>زانکه در اصل است خاک او همان خاک شما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر ملوث گشته، اوث او نه از بطن خودست</p></div>
<div class="m2"><p>ظاهرش آلوده گشت از باطن پاک شما</p></div></div>