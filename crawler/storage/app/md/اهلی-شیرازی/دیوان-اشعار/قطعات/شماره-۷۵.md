---
title: >-
    شمارهٔ ۷۵
---
# شمارهٔ ۷۵

<div class="b" id="bn1"><div class="m1"><p>رسول از ره معجز حکیم جانبخش است</p></div>
<div class="m2"><p>مسیح مرده او از حدیث شکر ریز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مگو رسول حدیثست حکمت آمیزش</p></div>
<div class="m2"><p>بگو مسیح بود حکمتش حدیث آمیز</p></div></div>