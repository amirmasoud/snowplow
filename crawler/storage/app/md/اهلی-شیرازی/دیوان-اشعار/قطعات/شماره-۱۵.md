---
title: >-
    شمارهٔ ۱۵
---
# شمارهٔ ۱۵

<div class="b" id="bn1"><div class="m1"><p>بدو نیک و شاه و گدای و ملک</p></div>
<div class="m2"><p>همه بندگانند بیگانه کیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یکی را جهان میدهی سر بسر</p></div>
<div class="m2"><p>یکی را جوی نیست چندانکه زیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یک قاف تا قاف خوان می کشد</p></div>
<div class="m2"><p>یکی بهر یک لقمه نان خون گریست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو قسمت بوفق عدالت بود</p></div>
<div class="m2"><p>نمیدانم افراط و تفریط چیست</p></div></div>