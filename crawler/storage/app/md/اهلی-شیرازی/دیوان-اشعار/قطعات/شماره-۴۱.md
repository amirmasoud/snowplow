---
title: >-
    شمارهٔ ۴۱
---
# شمارهٔ ۴۱

<div class="b" id="bn1"><div class="m1"><p>مگو که یاد تو اهلی نمیکند نفسی</p></div>
<div class="m2"><p>که ذکر خیر تو تا زنده است میگوید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مرنج اگر نرسد جانب تو نامه او</p></div>
<div class="m2"><p>که می نویسد و سیلاب گریه میشوید</p></div></div>