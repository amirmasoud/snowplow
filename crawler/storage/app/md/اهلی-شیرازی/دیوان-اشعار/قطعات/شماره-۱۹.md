---
title: >-
    شمارهٔ ۱۹
---
# شمارهٔ ۱۹

<div class="b" id="bn1"><div class="m1"><p>کسی نزاری مجنون کجاست در عالم</p></div>
<div class="m2"><p>به زور نیز چو فرهاد مرد کاری نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ببین که هر دو چه دیدند پس مراد ز دوست</p></div>
<div class="m2"><p>بطالع است سعادت به زور و زاری نیست</p></div></div>