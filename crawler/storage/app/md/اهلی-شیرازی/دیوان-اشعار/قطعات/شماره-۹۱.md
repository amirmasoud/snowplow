---
title: >-
    شمارهٔ ۹۱
---
# شمارهٔ ۹۱

<div class="b" id="bn1"><div class="m1"><p>با وجود پادشاهان سخن کز لطف نظم</p></div>
<div class="m2"><p>بر حدیث هر یکی واجب بود صد آفرین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عقل و فهم شاعران در عجز و حیرت آورند</p></div>
<div class="m2"><p>سعدی معجز نما و حافظ سحرآفرین</p></div></div>