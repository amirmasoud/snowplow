---
title: >-
    شمارهٔ ۳۹
---
# شمارهٔ ۳۹

<div class="b" id="bn1"><div class="m1"><p>اگر چه نامه سیاه است در جهان شخصی</p></div>
<div class="m2"><p>که بهر لقمه نانی عذاب خلق بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سیاه نامه تر از وی کسیست روز حساب</p></div>
<div class="m2"><p>که نان خود خورد و در حساب خلق بود</p></div></div>