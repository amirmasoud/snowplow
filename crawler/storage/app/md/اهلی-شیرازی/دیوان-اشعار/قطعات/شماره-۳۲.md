---
title: >-
    شمارهٔ ۳۲
---
# شمارهٔ ۳۲

<div class="b" id="bn1"><div class="m1"><p>ذات محمد و علی آیینه حق اند</p></div>
<div class="m2"><p>آیینه خود ضرورت آن حسن و ناز بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این باعث وجود من و تست ورنه حق</p></div>
<div class="m2"><p>از بودن و نبودن ما بی نیاز بود</p></div></div>