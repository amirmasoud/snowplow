---
title: >-
    شمارهٔ ۱۰۳
---
# شمارهٔ ۱۰۳

<div class="b" id="bn1"><div class="m1"><p>چند مردم بکشی رسم بزرگی آنست</p></div>
<div class="m2"><p>که گنه بینی و از غایت احسان بخشی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سهل باشد چو اجل جان ستدن از هر کس</p></div>
<div class="m2"><p>سعی آن کن که مسیحا شوی و جان بخشی</p></div></div>