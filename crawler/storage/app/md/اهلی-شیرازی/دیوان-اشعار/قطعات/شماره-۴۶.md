---
title: >-
    شمارهٔ ۴۶
---
# شمارهٔ ۴۶

<div class="b" id="bn1"><div class="m1"><p>ذره خاکی که دست قدرت او را برگرفت</p></div>
<div class="m2"><p>آدم از سیر و سلوک و گردش اطوار شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قطره آبی ز دریا برد ابرش در هوا</p></div>
<div class="m2"><p>چو بدریا باز آمد گوهر شهوار شد</p></div></div>