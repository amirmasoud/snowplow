---
title: >-
    شمارهٔ ۱۰۱
---
# شمارهٔ ۱۰۱

<div class="b" id="bn1"><div class="m1"><p>ایدل ز خود بمیر که گردی خلاص از آنک</p></div>
<div class="m2"><p>تا زنده یی مقید ایندام مانده‌ای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو شاهباز قدسی و تن سنگ پای تست</p></div>
<div class="m2"><p>بگسل ازو وگرنه بنا کام مانده‌ای</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دامیست زندگی و بزندان روزگار</p></div>
<div class="m2"><p>تا کی مقید از پی این دام مانده‌ای</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مهمانسراست عالم و مهمان سه روزه است</p></div>
<div class="m2"><p>شرمت نمیشود که بس ایام مانده‌ای</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آخر ز شام تا به کیی منتظر بصبح</p></div>
<div class="m2"><p>در صبح باز منتظر شام مانده‌ای</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سیر از جهان نیی اگرت میل بر بقاست</p></div>
<div class="m2"><p>خامی هنوز و در طمع خام مانده‌ای</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دورست از تو صحبت روحانیان انس</p></div>
<div class="m2"><p>تا وحشیان خاک از آن دام مانده‌ای</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از سر حسن شاهد گل بوی غافلی</p></div>
<div class="m2"><p>مستغرق جمال گل اندام مانده‌ای</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گور زمانه رام تو گر شد ز ره مرو</p></div>
<div class="m2"><p>ناگه اسیر گور چو بهرام مانده‌ای</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گیرم شدی ز بخت سلیمان روزگار</p></div>
<div class="m2"><p>آخر نه در میان دد و دام مانده‌ای</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کامل اگر شوی بهمه علم عاقبت</p></div>
<div class="m2"><p>در نکته یی زبون بسر انجام مانده‌ای</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ور هم نبی شوی و ولی وقت مشکلات</p></div>
<div class="m2"><p>موقوف وحی در ره الهام مانده‌ای</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>باری نظر بعلم حقیقت نمیکنی</p></div>
<div class="m2"><p>با صد هزار علم چنین عام مانده‌ای</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>آخر چو نیک و بد نه بفرمان خود کنی</p></div>
<div class="m2"><p>ببر چه در میانه تو بد نام مانده‌ای</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ای نفس شوخ چشم ز شرب مدام خویش</p></div>
<div class="m2"><p>خونی درون شیشه چو بادام مانده‌ای</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>کی لب بذکر دوست گشایی چنین که تو</p></div>
<div class="m2"><p>مستی مدام و لب بلب جام مانده‌ای</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>گر بت پرست گمشده ره در خطا بود</p></div>
<div class="m2"><p>تو در خطا ببلده اسلام مانده‌ای</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>تا از طمع متابع ارباب صورتی</p></div>
<div class="m2"><p>سر بر زمین بسجده اصنام مانده‌ای</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>آدم نیی وگرنه ز انعام روزگار</p></div>
<div class="m2"><p>تا کی اسیر از پی انعام مانده‌ای</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>اهلی بیا و بر سر جان زن قدم که تو</p></div>
<div class="m2"><p>تا منزل مراد بیک گام مانده‌ای</p></div></div>