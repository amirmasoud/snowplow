---
title: >-
    شمارهٔ ۳۱
---
# شمارهٔ ۳۱

<div class="b" id="bn1"><div class="m1"><p>آنشب که محمد سوی معراج برآمد</p></div>
<div class="m2"><p>بنگر که کمال نظرش تا بچه حد دید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زد عقده هستی گره میم در احمد</p></div>
<div class="m2"><p>شد عقده گشا احمد و فی الحال احد دید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در دیدن این واقعه پرسش مکن از غیر</p></div>
<div class="m2"><p>کآنجا که نظر کرد محمد همه خوردید</p></div></div>