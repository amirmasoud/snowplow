---
title: >-
    شمارهٔ ۷۱
---
# شمارهٔ ۷۱

<div class="b" id="bn1"><div class="m1"><p>گفت کسی خوش بود عمر دو کاندر یکی</p></div>
<div class="m2"><p>تجربه جمع آوری در دگر آری بکار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر بدو صد عمر نوح تجربه حاصل شود</p></div>
<div class="m2"><p>بیشتر از آن بود تجربه روزگار</p></div></div>