---
title: >-
    شمارهٔ ۶
---
# شمارهٔ ۶

<div class="b" id="bn1"><div class="m1"><p>در ره حق چو مکرمت طلبی</p></div>
<div class="m2"><p>معرفت جو که اصل مکرمت است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>معرفت با حق آشنا کندت</p></div>
<div class="m2"><p>آشنایی بقدر معرفت است</p></div></div>