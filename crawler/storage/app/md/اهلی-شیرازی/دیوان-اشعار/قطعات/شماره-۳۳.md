---
title: >-
    شمارهٔ ۳۳
---
# شمارهٔ ۳۳

<div class="b" id="bn1"><div class="m1"><p>کسی که بود سحر با فرشته در محراب</p></div>
<div class="m2"><p>شبش بمیکده دیدم سگش دهان لیسید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ببوی مستی عشق آنکه می خورد هیچ است</p></div>
<div class="m2"><p>بیاد ماست که؟ مهتاب در جهان لیسید</p></div></div>