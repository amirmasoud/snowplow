---
title: >-
    شمارهٔ ۱۱۱
---
# شمارهٔ ۱۱۱

<div class="b" id="bn1"><div class="m1"><p>هرچند کسی علم و هنر دارد و کوشش</p></div>
<div class="m2"><p>باید مدد بخت ز توفیق إلهی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا بخت نباشد نشود کار کسی راست</p></div>
<div class="m2"><p>ور بخت بود راست شود هر چه تو خواهی</p></div></div>