---
title: >-
    شمارهٔ ۷۳
---
# شمارهٔ ۷۳

<div class="b" id="bn1"><div class="m1"><p>دو حرفی بخدمت فرستادمت</p></div>
<div class="m2"><p>سلام و پیامی بصد دردسر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سلام آنکه باشی سلامت مدام</p></div>
<div class="m2"><p>پیام آنکه ما را ز خاطر مبر</p></div></div>