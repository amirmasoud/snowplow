---
title: >-
    شمارهٔ ۵۷
---
# شمارهٔ ۵۷

<div class="b" id="bn1"><div class="m1"><p>مرغان عرش بر سر طوبی نشسته اند</p></div>
<div class="m2"><p>چون مرغ دل اسیر هوی و هوس نی اند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ما همچو مرغ خانه ببند کسان اسیر</p></div>
<div class="m2"><p>آزاده آن کسان که گرفتار کس نی اند</p></div></div>