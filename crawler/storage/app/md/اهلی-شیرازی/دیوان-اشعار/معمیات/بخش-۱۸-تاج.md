---
title: >-
    بخش ۱۸ - تاج
---
# بخش ۱۸ - تاج

<div class="b" id="bn1"><div class="m1"><p>چشمم که بود از اشک در جی از لیالی</p></div>
<div class="m2"><p>تا دیده ام لب او شد درج دیده خالی</p></div></div>