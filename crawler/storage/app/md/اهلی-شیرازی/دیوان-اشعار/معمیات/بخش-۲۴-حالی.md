---
title: >-
    بخش ۲۴ - حالی
---
# بخش ۲۴ - حالی

<div class="b" id="bn1"><div class="m1"><p>نشد از آه و ناله احباب</p></div>
<div class="m2"><p>عالمی بیرخ مهم در خواب</p></div></div>