---
title: >-
    بخش ۶۶ - نوح
---
# بخش ۶۶ - نوح

<div class="b" id="bn1"><div class="m1"><p>نخواهم بنگرد در آینه خویش</p></div>
<div class="m2"><p>که چون خود بین شود گردد جفا کیش</p></div></div>