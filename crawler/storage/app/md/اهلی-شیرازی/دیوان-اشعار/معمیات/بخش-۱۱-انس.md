---
title: >-
    بخش ۱۱ - انس
---
# بخش ۱۱ - انس

<div class="b" id="bn1"><div class="m1"><p>قلب روی اندوده نستانند در بازار حشر</p></div>
<div class="m2"><p>خالصی باید که بیرون آید از آتش سلیم</p></div></div>