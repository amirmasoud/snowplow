---
title: >-
    بخش ۲۰ - پهلوان
---
# بخش ۲۰ - پهلوان

<div class="b" id="bn1"><div class="m1"><p>عاشق که مراد خویش جوید مادام</p></div>
<div class="m2"><p>هرگز نرسد ز وصل دلدار بکام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پروانه که سوخت جان خویش از پی وصل</p></div>
<div class="m2"><p>آنسوخته را شه مه اقبال تمام</p></div></div>