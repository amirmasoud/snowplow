---
title: >-
    بخش ۶۱ - مدامی
---
# بخش ۶۱ - مدامی

<div class="b" id="bn1"><div class="m1"><p>روز دل من بتلخکامی</p></div>
<div class="m2"><p>در جای خطر فتد ز خامی</p></div></div>