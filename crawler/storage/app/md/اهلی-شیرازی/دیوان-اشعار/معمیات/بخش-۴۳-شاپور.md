---
title: >-
    بخش ۴۳ - شاپور
---
# بخش ۴۳ - شاپور

<div class="b" id="bn1"><div class="m1"><p>نمک در آب حیوان زان لبت از خنده اندازد</p></div>
<div class="m2"><p>گر آن شوی برانگیزد که این شوریده نگذارد</p></div></div>