---
title: >-
    بخش ۲۳ - آواره شدن پروانه از بزم شمع
---
# بخش ۲۳ - آواره شدن پروانه از بزم شمع

<div class="b" id="bn1"><div class="m1"><p>چو جمعی را بلایی پیش آید</p></div>
<div class="m2"><p>ملامت بر زبونان بیش آید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر از برق بلا آتش فروزد</p></div>
<div class="m2"><p>بغیر از خرمن عاشق نسوزد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو صیاد از پی نخجیر راند</p></div>
<div class="m2"><p>زبون تر صید مسکین بازماند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر بر رهگذر صد مور آید</p></div>
<div class="m2"><p>بر او کافتاده باشد زور آید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگر باد از نمکزار آورد خاک</p></div>
<div class="m2"><p>نباشد جای او جز سینه چاک</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اگر خود بر نمک هم کف بری پیش</p></div>
<div class="m2"><p>نسوزد غیر انکشتی که شد ریش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به هر جا ز آهن و سنگ آتش افروخت</p></div>
<div class="m2"><p>در آن پر گاله گیرد کاتشش سوخت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو باد از شمع کوته دست گردید</p></div>
<div class="m2"><p>ز کین شمع بر پروانه پیچید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جدا کرد از بر شمعش بزاری</p></div>
<div class="m2"><p>بخاک ره فکند او را بخواری</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بدان جورش که باد آواره میکرد</p></div>
<div class="m2"><p>چو میشد باز پس نظاره میکرد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چو کردی باد باوی تندی آهنگ</p></div>
<div class="m2"><p>همی جستی ازو فرسنگ فرسنگ</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دلش از جور او می خست و می رفت</p></div>
<div class="m2"><p>چراغ از دیده اش می جست و می رفت</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ز جورش هر نفس دیدی بلایی</p></div>
<div class="m2"><p>چو موری در دهان اژدهایی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>پرش نزدیک کز افتان و خیزان</p></div>
<div class="m2"><p>شود چون برگ گل از باد ریزان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>طپیدی همچو مرغ نیم بسمل</p></div>
<div class="m2"><p>میان خاک و خون منزل به منزل</p></div></div>