---
title: >-
    بخش ۳۰ - بیمار شدن شمع و شکایت کردن از فلک
---
# بخش ۳۰ - بیمار شدن شمع و شکایت کردن از فلک

<div class="b" id="bn1"><div class="m1"><p>کسی تا کی ز داغ دل گدازد</p></div>
<div class="m2"><p>دلی تا کی سوز عشق سازد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تنی کز داغ عشق افگار باشد</p></div>
<div class="m2"><p>گر از مردن رهد بیمار باشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شد آخر شمع بیمار از غم دوست</p></div>
<div class="m2"><p>فتادش آتش تب در رگ و پوست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>وجود نازکش چون تب کشیدی</p></div>
<div class="m2"><p>عرق همچون گل از رویش چکیدی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو از جانش زدی آتش زبانه</p></div>
<div class="m2"><p>ز گلنارش فتادی نار دانه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گدازان شد تنش از تابش تب</p></div>
<div class="m2"><p>صدش تبخاله بیرون ریخت از لب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز تب سیم تنش از تاب میشد</p></div>
<div class="m2"><p>چو سیم از آتش دل آب میشد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تن کافوری آنماه گلچهر</p></div>
<div class="m2"><p>گدازان چو برف از تابش مهر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز دل آتش بجای ناله میخاست</p></div>
<div class="m2"><p>چو ماه چارده پیوسته میکاست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چو کلکی شد بدن از ضعف حالش</p></div>
<div class="m2"><p>عیان شد رشته جان همچو نالش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نماند از هستی اش چون لاله آنماه</p></div>
<div class="m2"><p>بجز داغ درون و شعله آه</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چنانش آتش تب رخ برافروخت</p></div>
<div class="m2"><p>که از گرمی جبینش دست میسوخت</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ز تاب تب فرا رفت از سرش دود</p></div>
<div class="m2"><p>کشید از سینه آهی آتش آلود</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>زبان بگشاد با چندین شکایت</p></div>
<div class="m2"><p>همیگفت از سر سوز این حکایت</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>که کینت ایفلک با من چها کرد</p></div>
<div class="m2"><p>ترا از یار و یار از من جدا کرد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>مگر کم سوخت جانم ای ستمگر</p></div>
<div class="m2"><p>که افزودی چنین داغش بر سر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>تنم میسوزی و من میگدازم</p></div>
<div class="m2"><p>که کوته میشود عمر درازم</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چو مه آن کز تو در اوج کمالست</p></div>
<div class="m2"><p>کمال دولتش عین زوالست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چراغ مهر کو را مهربانی</p></div>
<div class="m2"><p>سحر افروزی و شامش نشانی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ز خاکش برکشی تا اوج افلاک</p></div>
<div class="m2"><p>رسانی ذره ذره باز بر خاک</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>مه باریک را چون رشته تن</p></div>
<div class="m2"><p>قوی دل گردد از مهر تو چون من</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چو من آنگه که کار وی شود راست</p></div>
<div class="m2"><p>بداغ نا امیدی بایدش کاست</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>کسی چون باشد از مهر تو شاکر</p></div>
<div class="m2"><p>که خود سازی و هم خود سوزی آخر</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بسوزی هر که را رخ برفروزی</p></div>
<div class="m2"><p>نیفروزی چراغی تا نسوزی</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>شبی با کس نیاری روز کردن</p></div>
<div class="m2"><p>که در روزش گذاری زنده چون من</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ز تو هر کس چراغی برفروزد</p></div>
<div class="m2"><p>چو بیند کوکب بختش بسوزد</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>چو برق آن کز تو در عالم علم گشت</p></div>
<div class="m2"><p>کسی تا چشم برهم زد عدم گشت</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>نیم مومن اگر یکتن ز پستی</p></div>
<div class="m2"><p>بکام دل رسانی تا تو هستی</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>چراغ کس نگردد از تو روشن</p></div>
<div class="m2"><p>که ننشیند بزیر تیغ چون من</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بشهد شکرین پروردنم چیست</p></div>
<div class="m2"><p>بتیغ مرگ ضایع کردنم چیست</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>چو نخل مومم از بهر چه بستی</p></div>
<div class="m2"><p>چه بستی، از چه رو بازم شکستی</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>همه طور تو بی هنجار باشد</p></div>
<div class="m2"><p>همه دور تو بی پرگار باشد</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>نه من چون لاله دارم داغ ازین باغ</p></div>
<div class="m2"><p>که اهل دل همه مردند ازین باغ</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>به آتش سوزم اینم کامرانیست</p></div>
<div class="m2"><p>به حسرت میرم اینم زندگانیست</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>ز ضعفم بسکه یارای نفس نیست</p></div>
<div class="m2"><p>اگر میرم کسی فریاد رس نیست</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>چنان در خود فرو رفتم که دیگر</p></div>
<div class="m2"><p>مگر بردارم از جیب عدم سر</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>هوا داری که دل کردم بدو گرم</p></div>
<div class="m2"><p>ببادش داده یی بادت ز من شرم</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>روا داری که از جور رقیبی</p></div>
<div class="m2"><p>به تنهایی بمیرم چون غریبی</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>گر این جور و جفا بر من گزینی</p></div>
<div class="m2"><p>الهی هم به روز من نشینی</p></div></div>