---
title: >-
    بخش ۱۴ - صفت دو خادم شمع و پیام شمع بسوی پروانه
---
# بخش ۱۴ - صفت دو خادم شمع و پیام شمع بسوی پروانه

<div class="b" id="bn1"><div class="m1"><p>دو خادم داشت آن سرو گل اندام</p></div>
<div class="m2"><p>یکی کافور و دیگر عنبرش نام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دو خادم همدم و همراز و همسال</p></div>
<div class="m2"><p>بجانسوزی موافق در همه حال</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مخمر مهرشان با طینت شمع</p></div>
<div class="m2"><p>و زیشان بود زیب و زینت شمع</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تهی ز ایشان نبد اندیشه او</p></div>
<div class="m2"><p>پر از ایشان ز رگ تا ریشه او</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز عزت هر دو بود از نیکخواهی</p></div>
<div class="m2"><p>به چشمش چون سپیدی و سیاهی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ازین بوی وفا داری شنیده</p></div>
<div class="m2"><p>وز آن صد رو سفیدی نیز دیده</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>غم پروانه چون از حد بدر شد</p></div>
<div class="m2"><p>ز سوزش شمع را آخر خبر شد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دوان با خادمان خویشتن گفت</p></div>
<div class="m2"><p>که با پروانه باید این سخن گفت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>که ای بیهوده گرد باد پیما</p></div>
<div class="m2"><p>چه میگردی به گرد مجلس ما</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بگرد بزم ما تا چند پویی</p></div>
<div class="m2"><p>چه افتادت چه گم کردی چه جویی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مرا نادیده ای فرسوده تن گیر</p></div>
<div class="m2"><p>برو دنبال کار خویشتن گیر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نظر بازی ترا با من نسازد</p></div>
<div class="m2"><p>که جز دیوانه با آتش نبازد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چو اینها خادمان از وی شنفتد</p></div>
<div class="m2"><p>یکایک باز با پروانه گفتند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>یکی قهرش همی کردی بخواری</p></div>
<div class="m2"><p>یکی پندش همی دادی بیاری</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بر او کافور بس دلسرد بودی</p></div>
<div class="m2"><p>ولی عنبر نیاز آورد بودی</p></div></div>