---
title: >-
    بخش ۴۲ - بهم رسیدن پروانه و شمع و فدا کردن پروانه خود را
---
# بخش ۴۲ - بهم رسیدن پروانه و شمع و فدا کردن پروانه خود را

<div class="b" id="bn1"><div class="m1"><p>عجب وقتی جگر سوزست آندم</p></div>
<div class="m2"><p>که افتد عاشقان را دیده برهم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دو عاشق را نظر چون برهم افتد</p></div>
<div class="m2"><p>تو گویی آتشی در عالم افتد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو کردند آن دو تن درهم نگاهی</p></div>
<div class="m2"><p>برآمد از درون هر دو آهی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز چشم هر دو از غم زار زاری</p></div>
<div class="m2"><p>برآمد گریه بی اختیاری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نظر چون شمع را بر وی فتادی</p></div>
<div class="m2"><p>ز چشمش گریه شادی گشادی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بپروانه نظر چون باز میکرد</p></div>
<div class="m2"><p>ز شوقش مرغ جان پرواز میکرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز سوز سینه با هم راز گفتند</p></div>
<div class="m2"><p>حکایتهای دوری باز گفتند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ولیکن شمع بس حالی عجب داشت</p></div>
<div class="m2"><p>ز دست هجران جان بلب داشت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نه چندان زهر هجرش کارگر بود</p></div>
<div class="m2"><p>که تریاک وصالش داشتی سود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چو زهری کارگر گردید در دل</p></div>
<div class="m2"><p>شود تریاک هم زهر هلاهل</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مگو در اصل از هجران چه باک است</p></div>
<div class="m2"><p>که گه در وصل هم بیم هلاک است</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چراغ از نور خود گردیده افروخت</p></div>
<div class="m2"><p>بسی دیدیم کز وی خانه هم سوخت</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ز وصلش شمع سوز دل بتر گشت</p></div>
<div class="m2"><p>تو گفتی درد و داغش بیشتر گشت</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>فرو رفتی بداغ دردمندی</p></div>
<div class="m2"><p>به پستی رو نهادی از بلندی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>تنش گاهی ز مشتاقی نمودی</p></div>
<div class="m2"><p>بجز آهی ازو باقی نبودی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چو حال شمع را پروانه بد دید</p></div>
<div class="m2"><p>فدایی وار گرد شمع گردید</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بدو گفت ای سر من خاک پایت</p></div>
<div class="m2"><p>هزاران همچو من بادا فدایت</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چنین حالی که در عالم پذیرد؟</p></div>
<div class="m2"><p>که عاشق زنده و معشوق میرد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>پس از مرگ تو در روی که بینم؟</p></div>
<div class="m2"><p>چو بگشایم نظر سوی که بینم؟</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بگفت این و گذشت از کوی هستی</p></div>
<div class="m2"><p>در آتش شد درون از عین مستی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چنانش سوز عشق از دل علم زد</p></div>
<div class="m2"><p>که در آتش به شوق دل قدم زد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چنان سوز دلش آتش برافروخت</p></div>
<div class="m2"><p>که رخت هستیش سر تا قدم سوخت</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>در آتش سوخت جان خود بصد ذوق</p></div>
<div class="m2"><p>زهی عشق و زهی عاشق زهی شوق</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>زهی پروانه و جانسوزی خویش</p></div>
<div class="m2"><p>چراغش روشن از فیروزی خویش</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>زهی سرخیل و شاه عشقبازان</p></div>
<div class="m2"><p>زهی چشم و چراغ جانگدازان</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>زهی مجنون دل آزرده عشق</p></div>
<div class="m2"><p>زهی آهوی پیکان خورده عشق</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>فروغ سینه ارباب محنت</p></div>
<div class="m2"><p>چراغ دیده اهل محبت</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>در آتش عاقبت آن جور کش رفت</p></div>
<div class="m2"><p>به جانان داد وه وه چه خوش رفت</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>چنین باشد طریق جان سپردن</p></div>
<div class="m2"><p>براه دوست دادن جان و مردن</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>دلا، پروانه وش در عشق جان ده</p></div>
<div class="m2"><p>که این مرگ از حیات جاودان به</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>کسی در عاشقی فیروز باشد</p></div>
<div class="m2"><p>که چون پروانه خرمن سوز باشد</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>برای دوست بازد جان خود را</p></div>
<div class="m2"><p>بپایان آورد پیمان خود را</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>نه ز انسان عاشقان سست پیمان</p></div>
<div class="m2"><p>که سازد شهره خود در عشق جانان</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>نباشد عشق او جز کید و تزویر</p></div>
<div class="m2"><p>که گردد حیله ور چون روبه پیر</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>بحیله دلبران را رام سازد</p></div>
<div class="m2"><p>خود و معشوق را بد نام سازد</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>ازین جا باز گردم سوی مقصود</p></div>
<div class="m2"><p>بگویم حال شمع محنت آلود</p></div></div>