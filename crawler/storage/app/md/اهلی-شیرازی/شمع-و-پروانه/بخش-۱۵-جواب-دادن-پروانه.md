---
title: >-
    بخش ۱۵ - جواب دادن پروانه
---
# بخش ۱۵ - جواب دادن پروانه

<div class="b" id="bn1"><div class="m1"><p>چو گفتند این حکایت خادمانش</p></div>
<div class="m2"><p>فتاد از داغ دل آتش بجانش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زبان بگشاد و گفتا خادمان را</p></div>
<div class="m2"><p>که گویید از من آن سرو روان را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>که ای شمشاد قد لاله رخسار</p></div>
<div class="m2"><p>ز شوخی آتش محضی پریوار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>قدت سرو و ز سروت گل دمیده</p></div>
<div class="m2"><p>رخت گل و ز گلت سنبل دمیده</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چه شیرینی! مگر چون مادرت زاد</p></div>
<div class="m2"><p>بجای شیر نابت انگبین داد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز مادر کس بدین لطف و طراوت</p></div>
<div class="m2"><p>نزاید چون تو با چندین حلاوت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خوی افشان آتشین رویت شد از تاب</p></div>
<div class="m2"><p>زهی آتش که از وی میچکد آب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>قدت شاخ گل سبزست بی خار</p></div>
<div class="m2"><p>که از سر تا قدم گل آورد بار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گل رویت که در گلزار نبود</p></div>
<div class="m2"><p>چو او هرگز گلی بی خار نبود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تویی در جنت نیکویی آن حور</p></div>
<div class="m2"><p>که در شأن تو آمد آیت نور</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بنازم سرو قد شوخ شنگت</p></div>
<div class="m2"><p>بمیرم پیش روی لاله رنگت</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دلم خواهد که دفع هر گزندی</p></div>
<div class="m2"><p>بسوزد بر سرت همچون سپندی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به جسم و جان بخوبان زان نمانی</p></div>
<div class="m2"><p>که چیزی ماورای جسم و جانی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>به جان آتش زنم رنگ تو گیرم</p></div>
<div class="m2"><p>ببوسم پای تو آنگه بمیرم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ز رویت چشم من گر دور گردد</p></div>
<div class="m2"><p>چراغ دیده ام بی نور گردد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>من آن مستم که گر سازی کبابم</p></div>
<div class="m2"><p>بسوزم جان وز آتش رو نتابم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>همی گفت از سر سوزش ثنایی</p></div>
<div class="m2"><p>همی خواندش بصد زاری دعایی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>که چون شمع فلک تابنده باشی</p></div>
<div class="m2"><p>نمیری تا قیامت زنده باشی</p></div></div>