---
title: >-
    بخش ۲۸ - مژده دادن هاتف غیبی پروانه را
---
# بخش ۲۸ - مژده دادن هاتف غیبی پروانه را

<div class="b" id="bn1"><div class="m1"><p>زهی دانای مقصود نهانی</p></div>
<div class="m2"><p>زبان دان زبان بی زبانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زهی آگه ز درد دردمندان</p></div>
<div class="m2"><p>شفا بخش جراحتهای پنهان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عطای او که برق ناگهان است</p></div>
<div class="m2"><p>چراغ افروز راه گمراهان است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>محیط لطفش ارجنبد یکی دم</p></div>
<div class="m2"><p>بشوید از گنه دامان عالم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو خورشید کرم در بخشش آویخت</p></div>
<div class="m2"><p>ز بحر لطف ابر رحمت انگیخت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر آن لب تشنه رحمت کرد باران</p></div>
<div class="m2"><p>در آن ظلمت چشاندش آب حیوان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو از زاری دل پروانه میسوخت</p></div>
<div class="m2"><p>چراغ حاجتش ایزد برافروخت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بگوش هوشش از غیب آمد آواز</p></div>
<div class="m2"><p>که روشن شد چراغ دیده ات باز</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو برق لطف یزدانی درخشید</p></div>
<div class="m2"><p>چراغی خواهدت از غیب، بخشید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ازین شب خواهدت صبحی دمیدن</p></div>
<div class="m2"><p>بدان خورشید وش خواهی رسیدن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ترا زان نور چشم ای پاکدامن</p></div>
<div class="m2"><p>رسولی میرسد چشم تو روشن</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مخور غم زانکه ما از زاری تو</p></div>
<div class="m2"><p>نظر داریم با بیداری تو</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ز بیداری ترا کی خوار داریم</p></div>
<div class="m2"><p>که شرم از دیده بیدار داریم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>دلا گاهی درخشد کوکب تو</p></div>
<div class="m2"><p>که بیداری بروز آرد شب تو</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>سر بیدار صاحب تاج معنی است</p></div>
<div class="m2"><p>که بیداری شب معراج معنی است</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>تجلی را بشب کشف حجابست</p></div>
<div class="m2"><p>چه بیند دیده یی کو مست خوابست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چو دولت چشم بختش برگمارد</p></div>
<div class="m2"><p>نظر با دیده بیدار دارد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>سعادت را بود فرخنده کوکب</p></div>
<div class="m2"><p>طلوعی یکنفس آن هم دل شب</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بشب عمری دگر بی اشتباه است</p></div>
<div class="m2"><p>بخواب ار بگذرد عمری تباه است</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گر افروزی چراغ شب نشینی</p></div>
<div class="m2"><p>شب خود را بمعنی روز بینی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>درین فانوس بی دود چراغی</p></div>
<div class="m2"><p>نگردد حاصلت نقش فراغی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>مرا پیر طریقت نکته یی گفت</p></div>
<div class="m2"><p>به الماس حقیقت گوهری سفت</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>که گبرانی که آتشخانه تابند</p></div>
<div class="m2"><p>به بیداری به از مستان خوابند</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>خداوندا، به بیداران فردت</p></div>
<div class="m2"><p>به بیخوابی بیماران دردت</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>که از درد خودم بیماریی ده</p></div>
<div class="m2"><p>وز آن بیماری ام بیداریی ده</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بیا اهلی که وقت چاره سازیست</p></div>
<div class="m2"><p>دل پروانه بس در جانگدازیست</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بدلبر قاصد رازش رسانم</p></div>
<div class="m2"><p>بیار خویشتن بازش رسانم</p></div></div>