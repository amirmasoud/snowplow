---
title: >-
    بخش ۵ - نعت امیرالمؤمنین
---
# بخش ۵ - نعت امیرالمؤمنین

<div class="b" id="bn1"><div class="m1"><p>امیر المؤمنین شمع هدایت</p></div>
<div class="m2"><p>چراغ دیده ها شاه ولایت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فلک یک خادم شب زنده دارش</p></div>
<div class="m2"><p>چراغ افروز قندیل مزارش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دو شمع افروزد از مهر و مه بدر</p></div>
<div class="m2"><p>به بالین و به پایینش شب قدر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو گشتی ذوالفقارش گرم و خونریز</p></div>
<div class="m2"><p>کشیدی خود زبان چون آتش تیز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دو سر زان تیغش ایزد آفریده</p></div>
<div class="m2"><p>که خصمش کور گردد هر دو دیده</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به تیزی چون زبان مار بودی</p></div>
<div class="m2"><p>دو سر زان تیغ تیزش می نمودی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کس این پروانه چون بخشد به دشمن</p></div>
<div class="m2"><p>که برگیرد سرش چون شمع از تن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو شمع تیغ قهرش می برافروخت</p></div>
<div class="m2"><p>به گردش هر که می گردید می سوخت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نبی از جمله شمع جمع بوده است</p></div>
<div class="m2"><p>علی پروانه آن شمع بوده است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نبی جا بر کتف کردی ولی را</p></div>
<div class="m2"><p>نگه کن پایه قدر علی را</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>علی با نور احمد بود الحق</p></div>
<div class="m2"><p>دو شمع روشن از یک نور مشتق</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>الا ای پرتو انوار یزدان</p></div>
<div class="m2"><p>چو برقی گه درخشان گاه پنهان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>خوش ان کز شمع وصلت همچو خورشید</p></div>
<div class="m2"><p>رسد پروانه دیدار جاوید</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>مرا داغ غلامی بر جبین است</p></div>
<div class="m2"><p>نشان بخت سبز من همین است</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بود کز شمع دیوانخانه عفو</p></div>
<div class="m2"><p>دهد لطف توام پروانه عفو</p></div></div>