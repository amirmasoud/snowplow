---
title: >-
    بخش ۱۹ - یاری کردن شمع پروانه را در عشق
---
# بخش ۱۹ - یاری کردن شمع پروانه را در عشق

<div class="b" id="bn1"><div class="m1"><p>نهنگ شوق چون طوفان برآورد</p></div>
<div class="m2"><p>خرد را کشتی طاقت فرو برد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چنان بحر محبت گشت حوشان</p></div>
<div class="m2"><p>که شمعش ز آتش دل خاست طوفان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو شد تیزاب تیغ عشق حاصل</p></div>
<div class="m2"><p>نمودش جوهر آیینه دل</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو موم از آتش دل نرم گردد</p></div>
<div class="m2"><p>بجان پروانه را سرگرم گردد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چنانش سوز او در دل اثر کرد</p></div>
<div class="m2"><p>که از سرگرمی و تندی بدر کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو دید او را به مهر خویش صادق</p></div>
<div class="m2"><p>بر او مجنون چو لیلی گشت عاشق</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گذشت از ناز و او را یار خود کرد</p></div>
<div class="m2"><p>نیاز عاشق آخر کار خود کرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کسی داند که دارد سوز داغی</p></div>
<div class="m2"><p>که دل بر دل همیدارد چراغی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ولی پاکان چو شمع دلفروزند</p></div>
<div class="m2"><p>که نور از هم فرا گیرند و سوزند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دلا، گاهی وصال شمع یابی</p></div>
<div class="m2"><p>که چون پروانه رخ ز آتش نتابی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چونگریزی به جور از آشنایی</p></div>
<div class="m2"><p>بیابی ز آشنایی روشنایی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چو موسی زاتش فرعون نگریخت</p></div>
<div class="m2"><p>هم از آتش چراغ دل برانگیخت</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ندیدم عاشقی در عشق صادق</p></div>
<div class="m2"><p>که در عشقش نشد معشوق عاشق</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>اگر معشوق جوری می نماید</p></div>
<div class="m2"><p>ترا در عشق خود می آزماید</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>دل پروانه گر از داغ ریشست</p></div>
<div class="m2"><p>جگر سوزی و داغ شمع بیش است</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گرش داغی بود بر سینه بلبل</p></div>
<div class="m2"><p>بود صد داغ هم بر سینه گل</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>میان عاشق و معشوق رازی است</p></div>
<div class="m2"><p>که گر این سوزد او را هم گدازیست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بپای یار اگر خاری درآید</p></div>
<div class="m2"><p>ز عاشق ناله زاری برآید</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>وگر عاشق خورد تیری دل دوست</p></div>
<div class="m2"><p>بدرد آید چو خود در سینه اوست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>دل پروانه چون میسوخت از درد</p></div>
<div class="m2"><p>بجان شمع سوز او اثر کرد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چنان شمعش بدل آتش برافروخت</p></div>
<div class="m2"><p>که در پروانه چون میدید میسوخت</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بدو گفتا که ای پروانه مست</p></div>
<div class="m2"><p>بسوز و زاریم بردی دل از دست</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ترا هر چند در جور آزمودم</p></div>
<div class="m2"><p>فزون شد اعتقاد از آنچه بودم</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>مرا مهر تو در دل جا گرفته</p></div>
<div class="m2"><p>ز جانم آتشی بالا گرفته</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>من اینسان گرم مهری کز تو بینم</p></div>
<div class="m2"><p>بمیرم بیتو گر روزی نشینم</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>مبادم زندگی آنروز روزی</p></div>
<div class="m2"><p>که بی رویت کنم مجلس فروزی</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>مرا هرجا نشانی تا بمیرم</p></div>
<div class="m2"><p>از آنجا پای رفتن برنگیرم</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>چو شد سوز غمت راز نهانم</p></div>
<div class="m2"><p>زبان برم گر آید بر زبانم</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>نیاید بر زبان از عشق رازم</p></div>
<div class="m2"><p>گر از تن پاره برداری به گازم</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>به تیغم گر جدا گردد سر از تن</p></div>
<div class="m2"><p>به مهرت تیز تر گردد دل من</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>قسم خوردم که هر جا روی آری</p></div>
<div class="m2"><p>مرا هم باشد آنجا روی یاری</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>همه جان و دل و تن از تو باشم</p></div>
<div class="m2"><p>تو از من باشی و من از تو باشم</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>دل مومی چو شمعش نرم گردید</p></div>
<div class="m2"><p>میان هر دو صحبت گرم گردید</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>در آن صحبت ز گرمی بی شکفتی</p></div>
<div class="m2"><p>چراغ از گرمی صحبت گرفتی</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>ز وصل هم عجب دلشاد بودند</p></div>
<div class="m2"><p>ولی فارغ ز قصد باد بودند</p></div></div>