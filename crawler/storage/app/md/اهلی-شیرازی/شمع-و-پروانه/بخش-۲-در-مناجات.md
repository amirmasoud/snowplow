---
title: >-
    بخش ۲ - در مناجات
---
# بخش ۲ - در مناجات

<div class="b" id="bn1"><div class="m1"><p>بود یارب که از پروانه جود</p></div>
<div class="m2"><p>برافروزی دلم را شمع مقصود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز توفیقم نمایی راه تحقیق</p></div>
<div class="m2"><p>چراغی بخشیم از نور توفیق</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دلم پروانه جانسوز سازی</p></div>
<div class="m2"><p>به شمع دل شب من روز سازی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چراغ دل که مرد از ظلمت تن</p></div>
<div class="m2"><p>ز برق عشق بازش ساز روشن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو شمعم گرمیی از سوختن ده</p></div>
<div class="m2"><p>مرا از سوختن افروختن ده</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دل پرسوز من از سوز داغی</p></div>
<div class="m2"><p>برافروزان چو فانوس چراغی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>رهان زین ظلمتم از برق آهی</p></div>
<div class="m2"><p>ببخش از بخت سبزم خضر راهی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دل من مخزن اسرار خود کن</p></div>
<div class="m2"><p>چو شمعش روشن از انوار خود کن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>رهی بنمایم از شمع معانی</p></div>
<div class="m2"><p>مرا روشن کن اسرار نهانی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به چشم من نما از سرمایه غیب</p></div>
<div class="m2"><p>پری رویان معنیهای بی عیب</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>حدیث روشنم عقد گهر کن</p></div>
<div class="m2"><p>چراغ مجلس اهل نظر کن</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دلم انور کن از نور نظامی</p></div>
<div class="m2"><p>کمال سعدی ام ده در تمامی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چو حافظ شاهی الهامیم بخش</p></div>
<div class="m2"><p>نوای خسروی چون جامیم بخش</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نی کلک مرا گردان شکرریز</p></div>
<div class="m2"><p>چو شمعش ده زبان آتش انگیز</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>که چون از شمع و از پروانه گوید</p></div>
<div class="m2"><p>رخ مردم چو شمع از گریه شوید</p></div></div>