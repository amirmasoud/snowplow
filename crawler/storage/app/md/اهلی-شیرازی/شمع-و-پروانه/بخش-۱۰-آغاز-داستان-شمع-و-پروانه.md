---
title: >-
    بخش ۱۰ - آغاز داستان شمع و پروانه
---
# بخش ۱۰ - آغاز داستان شمع و پروانه

<div class="b" id="bn1"><div class="m1"><p>حدیثی دارم از روشندلی یاد</p></div>
<div class="m2"><p>بسی شیرین تر از شیرین و فرهاد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بدل افروزی و جانسوزی افزون</p></div>
<div class="m2"><p>ز حسن لیلی و از عشق مجنون</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عجب حسنی و عشقی کز حقیقت</p></div>
<div class="m2"><p>بود شمع ره اهل طریقت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نه تنها عاشقش در سوز و ساز است</p></div>
<div class="m2"><p>که دلبر همچو عاشق در گداز است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دلا، رازی که بازت می نمایم</p></div>
<div class="m2"><p>حقیقت در مجازت می نمایم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بیا بشنو حدیث عشق جانسوز</p></div>
<div class="m2"><p>چراغ دل ازین آتش برافروز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کهن پیر خرد کافسانه گوید</p></div>
<div class="m2"><p>چنین از شمع و از پروانه گوید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>که روزی خسروی بهر فراغی</p></div>
<div class="m2"><p>چو گل زد خیمه عشرت بباغی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نه باغی جنت روی زمین بود</p></div>
<div class="m2"><p>نه محفل مجلس نقاش چین بود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به گرد شمع روی آتشین گل</p></div>
<div class="m2"><p>همی گردید چون پروانه بلبل</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به شاخ ارغوان آتش فروزان</p></div>
<div class="m2"><p>چو صد پروانه گرد شمع سوزان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به دلسوزی داغ لاله در باغ</p></div>
<div class="m2"><p>سمن در کف گرفته پنبه داغ</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>قلندروار نرگس باده خورده</p></div>
<div class="m2"><p>قدح از نیمه نارنج کرده</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چه شد روشن چراغ عارض گل</p></div>
<div class="m2"><p>چو شمعش رشته جان سوخت بلبل</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چراغ لاله شمع گل برافروخت</p></div>
<div class="m2"><p>بنفشه از غمش پروانه وش سوخت</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>میان باغ چندان لاله شد جمع</p></div>
<div class="m2"><p>که شد صحن چمن طشتی پر از شمع</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>به بستانی چنان مستان به عشرت</p></div>
<div class="m2"><p>به چرخ آورده چون فانوس صحبت</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>نشسته سرو قدان جای بر جای</p></div>
<div class="m2"><p>چو شمع استاده ساقی بر سر پای</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>صراحی هر نفس آواز میکرد</p></div>
<div class="m2"><p>بآن آواز مطرب ساز میکرد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>حدیث جمله از عیش و طرب رفت</p></div>
<div class="m2"><p>بصد عیش و طرب روزی بشب رفت</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>پریشان کرد شب گیسوی مشکین</p></div>
<div class="m2"><p>نهان شد در بنفشه برگ نسرین</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>تو گفتی آهوی شب نافه بگشاد</p></div>
<div class="m2"><p>همی شد گرد مشک سوده بر باد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>چو شب شد طرح عیش از نو نهادند</p></div>
<div class="m2"><p>به مجلس شمع را پروانه دادند</p></div></div>