---
title: >-
    بخش ۳۴ - خطاب شمع با کافور و استعانت از او
---
# بخش ۳۴ - خطاب شمع با کافور و استعانت از او

<div class="b" id="bn1"><div class="m1"><p>خوش آنکس را که چون شمع دلاویز</p></div>
<div class="m2"><p>زبانی در دهانست آتش انگیز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نه چون باد خنک هرگه نفس راند</p></div>
<div class="m2"><p>چراغ شوق صاحب درد بنشاند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نصیحتگو چو خود بی درد باشد</p></div>
<div class="m2"><p>هر آن پندی که گوید سرد باشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو شمعش پند او بر دل سبک بود</p></div>
<div class="m2"><p>دم او بر دل گرمش خنک بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز پند سرد او بودی مشوش</p></div>
<div class="m2"><p>تو گفتی میزدی آبی بر آتش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بدو گفتا که نبود هیچ دردت</p></div>
<div class="m2"><p>از آن یخ بارد از گفتار سردت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تو ای فرسوده، سوز دل چه دانی</p></div>
<div class="m2"><p>که دایم همنفس با مردگانی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>فسون بر من مدم چندین فسردن</p></div>
<div class="m2"><p>که خواهم از دم سرد تو مردن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اگر بودی خنک باد دل آزار</p></div>
<div class="m2"><p>خنک تر بوده یی از وی تو بسیار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ترا بس از سفیدی این اشارت</p></div>
<div class="m2"><p>که کم باشد سفیدان را حرارت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بآزادی مده از عشق پندم</p></div>
<div class="m2"><p>که من در آتش دل پای بندم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ز داغ دل کی آسایش پذیرم</p></div>
<div class="m2"><p>مگر آسایدم دل چون بمیرم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>کسی کش همچو من آتش بجان است</p></div>
<div class="m2"><p>بدو مردن حیات جاودان است</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ازین آتش که عشقم در دل افروخت</p></div>
<div class="m2"><p>به هر حالی که باشد بایدم سوخت</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چو نتوانی رهاند از آتشم تن</p></div>
<div class="m2"><p>مرا در آتش دیگر میفکن</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گرفتم کز وفایی پند گویم</p></div>
<div class="m2"><p>خنک گردی نگویی چند گویم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چنین کاین آتشم در دل بلندست</p></div>
<div class="m2"><p>بر او آب سخن کی سودمند است</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>به هر جاییکه آتش کارگر گشت</p></div>
<div class="m2"><p>کسی آبش چو زد خود تیز تر گشت</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چو میسوزد ز سر تا پا وجودم</p></div>
<div class="m2"><p>ندارد مرهم کافور سودم</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>تو خود آن نیستی کز غمگساری</p></div>
<div class="m2"><p>نمایی گرمیی با من بیاری</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>مگر کاین کارم از عنبر گشاید</p></div>
<div class="m2"><p>که این بوی وفا از عنبر آید</p></div></div>