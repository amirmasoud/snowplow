---
title: >-
    بخش ۱۱ - آمدن شمع بمجلس
---
# بخش ۱۱ - آمدن شمع بمجلس

<div class="b" id="bn1"><div class="m1"><p>خوش آن دل کش بود محفل فروزی</p></div>
<div class="m2"><p>خوش آن محفل که دارد دلفروزی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خوش آن روزی که با یاری سرآید</p></div>
<div class="m2"><p>خوش آن شب کز درت شمعی در آید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>درآمد شمع با روی درخشان</p></div>
<div class="m2"><p>تو گویی تیغ زد خورشید رخشان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نهاده همچو شاهان تاج بر سر</p></div>
<div class="m2"><p>روان شد تا فراز کرسی زر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو کرسی خادمش بر فرش بنهاد</p></div>
<div class="m2"><p>به کرسی پا چو ساق عرش بنهاد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به صورت گر چه شمعش نام بودی</p></div>
<div class="m2"><p>بمعنی شاه ملک شام بودی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چه شمعی چشم بد از روی او دور</p></div>
<div class="m2"><p>بسان حوریان سر تا قدم نور</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نگویم شمع، سروی نو رسیده</p></div>
<div class="m2"><p>قبای شمع سان در بر کشیده</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ازین گلنار روی نازنینی</p></div>
<div class="m2"><p>لبی با او چو لعل آتشینی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به قد نخلی روان سر تا قدم خوش</p></div>
<div class="m2"><p>چو نخل موم بس موزون و دلکش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از آتش بر سرش تاجی زر اندود</p></div>
<div class="m2"><p>زده مشکین اتاقه بر سر از دود</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>قدی چون سرو نازی برکشیده</p></div>
<div class="m2"><p>عجب سروی که گل از وی دمیده</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>رخش گلگون ز تاب نار خوردن</p></div>
<div class="m2"><p>عرق رفته ز رویش تا به دامن</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نمودی زیر ده پیراهن او</p></div>
<div class="m2"><p>رگ جان از لطافت در تن او</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>قدش چون نیشکر شیرین و دلجو</p></div>
<div class="m2"><p>عجب شیرینی دلسوز با او</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>باین خوش منظری سرو روانی</p></div>
<div class="m2"><p>بدو نظارگی چشم جهانی</p></div></div>