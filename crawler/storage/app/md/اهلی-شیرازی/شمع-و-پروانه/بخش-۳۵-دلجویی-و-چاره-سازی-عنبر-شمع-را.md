---
title: >-
    بخش ۳۵ - دلجویی و چاره سازی عنبر شمع را
---
# بخش ۳۵ - دلجویی و چاره سازی عنبر شمع را

<div class="b" id="bn1"><div class="m1"><p>خوشا یاری که یار مبتلاییست</p></div>
<div class="m2"><p>ز خون گرمی در او بوی وفاییست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چراغ افروز شام درد مندیست</p></div>
<div class="m2"><p>نه بزم آرای روز سربلندیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو شمع از حد گذشتش سوز و زاری</p></div>
<div class="m2"><p>جگر میسوختش عنبر بیاری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو زلف دلبران عنبر برآشفت</p></div>
<div class="m2"><p>بدو از طیب انفاس این سخن گفت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>که ای کمتر غلام هندویت من</p></div>
<div class="m2"><p>سواد چشم از روی تو روشن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دل و جانم نه تنها بسته تست</p></div>
<div class="m2"><p>که هست و نیستم وابسته تست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اگر نورت دلیل من نگشتی</p></div>
<div class="m2"><p>چراغ کار من روشن نگشتی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز خون گرمی که بینم هر دم از تو</p></div>
<div class="m2"><p>سیه رو باشم ار برگردم از تو</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اگر در آتشم سوزی دل و تن</p></div>
<div class="m2"><p>همه بوی محبت خیزد از من</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ترا از داغ دل گر رخ برافروخت</p></div>
<div class="m2"><p>مرا از آتش داغت جگر سوخت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>اگر داری تو آتش در رگ و پوست</p></div>
<div class="m2"><p>مرا هم رگ بجان میگیری ایدوست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>منت یک هندوی خدمتگذارم</p></div>
<div class="m2"><p>که جان در آتش از بهر تو دارم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>منت پروردم ای حور بهشتی</p></div>
<div class="m2"><p>که داری طینت عنبر سرشتی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>مسوز از غم که گر من بایدم سوخت</p></div>
<div class="m2"><p>چو گل خواهم رخت از شادی افروخت</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گرم باید بر آتش شد بیادت</p></div>
<div class="m2"><p>بسوزم تا بدست آرم مرادت</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ز من بهتر نداری چاره جویی</p></div>
<div class="m2"><p>که دارم از فسون و سحر بویی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بود سحر و فسون پیوسته کارم</p></div>
<div class="m2"><p>ز گاو سامری میراث دارم</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چنان پی برده ام افسونگری را</p></div>
<div class="m2"><p>که آدم در خط فرمان پری را</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چو در تعزید پیچم همچو سالک</p></div>
<div class="m2"><p>شود بر وفق مقصودم ملایک</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بر آتش گر نهم بویی پی راز</p></div>
<div class="m2"><p>ز سر غیب گویم قصه ها باز</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ببوی من پری سوی من آید</p></div>
<div class="m2"><p>یکایک قصه غیبم نماید</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>کنون آن غایبت کز دیده دورست</p></div>
<div class="m2"><p>دلت از غیبت او بی حضورست</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بنام او نهم بویی بر آتش</p></div>
<div class="m2"><p>به بینم چیست حال آن بلاکش</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>اگر چه مه بود بر اوج گردون</p></div>
<div class="m2"><p>وگر زیر زمین چون گنج قارون</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>وگر چون زر بود در سنگ خارا</p></div>
<div class="m2"><p>کنم بهر تواش از سنگ پیدا</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>به بینم حالت صبر و سکونش</p></div>
<div class="m2"><p>بگویم یک بیک حال درویش</p></div></div>