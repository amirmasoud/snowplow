---
title: >-
    بخش ۲۶ - صفت شب گذراندن پروانه
---
# بخش ۲۶ - صفت شب گذراندن پروانه

<div class="b" id="bn1"><div class="m1"><p>شب هجران که بی روی چو ماه است</p></div>
<div class="m2"><p>بچشم عاشقان عالم سیاه است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شب هجران که بنماید ستاره</p></div>
<div class="m2"><p>مگو شب، هست دودی بی شراره</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همه دم عاشق دلریش سوزد</p></div>
<div class="m2"><p>ولی چون شب درآید بیش سوزد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کسی کش دردی از داغ درونست</p></div>
<div class="m2"><p>چو شب شد درد و داغ او فزونست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کسی داند چه در دست این که یاری</p></div>
<div class="m2"><p>شبی روز آورد در انتظاری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شب دوری دراز و جانگدازست</p></div>
<div class="m2"><p>حکایتهای او گفتن درازست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شب غم گر سیه باشد بر جمع</p></div>
<div class="m2"><p>سیه تر باشدش پروانه بی شمع</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز داغ سینه سوزان شب غم</p></div>
<div class="m2"><p>دل پروانه بودی در تب غم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو شمع از غم دل جمعش برآشفت</p></div>
<div class="m2"><p>در آن تاریک شب روشن همیگفت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>که ایشب گر سواد دیده هایی</p></div>
<div class="m2"><p>چه حاصل گر نداری روشنایی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چه شام است این، مگر باد جهانگرد</p></div>
<div class="m2"><p>گذر بر توده خاک سیه کرد؟</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نگویم عالم از شب ظلمت اندوخت</p></div>
<div class="m2"><p>که از آه من امشب عالمی سوخت</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چو آید سبز گون در دیده عالم</p></div>
<div class="m2"><p>چرا از دیده سازد روشنی کم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>عجب دارم کزین شام جدایی</p></div>
<div class="m2"><p>به دارو دیده یابد روشنایی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ترا ایشب کدورت از غم کیست؟</p></div>
<div class="m2"><p>سیه پوشی ترا از ماتم کیست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>همه زلفی جمال مهوشت کو؟</p></div>
<div class="m2"><p>همه دودی فروغ آتشت کو؟</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بقصد کشتن هر بیگناهی</p></div>
<div class="m2"><p>ز خون خلق خوردن دل سیاهی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گرفتم سر بسر مشک تتاری</p></div>
<div class="m2"><p>چه حاصل کز وفا بویی نداری</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چو شمع از من به مقراض جدایی</p></div>
<div class="m2"><p>جدا تا چند سازی روشنایی؟</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>شب یلدایی اما آنچنانی</p></div>
<div class="m2"><p>که با روز قیامت توأمانی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چنین شب را کی از راه سلامت</p></div>
<div class="m2"><p>دمد صبحی مگر روز قیامت</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ز شب پروانه چون خون در جگر داشت</p></div>
<div class="m2"><p>به یارب یارب شب دست برداشت</p></div></div>