---
title: >-
    بخش ۲۹ - زاری شمع از فراق پروانه
---
# بخش ۲۹ - زاری شمع از فراق پروانه

<div class="b" id="bn1"><div class="m1"><p>بلایی همچو تنهایی نباشد</p></div>
<div class="m2"><p>به تنهایی شکیبایی نباشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر آن لعلی که در سنگی درون است</p></div>
<div class="m2"><p>ز تنهاییست کش دل غرق خون است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نیارد تاب تنهایی دل کس</p></div>
<div class="m2"><p>که تنهایی خدا را زیبد و بس</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو شد در پرده شمع از زحمت باد</p></div>
<div class="m2"><p>دلش از بیکسی در آتش افتاد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو باد از روی یارش دیده بر دوخت</p></div>
<div class="m2"><p>به تنهایی درون پرده میسوخت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>فتاد از بیکسی آتش بجانش</p></div>
<div class="m2"><p>گدازان گشت مغز استخوانش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چنان افروخت در جانش چراغی</p></div>
<div class="m2"><p>که بر هر رشته جان دید داغی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نبودش رشته جان در بدن شمع</p></div>
<div class="m2"><p>پریشان بودش از محنت دل جمع</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دلش از داغ غم میسوخت از درد</p></div>
<div class="m2"><p>ولیکن زاریی در پرده میکرد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چنان میسوخت در هجران و میساخت</p></div>
<div class="m2"><p>کزو نیمی نماند از بسکه بگداخت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گدازان شد بفانوس آن پریوش</p></div>
<div class="m2"><p>چو زر در بوته سوزنده ز آتش</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دلش از بسکه سوز سینه دیدی</p></div>
<div class="m2"><p>کبابی شد کزو روغن چکیدی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>صدش داغ حبش از اشک بر رو</p></div>
<div class="m2"><p>کشیدی صد الف بر سینه هر سو</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بهم پیوسته اشک دانه دانه</p></div>
<div class="m2"><p>شده زنجیر پایش عاشقانه</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>همی کرد اضطراب و می نیاسود</p></div>
<div class="m2"><p>تو گفتی آتشش در پیرهن بود</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>اگر دستش به پیراهن رسیدی</p></div>
<div class="m2"><p>بسان غنچه اش صد جا دریدی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>به سوز سینه کز فانوس دیده</p></div>
<div class="m2"><p>بکین تیغ زبان بر وی کشیده</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>که این خلوت سرا زندان من شد</p></div>
<div class="m2"><p>نه زندان دوزخ سوزان من شد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چنان این خانه ام آتش بجان زد</p></div>
<div class="m2"><p>که خواهم آتش اندر خان و مان زد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ز دلتنگی چنین کاینجا به جانم</p></div>
<div class="m2"><p>بمیرم گر دمی دیگر بمانم</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>مگر آتش زنم در خانه خویش</p></div>
<div class="m2"><p>که این دیوار غم بردارم از پیش</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>مگر یاری نظر بر من گمارد</p></div>
<div class="m2"><p>مرا زین ورطه آتش برآرد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>وگرنه تا بکی پا بسته باشم</p></div>
<div class="m2"><p>درین پا بستگی دلخسته باشم</p></div></div>