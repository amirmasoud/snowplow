---
title: >-
    بخش ۱ - شمع و پروانه
---
# بخش ۱ - شمع و پروانه

<div class="b" id="bn1"><div class="m1"><p>بنام آنکه ما را از عنایت</p></div>
<div class="m2"><p>دهد پروانه شمع هدایت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رگ جان را دهد چون شمع روشن</p></div>
<div class="m2"><p>غذای زندگی از پهلوی تن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دلیل و رهنمای مقبلان است</p></div>
<div class="m2"><p>چراغ خاطر روشن دلان است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز نورش آتش وادی ایمن</p></div>
<div class="m2"><p>چراغ کار موسی ساخت روشن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بهر یکذره نورش در ظهورست</p></div>
<div class="m2"><p>دلیل این سخن الله نورست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز سنگ و آهن آن آتش که برخاست</p></div>
<div class="m2"><p>بود روشن که نور او در اشیاست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو شمع انوار او هر چند تابد</p></div>
<div class="m2"><p>کجا پروانه اندیشه یابد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>که جبریلی که عرشش زیر بال است</p></div>
<div class="m2"><p>کمین پروانه شمع جمال است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو برق از تیغ قهرش میزند دم</p></div>
<div class="m2"><p>بیکدم می فتد آتش بعالم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سخن از آتش قهرش چه رانم</p></div>
<div class="m2"><p>چو شمع آتش ببارد از زبانم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نسیم لطف او با هر که یار است</p></div>
<div class="m2"><p>در آتش گر بود در لاله زار است</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بصر کایینه قدرت نمایی است</p></div>
<div class="m2"><p>درو از عکس رویش روشنایی است</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بهفت اش پرده زان ترتیب کرده</p></div>
<div class="m2"><p>که باشد نور او در هفت پرده</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چراغ جان بد فانوس تن از اوست</p></div>
<div class="m2"><p>سرای دیده و دل روشن از اوست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چو او را با دل ما آشناییست</p></div>
<div class="m2"><p>چراغ دل هزارش روشناییست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>اگر شمع مرادش برفروزد</p></div>
<div class="m2"><p>ملک را بال چون پروانه سوزد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>زند پروانه را آتش به هستی</p></div>
<div class="m2"><p>از آن غیرت که کرد آتش پرستی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بسا شمع قد خوبان دلکش</p></div>
<div class="m2"><p>که برق غیرتش در وی زد آتش</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>شب تیره بصحرا جای مهتاب</p></div>
<div class="m2"><p>نماید شبچراغ از کرم شب تاب</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چنان شمع رخ گل برفروزد</p></div>
<div class="m2"><p>که چون پروانه بلبل را بسوزد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چو عیسی هر کرا نورش نوازد</p></div>
<div class="m2"><p>چراغ مرده در دم زنده سازد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چو نور شمع او پروانه بیند</p></div>
<div class="m2"><p>در آتش تا نسوزد کی نشنید</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>فروغ نور او از حد برون است</p></div>
<div class="m2"><p>ز فانوس خیال ما فزون است</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>اگر گردد زبان رگهای جانم</p></div>
<div class="m2"><p>چو شمع از وصف او سوزد زبانم</p></div></div>