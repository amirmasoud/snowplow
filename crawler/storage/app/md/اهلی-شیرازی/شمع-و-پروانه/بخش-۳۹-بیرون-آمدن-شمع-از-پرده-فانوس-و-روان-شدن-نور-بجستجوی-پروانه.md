---
title: >-
    بخش ۳۹ - بیرون آمدن شمع از پرده فانوس و روان شدن نور بجستجوی پروانه
---
# بخش ۳۹ - بیرون آمدن شمع از پرده فانوس و روان شدن نور بجستجوی پروانه

<div class="b" id="bn1"><div class="m1"><p>اگر عاشق گهی بهر صبوری</p></div>
<div class="m2"><p>بود در پرده عصمت ضروری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عجب نبود چو گردد شوق افزون</p></div>
<div class="m2"><p>که آید بی سبب از پرده بیرون</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گشاد پرده چونشمع آرزو کرد</p></div>
<div class="m2"><p>بهمت گل برون از غنچه آورد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>درآمد خادم و دامان فانوس</p></div>
<div class="m2"><p>به خدمت در میان زد از زمین بوس</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جمال شمع چون برقع برانداخت</p></div>
<div class="m2"><p>جهان روشن به نور خویشتن ساخت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از آزادی چو گل خندان برآمد</p></div>
<div class="m2"><p>تو گفتی یوسف از زندان برآمد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو مهر از صبح عارض چهره بگشود</p></div>
<div class="m2"><p>به نور خویش راه شام پیمود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>روان شد نور همچون برق تابان</p></div>
<div class="m2"><p>پی پروانه جستن در بیابان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نبود از جستجو یکدم فراغش</p></div>
<div class="m2"><p>همی جستی در آن شب باچراغش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در آن ظلمت همی شد با دل جمع</p></div>
<div class="m2"><p>که پشتش گرم بود از جانب شمع</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به هر منزل که از روزن همیرفت</p></div>
<div class="m2"><p>به چشم دیو و دد روشن همیرفت</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چنان جستش که گشت از جستجوست</p></div>
<div class="m2"><p>ولیکن یافت آخر آنچه می جست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>مدار ایطالب از جویندگی دست</p></div>
<div class="m2"><p>که در جویندگی یا بندگی هست</p></div></div>