---
title: >-
    مسمط
---
# مسمط

<div class="b2" id="bn1"><p>ناله قمری به گلستان و باغ</p>
<p>صل علی سیدنا المصطفی</p></div>
<div class="b" id="bn2"><div class="m1"><p>سوسن خود روی، زبان آخته</p></div>
<div class="m2"><p>گرد چمن نعره زنان فاخته</p></div></div>
<div class="b2" id="bn3"><p>روز و شب این ورد زبان ساخته</p>
<p>صل علی سیدنا المصطفی</p></div>
<div class="b" id="bn4"><div class="m1"><p>خواجه دین، والی خیر الانام</p></div>
<div class="m2"><p>احمد مختار، علیه السلام</p></div></div>
<div class="b2" id="bn5"><p>شاد بکن روح و بگو این کلام</p>
<p>صل علی سیدنا المصطفی</p></div>
<div class="b" id="bn6"><div class="m1"><p>روز ازل خامه صورت گشا</p></div>
<div class="m2"><p>بهر علی زد رقم انما</p></div></div>
<div class="b2" id="bn7"><p>ای دل آشفته، کجائی بیا</p>
<p>صل علی سیدنا المصطفی</p></div>
<div class="b" id="bn8"><div class="m1"><p>جامه دران غنچه خونین کفن</p></div>
<div class="m2"><p>در غم تیمار حسین و حسن</p></div></div>
<div class="b2" id="bn9"><p>نعره زنان بلبل بی خویشتن</p>
<p>صل علی سیدنا المصطفی</p></div>
<div class="b" id="bn10"><div class="m1"><p>آدم مقصود، شه باوفا</p></div>
<div class="m2"><p>نقد بنی آدم، آل عبا</p></div></div>
<div class="b2" id="bn11"><p>خیز و بگو از سر صدق و صفا</p>
<p>صل علی سیدنا المصطفی</p></div>
<div class="b" id="bn12"><div class="m1"><p>باقر و صادق، دوشه محترم</p></div>
<div class="m2"><p>دام سعادات و جهان کرم</p></div></div>
<div class="b2" id="bn13"><p>از ره اخلاص بگو دمبدم</p>
<p>صل علی سیدنا المصطفی</p></div>
<div class="b" id="bn14"><div class="m1"><p>موسی کاظم، شه عالیجناب</p></div>
<div class="m2"><p>شمع هدی، خواجه یوم الحساب</p></div></div>
<div class="b2" id="bn15"><p>از ره تحقیق بگو این جواب</p>
<p>صل علی سیدنا المصطفی</p></div>
<div class="b" id="bn16"><div class="m1"><p>ای که به حج رفتنت آمد هوس</p></div>
<div class="m2"><p>روضه سلطان خراسانت بس</p></div></div>
<div class="b2" id="bn17"><p>در رهش از صدق برآور نفس</p>
<p>صل علی سیدنا المصطفی</p></div>
<div class="b" id="bn18"><div class="m1"><p>مهر تقی در دل و جان منست</p></div>
<div class="m2"><p>حب نقی قوت روان منست</p></div></div>
<div class="b2" id="bn19"><p>دایم از اوراد نهان منست</p>
<p>صل علی سیدنا المصطفی</p></div>
<div class="b" id="bn20"><div class="m1"><p>دوش بر این طارم نیلوفری</p></div>
<div class="m2"><p>زهره به صد گونه زبان آوری</p></div></div>
<div class="b2" id="bn21"><p>گفت به روح حسن عسکری</p>
<p>صل علی سیدنا المصطفی</p></div>
<div class="b" id="bn22"><div class="m1"><p>مهدی هادی بدر آید ز غیب</p></div>
<div class="m2"><p>بشکند این شیشه ناموس و ریب</p></div></div>
<div class="b2" id="bn23"><p>نعره برآرد که نه کاری است عیب</p>
<p>صل علی سیدنا المصطفی</p></div>
<div class="b" id="bn24"><div class="m1"><p>حسرت و افسوس که عمری به کام</p></div>
<div class="m2"><p>رفت و نشد قصه شاهی تمام</p></div></div>
<div class="b2" id="bn25"><p>ختم کن این قصه بگو والسلام</p>
<p>صل علی سیدنا المصطفی</p></div>