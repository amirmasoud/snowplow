---
title: >-
    شمارهٔ ۱ - درباره هرات
---
# شمارهٔ ۱ - درباره هرات

<div class="b" id="bn1"><div class="m1"><p>شام رمضان خوشست و گلگشت هرات</p></div>
<div class="m2"><p>با نعره تکبیر و خروش صلوات</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خوبانش به تاریکی بازار ملک</p></div>
<div class="m2"><p>چون آب خضر نهان شده در ظلمات</p></div></div>