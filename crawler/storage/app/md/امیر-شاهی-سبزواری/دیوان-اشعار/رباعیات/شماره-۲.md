---
title: >-
    شمارهٔ ۲
---
# شمارهٔ ۲

<div class="b" id="bn1"><div class="m1"><p>ای مهر گسل که با توام پیوند است</p></div>
<div class="m2"><p>وز تو به یکی عشوه دلم خرسند است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>لطفی بکن و بگو که من زان توام</p></div>
<div class="m2"><p>پیداست که قیمت دروغی چند است</p></div></div>