---
title: >-
    شمارهٔ ۱۱
---
# شمارهٔ ۱۱

<div class="b" id="bn1"><div class="m1"><p>در حلقه عشق ره نیابد هر کس</p></div>
<div class="m2"><p>این گوشه مقام عارفان آمد و بس</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتی که رسم در سر زلفش به هوس</p></div>
<div class="m2"><p>زنار نبسته ای، در این حلقه مرس</p></div></div>