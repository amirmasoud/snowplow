---
title: >-
    شمارهٔ ۱۲
---
# شمارهٔ ۱۲

<div class="b" id="bn1"><div class="m1"><p>مائیم حریم انس را خاص شده</p></div>
<div class="m2"><p>در کوی تو پا بسته اخلاص شده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آهم به محیط چرخ غواص شده</p></div>
<div class="m2"><p>بر ناله من فرشته رقاص شده</p></div></div>