---
title: >-
    شمارهٔ ۳
---
# شمارهٔ ۳

<div class="b" id="bn1"><div class="m1"><p>شادم که ز من بر دل کس باری نیست</p></div>
<div class="m2"><p>کس را ز من و کار من آزاری نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر نیک شمارند و گرم بد گویند</p></div>
<div class="m2"><p>با نیک و بد هیچکسم کاری نیست</p></div></div>