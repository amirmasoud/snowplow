---
title: >-
    شمارهٔ ۱۶
---
# شمارهٔ ۱۶

<div class="b" id="bn1"><div class="m1"><p>ای دل، همه اسباب جهان خواسته گیر</p></div>
<div class="m2"><p>باغ طربت به سبزه آراسته گیر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وانگاه بر آن سبزه شبی چون شبنم</p></div>
<div class="m2"><p>بنشسته و بامداد برخاسته گیر</p></div></div>