---
title: >-
    شمارهٔ ۶
---
# شمارهٔ ۶

<div class="b" id="bn1"><div class="m1"><p>واعظی بود بر سر منبر</p></div>
<div class="m2"><p>لب به وعظ و به پند بگشاده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفت: هر مرد را بود به بهشت</p></div>
<div class="m2"><p>چند حور لطیف، آماده</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عورتی پیر از آن میان برخاست</p></div>
<div class="m2"><p>جانش اندر وساوس افتاده</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفت: بهر خدای مولانا</p></div>
<div class="m2"><p>یک سخن گوی، سوده و ساده</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هیچ در خلد حور نر باشد</p></div>
<div class="m2"><p>یا بود آن همه چو ما ماده؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفت: بنشین، که آنقدر باشد</p></div>
<div class="m2"><p>که نمانی تو نیز ناگاده</p></div></div>