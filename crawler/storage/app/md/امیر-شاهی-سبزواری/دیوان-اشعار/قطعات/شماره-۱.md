---
title: >-
    شمارهٔ ۱
---
# شمارهٔ ۱

<div class="b" id="bn1"><div class="m1"><p>شبی با صراحی چنین گفت شمع</p></div>
<div class="m2"><p>که: ای هر شبی مجلس آرای دوست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ترا با چنین قدر پیش قدح</p></div>
<div class="m2"><p>سجود دمادم بگو از چه روست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>صراحی بدو گفت: نشنیده ای</p></div>
<div class="m2"><p>تواضع ز گردن فرازان نکوست؟</p></div></div>