---
title: >-
    شمارهٔ ۳
---
# شمارهٔ ۳

<div class="b" id="bn1"><div class="m1"><p>در جمع ماهرویان، هم صحبتی است ما را</p></div>
<div class="m2"><p>کاسباب خرمی را، صد گونه ساز کرده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از باده های وصلش، هر کس گرفته جامی</p></div>
<div class="m2"><p>چون دور ما رسیده، آهنگ ناز کرده</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>لب بر لبش چو ساغر، خلقی بکام و شاهی</p></div>
<div class="m2"><p>از دور چون صراحی، گردن دراز کرده</p></div></div>