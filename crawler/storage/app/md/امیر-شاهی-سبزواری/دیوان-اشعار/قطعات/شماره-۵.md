---
title: >-
    شمارهٔ ۵
---
# شمارهٔ ۵

<div class="b" id="bn1"><div class="m1"><p>در آن کوش من بعد شاهی بدهر</p></div>
<div class="m2"><p>که روزی به انصاف ازاین خوان خوری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گرت نیم نان جو افتد بدست</p></div>
<div class="m2"><p>برغبت به از مرغ بریان خوری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نه زانسان که چندانکه مقدور تست</p></div>
<div class="m2"><p>ز افراط شهوت دو چندان خوری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز بسیار خوردن شوی مرده دل</p></div>
<div class="m2"><p>خود اندک خوری، گر غم جان خوری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو شد ز امتلا، طبع ناسازگار</p></div>
<div class="m2"><p>بود زهر اگر آب حیوان خوری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو عیسی به قرصی بساز از فلک</p></div>
<div class="m2"><p>که خر باشی اردیگ پالان خوری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بپای خودت رفت باید بگور</p></div>
<div class="m2"><p>چو بر اشتهای کسان نان خوری</p></div></div>