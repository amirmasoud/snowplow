---
title: >-
    شمارهٔ ۱۲
---
# شمارهٔ ۱۲

<div class="b" id="bn1"><div class="m1"><p>بسوخت آتش عشق تو بی‌گناه مرا</p></div>
<div class="m2"><p>بدوخت ناوک چشمت به یک نگاه مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به شمع نسبت بالای دلکشت کردم</p></div>
<div class="m2"><p>روا بود که بسوزی بدین گناه مرا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فتاده بر سر راه تو روی از آن مالم</p></div>
<div class="m2"><p>که پیر عشق چنین کرد رو براه مرا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به سایه که گریزم در این بلا که منم</p></div>
<div class="m2"><p>چو اهتمام تو نگرفت در پناه مرا؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خطای شاهی بیچاره را قلم درکش</p></div>
<div class="m2"><p>که هست لطف عمیم تو عذر خواه مرا</p></div></div>