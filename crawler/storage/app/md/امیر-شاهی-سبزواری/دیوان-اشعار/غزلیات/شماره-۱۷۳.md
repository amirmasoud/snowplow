---
title: >-
    شمارهٔ ۱۷۳
---
# شمارهٔ ۱۷۳

<div class="b" id="bn1"><div class="m1"><p>با اهل وفا ز هر چه داری</p></div>
<div class="m2"><p>جز جور و جفا دگر چه داری؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتی، ستم فراق سهلست</p></div>
<div class="m2"><p>بسم الله، از این بتر چه داری؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بردی دل و دین به چشم جادو</p></div>
<div class="m2"><p>تا چشم هنوز بر چه داری؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای پیک دیار آشنایی</p></div>
<div class="m2"><p>از غایب ما خبر چه داری؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خوش باش به عیب عشق، شاهی</p></div>
<div class="m2"><p>با خود بجز این هنر چه داری؟</p></div></div>