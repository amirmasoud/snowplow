---
title: >-
    شمارهٔ ۱۳۶
---
# شمارهٔ ۱۳۶

<div class="b" id="bn1"><div class="m1"><p>چشم تو خورد باده و من در خمار از آن</p></div>
<div class="m2"><p>آن غمزه کرد شوخی و من شرمسار از آن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیمار عشق را ز مداوا چه فایده</p></div>
<div class="m2"><p>فارغ شو ای طبیب، که بگذشت کار از آن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون دور لاله، عهد جوانی گذشت و ماند</p></div>
<div class="m2"><p>در سینه داغهای کهن یادگار از آن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آغشته شد به خون شهیدان عشق، خاک</p></div>
<div class="m2"><p>وین گل نمونه ای است به هر نوبهار از آن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شاهی، وفا مجوی ز اهل زمانه هیچ</p></div>
<div class="m2"><p>چون کس نشان نداد در این روزگار از آن</p></div></div>