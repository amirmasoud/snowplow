---
title: >-
    شمارهٔ ۱۶۸
---
# شمارهٔ ۱۶۸

<div class="b" id="bn1"><div class="m1"><p>ای دل، ار پی به سر کوی ارادت بردی</p></div>
<div class="m2"><p>گوی توفیق ز میدان سعادت بردی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر سیه نامه که بیمار شد از چشم خوشت</p></div>
<div class="m2"><p>نشنیدیم که نامش به عیادت بردی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دلبری شیوه و بیگانه شدن عادت تست</p></div>
<div class="m2"><p>دل عشاق بدین شیوه و عادت بردی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نرد خوبی به تو می باخت مه از کم بازی</p></div>
<div class="m2"><p>تا چه منصوبه نمودی که زیادت بردی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پیش ابروی بتان جمله قضا کن شاهی</p></div>
<div class="m2"><p>روزگاری که به محراب عبادت بردی</p></div></div>