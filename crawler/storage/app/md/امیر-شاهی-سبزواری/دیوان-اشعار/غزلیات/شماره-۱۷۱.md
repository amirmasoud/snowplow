---
title: >-
    شمارهٔ ۱۷۱
---
# شمارهٔ ۱۷۱

<div class="b" id="bn1"><div class="m1"><p>لب شیرین شکرخند داری</p></div>
<div class="m2"><p>ز خوبی هر چه می‌گویند داری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چه باشد گر گدای خویشتن را</p></div>
<div class="m2"><p>به دشنامی ز خود خرسند داری؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چه شیرین است بارت ای نی قند</p></div>
<div class="m2"><p>به سودای که دل در بند داری؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>متاب آن زلف را بهر دل من</p></div>
<div class="m2"><p>که آنجا مبتلایی چند داری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو دل در بند خوبانست، شاهی</p></div>
<div class="m2"><p>چه شد ار گوش سوی پند داری؟</p></div></div>