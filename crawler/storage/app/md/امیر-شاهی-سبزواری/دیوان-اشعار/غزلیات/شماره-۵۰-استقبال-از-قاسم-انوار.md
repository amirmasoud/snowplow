---
title: >-
    شمارهٔ ۵۰ - استقبال از قاسم انوار
---
# شمارهٔ ۵۰ - استقبال از قاسم انوار

<div class="b" id="bn1"><div class="m1"><p>هر کسی موسم گل گوشه باغی دارد</p></div>
<div class="m2"><p>ساکن کوی تو از روضه فراغی دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من در این کوی خوشم، گر چه به جنت رضوان</p></div>
<div class="m2"><p>مجلس خرم و آراسته باغی دارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>لاله بین چاک زده پیرهن خون آلود</p></div>
<div class="m2"><p>مگر او نیز ز سودای تو داغی دارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دل من در شب گیسوی تو ره گم کرده است</p></div>
<div class="m2"><p>مگرش روی تو در پیش چراغی دارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فکر سودای سر زلف تو دارد شاهی</p></div>
<div class="m2"><p>ظاهر آنست که آشفته دماغی دارد</p></div></div>