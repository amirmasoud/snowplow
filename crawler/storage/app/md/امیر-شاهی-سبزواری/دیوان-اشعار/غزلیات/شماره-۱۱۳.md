---
title: >-
    شمارهٔ ۱۱۳
---
# شمارهٔ ۱۱۳

<div class="b" id="bn1"><div class="m1"><p>پرده بگشا ز روی چون مه خویش</p></div>
<div class="m2"><p>که بجانم ز بخت گمره خویش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>میکشد سرو، پیش بالایت</p></div>
<div class="m2"><p>شرمساری ز قد کوته خویش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مینوازم چو چنگ در بر خود</p></div>
<div class="m2"><p>که فراموش میکنم ره خویش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>واعظا، ما و ناله دف و نی</p></div>
<div class="m2"><p>تو و گفتار ناموجه خویش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شاهی از بندگان تست، از او</p></div>
<div class="m2"><p>وامگیر التفات گه گه خویش</p></div></div>