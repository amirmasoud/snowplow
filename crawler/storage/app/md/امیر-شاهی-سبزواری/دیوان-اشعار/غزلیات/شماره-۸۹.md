---
title: >-
    شمارهٔ ۸۹
---
# شمارهٔ ۸۹

<div class="b" id="bn1"><div class="m1"><p>بی‌لبت هردم ز چشم درفشان خون می‌رود</p></div>
<div class="m2"><p>پاره‌های دل ز راه دیده بیرون می‌رود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یک شب ای شمع بتان، در کنج تاریک من آی</p></div>
<div class="m2"><p>تا ببینی حال تنها ماندگان چون می‌رود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خون که از زخمی رود، داغش نهی باز ایستد</p></div>
<div class="m2"><p>دل که صد جا داغ کردم، همچنان خون می‌رود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>باغبان از گفتگوی غنچه گو لب بسته دار</p></div>
<div class="m2"><p>بلبلان را چون سخن زان لعل میگون می‌رود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفته‌ای: فریاد شاهی کم نگشت از کوی ما</p></div>
<div class="m2"><p>آری آری، دل به کار عشق اکنون می‌رود</p></div></div>