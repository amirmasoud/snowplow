---
title: >-
    شمارهٔ ۱۶۱
---
# شمارهٔ ۱۶۱

<div class="b" id="bn1"><div class="m1"><p>عید است و نوبهار و جهانرا جوانئی</p></div>
<div class="m2"><p>هر مرغ را به وصل گلی شادمانئی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همچون هلال عید شدم زار و ناتوان</p></div>
<div class="m2"><p>روزی ندیدم از مه خود مهربانئی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>روزم به در دل گذرد، شب به سوز هجر</p></div>
<div class="m2"><p>دور از سعادت تو عجب زندگانئی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خلقی به عید، خرم و از نوبهار، خوش</p></div>
<div class="m2"><p>ما و فراق یاری و اندوه جانئی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شاهی به سوز عشق تو شد روشناس شهر</p></div>
<div class="m2"><p>داغ سگان بود ز برای نشانئی</p></div></div>