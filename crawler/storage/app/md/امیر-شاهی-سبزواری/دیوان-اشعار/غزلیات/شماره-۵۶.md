---
title: >-
    شمارهٔ ۵۶
---
# شمارهٔ ۵۶

<div class="b" id="bn1"><div class="m1"><p>هر دم ز عشق، بر دل من صد بلا رسد</p></div>
<div class="m2"><p>آری، بدور حسن تو اینها مرا رسد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جانم بلب رسید در این محنت و هنوز</p></div>
<div class="m2"><p>تا کار دل ز دیدن رویت کجا رسد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>انعام عام تو همه را میرسد، چه شد</p></div>
<div class="m2"><p>گر ناوکی به سینه این مبتلا رسد؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در جلوه گاه دوست رسیدن، نه حد ماست</p></div>
<div class="m2"><p>آنجا مگر شمال رود یا صبا رسد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شاهی بر آستان ارادت نشسته است</p></div>
<div class="m2"><p>با درد خو گرفته، که روزی دوا رسد</p></div></div>