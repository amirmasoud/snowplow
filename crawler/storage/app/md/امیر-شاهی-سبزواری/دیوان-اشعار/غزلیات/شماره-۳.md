---
title: >-
    شمارهٔ ۳
---
# شمارهٔ ۳

<div class="b" id="bn1"><div class="m1"><p>لبالب است ز خون جگر پیاله ما</p></div>
<div class="m2"><p>دم نخست چنین شد مگر حواله ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر آستان تو شب‌ها رود که مردم را</p></div>
<div class="m2"><p>به دیده خواب نیاید ز آه و ناله ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز قد و روی تو شرمنده باغبان می‌گفت:</p></div>
<div class="m2"><p>که آب و رنگ ندارند سرو و لاله ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به روز وصل تو از بیم هجر می‌ترسم</p></div>
<div class="m2"><p>که زهر می‌دهد ایام در نواله ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو گل به وصف رخت جامه چاک زد شاهی</p></div>
<div class="m2"><p>به هرکجا ورقی رفت از رساله ما</p></div></div>