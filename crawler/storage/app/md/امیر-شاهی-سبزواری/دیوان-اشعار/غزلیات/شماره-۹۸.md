---
title: >-
    شمارهٔ ۹۸
---
# شمارهٔ ۹۸

<div class="b" id="bn1"><div class="m1"><p>ای به لطف از آب حیوان پاک‌تر</p></div>
<div class="m2"><p>قدت از سرو روان چالاک‌تر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با که گویم درد خود، کز عشق اوست</p></div>
<div class="m2"><p>هر کرا بینم، ز من غمناک‌تر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بی‌رخت چون لاله داغم بر دلست</p></div>
<div class="m2"><p>سینه‌ام از دامن گل چاک‌تر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>لعلت از خونم ندارد هیچ باک</p></div>
<div class="m2"><p>نرگس شوخت از آن بی‌باک‌تر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هست شاهی در طریقت خاک راه</p></div>
<div class="m2"><p>لیک در کوی تو چیزی خاک‌تر</p></div></div>