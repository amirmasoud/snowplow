---
title: >-
    شمارهٔ ۳۷
---
# شمارهٔ ۳۷

<div class="b" id="bn1"><div class="m1"><p>امروز نسیم سحری بوی دگر داشت</p></div>
<div class="m2"><p>گویی گذر از خاک سر کوی دگر داشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ما یکدل و یکروی چنین، وان گل رعنا</p></div>
<div class="m2"><p>افسوس که از هر طرفی روی دگر داشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دی مردم از آن ذوق که در گفتن دشنام</p></div>
<div class="m2"><p>با ماش سخن بود و نظر سوی دگر داشت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خوش وقت کسی موسم نوروز، که چون گل</p></div>
<div class="m2"><p>هر روز سر آب و لب جوی دگر داشت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شاهی به تماشای مه عید نیامد</p></div>
<div class="m2"><p>بیچاره نظر در خم ابروی دگر داشت</p></div></div>