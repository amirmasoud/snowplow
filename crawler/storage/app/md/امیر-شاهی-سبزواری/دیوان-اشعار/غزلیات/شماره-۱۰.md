---
title: >-
    شمارهٔ ۱۰
---
# شمارهٔ ۱۰

<div class="b" id="bn1"><div class="m1"><p>خطت که درد و داغ تو نو می‌کند مرا</p></div>
<div class="m2"><p>جان در بلای عشق گرو می‌کند مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عمری به راه عشق ز سر داشتم قدم</p></div>
<div class="m2"><p>باز آرزوی آن تک و دو می‌کند مرا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من کشته از جواب سلامی و لطف یار</p></div>
<div class="m2"><p>امیدوار گفت و شنو می‌کند مرا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شرمنده خیال توام در غمی چنین</p></div>
<div class="m2"><p>کو پرسشی به آمد و رو می‌کند مرا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دید ابروی تو شاهی و دیوانه گشت باز</p></div>
<div class="m2"><p>آری خراب آن مه نو می‌کند مرا</p></div></div>