---
title: >-
    شمارهٔ ۱۰۹
---
# شمارهٔ ۱۰۹

<div class="b" id="bn1"><div class="m1"><p>گل نو آمد و هر کس به عیش و عشرت خویش</p></div>
<div class="m2"><p>من و عقوبت هجران و کنج محنت خویش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بلا و درد تو ما را نصیب شد، چکنم</p></div>
<div class="m2"><p>نه عاقلست که راضی نشد به قسمت خویش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زمانی از سر این خسته پا کشیده مدار</p></div>
<div class="m2"><p>که میبریم از این آستانه زحمت خویش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به داغ دوری اگر مبتلا شدیم، سزاست</p></div>
<div class="m2"><p>چو روز وصل نگفتیم شکر نعمت خویش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>قدم به کوی وفا مردوار نه، شاهی</p></div>
<div class="m2"><p>که پیر عشق روان کرد با تو همت خویش</p></div></div>