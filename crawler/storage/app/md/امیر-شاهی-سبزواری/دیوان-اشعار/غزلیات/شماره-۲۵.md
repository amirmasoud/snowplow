---
title: >-
    شمارهٔ ۲۵
---
# شمارهٔ ۲۵

<div class="b" id="bn1"><div class="m1"><p>بازم خدنگ غمزه زنی بر دل آمده است</p></div>
<div class="m2"><p>بازم ز عشق واقعه ای مشکل آمده است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر دیگران کشیده خدنگ جفای خویش</p></div>
<div class="m2"><p>این نکته ام ز یار بسی بر دل آمده است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آنکو نکرد سجده بمحراب ابرویی</p></div>
<div class="m2"><p>مردود شد ز قبله که ناقابل آمده است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نخل ترت که آب گل و یاسمین بریخت</p></div>
<div class="m2"><p>تا در کدام آب و هوا حاصل آمده است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شاهی بکوی عشق گر افتاده ای منال</p></div>
<div class="m2"><p>پایت ز آب دیده خود در گل آمده است</p></div></div>