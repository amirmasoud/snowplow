---
title: >-
    شمارهٔ ۱۸۵
---
# شمارهٔ ۱۸۵

<div class="b" id="bn1"><div class="m1"><p>چه شوخیست که در چشم پر فتن داری؟</p></div>
<div class="m2"><p>چه شیوه است که در زلف پر شکن داری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو ای رقیب، چه میخواهی از من بیدل</p></div>
<div class="m2"><p>که در میانه همین قصد جان من داری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حدیث خسرو وشیرین به دور تو گم شد</p></div>
<div class="m2"><p>که عاشقان بلاکش چو کوهکن داری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تو خون گرفته چه آئی بکوی او هر دم؟</p></div>
<div class="m2"><p>مگر چو من هوس خون خویشتن داری؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ببرد شیوه چشمت . . . دل شاهی</p></div>
<div class="m2"><p>هنوز تا تو در این شیوه‌ها چه فن داری</p></div></div>