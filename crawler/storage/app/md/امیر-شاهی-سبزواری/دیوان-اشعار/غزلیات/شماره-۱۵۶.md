---
title: >-
    شمارهٔ ۱۵۶
---
# شمارهٔ ۱۵۶

<div class="b" id="bn1"><div class="m1"><p>زهی عشقت آتش بجان در زده</p></div>
<div class="m2"><p>خطت کار خلقی بهم بر زده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چه ما را به سنگ جفا میزنی</p></div>
<div class="m2"><p>قدح با حریفان دیگر زده؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رخت نانوشته خط سبز خویش</p></div>
<div class="m2"><p>گل آتش در اوراق دفتر زده</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو من در خمار می لعل تو</p></div>
<div class="m2"><p>سبو را نگر دست بر سر زده</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گرو برده شاهی ز اقران به شعر</p></div>
<div class="m2"><p>چو با اوستادان برابر زده</p></div></div>