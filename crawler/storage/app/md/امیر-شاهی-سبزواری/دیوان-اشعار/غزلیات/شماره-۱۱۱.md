---
title: >-
    شمارهٔ ۱۱۱
---
# شمارهٔ ۱۱۱

<div class="b" id="bn1"><div class="m1"><p>هر کسی پهلوی یاری به هوای دل خویش</p></div>
<div class="m2"><p>ما گرفتار به داغ دل بی‌حاصل خویش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چند بینم سوی خوبان و دل از دست دهم</p></div>
<div class="m2"><p>وقت آنست که دستی بنهم بر دل خویش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>روزگاری سر من خاک درت منزل داشت</p></div>
<div class="m2"><p>گر بود عمر، رسم باز به سر منزل خویش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کارم از زلف تو درهم شد و مشکل اینست</p></div>
<div class="m2"><p>که گشادن نتوان پیش کسی مشکل خویش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دل ز اندیشه تو باز نیاید به جفا</p></div>
<div class="m2"><p>تو و بیداد و من و آرزوی باطل خویش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دم آخر سوی ما بین، که شهیدان ترا</p></div>
<div class="m2"><p>شرط باشد بحلی خواستن از قاتل خویش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شاهی، افتاده به خاک در او خوش می‌باش</p></div>
<div class="m2"><p>سگ گویی، که دهد جای تو در محفل خویش</p></div></div>