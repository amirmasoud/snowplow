---
title: >-
    شمارهٔ ۶۷
---
# شمارهٔ ۶۷

<div class="b" id="bn1"><div class="m1"><p>گر من از خاک درت رفتم، دل شیدا بماند</p></div>
<div class="m2"><p>تن روان شد بر طریق عزم و جان آنجا بماند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من خود آواره شدم، لیکن دل درمانده را</p></div>
<div class="m2"><p>پرسشی میکن، که در کویت تن تنها بماند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عاشقان را در غمت دل رفت و درد دل نرفت</p></div>
<div class="m2"><p>خستگان را در فراقت سرشد و سودا بماند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ساربان بر قصد دوری میزند طبل رحیل</p></div>
<div class="m2"><p>گو بران محمل، که ما را خاری اندر پا بماند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای صبا، از روی یاری با رفیق ما بگوی</p></div>
<div class="m2"><p>رو که شاهی را نظر بر صورت زیبا بماند</p></div></div>