---
title: >-
    شمارهٔ ۹۲
---
# شمارهٔ ۹۲

<div class="b" id="bn1"><div class="m1"><p>نصیب من ز تو گر درد و آه می‌آید</p></div>
<div class="m2"><p>خوشم که یاد منت گاهگاه می‌آید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو می‌روی و ز هر جانبی خلایق شهر</p></div>
<div class="m2"><p>پی نظاره شتابان که: شاه می‌آید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>غبار کوی تو در چشم دیده‌ام، زانست</p></div>
<div class="m2"><p>که سرمه در نظرم خاک راه می‌آید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نیاز من به چه در معرض قبول افتد</p></div>
<div class="m2"><p>به ملتی که عبادت گناه می‌آید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز اشک خویش شکایت کجا برد شاهی</p></div>
<div class="m2"><p>چو آب تیره‌اش از پیشگاه می‌آید</p></div></div>