---
title: >-
    شمارهٔ ۸۵
---
# شمارهٔ ۸۵

<div class="b" id="bn1"><div class="m1"><p>عمری دهان تنگ توام در خیال بود</p></div>
<div class="m2"><p>جان رمیده را همه فکر محال بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رفت آنکه در مسائل عشق و رموز شوق</p></div>
<div class="m2"><p>ز ابرو و غمزه با تو جواب و سؤال بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفتم: رسد میان توام باز در کنار</p></div>
<div class="m2"><p>گفتا: برو که آنچه تو دیدی خیال بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شرم آیدم که سجده برد پیش پای کس</p></div>
<div class="m2"><p>آن سر که سالها برهت پایمال بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آشفته رفت گفته شاهی در این غزل</p></div>
<div class="m2"><p>آری، بفکر زلف تو شوریده حال بود</p></div></div>