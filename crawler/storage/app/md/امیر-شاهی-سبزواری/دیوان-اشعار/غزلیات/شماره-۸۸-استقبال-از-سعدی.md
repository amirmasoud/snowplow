---
title: >-
    شمارهٔ ۸۸ - استقبال از سعدی
---
# شمارهٔ ۸۸ - استقبال از سعدی

<div class="b" id="bn1"><div class="m1"><p>سوی باغ آن سروبالا می‌رود</p></div>
<div class="m2"><p>باز کار فتنه بالا می‌رود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جان من، هرگه که جایی می‌روی</p></div>
<div class="m2"><p>عاشقان را دل به صد جا می‌رود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون دلم خون می‌کنی بشتاب از آنک</p></div>
<div class="m2"><p>روزگار از پهلوی ما می‌رود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هست گلگون سرشکم گرم رو</p></div>
<div class="m2"><p>در پِیَت میرانمش تا می‌رود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفتمش جان داد شاهی بی‌تو، گفت:</p></div>
<div class="m2"><p>بحث در خضر و مسیحا می‌رود</p></div></div>