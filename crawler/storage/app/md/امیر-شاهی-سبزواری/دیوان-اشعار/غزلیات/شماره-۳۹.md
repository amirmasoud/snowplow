---
title: >-
    شمارهٔ ۳۹
---
# شمارهٔ ۳۹

<div class="b" id="bn1"><div class="m1"><p>صبا تا ز زلف تو بویی نداشت</p></div>
<div class="m2"><p>دلم در جهان آرزویی نداشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جهان هرگز از نازنینان چو تو</p></div>
<div class="m2"><p>جفا پیشه تند خویی نداشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بهار آمد و هیچ بلبل نماند</p></div>
<div class="m2"><p>که پیش گلی گفتگویی نداشت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دلم کز جفای کسی خسته بود</p></div>
<div class="m2"><p>سر سبزه و طرف جویی نداشت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به ناکام شاهی برفت از درت</p></div>
<div class="m2"><p>که پیش تو هیچ آبرویی نداشت</p></div></div>