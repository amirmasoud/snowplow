---
title: >-
    شمارهٔ ۶
---
# شمارهٔ ۶

<div class="b" id="bn1"><div class="m1"><p>جان بهر تو در بلاست ما را</p></div>
<div class="m2"><p>دل پیش تو مبتلاست ما را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پیشت بدعا برآورم دست</p></div>
<div class="m2"><p>در دست همین دعاست ما را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر شب به هوای خاک کویت</p></div>
<div class="m2"><p>دیده بره صباست ما را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در منزل ما چو مه نیایی</p></div>
<div class="m2"><p>خود طالع آن کجاست ما را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تو ناوک غمزه زن، که پیشت</p></div>
<div class="m2"><p>سینه سپر بلاست ما را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مخرام چو گل قبا گشاده</p></div>
<div class="m2"><p>چون جامه جان قباست ما را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شاهی چه غم ار جفا کند یار</p></div>
<div class="m2"><p>چون رو به ره وفاست ما را</p></div></div>