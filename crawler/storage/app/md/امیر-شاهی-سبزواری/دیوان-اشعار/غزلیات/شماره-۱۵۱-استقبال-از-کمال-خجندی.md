---
title: >-
    شمارهٔ ۱۵۱ - استقبال از کمال خجندی
---
# شمارهٔ ۱۵۱ - استقبال از کمال خجندی

<div class="b" id="bn1"><div class="m1"><p>زهی از خطت نرخ عنبر شکسته</p></div>
<div class="m2"><p>قدت سرو را دست بر چوب بسته</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غباریست خطت نشسته بر آن لب</p></div>
<div class="m2"><p>بلی، خط یاقوت باشد نشسته</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز خرمای وصل تو ذوقی نیابند</p></div>
<div class="m2"><p>کسانی که از خار گردند خسته</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دلم بسته شد در شکنهای زلفت</p></div>
<div class="m2"><p>از آنروی گشتم چنین دلشکسته</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تو جایی که باشی، که باشند خوبان؟</p></div>
<div class="m2"><p>ز خاشاک با گل نبندند دسته</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در این باغ، روزی که نارسته بودم</p></div>
<div class="m2"><p>چو لاله نبودم ز داغ تو رسته</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دل شاهی از زلف خوبان هراسد</p></div>
<div class="m2"><p>چو آهوی از دام صیاد جسته</p></div></div>