---
title: >-
    شمارهٔ ۱۶۲
---
# شمارهٔ ۱۶۲

<div class="b" id="bn1"><div class="m1"><p>تا گشودی دو زلف عنبرسای</p></div>
<div class="m2"><p>باد شد عود سوز و نافه گشای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جای ما کوی تست، جور مکن</p></div>
<div class="m2"><p>که بدینها نمیرویم از جای</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بتماشا چو سرو قامت یار</p></div>
<div class="m2"><p>بر لب جوی شد قدح پیمای</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بنگر در هوای آن لب لعل</p></div>
<div class="m2"><p>گشته چشم پیاله خون پالای</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نرگس مست را فکند از چشم</p></div>
<div class="m2"><p>چمن از ساقیان بزم آرای</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر که را پیر عقل شد رهزن</p></div>
<div class="m2"><p>قول مطرب نگشت راهنمای</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سخن از زلف او مگو شاهی</p></div>
<div class="m2"><p>تا نیفتد سراسر اندر پای</p></div></div>