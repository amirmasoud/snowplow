---
title: >-
    شمارهٔ ۱۵۹ - استقبال از کمال خجندی
---
# شمارهٔ ۱۵۹ - استقبال از کمال خجندی

<div class="b" id="bn1"><div class="m1"><p>ای بهر قتل ما زده بر ابروان گره</p></div>
<div class="m2"><p>بگشا به خنده آن لب و از ابرو آن گره</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سوسن که با دهان تو از غنچه لاف زد</p></div>
<div class="m2"><p>از خجلتش فتاده نگر بر زبان گره</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مشاطه را ز طره او دست کوته است</p></div>
<div class="m2"><p>جعد بنفشه را نزند باغبان گره</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون گل زری که هست به می گر دهی بباد</p></div>
<div class="m2"><p>به زانکه غنچه وار زنی بر میان گره</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شبها چو چنگ ناله شاهی ز زلف اوست</p></div>
<div class="m2"><p>زانش فتاده است به رگهای جان گره</p></div></div>