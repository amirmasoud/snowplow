---
title: >-
    شمارهٔ ۲۳
---
# شمارهٔ ۲۳

<div class="b" id="bn1"><div class="m1"><p>تا خاک آستانه جانان مقام ماست</p></div>
<div class="m2"><p>در بزم عیش جرعه راحت به جام ماست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتی: فلان به کوی من از خاک کمتر است</p></div>
<div class="m2"><p>این هم چو بنگری سبب احترام ماست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زاهد حرام گفت می لعل را، بلی</p></div>
<div class="m2"><p>ما زائریم و میکده بیت‌الحرام ماست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا بر درش به خاک مذلت نشسته‌ایم</p></div>
<div class="m2"><p>سلطان چار بالش گردون غلام ماست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>روی چو زر به خاک درش تا نهاده‌ایم</p></div>
<div class="m2"><p>در ملک عشق سکه شاهی به نام ماست</p></div></div>