---
title: >-
    شمارهٔ ۱۷۰
---
# شمارهٔ ۱۷۰

<div class="b" id="bn1"><div class="m1"><p>اگر زلف تو خم در خم نبودی</p></div>
<div class="m2"><p>مرا حال این چنین در هم نبودی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غمی دارم ز زلفت یادگاری</p></div>
<div class="m2"><p>بلا بودی اگر این هم نبودی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کجا رفت آنکه در خلوتگه راز</p></div>
<div class="m2"><p>به جز ما و تو کس محرم نبودی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>غم از جور رقیبان است در عشق</p></div>
<div class="m2"><p>اگر از یار بودی، غم نبودی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رهایی جستی از بند تو شاهی</p></div>
<div class="m2"><p>بنای عشق اگر محکم نبودی</p></div></div>