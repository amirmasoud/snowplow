---
title: >-
    شمارهٔ ۳۸
---
# شمارهٔ ۳۸

<div class="b" id="bn1"><div class="m1"><p>دوش از رخ تو بزم گدایان چراغ داشت</p></div>
<div class="m2"><p>وز دیدن تو دیده گلستان و باغ داشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر جلوه‌ای که شاهد مه داشت بر فلک</p></div>
<div class="m2"><p>دل با فروغ روی تو زان‌ها فراغ داشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با شام طره تو نهان بود کار دل</p></div>
<div class="m2"><p>آن روی دلفروز مرا با چراغ داشت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون سبزه و گلست ز هجرت بخاک و خون</p></div>
<div class="m2"><p>آن دل که ذوق سبزه و گل در دماغ داشت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون لاله چاک شد دل شاهی ز سوز عشق</p></div>
<div class="m2"><p>کز گلشن زمانه بسی درد و داغ داشت</p></div></div>