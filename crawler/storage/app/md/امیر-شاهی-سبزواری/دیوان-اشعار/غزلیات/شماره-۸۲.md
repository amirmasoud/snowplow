---
title: >-
    شمارهٔ ۸۲
---
# شمارهٔ ۸۲

<div class="b" id="bn1"><div class="m1"><p>رفتیم، اگر چه دل به غمت دردمند بود</p></div>
<div class="m2"><p>در چین طره تو اسیر کمند بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بلبل به آه و ناله چمن را وداع کرد</p></div>
<div class="m2"><p>کان بزم را ترانه او ناپسند بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دشوار مینمود سفر با فراغ بال</p></div>
<div class="m2"><p>چون مرغ دل به دام کسی پای بند بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>القصه، در فراق سرآمد شمار عمر</p></div>
<div class="m2"><p>سرمایه وصال که داند که چند بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>راضی نشد که تکیه زند بر سریر ملک</p></div>
<div class="m2"><p>درویش را که پایه همت بلند بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خوش کردی ای رقیب، که آتش زدی بدل</p></div>
<div class="m2"><p>کاین داغ بر جراحت ما سودمند بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شاهی بهیچ روی ز تاب غمت نجست</p></div>
<div class="m2"><p>عمری اگر چه بر سر آتش سپند بود</p></div></div>