---
title: >-
    شمارهٔ ۳۲
---
# شمارهٔ ۳۲

<div class="b" id="bn1"><div class="m1"><p>گر نمی‌سوزد دلم، این آه دردآلود چیست؟</p></div>
<div class="m2"><p>آتشی گر نیست در کاشانه، چندین دود چیست؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عاقبت چون روی در نابود دارد بود ما</p></div>
<div class="m2"><p>این همه اندیشه بود و غم نابود چیست؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ناوک آن غمزه هر کس راست، ما را هم رسد</p></div>
<div class="m2"><p>چون مقدر گشت روزی، فکر دیر و زود چیست؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>درد دل را نیست بهبودی ز تشخیص علاج</p></div>
<div class="m2"><p>ای طبیب، آخر بگو درد مرا بهبود چیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر نه از بهر ربا پوشیده‌ای این خرقه را</p></div>
<div class="m2"><p>زاهد خودبین بگو، این قلب زراندود چیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یک شب ای آرام جان زان زلف سرکش بازپرس</p></div>
<div class="m2"><p>کز پریشانی دل‌ها آخرت مقصود چیست؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>محنت شاهی و تعظیم رقیبان تا به کی؟</p></div>
<div class="m2"><p>بندگانیم، این یکی مقبول و آن مردود چیست؟</p></div></div>