---
title: >-
    شمارهٔ ۱۸۴
---
# شمارهٔ ۱۸۴

<div class="b" id="bn1"><div class="m1"><p>چه حالتست که از حال من نمی‌پرسی؟</p></div>
<div class="m2"><p>سخن چه رفت که از من سخن نمی‌پرسی؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سرود ماست بهر مجلسی، نمی‌شنوی</p></div>
<div class="m2"><p>حدیث ماست بهر انجمن، نمی‌پرسی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خیال آن قد و رخسار می‌پزی، هیهات</p></div>
<div class="m2"><p>که از شمایل سرو و سمن نمی‌پرسی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز کنج غم به تمنا نمی‌رود شاهی</p></div>
<div class="m2"><p>تو بلبل قفسی از چمن نمی‌پرسی</p></div></div>