---
title: >-
    شمارهٔ ۱۴۲
---
# شمارهٔ ۱۴۲

<div class="b" id="bn1"><div class="m1"><p>تا خط تو برطرف مه آورد شبیخون</p></div>
<div class="m2"><p>از دیده روانست به هر نیم شبی خون</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خطی است به خون گل سیراب نوشته</p></div>
<div class="m2"><p>آن سبزه نو رسته بر آن عارض گلگون</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سنگی که زدی بر سر ما بیجهتی نیست</p></div>
<div class="m2"><p>لیلی به تکلف شکند کاسه مجنون</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چندانکه زدم گریه بر این شعله جانسوز</p></div>
<div class="m2"><p>ساکن نشد آتش ز درون، آب ز بیرون</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شاهی، به هواداری آن نرگس پرخواب</p></div>
<div class="m2"><p>بگذشت همه عمر به افسانه و افسون</p></div></div>