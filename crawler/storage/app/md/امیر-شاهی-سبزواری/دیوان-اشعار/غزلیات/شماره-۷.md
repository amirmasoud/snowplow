---
title: >-
    شمارهٔ ۷
---
# شمارهٔ ۷

<div class="b" id="bn1"><div class="m1"><p>به خود ره نیست در کوی تو مشتاقان شیدا را</p></div>
<div class="m2"><p>خم زلفت به قلاب محبت می‌کشد ما را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگر درپایت افکندم سری، عیبم مکن، کآنجا</p></div>
<div class="m2"><p>چنان بودم که از مستی ز سر نشناختم پا را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تو در دل می‌رسی مهمان چه جای صبر و عقل و جان</p></div>
<div class="m2"><p>زمانی باش، کز نامحرمان خالی کنم جا را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>غم ناآمده خوردن به نقدم رنجه می‌دارد</p></div>
<div class="m2"><p>همان بهتر که با فردا گذارم کار فردا را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز مژگانش دل زاهد کجا یابد اثر، شاهی</p></div>
<div class="m2"><p>بلی، خود کارگر ناید سنان خار بر خارا</p></div></div>