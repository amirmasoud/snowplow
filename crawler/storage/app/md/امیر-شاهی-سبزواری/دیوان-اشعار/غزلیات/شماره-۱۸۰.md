---
title: >-
    شمارهٔ ۱۸۰
---
# شمارهٔ ۱۸۰

<div class="b" id="bn1"><div class="m1"><p>ای بی خبر از سوز دل و داغ نهانی</p></div>
<div class="m2"><p>ما قصه خود با تو بگفتیم، تو دانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل مینگرد سوی تو، جان میرود از دست</p></div>
<div class="m2"><p>داریم از اینروی بسی دل نگرانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای شمع، که ما را به سخن شیفته کردی</p></div>
<div class="m2"><p>پروانه خود را مکش از چرب زبانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ما حال دل از گریه بجایی نرساندیم</p></div>
<div class="m2"><p>ای ناله، تو شاید که بجایی برسانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عمری است که با عارض تو شمع بدعویست</p></div>
<div class="m2"><p>وقتست که او را پی کاری بنشانی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون غنچه، ز خوناب درون لب نگشادیم</p></div>
<div class="m2"><p>افسوس که بر باد شد ایام جوانی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون دفتر گل، سر بسر از گفته شاهی</p></div>
<div class="m2"><p>هر جا ورقی باز کنی، خون بچکانی</p></div></div>