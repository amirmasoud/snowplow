---
title: >-
    شمارهٔ ۴۷
---
# شمارهٔ ۴۷

<div class="b" id="bn1"><div class="m1"><p>تا دل ز کف اختیار ننهاد</p></div>
<div class="m2"><p>پا بر سر کوی یار ننهاد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دور از تو چه داغ بود کایام</p></div>
<div class="m2"><p>بر جان و دل فکار ننهاد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مرغی که وفای دهر دانست</p></div>
<div class="m2"><p>دل بر گل نوبهار ننهاد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا بسته زلف او نشد دل</p></div>
<div class="m2"><p>سر بر خط روزگار ننهاد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در عشق تو زان فتاد شاهی</p></div>
<div class="m2"><p>کاول قدم استوار ننهاد</p></div></div>