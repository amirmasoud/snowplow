---
title: >-
    شمارهٔ ۳۶
---
# شمارهٔ ۳۶

<div class="b" id="bn1"><div class="m1"><p>مرا گر با تو روی همدمی نیست</p></div>
<div class="m2"><p>گدایان را به سلطان محرمی نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز عشقم درد و غم در دل بسی هست</p></div>
<div class="m2"><p>به اقبال توام زینها کمی نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پری را ماند آن مه در لطافت</p></div>
<div class="m2"><p>رقیبش نیز چندان آدمی نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کسی را گلبن امید نشکفت</p></div>
<div class="m2"><p>در این بستان که بوی خرمی نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خط جانان بکین برخاست، شاهی</p></div>
<div class="m2"><p>به غم بنشین، که جای بی‌غمی نیست</p></div></div>