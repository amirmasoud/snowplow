---
title: >-
    شمارهٔ ۷۷
---
# شمارهٔ ۷۷

<div class="b" id="bn1"><div class="m1"><p>پیکان غمزه را چو بتان آب می‌دهند</p></div>
<div class="m2"><p>اول نشان به سینه احباب می‌دهند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خاک رهش به مردم آسوده کی رسد</p></div>
<div class="m2"><p>کاین توتیا به دیده بی‌خواب می‌دهند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سیلی میان هر مژه ما را ز روی تست</p></div>
<div class="m2"><p>صد خار را ز بهر گلی آب می‌دهند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مژگان تو که یاری آن چشم می‌کنند</p></div>
<div class="m2"><p>تیغی کشیده در کف قصاب می‌دهند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شاهی به مجلس غم از آن می‌رود ز دست</p></div>
<div class="m2"><p>کش ساقیان دیده می ناب می‌دهند</p></div></div>