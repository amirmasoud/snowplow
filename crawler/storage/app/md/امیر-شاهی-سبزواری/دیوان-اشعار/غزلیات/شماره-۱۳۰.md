---
title: >-
    شمارهٔ ۱۳۰
---
# شمارهٔ ۱۳۰

<div class="b" id="bn1"><div class="m1"><p>بر بوی تو هر روز به گشت چمن آیم</p></div>
<div class="m2"><p>گریان به تماشاگه سرو و سمن آیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون غنچه دلی دارم از اندوه تو پر خون</p></div>
<div class="m2"><p>عیبم مکن ار چاکزده پیرهن آیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>درمانده شد از ناله من خلق، که هر روز</p></div>
<div class="m2"><p>گویند میا بر سر این کوی و من آیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یارب ز چنین باده پرذوق که خوردم</p></div>
<div class="m2"><p>روزی مکن آن روز که با خویشتن آیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عشق تو به دیوانگیم نام برآورد</p></div>
<div class="m2"><p>تا در خم آن سلسله پر شکن آیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>من طوطی قدسم، به قفس مانده گرفتار</p></div>
<div class="m2"><p>کو آینه روی تو تا در سخن آیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دیگر به فریبی نروم همره شاهی</p></div>
<div class="m2"><p>از بادیه عشق تو گر با وطن آیم</p></div></div>