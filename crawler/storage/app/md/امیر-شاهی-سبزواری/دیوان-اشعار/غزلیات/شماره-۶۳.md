---
title: >-
    شمارهٔ ۶۳
---
# شمارهٔ ۶۳

<div class="b" id="bn1"><div class="m1"><p>باغ را باز مگر مژده گلریز آمد</p></div>
<div class="m2"><p>که نسیم سحر از طرف چمن تیز آمد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>توتیا رنگ غباری ز رهش پیدا شد</p></div>
<div class="m2"><p>که صبا مشک فشان، غالیه آمیز آمد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نونو اسباب طرب ساخته کن، کاندر باغ</p></div>
<div class="m2"><p>گل نوخاسته و سبزه نوخیز آمد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>باز عشق توام از صبر جدایی فرمود</p></div>
<div class="m2"><p>باز بیمار مرا نوبت پرهیز آمد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جام شاهی که ز خون جگرش پر کردند</p></div>
<div class="m2"><p>خوار منگر، که زلال طرب‌انگیز آمد</p></div></div>