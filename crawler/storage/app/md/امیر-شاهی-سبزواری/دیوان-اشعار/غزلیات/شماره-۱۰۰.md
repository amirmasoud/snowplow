---
title: >-
    شمارهٔ ۱۰۰
---
# شمارهٔ ۱۰۰

<div class="b" id="bn1"><div class="m1"><p>سرو ما را هر زمان دل می‌کشد سوی دگر</p></div>
<div class="m2"><p>چون گل رعنا که دارد هر طرف روی دگر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر که دارد روی دل در قبله دیدار او</p></div>
<div class="m2"><p>سهو باشد سجده در محراب ابروی دگر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در طریق دوستی، ثابت قدم چون خاک باش</p></div>
<div class="m2"><p>چون صبا تا چند هر دم بر سر کوی دگر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جان بیمار مرا تاب شکیبایی نماند</p></div>
<div class="m2"><p>ای طبیب، ار عاقلی، جز صبر داروی دگر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پند گویا، بیش از اینم در صف طاعت مخوان</p></div>
<div class="m2"><p>زشت باشد روی در محراب و دل سوی دگر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>موسم نوروز و من در کنج تنهایی اسیر</p></div>
<div class="m2"><p>هر کسی در سایه سرو و لب جوی دگر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر دل شاهی به دشنامی بجویی دور نیست</p></div>
<div class="m2"><p>زان که همچون او نمی‌بینم دعاگوی دگر</p></div></div>