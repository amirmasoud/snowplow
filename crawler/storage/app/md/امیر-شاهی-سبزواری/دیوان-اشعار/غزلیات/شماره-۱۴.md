---
title: >-
    شمارهٔ ۱۴
---
# شمارهٔ ۱۴

<div class="b" id="bn1"><div class="m1"><p>زلف تو در کمند جنون می‌کشد مرا</p></div>
<div class="m2"><p>خوش‌خوش به کوی عشق درون می‌کشد مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هرجا که می‌گریزم از این فتنه، ناگهان</p></div>
<div class="m2"><p>عشقت عنان گرفته برون می‌کشد مرا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من دل نمی‌دهم به لب و چشم او، که یار</p></div>
<div class="m2"><p>گاه از فسانه گه به فسون می‌کشد مرا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر خاک آستان تو گریم به خون دل</p></div>
<div class="m2"><p>چون خاک می‌دواند و خون می‌کشد مرا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شاهی به کوی عشق مکن بعد از این قرار</p></div>
<div class="m2"><p>کاین دل به گوشه‌های جنون می‌کشد مرا</p></div></div>