---
title: >-
    شمارهٔ ۱۳۵
---
# شمارهٔ ۱۳۵

<div class="b" id="bn1"><div class="m1"><p>ما از حریم وصل تو با خاک در خوشیم</p></div>
<div class="m2"><p>گر جام باده نیست، به خون جگر خوشیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سامان ما مجو، که در این غصه شاکریم</p></div>
<div class="m2"><p>تدبیر ما مکن، که چنین بیخبر خوشیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خون خورده ایم دوش و خرابیم بامداد</p></div>
<div class="m2"><p>دیگر مده شراب دمادم، که سر خوشیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جان از برای تحفه جانان بود عزیز</p></div>
<div class="m2"><p>غافل گمان برد که بدین مختصر خوشیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شاهی، مقام قرب و کرامت رقیب راست</p></div>
<div class="m2"><p>ما را که رانده اند، ز بیرون در خوشیم</p></div></div>