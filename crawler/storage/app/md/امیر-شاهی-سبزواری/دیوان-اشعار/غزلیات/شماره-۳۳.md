---
title: >-
    شمارهٔ ۳۳
---
# شمارهٔ ۳۳

<div class="b" id="bn1"><div class="m1"><p>با طره تو سنبل شوریده‌حال چیست؟</p></div>
<div class="m2"><p>جاییکه ابروی تو نماید، هلال چیست؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا بر درت طریق گدایی گرفته‌ام</p></div>
<div class="m2"><p>دانسته‌ام که سلطنت بی‌زوال چیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حالا بوصل تو فلکم عشوه میدهد</p></div>
<div class="m2"><p>تا بخت خوابناک مرا در خیال چیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرغان باغ را چو نسیمی کفایتست</p></div>
<div class="m2"><p>با گل بگوی کین همه غنج و دلال چیست؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با آنکه باختیم سر اندر رهش چو گوی</p></div>
<div class="m2"><p>روزی نگفت شاهی شوریده، حال چیست</p></div></div>