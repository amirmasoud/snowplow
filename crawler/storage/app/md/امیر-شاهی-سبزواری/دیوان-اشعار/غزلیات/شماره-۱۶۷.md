---
title: >-
    شمارهٔ ۱۶۷
---
# شمارهٔ ۱۶۷

<div class="b" id="bn1"><div class="m1"><p>دلا تا ذوق هجران در نیابی</p></div>
<div class="m2"><p>ز باغ وصل جانان بر نیابی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگر در راه جانان جان ببازی</p></div>
<div class="m2"><p>تمنای دل از دلبر نیابی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>برغم من مکش بر دیگران تیغ</p></div>
<div class="m2"><p>که چون من کشتنی دیگر نیابی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هوس داری چو شمع این سوز، لیکن</p></div>
<div class="m2"><p>گر این سررشته یابی، سر نیابی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>متاب از کوی جانان روی، شاهی</p></div>
<div class="m2"><p>اگر یابی مرادی ور نیابی</p></div></div>