---
title: >-
    شمارهٔ ۸ - استقبال از کمال خجندی
---
# شمارهٔ ۸ - استقبال از کمال خجندی

<div class="b" id="bn1"><div class="m1"><p>چشم تو برانداخت به می، خانه ما را</p></div>
<div class="m2"><p>بگشود به رندی در میخانه ما را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از دیده و دل چند خورم خون خود، آخر</p></div>
<div class="m2"><p>سنگی بزن این ساغر و پیمانه ما را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر بگذری ای باد بدان زلف چو زنجیر</p></div>
<div class="m2"><p>زنهار بپرسی دل دیوانه ما را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر شب من و اندوه تو و گوشه محنت</p></div>
<div class="m2"><p>کاقبال نداند ره کاشانه ما را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آن بخت نداریم که یک شب مه رویت</p></div>
<div class="m2"><p>روشن کند این کلبه ویرانه ما را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حقا که به افسون دگرش خواب نیاید</p></div>
<div class="m2"><p>هرکس که شبی بشنود افسانه ما را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از تاب غمت سوخت به حسرت دل شاهی</p></div>
<div class="m2"><p>ای شمع تو آتش زده پروانه ما را</p></div></div>