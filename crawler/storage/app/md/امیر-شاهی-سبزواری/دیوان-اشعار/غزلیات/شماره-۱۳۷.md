---
title: >-
    شمارهٔ ۱۳۷
---
# شمارهٔ ۱۳۷

<div class="b" id="bn1"><div class="m1"><p>جان شد آواره و دل بهر تو افگار همان</p></div>
<div class="m2"><p>سر در این کار شد و با تو سر و کار همان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر کسی در پی کار و غم یاری و مرا</p></div>
<div class="m2"><p>دل همان، درد همان، عهد همان، یار همان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بلبلان چمن آسوده به همرازی گل</p></div>
<div class="m2"><p>در قفس ناله مرغان گرفتار همان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>قصه ما و تو افسانه هر کوی شده</p></div>
<div class="m2"><p>عشق را با دل سودا زده بازار همان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شاهی ار وصل بتان نیست، به هجران خوش باش</p></div>
<div class="m2"><p>گل همانست در این باغچه و خار همان</p></div></div>