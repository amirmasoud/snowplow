---
title: >-
    شمارهٔ ۵۳
---
# شمارهٔ ۵۳

<div class="b" id="bn1"><div class="m1"><p>مرا عشقت از ره برون می‌برد</p></div>
<div class="m2"><p>به کوی ملامت درون می‌برد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر اینست زنجیر زلف، ای حکیم</p></div>
<div class="m2"><p>ترا هم به قید جنون می‌برد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به تاراج دل چشم او بس نبود</p></div>
<div class="m2"><p>لبش نیز خطی به خون می‌برد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گل از روی او هست در انفعال</p></div>
<div class="m2"><p>ولیکن به خنده برون می‌برد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگر شاهی از لعل او برد جان</p></div>
<div class="m2"><p>از آن چشم خونریز چون می‌برد؟</p></div></div>