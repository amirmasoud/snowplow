---
title: >-
    شمارهٔ ۴۱
---
# شمارهٔ ۴۱

<div class="b" id="bn1"><div class="m1"><p>«گر تاج » بود عزم و یا هست «پچیو»</p></div>
<div class="m2"><p>«ور سنگ » بود عجب و وقار است «رزیو»</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>«سیفود» تواضع و شهامت «سیفور»</p></div>
<div class="m2"><p>«برکان » جهل است و کودنی هست «غلیو»</p></div></div>