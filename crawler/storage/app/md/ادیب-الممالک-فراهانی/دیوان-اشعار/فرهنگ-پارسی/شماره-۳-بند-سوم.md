---
title: >-
    شمارهٔ ۳ - بند سوم
---
# شمارهٔ ۳ - بند سوم

<div class="b" id="bn1"><div class="m1"><p>بزن ای دلبر هر هفت کرده</p></div>
<div class="m2"><p>نوای دوستی را هفت مرده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مفاعلین مفاعلین مفاعیل</p></div>
<div class="m2"><p>هزج آغاز کن در هفت پرده</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سروده و ستروع آهنگ است پرده</p></div>
<div class="m2"><p>دلیر و چابک و جنگی نبرده</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بط صهبا و میر باده نوشان</p></div>
<div class="m2"><p>دگر نوباوه بطیخ سرده</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>قسم سوگند و قول و شرط دمدار</p></div>
<div class="m2"><p>کتاب بیع و پیراهن نورده</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سرای بی روانان دخم و دخمه</p></div>
<div class="m2"><p>چو کاهو کب بود تابوت مرده</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مر آن، جذوار را با کاکنج گوی</p></div>
<div class="m2"><p>مه و پروین، نگار پشت پرده</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>قباله بیت و مزد آسیابان</p></div>
<div class="m2"><p>بود در پارسی این هر دو ترده</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کنیزک داده باشد عبد بنده</p></div>
<div class="m2"><p>اسیری را که بفروشند برده</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بود کاغاله و کاژیره قرطم</p></div>
<div class="m2"><p>چو خبه خاکشی زردچوبه هرده</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شش انداز اوستاد نردبازان</p></div>
<div class="m2"><p>بت آراسته، هر هفت کرده</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بود روشن سپهر از هفت خاتون</p></div>
<div class="m2"><p>شود بینا دو چشم از هفت پرده</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>کمیت اسب کهر اشقرع کرنگ است</p></div>
<div class="m2"><p>مجنس اکدش و ویژه است جرده</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بود آلا و شعله اخگر آذر</p></div>
<div class="m2"><p>و رزم آتش خدوک و جمره خرده</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>تنک نان و غیار قوم موسی</p></div>
<div class="m2"><p>نگاه و بالش و مجموع گرده</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>مرخشه نحس و فرخنده مبارک</p></div>
<div class="m2"><p>چو پیشانی چکاد و چهره چرده</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بود گرد دهان پتفوز و بدپوز</p></div>
<div class="m2"><p>کفل باشد سرین و کلیه گرده</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چو ویلان طفره و ویلانج حلوا</p></div>
<div class="m2"><p>فلاته میده حلوا رهشه ارده</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>درسته عفو و کین توزی زلیفن</p></div>
<div class="m2"><p>و جرگر، مفتی و چابک شکرده</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بود فرتور عکس و طبع دان چاپ</p></div>
<div class="m2"><p>غراره مضمضه ارابه غرده</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ترقی روز به ضدش فرارون</p></div>
<div class="m2"><p>چو پرمر انتظار و درد درده</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ضروری وایه و دربای و وایست</p></div>
<div class="m2"><p>جمد یخ منجمد باشد فسرده</p></div></div>