---
title: >-
    شمارهٔ ۵ - بند پنجم
---
# شمارهٔ ۵ - بند پنجم

<div class="b" id="bn1"><div class="m1"><p>ای خطت چون تازه سنبل وی رخت چون تازه ورد</p></div>
<div class="m2"><p>سنبل از رشک تو پیچان لاله از رنگ تو زرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فاعلاتن فاعلاتن فاعلاتن فاعلات</p></div>
<div class="m2"><p>از رمل این قطعه برخوان با نوای شاد ورد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اردکان قسمی از اشکال نجوم و خشم ارد</p></div>
<div class="m2"><p>انعکاس و انده و طیب و سپهر و مهر گرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>میخ کردن سکه باشد گرد نامه نقش آن</p></div>
<div class="m2"><p>جرمزه میدان سفرع کردن مسافر ره نورد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>«کژف » قیر و زاک شب شربین همان قطران بود</p></div>
<div class="m2"><p>هست دارو زرده زرچوبه منافق گوش زرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مشتری برجیس دان مریخ شد رزبادراد</p></div>
<div class="m2"><p>مانک ماه و زهره بیدخت است و هاله شادورد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>لمس را میدان پساویدن پژوهش جستجو</p></div>
<div class="m2"><p>تاخ ناف و صید کرد و درشکست او را شکرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کاردالی طلع و تارونه غلاف طلع دان</p></div>
<div class="m2"><p>بر شیان دارو عصا الراعی است یعنی سرخ مرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تکمه دان اخگوژنه سنهار باشد زن پسر</p></div>
<div class="m2"><p>گسسه خط مکتوب نامه فرو شوکت دارو برد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بایش ایجاب است و رانش سلب و جاور حال شد</p></div>
<div class="m2"><p>جاوری تبدیل دان پر ماس لمس و پهنه لرد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>رمز را پرخیده میدان واپرخیده صریح</p></div>
<div class="m2"><p>همچنین جفت واجفت آمد بمعنی زوج و فرد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دوله برهون و منش طبع و دما باشد مزاج</p></div>
<div class="m2"><p>سرمه دان حد و طرف اصل و تنه بیخ است و نرد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>هم نماری دان اشارت هم ضمیر آمد کشاک</p></div>
<div class="m2"><p>هم «درآمدجای » مصدر اسم نام و فعل کرد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نحل زیبود است و رسمو کارتن باشد رجال</p></div>
<div class="m2"><p>با سعادت بختیار و با فتوت راد مرد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>رهبر و فرنود و روشنگر همی باشد دلیل</p></div>
<div class="m2"><p>پیچ و تاب و جنگ و مانند و نکو باشد نورد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>هست هوتخشان کشاورز و بود ورزاو گاو</p></div>
<div class="m2"><p>مرد روزستار صنعت پیشه و جنگی نبرد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>اعتقاد آمد نمشته هم معاذالله ژکس</p></div>
<div class="m2"><p>هست سوگیری حمایت مهر رخشان روزگرد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>آنچه از گیسو نگیرد پیچ و خم فر خاک دان</p></div>
<div class="m2"><p>و آنچه از اشجار در پیراستن برند گرد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>شیم شیخ و خواجه ایشی بی بی و بانوستی</p></div>
<div class="m2"><p>خلسه فرزنشاد باشد طاعن السن سالخورد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>هست مزکت مسجد و شد سنجرستان خانقاه</p></div>
<div class="m2"><p>هم کشت آمد کلیسا هم «تموزی خانه » غرد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>پیشوارا مقتدادان مقتدی پی شو،پس ایست</p></div>
<div class="m2"><p>پیره و پوران خلیفه مرغزار آمد جفرد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چار نادر چار عنصر هفت گردون هفت مام</p></div>
<div class="m2"><p>چار آمیزه، طبایع هفت قراء هفت مرد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>هم سه پور آمد موالید آخشیان چار ضد</p></div>
<div class="m2"><p>قهری و قسری است شمپوری معاون پایمرد</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>شد بیوکانی طوی دغدو بیوک آمد عروس</p></div>
<div class="m2"><p>مام زن خشتامن و وردک جهاز اورنگ جرد</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چرخه دوراست و تسلسل زنخه و دشمیر ضد</p></div>
<div class="m2"><p>ارمغان و تحفه نور اهان عراضه راهورد</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>دان پذیر اراهیولی ماهیت او چیزی است</p></div>
<div class="m2"><p>زبده اروند است و میناگون سپهر لاژورد</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>هم سمیز آمد دعا چشمیده را منظوردان</p></div>
<div class="m2"><p>لای و تاه رخت و خواب مخمل و بیری است پرد</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>هست پودات و سترسا آنچه بشناسی بحس</p></div>
<div class="m2"><p>هم کسی باشد تعین همتراز و هم نبرد</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>فوطه را میدان بروفه هم دزک دستارچه</p></div>
<div class="m2"><p>ضلع دنده گفت شانه مازه پشت و رنج و درد</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>هم کژه باشد لهاة و هست کوشک لوزتین</p></div>
<div class="m2"><p>دژپه و دژپیهه دشپل ناملایم نانورد</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>صدقه ارزانش مسلم نیز ارزانی بود</p></div>
<div class="m2"><p>بشم و ژاله شبنم و یخچه تگرگ و بر دع سرد</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>بش بود کشتی که اندر دیم زار آید ببار</p></div>
<div class="m2"><p>کهپرک و غدو جوال کهکشان باشد الرد</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>«یتو» یعنی «لادبراین » را علیهذا شمر</p></div>
<div class="m2"><p>رشت گچ آگور آجر کلس آهک سنگ برد</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>رشوه بدگند است و ریما هن بود خبث الحدید</p></div>
<div class="m2"><p>پس خماهن دان حدید و مرد اشکمخواره ژرد</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>ذرع از تازی گز است از پارسی متر از فرنگ</p></div>
<div class="m2"><p>ساژن از روسی بود وز انگلیسی هست یرد</p></div></div>