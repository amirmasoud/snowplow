---
title: >-
    شمارهٔ ۵۰
---
# شمارهٔ ۵۰

<div class="b" id="bn1"><div class="m1"><p>کوته برف چو لارژ فراخ اترو است تنگ</p></div>
<div class="m2"><p>امپرسمان شتاب و آپاتی بود درنگ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سوردیته شد کری و گری تینیو آمده</p></div>
<div class="m2"><p>بیدست اسیترپی باشد بوات لنگ</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کاستر سگابی است و کشم خوک و سگ شین</p></div>
<div class="m2"><p>ضیغم لیون بود لئپار است خود پلنگ</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رو به رنار باشد و بوزینه سنژ، دان</p></div>
<div class="m2"><p>شد اژدها دراگن و بالن بود نهنگ</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بلبل بود رسینل و کرکس و وتور دان</p></div>
<div class="m2"><p>باشد حمامه پیژن و آمد گرو کلنگ</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اوتارد هست هوبره و پی بود کلاغ</p></div>
<div class="m2"><p>تنای گازو تیز اگو سر، شمار چنگ</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اف تخم مرغ و پوله بود جوجه کک خروس</p></div>
<div class="m2"><p>شد کایو سنگریزه پیر آمده است سنگ</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ارزق برن سفید بلان رنگ سبز، ور</p></div>
<div class="m2"><p>روشن همی کلر بود و کولر است رنگ</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شیطان ساتان و فارفاد جن، هومن آدمی</p></div>
<div class="m2"><p>سوکر است قند و چای ته باشد حشیش بنگ</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ماشان بدو کثیف ویلن شد گراسطبر</p></div>
<div class="m2"><p>خوشگل ژلی شمار وبل و بو بود فشنگ</p></div></div>