---
title: >-
    شمارهٔ ۷ - بند هفتم
---
# شمارهٔ ۷ - بند هفتم

<div class="b" id="bn1"><div class="m1"><p>زهی بچین دو زلف از حبش گرفته خراج</p></div>
<div class="m2"><p>نموده لشگر حسنت عقول را تاراج</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مفاعلن فعلاتن مفاعلن فعلات</p></div>
<div class="m2"><p>ز بهر مبحث این قطعه گشته استخراج</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کنونه حال و منش طبع و کوفشان نساج</p></div>
<div class="m2"><p>جشان بود گز درزی ارش بود قلاج</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>وظیفه جامگی و ماهواره شهریه</p></div>
<div class="m2"><p>پژول کعب و گزید و گزیت مال خراج</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>وژوه قطره باران که می چکد از سقف</p></div>
<div class="m2"><p>چنانکه بکران ته دیگ و طلمه نام کماج</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>قراقروت تو رخبین شمار با فرفور</p></div>
<div class="m2"><p>چو کشک پینو و آش سماق دان تتماج</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کبیده پست و بدوره مر آن که زله کند</p></div>
<div class="m2"><p>فروشه حلوا سختو همی بود زناج</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کرن پهول قرنفل چو باد رنگ خیار</p></div>
<div class="m2"><p>چنانکه کاهو کوک اسپناج اسفاناج</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ایازی است و ایاسی چو بیژه چشم آویز</p></div>
<div class="m2"><p>هو و وسنی و انباغ را بدان تو، نباج</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نویم محض و مجرد شوه بود باعث</p></div>
<div class="m2"><p>عداوت آمده آریغ و نهب شده تاراج</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سپیچه آنچه به بندد بروی سرکه و می</p></div>
<div class="m2"><p>سپهر بند طلسم است و سرکبا سگباج</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ایخشت است فلز دارتو بود طر طیر</p></div>
<div class="m2"><p>ضداخشیچ و سپیداب باشد اسفیذاج</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>تو بهرمان دان یاقوت و کامه شد مرجان</p></div>
<div class="m2"><p>چو لعل باشد گر کند و آبگینه زجاج</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>هزینه خرج بود چک برات و یافته قبض</p></div>
<div class="m2"><p>چو خفچه شوشه زربا ژوساو باشد باج</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>متاع باشد کالا اثاث کاچار است</p></div>
<div class="m2"><p>شله قصاص بود داو جنگ و کینه لجاج</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>دمان و گاه و کمانکش زمان و مدت و وقت</p></div>
<div class="m2"><p>شتاب باشد اوژول و ناگهان تا کاج</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>کلاژه کچله و دیگر کلاغ پیسه بود</p></div>
<div class="m2"><p>حمامه کالوچ است و تراج دان دراج</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>تویل مردم اصلع چکاد پیشانی</p></div>
<div class="m2"><p>بسونه زلف و مجعد غف است و تاری داج</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>شخار قلیا هم بیخ آن کنشتو دان</p></div>
<div class="m2"><p>چنانچه صابون برهوه و زاک باشد زاج</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چو مصطکی کیه گوشاد جنطیا نا شد</p></div>
<div class="m2"><p>شنوشه عطسه و پیلسته نیز باشد عاج</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چلاس لواس است و طفیل بشتالم</p></div>
<div class="m2"><p>کراس لقمه زناع کاره هست تو مرکاج</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>نماک رونق و نو سیره بحث و کاغذ نفج</p></div>
<div class="m2"><p>بود تماخره فیرید و مشوت کنگاج</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>کمند خام و سنان نیزه توپ کشگنجیر</p></div>
<div class="m2"><p>کباده هست کمان و هدف بود آماج</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بواشه آلت مذراة و ماله دان بتکن</p></div>
<div class="m2"><p>شیار شخم بود خیش و یوغ سر آماج</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>رعیتان دان گودهچگان و باد رمان</p></div>
<div class="m2"><p>چو امتان نبی بربروش و فر سنداج</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>سجاف هست فراویز و لبنه دان خشتک</p></div>
<div class="m2"><p>قبا ست یلمه و دیباه را شمر دیباج</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>هموخ مشعله باشد شماله اسپندار</p></div>
<div class="m2"><p>پلیته باشد افروشه و چراغ سراج</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>نی مجوف غرو است و نای پرهیرون</p></div>
<div class="m2"><p>درخت ساک که سازند کشتی از آن ساج</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>فرات باشد فالاد و دجله اروند است</p></div>
<div class="m2"><p>چو تنگه طنجه برنیو جزیره مهراج</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>فکانه هست جنینی که مرده سقط شود</p></div>
<div class="m2"><p>چنانکه آن زن نوزاده زاجه باشد و زاج</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>بیوک و دغد عروس است و بکر دوشیزه</p></div>
<div class="m2"><p>چنانکه حایض دشتان و قابله پازاج</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>کنشک تیرک عضو آمد و کنیسه کذشت</p></div>
<div class="m2"><p>ولیک کمرا زنار شد چلیپا خاج</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>دروگر است گته کار و کفشگر اسکاف</p></div>
<div class="m2"><p>خیاط درزی و الباد و پنبه زن حلاج</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>بدیهه آمده، انگارده فسانه و نقل</p></div>
<div class="m2"><p>نکشک مردم مقروض دان و عریان لاج</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>ضعیف غامی و مفلوج شیک و شیشله دان</p></div>
<div class="m2"><p>چهار چوبه در بواس و نردبان معراج</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>قدیم بوباشستی و نوشده حادث</p></div>
<div class="m2"><p>گواژه طعنه گواسه صفت بهشت اجماج</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>سروش هوش و خرد شد سرو شبد جبریل</p></div>
<div class="m2"><p>نیاز حاجت و آمین بود بجای تراج</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>بن است بطم و بود کاکیان خسکدانه</p></div>
<div class="m2"><p>علک و نیژد وزرنیله شد همان ریواج</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>مقطعه خامه زن و مصقله بود یزداغ</p></div>
<div class="m2"><p>ظروف و احول و ناژوو کاشکی همه کاج</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>سفینه هست سماری خله بود مردی</p></div>
<div class="m2"><p>چنانکه نوژیه سیل است و اشتراک امواج</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>بتک کتابت و کرکز علامت است و دلیل</p></div>
<div class="m2"><p>بنا به نوبت و دیهیم و گرزن آمد تاج</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>ستیم ریم جروح است و با خسه نشتر</p></div>
<div class="m2"><p>پروش مطلق جوشش هزار چشمه خراج</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>چو گردنا گل سرخ است و زعفران گیماس</p></div>
<div class="m2"><p>ولیک نیلپر و توله را شمر ور تاج</p></div></div>