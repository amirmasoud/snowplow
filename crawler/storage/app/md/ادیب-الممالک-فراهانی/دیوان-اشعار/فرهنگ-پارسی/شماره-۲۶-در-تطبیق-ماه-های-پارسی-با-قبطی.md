---
title: >-
    شمارهٔ ۲۶ - در تطبیق ماه‌های پارسی با قبطی
---
# شمارهٔ ۲۶ - در تطبیق ماه‌های پارسی با قبطی

<div class="b" id="bn1"><div class="m1"><p>ای همایون‌سرشت پاک‌نژاد</p></div>
<div class="m2"><p>وی گرامی ادیب فاضل راد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فاعلاتن مفاعلن فعلات</p></div>
<div class="m2"><p>جوی بحر خفیف از این سرواد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ماه فرسی قرینه قبطی</p></div>
<div class="m2"><p>بشنو از من که گفته است استاد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>«طوبه » «امشیر» و «برمهات » بود</p></div>
<div class="m2"><p>فرودین ماه و اردی و خرداد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>باز «برموده » و «بشنس » آمد</p></div>
<div class="m2"><p>این یکی «تیر و آندگر «مرداد»</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بونه «شهریور» است و مهر«ابیب »</p></div>
<div class="m2"><p>«مسری » آبان و «توت » «آذر» باد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بابه دی‌ماه و بهمن از «هاتور»</p></div>
<div class="m2"><p>وز «سپندارمذ» کیهک «افتاد»</p></div></div>