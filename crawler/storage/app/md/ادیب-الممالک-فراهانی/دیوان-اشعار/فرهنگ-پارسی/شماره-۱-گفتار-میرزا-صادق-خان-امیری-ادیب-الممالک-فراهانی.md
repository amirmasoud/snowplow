---
title: >-
    شمارهٔ ۱ - گفتار میرزا صادق خان امیری - ادیب الممالک فراهانی
---
# شمارهٔ ۱ - گفتار میرزا صادق خان امیری - ادیب الممالک فراهانی

<div class="b" id="bn1"><div class="m1"><p>آن بت شوخ چشم مه سیما</p></div>
<div class="m2"><p>نظم فرهنگ فرس جست از ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فاعلاتن مفاعلن فعلن</p></div>
<div class="m2"><p>شو به بحر خفیف چامه سرا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پاک یزدان و ایزد است خدا</p></div>
<div class="m2"><p>هده حق زنده حی عیان پیدا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دان نبی را پیمبر و خشور</p></div>
<div class="m2"><p>خاندان اهل بیت و جامه کسا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شرع آیین نظام دهناد است</p></div>
<div class="m2"><p>حکم پرمان روش بود یاسا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر زمان عرش و زیرگه کرسی</p></div>
<div class="m2"><p>هست کرفه و بزه ثواب و خطا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تار دوزخ صراط چینود است</p></div>
<div class="m2"><p>باغ مینو بهشت روح افزا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کار به نافله چنب سنت</p></div>
<div class="m2"><p>ناروا منع شد حلال روا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سحر فرهست و معجزه فرجود</p></div>
<div class="m2"><p>نیز فرجاد فاضل دانا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کعبه آباد خوان نوی فرقان</p></div>
<div class="m2"><p>گنگ دژهوخت مسجدالاقصی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شه ملک پیره دان ولیعهدش</p></div>
<div class="m2"><p>تیرم آن بانویست کش بسرا</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>شسن نامی و شسته دان محسوس</p></div>
<div class="m2"><p>دیم رخساره بشن دان بالا</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>منشی مردم طبیعی دان</p></div>
<div class="m2"><p>نیر نودی است مردم مشا</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نحو بربست و صرف بخش آمد</p></div>
<div class="m2"><p>علم منطق شمار بازگشا</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>خطه و نقطه چو ذره دان و محیط</p></div>
<div class="m2"><p>کشک و نیل و پنده و پیچا</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>کره گوی است و دائره برهون</p></div>
<div class="m2"><p>مرکزش وند سار و پن اما</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>هج عمودی و کج بود مایل</p></div>
<div class="m2"><p>قطب باشد نشین و ارض کنا</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>برش دید دان تو قطع نظر</p></div>
<div class="m2"><p>هست برگست معنی حاشا</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>پای خوان پچوه م پاچمی تورند</p></div>
<div class="m2"><p>شرح وستی کانه گویا</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>غر چه نامرد و قلتبان کردنگ</p></div>
<div class="m2"><p>قحبه لولی مخنث است بغا</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>هست سربار برد و سو تملیت</p></div>
<div class="m2"><p>لیک اندر میانه بکیاسا</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>کلمه واژه دان و نوله کلام</p></div>
<div class="m2"><p>نطق کرویز شد نمار ایما</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>وات لفظ آرش است و چم معنی</p></div>
<div class="m2"><p>هم لقب پاچنامه صوت آوا</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>فلک ادراک و فهم نیوند است</p></div>
<div class="m2"><p>قسوه نیرو و بیخرد شیدا</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>منشی آمد دبیر و نیز پناغ</p></div>
<div class="m2"><p>کلک و خامه قلم نکو شیوا</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>جزو فرشیم و سیمناد سور</p></div>
<div class="m2"><p>آیه چمراسو سیمراخ دعا</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>شد غزل گوی باد رنگین باف</p></div>
<div class="m2"><p>رمز گوی است مرد پیچه سرا</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>هجو جرشفت و شعر سرو اداست</p></div>
<div class="m2"><p>سجع سرواده ساختن انشا</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>هم پساوند قافیت باشد</p></div>
<div class="m2"><p>وزن سنجه حدیث دان سروا</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بخت و تاخیره طالع است و نصیب</p></div>
<div class="m2"><p>فال بد مرغوا و خوش مروا</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>ارتجک برق دان و تندر رعد</p></div>
<div class="m2"><p>باز ینوار جو پناد هوا</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>هست سوراک آب موج و حباب</p></div>
<div class="m2"><p>همچو گوراب دشت آب نما</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>لجه گرداب دان جزیره اداک</p></div>
<div class="m2"><p>شاخ آبه، خلیج و یم دریا</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>حصن و قلعه و حصار، انبا خون</p></div>
<div class="m2"><p>باز دژ دان و همچنین او را</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>منزل اسب باره بند بود</p></div>
<div class="m2"><p>خانه گوسپند انگژ وا</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>هست او در عمو و کاکو خال</p></div>
<div class="m2"><p>اب و جد را پدر شمار و نیا</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>ریش والانه و یقین واخ است</p></div>
<div class="m2"><p>پور واد است و آش باشد وا</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>آنج زعرورو شفترنک شلیل</p></div>
<div class="m2"><p>به و سیب است آبی و توپا</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>افد و افتد شگفت و مدح شگفت</p></div>
<div class="m2"><p>افتدستا شمار و افدستا</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>شهروا زر و سیم ناسره دان</p></div>
<div class="m2"><p>سره و ویژه هست شهر روا</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>لیت ای کاشکی لعل شاید</p></div>
<div class="m2"><p>ان و ان انما مانا</p></div></div>