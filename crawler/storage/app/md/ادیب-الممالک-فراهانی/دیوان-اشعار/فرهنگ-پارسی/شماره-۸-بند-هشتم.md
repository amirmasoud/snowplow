---
title: >-
    شمارهٔ ۸ - بند هشتم
---
# شمارهٔ ۸ - بند هشتم

<div class="b" id="bn1"><div class="m1"><p>ای آنکه گفتار ترا هوش و روان پاسخ بود</p></div>
<div class="m2"><p>وز آتش عشقت دلم تابنده چون دوزخ بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مستفعلن مستفعلن مستفعلن مستفعلن</p></div>
<div class="m2"><p>بلبل به تقطیع رجز گویا بشاخ و شخ بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دوزخ شمر تاریک را و آن شولمن دوزخ بود</p></div>
<div class="m2"><p>مانند آباد این پسر خود عالم برزخ بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پروز نژاد است و نسب بازیره یک حصه ز شب</p></div>
<div class="m2"><p>پنک است و اودس یک وجب فرسنگ خود فرسخ بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سیخ تباهه باب زن کش خوانده برخی تاب زن</p></div>
<div class="m2"><p>پاشنگ باشد آبزن جلاب خود آکخ بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نسترون آمد نسترن پروین همی باشد پرن</p></div>
<div class="m2"><p>هم زلزله شد بومهن دیگر تلوسه چخ بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در غاله شعب کوه دان رنجیده را بستوه دان</p></div>
<div class="m2"><p>بسیار را انبوه دان اشکاف و انده رخ بود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>حنظل کبست آمد همی ظاهر و غست آمد همی</p></div>
<div class="m2"><p>نشپیل شست آمد همی دام و نژنک آن فخ بود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>قند سپید ابلوج شد آبی همانا توج شد</p></div>
<div class="m2"><p>هم گردنه نغروج شد هم خودسری بر مخ بود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مغ جایگاه ژرف دان هلتاک را خود برف دان</p></div>
<div class="m2"><p>نغز نکو اشگرف دان خوب و بلند آوخ بود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مشعلچی آمد روشنگ شاهسپرم شد ونجنگ</p></div>
<div class="m2"><p>مفرق هباک و کف هبک وردان وژخ آزخ بود</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>وستاخها گستاخها دونان و شومان ماخها</p></div>
<div class="m2"><p>خالیگران طباخها هم پختگه مطبخ بود</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>زنبور منج و پشه بق وت پوستین و خوی عرق</p></div>
<div class="m2"><p>دیگر جواب و پاورق این هر دوان پاسخ بود</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>حمام و جامستی کدوخ آن فارتین دان پارگین</p></div>
<div class="m2"><p>آتشگه گرمابها گلخن و یا گولخ بود</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>باشد فراشالرزه تب پیسی بر ص پریون جرب</p></div>
<div class="m2"><p>پرهیختن یعنی ادب رحل کتب گیرخ بود</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>کچ فلس ماهی سب صدف دفزک سطبر و تاب تف</p></div>
<div class="m2"><p>سابور هاله پره صف اسب روان هیدخ بود</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>دان ساتگین پیمانه را و آن دلبر جانانه را</p></div>
<div class="m2"><p>میدان سفاهن شانه را زوبین همان ناچخ بود</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>آرایش آزین آمده ریشیده رنگین آمده</p></div>
<div class="m2"><p>جد وار پرپین آمده و آن پرپهن فرفخ بود</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>تاتاست لکنت در زبان تاتول باشد کژدهان</p></div>
<div class="m2"><p>هم ترجمان شد تاجمان هم چشم بد چشزخ بود</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>برق آذرخش آمد همی تقسیم بخش آمد همی</p></div>
<div class="m2"><p>آغاز وخش آمد همی خوب و خجسته دخ بود</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>متاره چرمین چغل تنسخ نفیس و کل کچل</p></div>
<div class="m2"><p>پر بر کلاه آمد کلل په په همان بخ بخ بود</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>شد سخت باز و شخ کمان رون باعث ورون امتحان</p></div>
<div class="m2"><p>فرش و نهالی ریسمان هم گاو آهن نخ بود</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>نانو همی دان نکره سکوی بیرون پاخره</p></div>
<div class="m2"><p>هم غلبکن شد پنجره هم دامن که شخ بود</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>نرموره بانوچ آمده تاج خره خوچ آمده</p></div>
<div class="m2"><p>مرد دوبین لوچ آمده لاغر بدن لخ لخ بود</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>زائیچ و خهر آمد وطن گور است و مدفن مرغزن</p></div>
<div class="m2"><p>پندار بد شید اهرمن آه و فسوس آوخ بود</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>دیوار میدان لادرا ریواز میخوان داد را</p></div>
<div class="m2"><p>بنیادگو بن لاد را چسبنده و آتش مخ بود</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>فرتاب وحی و تاب فر فرزین جری فرزان هنر</p></div>
<div class="m2"><p>آنگه تبرزین و تبردیگر نخ ناچخ بود</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>جهمرز می باشد زنا با عفت آمد پارسا</p></div>
<div class="m2"><p>باطل تبه تا با طلا لر، جوی و پهلوپخ بود</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>باشد قطایف فرخشه منحوس و ضایع مرخشه</p></div>
<div class="m2"><p>جنگ و خصومت خرخشه دیگر تسیز و چخ بود</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>ماریره شد مادند را هم ماد باشد مادرا</p></div>
<div class="m2"><p>خال، دایی و عم، افدرادخ دخت و دادا اخ بود</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>دست آورنجن یاره دان پر گاله لخت و پاره دان</p></div>
<div class="m2"><p>زشت و دده پتیاره دان آب فسرده یخ بود</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>گو خاکروبه رشت را هم محو و حک دان گشت را</p></div>
<div class="m2"><p>انبست و انبه مشت را مضراب و زخمه زخ بود</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>لک هرزه و لمتر کلان پیچه سیان و پرسیان</p></div>
<div class="m2"><p>سغری گفل ترسا، سه خوان زنارشان موسخ بود</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>انبه شدن زحمت شمر ماژیستان عصمت شمر</p></div>
<div class="m2"><p>آز و شره نهست شمر کشتارگه مسلخ بود</p></div></div>