---
title: >-
    شمارهٔ ۲ - بند دوم
---
# شمارهٔ ۲ - بند دوم

<div class="b" id="bn1"><div class="m1"><p>بت من چه این داستان می سرود</p></div>
<div class="m2"><p>ببحر تقارب تقرب نمود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فعولن فعولن فعولن فعول</p></div>
<div class="m2"><p>چه خوش باشد این سنجه با چنگ و رود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گریوه بود پشته و نهر رود</p></div>
<div class="m2"><p>زبر از فراز است و زیر از فرود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو بر بت بود بربط م و چنگ صنج</p></div>
<div class="m2"><p>کمانچه غژک دان و عود است رود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ربابه رواده بود و جد م و شت</p></div>
<div class="m2"><p>طرب را مشستی و خنیا سرود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سیاه آبه زاگاب و آمه دوات</p></div>
<div class="m2"><p>سلام است زندش تحیت درود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>حسد تیورک غبطه پژمان بود</p></div>
<div class="m2"><p>جگر خون دل دان و اندوه دود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>همان مرده ریگ است میراث وارث</p></div>
<div class="m2"><p>زیان است خسران و نفع است سود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو نیروی پنداره شد واهمه</p></div>
<div class="m2"><p>همان مکر و اندیشه دان نیرنود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زره پوش و خفتان و خرپشته شد</p></div>
<div class="m2"><p>دگر ترک و گبر است هم نام خود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>جماد و گیا بسته و رسته دان</p></div>
<div class="m2"><p>بسیط است کامود و ضد اشکیود</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چو خدیه مضاف است و مطلق بود</p></div>
<div class="m2"><p>همان موکده بشنو این نکته زود</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>«سبک موکده » عنصر آتش است</p></div>
<div class="m2"><p>«گران موکده » عنصر خاک بود</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گران خدیه آب و سبک خدیه باد</p></div>
<div class="m2"><p>سبب رون و اندیشه و بهره بود</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>کشک عقعق و سعوه سنگانه دان</p></div>
<div class="m2"><p>بود غاز خربت قطاع اسفرود</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>هزار است بلبل م غراب است زاغ</p></div>
<div class="m2"><p>کلنگ است گرگی عقاب است مود</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>صنوبرم بود ناژو و کاژو توژ</p></div>
<div class="m2"><p>تبر خون چو عناب توت است تود</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بشین و گهر ذات و وصف است زاب</p></div>
<div class="m2"><p>چو اویش هویت وجود است بود</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>همان گبر و ترسا و تیداک را</p></div>
<div class="m2"><p>مجوس و نصاری شمر با جهود</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بسودن بسفتن چو سائیدن است</p></div>
<div class="m2"><p>خراشیدن رخساره گوئی شخود</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بلارک پرند است و آتش زنه</p></div>
<div class="m2"><p>بود زند و غودان خف و پود و هود</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>هنایش اثر حاجت آیفت دان</p></div>
<div class="m2"><p>فلیوه بود هرزه غره فنود</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>تمطی است فنجا و خمیازه خاژ</p></div>
<div class="m2"><p>فلانه لود تار و پوده است پود</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چو ناویژ مغشوش و بیژه است ناب</p></div>
<div class="m2"><p>نبهره بود قلب و نو نابسود</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بنفشه بود فرمه شاهسپرم</p></div>
<div class="m2"><p>چو ریحان و سنبل بود آبرود</p></div></div>