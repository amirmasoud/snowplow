---
title: >-
    شمارهٔ ۴۰
---
# شمارهٔ ۴۰

<div class="b" id="bn1"><div class="m1"><p>بیشرمی «سلجن » است و عفت «هیوند»</p></div>
<div class="m2"><p>حقد است «سرول » و غدر باشد «نیوند»</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>«نیمول » عبوس و «نیوتور» آمد کبر</p></div>
<div class="m2"><p>طیس است تپنک و فهم باشد «بیوند»</p></div></div>