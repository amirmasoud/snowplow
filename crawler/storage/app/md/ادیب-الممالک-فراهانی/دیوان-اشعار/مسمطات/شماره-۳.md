---
title: >-
    شمارهٔ ۳
---
# شمارهٔ ۳

<div class="b" id="bn1"><div class="m1"><p>قدم گذار به دیوان عالی و بشناس</p></div>
<div class="m2"><p>که کیست آنکه به کرسی نشسته چون نسناس</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بگرد وی بنگر چند تن خدا نشناس</p></div>
<div class="m2"><p>مگو که چرخ عجب مهره ای فکنده بطاس</p></div></div>
<div class="b2" id="bn3"><p>که چرخ سفله بسی خوارها نموده عزیز</p></div>
<div class="b" id="bn4"><div class="m1"><p>نظر نما بنگر صورت هیولا را</p></div>
<div class="m2"><p>به جای طوطی و طاوس بین قولا را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به حکمرانی بنگر جوان …ـولا را</p></div>
<div class="m2"><p>معین و یاور و یارش به بین شمولا را</p></div></div>
<div class="b2" id="bn6"><p>بنی سرائیل آنجا نشسته بر سر میز</p></div>
<div class="b" id="bn7"><div class="m1"><p>یکی ز شدت پیری در آمده قوزش</p></div>
<div class="m2"><p>به سان روبه دیمی شده دک و پوزش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هزار رنگ به صورت چو آتش‌افروزش</p></div>
<div class="m2"><p>رسد به عرش برین بانک سرفه و …ـوزش</p></div></div>
<div class="b2" id="bn9"><p>که من معیلم و مسکینم و ندارم چیز</p></div>
<div class="b" id="bn10"><div class="m1"><p>مر آن که سخره سرودی رئیس اکوسه‌اش</p></div>
<div class="m2"><p>کسی ندیدی جز در مبال مدرسه‌اش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>فرار کردی قمل ز چرک البسه‌اش</p></div>
<div class="m2"><p>هراز گونه اثر دیده شد ز وسوسه‌اش</p></div></div>
<div class="b2" id="bn12"><p>کنون ز فرط نظافت شده است عنبربیز</p></div>