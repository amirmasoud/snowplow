---
title: >-
    شمارهٔ ۲
---
# شمارهٔ ۲

<div class="b" id="bn1"><div class="m1"><p>سال نهمین است که این ملت بیدار</p></div>
<div class="m2"><p>با خون خود آمد بحق خویش خریدار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شد نور عدالت ز پس پرده پدیدار</p></div>
<div class="m2"><p>پوشید بتن خلعت نو سرو و سپیدار</p></div></div>
<div class="b2" id="bn3"><p>زد شاهد مشروطه صلا از پی دیدار</p>
<p>تا در قدمش جان گرامی بسپارند</p></div>
<div class="b" id="bn4"><div class="m1"><p>آورد دبیر فلکی لوح و قلم را</p></div>
<div class="m2"><p>بسترد ز دیوان قضا نام ستم را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زد پادشه داد بر افلاک علم را</p></div>
<div class="m2"><p>بنشاند مهد اندر معشوقه جم را</p></div></div>
<div class="b2" id="bn6"><p>فرض است بعشاق که این باره صنم را</p>
<p>فرخنده شمارند و پسندیده بدارند</p></div>
<div class="b" id="bn7"><div class="m1"><p>از پرتو نور خرد عاقبت اندیش</p></div>
<div class="m2"><p>افروخته شد نور بکاشانه درویش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ای باد مزن لطمه بر این شمع و از آن پیش</p></div>
<div class="m2"><p>کز جانوران نیش رسد بر جگر ریش</p></div></div>
<div class="b2" id="bn9"><p>این جانوران را بشکن بال و پر و نیش</p>
<p>مگذار که از روزن خود سر بدر آرند</p></div>
<div class="b" id="bn10"><div class="m1"><p>ای شاهد مشروطه که از طره پر خم</p></div>
<div class="m2"><p>آشفته کنی هوش و روان بنی آدم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>انگشت سلیمان را لعلت شده خاتم</p></div>
<div class="m2"><p>آنی تو که از صدق و صفا مردم عالم</p></div></div>
<div class="b2" id="bn12"><p>اندر کمرت دست ارادت زده محکم</p>
<p>واندر طلبت پای جلادت بفشارند</p></div>
<div class="b" id="bn13"><div class="m1"><p>اندیشه ز طوفان مکن ای همسفر نوح</p></div>
<div class="m2"><p>شرح غم خود بازده ای سینه مشروح</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>طوبی لک یا نفس هنیئالک یا روح</p></div>
<div class="m2"><p>کاین باب بروح تو ز یزدان شده مفتوح</p></div></div>
<div class="b2" id="bn15"><p>این است طبیبی که دوای دل مجروح</p>
<p>بر زخم گذارد اگرش می بگذارند</p></div>
<div class="b" id="bn16"><div class="m1"><p>امسال بنامیزد سال نهمین است</p></div>
<div class="m2"><p>کاماده دوای دل رنجور غمین است</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>یسرت به یسار اندر و یمنت به یمین است</p></div>
<div class="m2"><p>هان تخم برافشان که کشاورز امین است</p></div></div>
<div class="b2" id="bn18"><p>گفتار چو تخم است و دل خلق زمین است</p>
<p>بی شک ز زمین روید تخمی که بکارند</p></div>
<div class="b" id="bn19"><div class="m1"><p>کودک به دل مام چو نه ماه بماند</p></div>
<div class="m2"><p>خود را بمقامی که سزد بکشاند</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>یا للعجب این کودک اگر می بتواند</p></div>
<div class="m2"><p>خود را پس نه سال از این ورطه رهاند</p></div></div>
<div class="b2" id="bn21"><p>امید که یزدانش به پیری برساند</p>
<p>نه ساله ما را که چو نه ماهه شمارند</p></div>
<div class="b" id="bn22"><div class="m1"><p>این کودک نه ساله که مشروطه شدش نام</p></div>
<div class="m2"><p>یک لحظه ز خون ریختنش کی بود آرام</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>کودک نشنیدستم کاندر بغل مام</p></div>
<div class="m2"><p>با خون دل خلق بشوید سر و اندام</p></div></div>
<div class="b2" id="bn24"><p>آنان که زنند از پی دلجوئی او گام</p>
<p>خون جگر و دل را چون باده گسارند</p></div>
<div class="b" id="bn25"><div class="m1"><p>مشروطه عروسی است که گر چهره نپوشد</p></div>
<div class="m2"><p>هر دیده مر او را پی دیدار بکوشد</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>مستی که از این دست یکی جرعه بنوشد</p></div>
<div class="m2"><p>دین و خرد وهوش بساقی بفروشد</p></div></div>
<div class="b2" id="bn27"><p>دیوانه این عشق نصیحت ننیوشد</p>
<p>گر خون دلش روز و شب از دیده ببارند</p></div>
<div class="b" id="bn28"><div class="m1"><p>ای مجلس ملی شه و دیهیم همایون</p></div>
<div class="m2"><p>هستند ترا منتظر مقدم میمون</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>ایام فراقت ز سه سال آمده افزون</p></div>
<div class="m2"><p>وین خلق فشانند به هجرت ز بصر خون</p></div></div>
<div class="b2" id="bn30"><p>آنان که شدستند بدیدارت مفتون</p>
<p>هجران ترا طاقت ازین بیش نیارند</p></div>
<div class="b" id="bn31"><div class="m1"><p>آنان که نهفتند ز دیدار خوشت رو</p></div>
<div class="m2"><p>رفتند ز کوی تو بدین سوی و بدان سو</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>شاگرد مسیحند ولی از دم جادو</p></div>
<div class="m2"><p>غلطیده بخاک اندر و افتاده ز نیرو</p></div></div>
<div class="b2" id="bn33"><p>انگشت بخایند بدندان که جفا جو</p>
<p>مهلت ندهدشان که سر خویش بخارند</p></div>
<div class="b" id="bn34"><div class="m1"><p>ای شاه جهان یکسره بر کام تو باشد</p></div>
<div class="m2"><p>زهره به فلک نوبتی بام تو باشد</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>آسایش این خلق در ایام تو باشد</p></div>
<div class="m2"><p>عمر ابدی جرعه ای از جام تو باشد</p></div></div>
<div class="b2" id="bn36"><p>سر دفتر شاهان جهان نام تو باشد</p>
<p>آن روز که تاریخ شهان را بنگارند</p></div>