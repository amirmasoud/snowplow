---
title: >-
    شمارهٔ ۵ - ترجمه اشعار ایل بیگی مرحوم جانشین تیمور
---
# شمارهٔ ۵ - ترجمه اشعار ایل بیگی مرحوم جانشین تیمور

<div class="b" id="bn1"><div class="m1"><p>آرم از قول بزرگان مه برون از زیر ابر</p></div>
<div class="m2"><p>طاعت عالم کنم تا بشکنم بازار جبر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گرم گردم در تماشای پلنگ و شیر و ببر</p></div>
<div class="m2"><p>منع نتوانم نمود از مردم بی تاب و صبر</p></div></div>
<div class="b2" id="bn3"><p>اینچنین بود است و خواهد شد چنین ای دوستان</p></div>
<div class="b" id="bn4"><div class="m1"><p>روزگاری شد که من تقلید دنیا می کنم</p></div>
<div class="m2"><p>سینه پرشور و فغان سر پر ز سودا می کنم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اهل دنیا را درین دنیا تماشا می کنم</p></div>
<div class="m2"><p>همچو موسی روی خود در طور سینا می کنم</p></div></div>
<div class="b2" id="bn6"><p>اینچنین بود است و خواهد شد چنین ای دوستان</p></div>
<div class="b" id="bn7"><div class="m1"><p>شعله آتش در ایران سخت ظاهر می شود</p></div>
<div class="m2"><p>آشکارا حکم از سلطان قاهر می شود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هر زمان ظلم و ستم از خلق صادر می شود</p></div>
<div class="m2"><p>دور دور شاه عالمگیر نادر می شود</p></div></div>
<div class="b2" id="bn9"><p>اینچنین بود است و خواهد شد چنین ای دوستان</p></div>
<div class="b" id="bn10"><div class="m1"><p>جمله خاصان دور از شهر و وطن خواهند شد</p></div>
<div class="m2"><p>بلبلان آواره از طرف چمن خواهند شد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>پادشاهان کشته بی غسل و کفن خواهند شد</p></div>
<div class="m2"><p>خسروان زند کم روزی بزن خواهند شد</p></div></div>
<div class="b2" id="bn12"><p>اینچنین بود است و خواهد شد چنین ای دوستان</p></div>
<div class="b" id="bn13"><div class="m1"><p>آنزمان اسرار پنهان آشکار آید همی</p></div>
<div class="m2"><p>زینت و آئین و زیور بیشمار آید همی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>هر که مست از خواب غفلت هوشیار آید همی</p></div>
<div class="m2"><p>هر که ناهموار شد هموار و خوار آید همی</p></div></div>
<div class="b2" id="bn15"><p>اینچنین بود است و خواهد شد چنین ای دوستان</p></div>
<div class="b" id="bn16"><div class="m1"><p>دولت قاجار خواهد سکه زد بر سیم و زر</p></div>
<div class="m2"><p>برجهد تیمور شاه از خواب و گردد باخبر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>با سر آید در صف میدان و سازد ترک سر</p></div>
<div class="m2"><p>هر طرف بینی شرار فتنه و آشوب و شر</p></div></div>
<div class="b2" id="bn18"><p>اینچنین بود است و خواهد شد چنین ای دوستان</p></div>
<div class="b" id="bn19"><div class="m1"><p>دسته چابکسواران بیدرنگ آید همی</p></div>
<div class="m2"><p>روز صید شیر و نخجیر پلنگ آید همی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>یک گل از یک شاخ با صدگونه رنگ آید همی</p></div>
<div class="m2"><p>عرصه گیتی بچشم خلق تنگ آید همی</p></div></div>
<div class="b2" id="bn21"><p>اینچنین بود است و خواهد شد چنین ای دوستان</p></div>
<div class="b" id="bn22"><div class="m1"><p>مردی مردم مبدل بر گزاف اندر شود</p></div>
<div class="m2"><p>راستی چون صارم کج در غلاف اندر شود</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>خلق را سرمایه از لاف و خلاف اندر شود</p></div>
<div class="m2"><p>گفتگوی مردمان با تلگراف اندر شود</p></div></div>
<div class="b2" id="bn24"><p>اینچنین بود است و خواهد شد چنین ای دوستان</p></div>
<div class="b" id="bn25"><div class="m1"><p>شورش و غوغا عیان در ملک ایرانی شود</p></div>
<div class="m2"><p>وز گرانی دردها بر خلق ارزانی شود</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>نیکمردی همچو مردان زایل و فانی شود</p></div>
<div class="m2"><p>آنکه بودت یار جانی دشمن جانی شود</p></div></div>
<div class="b2" id="bn27"><p>اینچنین بوداست و خواهدشد چنین ای دوستان</p></div>
<div class="b" id="bn28"><div class="m1"><p>ای دریغا کم غم دوران دلی دارم بتنگ</p></div>
<div class="m2"><p>هر طرف سرباز بینم با قطار و با فشنگ</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>هر زمان در گوشم آید نعره توپ و تفنگ</p></div>
<div class="m2"><p>کشور ایران بعینه گشت خواهد چون فرنگ</p></div></div>
<div class="b2" id="bn30"><p>اینچنین بود است و خواهد شد چنین ای دوستان</p></div>
<div class="b" id="bn31"><div class="m1"><p>قولشان یکسر خلاف و عهدشان یکباره سست</p></div>
<div class="m2"><p>لاله هاشان خار و زر مس کارهاشان نادرست</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>کس درین مردم درستی یا جوانمردی نجست</p></div>
<div class="m2"><p>نصف ایران روس برد ایرانی از آن دست شست</p></div></div>
<div class="b2" id="bn33"><p>اینچنین بود است و خواهد شد چنین ای دوستان</p></div>
<div class="b" id="bn34"><div class="m1"><p>در بر مردم نمانده غیرت ناموس و ننگ</p></div>
<div class="m2"><p>چون زنان پوشند مردان جامهای رنگ رنگ</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>امردان بینی تو چون دوشیزگان شوخ و شنگ</p></div>
<div class="m2"><p>دیده مست از خواب غفلت سرگران از چرس و بنگ</p></div></div>
<div class="b2" id="bn36"><p>اینچنین بود است و خواهد شد چنین ای دوستان</p></div>
<div class="b" id="bn37"><div class="m1"><p>ای برادر قتل و تاراج است در پی زینهار</p></div>
<div class="m2"><p>کار گیتی هست یکسر صورت و نقش و نگار</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>می رسد هر دم بگوشم نعره چابکسوار</p></div>
<div class="m2"><p>ساعتی صد رنگ در چشمم نماید روزگار</p></div></div>
<div class="b2" id="bn39"><p>اینچنین بود است و خواهد شد چنین ای دوستان</p></div>
<div class="b" id="bn40"><div class="m1"><p>خلق را بینم که از ره سوی بیراه اندرند</p></div>
<div class="m2"><p>کمترک در حکم و فرمان شهنشاه اندرند</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>ماده وارند این نران با عقل کوتاه اندرند</p></div>
<div class="m2"><p>وز چراغ و چرخ با گردون و با ماه اندرند</p></div></div>
<div class="b2" id="bn42"><p>اینچنین بود است و خواهد شد چنین ای دوستان</p></div>
<div class="b" id="bn43"><div class="m1"><p>کار باطل در جهان از حد و حصر افزون شود</p></div>
<div class="m2"><p>هر سری دنبال میلی از سرا بیرون شود</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>آه و واویلای مظلومان سوی گردون شود</p></div>
<div class="m2"><p>ناقه لیلی روان در خرگه مجنون شود</p></div></div>
<div class="b2" id="bn45"><p>اینچنین بود است و خواهد شد چنین ای دوستان</p></div>
<div class="b" id="bn46"><div class="m1"><p>ناقه لیلی روان در مرغزار آید همی</p></div>
<div class="m2"><p>رخش رستم در کنار جویبار آید همی</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>دلدل و شبدیز خسرو رهسپار آید همی</p></div>
<div class="m2"><p>اسب آهن پای در تک راهوار آید همی</p></div></div>
<div class="b2" id="bn48"><p>اینچنین بود است و خواهد شد چنین ای دوستان</p></div>
<div class="b" id="bn49"><div class="m1"><p>اسب آهن پا که بینی آتشین دارد شکم</p></div>
<div class="m2"><p>میبرد هر لحظه صد فرسنگ ره یا بیش و کم</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>دود از گوشش رود بر چرخ گردون دمبدم</p></div>
<div class="m2"><p>بی صدا چابکسواری تند بردارد قدم</p></div></div>
<div class="b2" id="bn51"><p>اینچنین بود است و خواهد شد چنین ای دوستان</p></div>
<div class="b" id="bn52"><div class="m1"><p>کار مردان اندرین موقع بنامردی رسد</p></div>
<div class="m2"><p>گاه در بازارشان گرمی گهی سردی رسد</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>لاله را زان قوم نیلی پیرهن زردی رسد</p></div>
<div class="m2"><p>کی دوائی دردشان را به ز بیدردی رسد</p></div></div>
<div class="b2" id="bn54"><p>اینچنین بود است و خواهد شد چنین ای دوستان</p></div>
<div class="b" id="bn55"><div class="m1"><p>مشکمویان پادشاهی ماهرویان دولتی</p></div>
<div class="m2"><p>می فروشان اندرونی باده نوشان خلوتی</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>جامه کوتاه و برهنه سر غزال تبتی</p></div>
<div class="m2"><p>رخت سیمین مخطط همچو حور جنتی</p></div></div>
<div class="b2" id="bn57"><p>اینچنین بود است و خواهد شد چنین ای دوستان</p></div>
<div class="b" id="bn58"><div class="m1"><p>بانگ ساز هفت سر آید مدانش سرسری</p></div>
<div class="m2"><p>گوش گردون کر شود ز آوای کوس حیدری</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>گلعذاران گرد بینی با دو زلف عنبری</p></div>
<div class="m2"><p>باغها بی باغبان دردانها بی مشتری</p></div></div>
<div class="b2" id="bn60"><p>اینچنین بوداست و خواهدشد چنین ای دوستان</p></div>
<div class="b" id="bn61"><div class="m1"><p>لشکر قاجار را یغما شود شمشیر و خود</p></div>
<div class="m2"><p>سر بریده تن دریده دیده تر دل پر ز دود</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>منکران را سیل خون جاری ز تن مانند رود</p></div>
<div class="m2"><p>بگسلد از خرقه دستاربندان تار و پود</p></div></div>
<div class="b2" id="bn63"><p>اینچنین بود است و خواهد شد چنین ای دوستان</p></div>
<div class="b" id="bn64"><div class="m1"><p>طاعت مردم در آن هنگام مجبوری شود</p></div>
<div class="m2"><p>سینه بازار و برزن جمله بلوری شود</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>شهر پر ز آیینه چینی و فغفوری شود</p></div>
<div class="m2"><p>روزگار پهلوانی و سلحشوری شود</p></div></div>
<div class="b2" id="bn66"><p>اینچنین بود است و خواهد شد چنین ای دوستان</p></div>