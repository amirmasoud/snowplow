---
title: >-
    شمارهٔ ۱ - قطعه
---
# شمارهٔ ۱ - قطعه

<div class="b" id="bn1"><div class="m1"><p>یک نقطه خط شمار لحیان</p></div>
<div class="m2"><p>انگیس بعکس او همی دان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خط و نقط و دو خط به تقدیر</p></div>
<div class="m2"><p>حمره است و بیاض عکس آن گیر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دو نقطه دو خط برون نصرت</p></div>
<div class="m2"><p>دو خط دو نقط درون نصرت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نقطه خط و نقطه خط برون را</p></div>
<div class="m2"><p>قبض است و مخالفش درون را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دو خط که دو نقطه در دو حدش</p></div>
<div class="m2"><p>شد عقله و اجتماع ضدش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سه نقطه و خط برون عتبه</p></div>
<div class="m2"><p>یک خط سه نقط درون عتبه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دو نقطه خطی و نقطه کوسج</p></div>
<div class="m2"><p>عکسش تو نقی شمر ز مخرج</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شد چار نقط طریق طاعت</p></div>
<div class="m2"><p>ور چار خط است دان جماعت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اشکال رمل اینست و بس</p></div>
<div class="m2"><p>خواهی اگر ترتیب آن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>لحن بقاع اعقفن</p></div>
<div class="m2"><p>بغطج بود نرکیب آن</p></div></div>