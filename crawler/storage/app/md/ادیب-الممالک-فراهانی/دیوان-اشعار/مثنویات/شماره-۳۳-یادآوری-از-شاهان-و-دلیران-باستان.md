---
title: >-
    شمارهٔ ۳۳ - یادآوری از شاهان و دلیران باستان
---
# شمارهٔ ۳۳ - یادآوری از شاهان و دلیران باستان

<div class="b" id="bn1"><div class="m1"><p>کجا شد فریدون زرین کلاه</p></div>
<div class="m2"><p>کجا شد منوچهر گیتی پناه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کجا کیقباد آن یل سرفراز</p></div>
<div class="m2"><p>کجا شاه کاوس دشمن گداز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کجا رفت کیخسرو تاجدار</p></div>
<div class="m2"><p>چه شد شاه گشتاسب و اسفندیار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کجا رفت شاپور و شاه اردشیر</p></div>
<div class="m2"><p>که با دشنه درید پهلوی شیر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کجا رفت بهرام و بهمن کجاست</p></div>
<div class="m2"><p>خداوند ایران و ارمن کجاست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کجا شاه اشکانیان اردوان</p></div>
<div class="m2"><p>ز ساسانیان شاه نوشیروان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کجا آن بزرگان ایران زمین</p></div>
<div class="m2"><p>که فرمانشان رفت تا هند و چین</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کجا پهلوانان دشمن شکار</p></div>
<div class="m2"><p>چو زال و چو نیرم چو سام سوار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو رستم خداوند زابلستان</p></div>
<div class="m2"><p>که بدناوکش چون اجل جانستان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چو گیو و چو بیژن چو گودرز گو</p></div>
<div class="m2"><p>مهان کهن نامداران نو</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دریغا که رفتند یکبارگی</p></div>
<div class="m2"><p>براندند ازین خاکدان بارگی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>یلان قوی پنجه سرفراز</p></div>
<div class="m2"><p>همه رخت بستند و رفتند باز</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گر از تخم آنان یکی داشتیم</p></div>
<div class="m2"><p>به دل تخم شادی همی کاشتیم</p></div></div>