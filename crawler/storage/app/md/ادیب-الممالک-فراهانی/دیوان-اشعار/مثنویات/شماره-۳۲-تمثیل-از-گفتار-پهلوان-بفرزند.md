---
title: >-
    شمارهٔ ۳۲ - تمثیل از گفتار پهلوان بفرزند
---
# شمارهٔ ۳۲ - تمثیل از گفتار پهلوان بفرزند

<div class="b" id="bn1"><div class="m1"><p>چه خوش گفت با پور خود پهلوان</p></div>
<div class="m2"><p>چو دیدش هم آغوش شیر ژیان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که گر زنده شیر نر اندر نبرد</p></div>
<div class="m2"><p>درد بر تنت چرم و نالی ز درد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از آن به که در گورت اندر کفن</p></div>
<div class="m2"><p>درد پنجه و ناخن گورکن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هلا ای دلیران ایران زمین</p></div>
<div class="m2"><p>بنازید چو شیر مست از کمین</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>که دشمن بتاراج ما چیره شد</p></div>
<div class="m2"><p>ز آهنگشان روز ما تیره شد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>برآنند کاین خانه ویران شود</p></div>
<div class="m2"><p>بداندیش دارای ایران شود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به مرز کیان روس باشد رئیس</p></div>
<div class="m2"><p>در ایوان جم پا نهد انگلیس</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بمانیم در چنبر اهرمن</p></div>
<div class="m2"><p>ابا خانه و گنج و فرزند و زن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بگور نیاکانمان در مغاک</p></div>
<div class="m2"><p>فروزند آتش بر آرند خاک</p></div></div>