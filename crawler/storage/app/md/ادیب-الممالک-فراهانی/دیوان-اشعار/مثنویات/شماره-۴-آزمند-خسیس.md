---
title: >-
    شمارهٔ ۴ - آزمند خسیس
---
# شمارهٔ ۴ - آزمند خسیس

<div class="b" id="bn1"><div class="m1"><p>آزمندی هواپرست و خسیس</p></div>
<div class="m2"><p>در دهی بود کدخدا و رئیس</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>داشت مرغی ظریف و زرین بال</p></div>
<div class="m2"><p>تیز پرهمچو شاهباز خیال</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر زمان زاغ شب بچرخ بلند</p></div>
<div class="m2"><p>خایه زر بطشت سیم افکند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرغ او هم در آشیان زمین</p></div>
<div class="m2"><p>هشتی از مهر بیضه زرین</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>وزن آن بیضه از هزار درم</p></div>
<div class="m2"><p>نه فزون آمدی بسنگ و نه کم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آزمند سفیه و ابله خام</p></div>
<div class="m2"><p>یافت زین مایه ثروتی بدوام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر سحرگه ز خواب بر می خواست</p></div>
<div class="m2"><p>بخت دادی نویدش از چپ و راست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون خروس سحر گشودی پر</p></div>
<div class="m2"><p>بود در زیر مرغ بیضه زر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خواجه آن بیضه را باستعجال</p></div>
<div class="m2"><p>برگرفتی ز مرغ زرین بال</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سوی بازار برده می بفروخت</p></div>
<div class="m2"><p>هر چه افزون ز خرج بود اندوخت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>روزی آن آزمند با خود گفت</p></div>
<div class="m2"><p>چند باشم بدین قناعت جفت</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تا بکی زین شکار دست آموز</p></div>
<div class="m2"><p>بستانم وظیفه روز بروز</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>تا بکی آب برکشم از چاه</p></div>
<div class="m2"><p>جست باید بسوی دریا راه</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بیشک این مرغ را بخانه دل</p></div>
<div class="m2"><p>کارگاهی است ما از آن غافل</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بیشک از چینه دان و قلب و جگر</p></div>
<div class="m2"><p>راه دارد بسوی معدن زر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گنجها را بگنجخانه نهد</p></div>
<div class="m2"><p>تخمی از آن در آشیانه نهد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>در دل اندوخته است مایه زر</p></div>
<div class="m2"><p>می فریبد مرا بخایه زر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>باید آن گنج خانه را دریافت</p></div>
<div class="m2"><p>شکمش بر درید و سینه شکافت</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>تا بکان زر درست رسم</p></div>
<div class="m2"><p>سوی انجام از نخست رسم</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>پس دل مرغ را درید از هم</p></div>
<div class="m2"><p>جستجو کرد از اندرون و شکم</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>دید جز رودهای پرخم و پیچ</p></div>
<div class="m2"><p>نیست وز زر خبر ندارد هیچ</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>طمع خام را زدم دامن</p></div>
<div class="m2"><p>آتشش سوخت نان پخته من</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>این مثل با تو گفتم ای فرزند</p></div>
<div class="m2"><p>تا نیندازدت طمع دربند</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>تا نیفتی چو غافلان در راه</p></div>
<div class="m2"><p>بهوای هریسه اندر چاه</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>گر فتادی درون چنبر آز</p></div>
<div class="m2"><p>نرهی زان بروزگار دراز</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>تا توانی بگرد آز مگرد</p></div>
<div class="m2"><p>که سیه روزی آرد آز بمرد</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>دل بزنجیر حرص و آز مبند</p></div>
<div class="m2"><p>ریسمان طمع دراز مبند</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بامید خزانه وهمی</p></div>
<div class="m2"><p>زرت از کف مده ز کج فهمی</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>با کم خود بساز تا ز طمع</p></div>
<div class="m2"><p>نشوی مبتلای سوک و جزع</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>میوه شاخ حریص بی برگی است</p></div>
<div class="m2"><p>اشتها مایه جوانمرگی است</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>مصطفی عز من قنع فرمود</p></div>
<div class="m2"><p>هم چنین ذل من طمع فرمود</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>کز قناعت بزرگوار شوی</p></div>
<div class="m2"><p>وز طمع رو سیاه و خوار شوی</p></div></div>