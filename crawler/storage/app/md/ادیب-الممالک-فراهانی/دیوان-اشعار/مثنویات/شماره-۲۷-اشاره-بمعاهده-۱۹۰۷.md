---
title: >-
    شمارهٔ ۲۷ - اشاره بمعاهده ۱۹۰۷
---
# شمارهٔ ۲۷ - اشاره بمعاهده ۱۹۰۷

<div class="b" id="bn1"><div class="m1"><p>چو پیمان شکن یار همسایه دید</p></div>
<div class="m2"><p>کسی را به او نیست گفت و شنید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز پیمان و عهد کهن دست شست</p></div>
<div class="m2"><p>سوی انگلیس آمد از در نخست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بدو گفت ایرانیان مرده اند</p></div>
<div class="m2"><p>وگر مرده نی سخت افسرده اند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در این خانه یک تن هشیوار نیست</p></div>
<div class="m2"><p>تن زنده و مغز بیدار نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زبونند و شوریده و نانورد</p></div>
<div class="m2"><p>نه ساز سلیح و نه مرد نبرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز دانش تهی مغز و از سیم گنج</p></div>
<div class="m2"><p>کدیور بسوگ است و دهقان برنج</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دو تن را نباشد بهم راستی</p></div>
<div class="m2"><p>رسید آندمی کز خدا خواستی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مهانشان که و کهتران مهترند</p></div>
<div class="m2"><p>همه دشمن خون یکدیگرند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بزرگان آن بوم و ویران همه</p></div>
<div class="m2"><p>هواخواه گرگند و یار رمه</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بما دل سپردند و با دوست روی</p></div>
<div class="m2"><p>نه آزرم جویند و نه آبروی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>رسیده کنون روز نخجیر ما</p></div>
<div class="m2"><p>که دشمن درآید بزنجیر ما</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به فردا منه کار امروز خویش</p></div>
<div class="m2"><p>که فردا بسی کارت آید به پیش</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>درین نغز هنگام ما را نکوست</p></div>
<div class="m2"><p>که با یکدیگر یار باشیم و دوست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بیا تا به هم دوست باشیم و یار</p></div>
<div class="m2"><p>ببندیم پیمان مهر استوار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نگوئیم هیچ از گذشته سخن</p></div>
<div class="m2"><p>نماند بدل کینه های کهن</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بتازیم در تخت گاه کیان</p></div>
<div class="m2"><p>نترسیم از پیل و شیر ژیان</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>که پیلان فتادند یکسر ز کار</p></div>
<div class="m2"><p>شدستند شیران سگان را شکار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>در این بیشه دیگر پی شیر نیست</p></div>
<div class="m2"><p>یلان را تبرزین و شمشیر نیست</p></div></div>