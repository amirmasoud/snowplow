---
title: >-
    شمارهٔ ۵ - دیباچه
---
# شمارهٔ ۵ - دیباچه

<div class="b" id="bn1"><div class="m1"><p>چو دانا ز گنجینه در باز کرد</p></div>
<div class="m2"><p>بنام خدا نامه آغاز کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خدائیکه در مغز هوش آفرید</p></div>
<div class="m2"><p>بتن آدمی با سروش آفرید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>روان را بدانش ستایش نمود</p></div>
<div class="m2"><p>سخن را ترازوی دانش نمود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سپس خامه را با زبان جفت کرد</p></div>
<div class="m2"><p>نی گنگ را داور گفت کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از او یافت و خشور یزدان پرست</p></div>
<div class="m2"><p>کلید در گنج دانش بدست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>محمد چراغ خرد گستران</p></div>
<div class="m2"><p>خداوند و سالار پیغمبران</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>که با نامه آسمانی بخاک</p></div>
<div class="m2"><p>فرود آمد از نزد یزدان پاک</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در آن نامه از راز هر تر و خشک</p></div>
<div class="m2"><p>بپا کند ناف جهان را بمشک</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ایا خواجه از داور هست و بود</p></div>
<div class="m2"><p>بجان تو و خاندانت درود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بر آن پیشکار جوان مرد تو</p></div>
<div class="m2"><p>بر آن دختر نازپرور تو</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بر آن پیشوایان با فرو داد</p></div>
<div class="m2"><p>که دارند از شیر یزدان نژاد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>همه وارث تاج و تخت تواند</p></div>
<div class="m2"><p>همه میوه های درخت تواند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بویژه علی بن موسی که هست</p></div>
<div class="m2"><p>مرا دامن مهرش اندر بدست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>برد آسمان بر زمینش نیاز</p></div>
<div class="m2"><p>کند کعبه در آستانش نماز</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بفروی این نامه را ساختم</p></div>
<div class="m2"><p>بامیدش این نکته پرداختم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>که او در جهان پادشاه من است</p></div>
<div class="m2"><p>ز نیرنگ اختر پناه من است</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چو کردم ز خاکش پر از نافه مغز</p></div>
<div class="m2"><p>همه کار من گشت ستوار و نغز</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بریدم بسی بندهای شگرف</p></div>
<div class="m2"><p>برون آمدم از بن چاه ژرف</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بنام تو ای شاه گردن فراز</p></div>
<div class="m2"><p>نمودم من این پارسی نامه ساز</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>پی آنکه بنیاد آیین و کیش</p></div>
<div class="m2"><p>در او زنده سازم بنیروی خویش</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>کنم تازه آیین شرع کهن</p></div>
<div class="m2"><p>براندازم از مشرکان بیخ و بن</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چنان خواهم از همت راد تو</p></div>
<div class="m2"><p>وز آن داد و بخش خدا دادتو</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>که تا هست گردنده گردون بپای</p></div>
<div class="m2"><p>ز من ماند این نامه اندر بجای</p></div></div>