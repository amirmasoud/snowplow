---
title: >-
    شمارهٔ ۱ - شادروان شاپور
---
# شمارهٔ ۱ - شادروان شاپور

<div class="b" id="bn1"><div class="m1"><p>شبی با گلعذاری مست و مخمور</p></div>
<div class="m2"><p>گذر کردم بشادروان شاپور</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کنار چشمه ای دیدم در آن کاخ</p></div>
<div class="m2"><p>درختی برزده بر آسمان شاخ</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به هر شاخش گلی خوشبوی و خوشرنگ</p></div>
<div class="m2"><p>به هر گل بلبلی در ساز و آهنگ</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>درون چشمه عکس ماه و پروین</p></div>
<div class="m2"><p>پراکنده گهر بر دیبه چین</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همی غلطید عکس مه به هر سو</p></div>
<div class="m2"><p>ز چوگان هوا در آب چون گو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مرا از این تماشا شد دل از دست</p></div>
<div class="m2"><p>ز بانگ مرغ و بوی گل شدم مست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گرفتم دست یار نازنین را</p></div>
<div class="m2"><p>ز روی عجز بوسیدم زمین را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>که در این سایه لختی گسترد رخت</p></div>
<div class="m2"><p>نشنید چون گل اندر زمردین تخت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گهی نو شد قدح، گاهی دهد می</p></div>
<div class="m2"><p>شود او از قدح مست و من از وی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نگارم همچو گل زین گفته بشگفت</p></div>
<div class="m2"><p>تقاضای مرا از دل پذیرفت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به روی آن چمن با هم نشستیم</p></div>
<div class="m2"><p>بزنجیر محبت عهد بستیم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>زدم جامی و دادم ساتگینی</p></div>
<div class="m2"><p>کشیدم ناز حسن نازنینی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>شده هوش از سر و رفته دل از دست</p></div>
<div class="m2"><p>دل از دلدار یغما سر ز می مست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بناگه ناله ای آمد بگوشم</p></div>
<div class="m2"><p>که از سر برد یکسر عقل و هوشم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>تو گفتی خسته ای را دست دشمن</p></div>
<div class="m2"><p>خلاند خار در دل تیر در تن</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>نظر کردم به هر سوی اندران دشت</p></div>
<div class="m2"><p>ندیدم هیچ کس در باغ و گلگشت</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ندانستم که این سوز از کجا بود</p></div>
<div class="m2"><p>برآمد از کدامین آتش این دود</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>شدم آشفته و دیوانه از هول</p></div>
<div class="m2"><p>دمیدم هر زمان بر خویش لاحول</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>دگر بار آمدم آن ناله در گوش</p></div>
<div class="m2"><p>چنان کز خویشتن کردم فراموش</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>نگارم گفت کاین سوز از درخت است</p></div>
<div class="m2"><p>درخت سبز مانا تیره بخت است</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چو این گفت آن پری بر پا ستادم</p></div>
<div class="m2"><p>بر آن آهنگ سوزان گوش دادم</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>یقینم شد از آن لحن شرربار</p></div>
<div class="m2"><p>که آید از درخت آن ناله زار</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بدو گفتم که ای شاخ برومند</p></div>
<div class="m2"><p>مرا آه تو آتش در دل افکند</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بجای آنکه همچون سرو بالی</p></div>
<div class="m2"><p>چرا چون استن حنانه نالی</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>درخت بیزبان چون نخله طور</p></div>
<div class="m2"><p>سخنگو شد بشادروان شاپور</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بگفتا قصه من بس دراز است</p></div>
<div class="m2"><p>یکی بشنو گرت سودای راز است</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>یقین دانم شنیدستی که شاپور</p></div>
<div class="m2"><p>به روم آمد ز ایران از رهی دور</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>به روی مردم آن ملک دربست</p></div>
<div class="m2"><p>پس تسخیر قسطنطین کمر بست</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>به قیصر از هجومش تنگ شد کار</p></div>
<div class="m2"><p>که با آن شه نبودش باب پیکار</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بناگه مرغ زیرک رفت دربند</p></div>
<div class="m2"><p>قضا شاپور را در چنبر افکند</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>ادب را پوست از تن برکشیدند</p></div>
<div class="m2"><p>تن شه را بچرم اندر کشیدند</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>درون شد شاه ما چون مغز در پوست</p></div>
<div class="m2"><p>فرو شد تیر دشمن در دل دوست</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>بایران راند قیصر لشگر خویش</p></div>
<div class="m2"><p>که از دشمن ستاند کیفر خویش</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>بهرجا یافت آبادی در ایران</p></div>
<div class="m2"><p>زد آتش کند از بن کرد ویران</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>ز بن بر کند هر جا بد درختی</p></div>
<div class="m2"><p>بدار آویخت هر جا شوربختی</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>پراکندند مسکینان ز مسکن</p></div>
<div class="m2"><p>زدند آتش کریمان را بخرمن</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>ولی زانجا که در این راه باریک</p></div>
<div class="m2"><p>بود روز ستم کوتاه و تاریک</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>بسی نگذشت کایزد جل شانه</p></div>
<div class="m2"><p>ز دود از چهر ایران رنگ اندوه</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>برون آمد ز چرم گا و شاپور</p></div>
<div class="m2"><p>بایران زد علم پیروز و منصور</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>بخاک رودبار آمد شبانگاه</p></div>
<div class="m2"><p>و زانجا سوی ششتر شد ز بیراه</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>شبیخون زد به لشگرگاه قیصر</p></div>
<div class="m2"><p>تکاور راند در خرگاه قیصر</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>شکارش کرد و بستش دست و بازو</p></div>
<div class="m2"><p>ستم با کیفر آمد هم ترازو</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>سپس امر آمد از دربار شاپور</p></div>
<div class="m2"><p>که معمار آوردند از روم و مزدور</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>ز آب روم و خاک روم گلها</p></div>
<div class="m2"><p>طرازند از پی تعمیر دلها</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>درخت میوه دار از روم آرند</p></div>
<div class="m2"><p>درون گلشن ایران بکارند</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>سراهای کهن از نو طرازند</p></div>
<div class="m2"><p>همه ویرانه ها آباد سازند</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>چنین کردند و روزی چند نگذشت</p></div>
<div class="m2"><p>که هامون باغ شد ویرانه گلگشت</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>هنوز از خاک قسطنطین در آن دشت</p></div>
<div class="m2"><p>یکی تل است در دامان گلگشت</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>که خواندندش حریفان تل رومی</p></div>
<div class="m2"><p>نباشد خاک آن چون خاک بومی</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>مرا رزبان شاپور اندرین بوم</p></div>
<div class="m2"><p>بباغ شهریار آورد از روم</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>نهالی خرد بودم نازک و تر</p></div>
<div class="m2"><p>که گشتم دور از پیوند مادر</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>در این خاک آب خوردم ریشه کردم</p></div>
<div class="m2"><p>ز شاخ خود چمن را بیشه کردم</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>کنون از عمرم اندر روزگاران</p></div>
<div class="m2"><p>گذشته سالها بیش از هزاران</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>بزرگان در پناهم آرمیدند</p></div>
<div class="m2"><p>مهان در سایه قدم خمیدند</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>پری رویان زمینم بوسه دادند</p></div>
<div class="m2"><p>بتان چون سایه در پایم فتادند</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>شهان در سایه پهن و فراخم</p></div>
<div class="m2"><p>ز بند خیمه فرسودند شاخم</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>ولی اکنون دلی دارم مشوش</p></div>
<div class="m2"><p>ز چرخ کج مدار و بخت سرکش</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>اگر چه زاده اندر خاک رومم</p></div>
<div class="m2"><p>هوا پرورده این مرز و بومم</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>بر این خاکی که در وی ریشه دارم</p></div>
<div class="m2"><p>ز جور آسمان اندیشه دارم</p></div></div>