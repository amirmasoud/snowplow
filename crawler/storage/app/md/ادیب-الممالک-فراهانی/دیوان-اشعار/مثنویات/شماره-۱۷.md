---
title: >-
    شمارهٔ ۱۷
---
# شمارهٔ ۱۷

<div class="b" id="bn1"><div class="m1"><p>آن شنیدم که روبهی عیار</p></div>
<div class="m2"><p>با بزی شد درون صحرا یار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روبهک سخت رند و دانا بود</p></div>
<div class="m2"><p>در همه کارها توانا بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گرم و سرد زمانه دیده بسی</p></div>
<div class="m2"><p>تلخ و ترش جهان چشیده بسی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دامها بگسلیده از نیرنگ</p></div>
<div class="m2"><p>پیرهن ها دریده رنگارنگ</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هدف صد هزار تیر شده</p></div>
<div class="m2"><p>کهنه تاریخ چرخ پیر شده</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>میخها کنده سیخها خورده</p></div>
<div class="m2"><p>داستانها بخاطر آورده</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>لیگ بز گول و خوپسندی بود</p></div>
<div class="m2"><p>در خور طنز و ریشخندی بود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ساده و بی خیال و خوش باور</p></div>
<div class="m2"><p>متملق پرست و دون پرور</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بیسبب مات و بی اراده به سیر</p></div>
<div class="m2"><p>آلت پیشرفت مقصد غیر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>می ندیده ز فرط خودبینی</p></div>
<div class="m2"><p>در جهان جز بروی خودبینی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>داشت ریشی دراز و شاخی سخت</p></div>
<div class="m2"><p>ریش چون سبزه شاخ همچو درخت</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>این دو تن بر خلاف عادت انس</p></div>
<div class="m2"><p>انس با هم گرفته چون همجنس</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بی نزاع و جدال و چون و چرا</p></div>
<div class="m2"><p>روبه اندر شکار و بز به چرا</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>راست گفتی که انس روبه وبز</p></div>
<div class="m2"><p>انس آرامی است با پر توز</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>اتفاقا در آفتاب تموز</p></div>
<div class="m2"><p>عطش افکندشان بسوگ و بسوز</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>هر طرف تاختند از پی آب</p></div>
<div class="m2"><p>آب بود اندران زمین نایاب</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بس دویدند تا در آخر کار</p></div>
<div class="m2"><p>چشمه ای یافتند ز آب گوارا</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>راه آن چشمه در مغاکی بود</p></div>
<div class="m2"><p>دره ژرف هولناکی بود</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>گاه رفتن چو بود رو بنشیب</p></div>
<div class="m2"><p>بسهولت شدند و بی آسیب</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>آب خوردند و دست و رو شستند</p></div>
<div class="m2"><p>سر و گردن در آب جو شستند</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چون شکم سیر شد گلو سیرآب</p></div>
<div class="m2"><p>چشمهاشان تهی ز سرمه خواب</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>آن دو یار موافق دمساز</p></div>
<div class="m2"><p>خواستند از نشیب شد بفراز</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>راه پرپیچ بود و درهم و سخت</p></div>
<div class="m2"><p>نه گیاه و نه سبزه و نه درخت</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>شکم از آب گشته همچون مشگ</p></div>
<div class="m2"><p>دل ز خون مال مال و دیده ز اشگ</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>از اشرار تموز تن بگداز</p></div>
<div class="m2"><p>مرغ اندیشه مانده از پرواز</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>دیرگاهی بخود فرو رفتند</p></div>
<div class="m2"><p>هر دو از بخت بد برآشفتند</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>پس دیری مبادلات سخن</p></div>
<div class="m2"><p>گفت روبه بدوستدار کهن</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>حیلتی بهر جستن از این دز</p></div>
<div class="m2"><p>ساز کردم که دیو از آن عاجز</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>گر بهم دست اتفاق دهیم</p></div>
<div class="m2"><p>هر دو از ورطه فنا برهیم</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>ورنه بی گفتگو در این زندان</p></div>
<div class="m2"><p>هر دو باشیم طعمه رندان</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>گفت بز ای حکیم دانشمند</p></div>
<div class="m2"><p>پیش رأی تو سرنهم بکمند</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>خاطرت گر هلاک من جوید</p></div>
<div class="m2"><p>بنده سمعا و طاعتا گوید</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>که خداوند گیتی از کم و بیش</p></div>
<div class="m2"><p>بتو داده است هوش و بر من ریش</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>شود از هوش آب و خاک آباد</p></div>
<div class="m2"><p>ریش پشم است و پشم در خور باد</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>گفت روبه چو خاطرت گرم است</p></div>
<div class="m2"><p>گوش تو سفته گردنت نرم است</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>حل این عقده سهل می بینم</p></div>
<div class="m2"><p>چون تو را یار اهل می بینم</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>باید دیوسان بر این دیوار</p></div>
<div class="m2"><p>شاخ خود را همی زنی ستوار</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>گنبدی سازی از سرین و سرون</p></div>
<div class="m2"><p>رام باشی نه سرکش و نه حرون</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>تا کمین بنده ات شود گستاخ</p></div>
<div class="m2"><p>پا نهد مرترا بشانه و شاخ</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>سوی بالا همی جهد چالاک</p></div>
<div class="m2"><p>زان سپس برکشد ترا ز مغاک</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>پشت کن بر من ای گل خودرو</p></div>
<div class="m2"><p>که مساوی است پشت گل بارو</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>گفت بز شکر دارم از ایزد</p></div>
<div class="m2"><p>که توئی گنج هوش و کان خرد</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>در فراست شدی معلم من</p></div>
<div class="m2"><p>اتقوا من فراسة المؤمن</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>مؤمن از هفت پرده شد آگاه</p></div>
<div class="m2"><p>«انه ینظر بنورالله »</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>یار دانا ز گنج سیم به است</p></div>
<div class="m2"><p>آدمی را خرد ندیم به است</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>مرحبا بک وحلت البرکة</p></div>
<div class="m2"><p>همچو ماهی به در شو از شبکه</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>خیز و پا برفراز شاخم نه</p></div>
<div class="m2"><p>از زمین سوی آسمان برجه</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>این همی گفت و خواست بر سر دست</p></div>
<div class="m2"><p>منجنیقی بچرخ گردون بست</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>رفت روبه ز پشت بز بر شاخ</p></div>
<div class="m2"><p>جست از آن تنگنا به دشت فراخ</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>جفته بر طاق آسمان انداخت</p></div>
<div class="m2"><p>یللی گفت و تللی بنواخت</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>چون رها شد زدام گفت به بز</p></div>
<div class="m2"><p>ای حریف یگانه گر بز</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>رفتم اینک خدا نگهدارت</p></div>
<div class="m2"><p>تا ابد باد فضل حق یارت</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>من رهیدم بسی و حیلت خویش</p></div>
<div class="m2"><p>تو هم البته حیلتی اندیش</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>تا مگر بشکنی بجهد طلسم</p></div>
<div class="m2"><p>همچو جان وارهی ز محبس جسم</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>سعی کن تا بحیلهای شگرف</p></div>
<div class="m2"><p>برهی زین مغاک تیره ژرف</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>بز بیچاره گفت ای «مسیو»</p></div>
<div class="m2"><p>دوست را در بلا منه به گرو</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>هست شرط طریق مهر رفیق</p></div>
<div class="m2"><p>«الرفیق الرفیق ثم طریق »</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>من ترا کرده ام ز بند آزاد</p></div>
<div class="m2"><p>حق شناسی چرا شدت از یاد</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>کفر نعمت مکن که در کفران</p></div>
<div class="m2"><p>نیست امید رحمت و غفران</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>ای رهیده بشاخ و شانه من</p></div>
<div class="m2"><p>بمن خسته شاخ و شانه مزن</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>که بدین زیرکی و بز بازی</p></div>
<div class="m2"><p>نه تومانی نه فخر دین رازی</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>گفت رو به بریش خویش بخند</p></div>
<div class="m2"><p>که مرا دانشم رهاند از بند</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>گر تو داری بهوش خود برهان</p></div>
<div class="m2"><p>خویشتن را از این بلا برهان</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>گفت بز چونکه حق شناس نه ای</p></div>
<div class="m2"><p>دوستان را پی سپاس نه ای</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>رحمتی کن ز حق عوض بستان</p></div>
<div class="m2"><p>گر شنیدی کماتدین تدان</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>که عمل را برابر آید مزد</p></div>
<div class="m2"><p>گنج از پاسبان و رنج از دزد</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>گفت این راست است لیک از من</p></div>
<div class="m2"><p>نکنی شمع آرزو روشن</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>اولا در نهایت افسوس</p></div>
<div class="m2"><p>بایدت بودن از رهی مایوس</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>کوته آمد طناب حیله من</p></div>
<div class="m2"><p>روشنی نیست در فتیله من</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>ثانیا در وزارت جنگل</p></div>
<div class="m2"><p>چند روزی است گشته ام انگل</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>یافتم منصب و محل و مقام</p></div>
<div class="m2"><p>سرفراز آمدم باستخدام</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>اینک آنجا اداره ای دارم</p></div>
<div class="m2"><p>مختصر ماهواره ای دارم</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>گر رسم دیرسوی خدمت خویش</p></div>
<div class="m2"><p>ثبت گردد به دفتر تفتیش</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>گاه اخذ وظیفه نصف حقوق</p></div>
<div class="m2"><p>میرود بهر جرم در صندوق</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>زین سبب زود بایدم رفتن</p></div>
<div class="m2"><p>تا نگردم دچار موج فتن</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>ثالثا وقت بنده می گذرد</p></div>
<div class="m2"><p>بس عزیز است وقت اهل خرد</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>کار امروز چون بفردا رفت</p></div>
<div class="m2"><p>کار فردا ز دست دانا رفت</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>حق نگهدارت ای برادر هان</p></div>
<div class="m2"><p>چاره اندیش و جان خود برهان</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>که چو اینجا بمانی اندر قید</p></div>
<div class="m2"><p>گر نمیری زجوع گردن صید</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>بز سوی آسمان فکند نگاه</p></div>
<div class="m2"><p>گفت ای خالق ستاره و ماه</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>کاش دادی بجای لحیه و شاخ</p></div>
<div class="m2"><p>بنده را عقل پهن و هوش فراخ</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>ای پسر این سخن مگیر بطنز</p></div>
<div class="m2"><p>کت بود بهتر از خزانه و کنز</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>لختی اندیش در سفاهت بز</p></div>
<div class="m2"><p>گاه تقدیم صدر و رد عجز</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>تا بدانی چگونه روبه پیر</p></div>
<div class="m2"><p>کرد او را به دام حیله اسیر</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>پس زیار بد اجتناب کنی</p></div>
<div class="m2"><p>خویشتن را چو زر ناب کنی</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>ریش خود را بدست کس ندهی</p></div>
<div class="m2"><p>دل بیاران بوالهوس ندهی</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>آلت دست مغرضان نشوی</p></div>
<div class="m2"><p>بی تفکر زره برون نروی</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>گر شنیدی کلام من رستی</p></div>
<div class="m2"><p>ورنه در دام مرگ پا بستی</p></div></div>