---
title: >-
    شمارهٔ ۹
---
# شمارهٔ ۹

<div class="b" id="bn1"><div class="m1"><p>ما را چه که باغ لاله دارد</p></div>
<div class="m2"><p>ما را چه که خسته ناله دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ما را چه که گربه می کند تخم</p></div>
<div class="m2"><p>ما را چه که گاو می زند شخم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ما را چه که گوش خر دراز است</p></div>
<div class="m2"><p>ما را چه که چشم گرگ باز است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ما را چه که حمله می کند ببر</p></div>
<div class="m2"><p>ما را چه که قطره بارد از ابر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ما را چه که میش بره دارد</p></div>
<div class="m2"><p>ما را چه که اسب کره دار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ما را چه که بجنگ روس و ژاپن</p></div>
<div class="m2"><p>یا حمله بالن و دراگن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ما در غم خویش ناله داریم</p></div>
<div class="m2"><p>کاندوه هزار ساله داریم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هستیم چو مرغ پر شکسته</p></div>
<div class="m2"><p>از تیر قضا نژند و خسته</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نه جفت و نه آب و دانه داریم</p></div>
<div class="m2"><p>نه لانه و نه آشیانه داریم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ما شکوه زبخت خویش داریم</p></div>
<div class="m2"><p>زاری بدرون ریش داریم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ما پشه دام عنکبوتیم</p></div>
<div class="m2"><p>باد برهوت بر بروتیم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>پی توشه علم و مایه فن</p></div>
<div class="m2"><p>افتاده بگرد بام و برزن</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>پی خاصیت کمال و تقوی</p></div>
<div class="m2"><p>از فضل و هنر کنیم دعوی</p></div></div>