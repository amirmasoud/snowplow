---
title: >-
    شمارهٔ ۱۸ - در ستایش آب شلف معدنی تنکابن
---
# شمارهٔ ۱۸ - در ستایش آب شلف معدنی تنکابن

<div class="b" id="bn1"><div class="m1"><p>آفریننده شفا و مرض</p></div>
<div class="m2"><p>آنکه او جوهر آفرید و عرض</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آدمی را ز خاک پیدا کرد</p></div>
<div class="m2"><p>خاک را محو و مات و شیدا کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خاک از آب و آب از آتش ساخت</p></div>
<div class="m2"><p>بستر خاک را بر آب انداخت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کودکان را چو گاهواره نهاد</p></div>
<div class="m2"><p>بر دل آب و مغز خاره نهاد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کرد پرخوابه شان ز در و گهر</p></div>
<div class="m2"><p>بالش از سیم و خوابگاه از زر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از شرف تخت و از کرامت تاج</p></div>
<div class="m2"><p>از هوا پوشش وز نوا دواج</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ابرشان دایه قهرشان لالا</p></div>
<div class="m2"><p>تا فرازند در چمن بالا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دینشان پیشوا و عقل پزشک</p></div>
<div class="m2"><p>داده جلابشان ز عنبر و مشک</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دردشان را دوا پدید آورد</p></div>
<div class="m2"><p>قفل شان را هنر کلید آورد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>فضلش آنجا که آبیاری کرد</p></div>
<div class="m2"><p>از دل آب چشمه جاری کرد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نحل را در شکم نهاد دو بهر</p></div>
<div class="m2"><p>در یکی زهر و در یکی پادزهر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>صدف و در کشیده از دریا</p></div>
<div class="m2"><p>خزف و لعل کرده از خارا</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>از یکی خاک زر و آهن کرد</p></div>
<div class="m2"><p>وز یکی گوهر و خماهن کرد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>در یکی شاخ خار و خرما ساخت</p></div>
<div class="m2"><p>وز یکی غوره کرد و حلوا ساخت</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>در یکی چشمه ریخت شربت مرگ</p></div>
<div class="m2"><p>وز یکی سبز و خرم آمد برگ</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>هر که ز آب شلف کفی نوشد</p></div>
<div class="m2"><p>گفته من درست بنیوشد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>که خداوند قادر بیچون</p></div>
<div class="m2"><p>گوهر از سنگ چون کشد بیرون</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>سالها در سرای پیروزه</p></div>
<div class="m2"><p>تشنه ماندیم و آب در کوزه</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>شکرلله که باز شاهد بخت</p></div>
<div class="m2"><p>کرد در پیکر از جوانی رخت</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ماه مشکو به کوی ما آمد</p></div>
<div class="m2"><p>آب دولت به جوی ما آمد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چشمه روشنی که خواست خضر</p></div>
<div class="m2"><p>زنده از وی روان اسکندر</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>گر سکندر بشام تیره نیافت</p></div>
<div class="m2"><p>در دل ما بروز روشن تافت</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>سوی آب حیات بردم پی</p></div>
<div class="m2"><p>و من الماء کل شی حی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>خضر را ره بسلسبیل آمد</p></div>
<div class="m2"><p>جام آب بقا سبیل آمد</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>الصبوع الصبوح یا احباب</p></div>
<div class="m2"><p>المدام المدام یا اصحاب</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>تنکابن مگر بهشتستی</p></div>
<div class="m2"><p>که گلشن عنبرین سرشتستی</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>آبش از سلسبیل برده گرو</p></div>
<div class="m2"><p>لاله اش بر مه افکند پرتو</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>باده آنجا چه آبرو دارد</p></div>
<div class="m2"><p>کابرو را چو آب جو دارد</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>زین روان بخش آب روح افزا</p></div>
<div class="m2"><p>عرق آرد بچهره آب بقا</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>گر جم از دور بنگرد جامش</p></div>
<div class="m2"><p>جام گیتی نما نهد نامش</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>هر که از سؤ هضم دارد رنج</p></div>
<div class="m2"><p>یا بنالد ز هیضه و قولنج</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>یا ز سنگی که در مثانه وی</p></div>
<div class="m2"><p>بشکند استخوان شانه وی</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>یا پیچد ز درد گره و پشت</p></div>
<div class="m2"><p>آنچنان کش تو گوئی اینک کشت</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>یا گدازد ز صدمه نقرس</p></div>
<div class="m2"><p>زر هستیش چون در آتش مس</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>چون ازین باده جرعه نوش آمد</p></div>
<div class="m2"><p>کز خم ایزدی بجوش آمد</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>بنگرد فاش داروی همه درد</p></div>
<div class="m2"><p>سرخ سازد ازین قدح رخ زرد</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>ور بشوید درون وی سر و تن</p></div>
<div class="m2"><p>نو شود روزگار مرد کهن</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>رنج پیسی و جوشش پریون</p></div>
<div class="m2"><p>رود از تن چو چربی از صابون</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>پوست نرم آید و بدن فربه</p></div>
<div class="m2"><p>کار هر عضو یک ز دیگر به</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>برده اند این متاع نغز نفیس</p></div>
<div class="m2"><p>از پی امتحان سوی پاریس</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>تا حکیمش بتجزیت پرداخت</p></div>
<div class="m2"><p>جمله املاح آن بنام شناخت</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>هر یکی را گرفت وزن و قیاس</p></div>
<div class="m2"><p>چوالومین، سیلیس و سود و پطاس</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>با تباشیر و آهن و آهک</p></div>
<div class="m2"><p>کلر و سوفر گفتمت یکیک</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>الغرض ز این زلال هستی بخش</p></div>
<div class="m2"><p>که بود رشگ اختران بدرخش</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>تا شود باده مایه رادی</p></div>
<div class="m2"><p>تا بود آب بیخ آبادی</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>باده عیش در سبوها باد</p></div>
<div class="m2"><p>آب شادی روان به جوها باد</p></div></div>