---
title: >-
    شمارهٔ ۴
---
# شمارهٔ ۴

<div class="b" id="bn1"><div class="m1"><p>یک قطره ز آب گرم و یک ذره وفا</p></div>
<div class="m2"><p>در چشم و دلت خدای داناست که نیست</p></div></div>