---
title: >-
    شمارهٔ ۱۱
---
# شمارهٔ ۱۱

<div class="b" id="bn1"><div class="m1"><p>همچو ملخی که شاخساری بخورد</p></div>
<div class="m2"><p>وانگاه ورا به شاخ ساری بخورد</p></div></div>