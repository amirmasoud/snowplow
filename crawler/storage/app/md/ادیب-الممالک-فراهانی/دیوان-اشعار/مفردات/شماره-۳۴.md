---
title: >-
    شمارهٔ ۳۴
---
# شمارهٔ ۳۴

<div class="b" id="bn1"><div class="m1"><p>پیش مرگت شوم ای لعبت چین</p></div>
<div class="m2"><p>که غلامیت به از سلطنت روی زمین</p></div></div>