---
title: >-
    شمارهٔ ۱۹
---
# شمارهٔ ۱۹

<div class="b" id="bn1"><div class="m1"><p>گریه مینا، نگر به چشم ترش بین</p></div>
<div class="m2"><p>ناله بربط شنو به داد دلش رس</p></div></div>