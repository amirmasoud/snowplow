---
title: >-
    شمارهٔ ۴۱
---
# شمارهٔ ۴۱

<div class="b" id="bn1"><div class="m1"><p>یک روز تو را به راه دیدم</p></div>
<div class="m2"><p>هر روز مراست دیده بر راه</p></div></div>