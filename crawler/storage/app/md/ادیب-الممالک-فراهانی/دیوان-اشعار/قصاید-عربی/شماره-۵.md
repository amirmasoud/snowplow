---
title: >-
    شمارهٔ ۵
---
# شمارهٔ ۵

<div class="b" id="bn1"><div class="m1"><p>بزغت شمس الخدود من سموات القدود</p></div>
<div class="m2"><p>و جنینا الورد من و جنه ابکار و خود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رافلات فی ثیاب من حریر و برود</p></div>
<div class="m2"><p>حسبتهاالعین حورالعین فی دارالخلود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>غاده فیهن کالسمط من الدر النضید</p></div>
<div class="m2"><p>قدها غصن به اینع رمان النهود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بابی حورآء اقدیها طریفی و تلیدی</p></div>
<div class="m2"><p>ضربت فی فرعهاالمسک بماورد و عود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>و حکست شاکله الارام فی جفن وجید</p></div>
<div class="m2"><p>و قضیب البان فی حسن قیام و قعود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>فشفینا النفس من تقبیلها رغم الحسود</p></div>
<div class="m2"><p>و سقینا شربه من کفها شرب الیهود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کلما نشرب قلنا یالهاهل من مزید</p></div>
<div class="m2"><p>و شدالطیر علی الاغصان انواع النشید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>من قواف و اعاریض طویل و مدید</p></div>
<div class="m2"><p>و حکمی القمری عن سجع حبیب و ولید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>و حمامات الحمی یروین عن شعر لبید</p></div>
<div class="m2"><p>والثریا شبه عنقود بدت او کعقود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نضدت من لولو فی نحرا تراب عنید</p></div>
<div class="m2"><p>و طلوع القمر الا زهر من افق بعید</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کطلوع الخیر من راحه مولانا الفرید</p></div>
<div class="m2"><p>حط فی اطرازند رحنا فی یوم عید</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>یوم تایید الامام المصطفی عبدالحمید</p></div>
<div class="m2"><p>آیه الرحمن کهف الخلق سالار الجنود</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>و امین الله ذی العزه و الملک السدید</p></div>
<div class="m2"><p>ثامن السبع العلی السامی علی سعدالسعود</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>و ابوالداهیه الدهیاء ذوالطن الشدید</p></div>
<div class="m2"><p>بطشه یفری قلوب الخصم من قبل الجلود</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>لا بسکین حدید و برمح من حدید</p></div>
<div class="m2"><p>هزمن ذکراه روحی یا غداه العید عودی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>فلقد بورکت یا عید مع الفال السعید</p></div>
<div class="m2"><p>یا امیرا سارفی موکبه جیش الوجود</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>و غدا طوع یدیه الناس طارا کالعبید</p></div>
<div class="m2"><p>قد ملکت الخلق بالاحسان من بیض و سود</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>و کذا تستعبد الناس باحسان وجود</p></div>
<div class="m2"><p>خصمک المشئوم امسی کقدار فی ثمود</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>یا مفیدا مستفیدا من نوال و سجود</p></div>
<div class="m2"><p>با بی انت و امی من مفید مستفید</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>عش حمیدا سالما فی شاهق القصر المشید</p></div>
<div class="m2"><p>کل یوم لک من عیش جدید فی جدید</p></div></div>