---
title: >-
    رقعه
---
# رقعه

<div class="b" id="bn1"><div class="m1"><p>ابا جعفر یشتافک السمع والبصر</p></div>
<div class="m2"><p>کفلمئان مشتاق الی الماء والنهر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>و قد تفتقدک العین من بعد فقدها</p></div>
<div class="m2"><p>کما فی اللیالی السود یفتقد القمر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شهدتک فاستغیت عنک من الوری</p></div>
<div class="m2"><p>و من یتبع بعدالمشاهدة الاثر؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رایتک ما فوق الروایات فی العلی</p></div>
<div class="m2"><p>و من یعتمد بعد العیان علی الخبر؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فانت من القوم الذین حسامهم</p></div>
<div class="m2"><p>علا فی رقاب العالمین متی شهر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اذا او قدوا نارالقری فی بیوتهم</p></div>
<div class="m2"><p>لهم جفته تسعی بها المفرد الجزر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>و مهما بنوا بیت الفخار رایتهم</p></div>
<div class="m2"><p>غیوقا و باقی الناس کلهم خضر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>لقد عمروا ایران بعد خرابها</p></div>
<div class="m2"><p>کما عمرت بیت الاله بنو مضر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ایا قادح الزند الذی حد ناره</p></div>
<div class="m2"><p>اعز من ان یلقی به المزح و العشر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>و یا شامخ العرنین لا متکبر</p></div>
<div class="m2"><p>تشین والا صعب تکلفنا الوعر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>یرومک اهل البدو والحضر سائلا</p></div>
<div class="m2"><p>فجدوی یدیک الغیث فی البدو والحضر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>لک الغایه القصوی من الفضل والنهی</p></div>
<div class="m2"><p>و شاوالندی عن حصر نائلک اقتصر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>لئن قلت انت الرکن فی کعبة العلی</p></div>
<div class="m2"><p>صدقت و رب البیت والرکن والحجر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>اراک شقیق الفرقدین و ثالث</p></div>
<div class="m2"><p>المساکین تلوالبدر والا نجم الزهر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بفرع ذکی من غصون کریمه</p></div>
<div class="m2"><p>اذا اهترت اهتزت به النجم و الشجر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ففی یدک الیمنی ریاض من العلی</p></div>
<div class="m2"><p>و فی یدک الیسری حیاض من المطر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>و مقولک النضناض سیف مخذم</p></div>
<div class="m2"><p>فری قلب من یقلیک کالصارم الذکر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>فتنظم فی نظم القوافی لئالیا</p></div>
<div class="m2"><p>و تنشر حین النشر اقلامک الدرر</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>اما تدران البین بدل یومنا</p></div>
<div class="m2"><p>بظلمه لیل لیس فی خلفه سحر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>الم تران الهجر عوض شهدنا</p></div>
<div class="m2"><p>بمر طعوم السلع والصاب والصبر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ففی صدرنا نار و فی عیننا قذی</p></div>
<div class="m2"><p>و فی عیشنا نغض و فی مائنا کدر</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>لنا کبد مقروحه من فراقکم</p></div>
<div class="m2"><p>فیا طول اسفار تعد من السقر</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>سادعو علی البین الممیت من الجوی</p></div>
<div class="m2"><p>کنوح النبی اذ قال یا رب لاتذر</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بلی و اله الراقصات الی منی</p></div>
<div class="m2"><p>یهرولن مشیا فی الرماط و فی الازر</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>لئن ابعد تنی فی الطریق مراحل</p></div>
<div class="m2"><p>خیالک عندی فی المنام و فی السهر</p></div></div>