---
title: >-
    شمارهٔ ۱ - سرود ملی
---
# شمارهٔ ۱ - سرود ملی

<div class="b" id="bn1"><div class="m1"><p>ز راه کرم ای نسیم سحرگه</p></div>
<div class="m2"><p>سوی پارسا گرد بگذر از این ره</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به سیروس از ما بگوی کای شهنشه</p></div>
<div class="m2"><p>چرا گشتی از حال این ملک غافل</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>که گشته چنین خراب و تبه، فتاده ز غم</p></div>
<div class="m2"><p>رعیت شده، بحال پریش و به روز سیه</p></div></div>
<div class="b2" id="bn4"><p>ز برای خدا، ز طریق وفا، بنگر سوی ما</p>
<p>که جهان به ما، شده چون قفس، به گلو رسیده همی نفس</p></div>
<div class="b" id="bn5"><div class="m1"><p>تو بودی که لشگر به قفقاز راندی</p></div>
<div class="m2"><p>و زانجا بشط العرب باز راندی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز ارمینیه سوی اهواز راندی</p></div>
<div class="m2"><p>خراسان و ری و وصل کردی به موصل</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کنون چه شدت که بیخبری، به کشور</p></div>
<div class="m2"><p>خود نمی گذری، به جانب ما نمی نگری</p></div></div>
<div class="b2" id="bn8"><p>ز برای خدای، ز طریق وفا، بنگر سوی ما</p>
<p>که جهان بما شده چون قفس، به گلو رسیده همی نفس</p></div>
<div class="b" id="bn9"><div class="m1"><p>تو با فارس انبار کردی مدی را</p></div>
<div class="m2"><p>گرفتی کریسوس شاه لدی را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نمودی عیان فره ایزدی را</p></div>
<div class="m2"><p>شکستی بهم سقف و دیوار بابل</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سپاه تو کرد، چو عزم سفر، به ساحل روم به دشت خزر</p></div>
<div class="m2"><p>احاطه نمود ز بحر و ز بر</p></div></div>
<div class="b2" id="bn12"><p>ز برای خدای، ز طریق وفا، بنگر سوی ما</p>
<p>که جهان به ما شده چون قفس، به گلو رسیده همی نفس</p></div>
<div class="b" id="bn13"><div class="m1"><p>دریغا که اقلیم سیروس و دارا</p></div>
<div class="m2"><p>فتاده است در بحر غم آشکارا</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>تو ای ناخدا همتی کن خدا را</p></div>
<div class="m2"><p>مگر کشتی ما برد ره به ساحل</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>رسد فرجی ز عالم غیب</p></div>
<div class="m2"><p>چنانکه رسید بصهر شعیب</p></div></div>
<div class="b2" id="bn16"><p>ز برای خدای، ز طریق وفا، بنگر سوی ما</p>
<p>که جهان به ما شده چون قفس، به گلو رسیده همی نفس</p></div>
<div class="b" id="bn17"><div class="m1"><p>چو ویرانه شد ملک کی، کشور جم</p></div>
<div class="m2"><p>ز علم و هنر باید افراشت پرچم</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ز همت کمر ساخت از عدل خاتم</p></div>
<div class="m2"><p>ز تقوی کلاه و ز دانش حمایل</p></div></div>
<div class="b2" id="bn19"><p>ز ساقی علم شراب بنوش، بجهد تمام به علم بکوش، لوای هنر بگیر بدوش</p></div>
<div class="b2" id="bn20"><p>ز برای خدای، ز طریق وفا، بنگر سوی ما</p>
<p>که جهان به ما شده چون قفس، به گلو رسیده همی نفس</p></div>