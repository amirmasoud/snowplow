---
title: >-
    شمارهٔ ۴ - در هجو میرزا بابا طبیب خلخالی
---
# شمارهٔ ۴ - در هجو میرزا بابا طبیب خلخالی

<div class="b" id="bn1"><div class="m1"><p>طبیبی زخلخال آمد بری</p></div>
<div class="m2"><p>کمر بسته وزرد رخ همچو نی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سر و پوز او همچو بوزینه بود</p></div>
<div class="m2"><p>زنخدانش آویزه سینه بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بشخصه رخش همچو صفرای من</p></div>
<div class="m2"><p>بعینه سرش همچو سر نای من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کنون هفت سال است کو خوانده طب</p></div>
<div class="m2"><p>ولی توبه نشناسد از شطر غب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نه از بول بشناسد او نبض را</p></div>
<div class="m2"><p>نه از بسط بشناسد او قبض را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پدر بر پدر عام بوده است لیک</p></div>
<div class="m2"><p>سیادت بخود بسته با ضرب سیک</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>همانا یقین تخمه باب نیست</p></div>
<div class="m2"><p>ز مادرش باید به پرسم که کیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز زردی رنگش هویداستی</p></div>
<div class="m2"><p>که او محترق خلط صفراستی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو گردد رخش از حجاب آشکار</p></div>
<div class="m2"><p>نباید دگر دهن خردع بکار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زبانش چو در قصه گویا شود</p></div>
<div class="m2"><p>برای مریان اپیکا شود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز سودا سرش آنچنان گشته خشک</p></div>
<div class="m2"><p>که سوزد در او خطمی و بیدمشک</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مگر قیل سمع است یرقان او</p></div>
<div class="m2"><p>که بر دوش چرخ است یرقان او</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چو عاشق بگیرد از او حب صبر</p></div>
<div class="m2"><p>ز بی صبری اندر شتابد بقبر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گرش دل به تنگ آید از کار من</p></div>
<div class="m2"><p>بجلدش رود حب ستار من</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ندانم که را داده گلقند خام</p></div>
<div class="m2"><p>که از کونش آمد ورا بار عام</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گمانم بود کرمی از مغز کون</p></div>
<div class="m2"><p>که افیون خوران ساختندش برون</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>مرا سالها فکر بد شام و روز</p></div>
<div class="m2"><p>که آیا چگونه بود شکل کون</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>کنونش بدیدم رخی زرد داشت</p></div>
<div class="m2"><p>قدی سخت نازک دمی سرد داشت</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>نه از حبس بول است رخ زردیش</p></div>
<div class="m2"><p>که در پیش قول است دم سردیش</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بگفتم دوا جستن از وی خطا است</p></div>
<div class="m2"><p>سخنهای سعدی شنیدن رواست</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>طبیبی که او خود بود زرد روی</p></div>
<div class="m2"><p>از او داروی سرخ روئی مجوی</p></div></div>