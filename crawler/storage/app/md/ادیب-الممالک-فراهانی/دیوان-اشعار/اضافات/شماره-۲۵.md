---
title: >-
    شمارهٔ ۲۵
---
# شمارهٔ ۲۵

<div class="b" id="bn1"><div class="m1"><p>طهماسب خداوند راستین</p></div>
<div class="m2"><p>داردیم و کان در دو آستین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دریا ز یسارش برد یسار</p></div>
<div class="m2"><p>گردون به یمینش خورد یمین</p></div></div>