---
title: >-
    شمارهٔ ۱۷ - آیین غسل و روزه حقیقت سه روزه
---
# شمارهٔ ۱۷ - آیین غسل و روزه حقیقت سه روزه

<div class="b" id="bn1"><div class="m1"><p>غسل روزه حقیقت این است</p></div>
<div class="m2"><p>راه دین و طریقت این است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که بگوئی زصدق دل یکبار</p></div>
<div class="m2"><p>جستم از پیر خرقه این اسرار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دیدن یار و ساغر ابرار</p></div>
<div class="m2"><p>نام طاس مقدس رزبار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اولش هست یار و آخر یار</p></div>
<div class="m2"><p>حکم خاوندگار در هر کار</p></div></div>