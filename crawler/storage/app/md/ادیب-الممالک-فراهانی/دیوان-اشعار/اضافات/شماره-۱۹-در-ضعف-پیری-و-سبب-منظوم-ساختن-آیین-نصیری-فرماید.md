---
title: >-
    شمارهٔ ۱۹ - در ضعف پیری و سبب منظوم ساختن آیین نصیری فرماید
---
# شمارهٔ ۱۹ - در ضعف پیری و سبب منظوم ساختن آیین نصیری فرماید

<div class="b" id="bn1"><div class="m1"><p>روزگاری که از طلایه مرگ</p></div>
<div class="m2"><p>شاخ عمر مرا خزان شد برگ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ریخت در جویبار و گلبن خشک</p></div>
<div class="m2"><p>برف و کافور جای سنبل و پشک</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نعمت و ناز رخت بسته زکوی</p></div>
<div class="m2"><p>سر بچوگان تن فتاده چو گوی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گشته در خانقاه گوشه نشین</p></div>
<div class="m2"><p>داده بر باده هوش و دانش و دین</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خوار و بیمار و زار و فرسوده</p></div>
<div class="m2"><p>خون برخساره از جگر سوده</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بسته بر رخ در خروج و دخول</p></div>
<div class="m2"><p>گشته از قیل و قال خلق ملول</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پیک رحلت که پیریش نام است</p></div>
<div class="m2"><p>مرگ را صحبتش سرانجام است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از در آمد مرا بداد نوید</p></div>
<div class="m2"><p>کاندرا سوی بوستان امید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>رخت بربند ازین سرای کهن</p></div>
<div class="m2"><p>جامه نو پوش و خانه را نو کن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>برهان روح را زمحبس تن</p></div>
<div class="m2"><p>وین پری را ز بند اهریمن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>جامه نو کن که شوخکن شد و زشت</p></div>
<div class="m2"><p>خوشه خوشیده شد دروکن کشت</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تا ببینی یکی جهان فراخ</p></div>
<div class="m2"><p>لاله در باغ و میوه اندر شاخ</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>شهد و شیر و شراب و شاهد و شمع</p></div>
<div class="m2"><p>دوستان در کنار و یاران جمع</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>آرزوها بکام و دلها شاد</p></div>
<div class="m2"><p>باغها سبز و خانها آباد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>اندرین دیولاخ تا کی و چند</p></div>
<div class="m2"><p>سر بچنبر درون و دل در بند</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>خلعت جاودان بگیر و بپوش</p></div>
<div class="m2"><p>باده ارغوان بخواه و بنوش</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>تا گشائی بسوی گردون پر</p></div>
<div class="m2"><p>ببری بند و بشکنی چنبر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>عزم ره کن که دیرگاهستی</p></div>
<div class="m2"><p>چشم یاران تو را براهستی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>گفتم ایجان خوشامدی اهلا</p></div>
<div class="m2"><p>من نه آنم که گویمت مهلا</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ز آنکه در این سراچه دلگیرم</p></div>
<div class="m2"><p>پای در بند و تن بزنجیرم</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>مرغ باغم نه جغد ویرانه</p></div>
<div class="m2"><p>یار خویشم نه جفت بیگانه</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>لمعه از تجلی طورم</p></div>
<div class="m2"><p>برقی از نار و شرقی از نورم</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>سوزم اندر فتیله می بسبو</p></div>
<div class="m2"><p>بویم اندر گل آبم اندر جو</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>آفتابم درون آیینه</p></div>
<div class="m2"><p>هوش در مغز و مهر در سینه</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بسته ام در کمند و خسته زدرد</p></div>
<div class="m2"><p>بردم کاش آنکه باز آورد</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>گر ازین بند زودتر بر هم</p></div>
<div class="m2"><p>مژدگانیت جان خویش دهم</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>گفت خواهی ترش نشین یا تلخ</p></div>
<div class="m2"><p>غره ماه زندگی شده سلخ</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>گر بهفتاد رفته یا هفت</p></div>
<div class="m2"><p>همچنان کامدی بباید رفت</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>لیک هستی به نیستی نرود</p></div>
<div class="m2"><p>و آنکه خود نیست هست می نشود</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>هست همواره هست و خواهد بود</p></div>
<div class="m2"><p>نیست آسوده شد زبود و نبود</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>شمع خاموش شد ولیکن نور</p></div>
<div class="m2"><p>هست در جای خود ز چشم تو دور</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>گر در ایوان نور بنشینی</p></div>
<div class="m2"><p>همه انوار گرد خود بینی</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>آب باران بخاک رفت فرو</p></div>
<div class="m2"><p>باز از چشمه شد روانه بجو</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>گر به جیحون شوی چو مرغابی</p></div>
<div class="m2"><p>قطره ای را همه در آن یابی</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>گر بخواهی ز بعد خاموشی</p></div>
<div class="m2"><p>خلعت نطق جاودان پوشی</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>سخنی گو که در رزق ماند</p></div>
<div class="m2"><p>راز حق پیش اهل حق ماند</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>تا بهوش اندری چو مردم مست</p></div>
<div class="m2"><p>ساغری ده گر آیدت از دست</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>تا درین خانه می بری شب و روز</p></div>
<div class="m2"><p>رو چراغی در آن سرای افروز</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>ورنه چون شمع مرد و صهبا ریخت</p></div>
<div class="m2"><p>سقف بگسست و بند خیمه گسیخت</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>بلبل از باغ رفت و گل پژمرد</p></div>
<div class="m2"><p>فرودین رخت از گلستان برد</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>تو بمانی و آه و ناله و دود</p></div>
<div class="m2"><p>نه بجا نام و نه زسودا سود</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>گفتم احسنت آفرین بتو باد</p></div>
<div class="m2"><p>نیکم اندرز کردی ای استاد</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>خواستم کلک و ساختم دفتر</p></div>
<div class="m2"><p>سمن انباشتم بنافه تر</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>بیش یا کم دو ساعت این ابیات</p></div>
<div class="m2"><p>راندم از خامه همچو آب حیات</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>هدیه کردم بیار خواجه راد</p></div>
<div class="m2"><p>آن که شد گوهرش سرشته بداد</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>صاحب قدردان صاحب قدر</p></div>
<div class="m2"><p>بدر انجم مهین سلاله بدر</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>میر درویش کیش حق پرور</p></div>
<div class="m2"><p>فضل را سر کمال را سرور</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>بوستان کرم حدیقه خیر</p></div>
<div class="m2"><p>زاده نصر و منتسب بنصیر</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>گفتم آئین حق پرستی را</p></div>
<div class="m2"><p>نکته نیستی و هستی را</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>تا کنم ارمغان بدرگاهش</p></div>
<div class="m2"><p>چون دل و دیده برخی راهش</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>گرچه این گفته گفتنی نبود</p></div>
<div class="m2"><p>گوهر راز سفتنی نبود</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>اندکی زان شده است هدیه دوست</p></div>
<div class="m2"><p>مابقی مانده همچو مغز بپوست</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>گرچه گفتار حق یکی باشد</p></div>
<div class="m2"><p>علم از این یک اندکی باشد</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>چون توانم عنان خامه گسیخت</p></div>
<div class="m2"><p>یا توانم بکوزه دریا ریخت</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>ای نصیری بپوش عیب مرا</p></div>
<div class="m2"><p>خواجگی کن شهود غیب مرا</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>کارم از جهل گوهر اندر کان</p></div>
<div class="m2"><p>نور بر چرخ و قطره در عمان</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>هدیه ام را زفضل خود بپذیر</p></div>
<div class="m2"><p>ور خطائی برفت خرده مگیر</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>بر تو چون برگشایم این ابواب</p></div>
<div class="m2"><p>که فزونی ز صد هزار کتاب</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>بستم این نوعروس را زیور</p></div>
<div class="m2"><p>روز اردی ز ماه شهریور</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>سالماهش زباستان بشمار</p></div>
<div class="m2"><p>بعد هشتادویک دویست و هزار</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>در سبو کردم این شراب از خم</p></div>
<div class="m2"><p>روز شنبه که بود بیست و دوم</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>از محرم هزار و سیصد و سی</p></div>
<div class="m2"><p>اینت سال هلالی آن شمسی</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>از حساب جمل که رفت سراغ</p></div>
<div class="m2"><p>بود در سال شغل و سال فراغ</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>یار از آغاز و یار در انجام</p></div>
<div class="m2"><p>حکم خاوندگار خیر ختام</p></div></div>