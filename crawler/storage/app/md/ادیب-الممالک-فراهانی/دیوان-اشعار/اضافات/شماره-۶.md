---
title: >-
    شمارهٔ ۶
---
# شمارهٔ ۶

<div class="b" id="bn1"><div class="m1"><p>ما می و کو کنار را کرده فدای یک نظر</p></div>
<div class="m2"><p>بی کمک پیاده ای یا مددسواره ای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر نظر علی بمن درفکند نظاره ای</p></div>
<div class="m2"><p>از پس مرگ بخشدم زندگی دوباره ای</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پیرو دلیل عاشقان آنکه ز نور معرفت</p></div>
<div class="m2"><p>پیر خرد بحضرتش کودک شیرخواره ای</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از سخنی چو انگبین کرده ز موم نرمتر</p></div>
<div class="m2"><p>خاطر منکری که شد سخت چو سنگ خاره ای</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ما همه کودکان او کارگردکان او</p></div>
<div class="m2"><p>در طلب مکان او خسته ز هر کناره ای</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ما پی دفع خواب خوش خواسته از مقام خود</p></div>
<div class="m2"><p>او ز برای خواب ما ساخته گاهواره ای</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جز نظر علی در این بستر آفت و مرض</p></div>
<div class="m2"><p>پیکر دردمند را نیست علاج و چاره ای</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ای نظر علی دلم در ره انتظار تو</p></div>
<div class="m2"><p>هست پی فدا شدن منتظر اشاره ای</p></div></div>