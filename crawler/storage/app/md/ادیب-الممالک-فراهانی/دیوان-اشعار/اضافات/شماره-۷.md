---
title: >-
    شمارهٔ ۷
---
# شمارهٔ ۷

<div class="b" id="bn1"><div class="m1"><p>تاریک شد جهان ز ملال نظرعلی</p></div>
<div class="m2"><p>دریا گریست خون ز خیال نظرعلی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ابر آورد قطیفه ز نهار در کنار</p></div>
<div class="m2"><p>گوید منم حال نظرعلی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مرغ آورد بدیعه شوریده آشکار</p></div>
<div class="m2"><p>کز من شنو جواب و سئوال نظرعلی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کوه از کنار خویش یم خرده ساز کرد</p></div>
<div class="m2"><p>در شستن حرام و حلال نظرعلی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کمتر کسی بزاویه دیدم سپرده جان</p></div>
<div class="m2"><p>محروم از عطا و نوال نظرعلی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>وارسته گشت دامن قدرش ز ما سوی</p></div>
<div class="m2"><p>کو شاخ بود و میوه ظلال نظرعلی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بنگر که شاهدان به کجا قائمند از آنک</p></div>
<div class="m2"><p>کوته شود سخن بکمال نظرعلی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دلدارم از وثاق چو آمد بمهر خوان</p></div>
<div class="m2"><p>بنشست گوشه ای بخیال نظرعلی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ناگه جمال فرخ وی را بدید باز</p></div>
<div class="m2"><p>پیدا از اوصاف و خصال نظرعلی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گفتا حقیقتی بود از سال مرگ او</p></div>
<div class="m2"><p>گفتم کدام گفت جمال نظرعلی</p></div></div>