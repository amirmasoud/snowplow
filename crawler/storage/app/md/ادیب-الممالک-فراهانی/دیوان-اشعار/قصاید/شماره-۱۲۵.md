---
title: >-
    شمارهٔ ۱۲۵
---
# شمارهٔ ۱۲۵

<div class="b" id="bn1"><div class="m1"><p>ز اصل پاک و نژاد بلند و طبع نکو</p></div>
<div class="m2"><p>بدی نزاید چونانکه نیکی از بدخو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هزار مرتبه گر قند را بجوشانی</p></div>
<div class="m2"><p>لطیف گردد و افزون شود حلاوت او</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ولی درخت مغیلان ترنجبین ندهد</p></div>
<div class="m2"><p>گرش چشانی از کوثر آب در مینو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کراگهر نبود خاصیت نمی بخشد</p></div>
<div class="m2"><p>گر آستینش آکنده سازی از لؤلو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نه ماهتاب کند رنگ هندوئی رومی</p></div>
<div class="m2"><p>نه آفتاب کند شکل رومیئی هندو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اگر عجوزی چون شاهدان مشکین خط</p></div>
<div class="m2"><p>بر وی غازه نهد یا که وسمه، بر ابرو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>همی بگوید روی کژ و قد کوژش</p></div>
<div class="m2"><p>کزین دو شاهد عادل طریق صدق مجو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>وگر عروسی رعنا برای مصلحتی</p></div>
<div class="m2"><p>پلاس پوشد و اندر زند نقاب به رو</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بود دو شاهد دانای راستگو او را</p></div>
<div class="m2"><p>نخست راستی قد، دویم خم گیسو</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>پس از شکستن دندان و رنجه کردن کام</p></div>
<div class="m2"><p>شود هویدا کان نقل بود و این پینو</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تن لطیف چه در خزچه در عبا چه گلیم</p></div>
<div class="m2"><p>شراب ناب چه در بط چه در قدح چه سبو</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>من این مقدمه زان چیدمی که این سخنان</p></div>
<div class="m2"><p>نمایم اثبات اندر گه جدل به عدو</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>که شاهزاده فرخ منش امامقلی</p></div>
<div class="m2"><p>بسوی رستاق از شهر اگر نماید رو</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>عجب مدار که شاهین در آشیانه خویش</p></div>
<div class="m2"><p>همی نگردد صیاد کبک یا تیهو</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>از آن بساحل دریا مکان گزیده که هیچ</p></div>
<div class="m2"><p>نهنگ تر نکند کام خویش اندر جو</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بلند مرتبه شهزاده که همت وی</p></div>
<div class="m2"><p>ز ارتفاع بگردون همی زند پهلو</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>نشانده مهرش از آفتاب تکمه زر</p></div>
<div class="m2"><p>از آن سپس که گریبان چرخ کرده رفو</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>خدایگانا گویند کاندر این دریا</p></div>
<div class="m2"><p>جزیره است ترا همچو روضه مینو</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>دران جزیره یکی کوه و اندران کهسار</p></div>
<div class="m2"><p>با من و عیش چرد شیر بیشه با آهو</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>شنیده ام من و باور ندارم این گفتار</p></div>
<div class="m2"><p>مگر کنایه شمارم حدیث این هر دو</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>همی بگویم کهی ز عفو و حلم تراست</p></div>
<div class="m2"><p>محیط گشته بر آن کوه رشحه کف تو</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>تمام جانوران در پناه مرحمتت</p></div>
<div class="m2"><p>همی شوند پرستش گرو ستایش گو</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>شنیده ام که هلاکو مراغه را بگزید</p></div>
<div class="m2"><p>در آن بساخت سرای و عمارت و مشکو</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>کنون مراغه اسبان و استران توشد</p></div>
<div class="m2"><p>مراغه ای که بدی تختگاه هولاکو</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>امیدوارم کاندر زمانه شاد زیئی</p></div>
<div class="m2"><p>ابا صلابت چنگیز و حشمت منکو</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>سر خیامت اندر فراز چرخ فرا</p></div>
<div class="m2"><p>بن سنانت اندر فرود خاک فرو</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>خدای عز و جل دولتت کند جاوید</p></div>
<div class="m2"><p>بحق اشهد ان لا اله الا هو</p></div></div>