---
title: >-
    شمارهٔ ۹۸ - چکامه
---
# شمارهٔ ۹۸ - چکامه

<div class="b" id="bn1"><div class="m1"><p>از عدل خویش قائمه ای ساخت ذوالجلال</p></div>
<div class="m2"><p>قائم اساس عدل بر آن نامش اعتدال</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون کرسی وجود بر آن پایه قائمست</p></div>
<div class="m2"><p>شد ایمن از زوال و فنا ملک لایزال</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>روح ستوده راست بر این پایه اتکاء</p></div>
<div class="m2"><p>عقل خجسته راست بر این پایه اتکال</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بنواخت نفس ملهمه در این ستون سرود</p></div>
<div class="m2"><p>گسترد مطمئنه بر این طاق پر و بال</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شد اعتدال طایر لوامه را جناح</p></div>
<div class="m2"><p>هست اعتدال توسن اماره را عقال</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>«الشیئی ان تجاوز عن حده » سرود</p></div>
<div class="m2"><p>والا حکیم بخرد دانای بیهمال</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یعنی ز اعتدال چو کاری برون فتد</p></div>
<div class="m2"><p>وارون کند اساس و گراید باختلال</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گیتی ز اعتدال منظم کند اساس</p></div>
<div class="m2"><p>هستی ز اعتدال فراهم کند کمال</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از اعتدال روح دمد ساغر شمول</p></div>
<div class="m2"><p>وز اعتدال روح دهد نفخه شمال</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در عالم طبیعت اگر اعتدال نیست</p></div>
<div class="m2"><p>اضداد را بهم نبود فعل و انفعال</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ور اعتدال قابله ممکنات نی</p></div>
<div class="m2"><p>طفل وجود را نه رضاع است و نه فصال</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>«ذومرة » شد رسول ازیرا که می نرست</p></div>
<div class="m2"><p>سروی بباغ حسن چو قدش باعتدال</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>تا اعتدال کم نشود مصطفی شدی</p></div>
<div class="m2"><p>گاهی انیس عایشه گه مونس بلال</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>قد الف اگر نشدی معتدل دگر</p></div>
<div class="m2"><p>کی ساختی ز شکل الف باء و جیم و دال</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گر جذب آفتاب و زمین معتدل نبود</p></div>
<div class="m2"><p>پیدا نمیشد هیچ شب و روز و ماه و سال</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ور معتدل نبود هواگاه فروردین</p></div>
<div class="m2"><p>در باغ گل نرستی و در بوستان نهال</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>تعدیل وزن و گردش خاک از جبال شد</p></div>
<div class="m2"><p>تا بر به یک و تیره کند سیر و انتقال</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>خورشید چو ز خط معدل برون رود</p></div>
<div class="m2"><p>وصفش باصطلاح دلوک است یا زوال</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>عشق ار باعتدال نه یکسوی آن هوس</p></div>
<div class="m2"><p>سوی دگر جنونشد و زشتست هر دو حال</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>عقل ار باعتدال نه حمق است و جربزه</p></div>
<div class="m2"><p>از حمق وزر زاید و از جربزه زوال</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>نور ار باعتدال نتابد شود دو چشم</p></div>
<div class="m2"><p>از تنگی و فراخی محتاج اکتحال</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>«داء الملوک والفقرا» وصف نقرس است</p></div>
<div class="m2"><p>کاین درد مهلک و مرض مزمن عضال</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>شه را رسد ز راحت و درویش را ز رنج</p></div>
<div class="m2"><p>جز این دو کس نیابد ازین درد گوشمال</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>اسراف و بخل هر دو قبیحند و اقتصاد</p></div>
<div class="m2"><p>باشد باتفاق پسندیده از رجال</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>کز اقتصاد مال و شرف باقیند لیک</p></div>
<div class="m2"><p>امساک خصم فخر شد اسراف خصم مال</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>جبن است عار و هست تهور نشان جهل</p></div>
<div class="m2"><p>حد وسط شجاعت مرد است در جدال</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>اضحوکه است الکن و مهذار مسخره</p></div>
<div class="m2"><p>حد وسط فصاحت مرد است در مقال</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بهتر ز عمر چیست در آنهم چو بنگری</p></div>
<div class="m2"><p>شد پیر سالخورده کم از پور خردسال</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>ای دل باعتدال گرا کاعتدال را</p></div>
<div class="m2"><p>شد مذهبی ستوده و شد مشربی زلال</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>مشرب گر اعتدال نه زهر است یا شرنگ</p></div>
<div class="m2"><p>مذهب گر اعتدال نه کفر است یا ظلال</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>ما اعتدالیان مه بدریم و دیگران</p></div>
<div class="m2"><p>در اوج خویش گاه محاقند و گه هلال</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>اندر فلک محرک خیریم چون نجوم</p></div>
<div class="m2"><p>اندر زمین معدل سیریم چون جبال</p></div></div>