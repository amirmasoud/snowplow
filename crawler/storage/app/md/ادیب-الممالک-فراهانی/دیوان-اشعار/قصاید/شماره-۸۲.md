---
title: >-
    شمارهٔ ۸۲
---
# شمارهٔ ۸۲

<div class="b" id="bn1"><div class="m1"><p>به حق تاج فلک سای شاه مهر سریر</p></div>
<div class="m2"><p>به جود حضرت اقدس به اقتدار وزیر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به لطف و مرحمت و جود خان حاکم راد</p></div>
<div class="m2"><p>به بندگان سرایش که در زمانه امیر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>که سوختم ز ستمهای دشمنان دغل</p></div>
<div class="m2"><p>به جان رسیدم از سعی مفسدان شریر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دو دشمن است مرا و ایندو دانشست و نسب</p></div>
<div class="m2"><p>که گشته یاس از ایشان مرا گریبان گیر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>توئی چو چوپان ما همچو گله ایم تمام</p></div>
<div class="m2"><p>سگ تو باشد یارو که هست کلب کبیر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کنون که گرگ ز بیم تو با غنم سازد</p></div>
<div class="m2"><p>بیا برای خدا این سگ از میان برگیر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عجب سگی که به تزویر و روبهی خواهد</p></div>
<div class="m2"><p>نژاد شیر خدا را همی کند نخجیر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>منم ز آل پیمبر که حضرت متعال</p></div>
<div class="m2"><p>به مدح ایشان فرمود آیه تطهیر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مقرری مرا می بری نمی ترسی</p></div>
<div class="m2"><p>مقرریت ببرد خدا بدین تقصیر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>اگر بدانش نازی نباشدت یک جو</p></div>
<div class="m2"><p>خلاف منکه ثناخوانم اعشی است و جریر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>اگر بمال پدر غره ای یهودان را</p></div>
<div class="m2"><p>فزون تر است ز تو روز یورو اکسیر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>وگر بنام پدر فخر میکنی مؤذن</p></div>
<div class="m2"><p>مدیح جد مرا گوید از پی تکبیر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>منم بصدق جگرگوشه رسول خدا</p></div>
<div class="m2"><p>منم سلیل خداوندگار روز غدیر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>منم ز آل پیمبر که حضرت متعال</p></div>
<div class="m2"><p>به مدح ایشان فرمود آیه تطهیر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ز خان حاکم ار می نبودیم حرمت</p></div>
<div class="m2"><p>در این شهور حرام برکشیدمی شمشیر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>فروفکند میت از فراز مسند حکم</p></div>
<div class="m2"><p>چنانکه حضرت پیغمبر از حرم تصویر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ازین خر خرف بی زبان گیج نفهم</p></div>
<div class="m2"><p>خدا گواست که از عمر خویش گشتم سیر</p></div></div>