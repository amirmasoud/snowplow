---
title: >-
    شمارهٔ ۸۸
---
# شمارهٔ ۸۸

<div class="b" id="bn1"><div class="m1"><p>مه من که خورشید گردون غلامش</p></div>
<div class="m2"><p>بگل پای سرو اندرون از خرامش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دو ابروی پیوسته اش با دو عارض</p></div>
<div class="m2"><p>دو ماه نواست و دو بدر تمامش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل از سنگ سازد تن از سیم سازد</p></div>
<div class="m2"><p>که سنگ رخام است در سیم خامش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کسی که ز لعلش چشد آب حیوان</p></div>
<div class="m2"><p>اگر درکشد باده بادا حرامش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پری را نبود این اطاعت همانا</p></div>
<div class="m2"><p>فرشته است یا خود فرشته است مامش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کسی کو فتد دور از آن روی و گیسو</p></div>
<div class="m2"><p>نه پیداست روزش نه پیداست شامش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کند مشک سائی نسیم سحرگه</p></div>
<div class="m2"><p>چو ساید بر آن طره مشک فامش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شگفتم بسی زان سرین شد که گوئی</p></div>
<div class="m2"><p>همی در قعود آورد از قیامش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مرا کرده چون دال کوژ و دژم قد</p></div>
<div class="m2"><p>الف قدی از زلفکان چو لامش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مرا آن پی هر چه دشنام گوید</p></div>
<div class="m2"><p>ببوسی از آن لب کشم انتقامش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>وگر سرکشی سازد این بت نمایم</p></div>
<div class="m2"><p>باقبال میر جوان بخت رامش</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>خداوند نام آوران کز بزرگی</p></div>
<div class="m2"><p>بگردون درافکند آوازه نامش</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چمن شاد و خرم ز خوی لطیفش</p></div>
<div class="m2"><p>فلک مست و سرخوش ز انعام عامش</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>تبارش بزرگ و نژادش خجسته</p></div>
<div class="m2"><p>ستوده عصام است و محکم عطامش</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بهر کار یزدانش یارست ازیرا</p></div>
<div class="m2"><p>بهر کار باشد بحق اعتصامش</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>کمیتش چو سر برکند از صطبلش</p></div>
<div class="m2"><p>حسامش چو دم برکشد از نیامش</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ببرد همی از پس غرم سمش</p></div>
<div class="m2"><p>بدرد همی بر تن ببر خامش</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>خروشنده رعدی است گوئی کمیتش</p></div>
<div class="m2"><p>درخشنده برقی است گوئی حسامش</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>زمام فلک گر نبودی بدستش</p></div>
<div class="m2"><p>یکی بختئی بد گسسته زمامش</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>نگویم که تیر است تنها دبیرش</p></div>
<div class="m2"><p>که کیوان پیر است هندوی بامش</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>سپهر ای بسا دیه نام آوران را</p></div>
<div class="m2"><p>درین گردش دوره صبح و شامش</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ولیکن نبوده است چون میر اعظم</p></div>
<div class="m2"><p>نه بهرام گورش نه دستان سامش</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>کجا مهر تابش کند جز بخاکش</p></div>
<div class="m2"><p>کجا چرخ گردش کند جز بکامش</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چو دولت فراهم شد از اقتدارش</p></div>
<div class="m2"><p>چو ملکت منظم شد از اهتمامش</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>شهنشه فرستاد تشریفی از نو</p></div>
<div class="m2"><p>که پوشد به پیکر امیر نظامش</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>خداوند تشریف را پیشرو شد</p></div>
<div class="m2"><p>بسر هشت و شایسته دید احترامش</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>یکی جشنی آراست فرخ که میران</p></div>
<div class="m2"><p>ستادند یکبارگی در سلامش</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بپیروزی آن را بپوشید در تن</p></div>
<div class="m2"><p>که شهدی فزون ریخت گردون بجامش</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>همایون و خوش باد تشریف سلطان</p></div>
<div class="m2"><p>بر اندام سالار با احتشامش</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>امیرا «امیری » که بگزیده استی</p></div>
<div class="m2"><p>ز اولاد و احفاد قایم مقامش</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>امیری بنام تو دارد تخلص</p></div>
<div class="m2"><p>ازین نام دارد فلک نیکنامش</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>امیر است ملک هنر را ولیکن</p></div>
<div class="m2"><p>بدرگاه میر است کهتر غلامش</p></div></div>