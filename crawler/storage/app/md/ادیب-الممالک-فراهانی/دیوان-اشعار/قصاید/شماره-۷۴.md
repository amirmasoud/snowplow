---
title: >-
    شمارهٔ ۷۴
---
# شمارهٔ ۷۴

<div class="b" id="bn1"><div class="m1"><p>کشور خاور شده است خسته و بیمار</p></div>
<div class="m2"><p>خیز و برایش یکی طبیب بدست آر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>باختر او را چو و سنی است بتحقیق</p></div>
<div class="m2"><p>دارد با او همی رقابت بسیار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>وسنی خواهد عدوی خویش کند پست</p></div>
<div class="m2"><p>وسنی خواهد رقیب خویش کند خوار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تیغ عداوت کشد نهفته و پیدا</p></div>
<div class="m2"><p>تیر شماتت زند نهان و پدیدار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>درد و دریغا که این عروس جوانبخت</p></div>
<div class="m2"><p>آه و فسوسا که این پریرخ دلدار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خسته چنان از هجوم نکبت و ذلت</p></div>
<div class="m2"><p>بسته چنان در کمند محنت و آزار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کش نرهاند بجز عنایت داور</p></div>
<div class="m2"><p>کش نجهاند بجز توجه دادار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ساحت مشرق شده ضمیمه مغرب</p></div>
<div class="m2"><p>کشور اسلام گشته سخره کفار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دین خدا خوار گشت و مرد خدا ماند</p></div>
<div class="m2"><p>خوار و زبون از جفای مردم خونخوار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کار گذشته است از علاج و مداوا</p></div>
<div class="m2"><p>عافیت آن سو فتاده از بر بیمار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دین خدا را کجا نشانه توان یافت</p></div>
<div class="m2"><p>شرع نبی از کجا بیابی آثار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>از لب رامشگران بخلوت رندان</p></div>
<div class="m2"><p>یا دم خنیاگران، بدکه خمار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>از نظر آهوان شوخ رمیده</p></div>
<div class="m2"><p>یا نگه لعبتان نغز پریوار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>از حرکات منافقان ریائی</p></div>
<div class="m2"><p>یا کلمات مرائیان رباخوار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>یا زکلامی که کرده شعرفروشان</p></div>
<div class="m2"><p>بهر تملق طراز دفتر و طومار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>یا ز سرودی که مطربان بسرایند</p></div>
<div class="m2"><p>نزد امیران بلحن بربط و مزمار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>یا ز عتابی که خواجگان بغلامان</p></div>
<div class="m2"><p>ساز کنند از طریق نخوت و پندار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>یا ز در مرد جاهلی که فروشد</p></div>
<div class="m2"><p>دین خدا را همی بدرهم و دینار</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>یا ز بر رند فاسقی که بپوشد</p></div>
<div class="m2"><p>روی ریا را همی بخرقه و دستار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>یا ز متاع فرنگ کز اثر وی</p></div>
<div class="m2"><p>گشته تهی خانها و پر شده بازار</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>یا ز دروغی که با هزار قسم جفت</p></div>
<div class="m2"><p>از پی فلسی کنند نزد خریدار</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>یا ز لباسی که شد مخرب پیکر</p></div>
<div class="m2"><p>یا ز اساسی که شد مهیج پیکار</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>همتی ای حارسان ملت بیضا</p></div>
<div class="m2"><p>غیرتی ای وارثان حیدر کرار</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ای علمای بزرگوار هنرمند</p></div>
<div class="m2"><p>ای فضلای خدا پرست نکوکار</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بهر خدا فکرتی بداروی این درد</p></div>
<div class="m2"><p>بهر خدا همتی بچاره این کار</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>خود نه شمائید راه ما بسوی حق؟</p></div>
<div class="m2"><p>خود نه شمائید ماه ما بشب تار؟</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>گر نشتابید سوی چاره چه گوئید</p></div>
<div class="m2"><p>روز قیامت جواب احمد مختار</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>اسلام اینک غریب مانده و مهجور</p></div>
<div class="m2"><p>ایمان اینک نژند مانده و افکار</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>گشت مشوه جمال دین پیمبر</p></div>
<div class="m2"><p>گشته مشوش خیال مردم دیندار</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>آینه شرع را نشسته برخ زنگ</p></div>
<div class="m2"><p>صارم دین را بچهره بر شده زنگار</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>خاک بریتانیا بهند رسیده است</p></div>
<div class="m2"><p>مملکت روس در گذشته ز تاتار</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>برمه و چین و سیام گشه مسخر</p></div>
<div class="m2"><p>کاپ و اورنز و بوئر شده است نگونسار</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>عهد مسیح است و روز ملت ترسا</p></div>
<div class="m2"><p>دور صلیب است و وقت بستن زنار</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>نور چلیپا دمد چو طلعت خورشید</p></div>
<div class="m2"><p>طاق کلیسا رسد بگنبد دوار</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>تیره از آن طاق گشته یکسره دلها</p></div>
<div class="m2"><p>خیره در آن نور مانده یکسره ابصار</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>چند شود مختفی دقایق احکام</p></div>
<div class="m2"><p>چند بود منطوی حقایق اخبار</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>رسم مدارس کنید و نشر جراید</p></div>
<div class="m2"><p>سوی معارف روید و در پی آثار</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>جام تدین شده است ممتلی از زهر</p></div>
<div class="m2"><p>باغ تمدن شده است یکسره پرخار</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>زهر جفا را تهی کنید ز ساغر</p></div>
<div class="m2"><p>خار ستم را برونکشید ز گلزار</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>گرگ ستمکار رفته بر سر گله</p></div>
<div class="m2"><p>موش غله خوار خفته در بن انبار</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>در تله بندید پای موش دغل باز</p></div>
<div class="m2"><p>وز گله برید دست گرگ ستمکار</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>ما همه سرمست و دشمنان همه باهوش</p></div>
<div class="m2"><p>ما همه در خواب و حاسدان همه بیدار</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>ما همه مدهوش و سست و تنبل و کاهل</p></div>
<div class="m2"><p>دشمن هشیار و چست و چابک و عیار</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>رخنه بدیوار ما فکنده بداندیش</p></div>
<div class="m2"><p>ما نگران بر رخش چو صورت دیوار</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>شکر خدا را که شهریار جوان بخت</p></div>
<div class="m2"><p>حمد خدا را که پادشاه جهاندار</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>قلب منیرش بود سپهر حقایق</p></div>
<div class="m2"><p>خاطر پاکش بود خزانه اسرار</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>گشته خیالش بکار ملت مصروف</p></div>
<div class="m2"><p>هست درونش ز راز بملک خبردار</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>هیچ نترسم از آنک مسکن ما را</p></div>
<div class="m2"><p>خانه ماران کنند مردم سحار</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>زانکه بتأیید حق سنان شهنشه</p></div>
<div class="m2"><p>گردد چون اژدها و بشکند آن مار</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>یارب این شه نگاهدار زمانه است</p></div>
<div class="m2"><p>نیز توأش از بد زمانه نگهدار</p></div></div>