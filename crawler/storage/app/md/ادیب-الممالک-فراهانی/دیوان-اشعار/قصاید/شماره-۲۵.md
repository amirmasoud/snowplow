---
title: >-
    شمارهٔ ۲۵
---
# شمارهٔ ۲۵

<div class="b" id="bn1"><div class="m1"><p>امروز که حقرا پی مشروطه قیام است</p></div>
<div class="m2"><p>بر شاه محمدعلی از عدل پیام است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کای شه به زمینت زند، این توسن دولت</p></div>
<div class="m2"><p>کامروز بزیر تو روان گشته و رام است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>این طبل زدن زیر گلیمت نکند سود</p></div>
<div class="m2"><p>چون طشت تو بشکسته و افتاده ز بام است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نام تو بیالوده تواریخ شهان را</p></div>
<div class="m2"><p>هرچند که نت ننگ و نه ناموس و نه نام است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا کی بدهان قفل خموشی زده باشم</p></div>
<div class="m2"><p>جان در هیجانست و گه کشف لئام است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>والا پدرت داد همی کرد و تو بیداد</p></div>
<div class="m2"><p>اینجا گنه و جرم تو بر گردن مام است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جائی که نماند اثر از داد مپندار</p></div>
<div class="m2"><p>بر مایه بیداد و ستم هیچ دوام است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کار تو تمام است و ندانی که از آن روز</p></div>
<div class="m2"><p>شاهی تو و دولت و ملک تو تمام است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>لعنت بچنین صدر که دایم ز پی آن</p></div>
<div class="m2"><p>گه اعظم و گه سلطنت و گاه انام است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هشدار که صیاد قضا، می نشناسد</p></div>
<div class="m2"><p>دستور که و شه که و شهزاده کدام است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آن باده که در جام کسان ریختی ای شاه</p></div>
<div class="m2"><p>ساقیت بر افشانده سرانجام به جام است</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>و آن زهر که در کام جهان کرده ای از قهر</p></div>
<div class="m2"><p>دور ملکت ریخته ناکام بکام است</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>وان شعله که از توپ تو افتاد بمجلس</p></div>
<div class="m2"><p>زودا که برافروخته أت در بخیام است</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گفتار مرا یافه مپندار که از صدق</p></div>
<div class="m2"><p>گفتار من ای شاه چو گفتار جذامست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>این نکبت و ذلت که فراز آمده اینک</p></div>
<div class="m2"><p>در پایه تخت تو ز ادبار پیام است</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>زاغان چو ابابیل برآیند ز بالا</p></div>
<div class="m2"><p>تو ابرهه و معبد ما بیت حرام است</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>یاران تو حجاج و حصین بن نمیرند</p></div>
<div class="m2"><p>و آن مرد مرادی که هواخواه قطامست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>از زخم تو خون در جگر شیر خدا شد</p></div>
<div class="m2"><p>وز تیر تو آذر بدل خیر انام است</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>اخگر زدم توپ تو در مسجد و مجلس</p></div>
<div class="m2"><p>فریاد ز بیداد تو در رکن و مقام است</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>روز عقلا از ستم و جور تو تار است</p></div>
<div class="m2"><p>صبح سعدا از طمع و حرص تو شام است</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>از مال فقیرانت در گنج زر و سیم</p></div>
<div class="m2"><p>وز خون شهیدانت در جام مدام است</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>در جامکی و راتبه فرمان تو مخصوص</p></div>
<div class="m2"><p>در کشتن و بردار زدن حکم تو عام است</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>سی روز اگر روزه بود فرض در اسلام</p></div>
<div class="m2"><p>روز و شب ما از تو چون ایام صیام است</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>فرزند نبی را کشی آنگاه نشینی</p></div>
<div class="m2"><p>بر تخت که عید نبی و روز سلام است</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>سرباز تو در شهر بغارت شده مشغول</p></div>
<div class="m2"><p>سرهنگ تو پندارد کاین شرط نظام است</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>اندر پی زخمی که زدی بر دل ابرار</p></div>
<div class="m2"><p>شمشیر خدا را رگ جان تو نیام است</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>هی هی جبلی قم قم و قم قم که ازین فتح</p></div>
<div class="m2"><p>شاهی بتو ختم آمد و دولت بختام است</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>گویند که اندر پی وام است شهنشه</p></div>
<div class="m2"><p>ماننده این قصه تو دانی که کدام است</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>ترکی که ز گرمابه برون آمده سرخوش</p></div>
<div class="m2"><p>مست است و برهنه تن اندر پی وام است</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>گر وام ستاند ز کس این ترک بناچار</p></div>
<div class="m2"><p>بر خواجه بازرگان عبد است و غلام است</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>تنخواهی و وامی که ز بیگانه ستانی</p></div>
<div class="m2"><p>تنخواه نه جانکاه بود وام نه دام است</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>در گردن شیر نر وام است چو زنجیر</p></div>
<div class="m2"><p>و اندر دهن مار سیه وام لگام است</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>هشیار شو ای شاه که این دولت دنیا</p></div>
<div class="m2"><p>چون کبک بپرواز و چو آهو بخرام است</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>از تخت تو تا تخته تابوت دو انگشت</p></div>
<div class="m2"><p>وز کاخ تو تا خاک مذلت دو سه گام است</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>دیگ طمع و حرصت ازین آتش بیداد</p></div>
<div class="m2"><p>پخته نشود هیچ که سودای تو خام است</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>نه عهد تو عهد و نه یمین تو یمین است</p></div>
<div class="m2"><p>نه قول تو قول و نه کلام تو کلام است</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>از خلف یمین گشت مسلم که در اسلام</p></div>
<div class="m2"><p>خون تو حلال است و نژاد تو حرام است</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>اطوار تو آثار جنون است وسفاه است</p></div>
<div class="m2"><p>افکار تو پندار صداع است و زکام است</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>این تاجوری نیست که در دست و دریغست</p></div>
<div class="m2"><p>این پادشهی نیست که مرگ است و جذام است</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>این افسر و اورنگ کیان است مپندار</p></div>
<div class="m2"><p>کز بهر تو میراث ز اجداد کرام است</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>ارث پدرت زنگ و جهاز شتران بود</p></div>
<div class="m2"><p>نه تاج و نه اورنگ و نه اسب و نه ستام است</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>ای کودک از این بستان بگذر که گذشته است</p></div>
<div class="m2"><p>ایام رضاع تو و هنگام فطام است</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>وی دزد ازین خانه بدرشو که خداوند</p></div>
<div class="m2"><p>بیدار و نگهبان سرا بر سر بام است</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>از ناوک او گر رهی از ناله مظلوم</p></div>
<div class="m2"><p>زنهار نیابی که جگر دوز سهام است</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>بگذار سنانرا که دم تیغ تو کند است</p></div>
<div class="m2"><p>بسپار عنان را که سمند تو جمام است</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>از تخت فرود آی و بنه تاج و فرو خسب</p></div>
<div class="m2"><p>با آنکه پس از میم یکی جیم و دولامست</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>بنگر بسوی نور مساوات که ستار</p></div>
<div class="m2"><p>زد چاک بر آن پرده که سرپوش ظلام است</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>زاد بار باقبال تو آن شد بصفاهان</p></div>
<div class="m2"><p>کش خون دل و دیده شرابست و طعام است</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>صمصام بفرق تو و ضرغام بقصدت</p></div>
<div class="m2"><p>آن صارم برنده و این شیر کنام است</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>از کشتن سردار یقین کن که ازین پس</p></div>
<div class="m2"><p>قاطع بمیان تو و این قوم حسام است</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>این صیحه حق است نه فریاد خلایق</p></div>
<div class="m2"><p>سودای خواص است نه غوغای عوام است</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>این خاک پر از خون ملوک است و سلاطین</p></div>
<div class="m2"><p>ایندشت همه گور صدور است و عظام است</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>دشتی که بهر دستی از آن خون سیاوش</p></div>
<div class="m2"><p>آمیخته با مغز جگرگوشه سام است</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>اکنون همه مأوای سباعست و وحوش است</p></div>
<div class="m2"><p>اینک همه بنگاه هوام است و سوام است</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>باغ ارم آرامگه دیو و شیاطین</p></div>
<div class="m2"><p>فردوس چراگاه گروهی دد و دام است</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>تا چند بفرمان لیاخوف درین شهر</p></div>
<div class="m2"><p>بام و در ما سخره مشتی زلئام است</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>سیلی خور سیلا خوریانیم و چو نالیم</p></div>
<div class="m2"><p>در گوش تو داد دل ما سجع حمام است</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>ما بر مثل آل محمد شده مقهور</p></div>
<div class="m2"><p>تو همچو یزیدستی و این شهر چو شامست</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>سالار سپاه تو امیری است بهادر</p></div>
<div class="m2"><p>کش جای خرد پشک خر اندر بمشامست</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>سعدی که زبن سعد دو صد پایه شقی تر</p></div>
<div class="m2"><p>در خارجه از حکم تو دستور مهام است</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>این هر دو بکام دل خودکار گذارند</p></div>
<div class="m2"><p>بیچاره تو پنداری گردونت بکام است</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>با نظم تر از ملک تو داهومه و سودان</p></div>
<div class="m2"><p>با عقل تر از شخص تو سلطان سیام است</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>از تو دل این خلق رمیده است ولیکن</p></div>
<div class="m2"><p>شاهان جهان را بدل خلق مقام است</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>این تخم عزازیل که از مادر خاقان</p></div>
<div class="m2"><p>روئیده درین ملک بهر برزن و بام است</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>یارب عجبستم که چرا مانده مگر خود</p></div>
<div class="m2"><p>سرسام و جنون در سر ذریه سام است</p></div></div>