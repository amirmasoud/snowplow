---
title: >-
    شمارهٔ ۱۰۶ - چکامه
---
# شمارهٔ ۱۰۶ - چکامه

<div class="b" id="bn1"><div class="m1"><p>مرد چو باشد بوقت کار هراسان</p></div>
<div class="m2"><p>مشکل گردد و را بدیده هر آسان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عزم درست و دل قویت چو باشد</p></div>
<div class="m2"><p>کوه توانی همی بسفت به پیکان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>باید دل ساخت ز آهنی که نگردد</p></div>
<div class="m2"><p>دستخوش امتحان و آژده سوهان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مشت چو سندان اگر نداری هرگز</p></div>
<div class="m2"><p>می نتوانی نواخت مشت بسندان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شیر خدا را شراب خون عدو شد</p></div>
<div class="m2"><p>کاسه سر خصم و تیغ و خنجر ریحان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا چو خضر نسپری مسالک ظلمت</p></div>
<div class="m2"><p>ره نبری در کنار چشمه حیوان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>صاحب لامیه العجم نشنیدید</p></div>
<div class="m2"><p>فخر نماید که لا اخل بغزلان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هر که در ایوان فشرد حلق صراحی</p></div>
<div class="m2"><p>پای نتاند فشرد در صف میدان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نادان خود را همی فکنده بگوری</p></div>
<div class="m2"><p>در چه ویل از هوای چاه ز نخدان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>باید دل را نمود گونه دریا</p></div>
<div class="m2"><p>ساخت تن از ابرو کرد دانش باران</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بی هنر از بخت ناله دارد چونان</p></div>
<div class="m2"><p>کس گره دست برگشود بدندان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بخت کدام است و چرخ کیست قضاچه</p></div>
<div class="m2"><p>باید در کار دست و پا و دل و جان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>دلکش و هشیار و نغز باید گفتار</p></div>
<div class="m2"><p>محکم و ستوار و سخت باید پیمان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بخت اگر کاردان و کارکن آمد</p></div>
<div class="m2"><p>خارج گشتی اصول خلق ز میزان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>آب روان همچو کوه کردی پیکر</p></div>
<div class="m2"><p>کوه گران همچو آب کردی ستخوان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>اینکه بشهنامه گفت خواجه طوسی</p></div>
<div class="m2"><p>بیژن را بخت چیره کرد بهومان</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>فی المثل ار دانیش مطابق واقع</p></div>
<div class="m2"><p>نادره کاری فتاده است بدوران</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گوهر دانش ترا چو باشد در تن</p></div>
<div class="m2"><p>جیب توان پرکنی ز گوهر و مرجان</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>سیرت انسان همی بباید ازیراک</p></div>
<div class="m2"><p>مهر گیانیز شد بصورت انسان</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>تا بجهان نام نیک مانی برجای</p></div>
<div class="m2"><p>بر سر گردون سمند همت بجهان</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ننگ بیفکن تن از هلاک میندیش</p></div>
<div class="m2"><p>نام طلب کن دل از زوال مترسان</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>سخت همی کوش در مقابل دشمن</p></div>
<div class="m2"><p>تند همی جوش در مقابل فرسان</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>التونتاش آن امیر خطه خوارزم</p></div>
<div class="m2"><p>چون ز مصاف علی تکین شد نالان</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>تا نفس آخرین که دست ز جان شست</p></div>
<div class="m2"><p>پای جلادت برون نهشت ز میدان</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>داشت بهنگام نزع گونه عبهر</p></div>
<div class="m2"><p>آن رخ رنگین که چون شقایق نعمان</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>احمد عبدالصمد ستاده ببالینش</p></div>
<div class="m2"><p>گریه کنان بود همچو ابر به نیسان</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>دید چو خوارزم شه گریستنش را</p></div>
<div class="m2"><p>گفت بمن بر مدار ناله و افغان</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>مرگ مرا کی ز گریه یابی چاره</p></div>
<div class="m2"><p>درد مرا کی ز ناله تانی درمان</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>من سر و سامان زندگی دهم از دست</p></div>
<div class="m2"><p>تو بسپاه و ملک ده سر و سامان</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>تا نشناسد عدو که خصمی چون من</p></div>
<div class="m2"><p>داده در آماجگاه ناوک اوجان</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>می بنخواهم بیان قصه و تاریخ</p></div>
<div class="m2"><p>ورنه حکایات نغز کردم عنوان</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>غصه مجنون و خوبروئی لیلی</p></div>
<div class="m2"><p>قصه ابسال و روزگار سلامان</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>ترجمه داستان خضر و سکندر</p></div>
<div class="m2"><p>عاقبت فتنهای دیو و سلیمان</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>تا بنیوشند خلق و عبرت گیرند</p></div>
<div class="m2"><p>از سیر مردمان و کار بزرگان</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>مهمان باشیم این دو روز و بناچار</p></div>
<div class="m2"><p>دیر و یا زود رفت باید مهمان</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>ای خنک انرا که نام نیک گذارد</p></div>
<div class="m2"><p>باقی و جاوید در صحیفه کیهان</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>همچو امیری که از مدیح خداوند</p></div>
<div class="m2"><p>فخر کند تا ابد به اعشی و حسان</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>خواجه افخم خدایگان معظم</p></div>
<div class="m2"><p>کشتی دانش محیط حکمت و عرفان</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>میر مهین آسمان رفعت و اقبال</p></div>
<div class="m2"><p>قطب یقین آفتاب کشور ایقان</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>حضرت اعظم مهین امیر نظام آنک</p></div>
<div class="m2"><p>در کف او شد نظام عالم امکان</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>داور سیف و قلم وزیر جهان بخش</p></div>
<div class="m2"><p>صاحب چتر و علم امیر جهانبان</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>چاکر بزمش بر از نبیره جوزی</p></div>
<div class="m2"><p>حاسد جاهش کم از معلم صبیان</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>پیل بدزدد همی ز بیمش خرطوم</p></div>
<div class="m2"><p>شیر بخاید همی ز سهمش دندان</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>آذر آبادگان ز مهرش آباد</p></div>
<div class="m2"><p>فضل و هنر اندرو چو لاله و ریحان</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>من دخله کان آمنا نبشته است</p></div>
<div class="m2"><p>عدلش بر باب این همایون بستان</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>گشته ز گلهای رنگ رنگ بعینه</p></div>
<div class="m2"><p>بستر مأمون شب عروسی بوران</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>ماهی و مرغش در آبگیر شناور</p></div>
<div class="m2"><p>چون دل عاشق بروز وعده جانان</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>تا بدمد اندرین فیافی لاله</p></div>
<div class="m2"><p>تا بچمد اندرین مراتع حیوان</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>تند نبیند کسی بدیده نرگس</p></div>
<div class="m2"><p>تیز نراند کسی بجانب ظبیان</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>مار در این روضه مهره داده بگنجشک</p></div>
<div class="m2"><p>شیر در این بیشه رام گشته بغزلان</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>سیل بناگه در اوفتاد در این شهر</p></div>
<div class="m2"><p>چونان سیلی که کس ندیده بدانسان</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>کرد بیکباره کوهها را دریا</p></div>
<div class="m2"><p>کند بیکباره خانها را بنیان</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>همچون سیل العرم که شهر سبارا</p></div>
<div class="m2"><p>کند ز بن دانی ار بخواندی قران</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>فریاد از جان اهل شهر برآمد</p></div>
<div class="m2"><p>بر در وی شد روان کلانتر و دهقان</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>گفتند ای خواجه بزرگ خجسته</p></div>
<div class="m2"><p>گفتند ای صاحب رشید سخندان</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>آب نموده است خاکهامان هموار</p></div>
<div class="m2"><p>سیل نموده است خانهامان ویران</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>شست یکی آنچه کاشتیم بصحرا</p></div>
<div class="m2"><p>برد یکی آنچه داشتیم در ایوان</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>زنهار ای داد بخش خسته دلان زود</p></div>
<div class="m2"><p>داد دل ما ز چرخ گردون بستان</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>میر مهین چون بدید روز رعیت</p></div>
<div class="m2"><p>بگشود ابواب لطف و رحمت و احسان</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>گفت چو از آسمان بلا شده نازل</p></div>
<div class="m2"><p>عاقله چرخم و مؤدب کیوان</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>سیل ز خشم من است چاره کنم این</p></div>
<div class="m2"><p>ابر بدست من است سود دهد آن</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>آنچه خسارت رسیده است شما را</p></div>
<div class="m2"><p>هین بنمائید تا ببخشم تاوان</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>خانه چوبین و سقفهای گلین را</p></div>
<div class="m2"><p>بهتر سازم ز صدهزار گلستان</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>گفت و وفا کرد ساخت در دو سه روزی</p></div>
<div class="m2"><p>خانه هر یک برا ز فراخور ایشان</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>الحق اینمردمی که زاد ازین میر</p></div>
<div class="m2"><p>واینهمه بخشش ز لعل و گوهر و مرجان</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>بشگفت آید بچشم خلق از ایراک</p></div>
<div class="m2"><p>ذاتش پیدا و قدر ذاتش پنهان</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>قصه میر مهین و مردم گیتی</p></div>
<div class="m2"><p>قصه پیل است و سیر کردن عمیان</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>ای لب لعلت حدیث عیسی مریم</p></div>
<div class="m2"><p>ای سر کلکت عصای موسی عمران</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>قبله گه جز درگهت نشیمن طاغوت</p></div>
<div class="m2"><p>سجده که جز بر درت عبادت اوثان</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>هر دو کفت همچو دو درخت برومند</p></div>
<div class="m2"><p>گشته ز تیغ و قلم ذواتاافنان</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>گه ز کفت فخر کرده خامه بشمشیر</p></div>
<div class="m2"><p>گه ز کفت رشک برده خامه بچوگان</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>بسکه فزودی بعدل و داد بفرسنگ</p></div>
<div class="m2"><p>در پی ظلم اندر است هر شبه دهقان</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>همچو کسی کش دهان ز خوردن حلوا</p></div>
<div class="m2"><p>رنجد و جوید از آن بترشی درمان</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>سبحان الله که ظلم نیز از این ملک</p></div>
<div class="m2"><p>گشته ابا صد هزار پای گریزان</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>قصه عدل تو و گریختن ظلم</p></div>
<div class="m2"><p>خواندن بسم الله هست و راندن شیطان</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>خور، سوی برج اسد شده است در اینروز</p></div>
<div class="m2"><p>تا برکاب تو شیر سازد قربان</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>حضرت باری اگر فدای سماعیل</p></div>
<div class="m2"><p>کبش فرستاد هم ز روضه رضوان</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>بهر فدای تو از نژاد سماعیل</p></div>
<div class="m2"><p>کبش کتائب رسید و صاحب ثعبان</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>آمدم اینک بحضرت تو نهم روی</p></div>
<div class="m2"><p>آمدم اینک بدرگه تو دهم جان</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>تا که رسد اعشی از یمامه و بطحا</p></div>
<div class="m2"><p>تا که بود نابغه ز جعده و ذبیان</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>اعشی را روح در بر تو ثناگوی</p></div>
<div class="m2"><p>نابغه را هوش بر در تو ثناخوان</p></div></div>