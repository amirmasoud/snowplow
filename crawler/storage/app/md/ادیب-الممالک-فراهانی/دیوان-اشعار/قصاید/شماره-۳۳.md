---
title: >-
    شمارهٔ ۳۳
---
# شمارهٔ ۳۳

<div class="b" id="bn1"><div class="m1"><p>چو شد چهره شاهد صبح ابلج</p></div>
<div class="m2"><p>ز خورشید بستند زرینه هودج</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بت من کمر بسته آمد به مشکو</p></div>
<div class="m2"><p>سلحشور و شاکی السلاح و مدجج</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به خویی چو مینو به مویی چو عنبر</p></div>
<div class="m2"><p>به رویی چو ورد و خطی چون بنفسج</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دو گیسو مطرا دو عارض مصفا</p></div>
<div class="m2"><p>دو جادو مکحل دو ابر و مزجج</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مرا گفت برخیز و عزم سفرکن</p></div>
<div class="m2"><p>که خنگ تو ملجم همی گشت و مسرج</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هلا چند مانی درین گور تاری</p></div>
<div class="m2"><p>چو کرم بریشم بزندان فیلج</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گرایدون نیایی ازین خانه بیرون</p></div>
<div class="m2"><p>نخواهی دگر یافتن راه مخرج</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پس آنگه بیاورد تا زنده رخشی</p></div>
<div class="m2"><p>که تخمش زیحموم و مادرش اعوج</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>یکی مرکبی سخت و ستوار و توسن</p></div>
<div class="m2"><p>یکی باره ای تند و رهوار و هیدج</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز پشت کمیت سواران کنده</p></div>
<div class="m2"><p>و یا تخم تازی نوندان مذحج</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به بیغوله اندر شدی چون عراده</p></div>
<div class="m2"><p>بز حلوفه اندر شدی همچو مزلج</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>رکابش فرا پیشم آورد و گفتا</p></div>
<div class="m2"><p>که اینست مرکوب و اینست منهج</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نشستم بران باره کوه پیکر</p></div>
<div class="m2"><p>شدم از طریق اندرون زی معرج</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>شبی قیرگون بود و دشتی پر از دد</p></div>
<div class="m2"><p>هوا آذر افشان و ره تار و معوج</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چو دریا همه چاهساران مقعر</p></div>
<div class="m2"><p>چو سلم همه کوهساران مدرج</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چو بر صخر صمازدی نعل توسن</p></div>
<div class="m2"><p>بزیر سمش خاره گشتی مدحرج</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>گهی تند راندم گهی نرم و توسن</p></div>
<div class="m2"><p>گهی راست بر زین نشستم گهی کج</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گهی از خراسان شدم زی سپاهان</p></div>
<div class="m2"><p>گهی از سپاهان شدم سوی ایذج</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>همی تاختم بارگی در بیابان</p></div>
<div class="m2"><p>چو هندو سوی گنگ و حاجی سوی حج</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ندانستم اینسان مضیق است این ره</p></div>
<div class="m2"><p>ندانستم اینسان عمیق است این فج</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>اگر نیک دانستمی این شدائد</p></div>
<div class="m2"><p>نه جستم ستبداد و نه کردمی لج</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>از آن پس که شد ساقم از خار خونین</p></div>
<div class="m2"><p>بغلطیدن از خاره بر تارکم شج</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>رسیدم بدربار میر معظم</p></div>
<div class="m2"><p>که دینار دانش از او شد مروج</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>یگانه امیر کبیری که باشد</p></div>
<div class="m2"><p>بفر فریدون و بازوی ایرج</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>رخ علم را کرده از می مصفا</p></div>
<div class="m2"><p>تن جهل را کرده در خون مضرج</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بر فکر او چشم تقدیر، اکمه</p></div>
<div class="m2"><p>بر هوش او پای تدبیر، اعرج</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ز علمش به پیکر ردائی است معلم</p></div>
<div class="m2"><p>ز حکمت ببر طیلسانی مدبج</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>امیرا تو محتاج خلقی بخدمت</p></div>
<div class="m2"><p>ولی خلق بر خدمت تست احوج</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>چو مرخ و عفار است کلکت ازیرا</p></div>
<div class="m2"><p>ززند تو نارالقری شد مؤجج</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>رقیبت کجا با تو باشد هم ترازو</p></div>
<div class="m2"><p>کجا همچو شمشاد شد شاخ عوسج</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>تو خود بهره ای و حسودت نه بهره</p></div>
<div class="m2"><p>تو چون زرنقدی رقیب تو بهرج</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>تو در فضل چون در سخا حاتم طی</p></div>
<div class="m2"><p>که بد پور عبدالله سعد خشرج</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>سر افسر از فضل داری چنان چون</p></div>
<div class="m2"><p>شهانند از تاج شاهی متوج</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>بر این خلق چون بنگری جمعشانرا</p></div>
<div class="m2"><p>چو دندانه شانه بینی مفرج</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>بقامت درازند و با رأی کوته</p></div>
<div class="m2"><p>هم از ریش پهنند و با عقل کوسج</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>رفیق نفاقند چون بکر و تغلب</p></div>
<div class="m2"><p>نه جفت و فاقند چون اوس و خزرج</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>بحکمت شفا ده بهر جان خسته</p></div>
<div class="m2"><p>بگفتار ستوار کن جسم افلج</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>باصلاحشان کوش با عقل متقن</p></div>
<div class="m2"><p>بجبرانشان خیز با رأی منضج</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>منه تا شود راه تکلیف بسته</p></div>
<div class="m2"><p>مهل تا بود باب تعلیم مرتج</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>که یافع شود طفل بعد از ترعرع</p></div>
<div class="m2"><p>که یانع شود میوه زان پس که بدفج</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>بکن پشم این ابلهان را ز سبلت</p></div>
<div class="m2"><p>بزن پنبه این خسان را به محلج</p></div></div>