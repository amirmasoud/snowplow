---
title: >-
    شمارهٔ ۱۴۲
---
# شمارهٔ ۱۴۲

<div class="b" id="bn1"><div class="m1"><p>طوبی و همایونا کاندر صف دنیا</p></div>
<div class="m2"><p>در گلشن فردوسم و در سایه طوبی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از چاکری شاه کنم فخر به قیصر</p></div>
<div class="m2"><p>وز فر ولیعهد زنم طعنه به کسرای</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مولای بزرگان جهان گشتم ازیراک</p></div>
<div class="m2"><p>دارای جهان را شده ام چاکر و مولی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا خواند امیرالشعرایم شه والا</p></div>
<div class="m2"><p>شعرم بزد از تاب و صفا طعنه به شعری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تشریف خداوند تنم نیک بیاراست</p></div>
<div class="m2"><p>چونان که دل مرد خدا جامه تقوی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر لفظ بود جامه معنی به حقیقت</p></div>
<div class="m2"><p>در پیکر من جامه لفظ آمده معنی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر چرخ تنم داشت نزار از ستم خویش</p></div>
<div class="m2"><p>زین دیبه به حمدالله جانم شده فربی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آن دیبه بپوشید مرا شه که ز نقشش</p></div>
<div class="m2"><p>چون نقش به دیباج فرو ماند مانی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از حشمت این دیبه ز نه اطلس گردون</p></div>
<div class="m2"><p>در پی کشد غاشیه ام اخطل و اعشی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ای خسرو فرزانه که شاهان اولی الامر</p></div>
<div class="m2"><p>امر تو شمارند ز هر طاعت والی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تا راست شود بوسه زند تیغ کجت چرخ</p></div>
<div class="m2"><p>گاهی به نثاوب در و گاهی به تمطی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>اقبال تو بیدارتر از دیده مجنون</p></div>
<div class="m2"><p>تیر تو جگر دوزتر از مژه لیلی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>اضحی که بهر سالی یکی روز بود عید</p></div>
<div class="m2"><p>در عصر تو هر شام و صباح آمده اضحی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>خورشید که همسایه عیسی است بگردون</p></div>
<div class="m2"><p>با سایه چتر تو کم از طایر عیسی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ابلیس ستم راست، دمت نفخه جبرئیل</p></div>
<div class="m2"><p>فرعون ظلم راست کفت آیت موسی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>حاشا که تفاریق سر کلک همایونت</p></div>
<div class="m2"><p>باشد ز تفاریق عصی انفع و اجدی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ویژه که در این دایره امروز بتحقیق</p></div>
<div class="m2"><p>تو مرکزی و فکرت تو نقطه اولی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>نام تو از آن برکه توان حرف ندا را</p></div>
<div class="m2"><p>تقدیم بر آن داد به هنگام منادی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>من بنده که از لا و نعم فرق نتانم</p></div>
<div class="m2"><p>شکر نعمت را چه توانم گفت آری</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>یزدانت دهد دولت جاوید که گیتی</p></div>
<div class="m2"><p>جاوید ز عدل تو بود جنت ماوی</p></div></div>