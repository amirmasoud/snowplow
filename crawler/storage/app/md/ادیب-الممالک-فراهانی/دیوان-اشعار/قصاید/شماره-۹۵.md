---
title: >-
    شمارهٔ ۹۵
---
# شمارهٔ ۹۵

<div class="b" id="bn1"><div class="m1"><p>مرا بخانه درون کودکی بسن دو سال</p></div>
<div class="m2"><p>بود خجسته و فرخ رخ و بدیع جمال</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دو هفته ماهی کاندر دو سالگی او را</p></div>
<div class="m2"><p>ز روی و ابرو باشد دو بدر با دو هلال</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بعهد مهد رخش را دو لعل عیسی دم</p></div>
<div class="m2"><p>نوشته زایت آتافی الکتاب مثال</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز نام عیسی مریم ورا ستوده لقب</p></div>
<div class="m2"><p>ز دست موسی عمران ورا خجسته جمال</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همی چو موسی زو غرقه لشکر فرعون</p></div>
<div class="m2"><p>همی چو عیسی زو خسته پیکر دجال</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بنام عیسی و با دست موسی است ولیک</p></div>
<div class="m2"><p>گرفته گوهر پاکش ز دست خضر زلال</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>رسوم و عادت اجداد از این پسر بینی</p></div>
<div class="m2"><p>چنانچه عادت آساد بینی از اشبال</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بسان فرخ سمندر زند در آتش پر</p></div>
<div class="m2"><p>چو بچه بط در آب برگشاید بال</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گهی بگرید بی من دو دیده اش از شوق</p></div>
<div class="m2"><p>گهی بخندد با من لبش بحسن مقال</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بظاهر ارچه مرا میوه دلست ولیک</p></div>
<div class="m2"><p>ریاض فضل و هنر را بود خجسته نهال</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نزاده زال فلک پوری اینچنین که بود</p></div>
<div class="m2"><p>بکودکی در بافر و برز رستم زال</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>یمین صدق فلک با بزرگ گوهر اوست</p></div>
<div class="m2"><p>ولی ز خردی نشناخته یمین ز شمال</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بجای شیر ز پستان همی بنوشد خون</p></div>
<div class="m2"><p>ز ساق شیر چو طفلان خرد کعب غزال</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بشوخ چشمی پیران و سالخوردانرا</p></div>
<div class="m2"><p>فریب داده لبش با وجود خردی سال</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نه القربی فی عین امها حسناء</p></div>
<div class="m2"><p>شنیده ام من و بستایمش بحسن مقال</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>که هم عصامش ستوار شد ز ترک حرام</p></div>
<div class="m2"><p>که هم عظامش محکم شد از نژاد حلال</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>شهید فکر درونش نباهت اب و ام</p></div>
<div class="m2"><p>گواه فضل برونش شباهت عم و خال</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ز پای تا سر جان و دل است و دانش و هوش</p></div>
<div class="m2"><p>گر آدم ایدون بودی ز طینت صلصال</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>نه خون خورد ز جفای زمانه چون پیران</p></div>
<div class="m2"><p>نه گریه سرکند از بهر شیر چون اطفال</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>روان بدانش پرورده هوش از او خرسند</p></div>
<div class="m2"><p>زبان بگفتن نگشوده عقل پیشش لال</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>دلش معاینه کوهی است ز آهن و پولاد</p></div>
<div class="m2"><p>اگر ز آهن و پولاد دیده ای تو جبال</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>اگرچه او ز هنر زاده چون گهر ز صدف</p></div>
<div class="m2"><p>هنر بزاید از او چو صفا ز آب زلال</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>چنانکه حربا بر آفتاب می نکرد</p></div>
<div class="m2"><p>بروی من نگرد از طلوع تا بزوال</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ز بانگ شیر نترسد که زهره شیران</p></div>
<div class="m2"><p>همی بدرد سهمش به رزمگاه رجال</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ولی ز بانک من و از نگاه من گه خشم</p></div>
<div class="m2"><p>دلش بلرزد چون کوه آهن از زلزال</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>چنانکه لاله تر از نسیم باد صبا</p></div>
<div class="m2"><p>همی بلرزد در جویبار باغ شمال</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بهشت روی زمین رشک آسمان برین</p></div>
<div class="m2"><p>ریاض امن و امان بوستان فضل و کمال</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>یکی جهان صفا پر ز بوی و رنگ و نگار</p></div>
<div class="m2"><p>یکی سپهر سنا پر ز کوکب اقبال</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بهار خلخ و کشمیر و جایگاه صنم</p></div>
<div class="m2"><p>نگارخانه ارژنگ و خوابگاه غزال</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بدوش نار ونانش ملمعی رایت</p></div>
<div class="m2"><p>بپای نسترنانش مرصعی خلخال</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>بنفشه تر همچون ستاره شب هجر</p></div>
<div class="m2"><p>شکوفه نو چون مهر بامداد وصال</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>بطره سنبل شوخش چو شاهدی شنگول</p></div>
<div class="m2"><p>بدیده نرگس مستش چو جادوئی محتال</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>یکی گشوده گره با دو صد کرشمه و ناز</p></div>
<div class="m2"><p>یکی نموده نظر با هزار غنج و دلال</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>فتاده لبلاب اندر گلوی رز گوئی</p></div>
<div class="m2"><p>عبا بگردن خالد فکنده است بلال</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>بسان قمری چون ابن بابویه قمی</p></div>
<div class="m2"><p>حدیث طوطی همچون علی عبدالعال</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>یکی نماید توضیح آیت الکرسی</p></div>
<div class="m2"><p>یکی سراید تفسیر سوره انفال</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>بروی سبزه غزالان عنبربن نافه</p></div>
<div class="m2"><p>فراز شاخان مرغان نازنین پر و بال</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>نه شیر تیز کند بهر آهوان دندان</p></div>
<div class="m2"><p>نه باز باز کند بهر تیهوان چنگال</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>کنار باغ پر از میوه های گوناگون</p></div>
<div class="m2"><p>چنانچه گوئی صحنی است پر ز برگ و نوال</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>و یا نهاده در ایوان مرد بازرگان</p></div>
<div class="m2"><p>هزار خرمن گوهر هزار مخزن مال</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>ز شاخسار درون بسکه بیخت لعل و گهر</p></div>
<div class="m2"><p>در آبگیر درون بسکه ریخت آب زلال</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>یکی بطره عذرا همی شده است نظیر</p></div>
<div class="m2"><p>یکی بدیده وامق همی شده است همال</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>جهیدن آب از آن فوارگان بینی</p></div>
<div class="m2"><p>چنانکه می بجهد از ضمیر مرد خیال</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>چو نقطه ای بدل نون کز او الف سازی</p></div>
<div class="m2"><p>وز آن الف بطرازی هزار صورت دال</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>چو آفتاب برآید بشکل قوس و قزح</p></div>
<div class="m2"><p>در او به بینی هر ساعتی قسی و نبال</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>کمان رستم زال است و تیرش از سم گور</p></div>
<div class="m2"><p>پر از قوادم سیمرغ و زه ز طره زال</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>معاینه قطراتی که باژگونه چکد</p></div>
<div class="m2"><p>در آبگیری کز آب صاف مالامال</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>گهی بساید مرسیم ساده در هاون</p></div>
<div class="m2"><p>گهی ببیزد الماس سوده در غربال</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>ببیخت گوئی بر سینه بتان گوهر</p></div>
<div class="m2"><p>بریخت گوئی بر سطح آبگینه لئال</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>و یا چو شقشقه بختیان بازل صعب</p></div>
<div class="m2"><p>که از دهان بدر آورده در قحط رجال</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>و یا چو فیلی کرده بر آسمان خرطوم</p></div>
<div class="m2"><p>چنانکه دیدی پیلان مست را به جدال</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>و یا تو گوئی طفلان خورد بر بانوج</p></div>
<div class="m2"><p>همی زنند معلق بعادت اطفال</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>و یا چو شمعی افروخته بسیمین طاس</p></div>
<div class="m2"><p>و یا ز سیماب اندر یکی خجسته نهال</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>و یا چو خیمه ای از لؤلؤ منضدتر</p></div>
<div class="m2"><p>بسطح سیمین با سیمگون عروض و حبال</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>و یا تو گوئی مرعوفکی بود منزوف</p></div>
<div class="m2"><p>که خون بجوشدش از مغز و اکحل و قیفال</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>شنیده بودم کاین باغ یکدو ماهی پیش</p></div>
<div class="m2"><p>خراب بودی و ویران ز گردش مه و سال</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>درست گفتی از طاق کسری و پرویز</p></div>
<div class="m2"><p>دمن بجای همی ماند و تیره گون اطلال</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>چنانکه خواندی در شعر طرفه ابن العبد</p></div>
<div class="m2"><p>بدست خوبان رسمی بجای مانده ز خال</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>بدی میانه آجام و طرف انهارش</p></div>
<div class="m2"><p>کنام ضیغم و ثعبان و حجر رقش و صلال</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>بغیر خاربن خمط و، اثل و سدر قلیل</p></div>
<div class="m2"><p>نه داشت خرم شاخ و نه برکشیده نهال</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>یکی زمینی بد سوخته ز تف سموم</p></div>
<div class="m2"><p>یکی فضائی بدکوفته ز باد شمال</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>گیا نرسته در او چون درون خارجیان</p></div>
<div class="m2"><p>که می نرسته در او برگ مهر احمد و آل</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>همه شفا جرف ها ربوده این نهار</p></div>
<div class="m2"><p>کنون شکوفه مطلول رسته زان اطلال</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>حدیث مرعی سعدان و مآء صدرا را</p></div>
<div class="m2"><p>اگر شنیدی و خواندی ز مجمع الامثال</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>ببین که ربع و دمن شد ربیع و قاع بقاع</p></div>
<div class="m2"><p>حمأحمی شد و صلصال مملو از سلسال</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>قصور عالیه بینی ز بوستان بقا</p></div>
<div class="m2"><p>قطوف دانیه چینی ز شاخسار کمال</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>ز دست همت فرخ امیرزاده راد</p></div>
<div class="m2"><p>گیاشجر شد سیم و زر است سنگ و سفال</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>ستوده سلطان عبدالمجید آنکه بود</p></div>
<div class="m2"><p>امیر آخور شهزاده خجسته خصال</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>هیون طبع زبون راز مهر کرده مهار</p></div>
<div class="m2"><p>ستور نفس حرون راز عقل بسته عقال</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>پس سواری شهزاد بلند اختر</p></div>
<div class="m2"><p>شود مر او را هم از بلندی و اقبال</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>سپهر توسن و خورشید زین و زهره رکاب</p></div>
<div class="m2"><p>هلال سیمین نعل و مجره تنگ و دوال</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>برای مدح تو ای میر اشرف امجد</p></div>
<div class="m2"><p>خجسته مطلعی آرم برون ز بحر خیال</p></div></div>