---
title: >-
    شمارهٔ ۱۵ - چکامه
---
# شمارهٔ ۱۵ - چکامه

<div class="b" id="bn1"><div class="m1"><p>شاد باش ای مجلس ملی که بینم عنقریب</p></div>
<div class="m2"><p>از تو آید درد ملت را درین دوران طبیب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شاد باش ای مجلس ملی که از تو چیره گشت</p></div>
<div class="m2"><p>دست مسجد بر کلیسا نور فرقان بر صلیب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شاد باش ای مجلس ملی که ایران از تو یافت</p></div>
<div class="m2"><p>دولت دور شباب اندر پی عهد مشیب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شاد باش ای مجلس ملی که باشد مر تو را</p></div>
<div class="m2"><p>شرع پشتیبان و دولت حافظ و ملت نقیب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شاد باش ای مجلس ملی که هستی بی گزاف</p></div>
<div class="m2"><p>آسمان مهر و ماه و زهره و کف الخضیب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شاد باش ای مجلس ملی که ظلم از تو گریخت</p></div>
<div class="m2"><p>همچو حجاج بن یوسف از غزاله وز شبیب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شاد باش ای مجلس ملی که از تایید تو</p></div>
<div class="m2"><p>عاشق بیچاره شد آسوده از جور رقیب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چشم ها را روی حوری کام ها را طعم شهد</p></div>
<div class="m2"><p>گوش ها را بانگ رودی مغزها را بوی طیب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تا تو برپائی درین کشور نرنجد آشنا</p></div>
<div class="m2"><p>تا تو برجائی درین سامان نفرساید غریب</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کس نباشد زین سپس از جور دیوان در شکنج</p></div>
<div class="m2"><p>کس نماند بعد ازین از عدل سلطان بی نصیب</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ناله مظلوم آید تا به تخت شهریار</p></div>
<div class="m2"><p>راز محبوبان شود یکباره پیدا بر حبیب</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>شومی بیداد و جور آید عیان بر دادگر</p></div>
<div class="m2"><p>حالت بیمار گردد آشکارا بر طبیب</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>کودکان را کس نترساند ز شکل هولناک</p></div>
<div class="m2"><p>عاجزان را کس نلرزاند ز فریاد مهیب</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>منجنیق آتش نبارد بر سر سکان لبیب</p></div>
<div class="m2"><p>محتکر قحطی نیارد در دل عام خصیب</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>خاره اندر خاک نستاند طراوت از گهر</p></div>
<div class="m2"><p>غوره اندر تاک نفروشد حلاوت بر زبیب</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گول را نبود رقابت با حکیم و هوشمند</p></div>
<div class="m2"><p>سفله نتواند عداوت با اصیل و با نجیب</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>جبرئیلی کی تواند بعد ازین دیو مرید</p></div>
<div class="m2"><p>پارسائی که نماید زین سپس شیخ مریب</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>کبک آمد در خرامش کرکس از رفتار ماند</p></div>
<div class="m2"><p>بلبل آمد در ترنم زاغ افتاد از نعیب</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بسکه ظالم را بکف شد خون مظلومان خضاب</p></div>
<div class="m2"><p>بسکه روی خستگان از اشک خونین شد خضیب</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بسکه هر ملهوف گفت ای رکن من لارکن له</p></div>
<div class="m2"><p>بسکه هر مظلوم برخواند آیت امن یجیب</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>شهریار دادگر بخشود بر قومی ذلیل</p></div>
<div class="m2"><p>خسرو عادل ترحم کرد بر مشتی کئیب</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>شه مظفر داور گیتی خدیو کامران</p></div>
<div class="m2"><p>آنکه ذاتش مستطابستی و خلقش مستطیب</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>آنکه خصمش هر کجا جنبش کند گردد مصاب</p></div>
<div class="m2"><p>آنکه رایش هر کجا تابش کند گردد مصیب</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>در حدود خصم قهرش همچو نار اندر حدید</p></div>
<div class="m2"><p>در قلوب خلق مهرش همچو آب اندر قلیب</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بر رعیت داد شه در مملکت و الطاف وی</p></div>
<div class="m2"><p>بهتر از سال فراح و خوشتر از عام خصیب</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>عدل این شه را کرام الکاتبین داند حساب</p></div>
<div class="m2"><p>کار این شه را امیرالمؤمنین باشد حسیب</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ای درخت شرع ازین فرخنده مجلس جاودان</p></div>
<div class="m2"><p>باد اصلت محکم و فرعت قوی غصنت رطیب</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>وی سپهداران دین بادا شما را تا ابد</p></div>
<div class="m2"><p>عون حق خیرالمعین و حفظ حق نعم الرقیب</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>عقل باشد مر شما را مادر و دانش پدر</p></div>
<div class="m2"><p>عدل باشد مر شما را زاده و حکمت ربیب</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>مسجد از دیدارتان بالد چو بستان از درخت</p></div>
<div class="m2"><p>منبر از گفتارتان نازد چو سرو از عندلیب</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>بس کرامتها نمودید ای کرامت را نسب</p></div>
<div class="m2"><p>بس شجاعتها نمودید ای شجاعت را نسیب</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>رنجها بردید کز آن رنجه شد کوهان کوه</p></div>
<div class="m2"><p>کارها کردید کز آن خیره شد هوش لبیب</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>شکر خوی نیکتان را با مقالی بس شگرف</p></div>
<div class="m2"><p>نعت ذات پاکتان را با لسانی بس عجیب</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>در فلک کروبیان گویند و در فردوس حور</p></div>
<div class="m2"><p>بر مناره مؤمنان خوانند و در منبر خطیب</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>گر سخن رانید در این بقعه حق گوید بلی</p></div>
<div class="m2"><p>ور دعا خوانید در این روضه حق باشد مجیب</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>مرحبا گوید بر این وضع بدیع و رای نیک</p></div>
<div class="m2"><p>آفرین خواند بر این فکر خوش و بزم رحیب</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>کردگار اندر فراز عرش و پیغمبر به خلد</p></div>
<div class="m2"><p>مرتضی اندر لب تسنیم و قائم در مغیب</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>بر فراز تخت زرین شاه و بر افلاک ماه</p></div>
<div class="m2"><p>در صف کروبیان جبریل و در محضر ادیب</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>مجلس ملی ز یاد شاعران برد آنچه بود</p></div>
<div class="m2"><p>از حماسه وز تهانی وز مدیح و از نسیب</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>اینزمان طرح سخن اینسان سزد نه آنکه گفت</p></div>
<div class="m2"><p>احمد اندر مدح کافور و حسن بهر خصیب</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>قدسیان فهرست این مجلس بحلق آویختند</p></div>
<div class="m2"><p>همچنان کاندر گلوی کودکان عودالصلیب</p></div></div>