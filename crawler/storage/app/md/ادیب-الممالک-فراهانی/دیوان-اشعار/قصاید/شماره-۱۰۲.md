---
title: >-
    شمارهٔ ۱۰۲
---
# شمارهٔ ۱۰۲

<div class="b" id="bn1"><div class="m1"><p>ای صبا گر رهت افتاد بر آن گوشه بام</p></div>
<div class="m2"><p>نائب السلطنه را بر زمن خسته پیام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کای خداوند هنرپرور دانشور راد</p></div>
<div class="m2"><p>که رفیع است ترا قدر و منیع است مقام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سالها خواستم از حق که بکام تو رود</p></div>
<div class="m2"><p>چرخ تا خلق بیابند ز انصاف تو کام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>توسن ملک شود رام تو تا از همت</p></div>
<div class="m2"><p>فتنه آرام شود دوست خوش و دشمن رام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آنچه میخواستم از یزدان فرمود عطا</p></div>
<div class="m2"><p>لله الحمد که یکباره رسیدم بمرام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آمد اندر کف راد تو مقالید امور</p></div>
<div class="m2"><p>پادشاهی را در دست تو افتاد زمام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هنری مردان یکسر بدرت دائره وار</p></div>
<div class="m2"><p>گرد کشتند چو حاجی بصف بیت حرام</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>همه لبریز ز فضل تو چو گل بر سر شاخ</p></div>
<div class="m2"><p>همه رخشنده ز نور تو چو می در دل جام</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>همه را دیدی مستوجب عنوان شرف</p></div>
<div class="m2"><p>همه را خواندی شایسته ارجاع مهام</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سخته شد از سخن نرم تو هر مشکل سخت</p></div>
<div class="m2"><p>پخته شد از نفس گرم تو هر جاهل خام</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>جز کمین بنده که پیش تو بدم از همه پیش</p></div>
<div class="m2"><p>بقلم گاه نبشتن بقدم گاه خرام</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>منطقم گفتی شیرین و حدیثم دلکش</p></div>
<div class="m2"><p>خردم خواندی ستوار و سخن با هنگام</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>این زمان رفته ز یادت که بدین نام و نشان</p></div>
<div class="m2"><p>بنده کی بود و کجا بود و چه بودست و کدام</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>تا بحدی که گرم بینی ترسم گوئی</p></div>
<div class="m2"><p>نیک بینید که غماز بود یا نمام</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>از کجا آمده اینجا و چه دارد مقصود</p></div>
<div class="m2"><p>در کجا دیده ام او را و چه بودستش نام</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>از فلک ناله کند یا ز قضا یا ز قدر</p></div>
<div class="m2"><p>از قمر شکوه کند یا ز زحل یا بهرام</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بشفاخانه بریدش که سراید هذیان</p></div>
<div class="m2"><p>بپزشگانش نمائید که دارد سرسام</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>داورا میرا ای کرده فلک بر تو سجود</p></div>
<div class="m2"><p>تا پی کار زمین ساختی از مهر قیام</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>من نه سرسامی و نه صرعی و نه بیخردم</p></div>
<div class="m2"><p>مغزم آسوده ز سودای صداعست و زکام</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>نه خرابم کند از نشأی می لعل افروز</p></div>
<div class="m2"><p>نه فریبم دهد از عشوه بت سیم اندام</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>نروم در پی نان خرده چو ماهی در شست</p></div>
<div class="m2"><p>نشوم در طلب دانه چو مرغ اندر دام</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>نز پی جاه برم سجده بدرگاه ملوک</p></div>
<div class="m2"><p>نز پی مال زنم شعله بجان ایتام</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>فطرتی دارم بالاتر ازین چرخ بلند</p></div>
<div class="m2"><p>فکرتی دارم والاتر از ان بدر تمام</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>توسن وزین و ستام ار نبود باکی نیست</p></div>
<div class="m2"><p>کم خرد توسن و فرهنگ بود زین وستام</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>رایض توسن عقل همه نفس است ولی</p></div>
<div class="m2"><p>نبود عقل مرا در کف اماره لگام</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>طمع و حرص بر این مردم شاهند و وزیر</p></div>
<div class="m2"><p>لیک بر بنده بحمدالله عبدند و غلام</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>نکنم سستی و مستی که ادب دارم هوش</p></div>
<div class="m2"><p>نگرایم سوی پستی که پدر دارم و مام</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>زاده احمد و حیدر پسر فاطمه امس</p></div>
<div class="m2"><p>خلف یثرب و بطحاولدر کن و مقام</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>منم آن مرد عظامی و عصامی که شرف</p></div>
<div class="m2"><p>از عصامم بعظام و ز عظامم بعصام</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>گر کسی را علم از علم رود بر گردون</p></div>
<div class="m2"><p>بنده را باید بر چرخ فرازم اعلام</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>تخم علم خود اگر در دل خاک افشانم</p></div>
<div class="m2"><p>برفتد بیخ خرافات و نشان اوهام</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>منطق و نحو معانی و قوافی و عروض</p></div>
<div class="m2"><p>اتفاقات و تواریخ شهور و اعوام</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>طب و جراحی و کحالی و تشریح بدن</p></div>
<div class="m2"><p>دوران دم و وصل عضل و فصل عظام</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>دانش بستنی و رستنی و جانوران</p></div>
<div class="m2"><p>علم قیافی و عیافی تعبیر منام</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>همه را خوانده و آموخته ام بر دگران</p></div>
<div class="m2"><p>گرچه بیفایده شد علم که الناس نیام</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>شاعری فحل و دبیری سره ذاتی پاکم</p></div>
<div class="m2"><p>جبلی شاهق و چرخی مه و بحری طمطام</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>نیک سنجم اگر از فلسفه رانی صحبت</p></div>
<div class="m2"><p>خوب دانم اگر از شرع سرائی احکام</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>در مذاق عرفا شیخ طریقم بل قطب</p></div>
<div class="m2"><p>در مقام فقها مجتهدم بلکه امام</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>چون سنمارم معمار و چو نوحم نجار</p></div>
<div class="m2"><p>آذر بتکر و در بت شکنی ابراهام</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>فاقدالعیشم در بزم بدستور خرد</p></div>
<div class="m2"><p>قائدالجیشم در رزم بآیین نظام</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>با هنر ورزم مهری که به کاوس رستم</p></div>
<div class="m2"><p>با ستم رانم قهری که بهرمز بسطام</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>ای بس ایام و لیالی که بدرگاه تو من</p></div>
<div class="m2"><p>شاد و خوش بودم از وقت سحرگه تا شام</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>تو از آن ایام ای خواجه فرامش کردی</p></div>
<div class="m2"><p>لیک من بنده فرامش نکنم آن ایام</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>هیچ دانی که مرا حال شبانروزی چیست</p></div>
<div class="m2"><p>از هجوم غم و رزق کم و افزونی وام</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>روز روشن ببرم چون شب یلدا تاریک</p></div>
<div class="m2"><p>آب شیرین بمذاقم چو می تلخ حرام</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>بهره دونان گنج است و مرا رنج رسد</p></div>
<div class="m2"><p>قسمت هر کس تقدیر شده است از قسام</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>دیو از طعمه شود تخمه و جم گرسنه دل</p></div>
<div class="m2"><p>گرگ برفآب خورد تشنه بمیرد ضرغام</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>سفلگان جمله بکار اندر و من بیکارم</p></div>
<div class="m2"><p>داس شاهر شد و شمشیر یمانی بنیام</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>ملک محتاج است اینک بدبیری چون من</p></div>
<div class="m2"><p>هم بنازد بهنرمندی چون من اسلام</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>ملک و اسلام چو بی من شود ای خواجه بخوان</p></div>
<div class="m2"><p>چار تکبیر براین ملک و بر اسلام سلام</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>تو ببایست کنی کسر دلم را جبران</p></div>
<div class="m2"><p>کر کریمم من و تو جابر عثرات کرام</p></div></div>