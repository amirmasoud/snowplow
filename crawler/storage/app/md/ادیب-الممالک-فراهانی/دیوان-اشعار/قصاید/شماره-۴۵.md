---
title: >-
    شمارهٔ ۴۵
---
# شمارهٔ ۴۵

<div class="b" id="bn1"><div class="m1"><p>ز آمدن فرودین و رفتن اسفند</p></div>
<div class="m2"><p>دلها خرم شد و روانها خرسند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گلها افروختند آتش زردشت</p></div>
<div class="m2"><p>مرغان آموختند ترجمه زند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ابر ببالای خاک لؤلؤ تربیخت</p></div>
<div class="m2"><p>باد فراز زمین عبیر پراکند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سبزه تر فرش نو بخاک بگسترد</p></div>
<div class="m2"><p>لاله همه ناف خود بنافه بیاکند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ترکی از شاهدان خطه بابل</p></div>
<div class="m2"><p>خوبتر از لعبتان چین و سمرقند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ترکی تازی زبان ولی حبشی موی</p></div>
<div class="m2"><p>زاده ز پشت ملوک ملت پازند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بسته بجانهای زار مهرش پیمان</p></div>
<div class="m2"><p>جسته بدلهای خسته زلفش پیوند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>غنچه سحرگاه اگر دهان بگشاید</p></div>
<div class="m2"><p>خون جگر نوشد از لبش بشکرخند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آمد در بوستان چو سروی آزاد</p></div>
<div class="m2"><p>شد بصف باغ همچو نخل برومند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>راه دلم زد سپس برفت و برآشفت</p></div>
<div class="m2"><p>این دل دیوانه با روان خردمند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بردم دل را بر حکیم که شاید</p></div>
<div class="m2"><p>پندش گوید نکرد سود بر او پند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>لاجرم از دل همی بکندم دل زانک</p></div>
<div class="m2"><p>دندان چون درد کرد باید برکند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>هاروت از آنگه ماند در چه بابل</p></div>
<div class="m2"><p>تن دژم و سرنگون دو بازو در بند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نرگس جادوگری ز چاه زنخدان</p></div>
<div class="m2"><p>هاروت آسا بچاه بابلم افکند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گریه کنم بر دلی فرو شده در چاه</p></div>
<div class="m2"><p>باش چو یعقوب بهر گم شده فرزند</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>لیک بهر ورطه زان خوشم که بفرقم</p></div>
<div class="m2"><p>سایه فکنده یکی درخت برومند</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>سروی کز قیروان بساحت کشمیر</p></div>
<div class="m2"><p>سایه بگسترده پی فسان و ترفند</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>هر نفس از جویبار همت فضلش</p></div>
<div class="m2"><p>شاخ بروید فزون ز هشتصد و اند</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>میری کاندر فنون دانش و مردی</p></div>
<div class="m2"><p>نیستش اندر همه زمانه همانند</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>صارم و درعش بود ز هیبت و تدبیر</p></div>
<div class="m2"><p>نزدم شمشیر و تیر و رخت کژ آکند</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>اسبش چون رخ نهد به پره دشمن</p></div>
<div class="m2"><p>پیل دمان است چون پیاده آوند</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>رمحش دشمن شکارد ار همه گردون</p></div>
<div class="m2"><p>تیغش خارا شکافد ار همه الوند</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>میران بسیار بوده اند از این پیش</p></div>
<div class="m2"><p>نیز بیایند مردمان هنرمند</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>لیک چنو چشم روزگار نه بیند</p></div>
<div class="m2"><p>ز آنکه چنو مام دهر نارد فرزند</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>خیزید ای خادمان بار و بیکبار</p></div>
<div class="m2"><p>بر رخ این میر می بسوزید اسپند</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>من همگی خاک آستان ویستم</p></div>
<div class="m2"><p>دور فتادم ز آستانش هر چند</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>خون خورم از هجر آستانش و هرگز</p></div>
<div class="m2"><p>می نخورم جز به آستانش سوگند</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>فریاد ای میر درد هجران تا کی</p></div>
<div class="m2"><p>الغوث ای خواجه سوز حرمان تا چند</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>از کف بی دولتان دولت ایران</p></div>
<div class="m2"><p>چند بنوشم شرنگ و خصم خورد قند</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>گر نرسم بر درت زمانه غدار</p></div>
<div class="m2"><p>پوست بخواهد ز استخوانیم برکند</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>بر تن رنجور من شماتت دشمن</p></div>
<div class="m2"><p>چرخ پسندیده ای امیر تو مپسند</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>بر تو فراوان درود باید خواندن</p></div>
<div class="m2"><p>اما هر چامه را نماند بساوند</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>تا که رسد نوبهار بعد زمستان</p></div>
<div class="m2"><p>تا که بود فروردین مه از پی اسفند</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>جشن فریدون و فروردین همیون</p></div>
<div class="m2"><p>خرم بادا به روزگار خداوند</p></div></div>