---
title: >-
    شمارهٔ ۴۴
---
# شمارهٔ ۴۴

<div class="b" id="bn1"><div class="m1"><p>این نبینی که چو هنگام بهار آید</p></div>
<div class="m2"><p>شاخ خرم شود و غنچه به بار آید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نک بهار آمد و خندید گل سوری</p></div>
<div class="m2"><p>که بخندد گل سوری چو بهار آید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همچنان مریم گلها شود آبستن</p></div>
<div class="m2"><p>همچنان عیسی گل بر سر دار آید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گل چو زیباصنمان چهره بیاراید</p></div>
<div class="m2"><p>مرغ دلشیفته او را بکنار آید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همچنان عنتره کاید ببر عیله</p></div>
<div class="m2"><p>یا فزردق که بنزدیک نوار آید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شمنانند بگلزار درون مرغان</p></div>
<div class="m2"><p>شاخ همچون بت و بستان چو بهار آید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>لحن داودی برخواند هزار آوا</p></div>
<div class="m2"><p>نار نمرودی فاش از گلنار آید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>باغ مانند عروسی دو رخش گلگون</p></div>
<div class="m2"><p>سر گیسویش پر مشک تتار آید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>یا چو ارژنگ که آراست بچین مانی</p></div>
<div class="m2"><p>از گل و لاله پر از نقش و نگار آید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>یا چو ترکی که قدش سرو و لبش غنچه</p></div>
<div class="m2"><p>گل و سنبلش همی روی و عذار آید</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نرگس مست بصد غمزه بباغ اندر</p></div>
<div class="m2"><p>چون دو چشم صنمی باده گسار آید</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>در خمار آمده چشمانش ز می آری</p></div>
<div class="m2"><p>هر که می خورد فراوان بخمار آید</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>خفته را ماند اما نبود خفته</p></div>
<div class="m2"><p>مست را ماند اما هشیار آید</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گل خیری چو بتی مقنعه اش زرین</p></div>
<div class="m2"><p>وز زبرجد بکفش چند سوار آید</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>وان بنفشه صنمی تنش ز بیجاده</p></div>
<div class="m2"><p>ز مردین مرکب همواره سوار آید</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بید مشک آمده بر شاخ چنان شیخی</p></div>
<div class="m2"><p>پوستین در بر بالای منار آید</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>شاخ محرم ز شکوفه است و سحاب از بر</p></div>
<div class="m2"><p>همچو حاجی به منی بهر جمار آید</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ارغوان ترکی یاقوت کله باشد</p></div>
<div class="m2"><p>ضیمران شوخی زرینه صدار آید</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>عارض نسرین همگونه سیمستی</p></div>
<div class="m2"><p>گونه عبهر همرنگ نضار آید</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>وان شقایق بچمن در بر آذریون</p></div>
<div class="m2"><p>چون دو زندانست که از مرخ و عفار آید</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>فروردین خیمه اسفند به هم برزد</p></div>
<div class="m2"><p>همچو غازی که پی نهب و اسار آید</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>راست پنداری کان عامر اسمعیل</p></div>
<div class="m2"><p>در سراپرده مروان حمار آید</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>نوبهار آمد در باغ بصد خوشی</p></div>
<div class="m2"><p>همچو یاری که بخلوتکه یار آید</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>دیدکان زاغ سیه چهره بباغ اندر</p></div>
<div class="m2"><p>در پی وصل کواعب چو یسار آید</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>گفت بایست برانیمش با ذلت</p></div>
<div class="m2"><p>که زماندنش بس عیب و عوار آید</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>لاجرم رخت بگاو اندر بنهادش</p></div>
<div class="m2"><p>تا پس از وی بچمن صلصل و سار آید</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>چنگزن سار شد و نغمه سرا صلصل</p></div>
<div class="m2"><p>ارغنون زن بصف باغ هزار آید</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>فاخته سازد طنبوره بسرو اندر</p></div>
<div class="m2"><p>نای زن قمری بر شاخ چنار آید</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>ابر با ماورد از گرد زمین شوید</p></div>
<div class="m2"><p>تا نه بر زلف سمن گرد وغبار آید</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>باد بر تهنیت باغ بدست اندر</p></div>
<div class="m2"><p>عنبر و کافور از بهر نثار آید</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>زان می غالیه بوسا تکنی در ده</p></div>
<div class="m2"><p>که نسیم سحری غالیه بار آید</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>ویژه در خطه گیلان که بمغز اندر</p></div>
<div class="m2"><p>نکهت مشک و گل از رود کنار آید</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>هر طرف سروی اندر بر شمشادی</p></div>
<div class="m2"><p>چون نگاری که در آغوش نگار آید</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>از ریاحین همه سو نکهت راح آید</p></div>
<div class="m2"><p>وز عقاقیر هوا بوی عقار آید</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>بر لب دریا آن سبزه تر گوئی</p></div>
<div class="m2"><p>شاهدان را خط نو گرد عذار آید</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>آب گه جزر کند گاه بمد کوشد</p></div>
<div class="m2"><p>میغ گه آب شود گاه بخار آید</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>دیده بگشا و یکی سوی هوا بنگر</p></div>
<div class="m2"><p>کابر چون اشتر بگسسته مهار آید</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>باد چون پی کند این اشتر بازل را</p></div>
<div class="m2"><p>تا ابد در خور نفرین چو قدار آید</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>در حصاریم ز ماه رمضان یارب</p></div>
<div class="m2"><p>شود آیا که فتوحی بحصار آید</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>می بروی گل نوشاندمان حوری</p></div>
<div class="m2"><p>که گل تازه بر رویش خار آید</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>گلوی مار را بفشرد بسی روزه</p></div>
<div class="m2"><p>گلوی مینا چندی بفشار آید</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>زاهد صومعه را گو بیکی ساغر</p></div>
<div class="m2"><p>روزه بشکن که تنت زار و نزار آید</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>من ازین روزه فگارستم و میترسم</p></div>
<div class="m2"><p>چون دل من دل تو نیز فگار آید</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>این تکلف را تحمیل بمفتی کن</p></div>
<div class="m2"><p>تا همه کار بسامان و قرار آید</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>شتر مست کشد بارگران دایم</p></div>
<div class="m2"><p>لاشه لاغر آسوده ز بار آید</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>هفته ای ماند که ماه رمضان زین در</p></div>
<div class="m2"><p>برود زود و گرفتار بوار آید</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>نک همانند مریضی است بنزع اندر</p></div>
<div class="m2"><p>که نفسهاش همی بر بشمار آید</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>عید چون قابض ارواح بر او تازد</p></div>
<div class="m2"><p>نیش زن بر وی چون تافته مار آید</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>هم از این خان سپنجی بردش آنجا</p></div>
<div class="m2"><p>که بصد حسرت و افسوس دچار آید</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>غره شعبان دیدی بمحاق اندر</p></div>
<div class="m2"><p>باش تا بر رمضان نیز سرار آید</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>باد بر وفق مرادست وزان ارجو</p></div>
<div class="m2"><p>که از این دریا کشتی بکنار آید</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>من ز فروردین چندان نیمی شادان</p></div>
<div class="m2"><p>که تو گوئی رمضان راهسپار آید</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>داستان من و ماه رمضان مانا</p></div>
<div class="m2"><p>راست چون واقعه ی کحل و عرار آید</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>من بقصد او با ناخن و ناب آیم</p></div>
<div class="m2"><p>او بخون من با تیغ و شفار آید</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>من سوی دکه خمار پناه آرم</p></div>
<div class="m2"><p>او سوی خانه مفتی بفرار آید</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>روز نوروز که با روزه شود توأم</p></div>
<div class="m2"><p>تازه وردی است همصحبت خار آید</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>گر رود روزه و نوروز رسد از پی</p></div>
<div class="m2"><p>هست عمری که پس از مرگ و تبار آید</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>یا ز قومی شود از سفره و آید شهد</p></div>
<div class="m2"><p>یا رقیبی رود از خانه و یار آید</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>یا وصالی که پس از هجر بتان بینی</p></div>
<div class="m2"><p>یا صباحی است که بعد از شب تار آید</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>یا بدرگاه خداوند پس از هجرت</p></div>
<div class="m2"><p>کمترین بنده اش را باز گذار آید</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>آنکه دولت را جوینده فخرستی</p></div>
<div class="m2"><p>آنکه ملت را حامی به دمار آید</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>میر دریا دل باذل که همه کارش</p></div>
<div class="m2"><p>همچو گفتارش نغز و ستوار آید</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>خدمتش مایه اقبال و بهی باشد</p></div>
<div class="m2"><p>همتش دافع آفات و مضار آید</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>در حسب با خرد وکربز و با دانش</p></div>
<div class="m2"><p>در نسب فرخ و فرخنده نجار آید</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>هر کجا تازد با فتح و ظفر تازد</p></div>
<div class="m2"><p>هر کجا آید با عز و وقار آید</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>نصرت و شوکت و یمنش بیمین آید</p></div>
<div class="m2"><p>دولت و نعمت و یسرش بیسار آید</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>دست او ابری کاندر نیسان بارد</p></div>
<div class="m2"><p>خوی او بوئی کاندر گلزار آید</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>عزمش آنگاه که بر خصم همی تازد</p></div>
<div class="m2"><p>هست سیلی که روان از کهسار آید</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>داورا میرا دور از در درگاهت</p></div>
<div class="m2"><p>بنده خوار است بهر شهر و دیار آید</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>مژه در چشمم چون سوزن و خارستی</p></div>
<div class="m2"><p>مو بر اندامم چون افعی و مار آید</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>چرخ خواهد که مرا بنده کند حاشا</p></div>
<div class="m2"><p>کز چنین کارم ننگستی و عار آید</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>او بود ساجد دونان و منش هرگز</p></div>
<div class="m2"><p>سجده نارم که ازین کار شنار آید</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>بندگی بر فلکم عار بود اما</p></div>
<div class="m2"><p>سجده بر خاک توام اصل فخار آید</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>ور فلک زارم ازین کین بکشد، غم نی</p></div>
<div class="m2"><p>که خداوند مهین مدرک، ثار آید</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>ای خداوند ز گردون نهراسد آن</p></div>
<div class="m2"><p>که بکوی تو همی در زنهار آید</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>تو بنامیزد بیضا و عصا داری</p></div>
<div class="m2"><p>چرخ با شعبده چون دیو و سحار آید</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>اندر آنجا که کلیم و ید بیضا شد</p></div>
<div class="m2"><p>سامری کیست که با عجل و خوار آید</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>گر بفرمان تو گردنده فلک گردد</p></div>
<div class="m2"><p>همه کاریش بسامان و قرار آید</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>سیم و زر در همه انظار گرامی شد</p></div>
<div class="m2"><p>لیک اندر نظر پاک تو خوار آید</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>از کفت نعمت بر خلق رسد چونان</p></div>
<div class="m2"><p>کآب در جدول از انهار و بحار آید</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>پارس را بیم ز صمصامه عمروستی</p></div>
<div class="m2"><p>روم را هول ز شمشیر ضرار آید</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>لیک عمرو از تف خشم تو تبه گردد</p></div>
<div class="m2"><p>هم ضرار از دم تیغت بفرار آید</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>تو نیندیشی اگر خصم فزون باشد</p></div>
<div class="m2"><p>باز نهراسد اگر کبک هزار آید</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>جز تو این مردم گیتی همه شومندی</p></div>
<div class="m2"><p>نابکارند و هنرشان نه بکار آید</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>شید بازند و سوی صید همی تازند</p></div>
<div class="m2"><p>چون پلنگی که بصحرا بشکار آید</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>یا چو گرگی شده در کسوت میش اندر</p></div>
<div class="m2"><p>یا چه دزدی است که با قافله یار آید</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>دعوی دانش دارند و ندانند ایچ</p></div>
<div class="m2"><p>که تهی مایه بسی داعیه دار آید</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>همه طبلند اگر طبل تهی دیدی</p></div>
<div class="m2"><p>در پی نوش رود یا پی خوار آید</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>همچون آن برجمی از فرط طمع هر یک</p></div>
<div class="m2"><p>در تف آتش بر بوی قتار آید</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>تو همیونی و فرخنده بنا میزد</p></div>
<div class="m2"><p>که شعارت را فرهنگ دثار آید</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>در حکمت را طبع تو بود مخزن</p></div>
<div class="m2"><p>زر دانش را فضل تو عیار آید</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>در بلاغت نبود کفو تو در گیتی</p></div>
<div class="m2"><p>ویژه چون کلکت توقیع نگار آید</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>پور هارونت شاگرد دبستان شد</p></div>
<div class="m2"><p>پسر یحیی فرمان بر بار آید</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>بنده ات نیز منستم که همی نامم</p></div>
<div class="m2"><p>از ادیبان و حکیمان بشمار آید</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>شعر را با لغت پارسی و تازی</p></div>
<div class="m2"><p>هر چه گویم همه نغز و ستوار آید</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>نه باعنات و تکلف سخنی گویم</p></div>
<div class="m2"><p>نه مرا نسج بدیهت دشوار آید</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>نه من از معنی شعر دگران آرم</p></div>
<div class="m2"><p>نه مرا قافیت و لفظ معار آید</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>شاعری دانم بهتر ز لبید اما</p></div>
<div class="m2"><p>شعر زینت بودم نی که شعار آید</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>هم عروضم هم موسیقی دانم</p></div>
<div class="m2"><p>گرچه زین هر دو مرا یکسره عار آید</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>هم بجغرافی و هیئت نبود کفوم</p></div>
<div class="m2"><p>چون سخن بر سر کانون و مدار آید</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>من همیدانم تغییر فصول از چه</p></div>
<div class="m2"><p>و اختلاف از چه بر این لیل و نهار آید</p></div></div>
<div class="b" id="bn102"><div class="m1"><p>نظر روشنم اندازه شناسستی</p></div>
<div class="m2"><p>اختر طالعم استاره شمار آید</p></div></div>
<div class="b" id="bn103"><div class="m1"><p>بوالعلاء باید نعلین مرا بوسد</p></div>
<div class="m2"><p>وان غیاث الدین چون غاشیه دار آید</p></div></div>
<div class="b" id="bn104"><div class="m1"><p>من کلیمستم اگر حکمت نیلستی</p></div>
<div class="m2"><p>من خلیلستم اگر دانش نار آید</p></div></div>
<div class="b" id="bn105"><div class="m1"><p>آتشین آهم اگر چرخ بود ز آهن</p></div>
<div class="m2"><p>آهنین کوهم اگر غصه شرار آید</p></div></div>
<div class="b" id="bn106"><div class="m1"><p>خرد منگر بمن ای خواجه مبین خوارم</p></div>
<div class="m2"><p>که بکار آیدت آنچیز که خوار آید</p></div></div>
<div class="b" id="bn107"><div class="m1"><p>منم آن شاخ کز اقبال تو روئیدم</p></div>
<div class="m2"><p>هر زمان از من صد گونه ثمار آید</p></div></div>
<div class="b" id="bn108"><div class="m1"><p>فربهی داشتم و چرخ نزارم کرد</p></div>
<div class="m2"><p>که نژادم ز بزرگان نزار آید</p></div></div>
<div class="b" id="bn109"><div class="m1"><p>پدرانم همه با چرخ بکین بودند</p></div>
<div class="m2"><p>هم از آن قوم مرا اصل و تبار آید</p></div></div>
<div class="b" id="bn110"><div class="m1"><p>واندرین نطعم بر ناصر بن خسرو</p></div>
<div class="m2"><p>تاختن باید چون گاه قمار آید</p></div></div>
<div class="b" id="bn111"><div class="m1"><p>لیک فضل از متقدم شده کو گوید</p></div>
<div class="m2"><p>«چند گوئی که چو هنگام بهار آید»</p></div></div>