---
title: >-
    شمارهٔ ۱۱۸ - چکامه
---
# شمارهٔ ۱۱۸ - چکامه

<div class="b" id="bn1"><div class="m1"><p>طهماسب خداوند راستین</p></div>
<div class="m2"><p>داردیم و کان در دو آستین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دریا ز یسارش برد یسار</p></div>
<div class="m2"><p>گردون بیمینش خورد یمین</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خوانده است مؤید به دولتش</p></div>
<div class="m2"><p>دارای جهان شهریار دین</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زیرا که خیام جلال را</p></div>
<div class="m2"><p>حبلی است ز تایید او متین</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بالد ز سرش رایت و کلاه</p></div>
<div class="m2"><p>نازد بکفش خامه نگین</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای خامه تو موی مهوشان</p></div>
<div class="m2"><p>ای نامه تو روی حور عین</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای خسته کمانت پر عقاب</p></div>
<div class="m2"><p>ای بسته گمانت در یقین</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جمشید بگیرد ترا رکاب</p></div>
<div class="m2"><p>خورشید ببوسد ترا زمین</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>با برز منوچهر و کیقباد</p></div>
<div class="m2"><p>با گرز فریدون و آبتین</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>فرهنگ ترا خوانده مرحبا</p></div>
<div class="m2"><p>اقبال ترا گفته آفرین</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شاها ملکا آسمان بمن</p></div>
<div class="m2"><p>بی سابقتی بسته است کین</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>آویخته حلقم بریسمان</p></div>
<div class="m2"><p>آمیخته زهرم بانگبین</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>جز خون نخورم روز و شب مگر</p></div>
<div class="m2"><p>دنیا چو مشیمه است و من جنین</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ز آن رو که بود درگهت مرا</p></div>
<div class="m2"><p>حصنی ز جفای فلک حصین</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>در بارگهت ملتجی شدم</p></div>
<div class="m2"><p>دادم بستان از سپهر هین</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>درگاه تو باشد پناه من</p></div>
<div class="m2"><p>فردوس بود جای متقین</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ایاک نولی و نستمد</p></div>
<div class="m2"><p>ایاک نرجی و نستعین</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بر خلق توئی صاحب و عمید</p></div>
<div class="m2"><p>بر شاه توئی ناصح و امین</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>تا مشک ترا بارد از قلم</p></div>
<div class="m2"><p>تا ماه ترا تابد از جبین</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>تا میل بنین است زی بنات</p></div>
<div class="m2"><p>تا شرم بنات است از بنین</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>باشی بهمه سروران مطاع</p></div>
<div class="m2"><p>باشی ز همه خسروان گزین</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>جور از فلک و مردمی ز تو</p></div>
<div class="m2"><p>شعر از من و مشک از غزال چین</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>معروف بلشکر کشی شوی</p></div>
<div class="m2"><p>در شرق چو پور سبکتکین</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>مشهور بدشمن کشی شوی</p></div>
<div class="m2"><p>در غرب چو فرزند تاشفین</p></div></div>