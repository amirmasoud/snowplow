---
title: >-
    شمارهٔ ۱۳۱ - المطلع الثالث
---
# شمارهٔ ۱۳۱ - المطلع الثالث

<div class="b" id="bn1"><div class="m1"><p>آمد به صد شوخی ز در ترکی که خون‌ها ریخته</p></div>
<div class="m2"><p>خون دل یک شهر را چشمش به تنها ریخته</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون او نباشد هیچکس سالار خوبانست و بس</p></div>
<div class="m2"><p>خوبانش زین ره هر نفس سر در کف پا ریخته</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خورشید شمع خرگهش کیوان غلام درگهش</p></div>
<div class="m2"><p>جانهای شیرین در رهش طوعا و کرها ریخته</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در مکتب او جاودان آدم بود سر عشر خوان</p></div>
<div class="m2"><p>تا نقش علمه البیان بر لوح اسما ریخته</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ادریس در تدریس او شوید ورق در آب جو</p></div>
<div class="m2"><p>وز نامه خود آبرو قسطای لوقا ریخته</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>با معجز عیسی لبش با نوش احمد مشربش</p></div>
<div class="m2"><p>با دست قدرت قالبش ایزد تعالی ریخته</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بخشد تعین ذات را روزی دهد ذرات را</p></div>
<div class="m2"><p>اشباح موجودات را او در هیولا ریخته</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از حرز مریم جوشنی بر کتف عیسی دوخته</p></div>
<div class="m2"><p>وز مغز آدم عطسه ای بر خاک حوا ریخته</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>فضل همیمش صبح و شام این چار عنصر را بجام</p></div>
<div class="m2"><p>آنسان که بایستی مدام از هفت آبا ریخته</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بر کاخ نصرش ای فتی نصر من الله آیتی</p></div>
<div class="m2"><p>در جام فتحش شربتی ز انا فتحنا ریخته</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چون پرده بردارد ز رو گیرد جهان از چارسو</p></div>
<div class="m2"><p>از بس کرشمه ناز او از روی زیبا ریخته</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>روح الله آید جان بکف در درگهش با صد شعف</p></div>
<div class="m2"><p>گردد براهش از شعف خون مسیحا ریخته</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>دجالها را برکشد با صد مذلتشان کشد</p></div>
<div class="m2"><p>هم نار گبران خامشد هم آب ترسا ریخته</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>خونی که هنگام جدل در سینه کرار یل</p></div>
<div class="m2"><p>از اهل صفین و جمل وز این گوارا ریخته</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>خواهد تلافی کرد تا فرصت بدست آورد تا</p></div>
<div class="m2"><p>خونشان کند از گرد نا بر سطح غبرا ریخته</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ای مهدی صاحب زمان کز عکس تیغت آسمان</p></div>
<div class="m2"><p>رنگ شفق را جاودان بر طاق خضرا ریخته</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>لختی به محزونان نگر سوی جگرخونان نگر</p></div>
<div class="m2"><p>در ساغر دونان نگر شهد گوارا ریخته</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ما تلخکام از زهر غم خونمان شراب و طعمه هم</p></div>
<div class="m2"><p>بر خوان شومان دژم صد گونه حلوا ریخته</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>خصم ترا با آبها آمیخته جلابها</p></div>
<div class="m2"><p>ما در غمت خونابها از چشم بینا ریخته</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ای سایه مهر تو پر گسترده بر شمس و قمر</p></div>
<div class="m2"><p>وی مایه قهرت شرر بر هفت دریا ریخته</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بنما رخ چون ماه را مرآت وجه الله را</p></div>
<div class="m2"><p>و آن غمزه جانکاه را کز چشم شهلا ریخته</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>در مولدت میر اجل آراسته جشنی بی خلل</p></div>
<div class="m2"><p>وز دست او در این محل زر بی تقاضا ریخته</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>میراست یکدریا کرم میراست یک گردون همم</p></div>
<div class="m2"><p>جودش گه بخشش درم بر پیر و برنا ریخته</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ویژه بمن کز شعر تر مدح ترا خواندم ز بر</p></div>
<div class="m2"><p>دارم ببارش چون گهر ابیات غرا ریخته</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چون نیک خواندی مقطعش بشنو چهارم مطلعش</p></div>
<div class="m2"><p>تا بینی از هر مصرعش شهد مصفا ریخته</p></div></div>