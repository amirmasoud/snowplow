---
title: >-
    شمارهٔ ۱۰۵
---
# شمارهٔ ۱۰۵

<div class="b" id="bn1"><div class="m1"><p>بسی جستم نشان از اسم اعظم</p></div>
<div class="m2"><p>که بد نقش نگین خاتم جم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همه اقطار عالم سیر کردم</p></div>
<div class="m2"><p>مگر جویم نشان زان نقش خاتم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بزرگی گفت چون آدم بمینو</p></div>
<div class="m2"><p>مقر بگزید والاسماء علم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بامر ایزد این نام از ملایک</p></div>
<div class="m2"><p>بباغ خلد تلقین شد بر آدم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز آدم یافت تلقین شیث و از شیث</p></div>
<div class="m2"><p>سبق آموخت ادریس مکرم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هم از ادریس هود آمد ملقی</p></div>
<div class="m2"><p>چنان کز هود نوح آمد معلم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نمیدانست اگر این نام را نوح</p></div>
<div class="m2"><p>نجاتش کی شد از طوفان فراهم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز نوح آمد بر ابراهیم و زین نام</p></div>
<div class="m2"><p>بر او شد نار ریحان و سپر غم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بابراهیم وارث شد سماعیل</p></div>
<div class="m2"><p>که جاری زیر پایش گشت زمزم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز اسمعیل بر اسحق و یعقوب</p></div>
<div class="m2"><p>هم از یعقوب موسی گشت ملهم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز فر آن ید و بیضا و ثعبان</p></div>
<div class="m2"><p>بدست آورد و راند اندر دل یم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ز موسی یافت داود و ز داود</p></div>
<div class="m2"><p>سلیمان را شد این مسند مسلم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چو او بدرود گیتی کرد از حق</p></div>
<div class="m2"><p>رسید این راز بر عیسی بن مریم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>مسیحا گرد ازین نام همایون</p></div>
<div class="m2"><p>علاج اکمه و درمان ابکم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>هم از این نام فرخ کرد عیسی</p></div>
<div class="m2"><p>هزاران مرده را احیا یکدم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چو بردار جهودان خواندش از دار</p></div>
<div class="m2"><p>بگردون رفت بی مرقات و سلم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>پس از عیسی سروش این خاتم آورد</p></div>
<div class="m2"><p>باحمد کانبیا را بود خاتم</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>پس از احمد علی را گشت میراث</p></div>
<div class="m2"><p>که بودش نایب و صهر و پسر عم</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چو شد ریش علی با خون مخضب</p></div>
<div class="m2"><p>ز تیغ عبد رحمن بن ملجم</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>از او بر یازده فرزند پاکش</p></div>
<div class="m2"><p>رسید این خاتم از خلاق عالم</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بغیر از انبیا یا اوصیا کس</p></div>
<div class="m2"><p>بدان راز مقدس نیست محرم</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>نه از خیرالوری بشنید بوذر</p></div>
<div class="m2"><p>نه از شیر خدا آموخت میثم</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>نه از شاه خراسان شیخ معروف</p></div>
<div class="m2"><p>نه از سجاد ابراهیم ادهم</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>مگر بدبخت مردی در فلسطین</p></div>
<div class="m2"><p>ز زهاد جهان کش نام بلعم</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>که از ابلیس دستان خورد و این نام</p></div>
<div class="m2"><p>فرامش کرد و رفت اندر جهنم</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بگفتم آنچه گفتی راست گفتی</p></div>
<div class="m2"><p>سر موئی نه افزون بود و نه کم</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ولی اینان که بر خواندی من از پیش</p></div>
<div class="m2"><p>سراسر خواندم از آیات محکم</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>هم از تفسیر و ابیات بزرگان</p></div>
<div class="m2"><p>هم از گفتار دانایان اقدم</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>نخواهم من که برخوانی تواریخ</p></div>
<div class="m2"><p>ز قول حمزه و گفتار اعثم</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بخواهم آنچه نه کلبی بدانست</p></div>
<div class="m2"><p>نه مسعودی نه وصاف و نه معجم</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>برآنم کاسم اعظم را بدانم</p></div>
<div class="m2"><p>گشایم پرده زین اسرار مبهم</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>برآنم تا در این الحان کنم جفت</p></div>
<div class="m2"><p>مثانی با مثالث زیر بابم</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>چه نام است آنکه آرد شیر و شکر</p></div>
<div class="m2"><p>ز نیش عقرب و دندان ارقم</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>اگر زین راز پنهان هیچ دانی</p></div>
<div class="m2"><p>بگو ور خود نمی دانی مزن دم</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>بگفت از صدر ایوان رسالت</p></div>
<div class="m2"><p>علیه و آله صلی و سلم</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>شنیدم کاسم اعظم داند آنکس</p></div>
<div class="m2"><p>که باشد با لسان صدق توام</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>لسان الصدق را دانند مردان</p></div>
<div class="m2"><p>کلید علم حق والله اعلم</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>بگفتم کر چنین باشد که گوئی</p></div>
<div class="m2"><p>ندانم یکتن از اولاد آدم</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>که دارای لسان الصدق باشد</p></div>
<div class="m2"><p>بگیتی جز سپهسالار اعظم</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>رئیس جمع دستوران دولت</p></div>
<div class="m2"><p>سرو سردار دانایان عالم</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>خداوندی که شیر بیشه باشد</p></div>
<div class="m2"><p>به پیش پرچمش چون شیر پرچم</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>بدرد در مسالک سینه جور</p></div>
<div class="m2"><p>ببرد از مهالک پای استم</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>یکی چون اجوف واوی باعلال</p></div>
<div class="m2"><p>یکی همچون منادای مرخم</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>توئی ای میر آن ذات مقدس</p></div>
<div class="m2"><p>توئی ای خواجه آن روح مجسم</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>که گر بر دیده ی گردون نشینی</p></div>
<div class="m2"><p>ز جان گوید سپهرت خیر مقدم</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>بود دیری که در ایران سپه نیست</p></div>
<div class="m2"><p>از آن روز است چون شب تار و مظلم</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>دل مردم پر از آزار و وحشت</p></div>
<div class="m2"><p>خزانه خالی از دینار و درهم</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>ایا دست تهی آن کار کردی</p></div>
<div class="m2"><p>که از اندیشه اش مات است رستم</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>بروزی چند با فر الهی</p></div>
<div class="m2"><p>نظامی ساز کردی بس منظم</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>همه با چره تابان و دل شاد</p></div>
<div class="m2"><p>همه با جسم پاک و جان خرم</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>بدشت اندر چو آهو لیک در رزم</p></div>
<div class="m2"><p>گرفته شیر از دیدارشان رم</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>نموده خاتم زرین در انگشت</p></div>
<div class="m2"><p>فکنده حلقه سیمین بمعصم</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>فراز پیرهن خفتان رومی</p></div>
<div class="m2"><p>بزیر پیرهن دیبای معلم</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>رکاب سیم بر اسبان تازی</p></div>
<div class="m2"><p>ستام لعل بر خیل مسوم</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>شنیدستم که هارون ز آل عباس</p></div>
<div class="m2"><p>بدی بر جمله در دانش مقدم</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>شبی در کار اقلیم خراسان</p></div>
<div class="m2"><p>همی زد رای با یحیی بن اکثم</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>بیحیی گفت هارون کار آن ملک</p></div>
<div class="m2"><p>فزون از حد پریشان است و درهم</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>جوابش گفت زخمی نیست در دهر</p></div>
<div class="m2"><p>که از درهم نشاید هست مرهم</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>بدان صفرای فاتح از رگ ملک</p></div>
<div class="m2"><p>برآید ریشه سودا و بلغم</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>بود سیم سره درمان هر درد</p></div>
<div class="m2"><p>چو زر جعفری تریاق هر سم</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>تو ای میر مهین اندر چنین روز</p></div>
<div class="m2"><p>که باشد تیره همچون لیل مظلم</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>رقیبان تو در پیش تو باشند</p></div>
<div class="m2"><p>چو پیش خوشه انگور حصرم</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>و یا در بوستان نخل و رمان</p></div>
<div class="m2"><p>پیاز و گندنا و ترب و شلغم</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>چگویم ز آن تهی مغزان که دیری</p></div>
<div class="m2"><p>در افکندند طرح شور با هم</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>بجای بستن سوراخ انگشت</p></div>
<div class="m2"><p>همی کردند در سوراخ کژدم</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>ز فکر تیره شان زد بر افق چتر</p></div>
<div class="m2"><p>سحابی قیرگون پر وحشت و غم</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>ازیدر شد بساط صلح جویان</p></div>
<div class="m2"><p>سپاه جنگجویان را مخیم</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>بساط پشه بر همخورد از باد</p></div>
<div class="m2"><p>سرای مور طوفان شد ز شبنم</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>شرار فتنه آتش فزوران</p></div>
<div class="m2"><p>رسید از دامن عمان بدیلم</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>شتابیدند دزدان روز روشن</p></div>
<div class="m2"><p>بخرمنگاه و بگسستند ز استم</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>در آن سختی عنان مملکت را</p></div>
<div class="m2"><p>گرفتی سخت با بازوی محکم</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>بنای ملک و ملت راست کردی</p></div>
<div class="m2"><p>نیفکندی بطاق ابروان خم</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>غزالان سرائی را رهاندی</p></div>
<div class="m2"><p>ز دندان پلنگ و چنگ ضیغم</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>درافکندی بساط شور و عشرت</p></div>
<div class="m2"><p>فرو چیدی اساس سوگ و ماتم</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>ولی عهدی بر آدم بلکه هستی</p></div>
<div class="m2"><p>ولی نعمت بفرزندان آدم</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>سپاهت را سپهدارست جمشید</p></div>
<div class="m2"><p>بنازد از یمینت خاتم جم</p></div></div>