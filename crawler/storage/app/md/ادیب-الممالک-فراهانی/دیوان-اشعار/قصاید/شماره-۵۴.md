---
title: >-
    شمارهٔ ۵۴
---
# شمارهٔ ۵۴

<div class="b" id="bn1"><div class="m1"><p>مژده ای دل که ز ره قافله داد آمد</p></div>
<div class="m2"><p>نایب السلطنه با داد خدا داد آمد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بحر علم آمد و از گوهر تابان زد موج</p></div>
<div class="m2"><p>کوه عزم آمد و با پنجه پولاد آمد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ناصرالملک ابوالقاسم مسعود ز راه</p></div>
<div class="m2"><p>با رخی خوب و تنی پاک و دلی شاد آمد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آمد اندر مه دی با نفس فروردین</p></div>
<div class="m2"><p>چون گل و میوه که اندر مه خرداد آمد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا چو باد سحری تاخت سوی گلشن داد</p></div>
<div class="m2"><p>خیمه ظلم و جهالت همه بر باد آمد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شاد باش ای چمن ملک که چون باد بهار</p></div>
<div class="m2"><p>باغبان با نفسی گرم و کفی راد آمد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نوبهار آمد و از بوی خوش باد ربیع</p></div>
<div class="m2"><p>تهنیت باد بسرو گل و شمشاد آمد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>باغ پژمرده ما از اثر مقدم وی</p></div>
<div class="m2"><p>خوبتر از چمن خلخ و نوشاد آمد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>غم ویرانی کشور چه خوری کاین معمار</p></div>
<div class="m2"><p>بهر آبادی ایوان مه آباد آمد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>حاسدت خشت بدریا زند و سنگ بسر</p></div>
<div class="m2"><p>کاوستاد هنری بر سر بنیاد آمد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خانه و گلشن ویران شده را خواهی دید</p></div>
<div class="m2"><p>عنقریبا که ز فکرش همه آباد آمد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گشت امید برومند شود کز قدمش</p></div>
<div class="m2"><p>آب در جوی روان چون شط بغداد آمد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ای جوانان نوآموز دبستان وطن</p></div>
<div class="m2"><p>لوح تعلیم بیارید که استاد آمد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>حال و فال همه نیکو ازین خواجه راد</p></div>
<div class="m2"><p>که نکوکار و نکوخواه و نکوزاد آمد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ملک مخطوبه و او قاضی و عدلش کابین</p></div>
<div class="m2"><p>شاه مشروطه بر این بالغه داماد آمد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>سائسی چونین نادیده و ناخوانده بدیم</p></div>
<div class="m2"><p>در تواریخ و سیر کاینهمه در یاد آمد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>با دل شاد بآزادی ملت کوشد</p></div>
<div class="m2"><p>که از او شاد دل بنده و آزاد آمد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>همگنانش همه از خلق فرستاده بدند</p></div>
<div class="m2"><p>اینک آن خواجه که یزدانش فرستاد آمد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>داور داد و فرستاده دادار ار نیست</p></div>
<div class="m2"><p>زو چرا کرسی بیداد بفریاد آمد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>لرزه بر پیکر بیداد گر افتد نه عجب</p></div>
<div class="m2"><p>کاین خداوند پی مالش بیداد آمد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بیستون باشد اگر دشمن سنگین دل ما</p></div>
<div class="m2"><p>خامه او به اثر تیشه فرهاد آمد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>علم هایی که خدا ریخته در سینه وی</p></div>
<div class="m2"><p>بشمر بیش ز هفتاد و ز هشتاد آمد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>عدل در بارگهش خادم دیرینه بود</p></div>
<div class="m2"><p>عقل در پیشگهش کودک نوزاد آمد</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>مصرع مطلع مامقطع مقطوعه نکوست</p></div>
<div class="m2"><p>مژده ای دل که زره قافله داد آمد</p></div></div>