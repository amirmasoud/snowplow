---
title: >-
    شمارهٔ ۴۹ - در مدح مولای متقیان حضرت علی علیه السلام
---
# شمارهٔ ۴۹ - در مدح مولای متقیان حضرت علی علیه السلام

<div class="b" id="bn1"><div class="m1"><p>بسی جستم نشان از اسم اعظم</p></div>
<div class="m2"><p>که بد نقش نگین خاتم جم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همه اقطار عالم سیر کردم</p></div>
<div class="m2"><p>مگر جویم نشان زان نقش خاتم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بزرگی گفت چون آدم بمینو</p></div>
<div class="m2"><p>مقر بگزید و الاسماء علم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بامر ایزد این نام از ملایک</p></div>
<div class="m2"><p>بباغ خلد تلقین شد بر آدم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز آدم یافت تلقین شیث و از شیث</p></div>
<div class="m2"><p>سبق آموخت ادریس مکرم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هم از ادریس هود آمد ملقی</p></div>
<div class="m2"><p>چنان کز هود نوح آمد معلم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نمیدانست اگر این نام را نوح</p></div>
<div class="m2"><p>نجاتش کی شد از طوفان فراهم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز نوح آمد بر ابراهیم و زین نام</p></div>
<div class="m2"><p>بر او شد نار ریحان و سپر غم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به ابراهیم وارث شد سماعیل</p></div>
<div class="m2"><p>که جاری زیر پایش گشت زمزم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز اسمعیل بر اسحق و یعقوب</p></div>
<div class="m2"><p>هم از یعقوب موسی گشت ملهم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز فر آن ید و بیضا و ثعبان</p></div>
<div class="m2"><p>بدست آورد و راند اندر دل یم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ز موسی یافت داود و ز داود</p></div>
<div class="m2"><p>سلیمان را شد این مسند مسلم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چو او بدرود گیتی کرد از حق</p></div>
<div class="m2"><p>رسید این راز بر عیسی بن مریم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>مسیحا کرد ازین نام همایون</p></div>
<div class="m2"><p>علاج اکمه و درمان ابکم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>هم از این نام فرخ کرد عیسی</p></div>
<div class="m2"><p>هزاران مرده را احیا بیکدم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چو بردارم جهودان خواندش از دار</p></div>
<div class="m2"><p>بگردون رفت بی مرقات و سلم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>پس از عیسی سروش این خاتم آورد</p></div>
<div class="m2"><p>به احمد کانبیا را بود خاتم</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>پس از احمد علی را گشت میراث</p></div>
<div class="m2"><p>که بودش نایب و صهر و پسر عم</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چو شد ریش علی با خون مخضب</p></div>
<div class="m2"><p>ز تیغ عبد رحمن بن ملجم</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>از او بر یازده فرزند پاکش</p></div>
<div class="m2"><p>رسید این خاتم از خلاق عالم</p></div></div>