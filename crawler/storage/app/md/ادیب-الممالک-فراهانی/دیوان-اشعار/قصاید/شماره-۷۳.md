---
title: >-
    شمارهٔ ۷۳
---
# شمارهٔ ۷۳

<div class="b" id="bn1"><div class="m1"><p>خسرو شرق سوی غرب همیکرده سفر</p></div>
<div class="m2"><p>باختر گشته ز نو مطلع مهر خاور</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ابر و باد ار نبود توسن فرخ پی شه</p></div>
<div class="m2"><p>از چه پیماید کوه و کند از بحرگذر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ورنه شمس و قمرست اینملک چرخ سریر</p></div>
<div class="m2"><p>از چه رو گرد زمین گردد چون شمس و قمر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ورنه اسکندر شرق است شهنشاه جهان</p></div>
<div class="m2"><p>گرد آفاق چرا گردد چون اسکندر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شاه ما را ملکان نیک پذیرند ازان</p></div>
<div class="m2"><p>که فراگیرند از حکمتش آداب و سیر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شاه ما عاقله دور زمان است و زمان</p></div>
<div class="m2"><p>تربیت یابد از آن شاه معالی گستر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون ملک عزم سفر کرد کلید در ملک</p></div>
<div class="m2"><p>داد در دست ملک زاده فرخنده گهر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پادشه زاده پیروز جوانبخت سعید</p></div>
<div class="m2"><p>شه محمدعلی آن درخور دیهیم و کمر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شاه اندر کف وی داده مقالید امور</p></div>
<div class="m2"><p>که کند کار جهان راست بنیروی هنر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خوبکرد الحق زیرا که کسی چون فرزند</p></div>
<div class="m2"><p>نیست در گیتی غمخوار و هواخواه پدر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ویژه این پور گرامی که میان پسران</p></div>
<div class="m2"><p>آنچنان است که اندر همه اعضا سر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گرچه اولاد شهنشه همه اعضای ویند</p></div>
<div class="m2"><p>هیچ عضوی را با سر نتوان شد همسر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>این ملکزاده بنامیزد مانند سر است</p></div>
<div class="m2"><p>که بود مرکز هوش و خرد و سمع و بصر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>او دل و مغز و جگر باشد و دیگر اعضا</p></div>
<div class="m2"><p>همه هستند بفرمان دل و مغز و جگر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>در فلک ثابت و سیار فزون است ولی</p></div>
<div class="m2"><p>همچو خورشید فروزان نبود یک اختر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>هنر شه ز ولیعهد پدید است آری</p></div>
<div class="m2"><p>هنر تیغ پدیدار بود از جوهر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ایکه بخشیدت یزدان پی آسایش خلق</p></div>
<div class="m2"><p>دو کف عقده گشایی و دو لب جان پرور</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>نایب شاه توئی باخبر از راه توئی</p></div>
<div class="m2"><p>مرد آگاه توئی بر تو عیان است خبر</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>نشود چشم تو مخمور ز صهبای هوس</p></div>
<div class="m2"><p>نه شود قلب تو مجروح ز شمشیر نظر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>سخن بهتان هرگز نبرد سوی تو راه</p></div>
<div class="m2"><p>جادوی دیوان هرگز نکند در تو اثر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چشم بینا دل دانا لب گویا داری</p></div>
<div class="m2"><p>راستی تو همه جانی و جهان چون پیکر</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>دانم افرشته نه ای لیک از آنم بشگفت</p></div>
<div class="m2"><p>که سرشت تو بود پاکتر از جنس بشر</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>تا بود کشور جم قاعده ملک عجم</p></div>
<div class="m2"><p>تا بود دست مظفرشه مفتاح ظفر</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>آسمان نازد بر ماه و زمین بر رخ تو</p></div>
<div class="m2"><p>تو بدیهیم شه و شه بعطای داور</p></div></div>