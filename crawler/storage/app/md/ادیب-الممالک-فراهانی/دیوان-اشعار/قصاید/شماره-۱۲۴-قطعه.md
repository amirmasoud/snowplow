---
title: >-
    شمارهٔ ۱۲۴ - قطعه
---
# شمارهٔ ۱۲۴ - قطعه

<div class="b" id="bn1"><div class="m1"><p>از دو چشمم آب یکسر گشته جاری خون ز یکسو</p></div>
<div class="m2"><p>دست و پایم بسته دین از یکطرف قانون ز یکسو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قامتم را کوژ دارد خون دل از دیده بارد</p></div>
<div class="m2"><p>آن قد موزون ز سوئی و آن رخ گلگون ز یکسو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بسته عهد اتفاق اندر پی تاراج دلها</p></div>
<div class="m2"><p>غمزه جانان ز سوئی گردش گردون ز یکسو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دست و پیمان داده با هم بر سر ویرانی ما</p></div>
<div class="m2"><p>اختر کجرو ز سوئی طالع وارون ز یکسو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر زمان نقشی عجب بر چهره ما برنگارد</p></div>
<div class="m2"><p>دهر بازیگر ز سوئی چرخ بوقلمون ز یکسو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کارمان افتاده با بیمار مهجوری که جانش</p></div>
<div class="m2"><p>خسته دارد تب ز سوئی تلخی معجون ز یکسو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>درد او را چاره نتوان کرد با جلاب و دارو</p></div>
<div class="m2"><p>گر ارسطو کوشد از سوئی و افلاطون ز یکسو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>حاصل از رنج طبیبان نیست کو را کشته دارد</p></div>
<div class="m2"><p>دردهای اندرون سوئی غم بیرون ز یکسو</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آن مریضی را که عزرائیل فرماید عیادت</p></div>
<div class="m2"><p>حال دیگر شد ز سوئی کار دیگرگون ز یکسو</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کی تواند زیست بیماری که جانش را بکاهد</p></div>
<div class="m2"><p>طعنه طاعن ز سوئی حمله طاعون ز یکسو</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چون توان رستم ازین سیلاب بنیان کن که دایم</p></div>
<div class="m2"><p>دیده بارد اشک سوئی دل فشاند خون ز یکسو</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>کشتی ما غرقه در دریا و تن محبوس هامون</p></div>
<div class="m2"><p>باد در دریا ز سوئی سیل در هامون ز یکسو</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>آفتابا در مدار خویش گردش کن که ترسم</p></div>
<div class="m2"><p>مرکزت از یکطرف ویران شود کانون ز یکسو</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>از پی تخریب ناموس تو ای خورشید روشن</p></div>
<div class="m2"><p>مشتری از یک طرف طغیان کند نپتون ز یکسو</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>آبی ام ابر کرم افشان بر این آتش که سوزد</p></div>
<div class="m2"><p>خیمه لیلی ز سوئی پیکر مجنون ز یکسو</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ای رسول هاشمی بردار سر اسلام را بین</p></div>
<div class="m2"><p>نالد از عیسی ز سوئی وز حواریون ز یکسو</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بر هلاک شیعه آل محمد گشته جازم</p></div>
<div class="m2"><p>لشکر لوقا ز سوئی امت شمعون ز یکسو</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>وز پی نسخ کتاب ما فراز آرد کتائب</p></div>
<div class="m2"><p>سفر یوحنا ز سوئی صحف انگلیون ز یکسو</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>دوست از راهی بکین ما و دشمن از طریقی</p></div>
<div class="m2"><p>پطر یکسو در کمین ما و ناپلیون ز یکسو</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>باد از جائی خرابم می کند باران ز جائی</p></div>
<div class="m2"><p>کنت از سوئی کبابم می کند بارون ز یکسو</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>هر چه در جیب عجائز بود و در کیس ارامل</p></div>
<div class="m2"><p>راه آهن از طریقی می برد واگون ز یکسو</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>سرمه یکجا برده هوشم غمزه مشاطه یکجا</p></div>
<div class="m2"><p>غازه از یکسو فریبم می دهد صابون ز یکسو</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>پاسبان یکجا دل از کف داده و دربان ز جائی</p></div>
<div class="m2"><p>خواجه سوئی مست خواب افتاده و خاتون ز یکسو</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>سامری گوساله را بر تخت بنشاند چو بیند</p></div>
<div class="m2"><p>غیبت موسی ز سوئی غفلت هارون ز یکسو</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>وای بر داود از آن ساعت که دید از لشکر خود</p></div>
<div class="m2"><p>باغ دین ویران ز سوئی داغ ایسالون ز یکسو</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ای دریغا رفت آن قصری که بود اندر کنارش</p></div>
<div class="m2"><p>دامن قلزم ز سوئی ساحل جیحون ز یکسو</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ای دریغا رفت آن گنجی که بروی رشک بردی</p></div>
<div class="m2"><p>دست موسی یکطرف گنجینه قارون ز یکسو</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>آنچه کالای شرف بد یا متاع آدمیت</p></div>
<div class="m2"><p>چرخ دون پرور ز سوئی برد و خصم دون ز یکسو</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>زین تجارت آتشم در دل فروزد چونکه بینم</p></div>
<div class="m2"><p>سود سوداگر ز سوئی حسرت مغبون ز یکسو</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>ای دریغا کرد غارت آنچه بود اندر عمارت</p></div>
<div class="m2"><p>غاصب مردود یکسو صاحب ملعون ز یکسو</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>مغزهامان را پریشان کرده دلهامان مکدر</p></div>
<div class="m2"><p>سبزه روشن ز سوئی شیره افیون ز یکسو</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>چشمها گه مست افیونند و گاهی مست باده</p></div>
<div class="m2"><p>گوشها ز افسانه سوئی گرم و از افسون ز یکسو</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>گر فشانم زنده رود از دیده جا دارد که دارم</p></div>
<div class="m2"><p>غصه اهواز از سوئی غم کارون ز یکسو</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>گردمان دیواری از بدبختی و غفلت کشیده</p></div>
<div class="m2"><p>فقر بی پایان ز سوئی قرض سی ملیون ز یکسو</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>ترسم ای ایرانیان تورانیان را قسمت افتد</p></div>
<div class="m2"><p>تخت کیخسرو ز سوئی تاج افریدون ز یکسو</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>بیستون از یکطرف نالد دل فرهاد یکجا</p></div>
<div class="m2"><p>تخت شیرین یکطرف غلطد سم گلگون ز یکسو</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>نوعروس ملک را کابین کنند از بهر خصمان</p></div>
<div class="m2"><p>اعتدالیون ز سوئی انقلابیون ز یکسو</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>ای امیری بر دو چیز امیدواری منحصر شد</p></div>
<div class="m2"><p>همت ملت ز سوئی رحمت بیچون ز یکسو</p></div></div>