---
title: >-
    شمارهٔ ۳۲
---
# شمارهٔ ۳۲

<div class="b" id="bn1"><div class="m1"><p>تا در میان اوباش تقسیم شد وزارت</p></div>
<div class="m2"><p>کردند مملکت را سرمایه تجارت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>طلاب گرسنه را خواندند از حماقت</p></div>
<div class="m2"><p>در مسند شرافت از مرکز حقارت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شد آن خبیث اقطع قطاع رزق مردم</p></div>
<div class="m2"><p>کرد آن پلید اعور در کارها نظارت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شیخی که بر وظیفه چون سگ دوان بجیفه</p></div>
<div class="m2"><p>میکرد از قطیفه پیراهن استعارت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در یک دو روز کامد در مجلس مقدس</p></div>
<div class="m2"><p>خود را نمود داخل در شور و استشارت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بنمود روز دیگر آکنده کیسه از زر</p></div>
<div class="m2"><p>هم اسب و هم درشکه هم باغ و هم عمارت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آن دلبران شاهد در کسوت مجاهد</p></div>
<div class="m2"><p>ساعی شدند و جاهد اندر پی امارت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شد کار و کسب احزاب حمالی وزیران</p></div>
<div class="m2"><p>شغل وزیر بی پیر دلالی سفارت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شد دفتر اساسی فرموش با برودت</p></div>
<div class="m2"><p>وآن کله سیاسی خاموش از حرارت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از مجلس مقدس کنده دم وکالت</p></div>
<div class="m2"><p>در پیشگاه اقدس بسته در صدارت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>اردوی شهریاری مشغول نهب و تاراج</p></div>
<div class="m2"><p>سردار بختیاری سرگرم قتل و غارت</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نه کاهلی نمودند از غارت و چپاول</p></div>
<div class="m2"><p>نه کوتهی نمودند از کشتن و اسارت</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>زین خلق زشت عادت باشد زهی سعادت</p></div>
<div class="m2"><p>شداد را عبادت حجاج را زیارت</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ضحاک اگر شود شاه از این بساط و خرگاه</p></div>
<div class="m2"><p>پیچد بگنبد ماه آوازه بشارت</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>باشد وزیر خائن سرچشمه رذالت</p></div>
<div class="m2"><p>چونانکه شد مجاهد سردسته شرارت</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>خواندند مشت جهال یا مرگ یاستقلال</p></div>
<div class="m2"><p>و اندر زبان اطفال تلقین شد این عبارت</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>گفتند مدعی را کز بهر بردن ملک</p></div>
<div class="m2"><p>از ما بسر دویدن از تو بیک اشارت</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>دشمن بخانه ما ناخوانده گشت وارد</p></div>
<div class="m2"><p>خورد و درید و چاپید با تندی و جسارت</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>از ظلم و جور و بیداد ناهشته جای آباد</p></div>
<div class="m2"><p>بعد از خراب بغداد خواهد ز ما خسارت</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>یارب حلاوت امن بر ما چشان که امروز</p></div>
<div class="m2"><p>افتاده ایم از رنج در ورطه مرارت</p></div></div>