---
title: >-
    شمارهٔ ۱۲۸
---
# شمارهٔ ۱۲۸

<div class="b" id="bn1"><div class="m1"><p>باد نوروزی ز روی گل نقاب انداخته</p></div>
<div class="m2"><p>زلف سنبل را همی در پیچ و تاب انداخته</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در رکاب فرودین بر رغم اسفندار مذ</p></div>
<div class="m2"><p>خون سرما را همی اندر رکاب انداخته</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سایه سرو جوان بر طرف باغ و جویبار</p></div>
<div class="m2"><p>نیکویها کرده است اما اندر آب انداخته</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا شقایق باده اندر ساغر گلرنگ ریخت</p></div>
<div class="m2"><p>نرگس مخمور را مست و خراب انداخته</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>باده چون خون سیاوش ده که کاوس بهار</p></div>
<div class="m2"><p>آتش اندر خیمه افراسیاب انداخته</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سرخ گل ماند عروسی را که هنگام زفاف</p></div>
<div class="m2"><p>جامه گلگون کرده دست اندر خضاب انداخته</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>لاله ترکی مست را ماند قدح پر می بدست</p></div>
<div class="m2"><p>کرده رخ گلگون بسر شور از شراب انداخته</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نرگس اندر شاخ زمردگون و صحن سیمگون</p></div>
<div class="m2"><p>سونش زر در دل تبر مذاب انداخته</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>و آن شقایق بر زبرجد درجی از یاقوت داشت</p></div>
<div class="m2"><p>در دل آن دانها از مشک ناب انداخته</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گر به بید اندر چمن چون زاهدی پشمینه پوش</p></div>
<div class="m2"><p>طیلسان خز بروی از بهر خواب انداخته</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>قاقم دی را که برفستی هوا از هم درید</p></div>
<div class="m2"><p>نک بدوش خویش سنجاب از سحاب انداخته</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>باد مشاطه است بستان را که در طرف چمن</p></div>
<div class="m2"><p>از عذار سوری و نسرین حجاب انداخته</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نامیه چون مادران مهربان بر دوش و بر</p></div>
<div class="m2"><p>شاهدان باغ را رنگین ثیاب انداخته</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بر سر این شاهدان ابر بهاری بامداد</p></div>
<div class="m2"><p>از نثار قطره لؤلؤی خوشاب انداخته</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>خیمه سرخی که شاخ ارغوان در باغ زد</p></div>
<div class="m2"><p>زلف سنبل را در او همچون طناب انداخته</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>سبزه فرش از سبز دیبا بر لب شط گسترید</p></div>
<div class="m2"><p>ابر مشکین کله بر نیلی قباب انداخته</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>فرش بوقلمون همی گسترد طاوس بهار</p></div>
<div class="m2"><p>وز سحاب اندر هوا پر غراب انداخته</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گردی از مستی برات نوگلان بر یخ نوشت</p></div>
<div class="m2"><p>فرودینشان جامه در دریای آب انداخته</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چنگ زن بلبل بگل برنای زن قمری بسرو</p></div>
<div class="m2"><p>هر یکی شوری بنوروز از رهاب انداخته</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>تا بعود اندر چکاوک ماوراء النهر ساخت</p></div>
<div class="m2"><p>خواب در مغز حکیم فاریاب انداخته</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>سار الحان ثمانی ساخت بطلمیوس وار</p></div>
<div class="m2"><p>کبک در صحرا نواها از رباب انداخته</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>همچو ماه فروردین در باغ شد دلدار من</p></div>
<div class="m2"><p>لشکر سرو و سمن را در رکاب انداخته</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>چون گل و سنبل که با هم توام آید در چمن</p></div>
<div class="m2"><p>گیسوان کرده پریش از رخ نقاب انداخته</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>هاله بر گرد مه آید ای عجب کان مشکموی</p></div>
<div class="m2"><p>هاله مشکین بگرد آفتاب انداخته</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>روزه چشم پر ز نازش راز مستی دوخته</p></div>
<div class="m2"><p>ذکر حق لعل لبش را از عتاب انداخته</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>زحمت تکلیف رنگش کرده همچون شنبلید</p></div>
<div class="m2"><p>طاعت یزدان تنش در التهاب انداخته</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>گفتم ای شیرین زبان بگشای بامی روزه را</p></div>
<div class="m2"><p>کت همی بینم ریاضت در عذاب انداخته</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>با گلاب می خمار روزه بیرون کن ز سر</p></div>
<div class="m2"><p>چند بینم عارضت بر گل گلاب انداخته</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>گفت اگر امروز من فرمان حق را نگروم</p></div>
<div class="m2"><p>حق تعالی داوری را در حساب انداخته</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>گفتم اهلا شادمان زی کاین حساب اندر حساب</p></div>
<div class="m2"><p>حضرت داور بدست بوتراب انداخته</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>بوتراب است آنکه رشگ خاک پایش چرخ را</p></div>
<div class="m2"><p>در غم یا لیتنی کنت تراب انداخته</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>قهرش اندر خاندان دشمنان آوازها</p></div>
<div class="m2"><p>از لدوا للموت و ابنوا للخراب انداخته</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>گوش و چشم و هوش را بی رخصت وی کردگار</p></div>
<div class="m2"><p>صم عمی بکم چون شرالدواب انداخته</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>بوالبشر عریان ز هستی کو ردای افتخار</p></div>
<div class="m2"><p>بر برو دوش قصی بن کلاب انداخته</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>نقش یمحوالله و یثبت مایشاء را خامه اش</p></div>
<div class="m2"><p>بر کف من عنده علم الکتاب انداخته</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>معجز لعل لب و جادوی چشمش آشکار</p></div>
<div class="m2"><p>فلسفی را همچو خر اندر خلاب انداخته</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>بلعم و ابلیس را مهجوری درگاه او</p></div>
<div class="m2"><p>از کرامت وز دعای مستجاب انداخته</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>در چنین روزیکه نوروز است عدلش در جهان</p></div>
<div class="m2"><p>آشکارا مسند فصل الخطاب انداخته</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>او چو خورشید است و ما سیارگان بر گردوی</p></div>
<div class="m2"><p>طرح این سیارگان را آفتاب انداخته</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>لاجرم زی مرکز این اجرام را لاینقطع</p></div>
<div class="m2"><p>گرم تک در اندفاع و انجذاب انداخته</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>هر یکی را در مداری مستوی بر گرد خویش</p></div>
<div class="m2"><p>گاه اندر بطو و گاه اندر شتاب انداخته</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>دست یزدان است و سامان داده کار ملک از آنک</p></div>
<div class="m2"><p>کار را در دست میر کامیاب انداخته</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>آن خداوندیکه فلک را بحر کفش</p></div>
<div class="m2"><p>گاه طوفان سوی بالا چون حباب انداخته</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>چون بپرد مرغ تیرش نسر طاثر را در آن</p></div>
<div class="m2"><p>در تطیر از اذا کان الغراب انداخته</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>درگه وی آفتابستی و دیگر اختران</p></div>
<div class="m2"><p>همچو حربا رخ بر این والاجناب انداخته</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>ابر دستش آنچنان بادر بهنگام کرم</p></div>
<div class="m2"><p>کابر نیسان را همی از فر و آب انداخته</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>تا سر شیر فلک را بشکند در مغز چرخ</p></div>
<div class="m2"><p>سنگ خورشید است گوئی در جراب انداخته</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>نیزه دلدوز و تیر جانشکافش خصم را</p></div>
<div class="m2"><p>گه نیازک بر فروزد گه شهاب انداخته</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>عهد پیروزش که هر روزیش نوروزی بود</p></div>
<div class="m2"><p>چرخ را در یاد ایام شباب انداخته</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>مفرش امن و امان گسترده در پهنای خاک</p></div>
<div class="m2"><p>فتنه را در دیدگان داروی خواب انداخته</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>ای خداوندیکه دست حق رقیبان ترا</p></div>
<div class="m2"><p>طوق حبل من مسد اندر رقاب انداخته</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>حمله خشمت در صف پیلان هند اندر زده</p></div>
<div class="m2"><p>لرزه بیمت در تن شیران غاب انداخته</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>تا سنانت چشم پیلان از طعان بردوخته</p></div>
<div class="m2"><p>تا حسامت تاب شیران در ضراب انداخته</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>پیل همچون پیل شطرنج است ستخوان خشکریش</p></div>
<div class="m2"><p>شیر همچون شیر دیوار است ناب انداخته</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>تا کمالت را فلک، او فر، نصیب آورده بهر</p></div>
<div class="m2"><p>تا جلالت را سپهر اندر نصاب انداخته</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>توأم بختی و سهمت را معلی و رقیب</p></div>
<div class="m2"><p>آسمان اندر سهام و در کعاب انداخته</p></div></div>