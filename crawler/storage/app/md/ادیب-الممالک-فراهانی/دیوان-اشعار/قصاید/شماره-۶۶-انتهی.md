---
title: >-
    شمارهٔ ۶۶ - انتهی
---
# شمارهٔ ۶۶ - انتهی

<div class="b" id="bn1"><div class="m1"><p>چو بی وجود خداوندگار آسایش</p></div>
<div class="m2"><p>حرم بود بخرد و بزرگ این کشور</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دوباره نامه نبشتند مفتیان مهین</p></div>
<div class="m2"><p>بمیر فرخ دانش پژوه دانشور</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>که ای بروشنی و فرخی شده مشهور</p></div>
<div class="m2"><p>فروغ تیغ تو و تاب مهر و نور قمر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بیا که فتنه برافروخت آتشی و بسوخت</p></div>
<div class="m2"><p>روان خاصان همچون سپند در مجمر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بیا که جان خلایق بسوخت زین آتش</p></div>
<div class="m2"><p>سپس بباد فنا رفت جمله خاکستر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بیا که مآء معینمان ز بس کدورت یافت</p></div>
<div class="m2"><p>ازین بلاد همی برکنیم آبشخور</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بیا و خانمان از تندباد غم برهان</p></div>
<div class="m2"><p>بیا و جانمان از ترکتاز فتنه بخر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بیا که بیتو خراب است دولت و دین</p></div>
<div class="m2"><p>بیا که بیتو حرامست خواب و راحت و خور</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بروز حادثه چشم جهان بحضرت تو است</p></div>
<div class="m2"><p>تو نیز میرا یکره بسوی ما بنکر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چو میر گشت ازین قصه شگفت آگاه</p></div>
<div class="m2"><p>چو خواجه گشت ازین واقعات مستحضر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بخشم برشد و بنوشت کامدم اینک</p></div>
<div class="m2"><p>ز جای درشد و فرمود میرسم ایدر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بخواستم که نباشد دلی ز من مجروح</p></div>
<div class="m2"><p>بگفتمی که نگردد تنی ز من مضطر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>لجاج کردند این سفلگان بیدانش</p></div>
<div class="m2"><p>خلاف جستند این جاهلان دون پرور</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>فریب دیوان کرده است عقلشان مختل</p></div>
<div class="m2"><p>غرور شیطان بنموده فعلشان منکر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بیامدم هان با توپهای آتشبار</p></div>
<div class="m2"><p>رسیدم اینک با تیغهای خارادر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>همان کنم که بعباسیان هلاکوخان</p></div>
<div class="m2"><p>همان کنم که بمروانیان ابوجعفر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>دگر نوشت بفرماندهان مسند شرع</p></div>
<div class="m2"><p>که می بباشید امروز ملک را یاور</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>نگاه دارید این چند روزه کشور را</p></div>
<div class="m2"><p>برون نمائید آشوب را از این کشور</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>سپس بباره ی که پیکری نشست که برد</p></div>
<div class="m2"><p>بگاه پویه سبق از سحاب و از صرصر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بسرعت برق آن باد پاروان شد و بود</p></div>
<div class="m2"><p>خدایگان بفرازش چو گنج بادآور</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>همی بتاخت ز بیجار تا بقرمیسین</p></div>
<div class="m2"><p>چنانکه تاخت علی از مدینه تا خیبر</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بتندی آمد همچون دم شمال و صبا</p></div>
<div class="m2"><p>بچابکی شد ماننده سحاب و مطر</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>چنانکه سیل ز بالای که فرود آید</p></div>
<div class="m2"><p>فرود آمد از آن خاره کوب که پیکر</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>نشست در صف ایوان و بارعام بداد</p></div>
<div class="m2"><p>نشسته گرد و نیاسوده تن ز رنج سفر</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بخواند یکسره میران و نامداران را</p></div>
<div class="m2"><p>ابا فقیهان وان فاضلان دانشور</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بنزد میر نشستند جمله صورت وار</p></div>
<div class="m2"><p>که میر بدهمه معنی و دیگران چو صور</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>پس از نشستن فرمود جمله میدانید</p></div>
<div class="m2"><p>که من نکرده ام از کار ملک صرف نظر</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>براه دولت چون توتیا بدیده کشم</p></div>
<div class="m2"><p>اگر بکارند اندر رهم همی نشتر</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>ز دست ندهم آسایش رعیت را</p></div>
<div class="m2"><p>وگر ببارند اندر سرم همی خنجر</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>ندیده بودم و پنداشتم که این مردم</p></div>
<div class="m2"><p>بزهد و صدق چو بن یاسرند یا بوذر</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>کنون بدیدم و دانستم من بدین سفهاء</p></div>
<div class="m2"><p>که ناستوده فعالند و ناخجسته سر</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>هزار مرتبه گفتم که از عتاب ملوک</p></div>
<div class="m2"><p>حذر کنید و بگیرید از گذشته عبر</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>چرا بباید در بوستان درختی کاشت</p></div>
<div class="m2"><p>که خشم شاه جهان باشدش بشاخ ثمر</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>کسیکه نعمت شه راهمی کند کفران</p></div>
<div class="m2"><p>روا یباشدش الا بتیغ کین کیفر</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>کسیکه چشمه انصاف با گل آلاید</p></div>
<div class="m2"><p>حرام باشدش الا بجام خون جگر</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>بر آن سرم که مراین جمله را فراخورکار</p></div>
<div class="m2"><p>سزا دهم که بهر کار شد سزا در خور</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>سزای معروف ایدر همی بود معروف</p></div>
<div class="m2"><p>جزای منکر ایدر همی بود منکر</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>ای آنکه حنظل کشتی ببوستان امل</p></div>
<div class="m2"><p>ترا نشاید شکر درود رنج مبر</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>ای آنکه تخم شکر کاشتی بباغ مراد</p></div>
<div class="m2"><p>نصیب تو همه شهد آمده است غصه مخور</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>من این حدیث ز اصحاب میر بشنودم</p></div>
<div class="m2"><p>که خویش بودم بیخویش خفته در بستر</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>که آن امیر مهین دام ظله العالی</p></div>
<div class="m2"><p>چو برگرفت ز کردار این گروه شمر</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>به پیشوایان فرمود کای وجود شما</p></div>
<div class="m2"><p>ز فضل زینت محراب و زیور منبر</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>چو بر وظیفه خود بوده اید راهنما</p></div>
<div class="m2"><p>چو بر طریقه حق گشته اید راه سپر</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>دل ملک ز شما شاد و جان ما خرسند</p></div>
<div class="m2"><p>خدای راضی و خرم روان پیغمبر</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>دگر بخواست مر آن میر پنجه را و بخواند</p></div>
<div class="m2"><p>ز روی لطف بر او آفرین بیحد و مر</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>چه گفت؟ گفت بزرگت کنم بدیده خلق</p></div>
<div class="m2"><p>چنانکه دیری افکنده بودمت ز نظر</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>امیر پنجه بدی تاکنون ولی زین پس</p></div>
<div class="m2"><p>امیر تومان باشی هماره بر لشکر</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>بجای آنکه گرفتی زمام صبر بکف</p></div>
<div class="m2"><p>بجای آنکه گذشتی ز اهل و مال و پسر</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>ز عز و فخر بپوشانمت یکی دیبا</p></div>
<div class="m2"><p>ز لطف و فضل بنوشانمت یکی ساغر</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>از آن لباس برانی هراس را از دل</p></div>
<div class="m2"><p>از آن شراب بگیری شباب را از سر</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>سپس ببارش با دست بسته آوردند</p></div>
<div class="m2"><p>کسان که بودند اصل فساد و مبدع شر</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>نژند حال چو در روز واپسین مشرک</p></div>
<div class="m2"><p>سیاه چهره چو در عرصه جزا کافر</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>امیر اعظم لختی برویشان نگریست</p></div>
<div class="m2"><p>که تا نماید مخبر از منظر</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>بیک نظاره بر او کشف شد حقیقت امر</p></div>
<div class="m2"><p>که میر کشف حقایق کند بنیم نظر</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>زبانه غضب میر باز باینه گفت</p></div>
<div class="m2"><p>که این خسان را بر جان همی زنید شرر</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>فروخت باید در نارشان چو هیزم خشک</p></div>
<div class="m2"><p>که بر بزرگان بفروختند هیزم تر</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>چو این بگفت ز پی در شدند دژخیمان</p></div>
<div class="m2"><p>کشان کشان بربودندشان ز پیش اندر</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>بکام توپ ببستند پشتشان و آنگاه</p></div>
<div class="m2"><p>یکی نهیب برآمد مهیب چون تندر</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>نعوذبالله پنداشتی که پیلی را</p></div>
<div class="m2"><p>همی بخاید و بیرون کند ز کام اژدر</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>و یا ز بالا ابری دمید صاعقه بار</p></div>
<div class="m2"><p>فراز خاک ببارید دست و گردن و سر</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>خروش آنچو ز سر حد ملک روم گذشت</p></div>
<div class="m2"><p>بلرزه درشد و افتاد بر زمین قیصر</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>ز بیم میر همی زرد چهره شد آشوب</p></div>
<div class="m2"><p>ز باس او بدن فتنه شد بسی لاغر</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>همی رمید در این وقعه مادر از فرزند</p></div>
<div class="m2"><p>همی گریخت در این ماجرا پدر ز پسر</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>کناره کرد ز اندیشه کودک از پستان</p></div>
<div class="m2"><p>کرانه جست بیکباره عاشق از دلبر</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>بقطره خون تبدیل گشت و کرد فرار</p></div>
<div class="m2"><p>جنین به پشت پدر از مشیمه مادر</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>بداد بهرام آن روز تاج کیوان را</p></div>
<div class="m2"><p>بزهره تا بعوض بخشدش یکی معجر</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>نهان شدند همه شوهران برخت زنان</p></div>
<div class="m2"><p>زنان شهر بریدند امید از شوهر</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>خبر رسید بدرگاه میر کز بیمت</p></div>
<div class="m2"><p>تنی نماند که ماند روانش در پیکر</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>همه ز بیم تو قالب تهی نمودستند</p></div>
<div class="m2"><p>اگر امان ندهیشان تهی شود کشور</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>امیر و فقه الله لکسب مرضاته</p></div>
<div class="m2"><p>چو از حقیقت این داستان گرفت خبر</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>دلش بسوخت بر احوال ساکنان دیار</p></div>
<div class="m2"><p>ز بس رحیم دلستی و مردمی پرور</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>گرفت خامه مشکین بدست گوهربار</p></div>
<div class="m2"><p>همی فشاند بسیمین پرند عنبر تر</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>رقم زد از پی تحمید کردگار که هان</p></div>
<div class="m2"><p>قبای عفو نمودیم زیب پیکر و بر</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>همه گناه گنهکارگان ببخشودیم</p></div>
<div class="m2"><p>ز جرمهاشان شد غمض عین و صرفنظر</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>بزینهار شهستند این گنهکاران</p></div>
<div class="m2"><p>نه مضطرب بزیند از هراس و نه مضطر</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>دعا کنند ز جان بر خدایگان ملوک</p></div>
<div class="m2"><p>که پادشاه کریم است و معدلت گستر</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>چو این رقیمه رقم زد بنان فرخ میر</p></div>
<div class="m2"><p>خطیب برد و بجامع بخواند در منبر</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>همه بدولت شاه جهان دعا کردند</p></div>
<div class="m2"><p>سپس بمیر که از جرمشان نمود گذر</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>شبی بحضرت میر اجل نشسته بدم</p></div>
<div class="m2"><p>که خادمی بدر آمد چو آفتاب از در</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>سجود کرد بر آن آسمان فضل و کرم</p></div>
<div class="m2"><p>نماز برد بر آن آستان جاه و خطر</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>بدستش اندر فرمان شاه و پنداری</p></div>
<div class="m2"><p>گرفته بود سها آفتاب را در بر</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>و یا تو گفتی جبریل بود و از بالا</p></div>
<div class="m2"><p>فرود آمد و آورد نامه داور</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>ز جای جست خداوند و خم شد از تعظیم</p></div>
<div class="m2"><p>نهاد سر بخط شاه آفتاب افسر</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>سپس بدیده همی سود و بر گرفتش مهر</p></div>
<div class="m2"><p>یکی صحیفه نظر کرد پر لئال و درر</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>نبشته بود در آن شهریار ملک ستان</p></div>
<div class="m2"><p>که ای امیر هریمن کش فریشته فر</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>بلطف ما همه اوقات باش خرم دل</p></div>
<div class="m2"><p>بفضل ما همه ایام باش مستظهر</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>شنیده ایم که از مرکز حکومت خویش</p></div>
<div class="m2"><p>شدی بدیدن یاران و دوستان حضر</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>سفر گزیدی چندی بسوی موطن خود</p></div>
<div class="m2"><p>چنان که شد بسوی بیشه شیر شرزه نر</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>بخواستی که در آنجا دمی بیاسائی</p></div>
<div class="m2"><p>ز کید گنبد گردون و طارم اخضر</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>ز وصل یاران یابی بذوق لذت و کام</p></div>
<div class="m2"><p>ز روی خویشان گیری بشوق بهره و بر</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>هنوز روی عزیزان بکام نادیده</p></div>
<div class="m2"><p>نچیده نوز ز گلزار امن و عیش ثمر</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>خبر رسیدت کاشوب مشتعل گردید</p></div>
<div class="m2"><p>ز فتنه در صف کرمانشهان فتاد شرر</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>ز عیش رستی و افراختی بر و کوپال</p></div>
<div class="m2"><p>ز جای جستی و نشناختی تو پای از سر</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>کرانه جستی و مایل شدی ز عیش و نشاط</p></div>
<div class="m2"><p>کناره کردی و غافل شدی ز راحت و خور</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>بصد شتاب ز بیجار سوی قرمیسین</p></div>
<div class="m2"><p>همی روانه شدی از طریق دیناور</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>بروزگار شدی همره شتاب و عجل</p></div>
<div class="m2"><p>بشام تار بدی همسر سهاد و سهر</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>به پیش پایت آن کوهسارها چو حریر</p></div>
<div class="m2"><p>به پیش چشمت آن رودبارها چو شمر</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>لدی الورود چنان کان وظیفه بود ترا</p></div>
<div class="m2"><p>درست کردی اوضاع ملک را یکسر</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>نه هیچ هشتی نام از ددان و اهرمنان</p></div>
<div class="m2"><p>نه هیچ ماندی رسم از بتان و از بتگر</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>ز ناوک تو همی چشم فتنه آمد کور</p></div>
<div class="m2"><p>ز سیلی تو همی گوش شورش آمد کر</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>به آشکارا گوئیم این سخن که هرگز</p></div>
<div class="m2"><p>نهفته نی بر ما قدر آن مهین چاکر</p></div></div>
<div class="b" id="bn102"><div class="m1"><p>درست کاری و جهد ترا بطاعت خویش</p></div>
<div class="m2"><p>شنیده ایم و نمودیم جملگی باور</p></div></div>
<div class="b" id="bn103"><div class="m1"><p>سزای طاعت و اخلاصت آنکه در پاداش</p></div>
<div class="m2"><p>کرم کنیم و بسر بر نهیمت افسر زر</p></div></div>
<div class="b" id="bn104"><div class="m1"><p>که با وجود ضعیفی و پیری و کهنی</p></div>
<div class="m2"><p>فزونتری ز جوانان بمایه و بهتر</p></div></div>
<div class="b" id="bn105"><div class="m1"><p>دو صد سپاس که در نوبهار امن و امان</p></div>
<div class="m2"><p>هزار شکر که در بوستان فتح و ظفر</p></div></div>
<div class="b" id="bn106"><div class="m1"><p>هنوز سرو چمن برگ سبز دارد و خوش</p></div>
<div class="m2"><p>هنوز شاخ کهن میوه تازه دارد و تر</p></div></div>
<div class="b" id="bn107"><div class="m1"><p>هزار گنج گهر بخشمت که دولت را</p></div>
<div class="m2"><p>نکوتری ز هزاران هزار گنج گهر</p></div></div>
<div class="b" id="bn108"><div class="m1"><p>همه رعیت و ملک تراست ارزانی</p></div>
<div class="m2"><p>بسروران تو سرستی و از مهان مهتر</p></div></div>
<div class="b" id="bn109"><div class="m1"><p>اگر بیکسره آن ملک و آن رعیت را</p></div>
<div class="m2"><p>در آب غرقه کنی یا بسوزی از آذر</p></div></div>
<div class="b" id="bn110"><div class="m1"><p>مؤاخذت نرود ورنه باور است ترا</p></div>
<div class="m2"><p>بکوب خاک و بکش مردم و بکش لشکر</p></div></div>
<div class="b" id="bn111"><div class="m1"><p>چو خاک ما شدی آن ملک خاک خود پندار</p></div>
<div class="m2"><p>چو ز آن مائی کشور از آن خود بشمر</p></div></div>
<div class="b" id="bn112"><div class="m1"><p>بچرخ بنده ما را برآور و بنواز</p></div>
<div class="m2"><p>بخاک دشمن ما را بیفکن و بشکر</p></div></div>
<div class="b" id="bn113"><div class="m1"><p>کسی که سجده بتمثال ما نکرده ز ملک</p></div>
<div class="m2"><p>بران چو دیوی کز امر حق ابی و کفر</p></div></div>
<div class="b" id="bn114"><div class="m1"><p>حرام باشدشان آب آن دیار چنانک</p></div>
<div class="m2"><p>بناسپاس حرام است جرعه کوثر</p></div></div>
<div class="b" id="bn115"><div class="m1"><p>بنعمت ما چون کافرند این دونان</p></div>
<div class="m2"><p>صواب نیست که در خلد پا نهد کافر</p></div></div>
<div class="b" id="bn116"><div class="m1"><p>کسان که روی بگردانده اند از فرمان</p></div>
<div class="m2"><p>کسان که حلق بتابیده اند از چنبر</p></div></div>
<div class="b" id="bn117"><div class="m1"><p>برمح و گرز برو کتفشان بسنب و بسای</p></div>
<div class="m2"><p>بتیر و تیغ دل و سینه شان بدوز و بدر</p></div></div>
<div class="b" id="bn118"><div class="m1"><p>بکش مخالف ما را در آن دیار چنانک</p></div>
<div class="m2"><p>در آن دیار بکشت آن قراجه را سنجر</p></div></div>
<div class="b" id="bn119"><div class="m1"><p>بعامه دستخط عفو و مغفرت بنگار</p></div>
<div class="m2"><p>بسوقه با نظر فضل و مکرمت بنگر</p></div></div>
<div class="b" id="bn120"><div class="m1"><p>چو ما نجستیم آزارشان تو نیز مجوی</p></div>
<div class="m2"><p>چو ما گذشتیم از جرمشان تو هم بگذر</p></div></div>
<div class="b" id="bn121"><div class="m1"><p>اشاره رفته که یرلیغ میر تومان را</p></div>
<div class="m2"><p>چنانچه شاید صادر کنند از مصدر</p></div></div>
<div class="b" id="bn122"><div class="m1"><p>چه قدر خواجگی ما نکو همی داند</p></div>
<div class="m2"><p>فرو ز کف نگذاریم قدر آن چاکر</p></div></div>
<div class="b" id="bn123"><div class="m1"><p>امیر خواند چو منشور شاه را بدرست</p></div>
<div class="m2"><p>ز ناز سر زد بر نه سپهر و هفت اختر</p></div></div>
<div class="b" id="bn124"><div class="m1"><p>بویژه آنکه بفرمان شه مطابق یافت</p></div>
<div class="m2"><p>هر آنچه رایش امضا نمود سرتاسر</p></div></div>
<div class="b" id="bn125"><div class="m1"><p>ای آن خجسته امیری که آفتاب بلند</p></div>
<div class="m2"><p>ز عکس تیغ تو آمد پدید در خاور</p></div></div>
<div class="b" id="bn126"><div class="m1"><p>کجاست فرخی آن اوستاد فرخ فال</p></div>
<div class="m2"><p>حکیم با هنر و نکته سنج دانشور</p></div></div>
<div class="b" id="bn127"><div class="m1"><p>که این حدیث بسنجد و ز آن سپس گوید</p></div>
<div class="m2"><p>فسانه گشت و کهن شد حدیث اسکندر</p></div></div>