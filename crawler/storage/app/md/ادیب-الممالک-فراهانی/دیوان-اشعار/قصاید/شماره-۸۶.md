---
title: >-
    شمارهٔ ۸۶
---
# شمارهٔ ۸۶

<div class="b" id="bn1"><div class="m1"><p>می طهور نیاید مرا بکار امروز</p></div>
<div class="m2"><p>که باده خورده ام از دست آن نگار امروز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بجای برف هما گو پراکند الماس</p></div>
<div class="m2"><p>که بزم ما زرخ دوست شد بهار امروز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بپشت گاو نهادند رخت زهد و شدند</p></div>
<div class="m2"><p>سبوکشان بخر خویشتن سوار امروز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز فرقت رمضان خونگریست دیده بط</p></div>
<div class="m2"><p>چنانکه بربط نالیده زارزار امروز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چه خندها که بطامات شیخ شهر زند</p></div>
<div class="m2"><p>پیاله در کف رند شرابخوار امروز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بنه مه رمضان را بپیش کفش ادب</p></div>
<div class="m2"><p>که شد طلایه شوال آشکار امروز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فقیه شهر که دی سنگ زد بساغر ما</p></div>
<div class="m2"><p>ز پیر میکده میجست اعتذار امروز</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ثواب روزه سی روزه را مصالحه کرد</p></div>
<div class="m2"><p>بیک پیاله می ناب خوشگوار امروز</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بتا از آن می دوشینه ساغری در ده</p></div>
<div class="m2"><p>بیاد مجلس میر بزرگوار امروز</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سر کرام و امیر نظام و صدر عظام</p></div>
<div class="m2"><p>که بختیار جهان شد باختیار امروز</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز روزگار ننالند بندگان درش</p></div>
<div class="m2"><p>که اوست عاقله دور روزگار امروز</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>خدایگانا مسرور و شاد و خرم زی</p></div>
<div class="m2"><p>بروی فرخ سالار کامکار امروز</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>اگرچه خاطرت آسوده است حال نژند</p></div>
<div class="m2"><p>مکن اراده نهضت ازین دیار امروز</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>که اختیار بد و نیک کار ملکت را</p></div>
<div class="m2"><p>نهاده است بدست تو شهریار امروز</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>مپوش زنهار ایخواجه چشم ازین مردم</p></div>
<div class="m2"><p>که آمدند بکویت بزینهار امروز</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>تو ای یمین ولیعهد شاه خطه شرق</p></div>
<div class="m2"><p>ز ذیل خواجه خود دست برمدار امروز</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بساز با هنر خویش کار گیتی را</p></div>
<div class="m2"><p>که نیست جز تو درین عرصه مرد کار امروز</p></div></div>