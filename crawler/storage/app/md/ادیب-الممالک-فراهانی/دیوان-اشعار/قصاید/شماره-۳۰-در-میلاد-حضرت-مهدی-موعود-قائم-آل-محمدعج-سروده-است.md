---
title: >-
    شمارهٔ ۳۰ - در میلاد حضرت مهدی موعود قائم آل محمدعج سروده است
---
# شمارهٔ ۳۰ - در میلاد حضرت مهدی موعود قائم آل محمدعج سروده است

<div class="b" id="bn1"><div class="m1"><p>روز میلاد شهی راد و عظیم الشانست</p></div>
<div class="m2"><p>کایة الله علی دائرة الامکانست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کاشف وحی و گشاینده تاویل که خود</p></div>
<div class="m2"><p>سر تنزیل نبی ترجمه فرقانست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شمع ناسوت نماینده ملک و ملکوت</p></div>
<div class="m2"><p>کانچه جز لاهوت اندر رخ او حیرانست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>قائم آل محمد که در اقلیم شهود</p></div>
<div class="m2"><p>وارث مسند و تاج علی عمرانست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شرف شاه زنان مادر سجاد از اوست</p></div>
<div class="m2"><p>زانکه او را شرف از نسل شه مردانست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>لامکانی که مکانش دل مؤمن شده زان</p></div>
<div class="m2"><p>برتر از کون و مکان برزده شادروانست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در چنین روز مبارک بحهان روح دمید</p></div>
<div class="m2"><p>پیکر پاک خدیوی که جهانرا جانست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گرنه او جان جهان نیست چرا در همه جای</p></div>
<div class="m2"><p>اثرش فاش و پدید است و رخش پنهانست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خسروا ای که طفیل قدمت در گیتی</p></div>
<div class="m2"><p>هفت گردون و سه مولود و چهار ارکانست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>علم یزدان را با آنهمه بسیاری و وزن</p></div>
<div class="m2"><p>هم دلت مخزن و هم خازن و هم خزانست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>عرصه کشور ناسوت و فضای جبروت</p></div>
<div class="m2"><p>بی جمال تو به نظارگیان زندانست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>این ملک زاده که میلاد ترا حرمت داشت</p></div>
<div class="m2"><p>پور جمشید سلاطین ملک ایرانست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>پادشه زاده امجد گرانمایه راد</p></div>
<div class="m2"><p>که مرا او را لقب از شه امجدالسلطانست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>کیقبادیست ز فر تو چو در خرگاهست</p></div>
<div class="m2"><p>آفتابیست ز نور تو چو در ایوانست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>دین پرستیست که تصدیق تواش آیینست</p></div>
<div class="m2"><p>حق شناسیست که اخلاص توأش ایمانست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>دل صافش را با فضل و هنر پیوندست</p></div>
<div class="m2"><p>جان پاکش را با هوش و خرد پیمانست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>مفتی حکم ترا دل بخط توقیعست</p></div>
<div class="m2"><p>منهی امر ترا سر بره فرمانست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ساکنان صف خرگاه ترا مسکینست</p></div>
<div class="m2"><p>چاکران در دربار ترا دربانست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>سر و سامان غلامی تو دارد گرچه</p></div>
<div class="m2"><p>اندرین سامان بهتر ز نبی سامانست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چون گشاید کتب دانش و آید بسخن</p></div>
<div class="m2"><p>بوعلی سینا یا خواجه ابوریحانست</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چون نشیند زبر تخت و گراید سوی داد</p></div>
<div class="m2"><p>راستگوئی که بر اورنگ انوشروانست</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>علم سرچشمه عدلست ولی بی چه و چون</p></div>
<div class="m2"><p>ای ملک ملک تو از عدل تو آبادانست</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>گوسفندان دو پا را برهان از کف گرگ</p></div>
<div class="m2"><p>ای شبان رمه کاینک رمه یزدانست</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چو شبان گله از قبل شاه بزرگ</p></div>
<div class="m2"><p>شه درین گله بفرمان خدا چوپانست</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>تا که میلادعلی سیزدهم از رجبست</p></div>
<div class="m2"><p>مولد مهدی در منتصف شعبانست</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>باش در بندگی قائم تا روز قیام</p></div>
<div class="m2"><p>که پناهنده او زنده جاویدانست</p></div></div>