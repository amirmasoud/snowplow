---
title: >-
    شمارهٔ ۲۰
---
# شمارهٔ ۲۰

<div class="b" id="bn1"><div class="m1"><p>سپیده دم چو در آغاز سال و ماه عرب</p></div>
<div class="m2"><p>گرفت مار سیه مهره سپید بلب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یکی سیه قصب داشت گیتی اندر بر</p></div>
<div class="m2"><p>دریده گشت گریبان آن سیاه قصب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سپیده خیمه سیمین فراشت بر کهسار</p></div>
<div class="m2"><p>ز هم گسست طناب سیاه خیمه شب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>طلایه سپه آفتاب راند از پیش</p></div>
<div class="m2"><p>ستارگان بگرفتند جمله راه هرب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کشید گفتی با گاز آهنین خورشید</p></div>
<div class="m2"><p>ز طاق گنبد پیروزه میخهای ذهب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گرفت پروین راه از فراز سوی نشیب</p></div>
<div class="m2"><p>ز تاک گفتی چیدند خوشهای عنب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نظاره کردم آن مرئة المسلسله را</p></div>
<div class="m2"><p>که داشت دست بزنجیر و تن اسیر تعب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو ذات کرسی می رفت بر فراز سریر</p></div>
<div class="m2"><p>بدست یاره و از سیم طوق در غبغب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بنات نعش که در گرد قطب بودندی</p></div>
<div class="m2"><p>نشسته چون بگه غزل دختران عرب</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چنان شدند پریشان که لشکر سلجوق</p></div>
<div class="m2"><p>ز رایت مغولان شد پریش اینت عجب</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>افق چو بحر محیط و مجره نهر روان</p></div>
<div class="m2"><p>که آب نهر بدریا فرو شود ز مصب</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>سمآء مبنی مانند صفحه سیمین</p></div>
<div class="m2"><p>بر آن سطور و حروف سطور آن معرب</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بشست ز آب طلا آن سطور را خورشید</p></div>
<div class="m2"><p>چو اوستادان الواح طفل در مکتب</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>تمام خلق در این روز رنگ شب پوشند</p></div>
<div class="m2"><p>چه شد که جامه بهمرنگ روز پوشد شب</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>همه سپید سلب را سیه پلاس کند</p></div>
<div class="m2"><p>فلک پلاس سیه را کند سپیده سلب</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>برفت کله انجم درون صیره غرب</p></div>
<div class="m2"><p>ز کوهسار چو جنباند گرگ خیره ذنب</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>همی تو گفتی جوقی کبوتران بپرید</p></div>
<div class="m2"><p>ز برج سیمین از بازی آتشین مخلب</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>فتاد زهره ز اورنگ آبنوش به خاک</p></div>
<div class="m2"><p>شکست چنگش در چنگ و نایش اندر لب</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>تو گوئی افتاد اندر قموص از سر تخت</p></div>
<div class="m2"><p>صفیه زوج نبی دخت حی بن اخطب</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بدست چرخ یکی تیغ آتشین دیدم</p></div>
<div class="m2"><p>چو ذوالفقار علی روز کشتن مرجب</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چنینه روزی کز بامداد تا به غروب</p></div>
<div class="m2"><p>بگوش خلق ز گردون رسید بانگ طرب</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>مرا رسید بشارت ز منهیان سرای</p></div>
<div class="m2"><p>که کرده اینک میر خدایگانت طلب</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ستوده خصلت فرخنده فرامیر نظام</p></div>
<div class="m2"><p>جهان فضل و هنر آسمان عقل و ادب</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>از این بشارت از جای جستمی چونان</p></div>
<div class="m2"><p>که برق بینی از مطلع و صبا ز مهب</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>همی گرفتم در دست خامه و دفتر</p></div>
<div class="m2"><p>همی کشیدم در پای موزه و جورب</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>کجا که درگه آن آفتاب رخشان بود</p></div>
<div class="m2"><p>همی بسودم و بوسیدمی دو روی و دولب</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>امیر اعظم فرمود مر مرا باری</p></div>
<div class="m2"><p>که ای تو راکب و دانش ترا بهین مرکب</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>منت همیدون خواندم بیار تا گویم</p></div>
<div class="m2"><p>سماع را چه مقام و نشاط را چه سبب</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بروز غره سال عرب دمیده مهی</p></div>
<div class="m2"><p>نه چون هلال محرم نه چون مه نخشب</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>ز شوق عزت تابنده شد یکی خورشید</p></div>
<div class="m2"><p>به چرخ نصرت رخشنده شد یکی کوکب</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>بلند اختری از آسمان مجد و کمال</p></div>
<div class="m2"><p>طلوع کرد ز تأیید و لطف حضرت رب</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>پدرش والا نواب نصرت الدوله</p></div>
<div class="m2"><p>که از نژاد شهانش بود تبار و نسب</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>خجسته مادر او عزت الملوک بود</p></div>
<div class="m2"><p>یکی فریشته از دودمان جاه و حسب</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>چراغ و چشم ولیعهد پادشاه عجم</p></div>
<div class="m2"><p>ملک مظفر دین مهتر از ملوک عرب</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>کریمه ی خود بر این کریم داد از آنک</p></div>
<div class="m2"><p>همیشه ابعد ممنوع باشد از اقرب</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>از آن سپرد که این گوهر درخشنده</p></div>
<div class="m2"><p>پدید آید با یک جهان کمال و ادب</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>چو لؤلؤئی که پدیدار گردد از دو صدف</p></div>
<div class="m2"><p>چو فضه که نمودار آید از دو ذهب</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>خجسته زاد پسر از چنین پدر مادر</p></div>
<div class="m2"><p>ستوده آید مولود از چنین ام و اب</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>بدین طراوت خیزد ازین دو دریا در</p></div>
<div class="m2"><p>بدین حلاوت ریزد ازین دو نخله رطب</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>ازین دو شاخ بدین خرمی برآید برگ</p></div>
<div class="m2"><p>ازین دو باغ بدین تازگی بروید حب</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>خجسته مادرش اما خجسته تر پدرش</p></div>
<div class="m2"><p>پدرش نیک نجیب است و مادرش انجب</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>ز هر دو سوی شریف و ز هر دو سوی عزیز</p></div>
<div class="m2"><p>ز هر دو سوی کریم و ز هر دو سو اطیب</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>چنین پسر شد در خورد اسب و تیغ و سپه</p></div>
<div class="m2"><p>چنین پسر شد شایان اسم و رسم و لقب</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>پی چراغان در مولد چنین مولود</p></div>
<div class="m2"><p>بروز روشن آبستن است شب همه شب</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>مگر ندیدی آن صبحدم که زاد زمام</p></div>
<div class="m2"><p>فروخت گردون شمعی ز عنبر اشهب</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>خدایگانا ای آنکه تیغ احدب تو</p></div>
<div class="m2"><p>نموده پشت فلک را براستی احدب</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>مبارزان و دلیران روزگار تمام</p></div>
<div class="m2"><p>بنام تیغ تو خوانند در خطوب خطب</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>نسیم خویت آذر بتازه ترعود است</p></div>
<div class="m2"><p>شرار خشمت آتش به خشکتر ز حطب</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>ز پای تا سر اگر لطف و رحمتی نه شگفت</p></div>
<div class="m2"><p>که از ملک همه مهر آید و ز دیو غضب</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>بدل رحیمت عفو خدای راست دلیل</p></div>
<div class="m2"><p>کف کریمت رزق عباد راست سبب</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>ز همت تو شود حرص بود لامه تمام</p></div>
<div class="m2"><p>ز نعمت تو شود سیر دیده اشعب</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>گفت طبیبی درمان فرست و درد شناس</p></div>
<div class="m2"><p>که شد خزانه والا بر این طبیب مطب</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>عدوی تو سر انگشت آنچنان خاید</p></div>
<div class="m2"><p>که پشت و پهلو خارند اشتران جرب</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>به لشکر تو ز بس پاکدامنند توان</p></div>
<div class="m2"><p>سپرد دختر دوشیزه بر جوان عزب</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>تو حکمرانی ما بین اوس با خزرج</p></div>
<div class="m2"><p>تو صلح دانی با جنگ بکر با تغلب</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>خلاف رای تو ممنوع شد بهر ملت</p></div>
<div class="m2"><p>قبول امر تو محتوم شد بهر مذهب</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>هزار سال بزی تا هزار سال منت</p></div>
<div class="m2"><p>هزار مدح سرایم چو جر دل وقعنب</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>گهی بمال کنم تهنیت گه از فرزند</p></div>
<div class="m2"><p>گهی به جاه کنم تهنیت گه از منصب</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>بویژه روز چنین تهنیت بدین مولود</p></div>
<div class="m2"><p>سرایمت به سرور و ستایمت به طرب</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>تبارک الله ازین شاهزاده فرخ</p></div>
<div class="m2"><p>کزو فتاده بسطح زمانه شور و شعب</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>مبین به خردیش ایدر که جذوه ای ز آتش</p></div>
<div class="m2"><p>بیک دقیقه زند بر فراز چرخ لهب</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>چو کوکبی است که در چشم ما نماید خرد</p></div>
<div class="m2"><p>ولی بگردون کی خرد باشدی کوکب</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>الا چو عشق جمیل است بر بثینه همی</p></div>
<div class="m2"><p>الا چو باشد مهر شریح بر زینب</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>چو زلف معشوق اندر تن عدویت تاب</p></div>
<div class="m2"><p>چو جان عاشق در جسم دشمنانت تب</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>همی به جام موالیت نوش از زنبور</p></div>
<div class="m2"><p>همی بجان اعادیت نیش از عقرب</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>یکی همیشه سزاوار مدح و نعت و سپاس</p></div>
<div class="m2"><p>یکی هماره گرفتار شتم و لعنت و سب</p></div></div>