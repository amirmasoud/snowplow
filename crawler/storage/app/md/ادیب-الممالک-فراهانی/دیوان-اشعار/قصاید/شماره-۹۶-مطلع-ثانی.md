---
title: >-
    شمارهٔ ۹۶ - مطلع ثانی
---
# شمارهٔ ۹۶ - مطلع ثانی

<div class="b" id="bn1"><div class="m1"><p>که ای سفینه دستت خزینه آمال</p></div>
<div class="m2"><p>که ای صفیحه تیغت صحیفه آجال</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در آن بساط همایون که صدر بار توئی</p></div>
<div class="m2"><p>فلک نشاند خورشید را بصف نعال</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>برای طوق حسام تو خور بشکل نگین</p></div>
<div class="m2"><p>برای نعل سمند تو مه بشکل هلال</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز رشک تیغ کجت چشم مهر جسته رمد</p></div>
<div class="m2"><p>ز نقطه قلمت روی ماه یافته خال</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بپای بی ادبان بسته دست تو زنجیر</p></div>
<div class="m2"><p>چنانکه گوئی بر ساق لعبتان خلخال</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز نسر طایر نامی نماند در واقع</p></div>
<div class="m2"><p>همای چتر تو چون برگشود زرین بال</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شعاع چتر فتوح تو رایت نصرت</p></div>
<div class="m2"><p>رموز نقش نگین تو آیت اقبال</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بگاه رزم ندانی رماح را ز ریاح</p></div>
<div class="m2"><p>بروز بزم ندانی تو مال را زر مال</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شفا تو داری دیگر کسان ضماد و طلا</p></div>
<div class="m2"><p>عصا تو آری دیگر کسان عصی و حبال</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بروز بزم و طرب لین العریکه توئی</p></div>
<div class="m2"><p>ولی شجاع و قوی الشکیمه گاه جدال</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز حشمت تو تن عافیت گرفت سمن</p></div>
<div class="m2"><p>ز سطوت تو تن درد و غصه یافت هزال</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بظرف جود تو بحر عمان کم از قطره</p></div>
<div class="m2"><p>بوزن حلم تو کوه گران کم از مثقال</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>سخاوت تو ز عبدالله بن جذعان بیش</p></div>
<div class="m2"><p>شجاعت تو فزونتر ز هاشم مرقال</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>توئی که پیگر خارا شکافی از شمشیر</p></div>
<div class="m2"><p>توئی که قلعه البرز کوبی از کوبال</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>الا چو عید غدیر آید از پی قربان</p></div>
<div class="m2"><p>الا چو باشد ذیقعده از پی شوال</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ز ابر مهر ولیعهد آسمان مهدت</p></div>
<div class="m2"><p>همیشه بادا گسترده بر بفرق طلال</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بدین عروض و قوافی غضایری گوید</p></div>
<div class="m2"><p>اگر کمال بجاه اندر است و جاه بمال</p></div></div>