---
title: >-
    شمارهٔ ۱۰۹ - در نکوهش حسودان
---
# شمارهٔ ۱۰۹ - در نکوهش حسودان

<div class="b" id="bn1"><div class="m1"><p>خرد پیر گفته بود که من</p></div>
<div class="m2"><p>نکنم در سیاق شعر سخن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زانکه همسنگ سنگ خاره شود</p></div>
<div class="m2"><p>گر برآید چو سنگ در عدن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سنگ خارا اگر شدی کمیاب</p></div>
<div class="m2"><p>بود قدرش بر از عقیق یمن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سنگ خارا، اگر نبود، نبود</p></div>
<div class="m2"><p>تیغ سای و صلایه و هاون</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>لاجرم در بهای این اشیاء</p></div>
<div class="m2"><p>جان همیداد مشتری بثمن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سخن، ارچه زر است و مردم خاک</p></div>
<div class="m2"><p>سخن ار چه روان ومردم تن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گرچه آهن ز خاک زر خیزد</p></div>
<div class="m2"><p>لاجرم کمتر آید از آهن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سخن ار چه ببوی نافه مشک</p></div>
<div class="m2"><p>سخن ار چه تمیز مرد ز زن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مغز را مایه صداع شود</p></div>
<div class="m2"><p>گر ببوئی همیشه مشک ختن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شعر من زر ناب جعفری است</p></div>
<div class="m2"><p>شعر دیگر کسان چو ریماهن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>من دو صد ساحری کنم بمقال</p></div>
<div class="m2"><p>من بسی جادوئی کنم بسخن</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نه بعجب است این فسانه نغز</p></div>
<div class="m2"><p>بل ز فخر است این ترانه من</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ز آن باشعار خویشتن نازم</p></div>
<div class="m2"><p>که بود در مدیح شاه ز من</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>سیدالاولیاء امام رشید</p></div>
<div class="m2"><p>اول الاوصیاء شه ذوالمن</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>دست یزدان، ممیت بدعت و کفر</p></div>
<div class="m2"><p>شیر حق محیی رسوم و سنن</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>آن کز او نور جسته دیده عقل</p></div>
<div class="m2"><p>آن کز او کور گشته چشم فتن</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>شاه مردان علی ابوطالب</p></div>
<div class="m2"><p>پدر اطهر حسین و حسن</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>کرده جاری برای این هر سه</p></div>
<div class="m2"><p>حق تعالی بخلد نهر لبن</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>تا بهار خجسته چون احمد</p></div>
<div class="m2"><p>بست طرف سفر ز طرف چمن</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>آن سه طرار نابکار که بود</p></div>
<div class="m2"><p>دی و اسفند ماه با بهمن</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>سوی باغ آمدند از ره کین</p></div>
<div class="m2"><p>همچو دزدی که خیزد از مکمن</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>آب بر روی بوستان بستند</p></div>
<div class="m2"><p>آتش افروختند در خرمن</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>سرد کردند شعله غیرت</p></div>
<div class="m2"><p>گرم راندند از جفا توسن</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>راست چون آن سه تن سخن کردند</p></div>
<div class="m2"><p>بدرشتی که خاکشان بدهن</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>جای رایات سبز هاشمیان</p></div>
<div class="m2"><p>از ورقهای سرو و برگ سمن</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>زد علامات سود در بستان</p></div>
<div class="m2"><p>همچو آل امیه زاغ و زغن</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>سبز پوشان سپید پوش شدند</p></div>
<div class="m2"><p>بر لب جوی و در صف گلشن</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>هر زمان سونش در و الماس</p></div>
<div class="m2"><p>می ببیزد هوا بپرویزن</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>آمد آن بوم شوم در بستان</p></div>
<div class="m2"><p>کبک را طوق بست در گردن</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>راست گوئی که زاده خطاب</p></div>
<div class="m2"><p>گردن شیر حق فکنده رسن</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>رفت بلبل در آشیانه ز باغ</p></div>
<div class="m2"><p>همچو صدیقه سوی بیت حزن</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>باغ شد جای زاغ پنداری</p></div>
<div class="m2"><p>تخت جم شد سریر اهریمن</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>زود باشد که فروردین آید</p></div>
<div class="m2"><p>باز چون شیر حق بطرف چمن</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>تاب گیرد عذار هر سنبل</p></div>
<div class="m2"><p>نطق یابد زبان هر سوسن</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>ریزد اندر کنار دامن باغ</p></div>
<div class="m2"><p>سر زلف بنفشه مشک ختن</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>بیزد اندر کرانه بستان</p></div>
<div class="m2"><p>ابر لؤلؤ و نسترن لادن</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>گرچه نشکفته شاخ اشکوفه</p></div>
<div class="m2"><p>ور چه نامد بکعبه شیخ قرن</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>مغز ما بوی گل شنیده ز باغ</p></div>
<div class="m2"><p>مغز احمد نسیم حق ز یمن</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>سیزده روز چون بشد ز رجب</p></div>
<div class="m2"><p>پی تعمیر این سرای کهن</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>اولین بانی سرای وجود</p></div>
<div class="m2"><p>آمد از پرده با رخی روشن</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>رکن بنیان کعبه را بشکافت</p></div>
<div class="m2"><p>حشمتش همچو سیل بنیان کن</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>زاد در خانه تا بدانی کوست</p></div>
<div class="m2"><p>خانه زاد مهیمن ذوالمن</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>از ولایت به پیکرش پوشاند</p></div>
<div class="m2"><p>حق تعالی قبا و پیراهن</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>با رسول خدای عزوجل</p></div>
<div class="m2"><p>همچو یک روح گشت در دو بدن</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>ای به ایزد ولی و مظهر و سر</p></div>
<div class="m2"><p>وی باحمد وصی و صهر و ختن</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>خاکپای تو موطن دل ماست</p></div>
<div class="m2"><p>لاجرم واجب است حب وطن</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>درگه مولدت بدرکه میر</p></div>
<div class="m2"><p>تهنیت را سخن سرایم من</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>صدر والاگهر امیر نظام</p></div>
<div class="m2"><p>کهف اهل زمین و فخر ز من</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>صاحب السیف والقلم آنکو</p></div>
<div class="m2"><p>خوانده بر فکرتش خرد احسن</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>باعث الجود والکرم کاو را</p></div>
<div class="m2"><p>کان بجیب است و بحر در دامن</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>تیغ وی ساغری است پر می ناب</p></div>
<div class="m2"><p>هر یک از جرعه هاش مردافکن</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>کلک او شاهدی است مشکین موی</p></div>
<div class="m2"><p>طره اش با دو صد هزار شکن</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>گردی از آب آهن آرد بار</p></div>
<div class="m2"><p>هیبتش آب آرد از آهن</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>دستش ار سایه بر زمین فکند</p></div>
<div class="m2"><p>روید از خاک زر پی روین</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>با خسان تیرش آن کند که کند</p></div>
<div class="m2"><p>نجم ثاقب بجان اهریمن</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>گشته بر نوعروس ملک او را</p></div>
<div class="m2"><p>تیغ داماد و خامه خشتامن</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>ای گشوده ز روی عدل نقاب</p></div>
<div class="m2"><p>وی به بسته بپای ظلم رسن</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>من بخوان تو آمدم مهمان</p></div>
<div class="m2"><p>همچو برگ شکوفه در گلشن</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>ساختم بهر دفع تیر حسود</p></div>
<div class="m2"><p>از مدیح تو آهنینه مجن</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>شاد گشتم بچاکری درت</p></div>
<div class="m2"><p>رستم از صدمت و بلا و محن</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>چون ز نیروی حرز مدحت تو</p></div>
<div class="m2"><p>گشتم از مکر حاسدان ایمن</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>گفتم امروز راست خواهم داشت</p></div>
<div class="m2"><p>قامت چرخ کوژپشت کهن</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>پا بمنت نهاد می بزمین</p></div>
<div class="m2"><p>تند راندم بر آسمان توسن</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>کار من بنده چون درستی یافت</p></div>
<div class="m2"><p>دل حاسد همی گرفت شکن</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>کرد بر جان من بحضرت تو</p></div>
<div class="m2"><p>خصم بدخواه و حاسد ریمن</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>آنچه گرگان نکرده با یوسف</p></div>
<div class="m2"><p>و آنچه گرگین نموده با بیژن</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>هان و هان ای وزیر فرزانه</p></div>
<div class="m2"><p>هان و هان ای امیر شیراوژن</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>تهمتی بر تنم نهد که بکوه</p></div>
<div class="m2"><p>گر نهی کوه کج کند گردن</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>آتش آه من هزاران کوه</p></div>
<div class="m2"><p>آب سازد و گر بود زاهن</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>جد من نحن کالجبال سرود</p></div>
<div class="m2"><p>بر همه مردمان بسرو علن</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>کوه فضلم من و سپهر هنر</p></div>
<div class="m2"><p>مهر تابانم و مه روشن</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>آنکه تقبیح نای بلبل کرد</p></div>
<div class="m2"><p>دوست دارد سرود زاغ و زعن</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>و آنکه با مسلمان درآویزد</p></div>
<div class="m2"><p>متوسل بود بجبت و وثن</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>ای ز تو نام فضل جاویدان</p></div>
<div class="m2"><p>وی ز تو مام دهر استرون</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>نز تو جویم مدد نه از سلطان</p></div>
<div class="m2"><p>که ولی را گرفته ام دامن</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>دشنه من نبرد این حلقوم</p></div>
<div class="m2"><p>حربه من ندرد آن جوشن</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>چون دو پیکر شود ز تیغ علی</p></div>
<div class="m2"><p>آن که نازد همی بعقد پرن</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>می توانم سزای بدمنشان</p></div>
<div class="m2"><p>دادن از زخم هجو و تیغ سخن</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>لیک با ذوالفقار شیر خدای</p></div>
<div class="m2"><p>داد خواهم بخصم پاداشن</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>همه جا شاعرم ولی اینجا</p></div>
<div class="m2"><p>نبود شاعری وظیفه من</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>زانکه اینجا بود مقام هجی</p></div>
<div class="m2"><p>مر مرا عار باشد از این فن</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>هجو آنان کنند کایشان راست</p></div>
<div class="m2"><p>بر بزرگان خویش ریبت و ظن</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>من بفضل خدا شناخته أم</p></div>
<div class="m2"><p>بوالحسن را همی بوجه حسن</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>دوش با شیر حق در این معنی</p></div>
<div class="m2"><p>شکوه کردم ز حاسدان بسخن</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>پاسخم داد جد امجد و گفت</p></div>
<div class="m2"><p>یا بنی لاتخف و لاتحزن</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>ذوالفقار مرا زبان تیز است</p></div>
<div class="m2"><p>گر زبان تو باشدی الکن</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>باش تا برق تیغ من سازد</p></div>
<div class="m2"><p>صدق و کذب حدیث را روشن</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>راست نامیخت هیچ با ترفند</p></div>
<div class="m2"><p>آب نفروخت هیچ با روغن</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>می بزاید همی سحرگاهان</p></div>
<div class="m2"><p>آنچه شب حامل است و آبستن</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>حاسدا تاب ذوالفقار علی</p></div>
<div class="m2"><p>چون توانی که رنجی از سوزن</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>تو که مستحسنات طبع مرا</p></div>
<div class="m2"><p>تاژگونه کنی و مستهجن</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>امتحان را که گفت پیکر خویش</p></div>
<div class="m2"><p>بر دم ذوالفقار برهنه زن</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>عنقریب ای اسیر بند غرور</p></div>
<div class="m2"><p>افتی اندر هوان و ذل و شجن</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>بس فروزی ز سوز دل اخگر</p></div>
<div class="m2"><p>بس فرازی بر آسمان شیون</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>من یکی فاطمی نژادستم</p></div>
<div class="m2"><p>از بقایای خاندان کهن</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>نه تجاوز نموده ام ز حدود</p></div>
<div class="m2"><p>نه تخلف نموده ام ز سنن</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>گر بمن داد شاه صد قنطار</p></div>
<div class="m2"><p>از تو هرگز نکاست یک ارزن</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>ور بمن داده میر صد خروار</p></div>
<div class="m2"><p>از تو هرگز نخواستم یک من</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>آب، چندین مبیز در غربال</p></div>
<div class="m2"><p>باد چندین مسای در هاون</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>دردی دن چنین خرابت کرد</p></div>
<div class="m2"><p>وای اگر برکشی ز صافی دن</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>این تو و این سرود و این طنبور</p></div>
<div class="m2"><p>این تو و این سماع و این ارغن</p></div></div>
<div class="b" id="bn102"><div class="m1"><p>من نیارم نواخت بهتر از این</p></div>
<div class="m2"><p>گر تو بهتر زنی بگیر و بزن</p></div></div>
<div class="b" id="bn103"><div class="m1"><p>چند نازی بدولت قارون</p></div>
<div class="m2"><p>چند تازی بصولت قارن</p></div></div>
<div class="b" id="bn104"><div class="m1"><p>گر شنیدی که پور رستم را</p></div>
<div class="m2"><p>کشت بهمن بخون روئین تن</p></div></div>
<div class="b" id="bn105"><div class="m1"><p>نه تو در عرصه چون فرامرزی</p></div>
<div class="m2"><p>نه من اندر هجا کم از بهمن</p></div></div>
<div class="b" id="bn106"><div class="m1"><p>آن کنم با تو در سخن که نمود</p></div>
<div class="m2"><p>با سپاه عجم ابوالمحجن</p></div></div>
<div class="b" id="bn107"><div class="m1"><p>من که خواهم شدن از این سامان</p></div>
<div class="m2"><p>من که خواهم برفت از این مسکن</p></div></div>
<div class="b" id="bn108"><div class="m1"><p>نه در این شهر ناقه ام نه جمل</p></div>
<div class="m2"><p>نه در این ملک خانه ام نه سکن</p></div></div>
<div class="b" id="bn109"><div class="m1"><p>ساعیا بیش از این تنم مشکر</p></div>
<div class="m2"><p>حاسدا ز این سپس دلم مشکن</p></div></div>
<div class="b" id="bn110"><div class="m1"><p>بر کمالم ز جهل خورده مگیر</p></div>
<div class="m2"><p>بر روانم ز رشک طعنه مزن</p></div></div>
<div class="b" id="bn111"><div class="m1"><p>زر و سیم ترا ندیدم هیچ</p></div>
<div class="m2"><p>چند آهن دلی کنی با من</p></div></div>
<div class="b" id="bn112"><div class="m1"><p>من عطا از خدایگان گیرم</p></div>
<div class="m2"><p>که نرنجانده خاطرم با من</p></div></div>
<div class="b" id="bn113"><div class="m1"><p>گر بمیرم ز جوع ننشینم</p></div>
<div class="m2"><p>خوان بخل ترا به پیرامن</p></div></div>
<div class="b" id="bn114"><div class="m1"><p>ور فتد در مغاره کالبدم</p></div>
<div class="m2"><p>می نجوید روانم از تو کفن</p></div></div>
<div class="b" id="bn115"><div class="m1"><p>ور بمیرم ز درد برهنگی</p></div>
<div class="m2"><p>نکنم در بر از تو پیراهن</p></div></div>
<div class="b" id="bn116"><div class="m1"><p>چون بدیدی مرا بسایه میر</p></div>
<div class="m2"><p>در صف خلد و دادی ایمن</p></div></div>
<div class="b" id="bn117"><div class="m1"><p>دود برخاست از دلت ز حسد</p></div>
<div class="m2"><p>همچو دودی که خیزد از گلخن</p></div></div>
<div class="b" id="bn118"><div class="m1"><p>خواستی بافسون و افسانه</p></div>
<div class="m2"><p>زشت نامم کنی و تر دامن</p></div></div>
<div class="b" id="bn119"><div class="m1"><p>بگمانت که من چو رخت برم</p></div>
<div class="m2"><p>خوابگاه تو گردد این مامن</p></div></div>
<div class="b" id="bn120"><div class="m1"><p>گر شنیدی ز خلد آدم را</p></div>
<div class="m2"><p>راند افسانهای اهریمن</p></div></div>
<div class="b" id="bn121"><div class="m1"><p>بوالبشر توبه کرد و خصم بماند</p></div>
<div class="m2"><p>دست بر فرق و طوق در گردن</p></div></div>
<div class="b" id="bn122"><div class="m1"><p>رو مترسان عصای موسی را</p></div>
<div class="m2"><p>از صف ساحر و عصا و رسن</p></div></div>
<div class="b" id="bn123"><div class="m1"><p>من همی نالم از فریسیموس</p></div>
<div class="m2"><p>تو چرا تهمتم زنی به عنن</p></div></div>
<div class="b" id="bn124"><div class="m1"><p>یا چو مردان گناه من بشمار</p></div>
<div class="m2"><p>یا ز خجلت بپوش چهره چو زن</p></div></div>
<div class="b" id="bn125"><div class="m1"><p>تا زبانی صفت زنم مشتت</p></div>
<div class="m2"><p>ز اخسؤالا تکلمو بدهن</p></div></div>
<div class="b" id="bn126"><div class="m1"><p>ای که نشناختی الف از بی</p></div>
<div class="m2"><p>بلکه خطی ز ابجد و کلمن</p></div></div>
<div class="b" id="bn127"><div class="m1"><p>بر امیر مدینه چون تازی</p></div>
<div class="m2"><p>ای چو اصحاب ظله در مدین</p></div></div>
<div class="b" id="bn128"><div class="m1"><p>عنکبوتی و خانه تو بود</p></div>
<div class="m2"><p>از همه خانها بسی اوهن</p></div></div>
<div class="b" id="bn129"><div class="m1"><p>ابلهانه بشهپر سیمرغ</p></div>
<div class="m2"><p>جای زنجیر، تار خویش، متن</p></div></div>
<div class="b" id="bn130"><div class="m1"><p>مگسی را بگیر و طعمه نمای</p></div>
<div class="m2"><p>پنجه در پنجه هما مفکن</p></div></div>
<div class="b" id="bn131"><div class="m1"><p>آدمی نی بچشم و گوش بود</p></div>
<div class="m2"><p>نه بابروی و روی و موی ذقن</p></div></div>
<div class="b" id="bn132"><div class="m1"><p>بلکه حیوان و آدمی را فرق</p></div>
<div class="m2"><p>می بباشد همی بجان و بتن</p></div></div>
<div class="b" id="bn133"><div class="m1"><p>گرچه سرگین به هیئت عنبر</p></div>
<div class="m2"><p>گرچه هیزم بصورت چندن</p></div></div>
<div class="b" id="bn134"><div class="m1"><p>این به بیت البغال و آن به بغل</p></div>
<div class="m2"><p>جای آن در تنور و این مدخن</p></div></div>
<div class="b" id="bn135"><div class="m1"><p>یکحدیث آورم در این محضر</p></div>
<div class="m2"><p>تا رباید ز چشم خفته و سن</p></div></div>
<div class="b" id="bn136"><div class="m1"><p>دشمن آل مرتضی باید</p></div>
<div class="m2"><p>مام خود را همی شود دشمن</p></div></div>
<div class="b" id="bn137"><div class="m1"><p>دعوت خصم را تمام کنم</p></div>
<div class="m2"><p>بدعای خدایگان زمن</p></div></div>
<div class="b" id="bn138"><div class="m1"><p>تا برآید همی در از دریا</p></div>
<div class="m2"><p>تا بزاید همی زر از معدن</p></div></div>
<div class="b" id="bn139"><div class="m1"><p>چرخ خرگاهش آفتاب چراغ</p></div>
<div class="m2"><p>ماه دینارش آسمان مخزن</p></div></div>