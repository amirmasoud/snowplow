---
title: >-
    شمارهٔ ۹۴ - در نکوهش محمدعلی میرزای مخلوع هنگام بستن و کشتن مشروطه خواهان در باغ شاه تهران
---
# شمارهٔ ۹۴ - در نکوهش محمدعلی میرزای مخلوع هنگام بستن و کشتن مشروطه خواهان در باغ شاه تهران

<div class="b" id="bn1"><div class="m1"><p>چو شه به دامن جادو و جنبل آرد چنگ</p></div>
<div class="m2"><p>همی‌گریزد از او مردمی به صد فرسنگ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کدام جنبل و جادو نماید آن آثار</p></div>
<div class="m2"><p>که آید از قلم و رأی مرد با فرهنگ</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دریده شد دل مرد آن شیر گیر و ز جهل</p></div>
<div class="m2"><p>همی نگارد افسون بچرم گرگ و پلنگ</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دلش مسخر دیو است و از تهی مغزی</p></div>
<div class="m2"><p>همی کند پی تسخیر دیو و دد آهنگ</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو عزم نیست ملک را شود عزیمت خوان</p></div>
<div class="m2"><p>چو رنگ نیست بکارش رود پی نیرنگ</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>میان این شه و اسکندر اینت بس توفیر</p></div>
<div class="m2"><p>که او شتافت پی نام و شاه ما پی ننگ</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>وزیر بار سکندر بدی ارسطالیس</p></div>
<div class="m2"><p>وزارت شه ما را کند بهادر جنگ</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بباغ خویش بنازد شهنشه ایران</p></div>
<div class="m2"><p>چنانکه، ما نی، از کارخانه ارژنگ</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چگونه باغی کز هر طرف در او نگری</p></div>
<div class="m2"><p>ز خون بیگنهان لاله رسته رنگارنگ</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نغوذبالله از آن دیولاخ تیره که هست</p></div>
<div class="m2"><p>شرر فروز چو دوزخ سیه چو دود آهنگ</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>همی تو گوئی آنجا حدیقة الموت است</p></div>
<div class="m2"><p>بجای سرو در آن نیزه جای سبزه خدنگ</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بجای نار دل بیدلان طپیده بخون</p></div>
<div class="m2"><p>بجای تاک سر خستگان ز دار آونگ</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ریاض آن همه آکنده از بلا و نقم</p></div>
<div class="m2"><p>حیاض آن همه انباشته بزهر و شرنگ</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>درختهاش عقابین و تازیانه و دار</p></div>
<div class="m2"><p>کدیورش همه دژخیم چهره پر آژنگ</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ز سیر سبزه سبزش جگر چو لاله بداغ</p></div>
<div class="m2"><p>ز دیدن گل سرخش چو غنچه دلها تنگ</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ز سیل اشک یتیمان و خون مظلومان</p></div>
<div class="m2"><p>بگل فرو رود اسب و سوار تا آرنگ</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>تفوبر آن قلم و دست و تیغ و طوق و نگین</p></div>
<div class="m2"><p>تفو بر آن علم و کوس و افسر و اورنگ</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>تفو بر آنکه چنین شاه را همی شمرد</p></div>
<div class="m2"><p>ز جهل وارث جم یا خلیفه هوشنگ</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بدان مثابه که ایران از او خرابی یافت</p></div>
<div class="m2"><p>نیافت از ستم بیور اسب و پور پشنگ</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>دلش ز ناله و فریاد عاجزان بنشاط</p></div>
<div class="m2"><p>چنانکه قحبه مست از نوای بربط و چنگ</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>تنش ز جهل و طمع کرده اند پنداری</p></div>
<div class="m2"><p>ز چشم و گوش و زبان تا سرین و اشتالنگ</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ز چه برآید همواره چون مه نخشب</p></div>
<div class="m2"><p>بکه بماند فغواره چون شه شترنگ</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>چو بست خیش خود از شاخ شیخ شوخ پلید</p></div>
<div class="m2"><p>بیوغ گردن آن گاو گردن کردنگ</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>شیار کرد دل خلق را و تخم خلاف</p></div>
<div class="m2"><p>در آن بکاشت بدستور آن سفیه دبنگ</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چو روید از دل این خاک جز نفاق و حسد</p></div>
<div class="m2"><p>چه زاید از زن بدکاره جز نکوهش و ننگ</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ندیم شه چو بود شاهدان بازاری</p></div>
<div class="m2"><p>بتان سعتری و لعبتان دلبر شنگ</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>سوارهاش ندارند در نبرد شتاب</p></div>
<div class="m2"><p>پیادهاش نیارند در گریز درنگ</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بروز رزم ز ابرو کمان کند سردار</p></div>
<div class="m2"><p>بگاه حمله ز مژگان سپه کشد سرهنگ</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>چو بست تیغ شه از خون بیگنه زنگار</p></div>
<div class="m2"><p>کجا ز آینه معدلت زداید زنگ</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>شها خدای ترا داده این جهان فراخ</p></div>
<div class="m2"><p>چرا کنیش چو زندان گور بر ما تنگ</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>چرا تو عشوه آن خربغا خری کار است</p></div>
<div class="m2"><p>چو روسپی رخ تزویر خود ببوی و برنگ</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>ز بوی و رنگش بی رنگ و بوی خواهی ماند</p></div>
<div class="m2"><p>چو هوش از اثر می خرد ز نشاه بنگ</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>ترا از آن چه سعادت رسد که گویندت</p></div>
<div class="m2"><p>که آفتاب بشیر است و ماه در خرچنگ</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>کجا بکام دل اندر رسی که مست و خراب</p></div>
<div class="m2"><p>تو خفته در چهی و آرزو بکام نهنگ</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>همیشه در هذیانی مگر بخواب اندر</p></div>
<div class="m2"><p>تنت بسان فرنجک فشرده دست فرنگ</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>تو سفله کی بمقام شهان رسی حاشا</p></div>
<div class="m2"><p>کجا سبق برد از اسب بادپا خر لنگ</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>چگونه خسبد و ایمن ز جان خویش زید</p></div>
<div class="m2"><p>شهی که با سپه خود همیشه دارد جنگ</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>بیاد دار و فرامش مکن که سنگی سخت</p></div>
<div class="m2"><p>نواختی بسر داد و دانش و فرهنگ</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>برای آنکه بمغز تو ناگهان کوبد</p></div>
<div class="m2"><p>ودیعت است در انبان روزگار آن سنگ</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>فغان خلق برآوردی و برآید زود</p></div>
<div class="m2"><p>ز خانمان تو بر آسمان غریو و غرنگ</p></div></div>