---
title: >-
    شمارهٔ ۹
---
# شمارهٔ ۹

<div class="b" id="bn1"><div class="m1"><p>چون مرد پیشه کرد شکیب و ثبات را</p></div>
<div class="m2"><p>بشکست پرچم علم حادثات را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مرد آن بود که چون خطر آید بجاه وی</p></div>
<div class="m2"><p>قربان کند به مجد و شرافت حیات را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر خوانده ای به مدرسه اندر کتاب فقه</p></div>
<div class="m2"><p>فصل جهاد و مسئله واجبات را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دانی که حفظ دین و وطن بهر مرد حق</p></div>
<div class="m2"><p>فرض است آنچنانکه طهارت صلوة را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بگسل ز خصم و دست بدامان دوست زن</p></div>
<div class="m2"><p>خواهی اگر ز ورطه طریق نجات را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اینک دموکرات پس انقلاب ملک</p></div>
<div class="m2"><p>تهدید کرده کرد و لر و ترک و تات را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مشتی منات داده بیگ جوقه مرد لات</p></div>
<div class="m2"><p>تا نو کند پرستش لات و منات را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>لات از پی منات بتان را برد نماز</p></div>
<div class="m2"><p>تجدید کرده بتکده سومنات را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>یونان برغم نامه و اشراف ساختند</p></div>
<div class="m2"><p>دیموکرات را و اریستوکرات را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ما راه اتفاق و ترقی سپرده ایم</p></div>
<div class="m2"><p>مبعوث کرده ایم درین ره دعات را</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خصم ترور و دشمن دیموکراسی ایم</p></div>
<div class="m2"><p>در گوشمان مخوانید این ترهات را</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>در کام ما حدیث تروریست روز و شب</p></div>
<div class="m2"><p>ملح اجاج ساخته عذب فرات را</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گه بمب سایه بر سر همسایه گسترد</p></div>
<div class="m2"><p>گه موزر افکند نظر التفات را</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>این رندک عیار گمانش که مردمان</p></div>
<div class="m2"><p>نوشیده بنگ و بیخبرند این نکات را</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>حامی شود به رنجبران لیک در نهان</p></div>
<div class="m2"><p>قربان خویش کرده الوف و مآت را</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>هم راعیان ملت و هم داعیان دین</p></div>
<div class="m2"><p>هم مؤمنین کشور و هم مؤمنات را</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>غافل که بخردان جهان با هزار چشم</p></div>
<div class="m2"><p>نظارگی شوند جمیع الجهات را</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>خوانده است داستان اکلت الرطب و لیک</p></div>
<div class="m2"><p>فرموش کرده لفظ لفظت النواة را</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>الفاظ را بجای معانی ادا کند</p></div>
<div class="m2"><p>صد من دو غاز شد ثمن این مهملات را</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>یاللعجب جماعت دیموکراسیان</p></div>
<div class="m2"><p>ننهاده فرق مصدر واسم وادات را</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>خون کسان مزند چو زنگی شراب را</p></div>
<div class="m2"><p>مال کسان خورند چو هندو نبات را</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>لوزینه خوانده پیکر کعب الغزال را</p></div>
<div class="m2"><p>پالوده گفته خرمن شعرالبنات را</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>تنها نه طالبند که گیتی بهم خورد</p></div>
<div class="m2"><p>بل طامعند ریختن از هم کرات را</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ویران کنند خرگه حیوان و آدمی</p></div>
<div class="m2"><p>آتش زنند بیخ جماد و نبات را</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ای خواجه ترورگر، اگر اهل غیرتی</p></div>
<div class="m2"><p>در خاک خود پدید کن این معجزات را</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بستان ز روس شکی و تفلیس و گنجه را</p></div>
<div class="m2"><p>بگشا حصون هند و حصار هرات را</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ما را به خود گذار که از دایه کی سزد</p></div>
<div class="m2"><p>در حفظ طفل طعنه زند امهات را</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>تشخیص مالیات از آنکس روا بود</p></div>
<div class="m2"><p>کاندر خزانه بخش کند مالیات را</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>ابنای دین و مردم کشور همی کنند</p></div>
<div class="m2"><p>تفکیک حکم مفتی و اقضی القضاة را</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>باید وزیر جنگ بداند که ما هوار</p></div>
<div class="m2"><p>خازن چسان دهد به سپاهی برات را</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>بر اوست نی به عهده همسایه کز خرد</p></div>
<div class="m2"><p>محکم کند مبانی حصن کلات را</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>داند دلیر راندن شمشیر و تیر را</p></div>
<div class="m2"><p>بیند دبیر نکته کلک و دوات را</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>باید که ما ز مجلس ملی طلب کنیم</p></div>
<div class="m2"><p>اصلاح هر مفاسد و جمع شتات را</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>بیگانه را بدان چه که در کیش خویش من</p></div>
<div class="m2"><p>ممنوع داشتم ز مساکین زکوة را</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>این خانه من است و من آنجا مراقبم</p></div>
<div class="m2"><p>بر ماه و آفتاب عشی و غدات را</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>اندر حصار خود ندهم ره بهیچ قسم</p></div>
<div class="m2"><p>دزدان و رهزنان و عدات و وشات را</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>ایران به خاک خود نپذیرد ترور را</p></div>
<div class="m2"><p>آتش ز خویش دور کند کربنات را</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>کشتید پیشوا و گرامی وزیر ما</p></div>
<div class="m2"><p>کردید آشکار و عیان خبث ذات را</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>اینک به خون خواجه ما قصد کرده اید</p></div>
<div class="m2"><p>شایسته دیده اید مه سیئات را</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>زان پیشتر که در شط رنج افتی ای ترور</p></div>
<div class="m2"><p>بزدا ز سینه نقشه این شاهمات را</p></div></div>