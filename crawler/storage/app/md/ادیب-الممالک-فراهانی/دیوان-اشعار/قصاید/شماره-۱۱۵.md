---
title: >-
    شمارهٔ ۱۱۵
---
# شمارهٔ ۱۱۵

<div class="b" id="bn1"><div class="m1"><p>ببال ای تخت افریدون بناز ای تاج نوشروان</p></div>
<div class="m2"><p>که آمد شه درون کاخ و تابد مه بشادروان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بگشت اندر شود دهقان و آرد آب اندر جو</p></div>
<div class="m2"><p>بباغ اندر شود رزبان و کارد سرو در بستان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سپهر پیر بر شاه جوان زودا که بسپارد</p></div>
<div class="m2"><p>نگین و رایت شاپور و تخت و افسر ساسان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سحر در نامه «شوری » چنین خواندم بتوقیعی</p></div>
<div class="m2"><p>که والا حضرتش فرمود بر فرماندهان اعلان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>که در سال هزار و سیصد و سی و دو از هجرت</p></div>
<div class="m2"><p>سه شنبه دوم مرداد و بیست و هفتم شعبان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زحل در برج جوزا زهره و بهرام در خوشه</p></div>
<div class="m2"><p>عطارد با مه و خورشید ماوی جسته در سرطان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بفالی نیک و سالی خوب و روزی سعد و ماهی خوش</p></div>
<div class="m2"><p>که گل با شاخ هم پیوند و می با جام هم پیمان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خدیو شرق «احمدشاه » با اقبال روز افزون</p></div>
<div class="m2"><p>گذارد تاج بر تارک فرازد تخت در ایوان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سپهرم گفت یا بشری کزین پس در همه گیتی</p></div>
<div class="m2"><p>نبینی ملک بی صاحب نیابی گله بی چوپان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بگفتم لاتقل بشری و لکن بشریان زیرا</p></div>
<div class="m2"><p>دو شادی دست برهم داد توأم گشت در یک آن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>یکی تشریف تاج از تارک شه دوم آن باشد</p></div>
<div class="m2"><p>که ملک آزاد از فتنه است و بحر آسوده از طوفان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بهار عمر شد شاداب و باغ دولتش ایمن</p></div>
<div class="m2"><p>ز سرمای زمستان است و از گرمای تابستان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ز والا حضرت ایدون شکرها باید که در کشور</p></div>
<div class="m2"><p>عنایت راند بر مردم نیابت کرد از سلطان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چو موسی روز و شب این گوسفندانرا چرانیدی</p></div>
<div class="m2"><p>کنار چشمه صداء و گرد روضه سعدان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>مرا باشد شگفت از معجزش زیرا محال آید</p></div>
<div class="m2"><p>دو کار اندر یکی پنجه دو گوی اندر یکی چوگان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ولی این خواجه با یکدست هم گوی زمین در کف</p></div>
<div class="m2"><p>گرفته هم ربوده گوی فضل و دانش از اقران</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ز دست و پنجه مشگل گشا آن عقده بگشاید</p></div>
<div class="m2"><p>بآسانی که کس نتواندش بگشود با دندان</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بدستی کرد خامش فتنهای خارج از سوزش</p></div>
<div class="m2"><p>بدستی کرد ساکن انقلاب داخل از طغیان</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>حسودش خویشتن را همچو او پنداشته است اما</p></div>
<div class="m2"><p>کجا قاف تهجی هست همچون ق و القرآن</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چنو بسیار دیدستم به صورت یا بنام اما</p></div>
<div class="m2"><p>ندیدم همچو او یکتن بمعنی در همه کیهان</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>همی سنجد اشیا را بثقل و جثه و پیکر</p></div>
<div class="m2"><p>خلاف مرد کورا دانش و حکمت بود میزان</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>دو دست ایزد بما داد است با هم جفت چون بینی</p></div>
<div class="m2"><p>ز دست راست آنچ آید پدید از دست چپ نتوان</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>برادر بود با سلمان ابوذر لیک کی شاید</p></div>
<div class="m2"><p>که گنجد در دل صد بوذر اندک راز یک سلمان</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>سفینه ملک کامد مضطرب ز امواج پی در پی</p></div>
<div class="m2"><p>در این گرداب بی پایان و این دریای بی پایان</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>عنان این سفینه بود اندر دست او گفتی</p></div>
<div class="m2"><p>عنان رخش خود را داشت در کف رستم دستان</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>هر آنکس دید این قدرت سرود از گفته سعدی</p></div>
<div class="m2"><p>چه باک از موج بحر آن را که باشد نوح کشتیبان</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>سپردندش کلید مملکت پیش از ملک زیرا</p></div>
<div class="m2"><p>نخست آرند کنیت آنگهی نامست در عنوان</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بصورت کنیه پیش از نام باشد لیک در معنی</p></div>
<div class="m2"><p>بقای شخص از نام است و زو شد زنده جاویدان</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>شنیدستم که اندر دور استبداد شیطانی</p></div>
<div class="m2"><p>خطاب معشرالجن خواند اندر سوره رحمن</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>در آنجا کرد استدلال کاندر صفحه گیتی</p></div>
<div class="m2"><p>نباید مملکت بی شه نشاید خلق بی سلطان</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>بدو گویم که ای ناخوانده از قرآن بجز حرفی</p></div>
<div class="m2"><p>بریش خویشتن خندیدنت بایست ازین برهان</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>ندیدی خواجه چندین سال بی شه ملک و دولت را</p></div>
<div class="m2"><p>بآیین شهی بخشید آب و رونق و سامان</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>خداوندا تن این ملک مجروح است و دل خسته</p></div>
<div class="m2"><p>طبیبان عاجز از تدبیر و تب در آخرین بحران</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>نه خاصیت دهد معجون نه بهبودی رسد ز افسون</p></div>
<div class="m2"><p>نه سود از عوذه خاتون و حرز مادر صبیان</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>تو غمخواری طبیبی کیمیا دانی روان بخشی</p></div>
<div class="m2"><p>لبت چون عیسی مریم کفت چون موسی عمران</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>ببین اوضاع را درهم اساس ملک را برهم</p></div>
<div class="m2"><p>بنه این زخم را مرهم ببار این درد را درمان</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>ببین بر میزبان تنگ است منزل بس فرود آید</p></div>
<div class="m2"><p>بناهنگام و ناخوانده بخرگاه اندرش مهمان</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>بویژه اندرین خانه که از غوغای بیگانه</p></div>
<div class="m2"><p>نیارد هشت خالیگر بغیر از خون دل برخوان</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>خدا را با کلید فکر بگشا قفل این مشکل</p></div>
<div class="m2"><p>که رای مرد باشد چیره بر شمشیر و بر سوهان</p></div></div>