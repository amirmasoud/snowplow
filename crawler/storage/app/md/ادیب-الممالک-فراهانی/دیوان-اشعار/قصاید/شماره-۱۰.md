---
title: >-
    شمارهٔ ۱۰
---
# شمارهٔ ۱۰

<div class="b" id="bn1"><div class="m1"><p>ای شده در ره پی پذیره دارا</p></div>
<div class="m2"><p>چند کند دل بدوری تو مدارا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این منم از نار فرقت تو سراپای</p></div>
<div class="m2"><p>سوخته همچون وکیل صدر بخارا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>لعل چو پیروزه کرده اشک چو مرجان</p></div>
<div class="m2"><p>دیده عقیق یمان و رخ زر سارا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خونم در سینه شد طعام مناسب</p></div>
<div class="m2"><p>اشکم در دیده شد شراب گوارا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بی تو نخواهد دلم جمال جمیلان</p></div>
<div class="m2"><p>بی تو نبوسد لبم عذار عذارا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مغزم کاود سرود ترک غزلخوان</p></div>
<div class="m2"><p>جانم کاهد جمال شوخ دل آرا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>راندم از بزم خود عقیده عشرت</p></div>
<div class="m2"><p>خواندم بر وی طلاق خلع و مبارا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مژه بخواری همی سنبد خاره</p></div>
<div class="m2"><p>دیده بتاری همی شمارد تارا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چند بر این تن فلک پسندد خواری</p></div>
<div class="m2"><p>مهلا مهلا نه من حدیدم و خارا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هیچکسم در تعب نسازد یاری</p></div>
<div class="m2"><p>یارب ز این بیشتر ندارم یارا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>جانم جانو سیار غم بستاند</p></div>
<div class="m2"><p>گر ندهی دادم ای سلاله دارا</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>وه که مرا در حقت عقیده بود آنک</p></div>
<div class="m2"><p>در حق عیسی شنیده ام ز نصارا</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>رشک برم بر مصاحبان تو چونانک</p></div>
<div class="m2"><p>رشک ببردی هم بهاجر سارا</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>شاها بفراز قد و فتنه بیارام</p></div>
<div class="m2"><p>ماها بفروز چهر و خانه بیارا</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>یا سوی یاران شتاب یا ز بنانت</p></div>
<div class="m2"><p>مشکین فرما مشام باد صبا را</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>تو دل و جان و خرد ز صیدگه آری</p></div>
<div class="m2"><p>شاهان دراج و اسفرود و حبارا</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>هر سو تازی سمند آیدت از پی</p></div>
<div class="m2"><p>دلها اندر کمند همچو اسارا</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>تا نبود از شماره هیچ فزون تر</p></div>
<div class="m2"><p>سال بقایت فزون بود ز شمارا</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>نوشند آن باده دشمنانت که گویند</p></div>
<div class="m2"><p>نحن سکاری و ما هم بسکاری</p></div></div>