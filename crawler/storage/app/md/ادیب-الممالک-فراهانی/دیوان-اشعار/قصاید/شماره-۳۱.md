---
title: >-
    شمارهٔ ۳۱
---
# شمارهٔ ۳۱

<div class="b" id="bn1"><div class="m1"><p>ای دوخته بر قد تو دیبای صدارت</p></div>
<div class="m2"><p>طالع ز بنانت ید بیضای صدارت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با نقد شرف خواسته سرمایه دولت</p></div>
<div class="m2"><p>با گنج هنر یافته کالای صدارت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عدل است خلیل تو در ایوان ریاست</p></div>
<div class="m2"><p>عقل است دلیل تو به صحرای صدارت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عقلت نشود تیره ز جادوی زمانه</p></div>
<div class="m2"><p>مغزت نشود خیره ز سودای صدارت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مغرور نگردی تو ز افسانه دیوان</p></div>
<div class="m2"><p>مخمور نباشی تو ز صهبای صدارت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>رای تو شهابی است به گردون سیاست</p></div>
<div class="m2"><p>کلک تو نهنگی است بدریای صدارت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای کور و کران مژده که روح القدس آمد</p></div>
<div class="m2"><p>از معجزه لعل مسیحای صدارت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>المنة لله که بیاراست شهنشه</p></div>
<div class="m2"><p>توقیع کمال تو بطغرای صدارت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ای گشته ترا جفت بکابین عدالت</p></div>
<div class="m2"><p>معشوق دلارام دلارای صدارت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در گوش تو شد قرطه زرین سعادت</p></div>
<div class="m2"><p>همدوش تو شد شاهد زیبای صدارت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از شاخ تو سر زد گل بویای حقایق</p></div>
<div class="m2"><p>در کاخ تو در شد بت رعنای صدارت</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>آنی که صدارت بتقاضای تو برخاست</p></div>
<div class="m2"><p>طبع تو نکرده است تقاضای صدارت</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>تو پا بسر و چشم صدارت بنهادی</p></div>
<div class="m2"><p>و اکفآء تو جان ریخته در پای صدارت</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چون دست صدارت ز تو شد قدرت خود را</p></div>
<div class="m2"><p>بنمای ز بازوی توانای صدارت</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>شاید که همی درنگری کار جهان را</p></div>
<div class="m2"><p>با روشنی دیده بینای صدارت</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>شادا و خوشا خرم و خوبا که بتابید</p></div>
<div class="m2"><p>در جام تو شد آب گوارای صدارت</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>طوبی و هنیئا و مریئا لک کامروز</p></div>
<div class="m2"><p>در کام تو شد شهد مصفای صدارت</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>شیرین شودت کام که خالیگر گردون</p></div>
<div class="m2"><p>آراسته خوان تو به حلوای صدارت</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>با این همه شیرینی و چربی عجبم زانک</p></div>
<div class="m2"><p>گرمی نرسد بر تو ز صفرای صدارت</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>پر زهر بود ساغر جلاب وزارت</p></div>
<div class="m2"><p>پر خار بود خوشه خرمای صدارت</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>اما تو بتدبیر بگیری رطب از خار</p></div>
<div class="m2"><p>و افشانی پادزهر بمینای یصدارت</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ای خواجه من آنم که نکردم بهمه عمر</p></div>
<div class="m2"><p>کاری که پسندش نکند رای صدارت</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>گر درزی فکرم گه و بیگاه همیدوخت</p></div>
<div class="m2"><p>دیبای سخن بر قد و بالای صدارت</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>از زحمت من خسته نگشتی دل دستور</p></div>
<div class="m2"><p>وز پیکر من تنگ نشد جای صدارت</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>نه سفره صدرالوزرا مرتع من بود</p></div>
<div class="m2"><p>نه شد شرف بنده یغمای صدارت</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بودم به دبستان خود از کنگره چرخ</p></div>
<div class="m2"><p>مانند عطارد به تماشای صدارت</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>نادیده قدم چرخ دو تا در بر آنکس</p></div>
<div class="m2"><p>کاندر ز بر مسند یکتای صدارت</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>اما چو صدارت بتو شیدا شده امروز</p></div>
<div class="m2"><p>من نیز شدم واله و شیدای صدارت</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بی خدعه و اغراق به یک جو نستانم</p></div>
<div class="m2"><p>حکمی که ندارد خط و امضای صدارت</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بر نام همیون ترا خواهم ابدالدهر</p></div>
<div class="m2"><p>در بام فلک خطبه ی غرای صدارت</p></div></div>