---
title: >-
    شمارهٔ ۱۸۴
---
# شمارهٔ ۱۸۴

<div class="b" id="bn1"><div class="m1"><p>میر یحیای دولت آبادی</p></div>
<div class="m2"><p>ای که همت و زکی باشی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خواهم از حق بمتکای جلال</p></div>
<div class="m2"><p>تا صف حشر متکی باشی</p></div></div>