---
title: >-
    شمارهٔ ۲۰
---
# شمارهٔ ۲۰

<div class="b" id="bn1"><div class="m1"><p>شنیده‌ام چو سلیمان به تخت داد نشست</p></div>
<div class="m2"><p>خرد به درگهش استاد و چشم فتنه بخفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز دور دید که گنجشک نر به جفت عزیز</p></div>
<div class="m2"><p>ترانه‌خوان و سرود آنچنان که شاه شنفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من این رواق سلیمان توانم از منقار</p></div>
<div class="m2"><p>ز جای کند و به دریا فکند و خاکش رفت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به خشم شد شه و گنجشک بی‌نوا چون یافت</p></div>
<div class="m2"><p>که این حدیث شهنشه شنید و زان آشفت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بگفت خشم مگیر ای ملک ز لغزش من</p></div>
<div class="m2"><p>که پیش همسر خود لاف‌ها زدم بنهفت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چرا که لاف زدن کیمیای مرد بود</p></div>
<div class="m2"><p>برای آنکه کند جلوه در برابر جفت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گرفته بود دل شهریار از آن گفتار</p></div>
<div class="m2"><p>پس از شنیدن این عذر همچو گل بشکفت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شنیدن سخن راست خشم وی بزدود</p></div>
<div class="m2"><p>گناه او همه بخشید و عذر او پذرفت</p></div></div>