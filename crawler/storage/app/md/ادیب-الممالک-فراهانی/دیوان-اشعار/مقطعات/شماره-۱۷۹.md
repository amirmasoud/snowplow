---
title: >-
    شمارهٔ ۱۷۹
---
# شمارهٔ ۱۷۹

<div class="b" id="bn1"><div class="m1"><p>جواب نامه نیامد ز شاه و چشمم گشت</p></div>
<div class="m2"><p>سپید از ز فرات سرشک یعقوبی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به وعده نیز دلم خوش نکرد و از من داشت</p></div>
<div class="m2"><p>دریغ شمه از وعدهای عرقوبی</p></div></div>