---
title: >-
    شمارهٔ ۲۲۷ - نیز تاریخ دیگر
---
# شمارهٔ ۲۲۷ - نیز تاریخ دیگر

<div class="b" id="bn1"><div class="m1"><p>چو کوفت کاروان بلاطبل اصلا</p></div>
<div class="m2"><p>شد بر فلک زکاخ شرف بانک الرحیل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زآوای کوس ایتهالنفس ارجعی</p></div>
<div class="m2"><p>افتاد شور و ولوله در عترت خلیل</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فرخ رخی ز آل علی مهتری سترگ</p></div>
<div class="m2"><p>صافی دلی زبیت صفی سیدی جلیل</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>«عبدالله بن عبدالباقی » که فکرتش</p></div>
<div class="m2"><p>شد فضل را مربی و فرهنگ را دلیل</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کلکش درخت طوبی و دفتر جمال حور</p></div>
<div class="m2"><p>خویش بهشت خرم و طبعش چو سلسبیل</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نثرش ستاره ریخته بر اطلس سپهر</p></div>
<div class="m2"><p>نظمش فکنده موج برخسار رود نیل</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در حل مشکلات قوافی بارتجال</p></div>
<div class="m2"><p>زد هودج عروس معانی بپشت پیل</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بس خامه اش بکوثر دانش در آشنا</p></div>
<div class="m2"><p>برده سبق ز قادمه پر جبرئیل</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اندر سرود شعر تخلص امیر داشت</p></div>
<div class="m2"><p>کاندر سخن نبود امیری چو او نبیل</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>روز دوشنبه نیمه شعبان برات خلد</p></div>
<div class="m2"><p>بگرفت و شد بدار بقا سالک سبیل</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شد خانه کمال زهجران او خراب</p></div>
<div class="m2"><p>شد پیکر کلام ز فقدان او علیل</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>فکرت عقیم و هوش مشوش هنر غریب</p></div>
<div class="m2"><p>دانش یتیم و عقل پریشان ادب ذلیل</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>دستار برگرفت ز سوگش خطیب چرخ</p></div>
<div class="m2"><p>وز ماتمش دبیر فلک جامه زد بنیل</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>جستم ز بحر طبع «امیری » بالتماس</p></div>
<div class="m2"><p>تاریخ سال رحلت آن میر بی عدیل</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گفتا یکی نخست رقم کن «بصحن خلد»</p></div>
<div class="m2"><p>ز آن پس نگار «شد بلب جوی سلسبیل »</p></div></div>