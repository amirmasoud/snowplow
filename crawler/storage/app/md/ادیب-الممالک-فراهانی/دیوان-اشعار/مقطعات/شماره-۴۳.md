---
title: >-
    شمارهٔ ۴۳
---
# شمارهٔ ۴۳

<div class="b" id="bn1"><div class="m1"><p>بسکه از بخت خویش مایوسم</p></div>
<div class="m2"><p>جاودان اندرین سرای سپنج</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روز تا شب بسان نرادان</p></div>
<div class="m2"><p>با غم دل همی زنم شش و پنج</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>استخوانیست پیکرم بی گوشت</p></div>
<div class="m2"><p>مانده بر جای چون شه شطرنج</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پیکرم را بود چو زلف بتان</p></div>
<div class="m2"><p>شکن و تاب و پیچ و چین و شکنج</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بدماغ و دلم زمانه نهشت</p></div>
<div class="m2"><p>فکر موزون و طبع قافیه سنج</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>راست گوئی که خورده ام افیون</p></div>
<div class="m2"><p>یا شراب و حشیش و بذر البنج</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سمر است این سخن که گنج رسد</p></div>
<div class="m2"><p>مردمان را پس از کشیدن رنج</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر چنین است بنده را ز چه روی</p></div>
<div class="m2"><p>از پس رنجها نیاید گنج</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آری ار بخت من مساعد بود</p></div>
<div class="m2"><p>تن زارم نخستی از قولنج</p></div></div>