---
title: >-
    شمارهٔ ۲۰۸ - در ستایش صدر اعظم
---
# شمارهٔ ۲۰۸ - در ستایش صدر اعظم

<div class="b" id="bn1"><div class="m1"><p>آنکه درگاهش با چرخ همی گوید</p></div>
<div class="m2"><p>نه مرا مانی و نه با تو رقیبستم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که تو مطموره بیدادی و من دایم</p></div>
<div class="m2"><p>ملجا خائف و ما و ای غریبستم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای خداوند پی مدح تو در محضر</p></div>
<div class="m2"><p>من یکی شاعر دانای لبیبستم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>که گهر ریزم و از غالیه دان خیزم</p></div>
<div class="m2"><p>مشک تر بیزم و با نفخه طبیبستم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همه دانند ز قحطانی و عدنانی</p></div>
<div class="m2"><p>بعد اما بعد این بنده خطیبستم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر ادیبم بممالک شمری شاید</p></div>
<div class="m2"><p>که ممالک را من نیک ادیبستم</p></div></div>