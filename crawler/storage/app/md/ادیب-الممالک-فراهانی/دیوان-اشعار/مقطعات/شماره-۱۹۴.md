---
title: >-
    شمارهٔ ۱۹۴
---
# شمارهٔ ۱۹۴

<div class="b" id="bn1"><div class="m1"><p>گویند در عمارت بابل بجای ماند</p></div>
<div class="m2"><p>این نکته یادگار ز شاپور اردشیر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گردون مقامر است و زمین نطع برد و باخت</p></div>
<div class="m2"><p>ما مردمان چو مهرهٔ شطرنج و نردشیر</p></div></div>