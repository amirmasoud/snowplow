---
title: >-
    شمارهٔ ۲۲۴
---
# شمارهٔ ۲۲۴

<div class="b" id="bn1"><div class="m1"><p>خدایگانا ای آنکه شاهد ظفرت</p></div>
<div class="m2"><p>بکاخ بخت قرین با عروس اقبال است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز سعی و همت و رای تو ملک و دولت و دین</p></div>
<div class="m2"><p>هژیر و فرخ و فرخنده و قوی حال است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رخت معاینه ماند بآفتاب منیر</p></div>
<div class="m2"><p>دلت چون قلزم و دستت چو ابر هطال است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به پیش بحر عطای تو ابر قطره شود</p></div>
<div class="m2"><p>بر ترازوی جود تو کوه مثقال است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به حضرت تو رهی را شکایتی پنهان</p></div>
<div class="m2"><p>ز چاکران درت بر سبیل اجمال است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در آن برات که فرمودیم بخازن جیب</p></div>
<div class="m2"><p>علی الدوام پی دفع وقت و اهمال است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>حریف پنجه این پهلوان مردافکن</p></div>
<div class="m2"><p>نه گیو گودرزستی نه رستم زال است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مرا تقاضا زین کس که آبرویش نیست</p></div>
<div class="m2"><p>درست بیختن آب جو بغربال است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بخاک پای عزیز تو ای شه از کم و بیش</p></div>
<div class="m2"><p>جز این حدیث نرانم که جزو امثال است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نصیب ما به جهان دوغ و ترب هم نشود</p></div>
<div class="m2"><p>که صرف جیب خداوند دست بقال است</p></div></div>