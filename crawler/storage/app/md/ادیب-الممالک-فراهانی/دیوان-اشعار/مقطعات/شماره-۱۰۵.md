---
title: >-
    شمارهٔ ۱۰۵
---
# شمارهٔ ۱۰۵

<div class="b" id="bn1"><div class="m1"><p>صنیع الممالک بود طرفه نقشی</p></div>
<div class="m2"><p>که هر بنده را واجب است احترامش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو دست طبیعت بزندان زهدان</p></div>
<div class="m2"><p>ز ستر عدم خواست دادن خرامش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز هر کشوری صانعی خواست ماهر</p></div>
<div class="m2"><p>پی صنع آن ذات در بطن مامش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یکی ساخت چشمش یکی ساخت گوشش</p></div>
<div class="m2"><p>یکی هر دو بازو یکی هر دو گامش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یکی کرد از خون لبالب عروقش</p></div>
<div class="m2"><p>یکی ساخت بر تن مرتب عظامش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مع القصه کردند شکلی که مادر</p></div>
<div class="m2"><p>ندانست حال رضاع و فطامش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو زنگی جمالش چو ترکی فعالش</p></div>
<div class="m2"><p>چو رومی خصالش چو هندی کلامش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>یکی اهل داهومه پنداشت او را</p></div>
<div class="m2"><p>یکی خواندی از مردم یار یامش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سرونش چو گاو و سرینش چو اشتر</p></div>
<div class="m2"><p>چو زرافه کردن چو اشتر سنامش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز هر موی او می برآمد مقامی</p></div>
<div class="m2"><p>که دانای آن خواند والامقامش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سپس در پی نام او در ممالک</p></div>
<div class="m2"><p>نمودند شوری خواص و عوامش</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چو در صنع از جمله را بود شرکت</p></div>
<div class="m2"><p>صنیع الممالک نهادند نامش</p></div></div>