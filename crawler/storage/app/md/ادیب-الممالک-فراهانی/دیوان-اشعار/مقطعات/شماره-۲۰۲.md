---
title: >-
    شمارهٔ ۲۰۲
---
# شمارهٔ ۲۰۲

<div class="b" id="bn1"><div class="m1"><p>عمید سلطنه سردار امجد آنکه ندید</p></div>
<div class="m2"><p>دو چشم گیتی چون او یکی سپهبد راد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دو چیز بنده فرمان اوست خامه و تیغ</p></div>
<div class="m2"><p>دو چیز زنده بگفتار اوست دانش و داد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از آن دو چیز شود پایه هنر ستوار</p></div>
<div class="m2"><p>ازین دو چیز بود خانه خرد آباد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بدان دو خرگه بیداد را زند آتش</p></div>
<div class="m2"><p>بدین دو بنگه فرهنگ را نهد بنیاد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هزار آزاد او را بمهر بنده شود</p></div>
<div class="m2"><p>هزار بنده زبند ستم کند آزاد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>امیدوار چنانم ز کردگار جهان</p></div>
<div class="m2"><p>که جاودانه تنش زنده باد و جانش شاد</p></div></div>