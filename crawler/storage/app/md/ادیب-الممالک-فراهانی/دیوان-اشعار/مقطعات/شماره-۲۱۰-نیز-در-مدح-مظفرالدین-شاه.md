---
title: >-
    شمارهٔ ۲۱۰ - نیز در مدح مظفرالدین شاه
---
# شمارهٔ ۲۱۰ - نیز در مدح مظفرالدین شاه

<div class="b" id="bn1"><div class="m1"><p>کسان زخارف دنیا بدین خریدارند</p></div>
<div class="m2"><p>تو این جهان بفروشی و نام نیک خری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو نام نیک بماند بجای بگذاریش</p></div>
<div class="m2"><p>چو مال بگذرد از وی تو زودتر گذری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>برای یکدرم آنان هزار رنج برند</p></div>
<div class="m2"><p>تو با یکی درم الحق هزار گنج بری</p></div></div>