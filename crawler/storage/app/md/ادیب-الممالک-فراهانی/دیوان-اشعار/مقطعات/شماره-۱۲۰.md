---
title: >-
    شمارهٔ ۱۲۰
---
# شمارهٔ ۱۲۰

<div class="b" id="bn1"><div class="m1"><p>ای دبیر حضرت ای میری که ذکر خیر تو</p></div>
<div class="m2"><p>آشکارا نزد ترک و دیلم و تازی کنم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن شنیدم کاین رهی دیری است کاندیدا شده</p></div>
<div class="m2"><p>گر چنین باشد سزد شوخی و طنازی کنم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>لفظ کاندیدا چو می باشد به معنی نامزد</p></div>
<div class="m2"><p>این سؤال از حضرتت در نکته پردازی کنم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر رهی را نامزد از بهر کاری کرده اند</p></div>
<div class="m2"><p>کو عروس من که با او نامزد بازی کنم</p></div></div>