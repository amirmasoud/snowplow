---
title: >-
    شمارهٔ ۱۲۹
---
# شمارهٔ ۱۲۹

<div class="b" id="bn1"><div class="m1"><p>گر آسمان به دلم صد کوه بار کند</p></div>
<div class="m2"><p>از نوک خامه خود فرهاد کوهکنم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از روزگار سیه خار است در جگرم</p></div>
<div class="m2"><p>وز نام فرخ شه شهد است در دهنم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر بخت یار شود کار استوار شود</p></div>
<div class="m2"><p>این شهد را به مزم و آن خار را بکنم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر جا که بارگهی است خورشید بارگهم</p></div>
<div class="m2"><p>هر جا که انجمنی است سالار انجمنم</p></div></div>