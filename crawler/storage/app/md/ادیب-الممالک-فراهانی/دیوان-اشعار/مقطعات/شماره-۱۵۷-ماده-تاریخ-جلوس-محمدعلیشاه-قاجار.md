---
title: >-
    شمارهٔ ۱۵۷ - ماده تاریخ جلوس محمدعلیشاه قاجار
---
# شمارهٔ ۱۵۷ - ماده تاریخ جلوس محمدعلیشاه قاجار

<div class="b" id="bn1"><div class="m1"><p>شهنشاه ایران محمدعلی شه</p></div>
<div class="m2"><p>بگردون دولت برافراشت خرگه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سریر از سپهر آمدش افسر از خور</p></div>
<div class="m2"><p>سپاه از کواکب شدش رایت از مه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نسیم عنایات او باغ دین را</p></div>
<div class="m2"><p>چو اردی بهشت است یا فروردین مه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همایون خدیوی که شاهان ببارش</p></div>
<div class="m2"><p>سر بندگی سوده بر خاک درگه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز سهم خدنگش هژبران جنگی</p></div>
<div class="m2"><p>خزیدند در غارها همچو روبه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شها آسمان از خدائی و شاهی</p></div>
<div class="m2"><p>نصیب شهان پنج داد و ترا ده</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>همین بس که رای ترا کرد امضا</p></div>
<div class="m2"><p>شهنشاه ماضی سقی الله رمسه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تو آزاد کردی همه بندگان را</p></div>
<div class="m2"><p>که بد قلبت از سر این حکمت آگه</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بدانستی ای شه که بیمار ملکت</p></div>
<div class="m2"><p>چو دارو نیابد بمیرد بنا گه</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بدانستی ای شه که در شام غفلت</p></div>
<div class="m2"><p>نسوزد چراغ ستم تا سحرگه</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نشاندی شه معدلت را به کرسی</p></div>
<div class="m2"><p>کشیدی برون یوسف داد از چه</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بخواندی همه مردمان هنرور</p></div>
<div class="m2"><p>براندی همه شوخ چشمان گمره</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ز روی تو شد دیده ملک روشن</p></div>
<div class="m2"><p>چو از معجز عیسوی چشم اکمه</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>خیال نفاق از وفاقت مشوش</p></div>
<div class="m2"><p>جمال ستم ز اعتدالت مشوه</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>رهی ساختی از کرامت که دایم</p></div>
<div class="m2"><p>رود کاروان عدالت در آن ره</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بقا آنت تشبیه کردم ولیکن</p></div>
<div class="m2"><p>خرد بانک زد کای فلان قصه کوته</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ازیرا که تشبیه کامل به ناقص</p></div>
<div class="m2"><p>خلاف است از این گفته استغفرالله</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>که سال جلوس همایونش آمد</p></div>
<div class="m2"><p>خداوند قاآن محمدعلی شه</p></div></div>