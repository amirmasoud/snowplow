---
title: >-
    شمارهٔ ۱۶۱
---
# شمارهٔ ۱۶۱

<div class="b" id="bn1"><div class="m1"><p>تا سپهدار به شطرنج هنر</p></div>
<div class="m2"><p>چیره بر دشمن خونخوار شده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ماحی سیرت ناهنجاران</p></div>
<div class="m2"><p>حامی زمره احرار شده</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چتر استبداد از صرصر داد</p></div>
<div class="m2"><p>پست و وارون و نگونسار شده</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با عدالت همه جا بود رفیق</p></div>
<div class="m2"><p>با خرد در همه جا یار شده</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شهر ری از قدمش خرم و شاد</p></div>
<div class="m2"><p>خوشتر از خلخ و فرخار شده</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شه محمدعلی از هیبت او</p></div>
<div class="m2"><p>خوار و شرمنده ز کردار شده</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شبش از برق چو روز روشن</p></div>
<div class="m2"><p>روزش از دود شب تار شده</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آخرالامر ز دیهیم و سریر</p></div>
<div class="m2"><p>گشته مستعفی و بیزار شده</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جستم از طبع امیری تاریخ</p></div>
<div class="m2"><p>گفت «شه مات سپهدار شده »</p></div></div>