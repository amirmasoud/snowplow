---
title: >-
    شمارهٔ ۵۶
---
# شمارهٔ ۵۶

<div class="b" id="bn1"><div class="m1"><p>ای وزرای عظام ای که در این ملک</p></div>
<div class="m2"><p>بر رمه عدل کردگار شبانید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گله شما را سپرده صاحب اغنام</p></div>
<div class="m2"><p>تا به چراگاه عافیت بچرانید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همتی آرید و گوسفند خدا را</p></div>
<div class="m2"><p>از کف گرگان خیره سر برهانید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ورنه چون از هم درید و خورد و تبه کرد</p></div>
<div class="m2"><p>چاره ندارید و معذرت نتوانید</p></div></div>