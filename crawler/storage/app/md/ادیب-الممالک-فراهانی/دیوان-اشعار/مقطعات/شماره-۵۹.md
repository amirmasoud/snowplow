---
title: >-
    شمارهٔ ۵۹
---
# شمارهٔ ۵۹

<div class="b" id="bn1"><div class="m1"><p>از سر این شهریار تاج بنازد</p></div>
<div class="m2"><p>وز قدم شاه تخت عاج بنازد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا ملک آید به باغ ملک خرامان</p></div>
<div class="m2"><p>سرو شود در سجود و کاج بنازد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون به عدالت زده خراج ستاند</p></div>
<div class="m2"><p>در کنف شه ده از خراج بنازد</p></div></div>