---
title: >-
    شمارهٔ ۱۹۹ - در مقدمه شاهنامه
---
# شمارهٔ ۱۹۹ - در مقدمه شاهنامه

<div class="b" id="bn1"><div class="m1"><p>فروشد به فرمان یزدان پاک</p></div>
<div class="m2"><p>ز رخشنده گردون بر این تیره خاک</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یکی نامه آسمانی بدست</p></div>
<div class="m2"><p>نبشته در آن راز بالا و پست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همه رازها در دل یکدیگر</p></div>
<div class="m2"><p>نهفته چو شیرینی اندر شکر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کلید در این فرزنده گنج</p></div>
<div class="m2"><p>سپرده است در پنجه هفت و پنج</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>که هستند فرمان گذاران وی</p></div>
<div class="m2"><p>همه از درون رازداران وی</p></div></div>