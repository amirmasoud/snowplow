---
title: >-
    شمارهٔ ۱۸۸ - شکایت از روزنامه نگاری خود
---
# شمارهٔ ۱۸۸ - شکایت از روزنامه نگاری خود

<div class="b" id="bn1"><div class="m1"><p>خدایگان من از حال بنده بیخبری</p></div>
<div class="m2"><p>که بر تنم چه رسید از غم زمانه همی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز غره رجب الفرد تاکنون شب و روز</p></div>
<div class="m2"><p>به پیش تیر بلا شد تنم نشانه همی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زمانه بسکه زافلاس و فقر و نوبه و تب</p></div>
<div class="m2"><p>نواخت بر سر من چوب و تازیانه همی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دلم سراچه غم شد چنانکه پنداری</p></div>
<div class="m2"><p>که مرغ غم بدلم بسته آشیانه همی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز سوئی آتش تب در جگر ز دیگر سوی</p></div>
<div class="m2"><p>غم زمانه زند بر دلم زبانه همی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هزار مرتبه عازم شدم که دریابم</p></div>
<div class="m2"><p>شفای عاجل از آن فرخ آستانه همی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بجای آنکه جدا بودم از درت چندی</p></div>
<div class="m2"><p>بیابم از درت اقبال جاودانه همی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بشیر چرخ کنم آن معاملت که نمود</p></div>
<div class="m2"><p>بشیر بادیه بشربن بوعوانه همی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>رمق به پیکرم اندر نبود آن مقدار</p></div>
<div class="m2"><p>که یک قدم سوی بیرون نهم ز خانه همی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چنان شدم ز نقاهت که گاه جنبش و سیر</p></div>
<div class="m2"><p>ز تن جدا شودم استخوان شانه همی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز روزنامه چه گویم که قدر خانه من</p></div>
<div class="m2"><p>از او شکسته بذات حق یگانه همی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مرا فکنده به بحری که هر چه می نگرم</p></div>
<div class="m2"><p>پدید نیست در آن ساحل و کرانه همی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نه قابلم بکرامات و فضل ملتیان</p></div>
<div class="m2"><p>نه شاملم شود الطاف خسروانه همی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چو نام رسمیت آزادیش گرفته به طبع</p></div>
<div class="m2"><p>کس نمی خرد او را بنیم آنه همی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>وز آن سبب که هواخواه دولت است کسی</p></div>
<div class="m2"><p>مساعدت نکند باری از اعانه همی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>حقوق نه مهی این اداره همچو جنین</p></div>
<div class="m2"><p>بمانده در رحم مادر خزانه همی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چگونه طبع توان کردن این جریده بوقت</p></div>
<div class="m2"><p>که ماهیانه آن گشته سالیانه همی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>عجب تر آنکه ندانم پس از سه ماه دگر</p></div>
<div class="m2"><p>مرا دهند حقوق گذشته یا نه همی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ز بس که در پی آن با خزانه دار عنود</p></div>
<div class="m2"><p>کنم فروتنی و عجز و استکانه همی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>به جای ذکر خدا از زبان من شب و روز</p></div>
<div class="m2"><p>شده است «یا لولو یاشیندلر» ترانه همی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>حکایت من و این روزنامه چون مرغی است</p></div>
<div class="m2"><p>که او فتاده بدام از هوای دانه همی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>سرود نظم من اندر سماع اهل خرد</p></div>
<div class="m2"><p>نکوتر است زچنگ و نی و چغانه همی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بزر خرند حدیثم ز جد و هزل اگر</p></div>
<div class="m2"><p>بود ز حکمت و تاریخ یا فسانه همی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ولی چه سود که کس در زمانه با یکدست</p></div>
<div class="m2"><p>گرفت نتوان هرگز دو هندوانه همی</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ازین ره است که زلف عروس فکرت من</p></div>
<div class="m2"><p>ز دست روز سیاه و غم شبانه همی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>چنان شده است پریشان که هیچ مشاطه</p></div>
<div class="m2"><p>نمی تواندش آراستن بشانه همی</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بود گواه من این نامه کش به خون جگر</p></div>
<div class="m2"><p>نبشته ساختمش بر درت روانه همی</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>برای آنکه زنم بوسه درگهت شب و روز</p></div>
<div class="m2"><p>ز فضل و رحمت تو جویم استعانه همی</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>کنون سزد که سپاس ترا ادا سازم</p></div>
<div class="m2"><p>به کلک روشن و گفتار صادقانه همی</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>کمان شکرم تیر دعا زند به هدف</p></div>
<div class="m2"><p>اگر چه نیست جز این سهم در کتانه همی</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>چنانکه خاطر ما از تو جاودان شاد است</p></div>
<div class="m2"><p>خدای بر تو دهد عمر جاودانه همی</p></div></div>