---
title: >-
    شمارهٔ ۷۷
---
# شمارهٔ ۷۷

<div class="b" id="bn1"><div class="m1"><p>مگرد ای پسر گرد دانش که دانش</p></div>
<div class="m2"><p>تنت غرق اندوه تا گردن آرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ره ابلهی جوی کآیین فکرت</p></div>
<div class="m2"><p>ترا روز تا شب به خون خوردن آرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تو خر زای و خر میر و خر زی که گردون</p></div>
<div class="m2"><p>پس از هفتصد سال خر مردن آرد</p></div></div>