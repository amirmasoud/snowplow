---
title: >-
    شمارهٔ ۱۸۵
---
# شمارهٔ ۱۸۵

<div class="b" id="bn1"><div class="m1"><p>صاحبا چند خفته ای در مهد</p></div>
<div class="m2"><p>بجنابت ز عین ناپاکی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آفتابم من ارشنیدستی</p></div>
<div class="m2"><p>قصه آفتاب و سکاکی</p></div></div>