---
title: >-
    شمارهٔ ۲۳۹
---
# شمارهٔ ۲۳۹

<div class="b" id="bn1"><div class="m1"><p>لاله رنگ رنگ ازو روید</p></div>
<div class="m2"><p>بوستان بهار را ماند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چشم شوخی گشوده بر رخ خلق</p></div>
<div class="m2"><p>نرگس آبدار را ماند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از تبسم همی شکر ریزد</p></div>
<div class="m2"><p>لب لعل نگار را ماند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جز بالماس سفته می نشود</p></div>
<div class="m2"><p>لؤلؤ شاهوار را ماند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اول و آخرش نمایان نیست</p></div>
<div class="m2"><p>عرصه روزگار را ماند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گرزها می زنند بر بسرش</p></div>
<div class="m2"><p>خسته روزگار را ماند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تیر رستم در او گرفته قرار</p></div>
<div class="m2"><p>چشم اسفندیار را ماند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جای پستان همی مکد انگشت</p></div>
<div class="m2"><p>کودک شیرخوار را ماند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>محکش میزنند هر شب و روز</p></div>
<div class="m2"><p>سیم کامل عیار را ماند</p></div></div>