---
title: >-
    شمارهٔ ۱۵۰
---
# شمارهٔ ۱۵۰

<div class="b" id="bn1"><div class="m1"><p>گربه و موش بهم ساخته اند ای بقال</p></div>
<div class="m2"><p>وای بر خیک پنیر و سبد میوه تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای پدر خانه و باغت به رقیبان دادند</p></div>
<div class="m2"><p>دختر بیوه تو وان پسر لیوه تو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گشت قربان می و ساغر و شیرینی و شمع</p></div>
<div class="m2"><p>زر تو سیم تو آیینه تو جیوه تو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای پدر مرده بخود باش که در این دو سه روز</p></div>
<div class="m2"><p>جفت همسایه شود مادرک بیوه تو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>میتوان چاره این درد گران کرد ولی</p></div>
<div class="m2"><p>خرد و هوش ندارد سر کالیوه تو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>لیک خوش باش که از پای کند میکائیل</p></div>
<div class="m2"><p>کفش تو چکمه تو موزه تو گیوه تو</p></div></div>