---
title: >-
    شمارهٔ ۱۶۴ - در زیر عکس جناب منتظم الدوله آقای مصطفی قلیخان فیروزکوهی نوشتم
---
# شمارهٔ ۱۶۴ - در زیر عکس جناب منتظم الدوله آقای مصطفی قلیخان فیروزکوهی نوشتم

<div class="b" id="bn1"><div class="m1"><p>منتظم الدوله فیروز بخت</p></div>
<div class="m2"><p>داور گردون فر کیوان شکوه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یافت چو مه ماهچه رایتش</p></div>
<div class="m2"><p>کرسی فیروزه ز فیروزه کوه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اختر فیروز مرا ره نمود</p></div>
<div class="m2"><p>در بر آن خواجه دانش پژوه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عکس رخش بر ورق انداختم</p></div>
<div class="m2"><p>با صفی از ناموران همگروه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا که شود خیره ز نورش عیون</p></div>
<div class="m2"><p>تا که شود تیره ز رویش وجوه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا ابد از دست دل و دست او</p></div>
<div class="m2"><p>خوشد و جوشد دل دریا و کوه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جودی و مهلان بر حلمش سبک</p></div>
<div class="m2"><p>قلزم و جیحون ز کفش در ستوه</p></div></div>