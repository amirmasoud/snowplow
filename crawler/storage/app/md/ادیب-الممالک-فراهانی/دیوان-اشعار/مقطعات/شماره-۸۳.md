---
title: >-
    شمارهٔ ۸۳
---
# شمارهٔ ۸۳

<div class="b" id="bn1"><div class="m1"><p>شاهین تیز پنجه زرین پرم پرید</p></div>
<div class="m2"><p>خواب از درون دیده و هوش از سرم پرید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در جنگ اژدها نپریدی کک نرم</p></div>
<div class="m2"><p>اینک ز بانک مرغ خروس نرم پرید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون کودکی که گفت به همسایه کای عمو</p></div>
<div class="m2"><p>از آشیان به بام شما کفترم پرید</p></div></div>