---
title: >-
    شمارهٔ ۱۸۰ - به مستشار عدلیه بالبداهة نگاشته
---
# شمارهٔ ۱۸۰ - به مستشار عدلیه بالبداهة نگاشته

<div class="b" id="bn1"><div class="m1"><p>ز من ایصبا نهانی تو به مستشار برگو</p></div>
<div class="m2"><p>سحر آمدم به کویت به شکار رفته بودی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خبر از وزیر جستم که نبود در رکابت</p></div>
<div class="m2"><p>تو که سگ نبرده بودی بچه کار رفته بودی</p></div></div>