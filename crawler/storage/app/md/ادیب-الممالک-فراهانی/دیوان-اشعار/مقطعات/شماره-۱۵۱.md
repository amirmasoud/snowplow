---
title: >-
    شمارهٔ ۱۵۱
---
# شمارهٔ ۱۵۱

<div class="b" id="bn1"><div class="m1"><p>سعید سلطنه ای آنکه تا ابد خجلم</p></div>
<div class="m2"><p>ز فضل بی شمر و لطف بی کرانه تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گمانم آنکه فرامش نکرده ای که رهی</p></div>
<div class="m2"><p>برای حاجتی آمد درون خانه تو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حقوق خود ز وزیر خزانه کرد طلب</p></div>
<div class="m2"><p>بعون و همت و الطاف جاودانه تو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پس از سه روز تهی آستین فراز آمد</p></div>
<div class="m2"><p>رسول بنده مسکین از آستانه تو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مرا چو اخوه یوسف پدر همی بفروخت</p></div>
<div class="m2"><p>مهین برادر فرخنده یگانه تو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کنون سزد ز کریمی که این ترانه من</p></div>
<div class="m2"><p>بدو رسانی و مستش کند ترانه تو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بگو بحضرت وی آنکه لعل و سنگ شود</p></div>
<div class="m2"><p>به یک ترازو سنجیده در خزانه تو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به جای آنکه خسان را دفاع داده بشعر</p></div>
<div class="m2"><p>ز خاندان و تبار تو و بطانه تو</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چه کرده ام من مسکین که چون اسیر ذلیل</p></div>
<div class="m2"><p>شدم ز قهر گرفتار تازیانه تو</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کجا شد آن کرم وجود و رادی و مردی</p></div>
<div class="m2"><p>کجا شد آن خرد و داد عادلانه تو</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>من آن عقاب قوی پنجه ام که دست قضا</p></div>
<div class="m2"><p>فکنده است به دامم به طمع دانه تو</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تو در لطیفه سرآئی هزار دستانی</p></div>
<div class="m2"><p>ولی عقاب نگنجد در آشیانه تو</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به نزد قاضی وجدان اگر برد دعوی</p></div>
<div class="m2"><p>درین ستم چو بود عذر یا بهانه تو</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>یکی به عاقبت کار خود نگر که نبست</p></div>
<div class="m2"><p>سعادت ابدی عهد با زمانه تو</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>شود که روزی سازد تنت نشانه تیر</p></div>
<div class="m2"><p>کسی که بوده دلش سالها نشانه تو</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>تو میروی و ازین کارهای زشت پلید</p></div>
<div class="m2"><p>همی بماند اندر جهان فسانه تو</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>مرا مگیر درین اشتلم که گرمتر است</p></div>
<div class="m2"><p>زبانه عطش وجوعم از زبانه تو</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ز من بجان تو خواری فزون رسد اما</p></div>
<div class="m2"><p>جز این نمانده دگر تیر در کتانه تو</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>که بشکنی دهنم را به مشت و بار خدای</p></div>
<div class="m2"><p>همی بساید با سنگ قهر چانه تو</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>وجوه خالصه و نقد و جنس دیوان شد</p></div>
<div class="m2"><p>تمام صرف می و بربط چغانه تو</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>حقوق مردم بیچاره سالها گردید</p></div>
<div class="m2"><p>نثار مطبخ و اصطبل و قهوخانه تو</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ولی چو بنده تقاضای رسم خویش کنم</p></div>
<div class="m2"><p>چو شاخ کرگدنان برخورد به شانه تو</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>از آن بخیره و غافل که جمله نزد منست</p></div>
<div class="m2"><p>حساب و دفتر روزانه و شبانه تو</p></div></div>