---
title: >-
    شمارهٔ ۱۷۸
---
# شمارهٔ ۱۷۸

<div class="b" id="bn1"><div class="m1"><p>یا امین الحق کهف الخلق شمس المذهب</p></div>
<div class="m2"><p>انت فی دیباجة العلیا طراز المذهب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عش حساما ماضیا فی الدین سیفا قاضیا</p></div>
<div class="m2"><p>فی الوری یا حجة الاسلام یا عبدالنبی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ما همه طفل دبستانیم و تو شیخ طریق</p></div>
<div class="m2"><p>ای که پیر عقل باشد در دبستانت صبی</p></div></div>