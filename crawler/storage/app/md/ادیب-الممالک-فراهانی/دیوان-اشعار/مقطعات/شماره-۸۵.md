---
title: >-
    شمارهٔ ۸۵
---
# شمارهٔ ۸۵

<div class="b" id="bn1"><div class="m1"><p>اندر پی اقتباس نورم</p></div>
<div class="m2"><p>همچون موسی ز نخله طور</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اخلاص مرا به حضرت خود</p></div>
<div class="m2"><p>دانی که عیان بود نه مستور</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مهر تو درون سینه من</p></div>
<div class="m2"><p>بنهفته چو می درون انگور</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای قطب رجای آدمیت</p></div>
<div class="m2"><p>ای مرکز احتیاج جمهور</p></div></div>