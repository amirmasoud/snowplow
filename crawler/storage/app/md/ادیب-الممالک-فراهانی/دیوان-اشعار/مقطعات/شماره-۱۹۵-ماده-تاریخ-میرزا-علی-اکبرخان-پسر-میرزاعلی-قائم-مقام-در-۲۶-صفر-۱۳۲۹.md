---
title: >-
    شمارهٔ ۱۹۵ - ماده تاریخ میرزا علی اکبرخان پسر میرزاعلی قائم مقام در ۲۶ صفر ۱۳۲۹
---
# شمارهٔ ۱۹۵ - ماده تاریخ میرزا علی اکبرخان پسر میرزاعلی قائم مقام در ۲۶ صفر ۱۳۲۹

<div class="b" id="bn1"><div class="m1"><p>ویرانه کرد چرخ بستان و کاخ ما</p></div>
<div class="m2"><p>شد تنگنا غم قصر فراخ ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن روح تابناک بر ذروه سپهر</p></div>
<div class="m2"><p>شد در صف ملک از دیولاخ ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پرسیدم از خرد تاریخ فوت وی</p></div>
<div class="m2"><p>گفتا «به ناگهان پژمرده شاخ ما»</p></div></div>