---
title: >-
    شمارهٔ ۳۱
---
# شمارهٔ ۳۱

<div class="b" id="bn1"><div class="m1"><p>بیگانه چو شد رئیس قومی</p></div>
<div class="m2"><p>نه جای تعجب است و حیرت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کان قوم ذلیل را رگ و پوست</p></div>
<div class="m2"><p>خالی ز تعصب است و غیرت</p></div></div>