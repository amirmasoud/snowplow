---
title: >-
    شمارهٔ ۲۰۶
---
# شمارهٔ ۲۰۶

<div class="b" id="bn1"><div class="m1"><p>بنده ام بنده ولی بیخردم</p></div>
<div class="m2"><p>خواجه با بیخردی می خردم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خواجه ام دید و پسندید و خرید</p></div>
<div class="m2"><p>واگهی داشت ز هر نیک و بدم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به سلیمان برسانید که من</p></div>
<div class="m2"><p>چون نگین در کف هر دیو و ددم</p></div></div>