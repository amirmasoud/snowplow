---
title: >-
    شمارهٔ ۳۰
---
# شمارهٔ ۳۰

<div class="b" id="bn1"><div class="m1"><p>در فتح ری نمود سپهدار نامدار</p></div>
<div class="m2"><p>کاری که خارج از هنر و زور رستمست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تاریخ این فتوح ز الهام کردگار</p></div>
<div class="m2"><p>جد و جهاد و جهد سپهدار اعظمست</p></div></div>