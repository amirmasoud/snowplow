---
title: >-
    شمارهٔ ۵۲
---
# شمارهٔ ۵۲

<div class="b" id="bn1"><div class="m1"><p>آن شنیدم خیمه ای از شاه روس</p></div>
<div class="m2"><p>ارمغان بر ناصرالدین شاه شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خیمه ای کز ارتفاع و عرض و طول</p></div>
<div class="m2"><p>اطلس گردون بر او کوتاه شد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در فضائی ساختندش استوار</p></div>
<div class="m2"><p>میخ بر ماهی ستون بر ماه شد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خسرو صاحبقران را در نظر</p></div>
<div class="m2"><p>هم پسند افتاد و هم دلخواه شد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ناگهان مشکوه ملک آمد در آن</p></div>
<div class="m2"><p>وین سخت جاری در آن افواه شد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کز ورود این خر بی سم و دم</p></div>
<div class="m2"><p>خیمه شاهنشهی خرگاه شد</p></div></div>