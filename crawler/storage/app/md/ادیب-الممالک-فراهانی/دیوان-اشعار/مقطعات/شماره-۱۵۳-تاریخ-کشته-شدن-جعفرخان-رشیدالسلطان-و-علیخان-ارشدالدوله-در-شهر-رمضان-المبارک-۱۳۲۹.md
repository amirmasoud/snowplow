---
title: >-
    شمارهٔ ۱۵۳ - تاریخ کشته شدن جعفرخان رشیدالسلطان و علیخان ارشدالدوله در شهر رمضان المبارک ۱۳۲۹
---
# شمارهٔ ۱۵۳ - تاریخ کشته شدن جعفرخان رشیدالسلطان و علیخان ارشدالدوله در شهر رمضان المبارک ۱۳۲۹

<div class="b" id="bn1"><div class="m1"><p>رشید و ارشد به جنگ ملت</p></div>
<div class="m2"><p>زاسب هستی شده پیاده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رشیدسلطان نخست از جهل</p></div>
<div class="m2"><p>در بلا را به رخ گشاده</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>معز سلطان به یاری بخت</p></div>
<div class="m2"><p>سزای او را به تیر داده</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ولیک ارشد بسان روباه</p></div>
<div class="m2"><p>به چنگ ضیغم در اوفتاده</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو تشنه بودند ز ساغر مرگ</p></div>
<div class="m2"><p>به کامشان ریخت زمانه باده</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>برای تاریخ سرود امیری</p></div>
<div class="m2"><p>رشید و ارشد دو مرد ماده</p></div></div>