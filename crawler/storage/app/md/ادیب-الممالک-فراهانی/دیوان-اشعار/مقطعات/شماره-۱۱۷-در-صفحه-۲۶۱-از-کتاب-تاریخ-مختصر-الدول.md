---
title: >-
    شمارهٔ ۱۱۷ - در صفحه ۲۶۱ از کتاب تاریخ مختصر الدول
---
# شمارهٔ ۱۱۷ - در صفحه ۲۶۱ از کتاب تاریخ مختصر الدول

<div class="b" id="bn1"><div class="m1"><p>کهن موبد پارسی دوش خواند</p></div>
<div class="m2"><p>ز تاریخ تازی مر این تازه حرف</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که چون معتمد بست رخت رحیل</p></div>
<div class="m2"><p>ز ملک جهان معتضد بست طرف</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خمارویه ترک را در سرای</p></div>
<div class="m2"><p>یکی دختری بود مخمور طرف</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پریچهره «قطرالندی » نام داشت</p></div>
<div class="m2"><p>به لب شکر افشان به بالا شگرف</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بدیدار روشن مهی تابناک</p></div>
<div class="m2"><p>به فرهنگ و دانش همی پهن و ژرف</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به کابین همی خواستش معتضد</p></div>
<div class="m2"><p>دل و جان بدیدار او کرد صرف</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دواج خلافت ازو یافت زیب</p></div>
<div class="m2"><p>چو صهبای روشن به سیمینه ظرف</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عقیقش بران تشنه برفاب داد</p></div>
<div class="m2"><p>بسالیکه تاریخ آن گشت «برف »</p></div></div>