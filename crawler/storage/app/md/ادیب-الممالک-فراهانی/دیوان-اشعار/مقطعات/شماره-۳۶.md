---
title: >-
    شمارهٔ ۳۶
---
# شمارهٔ ۳۶

<div class="b" id="bn1"><div class="m1"><p>استاد فاضلان سخنور ذکاء ملک</p></div>
<div class="m2"><p>آن منشی جریده غرای تربیت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دانشوری که فضلش در گوش آسمان</p></div>
<div class="m2"><p>آوازه در فکنده ز آوای تربیت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آن قائد سپاه معارف که از هنر</p></div>
<div class="m2"><p>آراست صد کتیبه به صحرای تربیت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کلکش مشاطه وار ز رسم ادب نهاد</p></div>
<div class="m2"><p>خالی به صفحه رخ زیبای تربیت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پیرایه یافت گردن دوشیزه ادب</p></div>
<div class="m2"><p>از فیض بحر طبع گهر زای تربیت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اینک سزد که بنده به پاداش این کرم</p></div>
<div class="m2"><p>از روی شوق بوسه زند پای تربیت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خواهم ز کردگار که تا روز رستخیز</p></div>
<div class="m2"><p>منت نهد بخلق ز ابقای تربیت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>روشن کند خدای تعالی روان ملک</p></div>
<div class="m2"><p>از آفتاب چهر دلارای تربیت</p></div></div>