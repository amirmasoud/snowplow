---
title: >-
    شمارهٔ ۱۷۰ - در نکوهش شاعری که یک خان بختیاری را مدح گفته بود
---
# شمارهٔ ۱۷۰ - در نکوهش شاعری که یک خان بختیاری را مدح گفته بود

<div class="b" id="bn1"><div class="m1"><p>ای ستاده به بزم تحقیقت</p></div>
<div class="m2"><p>پور سینا و پیر فارابی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بنده خامه و ضمیر تو شد</p></div>
<div class="m2"><p>قلم و رای صاحب و صابی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از شمیران ترا به ری آورد</p></div>
<div class="m2"><p>گردش آسمان دولابی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا بر این بنده ارمغان آری</p></div>
<div class="m2"><p>از ره لطف صحنی از آبی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون زنخدان شاهدان و برنگ</p></div>
<div class="m2"><p>چون رخ زاهدان محرابی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زرد چون روی عاشقی محجور</p></div>
<div class="m2"><p>از رخ ورد و لعل عنابی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پرتو افکند بر دریچه من</p></div>
<div class="m2"><p>آفتاب سخن ز مهتابی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خواندم از گفته ات دو بیت که بود</p></div>
<div class="m2"><p>رشک شعر جریر و عتابی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زنده کردی در آن بیان شگرف</p></div>
<div class="m2"><p>استخوان ادیب خندابی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زر دانش به بوته سخنت</p></div>
<div class="m2"><p>پاک شد همچو سیم تیزابی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بز دشتی و گاو کوهی را</p></div>
<div class="m2"><p>گذرانیدی از سگ آبی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ای برادر بر این لطیفه نغز</p></div>
<div class="m2"><p>باش بیدار اگر نه در خوابی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>شعر تازی به لر مخوان و مپوش</p></div>
<div class="m2"><p>خرقه خز به کرد سنجابی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>پیش لر هست شعر تازی چون</p></div>
<div class="m2"><p>پیش نازی نگار صقلابی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>یا چو فرقان به گوش مؤبد پارس</p></div>
<div class="m2"><p>یا اوستا به سمع اعرابی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>منتهی مدح گرگ آن باشد</p></div>
<div class="m2"><p>که ستائی توأش به قصابی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ور به چوپانیش کنی تصدیق</p></div>
<div class="m2"><p>زشت باشد چو نیک دریابی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>تا دهد در مذاق گرسنگان</p></div>
<div class="m2"><p>طعم جان شیردان و سیرابی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>تا به دیوان خراج ملک رسد</p></div>
<div class="m2"><p>بیشتر از منال اربابی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>باش در حوض های بلور</p></div>
<div class="m2"><p>روز و شب در شنا چو مرغابی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>مطرب عشق خواندت در گوش</p></div>
<div class="m2"><p>نغمه بوسلیک و رهابی</p></div></div>