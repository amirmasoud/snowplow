---
title: >-
    شمارهٔ ۱۱۱ - خطاب به آقای بینش
---
# شمارهٔ ۱۱۱ - خطاب به آقای بینش

<div class="b" id="bn1"><div class="m1"><p>ذره بینی که ماند از این ذره</p></div>
<div class="m2"><p>روز پیشین به خانه بینش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سیداصغر به بنده باز آورد</p></div>
<div class="m2"><p>گشت پیدا نشانه بینش</p></div></div>