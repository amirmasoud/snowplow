---
title: >-
    شمارهٔ ۲ - در نکوهش خطیبان بی زبان آغاز مشروطیت فرماید:«بوالعنبس »
---
# شمارهٔ ۲ - در نکوهش خطیبان بی زبان آغاز مشروطیت فرماید:«بوالعنبس »

<div class="b" id="bn1"><div class="m1"><p>بود «بوالعنبس » خطیب فحل و شیخ نامور</p></div>
<div class="m2"><p>بر خلایق پیشوا بر مسلمین فرمان روا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روزی اندر مسجد طائف به استدعای خلق</p></div>
<div class="m2"><p>بر فراز منبر تحقیق حکمت کرد جا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نطق ناکرده کمیت فکرتش همچون شتر</p></div>
<div class="m2"><p>خفت آنسان کش و گفتی در شکم شد دست و پا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آری آری آدمی را فکر دریائی است ژرف</p></div>
<div class="m2"><p>کاندرو ماند نهنگ از سیر و ماهی از شنا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون زبان در کام مردم بسته شد نتوان گشود</p></div>
<div class="m2"><p>نه ز افسون و نه از اندیشه و نز کیمیا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ماند «بوالعنبس » به منبر خشک لب خامش زبان</p></div>
<div class="m2"><p>چون بت اندر بتکده یا در زمین مردم گیا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>لختی اندر ریش دست آورد و لختی بر سبال</p></div>
<div class="m2"><p>لمحه ای شد ناظر دیوار و سقف و بوریا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گه تنحنح کرد و گاهی سرفه گاهی دست برد</p></div>
<div class="m2"><p>بر سجاف جبه چاک پیرهن بند قبا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>وز پس دیری تفکر روی با اصحاب کرد</p></div>
<div class="m2"><p>کاندر آنجا گرد بودند از غریب و آشنا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دید جمعی ناظرستند و گروهی منتظر</p></div>
<div class="m2"><p>هوششان در راه منطق گوش در راه صدا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گفت دانستید؟ ای یاران مرادم از سخن</p></div>
<div class="m2"><p>جمله گفتند آری ای دانش پژوه پارسا</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گفت چون دانسته اید آن راکه مقصود من است</p></div>
<div class="m2"><p>پیش دانشمند نبود عرض دانش جز خطا</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>پس فرود آمد ز منبر معتزل شد چند روز</p></div>
<div class="m2"><p>هفته دیگر به مسجد زد حریفان را صلا</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>باز در منبر سمند فکرتش چون خر به گل</p></div>
<div class="m2"><p>شد فرو چونانکه گفتی برنخیزد با عصا</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>یافت جان را در بحار حیرت اندر مهلکه</p></div>
<div class="m2"><p>دید تن را از فشار فکرت اندر تنگنا</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گفت میدانید؟ مقصودم چه باشد از بیان</p></div>
<div class="m2"><p>جملکی گفتند نی ای عامل حسن القضا</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>گفت چون اقرار بر نادانی خود می کنید</p></div>
<div class="m2"><p>گفتگو با جاهلان از چون منی نبود روا</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>باز از منبر فرود آمد به خلوتگه شتافت</p></div>
<div class="m2"><p>وز پس یک هفته در منبر شد از خلوت سرا</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بار دیگر فکرتش مانند آهو رم گرفت</p></div>
<div class="m2"><p>ریش خود بر باد داد از فکر و مالیخولیا</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>تا خر اندیشه را از گل برون آرد به جهد</p></div>
<div class="m2"><p>برد دست اندر محاسن سود ناخن بر قفا</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>پس به یاران گفت ای اصحاب من دانسته اید</p></div>
<div class="m2"><p>یا نمیدانید هان پاسخ دهیدم بر ملا؟</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>فرقه ای گفتند آری فرقه ای گفتند نی</p></div>
<div class="m2"><p>گفت اینک مشکل آسان گشت نعم المدعا</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>عالمان بر جاهلان گویند راز اندر علن</p></div>
<div class="m2"><p>جاهلان از عالمان جویند رمز اندر خفا</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چون رسد دانا بنادان گویدش «انظرالی »</p></div>
<div class="m2"><p>چون رسد عامی به عارف گویدش «حدث لنا»</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ما همه بوالعنبسیم ای خواجگان هنگام نطق</p></div>
<div class="m2"><p>راز در دل، لب خمش،دل گرسنه، جان ناشتا</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>از اشارت بی عبارت فهم باید کرد راز</p></div>
<div class="m2"><p>«این بدان در» گفتمت رو فهم کن «هذابذا»</p></div></div>