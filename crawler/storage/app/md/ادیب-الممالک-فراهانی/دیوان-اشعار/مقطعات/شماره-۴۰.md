---
title: >-
    شمارهٔ ۴۰
---
# شمارهٔ ۴۰

<div class="b" id="bn1"><div class="m1"><p>همه صاف طینت همه پاکدامن</p></div>
<div class="m2"><p>همه با شهامت همه با فتوت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همه شیر خورده ز پستان دانش</p></div>
<div class="m2"><p>همه بسته با علم عقد اخوت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همه زاده از خاندان رسالت</p></div>
<div class="m2"><p>همه رسته از بوستان نبوت</p></div></div>