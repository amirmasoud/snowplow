---
title: >-
    شمارهٔ ۱۷۴
---
# شمارهٔ ۱۷۴

<div class="b" id="bn1"><div class="m1"><p>به سردار اسعد بگو ای که از دم</p></div>
<div class="m2"><p>هزار آتش فتنه خاموش کردی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو آنی که اصلاح کار جهان را</p></div>
<div class="m2"><p>به دامان فضل خطاپوش کردی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تو آنی که افراسیاب ستم را</p></div>
<div class="m2"><p>معاقب به خون سیاوش کردی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تو آنی که هر گربه دزد خائن</p></div>
<div class="m2"><p>گریزان به سوراخ چون موش کردی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تو آنی که از جنبش عزم و رایت</p></div>
<div class="m2"><p>زمین و زمان را پر از جوش کردی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بزرگان و گندآوران جهان را</p></div>
<div class="m2"><p>ز اعجاز خود مات و مدهوش کردی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جهان را ز تیغت همه نیش ماری</p></div>
<div class="m2"><p>ولی از بیانت پر از نوش کردی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سخن های ستوار گوئی ازیرا</p></div>
<div class="m2"><p>که گفتار سنجیده در گوش کردی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز نیرنگ فرهنگ خود روبهان را</p></div>
<div class="m2"><p>همه خفته که خواب خرگوشی کردی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گرفتی به شمشیر و تدبیر گیتی</p></div>
<div class="m2"><p>عروس شبت را در آغوش کردی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چو در یاد داری همه کار گیتی</p></div>
<div class="m2"><p>چرا بنده ات را فراموش کردی</p></div></div>