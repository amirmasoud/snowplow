---
title: >-
    شمارهٔ ۱۱۹
---
# شمارهٔ ۱۱۹

<div class="b" id="bn1"><div class="m1"><p>پیش آن صاحب فرخنده بنالم به از آنک</p></div>
<div class="m2"><p>خاضع امر فلان بنده بهمان باشم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صدر دیوان وزارت اگرم بپذیرد</p></div>
<div class="m2"><p>در اقالیم سخن صاحب دیوان باشم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای خداوند خود انصاف بده شایسته است</p></div>
<div class="m2"><p>که من اینسان بغم دهر گروگان باشم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با چنین عزت و شأن و شرف و استغنا</p></div>
<div class="m2"><p>در پی رزق جدا از شرف و شأن باشم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چارصد تومان افزون بکفم مانده برات</p></div>
<div class="m2"><p>درم از بهر درم خسته پی نان باشم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>وام خواهم ندهد ریش و گریبان از دست</p></div>
<div class="m2"><p>زین سبب دست به سر سر به گریبان باشم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یا بدر این ورق شوم و یا وجهش را</p></div>
<div class="m2"><p>کن حوالت که دو روزی به تو مهمان باشم</p></div></div>