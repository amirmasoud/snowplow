---
title: >-
    شمارهٔ ۱۴۹ - ماده تاریخ
---
# شمارهٔ ۱۴۹ - ماده تاریخ

<div class="b" id="bn1"><div class="m1"><p>چون محمدعلی ز دار فنا</p></div>
<div class="m2"><p>کرد در بارگاه هستی رو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آمد از مشرق وجود فراز</p></div>
<div class="m2"><p>رفت در مغرب هبوط فرو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>قطره پیوسته شد به بحر وجود</p></div>
<div class="m2"><p>شد تهی ساغر و شکست سبو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در پی جوی کوثر از گیتی</p></div>
<div class="m2"><p>دامن اندر کشید و جست از جو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رفت در ظل رحمت حق از انک</p></div>
<div class="m2"><p>بود هم حق پرست و هم حق گو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سخنش استوار و طبع بلند</p></div>
<div class="m2"><p>فطرتش پاک و خصلتش نیکو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فکرتش نقشها کشیده بر آب</p></div>
<div class="m2"><p>همتش بر فلک زده پهلو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آنکه کلکش فشاند نافه مشک</p></div>
<div class="m2"><p>چون سر زلف یار در مشکو</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تنگ شد خاکدان بر او زین راه</p></div>
<div class="m2"><p>چرخ میناش برد در مینو</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>حور عینش ز چهره امید</p></div>
<div class="m2"><p>گرد انده فشاند با گیسو</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آری از دام مرگ نتوان جست</p></div>
<div class="m2"><p>نه به اندیشه و نه با نیرو</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>پنجه با ساعد اجل نتوان</p></div>
<div class="m2"><p>که حریفی است آهنین بازو</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>هر که زین سو گلیم خود گسترد</p></div>
<div class="m2"><p>بی سخن رخت بر کشد زان سو</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>الغرض چون از این سرای سپج</p></div>
<div class="m2"><p>رفت اندر پناه رحمت هو</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بود از هجرت رسول خدای</p></div>
<div class="m2"><p>سال بر الف و سیصد و سی و دو</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بیست و شش رفته از مه شوال</p></div>
<div class="m2"><p>که ز گیتی شتافت در مینو</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بهر تاریخ آن امیری گفت</p></div>
<div class="m2"><p>«فی ریاض الجنان آمنه »</p></div></div>