---
title: >-
    شمارهٔ ۶۲
---
# شمارهٔ ۶۲

<div class="b" id="bn1"><div class="m1"><p>چو از جهان به جنان شد علی اکبر خان</p></div>
<div class="m2"><p>ز ماتمش همه افکار و دل دو نیم شدند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فزود امیری دنبال غم به مصرع و گفت</p></div>
<div class="m2"><p>هزار حیف که یک دودمان یتیم شدند</p></div></div>