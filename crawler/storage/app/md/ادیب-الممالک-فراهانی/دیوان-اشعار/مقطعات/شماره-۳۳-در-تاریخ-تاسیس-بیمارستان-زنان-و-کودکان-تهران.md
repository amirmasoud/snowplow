---
title: >-
    شمارهٔ ۳۳ - در تاریخ تاسیس بیمارستان زنان و کودکان تهران
---
# شمارهٔ ۳۳ - در تاریخ تاسیس بیمارستان زنان و کودکان تهران

<div class="b" id="bn1"><div class="m1"><p>در عهد شه زمانه احمد</p></div>
<div class="m2"><p>شاهی که به عدل داستان است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عهدی که به مسند معارف</p></div>
<div class="m2"><p>ممتاز الملک را مکان است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با واسطه امیر اعلم</p></div>
<div class="m2"><p>کز کردارش هنر عیان است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پرداخته شد چنین بنائی</p></div>
<div class="m2"><p>کاسایش ملتی در آن است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اطفال و زنان ملک ما را</p></div>
<div class="m2"><p>زین کاخ شفا به رایگان است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تاریخ اساس و نام نامیش</p></div>
<div class="m2"><p>«بیمارستان بانوان »است</p></div></div>