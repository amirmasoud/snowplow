---
title: >-
    شمارهٔ ۱۳۶
---
# شمارهٔ ۱۳۶

<div class="b" id="bn1"><div class="m1"><p>خدایگانا از گرد راه موکب تو</p></div>
<div class="m2"><p>خدا گواست که شد چشم بندگان روشن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ورود مقدم میمونت اندر این سامان</p></div>
<div class="m2"><p>بود چو مقدم اردی بهشت در گلشن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تو زنده سازی در ملک داد و دانش را</p></div>
<div class="m2"><p>چنانکه باد بهاری به باغ سرو سمن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ولی چه سود که این بنده با هزار دریغ</p></div>
<div class="m2"><p>رهین بستر در دم درون بیت حزن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شکسته بازویم از سنگ منجنیق قضا</p></div>
<div class="m2"><p>فتاده حیران چون مورلنگ در به لگن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زمانه پنجه و بازوی حقگذار مرا</p></div>
<div class="m2"><p>شکست تا نتواند گرفتن آن دامن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به خیره ز آن که ترا دامنی است پهن و دراز</p></div>
<div class="m2"><p>فراخ بر قد و بالای این سپهر کهن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به فضل خویش کنی هر شکسته را جبران</p></div>
<div class="m2"><p>ز لطف خویش کنی هر رمیده را ایمن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>علی الخصوص رهی را که از نخستین روز</p></div>
<div class="m2"><p>در آستانه رفیع تو بوده است وطن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بگیر دستم و جبران کن این شکستگیم</p></div>
<div class="m2"><p>که دست چرخ نیارد به بسته تو شکن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بنان سحر بیان مرا که در مدحت</p></div>
<div class="m2"><p>ز خامه عنبر سارا فشاند و مشک ختن</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به جرم آنکه ز دامان خواجه گشت جدا</p></div>
<div class="m2"><p>بهم شکست و شد اکنون و بال در گردن</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ولی چه باک که گر دستم اوفتاده ز کار</p></div>
<div class="m2"><p>نمانده است زبانم به مدحتت ز سخن</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گرم چو شاخ صنوبر شکسته دست امید</p></div>
<div class="m2"><p>به صد زبانت سرآیم مدیحه چون سوسن</p></div></div>