---
title: >-
    شمارهٔ ۵۸
---
# شمارهٔ ۵۸

<div class="b" id="bn1"><div class="m1"><p>به درگاه دانش که باشد که از من</p></div>
<div class="m2"><p>سلامی رساند پیامی گذارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بگوید که منت برد از تو هر کس</p></div>
<div class="m2"><p>برانی پزد یا خورد یا که خوارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به مینو درون زی جهان جان بوران</p></div>
<div class="m2"><p>دعای تو گوید سپاس تو دارد</p></div></div>