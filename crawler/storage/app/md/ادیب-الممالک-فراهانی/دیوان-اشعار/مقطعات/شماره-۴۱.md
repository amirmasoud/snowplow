---
title: >-
    شمارهٔ ۴۱
---
# شمارهٔ ۴۱

<div class="b" id="bn1"><div class="m1"><p>شیخ عبدالغفور تبریزی</p></div>
<div class="m2"><p>نه مسلمان نه قوم زردشت است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هستش انگشتری بسوی قفا</p></div>
<div class="m2"><p>که در آن حلقه هر دم انگشت است</p></div></div>