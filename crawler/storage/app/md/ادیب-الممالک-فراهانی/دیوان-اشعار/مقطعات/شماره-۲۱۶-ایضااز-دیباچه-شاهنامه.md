---
title: >-
    شمارهٔ ۲۱۶ - ایضااز دیباچه شاهنامه
---
# شمارهٔ ۲۱۶ - ایضااز دیباچه شاهنامه

<div class="b" id="bn1"><div class="m1"><p>گر ژنده گشت و کهن رختم چه باک که من</p></div>
<div class="m2"><p>بافنده هنرم جولاهه سخنم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من شوخدیده نیم ز آیین رمیده نیم</p></div>
<div class="m2"><p>پاکست دل منگر این رخت شوخکنم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گردون زمین من است ابر آستین من است</p></div>
<div class="m2"><p>مه پوستین من است خورشید پیرهنم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>راه خدا نهلم کزان سرشته گلم</p></div>
<div class="m2"><p>آباد ازوست دلم آزاد ازوست تنم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر آسمان بدلم صد کوه بار کند</p></div>
<div class="m2"><p>از نوک خامه خود فرهاد کوهکنم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از روزگار سیه خار است در جگرم</p></div>
<div class="m2"><p>وز نام فرخ شه شهد است در دهنم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر بخت یار شود کار استوار شود</p></div>
<div class="m2"><p>این شهد را بمزم و آن خار را بکنم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هر جا که بارگهی است خورشید بارگهم</p></div>
<div class="m2"><p>هر جا که انجمنی است سالار انجمنم</p></div></div>