---
title: >-
    شمارهٔ ۲۳۸ - در علم کف شناسی
---
# شمارهٔ ۲۳۸ - در علم کف شناسی

<div class="b" id="bn1"><div class="m1"><p>نام تلهای کواکب که بود در کف دست</p></div>
<div class="m2"><p>گر ز ابهام شماری و به خنصر گروی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زهره و مشتری است و زحل و شمس آنگاه</p></div>
<div class="m2"><p>تیر و مریخ و قمر نیک شناس ای اخوی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خط زهره است چو از شمس روی سوی زحل</p></div>
<div class="m2"><p>خط مریخ چو از زهره به برجیس روی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خط قوس از متوازی بخط مریخ است</p></div>
<div class="m2"><p>نام آن خط حیات است و بآن شاد شوی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از تل شمس عمودی که بپایان آمد</p></div>
<div class="m2"><p>خط شمس است و شود فال تو زان نیک قوی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زیر تیر است بشکل افقی خط قران</p></div>
<div class="m2"><p>قوس آن سوی قمر خط بداهت شنوی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از زحل خط عمودی سوی کف خط نصیب</p></div>
<div class="m2"><p>هست تقدیر تو آسوده زبی یا بدوی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خط مایل زقرآن سوی کف از صحت دان</p></div>
<div class="m2"><p>کهکشان از سوی مریخ بر آید بسوی</p></div></div>