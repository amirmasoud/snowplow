---
title: >-
    شمارهٔ ۲۵۰
---
# شمارهٔ ۲۵۰

<div class="b" id="bn1"><div class="m1"><p>خدایگانا تا دیده ام در این کشور</p></div>
<div class="m2"><p>یکی چنانکه توئی راد و نیکخواه نبود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>درین زمانه بمقصد کسی نبردی راه</p></div>
<div class="m2"><p>اگر عنایت و فضلت رفیق راه نبود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>درین تزلزل اگر همتت نهشتی گام</p></div>
<div class="m2"><p>بهیچ تن سر و در هیچ سر کلاه نبود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نشان صدق و صفا از زمانه محو شدی</p></div>
<div class="m2"><p>اگر مساعدت پیر خانقاه نبود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ور آب لطف تو خامش نکردی این آتش</p></div>
<div class="m2"><p>درین بساط بهم خورده غیر آه نبود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یکی نگر که بلا زآسمان فرو بارید</p></div>
<div class="m2"><p>که جز در تو از آسیب آن پناه نبود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سه ماه رفت به گیتی چو صد هزار آنسال</p></div>
<div class="m2"><p>درین سه ماه فروغی به مهر و ماه نبود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گیاه را سر جنبیدن از نسیم نماند</p></div>
<div class="m2"><p>نسیم را دل بوئیدن گیاه نبود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مقام یونس دل شد درون کام نهنگ</p></div>
<div class="m2"><p>قرار یوسف جان جز بقعر چاه نبود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خیال خلق همه سوی حفظ خویش بدی</p></div>
<div class="m2"><p>ولی خیال تو جز پیش پادشاه نبود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شگفتم آید از آن دل که بدبشه مشغول</p></div>
<div class="m2"><p>چنانکه بیخبر از کشور و سپاه نبود</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تو در سحرگه بیدار بوده ای همه شب</p></div>
<div class="m2"><p>ستاره گاهی بیدار بود و گاه نبود</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گمان و شبهه و تردید از تو دورستند</p></div>
<div class="m2"><p>که فکرت تو سزاوار اشتباه نبود</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بآستان تو دور از ریا سخن گویم</p></div>
<div class="m2"><p>که در شریعت من غیر ازین گناه نبود</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>من آنکسم که دلم با وجود مرحمتت</p></div>
<div class="m2"><p>به هیچگونه طلبکار مال و جاه نبود</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>نگاه دار امیدم بفضل و رحمت خویش</p></div>
<div class="m2"><p>که آرزوی دلم جز یکی نگاه نبود</p></div></div>