---
title: >-
    شمارهٔ ۵
---
# شمارهٔ ۵

<div class="b" id="bn1"><div class="m1"><p>دولت جاوید خواهم از در یزدان</p></div>
<div class="m2"><p>«دانش » دانش پژوه صلح طلب را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آنکه نمود است وصف ذات جمیلش</p></div>
<div class="m2"><p>غیرت ارژنگ کارگاه ادب را</p></div></div>