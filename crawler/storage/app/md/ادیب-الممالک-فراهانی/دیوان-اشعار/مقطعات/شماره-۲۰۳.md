---
title: >-
    شمارهٔ ۲۰۳
---
# شمارهٔ ۲۰۳

<div class="b" id="bn1"><div class="m1"><p>ای درخت سبز اگر روزی بدست باغبان</p></div>
<div class="m2"><p>اره بیداد بینی سوی دست اره بین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گرنه دستش از تو بودی کی بر آوردی بعمد</p></div>
<div class="m2"><p>از برای قطع پایت دست جور از آستین</p></div></div>