---
title: >-
    شمارهٔ ۹
---
# شمارهٔ ۹

<div class="b" id="bn1"><div class="m1"><p>کاشکی بودی مرا طبعی چو قلزم در خروش</p></div>
<div class="m2"><p>کاشکی بودی مرا فکری چو مینو با صفا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خامه ای از ارض طولش تا محیط آسمان</p></div>
<div class="m2"><p>نامه ای از قطب عرضش تا به خط استوا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا ستودم ذات پاکت را همی در خورد قدر</p></div>
<div class="m2"><p>تا سرودم مدحتت آن سان که بایستی روا</p></div></div>