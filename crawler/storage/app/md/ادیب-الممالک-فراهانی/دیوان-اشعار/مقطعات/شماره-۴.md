---
title: >-
    شمارهٔ ۴
---
# شمارهٔ ۴

<div class="b" id="bn1"><div class="m1"><p>اف بر این دیوان سرا، لعنت بر این دیوان که برد</p></div>
<div class="m2"><p>ظلمشان در ظلمت از مه، نور و از شارق ضیا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مردمی بیرون ز راه مردمی دور از خرد</p></div>
<div class="m2"><p>فرد و طاق از دین پرستی جفت نیرنگ و ریا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>راستی گویم سعاتمند و خوش بخت آنکس است</p></div>
<div class="m2"><p>کاندرین گیتی، ببیند چهره این اشقیا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر که رخشان دید گوید تا ابد یالیتنی</p></div>
<div class="m2"><p>مت قبل الیوم حتی صرت نسیا منسیا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همچو انگورش بزیر پای بفشارند رگ</p></div>
<div class="m2"><p>نرم سایند استخوانش با لگد چون توتیا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مدعی به هرچه قاضی بگوش مدعی</p></div>
<div class="m2"><p>گویدی ربع لنا نصف لنا کل لیا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در جزا مردی رئیس آمد که نشناسد ز جهل</p></div>
<div class="m2"><p>تاک از تریاک و سیب از سنبه گیپا از گیا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عارض و معروض از او بینند در کار آنچه دید</p></div>
<div class="m2"><p>معده مرد سقیم از خوردن سقمونیا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>پیکرش را گوئیا ایزد تعالی آفرید</p></div>
<div class="m2"><p>ز آهک و زرنیخ و گوگرد و کنین و کاسیا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مولدش تبریز و اصلش از صفاهان است لیک</p></div>
<div class="m2"><p>فرق نهاده است اسپاهانی از اسپانیا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آن یکی گفتش که آلمانی مسلمانی گرفت</p></div>
<div class="m2"><p>گفت باشد امپراطورش ز نسل قانیا</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>قانیا اندر محرم داشت بر پا تعزیه</p></div>
<div class="m2"><p>این نبیره در عمل دارد تأسی بر نیا</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>دیگری گفتش که شاهان اروپا از چه رو</p></div>
<div class="m2"><p>هر طرف تازند بهر حمله اندر آسیا</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گفت شاهان نان جو خواهند از بهر ثواب</p></div>
<div class="m2"><p>«آرپا» در ترکی شعیر است و «دگرمان » آسیا</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>دیدم آنجا خسته را بسته اند اندر کمند</p></div>
<div class="m2"><p>گفتم این مسکین که باشد چیست جرمش ای کیا</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گفت در راپورت کمیساریا بنوشته اند</p></div>
<div class="m2"><p>کاین جوان گفته است مستم ساغری ده ساقیا</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>گفتمش ای کاش بودی ابن جوزی در حیات</p></div>
<div class="m2"><p>تا که نامت ثبت کردی در کتاب الاذکیا</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>شاعری با ذوق شعری گفت و او را وی شدش</p></div>
<div class="m2"><p>شعر خواندن در کجا ممنوع کردند اولیا؟</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>گیرم او کرده است تقصیری خلاف عقل و دین</p></div>
<div class="m2"><p>جور ار حدی است بر بیچارگان از اقویا</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>سی و یک روز از چه میزان سی و یک تومان ز چیست</p></div>
<div class="m2"><p>از فرانسه آمدست این حکم یا از روسیا؟</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>حد عرفی کس ندید از حد شرعی سخت تر</p></div>
<div class="m2"><p>این چه حکم است ای سرا پا بدعت و شرک و ریا</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>آنقدر بستان که تانی دادنش تاوان و جرم</p></div>
<div class="m2"><p>آنچنان بشکن که یاری بستنش با مومیا</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>دور عقل از تو چو مرد پارسا از پارگین</p></div>
<div class="m2"><p>هم تو دور از دین چو پیر برهمن از پاریا</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>از جزای حق نیندیشی مگر نشینده ای</p></div>
<div class="m2"><p>داستان حضرت داود و قتل اوریا؟</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>گفت این حکم آمد از شورای عالی پیش از این</p></div>
<div class="m2"><p>من نمیدانم بخوان راپورت کمیساریا</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>گفتمش شورای عالی چیست؟ و اعضایش که؟ جز</p></div>
<div class="m2"><p>محفلی بی دعوت اندر وی گروهی ز ادعیا</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>وضع قانون با وکیلان است و اجرا با ملوک</p></div>
<div class="m2"><p>حکم عرف است از حکیمان حکم شرع از انبیا</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>کیستند این خرسران در مرغزار معدلت!</p></div>
<div class="m2"><p>چیستند این خربطان در آبشار اتقیا!</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>نایب فرعون و هامان را کجا شاید شناخت</p></div>
<div class="m2"><p>چون سلیمان یا وزیرش آصف بن برخیا</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>نایب فرعون و هامان را کجا شاید شناخت</p></div>
<div class="m2"><p>چون سلیمان یا وزیرش آصف بن برخیا</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>زین شش اندازان چه بینی غیر تاراج و شتل</p></div>
<div class="m2"><p>از زکام ایدر چه زاید غیر مالیخولیا</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>من بخواندم نامه پیغمبران راستین</p></div>
<div class="m2"><p>آدم و نوح و خلیل الله کلیم و زکریا</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>یوشع بن نون و یونس پور متی دانیال</p></div>
<div class="m2"><p>صالح و هود و مسیحا و عزیز و ایلیا</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>نامه اسحق و اسمعیل و حزقیل و شعیب</p></div>
<div class="m2"><p>صحف داود و سلیمان نحمیا و برمیا</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>نامه ساسان و زرتشت و جی افرام مهین</p></div>
<div class="m2"><p>نامه پولس به سوی مردم ایتالیا</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>هم کتاب خاتم پیغمبران خواندم که هست</p></div>
<div class="m2"><p>واپس اندر عهد و پیش اندر حریم کبریا</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>این چنین حکمی ندیدم در کتاب هیچیک</p></div>
<div class="m2"><p>ز انبیا و اتقیا و اولیا و اصفیا</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>گفت خامش باش کاینان هر یکی در صفه ای</p></div>
<div class="m2"><p>پیشوایانند چون در چال ورزش پوریا</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>مجلس ملی نیارد حکمشان را نسخ کرد</p></div>
<div class="m2"><p>چون کلام انبیا اندر مقام اوصیا</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>گفتم آری پیشوایانند این شش تازنان</p></div>
<div class="m2"><p>بهر داو نرد در چال قمار و منگیا</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>پیشوایان تواند این قوم جبار عنید</p></div>
<div class="m2"><p>کل جبار عنید فی جهنم القیا</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>هر یکی چون قاشق ناشسته در آشند لیک</p></div>
<div class="m2"><p>آش ها را گه نخود باشند و گاهی لوبیا</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>همچو غولانند در بیغولها مردم شکار</p></div>
<div class="m2"><p>با سیاهانند آدم خواره در افریقیا</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>گونیا را جمله تصحیفند زیرا هر شبی</p></div>
<div class="m2"><p>در بر چندین عمود آورده شکل گونیا</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>بسکه الرحمن و یاسین در مساجد خوانده اند</p></div>
<div class="m2"><p>نقش حامیم است بر پهلویشان از بوریا</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>مادرانشان را حیا اندر محیا هیچ نیست</p></div>
<div class="m2"><p>لیک بفروشند بهر زرق در احیا حیا</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>چرم بلغارند و کفش صوفیان گر چه ز ناز</p></div>
<div class="m2"><p>پیش ما خاتون بلغارند و ترک صوفیا</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>شرمی از باری تعالی کن از این دزدان مترس</p></div>
<div class="m2"><p>بی ریا گویم که بی دینند یکسر باریا</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>ای که ناموس شریعت را دری با حربیان</p></div>
<div class="m2"><p>ای که اموال فقیران را خوری با اغنیا</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>گر کنی عمامه را مانند تاج داریوش</p></div>
<div class="m2"><p>ور بیاری از طراز خامه مشک داریا</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>گر نمائی از در نیرنگ صد رنگ و فسون</p></div>
<div class="m2"><p>ور پدید آری به جادو صد هزاران کیمیا</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>گر ز ستواری مکانت چون بیوت عادیان</p></div>
<div class="m2"><p>ور ز بالائی مقامت همچو حصن عادیا</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>در دل مامت فرستم باز با این ریش و پشم</p></div>
<div class="m2"><p>تا به زهدانش بپوشی طیلسان از سابیا</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>فرق نگذارم میان زشت و زیبا شیخ و شاب</p></div>
<div class="m2"><p>زانکه ننهادند فرق از مجرمین با ابریا</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>ای رئیس این چامه من چون کباب برمیا است</p></div>
<div class="m2"><p>در زمان شه «یهو یا قیم پور یوشیا»</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>آنکه آیین سلامت جست در دارالسلام</p></div>
<div class="m2"><p>شاه اسرائیل شد از صدق بعد از صدقیا</p></div></div>