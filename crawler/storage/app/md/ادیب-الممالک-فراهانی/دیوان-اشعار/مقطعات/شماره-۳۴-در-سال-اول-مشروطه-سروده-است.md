---
title: >-
    شمارهٔ ۳۴ - در سال اول مشروطه سروده است
---
# شمارهٔ ۳۴ - در سال اول مشروطه سروده است

<div class="b" id="bn1"><div class="m1"><p>بیچاره آدمی که گرفتار عقل شد</p></div>
<div class="m2"><p>خوش آن کسی که کره خر آمد الاغ رفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای باغبان منال ز رنج دی و خزان</p></div>
<div class="m2"><p>بنشین بجای و فاتحه برخوان که باغ رفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای پاسبان مخسب که در غارت سرای</p></div>
<div class="m2"><p>دزد دغل به خانه تو با چراغ رفت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای دهخدا عراق و ری و طوس هم نماند</p></div>
<div class="m2"><p>چو بانه رفت و سقز و ساوجبلاغ رفت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یاران حذر کنید که در بوستان عدل</p></div>
<div class="m2"><p>امروز جوقه جوقه بسی بوم و زاغ رفت</p></div></div>