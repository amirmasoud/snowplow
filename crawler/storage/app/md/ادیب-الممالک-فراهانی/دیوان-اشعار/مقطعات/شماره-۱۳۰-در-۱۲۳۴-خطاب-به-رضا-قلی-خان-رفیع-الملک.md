---
title: >-
    شمارهٔ ۱۳۰ - در ۱۲۳۴ خطاب به رضا قلی خان رفیع الملک
---
# شمارهٔ ۱۳۰ - در ۱۲۳۴ خطاب به رضا قلی خان رفیع الملک

<div class="b" id="bn1"><div class="m1"><p>رضا قلیخان ای خواجه ای که از سر صدق</p></div>
<div class="m2"><p>فکنده امر تو چون بنده حلقه در گوشم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هنوز می وزدم بوی مشک و گل به مشام</p></div>
<div class="m2"><p>از آن شبی که چو جان بودی اندر آغوشم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بغیر بندگی و مهر و صدق و یک رنگی</p></div>
<div class="m2"><p>چه کرده ام که ز دل کرده ای فراموشم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرا به هیچ فروشی ولی خورم سوگند</p></div>
<div class="m2"><p>که موئی از تو به تاج ملوک نفروشم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مرا چو بربط خود دان کت آید اندر گوش</p></div>
<div class="m2"><p>ترانه از زدن زخم و مالش گوشم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اگر نه بربطم ای جان چرا ز زخم حبیب</p></div>
<div class="m2"><p>ترانه خوانم و از کس ترانه ننیوشم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اگر نه بربطم ای دل چرا به زانوی تو</p></div>
<div class="m2"><p>سخن سرایم و دور از تو بر تو خاموشم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اگر نه بربطم این تار زرد و موی سپید</p></div>
<div class="m2"><p>ز چیست ریخته بر دامن از بنا گوشم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جهانیان را رگ زیر پوست باشد و من</p></div>
<div class="m2"><p>چو بربطم که به رگ پوست را همی پوشم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چو بربطم که دلم آشنای زخم تو شد</p></div>
<div class="m2"><p>چو بربطم که چو بنوازیم تو بخروشم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تو روز و شب پی آزار من بکوش که من</p></div>
<div class="m2"><p>پی رضای تو از جان و دل همی کوشم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مخر فسانه این آسمان حیلت باز</p></div>
<div class="m2"><p>ز راه حیله میفکن به خواب خرگوشم</p></div></div>