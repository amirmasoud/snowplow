---
title: >-
    شمارهٔ ۱۰۷
---
# شمارهٔ ۱۰۷

<div class="b" id="bn1"><div class="m1"><p>از حکایت سال سیصد و نه</p></div>
<div class="m2"><p>این حدیثم کجا شود فرمش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که چو حلاج را بدار زدند</p></div>
<div class="m2"><p>نه رخش زرد شد نه چهره ترش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون برآمد فراز دار بقا</p></div>
<div class="m2"><p>گفت ای غافلان ز دانش و هش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پنبه فرسوده از کمان گردد</p></div>
<div class="m2"><p>آتش از آب و آهن از چکش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عرش من ثابت است و نقش جلی</p></div>
<div class="m2"><p>ثبت العرش گفته ثم انقش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>این نه مرگ است زندگیست که نیست</p></div>
<div class="m2"><p>میزبان کریم مهمان کش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عطسه من ز نفخ رحمن است</p></div>
<div class="m2"><p>عطسه مغز صرعی از کندش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>من کلیمم عصای من دار است</p></div>
<div class="m2"><p>اتوکؤ علی العصا و اهش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گفتش آن یک شهادتان برگوی</p></div>
<div class="m2"><p>که زمانت رسیده گفت خمش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شمع ایوان دوست چهره اوست</p></div>
<div class="m2"><p>نور خورشید بین و شمع بکش</p></div></div>