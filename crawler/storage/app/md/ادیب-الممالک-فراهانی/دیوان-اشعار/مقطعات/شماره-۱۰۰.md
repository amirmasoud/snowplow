---
title: >-
    شمارهٔ ۱۰۰
---
# شمارهٔ ۱۰۰

<div class="b" id="bn1"><div class="m1"><p>ز تیز کیان فایده است آن بروت</p></div>
<div class="m2"><p>که چون بادگیر کشد تیغ تیز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بدست بزرگان ز بس ریخت آب</p></div>
<div class="m2"><p>نباشد چنو در جهان آبریز</p></div></div>