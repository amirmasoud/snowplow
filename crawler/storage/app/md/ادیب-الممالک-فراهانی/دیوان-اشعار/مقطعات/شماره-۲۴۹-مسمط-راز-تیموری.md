---
title: >-
    شمارهٔ ۲۴۹ - مسمط راز تیموری
---
# شمارهٔ ۲۴۹ - مسمط راز تیموری

<div class="b" id="bn1"><div class="m1"><p>بشنو ای فرزند تا ازین دفتر</p></div>
<div class="m2"><p>خوانمت از بر راز تیموری</p></div></div>
<div class="b2" id="bn2"><p>یک گروهی را بینم اندر سر</p></div>
<div class="b2" id="bn3"><p>جقه عدل است تاج منصوری</p></div>
<div class="b" id="bn4"><div class="m1"><p>حرفشان باشد سکه اندر زر</p></div>
<div class="m2"><p>حکمشان جاری در همه کشور</p></div></div>
<div class="b2" id="bn5"><p>کس نگوید چون کس نپیچد سر</p></div>
<div class="b2" id="bn6"><p>چون رسد زایشان حکم و دستوری</p></div>
<div class="b" id="bn7"><div class="m1"><p>دیگران را قول ناروا جستی</p></div>
<div class="m2"><p>گرچه شاهان بر تخت و عاجستی</p></div></div>
<div class="b2" id="bn8"><p>لیک اخراج از تخت و تاجستی</p></div>
<div class="b2" id="bn9"><p>می کشد زین باد شمعشان کوری</p></div>
<div class="b" id="bn10"><div class="m1"><p>ذوالفقار آنروز می کشد افزون</p></div>
<div class="m2"><p>پیکر شکاک می کشد در خون</p></div></div>
<div class="b2" id="bn11"><p>با اجل نزدیک با فنا مقرون</p></div>
<div class="b2" id="bn12"><p>از طریق حق هرکرا دوری</p></div>
<div class="b" id="bn13"><div class="m1"><p>آید آن شیری کز ره معراج</p></div>
<div class="m2"><p>از نبی بگرفت خاتمش را باج</p></div></div>
<div class="b2" id="bn14"><p>بسته خواهد شد راه حج بر حاج</p></div>
<div class="b2" id="bn15"><p>ثبت طومار است حکم مجبوری</p></div>
<div class="b" id="bn16"><div class="m1"><p>آنکه در محشر صاحب اورنگ</p></div>
<div class="m2"><p>بار موتش کین با یموتش جنگ</p></div></div>
<div class="b2" id="bn17"><p>ذوالفقار او گیرد از خون رنگ</p></div>
<div class="b2" id="bn18"><p>سوسنش گردد چون گل سوری</p></div>
<div class="b" id="bn19"><div class="m1"><p>آتشی بینم اندرن آن هنگام</p></div>
<div class="m2"><p>از ثری تا عرش از افق تا بام</p></div></div>
<div class="b2" id="bn20"><p>خلق عالم را پیکر و اندام</p></div>
<div class="b2" id="bn21"><p>می بسوزد چون شمع کافوری</p></div>
<div class="b" id="bn22"><div class="m1"><p>یار گردد روم با فرنگی زود</p></div>
<div class="m2"><p>هندو ارمن می کند نابود</p></div></div>
<div class="b2" id="bn23"><p>هم یهودی هم داسن ارنائود</p></div>
<div class="b2" id="bn24"><p>شور چنگیزی است قتل تیموری</p></div>
<div class="b" id="bn25"><div class="m1"><p>مردمان کوه ساکنان یم</p></div>
<div class="m2"><p>مکه و تفلیس تا یموت از غم</p></div></div>
<div class="b2" id="bn26"><p>مات و خوار و زار درهم و برهم</p></div>
<div class="b2" id="bn27"><p>غرق اندوهند جفت رنجوری</p></div>
<div class="b" id="bn28"><div class="m1"><p>هرکه در دنیا واجب التعظیم</p></div>
<div class="m2"><p>بر سریر عدل می شود تسلیم</p></div></div>
<div class="b2" id="bn29"><p>بندگی سازد شاه هفت اقلیم</p></div>
<div class="b2" id="bn30"><p>ور سلیمان است می کند موری</p></div>
<div class="b" id="bn31"><div class="m1"><p>ذوالفقار از ظلم می کند بنیاد</p></div>
<div class="m2"><p>خاک شکاکان می دهد برباد</p></div></div>
<div class="b2" id="bn32"><p>تانهند از سر تا برند از یاد</p></div>
<div class="b2" id="bn33"><p>نشاه خمر و شور مخموری</p></div>
<div class="b" id="bn34"><div class="m1"><p>بندگان بیدار روزگار آزاد</p></div>
<div class="m2"><p>در مراد خویش دوستان دلشاد</p></div></div>
<div class="b2" id="bn35"><p>مملکت آباد رسته از بیداد</p></div>
<div class="b2" id="bn36"><p>چون دل مؤمن چون رخ حوری</p></div>
<div class="b" id="bn37"><div class="m1"><p>خاطر تیمور آنزمان شاد است</p></div>
<div class="m2"><p>باقی آن عصر دوره داد است</p></div></div>
<div class="b2" id="bn38"><p>از ادیب این راز روشن افتاده است</p></div>
<div class="b2" id="bn39"><p>ز آنکه مستان را نیست مستوری</p></div>