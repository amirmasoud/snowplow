---
title: >-
    شمارهٔ ۱۹۱ - نعت رسول اکرم به پارسی
---
# شمارهٔ ۱۹۱ - نعت رسول اکرم به پارسی

<div class="b" id="bn1"><div class="m1"><p>یگانه رادی کش کردگار بی همتا</p></div>
<div class="m2"><p>گزیده است به پیغمبری و وخشوری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز تنگبار خدائی به تیمسار خرد</p></div>
<div class="m2"><p>رسید نامه که از وی گرفت دستوری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بکار خانه وی آفتاب مزدوری است</p></div>
<div class="m2"><p>که شب نخسبد و آید پگه به مزدوری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز دارو و برد سپاهش سپهر برد از یاد</p></div>
<div class="m2"><p>شکوه چتر کیانی و تخت شاپوری</p></div></div>