---
title: >-
    شمارهٔ ۱۲۲
---
# شمارهٔ ۱۲۲

<div class="b" id="bn1"><div class="m1"><p>آل عباس را به «قلب صد و سی و دو» رسید</p></div>
<div class="m2"><p>دولت از کوشش ابومسلم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پانصد و بیست و چار سال شدند</p></div>
<div class="m2"><p>ملک را خواجه خلق را منعم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پس قضای الهی آمد پیش</p></div>
<div class="m2"><p>گشت محرم به نقطه مجرم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خوار و موهون همی شدند «و من</p></div>
<div class="m2"><p>یهن الله ماله مکرم »</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ماند تاریخ سلب دولتشان</p></div>
<div class="m2"><p>چرخ برکند «دم » ز مستعصم</p></div></div>