---
title: >-
    شمارهٔ ۷۶ - در نعت حضرت رسالت
---
# شمارهٔ ۷۶ - در نعت حضرت رسالت

<div class="b" id="bn1"><div class="m1"><p>آن درختی که چون ز خاک برست</p></div>
<div class="m2"><p>سایه بر هفتم آسمان گسترد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آشکار است از شکوفه آن</p></div>
<div class="m2"><p>که بر زندگی ببار آورد</p></div></div>