---
title: >-
    شمارهٔ ۱۵۶
---
# شمارهٔ ۱۵۶

<div class="b" id="bn1"><div class="m1"><p>ای ز قسطنطین به دارالمک ایران تاخته</p></div>
<div class="m2"><p>صیت دانش در صف کون و مکان انداخته</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گه چو ابر اندر بهاران خیمه بر دریا زده</p></div>
<div class="m2"><p>گه چو سیل از کوهساران سوی صحرا تاخته</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در گلوی حق پرستان شهد رحمت ریخته</p></div>
<div class="m2"><p>بر سر اعدای دین شمشیر عدوان آخته</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شاد زی در پیشگاه شهریار حق شناس</p></div>
<div class="m2"><p>ای به درگاه شهنشه سر ز پا نشناخته</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تو بشه فرمان گذاری ما ترا فرمان پذیر</p></div>
<div class="m2"><p>تو به دولت باخته دل ما ترا دلباخته</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تابشی از برق تیغت خرمن مه سوخته</p></div>
<div class="m2"><p>جنبشی از نوک کلکت کار عالم ساخته</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آن یکی شمع کرامت در زمین افروخته</p></div>
<div class="m2"><p>وان دگر چتر شهامت بر سپهر افراخته</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عنقریبستی که بینم مر ترا ز اقبال شه</p></div>
<div class="m2"><p>ساخته کار زمین زی آسمان پرداخته</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تا تو از شادی چو کبکان در نشاط و خنده ای</p></div>
<div class="m2"><p>دشمنت بادا ز غم کوکو زنان چون فاخته</p></div></div>