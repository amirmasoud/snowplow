---
title: >-
    شمارهٔ ۱۸۳
---
# شمارهٔ ۱۸۳

<div class="b" id="bn1"><div class="m1"><p>ای خواجه عون سلطنه ای داوری که نیست</p></div>
<div class="m2"><p>یک تن همال و شبه تو در صفحه زمی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>داری هر آنچه ذکر شود جز کمال و فضل</p></div>
<div class="m2"><p>مانی بهر چه در نظر آید جز آدمی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گشت از نظام سلطنه شیراز منقلب</p></div>
<div class="m2"><p>مانند خاک بغداد از ابن علقمی</p></div></div>