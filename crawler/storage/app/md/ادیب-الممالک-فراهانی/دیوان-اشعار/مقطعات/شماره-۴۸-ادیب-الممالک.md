---
title: >-
    شمارهٔ ۴۸ - ادیب الممالک
---
# شمارهٔ ۴۸ - ادیب الممالک

<div class="n" id="bn1"><p>در سفر دوم که بآذربایجان آمده بودم این قطعه را از تبریز به کردستان خدمت خداوندزاده آقای عبدالحسین خان امیر تومان که به سالارالملک ملقب است و ایالت کردستان به وی مفوض می باشد فرستادم و در آن بر سبیل مطایبه اشعاری است بر اینکه میرزاعلی اکبر وقایع نگار که خود را در این دولت بقلب صادق الملکی ملقب کرده اسم مرا که صادق است به تقلب فرا گرفته.</p></div>
<div class="b" id="bn2"><div class="m1"><p>خدا یگانا از دستبرد چرخ دغل</p></div>
<div class="m2"><p>سه سال نام من از نامه ی جهان گم شد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو از صحیفه ایام محو شد نامم</p></div>
<div class="m2"><p>دلم چو دیده ز اندیشه در تلاطم شد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>برای یافتن وی بدست باد صبا</p></div>
<div class="m2"><p>کتابتم به خراسان و ساوه و قم شد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نشان نیافتم از وی به هیچ شهر و دیار</p></div>
<div class="m2"><p>شرار آهم ازین رو به چرخ هشتم شد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سپس شنیدم کس برده خواجه افسر کرد</p></div>
<div class="m2"><p>بدان مثابه که خود نیز در توهم شد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گرفته نام مرا از برای خویش لقب</p></div>
<div class="m2"><p>وزین شرف به همه خلق در تقدم شد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دلم بسوخت از این درد و دود ازو برخاست</p></div>
<div class="m2"><p>چنانکه دیدی آتش بخشک هیزم شد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>غمین شدم که چرا کرم پیله افعی گشت</p></div>
<div class="m2"><p>سته بدم که چرا عنکبوت کژدم شد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چگونه خود را صادق کند خطاب کسی</p></div>
<div class="m2"><p>که او مکذب نصب امیر در خم شد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هر آنکه بشندی این قصه در تحیر ماند</p></div>
<div class="m2"><p>هر آنکه برخواند این نکته در تبسم شد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نبشتمش که خدا را بخویش نام مرا</p></div>
<div class="m2"><p>مبند زانکه نخواهد شعیر گندم شد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>پلنگ باید سیاح کوه سهلان گشت</p></div>
<div class="m2"><p>نهنگ باید مساح بحر قلزم شد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ز نام نیکان کس نیکنام می نشود</p></div>
<div class="m2"><p>ببایدت پی نیکان گرفت و مردم شد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>به عجز و لابه ام آن سنگدل نبخشود ایچ</p></div>
<div class="m2"><p>بلی به بره کجا گرگ را ترحم شد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>جواب من همه از خامه اش سکوت آمد</p></div>
<div class="m2"><p>سلام من همه در حضرتش علیکم شد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چو بود جایش در آستان میر اجل</p></div>
<div class="m2"><p>کمینه نیز در آنجا پی تظلم شد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>وصول بنده و آهنگ وی به رسم فرار</p></div>
<div class="m2"><p>قرینه گشت و سر گاو رفته در خم شد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بسوی خانه خود شد ز آستان امیر</p></div>
<div class="m2"><p>ز خوان نعمت در بسترم تنعم شد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>خدایگانا بهر خدا اگر روزی</p></div>
<div class="m2"><p>به چرخ کاخ تو هم سلک عقد انجم شد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بگیر نام رهی را از او و باز فرست</p></div>
<div class="m2"><p>که مر ترا به هزاران چو وی تحکم شد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>وگر به محضر شرعم روان کنی گویم</p></div>
<div class="m2"><p>ز کره گی خرک لنگ بنده بی دم شد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>و گر به من ندهد گوش هوش خواهد دید</p></div>
<div class="m2"><p>که عنقریب دو گوشش جریمه دم شد</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>پی مطایبه این طرفه چامه بربستم</p></div>
<div class="m2"><p>اگر چه بر صفت تسخر و تهکم شد</p></div></div>