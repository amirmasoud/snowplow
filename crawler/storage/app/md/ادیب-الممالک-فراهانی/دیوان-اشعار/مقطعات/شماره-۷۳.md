---
title: >-
    شمارهٔ ۷۳
---
# شمارهٔ ۷۳

<div class="b" id="bn1"><div class="m1"><p>بخوان و خانه ات گر شکر و زر</p></div>
<div class="m2"><p>شود خرمن گدا و دزد ناید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>باین راه دراز و لقمه تلخ</p></div>
<div class="m2"><p>مرا پا رنج و دندان مزد باید</p></div></div>