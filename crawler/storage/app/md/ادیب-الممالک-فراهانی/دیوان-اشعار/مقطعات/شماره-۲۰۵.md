---
title: >-
    شمارهٔ ۲۰۵
---
# شمارهٔ ۲۰۵

<div class="b" id="bn1"><div class="m1"><p>ای ظهور تو چنان پدری</p></div>
<div class="m2"><p>کاشف راز یخرج المیت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گل و ریحان باغ چون نشدی</p></div>
<div class="m2"><p>باری آخر کرفس باش و شبت</p></div></div>