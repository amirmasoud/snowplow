---
title: >-
    شمارهٔ ۲۲۳ - در مرثیت برادر میرزا سید محمد مجتهد طباطبائی
---
# شمارهٔ ۲۲۳ - در مرثیت برادر میرزا سید محمد مجتهد طباطبائی

<div class="b" id="bn1"><div class="m1"><p>برادر پدر ما اگر ز دنیا رفت</p></div>
<div class="m2"><p>زسوگ او همه دلخسته و پریشانیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر این بزرگ پدر کردگار غم ندهاد</p></div>
<div class="m2"><p>که ما شریک غم آن وجود ذیشانیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو اوست حجة الاسلام و ما مسلمانان</p></div>
<div class="m2"><p>بزیر سایه او از خجسته کیشانیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بنص آیت المؤمنون اخوه تمام</p></div>
<div class="m2"><p>برادریم و ز جان غمگسار ایشانیم</p></div></div>