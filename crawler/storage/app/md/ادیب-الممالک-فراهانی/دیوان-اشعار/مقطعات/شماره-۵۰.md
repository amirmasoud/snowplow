---
title: >-
    شمارهٔ ۵۰
---
# شمارهٔ ۵۰

<div class="b" id="bn1"><div class="m1"><p>مردم ایران دو فرقه اند که هر یک</p></div>
<div class="m2"><p>تخم امل را درون مزرعه دارند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قومی مشروطه خواه گشته و بر دوش</p></div>
<div class="m2"><p>چتر ظفر با شعاع و شعشعه دارند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فرقه دیگر ز ارتجاع جهان را</p></div>
<div class="m2"><p>گاه به تضییق و گه به توسعه دارند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر دو پی عادت جراید ملی</p></div>
<div class="m2"><p>شور بنی عامربن صعصعه دارند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کوری آبای سبعه بین که سه مولود</p></div>
<div class="m2"><p>خشم بر آن مهارت اربعه دارند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مرتجعین مبتلای خیفره! هستند</p></div>
<div class="m2"><p>مردم مشروطه خواه جبلعه دارند</p></div></div>