---
title: >-
    شمارهٔ ۳۵
---
# شمارهٔ ۳۵

<div class="b" id="bn1"><div class="m1"><p>چشم مست تو مگر یپرم بمب انداز است</p></div>
<div class="m2"><p>یا ز ترکان صحیح النسب قفقاز است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چشم مستی که تو داری همه دارند ولی</p></div>
<div class="m2"><p>این روش در همه ساده رخان ممتاز است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دست خالی زده ام توپ به سودای تو من</p></div>
<div class="m2"><p>گر تو خیرم نکنی مشت من اینجا باز است</p></div></div>