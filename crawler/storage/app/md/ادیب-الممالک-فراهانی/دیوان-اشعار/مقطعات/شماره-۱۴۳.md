---
title: >-
    شمارهٔ ۱۴۳
---
# شمارهٔ ۱۴۳

<div class="b" id="bn1"><div class="m1"><p>شنیده ام عربان اشتران سالم را</p></div>
<div class="m2"><p>بجای اشتر گر داغ برنهند بتن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من اینک آن شتر سالمم که خواجه بعمد</p></div>
<div class="m2"><p>بجای اشتر گر داغ هشته بر گردن</p></div></div>