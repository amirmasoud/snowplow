---
title: >-
    شمارهٔ ۲۲۵
---
# شمارهٔ ۲۲۵

<div class="b" id="bn1"><div class="m1"><p>داورا ای که بهنگام مدیحت بورق</p></div>
<div class="m2"><p>بیزد از خامه مه و مهر و سهیل یمنم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا بحدیکه همه مدعیان پندارند</p></div>
<div class="m2"><p>مالک مرسله زهره و عقد پرنم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تو همه ماهی و خورشیدی و ابری و نسیم</p></div>
<div class="m2"><p>من خزان دیده و پژمرده گلی در چمنم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آفتابی که بگردون شده تابنده توئی</p></div>
<div class="m2"><p>سایه کافتد از آن، بر زبر خاک منم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خرد بودم من و دادیم بزرگی چندان</p></div>
<div class="m2"><p>که نگنجد بتن ای خواجه دگر پیرهنم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پیرهن بر تن سهل است که از وجد و طرب</p></div>
<div class="m2"><p>جای آنست که بیرون شود از پوست تنم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شیوه بوالحسنی داری وجود علوی</p></div>
<div class="m2"><p>ویژه با من که ز نسل علی «ع » بوالحسنم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>برگزیدی ز خردمندان در بارگهم</p></div>
<div class="m2"><p>برکشیدی زسخن سنجان در انجمنم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کرمت ماء معین ریخت بجام هوسم</p></div>
<div class="m2"><p>سخنت در ثمین داد بجای ثمنم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چون تو آوردی و اینجا تو نگهداشتیم</p></div>
<div class="m2"><p>هم تو بردی بر شاهنشه با خویشتنم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>می نترسم زبد چرخ و نباشم حیران</p></div>
<div class="m2"><p>کانکه آورده مرا باز برد در وطنم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بیژن آسا ز چه غصه برون آرد باز</p></div>
<div class="m2"><p>رستم فضل تو بی منت دلو و رسنم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>تو پرستنده حقی و گر این بنده ترا</p></div>
<div class="m2"><p>نپرستم ز سر صدق کم از برهمنم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ور ترا نیک پرستم همه دانند که من</p></div>
<div class="m2"><p>بنده حقم و از جان عدوی اهرمنم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>تو بنامیزد نقاد سخن باشی و من</p></div>
<div class="m2"><p>نیست در مخزن دانش گهری جز سخنم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گر سخن راست سرودم دهنم شیرین کن</p></div>
<div class="m2"><p>ورنه شاید که دو صد مشت زنی بر دهنم</p></div></div>