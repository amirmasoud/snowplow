---
title: >-
    شمارهٔ ۷۵
---
# شمارهٔ ۷۵

<div class="b" id="bn1"><div class="m1"><p>مهترابا یگانه منشی تو</p></div>
<div class="m2"><p>گرچه ایراد من خلاف بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>لیک از من بگو که در بروات</p></div>
<div class="m2"><p>ای دو سر قاف این چه قاف بود</p></div></div>