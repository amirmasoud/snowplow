---
title: >-
    شمارهٔ ۱۸۱
---
# شمارهٔ ۱۸۱

<div class="b" id="bn1"><div class="m1"><p>بحاجی رضاخان دکتر بگوی</p></div>
<div class="m2"><p>که جام طمع را حمیه توئی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو از دوده حارث کلده ای</p></div>
<div class="m2"><p>گل بوستان سمیه توئی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو عم تو باشد زیادبن صخر</p></div>
<div class="m2"><p>ز انصار آل امیه توئی</p></div></div>