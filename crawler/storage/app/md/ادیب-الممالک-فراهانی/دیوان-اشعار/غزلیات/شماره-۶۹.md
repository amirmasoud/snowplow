---
title: >-
    شمارهٔ ۶۹
---
# شمارهٔ ۶۹

<div class="b" id="bn1"><div class="m1"><p>فدای بدرو رخ ماه و زلف پر شکنش</p></div>
<div class="m2"><p>حلاوت لب شیرین ملاحت سخنش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سخن چو از لب لعلش برون شود گوئی</p></div>
<div class="m2"><p>بقند و مشک و می آمیخته است در دهنش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>قلم چو آهوی چین است و نامه دشت ختن</p></div>
<div class="m2"><p>عبیر و غالیه بارد ز نافه ختنش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چه آیت است ندانم که سجده کرد بر او</p></div>
<div class="m2"><p>بهار و باغ و ریاحین و سنبل و سمنش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگر چه شد غم عشقش بلای جان و تنم</p></div>
<div class="m2"><p>هزار جان و تن من فدای جان و تنش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عنان صبر رها کرده دل ز غصه آنک</p></div>
<div class="m2"><p>رها نمیکند ایام در کنار منش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کسی که لعل لبش خاتم سلیمان شد</p></div>
<div class="m2"><p>چه باک باشد از آسیب سحر اهرمنش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تو آن نگار دل افروز و شمع تابانی</p></div>
<div class="m2"><p>که کس نیافته پروانه را در انجمنش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بخاکپای عزیزت بود مرا شوقی</p></div>
<div class="m2"><p>که کور بر بصرش یا غریب بر وطنش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>غمی که بر دلم از دوریت فراز آمد</p></div>
<div class="m2"><p>نه بیستون متحمل شود نه کوهکنش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چنان نشسته خیال رخت به صفحه دل</p></div>
<div class="m2"><p>که ماه در فلکش یا که شمع در لگنش</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ادیب دست بدارد ز دامنت روزی</p></div>
<div class="m2"><p>که خاک تیره کند سوده دامن کفنش</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>وگرچو پیرهنت تنگ در بغل گیرد</p></div>
<div class="m2"><p>نگنجد این تن نالان درون پیرهنش</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>امیری از سر کویت همان طمع دارد</p></div>
<div class="m2"><p>که حاجی از حجر و بت پرست از وثنش</p></div></div>