---
title: >-
    شمارهٔ ۲۷ - چین
---
# شمارهٔ ۲۷ - چین

<div class="b" id="bn1"><div class="m1"><p>ای دختر خوبرو بدین طبع بلند</p></div>
<div class="m2"><p>از بام سپهر بر جهانی تو سمند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کن جهد و بدر سلسله و بگسل بند</p></div>
<div class="m2"><p>نه تن بقضا سپار و نه سر بکمند</p></div></div>