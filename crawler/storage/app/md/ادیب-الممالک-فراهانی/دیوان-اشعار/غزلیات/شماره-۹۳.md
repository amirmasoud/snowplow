---
title: >-
    شمارهٔ ۹۳
---
# شمارهٔ ۹۳

<div class="b" id="bn1"><div class="m1"><p>مهر در بیت الشرف شد ما به زندان اندریم</p></div>
<div class="m2"><p>ماه طالع گشت و ما با نحس کیوان اندریم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غرقه دریای اشگیم از غمش سر تا قدم</p></div>
<div class="m2"><p>لیک از هجران او در نار سوزان اندریم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای تن آسان مانده در ساحل باستخلاص ما</p></div>
<div class="m2"><p>همتی بگمار کاندر موج طوفان اندریم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پرتوی ای مهر رحمت لطفی ای باد بهار</p></div>
<div class="m2"><p>زانکه ما در دست سرمای زمستان اندریم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای ز وصل دوستان آسوده در دارالسرور</p></div>
<div class="m2"><p>یاد کن از ما که در این بیت الاحزان اندریم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>روزگاری شد که با جمعی پریشان روزگار</p></div>
<div class="m2"><p>بسته در زنجیر آن زلف پریشان اندریم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون سکندر تشنه آب حیاتیم از لبش</p></div>
<div class="m2"><p>زین سبب دیری است در ظلمات هجران اندریم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر چه می نالیم چون بلبل ز هجرانش مدام</p></div>
<div class="m2"><p>لیک از یاد رخش در باغ و بستان اندریم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نامسلمانست چشمش ای مسلمانان فغان</p></div>
<div class="m2"><p>کاین زمان در دست ترکی نامسلمان اندریم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دیو در خلوتگه ماره ندارد کآشکار</p></div>
<div class="m2"><p>با یر پرویان غیبی در شبستان اندریم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سرکشی کردیم از فرمان عقل اما بطوع</p></div>
<div class="m2"><p>شهریار عشق را گردن بفرمان اندریم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>از امیری خواستم اسرار پیر عشق را</p></div>
<div class="m2"><p>گفت ما با کودکان در یک دبستان اندریم</p></div></div>