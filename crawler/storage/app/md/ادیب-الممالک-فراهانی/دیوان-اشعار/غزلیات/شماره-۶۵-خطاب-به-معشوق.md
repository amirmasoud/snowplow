---
title: >-
    شمارهٔ ۶۵ - خطاب به معشوق
---
# شمارهٔ ۶۵ - خطاب به معشوق

<div class="b" id="bn1"><div class="m1"><p>دارم سری از خیال در پیش</p></div>
<div class="m2"><p>وز درد فتاده ام به تشویش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کان دلبر شوخ چشم عیار</p></div>
<div class="m2"><p>رانده است مرا ز حضرت خویش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در خانه خود صلا زد آنگاه</p></div>
<div class="m2"><p>کفش ادبم نهاد در پیش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>افکند ز طمراق و نازم</p></div>
<div class="m2"><p>خندید مرا بسلبت و ریش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفتم که ارادتم چند بیند</p></div>
<div class="m2"><p>هر روز محبتش شود بیش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر عکس مراد خویش دیدم</p></div>
<div class="m2"><p>سلطان نکند نظر بدرویش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای دل اگر آن نگار طناز</p></div>
<div class="m2"><p>مرهم ننهاد بر دل ریش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زین بیش ز مهر وی مجو کام</p></div>
<div class="m2"><p>زین بیش ز قهر وی میندیش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شاهان را این چنین بود رسم</p></div>
<div class="m2"><p>خوبان را این چنین بود کیش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بیچاره امیریست کو را</p></div>
<div class="m2"><p>جز تیر دعا نمانده در کیش</p></div></div>