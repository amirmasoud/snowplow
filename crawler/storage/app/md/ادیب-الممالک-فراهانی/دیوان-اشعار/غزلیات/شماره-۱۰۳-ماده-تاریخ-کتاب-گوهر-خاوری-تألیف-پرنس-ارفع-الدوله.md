---
title: >-
    شمارهٔ ۱۰۳ - ماده تاریخ کتاب گوهر خاوری تألیف پرنس ارفع الدوله
---
# شمارهٔ ۱۰۳ - ماده تاریخ کتاب گوهر خاوری تألیف پرنس ارفع الدوله

<div class="b" id="bn1"><div class="m1"><p>گوهر خاوری است این دیوان</p></div>
<div class="m2"><p>که بود رشک گوهر عمان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نامه ای در ضیا چو مهر منیر</p></div>
<div class="m2"><p>چامه ای در صفا چوآب روان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اثر کلک دانش است که یافت</p></div>
<div class="m2"><p>چون خضر ره بچشمه حیوان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بحر همت پرنس صلح طلب</p></div>
<div class="m2"><p>میر نویان فیلسوف جهان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ارفع الدوله آکه از رفعت</p></div>
<div class="m2"><p>برده بر باب فضل شادروان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در سخاوت گذشته از حاتم</p></div>
<div class="m2"><p>در فصاحت فزوده بر حسان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نام او زخم ننگ را مرهم</p></div>
<div class="m2"><p>صلح او درد جنگ را درمان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شاد زی ای یگانه آفاق</p></div>
<div class="m2"><p>شاد زی ای خلاصه دوران</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون بپایان رسید این دفتر</p></div>
<div class="m2"><p>که بود قدر ذوق را میزان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از امیری بخواستم تاریخ</p></div>
<div class="m2"><p>بهر انشاء و طبع این دیوان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گفت تاریخ ختم انشایش</p></div>
<div class="m2"><p>سزد از طبع گوهر غلطان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هم بتاریخ طبع آن بنگاشت</p></div>
<div class="m2"><p>خاوری گوهر آورد وجدان</p></div></div>