---
title: >-
    شمارهٔ ۷۰ - مرثیه
---
# شمارهٔ ۷۰ - مرثیه

<div class="b" id="bn1"><div class="m1"><p>نوجوان مرا فلک خوندل ریخت در ایاغ</p></div>
<div class="m2"><p>نونهال مرا سپهر کند از بن بطرف باغ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شمعی افروختم که گشت روشن از نور او جهان</p></div>
<div class="m2"><p>ناگهان صرصری وزید کرد خاموش آن چراغ</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای فقید کمال و فضل ای شهید سنان غم</p></div>
<div class="m2"><p>از غمت دیده پر ز اشک بی رخت سینه پر ز داغ</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در عزای تو قامتم گشت خمیده چون کمان</p></div>
<div class="m2"><p>وز فراق تو روز من شد سیه همچو پر زاغ</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون بیاد تو بگروم غافل از خویشتن شوم</p></div>
<div class="m2"><p>در پی جان شکر دوم تا کنم مرگرا سراغ</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بسکه چون لاله بر دلم داغ هجرت گرفته جای</p></div>
<div class="m2"><p>گشت تایخ رفتنت لاله دارد دلی بداغ</p></div></div>