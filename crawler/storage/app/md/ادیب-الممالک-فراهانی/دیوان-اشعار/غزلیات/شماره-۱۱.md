---
title: >-
    شمارهٔ ۱۱
---
# شمارهٔ ۱۱

<div class="b" id="bn1"><div class="m1"><p>لسان را سحر در طی لسانست</p></div>
<div class="m2"><p>مه و خورشیدش اندر طیلسانست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عروس فضلش اندر حجله طبع</p></div>
<div class="m2"><p>چو در فردوس خیرات حسانست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>لسانا ای که کلک در فشانم</p></div>
<div class="m2"><p>بمدحت جاودان رطب اللسانست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>توئی آنکس که تیغ خامه ات را</p></div>
<div class="m2"><p>دل سنگ پریرویان فسانست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نمی پرسی نشان از حال بیمار</p></div>
<div class="m2"><p>که روزش چون و حالش بر چه سانست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مشو در شام تار از روز نومید</p></div>
<div class="m2"><p>که نومیدی شعار ناکسانست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بسان کوه آهن دل قوی دار</p></div>
<div class="m2"><p>که ایزد بنده را روزی رسانست</p></div></div>