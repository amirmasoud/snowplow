---
title: >-
    شمارهٔ ۲۰ - در ستایش دانش بپارسی سره
---
# شمارهٔ ۲۰ - در ستایش دانش بپارسی سره

<div class="b" id="bn1"><div class="m1"><p>از آندمی که پدیدار گشت هوش نخست</p></div>
<div class="m2"><p>پی نماز کمر بست پیش یزدان چست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو سرور است شد و چون بنفشه سر در پیش</p></div>
<div class="m2"><p>چو غنچه دوخت لب از گفتگوی و چون گل رست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سپس بگفته یزدان شد از سپهر بخاک</p></div>
<div class="m2"><p>نشست در سر دانا و مغز او را شست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز کردگار رسیدش بگوش جان فر تاب</p></div>
<div class="m2"><p>که پیشوای جهانی و گفته گفته تست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کجا که باشی کفشیر هر شکسته کنی</p></div>
<div class="m2"><p>کجا که نیستی آنجا شکسته است درست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بگیر پورا دامان هوش و دست خرد</p></div>
<div class="m2"><p>مگیر گفت مرا یاوه و گزافه و سست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خرد رهی است کز او هر که هرچه جوید یافت</p></div>
<div class="m2"><p>خرد رهیست کزان هرکه هرچه خواهد جست</p></div></div>