---
title: >-
    شمارهٔ ۹۱ - در جشن سال دوم مجلس شورای ملی ۱۳۲۵
---
# شمارهٔ ۹۱ - در جشن سال دوم مجلس شورای ملی ۱۳۲۵

<div class="b" id="bn1"><div class="m1"><p>مهناباد این جشن معظم</p></div>
<div class="m2"><p>مبارک باد این عید مفخم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بفرزندان مرز و بوم ایران</p></div>
<div class="m2"><p>هواخواهان قانون مکرم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به همدستان دفع مستبدین</p></div>
<div class="m2"><p>بهمراهان خیر خلق عالم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بجانبازان عین عدل دستور</p></div>
<div class="m2"><p>بانبازان منع ما تقدم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بمبعوثان خیراندیش ملت</p></div>
<div class="m2"><p>مهین نواب مختار و مقدم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بانصار مهین شورای ملی</p></div>
<div class="m2"><p>بهمراهان این بنیاد محکم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چه بنیادی که با پیرایه و لاف</p></div>
<div class="m2"><p>تواند بود سر انی اعلم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بشد در موقع این عید ملی</p></div>
<div class="m2"><p>پی بنیان خود رایان مهدم</p></div></div>