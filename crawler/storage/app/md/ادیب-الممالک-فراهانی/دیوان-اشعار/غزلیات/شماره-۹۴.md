---
title: >-
    شمارهٔ ۹۴
---
# شمارهٔ ۹۴

<div class="b" id="bn1"><div class="m1"><p>تا رخ شمس السعاده تافت در ایوان</p></div>
<div class="m2"><p>سر زد ایوان ز راه فخر به کیوان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بود شب جمعه و چهارم شعبان</p></div>
<div class="m2"><p>کاین مه تابان گشود چهره در ایوان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چارده ماهی بسال چارده آمد</p></div>
<div class="m2"><p>خرم و سرسبز همچو سرو به بستان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ماشاء الله تبارک الله بنگر</p></div>
<div class="m2"><p>سرو سهی قد برخ چو لاله نعمان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دختری اندر حجاب صفوت و عصمت</p></div>
<div class="m2"><p>اختری از ماهتاب برده گروگان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خواست امیری زبحر طبع بتاریخ</p></div>
<div class="m2"><p>رشته کشد گوهری چو لؤلؤ و مرجان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>طبعش غواصیی نمود و پس آنگاه</p></div>
<div class="m2"><p>سر بدر آورد کای ادیب سخندان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آنچه ز مه رفته رفته گیر و رقم زن</p></div>
<div class="m2"><p>مولد شمس السعاده چارم شعبان</p></div></div>