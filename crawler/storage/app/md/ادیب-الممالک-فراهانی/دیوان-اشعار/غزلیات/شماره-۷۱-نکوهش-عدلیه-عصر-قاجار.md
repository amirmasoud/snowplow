---
title: >-
    شمارهٔ ۷۱ - نکوهش عدلیه عصر قاجار
---
# شمارهٔ ۷۱ - نکوهش عدلیه عصر قاجار

<div class="b" id="bn1"><div class="m1"><p>باست و قاف محاکم قضیب استیناف</p></div>
<div class="m2"><p>چنان سپوخت که دیگر نه است ماند و نه ناف</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نشان عدل چه جوئی در این دو سر قافان</p></div>
<div class="m2"><p>که زین شهپر سیمرغ شد به قله قاف</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>وزیر زیر لحاف از هوی سخن گوید</p></div>
<div class="m2"><p>اگر چه حق نتوان گفت جز بزیر لحاف</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>غلاف . . . من است این وزیر دون پلید</p></div>
<div class="m2"><p>که تیغ حق را پنهان کنند درون غلاف</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شیاف . . . من است اینکه از بزرگی سر</p></div>
<div class="m2"><p>بدهن گنجد در . . . یش نگنجد شیاف</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نگاه آل مکرم به اهل ایران شد</p></div>
<div class="m2"><p>چو حربیان به بنی هاشم بن عبد مناف</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یکی مشاوره تأسیس شد بدرالعدل</p></div>
<div class="m2"><p>که هست مذبح ایمان و مدفن انصاف</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گرفته فاضل خلخالی اندر آن مسند</p></div>
<div class="m2"><p>چو شیخ مهدی کاشی به مجلس اوقاف</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>برادران وطن را عدوی خود شمرند</p></div>
<div class="m2"><p>که رشک و کینه جبلی است فی هوالاجیاف</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>وزیر دون چو به آزار بیگناهی زار</p></div>
<div class="m2"><p>کمر به بندد و باشد زبون بگاه مصاف</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از آن گروه ستمگر که در اداره عدل</p></div>
<div class="m2"><p>حلیف باطل و با او بمائده احلاف</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تنی سه چار پی مشورت برانگیزد</p></div>
<div class="m2"><p>دهد برایشان دستوری از نفاق و خلاف</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>کسی که از طرف عدل حق نداشت عدول</p></div>
<div class="m2"><p>اشاره کرده و تحریک سازد از اطراف</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>که بر زنند به یکباره پشم و پنبه او</p></div>
<div class="m2"><p>بزخم مندفه ظلم و کینه چون نداف</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>همی بتازند آن جاهلان بدانایان</p></div>
<div class="m2"><p>چنانکه سبع سمان زیر پای سبع عجاف</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>مسافران را عوعو کنان براه از دور</p></div>
<div class="m2"><p>شوند همچو سگان قبیله بر اضیاف</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>درون روند تهی دست و چون برون آیند</p></div>
<div class="m2"><p>ز سیم دامنشان پرچو دکه صراف</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>تمام آکل و ماکول جنس یکدیگرند</p></div>
<div class="m2"><p>مرتبا ز اراذل بگیر تا اشراف</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>یکی درد دل اصداف بهر مروارید</p></div>
<div class="m2"><p>یکی ز گوهر آبستن است چون اصداف</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>به نیزه طمع انجیده اند شانه عدل</p></div>
<div class="m2"><p>چو شانه عربان از سنان ذوالاکتاف</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>درون محکمه بر ناز و عشوه افزایند</p></div>
<div class="m2"><p>از آن سپس که ستانند رشوه قدر کفاف</p></div></div>