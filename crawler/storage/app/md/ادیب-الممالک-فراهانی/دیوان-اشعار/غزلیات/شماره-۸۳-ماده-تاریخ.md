---
title: >-
    شمارهٔ ۸۳ - ماده تاریخ
---
# شمارهٔ ۸۳ - ماده تاریخ

<div class="b" id="bn1"><div class="m1"><p>چو توفیق و تایید حی قدیم</p></div>
<div class="m2"><p>بحاجی علی النقی شد ندیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یکی کعبه آراست در قم که گشت</p></div>
<div class="m2"><p>پناهیده در ظل رکنش حطیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بمحرابش افتد سلیمان بخاک</p></div>
<div class="m2"><p>بخاکش کشد موزه از پا گلیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو رخت سفر بست ازین خاکدان</p></div>
<div class="m2"><p>بفردوس جاوید و باغ نعیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بحاجی محمدعلی پوروی</p></div>
<div class="m2"><p>که از مثل او چار مادر عقیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بگوش خرد در رسید این سروش</p></div>
<div class="m2"><p>که ای مرد دانا و ذات کریم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زآثار کن زنده نام پدر</p></div>
<div class="m2"><p>که نام پدر بهتر از زر و سیم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو بشنید آن راد مرد این ندا</p></div>
<div class="m2"><p>بوجهی جمیل و بقلبی سلیم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>درین بقعه از این دکاکین نهاد</p></div>
<div class="m2"><p>اساسی متین و بنائی قویم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سپس جمله را جاودان وقف کرد</p></div>
<div class="m2"><p>بر این پاک بنیان و والاحریم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بتاریخ آن جستم از بحر طبع</p></div>
<div class="m2"><p>یکی شطر چون عقد در نظیم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>امیری بتاریخ این امر خیر</p></div>
<div class="m2"><p>رقم زد لواقف فوز عظیم</p></div></div>