---
title: >-
    شمارهٔ ۹۸
---
# شمارهٔ ۹۸

<div class="b" id="bn1"><div class="m1"><p>یک سوزن و یک سنجاق بودند به سوزندان</p></div>
<div class="m2"><p>مانند دو تن عیار افتاده به یک زندان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سنجاق به سوزن گفت کار تو در اینجا چیست</p></div>
<div class="m2"><p>وز بهر چه خسبیدی اندر دل سوزاندان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سوزن بجوابش گفت ای بی هنر سوری</p></div>
<div class="m2"><p>من خادم خاتونم سرخیل هنرمندان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دندانه من تیز است زین روی مرا بی بی</p></div>
<div class="m2"><p>بنشاند در این خانه مزد هنر دندان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سنجاق بگفتش رو، کز روزن زیرین است</p></div>
<div class="m2"><p>پیوسته ترا روزی بی ضامن و پایندان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دجال صفت یک چشم افسار نگون از پشم</p></div>
<div class="m2"><p>دندانه زنی با خشم زانگشتره بر سندان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گفتا چکنی منعم کز خدمت کدبانو</p></div>
<div class="m2"><p>چنانکه بلابینم هم شادم و هم خندان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بنگر تو بعیب خویش ای کله کلان کز جهل</p></div>
<div class="m2"><p>هم عبرت خویشانی هم حیرت پیوندان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زآنرو که شوی در کار کوژ و کژ و خمیده</p></div>
<div class="m2"><p>چون سیخ کباب مست اندر شب برغندان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ناگاه عجوز آمد سنجاق برون آورد</p></div>
<div class="m2"><p>تا وصله کند با وی رخت تن فرزندان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هنگفت بد آن جامه خمیده شد آن سنجاق</p></div>
<div class="m2"><p>چون در دل کج طبعان اندرز خردمندان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>سنجاق بخاک افکند برداشت یکی سوزن</p></div>
<div class="m2"><p>کاندر نظرش سنجاق باقدر نبد چندان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>از غلظت آن جامه بشکست سر سوزن</p></div>
<div class="m2"><p>چون بیل کشاورزان در موسم یخ بندان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>سوزن به زمین افتاد غلتید بر سنجاق</p></div>
<div class="m2"><p>سنجاق به تعظیمش برجست چو اسپندان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>آهسته بگوشش گفت مائیم دو فرمان بر</p></div>
<div class="m2"><p>کز بی هنری خواریم در چشم خداوندان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بدبختی خود را من از چشم تو می دانم</p></div>
<div class="m2"><p>بدبختی خود را تو نیز از قبل من دان</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>هنگام هنر بودیم با خویش عدو اینک</p></div>
<div class="m2"><p>از بی هنری هستیم ضرب المثل رندان</p></div></div>