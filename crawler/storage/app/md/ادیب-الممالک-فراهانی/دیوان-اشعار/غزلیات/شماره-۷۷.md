---
title: >-
    شمارهٔ ۷۷
---
# شمارهٔ ۷۷

<div class="b" id="bn1"><div class="m1"><p>آن خمیری را کز آب سلسبیل</p></div>
<div class="m2"><p>با دم عیسی سرشته جبرئیل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دست مریم گشته بیرون ز آستین</p></div>
<div class="m2"><p>پخته زاو نان و برنج و زنجبیل</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بوده از شهد شکر در مصر جان</p></div>
<div class="m2"><p>دیده از دریای روغن رود نیل</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مانده در طوفان حیرت همچو نوح</p></div>
<div class="m2"><p>رفته در نار محبت چون خلیل</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عاقبت از همت والای دوست</p></div>
<div class="m2"><p>جسته ره در مقصد دل بیدلیل</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر طبق بنهاده جان بی اختیار</p></div>
<div class="m2"><p>تا سبیل آرد به ابناء السبیل</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پشه گر شیرین کند زوکام جان</p></div>
<div class="m2"><p>حلقه طاعت کشد در گوش فیل</p></div></div>