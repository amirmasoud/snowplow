---
title: >-
    شمارهٔ ۸۲
---
# شمارهٔ ۸۲

<div class="b" id="bn1"><div class="m1"><p>ای برق نژاد آهن اندام</p></div>
<div class="m2"><p>مغناطیس عقول و افهام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جاسوس امور شرک و توحید</p></div>
<div class="m2"><p>ناموس رموز کفر و اسلام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پیغمبر ناطق جمادی</p></div>
<div class="m2"><p>انموزج داستان الهام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دانای سخنگذار بی لب</p></div>
<div class="m2"><p>سیاح جهان نور بی گام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در یم شوی و شنا ندانی</p></div>
<div class="m2"><p>گیتی سپری و داری آرام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مانند حمامة الهوادی</p></div>
<div class="m2"><p>نامه ببری بوقت و هنگام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از مانی پلاتر و رستبر</p></div>
<div class="m2"><p>داری پسری الکترون نام</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>و آن کودک نوبهر زمانی</p></div>
<div class="m2"><p>داناست ز ابتدا و انجام</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خواهم ز زبان بندگانش</p></div>
<div class="m2"><p>بگذاری نزد میر پیغام</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کای میر ستوده مؤید</p></div>
<div class="m2"><p>وی داور مهتر نکو نام</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تشریف ایالت خراسان</p></div>
<div class="m2"><p>فرخ بادت بفرخ اندام</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بر صفحه روزگار مانی</p></div>
<div class="m2"><p>در سایه شهریار پدرام</p></div></div>