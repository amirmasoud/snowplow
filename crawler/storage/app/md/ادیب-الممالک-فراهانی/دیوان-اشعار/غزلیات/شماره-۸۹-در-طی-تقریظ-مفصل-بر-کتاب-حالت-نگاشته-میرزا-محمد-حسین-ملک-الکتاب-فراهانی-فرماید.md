---
title: >-
    شمارهٔ ۸۹ - در طی تقریظ مفصل بر کتاب حالت نگاشته میرزا محمد حسین ملک الکتاب فراهانی فرماید
---
# شمارهٔ ۸۹ - در طی تقریظ مفصل بر کتاب حالت نگاشته میرزا محمد حسین ملک الکتاب فراهانی فرماید

<div class="b" id="bn1"><div class="m1"><p>دانا باید ز روی فکر زند دم</p></div>
<div class="m2"><p>تا ز پس دم زدن همی نخورد غم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هست سخن مرد را ترازوی دانش</p></div>
<div class="m2"><p>نیست بسنجیده مرد تا نزند دم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جز بسخن کان ترازوی هنرستی</p></div>
<div class="m2"><p>پایه نگیرد فزونی و نشود کم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پس تو سخن گوی را شناخت توانی</p></div>
<div class="m2"><p>ره نبری بر شناس اخرس و ابکم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نیست سخنگوی راستگوی خداجوی</p></div>
<div class="m2"><p>جز ملک ملک فضل در همه عالم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>احمد را باوه مرتضی را فرزند</p></div>
<div class="m2"><p>دانش را زاده مردمی را بن عم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شاه بکتاب عصر کردش سالار</p></div>
<div class="m2"><p>زانکه ز کتاب بود یکسره اقدم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>راستی آنرا مسلم است که هرگز</p></div>
<div class="m2"><p>جز بدر کردگار می نشود خم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ریشه گردون اگر زین بدر آید</p></div>
<div class="m2"><p>گفته وی استوار ماند و محکم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ای ز جمال تو چشم بینش روشن</p></div>
<div class="m2"><p>وی ز کمال تو باغ دانش خرم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کلکت با رد به نثر لؤلؤ منثور</p></div>
<div class="m2"><p>فکرت دارد عقود نظم منظم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>فضل و هنر بوده مرکسان را زین پیش</p></div>
<div class="m2"><p>لیک در این عصر بر تو گشته مسلم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>طبع تو بر آسکون طرازد کشتی</p></div>
<div class="m2"><p>فکر تو بر آسمان فرازد سلم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>هوش روان گر بچشم خلق درآید</p></div>
<div class="m2"><p>عقل مصور توئی و روح مجسم</p></div></div>