---
title: >-
    شمارهٔ ۲
---
# شمارهٔ ۲

<div class="b" id="bn1"><div class="m1"><p>سزد ار سجده برد میر فراهانی را</p></div>
<div class="m2"><p>گر ز خاقان گذرد مرتبه خاقانی را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای امیر قرشی زاده کت اعجاز سخن</p></div>
<div class="m2"><p>بند بر ناطقه زد منطق سحبانی را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر برند اینگهر نظم تو در سوق عکاظ</p></div>
<div class="m2"><p>کس پشیزی نخرد سلعه ذبیانی را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عرق از خجلت تشبیب تو از نیل گذشت</p></div>
<div class="m2"><p>چهره طبع منوچهری دمغانی را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مدعی گو گله کم کن که بهر خس ندهد</p></div>
<div class="m2"><p>فیض روح القدسی رتبه حسانی را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شعرا را همه گر سحر حلال است حدیث</p></div>
<div class="m2"><p>دیده بگشا و ببین آیت عمرانی را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا نیامد بسخن نطق تو معلوم نبود</p></div>
<div class="m2"><p>کابرنیسان ز که آموخت درافشانی را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر شود ختم سخن بر تو امیری چه عجب</p></div>
<div class="m2"><p>کاخرین پایه همین است سخندانی را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کوس تسخیر فروکوب که در کشور نظم</p></div>
<div class="m2"><p>بخت بر نام تو زد سکه قاآنی را</p></div></div>