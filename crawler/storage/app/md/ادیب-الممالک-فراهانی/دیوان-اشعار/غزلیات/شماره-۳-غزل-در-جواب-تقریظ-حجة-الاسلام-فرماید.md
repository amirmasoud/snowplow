---
title: >-
    شمارهٔ ۳ - غزل در جواب تقریظ حجة الاسلام فرماید
---
# شمارهٔ ۳ - غزل در جواب تقریظ حجة الاسلام فرماید

<div class="b" id="bn1"><div class="m1"><p>عجبی نیست مر آن آیت ربانی را</p></div>
<div class="m2"><p>گر کند زنده ز نو حکمت لقمانی را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای بتاریک شب کفر برافروخته باز</p></div>
<div class="m2"><p>پدرت در ره دین شمع مسلمانی را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگر آن آیت رخشنده هویدا نشدی</p></div>
<div class="m2"><p>کس نخواندی زورق آیت فرقانی را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تو از آن شاخ برومند بزادی که ز فضل</p></div>
<div class="m2"><p>درس توحید دهد نخله عمرانی را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حجج بالغه شرع بیاراست چنانک</p></div>
<div class="m2"><p>شست از صفحه دین حکمت یونانی را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>توئی آن عاقله دور مه و مهر که عقل</p></div>
<div class="m2"><p>نزد فرهنگ تو گیرد ره نادانی را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ملکات کلمات تو بنیروی مآل</p></div>
<div class="m2"><p>عقل بالفعل کند طبع هیولانی را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا به میدان خرد اسب هنر تاخته ای</p></div>
<div class="m2"><p>دست بستی به قفا فاضل میدانی را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>رقمت ناسخ ریحان خط لاله رخان</p></div>
<div class="m2"><p>بر شکسته خط طغرای صفاهانی را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دم عیسی ز عقیق لب لعل تو وزد</p></div>
<div class="m2"><p>گهرت خیره کند تاج سلیمانی را</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>حجة الاسلام آمد لقبت زانکه بخلق</p></div>
<div class="m2"><p>بشناسانی مر حجت یزدانی را</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بنده آن رتبه ندارد که تو در چامه خویش</p></div>
<div class="m2"><p>در حق وی کنی اینسان گهر افشانی را</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>لیک در سایه مهرت بشعیری نخرم</p></div>
<div class="m2"><p>زین سپس مخزن شعر حسن هانی را</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چند فرسوده کنم طبع بهل تاببرند</p></div>
<div class="m2"><p>چامه غیداء و آن ملحفه قانی را</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>سرو سامان شهی دارم و در بندگیت</p></div>
<div class="m2"><p>به فلک یاد دهم بی‌سر و سامانی را</p></div></div>