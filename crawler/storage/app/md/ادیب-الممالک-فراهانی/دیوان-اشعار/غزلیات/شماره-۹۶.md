---
title: >-
    شمارهٔ ۹۶
---
# شمارهٔ ۹۶

<div class="b" id="bn1"><div class="m1"><p>مرد چو باشد به وقت کار هراسان</p></div>
<div class="m2"><p>مشکل گردد ورا به دیده هر آسان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عزم درست و دل قویت چو باشد</p></div>
<div class="m2"><p>کوه توانی همی بسفت به پیکان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>باید دل ساخت ز آهنی که نگردد</p></div>
<div class="m2"><p>دستخوش امتحان و آژده سوهان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مشت چو سندان اگر نداری هرگز</p></div>
<div class="m2"><p>می نتوانی نواخت مشت بسندان</p></div></div>