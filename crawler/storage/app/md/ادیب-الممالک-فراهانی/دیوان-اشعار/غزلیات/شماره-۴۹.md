---
title: >-
    شمارهٔ ۴۹
---
# شمارهٔ ۴۹

<div class="b" id="bn1"><div class="m1"><p>مژده ای دل که ز ره قافله داد آمد</p></div>
<div class="m2"><p>نایب السلطنه با داد خداداد آمد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بحر علم آمد و از گوهر تابان زد موج</p></div>
<div class="m2"><p>کوه عزم آمد و با پنجه پولاد آمد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ناصرالملک ابوالقاسم مسعود ز راه</p></div>
<div class="m2"><p>با رخی خوب و تنی پاک و دلی شاد آمد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آمد اندر مه دی با نفس فروردین</p></div>
<div class="m2"><p>چون گل و میوه که اندر مه خرداد آمد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا چو باد سحری تاخت سوی گلشن داد</p></div>
<div class="m2"><p>خیمه ظلم و جهالت همه بر باد آمد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شاد باش ای چمن ملک که چون باد بهار</p></div>
<div class="m2"><p>باغبان با نفسی گرم و کفی راد آمد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نوبهار آمد و از بوی خوش باد ربیع</p></div>
<div class="m2"><p>تهنیت باد به سرو و گل و شمشاد آمد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>باغ پژمرده ما از اثر مقدم وی</p></div>
<div class="m2"><p>خوبتر از چمن خلخ و نوشاد آمد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>غم ویرانی کشور چه خوری کاین معمار</p></div>
<div class="m2"><p>بهر آبادی ایوان مه آباد آمد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>حاسدت خشت به دریا زند و سنگ به سر</p></div>
<div class="m2"><p>کاوستاد هنری بر سر بنیاد آمد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خانه و گلشن ویران شده را خواهی دید</p></div>
<div class="m2"><p>عنقریبا که ز فکرش همه آباد آمد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گشت امید برومند شود کز قدمش</p></div>
<div class="m2"><p>آب در جوی روان چون شط بغداد آمد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ای جوانان نوآموز دبستان وطن</p></div>
<div class="m2"><p>لوح تعلیم بیارید که استاد آمد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>حال و فال همه نیکو شد ازین خواجه راد</p></div>
<div class="m2"><p>که نکوکار و نکوخواه و نکوزاد آمد</p></div></div>