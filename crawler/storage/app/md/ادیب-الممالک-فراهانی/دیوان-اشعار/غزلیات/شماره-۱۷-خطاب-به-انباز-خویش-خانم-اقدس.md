---
title: >-
    شمارهٔ ۱۷ - خطاب به انباز خویش خانم اقدس
---
# شمارهٔ ۱۷ - خطاب به انباز خویش خانم اقدس

<div class="b" id="bn1"><div class="m1"><p>در دلم جز هوای اقدس نیست</p></div>
<div class="m2"><p>و اندران باغ جای هر خس نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غیر را ره در این سرا نبود</p></div>
<div class="m2"><p>خانه از او است از دگر کس نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>قله قاف جای سیمرغ است</p></div>
<div class="m2"><p>آشیان کلاغ و کرکس نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>غیر قدش که شد معدل حسن</p></div>
<div class="m2"><p>اختری در سپهر اطلس نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>قبلگاه دلم بجز کویش</p></div>
<div class="m2"><p>اندرین طارم مسدس نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آفتابی چو عارضش تابان</p></div>
<div class="m2"><p>اندرین گنبد مقرنس نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ذات او را بجان کنم تقدیس</p></div>
<div class="m2"><p>که به گیتی چو او مقدس نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سرو جز چوب خشک و گل جز خار</p></div>
<div class="m2"><p>پیش آن نونهال نورس نیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زیور اطلس و پرند است او</p></div>
<div class="m2"><p>زیور او پرند و اطلس نیست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گر به یک موی او مرا دو جهان</p></div>
<div class="m2"><p>حق تعالی عطا کند بس نیست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دلم از دوریش همی نالد</p></div>
<div class="m2"><p>که اسیری چو او به محبس نیست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گر امیری خلاف عهد کند</p></div>
<div class="m2"><p>بی شک از خاندان افطس نیست</p></div></div>