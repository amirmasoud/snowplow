---
title: >-
    شمارهٔ ۱۱۳ - در ستایش پرنس ارفع الدوله
---
# شمارهٔ ۱۱۳ - در ستایش پرنس ارفع الدوله

<div class="b" id="bn1"><div class="m1"><p>امروز جانرا با طرب هنگام پیوند آمده</p></div>
<div class="m2"><p>دل در نشاط آماده شد لب در شکر خنده آمده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سردار دانایان زره با تاب مهر روی مه</p></div>
<div class="m2"><p>در موکب مسعود شه فیروز و خرسند آمده</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آن طالب نام نکو و آن پرنس صلح جو</p></div>
<div class="m2"><p>مه ارفع الدوله که او بی مثل و مانند آمده</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از خاوران در باختر با شاه ما شد در سفر</p></div>
<div class="m2"><p>همراه وی در بحر و بر فضل خداوند آمده</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گوئی ز نو روح الامین آمده ز بالا بر زمین</p></div>
<div class="m2"><p>یا بار دیگر فرودین بر جای اسفند آمده</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خوشا هژیرا خرما کان خواجه عیسی دما</p></div>
<div class="m2"><p>در ساحت ملک جما شاد و فرهمند آمده</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هوش و خرد فتح و ظفر عقل و ادب فضل و هنر</p></div>
<div class="m2"><p>در پیش این فرخ پدر چون هشت فرزنده آمده</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دانش پرستی کار وی فضل و هنر آثار وی</p></div>
<div class="m2"><p>در گوش جان گفتار وی ستوار چون پند آمده</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اقبال او هر دم فزون بخت عدویند واژگون</p></div>
<div class="m2"><p>ملک از قدومش تازه چون مغز خردمند آمده</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>میری که گردون جاه او دولت رفیق راه او</p></div>
<div class="m2"><p>غم در دل بدخواه او چون کوه الوند آمده</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خورشید شمع منظرش بهرام سرلشکرش</p></div>
<div class="m2"><p>برجیس اندر مجمرش سوزان چو اسپند آمده</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دانش پژوه و دین طلب دانشور و دانش لقب</p></div>
<div class="m2"><p>در گلشن علم و ادب نخلی برومند آمده</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>فیروز و فرخ فال او شادان و خرم حال او</p></div>
<div class="m2"><p>بر سایه اقبال او از چرخ سوگند آمده</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>میرا ثنا خوانت منم کآیین بد را دشمنم</p></div>
<div class="m2"><p>در دام مهرت گردنم همواره در بند آمده</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>از خاک راهت شد گلم زین ره بکامت مایلم</p></div>
<div class="m2"><p>در حضرتت جان و دلم بس آرزومند آمده</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>تا نامه های بخردی شوید دل خود از بدی</p></div>
<div class="m2"><p>تا سیمناد ایزدی در زند و پا زند آمده</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>همواره باشی در جهان از بخت و دولت کامران</p></div>
<div class="m2"><p>الفاظت اندر کام جان چون شکر و قند آمده</p></div></div>