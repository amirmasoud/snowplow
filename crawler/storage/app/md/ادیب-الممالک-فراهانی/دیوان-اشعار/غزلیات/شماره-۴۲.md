---
title: >-
    شمارهٔ ۴۲
---
# شمارهٔ ۴۲

<div class="b" id="bn1"><div class="m1"><p>درین چمن که هوار و به اهتراز آورد</p></div>
<div class="m2"><p>گل شکفته از آنروی دلنواز آورد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غنیمتی است مرا زندگی که رضوان باز</p></div>
<div class="m2"><p>در بهشت بروی حبیب باز آورد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گل و شکوفه دگرگون نموده پنداری</p></div>
<div class="m2"><p>نشانی از رخ محبوب جانگداز آورد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زمین عجایب تاریخی آشکار کند</p></div>
<div class="m2"><p>جهان حقیقت هر عیش بر مجاز آورد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر از عجایب گیتی همی نویسم باز</p></div>
<div class="m2"><p>و یا مجاز نگارم چگونه باز آورد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هزار شرح و هزاران قضیه می باید</p></div>
<div class="m2"><p>یکی به بدرقه، آن یک به پیشباز آورد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بجز دو دوست زرافشان خواجه تا امروز</p></div>
<div class="m2"><p>ندیده ام که گلی رنگ مه فراز آورد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گلی است دست سپهدار اعظم سلطان</p></div>
<div class="m2"><p>که هر چه یابد گوئیش کارساز آورد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خدایگانا میرا توئی که از تحقیق</p></div>
<div class="m2"><p>فلک بر روی تو سجده، زمین نیاز آورد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>منم ستاده یکی پیر منحنی بدرت</p></div>
<div class="m2"><p>که آسمان به سخن گفتنم نماز آورد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مرا تو مردم خواندی گمان نمی کردم</p></div>
<div class="m2"><p>که مردمی منت رو باهتزار آورد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>از آنکه حقه پندار در کفم دادی</p></div>
<div class="m2"><p>شکسته دستم آیین به حقه باز آورد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نعوذبالله استغفرالله این نسبت</p></div>
<div class="m2"><p>جماعتی را ای خواجه در گداز آورد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ز من که خاک توأم دل مگیر و سخت درآی</p></div>
<div class="m2"><p>که نیکبخت نگاریت رسم ناز آورد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>خدا کند که تو باشی همیشه مثل کسی</p></div>
<div class="m2"><p>که نازنین دل او بر دو جهان جهاز آورد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چو راست گیری ابرو، جهان کنی روشن</p></div>
<div class="m2"><p>چو چین برو فکنی دل بترکتاز آورد</p></div></div>