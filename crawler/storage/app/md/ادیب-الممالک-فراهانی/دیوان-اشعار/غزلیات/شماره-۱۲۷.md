---
title: >-
    شمارهٔ ۱۲۷
---
# شمارهٔ ۱۲۷

<div class="b" id="bn1"><div class="m1"><p>سراج الهدی حاج ملاعلی</p></div>
<div class="m2"><p>که نفسی زکی بود و حبری ملی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کنوز حکم در دلش مختفی</p></div>
<div class="m2"><p>رموز هدی از لبش منجلی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دلش گنجی از ما سوی الله تهی</p></div>
<div class="m2"><p>ز اسرار عین الیقین ممتلی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>فلست اخاف من العاذلین</p></div>
<div class="m2"><p>و لم اخش مما یقولون لی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو فضلش برافروخت نارالقری</p></div>
<div class="m2"><p>خرد مقتبس بود و او مصطلی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دبستان و محراب و منبر گذاشت</p></div>
<div class="m2"><p>بفرزند فرزانه عبدالعلی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سفر کرد از این دارو در ماتمش</p></div>
<div class="m2"><p>پریشان عدو گشت و گریان ولی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>امیری به تاریخ گفتا لقد</p></div>
<div class="m2"><p>قضی نحبه الحاج ملاعلی</p></div></div>