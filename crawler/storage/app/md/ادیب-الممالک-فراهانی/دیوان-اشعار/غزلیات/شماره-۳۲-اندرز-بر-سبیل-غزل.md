---
title: >-
    شمارهٔ ۳۲ - اندرز بر سبیل غزل
---
# شمارهٔ ۳۲ - اندرز بر سبیل غزل

<div class="b" id="bn1"><div class="m1"><p>آن شنیدستم که از هومر حریفی ز اهل درد</p></div>
<div class="m2"><p>چامه ای آکنده از دشنام خود درخواست کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفت چون درخورد مدحت نیستم دشنام ده</p></div>
<div class="m2"><p>زانکه دشنامت مرا مدح است و خارت، به ز ورد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پاسخش گفتا که گر گرد از ستم خیزد بچرخ</p></div>
<div class="m2"><p>به که از نام تو بنشیند مرا برنامه گرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفت خواهم گفت اگر سرپیچی از گفتار من</p></div>
<div class="m2"><p>پیش دانایان که هومر در سخن خام است و سرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در ردیف اوستادانش نباید هشت از آنک</p></div>
<div class="m2"><p>خامه اش کند است و شعرش سست و طبعش نانورد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هومر اندر پاسخش زد داستانی بوالعجب</p></div>
<div class="m2"><p>تا حریف افتاد از آن جوش و خروش و خشم و درد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گفت در قبرس شنیدستم سگی با شیر گفت</p></div>
<div class="m2"><p>آزمون را با تو خواهم گشت لختی هم نبرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شیر گفتش من نه همزاد و هم آوردم ترا</p></div>
<div class="m2"><p>رو سگی را جوی و با پیوند خود کن دارو برد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گفت این گفتم اگر با من بناورد آمدی</p></div>
<div class="m2"><p>با سعادت دستیاری با شرافت پایمرد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ورنه گویم آشکارا در صف درندگان</p></div>
<div class="m2"><p>این منم کز بیم چنگم شیر را شد چهره زرد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کوفتم با شیر کوس جنگ و از پیکار من</p></div>
<div class="m2"><p>شیر خائف شد که چون من نیست در ناورد فرد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>شیر گفت ارزانکه شیرانم جبان خوانند به</p></div>
<div class="m2"><p>زانکه با خون سگم باید دهان آلوده کرد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>با قرین خویش یازد هر کسی شمشیر وگرز</p></div>
<div class="m2"><p>با حریف خویشتن بازد هر کسی شطرنج و نرد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>هرکه جز با کفو خود در جنگ همناورد گشت</p></div>
<div class="m2"><p>سند روسی شد رخش از دور چرخ لاجورد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>شیر نر را شیر نر کفو است و سگ را سگ قرین</p></div>
<div class="m2"><p>دستیار زن زن آمد پایمرد مرد مرد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>هرکه نز جنس تو زو پیوند صحبت درگسل</p></div>
<div class="m2"><p>آنکه نی کفو تو زو طومار عشرت در نورد</p></div></div>