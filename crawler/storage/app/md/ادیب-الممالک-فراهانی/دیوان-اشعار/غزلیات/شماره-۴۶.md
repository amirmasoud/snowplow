---
title: >-
    شمارهٔ ۴۶
---
# شمارهٔ ۴۶

<div class="b" id="bn1"><div class="m1"><p>باد نوروزی به بستان مشک و کافور آورد</p></div>
<div class="m2"><p>ابر فروردین نثار از در منثور آورد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چهره گل آب و رنگ از روی غلمان میبرد</p></div>
<div class="m2"><p>طره سنبل شکن بر گیسوی حور آورد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آن یکی یاقوت رخشان از بدخشان یافته</p></div>
<div class="m2"><p>آن یکی فیروزه از کان نشابور آورد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نرگس اندر باغ دارد کاسه زرین بکف</p></div>
<div class="m2"><p>جام جم گوئی به شادروان شاپور آورد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>باد اگر پیراهن یوسف ندارد نکهتش</p></div>
<div class="m2"><p>چشم نرگس را چرا یعقوب سان نور آورد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون بجنبد شاخ گل بر سبزه باغ بهار</p></div>
<div class="m2"><p>گوئی اندر تخت خاقان تاج فغفور آورد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بط درون شط بسان کشتی نوح است لیک</p></div>
<div class="m2"><p>غرش فواره یاد از فار تنور آورد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بلبل گویا فراز شاخ گل دستان سرای</p></div>
<div class="m2"><p>داستانها با سرود نای و طنبور آورد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>احسن الملکست پنداریکه از شعر ادیب</p></div>
<div class="m2"><p>تحفه اندر درگه فرخنده دستور آورد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آن خداوندی که بوی خوی روح افزای او</p></div>
<div class="m2"><p>مستی اندر مغز، همچون آب انگور آورد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>صاحبا میرا منم استاده در این آستان</p></div>
<div class="m2"><p>خادمت را منتظر تا خود چه دستور آورد</p></div></div>