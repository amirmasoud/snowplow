---
title: >-
    شمارهٔ ۱۰۶ - خطاب بمیرزا حیدر علی کمالی اصفهانی
---
# شمارهٔ ۱۰۶ - خطاب بمیرزا حیدر علی کمالی اصفهانی

<div class="b" id="bn1"><div class="m1"><p>ابوالکمال کمالی خدایگان سخن</p></div>
<div class="m2"><p>به پیکر قلمت جای جان کرده سخن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگر نه کلک تو طرح سخن درافکندی</p></div>
<div class="m2"><p>بر اوفتادی ازین مملکت نشان سخن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>توئی که کلک تو همواره ارمغان آرد</p></div>
<div class="m2"><p>طبق طبق گل سوری ببوستان سخن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو خامه در پی مدحت بنامه پویه کند</p></div>
<div class="m2"><p>کجا گرفت تواند کسی عنان سخن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بگاه ذکر تو اندر مشام خلق رسد</p></div>
<div class="m2"><p>شمیم مشک تتار از گلابدان سخن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو خواستی ز رهی قصه قرامطه را</p></div>
<div class="m2"><p>چو آفتاب شدم سوی آسمان سخن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>منم که بر صفت مرد آسمان پیما</p></div>
<div class="m2"><p>روم بعرش معارف زنردبان سخن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز من بگیر و مدون کن این صحیفه نو</p></div>
<div class="m2"><p>که قادری و ادیبی و قدردان سخن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>حدیث فتنه صد ساله را درافکندم</p></div>
<div class="m2"><p>چو نقش گوهر و مرجان به پرنیان سخن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هزار نقش زموج پرند ساده کنم</p></div>
<div class="m2"><p>پدید بر رخ سیمین پرنیان سخن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز یوسف بن ابی الساج و جیش بوطاهر</p></div>
<div class="m2"><p>دراز گشت درین وقعه داستان سخن</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>کنون بحضرتت این چامه را فرستادم</p></div>
<div class="m2"><p>که دوخت سوزن کلکم بریسمان سخن</p></div></div>