---
title: >-
    شمارهٔ ۱۲۰
---
# شمارهٔ ۱۲۰

<div class="b" id="bn1"><div class="m1"><p>مخور جانا فریب از گنج گیتی</p></div>
<div class="m2"><p>مشو اندوهگین از رنج گیتی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پیاده پیل گردد شاه ماتت</p></div>
<div class="m2"><p>همی در بازی شطرنج گیتی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همه دانشوران مستند و شیدا</p></div>
<div class="m2"><p>ز سحر و جادو و نیرنج گیتی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دل و چشم حکیمان خیره ماند</p></div>
<div class="m2"><p>ز افسون و دلال و غنج گیتی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چنان کاین مغزها را تیره دارد</p></div>
<div class="m2"><p>می و افیون و بذر البنج گیتی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نماند تا تو بشماری نفس را</p></div>
<div class="m2"><p>شبانه وز چهار و پنج گیتی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>امیری دوش کرد این نکته مشهود</p></div>
<div class="m2"><p>از آن دانای حکمت سنج گیتی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>که گیتی را نیاید رنج چونان</p></div>
<div class="m2"><p>که دیدی می نیاید گنج گیتی</p></div></div>