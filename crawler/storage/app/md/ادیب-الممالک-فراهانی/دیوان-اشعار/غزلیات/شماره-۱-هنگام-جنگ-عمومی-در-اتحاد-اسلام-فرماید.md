---
title: >-
    شمارهٔ ۱ - هنگام جنگ عمومی در اتحاد اسلام فرماید
---
# شمارهٔ ۱ - هنگام جنگ عمومی در اتحاد اسلام فرماید

<div class="b" id="bn1"><div class="m1"><p>علی نمود مصفا جمال علم یقین را</p></div>
<div class="m2"><p>فکند پرده ز رخسار ناز شاهد دین را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>علی ز تیغ شرربار و منطق گهر آگین</p></div>
<div class="m2"><p>گسست عروه کفر و ببست حبل متین را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نمود نصرت پیشینیان ز غیب ولیکن</p></div>
<div class="m2"><p>رفیق شد بعلن پیشوای باز پسین را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر نه ساقی کوثر علی شدی نچشیدی</p></div>
<div class="m2"><p>حسینش از دم شمشیر خصم مآء معین را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تبارک الله ازان شه که داد در ره یزدان</p></div>
<div class="m2"><p>نگین و تاج و سر و پیکر و بنات و بنین را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کسی که لعل لبش نایب نگین سلیمان</p></div>
<div class="m2"><p>علی اکبرش از تشنگی مکید نگین را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو خورد سنگ عدو بر جبین روشن پاکش</p></div>
<div class="m2"><p>برای شکر وسپاسش بخاک سود جبین را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>درین زمانه جزآن شه که را شناختی ایدون</p></div>
<div class="m2"><p>که نام فرخش آراسته شهور و سنین را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>برای قوت دین شد که دید حصن ولایت</p></div>
<div class="m2"><p>سنان و تیغ سنان را و زخم تیر حصین را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تو نیز جان برادر بگیر دامن این دین</p></div>
<div class="m2"><p>اگر مصدقی از راستی رسول امین را</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بزرگوار خدایا بجاه احمد و آلش</p></div>
<div class="m2"><p>مکن خموش در ایران ما چراغ یقین را</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ز اتحاد برافروز شمع مجلس یاران</p></div>
<div class="m2"><p>کزین چراغ بود روشنی سپهر و زمین را</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ایا برادر دینی رسیده وقت که ما هم</p></div>
<div class="m2"><p>دهیم از سر اخلاص دست عهد و یمین را</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>تو باش عاصم ناموس مسلمین و یقین کن</p></div>
<div class="m2"><p>که کردگار جهان عاصم است دین و مبین را</p></div></div>