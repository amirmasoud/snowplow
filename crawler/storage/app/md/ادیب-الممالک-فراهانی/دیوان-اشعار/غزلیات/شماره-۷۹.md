---
title: >-
    شمارهٔ ۷۹
---
# شمارهٔ ۷۹

<div class="b" id="bn1"><div class="m1"><p>ای که گفتی ملک درویشی نه بی لشکر گرفتم</p></div>
<div class="m2"><p>با سپاه اشک و فوج آه این کشور گرفتم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همت مردان راه حق ازین صد ره فزون شد</p></div>
<div class="m2"><p>هر چه گوئی بیش از این همتت باور گرفتم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>لیک سخت اندر شگفتم زآنکه گفتی از نکویان</p></div>
<div class="m2"><p>ساعتی دلبر گرفتم ساعتی دل بر گرفتم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از کنار خوبرویان سوی بدنامی نرفتم</p></div>
<div class="m2"><p>وز درخت نیکنامی تخم کشتم برگرفتم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بوسه را اقرار داری وز کنار انکار داری</p></div>
<div class="m2"><p>یا چنان اقرار انگاری چنین منکر گرفتم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون حکیمان جهان گفتند کار، از کار خیزد</p></div>
<div class="m2"><p>ایندو را من لازم و ملزوم همدیگر گرفتم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بوسه مفتاح کنار آمد کنار از وی نشاید</p></div>
<div class="m2"><p>کاین کنار از جویبار خلد من خوشتر گرفتم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر نمی جستی کنار ایدر چرا برگرد بوسه</p></div>
<div class="m2"><p>گشته ای من عقل را شاهد بر این محضر گرفتم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>عقل گوید چون زمام نفس در دست دل آمد</p></div>
<div class="m2"><p>قلب را مشرک شمردم نفس را کافر گرفتم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>جز که فرمائی بعون حق زمام نفس مشرک</p></div>
<div class="m2"><p>از کف دل با کمند همت حیدر گرفتم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>قل هوالله را بشیطان هیولا بردمیدم</p></div>
<div class="m2"><p>آیت سبح المثانی زال پیغمبر گرفتم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هر کجا منصور بودم عقل را یاور شمردم</p></div>
<div class="m2"><p>هر زمان مغلوب گشتم شرع را داور گرفتم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>رهبرم جوع و سهر بودند در سرآء و ضرآء</p></div>
<div class="m2"><p>ایندو تن را در بیابان طلب رهبر گرفتم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بردم از ظلمات کثرت پی بآب خضر وحدت</p></div>
<div class="m2"><p>خاتم از دست سلیمان تاج از اسکندر گرفتم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گاه از سفره شهود اندر غذای روح خوردم</p></div>
<div class="m2"><p>گاه از کوزه وجود اندر می احمر گرفتم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>جرعه حیوان ننوشم از کف خضر پیمبر</p></div>
<div class="m2"><p>چون ز دست ساقی کوثر می کوثر گرفتم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>با ولای چارده تن ز اولیا هفتاد نوبت</p></div>
<div class="m2"><p>هفت گردون سودم و آهو ز هفت اختر گرفتم</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>در جوانی مادر رز را بخاک تیره کردم</p></div>
<div class="m2"><p>چون زمان پیری آمد پیش از او دختر گرفتم</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>دادم از کف طره سیمین بران و اندر پی آن</p></div>
<div class="m2"><p>اشک چون سیماب جاری بر رخ چون زر گرفتم</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>سبزهای این چمن کمتر ز خضرآء الدمن شد</p></div>
<div class="m2"><p>لاله زار خاکیان را تل خاکستر گرفتم</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>کی ز خضرآء الدمن روشن شود چشمم که اکنون</p></div>
<div class="m2"><p>بالش از خورشید و فرش از طارم اخضر گرفتم</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>جلوه دیدار اندر خلوت اسرار دیدم</p></div>
<div class="m2"><p>مرگ را پیش از زمان نیستی زیور گرفتم</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>سال و ماهم جملگی اردیبهشت و فروردین شد</p></div>
<div class="m2"><p>کی به دل اندیشه از مرداد و شهریور گرفتم</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چشمت ای پروانه مات جلوه شمع هدی شد</p></div>
<div class="m2"><p>عنقریب از آتش غیرت تراپی پر گرفتم</p></div></div>