---
title: >-
    شمارهٔ ۱۵
---
# شمارهٔ ۱۵

<div class="b" id="bn1"><div class="m1"><p>تقدیم دوست کردم قرقاول محبت</p></div>
<div class="m2"><p>کز وی شنید مغزم بوی گل محبت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای خرم آنزمانی کاندر حضور آن شه</p></div>
<div class="m2"><p>جوشد صراحی دل از غلغل محبت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پای نشاط کوبم اندر بساط رفعت</p></div>
<div class="m2"><p>دست امید یازم در کاکل محبت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دهقان خمیر ما را از گندمی سرشته است</p></div>
<div class="m2"><p>کاندر بهشت روئید از سنبل محبت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از رود غصه ما را نتوان عبور کردن</p></div>
<div class="m2"><p>جز با سفینه عشق یا از پل محبت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اندر مقام محمود مستانه شد امیری</p></div>
<div class="m2"><p>در نغمه و ترنم چون بلبل محبت</p></div></div>