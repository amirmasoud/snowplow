---
title: >-
    شمارهٔ ۴۳ - موشح بنام بدرالدوله سلطان بیگم
---
# شمارهٔ ۴۳ - موشح بنام بدرالدوله سلطان بیگم

<div class="b" id="bn1"><div class="m1"><p>بوسه شیرین اگر زان لعلم ارزانی شود</p></div>
<div class="m2"><p>دل رها از درد و تن دور از گرانجانی شود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روزی آید کان پری با من نشیند روبرو</p></div>
<div class="m2"><p>از وصالش مشگلم مایل به آسانی شود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>لعل شیرینش ببوسم چون شکر تا بامداد</p></div>
<div class="m2"><p>دامنم از بوسه پر یاقوت رمانی شود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>وقت صحبت آنچنان مستش کنم کاندر نشاط</p></div>
<div class="m2"><p>لاله اش همرنگ می از راح ریحانی شود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر کسی چیزی نثار دوست سازد لیک من</p></div>
<div class="m2"><p>سر فشانم تا تنم بر دوست قربانی شود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>لاابالی وار در کویش زنم لبیک شوق</p></div>
<div class="m2"><p>طوف در کاهش دلم را کعبه ثانی شود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از خدا خواهم شبی آن ماه را گیرم ببر</p></div>
<div class="m2"><p>نقد دیدارش مرا گنج سلیمانی شود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بوسه از رویش ستانم چنگ در مویش زنم</p></div>
<div class="m2"><p>یا بمیرم تا وجودم در رهش فانی شود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر ز هر مصرع بگیری حرف اول نام آن</p></div>
<div class="m2"><p>ماه تابان آشکار از نور یزدانی شود</p></div></div>