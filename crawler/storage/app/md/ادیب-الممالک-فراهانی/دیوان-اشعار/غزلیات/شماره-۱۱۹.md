---
title: >-
    شمارهٔ ۱۱۹
---
# شمارهٔ ۱۱۹

<div class="b" id="bn1"><div class="m1"><p>ای ملک از همتت شد سبز چون بستان گیتی</p></div>
<div class="m2"><p>شادمان زی کز تو شد آباد شارستان گیتی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گشت محکم با اساس فکرتت بنیاد عالم</p></div>
<div class="m2"><p>ماند ستوار از پای همتت بنیان گیتی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گرنه عزمت سد شادی گرد عالم راست کردی</p></div>
<div class="m2"><p>سیل غم افکنده بودی رخنه در ارکان گیتی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>راست گویم بی تو گیتی قالبی بیروح باشد</p></div>
<div class="m2"><p>زانکه گیتی چون تنستی و تو هستی جان گیتی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جشن میلاد حسین ابن علی را تازه کردی</p></div>
<div class="m2"><p>لوحش الله گوی سبقت بردی از میدان گیتی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>طاعت آوردی در اینره تا جهانی شد مطیعت</p></div>
<div class="m2"><p>بندگی کردی در ایندر تا شدی سلطان گیتی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>با تو پیمان جهان محکم شد اکنون گر چه هرگز</p></div>
<div class="m2"><p>تاکنون با هیچکس محکم نشد پیمان گیتی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اقتدار و مردمی این بس که طبعت آشکارا</p></div>
<div class="m2"><p>شد کفیل دور گردون ضامن تاوان گیتی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ای مظفر شاه شاه ثانی ناصرالدین شاه سوم</p></div>
<div class="m2"><p>ای محمد شاه چارم پنجمین خاقان گیتی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>راست گویم کاین نظام السلطنه در پیشگاهت</p></div>
<div class="m2"><p>تالی بوذرجمهر است ای انوشروان گیتی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تا نخشکد شاخ فضلت در زمستان حوادث</p></div>
<div class="m2"><p>تا نخوشد گلبن جودت به تابستان گیتی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>آنقدر سرسبز بادا کشتزار عدل و دادت</p></div>
<div class="m2"><p>کش نیارد بدرویدن تا ابد دهقان گیتی</p></div></div>