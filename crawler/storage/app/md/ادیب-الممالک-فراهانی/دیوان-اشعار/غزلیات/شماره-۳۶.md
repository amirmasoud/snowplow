---
title: >-
    شمارهٔ ۳۶
---
# شمارهٔ ۳۶

<div class="b" id="bn1"><div class="m1"><p>بپای آل علی هر که روی زاری سود</p></div>
<div class="m2"><p>ز دستبرد حوادث در این جهان آسود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بگیر دامن احفاد مرتضی کاین قوم</p></div>
<div class="m2"><p>ز آفریده فرازند و از خدای فرود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سرشته در کف ایشان بقدرت ازلی</p></div>
<div class="m2"><p>خمیر هستی و گل مهره سپهر کبود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بباز در رهشان جان و عمر باقی گیر</p></div>
<div class="m2"><p>که این معامله یک بر هزار بخشد سود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بکار در دل خود تخم مهرشان شب و روز</p></div>
<div class="m2"><p>عزیز من که کسی غیر کشت خود ندرود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بروی فرخشان باد جاودان جاوید</p></div>
<div class="m2"><p>ز ما سلام و تحیت ز کردگار درود</p></div></div>