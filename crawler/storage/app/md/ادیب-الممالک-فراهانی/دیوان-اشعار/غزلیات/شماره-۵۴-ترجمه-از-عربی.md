---
title: >-
    شمارهٔ ۵۴ - ترجمه از عربی
---
# شمارهٔ ۵۴ - ترجمه از عربی

<div class="b" id="bn1"><div class="m1"><p>بار خدایا توئی که باطن اسرار</p></div>
<div class="m2"><p>دانی در روز روشن و به شب تار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خسته ز درک مشیتت همه افهام</p></div>
<div class="m2"><p>خیره ز تحقیق حکمتت همه ابصار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مردم بدبخت را قضای تو سازد</p></div>
<div class="m2"><p>در همه گیتی نژند و خوار و نگونسار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>باز شود نیک بخت را ز قضایت</p></div>
<div class="m2"><p>دولت دنیا قرین و بخت جوان یار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یک قدم ای ناظر جریده درین ره</p></div>
<div class="m2"><p>با من مسکین ز روی بینش بردار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا نگری هر دمی هزار شگفتی</p></div>
<div class="m2"><p>بر رخ خاک از قضای ایزد دادار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خیره ز اغلوطه های کیوان بینی</p></div>
<div class="m2"><p>خاطر پژمرده اکارم و اخیار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>وه چه فرومایگان سفله که دیدیم</p></div>
<div class="m2"><p>نزد لئیمان عزیز گشته ز دینار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نیز بدیدیم مردم هنری را</p></div>
<div class="m2"><p>بر در نامردمان ز بی درمی خوار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>یا رب ازین غم دلم فتاده چو یونس</p></div>
<div class="m2"><p>در دل ماهی درون لجه رخسار</p></div></div>