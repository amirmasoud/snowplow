---
title: >-
    شمارهٔ ۶۷
---
# شمارهٔ ۶۷

<div class="b" id="bn1"><div class="m1"><p>آن چای که داد همت والایت</p></div>
<div class="m2"><p>بر یار خود از مهر فلک فرسایت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از مهر تو دم زدیم و دم کردیمش</p></div>
<div class="m2"><p>در خوردن چای بود خالی جایت</p></div></div>