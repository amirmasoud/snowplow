---
title: >-
    شمارهٔ ۳۶
---
# شمارهٔ ۳۶

<div class="b" id="bn1"><div class="m1"><p>ای یار عزیز و عاشق قدرشناس</p></div>
<div class="m2"><p>سوی تو روانه کردم این دسته یاس</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از سیرت ناس بر حذر باش و بخوان</p></div>
<div class="m2"><p>هنگام فراق دوستان سوره ناس</p></div></div>