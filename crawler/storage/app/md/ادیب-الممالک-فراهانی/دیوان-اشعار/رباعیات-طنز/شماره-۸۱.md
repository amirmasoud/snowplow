---
title: >-
    شمارهٔ ۸۱
---
# شمارهٔ ۸۱

<div class="b" id="bn1"><div class="m1"><p>ای آنکه بر آسمان خوبی ماهی</p></div>
<div class="m2"><p>دادی ز برای دوستانت ماهی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در دیده چو دید عکس ساقت دل من</p></div>
<div class="m2"><p>حیران شد و گفت از تعجب ماهی</p></div></div>