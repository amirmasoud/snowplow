---
title: >-
    شمارهٔ ۲۰
---
# شمارهٔ ۲۰

<div class="b" id="bn1"><div class="m1"><p>زین تازه قبا که دست رنج مه ماست</p></div>
<div class="m2"><p>پیراهن دشمن به تن از غصه قباست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گیریم به چابکی و خوبی او را</p></div>
<div class="m2"><p>مانند قبای صحت اندر تن راست</p></div></div>