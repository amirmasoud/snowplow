---
title: >-
    شمارهٔ ۵۲ - سیب
---
# شمارهٔ ۵۲ - سیب

<div class="b" id="bn1"><div class="m1"><p>ای قد تو در گلشن جان نخل امید</p></div>
<div class="m2"><p>خطت چو بنفشه ای که در باغ دمید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دادم بحضور تو به صد روسیهی</p></div>
<div class="m2"><p>سیبی که چو رخسار تو سرخ است و سفید</p></div></div>