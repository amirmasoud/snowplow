---
title: >-
    شمارهٔ ۵۴ - انگور
---
# شمارهٔ ۵۴ - انگور

<div class="b" id="bn1"><div class="m1"><p>ای دوست ز رخ بدیدگان نورم ده</p></div>
<div class="m2"><p>وز غبغب خود شربت کافورم ده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من سوی تو انگور فرستادم و تو</p></div>
<div class="m2"><p>از جام لبت باده انگورم ده</p></div></div>