---
title: >-
    شمارهٔ ۲۴
---
# شمارهٔ ۲۴

<div class="b" id="bn1"><div class="m1"><p>خصم تو براه خیر هرگز نرود</p></div>
<div class="m2"><p>از کعبه کسی به دیر هرگز نرود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من کفش تو را به پای کردم اما</p></div>
<div class="m2"><p>در کفش تو پای غیر هرگز نرود</p></div></div>