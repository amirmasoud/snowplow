---
title: >-
    شمارهٔ ۴
---
# شمارهٔ ۴

<div class="b" id="bn1"><div class="m1"><p>انگشتر التفاتی ایدوست رسید</p></div>
<div class="m2"><p>ایزد نکند از تو مرا قطع امید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>انگشت رضا به چشم و جانم بنهاد</p></div>
<div class="m2"><p>در حلقه بندگی شدم چون خورشید</p></div></div>