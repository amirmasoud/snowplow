---
title: >-
    شمارهٔ ۵ - گوشواره
---
# شمارهٔ ۵ - گوشواره

<div class="b" id="bn1"><div class="m1"><p>زر در رهت از چهره نثار آوردم</p></div>
<div class="m2"><p>گوهر ز دو چشم اشکبار آوردم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر باد صبا رساند اندر گوشت</p></div>
<div class="m2"><p>از آه شبانه گوشوار آوردم</p></div></div>