---
title: >-
    شمارهٔ ۶۴ - قند
---
# شمارهٔ ۶۴ - قند

<div class="b" id="bn1"><div class="m1"><p>ای آنکه تو را همیشه در بر طلبم</p></div>
<div class="m2"><p>وز لعل لبت هماره شکر طلبم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من قند مکرر آورم پیش لبت</p></div>
<div class="m2"><p>وز لعل تو دشنام مکرر طلبم</p></div></div>