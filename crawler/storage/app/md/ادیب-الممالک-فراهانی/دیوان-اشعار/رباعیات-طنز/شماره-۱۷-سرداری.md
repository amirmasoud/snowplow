---
title: >-
    شمارهٔ ۱۷ - سرداری
---
# شمارهٔ ۱۷ - سرداری

<div class="b" id="bn1"><div class="m1"><p>ای آنکه باقلیم وفا سرداری</p></div>
<div class="m2"><p>همواره خمار عشق در سر داری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر چند که شرط عاشقی پاداریست</p></div>
<div class="m2"><p>از بهر تو دوختم من این سرداری</p></div></div>