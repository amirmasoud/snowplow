---
title: >-
    شمارهٔ ۶۲ - عرق
---
# شمارهٔ ۶۲ - عرق

<div class="b" id="bn1"><div class="m1"><p>ای گشته گل از رشک جمال تو ورق</p></div>
<div class="m2"><p>وز شرم لبت شراب کرده است عرق</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قدری عرق از بهر نثار لب تو</p></div>
<div class="m2"><p>چون گوهر اخلاص نهادم به طبق</p></div></div>