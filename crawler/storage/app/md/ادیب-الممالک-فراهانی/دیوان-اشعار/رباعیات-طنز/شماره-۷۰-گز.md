---
title: >-
    شمارهٔ ۷۰ - گز
---
# شمارهٔ ۷۰ - گز

<div class="b" id="bn1"><div class="m1"><p>گز در بر آن جان جهان تحفه برم</p></div>
<div class="m2"><p>یعنی که اگر شبی نیائی ببرم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من وصلت را به عالمی نفروشم</p></div>
<div class="m2"><p>هجرانت را گزی بگوزی نخرم</p></div></div>