---
title: >-
    شمارهٔ ۷۷
---
# شمارهٔ ۷۷

<div class="b" id="bn1"><div class="m1"><p>ای شیر رمیده ز آهوان مستت</p></div>
<div class="m2"><p>تیری که زدی بران شکار از دستت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر تیر نگاه بد فدای نگهت</p></div>
<div class="m2"><p>ور تیر خدنگ بد بنازم شستت</p></div></div>