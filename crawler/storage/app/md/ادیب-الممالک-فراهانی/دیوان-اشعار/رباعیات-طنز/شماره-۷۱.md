---
title: >-
    شمارهٔ ۷۱
---
# شمارهٔ ۷۱

<div class="b" id="bn1"><div class="m1"><p>ای مست لب چون شکرت دختر رز</p></div>
<div class="m2"><p>در عرصه عشق کرده آهنگ رجز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آنکس که شبی یک گره و بهر نخورد</p></div>
<div class="m2"><p>هرگز نکند تاب فرو بردن گز</p></div></div>