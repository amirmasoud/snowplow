---
title: >-
    شمارهٔ ۸۳
---
# شمارهٔ ۸۳

<div class="b" id="bn1"><div class="m1"><p>لا حول و لا قوة الا بالله</p></div>
<div class="m2"><p>چاقو داده است بر من آن غیرت ماه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دست ستم و زبان بدگویان را</p></div>
<div class="m2"><p>با این چاقو ببرم انشاء الله</p></div></div>