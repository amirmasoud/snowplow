---
title: >-
    شمارهٔ ۳۳ - بید مشک
---
# شمارهٔ ۳۳ - بید مشک

<div class="b" id="bn1"><div class="m1"><p>بر دل ز غم رقیب رشکی دارم</p></div>
<div class="m2"><p>وز دیده روان سیل سرشکی دارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>لرزم ز فراق زلف مشکینت چو بید</p></div>
<div class="m2"><p>زین است که تحفه بیدمشکی دارم</p></div></div>