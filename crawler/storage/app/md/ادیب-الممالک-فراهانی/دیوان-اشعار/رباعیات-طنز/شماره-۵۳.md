---
title: >-
    شمارهٔ ۵۳
---
# شمارهٔ ۵۳

<div class="b" id="bn1"><div class="m1"><p>ای آنکه ترا پنجه شیری باشد</p></div>
<div class="m2"><p>در جنگ غمت ساز دلیری باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سیب تو قبول کردم اما ترسم</p></div>
<div class="m2"><p>این راست شود که سیب سیری باشد</p></div></div>