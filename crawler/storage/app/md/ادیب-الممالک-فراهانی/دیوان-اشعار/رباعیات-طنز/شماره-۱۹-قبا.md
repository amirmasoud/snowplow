---
title: >-
    شمارهٔ ۱۹ - قبا
---
# شمارهٔ ۱۹ - قبا

<div class="b" id="bn1"><div class="m1"><p>ای گشته قبای حسن بر قد تو راست</p></div>
<div class="m2"><p>قدت سروی که گلشن جان آراست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر بر تنت این قبا به پوشی نه عجب</p></div>
<div class="m2"><p>سروی و ز برک بر تن سرو قباست</p></div></div>