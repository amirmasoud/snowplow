---
title: >-
    شمارهٔ ۷۵
---
# شمارهٔ ۷۵

<div class="b" id="bn1"><div class="m1"><p>آن قرص که داده بودی ای کبک خرام</p></div>
<div class="m2"><p>بردم به زبان همچو شکر بر بادام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون طعم لب تو اندر او دانستم</p></div>
<div class="m2"><p>آنقدر مکیدمش که شد آب تمام</p></div></div>