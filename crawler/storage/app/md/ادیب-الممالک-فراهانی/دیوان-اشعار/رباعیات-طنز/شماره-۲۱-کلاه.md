---
title: >-
    شمارهٔ ۲۱ - کلاه
---
# شمارهٔ ۲۱ - کلاه

<div class="b" id="bn1"><div class="m1"><p>ای آنکه باوج حسن تابنده مهی</p></div>
<div class="m2"><p>باروی سفید و گیسوان سیهی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بستان ز من این کلاه و بر سر بگذار</p></div>
<div class="m2"><p>تا خلق بدانند که صاحب کلهی</p></div></div>