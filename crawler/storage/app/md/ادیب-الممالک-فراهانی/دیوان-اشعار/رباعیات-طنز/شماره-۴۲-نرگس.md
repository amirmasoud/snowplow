---
title: >-
    شمارهٔ ۴۲ - نرگس
---
# شمارهٔ ۴۲ - نرگس

<div class="b" id="bn1"><div class="m1"><p>در باغ سحر نرگس تر دیده گشود</p></div>
<div class="m2"><p>با چشم تواش بنای هم چشمی بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من نیز ز خشم دیده اش برکندم</p></div>
<div class="m2"><p>بستان و بزیر پای خود افکن زود</p></div></div>