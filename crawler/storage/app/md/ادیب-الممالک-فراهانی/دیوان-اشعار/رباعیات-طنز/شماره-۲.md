---
title: >-
    شمارهٔ ۲
---
# شمارهٔ ۲

<div class="b" id="bn1"><div class="m1"><p>ای آنکه خوش است در فراقت مردن</p></div>
<div class="m2"><p>در هجر تو چاره نیست جز غم خوردن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن رشته حمایل تو را افکندم</p></div>
<div class="m2"><p>چون رشته مهربانیت در گردن</p></div></div>