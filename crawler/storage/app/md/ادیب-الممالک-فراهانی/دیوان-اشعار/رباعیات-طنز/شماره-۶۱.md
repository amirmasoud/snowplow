---
title: >-
    شمارهٔ ۶۱
---
# شمارهٔ ۶۱

<div class="b" id="bn1"><div class="m1"><p>زین باده فرستادمت ای رشک پری</p></div>
<div class="m2"><p>من باخبرم بسی که که تو بی خبری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون نرگس مست نیستم تا به خمار</p></div>
<div class="m2"><p>بوئی و چو پژمرده شدم درگذری</p></div></div>