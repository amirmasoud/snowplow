---
title: >-
    شمارهٔ ۶۳
---
# شمارهٔ ۶۳

<div class="b" id="bn1"><div class="m1"><p>ای لاله بخون ز چهر رنگین شما</p></div>
<div class="m2"><p>آمد عرق از لطف جهان بین شما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رندانه لب پیاله را بوسیدیم</p></div>
<div class="m2"><p>گفتیم بیاد لب شیرین شما</p></div></div>