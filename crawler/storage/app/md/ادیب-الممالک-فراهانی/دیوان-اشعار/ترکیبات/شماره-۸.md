---
title: >-
    شمارهٔ ۸
---
# شمارهٔ ۸

<div class="b" id="bn1"><div class="m1"><p>ای بتاراج عقل و دین چالاک</p></div>
<div class="m2"><p>وی بتعذیب جان و دل بی باک</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مایه جور و فتنه و بیداد</p></div>
<div class="m2"><p>آفت عقل و دانش و ادراک</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جگر نیستی ز هجرت خون</p></div>
<div class="m2"><p>بر سر هستی از فراقت خاک</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عقل گوید ترا سقاک الله</p></div>
<div class="m2"><p>فضل گوید ترا جعلت فداک</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر اسیر آوری به بند ستم</p></div>
<div class="m2"><p>یا شکار افکنی بخاک هلاک</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>این نخواهد رهائی از زنجیر</p></div>
<div class="m2"><p>وان نجوید جدائی از فتراک</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بصفا همچو جام جمشیدی</p></div>
<div class="m2"><p>بجفا همچو افعی ضحاک</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در حریمت ز نور مستوری</p></div>
<div class="m2"><p>رفته مستی ز یاد دختر تاک</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ای دل عافیت ز عشق تو ریش</p></div>
<div class="m2"><p>دامن زندگی ز دست تو چاک</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شاد و سرسبز و تازه باش که هست</p></div>
<div class="m2"><p>جایگاه تو این دل غمناک</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چند با خون ما بیالائی</p></div>
<div class="m2"><p>پنجه نازنین و دامن پاک</p></div></div>
<div class="b2" id="bn12"><p>آخر این خانه را خدائی هست</p>
<p>واندرین خانه پادشائی هست</p></div>
<div class="b" id="bn13"><div class="m1"><p>گر ز آشوب فتنه و بیداد</p></div>
<div class="m2"><p>خون ما ریختی حلالت باد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ما به رنج زمانه خو کردیم</p></div>
<div class="m2"><p>گر تو را دل خوشست و خاطر شاد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>وصل شیرین نصیب پرویز است</p></div>
<div class="m2"><p>تیشه بر سنگ میزند فرهاد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ای به تقدیم هر هنر ماهر</p></div>
<div class="m2"><p>وی به تعلیم هر فنی استاد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چاکران تو بهمن و دارا</p></div>
<div class="m2"><p>بندگان تو کیقباد و قباد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گلبن همت از کمال تو رست</p></div>
<div class="m2"><p>کودک حکمت از بیان تو زاد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>غیر یاد تو هرفسانه چو خواب</p></div>
<div class="m2"><p>غیر ذکر تو هر ترانه چو باد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>پنجه با آسمان اگر تابی</p></div>
<div class="m2"><p>آسمان را برآری از بنیاد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>شرح سوز درون سوخته را</p></div>
<div class="m2"><p>بشنو از بنده هرچه بادا باد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>یاد کن هر دلی که نزدودست</p></div>
<div class="m2"><p>مهرت از سینه و غمت از یاد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بنده چندیست کز طریق وفا</p></div>
<div class="m2"><p>سر طاعت بدرگه تو نهاد</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>گشت دوشیزگان فکرش را</p></div>
<div class="m2"><p>نوجوانان مدحتت داماد</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>خواست کز بندگی در این حضرت</p></div>
<div class="m2"><p>باشد از بند آسمان آزاد</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>خط آزادیش بده زیراک</p></div>
<div class="m2"><p>سرنوشتش به بندگی افتاد</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>گر برانی برآیدش ناله</p></div>
<div class="m2"><p>ور کشی بر نیایدش فریاد</p></div></div>
<div class="b2" id="bn28"><p>یا از آن بندگان خاصش کن</p>
<p>یا ز بند ستم خلاصش کن</p></div>
<div class="b" id="bn29"><div class="m1"><p>ایدریغا که سینه محرم نیست</p></div>
<div class="m2"><p>کس در آفاق یار و همدم نیست</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>هیچ انفال بی برائهنشد</p></div>
<div class="m2"><p>هیچ ذی الحجه بی محرم نیست</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>جانم از دست غصه فارغ نی</p></div>
<div class="m2"><p>دلم از عیش دهر خرم نیست</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>دلم از اشک دیده ویران گشت</p></div>
<div class="m2"><p>خانه مور جای شبنم نیست</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>دست دیوان بریده باد ز ملک</p></div>
<div class="m2"><p>که سرافراز خاتم جم نیست</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>جز سلیمان که آصفش بر در</p></div>
<div class="m2"><p>کس نگهبان اسم اعظم نیست</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>غیر روح القدس کسی بر ملک</p></div>
<div class="m2"><p>محرم آستین مریم نیست</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>باغبان بهشت دهقان نی</p></div>
<div class="m2"><p>نردبان سپهر سلم نیست</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>گرچه دانم که قدر این مسکین</p></div>
<div class="m2"><p>در ترازوی همتت کم نیست</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>گر نجویم ترا هلاک شوم</p></div>
<div class="m2"><p>ور نخواهی مرا ترا غم نیست</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>سر تسلیم پیشت آوردم</p></div>
<div class="m2"><p>که بجز خدمتت مسلم نیست</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>در دو عالم نشان آن جویم</p></div>
<div class="m2"><p>که نظیرش در این دو عالم نیست</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>هرکه را شور عشق نیست بدل</p></div>
<div class="m2"><p>جانور خانمش که آدم نیست</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>اندرین آستان سری دارم</p></div>
<div class="m2"><p>که حریفش کمند رستم نیست</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>بسپردیم سر که این گردن</p></div>
<div class="m2"><p>جز بدرگاه طاعتت خم نیست</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>ریسمان قضا و رشته عقل</p></div>
<div class="m2"><p>پیش زنجیر عشق محکم نیست</p></div></div>
<div class="b2" id="bn45"><p>سر من بسته قضای تو شد</p>
<p>دل من خسته رضای تو شد</p></div>
<div class="b" id="bn46"><div class="m1"><p>بره ابن حاجب تو منم</p></div>
<div class="m2"><p>نوکر بی مواجب تو منم</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>تو چو خورشید در حجاب خفا</p></div>
<div class="m2"><p>رفته ای لیک حاجب تو منم</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>آسمان کواکبی اما</p></div>
<div class="m2"><p>نور بخش کواکب تو منم</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>گرچه با خود برادرم خوانی</p></div>
<div class="m2"><p>بغلامی مناسب تو منم</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>گرچه مطلوب عالمی هستم</p></div>
<div class="m2"><p>از سر صدق طالب تو منم</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>قدر دان فضایل تو منم</p></div>
<div class="m2"><p>ترجمان مناقب تو منم</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>نکته دان حقایق تو منم</p></div>
<div class="m2"><p>راز دار مطالب تو منم</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>رای تو روشن است و صائب لیک</p></div>
<div class="m2"><p>مایه رای صائب تو منم</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>حدس تو ثاقب است وراست ولی</p></div>
<div class="m2"><p>مرجع حدس ثاقب تو منم</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>بر در کردگار اگر تازی</p></div>
<div class="m2"><p>طاعت فرض واجب تو منم</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>تو خداوند مالک الملکی</p></div>
<div class="m2"><p>لیک در ملک نایب تو منم</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>تو چو ماه رجب همایونی</p></div>
<div class="m2"><p>لیک لیل الرغائب تو منم</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>جانب حق اگر نظر داری</p></div>
<div class="m2"><p>معنی حق بجانب تو منم</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>یادگار گذشتگان توام</p></div>
<div class="m2"><p>دوستدار اقارب تو منم</p></div></div>
<div class="b2" id="bn60"><p>شمع امید من خموش مکن</p>
<p>دلم از غصه در خروش مکن</p></div>
<div class="b" id="bn61"><div class="m1"><p>پیش من سید مجلل کیست</p></div>
<div class="m2"><p>در بر شیر خرس جنگل کیست</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>خاک ره چیست نزد مشک و عبیر</p></div>
<div class="m2"><p>چوب گز پیش عود و صندل کیست</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>نزد کافور چیست آنقوزه</p></div>
<div class="m2"><p>در بر هندوانه حنظل کیست</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>ظلم را نزد عدل صرف چه جای</p></div>
<div class="m2"><p>جهل در پیش عقل اول کیست</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>پیش احمد کلاغ اسود چه</p></div>
<div class="m2"><p>نزد حیدر سوار یلیل کیست</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>کرم شب تاب نزد مه چه کند</p></div>
<div class="m2"><p>پیش خورشید نور مشعل کیست</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>بر در بارگاه کیخسرو</p></div>
<div class="m2"><p>گیو و گودرز و رستم یل کیست</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>معجز احمدی چو جلوه کند</p></div>
<div class="m2"><p>مکر و نیرنگ و سحر و تنبل کیست</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>با بیانات جعفر صادق</p></div>
<div class="m2"><p>گفته احمدبن حنبل کیست</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>صبح صادق چو پرتو افشاند</p></div>
<div class="m2"><p>شام تاریک و لیل و الیل کیست</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>توسن من چو گرم سیر شود</p></div>
<div class="m2"><p>آن شتر کره قزعمل کیست</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>خاک پای من و عمامه وی</p></div>
<div class="m2"><p>خود تو بر گو کز این دو افضل کیست</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>آنکه وارسته از جهان که بود</p></div>
<div class="m2"><p>آن که در قید غم مسلسل کیست</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>اندرین خوان شراب و نقل کدام</p></div>
<div class="m2"><p>وندرین سفره شیر و خردل کیست</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>گفتی آن کس که این حقایق را</p></div>
<div class="m2"><p>با براهین کند مدلل کیست</p></div></div>
<div class="b2" id="bn76"><p>بنده خاندان مصطفوی</p>
<p>احقرالساده صادق العلوی</p></div>
<div class="b" id="bn77"><div class="m1"><p>گفت صید منت هوس نشود</p></div>
<div class="m2"><p>زانکه عنقا شکار کس نشود</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>گفتمش آنچه گفته ای صدقست</p></div>
<div class="m2"><p>لیک این بنده باز پس نشود</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>دام برچین ز ره که باز سپید</p></div>
<div class="m2"><p>طعمه کرکس و مگس نشود</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>گردن شهریار هفت اقلیم</p></div>
<div class="m2"><p>بسته شحنه و عسس نشود</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>ماه مفتون آب و گل شده است</p></div>
<div class="m2"><p>لاله در بند خار و خس نشود</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>حور با دیو همنشین نسزد</p></div>
<div class="m2"><p>کبک با زاغ همنفس نشود</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>بخدا جز بسینه سینا</p></div>
<div class="m2"><p>موسی اندر پی قبس نشود</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>آنکه با تیشه کنده بیخ هوس</p></div>
<div class="m2"><p>مست رندان بوالهوس نشود</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>تا توانی چو سگ بلای که بحر</p></div>
<div class="m2"><p>به دهان سگان نجس نشود</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>دل مجنون به ناقه لیلی</p></div>
<div class="m2"><p>بیش نالیده چون جرس نشود</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>گر بمیرد هما ز بی برگی</p></div>
<div class="m2"><p>از کلاغانش ملتمس نشود</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>بر خر لنک خود نشین ای شیخ</p></div>
<div class="m2"><p>که ترا رام این فرس نشود</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>ورنه آنجا فتی بخاک سیه</p></div>
<div class="m2"><p>که ترا هیچ داد رس نشود</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>اندکی از برای تأدیبت</p></div>
<div class="m2"><p>گفته ام سعی کن که بس نشود</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>سخت بیرون شد از گلیمت پای</p></div>
<div class="m2"><p>جهد میکن کز این سپس نشود</p></div></div>
<div class="b2" id="bn92"><p>حسبی الله گذشتم از سر جان</p>
<p>یا شوم غرقه یا برم مرجان</p></div>