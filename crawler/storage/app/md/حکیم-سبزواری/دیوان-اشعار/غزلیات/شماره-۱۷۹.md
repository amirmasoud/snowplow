---
title: >-
    شمارهٔ ۱۷۹
---
# شمارهٔ ۱۷۹

<div class="b" id="bn1"><div class="m1"><p>نه از لفظ تو پیغامی نه از کلک تو تحریری</p></div>
<div class="m2"><p>نه از لعل تو دشنامی نه از نطق تو تقریری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نه پیکی تا فرستم سوی او ای ناله امدادی</p></div>
<div class="m2"><p>نه رحمی در دل چون آهنش ای آه تأثیری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به تنگ آمددلم ازنام و از ننگ ای جنون شوری</p></div>
<div class="m2"><p>نشد از عقل آسان مشگلم ای عشق تدبیری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رهم بس سنگلاخ ای رخش همت پای رفتاری</p></div>
<div class="m2"><p>شبم زان تار موتار ای فروغ دیده تنویری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رقیب سفله محرم درحریم یار و مامحروم</p></div>
<div class="m2"><p>سپهرا تا بکی دون پروری زین وضع تغییری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>برغم دشمن تشنه بخون ای دوست الطافی</p></div>
<div class="m2"><p>خلاف مدعای مدعی ای چرخ تدبیری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بلب آمد ز درد بی دوا جان ساقیا جامی</p></div>
<div class="m2"><p>بشد بنیاد دل زیر و زبر مطرب بم وزیری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پس از عمری ببالین مریض خویش میآید</p></div>
<div class="m2"><p>نگاه آخرین است ای اجل یک لحظه تأخیری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نگاهی کن از آن چشم خدنگ انداز صید افکن</p></div>
<div class="m2"><p>که جان دادیم ای ابرو کمان از حسرت تیری</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کشیده صورت گلگونه ها تابر گل خوبان</p></div>
<div class="m2"><p>نکرده کلک نقاش قضا اینگونه تصویری</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز عشق آن پری طلعت بشد دیوانه دل اسرار</p></div>
<div class="m2"><p>از آن زلف مسلسل افکنش بر پای زنجیری</p></div></div>