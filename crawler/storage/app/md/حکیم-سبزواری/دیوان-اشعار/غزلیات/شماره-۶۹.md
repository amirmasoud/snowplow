---
title: >-
    شمارهٔ ۶۹
---
# شمارهٔ ۶۹

<div class="b" id="bn1"><div class="m1"><p>تا کی ز غمت ناله و فریاد توان کرد</p></div>
<div class="m2"><p>ز افتاده به کُنج قفسی یاد توان کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آغوش و کنار از تو نداریم توقع</p></div>
<div class="m2"><p>از نیم نگاهی دل ما شاد توان کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رخش ستم این قدر نباید که بتازی</p></div>
<div class="m2"><p>گیرم که بما این همه بیداد توان کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زاهد چه دهی پند که ما از می لعلش</p></div>
<div class="m2"><p>نی همچو خرابیم که آباد توان کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای آن که بدست تو سررشتهٔ خلقی است</p></div>
<div class="m2"><p>یک رشته به پا طایری آزاد توان کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای نور خدا گویم اگر سوء ادب نیست</p></div>
<div class="m2"><p>دیگر ز کجا مثل تو ایجاد توان کرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جانی و دلی روح روانی همه آنی</p></div>
<div class="m2"><p>از مشت گلی این همه بنیاد توان کرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آورد هجومی بسرم خیل همومی</p></div>
<div class="m2"><p>ساقی به یکی ساغرم امداد توان کرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>یک ره ننمودی نظر اسرار حزین را</p></div>
<div class="m2"><p>گم کرده رهی رابره ارشاد توان کرد</p></div></div>