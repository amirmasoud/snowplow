---
title: >-
    شمارهٔ ۳۳
---
# شمارهٔ ۳۳

<div class="b" id="bn1"><div class="m1"><p>ره و رهبر دلا محبت اوست</p></div>
<div class="m2"><p>سود و سرمایه عشق حضرت اوست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قرة العین عارفان که فناست</p></div>
<div class="m2"><p>نیستی در فروغ طلعت اوست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>غیبتت از خودی و شرب مدام</p></div>
<div class="m2"><p>از دوام حضور ساحت اوست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دولت فقر و کنج آزادی</p></div>
<div class="m2"><p>بندگی گدای حضرت اوست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همگی دیده شد پی دیدار</p></div>
<div class="m2"><p>اندر آن مشهدی که رؤیت اوست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سر بسر گوش شو سرود نیوش</p></div>
<div class="m2"><p>اندر آن محضری که مدحت اوست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>همه اندیشه شو فلاطون کیش</p></div>
<div class="m2"><p>در خم دل که جای فکرت اوست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بر در دل نشین نگهبان باش</p></div>
<div class="m2"><p>کین سراپرده خاص خلوت اوست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چه عجب سر به عرش سود اسرار</p></div>
<div class="m2"><p>بندهٔ بندگان حضرت اوست</p></div></div>