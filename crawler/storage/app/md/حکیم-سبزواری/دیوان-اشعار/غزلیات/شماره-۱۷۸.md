---
title: >-
    شمارهٔ ۱۷۸
---
# شمارهٔ ۱۷۸

<div class="b" id="bn1"><div class="m1"><p>نبود چو ماه روی تو تابنده اختری</p></div>
<div class="m2"><p>نامد مثال لعل تو رخشنده گوهری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از خیل آن و حسن کشی بر سرم سپاه</p></div>
<div class="m2"><p>بر یک تنی که دیده شبیخون لشگری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>صد آفرین بصنع جهان آفرین که او</p></div>
<div class="m2"><p>جا داده صد جهان ملاحت بپیکری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گلزار خلد را شکند عطر خاطرم</p></div>
<div class="m2"><p>چون یاد آورم سر زلف معنبری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دیدم نگار را شده با غیر همنشین</p></div>
<div class="m2"><p>ای کاشکی به پهلوی من بود خنجری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عمر دوباره یابم و بیشک جوان شوم</p></div>
<div class="m2"><p>از دست دوست نوشم اگر یک دوساغری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اسرار طوطی است شکر خاء نطق او</p></div>
<div class="m2"><p>او را چه حاجت است بشهدی و شکری</p></div></div>