---
title: >-
    شمارهٔ ۱۱۳
---
# شمارهٔ ۱۱۳

<div class="b" id="bn1"><div class="m1"><p>هزاران آفرین بر جان حافظ</p></div>
<div class="m2"><p>همه غرقیم در احسان حافظ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز هفتم آسمان غیب آمد</p></div>
<div class="m2"><p>لسان الغیب اندر شان حافظ</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پیمبر نیست لیکن نسخ کرده</p></div>
<div class="m2"><p>اساطیر همه دیوان حافظ</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چه دیوان کز سپهرش جم دیوان</p></div>
<div class="m2"><p>نموده کوکب رخسان حافظ</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر آندعوی کند سحر حلال است</p></div>
<div class="m2"><p>دلیل ساطع البرهان حافظ</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ایا غواص دریای حقیقت</p></div>
<div class="m2"><p>چه گوهرهااست در عمان حافظ</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نه تنها آن وحسنش درنظر هست</p></div>
<div class="m2"><p>طریقت با حقیقت آن حافظ</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بیا اسرار تا ما برفشانیم</p></div>
<div class="m2"><p>دل و جان در ره دربان حافظ</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به بند اسرار لب را چون ندارد</p></div>
<div class="m2"><p>سخن پایانی اندر شان حافظ</p></div></div>