---
title: >-
    شمارهٔ ۸۲
---
# شمارهٔ ۸۲

<div class="b" id="bn1"><div class="m1"><p>دل بشد از دست یاران فکر درمانش کنید</p></div>
<div class="m2"><p>مرهم زخم عجین از آب پیکانش کنید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شهسوارم میرود ای اشک راهش را ببند</p></div>
<div class="m2"><p>ای سپاه ناله زود آهنگ میدانش کنید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر رود از اشک سیل انگیز و آه شعله خیز</p></div>
<div class="m2"><p>شور محشر میشود یاران پشیمانش کنید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خسرو چابک سوارم عزم جولان کرده است</p></div>
<div class="m2"><p>معشر عشاق سزها گوی چوگانش کنید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>میستیزد فارس رد گون بما ای همدمان</p></div>
<div class="m2"><p>از خدنگ آه دلها تیر بارانش کنید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آن دل نازک ندارد طاقت فریاد و داد</p></div>
<div class="m2"><p>دادخواهان دست خود کوته ز دامانش کنید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>وادی غم هر کف خاکیش جانی یا دلی است</p></div>
<div class="m2"><p>رهروان ترک دل و جان در بیابانش کنید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>طوطی گویای اسرار از فراقش تلخکام</p></div>
<div class="m2"><p>زان لب شکر شکن در شکرستانش کنید</p></div></div>