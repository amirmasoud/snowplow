---
title: >-
    شمارهٔ ۸۰
---
# شمارهٔ ۸۰

<div class="b" id="bn1"><div class="m1"><p>حسن رخی کان تراست ماه ندارد</p></div>
<div class="m2"><p>گو بر رخش طره سیاه ندارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این چه گیاه خط است وین چه گل روی</p></div>
<div class="m2"><p>خلد چو این گل چو آن گیاه ندارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دُرکه نهان کرده ای بحقّهٔ یاقوت</p></div>
<div class="m2"><p>جوهرئی را نبوده شاه ندارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دل که بیغما ربودی از کف او جان</p></div>
<div class="m2"><p>غیر دو چشم خودت گواه ندارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بوالعجبیهای عشق بین که مسخّر</p></div>
<div class="m2"><p>کرده جهان آن شه و سپاه ندارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>صبر و خِرَد دین و دل قرار و توانم</p></div>
<div class="m2"><p>برده به حدّیکه سینه آه ندارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای صنم اسرار را مران ز در خویش</p></div>
<div class="m2"><p>زانکه بغیر از درت پناه ندارد</p></div></div>