---
title: >-
    شمارهٔ ۴۰
---
# شمارهٔ ۴۰

<div class="b" id="bn1"><div class="m1"><p>شهر پر آشوب و غارت دل و دین است</p></div>
<div class="m2"><p>باز مگر شاه ما بخانه زین است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آینهٔ روست یا که جام جهان بین</p></div>
<div class="m2"><p>آتش طور است با شعاع جبین است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با که توان گفت این سخن که نگارم</p></div>
<div class="m2"><p>شاهد هر جائی است و پرده نشین است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شه توئی ای دوست در قلمرو دلها</p></div>
<div class="m2"><p>کشور جانها ترا بزیر نگین است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خسروی عالمم بچشم نیاید</p></div>
<div class="m2"><p>گر تو اشارت کنی که چاکرم این است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر سر بالین بیا که آخر عمر است</p></div>
<div class="m2"><p>رخ بنما کین نگاه بازپسین است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خون بدل ما کنی بخاطر دشمن</p></div>
<div class="m2"><p>جان من آئین دوستی نه چنین است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ساغر مینا بگیر و شاهد رعنا</p></div>
<div class="m2"><p>باشد اگر حاصلی ز عمر همین است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هرکه بروی تو دید زلف تو گفتا</p></div>
<div class="m2"><p>کفر بدین همچو شب بروز قرین است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نیست چو بی نور لطف نار جلالت</p></div>
<div class="m2"><p>نار تو خواهم که رشک خلد برین است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>درخورم اسرار تنگنای جهان نیست</p></div>
<div class="m2"><p>مرغ دلم شاهباز سدره نشین است</p></div></div>