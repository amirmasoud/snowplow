---
title: >-
    شمارهٔ ۲۰
---
# شمارهٔ ۲۰

<div class="b" id="bn1"><div class="m1"><p>الهی بر دلم ابواب تسلیم و رضا بگشا</p></div>
<div class="m2"><p>بروی ما، دری از زحمت بی منتها بگشا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رهی ما را بسوی کعبهٔ صدق و صفا بنما</p></div>
<div class="m2"><p>دری ما را بصوب گلشن فقر و فنا بگشا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ببسط وجه و اطلاق جبین اهل تسلیمت</p></div>
<div class="m2"><p>گره واکن ز ابرو عقده‌های کار ما بگشا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بعقد گیسوان پردهٔ عصمت نشینانت</p></div>
<div class="m2"><p>ز لطفت برفع از روی عروس مدعا بگشا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>درون تیرهٔ دارم ز خواطرهای نفسانی</p></div>
<div class="m2"><p>بسینه مطلعی از روزن نور و ضیا بگشا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بود دل چند رنجور از خمار و بسة میخانه</p></div>
<div class="m2"><p>بر این دردی کش دردت در دار الشفا بگشا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>درون درد پردردی بده کاید عذابش عذب</p></div>
<div class="m2"><p>ببند این دیدهٔ بدبین ما چشم صفا بگشا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از این ناصاف آب در گذر افزود سوز جان</p></div>
<div class="m2"><p>بسوی جویبار دل ره از عین بقا بگشا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>پرافشان در هوایت طایران و مرغ دل دربند</p></div>
<div class="m2"><p>پر و بال دلم در آن فضای جان فزا بگشا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز پیچ و تاب راه عشق اندر وادی حیرت</p></div>
<div class="m2"><p>مرا افتاده مشکلها تو ای مشگل گشا بگشا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>در گنجینهٔ حق الیقین را نام تو مفتاح</p></div>
<div class="m2"><p>به پیر مسلک آموز و جوان پارسا بگشا</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>زغم لبریز و خوندل چون صراحی تا بکی اسرار</p></div>
<div class="m2"><p>گشاده روچو جامم ساز و نطق بانوابگشا</p></div></div>