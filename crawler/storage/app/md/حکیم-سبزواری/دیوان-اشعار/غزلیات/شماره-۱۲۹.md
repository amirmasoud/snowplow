---
title: >-
    شمارهٔ ۱۲۹
---
# شمارهٔ ۱۲۹

<div class="b" id="bn1"><div class="m1"><p>ترا چون مهر با غیر است و اسرار نهانی هم</p></div>
<div class="m2"><p>برو ارزانی او باد این لطف زبانی هم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مرایکجرعه می از دستت ای ساقی بسی خوشتر</p></div>
<div class="m2"><p>ز شهد شکر مصری ز آب زندگانی هم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چونقش صورت زیبندهات ای رشک مهرویان</p></div>
<div class="m2"><p>نبسته خامه نقاش چین و کلک مانی هم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رخت را جام جم گفتند و هم آیینهٔ حق بین</p></div>
<div class="m2"><p>خطت تعویذ جان خواندند خط سبع المثالی هم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مرا از آتش هجران خود در اینجهان سوزی</p></div>
<div class="m2"><p>اگردلبر توئی فردا بسوزی آن جهانی هم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گدائی درت ما را بسی بهتر بود یا را</p></div>
<div class="m2"><p>ز سلطانی عالم و زبهشت جاودانی هم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>همه آیینه اعیان ز پیدائی تو پنهان</p></div>
<div class="m2"><p>چو حسنت هست بی پایان توئی عین نهانی هم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چه میپرسید از اسرار نماندش دفتر و دستار</p></div>
<div class="m2"><p>نظر باز است و می نوشد شراب ارغوانی هم</p></div></div>