---
title: >-
    شمارهٔ ۱۳۱
---
# شمارهٔ ۱۳۱

<div class="b" id="bn1"><div class="m1"><p>شد وقت آنکه باز هوای چمن کنم</p></div>
<div class="m2"><p>آمد بهار و فکر شراب کهن کنم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حاشا که با جمال جهانگیر عارضت</p></div>
<div class="m2"><p>نظاره جانب گل و برگ سمن کنم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در دوزخ از خیال توام دست میدهد</p></div>
<div class="m2"><p>دوزخ بیاد روی تو گلشن شکن کنم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بهر نثار مقدم تو هر دم از سرشک</p></div>
<div class="m2"><p>دامان خویش پر ز عقیق یمن کنم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا دیده ام من اهرمن خال عارضت</p></div>
<div class="m2"><p>بر آن سرم که سجده بر اهرمن کنم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز اسرار خویش آگهی اسرار را دهم</p></div>
<div class="m2"><p>چون با خود آیم و سفر از خویشتن کنم</p></div></div>