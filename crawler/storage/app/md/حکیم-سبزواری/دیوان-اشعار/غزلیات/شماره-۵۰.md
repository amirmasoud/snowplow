---
title: >-
    شمارهٔ ۵۰
---
# شمارهٔ ۵۰

<div class="b" id="bn1"><div class="m1"><p>دل و دین بتی نامسلمان گرفت</p></div>
<div class="m2"><p>بیک عشوهٔ کشور جان گرفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بت سبزوار از خط سبزه وار</p></div>
<div class="m2"><p>بخد خور آسا خراسان گرفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز پیکان او یافت حظی دلم را</p></div>
<div class="m2"><p>که گفتی که خطش ز پیکان گرفت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بدوران مخور غم به دور آن می آر</p></div>
<div class="m2"><p>که غم ها برد می چودوران گرفت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چه خواهد دگر شحنهٔ غم زمانه</p></div>
<div class="m2"><p>اگر نیم جان بود جانان گرفت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دلی داشتم بود غمخوار جان</p></div>
<div class="m2"><p>ولی ترک مستی ز این آن گرفت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مرا بود چشمی از او بهره ور</p></div>
<div class="m2"><p>ز بس اشک بارید طوفان گرفت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شه حسنش آهنگ تاراج کرد</p></div>
<div class="m2"><p>ز اسرار دل برد و ایمان گرفت</p></div></div>