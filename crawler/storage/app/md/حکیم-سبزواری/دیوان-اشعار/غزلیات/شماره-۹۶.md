---
title: >-
    شمارهٔ ۹۶
---
# شمارهٔ ۹۶

<div class="b" id="bn1"><div class="m1"><p>جاء الصبا بعطر ریاحین و الزهر</p></div>
<div class="m2"><p>از زلف یار میرسد این باد مشک اثر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پیک خجستهٔ مقدم فرخنده مرحبا</p></div>
<div class="m2"><p>اهلا حمام کعبة لیلای ما الخبر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در آرزوی سر و قد خوش خرام او</p></div>
<div class="m2"><p>القلب طول عمری فی دربها انتظر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آدم باین جمال نیامد باین جهان</p></div>
<div class="m2"><p>حوراء جنة هی ماهذه بشر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ساقی بیاد روی صبیحی صبوحی آر</p></div>
<div class="m2"><p>قد شوشت نسیم صبا طرة السحر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا کی نهان بمشرق خم آفتاب می</p></div>
<div class="m2"><p>گاه الصباح یسفر و الدیک قد نعر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آن می که آب خضر هوادار درد اوست</p></div>
<div class="m2"><p>آن می که نور موسی از آن یافت یک شرر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مشکوة دل فروغ ز مصباح باده یافت</p></div>
<div class="m2"><p>ان او مضت ز جاجتها یخطف البصر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>می سدفکر فاسد یاجوج مفسد است</p></div>
<div class="m2"><p>اشرار ارض قلبک اسرار لاتذر</p></div></div>