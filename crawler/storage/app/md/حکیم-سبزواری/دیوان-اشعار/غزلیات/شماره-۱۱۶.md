---
title: >-
    شمارهٔ ۱۱۶
---
# شمارهٔ ۱۱۶

<div class="b" id="bn1"><div class="m1"><p>ساقی بیا که عمر گران‌مایه شد تلف</p></div>
<div class="m2"><p>دایم نخواهد این در جان ماند در صدف</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>طفلی است جان و مهد تن او را قرارگاه</p></div>
<div class="m2"><p>چون گشت راهرو فکند مهد یک طرف</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در تنگنای بیضه بود جوجه از قصور</p></div>
<div class="m2"><p>پر زد سوی قصور چو شد طایر شرف</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز آغاز کار جانب جانان همی روم</p></div>
<div class="m2"><p>مرگ ار پسند نفس نه جانراست صد شعف</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تابی ز آفتاب بخاک آمد از شباک</p></div>
<div class="m2"><p>خود بودی آفتاب چو شد پرده منکشف</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>انگشت بین که جمره شد و گشت شعله ور</p></div>
<div class="m2"><p>پس در صفات نور شد آن نار مکتشف</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کرد آفتاب باده تجلی در انجمن</p></div>
<div class="m2"><p>قد کان من سنائها الارواح یختطف</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>موسی جان ز جلوه شدش کوه تن خراب</p></div>
<div class="m2"><p>ولی بوجهه هو ذاالشطر و انصرف</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اسرار جان کند ز چه رو ترک ملک و تن</p></div>
<div class="m2"><p>بیند جمال مهر جلال شه نجف</p></div></div>