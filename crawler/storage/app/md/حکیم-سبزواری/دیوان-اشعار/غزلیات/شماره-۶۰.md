---
title: >-
    شمارهٔ ۶۰
---
# شمارهٔ ۶۰

<div class="b" id="bn1"><div class="m1"><p>ای قبلهٔ حاجات ملک طرف کلاهت</p></div>
<div class="m2"><p>مجموعه آفات فلک طرز نگاهت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیچاره کشی پیشهٔ زلفان کمندت</p></div>
<div class="m2"><p>خونخواره وَشی شیوهٔ چشمان سیاهت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خونم بخور و غم مخور از پرسش محشر</p></div>
<div class="m2"><p>طفلی و ملایک ننویسند گناهت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>افکندیم از پا به یکی غمزه و رفتی</p></div>
<div class="m2"><p>باز آ که بود دیدهٔ امید براهت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>این جان بودت کشور و دل باشدت اورنگ</p></div>
<div class="m2"><p>کاکل بسرت افسر و از غمزه سپاهت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر زیرنشینان لوای غم عشقت</p></div>
<div class="m2"><p>رحمی که ندانند دری غیر پناهت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آهو روش اسرار ره دشت جنون گیر</p></div>
<div class="m2"><p>در شهر نیاسوده کس از ناله و آهت</p></div></div>