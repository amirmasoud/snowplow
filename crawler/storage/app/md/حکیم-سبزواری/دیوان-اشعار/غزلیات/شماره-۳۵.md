---
title: >-
    شمارهٔ ۳۵
---
# شمارهٔ ۳۵

<div class="b" id="bn1"><div class="m1"><p>ای من فدای عاشقی هرچندخونخوارمن است</p></div>
<div class="m2"><p>خار غمش گو جا کند در سینه گلزار من است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دادم نخستین دل بدودرسینه کشتم مهراو</p></div>
<div class="m2"><p>لیکن مدام آن جنگجودرقصد آزار من است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا تار گیسو ریخته جانها بتارآویخته</p></div>
<div class="m2"><p>گویددل بگسیخته منصورم این دارمن است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آنجا  که هستی حق است هستی کُل مستغرق است</p></div>
<div class="m2"><p>جائی که نورمطلق است کی جای اظهارمن است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>باشدمرا از خود تله کرمم تنم بر خود پله</p></div>
<div class="m2"><p>نبود مرا از وی گله دوری زپندار من است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هرجا نظر انداختم جز او کسی نشناختم</p></div>
<div class="m2"><p>زاغیار تا پرداختم دل را همه یارمن است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا دل بسیر افتاده است هرشروخیرافتاده است</p></div>
<div class="m2"><p>ظاهر بغیر افتاده است در خفیه درکار من است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اجرای عالم یک بیک گر خود سماک و گرسمک</p></div>
<div class="m2"><p>جن و ملک نجم و فلک کل شرح اسرار من است</p></div></div>