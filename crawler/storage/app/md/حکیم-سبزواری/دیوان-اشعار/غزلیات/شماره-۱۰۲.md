---
title: >-
    شمارهٔ ۱۰۲
---
# شمارهٔ ۱۰۲

<div class="b" id="bn1"><div class="m1"><p>غم از حد برونی دارم امروز</p></div>
<div class="m2"><p>دل لبریز خونی دارم امروز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فراق آمد زمان وصل سرشد</p></div>
<div class="m2"><p>چه بخت واژگونی دارم امروز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>قدی همچون الف ز آغوش جان رفت</p></div>
<div class="m2"><p>ز غم قد چو نونی دارم امروز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چونی هر استخوانم درنوائی است</p></div>
<div class="m2"><p>چه ساز ارغنونی دارم امروز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز ناخن تیشهام در سینهٔ کوه</p></div>
<div class="m2"><p>بپیشم بیستونی دارم امروز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز تحریک مه محمل نشینم</p></div>
<div class="m2"><p>نه صبری نی سکونی دارم امروز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بسر اسرار از سودای زلفش</p></div>
<div class="m2"><p>زده شور و جنونی دارم امروز</p></div></div>