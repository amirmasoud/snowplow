---
title: >-
    شمارهٔ ۱۲۴
---
# شمارهٔ ۱۲۴

<div class="b" id="bn1"><div class="m1"><p>چه شوری بود یاران بر سر دل</p></div>
<div class="m2"><p>ز غم گوئی سرشته پیکر دل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نریزد ساقی بزم محبت</p></div>
<div class="m2"><p>بجز خوناب غم در ساغر دل</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بجز سوزش نسازد هیچ باطبع</p></div>
<div class="m2"><p>گلستان خلیل است آذر دل</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر آتش پارهها برمیفشاند</p></div>
<div class="m2"><p>مگر بال سمندر شد پر دل</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نشد افسرده ز آب هفت دریا</p></div>
<div class="m2"><p>چه آتش بود اندر مجمر دل</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حمل جز برج ناری نیست گوئی</p></div>
<div class="m2"><p>اثر هم جز وبال از اختر دل</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بسوز نار دوزخ خندد اسرار</p></div>
<div class="m2"><p>جهدگر یک شرار از اخگر دل</p></div></div>