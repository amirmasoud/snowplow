---
title: >-
    شمارهٔ ۱۵۷
---
# شمارهٔ ۱۵۷

<div class="b" id="bn1"><div class="m1"><p>گیرم نقاب برفکنی از رخ چو ماه</p></div>
<div class="m2"><p>کو تاب یک کرشمه و کو طاقت نگاه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یک شمه از طراوت رویت بهار و باغ</p></div>
<div class="m2"><p>یک پرتو از فروغ رخت نور مهر و ماه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یکبار رخش نازبرون تا ز و باز بین</p></div>
<div class="m2"><p>عشاق را جبین مذلت به خاک راه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در خون نگر بمالم دل مردمان چشم</p></div>
<div class="m2"><p>بر پا نموده از مژگان رایت سپاه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عزم شکار کرده مرانم که عیب نیست</p></div>
<div class="m2"><p>وقت شکار بودن سگ در قفای شاه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آن مه سپه کشد پی تاراج جان ز ناز</p></div>
<div class="m2"><p>من میکنم مبارزه با خیل اشک و آه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جز پیش این بتان خداوندگار حسن</p></div>
<div class="m2"><p>در مذهب که بوده روا قتل بی‌گناه؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در ترک و تاز لشکر نازش به ملک حسن</p></div>
<div class="m2"><p>کس جان نبرد خاصه تو اسرار از این سپاه</p></div></div>