---
title: >-
    شمارهٔ ۳۰
---
# شمارهٔ ۳۰

<div class="b" id="bn1"><div class="m1"><p>جلوه گر در پرده آمد آفتاب</p></div>
<div class="m2"><p>از تعین بر رخ افکنده نقاب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا نسوزند از فروغ روی او</p></div>
<div class="m2"><p>رفته از مهر آن مهم زیر سحاب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نی غلط گفتم نقاب و پرده چیست</p></div>
<div class="m2"><p>بی حجابی آمده او را حجاب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شاهدان در پرده مستورند لیک</p></div>
<div class="m2"><p>ماه من بی پرده باشد در نقاب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دیدم اندر بزم میخواران شدی</p></div>
<div class="m2"><p>هم تو ساقی هم ساغر هم شراب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>قصهٔ ما قصهٔ آبست و حوت</p></div>
<div class="m2"><p>ای تو آب و جمله عالم سراب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تابی از آن مهر عالمتاب کو</p></div>
<div class="m2"><p>تا فسرده دل شود فانی در آب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مصدر و تعریف و اصل و فرع تو</p></div>
<div class="m2"><p>هم تکلم از تو هم با تو خطاب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از شراب بیخودی ساقی بده</p></div>
<div class="m2"><p>یک دو ساغر تا شوم مست و خراب</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گویم از اسرار هر ناگفتنی</p></div>
<div class="m2"><p>پیش زاهد گر خطا و گر ثواب</p></div></div>