---
title: >-
    شمارهٔ ۱۹
---
# شمارهٔ ۱۹

<div class="b" id="bn1"><div class="m1"><p>گر پریشان حالم او داند لسان حال را</p></div>
<div class="m2"><p>ورچو سوسن لالم او داند زبان لال را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گرچه بامت بس بلند و بی پر و بالیم ما</p></div>
<div class="m2"><p>همتی کان شمع رویت سوخت پر و بال را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای امیر کاروان کاندیشهٔ ما نبودت</p></div>
<div class="m2"><p>یک نظر هم میرسد افتاده در دنبال را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سنگی از طفلی نیامد بر سر ما در جنون</p></div>
<div class="m2"><p>چرخ در دوران ما افسرده کرد اطفال را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نغمه‌ام زاری دل شربم ز خوناب جگر</p></div>
<div class="m2"><p>بین ببزم کامرانی بادهٔ قوال را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عمر بگذشت و نگاهی بر من مسکین نکرد</p></div>
<div class="m2"><p>جان من آخر نه انجامی بود اهمال را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هرچه پیش آید زیار اسرار نبود شکوهٔ</p></div>
<div class="m2"><p>سوی ما نبود گذاری طایر اقبال را</p></div></div>