---
title: >-
    شمارهٔ ۱۰۱
---
# شمارهٔ ۱۰۱

<div class="b" id="bn1"><div class="m1"><p>ای شعله رخ آتش بدلم در زدهٔ باز</p></div>
<div class="m2"><p>یاقوت لب از خون که ساغر زدهٔ باز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زینسان که تو طرف کله از ناز شکستی</p></div>
<div class="m2"><p>بر افسر خورشید فلک برد زدهٔ باز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دیگر چه خطا دیدهٔ ای آهوی چین چون</p></div>
<div class="m2"><p>وحشی صفت از سرزدهٔ سرزدهٔ باز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تر کردهٔ از خون شهیدان لب لعلت</p></div>
<div class="m2"><p>داغی بدل لالهٔ احمر زدهٔ باز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زان آتش رخسار و زان غالیهٔ زلف</p></div>
<div class="m2"><p>آتش بدل و عود بمجمر زدهٔ باز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای آنکه تو بر تارک اخترزدهٔ گام</p></div>
<div class="m2"><p>بر لطف تو است دیدهٔ اختر زدهٔ باز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر همزدهٔ رشتهٔ جمعیت دلها</p></div>
<div class="m2"><p>چون شانه بر آن زلف معنبر زدهٔ باز</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شیرین ز شکرخنده کنی کام جهانی</p></div>
<div class="m2"><p>ای غنچه دهان خنده بشکر زدهٔ باز</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اسرار ز نظم تو چکد آب لطافت</p></div>
<div class="m2"><p>گویا که در آن آب و هوا پر زدهٔ باز</p></div></div>