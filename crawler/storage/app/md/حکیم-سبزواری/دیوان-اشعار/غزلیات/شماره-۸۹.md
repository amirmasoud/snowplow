---
title: >-
    شمارهٔ ۸۹
---
# شمارهٔ ۸۹

<div class="b" id="bn1"><div class="m1"><p>گل رنگ نگار ما ندارد</p></div>
<div class="m2"><p>بوی خوش یار ما ندارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زیباست چمن ولی صفائی</p></div>
<div class="m2"><p>بی لاله عذار ما ندارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در در صدف نگوئی این بحر</p></div>
<div class="m2"><p>چون دُر کنار ما ندارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نغز است ربیع و لیک آنی</p></div>
<div class="m2"><p>چون تازه بهار ما ندارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گل سر بکمند او نهاده</p></div>
<div class="m2"><p>او میل شکار ما ندارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عمری است که از برش پیامی</p></div>
<div class="m2"><p>پیکی بدیار ما ندارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اسرار ز دست شد دل و یار</p></div>
<div class="m2"><p>فکر دل زار ما ندارد</p></div></div>