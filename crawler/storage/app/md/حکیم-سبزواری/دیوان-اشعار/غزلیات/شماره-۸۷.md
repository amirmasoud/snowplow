---
title: >-
    شمارهٔ ۸۷
---
# شمارهٔ ۸۷

<div class="b" id="bn1"><div class="m1"><p>ما ز میخانه عشقیم گدایانی چند</p></div>
<div class="m2"><p>باده نوشان و خموشان و خروشانی چند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای که در حضرت او یافتهٔ بار ببر</p></div>
<div class="m2"><p>عرضهٔ بندگی بیسر و سامانی چند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کای شه کشور حسن و ملک ملک وجود</p></div>
<div class="m2"><p>منتظر بر سر راهند غلامانی چند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عشق صلح کل و باقی همه جنگست و جدل</p></div>
<div class="m2"><p>عاشقان جمع و فرق جمع پریشانی چند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سخن عشق یکی بود و لی آوردند</p></div>
<div class="m2"><p>این سخنها بمیان زمرهٔ نادانی چند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آنکه جوید حرمش گو بسر کوی دل آی</p></div>
<div class="m2"><p>نیست حاجت که کند قطع بیابانی چند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زاهد از باده فروشان بگذر دین مفروش</p></div>
<div class="m2"><p>خورده بینها است در این حلقه و رندانی چند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نه در اختر حرکت بود نه در قطب سکون</p></div>
<div class="m2"><p>گر نبودی بزمین خاک نشینانی چند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ای که مغرور بجاه دو سه روزی بر ما</p></div>
<div class="m2"><p>رو گشایش طلب از همت مردانی چند</p></div></div>