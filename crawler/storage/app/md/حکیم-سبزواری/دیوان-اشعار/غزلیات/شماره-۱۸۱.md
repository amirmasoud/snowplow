---
title: >-
    شمارهٔ ۱۸۱
---
# شمارهٔ ۱۸۱

<div class="b" id="bn1"><div class="m1"><p>شدم پیر از فراق نوجوانی</p></div>
<div class="m2"><p>که برهم میزند چشمش جهانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کحیل طرفه سود الذوایب</p></div>
<div class="m2"><p>خضیب کفه رخص البنانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>برآید فتنه ها از چشم مستش</p></div>
<div class="m2"><p>که ناید از قضای آسمانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>قسی الحاجب القاسی فؤاده</p></div>
<div class="m2"><p>فصیح قوله عذب البیانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بدیع است اینکه سازد تلخکامم</p></div>
<div class="m2"><p>بآن شکر لبی شیرین زبانی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>فرید فی ملاح لیس کفوه</p></div>
<div class="m2"><p>وحید ماله فی الحسن ثانی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تو چشم مردمی و مردم چشم</p></div>
<div class="m2"><p>تو جان اسرار را جان جهانی</p></div></div>