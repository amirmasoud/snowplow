---
title: >-
    شمارهٔ ۱۷
---
# شمارهٔ ۱۷

<div class="b" id="bn1"><div class="m1"><p>الا یا نفس قد زموالمطا یا</p></div>
<div class="m2"><p>خدایاده شکیبائی خدایا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو روز وصل را آمد شب هجر</p></div>
<div class="m2"><p>الی روحی دنت ایدی المنایا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بدل بارغم آمد کوه بر کوه</p></div>
<div class="m2"><p>کما یعلوا هوادجها الثنایا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز چشمم دجله‌های خون فشاندند</p></div>
<div class="m2"><p>وناراً اضرموها فی حشایا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گرم مانده است در تن نیم جانی</p></div>
<div class="m2"><p>الاعوجوالافدیکم بقایا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>الاحبواعنا دل ادنای الورد</p></div>
<div class="m2"><p>اعینونی علی بث الشکایا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بنال اسرار هنگام وداع است</p></div>
<div class="m2"><p>بنا حل النوی جل الرزایا</p></div></div>