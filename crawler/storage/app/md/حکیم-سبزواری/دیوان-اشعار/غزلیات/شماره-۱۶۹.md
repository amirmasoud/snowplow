---
title: >-
    شمارهٔ ۱۶۹
---
# شمارهٔ ۱۶۹

<div class="b" id="bn1"><div class="m1"><p>بر قامت تو شد راست دنیای کن فکانی</p></div>
<div class="m2"><p>بر تارک تو زیبا است اکلیل من رآنی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از یکدمت نخستین جانبازی است برطین</p></div>
<div class="m2"><p>چون زهرهٔ ریاحین از باده مهرگانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هستی بر انبیا شه فرمانبرت که و مه</p></div>
<div class="m2"><p>تاج تولی مع اللّه حق را تو نور ثانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>برتر نشست از املاک شاه سریر لولاک</p></div>
<div class="m2"><p>آن شب که شد برافلاک از بزم ام هانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شرع تو نسخ ادیان کرد آنچنان که ریزان</p></div>
<div class="m2"><p>گردد ورق ز اغصان در صرصر خزانی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>غیر هواش یکسر از سرفکن به آذر</p></div>
<div class="m2"><p>اسرار خاک آن در به زاب زندگانی</p></div></div>