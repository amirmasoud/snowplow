---
title: >-
    شمارهٔ ۷۱
---
# شمارهٔ ۷۱

<div class="b" id="bn1"><div class="m1"><p>تا بکی یار بکام دگران خواهد بود</p></div>
<div class="m2"><p>چشم امید دل من نگران خواهد بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زان تعلل وز ما صبر و تحمل تا چند</p></div>
<div class="m2"><p>ما بر این شیوه و دلدار بران خواهد بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عوض بادهٔ گلگون صراحی چندم</p></div>
<div class="m2"><p>شیشهٔ دیده ز خون جرعه فشان خواهد بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا کیم شعلهٔ دل روشنی خلوت و یار</p></div>
<div class="m2"><p>شمع در انجمن مدعیان خواهد بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همه شب بر درت از آمد و رفتم تا کی</p></div>
<div class="m2"><p>سگ کوی تو بفریاد و فغان خواهد بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چند مرغ دلم اندر قفس سینهٔ تنگ</p></div>
<div class="m2"><p>بهوای چمنت نوحه کنان خواهد بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سرگرانی تو عمری نپذیرد انجام</p></div>
<div class="m2"><p>کو شکیبابه چه تاب و چه توان خواهد بود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>روز در بیم که آمد شب و چون خواهد رفت</p></div>
<div class="m2"><p>شب در اندیشه که فردا بچه سان خواهد بود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>صدقران گر گذرد بخت اگربخت من است</p></div>
<div class="m2"><p>رو شکیب آر که در خواب گران خواهد بود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ای مه از دست تو در کوچه و بازار اسرار</p></div>
<div class="m2"><p>بعد از این نعره زنان جامه دران خواهد بود</p></div></div>