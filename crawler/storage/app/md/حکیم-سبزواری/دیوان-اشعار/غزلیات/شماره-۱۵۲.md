---
title: >-
    شمارهٔ ۱۵۲
---
# شمارهٔ ۱۵۲

<div class="b" id="bn1"><div class="m1"><p>راه عشق است و بهرگام دوصد جان بگرو</p></div>
<div class="m2"><p>عشق سریست نهانی به دراز گفت و شنو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کی شود این دل بی حاصل ما طعمه عشق</p></div>
<div class="m2"><p>بر این مرغ هما خرمنی از جان بدو جو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بسکه نزدیک بود شارع مقصد دور است</p></div>
<div class="m2"><p>تا یکی ای دل دیوانه بهر سو تک و دو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>این همه عکس که آغازی وانجامش نیست</p></div>
<div class="m2"><p>از فروغ رخ آن مهر بود یک پرتو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در بر ماه ببین آینه و آب جدار</p></div>
<div class="m2"><p>که چسان خود متفنن شود از یک خورضو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گوشه ابروئی از گوشهٔ برقع بنمود</p></div>
<div class="m2"><p>آسمان را که همی چرخ زنان شد در دو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>درد نوشان سماوی ترا آمده جام</p></div>
<div class="m2"><p>که بود باز از این فخر دهان مه نو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>می خور اسرار و از این خواب گران شو بیدار</p></div>
<div class="m2"><p>حاصل عمر خود اندوز که شد وقت درو</p></div></div>