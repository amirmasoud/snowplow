---
title: >-
    شمارهٔ ۱۳۲
---
# شمارهٔ ۱۳۲

<div class="b" id="bn1"><div class="m1"><p>تحمل از غم تو یا ز روزگار کنم</p></div>
<div class="m2"><p>بغیر آنکه خورم خون دل چکار کنم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگر عناصر این نه فلک ورق گردد</p></div>
<div class="m2"><p>غمت رقم نشود گرچه اختصار کنم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بطول روز قیامت شبی ببایستی</p></div>
<div class="m2"><p>که با تو من گله از درد انتظار کنم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ببزم غیر مکش می روا مدار که من</p></div>
<div class="m2"><p>مدام بی تو بخون جگر مدار کنم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بآن رسیده ز جور سپهر و کینهٔ غیر</p></div>
<div class="m2"><p>که رخت بندم و ترک دیار و یار کنم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کنون که ناشده طوفان بیار خاک رهش</p></div>
<div class="m2"><p>که بلکه چارهٔ این چشم اشکبار کنم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جفا مبر ز حد اندیشه کن از آن روزی</p></div>
<div class="m2"><p>که داوری بتو در نزد کردگار کنم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نصیب ما نشد ای دوست کنج دامت هم</p></div>
<div class="m2"><p>نه آشیان نه قفس کاندران قرار کنم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>عجب مدار گرت نغمه سنج شد اسرار</p></div>
<div class="m2"><p>که عندلیبم و افغان بنوبهار کنم</p></div></div>