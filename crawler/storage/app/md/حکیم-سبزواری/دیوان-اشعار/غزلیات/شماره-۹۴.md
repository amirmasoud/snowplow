---
title: >-
    شمارهٔ ۹۴
---
# شمارهٔ ۹۴

<div class="b" id="bn1"><div class="m1"><p>بر دلم قهر و رضای تو لذیذ</p></div>
<div class="m2"><p>بر تنم رنج و شفای تو لذیذ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همه اطوار تو زیبا و پسند</p></div>
<div class="m2"><p>فرق سر تا کف پای تو لذیذ</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خواه مهر از تو رسد خواه جفا</p></div>
<div class="m2"><p>مهر تو نغز و جفای تو لذیذ</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چه بسازی چه بسوزی سازیم</p></div>
<div class="m2"><p>چه ولا و چه بلای تو لذیذ</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نسبتم را بسگ در گاهت</p></div>
<div class="m2"><p>خواه لاخواه بلای تو لذیذ</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر برانی ز درت ور خوانی</p></div>
<div class="m2"><p>خود تو دانی همه رای تو لذیذ</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چه گذاری چه نوازی حکمی</p></div>
<div class="m2"><p>مانی و جمله نوای تو لذیذ</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زهر از دست توام نوش بود</p></div>
<div class="m2"><p>درد یعنی که دوای تو لذیذ</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از تناسب بر اسرار اسرار</p></div>
<div class="m2"><p>زان لب نکته سرای تو لذیذ</p></div></div>