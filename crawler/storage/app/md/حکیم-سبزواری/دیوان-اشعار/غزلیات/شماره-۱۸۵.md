---
title: >-
    شمارهٔ ۱۸۵
---
# شمارهٔ ۱۸۵

<div class="b" id="bn1"><div class="m1"><p>تو چون پیمان عهدت می شکستی</p></div>
<div class="m2"><p>چرا با ما نخستین عهد بستی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من از تو نگسلم پیوند و الفت</p></div>
<div class="m2"><p>اگرچه رشتهٔ جانم گسستی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سحرگاهان برون شد مست و مخمور</p></div>
<div class="m2"><p>بدستی ساغر و خنجر بدستی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هزاران رستخیز و فتنه برخواست</p></div>
<div class="m2"><p>بهرجا کان پری یکدم نشستی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بده ساقی دگر رطل گرانم</p></div>
<div class="m2"><p>که من مستم ز چشم می پرستی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بدو گفتم دهی کی کام اسرار</p></div>
<div class="m2"><p>بگفتا آن زمان کز خودبرستی</p></div></div>