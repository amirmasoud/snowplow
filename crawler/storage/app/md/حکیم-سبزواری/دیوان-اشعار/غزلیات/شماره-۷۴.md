---
title: >-
    شمارهٔ ۷۴
---
# شمارهٔ ۷۴

<div class="b" id="bn1"><div class="m1"><p>تشنهٔ نوش لبت چشمهٔ حیوان چکند</p></div>
<div class="m2"><p>خفتهٔ خاک درت روضهٔ رضوان چه کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن که از خاک نشینانِ درِ اهل دل است</p></div>
<div class="m2"><p>تخم جم کی نگرد ملک سلیمان چه کند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هرکه گردید بدور حرم اهل صفا</p></div>
<div class="m2"><p>ننگرد صف صفا قطع بیابان چه کند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>لذت چاشنی عشق تو هر کس که برد</p></div>
<div class="m2"><p>عافیت میشودش درد تو درمان چه کند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گیرم ای شوخ دل سوخته با جور تو ساخت</p></div>
<div class="m2"><p>با جفای فلک و طعن رقیبان چه کند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عندلیبان چمن گل بشما ارزانی</p></div>
<div class="m2"><p>دل غمدیدهٔ ما سیر گلستان چه کند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>قوت بازوی عشق و دل مسکین هیهات</p></div>
<div class="m2"><p>صید پیداست که در پنجهٔ شیران چه کند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گیرم آن شه ز کرم داد مرا فیض حضور</p></div>
<div class="m2"><p>دل باین تیرگی و موجب حرمان چه کند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>پای رفتار نمانده است و زبان گفتار</p></div>
<div class="m2"><p>دیگر اسرار بجز ناله و افغان چه کند</p></div></div>