---
title: >-
    شمارهٔ ۱۳۰
---
# شمارهٔ ۱۳۰

<div class="b" id="bn1"><div class="m1"><p>علی صدغ لیلی تهب النسیم</p></div>
<div class="m2"><p>از این غصه دل اوفتاده دو نیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر آنکس که چشم ترا دید و گفت</p></div>
<div class="m2"><p>الا ان هذا لسحر عظیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رقیبش بما بر سر خشم بود</p></div>
<div class="m2"><p>قنا ربنا ذالعذاب الالیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بهاران شد و میدمد گل ز شاخ</p></div>
<div class="m2"><p>فدعنی و کاساً رحیقاً ندیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو مردم بخاکم فشانید می</p></div>
<div class="m2"><p>لیحیی المرام العظام الرمیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>فتاده است اسرار شورم بسر</p></div>
<div class="m2"><p>بذکری لسملی و عهد قدیم</p></div></div>