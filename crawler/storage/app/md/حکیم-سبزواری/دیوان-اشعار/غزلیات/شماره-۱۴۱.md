---
title: >-
    شمارهٔ ۱۴۱
---
# شمارهٔ ۱۴۱

<div class="b" id="bn1"><div class="m1"><p>آنکه شیران را کشیدی در شطن</p></div>
<div class="m2"><p>وانکه پیلان را نشاندی در عطن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وانکه جاکردی بفرق فرقدین</p></div>
<div class="m2"><p>بلکه بالاتر ز فرقد یا پرن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نی همین اقلیم ظاهر راشه است</p></div>
<div class="m2"><p>هست میر ما ظهر مع ما بطن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نی همین مهرجهان را صورت است</p></div>
<div class="m2"><p>ملک معنی را بود پرتوفکن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خاتم الملک سمی الخاتم</p></div>
<div class="m2"><p>قلبه مرآت ذات ذی المنن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>الذی خیر القرون قرنه</p></div>
<div class="m2"><p>قرن ذی القرنین و الویس قرن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شاهدان کاورده تاریخ جلوس</p></div>
<div class="m2"><p>عهده خیر قرون کلک من</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون نهد در رزمگه پاخصم را</p></div>
<div class="m2"><p>در بنای هستی افتد بومهن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در خراسان یک شرر قهرش کنند</p></div>
<div class="m2"><p>مرغزاران هری شد مرغزن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چارمین شاه است از قاجار کو</p></div>
<div class="m2"><p>علت غائی بود زان چار تن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شد چهل سال و نگفت اسرار مدح</p></div>
<div class="m2"><p>لیک حسن شه بود پیمان شکن</p></div></div>