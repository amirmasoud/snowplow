---
title: >-
    شمارهٔ ۳۴
---
# شمارهٔ ۳۴

<div class="b" id="bn1"><div class="m1"><p>جرعهٔ ما را ز لعل می پرستش مشکل است</p></div>
<div class="m2"><p>گوشه چشمی بما از چشم مستش مشکل است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آنکه عالم را به تیغ بی نیازی قتل کرد</p></div>
<div class="m2"><p>گر بیارد در حساب مزد دستش مشکل است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پسته تنگ دهانش نکته سر بسته ایست</p></div>
<div class="m2"><p>حرف ازآن سرّی که بر گل سرببستش مشکل است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عشق بی پروا کجا و عقل پر اندیشه کو</p></div>
<div class="m2"><p>دام برچین کین هما باما نشستش مشکل است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر برهمن بینی و گراهرمن ور پارسا</p></div>
<div class="m2"><p>آنکه نبود مست از جام الستش مشکل است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آنکه عالم را بمستوری کند شیدای خویش</p></div>
<div class="m2"><p>چون درآید ساغر صهبا بدستش مشکل است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>طایر دل را خلاصی نیست از دامت بلی</p></div>
<div class="m2"><p>رستن مرغی که زلفت پای بستش مشکل است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>وصف آن رخسار با اسرار هم زان یاردان</p></div>
<div class="m2"><p>کان نمودی را که نبود بودهستش مشکل است</p></div></div>