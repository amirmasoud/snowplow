---
title: >-
    شمارهٔ ۱۸
---
# شمارهٔ ۱۸

<div class="b" id="bn1"><div class="m1"><p>وجودش بس ز حق دارد مزایا</p></div>
<div class="m2"><p>غدانی مریة منه البرایا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل از من برده شوخ مه لقائی</p></div>
<div class="m2"><p>تناهی حسنه اقصی القصایا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بتی سنگین دلی سیمین عذاری</p></div>
<div class="m2"><p>صبیح الوجه مرضی السجایا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ملاحتهای شیرینان پر شور</p></div>
<div class="m2"><p>عکوس من محیاه مرایا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بفردوسم مخوان از خلد رویش</p></div>
<div class="m2"><p>فمن خلی النقود بالنسایا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز صبح طلعت و زلف شب آساش</p></div>
<div class="m2"><p>غدت غدوات ایامی عشایا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سخن کوته بود در وصف قدش</p></div>
<div class="m2"><p>مدی الاعمار لو قلنا تحایا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو اسرار دهان و از میان داشت</p></div>
<div class="m2"><p>فقلبی فی زوایاه جنایا</p></div></div>