---
title: >-
    شمارهٔ ۱۴۰
---
# شمارهٔ ۱۴۰

<div class="b" id="bn1"><div class="m1"><p>از روز ازل می خور و رندانه سرشتیم</p></div>
<div class="m2"><p>برجبهه بجز قصّهٔ عشقت ننوشتیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زاهد تو بما دعوت فردوس مفر ما</p></div>
<div class="m2"><p>ما باغ بهشت از پی دیدار بهشتیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از عشق نکوهش منما خسته دلان را</p></div>
<div class="m2"><p>کز خامهٔ صنعیم چه زیبا و چه زشتیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جامی بکف آرید و بنوشید عزیزان</p></div>
<div class="m2"><p>فرداست که بر تارک خم ماهمه خشتیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اندر طلبت گه بحرم گاه بدیریم</p></div>
<div class="m2"><p>گه معتکف مسجد و گاهی بکنشتیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دادند نخستین چو بما کلک دبیری</p></div>
<div class="m2"><p>غیر از الف قد تو بردل ننوشتیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شد حلهٔ دارا به برو برد یمانی</p></div>
<div class="m2"><p>درکارگه فقر هر آن رشته که رشتیم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون رشته شدم بلکه شوم زال خریدار</p></div>
<div class="m2"><p>خود طرف نبستیم از این رشته که رشتیم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کی برخوری اسرار ز خاری که نشاندیم</p></div>
<div class="m2"><p>کی خرمنی اندوزی از این تخم که کشتیم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>اسرار دل اسرار سراز سد ره بر آورد</p></div>
<div class="m2"><p>باری درویدیم هر آن تخم که کشتیم</p></div></div>