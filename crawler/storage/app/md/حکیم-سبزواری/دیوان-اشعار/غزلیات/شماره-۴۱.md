---
title: >-
    شمارهٔ ۴۱
---
# شمارهٔ ۴۱

<div class="b" id="bn1"><div class="m1"><p>دمی نه کار زوی مرگ بر زبانم نیست</p></div>
<div class="m2"><p>چرا که طاقت بیداد آسمانم نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بزیر تیغ تو من پر زدن هوس دارم</p></div>
<div class="m2"><p>هوای بال فشانی ببوستانم نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خوشم که نیست مراروزن از قفس سوی باغ</p></div>
<div class="m2"><p>که تاب دیدن گلچین و باغبانم نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>میان آتش و آبم ز دیده و دل خویش</p></div>
<div class="m2"><p>شبی که جای بر آن خاک آستانم نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بگوشهٔ قفسش خوگرفتهام چندان</p></div>
<div class="m2"><p>که گر رهاکندم ذوق آشیانم نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دلت چو واقف اسرار و نکته دان باشد</p></div>
<div class="m2"><p>چه غم بساحت قرب تو گر بیانم نیست</p></div></div>