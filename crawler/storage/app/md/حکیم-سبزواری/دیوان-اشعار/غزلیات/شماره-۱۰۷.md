---
title: >-
    شمارهٔ ۱۰۷
---
# شمارهٔ ۱۰۷

<div class="b" id="bn1"><div class="m1"><p>دوش بگوشم رساند نکتهٔ غیبی سروش</p></div>
<div class="m2"><p>غبغب ساقی ببوس قرقف باقی بنوش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در همه جا با همه دیده بدلدار دوز</p></div>
<div class="m2"><p>از غم عشقش بگو در ره وصلش بکوش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سینه بجار غمش تا بتوان میخراش</p></div>
<div class="m2"><p>بهر گل عارضش تا بتوان میخروش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جز ره مهرش مپوی غیر حدیثش مگوی</p></div>
<div class="m2"><p>شارع میخانه جوی سبحه بساغر فروش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تاز تو باشد اثر نبود از آنت خبر</p></div>
<div class="m2"><p>نیست در این ره بتردشمنی از عقل و هوش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر سر کوی فنا سرخوش و رندانه رو</p></div>
<div class="m2"><p>قفل خموشی بلب وزتف جان دل بجوش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نقد بلا کآورند بر سر بازار عشق</p></div>
<div class="m2"><p>گر بستانند خیز جنس دل و جان فروش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بر در پیر مغان باش کمین بنده ای</p></div>
<div class="m2"><p>دست ادب بر میان حلقهٔ فرمان بگوش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>غاشیهٔ دولتش خیل ملایک کشند</p></div>
<div class="m2"><p>هرکه بجان میکشد بار دلی را بدوش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مشرب رندی کجا مرتبهٔ زهد کو</p></div>
<div class="m2"><p>طعن برندان مزن زاهد خودبین خموش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چون ز نکوجز نکو ناید و یک بیش نیست</p></div>
<div class="m2"><p>هیچ نکوهش مکن دیدهٔ بد بین بپوش</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بندهٔ احرار شو طالب دیدار شو</p></div>
<div class="m2"><p>واقف اسرار شو پند وی از جان نیوش</p></div></div>