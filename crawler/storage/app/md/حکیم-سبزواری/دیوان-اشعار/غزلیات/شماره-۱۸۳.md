---
title: >-
    شمارهٔ ۱۸۳
---
# شمارهٔ ۱۸۳

<div class="b" id="bn1"><div class="m1"><p>ای که با نور خرد نور خدا میجوئی</p></div>
<div class="m2"><p>خویش بین عکس نظر کن به کجا میپوئی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چیست ماهیت و مرآت چه عین ثابت</p></div>
<div class="m2"><p>حد تقریب نهند اهل حقیقت سوئی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مطربار است برو راه مخالف بگذار</p></div>
<div class="m2"><p>چند از این پرده بعشاق نوا میگوئی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خار این باغ عزیز است چو گل خوارمبین</p></div>
<div class="m2"><p>تا که از گلشن توحید بیابی بوئی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هرچه زیبنده ز چیزیست مخواه از دگری</p></div>
<div class="m2"><p>سیمی از روئی و آهن صفتی از روئی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خضر خطت که خورد آب حیات از دهنت</p></div>
<div class="m2"><p>بین که پهلو زندش اهرمن گیسوئی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آن چنان طوطی اسرار شدی نغمه سرا</p></div>
<div class="m2"><p>که همه دفتر ارباب خرد میشوئی</p></div></div>