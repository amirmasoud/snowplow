---
title: >-
    شمارهٔ ۸
---
# شمارهٔ ۸

<div class="b" id="bn1"><div class="m1"><p>گرفته سبزه و گل روی صحرا</p></div>
<div class="m2"><p>سقاک اللّه ساقی هات خمراً</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز هجرانت بسوزیم و بسازیم</p></div>
<div class="m2"><p>لعل اللّه یحدث بعد امراً</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>وفا در عهد حسنت گشته نایاب</p></div>
<div class="m2"><p>احسن العهد للحسناه یدری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز لعلت جرعهٔ روزی چشیدیم</p></div>
<div class="m2"><p>فاحسوا من دمآء القلب دهراً</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دلم بگداخت از سوز فراغت</p></div>
<div class="m2"><p>فاجفانی الدمآیهطلن قطراً</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>فروغ رخ ز تار موی بنما</p></div>
<div class="m2"><p>ارینی فی بهیم اللیل قجراً</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فروزی آتش طلعت بهر بزم</p></div>
<div class="m2"><p>باحشائی لقد سعرت جمراً</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به پیش گلشن فردوس رویش</p></div>
<div class="m2"><p>دعوا عنا ریاحینا و زهراً</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دهانت سر اسرار الهی است</p></div>
<div class="m2"><p>فقل و اکشف لسرفیک ستراً</p></div></div>