---
title: >-
    شمارهٔ ۵۶
---
# شمارهٔ ۵۶

<div class="b" id="bn1"><div class="m1"><p>باز یار بیوفای ما سر یاریش نیست</p></div>
<div class="m2"><p>ذرهٔ آن ماه مهر آسا وفادریش نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بخت من درخواب گویاروی زیبای تو دید</p></div>
<div class="m2"><p>زانکه عمری شدکه درخوابست و بیداریش نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مردآیادر قفس یا با خیالت خوگرفت</p></div>
<div class="m2"><p>مرغ دل کومدتی شد ناله و زاریش نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ما و دل بودیم کو اندیشهٔ ما داشتی</p></div>
<div class="m2"><p>لیک صدفریاد کآن هم تاب غمخواریش نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تکیه بر دل داده مژگانش ز بیداری چشم</p></div>
<div class="m2"><p>آری آری بیش ازاین تاب پرستاریش نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ترسم از بس چشم من خون از مژه جاری کنی</p></div>
<div class="m2"><p>مردمان گویند یارت بیمی از یاریش نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>روی آزادی مدام اسرار کی دید از قیود</p></div>
<div class="m2"><p>مرغ دل کاندر خم زلفی گرفتاریش نیست</p></div></div>