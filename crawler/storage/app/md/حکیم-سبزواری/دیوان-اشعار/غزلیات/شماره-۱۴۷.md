---
title: >-
    شمارهٔ ۱۴۷
---
# شمارهٔ ۱۴۷

<div class="b" id="bn1"><div class="m1"><p>از بهترین سلالهٔ آدم توئی بهین</p></div>
<div class="m2"><p>بر مهترین کلالهٔ حوا توئی مهین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در خاتم رسالتی ای ختم انبیا</p></div>
<div class="m2"><p>همچو نگین به خاتم و چون نقش درنگین</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تو بَدرِ ازهری و همه انبیا سُها</p></div>
<div class="m2"><p>تو مهر انوری و نجومند مرسلین</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بحر است علم و طفل دبستانت ار بود</p></div>
<div class="m2"><p>آن بحر بیکران و پر از لؤلؤ ثمین</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پیشت خرد زدانش اگردم زند چنانست</p></div>
<div class="m2"><p>کاید مگس بعرصهٔ عنقا کند طنین</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اندر بیان بدیع معانی حکمتت</p></div>
<div class="m2"><p>چون در شکر حلاوت و شهد اندر انگبین</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از شوق ذروهٔ تو فلاطون فیلسوف</p></div>
<div class="m2"><p>مست و خراب بوده و چون باده خم نشین</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اسرار در جمال و جلال تو فانی است</p></div>
<div class="m2"><p>صل علیک ثم علی آل اجمعین</p></div></div>