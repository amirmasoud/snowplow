---
title: >-
    شمارهٔ ۱۱۵
---
# شمارهٔ ۱۱۵

<div class="b" id="bn1"><div class="m1"><p>جدا شد از بر من یار گلعذار دریغ</p></div>
<div class="m2"><p>دریغ از ستم چرخ بیم دار دریغ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نمود ساکن بیت الحزن چو یعقوبم</p></div>
<div class="m2"><p>ربود یوسف من گرگ روزگار دریغ</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چمن شگفت و مرا عقدهٔ ز دل نگشود</p></div>
<div class="m2"><p>گلی نچیدم و بگذشت نوبهار دریغ</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>معلمی که ورق پیش من نهاد آغاز</p></div>
<div class="m2"><p>نوشت بر سبق من نخست بار دریغ</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>میان دایرهٔ غم چو نقطهایم اسرار</p></div>
<div class="m2"><p>تمام عمر گذشتی بدین مداردریغ</p></div></div>