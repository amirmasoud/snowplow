---
title: >-
    شمارهٔ ۶۸
---
# شمارهٔ ۶۸

<div class="b" id="bn1"><div class="m1"><p>دل و دین می کنی یغما بدین رخ</p></div>
<div class="m2"><p>جهان گشتم ندیدم اینچنین رخ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چه آتش پارهٔ بگرفته مأوا</p></div>
<div class="m2"><p>بکانون دلم ز آن آتشین رخ</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بشکر خنده زد آن انگبین لب</p></div>
<div class="m2"><p>بنسرین طعنه زد آن یاسمین رخ</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نیاز آرند خیل نازنینان</p></div>
<div class="m2"><p>بر آن سرو ناز نازنین رخ</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نهند بر آستان سر منکرانت</p></div>
<div class="m2"><p>ید و بیضا چو آرد ز آستین رخ</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز خط خضر بود آب بقانوش</p></div>
<div class="m2"><p>ز لب عیسی دم گردون نشین رخ</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از آن زلف و جبین در مجمع حسن</p></div>
<div class="m2"><p>نموده کفر و دین باهم قرین رخ</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سوی صورتگر چین گر خرامی</p></div>
<div class="m2"><p>بگوید مرحبا حسن آفرین رخ</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو اسرار الهی پرده پوش است</p></div>
<div class="m2"><p>مگر مرآت حق بینی است این رخ</p></div></div>