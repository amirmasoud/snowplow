---
title: >-
    شمارهٔ ۷۳
---
# شمارهٔ ۷۳

<div class="b" id="bn1"><div class="m1"><p>دل خود تنگ ز غنچه دهنی باید ساخت</p></div>
<div class="m2"><p>روز خود تیره ز زلف سیهی باید کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خاطر خویش پریشان ز پریشان موئی</p></div>
<div class="m2"><p>دل شکسته ز شکست کلهی باید کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مصر دل بایدت از بهر عزیزی آراست</p></div>
<div class="m2"><p>یوسف جان بدر از قعر چهی باید کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا بکی معتکف کاخ هوس باید بود</p></div>
<div class="m2"><p>کاروان رفت دلا رو برهی باید کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای که از مهر رخ تست فروغ دو جهان</p></div>
<div class="m2"><p>فکر بهبودی بخت تبهی باید کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خواجگان را به غلامان نظری باید بود</p></div>
<div class="m2"><p>محتشم را به حشم رحم گهی باید کرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سرگران این همه با ناز نمی باید رفت</p></div>
<div class="m2"><p>به شهید ره خود هم نگهی باید کرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نار اسرار چو نور است ازانرو که ازاوست</p></div>
<div class="m2"><p>طاعتی گر ننمودی گنهی باید کرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بوی زلف بیقراری برقرارم میرسد</p></div>
<div class="m2"><p>نافهٔ آهوی چین مُشک تتارم میرسد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>باد عنبر بوست گوئی آید از شهر ختن</p></div>
<div class="m2"><p>نی خطا گفتم ز چین زلف یارم میرسد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گردراهش مردمان روبند با مژگان چشم</p></div>
<div class="m2"><p>کان زمان از گرد ره آن شهسوارم میرسد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تا رساند مژدهٔ وصلت سوی دل هر نفس</p></div>
<div class="m2"><p>پیک آهی از دل امیدوارم میرسد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>رخش تازان سرشکم سرخ رودرتاز و تک</p></div>
<div class="m2"><p>کف زنان مژگان که شاه تاجدارم میرسد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>صفحهٔ جان پاک کن اسرار از نقس دوئی</p></div>
<div class="m2"><p>شهر دل آئین ببند آن شهریارم می رسد</p></div></div>