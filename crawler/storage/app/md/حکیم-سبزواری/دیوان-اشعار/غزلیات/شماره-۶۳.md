---
title: >-
    شمارهٔ ۶۳
---
# شمارهٔ ۶۳

<div class="b" id="bn1"><div class="m1"><p>جام جم مظهر اعظم دل درویشان است</p></div>
<div class="m2"><p>نخبهٔ جملهٔ عالم دل درویشان است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>طاعت و زهد ریائی همه بیحاصلی است</p></div>
<div class="m2"><p>بجز از عشق که او حاصل درویشان است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نقد عالم همه قلب است ولی نقد صحیح</p></div>
<div class="m2"><p>کیمیای نظر کامل درویشان است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بی نیاز از دو جهان زندهٔ جاوید شود</p></div>
<div class="m2"><p>هر که از فقر و فنا بسمل درویشان است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رجعت آل چو قائم به فنا در آل است</p></div>
<div class="m2"><p>جذب این سلسله بر کاهل درویشان است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بگذر از مرحله ریب و ریا ای سالک</p></div>
<div class="m2"><p>رو بصدق آر که سر منزل درویشان است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آن مغاکی که بود کوی خموشان نامش</p></div>
<div class="m2"><p>دانی البته که او محفل درویشان است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آتش آن نیست که در وادی ایمن زدهاند</p></div>
<div class="m2"><p>آتش آنست که اندر دل درویشان است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>باید اسرار گهر سفت و دُرر ّبهر نثار</p></div>
<div class="m2"><p>که نه هر سنگ و گلی قابل درویشان است</p></div></div>