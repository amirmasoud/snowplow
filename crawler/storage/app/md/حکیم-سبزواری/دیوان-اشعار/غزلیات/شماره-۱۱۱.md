---
title: >-
    شمارهٔ ۱۱۱
---
# شمارهٔ ۱۱۱

<div class="b" id="bn1"><div class="m1"><p>دمیده بر رخ آن نازنین خط</p></div>
<div class="m2"><p>بنفشه سان بگرد یاسمین خط</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جهان گیرد بخط دور لعلش</p></div>
<div class="m2"><p>سلیمان است و دارد برنگین خط</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ببین جوشیده بر سر چشمهٔ نوش</p></div>
<div class="m2"><p>مثال مور گرد انگبین خط</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نکرده تا نوشته کلک تقدیر</p></div>
<div class="m2"><p>رقم بر صفحهٔ روی چنین خط</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>برای حفظ او دست خداوند</p></div>
<div class="m2"><p>رقم کرده بر آن لوح جبین خط</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو خطت کلک مانی کم کشیده</p></div>
<div class="m2"><p>نبسته اینچنین نقاش چین خط</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بود سرخط آزادی اسرار</p></div>
<div class="m2"><p>ویامنشور نیکوئی است این خط</p></div></div>