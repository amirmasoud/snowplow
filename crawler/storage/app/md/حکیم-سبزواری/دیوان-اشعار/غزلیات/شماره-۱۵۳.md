---
title: >-
    شمارهٔ ۱۵۳
---
# شمارهٔ ۱۵۳

<div class="b" id="bn1"><div class="m1"><p>ای مهر همچو مه ز رخت کرده کسب ضو</p></div>
<div class="m2"><p>خال رخ تو برده ز مشک ختن گرو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از طرف بام چرخ برین باد و صد هراس</p></div>
<div class="m2"><p>سر میکشد برای تماشات ماه نو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بینم خراب حال دل ای عیسوی نفس</p></div>
<div class="m2"><p>پا از سرم مکش نفسی از برم مرو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در هر دلی که عشق بر افراشت رایتی</p></div>
<div class="m2"><p>او رنگ سلطنت چه و طرف کلاه کو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در جان آنکه تخم محبت نکاشتند</p></div>
<div class="m2"><p>باشد هزار خرمن طاعت به نیم جو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>برق سبک عنان هوا آنقدر نداد</p></div>
<div class="m2"><p>مهلت دل مرا که کند کشت خود درو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اسرار جام جم طلبی پیش پیر دیر</p></div>
<div class="m2"><p>جامی بنوش و غافل از اسرار خود مشو</p></div></div>