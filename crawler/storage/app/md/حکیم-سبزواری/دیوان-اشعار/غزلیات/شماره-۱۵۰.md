---
title: >-
    شمارهٔ ۱۵۰
---
# شمارهٔ ۱۵۰

<div class="b" id="bn1"><div class="m1"><p>حرف اغیار دغا در حق یاران مشنو</p></div>
<div class="m2"><p>آشنایان بگذار و پی بیگانه مرو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای که در مزرع روی تو دهد حاصل مهر</p></div>
<div class="m2"><p>بینوایم بنوازم که رسد وقت درو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بامیدی که بابروت مشابه گردد</p></div>
<div class="m2"><p>ز ریاضت شده چون موی میانت مه نو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پیش آنروی گل و سنبل و زلفی که ترا است</p></div>
<div class="m2"><p>خرمن مه بجوی خوشه پروین بدو جو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جز به آن مطلع انوار که دید و که شنید</p></div>
<div class="m2"><p>که بود مهر درخشنده قرین با مه نو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ترسم این دلق ملمع که تو داری اسرار</p></div>
<div class="m2"><p>می فروشش بیکی جرعه نگیرد بگرو</p></div></div>