---
title: >-
    شمارهٔ ۴۹
---
# شمارهٔ ۴۹

<div class="b" id="bn1"><div class="m1"><p>ای آفت جان ها خم ابروی کمندت</p></div>
<div class="m2"><p>غارت گر دل ها قد دل جوی بلندت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا آفت چشمت نرسد دست حق افشاند</p></div>
<div class="m2"><p>بر آتش رخسار تو از خال سپندت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای ترک سمنبر بسرم تاز سمندی</p></div>
<div class="m2"><p>گوی خم چوگان سرخوبان خجندت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>افتاده خلاصیش به فردای قیامت</p></div>
<div class="m2"><p>هر صید که گردیده گرفتار به بندت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شد رشک فلک روی زمین تا که نشسته</p></div>
<div class="m2"><p>برخاک هلال از اثر لعل سمندت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اندام تو خود قاقم و خزاست زنرمی</p></div>
<div class="m2"><p>سودی ندهد جامهٔ دیبا و برندت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دارد سر یغما شد من غمزهٔ شوخت</p></div>
<div class="m2"><p>اینک دل و جانی اگر این هست پسندت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا دفع عوارض بشود زان گل عارض</p></div>
<div class="m2"><p>یک بوسه بما ده بزکواة از لب قندت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ناصح چه دهی پند باسرار ز عشقش</p></div>
<div class="m2"><p>او نیست از آنها که دهد گوش به پندت</p></div></div>