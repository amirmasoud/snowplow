---
title: >-
    شمارهٔ ۲۸
---
# شمارهٔ ۲۸

<div class="b" id="bn1"><div class="m1"><p>ای ماه جبین سیم غبغب</p></div>
<div class="m2"><p>وی سیم ذقن بت شکر لب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بی ماه رخت شبان تیره</p></div>
<div class="m2"><p>کارم همه دم فغان و یارب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>لبریز شراب ناب جامت</p></div>
<div class="m2"><p>وز خون جگر دلم لبالب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بتوان دو سه گام رنجه کردن</p></div>
<div class="m2"><p>بالین مریض خویش یکشب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای اختر حسن چهره بنمای</p></div>
<div class="m2"><p>تا آنکه شوم خجسته کوکب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>می نوشی و عشق کار اسرار</p></div>
<div class="m2"><p>ای کاش نگردد او ز مذهب</p></div></div>