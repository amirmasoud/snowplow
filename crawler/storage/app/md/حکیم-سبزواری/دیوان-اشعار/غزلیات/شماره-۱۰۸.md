---
title: >-
    شمارهٔ ۱۰۸
---
# شمارهٔ ۱۰۸

<div class="b" id="bn1"><div class="m1"><p>مه آیینه داری است از طلعتش</p></div>
<div class="m2"><p>قیامت نموداری از قامتش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صفای ارم نزهت باغ خلد</p></div>
<div class="m2"><p>همه مستعار است از صفوتش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ملیحان و کان ملاحت تمام</p></div>
<div class="m2"><p>بود زیر بار حق نعمتش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بقد سر و آزاد در بندگیش</p></div>
<div class="m2"><p>یکی خانه زادیست در ساحتش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همانا که یعقوب در پیرهن</p></div>
<div class="m2"><p>شنیده است یک شمه از نکهتش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ببزمش دلاشمع نامحرم است</p></div>
<div class="m2"><p>کجا باریابی تو در حضرتش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز بس داغش اسرار دارد بدل</p></div>
<div class="m2"><p>نروید بجز لاله از تربتش</p></div></div>