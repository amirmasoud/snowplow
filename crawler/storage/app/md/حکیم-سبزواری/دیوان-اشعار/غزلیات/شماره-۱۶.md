---
title: >-
    شمارهٔ ۱۶
---
# شمارهٔ ۱۶

<div class="b" id="bn1"><div class="m1"><p>شهنشهی طلبی باش چاکر فقرا</p></div>
<div class="m2"><p>گدای خاک نشینی شو از در فقرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر آرزوست ترا فیض جام جم بردن</p></div>
<div class="m2"><p>بکش بمیکده دردی ز ساغر فقرا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بنجم ثابت و سیار گنبددوار</p></div>
<div class="m2"><p>رسد فروغ ز فرخنده اختر فقرا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ببر بمنظر کامل عیارشان مس قلب</p></div>
<div class="m2"><p>که خاک تیره شود رز ز منظر فقرا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همی دهند و ستانند خسروان را تاج</p></div>
<div class="m2"><p>بود دو کون عطای محقر فقرا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گرت بر آینهٔ دل نشسته زنگ خلاف</p></div>
<div class="m2"><p>بکن مقابله بارای انور فقرا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مبین مرقع خاکی چه دروی اخگرهاست</p></div>
<div class="m2"><p>نهفته اند به خاکستر آذر فقرا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو ملک تن بود اقلیم دل قلمروشان</p></div>
<div class="m2"><p>اگرچه تاج نمد باشد افسر فقرا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بر اهل فقر مکن فخر خواندی ار ورقی</p></div>
<div class="m2"><p>به سینه لوحهٔ دل هست دفتر فقرا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کنند شیر فلک رام همچو گاو زمین</p></div>
<div class="m2"><p>اگرچه مثل هلال است پیکر فقرا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گرت هوا است که عین الحیوة ظلمت چیست</p></div>
<div class="m2"><p>سواد دیده در آن خاک معبر فقرا</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مرا بدولت فقر آن دلیل روشن بس</p></div>
<div class="m2"><p>که فخر میکند از فقر سرور فقرا</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بود چو فقر سیه کردن خودی ز وجود</p></div>
<div class="m2"><p>چو خال گونه بود زیب و زیور فقرا</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ز فخر پا نهد اسرار بر فراز دوکون</p></div>
<div class="m2"><p>نهند نام گراو را سگ در فقرا</p></div></div>