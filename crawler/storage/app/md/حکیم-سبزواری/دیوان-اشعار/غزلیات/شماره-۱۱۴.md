---
title: >-
    شمارهٔ ۱۱۴
---
# شمارهٔ ۱۱۴

<div class="b" id="bn1"><div class="m1"><p>شمع رویش چو برافروخت ببزم ابداع</p></div>
<div class="m2"><p>همچو انجام در آغاز یکی داشت شعاع</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تافت بر طلعت ساقی پس از آن برباده</p></div>
<div class="m2"><p>آمدی مجلسیان را بنظر این اوضاع</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جلوه یکتا و مجالی بودش گوناگون</p></div>
<div class="m2"><p>هست در عین تفرد به هزاران انواع</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نبود بیش ز یک پرده نوای عشاق</p></div>
<div class="m2"><p>بر مخالف ره این راست نیاید بسماع</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نور و نار و گل و خار از ره هستی است یکی</p></div>
<div class="m2"><p>بشنو این کان سخنان دگر آرند صداع</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>فتنهها آمده از سر میانت بمیان</p></div>
<div class="m2"><p>از میان پرده برانداز و برانداز نزاع</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>این جهان چیست که کس زهدبورزداروی</p></div>
<div class="m2"><p>بس کساد است ببازار تو اینگونه متاع</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ای که جوئی در دلدار بیا بر در دل</p></div>
<div class="m2"><p>وی که پوئی ره اسرار بکن خویش وداع</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ای که جوئی در دلدار بیا بر در دل</p></div>
<div class="m2"><p>وی که پوئی ره اسرار بکن خویش وداع</p></div></div>