---
title: >-
    شمارهٔ ۲ - وله ایضاً
---
# شمارهٔ ۲ - وله ایضاً

<div class="b" id="bn1"><div class="m1"><p>ای از تو بهر چمن بهر گل بوئی</p></div>
<div class="m2"><p>هر چیزی را بیاد تو یاهوئی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کوی تو بود کعبهٔ مقصود همه</p></div>
<div class="m2"><p>‌‌اقطار بمرکز آید از هر سوئی</p></div></div>