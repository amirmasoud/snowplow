---
title: >-
    شمارهٔ ۵ - وله ایضاً
---
# شمارهٔ ۵ - وله ایضاً

<div class="b" id="bn1"><div class="m1"><p>ای حاجب ابروی تو هرابروئی</p></div>
<div class="m2"><p>از روی تو آب روی هر دلجوئی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حسن همه زان تست بل عشق همه</p></div>
<div class="m2"><p>‌‌در هر کوئی ز تست گفتگوئی</p></div></div>