---
title: >-
    شمارهٔ ۱۱ - و له ایضاً
---
# شمارهٔ ۱۱ - و له ایضاً

<div class="b" id="bn1"><div class="m1"><p>از فرقت آن سیمتن ماه جبین</p></div>
<div class="m2"><p>شد همچو قلم جسم من ز ارحزین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مسطرزده نامهٔ نوشتم سوی دوست</p></div>
<div class="m2"><p>‌‌یعنی تنم از هجر تو گردیده چنین</p></div></div>