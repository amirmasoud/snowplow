---
title: >-
    شمارهٔ ۲ - مناجات
---
# شمارهٔ ۲ - مناجات

<div class="b" id="bn1"><div class="m1"><p>خداوندا دلم لبریز غم کن</p></div>
<div class="m2"><p>درون درد پروردی کرم کن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پر از نوش محبت کن ایاغم</p></div>
<div class="m2"><p>ز جام عاشقی تر کن دماغم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز صهبای شهودم کن چنان مست</p></div>
<div class="m2"><p>که نشناسم سر از پا پای از دست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کلید گنج معنی کن بیانم</p></div>
<div class="m2"><p>شکر بار از حقیقت کن زبانم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چنان سرگرم عشق خود بسازم</p></div>
<div class="m2"><p>که نرد عشق جز با تو نبازم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سر از عشق تهی در گور بادا</p></div>
<div class="m2"><p>هر آنکه جز تو بیند کوربادا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>غلط گفتم جز او کی در میان بود</p></div>
<div class="m2"><p>کجا از غیر او نام و نشان بود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چگویم از جمال آفتابش</p></div>
<div class="m2"><p>که عین بی حجابی شد حجابش</p></div></div>