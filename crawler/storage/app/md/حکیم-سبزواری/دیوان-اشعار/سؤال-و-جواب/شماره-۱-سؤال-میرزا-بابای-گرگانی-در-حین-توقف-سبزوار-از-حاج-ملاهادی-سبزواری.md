---
title: >-
    شمارهٔ ۱ - سؤال میرزا بابای گرگانی در حین توقف سبزوار از حاج ملاهادی سبزواری
---
# شمارهٔ ۱ - سؤال میرزا بابای گرگانی در حین توقف سبزوار از حاج ملاهادی سبزواری

<div class="b" id="bn1"><div class="m1"><p>ای حکیمی که چون تو فرزندی</p></div>
<div class="m2"><p>مادر دهر در زمانه نزاد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وادی عشق را توئی هادی</p></div>
<div class="m2"><p>سالکان طریق را تو مراد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از تو بستان معرفت خرم</p></div>
<div class="m2"><p>وز تو ایوان معدلت آباد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بحر توحید را توئی زورق</p></div>
<div class="m2"><p>شهر تجرید را توئی استاد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هم کنوز ورموز سر وجود</p></div>
<div class="m2"><p>در نهاد تو کردگار نهاد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر تو و چون توئی نبود مراد</p></div>
<div class="m2"><p>ننمودی خدای خلق ایجاد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چیست اقرار فضل تو ایمان</p></div>
<div class="m2"><p>کیست انکار امر توالحاد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون کلید خزائن دانش</p></div>
<div class="m2"><p>بر کف قدرت تو قادر داد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سر این نکته را بیان فرما</p></div>
<div class="m2"><p>تا شود قلب مستمندان شاد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در سه جاموت دادهاند نشان</p></div>
<div class="m2"><p>عارفان طریق را ارشاد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>زان یکی ذاتی است و آندیگر</p></div>
<div class="m2"><p>اضطراری است در جمیع عباد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>واندگر هست اختیاری شخص</p></div>
<div class="m2"><p>کو بتاراج زندگانی داد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>زندهٔ مرده چون تو اندزیست</p></div>
<div class="m2"><p>مردهٔ زنده چون کند دلشاد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ور خمولی گزیند و عزلت</p></div>
<div class="m2"><p>هستی خویش را دهد بر باد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>حکمت و عفت و شجاعت و عدل</p></div>
<div class="m2"><p>همه افتد ز کار همچو جماد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>شهوتی گر نبود عفت نیست</p></div>
<div class="m2"><p>کافرار نیست بهر چیست جهاد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ور رضا بر قضای ربانی</p></div>
<div class="m2"><p>داد گوید هر آنچه بادا باد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>قوت اطفال و کسب رزق حلال</p></div>
<div class="m2"><p>امر فرمود سید امجاد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ور بتحصیل رزق پردازد</p></div>
<div class="m2"><p>در میان گروه بی بنیاد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>روز و شب صاحبان نحوت و آز</p></div>
<div class="m2"><p>فارغش کی کنند از الحاد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>مرده با زندگان بخل و حسد</p></div>
<div class="m2"><p>کی تواند نمود او اسعاد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>نیست ما را چو چشم دل روشن</p></div>
<div class="m2"><p>صد نماید بچشم ما آحاد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>راه باریک و دور و ویرانست</p></div>
<div class="m2"><p>شب تاریک و کور مادرزاد</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>گر ز برهان عقلی و نقلی</p></div>
<div class="m2"><p>راه مقصود را کنی ارشاد</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>در دو عالم خدای هر دو جهان</p></div>
<div class="m2"><p>قدرت افزون کند و قرب زیاد</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>لیک منظوم میرود مسئول</p></div>
<div class="m2"><p>گر کنی ز التفات خود انشاد</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بعد ماو شما بعمر دراز</p></div>
<div class="m2"><p>نفع گیرند اهل علم و سداد</p></div></div>
<div class="n" id="bn28"><p>روحی فداک کمترین درباب حدیث موتوا قبل ان تموتواحیران و سرگردانم</p></div>
<div class="b" id="bn29"><div class="m1"><p>ما بدین مقصد عالی نتوانیم رسید</p></div>
<div class="m2"><p>هم مگر لطف شما پیش نهد گامی چند</p></div></div>
<div class="n" id="bn30"><p>چشم بصیرت کور و راه مقصود دور مگر بهدایت هادی طریق سعادت در این ورطهٔ هلاکت جانی بسلامت بیرون برده از چاه ضلالت بدر آئیم و ببرهان عقلی و نقلی آن صاحب دانش و بینش ناسوران سوختگان آتش حسرت مرهم پذیر شود چون استدعا از بندگان عالی چنان بود که چند کلمه منظوم مرقوم فرمایند از این جهت گستاخی شد جواب سئوال منظوم استدعا نمودم و تا بحال نظم و غزلی معروض نشده این هم از التفات سرکار است.</p></div>
<div class="b" id="bn31"><div class="m1"><p>ما چو نائیم و نوا از ما ز تست</p></div>
<div class="m2"><p>ما چو کوهیم و صدا در ماز تست</p></div></div>
<div class="n" id="bn32"><p>و اگر در سئوآل خبط و خطائی شده باشد باصلاح آن کوشیده</p></div>
<div class="b" id="bn33"><div class="m1"><p>من هیچم و کم ز هیچ هم بسیاری</p></div>
<div class="m2"><p>از هیچ و کم از هیچ نیاید کاری</p></div></div>
<div class="n" id="bn34"><p>جواب و سئوال هر دو از سرکار است (ای دعا از تو و اجابت هم ز تو) والسلام علیکم و رحمة اللّه و برکاته</p></div>