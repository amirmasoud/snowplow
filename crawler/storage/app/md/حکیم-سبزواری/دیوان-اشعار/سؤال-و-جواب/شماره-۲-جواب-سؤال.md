---
title: >-
    شمارهٔ ۲ - جواب سؤال
---
# شمارهٔ ۲ - جواب سؤال

<div class="b2" id="bn1"><p>بسم‌الله الرحمن الرحیم</p></div>
<div class="b" id="bn2"><div class="m1"><p>ای عزیزی که چون تو بابائی</p></div>
<div class="m2"><p>ایزد ابناء معرفت را داد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دایم از کوشش تو و چو توئی</p></div>
<div class="m2"><p>قوت و قوت رسد باین اولاد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>قافله عشق را توئی چو جرس</p></div>
<div class="m2"><p>مقدحه شوق را توئی چوزناد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سردی روزگار و ابنائش</p></div>
<div class="m2"><p>طبعم افسرده کرد همچو جماد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نسر طایر ز نسر شد واقع</p></div>
<div class="m2"><p>باشهٔ نظم همچو پشه فتاد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>لیک گر طبع نیست باکی نیست</p></div>
<div class="m2"><p>جو ز نصر من اللّه اسمتداد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ای که انواع مرگ پرسیدی</p></div>
<div class="m2"><p>ایزد انواع زندگیت دهاد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مرگ نبود که زندگی باشد</p></div>
<div class="m2"><p>وین نمط را بسی بود افراد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سورها ماتم است و ماتم سور</p></div>
<div class="m2"><p>فاقه باشد توانگری عباد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کثرت بی حد و حقیقت تر</p></div>
<div class="m2"><p>شدت نور و قرب سربعاد</p></div></div>
<div class="b2" id="bn12"><p>فی الموت الذاتی</p></div>
<div class="b" id="bn13"><div class="m1"><p>موت ذاتی ترقی اکوان است</p></div>
<div class="m2"><p>سوی وحدت ز عالم اضداد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>رفتن نطفه از جهان گیاه</p></div>
<div class="m2"><p>سوی حیوان پس از مقام جماد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>همچنین نفس سوی عقل و عقول</p></div>
<div class="m2"><p>شود ابدال بعد از آن اوتاد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>هرچه اندوخت در عوالم پست</p></div>
<div class="m2"><p>در جهان بلند ساخت زیاد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>می نکاهد از آن سر موئی</p></div>
<div class="m2"><p>ذلک الواحد هو الاعداد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>اضطراری موت معلومست</p></div>
<div class="m2"><p>اختیاری او چهار افتاد</p></div></div>
<div class="b2" id="bn19"><p>در بیان موتات اربعه</p></div>
<div class="b" id="bn20"><div class="m1"><p>موت ابیض که هست جوع و عطش</p></div>
<div class="m2"><p>در ریاضات یا شروط رشاد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>این سحابی است یمطر الحکمه</p></div>
<div class="m2"><p>در احادیث عالی الاسناد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ابیضاض و صفا همی آرد</p></div>
<div class="m2"><p>عکس البطنة تمیت فؤاد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>موت اخضر مرقع اندوزیست</p></div>
<div class="m2"><p>در زنی چون دراعهٔ زهاد</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>مرقعه مدرعه و استحیی</p></div>
<div class="m2"><p>گشته مروی ز سید زهاد</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>سبزیش خرمی عیش بود</p></div>
<div class="m2"><p>که قناعت کنوز لیس نقاد</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>موت اسود که شد بلای سیاه</p></div>
<div class="m2"><p>احتمال ملامت است و عناد</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>لا یخافون لومة لائم</p></div>
<div class="m2"><p>رو ز قرآن بخوان باستشهاد</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>موت احمر که رنگ خون آرد</p></div>
<div class="m2"><p>باشد این جاخلاف نفس و جهاد</p></div></div>
<div class="b2" id="bn29"><p>گفت ز اصغر بسوی اکبر باز</p>
<p>آمدیم آن نبی ز بعد جهاد</p></div>
<div class="b" id="bn30"><div class="m1"><p>مردهٔ زنده زندهٔ مرده</p></div>
<div class="m2"><p>عقدهاش دست معرفت بگشاد</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>مردهٔ زنده زندهٔ عشق است</p></div>
<div class="m2"><p>کرده نفی مراد پیش مراد</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>میت بین ایدی الغسال</p></div>
<div class="m2"><p>شلاخسر ضعیف در ره باد</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>تو باو زنده او بحق زنده</p></div>
<div class="m2"><p>اوفنا فی اللّه و توفی الاسناد</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>زندهٔ مرده مردهٔ جهل است</p></div>
<div class="m2"><p>بی خبر از خدا و راه رشاد</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>مانده درگورتن جلیس و حوش</p></div>
<div class="m2"><p>همه اهل مقابر اجساد</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>نفس گیرد ز یار بهتر خوی</p></div>
<div class="m2"><p>چه نشینی تو باقراد و جراد</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>رفته اندر سئوال کز پس مرگ</p></div>
<div class="m2"><p>کافر ار نیست بهرچیست جهاد</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>نیست آنی زمانی است این مرگ</p></div>
<div class="m2"><p>بارها مردهاند اهل وداد</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>موتوا من قبل ان تموتوانه آنست</p></div>
<div class="m2"><p>که کشد دست آدمی ز جهاد</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>کشش و کوشش از پی مرگ است</p></div>
<div class="m2"><p>تا نباشد نمیرد ام فساد</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>گر ز اوصاف مرگ میرد کس</p></div>
<div class="m2"><p>شود از غل و سلسلش آزاد</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>بدر و تقطیر هم تهور وجبن</p></div>
<div class="m2"><p>جربزه و ابلهی شره اخماد</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>باز ز اوصاف عقل باید مرد</p></div>
<div class="m2"><p>حکمت و عفت و شجاعت و داد</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>پس شجاعت رود زید قدرت</p></div>
<div class="m2"><p>حکمت خلقی هش رود بر باد</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>سهرو جوعش فی المثل آرد</p></div>
<div class="m2"><p>ذکر قیوم یا صمد را یاد</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>متخلق شود لخلق اللّه</p></div>
<div class="m2"><p>همه اسما خداش یادرهاد</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>آری از بعد طمس هیچ نماند</p></div>
<div class="m2"><p>که پس از مرگ نوش دارو داد</p></div></div>