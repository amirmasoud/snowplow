---
title: >-
    رباعی شمارهٔ ۸
---
# رباعی شمارهٔ ۸

<div class="b" id="bn1"><div class="m1"><p>این تکیه که رشک گلستان ارم است</p></div>
<div class="m2"><p>مانند حرم مکرم و محترم است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بگریز در آن از ستم چرخ که صید</p></div>
<div class="m2"><p>از هر خطر ایمن است تا در حرم است</p></div></div>