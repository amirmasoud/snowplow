---
title: >-
    رباعی شمارهٔ ۹
---
# رباعی شمارهٔ ۹

<div class="b" id="bn1"><div class="m1"><p>یک لحظه کسی که با تو دمساز آید</p></div>
<div class="m2"><p>یا با تو دمی همدم و همراز آید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از کوی تو گر سوی بهشتش خوانند</p></div>
<div class="m2"><p>هرگز نرود وگر رود باز آید</p></div></div>