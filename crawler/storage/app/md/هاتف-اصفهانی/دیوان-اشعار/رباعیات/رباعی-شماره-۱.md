---
title: >-
    رباعی شمارهٔ ۱
---
# رباعی شمارهٔ ۱

<div class="b" id="bn1"><div class="m1"><p>گر فاش شود عیوب پنهانی ما</p></div>
<div class="m2"><p>ای وای به خجلت و پریشانی ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ما غره به دین‌داری و شاد از اسلام</p></div>
<div class="m2"><p>گبران متنفر از مسلمانی ما</p></div></div>