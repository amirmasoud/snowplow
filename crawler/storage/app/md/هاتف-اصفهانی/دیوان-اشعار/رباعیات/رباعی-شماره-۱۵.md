---
title: >-
    رباعی شمارهٔ ۱۵
---
# رباعی شمارهٔ ۱۵

<div class="b" id="bn1"><div class="m1"><p>باز آی و دلم ز هجر پردرد نگر</p></div>
<div class="m2"><p>در سینهٔ گرمم نفس سرد نگر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در گوشهٔ بی‌مو نسیم تنها بین</p></div>
<div class="m2"><p>در زاویهٔ بی‌کسیم فرد نگر</p></div></div>