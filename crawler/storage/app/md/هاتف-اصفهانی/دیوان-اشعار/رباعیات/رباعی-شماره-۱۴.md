---
title: >-
    رباعی شمارهٔ ۱۴
---
# رباعی شمارهٔ ۱۴

<div class="b" id="bn1"><div class="m1"><p>باز آی و به کوی فرقتم فرد نگر</p></div>
<div class="m2"><p>وز درد فراق چهره‌ام زرد نگر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از مرگ دوای درد خود می‌طلبم</p></div>
<div class="m2"><p>بیمار نگر دوانگر درد نگر</p></div></div>