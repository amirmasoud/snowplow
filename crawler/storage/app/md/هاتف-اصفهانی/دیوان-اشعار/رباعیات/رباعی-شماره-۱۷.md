---
title: >-
    رباعی شمارهٔ ۱۷
---
# رباعی شمارهٔ ۱۷

<div class="b" id="bn1"><div class="m1"><p>مهجور تو را شب خیالی که مپرس</p></div>
<div class="m2"><p>رنجور تو را روز ملالی که مپرس</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتی هاتف چه حال داری بی من</p></div>
<div class="m2"><p>در گوشه‌ای افتاده به حالی که مپرس</p></div></div>