---
title: >-
    رباعی شمارهٔ ۲۲
---
# رباعی شمارهٔ ۲۲

<div class="b" id="bn1"><div class="m1"><p>آن گل که چو من هزار دارد بلبل</p></div>
<div class="m2"><p>دانی به سرش چیست پریشان کاکل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روئیده میان سبزه‌زاری ریحان</p></div>
<div class="m2"><p>یا سرزده در بنفشه زاری سنبل</p></div></div>