---
title: >-
    رباعی شمارهٔ ۳۶
---
# رباعی شمارهٔ ۳۶

<div class="b" id="bn1"><div class="m1"><p>هرچند که گلچهره و سیمین بدنی</p></div>
<div class="m2"><p>حیف از تو ولی که شمع هر انجمنی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای یار وفادار اگر یار منی</p></div>
<div class="m2"><p>با غیر مگو حرفی و مشنو سخنی</p></div></div>