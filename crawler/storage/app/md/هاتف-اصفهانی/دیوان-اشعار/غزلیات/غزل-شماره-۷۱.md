---
title: >-
    غزل شمارهٔ ۷۱
---
# غزل شمارهٔ ۷۱

<div class="b" id="bn1"><div class="m1"><p>منم آن رند قدح نوش که از کهنه و نو</p></div>
<div class="m2"><p>باشدم خرقه‌ای آنهم به خرابات گرو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زاهد آن راز که جوید ز کتاب و سنت</p></div>
<div class="m2"><p>گو به میخانه در آی و ز نی و چنگ شنو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>راز کونین به میخانه شود زان روشن</p></div>
<div class="m2"><p>که فتاده‌است به جام از رخ ساقی پرتو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چه کند کوه کن دلشده با غیرت عشق</p></div>
<div class="m2"><p>گر نه بر فرق زند تیشه ز رشک خسرو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر طرف غول نوا خوان جرس جنبانی است</p></div>
<div class="m2"><p>در ره عشق به هر زمزمه از راه مرو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>منزل آنجاست درین بادیه کز پا افتی</p></div>
<div class="m2"><p>در ره عشق همین است غرض از تک و دو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بستگی‌ها به ره عشق و گشایش‌ها هست</p></div>
<div class="m2"><p>بسته شد هاتف اگر کار تو دلتنگ مشو</p></div></div>