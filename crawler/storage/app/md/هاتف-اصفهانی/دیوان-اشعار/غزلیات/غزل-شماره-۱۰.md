---
title: >-
    غزل شمارهٔ ۱۰
---
# غزل شمارهٔ ۱۰

<div class="b" id="bn1"><div class="m1"><p>نوید آمدن یار دلستان مرا</p></div>
<div class="m2"><p>بیار قاصد و بستان به مژده جان مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فغان و ناله کنم صبح و شام و در دل یار</p></div>
<div class="m2"><p>فغان که نیست اثر ناله و فغان مرا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فغان که تا به گلستان شکفت گل، بادی</p></div>
<div class="m2"><p>وزید و زیر و زبر کرد آشیان مرا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرا جدا ز تو ویرانه‌ای است هر شب جای</p></div>
<div class="m2"><p>که سوخت آتش هجر تو خانمان مرا</p></div></div>