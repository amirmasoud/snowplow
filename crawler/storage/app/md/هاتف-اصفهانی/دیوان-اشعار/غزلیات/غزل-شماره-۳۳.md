---
title: >-
    غزل شمارهٔ ۳۳
---
# غزل شمارهٔ ۳۳

<div class="b" id="bn1"><div class="m1"><p>دل بوی او سحر ز نسیم صبا شنید</p></div>
<div class="m2"><p>تا بوی او نسیم صبا از کجا شنید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیگانه گفت اگر سخنی در حقم چه باک</p></div>
<div class="m2"><p>این می‌کشد مرا که ازو آشنا شنید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رازی که با تو گفتم و آنجا کسی نبود</p></div>
<div class="m2"><p>غیر از من و خدا و تو، غیر از کجا شنید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دل سوخت بر منش همه گر سنگ خاره بود</p></div>
<div class="m2"><p>غیر از تو هر که حال مرا دید یا شنید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فرخنده عاشقی که ز دلدار مهربان</p></div>
<div class="m2"><p>گر حرف مهر گفت حدیث وفا شنید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پیغام حور نشنود از خازن بهشت</p></div>
<div class="m2"><p>گوئی کز آشنا سخن آشنا شنید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نشنیدی ای دریغ و ندیدی که از کسان</p></div>
<div class="m2"><p>هاتف چها ز عشق تو دید و چها شنید</p></div></div>