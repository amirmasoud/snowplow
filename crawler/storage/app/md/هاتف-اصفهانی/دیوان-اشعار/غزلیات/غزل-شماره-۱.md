---
title: >-
    غزل شمارهٔ ۱
---
# غزل شمارهٔ ۱

<div class="b" id="bn1"><div class="m1"><p>سوی خود خوان یک رهم تا تحفه جان آرم تو را</p></div>
<div class="m2"><p>جان نثار افشان خاک آستان آرم تو را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از کدامین باغی ای مرغ سحر با من بگوی</p></div>
<div class="m2"><p>تا پیام طایر هم آشیان آرم تو را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من خموشم حال من می‌پرسی ای همدم که باز</p></div>
<div class="m2"><p>نالم و از نالهٔ خود در فغان آرم تو را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شکوه از پیری کنی زاهد بیا همراه من</p></div>
<div class="m2"><p>تا به میخانه برم پیر و جوان آرم تو را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ناله بی‌تاثیر و افغان بی‌اثر چون زین دو من</p></div>
<div class="m2"><p>بر سر مهر ای مه نامهربان آرم تو را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر نیارم بر زبان از غیر حرفی چون کنم</p></div>
<div class="m2"><p>تا به حرف ای دلبر نامهربان آرم تو را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در بهار از من مرنج ای باغبان گاهی اگر</p></div>
<div class="m2"><p>یاد از بی برگی فصل خزان آرم تو را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خامشی از قصهٔ عشق بتان هاتف چرا</p></div>
<div class="m2"><p>باز خواهم بر سر این داستان آرم تو را</p></div></div>