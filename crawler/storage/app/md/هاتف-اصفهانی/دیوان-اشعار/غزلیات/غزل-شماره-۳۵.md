---
title: >-
    غزل شمارهٔ ۳۵
---
# غزل شمارهٔ ۳۵

<div class="b" id="bn1"><div class="m1"><p>داغ عشق تو نهان در دل و جان خواهد ماند</p></div>
<div class="m2"><p>در دل این آتش جانسوز نهان خواهد ماند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آخر آن آهوی چین از نظرم خواهد رفت</p></div>
<div class="m2"><p>وز پیش دیده به حسرت نگران خواهد ماند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من جوان از غم آن تازه جوان خواهم مرد</p></div>
<div class="m2"><p>در دلم حسرت آن تازه جوان خواهد ماند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به وفای تو، من دلشده جان خواهم داد</p></div>
<div class="m2"><p>بی‌وفایی به تو ای مونس جان خواهد ماند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هاتف از جور تو اینک ز جهان خواهد رفت</p></div>
<div class="m2"><p>قصهٔ جور تو با او به جهان خواهد ماند</p></div></div>