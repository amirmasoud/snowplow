---
title: >-
    غزل شمارهٔ ۹
---
# غزل شمارهٔ ۹

<div class="b" id="bn1"><div class="m1"><p>گل خواهد کرد از گل ما</p></div>
<div class="m2"><p>خاری که شکسته در دل ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از کوی وفا برون نیائیم</p></div>
<div class="m2"><p>دامن‌گیر است منزل ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مرغان حرم ز رشک مردند</p></div>
<div class="m2"><p>چون بال فشاند بسمل ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نام گنهی نبرد تا کشت</p></div>
<div class="m2"><p>ما را به چه جرم قاتل ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کار دگر از صبا نیامد</p></div>
<div class="m2"><p>جز کشتن شمع محفل ما</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بی‌رحمی برق بین چه پرسی</p></div>
<div class="m2"><p>از کشته ما و حاصل ما</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خندد به هزار مرغ زیرک</p></div>
<div class="m2"><p>در دام تو صید غافل ما</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هاتف آخر به مکتب عشق</p></div>
<div class="m2"><p>طفلی حل کرد مشکل ما</p></div></div>