---
title: >-
    غزل شمارهٔ ۲۱
---
# غزل شمارهٔ ۲۱

<div class="b" id="bn1"><div class="m1"><p>لبم خموش ز آواز مدعا طلبی است</p></div>
<div class="m2"><p>که مدعا طلبیدن ز یار بی‌ادبی است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حکیم جام جم و آب خضر چون گوید</p></div>
<div class="m2"><p>مراد جام زجاجی و بادهٔ عنبی است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نرنجم ار سخن تلخ گویدم که ز پی</p></div>
<div class="m2"><p>شکرفشان لبش از خنده‌های زیر لبی است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شب از جفای تو می‌نالم و چو می‌نگرم</p></div>
<div class="m2"><p>همان دعای تو با ناله‌های نیمه شبی است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به یک کرشمهٔ چشم فسونگر تو شود</p></div>
<div class="m2"><p>یکی هلاک یکی زنده این چه بوالعجبی است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>برد دل از همه کس نظم او که هاتف را</p></div>
<div class="m2"><p>ملاحت عجمی و فصاحت عربی است</p></div></div>