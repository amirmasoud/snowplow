---
title: >-
    غزل شمارهٔ ۷۹
---
# غزل شمارهٔ ۷۹

<div class="b" id="bn1"><div class="m1"><p>چو نی نالدم استخوان از جدایی</p></div>
<div class="m2"><p>فغان از جدایی فغان از جدایی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قفس به بود بلبلی را که نالد</p></div>
<div class="m2"><p>شب و روز در آشیان از جدایی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دهد یاد از نیک بینی به گلشن</p></div>
<div class="m2"><p>بهار از وصال و خزان از جدایی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چسان من ننالم ز هجران که نالد</p></div>
<div class="m2"><p>زمین از فراق، آسمان از جدایی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به هر شاخ این باغ مرغی سراید</p></div>
<div class="m2"><p>به لحنی دگر داستان از جدایی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو شمعم به جان آتش افتد به بزمی</p></div>
<div class="m2"><p>که آید سخن در میان از جدایی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کشد آنچه خاشاک از برق سوزان</p></div>
<div class="m2"><p>کشیده است هاتف همان از جدایی</p></div></div>