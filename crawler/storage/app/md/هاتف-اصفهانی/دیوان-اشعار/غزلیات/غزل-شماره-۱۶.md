---
title: >-
    غزل شمارهٔ ۱۶
---
# غزل شمارهٔ ۱۶

<div class="b" id="bn1"><div class="m1"><p>چون شیشهٔ دل نه از ستم آسمان پر است</p></div>
<div class="m2"><p>مینای ما تهی است دل ما از آن پر است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای عندلیب باغ محبت گل وفا</p></div>
<div class="m2"><p>کم جو ز گلبنی که بر آن آشیان پر است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خالی است گر خم فلک از بادهٔ نشاط</p></div>
<div class="m2"><p>غم نیست چون ز می خم پیر مغان پر است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سرو تو را به تربیت من چه احتیاج</p></div>
<div class="m2"><p>نخل رطب فشان تو را باغبان پر است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جانی نماند لیک اگر جان طلب کنی</p></div>
<div class="m2"><p>بهر تن ضعیف من این نیم جان پر است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هاتف به من ز جور رقیب و جفای یار</p></div>
<div class="m2"><p>کم کن سخن که گوشم ازین داستان پر است</p></div></div>