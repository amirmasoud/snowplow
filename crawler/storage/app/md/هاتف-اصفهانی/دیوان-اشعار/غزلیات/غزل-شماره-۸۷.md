---
title: >-
    غزل شمارهٔ ۸۷
---
# غزل شمارهٔ ۸۷

<div class="b" id="bn1"><div class="m1"><p>من پس از عزت و حرمت شدم ار خار کسی</p></div>
<div class="m2"><p>کار دل بود که با دل نفتد کار کسی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دین و دنیا و دل و جان همه دادم چه کنم</p></div>
<div class="m2"><p>وای بر حال کسی کوست گرفتار کسی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ناامید است ز درمان دو بیمار طبیب</p></div>
<div class="m2"><p>چشم بیمار کسی و دل بیمار کسی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آخر کار فروشند به هیچش این است</p></div>
<div class="m2"><p>سود آن کس که به جان است خریدار کسی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هاتف این پند ز من بشنو و تا بتوانی</p></div>
<div class="m2"><p>بکش آزار کسان و مکن آزار کسی</p></div></div>