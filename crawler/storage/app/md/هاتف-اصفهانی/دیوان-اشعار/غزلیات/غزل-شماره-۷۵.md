---
title: >-
    غزل شمارهٔ ۷۵
---
# غزل شمارهٔ ۷۵

<div class="b" id="bn1"><div class="m1"><p>مهر رخسار و مه جبین شده‌ای</p></div>
<div class="m2"><p>آفت دل بلای دین شده‌ای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مهر و مه را شکسته‌ای رونق</p></div>
<div class="m2"><p>غیرت آن و رشک این شده‌ای</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پیش ازین دوست بودیم از مهر</p></div>
<div class="m2"><p>دشمن من کنون ز کین شده‌ای</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>من چنانم که پیش ازین بودم</p></div>
<div class="m2"><p>تو ندانم چرا چنین شده‌ای</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ننشستی چرا دمی با من</p></div>
<div class="m2"><p>گرنه با غیر همنشین شده‌ای</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دل ز رشکم تپد چو بسمل باز</p></div>
<div class="m2"><p>بهر صیدی که در کمین شده‌ای</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>غزلی گفته‌ای دگر هاتف</p></div>
<div class="m2"><p>که سزاوار آفرین شده‌ای</p></div></div>