---
title: >-
    غزل شمارهٔ ۶
---
# غزل شمارهٔ ۶

<div class="b" id="bn1"><div class="m1"><p>به بزمم دوش یار آمد به همراه رقیب اما</p></div>
<div class="m2"><p>شبی با او بسر بردم ز وصلش بی‌نصیب اما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مرا بی او شکیبایی چه می‌فرمائی ای همدم</p></div>
<div class="m2"><p>شکیب آمد علاج هجر دانم کو شکیب اما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز هر عاشق رموز عشق مشنو سر عشق گل</p></div>
<div class="m2"><p>ز مرغان چمن نتوان شنید از عندلیب اما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خورد هر تشنه لب آب از لب مردم فریب او</p></div>
<div class="m2"><p>از آن سرچشمه من هم می‌خورم گاهی فریب اما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به حال مرگ افتاده است هاتف ای پرستاران</p></div>
<div class="m2"><p>طبیبش کاش می‌آمد به بالین عنقریب اما</p></div></div>