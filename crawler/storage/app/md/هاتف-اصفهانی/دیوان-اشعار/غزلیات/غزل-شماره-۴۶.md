---
title: >-
    غزل شمارهٔ ۴۶
---
# غزل شمارهٔ ۴۶

<div class="b" id="bn1"><div class="m1"><p>بر دست کس افتد چو تو یاری نه و هرگز</p></div>
<div class="m2"><p>در دام کسی چون تو شکاری نه و هرگز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روزم سیه است از غم هجران بود آیا</p></div>
<div class="m2"><p>چون روز سیاهم شب تاری نه و هرگز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در بادیهٔ عشق و ره شوق رساند</p></div>
<div class="m2"><p>آزار به هر پا سر خاری نه و هرگز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گردون ستمگر کند این کار که باشد؟</p></div>
<div class="m2"><p>یاری به مراد دل یاری نه و هرگز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در خاطر هاتف همهٔ عمر گذشته است</p></div>
<div class="m2"><p>جز عشق تو اندیشهٔ کاری نه و هرگز</p></div></div>