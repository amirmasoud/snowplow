---
title: >-
    غزل شمارهٔ ۴۸
---
# غزل شمارهٔ ۴۸

<div class="b" id="bn1"><div class="m1"><p>با من ار هم آشیان می‌داشت ما را در قفس</p></div>
<div class="m2"><p>کی شکایت داشتم از تنگی جا در قفس</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عندلیبم آخر ای صیاد خود گو، کی رواست</p></div>
<div class="m2"><p>زاغ در باغ و زغن در گلشن و ما در قفس</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>قسمت ما نیست سیر گلشن و پرواز باغ</p></div>
<div class="m2"><p>بال ما در دام خواهد ریختن یا در قفس</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر من ای صیاد چون امروز اگر خواهد گذشت</p></div>
<div class="m2"><p>جز پری از من نخواهی دید فردا در قفس</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هاتف از من نغمهٔ دلکش سرودن خوش مجوی</p></div>
<div class="m2"><p>کز نوا افتاده‌ام افتاده‌ام تا در قفس</p></div></div>