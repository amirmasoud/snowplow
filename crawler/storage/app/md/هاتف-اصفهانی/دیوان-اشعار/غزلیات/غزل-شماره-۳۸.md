---
title: >-
    غزل شمارهٔ ۳۸
---
# غزل شمارهٔ ۳۸

<div class="b" id="bn1"><div class="m1"><p>به ره او چه غم آن را که ز جان می‌گذرد</p></div>
<div class="m2"><p>که ز جان در ره آن جان جهان می‌گذرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از مقیم حرم کعبه نباشد کمتر</p></div>
<div class="m2"><p>آنکه گاهی ز در دیر مغان می‌گذرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نه ز هجران تو غمگین نه ز وصلت شادم</p></div>
<div class="m2"><p>که بد و نیک جهان گذران می‌گذرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دل بیچاره از آن بیخبر است ار گاهی</p></div>
<div class="m2"><p>شکوه از جور تو ما را به زبان می‌گذرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آه پیران کهن می‌گذرد از افلاک</p></div>
<div class="m2"><p>هر کجا جلوهٔ آن تازه جوان می‌گذرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون ننالم که مرا گریه کنان می‌بیند</p></div>
<div class="m2"><p>به ره خویش و ز من خنده‌زنان می‌گذرد</p></div></div>