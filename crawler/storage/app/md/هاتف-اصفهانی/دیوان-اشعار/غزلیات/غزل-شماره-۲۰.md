---
title: >-
    غزل شمارهٔ ۲۰
---
# غزل شمارهٔ ۲۰

<div class="b" id="bn1"><div class="m1"><p>حرف غمت از دهان ما جست</p></div>
<div class="m2"><p>یا آتشی از زبان ما جست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رو جانب دام یا قفس کرد</p></div>
<div class="m2"><p>هر مرغ کز آشیان ما جست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یک‌یک ز نشان فراتر افتاد</p></div>
<div class="m2"><p>هر تیر که از کمان ما جست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آتش به سپهر زد شراری</p></div>
<div class="m2"><p>کز آه شررفشان ما جست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>غیر از که شنید سر عشقت</p></div>
<div class="m2"><p>حرفی مگر از دهان ما جست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز انسان که خورد نسیم بر گل</p></div>
<div class="m2"><p>تیر تو ز استخوان ما جست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هاتف چو شراره‌ای که ناگاه</p></div>
<div class="m2"><p>ز آتش جهد از میان ما جست</p></div></div>