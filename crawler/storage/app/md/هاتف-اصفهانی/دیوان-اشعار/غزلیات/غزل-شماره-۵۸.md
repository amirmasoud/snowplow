---
title: >-
    غزل شمارهٔ ۵۸
---
# غزل شمارهٔ ۵۸

<div class="b" id="bn1"><div class="m1"><p>بی‌مهری اگر چه بی‌وفا هم</p></div>
<div class="m2"><p>جور از تو نکو بود جفا هم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیگانه و آشنا ندانی</p></div>
<div class="m2"><p>بیگانه کشی و آشنا هم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پیش که برم شکایت تو</p></div>
<div class="m2"><p>کز خلق نترسی از خدا هم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بس تجربه کرده‌ام ندارد</p></div>
<div class="m2"><p>آه سحری اثر دعا هم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در وصل چو هجر سوزدم جان</p></div>
<div class="m2"><p>از درد به جانم از دوا هم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای گل که ز هر گلی فزون است</p></div>
<div class="m2"><p>در حسن، رخ تو در صفا هم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شد فصل بهار و بلبل و گل</p></div>
<div class="m2"><p>در باغ به عشرتند با هم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>با هم ستم است اگر نباشیم</p></div>
<div class="m2"><p>چون بلبل و گل به باغ ما هم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جز هاتف بی‌نوا در آن کوی</p></div>
<div class="m2"><p>شاه آمد و شد کند، گدا هم</p></div></div>