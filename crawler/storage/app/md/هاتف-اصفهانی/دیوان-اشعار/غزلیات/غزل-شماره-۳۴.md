---
title: >-
    غزل شمارهٔ ۳۴
---
# غزل شمارهٔ ۳۴

<div class="b" id="bn1"><div class="m1"><p>نه با من دوست آن گفت و نه آن کرد</p></div>
<div class="m2"><p>که با دشمن توان گفت و توان کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گرفت از من دل و زد راه دینم</p></div>
<div class="m2"><p>ز دین و دل گذشتم قصد جان کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کی از شرمندگی با مهربانان</p></div>
<div class="m2"><p>توان گفت آنچه آن نامهربان کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>منش از مردمان رخ می‌نهفتم</p></div>
<div class="m2"><p>ستم بین کآخر از من رخ نهان کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تو با من کردی از جور آنچه کردی</p></div>
<div class="m2"><p>من از شرم تو گفتم آسمان کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دو عالم سود کرد آن کس که در عشق</p></div>
<div class="m2"><p>دلی درباخت یا جانی زیان کرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نه از کین خون هاتف ریخت آن شوخ</p></div>
<div class="m2"><p>وفای او به کشتن امتحان کرد</p></div></div>