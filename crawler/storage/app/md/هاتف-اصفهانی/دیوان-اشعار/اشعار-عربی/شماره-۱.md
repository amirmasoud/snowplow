---
title: >-
    شمارهٔ ۱
---
# شمارهٔ ۱

<div class="b" id="bn1"><div class="m1"><p>تجافی طبیبی نائیا عن‌دوائیا</p></div>
<div class="m2"><p>اخلای خلوتی ابیت و دائیا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بنی ام قد ابکی دما و تروننی</p></div>
<div class="m2"><p>فما بالکم لاترحمون بکائیا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>الم یان اخوانی لکم ان ترحموا</p></div>
<div class="m2"><p>علیکم کئیبا فی دمی اللیل باکیا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>فصرت ولا ادری من‌الیوم لیلتی</p></div>
<div class="m2"><p>ولا عن یمینی لو نظرت شمالیا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اذا غالنی یا قوم دائی خلالکم</p></div>
<div class="m2"><p>و مت فممن یطلبون بثاریا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>فقوموا بلامهل و شوقوا مطیکم</p></div>
<div class="m2"><p>الی کعبه الامال دار الامانیا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>الی بلدة حفت بکل مسرة</p></div>
<div class="m2"><p>الی بلدة اضحت من الهم خالیا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>الی بلدة فیها هوای و منیتی</p></div>
<div class="m2"><p>الی بلدة فیها جیبی ثاویا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>قفوا عنده مستانسین و بلغوا</p></div>
<div class="m2"><p>الیه سلامی ثم بثوا غرامیا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>و قصوا له همی و کربی و لوعتی</p></div>
<div class="m2"><p>و شدة اسقامی و طول عنائیا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>و کثرة آلامی و قلة حیلتی</p></div>
<div class="m2"><p>و طول مقاساة النوی و اصطباریا</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>و قولواله یا صاح یا غایة المنی</p></div>
<div class="m2"><p>و قاک اله العالمین الدواهیا</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>امن طول ایام الفراق نسیتنی</p></div>
<div class="m2"><p>و حاشاک ان تنسی محبا موافیا</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ام اخترت غیری من محبیک مؤثرا</p></div>
<div class="m2"><p>و حاشاک ان تعتاضنی بسوائیا</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نسیت عهودا بیننا و نقضتها</p></div>
<div class="m2"><p>فیاویح نفسی ما حسبتک ناسیا</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>مضی‌العمر فی ضر من‌العیش و انقضی</p></div>
<div class="m2"><p>و ما الدهر الاباخل عن مرامیا</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>الی الله اشکو لیلة مد لهمة</p></div>
<div class="m2"><p>علی‌العین ارخت من دجاها غواشیا</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>الی الله اشکو من هموم صغارها</p></div>
<div class="m2"><p>یحاکی الجبال الشامخات رواسیا</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>سئمت حبیبی من انیتی ورنتی</p></div>
<div class="m2"><p>و اصغاء آلامی و طول مقالیا</p></div></div>