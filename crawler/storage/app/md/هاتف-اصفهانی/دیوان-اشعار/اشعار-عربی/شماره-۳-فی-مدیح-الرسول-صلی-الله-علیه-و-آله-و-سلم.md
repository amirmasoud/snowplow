---
title: >-
    شمارهٔ ۳ - فی مدیح الرسول صلی الله علیه و آله و سلم
---
# شمارهٔ ۳ - فی مدیح الرسول صلی الله علیه و آله و سلم

<div class="b" id="bn1"><div class="m1"><p>نادمت اهل الحمی یوما بذی سلم</p></div>
<div class="m2"><p>فارفتهم و ندیمی بعدهم ندم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عاشرتهم غانما بالطیب و الطرب</p></div>
<div class="m2"><p>هاجرتهم نادما بالهم و السدم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اصبحت من وصلهم فی‌الروح و الفرح</p></div>
<div class="m2"><p>امسیت من هجرهم فی‌الضر و السقم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>فی ربعهم عشت ملتذا بصحبتهم</p></div>
<div class="m2"><p>والدهر یعتقب اللذات بالالم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حاشای ما کنت من یختار فرقتهم</p></div>
<div class="m2"><p>لکن قضاء جری فی اللوح بالقلم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>فلیس لی منیه منذ افتقدتهم</p></div>
<div class="m2"><p>الا ملاقاتهم فی ذلک الحرم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ما بال عینی تذری من تذکرهم</p></div>
<div class="m2"><p>بمدمع هطل کالغیث منسجم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کالمزن تهمی بوبل معذق و دق</p></div>
<div class="m2"><p>متی تشاهد و مض البرق من اضم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>حاولت املی کتابا کی اشیر بما</p></div>
<div class="m2"><p>قلبی یقاسیه فی نبذ من الکلم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>من ذکرهم هملت عینی فما نزلت</p></div>
<div class="m2"><p>علی الرقیمه حرف غیر منعجم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مهما و طئت ربی نجد و تربته</p></div>
<div class="m2"><p>مالی تسابق راسی مسرعا قدم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>یا حبذا الربع و الاطلال و الدمن</p></div>
<div class="m2"><p>من ارض نجد سقاه الله من دیم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>فیالها تربه کالمسک طیبة</p></div>
<div class="m2"><p>جادت علیه الغوادی اجود الرهم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>کانها رفرف خضر قد انبسطت</p></div>
<div class="m2"><p>تحت القر تفل و الریحان و العنم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>متی تهب صبا نجد بریلها</p></div>
<div class="m2"><p>یستنشق المسک منها کل ذی خشم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>طوبی لصاد تروی من مناهلها</p></div>
<div class="m2"><p>فی الحر مغترفا من مائها الشیم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>فلو غسلت العظام البالیات به</p></div>
<div class="m2"><p>تعود منه حیوة الاعظم الرمم</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>قد کان سکانها مستانسین بها</p></div>
<div class="m2"><p>فی ارغد العیش محفوفین با النعم</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>فالدهر غافصهم فیها و اجلاهم</p></div>
<div class="m2"><p>عنها و فرقهم بالاهل و الحشم</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بیوتهم قد حوت صفرا بلا اهل</p></div>
<div class="m2"><p>خیامها قد خلت من ساکن الخیم</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>اضحت مساکن سادات اولی خطر</p></div>
<div class="m2"><p>ظلت منازل اشراف ذوی همم</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>مأوی الثعالب و الذئبان الضبع</p></div>
<div class="m2"><p>مثوی الرفاقیف و الغربان و الرخم</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>فاقفرت دورهم حتی کان بها</p></div>
<div class="m2"><p>مستأنسا بعد لم یسکن و لم یقم</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>و سد باب لدار ترب سدته</p></div>
<div class="m2"><p>کانت مناص و جوه العرب و العجم</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>دار لال رسول الله مقفرة</p></div>
<div class="m2"><p>بنائها اسست بالجود و الکرم</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>داریباهی بها جبریل مفتخرا</p></div>
<div class="m2"><p>لوعد فیها من الحجاب و الخدم</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>عفت رسوم مغاینهم و لولاهم</p></div>
<div class="m2"><p>رب‌الخلیقة خلق‌الخق لم یرم</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>قلوبهم من سلاف العلم طافحة</p></div>
<div class="m2"><p>تفض منها و تجری صفوة الحکم</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>وجوههم عن جمال‌الحق حاکیه</p></div>
<div class="m2"><p>عن درک انوارهم طرف العقول عمی</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>ما للقدیم شبیه حادث لکن</p></div>
<div class="m2"><p>حدوثهم اشبه الاشیاء بالقدم</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>یا فجعتی حین ما اصغی مصائبهم</p></div>
<div class="m2"><p>ما لا یطاق لسانی ذکرها و فمی</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>اوذوا و قد صبروا فی کل ماظلموا</p></div>
<div class="m2"><p>والله من ظالمیهم خیر منتقم</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>یعجل الله فی اظهار قائمهم</p></div>
<div class="m2"><p>حتی یزیج ظلام الاعصر الدهم</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>و یملاء الارض عدلا بعد ماملئت</p></div>
<div class="m2"><p>ظلماء ظلم علی الافاق مرتکم</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>یا سادتی یا موالی الکرام بکم</p></div>
<div class="m2"><p>رجاء عبد کثیرالذنب مجترم</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>قد اصبحت لممی بیضاء فی سرف</p></div>
<div class="m2"><p>والوجه کالقلب مسود من اللمم</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>ظهری انحنی و انثنی من حمل اوزار</p></div>
<div class="m2"><p>صغارها کالجبال الشم فی‌العظم</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>مالی سوی حبکم والاعتصام بکم</p></div>
<div class="m2"><p>مطفی لحدة نار اوقدت جرمی</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>فحبکم لمضیق اللحد مدخری</p></div>
<div class="m2"><p>و بغض اعدائکم فی‌الحشر معتصمی</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>لو لم ینلنی شراب من شفاعتکم</p></div>
<div class="m2"><p>یا حر قلب من‌الحرمان مضطرم</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>اتیتکم بمدیح لایلیق بکم</p></div>
<div class="m2"><p>و هل یلیق بکم ما اسود من قلمی</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>کلا هل یتاتی نشر مدحتکم</p></div>
<div class="m2"><p>من اعجمی بنظم غیر منتظم</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>هیهات و البلغاء الماد حون وان</p></div>
<div class="m2"><p>اطروا بکل لسان عد فی بکم</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>لا من مدبحی و لکن من مواهبکم</p></div>
<div class="m2"><p>ارجو الحمایه یوما للعصاه حمی</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>و کل ذی و طراعیت مذاهبه</p></div>
<div class="m2"><p>لورام ابواب اهل الجود لم یلم</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>صلی علیکم باذکاها و اطیبها</p></div>
<div class="m2"><p>رب البرایا صلوة غیر منحسم</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>ما انضرت ارض نجد من غمایمها</p></div>
<div class="m2"><p>خضر المرابع و الاطلال و الاکم</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>و استطربت سجعا فیها حمایمها</p></div>
<div class="m2"><p>مغردات علی اغصان بالنغم</p></div></div>