---
title: >-
    قطعه شمارهٔ ۶
---
# قطعه شمارهٔ ۶

<div class="b" id="bn1"><div class="m1"><p>امیر داد گستر خان عادل</p></div>
<div class="m2"><p>دلیر عدل پرور شاهرخ خان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خدیو کامران کز یاری بخت</p></div>
<div class="m2"><p>نپچید آسمانش سر ز فرمان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>برای قطع نخل هستی خصم</p></div>
<div class="m2"><p>تبرزینی به دستش داد دوران</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تبرزین نه کلید فتح و نصرت</p></div>
<div class="m2"><p>تبرزین نه نشان شوکت و شان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تبرزین نه رگ ابری شرر بار</p></div>
<div class="m2"><p>که انگیزد ز خون خصم طوفان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تبرزین نه عقابی صیدپیشه</p></div>
<div class="m2"><p>که قوت اوست مغز اهل عدوان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کسی کو گیردش بر کف نماند</p></div>
<div class="m2"><p>چو موسی و ید بیضا و ثعبان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز آسیبش پریشان باد دایم</p></div>
<div class="m2"><p>سر دشمن چو گوی از ضرب چوگان</p></div></div>