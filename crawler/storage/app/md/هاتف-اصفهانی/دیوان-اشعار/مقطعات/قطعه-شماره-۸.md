---
title: >-
    قطعه شمارهٔ ۸
---
# قطعه شمارهٔ ۸

<div class="b" id="bn1"><div class="m1"><p>زنگیی با دو ترک و دو هندو</p></div>
<div class="m2"><p>بیضه‌ای با سه زاغ ای آگاه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پس از آن چار کوکب تابان</p></div>
<div class="m2"><p>چار تیره شب و دو روشن ماه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون به ترتیب ذکر جمع آیند</p></div>
<div class="m2"><p>هفت هفت ار تو بشمری آنگاه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هفتمین را برون کنی میدان</p></div>
<div class="m2"><p>که نماند در آن میانه سیاه</p></div></div>