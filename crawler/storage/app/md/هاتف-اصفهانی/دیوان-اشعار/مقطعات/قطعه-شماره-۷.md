---
title: >-
    قطعه شمارهٔ ۷
---
# قطعه شمارهٔ ۷

<div class="b" id="bn1"><div class="m1"><p>صبح و شامی و ماه‌رخساری</p></div>
<div class="m2"><p>با دو زلف و دو رخ دو خال آنگاه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روزی و از قفا شبی و ز پی</p></div>
<div class="m2"><p>اختری با دو تیره ابر و دو ماه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دو ز اهل حبش چهار از روم</p></div>
<div class="m2"><p>پنج از زنگبارشان همراه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دو گهر یک شبه دو لؤلؤ را</p></div>
<div class="m2"><p>گر تو نه نه شماری ای آگاه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بعد وضع نهم نخواهد ماند</p></div>
<div class="m2"><p>بی‌شک و شبه دانه‌ای ز سیاه</p></div></div>