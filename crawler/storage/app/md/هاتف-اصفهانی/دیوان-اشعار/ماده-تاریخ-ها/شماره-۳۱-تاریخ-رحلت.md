---
title: >-
    شمارهٔ ۳۱ - تاریخ رحلت
---
# شمارهٔ ۳۱ - تاریخ رحلت

<div class="b" id="bn1"><div class="m1"><p>چو حوری جهان آن پسندیده زن</p></div>
<div class="m2"><p>از این عالم پرشر و شور شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خرد بهر تاریخ فوتش نوشت</p></div>
<div class="m2"><p>به جنات عدن از جهان حور شد</p></div></div>