---
title: >-
    شمارهٔ ۱
---
# شمارهٔ ۱

<div class="b" id="bn1"><div class="m1"><p>در زمان خدیو دارا شان</p></div>
<div class="m2"><p>آن کرم پیشهٔ کریم نهاد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سایه حق کریمخان که ز عدل</p></div>
<div class="m2"><p>زینت دهر و زیب دوران داد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شهریار جهان که در گیتی</p></div>
<div class="m2"><p>کرمش عقده‌های بسته گشاد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کامیابی که هر مراد که خواست</p></div>
<div class="m2"><p>دادش از لطف کردگار عباد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کام‌بخشی که یافت از در او</p></div>
<div class="m2"><p>هر که آمد به جستجوی مراد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خسرو معدلت نشان که بود</p></div>
<div class="m2"><p>دولتش متصل به روز معاد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ریزه‌خوار نوالهٔ کرمش</p></div>
<div class="m2"><p>ترک و تاجیک و بنده و آزاد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>امر او را به جان ستاره مطیع</p></div>
<div class="m2"><p>حکم او را به دل فلک منقاد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در دل اندیشهٔ مراد ازو</p></div>
<div class="m2"><p>وز قضا سعی و از قدر امداد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>حاجی آقا محمد آنکه چو او</p></div>
<div class="m2"><p>در هنر مادر زمانه نزاد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دادگر داوری که در عهدش</p></div>
<div class="m2"><p>کس نبیند ز گلرخان بیداد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>معدلت گستری که از بیمش</p></div>
<div class="m2"><p>صید ناید به خاطر صیاد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چون ز بخت بلند امارت یافت</p></div>
<div class="m2"><p>در صفاهان که هست رشک بلاد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>پی آبادیش به جان کوشید</p></div>
<div class="m2"><p>که خدایش جزای خیر دهاد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>صد هزاران بنای خیر آنجا</p></div>
<div class="m2"><p>ز اقتضای نهاد نیک، نهاد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>دلگشا کاروانسرایی ساخت</p></div>
<div class="m2"><p>زینت افزای عالم ایجاد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>که بنایی ندیده مانندش</p></div>
<div class="m2"><p>چشم گردون در این خراب آباد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چون فلک سربلند و ذات بروج</p></div>
<div class="m2"><p>چون ارم جان فزای و ذات عماد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>همه وقتش هوای فروردین</p></div>
<div class="m2"><p>گر همه بهمن است یا مرداد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>حوض کوثر نشان آن گویی</p></div>
<div class="m2"><p>نیل مصر است و دجلهٔ بغداد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>هر که بر وضع آن نظر افکند</p></div>
<div class="m2"><p>باغ فردوسش از نظر افتاد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>هر غریبی که جا گرفت آنجا</p></div>
<div class="m2"><p>هرگزش از وطن نیامد یاد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>خان گلشن به نام خوانندش</p></div>
<div class="m2"><p>در صفا چون نشان ز گلشن داد</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>داده استاد، جان به آب و گلش</p></div>
<div class="m2"><p>کافرین بر روان آن استاد</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>سحر دستش کشیده بر خارا</p></div>
<div class="m2"><p>شکل مانی ز تیشهٔ فرهاد</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>چون به معماری قضا و قدر</p></div>
<div class="m2"><p>یافت اتمام این نکو بنیاد</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بهر تاریخ زد رقم هاتف</p></div>
<div class="m2"><p>جاودان داردش خدا آباد</p></div></div>