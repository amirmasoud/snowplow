---
title: >-
    شمارهٔ ۳۰ - تاریخ بنای خانه
---
# شمارهٔ ۳۰ - تاریخ بنای خانه

<div class="b" id="bn1"><div class="m1"><p>به حکم بندهٔ خلاق آن رزاق بی‌منت</p></div>
<div class="m2"><p>که کردش کافل ارزاق لطف قادر منان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>امیر بی‌نظیر مرحمت‌پرور که از دادش</p></div>
<div class="m2"><p>شود بی‌باک آهوبره گرگ پیر را مهمان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دلیر شیرگیر معدلت‌پرور که از بیمش</p></div>
<div class="m2"><p>کند در بیشه شیر شرزه چنگال خود از دندان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پس از تعمیر کاشان کز ازل می‌بود ویرانه</p></div>
<div class="m2"><p>به یمن همت عالیش چون گردید آبادان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بنا شد خانهٔ دلکش روان شد جوی آبی خوش</p></div>
<div class="m2"><p>به خوبی روضهٔ رضوان به صافی چشمهٔ حیوان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زلال حوض آن پیوسته روح‌افزار و جان‌پرور</p></div>
<div class="m2"><p>نسیم صحن آن همواره عنبربیز و مشک‌افشان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ازین دلکش بنا کاشان به اصفاهان همی نازد</p></div>
<div class="m2"><p>سزد هر چند بر گلزار جنت نازد اصفاهان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو از معماری لطف خدا بر پا شد این خانه</p></div>
<div class="m2"><p>که در وی با نیش خرم زید با عمر جاویدان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>پی تاریخ سال آن رقم زد خامهٔ هاتف</p></div>
<div class="m2"><p>همی نازد به اصفاهان ازین دلکش بنا کاشان</p></div></div>