---
title: >-
    شمارهٔ ۷
---
# شمارهٔ ۷

<div class="b" id="bn1"><div class="m1"><p>دریغ از حاجی ابراهیم آن دانای روشندل</p></div>
<div class="m2"><p>که زاد از مادر ایام با ایمان و دین توام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دریغ و درد از آن شمع سحر خیزان که بود او را</p></div>
<div class="m2"><p>دلی پر آتش از ترس خدا و دیدهٔ پرنم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هزار افسوس از آن نخل برومند ثمرپرور</p></div>
<div class="m2"><p>که در باغ جهانش قامت از باد اجل شد خم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گرفتش دل ازین تنگ آشیان و طایر روحش</p></div>
<div class="m2"><p>به عزم گلشن فردوس بال شوق زد برهم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>روان شد جانب گلزار جنت زین جهان و شد</p></div>
<div class="m2"><p>روان از دیدهٔ احباب سیل خون ازین ماتم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو بیرون رفت از غمخانهٔ دنیای دون و شد</p></div>
<div class="m2"><p>به عشرتخانهٔ فردوس اعلی با دلی خرم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دبیر خامه هاتف پی تاریخ فوت او</p></div>
<div class="m2"><p>رقم زد: شد به جنت حاجی ابراهیم از عالم</p></div></div>