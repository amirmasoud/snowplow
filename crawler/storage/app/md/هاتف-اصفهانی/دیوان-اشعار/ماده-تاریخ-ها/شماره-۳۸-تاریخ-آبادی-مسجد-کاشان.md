---
title: >-
    شمارهٔ ۳۸ - تاریخ آبادی مسجد کاشان
---
# شمارهٔ ۳۸ - تاریخ آبادی مسجد کاشان

<div class="b" id="bn1"><div class="m1"><p>خان جم کوکبه عبدالرزاق</p></div>
<div class="m2"><p>که کند دیدن او جان تازه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن که رخسار و جمالش دایم</p></div>
<div class="m2"><p>هست چون گل به گلستان تازه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آن که ز ابر کرمش کشت امید</p></div>
<div class="m2"><p>هست چون سبزه ز باران تازه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آن که با جود کفش هر روزه</p></div>
<div class="m2"><p>عهد نو سازد و پیمان تازه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شهر کاشان را از همت او</p></div>
<div class="m2"><p>شد پس از زلزله بنیان تازه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گشت از مسجد و بازار و حصار</p></div>
<div class="m2"><p>همهٔ ابنیهٔ آن تازه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پایه‌ها راست شد ارکان محکم</p></div>
<div class="m2"><p>گنبدش نوشد و ایوان تازه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زان بناهای مجدد گردید</p></div>
<div class="m2"><p>مسجد جامع ویران تازه</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>منهدم بود چنان کش گفتی</p></div>
<div class="m2"><p>نتوان کرد به عمران تازه</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>همتش گشت چون آنجا معمار</p></div>
<div class="m2"><p>سقف‌ها نوشد و جدران تازه</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شد چنان تازه که در هفت اقلیم</p></div>
<div class="m2"><p>مسجدی نیست بدین‌سان تازه</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>از طواف حرم محترمش</p></div>
<div class="m2"><p>مؤمنان را شود ایمان تازه</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>در وی افواج ملایک آیند</p></div>
<div class="m2"><p>هر دم از گنبد گردان تازه</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بهر تاریخ خرد با هاتف</p></div>
<div class="m2"><p>گفت شد مسجد کاشان تازه</p></div></div>