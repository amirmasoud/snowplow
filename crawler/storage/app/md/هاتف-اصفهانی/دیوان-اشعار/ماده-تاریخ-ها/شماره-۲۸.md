---
title: >-
    شمارهٔ ۲۸
---
# شمارهٔ ۲۸

<div class="b" id="bn1"><div class="m1"><p>آه که از جور فلک شد به باد</p></div>
<div class="m2"><p>تازه گل خرم باغ جهان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آه که بر خاک هلاک اوفتاد</p></div>
<div class="m2"><p>سرو سهی قامت این بوستان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رفت محمدعلی آن تازه گل</p></div>
<div class="m2"><p>در چمن دهر به باد خزان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حیف از آن گوهر یکتا که کرد</p></div>
<div class="m2"><p>جا به دل خاک ازین خاکدان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حیف از آن کوکب رخشان که ساخت</p></div>
<div class="m2"><p>دور سپهرش ز نظرها نهان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون به جوانی ز جهان خراب</p></div>
<div class="m2"><p>گشت روان سوی ریاض جنان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هاتف دلخسته که در ماتمش</p></div>
<div class="m2"><p>داشت شب و روز خروش و فغان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گفت به تاریخ که سوی جنان</p></div>
<div class="m2"><p>رفت محمدعلی نوجوان</p></div></div>