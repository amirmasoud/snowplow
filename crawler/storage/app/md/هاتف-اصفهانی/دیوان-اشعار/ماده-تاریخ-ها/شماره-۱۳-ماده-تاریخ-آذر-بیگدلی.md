---
title: >-
    شمارهٔ ۱۳ - ماده تاریخ آذر بیگدلی
---
# شمارهٔ ۱۳ - ماده تاریخ آذر بیگدلی

<div class="b" id="bn1"><div class="m1"><p>بلبل گویای این باغ آذر از دور سپهر</p></div>
<div class="m2"><p>لب فروبست از نوای زندگی ناگاه آه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ناگهان دم درکشید از بذلهٔ دلکش دریغ</p></div>
<div class="m2"><p>عاقبت خاموش گشت از نغمهٔ دلخواه آه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دامن صحبت کشید از چنگ اهل دل فسوس</p></div>
<div class="m2"><p>ظل رحمت برگرفت از فرق اهل الله آه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>صبح او گردید شام از گردش انجم فغان</p></div>
<div class="m2"><p>روز عالم شد سیاه از دور مهر و ماه آه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رشتهٔ آمال ما زان در فاخر بس دراز</p></div>
<div class="m2"><p>رشتهٔ عمر وی آمد لیک بس کوتاه آه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کرد تنها عزم ره وز دوستان کس را نبرد</p></div>
<div class="m2"><p>خاصه چون من چاکری با خویشتن همراه آه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>راز دل ناگفته چشم از محرمان پوشید و رفت</p></div>
<div class="m2"><p>کس ز راز آن دل آگه نشد آگاه آه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چرخ روبه باز کردش طعمهٔ گرگ اجل</p></div>
<div class="m2"><p>شد زبون شیری چو او در چنگ این روباه آه</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>یوسف افتاد ار به چاه آخر ز چاه آمد برون</p></div>
<div class="m2"><p>یوسف من ماند تا آخر زمان در چاه آه</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چون سوی جنت به پرواز آمد اندر ماتمش</p></div>
<div class="m2"><p>بر فلک رفت از دل و جان گدا و شاه آه</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کلک هاتف از پی تاریخ سال رحلتش</p></div>
<div class="m2"><p>زد رقم از بلبل گویای این باغ آه آه</p></div></div>