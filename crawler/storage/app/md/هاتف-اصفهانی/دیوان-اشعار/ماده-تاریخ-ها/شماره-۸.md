---
title: >-
    شمارهٔ ۸
---
# شمارهٔ ۸

<div class="b" id="bn1"><div class="m1"><p>شکرلله که جهان را ز قدوم</p></div>
<div class="m2"><p>زیب نو داد محمد کاظم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روشن از مقدم خود گیتی را</p></div>
<div class="m2"><p>ساخت چون زاد محمد کاظم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از رخ خود همهٔ یاران را</p></div>
<div class="m2"><p>کرد دلشاد محمد کاظم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>طعن‌ها از قد چون سرو روان</p></div>
<div class="m2"><p>زد به شمشاد محمد کاظم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خلق و خویش همه چون آمد خوب</p></div>
<div class="m2"><p>بد مبیناد محمد کاظم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هاتف از شوق چو در باغ جهان</p></div>
<div class="m2"><p>گان بنهاد محمد کاظم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بهر تاریخ رقم زد: به جهان</p></div>
<div class="m2"><p>جاودان باد محمد کاظم</p></div></div>