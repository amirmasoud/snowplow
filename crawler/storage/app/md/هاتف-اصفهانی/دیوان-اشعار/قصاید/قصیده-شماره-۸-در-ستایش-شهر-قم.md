---
title: >-
    قصیدهٔ شمارهٔ ۸ - در ستایش شهر قم
---
# قصیدهٔ شمارهٔ ۸ - در ستایش شهر قم

<div class="b" id="bn1"><div class="m1"><p>حبذا شهری که سالار است در وی سروری</p></div>
<div class="m2"><p>عدل‌پرور شهریاری دادگستر داوری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شهری آبش جانفزا ملکی هوایش دلگشا</p></div>
<div class="m2"><p>شهریارش دلنوازی والیش جان پروری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شهری از قصر جنان و باغ جنت نسخه‌ای</p></div>
<div class="m2"><p>شهریاری لطف و انعام خدا را مظهری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>روضهٔ خاکش عبیر و روح‌پرور روضه‌ای</p></div>
<div class="m2"><p>سروری در وی امیری عدل‌پرور سروری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چیست دانی نام آن شهر و کدام آن شهریار</p></div>
<div class="m2"><p>کین دو را در زیب و فر، ثانی نباشد دیگری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نام آن شهر است قم فخرالبلاد ام‌القری</p></div>
<div class="m2"><p>کش به خاک آسوده از آل پیمبر دختری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دختری کش دایه دوران نیابد همسری</p></div>
<div class="m2"><p>دختری کش مادر گیتی نزاید خواهری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دختری کاباء و اجداد گرامش یک به یک</p></div>
<div class="m2"><p>تا به آدم یا امامی بوده یا پیغمبری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بنت شاه اولیا موسی ابن‌جعفر فاطمه</p></div>
<div class="m2"><p>کش بود روح‌القدس بیرون درگه چاکری</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ماه بطحا زهرهٔ یثرب چراغ قم که دوخت</p></div>
<div class="m2"><p>دست حق بر دامن پاکش ز عصمت چادری</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شهریار آن ولایت والی آن مملکت</p></div>
<div class="m2"><p>زیبد الحق کسری آیینی تهمتن گوهری</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>خان داراشان جم فرمان کی دربان حسین</p></div>
<div class="m2"><p>آنکه فرزندی به فر او نزاد از مادری</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>آن که اوج قدر را بختش فروزان کوکبی است</p></div>
<div class="m2"><p>آسمان مجد را رویش فروزان اختری</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>آن که بهر تارک و بالای او پرداخته است</p></div>
<div class="m2"><p>چرخ سیمین جوشنی خورشید زرین مغفری</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بر عروس دولتش مشاطهٔ بخت بلند</p></div>
<div class="m2"><p>هردم از فتح و ظفر بندد دگرگون زیوری</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>دایهٔ گردون پیر آمد شد بسیار کرد</p></div>
<div class="m2"><p>داد تا دوشیزهٔ دولت به چون او شوهری</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>افسرش بر فرق فر ایزدی بس گو مباش</p></div>
<div class="m2"><p>بر سر از دانگی زر و ده دانه درش افسری</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>از خم انعام و مینای نوالش بهره داشت</p></div>
<div class="m2"><p>هر سفالین کاسه‌ای دیدیم و زرین ساغری</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>این که نامش چرخ ازرق کرده‌اند از مطبخش</p></div>
<div class="m2"><p>تیره‌گون دودی است بالا رفته یا خاکستری</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>تا زند بر دیدهٔ اعدای او هر صبح مهر</p></div>
<div class="m2"><p>چون برون آید به هر انگشت گیرد نشتری</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>از کمالاتش که نتوان حصر جستم شمه‌ای</p></div>
<div class="m2"><p>از ادیب عقل طوماری گشود و دفتری</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>خود به تنها بشکند هر لشکری را گرچه هست</p></div>
<div class="m2"><p>همرهش ز اقبال و بخت و فتح و نصرت لشکری</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>امن را تا پاسبان عدل او بیدار کرد</p></div>
<div class="m2"><p>ظلم جوید باد جوید فتنه جوید بستری</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>شهر قم کز تندی باد حوادث دیده بود</p></div>
<div class="m2"><p>آنچه بیند مشت خاکی از عبور صرصری</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>در همه این شهر دیدم بارها بر پا نمود</p></div>
<div class="m2"><p>کهنه دیواری که بر وی جغدی افشاند پری</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>از قدوم او در دولت به رویش باز شد</p></div>
<div class="m2"><p>گوئی از فردوس بگشودند بر رویش دری</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>شد به سعی او چنان آباد کاهل آن دیار</p></div>
<div class="m2"><p>مصر را ده می‌شمارند و ده مستحقری</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>پیش ازین گر هر ده ویران به حالش می‌گریست</p></div>
<div class="m2"><p>خندد اکنون بر هر اقلیمی و بر هر کشوری</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>کرد بر پا بس اساس نو در آن شهر کهن</p></div>
<div class="m2"><p>دادش اول از حصاری تازه زیبی و فری</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>لوحش الله چون حصار آسمان ذات‌البروج</p></div>
<div class="m2"><p>فرق هر برجی بلند از فرقدان سامنظری</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>شوخ چشمان فلک شب‌ها پی نظاره‌اش</p></div>
<div class="m2"><p>از بروج آسمان هر یک برون آرد سری</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>بارهٔ چون سد اسکندر به گرد قم کشید</p></div>
<div class="m2"><p>لطف حقش یاور و الحق چه نیکو یاوری</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>عقل چون دید از پی تاریخ این حصن حصین</p></div>
<div class="m2"><p>گفت «سدی نیک گرد قم کشید اسکندری»</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>ای بر خورشید رایت مهر گردون ذره‌ای</p></div>
<div class="m2"><p>آسمان در حکم انگشت تو چون انگشتری</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>با کف دریا نوالت هفت دریا قطره‌ای</p></div>
<div class="m2"><p>پیش خرگاه جلالت هفت گردون چنبری</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>حال زار من چه پرسی این نه بس کز روی تو</p></div>
<div class="m2"><p>دور ماندستم چو دور از روی خور نیلوفری</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>بوی دود عنبرین من گواه من که چرخ</p></div>
<div class="m2"><p>بی تو افکنده است چون عودم به سوزان مجمری</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>روزها بیداد و شب‌ها غمزه از بس دیده‌ام</p></div>
<div class="m2"><p>ز اختران هر یک جدا می‌سوزدم چون اخگری</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>گر ستودم حسن اخلاق تو را دانی که نیست</p></div>
<div class="m2"><p>از حطام دنیوی چشمم به خشکی یا تری</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>قمری و بلبل که مدح سرو و وصف گل کنند</p></div>
<div class="m2"><p>روز و شب زان سرو گل، سیمی نخواهند و زری</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>خلق نیکو هر کجا هست آن درخت خرم است</p></div>
<div class="m2"><p>کو به جز مدح و ثنای خلق برنارد بری</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>طبع من بحری است پهناور که ریزد بر کنار</p></div>
<div class="m2"><p>گه دری و گاه مرجانی و گاهی عنبری</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>کی رهین کس شود دریا که گر گیرد ز ابر</p></div>
<div class="m2"><p>قطرهٔ آبی، دهد واپس درخشان گوهری</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>شادباش و شاد زی کین بزم و این آرامگاه</p></div>
<div class="m2"><p>مانده از سلطان ملکشاهی و سلطان سنجری</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>من به نیروی تو در میدان نظم آویختم</p></div>
<div class="m2"><p>هیچ دانی با که؟ با چون انوری گندآوری</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>هم به امداد نسیم لطفت آمد بر کنار</p></div>
<div class="m2"><p>از چنین بحری سلامت کشتی بی‌لنگری</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>راستی نندیشم از تیغ زبان کس که هست</p></div>
<div class="m2"><p>در نیام کام همچون ذوالفقارم خنجری</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>من که نظمم معجز فصل‌الخطاب احمدی است</p></div>
<div class="m2"><p>نشمرم جز باد سرد، افسون هر افسونگری</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>ریسمانی چند اگر جنبد به افسون ناورد</p></div>
<div class="m2"><p>تاب چون گردد عصا در دست موسی اژدری</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>هان و هان هاتف چه گوئی چیستی و کیستی</p></div>
<div class="m2"><p>لاف بیش از پیش چند ای کمتر از هر کمتری</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>لب فروبند و زبان درکش ره ایجاز گیر</p></div>
<div class="m2"><p>تا نگردیدستی از اطناب بار خاطری</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>تا گذارد گردش ایام و بیزد دور چرخ</p></div>
<div class="m2"><p>تاج عزت بر سری خاک مذلت بر سری</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>دوستانت را کلاهی بر سر از عز و شرف</p></div>
<div class="m2"><p>دشمنانت را به فرق از ذل و خواری معجری</p></div></div>