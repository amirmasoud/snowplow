---
title: >-
    شمارهٔ ۱
---
# شمارهٔ ۱

<div class="b" id="bn1"><div class="m1"><p>بیار وعده خلافم گر اتفاق افتاد</p></div>
<div class="m2"><p>نخست گوشزدش این پیام خواهم کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که تا کیم به فسون گویی آنچه می‌خواهی</p></div>
<div class="m2"><p>به صبح اگرچه نکردم به شام خواهم کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خدا گواست که گر آنچه گرفته‌ام نکنی</p></div>
<div class="m2"><p>ز حرف تلخ تو را تلخکام خواهم کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز هزل شربت زهرت به کام خواهم ریخت</p></div>
<div class="m2"><p>ز هجو جرعهٔ خونت به کام خواهم کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همین نه هجو تو بی‌آبروی خواهم گفت</p></div>
<div class="m2"><p>که قصد جان تو بی‌ننگ و نام خواهم کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اگر بزودی زود آنچه گفته‌ام کردی</p></div>
<div class="m2"><p>ز هجو تیغ زبان در نیام خواهم کرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر آستان شب و روزت مقیم خواهم شد</p></div>
<div class="m2"><p>به خدمتت گه و بیگه قیام خواهم کرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>همین نه بلکه تو را با وجود اینهمه نقص</p></div>
<div class="m2"><p>ز مدح غیرت ماه تمام خواهم کرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز نیت خودت آگاه ساز تا من هم</p></div>
<div class="m2"><p>ازین دو کار بدانم کدام خواهم کرد</p></div></div>