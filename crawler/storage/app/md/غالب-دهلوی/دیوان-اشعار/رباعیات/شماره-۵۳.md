---
title: >-
    شمارهٔ ۵۳
---
# شمارهٔ ۵۳

<div class="b" id="bn1"><div class="m1"><p>منصور غمش ز نکته چینان چه بود؟</p></div>
<div class="m2"><p>در راست خطر ز همنشینان چه بود؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون عاقبت یگانه بینان دارست</p></div>
<div class="m2"><p>دریاب که انجام دوبینان چه بود</p></div></div>