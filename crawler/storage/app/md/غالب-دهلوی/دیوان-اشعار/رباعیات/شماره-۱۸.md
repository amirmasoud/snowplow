---
title: >-
    شمارهٔ ۱۸
---
# شمارهٔ ۱۸

<div class="b" id="bn1"><div class="m1"><p>آن را که عطیه ازل در نظرست</p></div>
<div class="m2"><p>هر چند بلا بیش طرب بیشترست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فرق ست میان من و صنعان در کفر</p></div>
<div class="m2"><p>بخشش دگر و مزد عبادت دگرست</p></div></div>