---
title: >-
    شمارهٔ ۹
---
# شمارهٔ ۹

<div class="b" id="bn1"><div class="m1"><p>در عالم بی زری که تلخ ست حیات</p></div>
<div class="m2"><p>طاعت نتوان کرد به امید نجات</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای کاش ز حق اشارت صوم و صلات</p></div>
<div class="m2"><p>بودی به وجود مال چون حج و زکات</p></div></div>