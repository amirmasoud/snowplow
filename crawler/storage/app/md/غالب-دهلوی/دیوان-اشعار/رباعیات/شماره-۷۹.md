---
title: >-
    شمارهٔ ۷۹
---
# شمارهٔ ۷۹

<div class="b" id="bn1"><div class="m1"><p>رنجورم و می به دهر درمان بودم</p></div>
<div class="m2"><p>نیروی دل و روشنی جان بودم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتم به پدر که خو به می نوشی کن</p></div>
<div class="m2"><p>تا باده به میراث فراوان بودم</p></div></div>