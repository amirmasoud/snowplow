---
title: >-
    شمارهٔ ۹۳
---
# شمارهٔ ۹۳

<div class="b" id="bn1"><div class="m1"><p>بر قول تو اعتماد نتوان کردن</p></div>
<div class="m2"><p>خود را به گزاف شاد نتوان کردن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از کثرت وعده های پی در پی تو</p></div>
<div class="m2"><p>یک وعده درست یاد نتوان کردن</p></div></div>