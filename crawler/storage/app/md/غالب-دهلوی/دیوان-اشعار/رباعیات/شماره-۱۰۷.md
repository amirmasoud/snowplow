---
title: >-
    شمارهٔ ۱۰۷
---
# شمارهٔ ۱۰۷

<div class="b" id="bn1"><div class="m1"><p>آنی تو که شخص مردمی را چشمی</p></div>
<div class="m2"><p>سبحان الله چه مایه بینا چشمی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>البته عجب نیست که باشی بیمار</p></div>
<div class="m2"><p>زان رو که به دلبری سراپا چشمی</p></div></div>