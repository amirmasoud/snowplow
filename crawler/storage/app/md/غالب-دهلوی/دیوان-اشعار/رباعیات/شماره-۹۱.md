---
title: >-
    شمارهٔ ۹۱
---
# شمارهٔ ۹۱

<div class="b" id="bn1"><div class="m1"><p>شرطست به دهر در مظفر گشتن</p></div>
<div class="m2"><p>اسباب دلاوری میسر گشتن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جامی ز شراب ارغوانی باید</p></div>
<div class="m2"><p>آن را که بود هوای خاور گشتن</p></div></div>