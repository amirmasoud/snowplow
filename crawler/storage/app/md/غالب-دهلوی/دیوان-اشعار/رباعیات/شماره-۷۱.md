---
title: >-
    شمارهٔ ۷۱
---
# شمارهٔ ۷۱

<div class="b" id="bn1"><div class="m1"><p>داری چه هراس جان ستانی از مرگ؟</p></div>
<div class="m2"><p>می جوی حیات جاودانی از مرگ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از سوز حرارت غریزی داغم</p></div>
<div class="m2"><p>ناسازترست زندگانی از مرگ</p></div></div>