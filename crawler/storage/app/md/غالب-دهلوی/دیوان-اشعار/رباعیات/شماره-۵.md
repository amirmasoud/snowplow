---
title: >-
    شمارهٔ ۵
---
# شمارهٔ ۵

<div class="b" id="bn1"><div class="m1"><p>بر دل ز دو دیده فتح بابست این خواب</p></div>
<div class="m2"><p>باران امید را سحابست این خواب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زنهار گمان مبر که خوابست این خواب</p></div>
<div class="m2"><p>تعبیر ولای بوترابست این خواب</p></div></div>