---
title: >-
    شمارهٔ ۱۰۳
---
# شمارهٔ ۱۰۳

<div class="b" id="bn1"><div class="m1"><p>ای آن که به راه کعبه رویی داری</p></div>
<div class="m2"><p>نازم که گزیده آرزویی داری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زین گونه که تند می خرامی دانم</p></div>
<div class="m2"><p>در خانه زن ستیزه خویی داری</p></div></div>