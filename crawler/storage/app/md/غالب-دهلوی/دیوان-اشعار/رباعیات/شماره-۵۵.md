---
title: >-
    شمارهٔ ۵۵
---
# شمارهٔ ۵۵

<div class="b" id="bn1"><div class="m1"><p>باید که جهانی دگر ایجاد شود</p></div>
<div class="m2"><p>تا کلبه ویران من آباد شود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در عالم انبساط از من خوشتر</p></div>
<div class="m2"><p>مطرب که به سوز دگران شاد شود</p></div></div>