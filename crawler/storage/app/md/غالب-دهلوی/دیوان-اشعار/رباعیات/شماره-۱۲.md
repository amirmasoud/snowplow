---
title: >-
    شمارهٔ ۱۲
---
# شمارهٔ ۱۲

<div class="b" id="bn1"><div class="m1"><p>غالب روش مردم آزاد جداست</p></div>
<div class="m2"><p>رفتار اسیران ره و زاد جداست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ما ترک مراد را ارم می دانیم</p></div>
<div class="m2"><p>وان باغچه ضبطی شداد جداست</p></div></div>