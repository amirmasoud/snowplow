---
title: >-
    شمارهٔ ۲۳
---
# شمارهٔ ۲۳

<div class="b" id="bn1"><div class="m1"><p>بسمل که سخن طراز مهرآیین ست</p></div>
<div class="m2"><p>ارزش ده آن و مایه بخش این ست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>او پادشه ست گر سخن اقلیم ست</p></div>
<div class="m2"><p>او پیشروست گر محبت دین ست</p></div></div>