---
title: >-
    شمارهٔ ۱۶
---
# شمارهٔ ۱۶

<div class="b" id="bn1"><div class="m1"><p>زان دوست که جان قالب مهر و وفاست</p></div>
<div class="m2"><p>گر دیر رسد پاسخ مکتوب رواست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زان اشک که ریخت دیده هنگام رقم</p></div>
<div class="m2"><p>فی الجمله نورد نامه دشوارگشاست</p></div></div>