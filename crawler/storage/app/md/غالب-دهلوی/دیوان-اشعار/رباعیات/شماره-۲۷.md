---
title: >-
    شمارهٔ ۲۷
---
# شمارهٔ ۲۷

<div class="b" id="bn1"><div class="m1"><p>آن خسته که در نظر به جز یارش نیست</p></div>
<div class="m2"><p>با سود و زیان خویشتن کارش نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>طالب ز طلب رهین آثارش نیست</p></div>
<div class="m2"><p>هر چند حنا برگ دهد بارش نیست</p></div></div>