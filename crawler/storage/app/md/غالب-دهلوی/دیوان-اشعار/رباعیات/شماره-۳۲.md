---
title: >-
    شمارهٔ ۳۲
---
# شمارهٔ ۳۲

<div class="b" id="bn1"><div class="m1"><p>گردیدن زاهدان به جنت گستاخ</p></div>
<div class="m2"><p>وین دست درازی به ثمر شاخ به شاخ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون نیک نظر کنی ز روی تشبیه</p></div>
<div class="m2"><p>ماند به بهایم و علفزار فراخ</p></div></div>