---
title: >-
    شمارهٔ ۱۱
---
# شمارهٔ ۱۱

<div class="b" id="bn1"><div class="m1"><p>ای آن که به دهر نام تو شاهرخ است</p></div>
<div class="m2"><p>پیوسته ترا به حضرت شاه رخ است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نازد به تو شه که باشد اندر شطرنج</p></div>
<div class="m2"><p>امید ظفر قوی چو با شاه رخ است</p></div></div>