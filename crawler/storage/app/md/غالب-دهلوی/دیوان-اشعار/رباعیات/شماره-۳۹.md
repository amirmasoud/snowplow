---
title: >-
    شمارهٔ ۳۹
---
# شمارهٔ ۳۹

<div class="b" id="bn1"><div class="m1"><p>گر گرد ز گنج گهری برخیزد</p></div>
<div class="m2"><p>مپسند که دود از جگری برخیزد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>منت نتوان نهاد بر کدیه گران</p></div>
<div class="m2"><p>بنشین که به خدمت دگری برخیزد</p></div></div>