---
title: >-
    شمارهٔ ۸۸
---
# شمارهٔ ۸۸

<div class="b" id="bn1"><div class="m1"><p>تا میکش و جوهر دو سخنور داریم</p></div>
<div class="m2"><p>شأن دگر و شوکت دیگر داریم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در میکده پیریم که میکش از ماست</p></div>
<div class="m2"><p>در معرکه تیغیم که جوهر داریم</p></div></div>