---
title: >-
    شمارهٔ ۸۷
---
# شمارهٔ ۸۷

<div class="b" id="bn1"><div class="m1"><p>در عهد تو و منست در هفت اقلیم</p></div>
<div class="m2"><p>برخاستن امید و خون گشتن بیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از جلوه چه ماند تا بسازند بهشت</p></div>
<div class="m2"><p>از شعله چه ماند تا بتابند جحیم</p></div></div>