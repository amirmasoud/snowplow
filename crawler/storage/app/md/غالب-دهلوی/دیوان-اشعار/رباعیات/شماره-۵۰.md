---
title: >-
    شمارهٔ ۵۰
---
# شمارهٔ ۵۰

<div class="b" id="bn1"><div class="m1"><p>امروز که روز عید نوروز بود</p></div>
<div class="m2"><p>روزی فرخنده و دل افروز بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر عیش و نشاطی که درین روز بود</p></div>
<div class="m2"><p>هر روز ترا ز بخت فیروز بود</p></div></div>