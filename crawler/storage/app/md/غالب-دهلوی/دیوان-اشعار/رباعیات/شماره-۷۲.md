---
title: >-
    شمارهٔ ۷۲
---
# شمارهٔ ۷۲

<div class="b" id="bn1"><div class="m1"><p>شب چیست سویدای دل اهل کمال</p></div>
<div class="m2"><p>سرمایه ده حسن به زلف و خط و خال</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>معراجی نبی به شب از آن بود که نیست</p></div>
<div class="m2"><p>وقتی شایسته تر ز شب بهر وصال</p></div></div>