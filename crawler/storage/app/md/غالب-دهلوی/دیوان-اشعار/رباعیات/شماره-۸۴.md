---
title: >-
    شمارهٔ ۸۴
---
# شمارهٔ ۸۴

<div class="b" id="bn1"><div class="m1"><p>غالب به گهر ز دوده زاد شمم</p></div>
<div class="m2"><p>زان رو به صفای دم تیغ ست دمم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون رفت سپهبدی زدم چنگ به شعر</p></div>
<div class="m2"><p>شد تیر شکسته نیاکان قلمم</p></div></div>