---
title: >-
    شمارهٔ ۴
---
# شمارهٔ ۴

<div class="b" id="bn1"><div class="m1"><p>ای دوست به سوی این فرومانده بیا</p></div>
<div class="m2"><p>از کوچه غیر راه گردانده بیا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتی که مرا مخوان که من مرگ توام</p></div>
<div class="m2"><p>بر گفته خویش باش و ناخوانده بیا</p></div></div>