---
title: >-
    شمارهٔ ۶۴
---
# شمارهٔ ۶۴

<div class="b" id="bn1"><div class="m1"><p>ای روی تو همچو مهر گیتی افروز</p></div>
<div class="m2"><p>وی بخت تو در جهانستانی فیروز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حق کرده به روزنامه عمر تو ثبت</p></div>
<div class="m2"><p>توقیع توقع هزاران نوروز</p></div></div>