---
title: >-
    شمارهٔ ۳
---
# شمارهٔ ۳

<div class="b" id="bn1"><div class="m1"><p>گر دل ز شرر زدوده باشم خود را</p></div>
<div class="m2"><p>ور بر دم تیغ سوده باشم خود را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حاشا که ز تو ربوده باشم خود را</p></div>
<div class="m2"><p>با خوی تو آزموده باشم خود را</p></div></div>