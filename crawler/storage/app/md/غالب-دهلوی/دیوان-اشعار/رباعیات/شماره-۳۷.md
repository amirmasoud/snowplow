---
title: >-
    شمارهٔ ۳۷
---
# شمارهٔ ۳۷

<div class="b" id="bn1"><div class="m1"><p>وقت ست که آسمان موجه نازد</p></div>
<div class="m2"><p>مهر آینه پیش رخ نهد مه نازد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این خود شرف دگر بود نیست عجب</p></div>
<div class="m2"><p>گر مهر به پابوس شهنشه نازد</p></div></div>