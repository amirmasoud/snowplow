---
title: >-
    شمارهٔ ۹۷
---
# شمارهٔ ۹۷

<div class="b" id="bn1"><div class="m1"><p>ای آن که گرفته ام به کوی تو پناه</p></div>
<div class="m2"><p>رانی چو به عنف از در خویشم ناگاه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا کعبه روم ز درگهت رو به قفا</p></div>
<div class="m2"><p>چون بگذرم از کعبه نهم روی به راه</p></div></div>