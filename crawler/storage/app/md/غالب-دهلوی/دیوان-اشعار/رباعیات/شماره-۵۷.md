---
title: >-
    شمارهٔ ۵۷
---
# شمارهٔ ۵۷

<div class="b" id="bn1"><div class="m1"><p>زین رنگ که در گلشن احباب دمید</p></div>
<div class="m2"><p>پژمرد گل و لاله شاداب دمید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در کلبه اقبال ترقی طلبان</p></div>
<div class="m2"><p>گر مهر فرو نشست مهتاب دمید</p></div></div>