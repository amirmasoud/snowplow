---
title: >-
    شمارهٔ ۲۸
---
# شمارهٔ ۲۸

<div class="b" id="bn1"><div class="m1"><p>اوراق زمانه درنوشتیم و گذشت</p></div>
<div class="m2"><p>در فن سخن یگانه گشتیم و گذشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می بود دوای ما به پیری غالب</p></div>
<div class="m2"><p>زان نیز به ناکام گذشتیم و گذشت</p></div></div>