---
title: >-
    شمارهٔ ۳۴
---
# شمارهٔ ۳۴

<div class="b" id="bn1"><div class="m1"><p>از وهم قطرگی ست که در خود گمیم ما</p></div>
<div class="m2"><p>اما چو وارسیم همان قلزمیم ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در خاک از هوای گل و شمع فارغیم</p></div>
<div class="m2"><p>از توسن تو طالب نقش سمیم ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تمکین ما ز چرخ سبکسر به باد رفت</p></div>
<div class="m2"><p>خوش دستگاه انجمن انجمیم ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مردم به کینه تشنه خون همند و بس</p></div>
<div class="m2"><p>خون می خوریم چون هم از این مردمیم ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از حد گذشت شمله دستار و ریش شیخ</p></div>
<div class="m2"><p>حیران این درازی یال و دمیم ما</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دستت ز ما بشوی مسیحا که زیر خاک</p></div>
<div class="m2"><p>آب از تف نهیب صدای قمیم ما</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پنهان به عالمیم ز بس عین عالمیم</p></div>
<div class="m2"><p>چون قطره در روانی دریا گمیم ما</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ما را مدد ز فیض ظهوری ست در سخن</p></div>
<div class="m2"><p>چون جام باده را تبه خوار خمیم ما</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>غالب ز هند نیست نوایی که می کشیم</p></div>
<div class="m2"><p>گویی ز اصفهان و هرات و قمیم ما</p></div></div>