---
title: >-
    شمارهٔ ۲۱۷
---
# شمارهٔ ۲۱۷

<div class="b" id="bn1"><div class="m1"><p>نیست معبودش حریف تاب ناز آوردنش</p></div>
<div class="m2"><p>پیش آتش دیده ام روی نیاز آوردنش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>موعظت را سنگسار قلقل مینا کند</p></div>
<div class="m2"><p>از ره گوشم به دل یک ره فراز آوردنش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا خود از بهر نثار کیست؟ من میرم ز رشک</p></div>
<div class="m2"><p>خضر و چندین کوشش و عمر دراز آوردنش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رحمت حق باد بر همدم که داند مست مست</p></div>
<div class="m2"><p>بر سر نعشم به تقریب نماز آوردنش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شوق گستاخست و من در لرزه کاخر سهل نیست</p></div>
<div class="m2"><p>صبحدم در دل به چشم نیم باز آوردنش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>وای ما گر غیر اندر خاطرش جا کرده است</p></div>
<div class="m2"><p>رفتن و پیرایه و پیرایه ساز آوردنش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>امتحان طاقت خویش ست از بیداد نیست</p></div>
<div class="m2"><p>خلق را در ناله های جانگداز آوردنش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون نمیرد قاصد اندر ره؟ که رشکم برنتافت</p></div>
<div class="m2"><p>از زبانت نکته های دلنواز آوردنش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مفت یاران وطن کز سادگیهای منست</p></div>
<div class="m2"><p>در غریبی مردن و از جور بازآوردنش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بی زبانیهای غالب را چه آسان دیده ای</p></div>
<div class="m2"><p>ای تو ناسنجیده تاب ضبط راز آوردنش</p></div></div>