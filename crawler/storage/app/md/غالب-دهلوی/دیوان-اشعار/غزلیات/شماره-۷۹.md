---
title: >-
    شمارهٔ ۷۹
---
# شمارهٔ ۷۹

<div class="b" id="bn1"><div class="m1"><p>ز من گسستی و پیوند مشکل افتاده ست</p></div>
<div class="m2"><p>مرا مگیر به خونی که در دل افتاده ست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رسد دمی که خجالت کشم ز گرمی دوست</p></div>
<div class="m2"><p>ز خصم داغم و اندیشه باطل افتاده ست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به قدر ذوق تپیدن به کشته جا بخشند</p></div>
<div class="m2"><p>سخن به محکمه در کیش قاتل افتاده ست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شکافی ار جگر ذره نم برون ندهد</p></div>
<div class="m2"><p>به وادیی که مرا بار در گل افتاده ست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در این روش به چه امید دل توان بستن</p></div>
<div class="m2"><p>میانه من و او شوق حائل افتاده ست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به ترک گریه برم دهشت اثر ز دلش</p></div>
<div class="m2"><p>که خود ز شبروی ناله غافل افتاده ست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به صبر کم نیم اما عیار ایوبی</p></div>
<div class="m2"><p>به قدر آن که گرفتند کامل افتاده ست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چرد نهنگ و سمندر در آب و آتش من</p></div>
<div class="m2"><p>تنم به قلزم و کشتی به ساحل افتاده ست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به روی صید تو از ذوق استخوان تنش</p></div>
<div class="m2"><p>هما ز تیزی پرواز بسمل افتاده ست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چو اندر آینه با خویش لابه ساز شوی</p></div>
<div class="m2"><p>ز خود بجوی که ما را چه در دل افتاده ست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>حریف ما همه بی بذله می خورد غالب</p></div>
<div class="m2"><p>مگر ز خلوت واعظ به محفل افتاده ست</p></div></div>