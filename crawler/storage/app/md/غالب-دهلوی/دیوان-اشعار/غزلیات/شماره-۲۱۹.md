---
title: >-
    شمارهٔ ۲۱۹
---
# شمارهٔ ۲۱۹

<div class="b" id="bn1"><div class="m1"><p>چون عکس پل به سیل به ذوق بلا برقص</p></div>
<div class="m2"><p>جا را نگاه دار و هم از خود جدا برقص</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نبود وفای عهد دمی خوش غنیمت ست</p></div>
<div class="m2"><p>از شاهدان به نازش عهد وفا برقص</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ذوقی ست جستجو چه زنی دم ز قطع راه</p></div>
<div class="m2"><p>رفتار گم کن و به صدای درا برقص</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سرسبز بوده و به چمن ها چمیده ایم</p></div>
<div class="m2"><p>ای شعله در گداز خس و خار ما برقص</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هم بر نوای جغد طریق سماع گیر</p></div>
<div class="m2"><p>هم در هوای جنبش بال هما برقص</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در عشق انبساط به پایان نمی رسد</p></div>
<div class="m2"><p>چون گردباد خاک شو و در هوا برقص</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فرسوده رسمهای عزیزان فرو گذار</p></div>
<div class="m2"><p>در سور نوحه خوان و به بزم عزا برقص</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون خشم صالحان و ولای منافقان</p></div>
<div class="m2"><p>در نفس خود مباش ولی بر ملا برقص</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از سوختن الم ز شگفتن طرب مجوی</p></div>
<div class="m2"><p>بیهوده در کنار سموم و صبا برقص</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>غالب بدین نشاط که وابسته که ای</p></div>
<div class="m2"><p>در خویشتن ببال و به بند بلا برقص</p></div></div>