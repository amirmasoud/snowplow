---
title: >-
    شمارهٔ ۱۲۸
---
# شمارهٔ ۱۲۸

<div class="b" id="bn1"><div class="m1"><p>کو فنا تا همه آلایش پندار برد</p></div>
<div class="m2"><p>از صور جلوه و از آینه زنگار برد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شب ز خود رفتم و بر شعله گشودم آغوش</p></div>
<div class="m2"><p>کو بدآموز که پیغاره به دلدار برد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفته باشی که به هر حیله در آتش فگنش</p></div>
<div class="m2"><p>غیر می خواست مرا بی تو به گلزار برد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>باز چسبیده لب از جوش حلاوت با هم</p></div>
<div class="m2"><p>مرگ مشکل که ز ما لذت گفتار برد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عشوه مرحمت چرخ مخر کاین عیار</p></div>
<div class="m2"><p>یوسف از چاه برآرد که به بازار برد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شوق گستاخ و تو سرمست بدان رسوایی</p></div>
<div class="m2"><p>هان ادایی که دل و دست من از کار برد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خونچکانست نسیم از اثر ناله من</p></div>
<div class="m2"><p>کیست کز سعی نظر پی به در یار برد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تو نیایی به لب بام و به کوی تو مدام</p></div>
<div class="m2"><p>دیده ذوق نگه از روزن دیوار برد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ناز را آینه ماییم بفرما تا شوق</p></div>
<div class="m2"><p>به تو از جانب ما مژده دیدار برد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مژه ات سفت دل و رفت نگاه تو فرو</p></div>
<div class="m2"><p>کز ضمیرم گله سرزنش خار برد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خاکی از رهگذر دوست به فرقم ریزید</p></div>
<div class="m2"><p>تا ز دل حسرت آرایش دستار برد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>می زند دم ز فنا غالب و تسکینش نیست</p></div>
<div class="m2"><p>بو که توفیق ز گفتار به کردار برد</p></div></div>