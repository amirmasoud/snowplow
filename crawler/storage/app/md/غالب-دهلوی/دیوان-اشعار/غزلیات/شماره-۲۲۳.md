---
title: >-
    شمارهٔ ۲۲۳
---
# شمارهٔ ۲۲۳

<div class="b" id="bn1"><div class="m1"><p>مرا که باده ندارم ز روزگار چه حظ؟</p></div>
<div class="m2"><p>ترا که هست و نیاشامی از بهار چه حظ؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خوش ست کوثر و پاکست باده ای که دروست</p></div>
<div class="m2"><p>از آن رحیق مقدس درین خمار چه حظ؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چمن پر از گل و نسرین و دلربایی نی</p></div>
<div class="m2"><p>به دشت فتنه ازین گرد بی سوار چه حظ؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به ذوق بی خبر از در درآمدن محوم</p></div>
<div class="m2"><p>به وعده ام چه نیاز و ز انتظار چه حظ؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در آنچه من نتوانم ز احتیاط چه سود؟</p></div>
<div class="m2"><p>بدانچه دوست نخواهد ز اختیار چه حظ؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چنین که نخل بلندست و سنگ ناپیدا</p></div>
<div class="m2"><p>ز میوه تا نفتد خود ز شاخسار چه حظ؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نه هر که خونی و رهزن به پایه منصورست</p></div>
<div class="m2"><p>بدین حضیض طبیعی ز اوج دار چه حظ؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به بند زحمت فرزند و زن چه می کشییم</p></div>
<div class="m2"><p>از این نخواسته غمهای روزگار چه حظ؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تو آنی آن که نشانی به جای رضوانم</p></div>
<div class="m2"><p>مرا که محو خیالم ز کار و بار چه حظ؟</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به عرض غصه نظیری وکیل غالب بس</p></div>
<div class="m2"><p>«اگر تو نشنوی از ناله های زار چه حظ؟»</p></div></div>