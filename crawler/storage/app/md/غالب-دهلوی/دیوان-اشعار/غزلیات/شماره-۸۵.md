---
title: >-
    شمارهٔ ۸۵
---
# شمارهٔ ۸۵

<div class="b" id="bn1"><div class="m1"><p>هر ذره محو جلوه حسن یگانه‌ای‌ست</p></div>
<div class="m2"><p>گویی طلسم شش جهت آینه‌خانه‌ای‌ست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حیرت به دهر بی سر و پا می‌برد مرا</p></div>
<div class="m2"><p>چون گوهر از وجود خودم آب و دانه‌ای‌ست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ناچار با تغافل صیاد ساختم</p></div>
<div class="m2"><p>پنداشتم که حلقه دام آشیانه‌ای‌ست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پابسته نورد خیالی، چو وارسی</p></div>
<div class="m2"><p>هر عالمی ز عالم دیگر فسانه‌ای‌ست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خود داریم به فصل بهاران عنان گسیخت</p></div>
<div class="m2"><p>گلگون شوق را رگ گل تازیانه‌ای‌ست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر سنگ عین ثابته آبگینه‌ای</p></div>
<div class="m2"><p>هر برگ تاک قفل در شیره‌خانه‌ای‌ست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر ذره در طریق وفای تو منزلی</p></div>
<div class="m2"><p>هر قطره از محیط خیالت کرانه‌ای‌ست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در پرده‌ای تو چند کشم ناز عالمی</p></div>
<div class="m2"><p>داغم ز روزگار و فراقت بهانه‌ای‌ست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>وحشت چو شاهدان به نظر جلوه می‌کند</p></div>
<div class="m2"><p>گرد ره و هوا سر زلفی و شانه‌ای‌ست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>غالب دگر ز منشأ آوارگی مپرس</p></div>
<div class="m2"><p>گفتم که جبهه را هوس آستانه‌ای‌ست</p></div></div>