---
title: >-
    شمارهٔ ۷۳
---
# شمارهٔ ۷۳

<div class="b" id="bn1"><div class="m1"><p>لذت عشقم ز فیض بینوایی حاصلست</p></div>
<div class="m2"><p>آنچنان تنگست دست من که پنداری دلست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هم به قدر جوشش دریا تنومندست موج</p></div>
<div class="m2"><p>تیغ سیراب از روانی های خون بسملست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>وای لب گر دل ز تاب تشنگی نگدازدم</p></div>
<div class="m2"><p>میگساران مست و من مخمور و ساقی غافلست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در خم بند تغافل نالم از بیداد عمر</p></div>
<div class="m2"><p>پرده ساز فغانم پشت چشم قاتلست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بس که ضبط مشق غم فرسود اعضای مرا</p></div>
<div class="m2"><p>راز دل از همنشینانم نهفتن مشکلست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شهری دل نیست گر حسرت مر اینجا از چه رو</p></div>
<div class="m2"><p>چشم اهل دل زبان دان نگاه سائلست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>با همه نزدیکی از وی کام دل نتوان گرفت</p></div>
<div class="m2"><p>تشنه ما بر کنار آب جو پا در گلست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در نورد گفتگو از آگهی وامانده ایم</p></div>
<div class="m2"><p>پیچ و تاب ره نشان دوری سر منزلست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>عقل در اثبات وحدت خیره می گردد چرا</p></div>
<div class="m2"><p>هر چه جز هستی ست هیچ و هر چه جز حق باطلست؟</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ما همان عین خودیم اما خود از وهم دویی</p></div>
<div class="m2"><p>در میان و غالب ما و غالب حائلست</p></div></div>