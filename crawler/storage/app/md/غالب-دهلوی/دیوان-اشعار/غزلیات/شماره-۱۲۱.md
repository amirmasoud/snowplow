---
title: >-
    شمارهٔ ۱۲۱
---
# شمارهٔ ۱۲۱

<div class="b" id="bn1"><div class="m1"><p>دماغ اهل فنا نشئه بلا دارد</p></div>
<div class="m2"><p>به فرقم اره طلوع پر هما دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به وعده گاه خرام تو کرد نمناکم</p></div>
<div class="m2"><p>بیا که شوقم از آوارگی حیا دارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گشاد شست ادای تو دلنشین منست</p></div>
<div class="m2"><p>اگر خدنگ تو در دل نشست جا دارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز من مترس که ناگه به پیش قاضی حشر</p></div>
<div class="m2"><p>هجوم ناله لبم را ز ناله وا دارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دلم فسرد بیفزا به وعده ذوق وصال</p></div>
<div class="m2"><p>چراغ کشته همان شعله خونبها دارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تپم ز رشک، همانا به جستجوی کسی ست</p></div>
<div class="m2"><p>که خور ز تاب خود آتش به زیر پا دارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پی عتاب همانا بهانه می طلبد</p></div>
<div class="m2"><p>شکایتی که ز ما نیست هم به ما دارد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خوش ست دعوی آرایش سر و دستار</p></div>
<div class="m2"><p>ز جلوه کف خاکی که نقش پا دارد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز جور دست تهی ناله از نهادم جست</p></div>
<div class="m2"><p>نیی که برگ ندارد همان نوا دارد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز سادگی رمد از حرف عشق و من به گمان</p></div>
<div class="m2"><p>که دوست تجربه ای دارد از کجا دارد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به خون تپیدن گلها نشان یکرنگی ست</p></div>
<div class="m2"><p>چمن عزای شهیدان کربلا دارد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>فغان که رحم بدآموز یار شد غالب</p></div>
<div class="m2"><p>روا نداشت که بر ما ستم روا دارد</p></div></div>