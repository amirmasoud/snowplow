---
title: >-
    شمارهٔ ۱۱۱
---
# شمارهٔ ۱۱۱

<div class="b" id="bn1"><div class="m1"><p>باده پرتو خورشید و ایاغ دم صبح</p></div>
<div class="m2"><p>مفت آنان که درآیند به باغ دم صبح</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آفتابیم، به هم دشمن و همدرد ای شمع</p></div>
<div class="m2"><p>ما هلاک سر شامیم و تو داغ دم صبح</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بعد آنان که قریب اند به ما نوبت ماست</p></div>
<div class="m2"><p>آخر کلفت شبهاست فراغ دم صبح</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زین سپس جلوه خور جای چراغان گیرد</p></div>
<div class="m2"><p>شب اندیشه ز ما یافت سراغ دم صبح</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پیش از این باد بهار این همه سرمست نبود</p></div>
<div class="m2"><p>شبنم ماست که تر کرده دماغ دم صبح</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سخن ما ز لطافت همه سر جوش میی ست</p></div>
<div class="m2"><p>که فرو ریخته از طرف ایاغ دم صبح</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ذوق مستی ز هماهنگی بلبل خیزد</p></div>
<div class="m2"><p>مفگن آواز بر آواز کلاغ دم صبح</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>حق آن گرمی هنگامه که دارم بشناس</p></div>
<div class="m2"><p>ای که در بزم تو مانم به چراغ دم صبح</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بوی گل گرنه نوید کرمت داشت چه داشت؟</p></div>
<div class="m2"><p>ای به شب کرده فراموش جناغ دم صبح</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>غالب امروز به وقتی که صبوحی زده ام</p></div>
<div class="m2"><p>چیده‌ام این گل اندیشه ز باغ دم صبح</p></div></div>