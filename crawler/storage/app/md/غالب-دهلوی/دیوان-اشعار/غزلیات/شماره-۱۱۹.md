---
title: >-
    شمارهٔ ۱۱۹
---
# شمارهٔ ۱۱۹

<div class="b" id="bn1"><div class="m1"><p>هر دم ز نشاطم دل آزاد بجنبد</p></div>
<div class="m2"><p>تا کیست درین پرده که بی باد بجنبد؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر هم زدن کار من آسان تر از آنست</p></div>
<div class="m2"><p>کز باد سحر طره شمشاد بجنبد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خواهم ز تو آزردگی غیر و چو بینم</p></div>
<div class="m2"><p>عرق حسد خاطر ناشاد بجنبد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مردم به دم و داغم از آن صید که در دام</p></div>
<div class="m2"><p>لختی پی مشغولی صیاد بجنبد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هان شیخ پریخوان می گلگون به قدح ریز</p></div>
<div class="m2"><p>تا در نظرت بال پریزاد بجنبد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>برقی به فشار آرم و ابری به تراوش</p></div>
<div class="m2"><p>زان دشنه که اندر کف جلاد بجنبد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از رشک به خون غلتم و از ذوق برقصم</p></div>
<div class="m2"><p>زان تیشه که در پنجه فرهاد بجنبد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ای آن که در اصلاح تو هرگز ندهد سود</p></div>
<div class="m2"><p>چون طبع کجت را رگ بیداد بجنبد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هر پویه که گرد دل آگاه بگردد</p></div>
<div class="m2"><p>هر چاره که در خاطر استاد بجنبد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>وصل تو به نیروی دعانیست ازین بعد</p></div>
<div class="m2"><p>خون باد زبانی که به اوراد بجنبد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>غالب قلمت پرده گشای دم عیسی ست</p></div>
<div class="m2"><p>چون بر روش طرز خداداد بجنبد</p></div></div>