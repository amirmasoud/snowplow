---
title: >-
    شمارهٔ ۱۱
---
# شمارهٔ ۱۱

<div class="b" id="bn1"><div class="m1"><p>من آن نیم که دگر می توان فریفت مرا</p></div>
<div class="m2"><p>فریبمش که مگر می توان فریفت مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به حرف ذوق نگه می توان ربود مرا</p></div>
<div class="m2"><p>به وهم تاب کمر می توان فریفت مرا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز ذکر مل به گمان می توان فگند مرا</p></div>
<div class="m2"><p>ز شاخ گل به ثمر می توان فریفت مرا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز درددل که به افسانه در میان آید</p></div>
<div class="m2"><p>به نیم جنبش سر می توان فریفت مرا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز سوز دل که به واگویه بر زبان گذرد</p></div>
<div class="m2"><p>به یک دو حرف حذر می توان فریفت مرا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>من و فریفتگی هرگز، آن محال اندیش</p></div>
<div class="m2"><p>چرا فریفت، اگر می توان فریفت مرا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خدنگ جز به گرایش گشاد نپذیرد</p></div>
<div class="m2"><p>ازو به زخم جگر می توان فریفت مرا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز باز نامدن نامه بر خوشم که هنوز</p></div>
<div class="m2"><p>به آرزوی خبر می توان فریفت مرا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شب فراق ندارد سحر ولی یک چند</p></div>
<div class="m2"><p>به گفتگوی سحر می توان فریفت مرا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نشان دوست ندانم جز این که پرده در است</p></div>
<div class="m2"><p>ز در به روزن در می توان فریفت مرا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گرسنه چشم اثر نیستم که در ره دید</p></div>
<div class="m2"><p>به کیمیای نظر می توان فریفت مرا</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>سرشت من بود این ور نه، آن نیم، غالب</p></div>
<div class="m2"><p>که از وفا به اثر می توان فریفت مرا</p></div></div>