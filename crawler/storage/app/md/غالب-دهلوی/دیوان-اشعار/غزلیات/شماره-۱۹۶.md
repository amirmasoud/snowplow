---
title: >-
    شمارهٔ ۱۹۶
---
# شمارهٔ ۱۹۶

<div class="b" id="bn1"><div class="m1"><p>بیا و جوش تمنای دیدنم بنگر</p></div>
<div class="m2"><p>چو اشک از سر مژگان چکیدنم بنگر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز من به جرم تپیدن کناره می کردی</p></div>
<div class="m2"><p>بیا به خاک من و آرمیدنم بنگر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گذشته کار من از رشک غیر شرمت باد</p></div>
<div class="m2"><p>به بزم وصل تو خود را ندیدنم بنگر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شنیده ام که نبینی و ناامید نیم</p></div>
<div class="m2"><p>ندیدن تو شنیدم شنیدنم بنگر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دمید دانه و بالید و آشیانگه شد</p></div>
<div class="m2"><p>در انتظار هما دام چیدنم بنگر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نیازمندی حسرت کشان نمی دانی</p></div>
<div class="m2"><p>نگاه من شو و دزدیده دیدنم بنگر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اگر هوای تماشای گلستان داری</p></div>
<div class="m2"><p>بیا و عالم در خون تپیدنم بنگر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جفای شانه که تاری گسسته زان سر زلف</p></div>
<div class="m2"><p>ز پشت دست به دندان گزیدنم بنگر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بهار من شو و گل گل شگفتنم دریاب</p></div>
<div class="m2"><p>به خلوتم بر و ساغر کشیدنم بنگر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به داد من نرسیدی ز درد جان دادم</p></div>
<div class="m2"><p>به داد طرز تغافل رسیدنم بنگر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تواضعی نکنم، بی تواضعی غالب</p></div>
<div class="m2"><p>به سایه خم تیغش خمیدنم بنگر</p></div></div>