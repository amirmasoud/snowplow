---
title: >-
    شمارهٔ ۳۲
---
# شمارهٔ ۳۲

<div class="b" id="bn1"><div class="m1"><p>نقشی ز خود به راهگذر بسته‌ایم ما</p></div>
<div class="m2"><p>بر دوست راه ذوق نظر بسته‌ایم ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با بنده خود این همه سختی نمی‌کنند</p></div>
<div class="m2"><p>خود را به زور بر تو مگر بسته‌ایم ما؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل مشکن و دماغ و دل خو نگاه دار</p></div>
<div class="m2"><p>کاین خود طلسم دود و شرر بسته‌ایم ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر روی حاسدان در دوزخ گشوده رشک</p></div>
<div class="m2"><p>از بهر خویش جنت در بسته‌ایم ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فرمان درد تا چه روایی گرفته است</p></div>
<div class="m2"><p>صد جا چو نی به ناله کمر بسته‌ایم ما</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سوز ترا روان همه در خویشتن گرفت</p></div>
<div class="m2"><p>از داغ تهمتی به جگر بسته‌ایم ما</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گویی وفا ندارد اثر هم به ما گرای</p></div>
<div class="m2"><p>زین سادگی که دل به اثر بسته‌ایم ما</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا در وادع خویش چه خون در جگر کنیم</p></div>
<div class="m2"><p>از کوی دوست رخت سفر بسته‌ایم ما</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هرجاست ناله همت ما حق‌گزار اوست</p></div>
<div class="m2"><p>حرزی به بال مرغ سحر بسته‌ایم ما</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از خوان نطق غالب شیرین‌سخن بود</p></div>
<div class="m2"><p>کاین مایه زله‌ها ز شکر بسته‌ایم ما</p></div></div>