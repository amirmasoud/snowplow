---
title: >-
    شمارهٔ ۱۱۸
---
# شمارهٔ ۱۱۸

<div class="b" id="bn1"><div class="m1"><p>بی دل نشد ار دل به بت غالیه مو داد</p></div>
<div class="m2"><p>گویی مگر آن دل که ز من برد به او داد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سخته ست دل غیر وگر از ننگ نگویی</p></div>
<div class="m2"><p>برگشتن مژگان تو گوید که چه رو داد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شایسته همین ما و تو بودیم که تقدیر</p></div>
<div class="m2"><p>ما را سخن نغز و ترا روی نکو داد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ساقی دگرم بود به میخانه ز مسجد</p></div>
<div class="m2"><p>می یک دو قدح بود و فریبم به سبو داد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>برخیز که دلجویی من بر تو حرام ست</p></div>
<div class="m2"><p>ای آن که ندانی خبرم زان سر کو داد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زین ساده دلی داد که چون دید به خوابم</p></div>
<div class="m2"><p>ترسید خود و مژده مرگم به عدو داد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>حسن تو به ساقیگری آیین نشناسد</p></div>
<div class="m2"><p>مست آمد و یکبار دو ساغر ز دو سو داد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در گلشنم و آرم از آن روی نکو یاد</p></div>
<div class="m2"><p>در دوزخم و خواهم از آن تندی خو داد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گفتن سخن از پایه غالب نه ز هوش ست</p></div>
<div class="m2"><p>امروز که مستم خبری خواهم ازو داد</p></div></div>