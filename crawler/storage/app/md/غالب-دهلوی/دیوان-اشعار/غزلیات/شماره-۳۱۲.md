---
title: >-
    شمارهٔ ۳۱۲
---
# شمارهٔ ۳۱۲

<div class="b" id="bn1"><div class="m1"><p>خشنود شوی چون دل خشنود نیابی</p></div>
<div class="m2"><p>ترسم که زیانکار کسی سود نیابی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از قافله گرمروان تو نباشد</p></div>
<div class="m2"><p>رختی که به سیلش شرراندود نیابی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فرقی ست نه اندک ز دلم تا به دل تو</p></div>
<div class="m2"><p>معذوری اگر حرف مرا زود نیابی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر ذوق خداداد نظردوختگانیم</p></div>
<div class="m2"><p>در سینه ما زخم نمکسود نیابی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در وجد به هنجار نفس دست فشانیم</p></div>
<div class="m2"><p>در حلقه ما رقص دف و عود نیابی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در مشرب ما خواهش فردوس نجویی</p></div>
<div class="m2"><p>در مجمع ما طالع مسعود نیابی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در باده اندیشه ما درد نبینی</p></div>
<div class="m2"><p>در آتش هنگامه ما دود نیابی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون آخر حسن ست به ما ساز که دیگر</p></div>
<div class="m2"><p>با هم کششی مانع مقصود نیابی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آن شرم که در پرده گری بود نداری</p></div>
<div class="m2"><p>آن شوق که در پرده دری بود نیابی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>غالب به دکانی که به امید گشودیم</p></div>
<div class="m2"><p>سرمایه ما جز هوس سود نیابی</p></div></div>