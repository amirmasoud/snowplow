---
title: >-
    شمارهٔ ۱۳۸
---
# شمارهٔ ۱۳۸

<div class="b" id="bn1"><div class="m1"><p>صبح ست خوش بود قدحی بر شراب زد</p></div>
<div class="m2"><p>یاقوت باده بر قوه آفتاب زد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نشتر به مغز پنبه مینا فرو برید</p></div>
<div class="m2"><p>کافاق امتلا ز هجوم سحاب زد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ذوقی می مغانه ز کردار بازداشت</p></div>
<div class="m2"><p>آه از فسون دیو که راهم به آب زد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا خاک کشتگان فریب وفای کیست</p></div>
<div class="m2"><p>کاندر هزار مرحله موج سراب زد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رنگی که در خیال خود اندوختم ز دوست</p></div>
<div class="m2"><p>تا جلوه کرد چشمک برق عتاب زد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفتم گره ز کار دل و دیده باز کن</p></div>
<div class="m2"><p>از جبهه ناگشوده به بند نقاب زد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر هوش ما بساط ادای خرام نیست</p></div>
<div class="m2"><p>نقشی توان به صفحه دیبای خواب زد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا در هجوم ناله نفس باختم به کوه</p></div>
<div class="m2"><p>سنگ از گداز خویش به رویم گلاب زد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ای لاله بر دلی که سیه کرده ای مناز</p></div>
<div class="m2"><p>داغ تو بر دماغ که بوی کباب زد؟</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>غم مشربان به چشمه حیوان نمی دهند</p></div>
<div class="m2"><p>موجی که دشنه در جگر از پیچ و تاب زد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>غالب کسان ز جهل حکیمش گرفته اند</p></div>
<div class="m2"><p>بی دانشی که طعنه بر اهل کتاب زد</p></div></div>