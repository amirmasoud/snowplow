---
title: >-
    شمارهٔ ۲۵۲
---
# شمارهٔ ۲۵۲

<div class="b" id="bn1"><div class="m1"><p>اگر بر خود نمی بالد ز غارت کردن هوشم</p></div>
<div class="m2"><p>مر او را از چه دشوارست گنجیدن در آغوشم؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نیم در بند آزادی ملامت شیوه ها دارد</p></div>
<div class="m2"><p>شنیدم جامه رندان ترا عیبست می پوشم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نیرزم هیچ چون لفظ مکرر ضایعم ضایع</p></div>
<div class="m2"><p>مگر کزلک کشد دست نوازش بر بر و دوشم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خدایا زندگی تلخ ست گر خود نقل و می نبود</p></div>
<div class="m2"><p>دلی ده کز گداز خویش گردد چشمه نوشم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مرنج از وعده وصلی که با من در میان آری</p></div>
<div class="m2"><p>که خواهد شد به ذوق وعده ای دیگر فراموشم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر امشب میرم و در هفت دوزخ سرنگون غلتم</p></div>
<div class="m2"><p>همان دانم که غرق لذت بی تابی دوشم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بخندم بر بهار و روستایی شیوه شمشادش</p></div>
<div class="m2"><p>ز گل چینان طرز جلوه سرو قبا پوشم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بهار گلشن کوی توام مسپار در خاکم</p></div>
<div class="m2"><p>چراغ بزم نیرنگ توام مپسند خاموشم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ادای می به ساغر کردنت نازم زهی ساقی</p></div>
<div class="m2"><p>بیفشان جرعه بر خاک و ز من بگذر که مدهوشم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مرنج از من اگر نبود کلامم را صفا غالب</p></div>
<div class="m2"><p>خمستان غبارم سر به سر دردی‌ست سر جوشم</p></div></div>