---
title: >-
    شمارهٔ ۳۱۸
---
# شمارهٔ ۳۱۸

<div class="b" id="bn1"><div class="m1"><p>به دل ز عربده جایی که داشتی داری</p></div>
<div class="m2"><p>شمار عهد وفایی که داشتی داری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به لب چه خیزد از انگیز وعده های وفا</p></div>
<div class="m2"><p>به دل نشست جفایی که داشتی داری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تو کی ز جور پشیمان شدی چه می گویی؟</p></div>
<div class="m2"><p>دروغ راست نمایی که داشتی داری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به سینه چون دل و در دل چو جان خزیدی و باز</p></div>
<div class="m2"><p>نگاه مهرفزایی که داشتی داری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عتاب و مهر تو از هم شناختن نتوان</p></div>
<div class="m2"><p>خرد فریب ادایی که داشتی داری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خراب باده دوشینه ای سرت گردم</p></div>
<div class="m2"><p>ادای لغزش پایی که داشتی داری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به کردگار نگردیدی و همان به فسوس</p></div>
<div class="m2"><p>حدیث روز جزایی که داشتی داری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کرشمه بار نهالی که بوده ای هستی</p></div>
<div class="m2"><p>به سر ز فتنه هوایی که داشتی داری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هنوز ناز پی غمزه گم نداند کرد</p></div>
<div class="m2"><p>ادای پرده گشایی که داشتی داری</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>جهانیان ز تو برگشته اند گر غالب</p></div>
<div class="m2"><p>ترا چه باک خدایی که داشتی داری</p></div></div>