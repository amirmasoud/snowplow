---
title: >-
    شمارهٔ ۱۶۵
---
# شمارهٔ ۱۶۵

<div class="b" id="bn1"><div class="m1"><p>پروا اگر از عربده دوش نکردند</p></div>
<div class="m2"><p>امشب چه خطر بود که می نوش نکردند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در تیغ زدن منت بسیار نهادند</p></div>
<div class="m2"><p>بردند سر از دوش و سبکدوش نکردند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از تیرگی طره شبرنگ نظرها</p></div>
<div class="m2"><p>پرواز در آن صبح بناگوش نکردند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>داغ دل ما شعله فشان ماند به پیری</p></div>
<div class="m2"><p>این شمع شب آخر شد و خاموش نکردند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>روزی که به می زور و به نی شور نهفتند</p></div>
<div class="m2"><p>اندیشه به کار خرد و هوش نکردند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر داغ نهادند وگر درد فزودند</p></div>
<div class="m2"><p>نازم که به هنگامه فراموش نکردند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خون می خورم از حسن که این گنج روان را</p></div>
<div class="m2"><p>در کار تهیدستی آغوش نکردند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اکنون خطری نیست که تا پر نشد از دل</p></div>
<div class="m2"><p>خود چاه زنخدان تو خس پوش نکردند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر خود به غلامی نپذیرند گدا باش</p></div>
<div class="m2"><p>بر در بزن آن حلقه که در گوش نکردند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>غالب ز تو آن باده که خود گفت نظیری</p></div>
<div class="m2"><p>«در کاسه ما باده سرجوش نکردند»</p></div></div>