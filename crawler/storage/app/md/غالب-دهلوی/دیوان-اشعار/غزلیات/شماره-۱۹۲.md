---
title: >-
    شمارهٔ ۱۹۲
---
# شمارهٔ ۱۹۲

<div class="b" id="bn1"><div class="m1"><p>ای دل از گلبن امید نشانی به من آر</p></div>
<div class="m2"><p>نیست گر تازه گلی برگ خزانی به من آر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا دگر زخم به ناسور توانگر گردد</p></div>
<div class="m2"><p>هدیه ای از کف الماس فشانی به من آر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همدم روز گدایی سبک از جا برخیز</p></div>
<div class="m2"><p>جان گرو، جامه گرو رطل گرانی به من آر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دلم ای شوق ز آشوب غمی نگشاید</p></div>
<div class="m2"><p>فتنه ای چند ز هنگامه ستانی به من آر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گیرم ای بخت هدف نیستم آخر گاهی</p></div>
<div class="m2"><p>غلط انداز خدنگی ز کمانی به من آر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای نیاورده به کف نامه شوقی ز کفی</p></div>
<div class="m2"><p>بی زبان مژده وصلی ز زبانی به من آر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای در اندوه تو جان داده جهانی از رشک</p></div>
<div class="m2"><p>مکش از رشکم و اندوه جهانی به من آر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ای ز تار دم شمشیر توام بستر خواب</p></div>
<div class="m2"><p>شمع بالین ز درخشنده سنانی به من آر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>یارب این مایه وجود از عدم آورده تست</p></div>
<div class="m2"><p>بوسه ای چند هم از گنج دهانی به من آر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سخن ساده دلم را نفریبد غالب</p></div>
<div class="m2"><p>نکته ای چند ز پیچیده بیانی به من آر</p></div></div>