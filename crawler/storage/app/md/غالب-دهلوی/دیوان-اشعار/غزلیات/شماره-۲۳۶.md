---
title: >-
    شمارهٔ ۲۳۶
---
# شمارهٔ ۲۳۶

<div class="b" id="bn1"><div class="m1"><p>ای ترا و مرا درین نیرنگ</p></div>
<div class="m2"><p>دهن و چشم و دست و دل همه تنگ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هم تو خود در کمین خویشتنی</p></div>
<div class="m2"><p>ای به رخ ماه و ای به خوی پلنگ</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هان مغنی که در هوای شراب</p></div>
<div class="m2"><p>می سرایی غزل به ناله چنگ</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زخمه می ریز هم بدین انداز</p></div>
<div class="m2"><p>نغمه می سنج هم بدین آهنگ</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فرصتت باد ساقی چالاک</p></div>
<div class="m2"><p>ای به دفع غم ایزدی سرهنگ</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شیشه بشکن قدح به خم درزن</p></div>
<div class="m2"><p>تا نگنجد درین میانه درنگ</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شود انبان ادیم کو آن فیض؟</p></div>
<div class="m2"><p>گردد انده نشاط کو آن رنگ؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پرتو خاص در نهاد سهیل</p></div>
<div class="m2"><p>باده ناب در دیار فرنگ</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شکوه و شکر هرزه و باطل</p></div>
<div class="m2"><p>غالب و دوست آبگینه و سنگ</p></div></div>