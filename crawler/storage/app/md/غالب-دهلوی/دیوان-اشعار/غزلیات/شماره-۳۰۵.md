---
title: >-
    شمارهٔ ۳۰۵
---
# شمارهٔ ۳۰۵

<div class="b" id="bn1"><div class="m1"><p>بر دست و پای بند گرانی نهاده ای</p></div>
<div class="m2"><p>نازم به بندگی که نشانی نهاده ای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ایمن نیم ز مرگ اگر رسته ام ز بند</p></div>
<div class="m2"><p>دلدوز ناوکی به کمانی نهاده ای</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گوهر ز بحر خیزد و معنی ز فکر ژرف</p></div>
<div class="m2"><p>بر ما خراج طبع روانی نهاده ای</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا در امید عمر به پندار بگذرد</p></div>
<div class="m2"><p>از لطف در حیات نشانی نهاده ای</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا خسته بلا نبود بی گریزگاه</p></div>
<div class="m2"><p>در مرگ احتمال امانی نهاده ای</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>رازست، گر دلی به جفایی شکسته ای</p></div>
<div class="m2"><p>دارست، گر سری به سنانی نهاده ای</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دوزخ به داغ سینه گدازی نهفته ای</p></div>
<div class="m2"><p>قلزم به چشم اشک فشانی نهاده ای</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بر هر دلی فسون نشاطی دمیده ای</p></div>
<div class="m2"><p>بر هر تنی سپاس روانی نهاده ای</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هر دیده را دری به خیالی گشوده ای</p></div>
<div class="m2"><p>هر فرقه را دلی به گمانی نهاده ای</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>غالب ز غصه مرد همانا خبر نداشت</p></div>
<div class="m2"><p>کاندر خرابه گنج نهانی نهاده ای</p></div></div>