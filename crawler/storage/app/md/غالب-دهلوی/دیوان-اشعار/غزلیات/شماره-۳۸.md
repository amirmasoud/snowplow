---
title: >-
    شمارهٔ ۳۸
---
# شمارهٔ ۳۸

<div class="b" id="bn1"><div class="m1"><p>مدام محرم صهبا بود پیاله ما</p></div>
<div class="m2"><p>به گرد مهر تنیده ست خط هاله ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زهی ز گرمی خویت نفس گرانمایه</p></div>
<div class="m2"><p>گداز ناله ما آبیار ناله ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چمن طراز جنونیم و دشت و کوه از ماست</p></div>
<div class="m2"><p>به مهر داغ شقایق بود قباله ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به دل ز جور تو دندان فشرده ایم و خوشیم</p></div>
<div class="m2"><p>ز استخوان اثری نیست در نواله ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تو زودمستی و ما رازدار خوی توایم</p></div>
<div class="m2"><p>شراب درکش و پیمانه کن حواله ما</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>درازی شب هجران ز حد گذشت، بیا</p></div>
<div class="m2"><p>فدای روی تو عمر هزار ساله ما</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جنون به بادیه پرداز گلستان بخشید</p></div>
<div class="m2"><p>سواد دیده آهوست داغ لاله ما</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز سعی هرزه به بیحاصلی علم گشتیم</p></div>
<div class="m2"><p>چو باد بید پدید آمد از اماله ما</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>همین گداختن است آبروی ما غالب</p></div>
<div class="m2"><p>گهر چه ناز فروشد به پیش ژاله ما</p></div></div>