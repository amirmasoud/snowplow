---
title: >-
    شمارهٔ ۲۵۹
---
# شمارهٔ ۲۵۹

<div class="b" id="bn1"><div class="m1"><p>در هر انجام محبت طرح آغاز افگنم</p></div>
<div class="m2"><p>مهر بردارم ازو تا هم بر او بازافگنم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در هوای قتل سر بر آستانش می نهم</p></div>
<div class="m2"><p>تا به لوح مدعا نقش خداساز افگنم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>لاف پرکاری ست صبر روستایی شیوه را</p></div>
<div class="m2"><p>خواهمش کاندر سواد اعظم ناز افگنم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>صعوه من هرزه پروازست بو کز فرط مهر</p></div>
<div class="m2"><p>بیخودش در آشیان چنگل باز افگنم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بی زبانم کرده ذوق التفات تازه ای</p></div>
<div class="m2"><p>لاجرم شغل وکالت را به غماز افگنم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر قدر کز حسرت آبم در دهن گردد همی</p></div>
<div class="m2"><p>هم ز استغنا به روی بخت ناساز افگنم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مردم از افسردگی هنگام آن آمد که باز</p></div>
<div class="m2"><p>رستخیزی در دل از خون کرد و بگداز افگنم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>همزبانم با ظهوری مطلعی کو تا ز شوق</p></div>
<div class="m2"><p>با جرس در ناله آوازی بر آواز افگنم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نامه بر گم شد در آتش نامه را باز افگنم</p></div>
<div class="m2"><p>چون کبوتر نیست طاووسی به پرواز افگنم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از نمک جان در تن طرز نکویان کرده ام</p></div>
<div class="m2"><p>زین سپس در مغز دعوی شور اعجاز افگنم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>رنجه دارد صورت اندیشه یاران مرا</p></div>
<div class="m2"><p>مفت من کایینه خود را ز پرداز افگنم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ترک صحبت کردم و در بند تکمیل خودم</p></div>
<div class="m2"><p>نغمه ام جان گشت خواهم در تن ساز افگنم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>تا ز دود اهل نظر چشمی توانند آب داد</p></div>
<div class="m2"><p>رخنه در دیوار آتشخانه راز افگنم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بگسلم بند و دهم اوراق دیوان را به باد</p></div>
<div class="m2"><p>خیل طوطی اندرین گلشن به پرواز افگنم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>غالب از آب و هوای هند بسمل گشت نطق</p></div>
<div class="m2"><p>خیز تا خود را به اصفاهان و شیراز افگنم</p></div></div>