---
title: >-
    شمارهٔ ۳۲۸
---
# شمارهٔ ۳۲۸

<div class="b" id="bn1"><div class="m1"><p>مژده خرمی و بی خللی را مانی</p></div>
<div class="m2"><p>ابدی جنت و فیض ازلی را مانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بس که همواره دلاویزی و شیرین حرکات</p></div>
<div class="m2"><p>سایه طوبی و جوی عسلی را مانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جلوه فرمایی و جاوید نمانی به کسی</p></div>
<div class="m2"><p>سیمیایی و بهشت عملی را مانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به ستم معنی پیچیده نازک باشی</p></div>
<div class="m2"><p>ای که در لطف رقمهای جلی را مانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به توانایی کوشش نتوان یافت ترا</p></div>
<div class="m2"><p>سرخوشی های قبول ازلی را مانی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جز به چشم و دل والاگهران جا نکنی</p></div>
<div class="m2"><p>جلوه نقش کف پای علی را مانی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به دل هر که به چشم تو درآید ناگاه</p></div>
<div class="m2"><p>داری آن مایه تصرف که ولی را مانی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ای که در طالع ما نقش تو هرگز ننشست</p></div>
<div class="m2"><p>زهره حوتی و شمس حملی را مانی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اندرین شیوه گفتار که داری غالب</p></div>
<div class="m2"><p>گر ترقی نکنم شیخ علی را مانی</p></div></div>