---
title: >-
    شمارهٔ ۱۰۵
---
# شمارهٔ ۱۰۵

<div class="b" id="bn1"><div class="m1"><p>دل برد و حق آنست که دلبر نتوان گفت</p></div>
<div class="m2"><p>بیداد توان دید و ستمگر نتوان گفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در رزمگهش ناچخ و خنجر نتوان برد</p></div>
<div class="m2"><p>در بزمگهش باده و ساغر نتوان گفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رخشندگی ساعد و گردن نتوان جست</p></div>
<div class="m2"><p>زیبندگی یاره و پرگر نتوان گفت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پیوسته دهد باده و ساقی نتوان خواند</p></div>
<div class="m2"><p>همواره ترا شد بت و آزر نتوان گفت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از حوصله یاری مطلب صاعقه تیزست</p></div>
<div class="m2"><p>پروانه شو اینجا ز سمندر نتوان گفت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هنگامه سرآمد چه زنی دم ز تظلم</p></div>
<div class="m2"><p>گر خود ستمی رفت به محشر نتوان گفت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در گرمروی سایه و سرچشمه نجوییم</p></div>
<div class="m2"><p>با ما سخن از طوبی و کوثر نتوان گفت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آن راز که در سینه نهانست نه وعظ ست</p></div>
<div class="m2"><p>بر دار توان گفت و به منبر نتوان گفت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کاری عجب افتاد بدین شیفته ما را</p></div>
<div class="m2"><p>مؤمن نبود غالب و کافر نتوان گفت</p></div></div>