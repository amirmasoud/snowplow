---
title: >-
    شمارهٔ ۲۱
---
# شمارهٔ ۲۱

<div class="b" id="bn1"><div class="m1"><p>تا دوخت چاره گر جگر چارپاره را</p></div>
<div class="m2"><p>از بخیه خنده بر دم تیغ ست چاره را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با اضطراب دل ز هر اندیشه فارغم</p></div>
<div class="m2"><p>آسایشی ست جنبش این گاهواره را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون شعله هم ز روی تو پیداست خوی تو</p></div>
<div class="m2"><p>تا کی به تاب باده فریبی نظاره را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سرگرم مهر شد دل چرخ ستیزه خو</p></div>
<div class="m2"><p>چندان که داغ کرده جبین ستاره را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دانی که ریگ باده غم روان چراست؟</p></div>
<div class="m2"><p>اینجا گسسته اند عنان شماره را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گیتی ز گریه ام ته و بالاست بعد ازین</p></div>
<div class="m2"><p>جویند در میانه دریا کناره را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای لذت جفای تو در خاک بعد مرگ</p></div>
<div class="m2"><p>با جان سرشته حسرت عمر دوباره را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جوهر دمید ز آینه دلخسته تا کجا</p></div>
<div class="m2"><p>دزدد به خود ز بیم نگاهت اشاره را؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خونم ستاده بود به درد فسردگی</p></div>
<div class="m2"><p>دل داد پایمردی تیغت گدازه را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شمع از فروغ چهره ساقی در انجمن</p></div>
<div class="m2"><p>چون گل به سرزده ست ز مستی نظاره را</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بنگر نخست تا ستم از جانب که بود</p></div>
<div class="m2"><p>با شیشه داوری پی داد است خاره را</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>داغم ز بخت گر همه اوج اثر گرفت</p></div>
<div class="m2"><p>آه از سپهر ریخت به فرقم اشاره را</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>غالب مرا ز گریه نوید شهادتی ست</p></div>
<div class="m2"><p>کاین سبحه رنگ داد به خون استخاره را</p></div></div>