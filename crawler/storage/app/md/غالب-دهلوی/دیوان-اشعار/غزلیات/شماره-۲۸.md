---
title: >-
    شمارهٔ ۲۸
---
# شمارهٔ ۲۸

<div class="b" id="bn1"><div class="m1"><p>به گیتی شد عیان از شیوه عجز اضطرار ما</p></div>
<div class="m2"><p>ز پشت دست ما باشد قماش روی کار ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به بیم افگنده می را چاره رنج خمار ما</p></div>
<div class="m2"><p>قدح بر خویش می لرزد ز دست رعشه دار ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خوشا جانی که اندوهی فرو گیرد سراپایش</p></div>
<div class="m2"><p>ز نومیدی توان پرسید لطف انتظار ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نشستن بر سر راه تحیر عالمی دارد</p></div>
<div class="m2"><p>که هر کس می رود از خویش می گردد دچار ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو بوی گل جنون تازیم، از مستی چه می پرسی؟</p></div>
<div class="m2"><p>گسستن دارد از صد جا عنان اختیار ما</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>فروزد هر قدر رنگ گل افزاید تب و تابش</p></div>
<div class="m2"><p>کباب آتش خویش ست پنداری بهار ما</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>حریفان شورش عشق ترا بی پرده دیدندی</p></div>
<div class="m2"><p>به دامان گر نگشتی موسم گل پرده دار ما</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هنوز از مستی چشم تو می بالد تماشایی</p></div>
<div class="m2"><p>به موج باده ماند پرتو شمع مزار ما</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بدین تمکین حریف دستبرد ناله نتوان شد</p></div>
<div class="m2"><p>بود سنگ فلاخن مر صدا را کوهسار ما</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خوشا آوارگی گر درنورد شوق بربندد</p></div>
<div class="m2"><p>به تار دامنی شیرازه مشت غبار ما</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بدین یک آسمان دردانه می بینی نمی بینی</p></div>
<div class="m2"><p>که ماه نو شد از سودن کف گوهر شمار ما</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نهال شمع را بالیدن از کاهیدن است اینجا</p></div>
<div class="m2"><p>گداز جوهر هستی است غالب آبیار ما</p></div></div>