---
title: >-
    شمارهٔ ۳۱۵
---
# شمارهٔ ۳۱۵

<div class="b" id="bn1"><div class="m1"><p>در بستن تمثال تو حیرت رقمستی</p></div>
<div class="m2"><p>بینش که به پرگار گشایی علمستی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غم را به تنومندی سهراب گرفتم</p></div>
<div class="m2"><p>خود موج می از دشنه رستم چه کمستی؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بیداد بود یکسره هشتن به کمر بر</p></div>
<div class="m2"><p>زلفی که ز انبوهی دل خم به خمستی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خرسندی دل پرده گشای اثری هست</p></div>
<div class="m2"><p>شادم که مرا این همه شادی به غمستی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفتن ز میان رفته و دانم که ندانی</p></div>
<div class="m2"><p>با من که به مرگم ز تو پرسش ستمستی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>این ابر که شوید رخ گلهای بهاری</p></div>
<div class="m2"><p>از دامن ما پرورش آموز نمستی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در بادیه از ریزش خونابه مژگان</p></div>
<div class="m2"><p>رو داد مرا هر رگ خاری قلمستی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زان سان که نظر خیره کند برق جهانسوز</p></div>
<div class="m2"><p>با حرف تمنای تو گفتن دژمستی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در عهد تو هنگام تماشای گل از شرم</p></div>
<div class="m2"><p>نظاره و گل غرقه خوناب همستی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زین نقش نوآیین که برانگیخته غالب</p></div>
<div class="m2"><p>کاغذ همه تن وقف سپاس قلمستی</p></div></div>