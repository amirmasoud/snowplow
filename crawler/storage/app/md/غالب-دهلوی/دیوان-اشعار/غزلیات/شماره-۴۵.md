---
title: >-
    شمارهٔ ۴۵
---
# شمارهٔ ۴۵

<div class="b" id="bn1"><div class="m1"><p>خیز و بیراهه روی را سر راهی دریاب</p></div>
<div class="m2"><p>شورش افزا نگه حوصله کاهی دریاب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عالم آیینه رازست چه پیدا چه نهان</p></div>
<div class="m2"><p>تاب اندیشه نداری به نگاهی دریاب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر به معنی نرسی جلوه صورت چه کم ست</p></div>
<div class="m2"><p>خم زلف و شکن طرف کلاهی دریاب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>غم افسردگیم سوخت کجایی ای شوق</p></div>
<div class="m2"><p>نفسم را به پرافشانی آهی دریاب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر توانایی ناز تو گواهیم ز عجز</p></div>
<div class="m2"><p>تاب بیجاده به جذب پر کاهی دریاب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا چه ها آینه حسرت دیدار توایم</p></div>
<div class="m2"><p>جلوه بر خود کن و ما را به نگاهی دریاب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تو در آغوشی و دست و دلم از کار شده</p></div>
<div class="m2"><p>تشنه بی دلو و رسن بر سر چاهی دریاب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>داغ ناکامی حسرت بود آیینه وصل</p></div>
<div class="m2"><p>شب روشن طلبی روز سیاهی دریاب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>فرصت از کف مده و وقت غنیمت پندار</p></div>
<div class="m2"><p>نیست گر صبح بهاری شب ماهی دریاب</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>غالب و کشمکش بیم و امیدش هیهات</p></div>
<div class="m2"><p>یا به تیغی بکش و یا به نگاهی دریاب</p></div></div>