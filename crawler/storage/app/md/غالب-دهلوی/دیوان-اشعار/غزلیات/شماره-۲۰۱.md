---
title: >-
    شمارهٔ ۲۰۱
---
# شمارهٔ ۲۰۱

<div class="b" id="bn1"><div class="m1"><p>خون قطره قطره می چکد از چشم تر هنوز</p></div>
<div class="m2"><p>نگسسته ایم بخیه زخم جگر هنوز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با آن که خاک شد به سر راه انتظار</p></div>
<div class="m2"><p>پر می زند نفس به هوای اثر هنوز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا خود پس از رسیدن قاصد چه رو دهد؟</p></div>
<div class="m2"><p>خوش می کنم دلی به امید خبر هنوز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بختم ز بزم عیش به غربت فگند و من</p></div>
<div class="m2"><p>مستم چنان که پا نشناسم ز سر هنوز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دیدار جوست دیده و دارد خجل مرا</p></div>
<div class="m2"><p>از جوش دل نبستن راه نظر هنوز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شد روز رستخیز و به یاد شب وصال</p></div>
<div class="m2"><p>محوم همان به لذت بیم سحر هنوز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای سنگ بر تو دعوی طاقت مسلم ست</p></div>
<div class="m2"><p>خود را ندیده ای به کف شیشه گر هنوز</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پرویزن ست تارکم از زخم خار پا</p></div>
<div class="m2"><p>از سر برون نرفته هوای سفر هنوز</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بلبل سزد ز غیرت پروانه سوختن</p></div>
<div class="m2"><p>رنگین به شعله نیست ترا بال و پر هنوز</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>غالب نگشته خاک به راهت تو و خدا</p></div>
<div class="m2"><p>گردی ست پرفشان به سر رهگذر هنوز</p></div></div>