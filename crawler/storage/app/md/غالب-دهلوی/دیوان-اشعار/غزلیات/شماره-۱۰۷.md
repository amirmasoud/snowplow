---
title: >-
    شمارهٔ ۱۰۷
---
# شمارهٔ ۱۰۷

<div class="b" id="bn1"><div class="m1"><p>نقشم گرفته دوست نمودن چه احتیاج؟</p></div>
<div class="m2"><p>آیینه مرا به زدودن چه احتیاج؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با پیرهن ز ناز فرو می رود به دل</p></div>
<div class="m2"><p>بند قبای دوست گشودن چه احتیاج؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون می توان به رهگذر دوست خاک شد</p></div>
<div class="m2"><p>بر خاک راه ناصیه سودن چه احتیاج؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بنگر که شعله از نفسم بال می زند</p></div>
<div class="m2"><p>دیگر ز من فسانه شنودن چه احتیاج؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از خود به ذوق زمزمه ای می توان گذشت</p></div>
<div class="m2"><p>چندین هزار پرده سرودن چه احتیاج؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در دست دیگری ست سفید و سیاه ما</p></div>
<div class="m2"><p>با روز و شب به عربده بودن چه احتیاج؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا لب گشوده ای مزه در دل دویده است</p></div>
<div class="m2"><p>بوس لب ترا به ربودن چه احتیاج؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بفگن در آتش و تب و تابم نظاره کن</p></div>
<div class="m2"><p>غمنامه مرا به گشودن چه احتیاج؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آن کن که در نگاه کسان محتشم شوی</p></div>
<div class="m2"><p>بر خویش هم ز خویش فزودن چه احتیاج؟</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خوابست وجه همت آواره بینشان</p></div>
<div class="m2"><p>محو رخ ترا به غنودن چه احتیاج</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تاب سموم فتنه گرانی ست غالبا</p></div>
<div class="m2"><p>کشت امید را به درودن چه احتیاج؟</p></div></div>