---
title: >-
    شمارهٔ ۹۳
---
# شمارهٔ ۹۳

<div class="b" id="bn1"><div class="m1"><p>نه هرزه همچو نی از مغزم استخوان خالی ست</p></div>
<div class="m2"><p>که جای ناله زاری در این میان خالی ست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روم به کعبه ز کوی تو و ز حق خجلم</p></div>
<div class="m2"><p>ز سجده جبهه و از پوزشم زبان خالی ست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هجوم گل به گلستان هلاک شوقم کرد</p></div>
<div class="m2"><p>که جا نمانده و جای تو همچنان خالی ست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گریستم نگرستی به خون تپم کامروز</p></div>
<div class="m2"><p>ز پاره جگرم چشم خونچکان خالی ست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نه شاهدی به تماشا نه بیدلی به نوا</p></div>
<div class="m2"><p>ز غنچه گلبن و از بلبل آشیان خالی ست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کنم به جنبش دل شیشه از پری لبریز</p></div>
<div class="m2"><p>سرم ز باد فسونسنجی زبان خالی ست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گرش به دیدن من گریه رو نداد چه جرم</p></div>
<div class="m2"><p>نهاد آتش شوق من از دخان خالی ست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پر از سپاس ادای تو دفتری دارم</p></div>
<div class="m2"><p>که یکسر از رقم پرسش نهان خالی ست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>امام شهر به مسجد اگر رهم ندهد</p></div>
<div class="m2"><p>نه جای من به نیایشگه مغان خالی ست؟</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خراب ذوق بر و دوش کیستم غالب</p></div>
<div class="m2"><p>که چون هلال سراپایم از میان خالی ست؟</p></div></div>