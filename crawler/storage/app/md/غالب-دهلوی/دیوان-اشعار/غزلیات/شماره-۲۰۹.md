---
title: >-
    شمارهٔ ۲۰۹
---
# شمارهٔ ۲۰۹

<div class="b" id="bn1"><div class="m1"><p>هر که را بینی ز می بیخود، ثنایش می نویس</p></div>
<div class="m2"><p>بهر دفع فتنه حرزی از برایش می نویس</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای رقم سنج یمین دوست بیکاری چرا</p></div>
<div class="m2"><p>خود سپاس دست خنجر آزمایش می نویس</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آنچه همدم هر شب غم بر سرم می بگذرد</p></div>
<div class="m2"><p>هر سحر یکسر به دیوار سرایش می نویس</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر همین دیو و غریو و رنگ و نیرنگست و بس</p></div>
<div class="m2"><p>هر کجا شیخی ست کافر ماجرایش می نویس</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خواریی کاندر طریق دوستداری رو دهد</p></div>
<div class="m2"><p>از مداد سایه بال همایش می نویس</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>می فرستی نامه وین را چشم زخمی در پی ست</p></div>
<div class="m2"><p>چشم حاسد کور بادا در دعایش می نویس</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر که بعد از مرگ عاشق بر مزارش گل برد</p></div>
<div class="m2"><p>فتوی از من در بتان زود آشنایش می نویس</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>رحمی از معشوق هر جا در کتابی بنگری</p></div>
<div class="m2"><p>بر کنار آن ورق جانها فدایش می نویس</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ای که با یارم خرامی گر دل و دستیت هست</p></div>
<div class="m2"><p>نام من در رهگذر بر خاک پایش می نویس</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هر کجا غالب تخلص در غزل بینی مرا</p></div>
<div class="m2"><p>می تراش آن را و مغلوبی به جایش می نویس</p></div></div>