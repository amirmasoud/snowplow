---
title: >-
    شمارهٔ ۱۸۶
---
# شمارهٔ ۱۸۶

<div class="b" id="bn1"><div class="m1"><p>داغم از پرده دل رو به قفا می‌آید</p></div>
<div class="m2"><p>تا ببینم که ازین پرده چه‌ها می‌آید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همچو رازی که به مستی ز دل آید بیرون</p></div>
<div class="m2"><p>در بهاران همه بویت ز صبا می‌آید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جلوه‌ای داغ که ذوقم ز نمک می‌خیزد</p></div>
<div class="m2"><p>مژده‌ای درد که ننگم ز دوا می‌آید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سود غارت‌زدگی‌های غمت را نازم</p></div>
<div class="m2"><p>که نفس می‌رود و آه رسا می‌آید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زیستم بی‌تو و زین ننگ نکشتم خود را</p></div>
<div class="m2"><p>جان فدای تو میا کز تو حیا می‌آید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دعوی گمشدگی محضر رسوایی‌هاست</p></div>
<div class="m2"><p>کز پی مور به ویرانه ما می‌آید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>راز از سینه به مضراب نریزم بیرون</p></div>
<div class="m2"><p>ساز عاشق ز شکستن به صدا می‌آید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>برگ گل پرده‌ساز است تمنای ترا</p></div>
<div class="m2"><p>بو که دریافته باشی چه نوا می‌آید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در هم افشردن اندام تو چون ما می‌خواست</p></div>
<div class="m2"><p>خنده بر تنگی آغوش قبا می‌آید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>رفته در حسرت نقش قدمی عمر به سر</p></div>
<div class="m2"><p>جاده‌ای را که به سرمنزل ما می‌آید</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>اتفاق سفر افتاد به پیری غالب</p></div>
<div class="m2"><p>آنچه از پای نیامد ز عصا می‌آید</p></div></div>