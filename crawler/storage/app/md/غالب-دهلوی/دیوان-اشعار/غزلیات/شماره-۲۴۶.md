---
title: >-
    شمارهٔ ۲۴۶
---
# شمارهٔ ۲۴۶

<div class="b" id="bn1"><div class="m1"><p>نوگرفتار تو و دیرینه آزاد خودم</p></div>
<div class="m2"><p>وه چه خوش بودی که بودی ذوق بهباد خودم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>معنی بیگانه خویشم، تکلف بر طرف</p></div>
<div class="m2"><p>چون مه نو مصرع تاریخ ایجاد خودم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جوهر اندیشه دلخون گشتنی در کار داشت</p></div>
<div class="m2"><p>غازه رخساره حسن خداداد خودم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از بهار رفته درس رنگ و بو دارم هنوز</p></div>
<div class="m2"><p>در غمت خاطر فریب جان ناشاد خودم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر فراموشی به فریادم رسد وقت ست وقت</p></div>
<div class="m2"><p>رفته ام از خویشتن چندان که در یاد خودم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گرم استغناست بامن گرچه مهرش در دلست</p></div>
<div class="m2"><p>تا نباشد دعوی تأثیر فریاد خودم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر قدم لختی ز خود رفتن بود در بار من</p></div>
<div class="m2"><p>همچو شمع بزم در راه فنازاد خودم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا چه خونها خورده ام شرمنده از روی دلم</p></div>
<div class="m2"><p>غنچه آسا پیچش طومار بیداد خودم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>می دهم دل را ز بیدادت فریب التفات</p></div>
<div class="m2"><p>سادگی بنگر که در دام تو صیاد خودم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>عالم توفیق را غالب سواد اعظمم</p></div>
<div class="m2"><p>مهر حیدر پیشه دارم حیدرآباد خودم</p></div></div>