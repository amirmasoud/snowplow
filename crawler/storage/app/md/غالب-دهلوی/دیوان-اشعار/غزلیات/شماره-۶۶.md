---
title: >-
    شمارهٔ ۶۶
---
# شمارهٔ ۶۶

<div class="b" id="bn1"><div class="m1"><p>چو صبح من ز سیاهی به شام مانندست</p></div>
<div class="m2"><p>چه گوییم که ز شب چند رفت یا چندست؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به رنج از پی راحت نگاه داشته اند</p></div>
<div class="m2"><p>ز حکمت ست که پای شکسته در بندست؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>درازدستی من چاکی ار فگند چه عیب</p></div>
<div class="m2"><p>ز پیش دلق ورع با هزار پیوندست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نگفته ای که به تلخی بساز و پند پذیر</p></div>
<div class="m2"><p>برو که باده ما تلخ تر از این پندست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>وجود او همه حسنست و هستیم همه عشق</p></div>
<div class="m2"><p>به بخت دشمن و اقبال دوست سوگندست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نگاه مهر به دل سر نداده چشمه نوش</p></div>
<div class="m2"><p>هنوز عیش به اندازه شکرخندست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز بیم آن که مبادا بمیرم از شادی</p></div>
<div class="m2"><p>نگوید ار چه به مرگ من آرزمندست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شمار کج روی دوست در نظر دارم</p></div>
<div class="m2"><p>درین نورد ندانم که آسمان چندست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اگر نه بهر من از بهر خود عزیز دار</p></div>
<div class="m2"><p>که بنده خوبی او خوبی خداوندست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نه آن بود که وفا خواهد از جهان غالب</p></div>
<div class="m2"><p>بدین که پرسد و گویند هست، خرسندست</p></div></div>