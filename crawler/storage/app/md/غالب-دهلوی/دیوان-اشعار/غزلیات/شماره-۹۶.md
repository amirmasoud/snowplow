---
title: >-
    شمارهٔ ۹۶
---
# شمارهٔ ۹۶

<div class="b" id="bn1"><div class="m1"><p>منع ما از باده عرض احتسابی بیش نیست</p></div>
<div class="m2"><p>محتسب افشرده انگور، آبی بیش نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رنج و راحت بر طرف شاهد پرستانیم ما</p></div>
<div class="m2"><p>دوزخ از سرگرمی نازش عتابی بیش نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خارج از هنگامه سر تا سر به بیکاری گذشت</p></div>
<div class="m2"><p>رشته عمر خضر مد حسابی بیش نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>قطره و موج و کف و گرداب جیحونست و بس</p></div>
<div class="m2"><p>این من و مایی که می بالد حجابی بیش نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خویش را صورت پرستان هرزه رسوا کرده اند</p></div>
<div class="m2"><p>جلوه می نامند و در معنی نقابی بیش نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شوخی اندیشه خویش ست سر تا پای ما</p></div>
<div class="m2"><p>تار و پود هستی ما پیچ و تابی بیش نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زخم دل لب تشنه شور تبسم های تست</p></div>
<div class="m2"><p>این نمکدان ها به چشم ما سرابی بیش نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نامه بر از پیشگاه ناز مکتوب مرا</p></div>
<div class="m2"><p>پاسخی آورده است اما جوابی بیش نیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جلوه کن منت منه از ذره کمتر نیستم</p></div>
<div class="m2"><p>حسن با این تابناکی آفتابی بیش نیست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چند رنگین نکته دلکش، تکلف بر طرف</p></div>
<div class="m2"><p>دیده ام دیوان غالب انتخابی بیش نیست</p></div></div>