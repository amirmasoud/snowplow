---
title: >-
    شمارهٔ ۱۶۶
---
# شمارهٔ ۱۶۶

<div class="b" id="bn1"><div class="m1"><p>عجب که مژده دهان رو به سوی ما آرند</p></div>
<div class="m2"><p>کدام مژده که آرند و از کجا آرند؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز دوستان نبود خوشنما درین هنگام</p></div>
<div class="m2"><p>که وایه بهر گدای شکسته پا آرند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز غم چنان شده ام مضمحل که اعدا را</p></div>
<div class="m2"><p>سزد که گنج گهر بهر رونما آرند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نه روی خواستن از حق بود جز آنان را</p></div>
<div class="m2"><p>که بنده وار همی طاعتش بجا آرند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نه بی رضای خدا کارها روان گردد</p></div>
<div class="m2"><p>سپهر و انجم اگر ساز مدعا آرند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نماند ساز مرا هیچ نغمه همنفسان</p></div>
<div class="m2"><p>جز آن که برشکنندش چو در نوا آرند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نخست عمر دگر خواهد از خدا غالب</p></div>
<div class="m2"><p>اگر نوید پذیرایی دعا آرند</p></div></div>