---
title: >-
    شمارهٔ ۱۶۷
---
# شمارهٔ ۱۶۷

<div class="b" id="bn1"><div class="m1"><p>شوقم ز پند بر در فریاد می‌زند</p></div>
<div class="m2"><p>بر آتش من آب دم از باد می‌زند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا افگنی چه ولوله اندر نهاد ما</p></div>
<div class="m2"><p>کآیینه از تو موج پریزاد می‌زند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از جوی شیر و عشرت خسرو نشان نماند</p></div>
<div class="m2"><p>غیرت هنوز طعنه به فرهاد می‌زند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هرگز مذاق درد اسیری نبوده است</p></div>
<div class="m2"><p>با ناله‌ای که مرغ قفس‌زاد می‌زند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ممنون کاوش مژه و نیشتر نیم</p></div>
<div class="m2"><p>دل موج خون ز درد خداداد می‌زند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خونی که دی به جیبم ازو خارخار بود</p></div>
<div class="m2"><p>امروز گل به دامن جلّاد می‌زند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اندر هوای شمع همانا ز بال و پر</p></div>
<div class="m2"><p>پروانه دشنه در جگر باد می‌زند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زین بیش نیست قافله رنگ را درنگ</p></div>
<div class="m2"><p>گل یک قدح به سایه شمشاد می‌زند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ذوقم به هر شراره که از داغ می‌جهد</p></div>
<div class="m2"><p>دل را نوای دیر بماناد می‌زند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چون دید کز شکایت بیداد فارغم</p></div>
<div class="m2"><p>بر زخم سینه‌ام نمک داد می‌زند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تا دستبرد آتش سوزان دهد به باد</p></div>
<div class="m2"><p>سنگ از شرار خنده به پولاد می‌زند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>غالب سرشک چشم تو عالم فرو گرفت</p></div>
<div class="m2"><p>موجی‌ست دجله را که به بغداد می‌زند</p></div></div>