---
title: >-
    شمارهٔ ۲۴۲
---
# شمارهٔ ۲۴۲

<div class="b" id="bn1"><div class="m1"><p>هم به عالم ز اهل عالم بر کنار افتاده‌ام</p></div>
<div class="m2"><p>چون امام سبحه بیرون از شمار افتاده‌ام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ریزم از وصف رخت گل را شرر در پیرهن</p></div>
<div class="m2"><p>آتش رشکم به جان نوبهار افتاده‌ام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>می‌فشانم بال و در بند رهایی نیستم</p></div>
<div class="m2"><p>طایر شوقم به دام انتظار افتاده‌ام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کار و بار موج با بحر است خودداری مجوی</p></div>
<div class="m2"><p>در شکست خویشتن بی‌اختیار افتاده‌ام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سر به سر میناست اجزایم چو کوه اما هنوز</p></div>
<div class="m2"><p>برنمی‌خیزم ز بس سنگین خمار افتاده‌ام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر شکست استخوانم خنده‌ای دندان‌نماست</p></div>
<div class="m2"><p>راز غم را بخیه‌ای بر روی کار افتاده‌ام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هم ز من طرز آشنای عشقبازان گشته‌ای</p></div>
<div class="m2"><p>هم ز تو عاشق‌کشان را رازدار افتاده‌ام</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا ز مستی می‌زنی بر تربت اغیار گل</p></div>
<div class="m2"><p>خویشتن را همچو آتش در مزار افتاده‌ام</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>یک جهان معنی تنومندست از پهلوی من</p></div>
<div class="m2"><p>چون قلم هر چند در ظاهر نزار افتاده‌ام</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>جان به غم می‌بازم و می‌نالم از جور سپهر</p></div>
<div class="m2"><p>وه که هم بدنقشم و هم بدقمار افتاده‌ام</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کشتی بی‌ناخدایم سرگذشت من مپرس</p></div>
<div class="m2"><p>از شکست خویش بر دریاکنار افتاده‌ام</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ناتوانی محو غم کرده‌ست اجزای مرا</p></div>
<div class="m2"><p>در پرند ناله نقش زرنگار افتاده‌ام</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>رفته از خمیازه‌ام بر باد ناموس چمن</p></div>
<div class="m2"><p>چاک اندر خرقه صبح بهار افتاده‌ام</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>از روانی‌های طبعم تشنه خون است دهر</p></div>
<div class="m2"><p>آبم آب اما تو گویی خوشگوار افتاده‌ام</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>این جواب آن غزل غالب که صائب گفته است</p></div>
<div class="m2"><p>«در نمود نقش‌ها بی‌اختیار افتاده‌ام»</p></div></div>