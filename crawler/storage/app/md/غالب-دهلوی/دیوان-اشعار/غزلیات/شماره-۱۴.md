---
title: >-
    شمارهٔ ۱۴
---
# شمارهٔ ۱۴

<div class="b" id="bn1"><div class="m1"><p>شکست رنگ تا رسوا نسازد بی قراران را</p></div>
<div class="m2"><p>جگر خونست از بیم نگاهت رازداران را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز پیکان های ناوک در دل گرمم نشان نبود</p></div>
<div class="m2"><p>به ریگستان چه جویی قطره های آب باران را؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بود پیوسته پشت صبر بر کوه از گرانجانی</p></div>
<div class="m2"><p>چه افسون خوانده ای در گوش دل امیدواران را؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کف خاکیم، از ما برنخیزد جز غبار آنجا</p></div>
<div class="m2"><p>فزون از صرصری نبود قیامت خاکساران را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به ترک جاه گو تا گردش ایام برخیزد</p></div>
<div class="m2"><p>که گلخن تاب دائم در نظر دارد بهاران را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>درآ بیخود به بازیگاه اهل حسن تا بینی</p></div>
<div class="m2"><p>به روی شعله گرم مشق جولان نی سواران را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نگشت از سجده حق جبهه زهاد نورانی</p></div>
<div class="m2"><p>چنان کافروخت تاب باده روی باده خواران را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دریغ آگاهیی کافسردگی گردد سر و برگش</p></div>
<div class="m2"><p>ز مستی بهره جز غفلت نباشد هوشیاران را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز غیرت می گدازد در خجالتگاه تأثیرم</p></div>
<div class="m2"><p>زبون دیدن به دست شیشه سازان کوهساران را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به رنجم غالب از ذوق سخن، خوش بودی ار بودی</p></div>
<div class="m2"><p>مرا لختی شکیب و پاره ای انصاف یاران را</p></div></div>