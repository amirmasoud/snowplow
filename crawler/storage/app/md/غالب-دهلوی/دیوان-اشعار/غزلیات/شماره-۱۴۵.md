---
title: >-
    شمارهٔ ۱۴۵
---
# شمارهٔ ۱۴۵

<div class="b" id="bn1"><div class="m1"><p>خوبان نه آن کنند که کس را زیان رسد</p></div>
<div class="m2"><p>دل برد تا دگر چه از آن دلستان رسد؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دارد خبر دریغ و من از سادگی هنوز</p></div>
<div class="m2"><p>سنجم همی که دوست مگر ناگهان رسد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مقصود ما ز دیر و حرم جز حبیب نیست</p></div>
<div class="m2"><p>هر جا کنیم سجده بدان آستان رسد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دردی کشان میکده در هم فتاده اند</p></div>
<div class="m2"><p>نازم به خواریی که به من زین میان رسد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گم شد نشان من چو رسیدم به کنج در</p></div>
<div class="m2"><p>مانند آن صدا که به گوش گران رسد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در دام بهر دانه نیفتم مگر قفس</p></div>
<div class="m2"><p>چندان کنی بلند که تا آشیان رسد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>راهی که تا منست همانا نه ایمنست</p></div>
<div class="m2"><p>خون می خورم که چون بخورم می چه سان رسد؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>رفتم سوی وی و مژه اندر جگر خلید</p></div>
<div class="m2"><p>زان پیشتر که سینه به نوک سنان رسد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تیر نخست را غلط انداز گفته ام</p></div>
<div class="m2"><p>ای وای گر نه تیر دگر بر نشان رسد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>امید غلبه نیست به کیش مغان درآی</p></div>
<div class="m2"><p>می گر به جزیه دست نداد ارمغان رسد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خوارم نه آنچنان که دگر مژده وصال</p></div>
<div class="m2"><p>باور کنم اگر همه از آسمان رسد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>صاحبقران ثانی اگر در جهان نماند</p></div>
<div class="m2"><p>گفتار من به ثانی صاحبقران رسد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چون نیست تاب برق تجلی کلیم را</p></div>
<div class="m2"><p>کی در سخن به غالب آتش بیان رسد</p></div></div>