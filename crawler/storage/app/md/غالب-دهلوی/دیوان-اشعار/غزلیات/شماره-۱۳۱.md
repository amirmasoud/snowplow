---
title: >-
    شمارهٔ ۱۳۱
---
# شمارهٔ ۱۳۱

<div class="b" id="bn1"><div class="m1"><p>در کلبه ما از جگر سوخته بو برد</p></div>
<div class="m2"><p>با ما گله سنجید و شماتت به عدو برد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خواهم که برد ناله غبارم ز دل دوست</p></div>
<div class="m2"><p>چون گریه تن زار مرا زان سر کو برد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همره رودش کوثر و حوران که دم مرگ</p></div>
<div class="m2"><p>ذوق می ناب و هوس روی نکو برد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بستند ره جرعه آبی به سکندر</p></div>
<div class="m2"><p>دریوزه گر میکده صهبا به کدو برد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دی رند به هنگامه خجل کرد عسس را</p></div>
<div class="m2"><p>می خورد و هم از میکده آبی به سبو برد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر ما غم تیمار دل زار سرآمد</p></div>
<div class="m2"><p>دیوانه ما را صنم سلسله مو برد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ما را نبود هستی و او را نبود صبر</p></div>
<div class="m2"><p>دستی که ز ما شست به خون که فرو برد؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دلدار تو هم چون تو فریبنده نگاری ست</p></div>
<div class="m2"><p>در حلقه وفا یکدلم آورد و دورو برد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>یک گریه پس از ضبط دو صد گریه رضا ده</p></div>
<div class="m2"><p>تا تلخی آن زهر توانم ز گلو برد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نازد به نکویان ز گرفتاری غالب</p></div>
<div class="m2"><p>گویی به گرو برد دلی را که ازو برد</p></div></div>