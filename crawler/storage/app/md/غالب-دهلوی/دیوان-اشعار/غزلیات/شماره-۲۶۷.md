---
title: >-
    شمارهٔ ۲۶۷
---
# شمارهٔ ۲۶۷

<div class="b" id="bn1"><div class="m1"><p>وحشتی در سفر از برگ سفر داشته‌ایم</p></div>
<div class="m2"><p>توشه راه دلی بود که برداشته‌ایم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>لغزد از تاب بناگوش تو مستانه و ما</p></div>
<div class="m2"><p>تکیه بر پاکی دامان گهر داشته‌ایم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زخم ناخورده ما روزی اغیار مکن</p></div>
<div class="m2"><p>کان به آرایش دامان نظر داشته‌ایم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ناله تا گم نکند راه لب از ظلمت غم</p></div>
<div class="m2"><p>جان چراغی‌ست که بر راهگذر داشته‌ایم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تو دماغ از می پر زور رسانیده و ما</p></div>
<div class="m2"><p>بر در خُمکده خشتی ته سر داشته‌ایم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جا گرفتن به دل دوست نه اندازه ماست</p></div>
<div class="m2"><p>تو همان گیر که آهیم و اثر داشته‌ایم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مژه تا خون دل افشاند ز ریزش استاد</p></div>
<div class="m2"><p>ماتم طالع اجزای جگر داشته‌ایم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>داغ احسان قبولی ز لئیمانش نیست</p></div>
<div class="m2"><p>ناز بر خرمی بخت هنر داشته‌ایم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>پیش ازین مشرب ما نیز سخن‌سازی بود</p></div>
<div class="m2"><p>لختی از خوشدلی غیر خبر داشته‌ایم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>وارسیدیم که غالب به میان بود نقاب</p></div>
<div class="m2"><p>کاش دانیم که از روی که برداشته‌ایم؟</p></div></div>