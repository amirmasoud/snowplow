---
title: >-
    شمارهٔ ۲۴۷
---
# شمارهٔ ۲۴۷

<div class="b" id="bn1"><div class="m1"><p>این چه شورست که از شوق تو در سر دارم؟</p></div>
<div class="m2"><p>دل پروانه و تمکین سمندر دارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آهم از پرده دل بی تو شرر می بیزد</p></div>
<div class="m2"><p>شیشه لبریز می و سینه پر آذر دارم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای متاع دو جهان رنگ به عرض آورده</p></div>
<div class="m2"><p>هان صلایی که ازین جمله دلی بردارم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>من و پشتی که به خورشید قیامت گرم ست</p></div>
<div class="m2"><p>تکیه بر داوری عرصه محشر دارم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آن چرا در طرب و این ز چه ره در تعب است؟</p></div>
<div class="m2"><p>خنده بر غفلت درویش و توانگر دارم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کیست تا خار و خس از رهگذرش برچیند؟</p></div>
<div class="m2"><p>دگر امشب سر آرایش بستر دارم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پرتو مهر سیاهی ز گلیمم نبرد</p></div>
<div class="m2"><p>سایه ام سایه شب و روز برابر دارم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سوخت دل بی تو ز وصلم چه گشاید اکنون؟</p></div>
<div class="m2"><p>حسرتت بیشتر و ذوق تو کمتر دارم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کهنه تاریخی داغم نفسم شعله ورست</p></div>
<div class="m2"><p>شرح کشاف صد آتشکده از بر دارم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هم ز شادابی ناز تو به خود می بالم</p></div>
<div class="m2"><p>ریشه در آب ز تار دم خنجر دارم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>رازدار تو و بدنام کن گردش چرخ</p></div>
<div class="m2"><p>هم سپاس از تو و هم شکوه ز اختر دارم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مرحبا سوهن و جان بخشی آبش غالب</p></div>
<div class="m2"><p>خنده بر گمرهی خضر و سکندر دارم</p></div></div>