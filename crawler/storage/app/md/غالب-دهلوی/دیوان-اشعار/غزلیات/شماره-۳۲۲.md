---
title: >-
    شمارهٔ ۳۲۲
---
# شمارهٔ ۳۲۲

<div class="b" id="bn1"><div class="m1"><p>از جسم به جان نقاب تا کی؟</p></div>
<div class="m2"><p>این گنج درین خراب تا کی؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این گوهر پرفروغ یارب</p></div>
<div class="m2"><p>آلوده خاک و آب تا کی؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>این راهرو مسالک قدس</p></div>
<div class="m2"><p>وامانده خورد و خواب تا کی؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بی تابی برق جز دمی نیست</p></div>
<div class="m2"><p>ما وین همه اضطراب تا کی؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جان در طلب نجات تا چند</p></div>
<div class="m2"><p>دل در تعب عتاب تا کی؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پرسش ز تو بی حساب باید</p></div>
<div class="m2"><p>غمهای مرا حساب تا کی؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>غالب به چنین کشاکش اندر</p></div>
<div class="m2"><p>یا حضرت بوتراب تا کی؟</p></div></div>