---
title: >-
    شمارهٔ ۹۹
---
# شمارهٔ ۹۹

<div class="b" id="bn1"><div class="m1"><p>یاد از عدو نیارم وین هم ز دوربینی ست</p></div>
<div class="m2"><p>کاندر دلم گذشتن با دوست همنشینی ست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در عالم خرابی از خیل منعمانم</p></div>
<div class="m2"><p>سیلم به رخت شویی برقم به خوشه چینی ست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>میرم ولی بترسم کز فرط بدگمانی</p></div>
<div class="m2"><p>داند که جان سپردن از عافیت گزینی ست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در باده دیر مستم آری ز سخت جانی ست</p></div>
<div class="m2"><p>در غمزه زودرنجی آری ز نازنینی ست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>من سوی او ببینم داند ز بی حیایی ست</p></div>
<div class="m2"><p>او سوی من نبیند دانم ز شرمگینی ست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ذوقی ست در ادایت قاصد تو و خدایت</p></div>
<div class="m2"><p>در جیب من بیفشان خلدی که آستینی ست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زین خونچکان نواها دریاب ماجراها</p></div>
<div class="m2"><p>هنگامه ام اسیری اندیشه ام حزینی ست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>درد شکست دل را رام صدا نخواهم</p></div>
<div class="m2"><p>ساز شکایت من تارش ز موی چینی ست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نازم به زودیابی نازد به گوش و گردن</p></div>
<div class="m2"><p>چندان که ابر نیسان در گوهرآفرینی ست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سوزم دمی که یارم یاد آورد که غالب</p></div>
<div class="m2"><p>در خاطرش گذشتن با غیر همنشینی ست</p></div></div>