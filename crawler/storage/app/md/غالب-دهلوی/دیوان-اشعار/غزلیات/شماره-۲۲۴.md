---
title: >-
    شمارهٔ ۲۲۴
---
# شمارهٔ ۲۲۴

<div class="b" id="bn1"><div class="m1"><p>تا رغبت وطن نبود از سفر چه حظ؟</p></div>
<div class="m2"><p>آن را که نیست خانه به شهر از خبر چه حظ؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از ناله مست زمزمه ام همنشین برو</p></div>
<div class="m2"><p>چون نیست مطلبی ز نوید اثر چه حظ؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در هم فگنده ایم دل و دیده را ز رشک</p></div>
<div class="m2"><p>چون جنگ با خودست ز فتح و ظفر چه حظ؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دلهای مرده را به نشاط نفس چه کار؟</p></div>
<div class="m2"><p>گلهای چیده را ز نسیم سحر چه حظ؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا فتنه در نظر ننهی از نظر چه سود؟</p></div>
<div class="m2"><p>تا دشنه بر جگر نخوری از جگر چه حظ؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زانسوی کاخ روزن دیوار بسته اند</p></div>
<div class="m2"><p>بی دوست از مشاهده بام و در چه حظ؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>لرزد به جان دوست دل ساده ام ز مهر</p></div>
<div class="m2"><p>بیچاره را ز غمزه تاب کمر چه حظ؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون پرده محافه به بالا نمی زند</p></div>
<div class="m2"><p>از وی به داعیان سر رهگذر چه حظ؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>باید نبشت نکته غالب به آب زر</p></div>
<div class="m2"><p>بی آنکه وجه می شود از سیم و زر چه حظ؟</p></div></div>