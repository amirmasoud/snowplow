---
title: >-
    شمارهٔ ۱۴۱
---
# شمارهٔ ۱۴۱

<div class="b" id="bn1"><div class="m1"><p>تا کیم دود شکایت ز بیان برخیزد</p></div>
<div class="m2"><p>بزن آتش که شنیدن ز میان برخیزد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می رمی از من و خلقی به گمانست ز تو</p></div>
<div class="m2"><p>بی محابا شو و بنشین که گمان برخیزد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر دهم شرح عتابی که به دلها داری</p></div>
<div class="m2"><p>دود از کارگه شیشه گران برخیزد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با قدت سرو چو شخصی ست که ناگه یکبار</p></div>
<div class="m2"><p>بیخود از جا ز هجوم خفقان برخیزد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به چه گیرند عیار هوس و عشق دگر</p></div>
<div class="m2"><p>رسم بیداد مبادا ز جهان برخیزد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کشته دعوی پیدایی خویشیم همه</p></div>
<div class="m2"><p>وای گر پرده ازین راز نهان برخیزد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زینهار از تعب دوزخ جاوید مترس</p></div>
<div class="m2"><p>خوش بهاری ست کزو بیم خزان برخیزد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ناله برخاست دم جستن از آتش ز سپند</p></div>
<div class="m2"><p>کو شگرفی که چو ما از سر جان برخیزد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جزوی از عالمم و از همه عالم بیشم</p></div>
<div class="m2"><p>همچو مویی که بتان را ز میان برخیزد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>عمرها چرخ بگردد که جگر سوخته ای</p></div>
<div class="m2"><p>چون من از دوده آذرنفسان برخیزد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گر دهم شرح ستمهای عزیزان غالب</p></div>
<div class="m2"><p>رسم امید همانا ز جهان برخیزد</p></div></div>