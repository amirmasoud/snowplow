---
title: >-
    شمارهٔ ۳۰
---
# شمارهٔ ۳۰

<div class="b" id="bn1"><div class="m1"><p>چون عذار خویش دارد نامه اعمال ما</p></div>
<div class="m2"><p>ساده پرکار فراوان شرم اندک سال ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>میل ما سوی وی و میلش به سوی چون خودی ست</p></div>
<div class="m2"><p>آرد از خود رفتنش ناگه به استقبال ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حال ما از غیر می پرسی و منت می بریم</p></div>
<div class="m2"><p>آگهی باری که آگه نیستی از حال ما؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عیش و غم در دل نمی استد خوشا آزادگی</p></div>
<div class="m2"><p>باده و خونابه یکسانست در غربال ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نقش ما در خاطر یاران دژم صورت گرفت</p></div>
<div class="m2"><p>بس که رو در هم کشید آیینه از تمثال ما</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نیشتر سازید و بگدازید هر جا تیشه ای ست</p></div>
<div class="m2"><p>خون گرم کوهکن دارد رگ قیفال ما</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ما همای گرم پروازیم، فیض از ما مجوی</p></div>
<div class="m2"><p>سایه همچون دود، بالا می رود از بال ما</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خضر و در سرچشمه حیوان فرو غلتیدنش</p></div>
<div class="m2"><p>لغزش پایی ست کش رو داده در دنبال ما</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خاک را از ابر ادرار معین داده اند</p></div>
<div class="m2"><p>بی می پارینه بر ما رانده اند، امسال ما</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>با چنین گنجینه ارزد اژدهایی همچنین</p></div>
<div class="m2"><p>حلقه بر گرد دل ما زد زبان لال ما</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>جان غالب تاب گفتاری گمان داری هنوز؟</p></div>
<div class="m2"><p>سخت بی‌دردی که می‌پرسی ز ما احوال ما</p></div></div>