---
title: >-
    شمارهٔ ۱۴۰
---
# شمارهٔ ۱۴۰

<div class="b" id="bn1"><div class="m1"><p>گرسنه به که برآید ز فاقه جانش و لرزد</p></div>
<div class="m2"><p>از آن که دررسد از راه میهمانش و لرزد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نفس به گرد دل از مهر می تپد به فراقت</p></div>
<div class="m2"><p>چو طایری که بسوزانی آشیانش و لرزد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>منم به وصل به گنجینه راه یافته دزدی</p></div>
<div class="m2"><p>که در ضمیر بود بیم پاسبانش و لرزد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دگر به کام خود ای دل چه بهره برد توانی</p></div>
<div class="m2"><p>ز ساده ای که زنی بوسه بر دهانش و لرزد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نترسد ار ز گسستن خدا نخواسته باشد</p></div>
<div class="m2"><p>چرا رسد سر آن طره بر میانش و لرزد؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز شور ناله دل دارد اضطراب روانم</p></div>
<div class="m2"><p>چو رایضی که ز کف در رود عنانش و لرزد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز جنبش مژه مانی دم نگاه به مستی</p></div>
<div class="m2"><p>که بی اراده جهد تیر از کمانش و لرزد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز شیخ وجد به ذوق نشاط نغمه نیابی</p></div>
<div class="m2"><p>مگر به دل گذرد مرگ ناگهانش و لرزد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>فغان ز خجلت صراف کم عیار که ناگه</p></div>
<div class="m2"><p>برآورند زر قلب از دکانش و لرزد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گر از فشاندن جان شور نیست در سر غالب</p></div>
<div class="m2"><p>چرا به سجده نهد سر بر آستانش و لرزد</p></div></div>