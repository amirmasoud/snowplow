---
title: >-
    شمارهٔ ۱۰۲
---
# شمارهٔ ۱۰۲

<div class="b" id="bn1"><div class="m1"><p>ساخت ز راستی به غیر ترک فسونگری گرفت</p></div>
<div class="m2"><p>زهره به طالع عدو شیوه مشتری گرفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شه به گدا کجا رسد زان که چو فتنه روی داد</p></div>
<div class="m2"><p>خاتم دست دیو برد کشور دل پری گرفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ترک مرا ز گیر و دار شغل غرض بود نه سود</p></div>
<div class="m2"><p>فربه اگر نیافت صید خرده به لاغری گرفت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آمد و از ره غرور بوسه به خلوتم نداد</p></div>
<div class="m2"><p>رفت و در انجمن ز غیر مزد نواگری گرفت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای که دلت ز غصه سوخت شکوه نه در خور وفاست</p></div>
<div class="m2"><p>ور سزد آن که سرکنی گیر که سرسری گرفت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جاده شناس کوی خصم بودم و دوست راه جوی</p></div>
<div class="m2"><p>منکر ذوق همرهی خرده به رهبری گرفت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مستی مرغ صبحدم بر رخ گل به بوی تست</p></div>
<div class="m2"><p>هرزه ز شرم باغبان جبهه گل تری گرفت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>رای زدم که بار غم هم به رقم ز دل رود</p></div>
<div class="m2"><p>نامه چو بستمش به بال مرغ سبکپری گرفت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>غالب اگر به بزم شعر دیر رسید دور نیست</p></div>
<div class="m2"><p>کش به فراق حسرتی دل ز سخنوری گرفت</p></div></div>