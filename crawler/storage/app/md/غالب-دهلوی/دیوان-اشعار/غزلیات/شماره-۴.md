---
title: >-
    شمارهٔ ۴
---
# شمارهٔ ۴

<div class="b" id="bn1"><div class="m1"><p>ای به خلا و ملا خوی تو هنگامه زا</p></div>
<div class="m2"><p>با همه در گفتگو بی همه با ماجرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شاهد حسن ترا در روش دلبری</p></div>
<div class="m2"><p>طره پر خم صفات موی میان ماسوا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دیده وران را کند دید تو بینش فزون</p></div>
<div class="m2"><p>از نگه تیزرو گشته نگه توتیا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آب نبخشی به زور خون سکندر هدر</p></div>
<div class="m2"><p>جان نپذیری به هیچ نقد خضر ناروا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بزم ترا شمع و گل خستگی بوتراب</p></div>
<div class="m2"><p>ساز ترا زیر و بم واقعه کربلا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نکبتیان ترا قافله بی آب و نان</p></div>
<div class="m2"><p>نعمتیان ترا مائده بی اشتها</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گرمی نبض کسی کز تو به دل داشت سوز</p></div>
<div class="m2"><p>سوخته در مغز خاک ریشه دارو گیا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مصرف زهر ستم داده به یاد توام</p></div>
<div class="m2"><p>سبز بود جای من در دهن اژدها</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کم مشمر گریه ام زانکه به علم ازل</p></div>
<div class="m2"><p>بوده در این جوی آب گردش هفت آسیا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ساده ز علم و عمل، مهر تو ورزیده ایم</p></div>
<div class="m2"><p>مستی ما پایدار، باده ما ناشتا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خلد به غالب سپار زان که بدان روضه در</p></div>
<div class="m2"><p>نیک بود عندلیب خاصه نوآیین نوا</p></div></div>