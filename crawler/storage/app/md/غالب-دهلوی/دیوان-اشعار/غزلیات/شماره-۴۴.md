---
title: >-
    شمارهٔ ۴۴
---
# شمارهٔ ۴۴

<div class="b" id="bn1"><div class="m1"><p>ز من گرت نبود باور انتظار، بیا</p></div>
<div class="m2"><p>بهانه جوی مباش و ستیزه کار، بیا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به یک دو شیوه ستم دل نمی شود خرسند</p></div>
<div class="m2"><p>به مرگ من که به سامان روزگار بیا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بهانه جوست در الزام مدعی شوقت</p></div>
<div class="m2"><p>یکی به رغم دل ناامیدوار بیا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هلاک شیوه تمکین مخواه مستان را</p></div>
<div class="m2"><p>عنان گسسته تر از باد نوبهار بیا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز ما گسستی و با دیگران گرو بستی</p></div>
<div class="m2"><p>بیا، که عهد وفا نیست استوار، بیا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>وداع و وصل جداگانه لذتی دارد</p></div>
<div class="m2"><p>هزار بار برو، صد هزار بار بیا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تو طفل ساده دل و همنشین بدآموزست</p></div>
<div class="m2"><p>جنازه گر نتوان دید بر مزار بیا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>فریب خورده نازم چه ها نمی خواهم؟</p></div>
<div class="m2"><p>یکی به پرسش جان امیدوار بیا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز خوی تست نهاد شکیب نازکتر</p></div>
<div class="m2"><p>بیا که دست و دلم می رود ز کار، بیا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>رواج صومعه هستی ست زینهار مرو</p></div>
<div class="m2"><p>متاع میکده مستی ست هوشیار، بیا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>حصار عافیتی گر هوس کنی غالب</p></div>
<div class="m2"><p>چو ما به حلقه رندان خاکسار بیا</p></div></div>