---
title: >-
    شمارهٔ ۳۲۵
---
# شمارهٔ ۳۲۵

<div class="b" id="bn1"><div class="m1"><p>رفت آن که کسب بوی تو از باد کردمی</p></div>
<div class="m2"><p>گل دیدمی و روی ترا یاد کردمی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رفت آن که گر به راه تو جان دادمی ز ذوق</p></div>
<div class="m2"><p>از موج گرد ره نفس ایجاد کردمی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رفت آن که گر لبت نه به نفرین نواختی</p></div>
<div class="m2"><p>رنجیدمی و عربده بنیاد کردمی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رفت آن که قیس را به سترگی ستودمی</p></div>
<div class="m2"><p>در چابکی ستایش فرهاد کردمی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رفت آن که جانب رخ و قدت گرفتمی</p></div>
<div class="m2"><p>در جلوه بحث با گل و شمشاد کردمی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>رفت آن که در ادای سپاس پیام تو</p></div>
<div class="m2"><p>هرگونه مرغ صد قفس آزاد کردمی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اکنون خود از وفای تو آزار می کشم</p></div>
<div class="m2"><p>رفت آن که از جفای تو فریاد کردمی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بندم منه ز طره که تابم نمانده است</p></div>
<div class="m2"><p>رفت آن که خویش را به بلا شاد کردمی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آخر به دادگاه دگر اوفتاد کار</p></div>
<div class="m2"><p>رفت آن که از تو شکوه بیداد کردمی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>غالب هوای کعبه به سر جا گرفته است</p></div>
<div class="m2"><p>رفت آن که عزم خلخ و نوشاد کردمی</p></div></div>