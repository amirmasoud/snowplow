---
title: >-
    شمارهٔ ۹۴
---
# شمارهٔ ۹۴

<div class="b" id="bn1"><div class="m1"><p>گلشن به فضای چمن سینه ما نیست</p></div>
<div class="m2"><p>هر دل که نه زخمی خورد از تیغ تو وا نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می سوزم و می ترسم از آسیب ز دانش</p></div>
<div class="m2"><p>آوخ که در آتش اثر آب بقا نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عمری ست که می میرم و مردن نتوانم</p></div>
<div class="m2"><p>در کشور بیداد تو فرمان قضا نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هفت اخیر و نه چرخ خود آخر به چه کارند؟</p></div>
<div class="m2"><p>بر قتل من این عربده با یار روا نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عمری سپری گشت و همان بر سر جورست</p></div>
<div class="m2"><p>گویند بتان را که وفا نیست، چرا نیست؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جنت نکند چاره افسردگی دل</p></div>
<div class="m2"><p>تعمیر به اندازه ویرانی ما نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>با خصم زبون غیر ترحم چه توان کرد</p></div>
<div class="m2"><p>من ضامن تأثیر اگر ناله رسا نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>فریاد ز زخمی که نمکسود نباشد</p></div>
<div class="m2"><p>هنگامه بیفزای که پرسش به سزا نیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر مهر وگر کین همه از دوست قبول ست</p></div>
<div class="m2"><p>اندیشه جز آیینه تصویرنما نیست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مینای می از تندی این می بگدازد</p></div>
<div class="m2"><p>پیغام غمت در خور تحویل صبا نیست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هر مرحله از دهر سرابست لبی را</p></div>
<div class="m2"><p>کز نقش کف پای کسی بوسه ربا نیست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>از ناز دل بی هوس ما نپسندید</p></div>
<div class="m2"><p>دل تنگ شد و گفت در این خانه هوا نیست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>برگشتن مژگان تو از روی عتابست</p></div>
<div class="m2"><p>کاندر دلم از تنگی جا یک مژه جا نیست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>دریوزه راحت نتوان کرد ز مرهم</p></div>
<div class="m2"><p>غالب همه تن خسته یارست گدا نیست</p></div></div>