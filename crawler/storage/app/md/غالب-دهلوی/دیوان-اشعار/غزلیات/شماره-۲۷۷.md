---
title: >-
    شمارهٔ ۲۷۷
---
# شمارهٔ ۲۷۷

<div class="b" id="bn1"><div class="m1"><p>چون شمع رود شب همه شب دود ز سرمان</p></div>
<div class="m2"><p>زین گونه کرا روز به سر رفت؟ مگرمان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آذر بپرستیم و رخ از شعله نتابیم</p></div>
<div class="m2"><p>ای خوانده به سوی خود ازین راهگذرمان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در عشق تو ضرب المثل راهروانیم</p></div>
<div class="m2"><p>بگذار به ره خفته و از بیشه مبرمان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از بی خردی کوی ترا خلد شمردیم</p></div>
<div class="m2"><p>چونست که در کوی تو ره نیست دگرمان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مستیم بیا تن زن و لب بر لب ما نه</p></div>
<div class="m2"><p>حاشا که بود تفرقه لب ز شکرمان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>طول شب هجران بود اندر حق ما خاص</p></div>
<div class="m2"><p>از همنفسان کس نشناسد به سحرمان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بی وجه می آشفته و خواریم بدا ما</p></div>
<div class="m2"><p>در میکده از ما نستانند اگرمان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از ارزش ما بی هنران مانده شگفتی</p></div>
<div class="m2"><p>در بند غم انداخته گردون به هنرمان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون تازگی حوصله خویش نداند</p></div>
<div class="m2"><p>داند که بود ناله به امید اثرمان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>غالب چه زیان ناله اگر گرمروی کرد</p></div>
<div class="m2"><p>سوزی به دل اندر نه و داغی به جگرمان</p></div></div>