---
title: >-
    شمارهٔ ۱۲۳
---
# شمارهٔ ۱۲۳

<div class="b" id="bn1"><div class="m1"><p>به ذوقی سر ز مستی در قفای ره روان دارد</p></div>
<div class="m2"><p>که پنداری کمند یار همچون مار جان دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تنم ساز تمنایی ست کز هر زخمه دردی</p></div>
<div class="m2"><p>هما را مست آواز شکست استخوان دارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هوای ساقیی دارم که تاب ذوق رفتارش</p></div>
<div class="m2"><p>صراحی را چو طاووسان بسمل پرفشان دارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بنازم سادگی طفل ست و خونریزی نمی داند</p></div>
<div class="m2"><p>به گل چیدن همان ذوق شمار کشتگان دارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دل از هم ریزد و حسرت اساس محکمی خواهد</p></div>
<div class="m2"><p>غم آذر بیزد و طاقت قماش پرنیان دارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>برون بردم گلیم از موج دامن زیر کوه آمد</p></div>
<div class="m2"><p>نم گرداب طوفان تا چه رختم را گران دارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>برنجد از دم تیغ تو صید و در رمیدن ها</p></div>
<div class="m2"><p>به امید تلافی چشم بر پشت کمان دارد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دلم در حلقه دام بلا می رقصد از شادی</p></div>
<div class="m2"><p>همانا خویشتن را در خم زلفش گمان دارد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به گلهای بهشتم مژده نتوان داد در راهش</p></div>
<div class="m2"><p>من و خاکی که از نقش کف پایی نشان دارد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به شرع آویز و حق می جو کم از مجنون نه‌ای باری</p></div>
<div class="m2"><p>دلش با محمل است اما زبان با ساربان دارد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>رمم زان ترک صیدافگن که خواهم صرف من گردد</p></div>
<div class="m2"><p>گسستن های بی اندازه ای کاندر عنان دارد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>خدا را وقت پرسش نیست گفتم بگذر از غالب</p></div>
<div class="m2"><p>که هم جان بر لب و هم داستان‌ها بر زبان دارد</p></div></div>