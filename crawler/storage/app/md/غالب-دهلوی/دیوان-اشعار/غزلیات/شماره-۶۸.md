---
title: >-
    شمارهٔ ۶۸
---
# شمارهٔ ۶۸

<div class="b" id="bn1"><div class="m1"><p>چشمم از ابر اشکبارترست</p></div>
<div class="m2"><p>از عرق جبهه بهارترست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گریه کرد از فریب و زارم کشت</p></div>
<div class="m2"><p>نگه از تیغ آبدارترست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>می برانگیزدش به کشتن من</p></div>
<div class="m2"><p>دشمن از دوست غمگسارترست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دی مگر مست بوده ای کامروز</p></div>
<div class="m2"><p>شکرم از شکوه ناگوارترست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای که خوی تو همچو روی تو نیست</p></div>
<div class="m2"><p>دیده از دل امیدوارترست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نو به دولت رسیده را نگرید</p></div>
<div class="m2"><p>خطش از زلف مشکبارترست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>طفلی و پر دلیر می شکنی</p></div>
<div class="m2"><p>آه عهدی که استوارترست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>همه عجز و نیاز می خواهند</p></div>
<div class="m2"><p>زارتر هر که حق گزارترست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خسته از راه دور می آیم</p></div>
<div class="m2"><p>پا ز تن پاره ای فگارترست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شکوه از خوی دوست نتوان کرد</p></div>
<div class="m2"><p>باده تند سازگارترست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>می‌رسد گر به خویشتن نازد</p></div>
<div class="m2"><p>غالب از خویش خاکسارترست</p></div></div>