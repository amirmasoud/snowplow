---
title: >-
    شمارهٔ ۲۰۶
---
# شمارهٔ ۲۰۶

<div class="b" id="bn1"><div class="m1"><p>داغ تلخ گویانم لذت سم از من پرس</p></div>
<div class="m2"><p>محو تندخویانم حیرت رم از من پرس</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>موجی از شرابستم لختی از کبابستم</p></div>
<div class="m2"><p>شور من هم از من جوی سوز من هم از من پرس</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نیست با غنودن‌ها برگ پر گشودن‌ها</p></div>
<div class="m2"><p>از عدم برون آمد سعی آدم از من پرس</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نفس چون زبون گردد دیو را به فرمان گیر</p></div>
<div class="m2"><p>محرم سلیمانم نقش خاتم از من پرس</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای که در دل آزاری بیش را کم انگاری</p></div>
<div class="m2"><p>در شمار غمخواری بیشی کم از من پرس</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بوسه از لبانم ده عمر خضر از من خواه</p></div>
<div class="m2"><p>جام می به پیشم نه عشرت جم از من پرس</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تیغ غمزه با اغیار آنچه کرد می دانی</p></div>
<div class="m2"><p>خنجر تغافل را تیزی دم از من پرس</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خلد را نهادم من لطف کوثر از من جوی</p></div>
<div class="m2"><p>کعبه را سوادم من شور زمزم از من پرس</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ورد من بود غالب یا علی بوطالب</p></div>
<div class="m2"><p>نیست بخل با طالب اسم اعظم از من پرس</p></div></div>