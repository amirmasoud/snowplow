---
title: >-
    شمارهٔ ۱۹
---
# شمارهٔ ۱۹

<div class="b" id="bn1"><div class="m1"><p>تعالی الله به رحمت شاد کردن بی‌گناهان را</p></div>
<div class="m2"><p>خجل نپسندد آزرم کرم بی‌دستگاهان را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خوی شرم گنه در پیشگاه رحمت عامت</p></div>
<div class="m2"><p>سهیل و زهره افشانده ز سیما روسیاهان را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زهی دردت که با یک عالم آشوب جگرخایی</p></div>
<div class="m2"><p>دود در دل گدایان را و در سر پادشاهان را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به حرفی حلقه در گوش افگنی آزادمردان را</p></div>
<div class="m2"><p>به خوابی مغز در شور آوری بالین‌پناهان را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز شوقت بی‌قراری آرزو، خارا نهادان را</p></div>
<div class="m2"><p>به بزمت لای‌خواری آبرو، پرویزجاهان را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به بزمت شادم اما زین خجالت چون برون آیم؟</p></div>
<div class="m2"><p>که رشکم، در جحیم افگند، خلد آرامگاهان را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به دل‌ها ریختی یکسر شکستن هم ز یزدان دان</p></div>
<div class="m2"><p>که لختی بر خم زلف و کله زد، کج‌کلاهان را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بنازم خوبی خونگرم محبوبی که در مستی</p></div>
<div class="m2"><p>کند ریش از مکیدن‌ها زبان عذرخواهان را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به می آسایش جان‌ها بدان ماند که ناگاهان</p></div>
<div class="m2"><p>گذر بر چشم افتد، تشنه‌لب گم‌کرده‌راهان را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز جورش داوری بردم به دیوان، لیک زین غافل</p></div>
<div class="m2"><p>که سعی رشکم از خاطر برد نامش گواهان را</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گسست تار و پود پرده ناموس را نازم</p></div>
<div class="m2"><p>که دام رغبت نظاره شد رسوانگاهان را</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نشاط هستی حق دارد از مرگ ایمنم غالب</p></div>
<div class="m2"><p>چراغم چون گل آشامد نسیم صبحگاهان را</p></div></div>