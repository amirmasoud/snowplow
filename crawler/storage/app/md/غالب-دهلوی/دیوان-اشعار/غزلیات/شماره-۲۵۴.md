---
title: >-
    شمارهٔ ۲۵۴
---
# شمارهٔ ۲۵۴

<div class="b" id="bn1"><div class="m1"><p>در وصل دل آزاری اغیار ندانم</p></div>
<div class="m2"><p>دانند که من دیده ز دیدار ندانم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>طعنم نسزد مرگ ز هجران نشناسم</p></div>
<div class="m2"><p>رشکم نگزد خویشتن از یار ندانم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پرسد سبب بیخودی از مهر و من از بیم</p></div>
<div class="m2"><p>در عذر به خون غلتم و گفتار ندانم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بوسم به خیالش لب و چون تازه کند جور</p></div>
<div class="m2"><p>از سادگیش بی سبب آزار ندانم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر خون که فشاند مژه در دل فتدم باز</p></div>
<div class="m2"><p>خود را به غم دوست زیانکار ندانم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آویزش جعد از ته چادر بردم دل</p></div>
<div class="m2"><p>آشفتگی طره به دستار ندانم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بوی جگرم می دهد از خون سر هر خار</p></div>
<div class="m2"><p>شد پای که در راه وی افگار ندانم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زخم جگرم بخیه و مرهم نپسندم</p></div>
<div class="m2"><p>موج گهرم جنبش و رفتار ندانم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نقد خردم سکه سلطان نپذیرم</p></div>
<div class="m2"><p>جنس هنرم گرمی بازار ندانم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>غالب نبود کوتهی از دوست همانا</p></div>
<div class="m2"><p>زان سان دهدم کام که بسیار ندانم</p></div></div>