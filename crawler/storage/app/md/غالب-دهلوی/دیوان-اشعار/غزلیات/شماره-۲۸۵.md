---
title: >-
    شمارهٔ ۲۸۵
---
# شمارهٔ ۲۸۵

<div class="b" id="bn1"><div class="m1"><p>چه غم ار به جد گرفتی ز من احتراز کردن؟</p></div>
<div class="m2"><p>نتوان گرفت از من به گذشته ناز کردن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نگهت به موشکافی ز فریب رم نخوردن</p></div>
<div class="m2"><p>نفسم به دام بافی ز سخن دراز کردن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تو و در کنار شوقم گره از جبین گشودن</p></div>
<div class="m2"><p>من و بر رخ دو عالم در دل فراز کردن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مژه را ز خونفشانی به دل ست همزبانی</p></div>
<div class="m2"><p>که شماردم به دامن ستم گداز کردن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به نورد پاس رازت خجل از غبار خویشم</p></div>
<div class="m2"><p>که ز پرده ریخت بیرون غم ناله ساز کردن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز غم تو باد شرمم که چه مایه شوخ چشمی ست</p></div>
<div class="m2"><p>ز شکست رنگ بر رخ در خلد باز کردن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نفسم گداخت شوقت ستم ست گر تو دانی</p></div>
<div class="m2"><p>که ز تاب ناله خون شد نه ز پاس راز کردن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به فشار رشک بزمت نه چنان گداخت گلشن</p></div>
<div class="m2"><p>که میانه گل و مل رسد امتیاز کردن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>رخ گل ز غازه کاری به نگاه بندد آیین</p></div>
<div class="m2"><p>نرسد به خس شکایت ز چمن طراز کردن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>همه تن ز شوق چشمم که چو دل فشانده گردد</p></div>
<div class="m2"><p>به سرشک مایه بخشم ز جگر گداز کردن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هله تازه گشته غالب روش نظیری از تو</p></div>
<div class="m2"><p>سزد این چنین غزل را به سفینه ناز کردن</p></div></div>