---
title: >-
    شمارهٔ ۳۳۳
---
# شمارهٔ ۳۳۳

<div class="b" id="bn1"><div class="m1"><p>زاهد که و مسجد چه و محراب کجایی؟</p></div>
<div class="m2"><p>عیدست و دم صبح می ناب کجایی؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دریا ز حباب آبله پای طلب تست</p></div>
<div class="m2"><p>نور نظر ای گوهر نایاب کجایی؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بوی گل و شبنم نسزد کلبه ما را</p></div>
<div class="m2"><p>صرصر تو کجا رفتی و سیلاب کجایی؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حشرست و خدا داور و هنگامه به پایان</p></div>
<div class="m2"><p>ای شکوه بی مهری احباب کجایی؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آن شور که گرداب جگر داشت ندارد</p></div>
<div class="m2"><p>ای لخت دل غرقه به خوناب کجایی؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>با گرمی هنگامه خواهش نشکیبم</p></div>
<div class="m2"><p>آتش به شبستان زدم ای آب کجایی؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون نیست نمکسایی اشکم به فغانم</p></div>
<div class="m2"><p>کای روشنی دیده بی خواب کجایی؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>غواصی اجزای نفس دیر ندارد</p></div>
<div class="m2"><p>از دل ندمی داغ جگرتاب کجایی؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شوری ست نواریزی تار نفسم را</p></div>
<div class="m2"><p>پیدا نه ای ای جنبش مضراب کجایی؟</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بنمای به گوساله پرستان ید بیضا</p></div>
<div class="m2"><p>غالب به سخن صاحب فرتاب کجایی؟</p></div></div>