---
title: >-
    شمارهٔ ۱۴۹
---
# شمارهٔ ۱۴۹

<div class="b" id="bn1"><div class="m1"><p>ترا گویند عاشق دشمنی آری چنین باشد</p></div>
<div class="m2"><p>ز رشک غیر باید مرد گر مهر تو کین باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از آن سرمایه خوبی به وصلم کام دل جستن</p></div>
<div class="m2"><p>بدان ماند که موری خرمنی را در کمین باشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>محبت هر چه با آن تیشه زن کرد از ستم نبود</p></div>
<div class="m2"><p>چنین افتد چو عاشق سخت و شاهد نازنین باشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به روزی کش شبی با مدعی باید به سر بردن</p></div>
<div class="m2"><p>به من ضایع کند گر صد نگاه خشمگین باشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نسوزد بر خودم دل گر بسوزد برق خرمن را</p></div>
<div class="m2"><p>که دانم آنچه از من رفت حق خوشه چین باشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به پیر خانقه در روضه یکجا خوش توان بودن</p></div>
<div class="m2"><p>به شرط آن که از ما باده وز شیخ انگبین باشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جفاهای ترا آخر وفایی هست پندارم</p></div>
<div class="m2"><p>درین میخانه صاف می به جام واپسین باشد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بری از شحنه دل تا خون بریزی بی گناهی را</p></div>
<div class="m2"><p>نترسی از خدا آیین بی باکی نه این باشد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چه رفت از زهره با هاروت خاکم در دهن بادا</p></div>
<div class="m2"><p>تو مریم باشی و کار تو با روح الامین باشد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از آن گردی که در راهش نشیند بر رخم غالب</p></div>
<div class="m2"><p>چه خیزد چون هم از من رخ هم از من آستین باشد</p></div></div>