---
title: >-
    شمارهٔ ۲۲۶
---
# شمارهٔ ۲۲۶

<div class="b" id="bn1"><div class="m1"><p>تا تف شوق تو انداخته جان در تن شمع</p></div>
<div class="m2"><p>شرر از رشته خویش ست به پیراهن شمع</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جان به ناموس دهی چند فراهم شده اند</p></div>
<div class="m2"><p>ور نه خود با تو چه بوده ست رگ گردن شمع؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مجمعی از دل و جانست به گرد در دوست</p></div>
<div class="m2"><p>توده ای از پر و بال ست به پیرامن شمع</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>روزم از تیرگی آن وسوسه ریزد به نظر</p></div>
<div class="m2"><p>که شب تار به هنگام فرو مردن شمع</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بی تو از خویش چه گویم که به بزم طربم</p></div>
<div class="m2"><p>پرده گوش گل افگار شد از شیون شمع</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نازم آن حسن که در جلوه ز شهرت باشد</p></div>
<div class="m2"><p>خاطرآشوب گل و قاعده برهمزن شمع</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>برنتابد ز بتان جلوه گرفتار کسی</p></div>
<div class="m2"><p>صبح را کرده هواداری گل دشمن شمع</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>می گدازم نفسی بی شرر و شعله و دود</p></div>
<div class="m2"><p>داغ آن سوز نهانم که نباشد فن شمع</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>وقت آرایش ایوان بهارست که باز</p></div>
<div class="m2"><p>کوه از جوش گل و لاله بود معدن شمع</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>غالب از هستی خویش ست عذابی که مراست</p></div>
<div class="m2"><p>هم ز خود خار غم آویخته در دامن شمع</p></div></div>