---
title: >-
    شمارهٔ ۲۹۸
---
# شمارهٔ ۲۹۸

<div class="b" id="bn1"><div class="m1"><p>گویی به من کسی که ز دشمن رسیده کو؟</p></div>
<div class="m2"><p>آن پیر زال سست پی قد خمیده کو؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یادت نکرده خصم به عنوان به لفظ دوست</p></div>
<div class="m2"><p>آن نامه نخوانده ز صد جا دریده کو؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رعنا دلت به دختر همسایه بند نیست</p></div>
<div class="m2"><p>آن مه رخ به گوشه ایوان خزیده کو؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دوشینه گل به بستر و بالین نداشتی</p></div>
<div class="m2"><p>آن برگ گل که در تن نازک خلیده کو؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کس داوری نبرده ز جورت به دادگاه</p></div>
<div class="m2"><p>آن بی گنه که شاه زبانش بریده کو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گویی به شحنه گوی که کس را نکشته ایم</p></div>
<div class="m2"><p>آن نعش نیم سوخته ز آتش کشیده کو؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گویی خمش شوی چو ز کویم بدر روی</p></div>
<div class="m2"><p>آن دل که جز به ناله به هیچ آرمیده کو؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گویی دمی ز گریه خونین به ما برآر</p></div>
<div class="m2"><p>آن مایه خون که سر دهم از دل به دیده کو؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بشنو که غالب از تو رمیده و به کعبه رفت</p></div>
<div class="m2"><p>گفتی شگفتیی که بود ناشنیده کو؟</p></div></div>