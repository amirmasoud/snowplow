---
title: >-
    شمارهٔ ۱۱۵
---
# شمارهٔ ۱۱۵

<div class="b" id="bn1"><div class="m1"><p>لبم از زمزمه یاد تو خاموش مباد</p></div>
<div class="m2"><p>غیر تمثال تو نقش ورق هوش مباد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نگهی کش به هزار آب نشویند ز اشک</p></div>
<div class="m2"><p>محرم جلوه آن صبح بناگوش مباد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هوس چادر گل گر ته خاکم باشد</p></div>
<div class="m2"><p>خاکم از نقش کف پای تو گلپوش مباد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>وعده گردیده وفا طره پریشانی را</p></div>
<div class="m2"><p>یارب امشب به درازی خجل از دوش مباد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>غیر اگر دیده به دیدار تو محرم دارد</p></div>
<div class="m2"><p>فارغ از انده محرومی آغوش مباد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گهری کش نظر از همت پاکان نبود</p></div>
<div class="m2"><p>صرف پیرایه آن گردن و آن گوش مباد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر که را رخت نمازی نبود از نم می</p></div>
<div class="m2"><p>جای در حلقه رندان قدح نوش مباد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>رهرو بادیه شوق سبک سیرانند</p></div>
<div class="m2"><p>بار سر نیز درین مرحله بر دوش مباد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مفتیان باده عزیزست مریزید به خاک</p></div>
<div class="m2"><p>جوشد از پرده دگر خون سیاووش مباد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>همه گر میوه فردوس به خوانت باشد</p></div>
<div class="m2"><p>غالب آن انبه بنگاله فراموش مباد</p></div></div>