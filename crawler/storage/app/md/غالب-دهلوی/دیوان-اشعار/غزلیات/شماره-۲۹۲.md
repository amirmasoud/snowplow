---
title: >-
    شمارهٔ ۲۹۲
---
# شمارهٔ ۲۹۲

<div class="b" id="bn1"><div class="m1"><p>رشک سخنم چیست؟ نه شهد هوس ست این</p></div>
<div class="m2"><p>تلخابه سر جوش گداز نفس ست این</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای ناله جگر در شکن دام میفشان</p></div>
<div class="m2"><p>سرمایه آرایش چاک قفس ست این</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مستم، به کنارم خز و تن زن که درین وقت</p></div>
<div class="m2"><p>هرگز نشناسم که چه بود و چه کس ست این</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>واعظ سخن از توبه مگو، این که پس از می</p></div>
<div class="m2"><p>دست و دهنی آب کشیدیم بس ست این</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تقوی اثری چند به عمر دگرستش</p></div>
<div class="m2"><p>نازم می بی غش چه بلا زودرس ست این</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>با غیر نشایی و به ما نیز نیرزی</p></div>
<div class="m2"><p>لیک آن گل و خار آمد و نسرین و خس ست این</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>لب بر لب دلبر نهم و جان بسپارم</p></div>
<div class="m2"><p>ترکیب یکی کردن صد ملتمس ست این</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شوری ست ز خواباندن جمازه به منزل</p></div>
<div class="m2"><p>اما نه به دمسازی بانگ جرس ست این</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>داغ دل غالب به دوا چاره پذیرست</p></div>
<div class="m2"><p>این را چه کنم چاره که مشکین نفس ست این</p></div></div>