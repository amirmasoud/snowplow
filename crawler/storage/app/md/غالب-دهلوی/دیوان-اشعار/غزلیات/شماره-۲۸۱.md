---
title: >-
    شمارهٔ ۲۸۱
---
# شمارهٔ ۲۸۱

<div class="b" id="bn1"><div class="m1"><p>چیست به لب خنده از عتاب شکستن</p></div>
<div class="m2"><p>رونق پروین ز آفتاب شکستن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر نه ورق راست ز انتخاب شکستن</p></div>
<div class="m2"><p>چیست به رخ طرف آن نقاب شکستن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>غازه بر آن روی تابناک فزودن</p></div>
<div class="m2"><p>رونق بازار آفتاب شکستن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شانه بر آن طره سیاه کشیدن</p></div>
<div class="m2"><p>قیمت کالای مشک ناب شکستن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جوشش سرمستیم ز برق پسندد</p></div>
<div class="m2"><p>نیشتر اندر رگ سحاب شکستن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نیک بود گر به حکم حوصله باشد</p></div>
<div class="m2"><p>جام به پای خم شراب شکستن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شغل ندارد فراق ساقی و مطرب</p></div>
<div class="m2"><p>جز قدح و بربط و رباب شکستن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>قحط می ست امشب از کجا که نخواهم</p></div>
<div class="m2"><p>شیشه خالی به رختخواب شکستن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تیغ تو نازد به سرفشانی عاشق</p></div>
<div class="m2"><p>موج همی بالد از حباب شکستن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چیست دم وصل جان ز ذوق سپردن؟</p></div>
<div class="m2"><p>تشنه لبی را سبو در آب شکستن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از گل روی تو باغ باغ شکفتن</p></div>
<div class="m2"><p>وز خم موی تو فتحیاب شکستن</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>طره میارا به رغم خواهش غالب</p></div>
<div class="m2"><p>چیست دلش را ز پیچ و تاب شکستن</p></div></div>