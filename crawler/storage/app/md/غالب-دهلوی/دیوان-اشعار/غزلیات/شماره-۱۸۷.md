---
title: >-
    شمارهٔ ۱۸۷
---
# شمارهٔ ۱۸۷

<div class="b" id="bn1"><div class="m1"><p>نه از شرمست کز چشم وی آسان برنمی‌آید</p></div>
<div class="m2"><p>نگاهش با درازی‌های مژگان برنمی‌آید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ازین شرمندگی کز بند سامان برنمی‌آید</p></div>
<div class="m2"><p>سر شوریده ما از گریبان برنمی‌آید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر از سروایی ناز تو پروا نیست عاشق را</p></div>
<div class="m2"><p>چرا دل خون نمی‌گردد چرا جان برنمی‌آید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به بزم سوختن دود از چراغان برنمی‌خیزد</p></div>
<div class="m2"><p>به باغ خون شدن بو از گلستان برنمی‌آید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سرت گردم بزن تیغ و دری بر روی دل بگشا</p></div>
<div class="m2"><p>دلم تنگست کار از زخم پیکان برنمی‌آید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شکفتن عرض بی‌تابی‌ست هان ای غنچه می‌دانم</p></div>
<div class="m2"><p>دلت با ناله مرغ سحرخوان برنمی‌آید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>همان خون کردن و از دیده بیرون ریختن دارد</p></div>
<div class="m2"><p>دلی کز عهده غم‌های پنهان برنمی‌آید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مگر آتش نفس دیوانه‌ای مرد از اسیرانت</p></div>
<div class="m2"><p>که دود از روزن دیوار زندان برنمی‌آید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چه گیرایی‌ست کاین تار ز مو باریک‌تر دارد</p></div>
<div class="m2"><p>کسی از دام این نازک‌میانان برنمی‌آید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مجو آسودگی گر مرد راهی کاندرین وادی</p></div>
<div class="m2"><p>چو خار از پا برآمد پا ز دامان برنمی‌آید</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>برم پیش که یارب شکوه اندوه دلتنگی؟</p></div>
<div class="m2"><p>نفس چندان که می‌نالم پریشان برنمی‌آید</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به دوش خلق نعشم عبرت صاحبدلان باشد</p></div>
<div class="m2"><p>به پای خود کسی از کوی جانان برنمی‌آید</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>برآر از بزم بحث ای جذبه توحید غالب را</p></div>
<div class="m2"><p>که ترک ساده ما با فقیهان برنمی‌آید</p></div></div>