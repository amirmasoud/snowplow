---
title: >-
    شمارهٔ ۷۸
---
# شمارهٔ ۷۸

<div class="b" id="bn1"><div class="m1"><p>به وادیی که در آن خضر را عصا خفته ست</p></div>
<div class="m2"><p>به سینه می سپرم ره اگر چه پا خفته ست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بدین نیاز که با تست ناز می رسدم</p></div>
<div class="m2"><p>گدا به سایه دیوار پادشا خفته ست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به صبح حشر چنین خسته رو سیه خیزد</p></div>
<div class="m2"><p>که در شکایت درد و غم دوا خفته ست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خروش حلقه رندان ز نازنین پسری ست</p></div>
<div class="m2"><p>که سر به زانوی زاهد به بوریا خفته ست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هوا مخالف و شب تار و بحر طوفان خیز</p></div>
<div class="m2"><p>گسسته لنگر کشتی و ناخدا خفته ست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>غمت به شهر شبیخون زنان به بنگه خلق</p></div>
<div class="m2"><p>عسس به خانه و شه در حرمسرا خفته ست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دلم به سبحه و سجاده و ردا لرزد</p></div>
<div class="m2"><p>که دزد مرحله بیدار و پارسا خفته ست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>درازی شب و بیداری من این همه نیست</p></div>
<div class="m2"><p>ز بخت من خبر آرید تا کجا خفته ست؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ببین ز دور و مجو قرب شه که منظر را</p></div>
<div class="m2"><p>دریچه باز و به دروازه اژدها خفته ست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به راه خفتن من هر که بنگرد داند</p></div>
<div class="m2"><p>که میر قافله در کاروانسرا خفته ست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دگر ز ایمنی راه و قرب کعبه چه حظ</p></div>
<div class="m2"><p>مرا که ناقه ز رفتار ماند و پا خفته ست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بخواب چون خودم آسوده دل مدان غالب</p></div>
<div class="m2"><p>که خسته غرقه به خون خفته است تا خفته ست</p></div></div>