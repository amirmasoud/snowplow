---
title: >-
    شمارهٔ ۲۲۰
---
# شمارهٔ ۲۲۰

<div class="b" id="bn1"><div class="m1"><p>دل در غمش بسوز که جان می‌دهد عوض</p></div>
<div class="m2"><p>ور جان دهی غمی به از آن می‌دهد عوض</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فارغ مشو ز دوست به می در ریاض خلد</p></div>
<div class="m2"><p>از ما گرفت آنچه همان می‌دهد عوض</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>داغم از آن حریف که چون خانمان بسوخت</p></div>
<div class="m2"><p>چشمی به سوی در نگران می‌دهد عوض</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سرمایه خرد به جنون ده که این کریم</p></div>
<div class="m2"><p>یک سود را هزار یان می‌دهد عوض</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نبود سخن‌سرایی ما رایگان که دوست</p></div>
<div class="m2"><p>دل می‌برد ز ما و زبان می‌دهد عوض</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از هرچه نقش وهم و گمان است در گذر</p></div>
<div class="m2"><p>کو خود برون ز وهم و گمان می‌دهد عوض</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آن را که نیستی نظر از ماه و مشتری</p></div>
<div class="m2"><p>چشم سهیل و زهره‌فشان می‌دهد عوض</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نازم به دست سبحه‌شماری که عاقبت</p></div>
<div class="m2"><p>شوقش کف پیاله‌ستان می‌دهد عوض</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آه از غمش که چون ز دل آرام می‌برد</p></div>
<div class="m2"><p>ناسازیی ز هم‌نفسان می‌دهد عوض</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>پاداش هر وفا به جفای دگر کند</p></div>
<div class="m2"><p>غالب ببین که دوست چه سان می‌دهد عوض</p></div></div>