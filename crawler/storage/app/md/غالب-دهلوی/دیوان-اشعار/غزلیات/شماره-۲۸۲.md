---
title: >-
    شمارهٔ ۲۸۲
---
# شمارهٔ ۲۸۲

<div class="b" id="bn1"><div class="m1"><p>خوش بود فارغ ز بند کفر و ایمان زیستن</p></div>
<div class="m2"><p>حیف کافر مردن و آوخ مسلمان زیستن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شیوه رندان بی پروا خرام از من مپرس</p></div>
<div class="m2"><p>اینقدر دانم که دشوارست آسان زیستن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>برد گوی خرمی از هر دو عالم هر که یافت</p></div>
<div class="m2"><p>در بیابان مردن و در قصر و ایوان زیستن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>راحت جاوید ترک اختلاط مردم ست</p></div>
<div class="m2"><p>چون خضر باید ز چشم خلق پنهان زیستن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا چه راز اندر ته این پرده پنهان کرده اند</p></div>
<div class="m2"><p>مرگ مکتوبی بود کو راست عنوان زیستن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>روز وصل یار جان ده ور نه عمری بعد ازین</p></div>
<div class="m2"><p>همچو ما از زیستن خواهی پشیمان زیستن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>با رقیبان همفنیم اما به دعویگاه شوق</p></div>
<div class="m2"><p>مردنست از ما و زین مشتی گرانجان زیستن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بر نوید مقدمت صد بار جان باید فشاند</p></div>
<div class="m2"><p>بر امید وعده ات زنهار نتوان زیستن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دیده گر روشن سواد ظلمت و نورست چیست</p></div>
<div class="m2"><p>فارغ از اهریمن و غافل ز یزدان زیستن؟</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ابتذالی دارد این مضمون توارد عیب نیست</p></div>
<div class="m2"><p>نگذرد در خاطر نازک خیالان زیستن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>غالب از هندوستان بگریز فرصت مفت تست</p></div>
<div class="m2"><p>در نجف مردن خوش است و در صفاهان زیستن</p></div></div>