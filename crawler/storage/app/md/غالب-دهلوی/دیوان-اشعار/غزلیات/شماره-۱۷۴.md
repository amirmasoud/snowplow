---
title: >-
    شمارهٔ ۱۷۴
---
# شمارهٔ ۱۷۴

<div class="b" id="bn1"><div class="m1"><p>دانست کز شهادتم امید حور بود</p></div>
<div class="m2"><p>برگشتنم ز دین دم بسمل ضرور بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رفت آن که ما ز حسن مدارا طمع کنیم</p></div>
<div class="m2"><p>سررشته در کف «ارنی گوی » طور بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>محرم مسنج رند «انا الحق » سرای را</p></div>
<div class="m2"><p>معشوقه خودنمای و نگهبان غیور بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سالک نگفته ایم که منزل شناس نیست</p></div>
<div class="m2"><p>بی جاده ماند راه از آن رو که دور بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نازم به امتیاز که بگذشتن از گناه</p></div>
<div class="m2"><p>با دیگران ز عفو و به ما از غرور بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای آن که از غرور به هیچم نمی خری</p></div>
<div class="m2"><p>زان پایه بازگوی که پیش از ظهور بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>درد دلم به حشر ز شدت نهفته ماند</p></div>
<div class="m2"><p>خون باد ناله ای که هم آهنگ صور بود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دل از تو بود و تو پی الزام ما ز ما</p></div>
<div class="m2"><p>بردی نخست آنچه ز جنس شعور بود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>قطع پیام کردی و دانستم آشتی ست</p></div>
<div class="m2"><p>دلاله خوبروی و دلم ناصبور بود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دادی صلای جلوه و غالب کناره کرد</p></div>
<div class="m2"><p>کو بخش آن گدا که ز غوغا نفور بود</p></div></div>