---
title: >-
    شمارهٔ ۸۰
---
# شمارهٔ ۸۰

<div class="b" id="bn1"><div class="m1"><p>ایمنیم از مرگ تا تیغت جراحت بار هست</p></div>
<div class="m2"><p>روزی ناخورده ما در جهان بسیار هست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ما و خاک رهگذر بر فرق عریان ریختن</p></div>
<div class="m2"><p>گل کسی جوید که او را گوشه دستار هست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پاره ای امیدوارستم، تکلف بر طرف</p></div>
<div class="m2"><p>با همه بی التفاتی دردمند آزار هست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر سر کوی تو با مهرم به جنگ آرد همی</p></div>
<div class="m2"><p>این هجوم ذره کاندر روزن دیوار هست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در خموشی تابش روی عرقناکش نگر</p></div>
<div class="m2"><p>تا چه ها هنگامه سرگرمی گفتار هست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بینوایی بین که گر در کلبه ام باشد چراغ</p></div>
<div class="m2"><p>بخت را نازم که با من دولت بیدار هست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در پرستش سستم و در کامجویی استوار</p></div>
<div class="m2"><p>پادشه را بنده کم خدمت پرخوار هست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>راز دیدن‌ها مجوی و از شنیدن‌ها مگوی</p></div>
<div class="m2"><p>نقش ها در خامه و آهنگ ها در تار هست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر نموداری ست نقش سجده بر سیما دریغ</p></div>
<div class="m2"><p>ور نشانمندی ست دوش خسته زنار هست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دور باش از ریزه های استخوانم ای هما</p></div>
<div class="m2"><p>کاین بساط دعوت مرغان آتشخوار هست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کهنه نخل تازه از صرصر ز پا افتاده ام</p></div>
<div class="m2"><p>خاکم ار کاوی هنوزم ریشه در گلزار هست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>باد برد آن گنج باد آورد و غالب را هنوز</p></div>
<div class="m2"><p>ناله الماس پاش و چشم گوهربار هست</p></div></div>