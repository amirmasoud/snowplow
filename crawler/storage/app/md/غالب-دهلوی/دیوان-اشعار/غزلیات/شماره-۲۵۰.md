---
title: >-
    شمارهٔ ۲۵۰
---
# شمارهٔ ۲۵۰

<div class="b" id="bn1"><div class="m1"><p>آسمان بلند را میرم</p></div>
<div class="m2"><p>ابر کحلی پرند را میرم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می فریبد مرا به بازیچه</p></div>
<div class="m2"><p>دل زار و نژند را میرم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شوری اشک در نظر خوارست</p></div>
<div class="m2"><p>تلخی زهرخند را میرم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شحنه مدح حضرت اعلی است</p></div>
<div class="m2"><p>سخن دلپسند را میرم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سر راهش نشستنم هوس ست</p></div>
<div class="m2"><p>خاک پای سمند را میرم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ره نشین ویم زهی توقیری</p></div>
<div class="m2"><p>طالع ارجمند را میرم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جذب الفت به سوی وی کشدم</p></div>
<div class="m2"><p>این نوآیین کمند را میرم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>می کند رخنه در جگر غم هجر</p></div>
<div class="m2"><p>این جگر در کلند را میرم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شاعرم منشیم ظریف و شریف</p></div>
<div class="m2"><p>این اضافات چند را میرم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>وایه جوید ز حضرت اعلی</p></div>
<div class="m2"><p>غالب مستمند را میرم</p></div></div>