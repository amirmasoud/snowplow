---
title: >-
    شمارهٔ ۲۹۷
---
# شمارهٔ ۲۹۷

<div class="b" id="bn1"><div class="m1"><p>گستاخ گشته ایم غرور جمال کو؟</p></div>
<div class="m2"><p>پیچیده ایم سر ز وفا گوشمال کو؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا کی فریب حلم خدا را خدا نه ای</p></div>
<div class="m2"><p>آن خوی خشمگین و ادای ملال کو؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>برگشته ام ز مهر و نمی گیریم به قهر</p></div>
<div class="m2"><p>دارم دو صد جواب ولی یک سؤال کو؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یا می گسست صحبت و یا می فزود ربط</p></div>
<div class="m2"><p>لیکن مرا ملال و ترا انفعال کو؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خواهی که برفروزی و سوزی درنگ چیست؟</p></div>
<div class="m2"><p>خواهم که تیز سوی تو بینم مجال کو؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر گفته ایم کشتن و بستن به ما مخند</p></div>
<div class="m2"><p>ما را تدارکی به سزا در خیال کو؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>داغم ز رشک شوکت صنعان ولی چه سود</p></div>
<div class="m2"><p>آن دستگاه طاعت هفتاد سال کو؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>من بوسه جوی و تو به سخن داریم نگاه</p></div>
<div class="m2"><p>لب تشنه با گهر چه شکیبد زلال کو؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دل فتنه جوی و فرصت تکمیل عشق نیست</p></div>
<div class="m2"><p>هنگامه سازی هوس زود بال کو؟</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>لب تا جگر ز تشنگیم سوخت در تموز</p></div>
<div class="m2"><p>صاف شراب غوره و جام سفال کو؟</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>در باده طهور غم محتسب کجا؟</p></div>
<div class="m2"><p>در عیش خلد آفت بیم زوال کو؟</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>غالب به شعر کم ز ظهوری نیم ولی</p></div>
<div class="m2"><p>عادل شه سخن رس دریا نوال کو؟</p></div></div>