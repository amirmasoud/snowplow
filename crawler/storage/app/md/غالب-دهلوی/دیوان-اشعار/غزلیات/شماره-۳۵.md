---
title: >-
    شمارهٔ ۳۵
---
# شمارهٔ ۳۵

<div class="b" id="bn1"><div class="m1"><p>راز خویت از بدآموز تو می‌جوییم ما</p></div>
<div class="m2"><p>از تو می‌گوییم گر با غیر می‌گوییم ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حشر مشتاقان همان بر صورت مژگان بود</p></div>
<div class="m2"><p>مر ز خاک خویشتن چون سبزه می‌روییم ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>راز عاشق از شکست رنگ رسوا می‌شود</p></div>
<div class="m2"><p>با وجود سخت‌جانی‌ها تنک‌روییم ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زین بهار آیین نگاهان بو که بپذیرد یکی</p></div>
<div class="m2"><p>عمرها شد، رخ به خون دیده می‌شوییم ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آفتاب عالم سرگشتگی‌های خودیم</p></div>
<div class="m2"><p>می‌رسد بوی تو از هر گل که می‌بوییم ما</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا چه‌ها مجموعه لطف بهاران بوده‌ای</p></div>
<div class="m2"><p>تا به زانو، سوده پای ما و می‌پوییم ما</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زحمت احباب نتوان داد غالب بیش از این</p></div>
<div class="m2"><p>هرچه می‌گوییم بهر خویش می‌گوییم ما</p></div></div>