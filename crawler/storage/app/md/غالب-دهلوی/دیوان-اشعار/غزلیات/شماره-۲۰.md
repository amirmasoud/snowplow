---
title: >-
    شمارهٔ ۲۰
---
# شمارهٔ ۲۰

<div class="b" id="bn1"><div class="m1"><p>به خلوت مژده نزدیکی یارست پهلو را</p></div>
<div class="m2"><p>فریب امتحان پاکبازی داده ام او را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز محو پرده محمل، مگو فرهاد را میرم</p></div>
<div class="m2"><p>که می خاید به ذوق فتنه شادروان مشکو را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جهان از باده و شاهد بدان ماند که پنداری</p></div>
<div class="m2"><p>به دنیا از پس آدم فرستادند مینو را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز من رنجیده با اغیار در نازست و می خواهد</p></div>
<div class="m2"><p>به جنبش های ابرو از گره پردازد ابرو را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به زور تندخویی خستگان را رام خود کردن</p></div>
<div class="m2"><p>به آتش بردن است از موی تاب پیچش مو را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نباشد دیده تا حق بین مده دستوری اشکش</p></div>
<div class="m2"><p>چو گوهرسنج کو پیش از گهر سنجد ترازو را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو بنشیند به محفل بگذرانم در دل تنگش</p></div>
<div class="m2"><p>که رنجد غیر ازو چون بی سبب در هم کشد رو را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اگر داند که در نسبت مرا با کیست همچشمی</p></div>
<div class="m2"><p>کشد در دیده هر گردی که از ره خیزد آهو را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بهاران گو برو مشاطه کوه و بیابان شو</p></div>
<div class="m2"><p>گل از لخت دل عشاق زیبد آن سر کو را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نشان دور است غالب در سخن این شیوه بس نبود</p></div>
<div class="m2"><p>بدین زورین کمان می‌آزمایم دست و بازو را</p></div></div>