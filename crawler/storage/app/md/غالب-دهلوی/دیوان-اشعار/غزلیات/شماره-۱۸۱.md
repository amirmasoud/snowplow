---
title: >-
    شمارهٔ ۱۸۱
---
# شمارهٔ ۱۸۱

<div class="b" id="bn1"><div class="m1"><p>چون گویم از تو بر دل شیدا چه می‌رود؟</p></div>
<div class="m2"><p>بنگر بر آبگینه ز خارا چه می‌رود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خوابیده است تا که به کویت رسیده است</p></div>
<div class="m2"><p>گر سر رود به راه تو از پا چه می‌رود؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گویی، مباد در شکن طره خون شود</p></div>
<div class="m2"><p>دل زان تست از گره ما چه می‌رود؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پیداست بی‌نیازی عشق از فنای ما</p></div>
<div class="m2"><p>گر زورقی شکست ز دریا چه می‌رود؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آیینه خانه‌ای‌ست غبارم ز انتظار</p></div>
<div class="m2"><p>او جانب چمن به تماشا چه می‌رود؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر جلوهٔ رخ تو به ساغر ندیده‌ایم</p></div>
<div class="m2"><p>چندین به ذوق باده دل از جا چه می‌رود؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>با ما که محو لذت بیداد گشته‌ایم</p></div>
<div class="m2"><p>دیگر سخن ز مهر و مدارا چه می‌رود؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>یک ره اگر به وادی مجنون کند گذر</p></div>
<div class="m2"><p>از ساربان ناقه لیلا چه می‌رود؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ای شرم بازداشته از جلوه‌سازیت</p></div>
<div class="m2"><p>از پشت پا بر آینه آیا چه می‌رود؟</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هفت آسمان به گردش و ما در میانه‌ایم</p></div>
<div class="m2"><p>غالب دگر مپرس که بر ما چه می‌رود</p></div></div>