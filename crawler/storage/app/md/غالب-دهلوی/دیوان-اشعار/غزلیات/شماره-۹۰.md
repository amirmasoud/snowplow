---
title: >-
    شمارهٔ ۹۰
---
# شمارهٔ ۹۰

<div class="b" id="bn1"><div class="m1"><p>ببین که در گل و مل جلوه گر برای تو کیست؟</p></div>
<div class="m2"><p>مپوش دیده ز حق طالب رضای تو کیست؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چه ناکسی که ز درد فراق می نالی</p></div>
<div class="m2"><p>نمی رسی که در این پرده همنوای تو کیست؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کلید بستگی تست غم بجوش ای دل</p></div>
<div class="m2"><p>تو گر چنین نگدازی گره گشای تو کیست؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شکایتی نفروشی و عشوه ای نخری</p></div>
<div class="m2"><p>تو آشنای که ای خواجه و آشنای تو کیست؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ترا که موجه گل تا کمر بود دریاب</p></div>
<div class="m2"><p>که غرق خون به در بوستان سرای تو کیست؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بلا به صورت زلف تو رو به ما آورد</p></div>
<div class="m2"><p>به بند خصمی دهریم مبتلای تو کیست؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تراست جلوه فراوان درین بساط ولی</p></div>
<div class="m2"><p>حریف باده میخواره آزمای تو کیست؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز وارثان شهیدان هراس یعنی چه؟</p></div>
<div class="m2"><p>قوی ست دست قضا کشته ادای تو کیست؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به انتظار تو در پاس وقت خویشتنم</p></div>
<div class="m2"><p>فریب خورده نیرنگ وعده های تو کیست؟</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زلال لطف تو سیرابی هوسناکان</p></div>
<div class="m2"><p>یکی ببین که جگرتشنه جفای تو کیست؟</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ترا ز اهل هوس هر یکی به جای منست</p></div>
<div class="m2"><p>تو و خدای تو، شاهم مرا به جای تو کیست؟</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>فرشته، معنی «من ربک » نمی فهمم</p></div>
<div class="m2"><p>به من بگوی که غالب بگو خدای تو کیست؟</p></div></div>