---
title: >-
    شمارهٔ ۷۰
---
# شمارهٔ ۷۰

<div class="b" id="bn1"><div class="m1"><p>سینه بگشودیم و خلقی دید کاینجا آتش است</p></div>
<div class="m2"><p>بعد از این گویند آتش را که گویا آتش است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>انتظار جوله ساقی کبابم می‌کند</p></div>
<div class="m2"><p>می به ساغر آب حیوان و به مینا آتش است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گریه‌ات در عشق از تأثیر دود آه ماست</p></div>
<div class="m2"><p>اشک در چشم تو آب و در دل ما آتش است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای که می‌گویی تجلی‌گاه نازش دور نیست</p></div>
<div class="m2"><p>صبر مشتی از خس و ذوق تماشا آتش است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بی‌تکلف، در بلا بودن به از بیم بلاست</p></div>
<div class="m2"><p>قعر دریا سلسبیل و روی دریا آتش است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پرده از رخ برگرفت و بی‌محابا سوختیم</p></div>
<div class="m2"><p>باده با دست آتش او را و ما را آتش است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هم بدین نسبت ز شوخی در دلت جا کرده‌ایم</p></div>
<div class="m2"><p>فاش گوییم از تو سنگ است آنچه از ما آتش است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گریه‌ای دارم که تا تحت‌الثری آب است و بس</p></div>
<div class="m2"><p>ناله‌ای دارم که تا اوج ثریا آتش است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>پاک خور امروز و زنهار از پی فردا منه</p></div>
<div class="m2"><p>در شریعت باده امروز آب و فردا آتش است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>راز بدخویان نهفتن برنتابد بیش از این</p></div>
<div class="m2"><p>پرده‌دار سوز و ساز ماست هرجا آتش است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گشته‌ام غالب طرف با مشرب عرفی که گفت</p></div>
<div class="m2"><p>«روی دریا سلسبیل و قعر دریا آتش است»</p></div></div>