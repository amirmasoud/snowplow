---
title: >-
    شمارهٔ ۲۶۵
---
# شمارهٔ ۲۶۵

<div class="b" id="bn1"><div class="m1"><p>صبح شد خیز که روداد اثر بنمایم</p></div>
<div class="m2"><p>چهره آغشته به خوناب جگر بنمایم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پنبه یکسو نهم از داغ که رخشد چون روز</p></div>
<div class="m2"><p>آخری نیست شبم را که سحر بنمایم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خویشتن را دگر از گریه نگهداشت به زور</p></div>
<div class="m2"><p>جگر خسته خود آن به که دگر بنمایم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حد من نیست که بنمایمش آری از دور</p></div>
<div class="m2"><p>با من آ تا سر آن راهگذار بنمایم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>می کند ناز گمان کرده که خط دیر دمد</p></div>
<div class="m2"><p>خیز تا شعبده جذب نظر بنمایم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آتش افروخته و خلق به حیرت نگران</p></div>
<div class="m2"><p>رخصتی ده که به هنگامه هنر بنمایم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون به محشر اثر سجده ز سیما جویند</p></div>
<div class="m2"><p>داغ سودای تو ناچار ز سر بنمایم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دلربایانه به زندان همه روزم گذرد</p></div>
<div class="m2"><p>بس که خود را به تو از روزن در بنمایم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بر رقم سنج یسار تو زنم بانگ به حشر</p></div>
<div class="m2"><p>کش رضانامه خونهای هدر بنمایم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>غالب این لعب به گل مهره رضاجویی تست</p></div>
<div class="m2"><p>تو خریدار گهر باش گهر بنمایم</p></div></div>