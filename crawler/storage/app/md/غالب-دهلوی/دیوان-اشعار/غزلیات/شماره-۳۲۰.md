---
title: >-
    شمارهٔ ۳۲۰
---
# شمارهٔ ۳۲۰

<div class="b" id="bn1"><div class="m1"><p>نفس را بر در این خانه صد غوغاست پنداری</p></div>
<div class="m2"><p>دلی دارم که سرکار تمناهاست پنداری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حباب از فرق عشاق ست و موج از تیغ خوبانش</p></div>
<div class="m2"><p>شهادتگاه ارباب وفا دریاست پنداری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به گوشم می رسد از دور آواز درا امشب</p></div>
<div class="m2"><p>دلی گم گشته ای دارم که در صحراست پنداری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ازو باور ندارد دعوی ذوق شهادت را</p></div>
<div class="m2"><p>نگاهش با رقیب و خاطرش با ماست پنداری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در و دیوار را در زر گرفت آه شرربارم</p></div>
<div class="m2"><p>شب آتش نوایان آفتاب انداست پنداری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>فدایش جان که بهر کشتنم تدبیرها دارد</p></div>
<div class="m2"><p>عتاب من به بخت خویشتن بیجاست پنداری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گرستیم آنقدر کز خون بیابان لاله زاری شد</p></div>
<div class="m2"><p>خزان ما بهار دامن صحراست پنداری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جنون الفت همچون خودی دارد تماشا کن</p></div>
<div class="m2"><p>شکست صد دل از رنگ رخش پیداست پنداری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نوید وعده قتلی به گوشم می‌رسد غالب</p></div>
<div class="m2"><p>لب لعلش به کام بیدلان گویاست پنداری</p></div></div>