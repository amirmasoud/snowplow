---
title: >-
    شمارهٔ ۲۶۸
---
# شمارهٔ ۲۶۸

<div class="b" id="bn1"><div class="m1"><p>تا فصلی از حقیقت اشیا نوشته‌ایم</p></div>
<div class="m2"><p>آفاق را مرادف عنقا نوشته‌ایم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ایمان به غیب تفرقه‌ها رفت از ضمیر</p></div>
<div class="m2"><p>ز اسما گذشته‌ایم و مسما نوشته‌ایم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عنوان رازنامه اندوه ساده بود</p></div>
<div class="m2"><p>سطر شکست رنگ به سیما نوشته‌ایم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>قلزم فشانی مژه از پهلوی دل است</p></div>
<div class="m2"><p>این ابر را برات به دریا نوشته‌ایم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خاکی به روی نامه نیفشانده‌ایم ما</p></div>
<div class="m2"><p>رخصت بدان حریف خودآرا نوشته‌ایم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در هیچ نسخه معنی لفظ امید نیست</p></div>
<div class="m2"><p>فرهنگ‌نامه‌های تمنا نوشته‌ایم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آینده و گذشته تمنا و حسرت است</p></div>
<div class="m2"><p>یکی کاشکی بود که به صد جا نوشته‌ایم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دارد رخت به خون تماشا خطی ز حسن</p></div>
<div class="m2"><p>روشن سواد این ورق نانوشته‌ایم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>رنگ شکسته عرض سپاس بلای تست</p></div>
<div class="m2"><p>پنهان سپرده‌ای غم و پیدا نوشته‌ایم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آغشته‌ایم هر سر خاری به خون دل</p></div>
<div class="m2"><p>قانون باغبانی صحرا نوشته‌ایم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کویت ز نقش جبهه ما یک قلم پر است</p></div>
<div class="m2"><p>لختی سپاس همدمی پا نوشته‌ایم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>غالب الف همان علم وحدت خودست</p></div>
<div class="m2"><p>بر «لا» چه بر فروزد گر «الا» نوشته‌ایم؟</p></div></div>