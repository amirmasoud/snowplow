---
title: >-
    شمارهٔ ۴۱
---
# شمارهٔ ۴۱

<div class="b" id="bn1"><div class="m1"><p>ای خداوند خردمند و جهان داور دانا</p></div>
<div class="m2"><p>وی به نیروی خرد بر همه کردار توانا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای به رفتار و به دیدار ز زیبائی و خوبی</p></div>
<div class="m2"><p>سرو نوخاسته آسا مه ناکاسته مانا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به ادا پایه فزایا به نظر عقده گشایا</p></div>
<div class="m2"><p>به کرم ابر عطایا به غضب برق سنانا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به نگه خسته نوازا به سخن بذله طرازا</p></div>
<div class="m2"><p>به قلم غالیه سایا به نفس عطرفشانا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شه نشان کلب علیخان که تویی یوسف ثانی</p></div>
<div class="m2"><p>نبود ثانی و همتای تو در دهر همانا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دانم از حال و مآلم خبری داشته باشی</p></div>
<div class="m2"><p>سرنوشت ازلی گر چه ندارد خط خوانا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دشمنم چرخ تو بینی و نسوزی به عتابش</p></div>
<div class="m2"><p>به عدو صاعقه ریزا به محب فیض رسانا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جانشین تو کند نام ترا زنده به گیتی</p></div>
<div class="m2"><p>باد فردوس برین جای تو فردوس مکانا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>غالب از غم چه خروشی به تو زیباست خموشی</p></div>
<div class="m2"><p>با کریم همه دان هیچ مگو هیچ مدانا</p></div></div>