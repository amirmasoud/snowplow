---
title: >-
    شمارهٔ ۷۵
---
# شمارهٔ ۷۵

<div class="b" id="bn1"><div class="m1"><p>گرد ره خویش از نفسم باز ندانست</p></div>
<div class="m2"><p>ننگش ز خرام آمد و پرواز ندانست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز انسان غم ما خورد که رسوایی ما را</p></div>
<div class="m2"><p>خصم از اثر غمزه غماز ندانست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فریاد که تا این همه خون خوردنم از غم</p></div>
<div class="m2"><p>یک ره به دلش کرد گذر راز ندانست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نازم نگه شرم که دلها ز میان برد</p></div>
<div class="m2"><p>زانسان که خود آن چشم فسونساز ندانست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یک چند به هم ساخته ناکام گذشتیم</p></div>
<div class="m2"><p>من عشوه نپذرفتم و او ناز ندانست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از شاخ گل افشاند و ز خارا گهر انگیخت</p></div>
<div class="m2"><p>آیینه ما در خور پرداز ندانست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گریم که برد موجه خون خوابگهش را</p></div>
<div class="m2"><p>در ناله مرا دوست ز آواز ندانست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>همدم که ز اقبال نوید اثرم داد</p></div>
<div class="m2"><p>اندوه نگاه غلط انداز ندانست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مخمور مکافات به خلد و سقر آویخت</p></div>
<div class="m2"><p>مشتاق عطا شعله ز گل باز ندانست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>غالب سخن از هند برون بر که کس اینجا</p></div>
<div class="m2"><p>سنگ از گهر و شعبده ز اعجاز ندانست</p></div></div>