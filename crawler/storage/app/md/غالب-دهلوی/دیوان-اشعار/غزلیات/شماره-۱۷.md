---
title: >-
    شمارهٔ ۱۷
---
# شمارهٔ ۱۷

<div class="b" id="bn1"><div class="m1"><p>غمت در بوته دانش گدازد مغز خامان را</p></div>
<div class="m2"><p>لبت تنگ شکر سازد دهان تلخ کامان را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قضا در کارها اندازه هر کس نگه دارد</p></div>
<div class="m2"><p>به قطع وادی غم می گمارد تیزگامان را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز هستی پاک شو گر مرد راهی کاندرین وادی</p></div>
<div class="m2"><p>گرانیهاست رخت رهرو آلوده دامان را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دماغ فتنه می نازد به سامان رسیدن ها</p></div>
<div class="m2"><p>طلوع نشئه گرد راه باشد خوش خرامان را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پی رسوایی ارباب تقوی جلوه ای سر کن</p></div>
<div class="m2"><p>کتانها ماهتابی ساز شاهم نیک نامان را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به عرض ناز خوبان را ز ما بی تاب تر دارد</p></div>
<div class="m2"><p>عنان از برق باشد در رهش زرین ستامان را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خرابیم و رضایش در خرابی های ما باشد</p></div>
<div class="m2"><p>ز چشم بد نگه دارد خدا ما دوستکامان را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بسا افتاده سرمست و بسا افتاده در طاعت</p></div>
<div class="m2"><p>تو دانی تا به لطف از خاک برداری گدایان را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز قاتل مژده زخمی گلم در جیب جان ریزد</p></div>
<div class="m2"><p>نشاط انگیز باشد بوی خون خونین مشامان را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>جهان را خاصی و عامی ست آن مغرور و این عاجز</p></div>
<div class="m2"><p>بیا غالب ز خاصان بگذر و بگذار عامان را</p></div></div>