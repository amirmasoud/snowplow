---
title: >-
    شمارهٔ ۱۶۴
---
# شمارهٔ ۱۶۴

<div class="b" id="bn1"><div class="m1"><p>مژده صبح درین تیره شبانم دادند</p></div>
<div class="m2"><p>شمع کشتند و ز خورشید نشانم دادند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رخ گشودند و لب هرزه سرایم بستند</p></div>
<div class="m2"><p>دل ربودند و دو چشم نگرانم دادند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سوخت آتشکده ز آتش نفسم بخشیدند</p></div>
<div class="m2"><p>ریخت بتخانه ز ناقوس فغانم دادند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گهر از رایت شاهان عجم برچیدند</p></div>
<div class="m2"><p>به عوض خامه گنجینه فشانم دادند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>افسر از تارک ترکان پشنگی بردند</p></div>
<div class="m2"><p>به سخن ناصیه فر کیانم دادند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گوهر از تاج گسستند و به دانش بستند</p></div>
<div class="m2"><p>هر چه بردند به پیدا به نهانم دادند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر چه در جزیه ز گبران می ناب آوردند</p></div>
<div class="m2"><p>به شب جمعه ماه رمضانم دادند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هر چه از دستگه پارس به یغما بردند</p></div>
<div class="m2"><p>تا بنالم هم از آن جمله زبانم دادند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دل ز غم مرده و من زنده همانا این مگر</p></div>
<div class="m2"><p>بود ارزنده به ماتم که امانم دادند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هم ز آغاز به خوف و خطرستم غالب</p></div>
<div class="m2"><p>طالع از قوس و شمار از سرطانم دادند</p></div></div>