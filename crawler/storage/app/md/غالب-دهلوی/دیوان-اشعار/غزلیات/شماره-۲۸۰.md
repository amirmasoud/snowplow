---
title: >-
    شمارهٔ ۲۸۰
---
# شمارهٔ ۲۸۰

<div class="b" id="bn1"><div class="m1"><p>حیف ست قتلگه ز گلستان شناختن</p></div>
<div class="m2"><p>شاخ از خدنگ و غنچه ز پیکان شناختن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>لب دوختم ز شکوه ز خود فارغم شمرد</p></div>
<div class="m2"><p>نشناخت قدر پرسش پنهان شناختن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از شیوه های خاطر مشکل پسند کیست</p></div>
<div class="m2"><p>کشتن به جرم درد ز درمان شناختن؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از پیکرت بساط صفای خیال یافت</p></div>
<div class="m2"><p>وصل تو از فراق تو نتوان شناختن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نازم دماغ ناز ندانی ز سادگی ست</p></div>
<div class="m2"><p>کشتن به ظلم و کشته احسان شناختن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یاد آیدم به وصل تو در صحن گلستان</p></div>
<div class="m2"><p>آن جلوه گل آتش سوزان شناختن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خاکی به روی نامه فشاندیم مفت تست</p></div>
<div class="m2"><p>ناخوانده صفحه حال ز عنوان شناختن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ماییم و ذوق سجده چه مسجد چه بتکده</p></div>
<div class="m2"><p>در عشق نیست کفر ز ایمان شناختن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مینا شکسته و می گلفام ریخته</p></div>
<div class="m2"><p>محوم هنوز در گل و ریحان شناختن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>لخت دلم به دامن و چاک غمم به جیب</p></div>
<div class="m2"><p>اینک سزای جیب ز دامان شناختن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بگداخت بس که از اثر تاب روی تو</p></div>
<div class="m2"><p>مهر از شفق به کوی تو نتوان شناختن</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>غالب به قدر حوصله باشد کلام مرد</p></div>
<div class="m2"><p>باید ز حرف نبض حریفان شناختن</p></div></div>