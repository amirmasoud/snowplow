---
title: >-
    شمارهٔ ۲۷۲
---
# شمارهٔ ۲۷۲

<div class="b" id="bn1"><div class="m1"><p>رفت بر ما آنچه خود ما خواستیم</p></div>
<div class="m2"><p>وایه از سلطان به غوغا خواستیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دیگران شستند رخت خویش و ما</p></div>
<div class="m2"><p>تری دامن ز دریا خواستیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دانش و گنجینه پنداری یکی ست</p></div>
<div class="m2"><p>حق نهان داد آنچه پیدا خواستیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون به خواهش کارها کردند راست</p></div>
<div class="m2"><p>خویش را سرمست و رسوا خواستیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>غافل از توفیق طاعت کان عطاست</p></div>
<div class="m2"><p>مزد کار از کارفرما خواستیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر گنهکاریم واعظ گو مرنج</p></div>
<div class="m2"><p>خواجه را در روضه تنها خواستیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سینه چون تنگست پر خون بود دل</p></div>
<div class="m2"><p>دیده خونابه پالا خواستیم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>رفت و باز آمد هما در دام او</p></div>
<div class="m2"><p>باز سر دادیم و عنقا خواستیم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هم به خواهش قطع خواهش خواستند</p></div>
<div class="m2"><p>عذر خواهشهای بیجا خواستیم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>قطع خواهشها ز ما صورت نداشت</p></div>
<div class="m2"><p>همت از غالب همانا خواستیم</p></div></div>