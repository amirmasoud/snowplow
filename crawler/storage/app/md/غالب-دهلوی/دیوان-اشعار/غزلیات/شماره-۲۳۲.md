---
title: >-
    شمارهٔ ۲۳۲
---
# شمارهٔ ۲۳۲

<div class="b" id="bn1"><div class="m1"><p>به گونه می نپذیرد ز همدگر تفریق</p></div>
<div class="m2"><p>تجلی تو به دل همچو می به جام عقیق</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به راه شوق بر آن آب خون همی گریم</p></div>
<div class="m2"><p>که قطره قطره چو ابرم چکیده از ابریق</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به جز دمی نکند خسته ام چو سنگ در آب</p></div>
<div class="m2"><p>هجوم ریزش غمهای سخت و قلب رقیق</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به هیچ پایه نگشت اضطرار ما زایل</p></div>
<div class="m2"><p>بود ستاره عاشق در اوج دست غریق</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بهانه جوست کرم زانکه در گزارش کار</p></div>
<div class="m2"><p>نبوده حسن عمل بی علاقه توفیق</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مرا که ذره لقب داده ای همی رقصم</p></div>
<div class="m2"><p>که نسبتی به زبان تو کرده ام تحقیق</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>حدیث تشنگی لب به پیر ره گفتم</p></div>
<div class="m2"><p>ز پاره جگرم در دهن نهاد عقیق</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به راه کعبه هلاکم نمی کنی باور</p></div>
<div class="m2"><p>تو ای که بیهده باز آمدی ز بیت عتیق</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ندیده ای به بیابان به زیر خاربنی</p></div>
<div class="m2"><p>شکسته مشربه آب و پاره ای ز سویق</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ترا به پهلوی میخانه جا دهم غالب</p></div>
<div class="m2"><p>به شرط آن که قناعت کنی به بوی رحیق</p></div></div>