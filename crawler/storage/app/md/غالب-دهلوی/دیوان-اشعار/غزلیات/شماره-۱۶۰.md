---
title: >-
    شمارهٔ ۱۶۰
---
# شمارهٔ ۱۶۰

<div class="b" id="bn1"><div class="m1"><p>نفس از بیم خویت رشته پیچیده را ماند</p></div>
<div class="m2"><p>نگاه از تاب رویت موی آتش دیده را ماند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز جوش دل هنوزش ریشه در آبست پنداری</p></div>
<div class="m2"><p>به مژگان قطره خون غنچه ناچیده را ماند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز بس کز لاله و گل حسرت ناز تو می جوشد</p></div>
<div class="m2"><p>خیابان محشر دلهای خون گردیده را ماند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خوشا دلداده چشم خودش بودن در آیینه</p></div>
<div class="m2"><p>ز سرگرمی نگه صیاد آهودیده را ماند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>غبار از جاده تا اوج سپهر ساده می بالد</p></div>
<div class="m2"><p>ز جوش وحشتم صحرا دل رنجیده را ماند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به هر جا می خرامی جلوه ات در ماست پنداری</p></div>
<div class="m2"><p>دل از آیینه داریهای شوقت دیده را ماند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چه غم ز افتادگی ها چون روان پالاست اندوهت</p></div>
<div class="m2"><p>تن از مستی به کویت جان آرامیده را ماند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بهار از رنگ و بو در پیشگاه جلوه نازش</p></div>
<div class="m2"><p>گدایان نثار از رهگذر برچیده را ماند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>رقیبش برده از راه و وفا بنگر که در چشمم</p></div>
<div class="m2"><p>غبار راه او مژگان برگردیده را ماند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>جهان دودی ست از سودا که می گرداندش غالب</p></div>
<div class="m2"><p>تو گویی گنبد گردون سر شوریده را ماند</p></div></div>