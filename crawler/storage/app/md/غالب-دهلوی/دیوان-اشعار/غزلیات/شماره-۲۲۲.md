---
title: >-
    شمارهٔ ۲۲۲
---
# شمارهٔ ۲۲۲

<div class="b" id="bn1"><div class="m1"><p>تکیه بر عهد زبان تو غلط بود غلط</p></div>
<div class="m2"><p>کان خود از طرز بیان تو غلط بود غلط</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن که گفت از من دلخسته به پیش تو رقیب</p></div>
<div class="m2"><p>که غلط بود به جان تو غلط بود غلط</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>غنچه را نیک نظر کردم ادایی دارد</p></div>
<div class="m2"><p>وین که ماند به دهان تو غلط بود غلط</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دل نهادن به پیام تو خطا بود خطا</p></div>
<div class="m2"><p>کام جستن ز لبان تو غلط بود غلط</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>این مسلم که لب هیچ مگویی داری</p></div>
<div class="m2"><p>خاطر هیچمدان تو غلط بود غلط</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر جفای تو به پاداش وفایی ست هنوز</p></div>
<div class="m2"><p>دعوی ما به گمان تو غلط بود غلط</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آخر ای بوقلمون جلوه کجایی کاینجا</p></div>
<div class="m2"><p>هر چه دادند نشان تو غلط بود غلط</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شوق می تافت سر رشته وهمی ور نه</p></div>
<div class="m2"><p>هستی ما و میان تو غلط بود غلط</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آن تو باشی که نظیر تو عدم بود عدم</p></div>
<div class="m2"><p>سایه در سرو روان تو غلط بود غلط</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>می‌پسندی که بدین زمزمه میرد غالب</p></div>
<div class="m2"><p>تکیه بر عهد زبان تو غلط بود غلط</p></div></div>