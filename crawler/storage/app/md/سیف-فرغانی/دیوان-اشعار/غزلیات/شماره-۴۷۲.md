---
title: >-
    شمارهٔ ۴۷۲
---
# شمارهٔ ۴۷۲

<div class="b" id="bn1"><div class="m1"><p>ای بگرد خرمن تو خوشه چین خورشید و ماه</p></div>
<div class="m2"><p>ماه با روی تو نبود در محل اشتباه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پادشاه ملک حسنی کس چنین ملکی نداشت</p></div>
<div class="m2"><p>زابتدای دور عالم تا بوقت پادشاه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بی شعاع روی تو با سایه هستی خود</p></div>
<div class="m2"><p>ره نبردم سوی تو چندانکه می کردم نگاه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون رخ اندر آینه پیدا شود پشت زمین</p></div>
<div class="m2"><p>ظلمت شب را اگر بر روی افتد نور ماه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عاشق ار با خلق باشد ماند از معشوق دور</p></div>
<div class="m2"><p>لشکری بر خر نشیند باز ماند از سپاه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همتی باید که عاشق را ز خود بخشد خلاص</p></div>
<div class="m2"><p>رستمی باید که بیژن را برون آرد ز چاه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عاشق اندر پایگاه خدمت سلطان عشق</p></div>
<div class="m2"><p>گر بود ثابت قدم چون تخت یابد پیشگاه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عشق هرجا تخت خود بنهاد و اسبی راند، شد</p></div>
<div class="m2"><p>پای قیصر بی رکاب و فرق کسری بی کلاه</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بی جواز عشق فردا در سیاستگاه حشر</p></div>
<div class="m2"><p>طاعتت محتاج آمرزش بود همچون گناه</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گر بگردانی عنان از جانب این خاکدان</p></div>
<div class="m2"><p>از رکاب خود در آن حضرت فشانی گرد راه</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مرکب تن را جو (و) نان کم کن ای رایض که نیست</p></div>
<div class="m2"><p>حاجتی در مرج ایران رخش رستم را بکاه</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>سیف فرغانی تو در معنی چو صبح کاذبی</p></div>
<div class="m2"><p>ورچه در دعوی بیاری صبح صادق را گواه</p></div></div>