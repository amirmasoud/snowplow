---
title: >-
    شمارهٔ ۵۱۱
---
# شمارهٔ ۵۱۱

<div class="b" id="bn1"><div class="m1"><p>ای بکویت عاشقان رانور رویت رهنمای</p></div>
<div class="m2"><p>همچو شادی دوستان را انده تو دلگشای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خاک درگاه تو چون باد بهاری مشک بوی</p></div>
<div class="m2"><p>آتش عشق تو همچون آب حیوان جان فزای</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شور بختی را که با تلخی اندوهت خوشست</p></div>
<div class="m2"><p>دوستی جان شیرین در دلش نگرفت جای</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اندرین دوران ناقص جزتو از خوبان کراست</p></div>
<div class="m2"><p>معنیی کامل چودین صورت چودنیا دلربای</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گرچه گردون شان نهد در راه تو سربر قدم</p></div>
<div class="m2"><p>بر سر گردون گردان عاشقان بینند پای</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از برای زر گدایی کی کند درویش تو</p></div>
<div class="m2"><p>زآنکه زر نزدیک او بی قدر باشد چون گدای</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای که وصل دوست خواهی دشمن خود گرنه یی</p></div>
<div class="m2"><p>ترک عالم کن مخوه جز دوست چیزی از خدای</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بر سر خار ریاضت مدتی بنشین ببین</p></div>
<div class="m2"><p>روی معنی دار او اندر گل صورت نمای</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نردبان همت اندر زیر پای روح نه</p></div>
<div class="m2"><p>زآنکه دل می گویدت کای جان بعلیین برآی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>طایر میمون نخواهدشد زشؤم بخت خویش</p></div>
<div class="m2"><p>جغد را گر سالها در زیر پر گیرد همای</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سیف فرغانی بخود کس را بر او راه نیست</p></div>
<div class="m2"><p>گر در او می خوهی بیخود بکوی او درآی</p></div></div>