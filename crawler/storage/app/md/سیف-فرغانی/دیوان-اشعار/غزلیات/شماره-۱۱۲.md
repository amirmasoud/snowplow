---
title: >-
    شمارهٔ ۱۱۲
---
# شمارهٔ ۱۱۲

<div class="b" id="bn1"><div class="m1"><p>ماه پیش رخ تو تاب نداشت</p></div>
<div class="m2"><p>تاب روی تو آفتاب نداشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عقل با عشق تو ثبات نکرد</p></div>
<div class="m2"><p>شمع آتش بدید وتاب نداشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عاشق روی همچو خورشیدت</p></div>
<div class="m2"><p>شب چو چشم ستاره خواب نداشت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آنچنان روی چون توان دیدن</p></div>
<div class="m2"><p>که به جز نور خود نقاب نداشت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در جهان هیچ چیز جز عشقت</p></div>
<div class="m2"><p>بهر مستی ما شراب نداشت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دل که در وی نباشد آتش عشق</p></div>
<div class="m2"><p>چشمه زندگیش آب نداشت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بزبان کرم سگم خواندی</p></div>
<div class="m2"><p>چون منی حد این خطاب نداشت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عاقل از عشق هیچ بهره نیافت</p></div>
<div class="m2"><p>خارجی مهر بو تراب نداشت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>عقل اگر چند عقدها حل کرد</p></div>
<div class="m2"><p>مشکل عشق را جواب نداشت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>علم بی عشق هیچ سود نکرد</p></div>
<div class="m2"><p>عمل مبتدع ثواب نداشت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بر در دوست سیف فرغانی</p></div>
<div class="m2"><p>بجز از خویشتن حجاب نداشت</p></div></div>