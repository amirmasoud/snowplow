---
title: >-
    شمارهٔ ۲۰۲
---
# شمارهٔ ۲۰۲

<div class="b" id="bn1"><div class="m1"><p>هر که یک شکر از آن پسته دهان بستاند</p></div>
<div class="m2"><p>از لبش کام دل وقوت روان بستاند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زآن شهیدان که بشمشیر غمش کشته شدند</p></div>
<div class="m2"><p>ملک الموت نیارست که جان بستاند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر کجا پسته تنگش شکر افشانی کرد</p></div>
<div class="m2"><p>بنده چون دست ندارد بدهان بستاند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دست لطفش بدهدهر چه بخواهی لیکن</p></div>
<div class="m2"><p>چشم مستش دل صاحبنظران بستاند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چشم او صید دل خلق بتنها می کرد</p></div>
<div class="m2"><p>باش تا غمزه او تیروکمان بستاند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون درآید بچمن بارگه بستان را</p></div>
<div class="m2"><p>از گل و نارون آن سرو روان بستاند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از همه خلق (سخن) باز ستد عاشق تو</p></div>
<div class="m2"><p>طوطی ای دوست شکر ازمگسان بستاند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر زدست تو خوردگوشت بیابدچون شیر</p></div>
<div class="m2"><p>گربه آن پنجه که نان را ز سگان بستاند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زآستینی که ندارد چو بخواهد عاشق</p></div>
<div class="m2"><p>دست بیرون کندو هر دو جهان بستاند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بر سر خوانش صد کاسه گدایی بخورد</p></div>
<div class="m2"><p>تا یکی لقمه توانگر ز میان بستاند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دوست چون سر خود اندر دل عشاق نهاد</p></div>
<div class="m2"><p>هر کرا قوت نطق است زبان بستاند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>من چو سرباز کشم اسب سخن را در دم</p></div>
<div class="m2"><p>فکر هرجائیم از دست عنان بستاند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>سیف فرغانی رو بر خط او نه سر خویش</p></div>
<div class="m2"><p>تا ز دست اجلت خط امان بستاند</p></div></div>