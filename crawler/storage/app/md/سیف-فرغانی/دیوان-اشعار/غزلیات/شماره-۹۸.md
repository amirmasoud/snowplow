---
title: >-
    شمارهٔ ۹۸
---
# شمارهٔ ۹۸

<div class="b" id="bn1"><div class="m1"><p>دلبرا عشق تو اندر دل وجان داشتنیست</p></div>
<div class="m2"><p>عشق سریست که از خلق نهان داشتنیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا پس از مرگ وفنا زنده باقی باشم</p></div>
<div class="m2"><p>دل بعشق تو چو تن زنده بجان داشتنیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا مرا ظاهر و باطن ز تو غایب نبود</p></div>
<div class="m2"><p>دل بتو حاضر ودیده نگران داشتنیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای ولی نعمت جان چون در دندان دایم</p></div>
<div class="m2"><p>گوهر شکرتو در درج دهان داشتنیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا فشاند زلب اندر قفس تنگ دهان</p></div>
<div class="m2"><p>شکر ذکر تو، طوطی زبان داشتنیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>فارغ از جامه ونانم که چو نان وجامه</p></div>
<div class="m2"><p>غم وسودای تو این خوردنی آن داشتنیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا بترک غم تو پند کسی ننیوشد</p></div>
<div class="m2"><p>سر ازین عقل سبک گوش گران داشتنیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تازهرچه نتوانم نگهم دارد دوست</p></div>
<div class="m2"><p>شیر همت زپی دفع سگان داشتنیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تا که همکاسه مردم نشود، مروحه یی</p></div>
<div class="m2"><p>از پی رد مگس برسر خوان داشتنیست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تا زخوان ملکوتی شودت حوصله پر</p></div>
<div class="m2"><p>شکم وهم تهی از غم نان داشتنیست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سیف فرغانی ازین درد نمی کرد فغان</p></div>
<div class="m2"><p>عشق گل گفت ببلبل که فغان داشتنیست</p></div></div>