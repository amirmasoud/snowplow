---
title: >-
    شمارهٔ ۵۷۱
---
# شمارهٔ ۵۷۱

<div class="b" id="bn1"><div class="m1"><p>زهی خورشید را داده رخ تو حسن و زیبایی</p></div>
<div class="m2"><p>در لطف تو هرکس بر من نبندد گر تو بگشایی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بزیورها نکورویان بیارایند گر خود را</p></div>
<div class="m2"><p>تو بی زیور چنان خوبی که عالم را بیارایی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ترا همتا کجا باشد که در باغ جمال تو</p></div>
<div class="m2"><p>کند پسته شکرریزی کند سنبل سمن سایی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر نز بهر آن باشد که در پایت فتد روزی</p></div>
<div class="m2"><p>که باشد گل که در بستان برآرد سر بر عنایی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هم از آثار روی تست اگر گل راست بازاری</p></div>
<div class="m2"><p>ادب نبود ترا گفتن که چون گل حور سیمایی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اگر روزی ز درویشی دلی بردی زیان نبود</p></div>
<div class="m2"><p>که گر دولت بود یکشب بوصلش جان بیفزایی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چه باشد حال مسکینی که او را با غنای تو</p></div>
<div class="m2"><p>نه استحقاق وصل تست و نی از تو شکیبایی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>من مسکین بدین حضرت بصد اندیشه می آیم</p></div>
<div class="m2"><p>ز بیم آنک گویندم که حضرت را نمی شایی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اگر چه دیده مردم بماند خیره در رویت</p></div>
<div class="m2"><p>ببخشی دیده را صد نور اگر تو روی بنمایی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تو از من نیستی غایب که اندر جان خیال تو</p></div>
<div class="m2"><p>مرا در دل چو اندیشه است و در دیده چو بینایی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مرا با تو وصال ای جان میسر کی شود هرگز</p></div>
<div class="m2"><p>که من از خود روم آن دم که گویندم تو می آیی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چنان شیرینی ای خسرو که چون فرهاد در کویت</p></div>
<div class="m2"><p>جهانی چون مگس جمعند بر دکان حلوایی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>کنون ای سیف فرغانی که پایت خسته شد در ره</p></div>
<div class="m2"><p>برو بار سر از گردن بیفگن تا بیاسایی</p></div></div>