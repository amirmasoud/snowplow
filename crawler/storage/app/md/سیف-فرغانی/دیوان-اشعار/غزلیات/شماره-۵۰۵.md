---
title: >-
    شمارهٔ ۵۰۵
---
# شمارهٔ ۵۰۵

<div class="b" id="bn1"><div class="m1"><p>چو نیست غیر تو کس آفتاب با سایه</p></div>
<div class="m2"><p>تو سایه بر سر من افگن ای هما سایه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دلم بوصل طمع کرد گفتم اینت محال</p></div>
<div class="m2"><p>که جمع می نشود آفتاب با سایه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>میان روی تو و آفتاب تابان هست</p></div>
<div class="m2"><p>همان تفاوت کز آفتاب تا سایه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرا بسایه تو حاجتست واین دولت</p></div>
<div class="m2"><p>خوهم ولی نبود آفتاب را سایه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بتو پناه برم ازهمه که بر سر خاک</p></div>
<div class="m2"><p>درخت وار نمی افگند گیا سایه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>توانگری وجمال ترا چه نقصانست</p></div>
<div class="m2"><p>بلطف اگر فگنی برچو من گدا سایه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از آسمان برکت جذب کرد روی زمین</p></div>
<div class="m2"><p>چو بر وی افتاد از عدل پادشا سایه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تو پادشاهی وهمچون گدا نیندازد</p></div>
<div class="m2"><p>سگ فقیر تو بر نان اغنیا سایه</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>فقیر تو نکند اعتماد بر جزتو</p></div>
<div class="m2"><p>چو آدمی نزند تکیه بر عصا سایه</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شراب عشق تو درما چنان اثر کردست</p></div>
<div class="m2"><p>که مستی آرد زیر درخت ما سایه</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بسوی خرگه جانان گریزم ای گردون</p></div>
<div class="m2"><p>که نیست بهر امان خیمه ترا سایه</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>قتیل تیغ فنا جامه بقا پوشد</p></div>
<div class="m2"><p>چو بر وی افتد ازآن سرو در قبا سایه</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چو عشق در دلم آمد زمن نماند اثر</p></div>
<div class="m2"><p>چو دید طلعت خورشید شد زجا سایه</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نرنجد ار بزنی تیغ بر سر عاشق</p></div>
<div class="m2"><p>ننالد ارچه چوخاکست زیر پا سایه</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ثنای او نتوان گفت سیف فرغانی</p></div>
<div class="m2"><p>چگونه گوید خورشید را ثنا سایه</p></div></div>