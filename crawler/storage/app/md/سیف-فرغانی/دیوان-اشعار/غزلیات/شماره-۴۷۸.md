---
title: >-
    شمارهٔ ۴۷۸
---
# شمارهٔ ۴۷۸

<div class="b" id="bn1"><div class="m1"><p>ای لطف تو بسی مرا کار ساخته</p></div>
<div class="m2"><p>کارم شده زفضل تو صدبار ساخته</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر پای سعی در سر کارم نهند خلق</p></div>
<div class="m2"><p>بی دست لطف تو نشود کار ساخته</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کار مرا که غیر تو دیگر کسی نساخت</p></div>
<div class="m2"><p>کی گردد از معونت اغیار ساخته</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اندر عدم چو یک نظر از جودتو بیافت</p></div>
<div class="m2"><p>کار دو کون گشت بیکبار ساخته</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آن را که از خزانه فیض تو بهره نیست</p></div>
<div class="m2"><p>کارش نشد بدرهم ودینار ساخته</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از نعمت تو آنچه من امسال می خورم</p></div>
<div class="m2"><p>آن راوکیل رحمت تو پار ساخته</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از خرمن عطای تو هر آفریده یی</p></div>
<div class="m2"><p>چون مور دانه برده وانبار ساخته</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>حسن قضات بر طبق روی نیکوان</p></div>
<div class="m2"><p>در پسته طوطیان شکر بار ساخته</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هر دم چو عافیت دل رنجور عشق را</p></div>
<div class="m2"><p>درد تو نوش داروی بیمار ساخته</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ای نخل بند صنع تو در باغ و بوستان</p></div>
<div class="m2"><p>میوه زشاخ خشک وگل از خار ساخته</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ذکرت که ظلمت از دل عاقل برون برد</p></div>
<div class="m2"><p>جان راچو چشم مطلع انوار ساخته</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>در محکم کلام تو هر حرف ونقطه را</p></div>
<div class="m2"><p>علمت کتاب خانه اسرار ساخته</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>عراده قضای ترا گر کسی بجهد</p></div>
<div class="m2"><p>از بهر دفع قلعه ودیوار ساخته</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بیچاره درمقابل ثعبان موسوی</p></div>
<div class="m2"><p>چون ساحر از عصاورسن مار ساخته</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>سیف از دل صدف صفت خود پرازگهر</p></div>
<div class="m2"><p>صد بحر در سفینه اشعار ساخته</p></div></div>