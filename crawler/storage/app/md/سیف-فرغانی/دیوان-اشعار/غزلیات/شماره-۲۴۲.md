---
title: >-
    شمارهٔ ۲۴۲
---
# شمارهٔ ۲۴۲

<div class="b" id="bn1"><div class="m1"><p>رفتی و نام تو ز زبانم نمی رود</p></div>
<div class="m2"><p>واندیشه تو از دل و جانم نمی رود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر چه حدیث وصل تو کاری نه حد ماست</p></div>
<div class="m2"><p>الا بدین حدیث زبانم نمی رود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تو شاهدی نه غایب ازیرا خیال تو</p></div>
<div class="m2"><p>از پیش خاطر نگرانم نمی رود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گریم ز درد عشق و نگویم که حال چیست</p></div>
<div class="m2"><p>کین عذر بیش با همگانم نمی رود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خونی روانه کرده ام از دیده وین عجب</p></div>
<div class="m2"><p>کز حوض قالب آب روانم نمی رود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چندان چو سگ بکوی تو در خفته ام که هیچ</p></div>
<div class="m2"><p>از خاک درگه تو نشانم نمی رود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ذکر لب تو کرده ام ای دوست سالها</p></div>
<div class="m2"><p>هرگز حلاوتش ز دهانم نمی رود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از مشرب وصال خود این جان تشنه را</p></div>
<div class="m2"><p>آبی بده که دست بنانم نمی رود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دانم یقین که ماه رخی قاتل منست</p></div>
<div class="m2"><p>جز بر تو این نگار گمانم نمی رود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آبم روان ز دیده و خوابم شده ز چشم</p></div>
<div class="m2"><p>اینم همی نیاید و آنم نمی رود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از سیف رفت صبر و دل و هردم اندهی</p></div>
<div class="m2"><p>ناخوانده آید و چو برانم نمی رود</p></div></div>