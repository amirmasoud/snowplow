---
title: >-
    شمارهٔ ۲۳۶
---
# شمارهٔ ۲۳۶

<div class="b" id="bn1"><div class="m1"><p>گر جمله شهر صورت و روی نکو بود</p></div>
<div class="m2"><p>کو صورتی که این همه معنی درو بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خرم دل بهشتی و خوش عالمی بهشت</p></div>
<div class="m2"><p>گر در بهشت حور باین رنگ و بو بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در سجده گاه بندگی تو چو آسمان</p></div>
<div class="m2"><p>پیش تو بر زمین نهد آن را که رو بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آن کو بر آستانه کویت مقیم نیست</p></div>
<div class="m2"><p>چون کلب دربدر چو گدا کو بکو بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خو کرده با وصال ترا ای فرشته خو</p></div>
<div class="m2"><p>از خود بهجر دور کنی این چه خو بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آن زلف بسته گر بگشایی و هر دمی</p></div>
<div class="m2"><p>بر دوش افگنی سرش از پا فرو بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بوی شراب عشق تو آید ز جان من</p></div>
<div class="m2"><p>گر جسم خاک باشد و خاکش سبو بود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گفتم بسی و میل نکردی بسوی سیف</p></div>
<div class="m2"><p>گل را چه میل بلبل بسیار گو بود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>با عاشقان خویش جفاها کند بسی</p></div>
<div class="m2"><p>«ناچار هرکه صاحب روی نکو بود»</p></div></div>