---
title: >-
    شمارهٔ ۴۲۷
---
# شمارهٔ ۴۲۷

<div class="b" id="bn1"><div class="m1"><p>چو سعدی سیف فرغانی حدیث عشق با هرکس</p></div>
<div class="m2"><p>همی گوید که درد دل بیفزاید ز ناگفتن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگر چه حد من نبود حدیث عشق تو گفتن</p></div>
<div class="m2"><p>چو بلبل روی گل بیند بود معذور از آشفتن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هوس بازان عشق تو ز وصل چون تو شیرینی</p></div>
<div class="m2"><p>چو فرهادند بی حاصل ز کوه بیستون سفتن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ترا در خواب چون بینم که مشتاقان رویت را</p></div>
<div class="m2"><p>شبست از بهر بیداری و روز از بهر ناخفتن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گل خوش بوی مردم را بخود مشغول می دارد</p></div>
<div class="m2"><p>بخند ای غنچه لب تا گل خجل ماند زاشکفتن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اگر همچون نگین در زر نشاند بخت و اقبالم</p></div>
<div class="m2"><p>ز غیر تو اگر شمعم بخواهم نقش پذرفتن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>وگر تو نزد من آیی ز عزت خاک راهت را</p></div>
<div class="m2"><p>بخواهد مردم چشمم بجاروب مژه رفتن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گدا گر توشه یی خواهد کرامت کن ببخشیدن</p></div>
<div class="m2"><p>فقیر از تحفه یی آرذ تفضل کن بپذرفتن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اگر چه ترک من گفتی نگویم ترک تو زیرا</p></div>
<div class="m2"><p>خلاف دوستی باشد بترک دوستان گفتن</p></div></div>