---
title: >-
    شمارهٔ ۵۷۹
---
# شمارهٔ ۵۷۹

<div class="b" id="bn1"><div class="m1"><p>آن روی نگر بدان نکویی</p></div>
<div class="m2"><p>پرگشته ازو جهان نکویی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای از رخ تو خجل مه وخور</p></div>
<div class="m2"><p>دیگر مفزا برآن نکویی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>این آمدن تو در جهان هست</p></div>
<div class="m2"><p>در حق جهانیان نکویی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر روی زمین نمی دهد کس</p></div>
<div class="m2"><p>جز دررخ تو نشان نکویی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>درعهد تو بر زمین چو باران</p></div>
<div class="m2"><p>می بارد از آسمان نکویی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ازبهر لب تو گفته یاقوت</p></div>
<div class="m2"><p>چون لعل بصد زبان نکویی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عاشق نبود کسی که ازدل</p></div>
<div class="m2"><p>با تو نکند بجان نکویی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بر باد مده مرا که گشتست</p></div>
<div class="m2"><p>چون آب زتو روان نکویی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>عاشق بکند بهر چه دارد</p></div>
<div class="m2"><p>درکوی تو با سگان نکویی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>درحق من ای نگار می کن</p></div>
<div class="m2"><p>چون کرد همی توان نکویی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دانی که همی رسد بدرویش</p></div>
<div class="m2"><p>ازمال توانگران نکویی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>از خوان خود ای توانگر حسن</p></div>
<div class="m2"><p>در حق گدا بنان نکویی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>می کن که همی کنند مردم</p></div>
<div class="m2"><p>با کلب باستخوان نکویی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>از روی تو می خوهم نشانی</p></div>
<div class="m2"><p>ای روی ترا نشان نکویی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>سیف از سخن تو سودها کرد</p></div>
<div class="m2"><p>هرگز نکند زیان نکویی</p></div></div>