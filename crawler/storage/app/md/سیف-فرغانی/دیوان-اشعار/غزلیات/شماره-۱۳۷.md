---
title: >-
    شمارهٔ ۱۳۷
---
# شمارهٔ ۱۳۷

<div class="b" id="bn1"><div class="m1"><p>حق که این روی دلستان بتو داد</p></div>
<div class="m2"><p>پادشاهی نیکوان بتو داد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در جهان هرچه می خوهی می کن</p></div>
<div class="m2"><p>که جهان آفرین جهان بتو داد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در جهان نیکوان بسی بودند</p></div>
<div class="m2"><p>بنده خود را ازآن میان بتو داد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دل گم گشته باز می جستم</p></div>
<div class="m2"><p>چشم وابروی تو نشان بتو داد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مرغ مرده است دل که صید تو نیست</p></div>
<div class="m2"><p>بتو زنده است هرکه جان بتو داد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حسن روی تو بیش ازین چه کند</p></div>
<div class="m2"><p>که دل وجان عاشقان بتو داد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آفتاب ارچه صورتش پیداست</p></div>
<div class="m2"><p>معنی خویش در نهان بتو داد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زآسمان تا زمین گرفت بخود</p></div>
<div class="m2"><p>وز زمین تا بآسمان بتو داد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هرکه یک روز در رکاب تو رفت</p></div>
<div class="m2"><p>گر بدوزخ بری عنان بتو داد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بخ بخ ای دل (که) دوست در پیری</p></div>
<div class="m2"><p>اینچنین دولت جوان بتو داد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>روی نی، شمس غیب باتو نمود</p></div>
<div class="m2"><p>بوسه نی، عمر جاودان بتو داد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>آن حیاتی که روح زنده بدوست</p></div>
<div class="m2"><p>از دو لعل شکر فشان بتو داد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بر در دوست سیف فرغانی</p></div>
<div class="m2"><p>سگ درون رفت و آستان بتو داد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بر سر خوان لطف او اصحاب</p></div>
<div class="m2"><p>مغز خوردند واستخوان بتو داد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>آنکه عشقش بروح جان بخشد</p></div>
<div class="m2"><p>دل بغیر تو وزبان بتو داد</p></div></div>