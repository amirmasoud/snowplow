---
title: >-
    شمارهٔ ۳۳۵
---
# شمارهٔ ۳۳۵

<div class="b" id="bn1"><div class="m1"><p>دل سقیم شفا یابد از اشارت عشق</p></div>
<div class="m2"><p>اگر نجات خوهی گوش کن عبارت عشق</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو غافلان منشین، راه رو که برخیزد</p></div>
<div class="m2"><p>دوکون از سر راهت بیک اشارت عشق</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خبر دهد که تو مردی وشد دلت زنده</p></div>
<div class="m2"><p>زمرگ رستی اگر بشنوی بشارت عشق</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو هیزم ارچه بسی سوختی ولی خامی</p></div>
<div class="m2"><p>که همچو دیگ نجوشیدی از حرارت عشق</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تو بر وضوی قدم باش ودل مده بکسی</p></div>
<div class="m2"><p>که دوستی حدث بشکند طهارت عشق</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گرت دلست که سرمایه دار وصل شوی</p></div>
<div class="m2"><p>زسوز بگذر ودرساز با خسارت عشق</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چوآسمان اگرش صد هزار باشد چشم</p></div>
<div class="m2"><p>همیشه کور بود مرد بی بصارت عشق</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ورای عشق خرابیست تاسرت نرود</p></div>
<div class="m2"><p>برون منه قدمی هرگز از عمارت عشق</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>غلام وار همی کن ایاز را خدمت</p></div>
<div class="m2"><p>که خواجه چاکر بنده است در امارت عشق</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شبی زشربت وصلش دهان کنی شیرین</p></div>
<div class="m2"><p>چوتلخ کام شوی روزی از مرارت عشق</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دگر زحادثه غم نیست سیف فرغانی</p></div>
<div class="m2"><p>ترا که خانه بتاراج شد زغارت عشق</p></div></div>