---
title: >-
    شمارهٔ ۲۷۰
---
# شمارهٔ ۲۷۰

<div class="b" id="bn1"><div class="m1"><p>ای دل از دوست جان دریغ مدار</p></div>
<div class="m2"><p>جان ازآن دلستان دریغ مدار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ماه رویا توهم ز روی کرم</p></div>
<div class="m2"><p>نظر از عاشقان دریغ مدار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آفتابی برآسمان جمال</p></div>
<div class="m2"><p>نور خویش ازجهان دریغ مدار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>من زچشم بدان رقیب توام</p></div>
<div class="m2"><p>میوه ازباغبان دریغ مدار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>طوطی خوش سخن منم، ازمن</p></div>
<div class="m2"><p>شکری از آن لبان دریغ مدار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>لب نوشینت آب حیوانست</p></div>
<div class="m2"><p>آب ازتشنگان دریغ مدار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مست خوابست چشم مخمورت</p></div>
<div class="m2"><p>خواب ازپاسبان دریغ مدار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>لب بدندان من سپار وبخسب</p></div>
<div class="m2"><p>رطب ازاستخوان دریغ مدار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>صحبت ازناکسان دریغت نیست</p></div>
<div class="m2"><p>سخنی ازکسان دریغ مدار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بر درتست سیف فرغانی</p></div>
<div class="m2"><p>زآن گدا پیشه نان دریغ مدار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ازتو مارا سلام یا دشنام</p></div>
<div class="m2"><p>این اگر نیست آن دریغ مدار</p></div></div>