---
title: >-
    شمارهٔ ۹۳
---
# شمارهٔ ۹۳

<div class="b" id="bn1"><div class="m1"><p>عاشقانرا می دهد دایم نشان از روی دوست</p></div>
<div class="m2"><p>گل که هر سالی بمردم می رساند بوی دوست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دم بدم چون تار موسیقار در هر پرده یی</p></div>
<div class="m2"><p>خوش بنال ای یار تا در چنگت افتد موی دوست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زآفتاب و ماه و انجم گر تو خواهی راه رفت</p></div>
<div class="m2"><p>مشعله بر مشعله است از کوی تو با کوی دوست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر نظر داری برو از دیدن آن مشعله</p></div>
<div class="m2"><p>چشم دربند ای مبصر تا ببینی روی دوست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اندرین پستی ندیدم هیچ، زیباتر نبود</p></div>
<div class="m2"><p>زیر گردون همچو بر بالای چشم ابروی دوست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گاو گردون که کشد از بهر اسب دولتت</p></div>
<div class="m2"><p>گر شوی یک روز شهمات از رخ نیکوی دوست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خفته مر مقصود را چون دست در گردن کنی</p></div>
<div class="m2"><p>ای بپای جست و جو گامی نرفته سوی دوست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زاهل این خرگاه اطناب تعلق قطع کن</p></div>
<div class="m2"><p>پس بزن هرجا که خواهی خیمه در پهلوی دوست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سیف فرغانی بتیغ دوست گر کشته شوی</p></div>
<div class="m2"><p>عاشقی باش که (عاشق) کشتن آمد خوی دوست</p></div></div>