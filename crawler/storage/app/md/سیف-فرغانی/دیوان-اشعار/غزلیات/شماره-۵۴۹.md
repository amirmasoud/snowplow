---
title: >-
    شمارهٔ ۵۴۹
---
# شمارهٔ ۵۴۹

<div class="b" id="bn1"><div class="m1"><p>ماه سعادتم را باشد شب تمامی</p></div>
<div class="m2"><p>با تو گرم ببیند دشمن بدوستکامی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر آفتاب زردم ز آن روی شاد گردان</p></div>
<div class="m2"><p>کین روزه دار غم را تو چون نماز شامی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از چنگ باز عشقت چون کبک کوه گیرد</p></div>
<div class="m2"><p>طاوست ار ببیند وقتی که می خرامی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>محروم کرد ایام از خدمت تو مارا</p></div>
<div class="m2"><p>هجران زشاخ برکند آن میوه رابخامی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خال ترا چو دیدم بازلف تو بگفتم</p></div>
<div class="m2"><p>چون صید درنیفتد کین دانه راتو دامی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دل شد زخمر عشقت رسوا بنیم جرعه</p></div>
<div class="m2"><p>ننهفت حال خود را مست از رقیق جامی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>با عشق تو دل من از دشمنست ایمن</p></div>
<div class="m2"><p>هرچ آن حلال باشد نستاندش حرامی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از وصف حسن آن مه گر عاقلی حذر کن</p></div>
<div class="m2"><p>کاینجا ز ذکر لیلی مجنون شود نظامی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>با عاشقان عارف ازجام لاابالی</p></div>
<div class="m2"><p>خوردم شراب و رستم از ننگ نیکنامی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ترک ادب کن ای دل زیرا که دست ندهد</p></div>
<div class="m2"><p>از خواندن مصادر این پایگاه سامی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>جز مدح دوست ای سیف از تو چه خدمت آید</p></div>
<div class="m2"><p>در حضرتی که آنجا سلطان کند غلامی</p></div></div>