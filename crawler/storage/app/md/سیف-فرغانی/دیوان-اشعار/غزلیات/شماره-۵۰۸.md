---
title: >-
    شمارهٔ ۵۰۸
---
# شمارهٔ ۵۰۸

<div class="b" id="bn1"><div class="m1"><p>تا بعقل ورای خود در راه تو ننهیم پای</p></div>
<div class="m2"><p>طفل بی تدبیر باشد در ره تو عقل و رای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عاشق ثابت قدم را بر سر کوی تو هست</p></div>
<div class="m2"><p>ملک شاهان زیر دست وچرخ گردان زیر پای</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عاشق روی ترا دنیا نگر داند بخود</p></div>
<div class="m2"><p>همچو مغناطیس آهن جذب نکند کهربای</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عاشق رویت که در دنیا نیابد نان درست</p></div>
<div class="m2"><p>از دل اشکسته دارد لشکر عالم‌گشای</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مقبلی را کز جهان بر عشق تو اقبال کرد</p></div>
<div class="m2"><p>هست دولت داعی وبخت و سعادت رهنمای</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پرتو او جمله را در خور بود چون آفتاب</p></div>
<div class="m2"><p>سایه او بر همه میمون بود همچون همای</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>حاصل عالم چه باشد؟عاشقان روی تو</p></div>
<div class="m2"><p>میوه باشد معنی اشکوفه صورت نمای</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شوق چون غالب شود عاشق برون آید زخود</p></div>
<div class="m2"><p>زلزله چون سخت باشد کوه درگردد زجای</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بهر بار عشق ار از گاوزمین اشتر کنی</p></div>
<div class="m2"><p>بر سر گردون نهی اشتر بنالد چون درای</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شعر ما را نظم بخشد عشق تو مانند در</p></div>
<div class="m2"><p>باد را آواز سازد مطرب ما همچو نای</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سیف فرغانی اگر حاجت خوهی از غیر دوست</p></div>
<div class="m2"><p>آنچنان باشد که حاجت از گدا خواهد گدای</p></div></div>