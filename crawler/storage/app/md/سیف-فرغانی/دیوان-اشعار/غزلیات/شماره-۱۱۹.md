---
title: >-
    شمارهٔ ۱۱۹
---
# شمارهٔ ۱۱۹

<div class="b" id="bn1"><div class="m1"><p>شکری بجان خریدم زلب شکرفروشت</p></div>
<div class="m2"><p>که درون پرده با دل شب وصل بود دوشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بسخن جدا نمی شد لب لعل تو ز گوشم</p></div>
<div class="m2"><p>چو علم فرو نیامد سر دست من زدوشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بلبت حلاوتی ده دهن مرا که دایم</p></div>
<div class="m2"><p>ترش است روی زردم ز نبات سبز پوشت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بوصال جبر می کن دلک شکسته یی را</p></div>
<div class="m2"><p>که گرفت صبر سستی ز فراق سخت کوشت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سحری مرا خیالت بکرشمه گفت مسکین</p></div>
<div class="m2"><p>تویی آنکه داغ عشقش نگذاشت بی خروشت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>برخ چو آفتابش نگری بچشم شادی</p></div>
<div class="m2"><p>چو بمجلس وی آرد غم او گرفته گوشت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز دهن چو جام سازد چه شرابها که هر دم</p></div>
<div class="m2"><p>ز لبان باده رنگش بخوری و باد نوشت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تو ز دست رفتی آن دم که برید صیت حسنش</p></div>
<div class="m2"><p>خبری بگوشت آورد وز دل ببرد هوشت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تو که خار دیده بودی نبدی خمش چو بلبل</p></div>
<div class="m2"><p>چو بگلستان رسیدی که کند دگر خموشت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>همه شب ز بی قراری ز بسی فغان و زاری</p></div>
<div class="m2"><p>چو ندیده بودی او را بفلک شدی خروشت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>رخ وی آرمیدی، عجبست سیف از تو</p></div>
<div class="m2"><p>که بآتشی رسیدی و فرو نشست جوشت!</p></div></div>