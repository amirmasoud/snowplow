---
title: >-
    شمارهٔ ۵۰۴
---
# شمارهٔ ۵۰۴

<div class="b" id="bn1"><div class="m1"><p>چو کرد زلف تو پیرامن قمر حلقه</p></div>
<div class="m2"><p>قمر ز هر طرفی بوسه داد بر حلقه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز بند و حلقه زلف تو برده بودم جان</p></div>
<div class="m2"><p>کمند زلف تو بازم کشید در حلقه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز عاشقان تو ز آن زلف کس پریشان نیست</p></div>
<div class="m2"><p>بغیر بنده که آن جمع راست سر حلقه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز زلف تو که گریزد که بند کردستی</p></div>
<div class="m2"><p>هزار عاشق شوریده را بهر حلقه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو بند زلف تو زنجیر کرد در پایم</p></div>
<div class="m2"><p>زدم ز بدست طلب بر هزار در حلقه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بسان پای دلم ای صبادر آوردست</p></div>
<div class="m2"><p>در آن دو زلف چو زنجیر و می شمر حلقه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بهر طرف که نظر می کند دلم ز آن سوست</p></div>
<div class="m2"><p>کمند زلف تو افتاده حلقه برحلقه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو نقطه زو نتواند نهاد پای برون</p></div>
<div class="m2"><p>کجا کشید قضا دایره قدر حلقه</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>همین که پای دلی دست دادش اندر حال</p></div>
<div class="m2"><p>در افگند سر زلفت بیکد گر حلقه</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دلم چو سلسله در هم شود ز رشک آن دم</p></div>
<div class="m2"><p>که گرد موی میانت کند کمر حلقه</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بسعی بخت بگردن درافگند اقبال</p></div>
<div class="m2"><p>مرا ز ساعد سیمینت ای پسر حلقه</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مپوش روی ز قومی که زیر پرده شب</p></div>
<div class="m2"><p>زنند بر درت از شام تا سحر حلقه</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ز دیده در بفشانم چو دست سیمم نیست</p></div>
<div class="m2"><p>که بهر گوش غلامت کنم ز زر حلقه</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>تو گنج حسنی و کرده چو مار بر سر گنج</p></div>
<div class="m2"><p>زمرد خط تو گردلعل تر حلقه</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>برای گوش تو کردیم حلقها پر در</p></div>
<div class="m2"><p>که نیست آن لایق آن گوش بی درر حلقه</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ردیف این در از آن (حلقه) کرده ام کو را</p></div>
<div class="m2"><p>بگوش تو نرساند کس مگر حلقه</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بدستیاری زلف تو سیف فرغانی</p></div>
<div class="m2"><p>شکست بر دل اگر بند داشت ور حلقه</p></div></div>