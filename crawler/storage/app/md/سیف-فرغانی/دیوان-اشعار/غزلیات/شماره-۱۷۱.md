---
title: >-
    شمارهٔ ۱۷۱
---
# شمارهٔ ۱۷۱

<div class="b" id="bn1"><div class="m1"><p>کسی که ازلب شیرین تو دهان خوش کرد</p></div>
<div class="m2"><p>ببوسه تودل خویشتن چو جان خوش کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سزد که وقت مرا خوش کنی بدان رخ خوب</p></div>
<div class="m2"><p>که گل بر وی نکو وقت بلبلان خوش کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دهان غنچه لب و روی چون گلستانت</p></div>
<div class="m2"><p>بهاروار چوگل سربسر جهان خوش کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگرچه وصل تو مأمول ما بود لیکن</p></div>
<div class="m2"><p>چو غافلان بامل دل نمی توان خوش کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عجب مدار مرا گر سخن شود شیرین</p></div>
<div class="m2"><p>که ذکر شهد لب تو مرا زبان خوش کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کنون که موسم نوروز گشت وباد بهار</p></div>
<div class="m2"><p>وزید ناگه واطراف بوستان خوش کرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گلست گویی طالع شده زبرج حمل</p></div>
<div class="m2"><p>ستاره یی که زمین راچو آسمان خوش کرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نسیم بوی تو آورد وما نیاسودیم</p></div>
<div class="m2"><p>بمرهم تو جراحات خستگان خوش کرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ببنده گفت بیا کآن عزیز مصر جمال</p></div>
<div class="m2"><p>چو یوسف است که دل با برادران خوش کرد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>رخ چو ماه تو منشور ملک خوبی داشت</p></div>
<div class="m2"><p>خط تو برسر منشور او نشان خوش کرد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بدوست گفتم هرگز توان بدرویشی</p></div>
<div class="m2"><p>دل رقیب گدا روی او بنان خوش کرد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چو گربگان سر سفره کاسه می لیسند</p></div>
<div class="m2"><p>کجا توان دل سگ را باستخوان خوش کرد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>برای خلق سخن گفت سیف فرغانی</p></div>
<div class="m2"><p>بشهد خویش مگس کام دیگران خوش کرد</p></div></div>