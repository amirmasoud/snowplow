---
title: >-
    شمارهٔ ۴۴۱
---
# شمارهٔ ۴۴۱

<div class="b" id="bn1"><div class="m1"><p>ای پسر گر عاشقی دعوی ما ومن مکن</p></div>
<div class="m2"><p>از صفا تن را چو جان گردان وجان را تن مکن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بامدادان گر نبینی روی چون خورشید دوست</p></div>
<div class="m2"><p>روز را شب دان وچشم خود بدو روشن مکن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون نمی سوزی چو شمع اندر شب سودای یار</p></div>
<div class="m2"><p>گر چراغت روز باشد اندرو روغن مکن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اندرین معدن که مردان آستین پر زر کنند</p></div>
<div class="m2"><p>خویشتن را همچو طفلان خاک در دامن مکن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون نرفتی راه بر خود رنگ درویشی مبند</p></div>
<div class="m2"><p>چون شکاری نیست سگ را طوق در گردن مکن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نفس روباهست، اگر تو سگ نیی با آدمی</p></div>
<div class="m2"><p>گر گساری بهر این روباه شیرافگن مکن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عقل نیکوخواه داری نفس را فرمان مبر</p></div>
<div class="m2"><p>چون بلشکر استواری صلح با دشمن مکن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سر بسر ملک سلیمان زآدمی پر دیو شد</p></div>
<div class="m2"><p>چون پری دارست خانه اندرو مسکن مکن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون کسی دنیا خوهد با او حدیث دین مگو</p></div>
<div class="m2"><p>هرکه سر گین سوزد اندر مجمرش لادن مکن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سیف فرغانی برو همت زدنیا بر گسل</p></div>
<div class="m2"><p>از پی عنقای مغرب دانه از ارزن مکن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بهر یار ار شعر گویی نام غیر او مبر</p></div>
<div class="m2"><p>بهر چشم ار سرمه یی سایی خاک در هاون مکن</p></div></div>