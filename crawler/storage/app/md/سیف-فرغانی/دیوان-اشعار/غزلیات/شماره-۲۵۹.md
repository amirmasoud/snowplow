---
title: >-
    شمارهٔ ۲۵۹
---
# شمارهٔ ۲۵۹

<div class="b" id="bn1"><div class="m1"><p>بیا که بی تو مرا کار برنمی آید</p></div>
<div class="m2"><p>مهم عشق تو بی یار برنمی آید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مرا بکوی تو کاری فتاد،یاری ده</p></div>
<div class="m2"><p>که جز بیاری تو کار برنمی آید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مقام وصل بلندست ومن برونرسم</p></div>
<div class="m2"><p>سگش چو گربه بدیواربر نمی آید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ازآن درخت که درنوبهار گل رستی</p></div>
<div class="m2"><p>ببخت بنده به جز خار برنمی آید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو شغل عشق تو کاری چو موی باریکست</p></div>
<div class="m2"><p>ازآن چو موی بیکبار برنمی آید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بآب چشم برین خاک در نهال امید</p></div>
<div class="m2"><p>بسی نشاندم و بسیار برنمی آید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سزد که مزرعه را تخم نو کنم امسال</p></div>
<div class="m2"><p>که آنچه کاشته ام پابرنمی آید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز ذکر شوق خمش باش سیف فرغانی</p></div>
<div class="m2"><p>که آن حدیث بگفتار برنمی آید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>میان عاشق و معشوق بعد ازاین کاریست</p></div>
<div class="m2"><p>که آن بگفتن اشعار برنمی آید</p></div></div>