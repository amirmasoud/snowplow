---
title: >-
    شمارهٔ ۱۱۶
---
# شمارهٔ ۱۱۶

<div class="b" id="bn1"><div class="m1"><p>در آن زمان که دلم میل با جمالی داشت</p></div>
<div class="m2"><p>نبود بی خبر از سر عشق و حالی داشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زلطف معنی حسن ورا کمالی بود</p></div>
<div class="m2"><p>زعشق صورت حال رهی جمالی داشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زکان لطفش گویی برو فشانده بود</p></div>
<div class="m2"><p>هرآن جواهر مخزون که حق تعالی داشت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بعون طالع سعد آسمان همت من</p></div>
<div class="m2"><p>ز روی او مه و از ابروش هلالی داشت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو صفحهای رخش روی روزگار رهی</p></div>
<div class="m2"><p>زعشق چهره او دلفریب خالی داشت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو ذره بودم وبا آفتاب قربم بود</p></div>
<div class="m2"><p>ستاره بودم و با ماه اتصالی داشت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اگر وصال همی خواست درزمان می یافت</p></div>
<div class="m2"><p>ورانبساط همی کرد دل مجالی داشت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زحال دل چو بگفتم بجان جوابم داد</p></div>
<div class="m2"><p>که درمشاهده من بودم او خیالی داشت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مثال جان من آن روز همچو ریحان بود</p></div>
<div class="m2"><p>که درسراچه قرب از بدن سفالی داشت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>جمال دوست زهر پرده جلوه خود کرد</p></div>
<div class="m2"><p>کسی ندید که اهلیت وصالی داشت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>درآن دیار که یوسف رخی پدید آمد</p></div>
<div class="m2"><p>خرید و سود کند هر کسی که مالی داشت</p></div></div>