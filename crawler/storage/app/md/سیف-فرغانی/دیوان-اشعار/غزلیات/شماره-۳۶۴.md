---
title: >-
    شمارهٔ ۳۶۴
---
# شمارهٔ ۳۶۴

<div class="b" id="bn1"><div class="m1"><p>مجلس انس ترا چون محرم راز آمدم</p></div>
<div class="m2"><p>پیش شمع عشق چون پروانه جان بازآمدم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عشقت آمد در درونم از حجاب خود برون</p></div>
<div class="m2"><p>رفتم و اینجازبهر کشف آن راز آمدم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همچو نی درمجلس تو سالها بودم خموش</p></div>
<div class="m2"><p>بر دهان من نهادی لب بآواز آمدم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر نوازی ور زنی هرگز ننالم بی اصول</p></div>
<div class="m2"><p>کز در تسلیم با هرپرده دمساز آمدم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>درمن ار آتش زدی خندان شدم چون سوخته</p></div>
<div class="m2"><p>ور چو شمعم سر بریدی گردن افراز آمدم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سر بزیر پا نهادم تا مرادم دست داد</p></div>
<div class="m2"><p>چون فگندم بال وپر آنگه بپرواز آمدم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گرچه از شوق تو دارم آب چشمی همچو ابر</p></div>
<div class="m2"><p>همچو برق از شور عشقت آتش انداز آمدم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>من گدای حضرتم دریوزه من نان لطف</p></div>
<div class="m2"><p>نی زبهر استخوان چون سگ بدرباز آمدم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سیف فرغانی همی گوید بیا خونم بریز</p></div>
<div class="m2"><p>زآنکه من در کشتن خود با تو انباز آمدم</p></div></div>