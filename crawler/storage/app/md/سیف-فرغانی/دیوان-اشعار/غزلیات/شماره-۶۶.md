---
title: >-
    شمارهٔ ۶۶
---
# شمارهٔ ۶۶

<div class="b" id="bn1"><div class="m1"><p>طوطی روح که دامش قفس ناسوتست</p></div>
<div class="m2"><p>عندلیبی است که جایش چمن لاهوتست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بشکرهای ملون چو مگس حاجت نیست</p></div>
<div class="m2"><p>طوطیی را که ز خوان ملکوتش قوتست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یوسف عقل ترا نفس تو چون زندانست</p></div>
<div class="m2"><p>یونس روح ترا جسم تو بطن الحوتست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای بدنیا متمتع اگر این عمره و حج</p></div>
<div class="m2"><p>از پی نام کنی کعبه ترا حانوتست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در کهولت چو صبی طبع جوان آیین را</p></div>
<div class="m2"><p>زآن مریدی تو که پیر خردت فرتوتست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دل تو مرده دنیا و چنین تا لب گور</p></div>
<div class="m2"><p>سوبسو مرده کش توتن چون تابوتست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هرکه او تشنه دنیاست ازو ناید عشق</p></div>
<div class="m2"><p>مطلب آب ز چاهی که درو هاروتست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ای جوانمرد تو مرد پیرزن دنیا را</p></div>
<div class="m2"><p>زهره یی دان که رخش آفت صد ماروتست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دل خودبین تو امروز چو افعی شد کور</p></div>
<div class="m2"><p>زآن زمرد که بگرد لب چون یاقوتست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خویشتن را تو چو داود شماری لیکن</p></div>
<div class="m2"><p>هر سر موی تو در ملک یکی جالوتست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سیف فرغانی رو خدمت درویشان کن</p></div>
<div class="m2"><p>کرم قزاز بریشم گر برگ توتست</p></div></div>