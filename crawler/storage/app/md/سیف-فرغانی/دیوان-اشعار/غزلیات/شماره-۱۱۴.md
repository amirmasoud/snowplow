---
title: >-
    شمارهٔ ۱۱۴
---
# شمارهٔ ۱۱۴

<div class="b" id="bn1"><div class="m1"><p>گرچه متاع جان بر جانان خطرنداشت</p></div>
<div class="m2"><p>جان باز کز جهان دل ازو دوستر نداشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عاشق بدست همت خود در طریق عشق</p></div>
<div class="m2"><p>هرچ آن نه دوست بود بیفگند و برنداشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>قومی ز عشق خاص ندارند بهره یی</p></div>
<div class="m2"><p>خود عامتر بگو که کسی زین خبر نداشت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خفته درین نشیمن وز آن اوج مانده باز</p></div>
<div class="m2"><p>زیرا همای همت آن قوم پر نداشت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر سنگدل که او نپذیرفت نقش عشق</p></div>
<div class="m2"><p>او قلب بود و لایق این سکه زر نداشت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در آستین صدره دولت نکرد دست</p></div>
<div class="m2"><p>هر دامنی که درخور این جیب سر نداشت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عاشق نخواست مال چو حرصی درو نبود</p></div>
<div class="m2"><p>جوکی خرد مسیح چو در خانه خر نداشت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عاشق از آب وخاک نزاده است ای پسر</p></div>
<div class="m2"><p>پوشیده نیست بر تو که عیسی پدر نداشت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بی عشق هرچه گفت ازو کس نیافت ذوق</p></div>
<div class="m2"><p>باران نخورد از آن صدف او گهر نداشت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شعر کسی چو خواندی و حالت دگر نشد</p></div>
<div class="m2"><p>تیغش نبود تیز که زخمش اثر نداشت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آنکس که همچو سیف نخورد آب نیل عشق</p></div>
<div class="m2"><p>گر خاک مصر شد قصب او شکر نداشت</p></div></div>