---
title: >-
    شمارهٔ ۹۷
---
# شمارهٔ ۹۷

<div class="b" id="bn1"><div class="m1"><p>ای دریغا کز وصال یار ما را رنگ نیست</p></div>
<div class="m2"><p>دل ز دستم رفته و دلدارم اندر چنگ نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون بمهر دوست ورزیدن مرا نیکوست نام</p></div>
<div class="m2"><p>گر بطعن دشمنان بدنام باشم ننگ نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بی تو عالم بر دلم ای جان چو چشم سوزنست</p></div>
<div class="m2"><p>چشم سوزن بر دلم چون با تو باشم تنگ نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کس بتو مانند و نسبت نیست با تو خلق را</p></div>
<div class="m2"><p>زنگ همچون آینه آیینه همچون زنگ نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر میانت در زرو یاقوت گیرم چون کمر</p></div>
<div class="m2"><p>خدمتی نبود که آن جز خاک و این جز سنگ نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سعدی اریک چند در میدان تو اسبی براند</p></div>
<div class="m2"><p>مرکب ما پشت ریش و باره ما لنگ نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در سخن نیکست هرکس را و بر بالای چنگ</p></div>
<div class="m2"><p>این بریشمها که می بینی بیک آهنگ نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سازها دارند مردم مختلف بهر طرب</p></div>
<div class="m2"><p>لیک از آنها در خوش آوازی یکی چون چنگ نیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سیف فرغانی نکو گوید سخن منکر مشو</p></div>
<div class="m2"><p>چون توان گفتن شکر را طعم و گل را رنگ نیست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کار خواهی کرد عاشق شو که به زین نیست کار</p></div>
<div class="m2"><p>شعر خواهی گفت ازین سان گو که به زین لنک نیست</p></div></div>