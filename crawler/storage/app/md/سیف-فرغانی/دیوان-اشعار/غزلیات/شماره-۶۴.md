---
title: >-
    شمارهٔ ۶۴
---
# شمارهٔ ۶۴

<div class="b" id="bn1"><div class="m1"><p>هر کو نه خدمت تو کند در بطالتست</p></div>
<div class="m2"><p>وآن کو نه مدحت تو کند در ضلالتست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قومی بکار دنیی وقومی بآخرت</p></div>
<div class="m2"><p>مشغولیی که با تو نباشد بطالتست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نظمی که می کنیم وبآخر نمی رسد</p></div>
<div class="m2"><p>از داستان عشق تو اول مقالتست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از حال ما خبر ز مجانین عشق پرس</p></div>
<div class="m2"><p>هشیار را خبر نبود کین چه حالتست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفتم بدل که تحفه جانان مکن زجان</p></div>
<div class="m2"><p>گو را بکار ناید ومارا خجالتست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ابرام نامه گرچه ازآن در بریده ام</p></div>
<div class="m2"><p>آهم رسول صادق وشعرم رسالتست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای عالم ار تو وعده بجنت کنی خطاست</p></div>
<div class="m2"><p>مشتاق دوست را که ز حورش ملالتست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آنکو بعلم وعقل خود از دوست باز ماند</p></div>
<div class="m2"><p>عقلش جنون شناس که علمش جهالتست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>وقتست سیف را که نگوید دگر سخن</p></div>
<div class="m2"><p>ذکرت بدل کند که زبان را کلالتست</p></div></div>