---
title: >-
    شمارهٔ ۵۰۳
---
# شمارهٔ ۵۰۳

<div class="b" id="bn1"><div class="m1"><p>از پسته تنگ خود آن یار شکر بوسه</p></div>
<div class="m2"><p>دوشم بلب شیرین جان داد بهر بوسه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از بهر غذای جان ای زنده بآب و نان</p></div>
<div class="m2"><p>بستد لب خشک من زآن شکرتر بوسه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای کرده رخت پیدا بر روی قمر لاله</p></div>
<div class="m2"><p>وی کرده لبت پنهان در تنگ شکر بوسه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مه نور همی خواهد از روی تو در پرده</p></div>
<div class="m2"><p>جان را ز همی گوید با لعل تو در بوسه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نزد تو خریداران گر معدن سیم آرند</p></div>
<div class="m2"><p>ای گنج گهر زآن لب مفروش بزر بوسه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای قبله جان هر شب بر خاک درت عاشق</p></div>
<div class="m2"><p>چون کعبه روان داده بر روی حجر بوسه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون جوف صدف او را پر در دهنی باید</p></div>
<div class="m2"><p>وآنگه طلب کردن زآن درج گهر بوسه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خواهی که شکر بارد از چشم چو بادامت</p></div>
<div class="m2"><p>رو آینه بین وز خود بستان بنظر بوسه</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون خاک سر کویت آهنگ هوا کرده</p></div>
<div class="m2"><p>بر ذره بمهر دل داده مه و خور بوسه</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هرجا که تو برخیزی از پای تو بستاند</p></div>
<div class="m2"><p>زنجیر سر زلفت چون حلقه ز در بوسه</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>لطفت که چو اندیشه حد نیست کنارش را</p></div>
<div class="m2"><p>از روی تو انعامی دیدیم مگر بوسه</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>سیف ار ز تو می خواهد بوسه تو برو می خند</p></div>
<div class="m2"><p>کز لعل تو خوش باشد گر خنده و گر بوسه</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گر پای رقیبانت بوسند محبانت</p></div>
<div class="m2"><p>ترسا ز پی عیسی زد بر سم خر بوسه</p></div></div>