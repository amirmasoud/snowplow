---
title: >-
    شمارهٔ ۵۲۶
---
# شمارهٔ ۵۲۶

<div class="b" id="bn1"><div class="m1"><p>تویی سلطان ملک حسن و چون من صد گدا داری</p></div>
<div class="m2"><p>ترا کی برگ من باشد که چندین بی نوا داری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وصالت خوان سلطانست، ازو محروم محتاجان</p></div>
<div class="m2"><p>زنانش گوشه یی بشکن که بر در صد گدا داری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سپاه ماه بشکستی بدان روی و نمی دانی</p></div>
<div class="m2"><p>کزین دلهای اشکسته چه لشکر در قفا داری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کلاه شاهی خوبان بدست ناز بر سر نه</p></div>
<div class="m2"><p>که با این جسم همچون جان دو عالم در قبا داری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سزد گر اسم الرحمان شود کرسی فخر او</p></div>
<div class="m2"><p>که عرشی از دل عاشق محل استوا داری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز تو ای دوست تا دیدم همه رنج و بلا دیدم</p></div>
<div class="m2"><p>نرفتم گر جفا دیدم، همین باشد وفاداری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز عدل چون تو سلطانی چنین احسان روا نبود</p></div>
<div class="m2"><p>کی نی دستم همی گیری نه از من دست واداری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بهر چشمی که می خواهی بلطف و قهر یک نوبت</p></div>
<div class="m2"><p>نظر کن سوی من گرچه ز درویشان غنا داری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>پس از چندین دعا نتوان تهی در آستین کردن</p></div>
<div class="m2"><p>کف در یوزه ما را چو تو دست عطا داری</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مرا دی گفت روی تو ز وصافان حسن من</p></div>
<div class="m2"><p>سخن از دل تو می گویی که جان آشنا داری</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بضر و نفع عاشق وار ثابت باش در کویش</p></div>
<div class="m2"><p>که گردورت کند از در دری دیگر کجا داری</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چو باشد سیف فرغانی بر خلق از فراموشان</p></div>
<div class="m2"><p>بوقتی کین غزل خوانی مرا ای دوست یاد آری</p></div></div>