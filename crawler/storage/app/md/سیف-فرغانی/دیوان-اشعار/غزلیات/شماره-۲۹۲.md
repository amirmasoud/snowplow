---
title: >-
    شمارهٔ ۲۹۲
---
# شمارهٔ ۲۹۲

<div class="b" id="bn1"><div class="m1"><p>مست عشقت بخود نیاید باز</p></div>
<div class="m2"><p>ور ببری سرش چو شمع بگاز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای بنیکی ز خوب رویان فرد</p></div>
<div class="m2"><p>وی بخوبی ز نیکوان ممتاز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هرکه در سایه تو باشد نیست</p></div>
<div class="m2"><p>روز او را بآفتاب نیاز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هرکه را عشق تو طهارت داد</p></div>
<div class="m2"><p>در دو عالم نیافت جای نماز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>قبله چون روی تست عاشق را</p></div>
<div class="m2"><p>دل بسوی تو به که رو بحجاز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عشق تو در درون ما ازلیست</p></div>
<div class="m2"><p>ما نه اکنون همی کنیم آغاز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هیچ بی درد را نخواهد عشق</p></div>
<div class="m2"><p>هیچ گنجشک را نگیرد باز</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عشق بر من ببست راه وصال</p></div>
<div class="m2"><p>شیر بر سگ نمی کند در باز</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تا سخن از پی تو می گویم</p></div>
<div class="m2"><p>بلبل از بهر گل کند آواز</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>عشق سلطان قاهرست و کند</p></div>
<div class="m2"><p>صد چو محمود را غلام ایاز</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>همچو فرهاد بی نوایی را</p></div>
<div class="m2"><p>عشق با خسروان کند انباز</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هرکه از بهر تو نگفت سخن</p></div>
<div class="m2"><p>سخنش در حقیقت است مجاز</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>دلم از قوس ابروت آن دید</p></div>
<div class="m2"><p>که هدف از کمان تیرانداز</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بتو حسن تو ره نمود مرا</p></div>
<div class="m2"><p>بوی مشکست مشک را غماز</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نوبت تست سیف فرغانی</p></div>
<div class="m2"><p>بسخن شور در جهان انداز</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>کآفرین می کنند بر سخنت</p></div>
<div class="m2"><p>شکر از مصر و سعدی از شیراز</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>سوز اهل نیاز نشناسد</p></div>
<div class="m2"><p>متنعم درون پرده ناز</p></div></div>