---
title: >-
    شمارهٔ ۵۸۰
---
# شمارهٔ ۵۸۰

<div class="b" id="bn1"><div class="m1"><p>این اتفاق طرفه بین کاندر فتد</p></div>
<div class="m2"><p>چون من گدایی با چو تو شهزاده‌ای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کی کفؤ باشد ماه را استاره‌ای</p></div>
<div class="m2"><p>چون مثل باشد لعل را بی‌جاده‌ای</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مپسند کز کمتر غلامی کم بود</p></div>
<div class="m2"><p>در بندگی تو چو من آزاده‌ای</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>انصاف ده تا چون شکیبایی کند</p></div>
<div class="m2"><p>بی‌چون تو دلبر همچو من دلداده‌ای</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نگرفت نقش دیگری تا نقش خود</p></div>
<div class="m2"><p>بنشاندی در طبع چون من ساده‌ای</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای خورده روح از جام عشقت باده‌ای</p></div>
<div class="m2"><p>می‌کن نظر در کار کارافتاده‌ای</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مخمور کرده عقل هشیار مرا</p></div>
<div class="m2"><p>چشمت که مستی می‌کند بی‌باده‌ای</p></div></div>