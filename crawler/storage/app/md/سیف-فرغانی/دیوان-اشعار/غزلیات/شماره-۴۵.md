---
title: >-
    شمارهٔ ۴۵
---
# شمارهٔ ۴۵

<div class="b" id="bn1"><div class="m1"><p>چو حسن روی تو آوازه در جهان انداخت</p></div>
<div class="m2"><p>هوای عشق تو در جان بی دلان انداخت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سمن بران همه چوگان خویش بشکستند</p></div>
<div class="m2"><p>کنون که شاه رخت گوی در میان انداخت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از آن میانه گل و لاله را برآمد نام</p></div>
<div class="m2"><p>چو بحر حسن تو خاشاک بر کران انداخت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کمان ابروی خود بین که ترک غمزه تو</p></div>
<div class="m2"><p>خطا نکرد خدنگی کزآن کمان انداخت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ترا بدیدم و صبر و قرار رفت از من</p></div>
<div class="m2"><p>مگس چو دید عسل خویشتن در آن انداخت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عقاب عشق توام صید کرد و در اول</p></div>
<div class="m2"><p>چو گوشت خورد و بآخر چو استخوان انداخت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو تو ز نور سپر پیش روی داشته ای</p></div>
<div class="m2"><p>کجا بسوی تو تیر نظر توان انداخت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مرا یقین شده بود آنکه من بتو برسم</p></div>
<div class="m2"><p>کرشمهای توام باز در گمان انداخت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بجهد بنده بوصلت رسد اگر بتوان</p></div>
<div class="m2"><p>ببیل خاک زمین را بر آسمان انداخت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بشعر وصف جمال تو خواستم کردن</p></div>
<div class="m2"><p>ولی جلال توام عقده بر زبان انداخت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چو خواستم که کنم نسبتش بلعل و عقیق</p></div>
<div class="m2"><p>لب تو ناطقه را سنگ در دهان انداخت</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>کسی که در ره عشق آمد او دو عالم را</p></div>
<div class="m2"><p>چو میخ کفش برفتن یکان یکان انداخت</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بآب شعر رهی غسل دل کند درویش</p></div>
<div class="m2"><p>که آتش طلبش در میان جان انداخت</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ترا چو دید بسی گفت سیف فرغانی</p></div>
<div class="m2"><p>«چه فتنه بود که حسن تو در جهان انداخت »</p></div></div>