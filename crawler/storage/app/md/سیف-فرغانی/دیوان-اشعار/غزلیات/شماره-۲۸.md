---
title: >-
    شمارهٔ ۲۸
---
# شمارهٔ ۲۸

<div class="b" id="bn1"><div class="m1"><p>فرامش کرد جان تو تماشاگاه اعلی را</p></div>
<div class="m2"><p>که خاکش دام دل باشد نگارستان دنیی را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>منه رخت اندرین ویران که در خلد برین رضوان</p></div>
<div class="m2"><p>بشارت می دهد هردم بتو فردوس اعلی را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بخارستان دنیا در مکن با هر خس آمیزش</p></div>
<div class="m2"><p>که دولت بهر تو دارد پر از گل باغ عقبی را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تو اندر تیه دنیایی چو اسراییلیان حیران</p></div>
<div class="m2"><p>عجب باشد که نفروشی بتره من وسلوی را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>برو علم پیمبر را مسلمان وار تابع شو</p></div>
<div class="m2"><p>که ترسایان ز جهل خود خدا گفتند عیسی را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بدستار و بدراعه نباشد قیمت عارف</p></div>
<div class="m2"><p>که عزت زآستین نبود ید بیضای موسی را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو ابراهیم اگر مردی بت آزر شکن، تا کی</p></div>
<div class="m2"><p>چو صورت بین بی معنی پرستی نقش مانی را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>برسم صورت آرایان برای چشم رعنایان</p></div>
<div class="m2"><p>چو تو پیراهنی شستی نجس شد جامه تقوی را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اگر تو ترک جان نکنی کمال او نگردد کم</p></div>
<div class="m2"><p>وگر حاجی بزی نکشد چه نقصان عید اضحی را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مکن چون طالب دنیا جهان صورت آبادان</p></div>
<div class="m2"><p>که در ویرانی صورت بیابی گنج معنی را</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ورای دوست اندر دل بت است ای خواجه محوش کن</p></div>
<div class="m2"><p>که اندر کعبه نپسندد مسلمان لات و عزی را</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مکن گر شاه و سلطانی بظلم و جور ویرانی</p></div>
<div class="m2"><p>که تا اکنون اثر مانده است عدل آباد کسری را</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چو رهبر نیست ای ره رو تمسک کن بشعر من</p></div>
<div class="m2"><p>چو در ره قایدی نبود عصا چشم است اعمی را</p></div></div>