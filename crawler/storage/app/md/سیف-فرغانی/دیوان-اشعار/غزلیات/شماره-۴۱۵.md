---
title: >-
    شمارهٔ ۴۱۵
---
# شمارهٔ ۴۱۵

<div class="b" id="bn1"><div class="m1"><p>ای از چو تو شیرین لبی صد شور در هر انجمن</p></div>
<div class="m2"><p>آن را که آمد یاد تو چون من برفت از خویشتن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گردن کشان حسن را در زر پای تست سر</p></div>
<div class="m2"><p>ای پست پیش قامتت بالای سرو ونارون</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون نافه آهوی چین پر مشک گشتی سربسر</p></div>
<div class="m2"><p>گر باد بوی زلف تو بردی سوی خاک ختن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اندر میان عاشقان صد کشته وخسته بود</p></div>
<div class="m2"><p>چشم ترا در هر نظر زلف ترا در هر شکن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر برفروزی روی خود ور بر فشانی موی خود</p></div>
<div class="m2"><p>هم مشک پاشی بر هوا هم لاله پوشی بر سمن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر نیکوان سلطان تویی سلطان نیکویان تویی</p></div>
<div class="m2"><p>خود خسرو خوبان تویی ای دلبر شیرین سخن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>قند ونبات اندر دهان آب حیات اندر لبان</p></div>
<div class="m2"><p>مه داری اندر برقع وگل داری اندر پیرهن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>با روی همچون گلستان هر گل ازو صد بوستان</p></div>
<div class="m2"><p>شاید که عار آید ترا از لاله و از یاسمن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نشد سیف فرغانی خموش اندر صفات حسن تو</p></div>
<div class="m2"><p>بلبل کجا گوید سخن چون گل نباشد در چمن</p></div></div>