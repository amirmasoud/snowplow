---
title: >-
    شمارهٔ ۴۰۷
---
# شمارهٔ ۴۰۷

<div class="b" id="bn1"><div class="m1"><p>درعشق دوست ازسر جان نیز بگذریم</p></div>
<div class="m2"><p>دریک نفس زهر دو جهان نیز بگذریم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مالی کزو فقیر وغنی را توانگریست</p></div>
<div class="m2"><p>درویش وار ازسر آن نیز بگذریم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر دل چو دیگران نگرانی کند بغیر</p></div>
<div class="m2"><p>درحال ازین دل نگران نیز بگذریم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>قومی نشسته اند بر ای جنان وحور</p></div>
<div class="m2"><p>برخیز تا زحور وجنان نیز بگذریم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ازلامکان گذشتن اگرچه نه کار ماست</p></div>
<div class="m2"><p>گر لا مدد کند زمکان نیز بگذریم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هرچند ازمکان بزمانی توان گشت</p></div>
<div class="m2"><p>وقتی بود که ما ز زمان نیز بگذریم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>این عقل وبخت ازپی دنیا بود بکار</p></div>
<div class="m2"><p>ازعقل پیرو بخت جوان نیز بگذریم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>باشدکه باز همت ما پر برآورد</p></div>
<div class="m2"><p>تا زین شکارگاه سگان نیز بگذریم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بیچاره سیف ذوق خموشی نیافتست</p></div>
<div class="m2"><p>تا(خود)زنظم این سخنان نیز بگذریم</p></div></div>