---
title: >-
    شمارهٔ ۳۵۸
---
# شمارهٔ ۳۵۸

<div class="b" id="bn1"><div class="m1"><p>بتی که ازلب او ذوق جان همی یابم</p></div>
<div class="m2"><p>درو چو گم شدم او را ازآن همی یابم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هرآن نفس که ببینم جمال او گویی</p></div>
<div class="m2"><p>که مرده بوده ام و باز جان همی یابم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زیاد آن رخ رنگین که گل نمونه اوست</p></div>
<div class="m2"><p>مدام در دل خود گلستان همی یابم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همی نیافتم از وی نشان بهیچ طرف</p></div>
<div class="m2"><p>کنون بهر طرفی زو نشان همی یابم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بهر کنار که دامی نهد سر زلفش</p></div>
<div class="m2"><p>چو مرغ پای خود اندر میان همی یابم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ازین جهان چو مرا کرد بی خبر عشقش</p></div>
<div class="m2"><p>زخویشتن خبری زآن جهان همی یابم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کسی زصحبت حور آن نیابد اندر خلد</p></div>
<div class="m2"><p>که من ز دیدن این دلستان همی یابم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نیافت خسرو آن ذوق از لب شیرین</p></div>
<div class="m2"><p>که من زبوسه پای فلان همی یابم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نمی روم ز درش همچو سیف فرغانی</p></div>
<div class="m2"><p>که هر چه خواهم ازین آستان همی یابم</p></div></div>