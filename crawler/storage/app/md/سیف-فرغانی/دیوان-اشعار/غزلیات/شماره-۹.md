---
title: >-
    شمارهٔ ۹
---
# شمارهٔ ۹

<div class="b" id="bn1"><div class="m1"><p>چنان عشقش پریشان کرد ما را</p></div>
<div class="m2"><p>که دیگر جمع نتوان کرد ما را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سپاه صبر ما بشکست چون او</p></div>
<div class="m2"><p>بغمزه تیر باران کرد ما را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حدیث عاشقی با او بگفتیم</p></div>
<div class="m2"><p>بخندید او و گریان کرد ما را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو بربط برکناری خفته بودیم</p></div>
<div class="m2"><p>بزد چنگی و نالان کرد ما را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>لب چون غنچه را بلبل نوا کرد</p></div>
<div class="m2"><p>چو گل بشکفت و خندان کرد ما را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بشمشیری که از تن سر نبرد</p></div>
<div class="m2"><p>بکشت و زنده چون جان کرد ما را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>غمش چون قطب ساکن گشت در دل</p></div>
<div class="m2"><p>ولی چون چرخ گردان کرد ما را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کنون انفاس ما آب حیاتست</p></div>
<div class="m2"><p>که از غمهای خود نان کرد ما را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بسان ذره بی تاب بودیم</p></div>
<div class="m2"><p>کنون خورشید تابان کرد ما را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مرا هرگز نبینی تا نمیری</p></div>
<div class="m2"><p>بگفت و کار آسان کرد ما را</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چو بر درد فراقش صبر کردیم</p></div>
<div class="m2"><p>بوصل خویش درمان کرد ما را</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بسان سیف فرغانی بر این در</p></div>
<div class="m2"><p>گدا بودیم سلطان کرد ما را</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نسیم حضرت لطفش صباوار</p></div>
<div class="m2"><p>بیکدم چون گلستان کرد ما را</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چو نفس خویش را گردن شکستیم</p></div>
<div class="m2"><p>سر خود در گریبان کرد ما را</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>کنون او ما و ما اوییم در عشق</p></div>
<div class="m2"><p>دگر زین بیش چه توان کرد ما را</p></div></div>