---
title: >-
    شمارهٔ ۲۱۹
---
# شمارهٔ ۲۱۹

<div class="b" id="bn1"><div class="m1"><p>باز آن زمان رسید که گلزار گل کند</p></div>
<div class="m2"><p>هر شاخ میوه آرد وهرخارگل کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عاشق بدو نظر نکند جز ببوی دوست</p></div>
<div class="m2"><p>باغ ار شکوفه آرد وگلزار گل کند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>میوه فروش کی خردش بر امید سود</p></div>
<div class="m2"><p>گرموم رنگ داده ببازار گل کند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بربوی وصل دوست درخت امید ماست</p></div>
<div class="m2"><p>شاخی که کم برآرد وبسیار گل کند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با روی همچو روضه شود شرمسار حور</p></div>
<div class="m2"><p>باغ بهشت اگر چو رخ یار گل کند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گرشاه (من) برقعه شطرنج بنگرد</p></div>
<div class="m2"><p>نبود عجب که هردو رخش خارگل کند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در روضه دلی که غم عشق بیخ کرد</p></div>
<div class="m2"><p>کی شعبه محبت اغیار گل کند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کی مستعد عشق شود جان منجمد</p></div>
<div class="m2"><p>هرگز طمع مکن که سپیدار گل کند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آن را که خار عشق فرو شد بپای دل</p></div>
<div class="m2"><p>سرچون درخت میوه ودستار گل کند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بار درخت حالش اناالحق بود مدام</p></div>
<div class="m2"><p>حلاج راکه شعبه اسرار گل کند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بر هر ورق که ذکر جمالش نوشت سیف</p></div>
<div class="m2"><p>شاید که در سفینه اشعار گل کند</p></div></div>