---
title: >-
    شمارهٔ ۳۹۵
---
# شمارهٔ ۳۹۵

<div class="b" id="bn1"><div class="m1"><p>گر دست دوست وار در آری بگردنم</p></div>
<div class="m2"><p>پیوسته بوی دوستی آید ز دشمنم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دیده رخ چو آتش تو دید برفروخت</p></div>
<div class="m2"><p>قندیل عشق در دل چون آب روشنم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در سوز و در گداز چو شمعم که روز و شب</p></div>
<div class="m2"><p>سوزی فتیله وار و گدازی چو روغنم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر یکدم آستین کنم از پیش چشم دور</p></div>
<div class="m2"><p>از آب دیده تر شود ای دوست دامنم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هرگه شراب عشق تو در من اثر کند</p></div>
<div class="m2"><p>گر توبه آهنست بخامیش بشکنم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون که ز دانه هیچ نگردم ز تو جدا</p></div>
<div class="m2"><p>گر چه بباد بر دهی ای جان چو خرمنم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در فرقت تو زین تن بی جان خویشتن</p></div>
<div class="m2"><p>در جامه ناپدید از جان بی تنم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر همچو میخ سنگ جفا بر سرم زنی</p></div>
<div class="m2"><p>آن ظن مبر که خیمه ز پهلوت برکنم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ور تار ریسمان شود این تن عجب مدار</p></div>
<div class="m2"><p>غمهای تست در دل چون چشم سوزنم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>فردا که روز آخر خواندست ایزدش</p></div>
<div class="m2"><p>اول کسی که با تو خصومت کند منم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>من جان چو سیف پیش محبان کنم سپر</p></div>
<div class="m2"><p>گر تیغ برکشی که محبان همی زنم</p></div></div>