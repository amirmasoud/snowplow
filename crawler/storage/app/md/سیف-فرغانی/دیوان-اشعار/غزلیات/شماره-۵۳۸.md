---
title: >-
    شمارهٔ ۵۳۸
---
# شمارهٔ ۵۳۸

<div class="b" id="bn1"><div class="m1"><p>اگر فراق تو زین سان اثر کند روزی</p></div>
<div class="m2"><p>مرا بخون جگر دیده تر کند روزی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ازین نکوترم ای دوست گر نخواهی داشت</p></div>
<div class="m2"><p>غم تو حال مرا زین بتر کندروزی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>برین خرابه که محنت ازو نمی گذرد</p></div>
<div class="m2"><p>امید هست که دولت گذر کند روزی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر بباغ بقاصر صرفنا نرسد</p></div>
<div class="m2"><p>شجر شکوفه شکوفه ثمر کند روزی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>وگر بجانب عمان کند سحاب گذر</p></div>
<div class="m2"><p>صدف ز قطره باران گهر کند روزی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو اهل وجد زخود بی خبر شوم چو کسی</p></div>
<div class="m2"><p>مرا زآمدن تو خبر کند روزی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بود که بخت بشمع عنایت ایزد</p></div>
<div class="m2"><p>چراغ مرده ما باز بر کند روزی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زباغ وصل تو درویش میوه یی بخورد</p></div>
<div class="m2"><p>درخت دولت اگر هیچ برکند روزی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بوصل هجر مبدل شود عجب نبود</p></div>
<div class="m2"><p>خدای قادر شب را اگر کند روزی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>برای دیدن روی تو چشم می دارم</p></div>
<div class="m2"><p>که بخت کور مرا دیده ور کند روزی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مباش ایمن از آه سیف فرغانی</p></div>
<div class="m2"><p>که ناله شب او هم اثر کند روزی</p></div></div>