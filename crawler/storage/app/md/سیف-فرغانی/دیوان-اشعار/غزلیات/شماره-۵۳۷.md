---
title: >-
    شمارهٔ ۵۳۷
---
# شمارهٔ ۵۳۷

<div class="b" id="bn1"><div class="m1"><p>بهار آمد و گویی که باد نوروزی</p></div>
<div class="m2"><p>فشانده مشک بر اطراف باغ پیروزی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کنار جوی چو شد سبز در میان چمن</p></div>
<div class="m2"><p>بیا بگو که چه خواهم من از تو نوروزی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز وصل شاهد گل گشت ناله بلبل</p></div>
<div class="m2"><p>چو در فراق تو اشعار من بدلسوزی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چه باشد ار تو بدان طلعت جهان افروز</p></div>
<div class="m2"><p>چراغ دولت بیچاره یی برافروزی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بلای عشق تو تا زنده ام نصیب منست</p></div>
<div class="m2"><p>باقتضای اجل منقطع شود روزی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو هست در حق من اقتضای رای تو بد</p></div>
<div class="m2"><p>مگر که بخت منت می کند بدآموزی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مخواه هیچ به جز عشق سیف فرغانی</p></div>
<div class="m2"><p>که عشق دوست ترا به ز هر چه اندوزی</p></div></div>