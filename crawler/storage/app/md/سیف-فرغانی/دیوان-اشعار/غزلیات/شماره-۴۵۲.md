---
title: >-
    شمارهٔ ۴۵۲
---
# شمارهٔ ۴۵۲

<div class="b" id="bn1"><div class="m1"><p>ایا چو فصل بهار از رخت جهانرا زین</p></div>
<div class="m2"><p>رخ تو ثانی خورشید و ثالث القمرین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بسوی جدول خوبان که مظهر حسنند</p></div>
<div class="m2"><p>لطافت آب روان آمد و تو رأس العین</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همین که در تو اثر کرد شرم عثمانی</p></div>
<div class="m2"><p>شود زرنگ دو رخ چهره تو ذوالنورین</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از آدمی و پری هیچ کس نماند زشت</p></div>
<div class="m2"><p>چو نور روی تو قسمت کنند بر ثقلین</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگر چه کوی تو امروز شهرتی دارد</p></div>
<div class="m2"><p>بکشتگان غم تو چو کربلا بحسین</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کنون بلعل تو آب حیات نسبت یافت</p></div>
<div class="m2"><p>چو طول عمر بخضر و چوسد بذوالقرنین</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>همای وصل توام سایه بر سر اندازد</p></div>
<div class="m2"><p>رقیب ار نبود در میان غراب البین</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گهرفشان کن بر دوست سیف فرغانی</p></div>
<div class="m2"><p>که هست طبع و دلت در نظم را بحرین</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بدانک در دو جهان کعبه دل عشاق</p></div>
<div class="m2"><p>بدوست فخر کند چون بمصطفی حرمین</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بکوی عشق وطن ساز و رخت آنجا نه</p></div>
<div class="m2"><p>که دلگشاتر از آن جای نیست در کونین</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>فراز قله طور است، کسب کن دیدار</p></div>
<div class="m2"><p>کنار وادی قدسست خلع کن نعلین</p></div></div>