---
title: >-
    شمارهٔ ۲۴۸
---
# شمارهٔ ۲۴۸

<div class="b" id="bn1"><div class="m1"><p>بی‌تو دل خسته جان نمی‌خواهد</p></div>
<div class="m2"><p>جان بی‌رخ تو جهان نمی‌خواهد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جان می‌دهد و جهان خود آن تست</p></div>
<div class="m2"><p>دل وصل تو رایگان نمی‌خواهد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>وز آنکه درین بهات سودی نیست</p></div>
<div class="m2"><p>این بنده ترا زیان نمی‌خواهد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>من با تو نشستن آرزو دارم</p></div>
<div class="m2"><p>وین مجلس ما مکان نمی‌خواهد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آن را که حدیث تو به دل پیوست</p></div>
<div class="m2"><p>دیگر دهنش زبان نمی‌خواهد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زهر غم تو به جان خورم زیرا</p></div>
<div class="m2"><p>کآن لقمه جز این دهان نمی‌خواهد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مشتاق تو در جهان نمی‌گنجد</p></div>
<div class="m2"><p>سیمرغ تو آشیان نمی‌خواهد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از دنیی و آخرت تبرا کرد</p></div>
<div class="m2"><p>این ترک بگفت وآن نمی‌خواهد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هر تیر که عشق راست در جعبه</p></div>
<div class="m2"><p>جز ابروی تو کمان نمی‌خواهد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بر هرکه نشانه گشت تیرت را</p></div>
<div class="m2"><p>گر تیغ زنی امان نمی‌خواهد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>منویس و مگوی سیف فرغانی</p></div>
<div class="m2"><p>کین قصه دگر بیان نمی‌خواهد</p></div></div>