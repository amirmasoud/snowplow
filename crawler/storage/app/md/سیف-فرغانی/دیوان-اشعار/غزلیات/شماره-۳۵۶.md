---
title: >-
    شمارهٔ ۳۵۶
---
# شمارهٔ ۳۵۶

<div class="b" id="bn1"><div class="m1"><p>ای ز رویت پرتوی مرآفرینش را تمام</p></div>
<div class="m2"><p>از وجود تست سلک آفرینش را نظام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر ز مه خود را نقابی سازی ای خورشید روی</p></div>
<div class="m2"><p>ماه بر روی تو چون بر روی مه باشد غمام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با جمال تو ملاحت همچو شوری با نمک</p></div>
<div class="m2"><p>در حدیث تو حلاوت همچو معنی در کلام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مبتلای تو سلامت می دهد بر وی درود</p></div>
<div class="m2"><p>آشنای تو سعادت می کند بر وی سلام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خدمتی از من نیاید لایق حضرت که تو</p></div>
<div class="m2"><p>پادشاهان بندگان داری و آزادان غلام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در مقام شوق تو مست شراب عشق تو</p></div>
<div class="m2"><p>دارد از جز تو فراغت چون فرشته از طعام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بی سر و پایی که اندر راه عشقت زد قدم</p></div>
<div class="m2"><p>بر زمین نگرفت جا بر آسمان ننهاد گام</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بر در تو با دل پرآتش و چشم پر آب</p></div>
<div class="m2"><p>خویشتن را سوخته از پختن سودای خام</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون تویی همچون منی را کی شود حاصل بشعر</p></div>
<div class="m2"><p>چون کبوتر صید نتوان کرد عنقا را بدام</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چون مگس هرگز نیالاید دهان خود بشهد</p></div>
<div class="m2"><p>شوربختی کز لب شیرین تو خوش کرد کام</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>در چنین محراب گه با شعر تحت المنبری</p></div>
<div class="m2"><p>سیف فرغانی نزیبد این جماعت را امام</p></div></div>