---
title: >-
    شمارهٔ ۳۶۷
---
# شمارهٔ ۳۶۷

<div class="b" id="bn1"><div class="m1"><p>نام تو شنیدم رخ خوب تو ندیدم</p></div>
<div class="m2"><p>چون روی نمودی به از آنی که شنیدم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ازمن مبر ای دوست که بی صحبت تو عمر</p></div>
<div class="m2"><p>بادیست که ازوی به جز از گرد ندیدم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شمشیر مکن تیز بخون من مسکین</p></div>
<div class="m2"><p>کز دست تو غازی من ناکشته شهیدم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای هجر برو رخت بجای دگر افگن</p></div>
<div class="m2"><p>ای وصل بیا کز همه پیوند بریدم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بسیار بهر سو شدم اندر طلب تو</p></div>
<div class="m2"><p>نی ازتو گذشتم (من) ونی در تو رسیدم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گرچه زپیت اسب طلب تیز براندم</p></div>
<div class="m2"><p>نی ره سپری شد نه عنان باز کشیدم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کارم نپذیرد ز درغیر گشایش</p></div>
<div class="m2"><p>اکنون که درافتاد بدست تو کلیدم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خورشید رخ تو (چو) بدیدم بسعادت</p></div>
<div class="m2"><p>چون مهر شدم طالع وچون صبح دمیدم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بر پشت فلک رفتم ناگاه وچو خورشید</p></div>
<div class="m2"><p>هر ذره که بر روی زمین بود بدیدم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چون ذره در سایه کسم روی نمی دید</p></div>
<div class="m2"><p>امروز چو خورشید بهر جای پدیدم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گر هشت بهشتم بدهد دوست که بستان</p></div>
<div class="m2"><p>نستانم وچون دوزخ جویای مزیدم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>در عشق که از غصه کند پیر جوان را</p></div>
<div class="m2"><p>کامل شوم ارچند که ناقص چو مریدم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>از طبع چو آتش پس ازین آب سخن را</p></div>
<div class="m2"><p>چون جرعه چکانم چو می عشق چشیدم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>دی زاهد وعابد بدم وعاشقم امروز</p></div>
<div class="m2"><p>آن شد که کهن بود کنون خلق جدیدم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>سیفم که بریدم زهمه نسبت خود لیک</p></div>
<div class="m2"><p>در گفتن طامات چو عطار فریدم</p></div></div>