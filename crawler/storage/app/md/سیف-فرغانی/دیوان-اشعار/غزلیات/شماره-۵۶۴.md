---
title: >-
    شمارهٔ ۵۶۴
---
# شمارهٔ ۵۶۴

<div class="b" id="bn1"><div class="m1"><p>دی بامداد آن صنم آفتاب روی</p></div>
<div class="m2"><p>بر من گذشت همچو مه اندر میان کوی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خورشید در کشیده رخ از شرم طلعتش</p></div>
<div class="m2"><p>زآن سان که سایه درکشد از آفتاب روی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفتم مگر که نیت حمام کرده ای؟</p></div>
<div class="m2"><p>گفتا برو تو نیز بیا، با کسی مگوی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون ساعتی برآمد رفتم درآمدم</p></div>
<div class="m2"><p>من در درون و خلق ز بیرون بگفت و گوی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دیدم بناز تکیه زده بر کنار حوض</p></div>
<div class="m2"><p>همچون گلی که نوشکفد بر کران جوی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>می کرد آب را تن و اندام او خجل</p></div>
<div class="m2"><p>می زد شراب را لب او سنگ بر سبوی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>حمام را که هیچ نه رنگ و نه بوی بود</p></div>
<div class="m2"><p>از روی و موی او شده گل رنگ و مشک بوی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گیسوی مشکبار گشاده ز هم چنانک</p></div>
<div class="m2"><p>موی میانش گم شده اندر میان موی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>میدان عیش خالی و من برده بهر لعب</p></div>
<div class="m2"><p>چوگان دست سوی زنخدان همچو گوی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>من لابه کردم آن دم و او ناز چون نبود</p></div>
<div class="m2"><p>بیم رقیب و دهشت لالای تنگخوی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>او دید کآب دیده من گرم می رود</p></div>
<div class="m2"><p>مشتی گلم بداد که دست از دلت بشوی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>پس گفت سیف و اله و حیران چه مانده ای</p></div>
<div class="m2"><p>فرصت چو یافتی سخن خویشتن بگوی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>در بند وصل باش چو ناجسته یافتی</p></div>
<div class="m2"><p>این دولتی که یافت نگردد بجست و جوی</p></div></div>