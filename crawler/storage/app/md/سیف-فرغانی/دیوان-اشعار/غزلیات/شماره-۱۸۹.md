---
title: >-
    شمارهٔ ۱۸۹
---
# شمارهٔ ۱۸۹

<div class="b" id="bn1"><div class="m1"><p>عمر بی روی یار چون باشد</p></div>
<div class="m2"><p>بوستان بی بهار چون باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عشق با من چه می کند دانی</p></div>
<div class="m2"><p>آتش و مرغزار چون باشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چند گویی که باغمش چونی</p></div>
<div class="m2"><p>ملخ و کشتزار چون باشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بار بر سر گرفته ره درپیش</p></div>
<div class="m2"><p>رفته در پای خار چون باشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>من پیاده کمند در گردن</p></div>
<div class="m2"><p>هم ره من سوار چون باشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عالمی در وصال و من محروم</p></div>
<div class="m2"><p>عید و من روزه دار چون باشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>درچنین کار دورم از دل و صبر</p></div>
<div class="m2"><p>هیچ دانی که کار من چون باشد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شتری زیر بار در صحرا</p></div>
<div class="m2"><p>بگسلد ازقطار چون باشد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خود تو دانی که سیف فرغانی</p></div>
<div class="m2"><p>دور از روی یار چون باشد</p></div></div>