---
title: >-
    شمارهٔ ۱۱۳
---
# شمارهٔ ۱۱۳

<div class="b" id="bn1"><div class="m1"><p>در کوی عشق هرکه چومن سیم وزر نداشت</p></div>
<div class="m2"><p>هرگز درخت عشرت او برگ وبر نداشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بسیار حلقه بردر وصل بتان زدیم</p></div>
<div class="m2"><p>دیدیم هم کلید به جز سیم وزر نداشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفتم بکوی حیله زمانی فرو شوم</p></div>
<div class="m2"><p>رفتم سرای وصل درآن کوی در نداشت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای پادشاه حسن که همچون من فقیر</p></div>
<div class="m2"><p>سلطان سزای افسر عشق تو سر نداشت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هرکس که آفتاب رخت دید ناگهان</p></div>
<div class="m2"><p>هرگز چو سایه روی خود ازخاک برنداشت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گویی سپاه عشق تو چون بردلم گذشت</p></div>
<div class="m2"><p>بگذشت ازین خرابه که جای دگر نداشت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خود راچو شمع بر سر کویت بسوختم</p></div>
<div class="m2"><p>اندر شب فراق که گویی سحر نداشت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون صبح وصل دم زد وخورشید رو نمود</p></div>
<div class="m2"><p>این طالب مشاهده چشم نظر نداشت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آن مدعی بخنده نبیند جمال وصل</p></div>
<div class="m2"><p>کو چشم در فراق تو از گریه تر نداشت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گرد در تو در طیرانست روز و شب</p></div>
<div class="m2"><p>مرغ دل ارچه لایق آن اوج پرنداشت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گر تیغ بر سرش زنی آگاه نیست سیف</p></div>
<div class="m2"><p>هر کو زخود خبیر شد ازخود خبر نداشت</p></div></div>