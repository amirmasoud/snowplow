---
title: >-
    شمارهٔ ۴۳۲
---
# شمارهٔ ۴۳۲

<div class="b" id="bn1"><div class="m1"><p>ای لبت را خواص جان دادن</p></div>
<div class="m2"><p>عادتش بوسه روان دادن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بوسه تست جان و من مرده</p></div>
<div class="m2"><p>نیست آسان بمرده جان دادن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون کبوتر چرا نیاموزی</p></div>
<div class="m2"><p>دوست را طعمه از دهان دادن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بگه بوسه رو ترش چه کنی</p></div>
<div class="m2"><p>چو بخیلان بوقت نان دادن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با لب خشک من چه خوش باشد</p></div>
<div class="m2"><p>بوسه تر بر آن لبان دادن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دهنت هست لیک پیدا نیست</p></div>
<div class="m2"><p>چون خبر شاید از نهان دادن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>راست چون چشمه خضر نامیست</p></div>
<div class="m2"><p>زآنک نتوان ازو نشان دادن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بوسه یی گر دهی رضا نبود</p></div>
<div class="m2"><p>مر رقیب ترا در آن دادن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گربه خانه را دریغ آید</p></div>
<div class="m2"><p>بسگ کوی استخوان دادن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بوسه یی داذی و همی گویی</p></div>
<div class="m2"><p>که پشیمان شدم از آن دادن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>من چو منکر نیم یکی را صد</p></div>
<div class="m2"><p>باز واپس همی توان دادن</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چون مرا هیچ چیز دیگر نیست</p></div>
<div class="m2"><p>که توانم بدوستان دادن</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بوسهایی که اندرین غزلست</p></div>
<div class="m2"><p>بتو خواهم یکان یکان دادن</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>سیف فرغانی از زمین هرگز</p></div>
<div class="m2"><p>بوسه نتوان بر آسمان دادن</p></div></div>