---
title: >-
    شمارهٔ ۵۳۳
---
# شمارهٔ ۵۳۳

<div class="b" id="bn1"><div class="m1"><p>دلبرا حسن رخت می ندهد دستوری</p></div>
<div class="m2"><p>که بهم جمع شود عاشقی و مستوری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آمدن نزد تو بختم ننماید یاری</p></div>
<div class="m2"><p>رفتن از کوی تو عشقم ندهد دستوری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ترک رویی تو وبی هندوی خال سیهت</p></div>
<div class="m2"><p>زنگی چشم من از گریه شود کافوری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ذره از پرتو خورشید رخ روشن تو</p></div>
<div class="m2"><p>در شب تیره چو استاره نماید نوری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تو ازین دستم اگر گوش بمالی چو رباب</p></div>
<div class="m2"><p>من درین پرده بسی ناله کنم طنبوری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اگر از حال منت هیچ نمی سوزد دل</p></div>
<div class="m2"><p>تو که این حال نبودست ترا معذوری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر بنزدیک تو سهلست مرا طاقت نیست</p></div>
<div class="m2"><p>اگرم یک نفس از روی تو باشد دوری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بنده بیمار فراقست و نه قانون ویست</p></div>
<div class="m2"><p>که بوصل تو شفا خواهد ازین رنجوری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>رنج عشقت نکشم بر طمع راحت نفس</p></div>
<div class="m2"><p>پادشازاده ملکم نکنم مزدوری</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گر بشفتالوی سیب زنخش یابم دست</p></div>
<div class="m2"><p>هیچ شک نیست که به یابم ازین رنجوری</p></div></div>