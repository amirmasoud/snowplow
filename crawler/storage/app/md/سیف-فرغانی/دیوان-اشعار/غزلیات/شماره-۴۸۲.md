---
title: >-
    شمارهٔ ۴۸۲
---
# شمارهٔ ۴۸۲

<div class="b" id="bn1"><div class="m1"><p>ای پسته دهانت نرخ شکر شکسته</p></div>
<div class="m2"><p>وی زاده زبانت قدر گهر شکسته</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من طوطیم لب تو شکر بود که بینم</p></div>
<div class="m2"><p>در خدمت تو روزی طوطی شکر شکسته</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آنجا که چهره تو گسترده خوان خوبی</p></div>
<div class="m2"><p>گردد ز شرم رویت قرص قمر شکسته</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون بازگرد عالم گشتم بسی و آخر</p></div>
<div class="m2"><p>در دامت اوفتادم چون مرغ پرشکسته</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نقد روان جان را جوجو نثار کردم</p></div>
<div class="m2"><p>زین سان درست کاری ناید ز هر شکسته</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>من خود شکسته بودم ازلشکر غم تو</p></div>
<div class="m2"><p>این حمله بین که هجرت آورد بر شکسته</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>وز طعنهای مردم در حق خود چگویم</p></div>
<div class="m2"><p>هر کو رسید سنگی انداخت بر شکسته</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بارم محبت تست ای جان و وقت باشد</p></div>
<div class="m2"><p>کز بار خویش گردد شاخ شجر شکسته</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گرمن شکسته گشتم از عشق تو چه نقصان</p></div>
<div class="m2"><p>هیچ از شکستگی شد بازار زر شکسته؟</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>امشب زسنگ آهم در کارگاه گردون</p></div>
<div class="m2"><p>شد شیشهای انجم دریکدگر شکسته</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دی گفت عزت تو مارابکس چه حاجت</p></div>
<div class="m2"><p>من کس نیم چه دارم دل زین قدر شکسته</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>از هیبت خطابت شد سیف رادل ای جان</p></div>
<div class="m2"><p>همچون ردیف شعرش سرتا بسر شکسته</p></div></div>