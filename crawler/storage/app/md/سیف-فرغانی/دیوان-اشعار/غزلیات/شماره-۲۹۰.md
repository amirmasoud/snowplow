---
title: >-
    شمارهٔ ۲۹۰
---
# شمارهٔ ۲۹۰

<div class="b" id="bn1"><div class="m1"><p>ای دل ارزنده بعشقی منت جان برمگیر</p></div>
<div class="m2"><p>همچو مردان ترک کن دل را زجانان برمگیر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عشق چون در دل بود جان و جهان را ترک کن</p></div>
<div class="m2"><p>آب حیوان زاد داری بهر ره نان بر مگیر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر نعیم هر دو عالم یا بی اندر آستین</p></div>
<div class="m2"><p>جمع کن در دامن ترک وبیفشان بر مگیر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دوست گر از لعل خود حلوای رنگینت دهد</p></div>
<div class="m2"><p>دست را انگشت بشکن جز بدندان بر مگیر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زمزم اندر جنب کعبه تا بسر پر بهر تست</p></div>
<div class="m2"><p>در رهش گر تشنه گردی آب حیوان برمگیر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مرکب خاص است جان بر درگه سلطان عشق</p></div>
<div class="m2"><p>طوقش از گردن میفگن داغش ازران برمگیر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>توچو سلطانی بدولت کار سرهنگان مکن</p></div>
<div class="m2"><p>تو سلیمانی بر تبت بار دیوان برمگیر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اندر آن میدان که بینی تیر باران بلا</p></div>
<div class="m2"><p>چون تو در جوشن گریزی تیغ مردان برمگیر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>با وجود نازپرور دلق درویشی مپوش</p></div>
<div class="m2"><p>بر سری کش تاج نبود چتر سلطان برمگیر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تا برآن ماه خندان آب رو حاصل کنی</p></div>
<div class="m2"><p>هر شبی ازخاک کویش چشم گریان برمگیر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>پیر گشتی باده غفلت جوانانه منوش</p></div>
<div class="m2"><p>نیمه شهر صیام از ماه شعبان برمگیر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ای توانگر ما گدایانیم اندر کوی تو</p></div>
<div class="m2"><p>خوان لطف خود زپیش ما گدایان برمگیر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>تا درین ره ذره یی ازمن مرا باقی بود</p></div>
<div class="m2"><p>سایه ازکار من ای خورشید تابان برمگیر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>سیف فرغانی چو در دستت فتد درج سخن</p></div>
<div class="m2"><p>مهر سلطانیست بروی جز بفرمان برمگیر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>خرمن مه را اگر گردون که واختر جوست</p></div>
<div class="m2"><p>تو برو بگذر چو باد ودانه یی زآن برمگیر</p></div></div>