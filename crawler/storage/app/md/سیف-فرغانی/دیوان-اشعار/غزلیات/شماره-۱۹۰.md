---
title: >-
    شمارهٔ ۱۹۰
---
# شمارهٔ ۱۹۰

<div class="b" id="bn1"><div class="m1"><p>گر نور حسن نبود رو کی چو ماه باشد</p></div>
<div class="m2"><p>ور رنگ و بوی نبود گل چون گیاه باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اندر زمین چه جویی آنرا که از نکویی</p></div>
<div class="m2"><p>چون آسمانش بر رو خورشید و ماه باشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای دانه وجودت بی مغز جان چو کاهی</p></div>
<div class="m2"><p>گر جان مغز نبود دانه چو کاه باشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>صورت بجان معنی آراستست ورنی</p></div>
<div class="m2"><p>در خانهای شطرنج از چوب شاه باشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جانرا درین گریبان (دیگر) سریست با تو</p></div>
<div class="m2"><p>کین سر که هست پیدا آنرا کلاه باشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تو معتبر بعشقی ای مشتغل بصورت</p></div>
<div class="m2"><p>وین را ز روی معنی بیتی گواه باشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر نان خور نباشد بر روی خوان گردون</p></div>
<div class="m2"><p>چون پشت دیگ مه را کاسه سیاه باشد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر کم ز تار مویی از تست با تو باقی</p></div>
<div class="m2"><p>می دان که از تو تا او بسیار راه باشد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سر را بدست خدمت جاروب کوی او کن</p></div>
<div class="m2"><p>تا مر ترا درین ره آن پایگاه باشد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ای سیف عاشق او آفاق را بسوزد</p></div>
<div class="m2"><p>زآن آتشی که او را در دود آه باشد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>با صد هزار لشکر سلطان نباشد ایمن</p></div>
<div class="m2"><p>زآن صفدری که او را همت سپاه باشد</p></div></div>