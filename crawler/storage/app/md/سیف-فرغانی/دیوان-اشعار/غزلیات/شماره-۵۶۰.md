---
title: >-
    شمارهٔ ۵۶۰
---
# شمارهٔ ۵۶۰

<div class="b" id="bn1"><div class="m1"><p>چنین که تو سمری ای پسر بشیرینی</p></div>
<div class="m2"><p>بدور تو نکند کس نظر بشیرینی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شکرفشان لب تو دید و شد بجان مایل</p></div>
<div class="m2"><p>دلم بسوی تو همچون جگر بشیرینی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگر ز لعل تو بودی نشان در اول عهد</p></div>
<div class="m2"><p>چگونه نام گرفتی شکر بشیرینی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جهان بدور تو شیرین شود چو آب از قند</p></div>
<div class="m2"><p>چرا دهند بدور تو زر بشیرینی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رهی که تلخی هجر تو داشت کام دلش</p></div>
<div class="m2"><p>نداشت رغبت ازین بیشتر بشیرینی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ترا کنار گرفت و در آن میان ناگاه</p></div>
<div class="m2"><p>لب تو دید و فرو برد سر بشیرینی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دگر بمأمن اصلی خود چگونه رسد</p></div>
<div class="m2"><p>هر آن مگس که بیالود پر بشیرینی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز حسرت تو چو شمع از نگین اثر پذرفت</p></div>
<div class="m2"><p>چو دادم از لب لعلت خبر بشیرینی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دل مرا بتو از جمله میل بیشترست</p></div>
<div class="m2"><p>که میل طفل بود بیشتر بشیرینی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بپیش صادر و وارد حکایت تو کنم</p></div>
<div class="m2"><p>بسنده کرده ام از ماحضر بشیرینی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز شعر خود غزلی نزد یار بردم و گفت</p></div>
<div class="m2"><p>یکی بچشم رضا در نگر بشیرینی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چو می روی بسفر شعر بنده با خود بر</p></div>
<div class="m2"><p>که طبع میل کند در سفر بشیرینی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>جواب داد که با این همه شکر که مراست</p></div>
<div class="m2"><p>کی التفات کنم من دگر بشیرینی</p></div></div>