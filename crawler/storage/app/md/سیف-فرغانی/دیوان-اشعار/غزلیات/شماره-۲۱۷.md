---
title: >-
    شمارهٔ ۲۱۷
---
# شمارهٔ ۲۱۷

<div class="b" id="bn1"><div class="m1"><p>گر درد عشق در دل و در جان اثر کند</p></div>
<div class="m2"><p>در دردمند عشق چه درمان اثر کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در دین عاشقان که از اسلام برترست</p></div>
<div class="m2"><p>کفریست عشق تو که در ایمان اثر کند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در کنه وصف تو نرسد فهم چون منی</p></div>
<div class="m2"><p>حاشا که در کمال تو نقصان اثر کند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از جور عشق تو دل و جانم خراب شد</p></div>
<div class="m2"><p>در مملکت تعدی سلطان اثر کند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بعد از چنین ستم چه زیان دارد ار کنی</p></div>
<div class="m2"><p>عدلی که در ولایت ویران اثر کند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ترسم که روز وصل نیابی اثر ز من</p></div>
<div class="m2"><p>در من (اگر) فراق تو زین سان اثر کند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بسیار جهد کردم و خاطر بر آن گماشت</p></div>
<div class="m2"><p>تا خدمتی کنم که ترا آن اثر کند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کردم برین قرار که یک شب بسوز دل</p></div>
<div class="m2"><p>آهی کنم که در دلت ای جان اثر کند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شبها بگریه روز کنم در فراق تو</p></div>
<div class="m2"><p>تا صبح وصل در شب هجران اثر کند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>یک نکته از لب تو دلم را حیات داد</p></div>
<div class="m2"><p>در مرده آب چشمه حیوان اثر کند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گر تو بلطف یاد کنی عاشقانت را</p></div>
<div class="m2"><p>لطفت ز راه دور در ایشان اثر کند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>یوسف چو پیرهن ببشیر وصال داد</p></div>
<div class="m2"><p>بویش ز مصر تا در کنعان اثر کند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>اشعار سیف جمله بذکرت مرصعست</p></div>
<div class="m2"><p>در زر و سیم سکه شاهان اثر کند</p></div></div>