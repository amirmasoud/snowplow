---
title: >-
    شمارهٔ ۲۰
---
# شمارهٔ ۲۰

<div class="b" id="bn1"><div class="m1"><p>ای نبرده وصل تو روزی بمهمانی مرا</p></div>
<div class="m2"><p>هیچت افتد کز فراق خویش برهانی مرا؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در هلاک من چو هجرانت سبک دستی نکرد</p></div>
<div class="m2"><p>بر درت از بهر وصلست این گرانجانی مرا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من بپای جست و جوی از بهر تو برخاستم</p></div>
<div class="m2"><p>لطف باشد گر بگیری دست و بنشانی مرا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تو اگر آیی و گرنه من ترا خوانم مدام</p></div>
<div class="m2"><p>من چو نامه تا نمی آیم نمی خوانی مرا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر بهاری پیش ازین مانند بلبل درخزان</p></div>
<div class="m2"><p>بر نمی آمد نفس از بی گلستانی مرا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>می زنم بر بوی تو اکنون نوا چون عندلیب</p></div>
<div class="m2"><p>کاش بشکفتی گلی زین بلبل الحانی مرا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>من بآب صبر ازین گل شسته بودم پای روح</p></div>
<div class="m2"><p>دست دل انداخت اندر ورطه جانی مرا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>من چراغ مرده ام تو مجلس افروزی چو شمع</p></div>
<div class="m2"><p>بر دهانم نه لبی تا زنده گردانی مرا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آفتابی در شرف من همچو ماهم در خسوف</p></div>
<div class="m2"><p>روشنایی نیست بی آن روی نورانی مرا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از جهان بیزار گشتم چون بدیدم کوی دوست</p></div>
<div class="m2"><p>از عمارت کی کشد خاطر بویرانی مرا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>واله و حیران تو من بنده تنها نیستم</p></div>
<div class="m2"><p>خود کجا باشد باستقلال سلطانی مرا</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>در فراخای جهان از ازدحام عاشقان</p></div>
<div class="m2"><p>جای جولانی نماند از تنگ میدانی مرا</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ای ز صحت بی تو رنجوری،براحت کن بدل</p></div>
<div class="m2"><p>زحمتی کندر رهست از سیف فرغانی مرا</p></div></div>