---
title: >-
    شمارهٔ ۳۴۸
---
# شمارهٔ ۳۴۸

<div class="b" id="bn1"><div class="m1"><p>ای آنکه عشق تو دل جانست وجان دل</p></div>
<div class="m2"><p>مهرت نهاده لقمه غم در دهان دل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وصل تو قلب دل طلبد از میان جان</p></div>
<div class="m2"><p>ذکر تو گوش جان شنود از زبان دل</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عشقت چو صبح در افق جان کند اثر</p></div>
<div class="m2"><p>پر آفتاب وماه شود آسمان دل</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جانم بجام غم همه خون جگر خورد</p></div>
<div class="m2"><p>تا دل دمی از آن تو باشد توآن دل</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر عشق تو بود ز ازل در میان جان</p></div>
<div class="m2"><p>همچون ابد پدید نباشد کران دل</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون زر بسکه ملکان نام دارتست</p></div>
<div class="m2"><p>هر گوهری که طبع بر آرد زکان دل</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از رنگ وبوی تو دهدم همچو گل نشان</p></div>
<div class="m2"><p>هر غنچه یی که بشکفد از بوستان دل</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>این بیتها که بهر تو گفتیم هر یکی</p></div>
<div class="m2"><p>یک عشق نامه است بسر بر نشان دل</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ازهر چه آن بدوست تعلق نداشت سیف</p></div>
<div class="m2"><p>بگشای پای جان بگسل ریسمان دل</p></div></div>