---
title: >-
    شمارهٔ ۵۴۴
---
# شمارهٔ ۵۴۴

<div class="b" id="bn1"><div class="m1"><p>ای دوست بی تو ما را اندر جهان چه خوشی</p></div>
<div class="m2"><p>بی چون تو دلستانی در تن ز جان چه خوشی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پرسی ز من که بی من با خود چه بود حالت</p></div>
<div class="m2"><p>اندر جوار دشمن بی دوستان چه خوشی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مشتاق چون تویی را از غیر تو چه راحت</p></div>
<div class="m2"><p>جویای قوت جان را از آب و نان چه خوشی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفتی مرا که چونی در دامگاه دنیا</p></div>
<div class="m2"><p>مرغ آبی فلک را در خاکدان چه خوشی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گویی خوشست حالت با مردم زمانه</p></div>
<div class="m2"><p>با همرهان رهزن در کاروان چه خوشی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از جان خود دلی را بی وصل تو چه نیکی</p></div>
<div class="m2"><p>زآب دهن کسی را اندر دهان چه خوشی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>این جان نازنین را از جسم راحتی نه</p></div>
<div class="m2"><p>با همدگر دو ضد را در یک مکان چه خوشی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جویای حضرتت را از سیف نیست بهره</p></div>
<div class="m2"><p>آنرا که زنده باشد از مردگان چه خوشی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دنیا و آخرت را بهر تو ترک کردم</p></div>
<div class="m2"><p>بی تو درین چه راحت جز تو در آن چه خوشی</p></div></div>