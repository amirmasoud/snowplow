---
title: >-
    شمارهٔ ۵۱
---
# شمارهٔ ۵۱

<div class="b" id="bn1"><div class="m1"><p>تبارک الله از آن روی دلستان که تراست</p></div>
<div class="m2"><p>ز حسن و لطف کسی را نباشد آن که تراست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گمان مبر که شود منقطع بدادن جان</p></div>
<div class="m2"><p>تعلق دل از آن روی دلستان که تراست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بخنده ای بت بادام چشم شیرین لب</p></div>
<div class="m2"><p>شکر بریزد از آن پسته دهان که تراست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز جوهری که ترا آفریده اند ای دوست</p></div>
<div class="m2"><p>چگونه جسم بود آن تن چو جان که تراست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز راه چشم بدل می رسد خدنگ مژه</p></div>
<div class="m2"><p>مرا مدام ز ابروی چون کمان که تراست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چه خوش بود که چو من طوطیی شکر چیند</p></div>
<div class="m2"><p>ببوسه زآن لب لعل شکرفشان که تراست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بغیر ساغر می کش بر تو آبی هست</p></div>
<div class="m2"><p>ببوسه یی نرسد کس از آن لبان که تراست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اگر کمر بگشایی و زلف باز کنی</p></div>
<div class="m2"><p>میان موی تو گم گردد آن میان که تراست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو عندلیب مرا صد هزار دستانست</p></div>
<div class="m2"><p>بوصف آن دو رخ همچو گلستان که تراست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>صبا بیامد و آورد بوی تو، گفتم</p></div>
<div class="m2"><p>هزار جان بدهم من بدین نشان که تراست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بیا که هیچ کس امروز سیف فرغانی</p></div>
<div class="m2"><p>ندارد آب سخن اینچنین روان که تراست</p></div></div>