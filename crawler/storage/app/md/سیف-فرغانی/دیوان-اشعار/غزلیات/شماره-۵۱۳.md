---
title: >-
    شمارهٔ ۵۱۳
---
# شمارهٔ ۵۱۳

<div class="b" id="bn1"><div class="m1"><p>ای سازگار با همه با من نساختی</p></div>
<div class="m2"><p>با دوستدار خویش چو دشمن نساختی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو همچو جان لطیفی و من همچو تن کثیف</p></div>
<div class="m2"><p>ای جان ترا چه بود که با تن نساختی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای از زبان چرب سخن گفته همچو آب</p></div>
<div class="m2"><p>با آب شعر بنده چو روغن نساختی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بیتی نگفتم از پی سوز وصال تو</p></div>
<div class="m2"><p>کآن را بهجر نوحه و شیون نساختی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عاشق بسی بکشتی و خونش نهان بماند</p></div>
<div class="m2"><p>خوشه بسی درودی و خرمن نساختی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هرگز نتافتی چو مه اندر شب کسی</p></div>
<div class="m2"><p>کش همچو روز از آن رخ روشن نساختی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای معدن گهر نگذشتی بهیچ جای</p></div>
<div class="m2"><p>تا خاک آن چو گوهر معدن نساختی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در هیچ بقعه یی نشدی کآن مقام را</p></div>
<div class="m2"><p>میمون بسان وادی ایمن نساختی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>این گردن و سر از پی تیغ تو داشت سیف</p></div>
<div class="m2"><p>لیکن چو تیغ با سر و گردن نساختی</p></div></div>