---
title: >-
    شمارهٔ ۴۱۶
---
# شمارهٔ ۴۱۶

<div class="b" id="bn1"><div class="m1"><p>ای از خمار چشم تو آشوب در جهان</p></div>
<div class="m2"><p>وی لعل مدح گفته لبت را بصد زبان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای نو شده بعهد تو آیین دلبری</p></div>
<div class="m2"><p>وی برشکسته حسن تو بازار دلبران</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از دل نهم نشانه واز جان کنم سپر</p></div>
<div class="m2"><p>چون تیر غمزه را تو زابرو کنی کمان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>درسایه دو زلف تو پیدا نمی شود</p></div>
<div class="m2"><p>برآفتاب روی تو یک ذره آن دهان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جانست بوسه تو ومردم در انتظار</p></div>
<div class="m2"><p>تا کی بود که با تو رسد کار من بجان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اندر محیط عشق تو ای مرکز جمال</p></div>
<div class="m2"><p>کآن هست همچو دایره وهم بی کران</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>باید بسان نقطه سر خود گذاشتن</p></div>
<div class="m2"><p>پرگاروار اگر ننهی پای در میان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ما را ازآن برون درت جای کرده اند</p></div>
<div class="m2"><p>تا روز و شب چو پرده ببوسیم آستان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آنها که در عشق تو در دل نهفته اند</p></div>
<div class="m2"><p>همچون صدف شدند زغم جمله استخوان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بر هفت چرخ پای نهادند ویافتند</p></div>
<div class="m2"><p>زآن سوی شش جهت سر کوی ترا نشان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آب حیات راست چو آتش بسنگ در</p></div>
<div class="m2"><p>گویی که مضمرست درآن لعل درفشان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>در عالمی که وهم اشارت بدان کند</p></div>
<div class="m2"><p>نی دوست راست منزل ونی روح را مکان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ای بر بساط نظم شهی گشته همچو سیف</p></div>
<div class="m2"><p>معنی چو رخ نمود تو اسب سخن بران</p></div></div>