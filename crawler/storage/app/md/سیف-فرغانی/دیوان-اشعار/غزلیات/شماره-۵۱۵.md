---
title: >-
    شمارهٔ ۵۱۵
---
# شمارهٔ ۵۱۵

<div class="b" id="bn1"><div class="m1"><p>ای همه هستی مبر در خود گمان نیستی</p></div>
<div class="m2"><p>ترک سر گیر و بنه پا در جهان نیستی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نیستی نزدیک درویشان ز خود وارستنست</p></div>
<div class="m2"><p>مرگ صورت نیست نزد مانشان نیستی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کردمان و می کورا مسلم کی شود (کذا)</p></div>
<div class="m2"><p>داشتن داغ فنا بر گرد ران نیستی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در ره معنی میسر کشتگان عشق راست</p></div>
<div class="m2"><p>زیستن بی زحمت صورت بجان نیستی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هرچه هست اندر جهان گر دشمنت باشد، مخور</p></div>
<div class="m2"><p>از حوادث غم، چو هستی در امان نیستی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عشق شیر پنجه دار آمد چو دستش در شود</p></div>
<div class="m2"><p>گاو گردون را کشد در خر کمان نیستی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اندرین خاکست همچون آب حیوان ناپدید</p></div>
<div class="m2"><p>جای درویشان جان پرور بنان نیستی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جان عاشق فارغست از گفت و گوی هر دو کون</p></div>
<div class="m2"><p>حشو هستی را چه کار اندر میان نیستی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>حبذا قومی که گر خواهند چون نان بشکنند</p></div>
<div class="m2"><p>قرصه خورشید را بر روی خوان نیستی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>معتبر باشد ازیشان نزد جانان بذل جان</p></div>
<div class="m2"><p>چون سخا در فقر و جود اندر زمان نیستی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>جمله هستیهای عالم (را) که دل مشغول اوست</p></div>
<div class="m2"><p>لقمه یی ساز و بنه اندر دهان نیستی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>راه رو شب چون شتر تا خوش بیاسایی بروز</p></div>
<div class="m2"><p>ای جرس جنبان چو خر در کاروان نیستی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>سیف فرغانی دهان در بند و از دل گوش ساز</p></div>
<div class="m2"><p>نطق جان بشنو که گویا شد زبان نیستی</p></div></div>