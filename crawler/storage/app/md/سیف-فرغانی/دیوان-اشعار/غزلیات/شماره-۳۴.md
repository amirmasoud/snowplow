---
title: >-
    شمارهٔ ۳۴
---
# شمارهٔ ۳۴

<div class="b" id="bn1"><div class="m1"><p>ای گل روی تو برده رونق گلزارها</p></div>
<div class="m2"><p>در دل غنچه بسی حسن ترا اسرارها</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر بیاد روی تو آبی خورم در وقت مرگ</p></div>
<div class="m2"><p>بی گل از خاک رهی سر بر نیارد خارها</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گل که باشد پیش روی تو که او را چون گیاه</p></div>
<div class="m2"><p>بعد ازین آرند و بفروشند در بازارها</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با چلیپای سر زلفت که ناقوس اشکند</p></div>
<div class="m2"><p>نعره توحید خیزد زین پس از زنارها</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حسن شهر آشوب (تو) چون بر ولایت دست یافت</p></div>
<div class="m2"><p>سروران ملک را در پا رود دستارها</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کار من زهد است و توبه دادن مردم زمی</p></div>
<div class="m2"><p>گر می عشقت خورم توبه کنم زین کارها</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عشق داند نقش اغیار از دل عاشق سترد</p></div>
<div class="m2"><p>سکه را آتش تواند بردن از دینارها</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کس برون خانه محرم نیست سر عشق را</p></div>
<div class="m2"><p>در فرو بند این سخن می گوی با دیوارها</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر درختانرا بود از سر حلاج آگهی</p></div>
<div class="m2"><p>آنچه از وی می شنودی بشنوی از دارها</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گر بهار وصل خواهی سیف فرغانی برو</p></div>
<div class="m2"><p>همچو بلبل در خزان دم درکش از گفتارها</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دلبرا بی من مرو گر گویدت پور حسن</p></div>
<div class="m2"><p>خیز تا طوفی کنیم ای دوست در گلزارها</p></div></div>