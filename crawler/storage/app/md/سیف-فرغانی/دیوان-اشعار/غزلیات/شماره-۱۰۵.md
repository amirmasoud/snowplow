---
title: >-
    شمارهٔ ۱۰۵
---
# شمارهٔ ۱۰۵

<div class="b" id="bn1"><div class="m1"><p>چون کنم ای جان مرااز چون تو یاری چاره نیست</p></div>
<div class="m2"><p>غمخور عشقم مرااز غمگساری چاره نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گرچه عشقت چاره دارد ازهزاران همچو ما</p></div>
<div class="m2"><p>چاره ما کن که ماراازتو باری چاره نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پایدار آمد سر زلفت بدست دیگران</p></div>
<div class="m2"><p>آخر اندر چنگ ما از چند تاری چاره نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تن بزن در هجر او ای دل که اندر کوی عشق</p></div>
<div class="m2"><p>تا بدانی قدر وصل از انتظاری چاره نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از سر رحم ای رفیقان بنده را یاری کنید</p></div>
<div class="m2"><p>کین زپا افتاده را از دستیاری چاره نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>راحت دیدار جانان نیست بی رنج رقیب</p></div>
<div class="m2"><p>هرکجا باشد گلی آنجا ز خاری چاره نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بی گمان چون موکب سلطان جایی بگذرد</p></div>
<div class="m2"><p>دیده نظارگی رااز غباری چاره نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در شب وصلش بسی اندیشه کردم از فراق</p></div>
<div class="m2"><p>هر که می نوشید او را از خماری چاره نیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در جهان افسانه یی شد سیف فرغانی بعشق</p></div>
<div class="m2"><p>عاشقان هستند لیک از نامداری چاره نیست</p></div></div>