---
title: >-
    شمارهٔ ۵۴۶
---
# شمارهٔ ۵۴۶

<div class="b" id="bn1"><div class="m1"><p>مباد دل ز هوای تو یک زمان خالی</p></div>
<div class="m2"><p>که بی هوای تو دل تن بود ز جان خالی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همای عشق ترا هست آشیانه دلم</p></div>
<div class="m2"><p>مباد سایه این مرغ از آشیان خالی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز روی تو ز زمین تا بآسمان پرنور</p></div>
<div class="m2"><p>ز مثل تو ز مکان تا بلامکان خالی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خیال روی توام در دلست پیوسته</p></div>
<div class="m2"><p>ز مهر و ماه کجا باشد آسمان خالی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دلم ز معنی عشقت تهی نخواهد شد</p></div>
<div class="m2"><p>اجل اگر چه کند صورتم ز جان خالی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شراب عشق ترا عیب چیست تلخی هجر</p></div>
<div class="m2"><p>نواله تو نباشد ز استخوان خالی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>رسید عشق و ز اغیار گشت صافی دل</p></div>
<div class="m2"><p>پیمبر آمد و شد کعبه از بتان خالی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو مرغ سیر ز ذکر تو و حکایت غیر</p></div>
<div class="m2"><p>همیشه حوصله پر دارم و دهان خالی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>صفیر مرغ دلم ذکر تست در همه حال</p></div>
<div class="m2"><p>چو ماهی ارچه بود کامم از زبان خالی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>غم تو و دل من همچو جان و تن شده اند</p></div>
<div class="m2"><p>که می بمیرد اگر باشد این از آن خالی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مرا که دل ز هوای تو پر شدست چه غم</p></div>
<div class="m2"><p>اگر بمیرم و از من شود جهان خالی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چو لوح عشق تو محفوظ جان من گردد</p></div>
<div class="m2"><p>مرا قلم نبود ز آن پس از بنان خالی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بعاشقان تو دنیا خوشست و بی ایشان</p></div>
<div class="m2"><p>چو دوزخست که هست از بهشتیان خالی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بر آستان تو مانده است سیف فرغانی</p></div>
<div class="m2"><p>در تو نیست چو بازار از سگان خالی</p></div></div>