---
title: >-
    شمارهٔ ۳۲۸
---
# شمارهٔ ۳۲۸

<div class="b" id="bn1"><div class="m1"><p>گر خوش کنم دهان زلب دلستان خویش</p></div>
<div class="m2"><p>هرگز ز تن برون ننهم پای جان خویش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سلطان فقیر من شود ار تربیت کند</p></div>
<div class="m2"><p>من بنده را بلقمه نانی زخوان خویش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل صید دوست گشت چو بر مرغ روح (زد)</p></div>
<div class="m2"><p>یک تیر غمزه ز ابروی همچون کمان خویش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر جان چو سایه یی نرسد از همای عشق</p></div>
<div class="m2"><p>رو پیش سگ فگن تن چون استخوان خویش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از کوی او بدر نروم گرچه بنده را</p></div>
<div class="m2"><p>چون سگ بسنگ دور کند زآستان خویش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر زرد روی عشق که در دستم اوفتد</p></div>
<div class="m2"><p>در پاش مالم این رخ چون زعفران خویش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر خاک پای دوست کسی کو نهاد لب</p></div>
<div class="m2"><p>او مرده زنده کرد بآب روان خویش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کس بر بساط عشق مر آن شاه را نبرد</p></div>
<div class="m2"><p>او با کسی بماند که در باخت آن خویش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از بهر دوست دایره یی کن زجان ودل</p></div>
<div class="m2"><p>پس دوست را چو نقطه ببین در میان خویش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هرکو خورد زمشرب عشقش مدام آب</p></div>
<div class="m2"><p>بحری شود محیط ونبیند کران خویش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دلها بسوخت عشق وبجز دل نداشت جای</p></div>
<div class="m2"><p>جز عشق کس خراب نخواهد مکان خویش</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>در پیش جیش همت عاشق نایستاد</p></div>
<div class="m2"><p>سلطان ماه با سپه اختران خویش</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>قطب فلک بمذهب عشاق مرده است</p></div>
<div class="m2"><p>ای نعش بر جنازه فگن دختران خویش</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گر زین غزل سماع کند زهره، مشتری</p></div>
<div class="m2"><p>در پای چنگ او فگند طیلسان خویش</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>تا سیف ذکر دوست بگوید بکام خود</p></div>
<div class="m2"><p>بنهاد دوست در دهن او زبان خویش</p></div></div>