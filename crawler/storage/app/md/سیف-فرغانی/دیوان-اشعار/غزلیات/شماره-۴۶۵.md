---
title: >-
    شمارهٔ ۴۶۵
---
# شمارهٔ ۴۶۵

<div class="b" id="bn1"><div class="m1"><p>ای مشک و عنبر شمه یی از بوی تو</p></div>
<div class="m2"><p>مه پرتوی از آفتاب روی تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گل را برخسار تو نسبت می کنند</p></div>
<div class="m2"><p>رنگش خوش است اما ندارد بوی تو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در عشق بازی از تو چون من بیدقی</p></div>
<div class="m2"><p>شه می خوهد یعنی رخ نیکوی تو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ما تشنگان را سیل غم از سر گذشت</p></div>
<div class="m2"><p>ای آب حیوان قطره یی از جوی تو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر خاک هر در آب رو بفروختم</p></div>
<div class="m2"><p>تا نان خرم بهر سگان کوی تو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بالای تو ای شاخ طوبی زو خجل</p></div>
<div class="m2"><p>سرو (و)، بنفشه ترک شد از موی تو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دیوانه زنجیر دار دل نگر</p></div>
<div class="m2"><p>اندر کشاکش مانده با گیسوی تو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چشم تو کیش تیر مژگان پوشد</p></div>
<div class="m2"><p>گر چه کمان دارست از ابروی تو</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آن تیرها یک یک بلحظ جان شکر</p></div>
<div class="m2"><p>می افگند گه سوی من گه سوی تو</p></div></div>