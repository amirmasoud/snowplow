---
title: >-
    شمارهٔ ۱۱۸
---
# شمارهٔ ۱۱۸

<div class="b" id="bn1"><div class="m1"><p>منم آن کس که عشق یارم کشت</p></div>
<div class="m2"><p>زنده گشتم چوآن نگارم کشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گنج وصلش طلب همی کردم</p></div>
<div class="m2"><p>سنگ بر سر زدوچو مارم کشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من بی آب رستم از آتش</p></div>
<div class="m2"><p>چون ببادی چراغ وارم کشت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با سلیمان چه پنجه یارم کرد</p></div>
<div class="m2"><p>من که موری همی نیارم کشت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر شبی طول عمر او خواهم</p></div>
<div class="m2"><p>گرچه روزی هزار بارم کشت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عاشقان جمله کشتگان غمند</p></div>
<div class="m2"><p>منم آنکس که غم گسارم کشت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گرچه بشنود ناله زارم</p></div>
<div class="m2"><p>دوست رحمت نکرد وزارم کشت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>قوس ابروش صید دل می کرد</p></div>
<div class="m2"><p>زد یکی تیر ودر شکارم کشت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زنده وصل می کند امسال</p></div>
<div class="m2"><p>آنکه از هجر خویش پارم کشت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>این گلستان زباغ وصل مرا</p></div>
<div class="m2"><p>گل کنون می دهد که خارم کشت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>من مرده چو سیف فرغانی</p></div>
<div class="m2"><p>زنده اکنون شدم که یارم کشت</p></div></div>