---
title: >-
    شمارهٔ ۱۰۰
---
# شمارهٔ ۱۰۰

<div class="b" id="bn1"><div class="m1"><p>دل زمن برد آنکه جان را نزد او مقدارنیست</p></div>
<div class="m2"><p>یک جهان عشاق را دل برده ودلدار نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هرکه ترک جان کند آسان بدست آرد دلش</p></div>
<div class="m2"><p>گر بدست آید دل او ترک جان دشوار نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در بلای عشق او بی اختیار افتاده ام</p></div>
<div class="m2"><p>گرچه این مذهب ندارم کآدمی مختار نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفتم اندر کنج عزلت رو بدیوار آورم</p></div>
<div class="m2"><p>چون کنم در شهر ما یک خانه را دیوار نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دوست ازما بی نیاز و وصل مارا ناگزیر</p></div>
<div class="m2"><p>عشق با جان همنشین وصبر با دل یارنیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گرچو سگ ازکوی او نانی خوری اندک مدان</p></div>
<div class="m2"><p>ور سگان کوی اورا جان دهی بسیار نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>حضرت او منزل اصحاب کهفست ای عجب</p></div>
<div class="m2"><p>کاندر آن حضرت سگان را بارومارا بار نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ای زتو روزم سیه، شبها که مردم خفته اند</p></div>
<div class="m2"><p>جز سگ ومن هیچ کس در کوی تو بیدار نیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بار عشقت می کشم خوش زآنکه مر فرهاد را</p></div>
<div class="m2"><p>کوه کندن بر امید وصل شیرین بار نیست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گر سرت در پا نهم ور تیغ بر فرقم زنی</p></div>
<div class="m2"><p>از منت خوشنودی ای جان وزتوام آزار نیست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ور نگارستان شود پشت زمین از روی گل</p></div>
<div class="m2"><p>بلبل جان مرا جز روی تو گلزار نیست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>راست گفتی آزمودم با تو گشتم متفق</p></div>
<div class="m2"><p>(ای که گفتی هیچ مشکل چون فراق یار نیست)</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>سیف فرغانی چومن گر حاجتی داری بدوست</p></div>
<div class="m2"><p>دوست خود ناگفته داند، حاجت گفتار نیست</p></div></div>