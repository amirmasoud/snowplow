---
title: >-
    شمارهٔ ۵۵
---
# شمارهٔ ۵۵

<div class="b" id="bn1"><div class="m1"><p>عذر قدمت بسر توان خواست</p></div>
<div class="m2"><p>بوسی زلبت بزر توان خواست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گرچه تو کرم کنی ولیکن</p></div>
<div class="m2"><p>بی زر نتوان اگر توان خواست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>درکیسه خراج مصر باید</p></div>
<div class="m2"><p>تا ازلب تو شکر توان خواست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بوسی برتو چه قدر دارد</p></div>
<div class="m2"><p>دانم زتو اینقدر توان خواست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بالای تو سرو میوه دارست</p></div>
<div class="m2"><p>این میوه ازآن شجر توان خواست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نی نی غلطم درین حکایت</p></div>
<div class="m2"><p>ازسرو کجا ثمر توان خواست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ازمعدن اگر چه هیچ ندهند</p></div>
<div class="m2"><p>عیبی نبود گهر توان خواست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گنج از (تو) توقع است مارا</p></div>
<div class="m2"><p>آنرا زکدام در توان خواست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>وآنچ ازدر تو رسد بدرویش</p></div>
<div class="m2"><p>هرگز زکسی دگر توان خواست</p></div></div>