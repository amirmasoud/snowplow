---
title: >-
    شمارهٔ ۵۰۹
---
# شمارهٔ ۵۰۹

<div class="b" id="bn1"><div class="m1"><p>ایا خلاصه خوبان کراست درهمه دنیی</p></div>
<div class="m2"><p>چنین تنی همگی جان وصورتی همه معنی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غم تو دنیی ودینست نزد عاشق صادق</p></div>
<div class="m2"><p>که دل فروز چو دینی ودل ربای چو دنیی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>برآستان تو بودن مراست مجلس عالی</p></div>
<div class="m2"><p>بزیر پای تو مردن مراست پایه اعلی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگرچه نیست تویی ومنی میان من وتو</p></div>
<div class="m2"><p>منم منم بتو لایق تویی تویی بمن اولی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تو در مشاهده با دیگران ومن شده قانع</p></div>
<div class="m2"><p>زروی تو بخیال وز وصل تو بتمنی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خراب گشتن ملکست دل شکستن عاشق</p></div>
<div class="m2"><p>حصار کردن قدس است بهر کشتن یحیی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز زنده دل برباید رخ تو چون زر رنگین</p></div>
<div class="m2"><p>بمرده روح ببخشد لب تو چون دم عیسی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چراغ ماه نتابد بپیش شمع رخ تو</p></div>
<div class="m2"><p>شعاع مهر چه باشد بنزد نور تجلی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بدست دل قدم صدق سیف برسر کویت</p></div>
<div class="m2"><p>نهاده چون سر مجنون بر آستانه لیلی</p></div></div>