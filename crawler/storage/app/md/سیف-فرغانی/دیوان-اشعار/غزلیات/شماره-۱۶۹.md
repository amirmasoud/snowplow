---
title: >-
    شمارهٔ ۱۶۹
---
# شمارهٔ ۱۶۹

<div class="b" id="bn1"><div class="m1"><p>بر صفحه رخسار تو آنکس که نظر کرد</p></div>
<div class="m2"><p>خط تو چو اعراب دلش زیر و زبر کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آنرا که دمی دیده دل گشت گشاده</p></div>
<div class="m2"><p>چشم از همه در بست و بروی تو نظر کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ما را کمر تو ز میان تو نشان داد</p></div>
<div class="m2"><p>ما را سخن تو ز دهان تو خبر کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خباز مشیت نمک از روی تو درخواست</p></div>
<div class="m2"><p>از بهر فطیری که ازو قرص قمر کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون صورت تو معنی صد رنگ ندیدم</p></div>
<div class="m2"><p>تا دیده معنیم تماشای صور کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>با یوسف اگر چند فرو رفت مه حسن</p></div>
<div class="m2"><p>خورشید شد و سر ز گریبان تو بر کرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در کوی تو ما را نبود جای اقامت</p></div>
<div class="m2"><p>وآن فند نکردی که توانیم سفر کرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چندانکه توانم من گریان ز فراقت</p></div>
<div class="m2"><p>زآن لب که بیک خنده جهان پر ز شکر کرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بوسی خوهم و گر ندهی باز نیایم</p></div>
<div class="m2"><p>زین کدیه که کار من درویش چو زر کرد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>با بنده چنان نیستی ای دوست که بودی</p></div>
<div class="m2"><p>پیداست که در تو سخن دشمن اثر کرد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>در حسرت وصل تو دل سوخته بگریست</p></div>
<div class="m2"><p>آبش چو کم آمد مددش خون جگر کرد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>زین کار خلاصی نتوان یافت بتدبیر</p></div>
<div class="m2"><p>زین سیل بکشتی نتوانیم گذر کرد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>در خوابگه وصل تو یک روز نخسبد</p></div>
<div class="m2"><p>عاشق که شبی در غم هجرانت سحر کرد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>هرگز من و تو هر دو بدین حال نبودیم</p></div>
<div class="m2"><p>حسن تو ترا شکل و مرا شیوه دگر کرد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>لعلش بسخن سیف ترا شاد بسی داشت</p></div>
<div class="m2"><p>طوطی لبش پرورش تو بشکر کرد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>سیف این همه اشعار نه خود گفت اگر گفت</p></div>
<div class="m2"><p>مست این همه غوغا (نه) بخود کرد اگر کرد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>از وعده وصل تو دلم چون نشود شاد</p></div>
<div class="m2"><p>گویند بود میوه ز شاخی که زهر کرد</p></div></div>