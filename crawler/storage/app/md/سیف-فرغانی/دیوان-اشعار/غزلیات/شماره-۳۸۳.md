---
title: >-
    شمارهٔ ۳۸۳
---
# شمارهٔ ۳۸۳

<div class="b" id="bn1"><div class="m1"><p>ای سعادت مددی کن که بدان یار رسم</p></div>
<div class="m2"><p>لطف کن تا من دل داده بدلدار رسم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>او زمن بنده باین دیده خون بار رسد</p></div>
<div class="m2"><p>من ازآن دوست بیاقوت شکربار رسم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عندلیبم ز چمن دور زبانم بستست</p></div>
<div class="m2"><p>آن زمان در سخن آیم که بگلزار رسم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا بدان دوست رسم بگذرم از هر چه جزاوست</p></div>
<div class="m2"><p>بزنم بر سپه آنگه بسپهدار رسم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نخوهم ملک دو عالم چو ببینم رویش</p></div>
<div class="m2"><p>جنتم یاد نیاید چو بدیدار رسم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کس بدان یار برفتن نتوانست رسید</p></div>
<div class="m2"><p>برسانیدن آن یار بدان یار رسم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گرچه نارفته بدان دوست نخواهی پیوست</p></div>
<div class="m2"><p>تا نگویی که بدان دوست برفتار رسم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دوست پیغام فرستاد که در فرقت من</p></div>
<div class="m2"><p>صبر کن گرچه بسالی بتو یکبار رسم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گفتمش کی بود آن بار؟ معین کن!گفت:</p></div>
<div class="m2"><p>من گلم وقت بهاران بسر خار رسم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نعمت عشق مرا کز دگران کردم منع</p></div>
<div class="m2"><p>گرکنی شکر چو مردان بتو بسیار رسم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>توچو بیماری و، چون صحت راحت افزای</p></div>
<div class="m2"><p>رنج زایل کنم آنگه که ببیمار رسم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>از در باغ خودم میوه ده ای دوست که من</p></div>
<div class="m2"><p>نه چنان دست درازم که بدیوار رسم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>از درت گرچه گدایان بدرم واگردند</p></div>
<div class="m2"><p>چه شود گر من درویش بدینار رسم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>من برنگین سخنان ازتو نیابم بویی</p></div>
<div class="m2"><p>ور چه در گفتن طامات بعطار رسم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>سیف فرغانی در کار تویی مانع من</p></div>
<div class="m2"><p>پایم از دست بهل تا بسر کار رسم</p></div></div>