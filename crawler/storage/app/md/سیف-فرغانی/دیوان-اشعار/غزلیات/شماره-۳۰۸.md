---
title: >-
    شمارهٔ ۳۰۸
---
# شمارهٔ ۳۰۸

<div class="b" id="bn1"><div class="m1"><p>شبی از مجلس مستان برآمد ناله چنگش</p></div>
<div class="m2"><p>رسد از غایت تیزی بگوش زهره آهنگش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو بشنودم سماع او، نگردد کم نخواهد شد</p></div>
<div class="m2"><p>ز چشمم ژاله اشک و ز گوشم ناله چنگش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چگونه گلستان گوید کسی آن دلستانی را</p></div>
<div class="m2"><p>که گل با رنگ و بوی خود نموداریست از رنگش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>لب شیرین آن دلبر در آغشته است پنداری</p></div>
<div class="m2"><p>بآب چشمه حیوان شکر در پسته تنگش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کفی از خاک پای او بدست پادشا ندهم</p></div>
<div class="m2"><p>وگر چون (من) گدایی را دهد گوهر بهمسنگش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مشهر کردمی خود را چو شعر خویش در عالم</p></div>
<div class="m2"><p>بنام عاشقی او گر از من نامدی ننگش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فغان از سیف فرغانی برآمد ناگهان گویی</p></div>
<div class="m2"><p>بگوش عاشقان آمد سحرگه ناله چنگش</p></div></div>