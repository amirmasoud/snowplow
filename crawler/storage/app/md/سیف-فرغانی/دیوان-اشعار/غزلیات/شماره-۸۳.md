---
title: >-
    شمارهٔ ۸۳
---
# شمارهٔ ۸۳

<div class="b" id="bn1"><div class="m1"><p>ای که لعل لب تو آبخور جان منست</p></div>
<div class="m2"><p>تو اگر آن منی هر دوجهان آن منست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آب دریا ننشاند پس ازین شعله او</p></div>
<div class="m2"><p>گربآتش رسد این سوز که درجان منست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بتمنای وصال تو بسی سودا پخت</p></div>
<div class="m2"><p>طمع خام که اندر دل بریان منست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خود (تو) یک روز نگفتی که بدو مرهم وصل</p></div>
<div class="m2"><p>بفرستم، که دلش خسته هجران منست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دارم امید که منسوخ نگردد بفراق</p></div>
<div class="m2"><p>آیت رحمت وصل توکه در شان منست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا بوصلت نشوم جمع نگویم با کس</p></div>
<div class="m2"><p>آنچه در فرقت تو حال پریشان منست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>درد هجران تو بیماری مرگست مرا</p></div>
<div class="m2"><p>روی بنمای که دیدار تو درمان منست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>رخ چون لاله مپوش ازمن مسکین که منم</p></div>
<div class="m2"><p>عندلیب تو وروی تو گلستان منست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>یوسف من چو زمن دور بود چون یعقوب</p></div>
<div class="m2"><p>ملک اگر مصر بود کلبه احزان منست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ور مرا زلف چو چوگان تو در چنگ آید</p></div>
<div class="m2"><p>سربسر گوی زمین عرصه میدان منست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آدمی کو زغم عشق مرا منع کند</p></div>
<div class="m2"><p>گر فرشته است درین وسوسه شیطان منست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گر دهی تاج وگر تیغ زنی بر گردن</p></div>
<div class="m2"><p>سر سرتست که در قید گریبان منست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>سیف فرغانی در عشق چنین ماه تمام</p></div>
<div class="m2"><p>بکمال ار نرسم غایت نقصان منست</p></div></div>