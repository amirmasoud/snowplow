---
title: >-
    شمارهٔ ۷۹
---
# شمارهٔ ۷۹

<div class="b" id="bn1"><div class="m1"><p>دل تنگم و ز عشق توام بار بر دلست</p></div>
<div class="m2"><p>وز دست تو بسی چو مرا پای در گلست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شیرین تری ز لیلی و در کوی تو بسی</p></div>
<div class="m2"><p>فرهاد جان سپرده و مجنون بی دلست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر چه ز دوستی تو دیوانه گشته ام</p></div>
<div class="m2"><p>جز با تو دوستی نکند هرکه عاقلست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر من ببوسه مهر نهم بر لبت رواست</p></div>
<div class="m2"><p>شهد عقیق رنگ تو چون موم قابلست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در روز وصلت از شب هجرم غمست و من</p></div>
<div class="m2"><p>روزی نمی خوهم که شبش در مقابلست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دلرا مدام زاری از اندوه عشق تست</p></div>
<div class="m2"><p>اشتر بناله چون جرس از بار محملست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>روز وصال یار اجل عمر باقی است</p></div>
<div class="m2"><p>وقت وداع دوست شکر زهر قاتلست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بیند ترا در آینه جان خویشتن</p></div>
<div class="m2"><p>دلرا چو با خیال تو پیوند حاصلست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هرجا حدیث تست ز ما هم حکایتیست</p></div>
<div class="m2"><p>این شاهباز را سخنش با جلاجلست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>من چون درای ناله کنانم ولی چه سود</p></div>
<div class="m2"><p>محمول این شتر چو جرس آهنین دلست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>اشعار سیف گوهر در پای عشق تست</p></div>
<div class="m2"><p>این نظم در سراسر این بحر کاملست</p></div></div>