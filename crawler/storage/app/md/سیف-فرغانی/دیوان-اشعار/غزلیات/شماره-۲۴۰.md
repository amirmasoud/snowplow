---
title: >-
    شمارهٔ ۲۴۰
---
# شمارهٔ ۲۴۰

<div class="b" id="bn1"><div class="m1"><p>مشکل است این که کسی را به کسی دل برود</p></div>
<div class="m2"><p>مهرش آسان به درون آید و مشکل برود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل من مهر ترا گر چه به خود زود گرفت</p></div>
<div class="m2"><p>دیر باید که مرا نقش تو از دل برود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بحر عشقت گر ازین شیوه زند موج فراق</p></div>
<div class="m2"><p>کشتی من نه همانا که بساحل برود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بی وصال تو من مرده چراغم مانده</p></div>
<div class="m2"><p>همچو پروانه که شمعش ز مقابل برود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در عروسی جمال تو نمی دانم کس</p></div>
<div class="m2"><p>که ز پیرایه سودای تو عاطل برود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>با تو خوبی نتوان گفت و ندارم باور</p></div>
<div class="m2"><p>که بتبریز کسی آید و عاقل برود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آمن از فتنه حسن تو درین دوران نیست</p></div>
<div class="m2"><p>مگر آنکس که بشهر آید و غافل برود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>لایق بدرقه راه تو از هرچه مراست</p></div>
<div class="m2"><p>آب چشمی است که آن با تو بمنزل برود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خاک کویت همه گل گشت زآب چشمم</p></div>
<div class="m2"><p>چون گران بار جفاهای تو در گل برود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>عهد کرده است که در محمل تن ننشیند</p></div>
<div class="m2"><p>جانم آن روز که از کوی تو محمل برود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سیف فرغانی یارست ترا حاصل عمر</p></div>
<div class="m2"><p>چه بود فایده از عمر چو حاصل برود</p></div></div>