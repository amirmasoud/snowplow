---
title: >-
    شمارهٔ ۱۱۷
---
# شمارهٔ ۱۱۷

<div class="b" id="bn1"><div class="m1"><p>یار در شیرینی از شکر گذشت</p></div>
<div class="m2"><p>عشق در دلسوزی از آذر گذشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون کنم چون انگبین آگاه نیست</p></div>
<div class="m2"><p>زآنکه بی او شمع را بر سر گذشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>باد زلفش را پریشان کرد دی</p></div>
<div class="m2"><p>بوی او خوش شد چو بر عنبر گذشت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>می نیارم زآن رقیبان چو دیو</p></div>
<div class="m2"><p>گرد کوی آن پری پیکر گذشت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>منع را بر آستان خفته است سگ</p></div>
<div class="m2"><p>زآن نمی آرد گدا بردر گذشت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دی مرا پرسید یار از حال صبر</p></div>
<div class="m2"><p>گفتمش تو دیر زی کو در گذشت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آب چشمم بی تو بگذشت از سرم</p></div>
<div class="m2"><p>بی تو این دارم یکی از سرگذشت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>او مرا طالب من اورا عاشقم</p></div>
<div class="m2"><p>انتظار از حد شد و از مر گذشت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>راست چون لیلی و مجنون هر دو را</p></div>
<div class="m2"><p>عمر در سودای یکدیگر گذشت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>یار دی اشعار من می خواند، گفت</p></div>
<div class="m2"><p>پایه شعر تو از خوشتر گذشت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گفتم آری این عجب نبود ازآنک</p></div>
<div class="m2"><p>آب شیرین شد چوبر شکر گذشت</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>سیف فرغانی بیمن ذکر دوست</p></div>
<div class="m2"><p>گوهر نظمت بقدر از زر گذشت</p></div></div>