---
title: >-
    شمارهٔ ۱۷۶
---
# شمارهٔ ۱۷۶

<div class="b" id="bn1"><div class="m1"><p>دلبرا اندوه عشقت شادی جان آورد</p></div>
<div class="m2"><p>بهر بیماری دل درد تو درمان آورد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر نفس در کوی عشقت روی یوسف حسن تو</p></div>
<div class="m2"><p>صدچو من یعقوب را در بیت احزان آورد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سالها محزون نشینیم از پی آن تا بشیر</p></div>
<div class="m2"><p>ناگهان پیراهن یوسف بکنعان آورد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آفتاب روی تو چون در عرب پیدا شود</p></div>
<div class="m2"><p>از حبش عاشق بلال ار پارس سلمان آورد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همتی باید که عاشق را درین راه افگند</p></div>
<div class="m2"><p>رخش می باید که رستم را بمیدان آورد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دل فگند این نفس را اندر بلای عشق تو</p></div>
<div class="m2"><p>برسرکافر دعای نوح طوفان آورد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دل چو از شوقت بنالد دیده گردد اشک بار</p></div>
<div class="m2"><p>چون بغرد رعد آنگه ابر باران آورد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هیچ دنیای دوست را عشقت زتو آگه نکرد</p></div>
<div class="m2"><p>خضر کی بهر سکندر آب حیوان آورد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>برسر شاهان زند درویش با شمشیر عشق</p></div>
<div class="m2"><p>جنگ با شیران کند چون پیل دندان آورد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ملک جان ودل بغارت می رود درویش را</p></div>
<div class="m2"><p>کز بر سلطان حسنت عشق فرمان آورد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>عاشق تو گرچه درویش است زر بخشد چو جان</p></div>
<div class="m2"><p>نی زهر در همچو زنبیل گدا نان آورد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ماه با خرمن نشاید کز برای دانه یی</p></div>
<div class="m2"><p>همچو خوشه سر بزیر پای گاوان آورد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>آرزوی لعل خندانت که جان را شیر داد</p></div>
<div class="m2"><p>پیر را چون طفل پستان جوی گریان آورد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گنج گوهر چون زبان اندر دهان یابد کجا</p></div>
<div class="m2"><p>تنگ دستی چون من آن لب را بدندان آورد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>روز آخر شاد خیزد سیف فرغانی زخاک</p></div>
<div class="m2"><p>درغم عشقت اگر یک شب بپایان آورد</p></div></div>