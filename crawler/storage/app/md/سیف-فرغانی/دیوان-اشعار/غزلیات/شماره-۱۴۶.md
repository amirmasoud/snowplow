---
title: >-
    شمارهٔ ۱۴۶
---
# شمارهٔ ۱۴۶

<div class="b" id="bn1"><div class="m1"><p>درین سخن صفت حسن یار چون گنجد</p></div>
<div class="m2"><p>حساب بی عدد اندر شمار چون گنجد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>درین جهان که مرا بهره زوست دلتنگی</p></div>
<div class="m2"><p>چو عشق یار نگنجید یار چون گنجد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بعالمی که ز زلف و رخش اثر باشد</p></div>
<div class="m2"><p>درو دو رنگی لیل و نهار چون گنجد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو ماه اشرقت الارض بر جهان تابد</p></div>
<div class="m2"><p>در آسمان و زمین نور و نار چون گنجد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز شرم روی چو گلزار او عجب دارم</p></div>
<div class="m2"><p>که در فضای جهان نوبهار چون گنجد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ندای وصلش در گوش خلق چون آید</p></div>
<div class="m2"><p>فروغ رویش در روز بار چون گنجد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>من از شگرفی آن مه همیشه در عجبم</p></div>
<div class="m2"><p>که روز وصل مرا در کنار چون گنجد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>امیدم ارچه فراخست دست تنگی هست</p></div>
<div class="m2"><p>ببین که در کف من آن نگار چون گنجد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>منش نیامدم اندر نظر، در آن چشمی</p></div>
<div class="m2"><p>که سرمه راه نیابد غبار چون گنجد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>غم تو و دل مسکین سیف فرغانی!</p></div>
<div class="m2"><p>درین طویله در شاهوار چون گنجد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بکام خویش غمش جای ساخت در دل من</p></div>
<div class="m2"><p>وگرنه در دهن مور مار چون گنجد</p></div></div>