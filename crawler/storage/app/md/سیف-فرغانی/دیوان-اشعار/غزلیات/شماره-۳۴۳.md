---
title: >-
    شمارهٔ ۳۴۳
---
# شمارهٔ ۳۴۳

<div class="b" id="bn1"><div class="m1"><p>ز روی پرده برافگن که خلق را عیدست</p></div>
<div class="m2"><p>هلال ابروی تو همچو غره شوال</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>محیط لطف چو دریا مدام در موج است</p></div>
<div class="m2"><p>میان دایره روی تو ز نقطه خال</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رخ تو بر طبق روی تو بدان ماند</p></div>
<div class="m2"><p>که بر رخ گل سرخست روی لاله آل</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز نور چهره تو پرتوی مه و خورشید</p></div>
<div class="m2"><p>ز قوس ابروی تو گوشه یی کمان هلال</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بپیش تست مکدر چو سیل و تیره چو زنگ</p></div>
<div class="m2"><p>بروشنی اگر آیینه باشد آب زلال</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز خرقها بدر آیند چون کند تأثیر</p></div>
<div class="m2"><p>شراب عشق تو در صوفیان صاحب حال</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بوصف آن دهن و لب کجا بود قدرت</p></div>
<div class="m2"><p>مرا که لکنت عجزست در زبان مقال</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گدای کوی توام کی بود چو من درویش</p></div>
<div class="m2"><p>بنزد چون تو توانگر عزیز همچون مال</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز شاخ بید کجا باد زن کند سلطان</p></div>
<div class="m2"><p>وگر چه مروحه گردان ترک اوست شمال</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چو کوزه ز آب وصالت دهان من پر کن</p></div>
<div class="m2"><p>بقطره یی دو که لب خشک مانده ام چو سفال</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>رخ تو دید و بنالید سیف فرغانی</p></div>
<div class="m2"><p>چو گل شکفت مگو عندلیب را که منال</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بیا که در شب هجران تو بسی دیدیم</p></div>
<div class="m2"><p>«جزای آنکه نگفتیم شکر روز وصال »</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>هلال حسن بعهد رخ تو یافت کمال</p></div>
<div class="m2"><p>که هم جمال جهانی و هم جهان جمال</p></div></div>