---
title: >-
    شمارهٔ ۵۵۳
---
# شمارهٔ ۵۵۳

<div class="b" id="bn1"><div class="m1"><p>کیست درین دور پیر اهل معانی</p></div>
<div class="m2"><p>آنکه بهم جمع کرد عشق و جوانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قربت معشوق از اهل عشق توان یافت</p></div>
<div class="m2"><p>راه بود بی شک از صور بمعانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر تو چو شاهان برین بساط نشینی</p></div>
<div class="m2"><p>نیست ترا خانه در حدود مکانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در نفسی هرچه آن تست ببازی</p></div>
<div class="m2"><p>درند بی ملک هر دو کون نمانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نور امانت ز تو چنان بدرخشد</p></div>
<div class="m2"><p>کآتش برق از خلال ابر دخانی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خضر شوی در بقا و دانش و آنگاه</p></div>
<div class="m2"><p>آب در اجزای تو کند حیوانی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>علم تو آنجا رسد بدو که چو حلاج</p></div>
<div class="m2"><p>گویی اناالحق و نام خویش ندانی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>همچو عروسان بچشم سر تو پیدا</p></div>
<div class="m2"><p>رو بنمایند رازهای نهانی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جسم تو زآن سان سبک شود که تو گویی</p></div>
<div class="m2"><p>برد بدن از جوار روح گرانی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>فاتحه این حدیث دارد یک رنگ</p></div>
<div class="m2"><p>ست جهت را بنور سبع مثانی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هرکه مرو را شناخت نیز نپرداخت</p></div>
<div class="m2"><p>از عمل جان بعلمهای زبانی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گر خورد آب حیات زنده نگردد</p></div>
<div class="m2"><p>دل که ندارد بدو تعلق جانی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>من نرسیدم بدین مقام که گفتم</p></div>
<div class="m2"><p>گر برسی تو سلام من برسانی</p></div></div>