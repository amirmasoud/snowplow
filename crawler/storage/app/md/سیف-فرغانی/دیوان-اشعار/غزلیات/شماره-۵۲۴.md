---
title: >-
    شمارهٔ ۵۲۴
---
# شمارهٔ ۵۲۴

<div class="b" id="bn1"><div class="m1"><p>ای زآفتاب رویت مه برده شرمساری</p></div>
<div class="m2"><p>پیداست بر رخ تو آثار بختیاری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اندر بیان نگنجد واندر زبان نیاید</p></div>
<div class="m2"><p>از عشق آنچه دارم و از حسن آنچه داری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای نوش داروی جان اندر لبت نهفته</p></div>
<div class="m2"><p>بامر همی چنینم چون خسته می گذاری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>افغان و زاری من از حد گذشت بی تو</p></div>
<div class="m2"><p>گر چه بکرد بلبل بی گل فغان و زاری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>امیدوار وصلم از خود مبر امیدم</p></div>
<div class="m2"><p>صعبست نا امیدی بعداز امیدواری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون خاک اگر عزیزی بنشست بر در تو</p></div>
<div class="m2"><p>هر جا که رفت از آن پس چون زر ندید خواری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>من با چنین ارادت در تو رسم بشرطی</p></div>
<div class="m2"><p>کز بنده سعی باشد وز همت تو یاری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شیرین از آنی ای جان کز تلخی غم خود</p></div>
<div class="m2"><p>فرهادوار هر دم سوزی ز من بر آری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ای خوبتر ز لیلی هرگز مده چو مجنون</p></div>
<div class="m2"><p>دیوانه دلم را زین بند رستگاری</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گل را نمی توانم کردن بدوست نسبت</p></div>
<div class="m2"><p>ای گل بپیش جانان در پیش گل چو خاری</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هر جا که سیف باشد بستان اوست رویش</p></div>
<div class="m2"><p>چونست حال بوستان ای باد نوبهاری</p></div></div>