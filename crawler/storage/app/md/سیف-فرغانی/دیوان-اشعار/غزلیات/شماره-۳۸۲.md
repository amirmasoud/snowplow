---
title: >-
    شمارهٔ ۳۸۲
---
# شمارهٔ ۳۸۲

<div class="b" id="bn1"><div class="m1"><p>بروز وصل زهجران یار می ترسم</p></div>
<div class="m2"><p>اگر چه یافته ام گل زخار می ترسم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>درون قلعه مرا گرچه یار ودوست بسیست</p></div>
<div class="m2"><p>زدشمنان برون از حصار می ترسم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو روی دوست اگر چند حال من نیکوست</p></div>
<div class="m2"><p>ولی زچشم بد روزگار می ترسم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو باد فتنه برانگیخت گرد از هر سو</p></div>
<div class="m2"><p>برآن عزیز چو چشم از غبار می ترسم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>درین حدیقه که گل جا نکرد گرم درو</p></div>
<div class="m2"><p>زباد سرد برآن لاله زار می ترسم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بقطع حبل تعلق که محکم افتادست</p></div>
<div class="m2"><p>زحکم مبرم پروردگار می ترسم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هراس بنده زبازوی کام کار علیست</p></div>
<div class="m2"><p>گمان مبر که من از ذوالفقار می ترسم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پیادکان حشم خود باسب می نرسند</p></div>
<div class="m2"><p>زرخش رستم چابک سوار می ترسم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اگر چه رفت زمستان وشاخها گل کرد</p></div>
<div class="m2"><p>ولی زجارحه اندر بهار می ترسم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چو بحر وموج ببینم چگونه باشد حال</p></div>
<div class="m2"><p>که من زکشتی دریا گذار می ترسم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ببوسه از دهن دوست مهره تریاک</p></div>
<div class="m2"><p>بلب گرفته زدندان مار می ترسم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>از آنکه من غم او می خورم ندارم خوف</p></div>
<div class="m2"><p>ازین که غم نخورد غمگسار می ترسم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>درین میان زجدایی چو سیف فرغانی</p></div>
<div class="m2"><p>ورا گرفته ام اندر کنار می ترسم</p></div></div>