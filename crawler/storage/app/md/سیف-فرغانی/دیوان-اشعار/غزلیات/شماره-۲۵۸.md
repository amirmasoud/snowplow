---
title: >-
    شمارهٔ ۲۵۸
---
# شمارهٔ ۲۵۸

<div class="b" id="bn1"><div class="m1"><p>نسیم صبح پنداری ز کوی یار می‌آید</p></div>
<div class="m2"><p>به جان‌ها مژده می‌آرد که آن دلدار می‌آید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به صد اکرام می‌باید به استقبال او رفتن</p></div>
<div class="m2"><p>که بوی دوست می‌آرد ز کوی یار می‌آید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بدین خوبی و خوشبویی چنان پیدایی و گویی</p></div>
<div class="m2"><p>که سوی بنده چون صحت سوی بیمار می‌آید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به نیکی همچو شعر من در اوصاف جمال او</p></div>
<div class="m2"><p>به خوشی همچو ذکر او که در اشعار می‌آید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حکایت کرد کآن شیرین برای چون تو فرهادی</p></div>
<div class="m2"><p>شکر از پسته می‌بارد چو در گفتار می‌آید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز لشکرگاه حرب آن مه سوی میدان صلح می‌آید</p></div>
<div class="m2"><p>مظفر همچو سلطانی که از پیکار می‌آید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به دست حیله‌ای عاشق سزد کز سر قدم سازی</p></div>
<div class="m2"><p>گرت در جستن این گل قدم بر خار می‌آید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بدادم دنیی و گشتم گدای کوی سلطانی</p></div>
<div class="m2"><p>که درویشان کویش را ز سلطان عار می‌آید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خراباتیست عشق او که هر دم پیش مستانش</p></div>
<div class="m2"><p>زهادت چون گنه‌کاران به استغفار می‌آید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بسان دانه نارست اندر زعفران غلتان</p></div>
<div class="m2"><p>ز شوقش اشک رنگینم که بر رخسار می‌آید</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به نانی از در جانان رضا ده سیف فرغانی</p></div>
<div class="m2"><p>که همچون تو درین حضرت گدا بسیار می‌آید</p></div></div>