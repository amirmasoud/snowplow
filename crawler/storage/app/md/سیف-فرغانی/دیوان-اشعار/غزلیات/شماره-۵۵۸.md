---
title: >-
    شمارهٔ ۵۵۸
---
# شمارهٔ ۵۵۸

<div class="b" id="bn1"><div class="m1"><p>ایا بحسن رخت را لوای سلطانی</p></div>
<div class="m2"><p>بروی صورت زیبای حسن را جانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فراز کرسی افلاک شاه خورشیدست</p></div>
<div class="m2"><p>خلیفه رخ تو بر سریر سلطانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خطیست بر رخ تو از مداد نورالله</p></div>
<div class="m2"><p>نه از حروف مرکب، چو خط پیشانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>برآن زمین که تویی با تو مرد میدان نیست</p></div>
<div class="m2"><p>بگوی مهر ومه این آسمان چوگانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>من ازلطافت جان تو چون کنم تعبیر</p></div>
<div class="m2"><p>که جسم تو زصفا عالمیست روحانی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دل منست زعالم حواله گاه غمت</p></div>
<div class="m2"><p>حواله چند کنی گنج را بویرانی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>غم تو در دل از آثار فیض رحمانست</p></div>
<div class="m2"><p>چو خاطر ملکی در نفوس انسانی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>من از تعصب دین دشمن ترا کافر</p></div>
<div class="m2"><p>بگویم و نکند رخنه در مسلمانی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هوای چون تو پری روی در سر چومنی</p></div>
<div class="m2"><p>بدست دیو بود خاتم سلیمانی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مرا سخن زتو در دل همی شود پیدا</p></div>
<div class="m2"><p>که در درون من اندیشه وار پنهانی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>من ازتو چون مگس از خوان جدا نخواهم شد</p></div>
<div class="m2"><p>وگر چنانکه زنندم بسان سر خوانی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بسان نکته از یاد رفته بازآیم</p></div>
<div class="m2"><p>ورم بتیغ زبان چون سخن همی رانی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بشرح صفحه رویت کتابها سازم</p></div>
<div class="m2"><p>وگرچه زمن چون ورق بگردانی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>فدای (تو)چه نکردند جان گران جانان</p></div>
<div class="m2"><p>بهر بها که ترا می خرند ارزانی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>همی خوهم که مرا از جهانیان باشد</p></div>
<div class="m2"><p>فراغتی که تو داری زسیف فرغانی</p></div></div>