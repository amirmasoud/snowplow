---
title: >-
    شمارهٔ ۳۷۸
---
# شمارهٔ ۳۷۸

<div class="b" id="bn1"><div class="m1"><p>اگر شبی ز وصال تو کام برگیرم</p></div>
<div class="m2"><p>غذای جان ز لب تو مدام برگیرم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چه باشد از پس چندین هزار ناکامی</p></div>
<div class="m2"><p>اگر من از لب شیرینت کام برگیرم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دلم بسوخت درین غم بگوی تا از تو</p></div>
<div class="m2"><p>اگر مرا طمعی هست خام برگیرم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تو صید من نشوی ور کنم ز جان دانه</p></div>
<div class="m2"><p>بر آن نهادم دل را که دام برگیرم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مرا ز لعل لبت بوسه یی تمنا هست</p></div>
<div class="m2"><p>وگر چنانک نبخشی بوام برگیرم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مقیم کوی توام، دل نمی کند رغبت</p></div>
<div class="m2"><p>که خاطر از تو و رخت از مقام برگیرم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فغان مرغ سحر دوش خود مرا نگذاشت</p></div>
<div class="m2"><p>که حظ خویشتن از تو تمام برگیرم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شمایل تو یکی از یکی بدیع ترست</p></div>
<div class="m2"><p>بگوی تا دل خویش از کدام برگیرم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سخن ز شرم تو پوشیده تا بکی گویم</p></div>
<div class="m2"><p>حجاب شبهه ز روی کلام برگیرم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مرا چو لعل تو هر جزو گوهری گردد</p></div>
<div class="m2"><p>چو شمع اگر ز نگین تو نام برگیرم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مرا بکام رسان از لبان خود گرچه</p></div>
<div class="m2"><p>من این طمع نکنم کز تو کام برگیرم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مرا اگر تو مشرف کنی بدشنامی</p></div>
<div class="m2"><p>منش بجای هزاران سلام برگیرم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>کمینه بنده تو گفت سیف فرغانی</p></div>
<div class="m2"><p>که بار خدمت تو چون غلام برگیرم</p></div></div>