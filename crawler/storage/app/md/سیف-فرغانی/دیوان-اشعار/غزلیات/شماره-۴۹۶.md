---
title: >-
    شمارهٔ ۴۹۶
---
# شمارهٔ ۴۹۶

<div class="b" id="bn1"><div class="m1"><p>تو می روی و مرا نقش تست در دیده</p></div>
<div class="m2"><p>بیا که سیر نمی گردد از نظر دیده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از آن چراغ که در مجلس تو برمی شد</p></div>
<div class="m2"><p>بجای سرمه کشیدیم دوده در دیده</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از آن ز دیده مردم چو روح پنهانی</p></div>
<div class="m2"><p>که اهل دیدن روی تو نیست هر دیده</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر بیایی بر چشم ما نه آن قدمی</p></div>
<div class="m2"><p>که بار منت او می کشیم بر دیده</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>درون خانه چنان جای پاک نیست که تو</p></div>
<div class="m2"><p>قدم برو نهی ای نازنین مگر دیده</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مهی که شب همه بر روزن تو دارد چشم</p></div>
<div class="m2"><p>چو آفتاب ترا از شکاف در دیده</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هزار بار اگر بنگرم ز باریکی</p></div>
<div class="m2"><p>نمی شود ز میان تو جز کمر دیده</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مدام بر در تو عاشقان خشک لبند</p></div>
<div class="m2"><p>که جز بخون جگرشان نگشت تر دیده</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مقیم کوی تو روشن دلان بیدارند</p></div>
<div class="m2"><p>ترا بنور جمال تو هر سحر دیده</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دهان پسته مثال تو کس ندیده بچشم</p></div>
<div class="m2"><p>وگر چه گشته چو بادام سربسر دیده</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز گفته لاف مزن هیچ سیف فرغانی</p></div>
<div class="m2"><p>که نزد رهرو عشقست معتبر دیده</p></div></div>