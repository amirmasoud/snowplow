---
title: >-
    شمارهٔ ۴۷۴
---
# شمارهٔ ۴۷۴

<div class="b" id="bn1"><div class="m1"><p>ای مهر و مه رعیت و روی تو پادشاه</p></div>
<div class="m2"><p>پشت زمین ز روی تو چون آسمان ز ماه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جستم بسی و ره ننمودی مرا به خود</p></div>
<div class="m2"><p>گفتم بسی و داد ندادی به دادخواه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در معرض رخ تو نیارد کشید تیغ</p></div>
<div class="m2"><p>خورشید را گر از مه و انجم بود سپاه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از حسن تو به عشق در آویخت جان و دل</p></div>
<div class="m2"><p>بادش بزد به آب درآمیخت خاک راه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ما عاشقان صادق آن حضرتیم و هست</p></div>
<div class="m2"><p>هم آب چشم حجت و هم رنگ رو گواه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر یاد روز وصل تو ای دوست می‌کنیم</p></div>
<div class="m2"><p>هر شب هزار ناله و هر دم هزار آه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تو تاجدار حسنی و دستار بر سرت</p></div>
<div class="m2"><p>زیباترست از پر طاوس بر کلاه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از نیکویی رخ تو گل سرخ میکند</p></div>
<div class="m2"><p>بر عارض سپید تو آن خال رو سیاه</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خورشید کاهل کفر ورا سجده می‌کنند</p></div>
<div class="m2"><p>قبله ز روی تو کند اندر نمازگاه</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از لطف و حسن دایم در جمع نیکوان</p></div>
<div class="m2"><p>هستی چو ژاله بر گل و چون لاله در گیاه</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>در روی ما چنان به ارادت نظر کنی</p></div>
<div class="m2"><p>کآهو سوی سگان شکاری کند نگاه</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>لطفی بکن ز قهر خودم در پناه گیر</p></div>
<div class="m2"><p>کز قهر تو به لطف تو دارم گریزگاه</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گفتم به دوست لابه کنم وز سر حضور</p></div>
<div class="m2"><p>خواهم قبول طاعت و آمرزش گناه</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>همت به نعره بانگ برآورد و گفت سیف</p></div>
<div class="m2"><p>از دوست غیر دوست دگر حاجتی مخواه</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ای ره به دوست برده چنین از رهت که برد؟</p></div>
<div class="m2"><p>آن سرو نازنین که چه خوش می‌رود براه</p></div></div>