---
title: >-
    شمارهٔ ۲۳۵
---
# شمارهٔ ۲۳۵

<div class="b" id="bn1"><div class="m1"><p>هرچند لطف عادت آن نازنین بود</p></div>
<div class="m2"><p>با جمله مهر ورزد وبا ما بکین بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رویش درآب آینه بیند نظیر خویش</p></div>
<div class="m2"><p>حد جمال وغایت خوبی همین بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مانند رنگ داده صباغ صنع نیست</p></div>
<div class="m2"><p>صورت که نقش کرده نقاش چین بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>معنی لعل وقیمت یاقوت کی دهند</p></div>
<div class="m2"><p>مر شمع را که صورت نقش از نگین بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در دلبران شمایل آن دلستان کجاست</p></div>
<div class="m2"><p>درخاک کی لطافت ماء معین بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در گیسوی بتان نبود تاب زلف یار</p></div>
<div class="m2"><p>در ریسمان چه قوت حبل المتین بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای دوست با رخ تو چه باشد چراغ ماه</p></div>
<div class="m2"><p>شب را چه روشنایی نور مبین بود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>لعل لب تو گنج گهر را بها شکست</p></div>
<div class="m2"><p>خرمهره را چه قیمت در ثمین بود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>برخاستن زجان وجهان از لوازمست</p></div>
<div class="m2"><p>هر کس خوهد که باتو دمی همنشین بود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گوید خرد که بهر کسی ترک جان مکن</p></div>
<div class="m2"><p>این رسم عشق باشد وآن حکم دین بود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کس نیست در زمانه که باتو بنیکویی</p></div>
<div class="m2"><p>چون مه بآفتاب بخوبی قرین بود</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گردون ندید ومادر ایام هم نزاد</p></div>
<div class="m2"><p>آنرا که حسن وشکل وشمایل چنین بود</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چندانکه سیف گفت سخن کرد ذکر تو</p></div>
<div class="m2"><p>هرجا که نحل شمع نهاد انگبین بود</p></div></div>