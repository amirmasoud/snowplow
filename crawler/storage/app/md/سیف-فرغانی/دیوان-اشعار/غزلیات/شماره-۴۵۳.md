---
title: >-
    شمارهٔ ۴۵۳
---
# شمارهٔ ۴۵۳

<div class="b" id="bn1"><div class="m1"><p>ای جمال تو رشک حورالعین</p></div>
<div class="m2"><p>روح را کوی تست خلد برین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا پدید آمد آفتاب رخت</p></div>
<div class="m2"><p>شرمسارست آسمان ز زمین</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گرچه زلف تو داده یاری کفر</p></div>
<div class="m2"><p>روی خوب تو کرده پشتی دین</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>وصل ما خود کی اتفاق افتد</p></div>
<div class="m2"><p>تو توانگر بحسن و من مسکین</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دور خوبی چو روزگار گلست</p></div>
<div class="m2"><p>تکیه بر نیکویی مکن چندین</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در فراقت همی کنم بسخن</p></div>
<div class="m2"><p>این دل بی قرار را تسکین</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>همچو خسرو که کرد روزی چند</p></div>
<div class="m2"><p>بشکر دفع غصه شیرین</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر بگریم من از فراقت زار</p></div>
<div class="m2"><p>ور بنالم من از جفات حزین</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دل تهی می کنم ز غصه تو</p></div>
<div class="m2"><p>هست اشکم بهانه یی رنگین</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>عاشق تو بخلق دل ندهد</p></div>
<div class="m2"><p>میل نکند ملک بعجل سمین</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چند گویی که هست جان و دلم</p></div>
<div class="m2"><p>از جفای رقیب او غمگین</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>سر جور شکر فروشت نیست</p></div>
<div class="m2"><p>ای مگس خیز و با شکر منشین</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>عشق در جان سیف فرغانیست</p></div>
<div class="m2"><p>چون در اجزای خاک ماء معین</p></div></div>