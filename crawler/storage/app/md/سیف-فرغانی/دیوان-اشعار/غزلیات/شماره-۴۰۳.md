---
title: >-
    شمارهٔ ۴۰۳
---
# شمارهٔ ۴۰۳

<div class="b" id="bn1"><div class="m1"><p>مرا گر دولتی باشد که روزی با تو بنشینم</p></div>
<div class="m2"><p>ز لبهای تو می نوشم ز رخسار تو گل چینم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شبی در خلوت وصلت چو بخت خود همی خفتم</p></div>
<div class="m2"><p>اگر اقبال بنهادی ز زانوی تو بالینم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مرا گر بی توام غم نیست از هجران و تنهایی</p></div>
<div class="m2"><p>بهر چیزی که روی آرم درو روی تو می بینم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر چون گل خس و خاری گزینی بر چو من یاری</p></div>
<div class="m2"><p>من آن بلبل نیم باری که گل را بر تو بگزینم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خراج جان و دل خواهی ترا زیبد که سلطانی</p></div>
<div class="m2"><p>زکات حسن اگر بدهی بمن باری که مسکینم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جهانی شاد و غمگین اند از هجر و وصال تو</p></div>
<div class="m2"><p>بوصلم شادمان گردان که از هجر تو غمگینم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دلم ببرید چون فرهاد عمری کوه اندوهت</p></div>
<div class="m2"><p>مکن ای خسرو خوبان طمع در جان شیرینم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز کین و مهر دلداران سخن رانند با یاران</p></div>
<div class="m2"><p>تو با من کین بی مهری و با تو مهر بی کینم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نظر کردم بتو خوبان بیفتادند از چشمم</p></div>
<div class="m2"><p>چو مه دیدم کجا ماند دگر پروای پروینم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مسلمان آن زمان گردد که گوید سیف فرغانی</p></div>
<div class="m2"><p>که من بی وصل تو بی جان و بی عشق تو بی دینم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چنان افتاده عشقت شدم جانا که چون سعدی</p></div>
<div class="m2"><p>« ز دستم بر نمی آید که یکدم بی تو بنشینم »</p></div></div>