---
title: >-
    شمارهٔ ۱۵۰
---
# شمارهٔ ۱۵۰

<div class="b" id="bn1"><div class="m1"><p>کسی کز دل سخن گوید دمش چون جان اثر دارد</p></div>
<div class="m2"><p>بپرس از وی که صاحب دل زعلم جان خبر دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ازآن معدن طلب کن زر که باشد اندرو وجوهر</p></div>
<div class="m2"><p>گل ومیوه زشاخی جو که برگ سبز وتر دارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تو هر صورت نمایی را مدان از اهل این معنی</p></div>
<div class="m2"><p>که نی هر بحر مروارید ونی هر نی شکر دارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>درین بازار قلابان بهر جانب نظر می کن</p></div>
<div class="m2"><p>زصرافی حذر می کن که روی اندود زر دارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو آیینه دلی داری وبروی زنگ تو بر تو</p></div>
<div class="m2"><p>بدست آور ده آیینه که از وی زنگ بردارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بوقت صید مرغابی گر او را درهوا یابی</p></div>
<div class="m2"><p>نه شاهینی کند موری که همچون تیر پر دارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>درین شهر ار کسی بینی درین مردم بسی بینی</p></div>
<div class="m2"><p>کسی کوبر سری دوروی و بر گردن دو سر دارد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زحال عاشقان او عبارت کردمی نتوان</p></div>
<div class="m2"><p>بلفظ وحرف درناید معانی کین صور دارد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بقوت همت عاشق برآرد کوه را ازجا</p></div>
<div class="m2"><p>چو آهن تیز شد در سنگ اثر دارد اثر دارد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بلای عاشقی صعبست یا بگریز یا خود را</p></div>
<div class="m2"><p>چو هیزم بشکن ای مروان که بو مسلم تبر دارد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>وگر زآن مخزن شاهی ترا دادند آگاهی</p></div>
<div class="m2"><p>همی کن کتم اسرارش که کشف سر خطیر دارد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>زجهال بنی آدم نه سر روح را محرم</p></div>
<div class="m2"><p>بسی تهمت کشد مریم که چون عیسی پسر دارد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بر معشوق معیوبی بر عشاق محجوبی</p></div>
<div class="m2"><p>بجان این رمز را بشنو دلت گوشی اگر دارد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گرت درخانه کاهی هست گو یکجو بخود گیرد</p></div>
<div class="m2"><p>ورت درکیسه کوهی هست گو زر برکمر دارد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>درین صف سیف فرغانیست خون خود هدر کرده</p></div>
<div class="m2"><p>که این شمشیر تیز و او نه جوشن نی سپر دارد</p></div></div>