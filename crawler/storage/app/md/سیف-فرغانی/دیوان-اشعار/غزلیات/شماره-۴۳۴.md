---
title: >-
    شمارهٔ ۴۳۴
---
# شمارهٔ ۴۳۴

<div class="b" id="bn1"><div class="m1"><p>در شهر بحسن تو رویی نتوان دیدن</p></div>
<div class="m2"><p>از دل نشود پنهان روی تو بپوشیدن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من در عجبم از تو زیرا که ندیدستم</p></div>
<div class="m2"><p>از ماه سخن گفتن وز سرو خرامیدن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هنگام بهار ای جان در باغ چه خوش باشد</p></div>
<div class="m2"><p>بر یاد تو می خوردن بر بوی تو گل چیدن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با پسته خندانت گر توبه کند شاید</p></div>
<div class="m2"><p>هم قند ز شیرینی هم پسته ز خندیدن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در ملکش اگر بودی مانند تو شیرینی</p></div>
<div class="m2"><p>فرهاد شدی خسرو در سنگ تراشیدن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در مذهب عشاقت آنراست مسلمانی</p></div>
<div class="m2"><p>کورا نبود دینی جز دوست پرستیدن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کردیم بسی کوشش تا دوست بدست آید</p></div>
<div class="m2"><p>چون بخت مدد نکند چه سود ز کوشیدن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا دیده خود بینت با غیر نظر دارد</p></div>
<div class="m2"><p>گر چشم ز جان سازی او را نتوان دیدن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از تیغ جفای او اندیشه مکن ای سیف</p></div>
<div class="m2"><p>تأثیر ظفر نبود از معرکه ترسیدن</p></div></div>