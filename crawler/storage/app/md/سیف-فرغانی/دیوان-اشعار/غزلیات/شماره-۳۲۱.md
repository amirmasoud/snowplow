---
title: >-
    شمارهٔ ۳۲۱
---
# شمارهٔ ۳۲۱

<div class="b" id="bn1"><div class="m1"><p>تا چه معنیست در آن روی جهان آرایش</p></div>
<div class="m2"><p>که دلم برد بدان صورت جان افزایش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون جهان سربسر آرایش از آن رو دارد</p></div>
<div class="m2"><p>بچه آراسته شد روی جهان آرایش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر خود این جامه چو دراعه غنچه بدرم</p></div>
<div class="m2"><p>از تن چون گل و پیراهن گل پیمایش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آن بت پسته دهن را لب همچون یاقوت</p></div>
<div class="m2"><p>شکرینست و منم طوطی شکر خایش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون بوصلش طمع خام تو ناپخته بماند</p></div>
<div class="m2"><p>ای دل سوخته تا چند پزی سودایش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>لاله را در چمن و غنچه گل را در باغ</p></div>
<div class="m2"><p>مشک در جیب کند طره عنبر زایش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون کسی را نبود دیده معنی روشن</p></div>
<div class="m2"><p>ای تن تو همه جان صورت خود منمایش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بنده از دست جفای تو بجایی نرود</p></div>
<div class="m2"><p>که سر کوی تو بندیست گران برپایش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ذره یی را که رخ روشن تو بر وی تافت</p></div>
<div class="m2"><p>آفتابی نتواند که بگیرد جایش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>یک کف از خاک سر کوی تو وز عاشق جان</p></div>
<div class="m2"><p>زر بمزدور ده و کار همی فرمایش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سیف فرغانی از تست چو جام از باده</p></div>
<div class="m2"><p>که بکلی همه رنگ تو گرفت اجزایش</p></div></div>