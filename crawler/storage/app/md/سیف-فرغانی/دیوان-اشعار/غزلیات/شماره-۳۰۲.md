---
title: >-
    شمارهٔ ۳۰۲
---
# شمارهٔ ۳۰۲

<div class="b" id="bn1"><div class="m1"><p>اگرچه راه بسی بود تا من از آتش</p></div>
<div class="m2"><p>دلم بسوخت ز عشق تو چون تن از آتش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز سوز عشق تو در سینه چو کوره من</p></div>
<div class="m2"><p>دلم گرفت حرارت چو آهن از آتش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز باد دم شودش سیم و زر روان چون آب</p></div>
<div class="m2"><p>اثرپذیر چو شد خاک معدن از آتش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز باد سرد کز آن کوی آورد خاکی</p></div>
<div class="m2"><p>چو آب گرم فتد جوش در من از آتش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بعشق دانه دل را چو کاه داد بباد</p></div>
<div class="m2"><p>دلم اگرچه نگه داشت خرمن از آتش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نبود ایمن از آفات، در گریخت بعشق</p></div>
<div class="m2"><p>ندیده ام که کند عود مأمن از آتش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو بستدش ز جهان و بخود گرفتش عشق</p></div>
<div class="m2"><p>چو ماهی است که کردست مسکن از آتش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اگرچه شمع سر اندر دهان گاز نهاد</p></div>
<div class="m2"><p>گرفت نور و برافراخت گردن از آتش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دل مجرد از آفات غیر محفوظست</p></div>
<div class="m2"><p>که بی فتیل سلیم است روغن از آتش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز کار عشق بتن رنج می رسد آری</p></div>
<div class="m2"><p>مدام دود بود قسم گلخن از آتش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نصیب دیده من از رخ تو حرمانست</p></div>
<div class="m2"><p>همیشه دود خورد چشم روزن از آتش</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بنور عشق کند حسن همچو گلشن دل</p></div>
<div class="m2"><p>بلی چراغ کند خانه روشن از آتش</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بعشق راه توان یافت سوی تو که کلیم</p></div>
<div class="m2"><p>ببرد راه بوادی ایمن از آتش</p></div></div>