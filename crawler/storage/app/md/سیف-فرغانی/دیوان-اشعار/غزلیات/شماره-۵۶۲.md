---
title: >-
    شمارهٔ ۵۶۲
---
# شمارهٔ ۵۶۲

<div class="b" id="bn1"><div class="m1"><p>ای گرفته زحدیث تو جهان شیرینی</p></div>
<div class="m2"><p>از تو در کام دل ودر لب جان شیرینی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شکر و شهد مرا بهر غذای دل وجان</p></div>
<div class="m2"><p>در لب لعل تو دادند نشان شیرینی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>راست چون لقمه غم کام دلم تلخ کند</p></div>
<div class="m2"><p>گر مرا جز لبت آید بدهان شیرینی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چند در حسرت خود کام کنی تلخ مرا</p></div>
<div class="m2"><p>از لب چون شکر خود بچشان شیرینی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا حدیث لب لعلت بزبان آوردم</p></div>
<div class="m2"><p>همچو آب از سخنم گشت روان شیرینی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر چه در عهد تو بسیار نکویان هستند</p></div>
<div class="m2"><p>روح لیسیده زلبشان بزبان شیرینی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کس چو تو نیست ازیراکه برابر نبود</p></div>
<div class="m2"><p>گر چه با تره بود بر سر خوان شیرینی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از سر لطف بمن بنده نظر کن یکبار</p></div>
<div class="m2"><p>بمگس گرچه نباشد نگران شیرینی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>عاشقان را زتو هر لحظه امیدی دگرست</p></div>
<div class="m2"><p>درد سر چند کشد ازمگسان شیرینی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بر سر خوان که برو شور و ترش خورده شود</p></div>
<div class="m2"><p>چون مسلم بدر آید زمیان شیرینی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گرچه رو ترش کنی و سخنت، باکی نیست</p></div>
<div class="m2"><p>که بسازند زغوره بزمان شیرینی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ور بجان از لب تو بوسه خرم گویم شکر</p></div>
<div class="m2"><p>که ببازار شما نیست گران شیرینی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>سیف فرغانی از ذکر لب چون شکرش</p></div>
<div class="m2"><p>تو درین شعر بآفاق رسان شیرینی</p></div></div>