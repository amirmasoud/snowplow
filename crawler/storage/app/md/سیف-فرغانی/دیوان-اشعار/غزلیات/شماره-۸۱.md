---
title: >-
    شمارهٔ ۸۱
---
# شمارهٔ ۸۱

<div class="b" id="bn1"><div class="m1"><p>بس کور دلست آنکه به جز تو نگرانست</p></div>
<div class="m2"><p>یا خود نظرش با تو ودل باد گرانست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روی تو دلم را بسوی خود نگران کرد</p></div>
<div class="m2"><p>آن را که دلی هست برویت نگرانست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در حسن نباشد چوتو هرکس که نکوروست</p></div>
<div class="m2"><p>چون آب نباشد بصفا هرچه روانست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جان دل من شد غم عشق تو ازیرا</p></div>
<div class="m2"><p>دل زنده بعشق تو چو تن زنده بجانست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دل کو خط آزادی خویش ازهمه بستد</p></div>
<div class="m2"><p>جان داد بخطی که برو از تو نشانست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر طفل که از مادر ایام بزاید</p></div>
<div class="m2"><p>در عشق شود پیر اگرش بخت جوانست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر چند که جان در خطرست از غمت ای دوست</p></div>
<div class="m2"><p>دل کونه غم دوست خورد دشمن جانست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون کرد درونی بغم عشق تعلق</p></div>
<div class="m2"><p>آنست درونی که برون از دو جهانست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>با یار غم عشق مرا بر تن وبر دل</p></div>
<div class="m2"><p>نی کاه سبک باشد ونی کوه گرانست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سودا زده یی دوش چو فرهاد همی گفت</p></div>
<div class="m2"><p>کین دلبر ما خسرو شیرین پسرانست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مه طلعت و خورشید رخ و زهره جبینست</p></div>
<div class="m2"><p>شکر لب وشیرین سخن وپسته دهانست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تو دلبر خود را بکسی نام مگو سیف</p></div>
<div class="m2"><p>کآن چیز که در دل گذرد دوست نه آنست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>اینست یکی وصف زاوصاف کمالش</p></div>
<div class="m2"><p>کندر دل وجان ظاهر واز دیده نهانست</p></div></div>