---
title: >-
    شمارهٔ ۵۵۴
---
# شمارهٔ ۵۵۴

<div class="b" id="bn1"><div class="m1"><p>ای چون تو نبوده گل در هیچ گلستانی</p></div>
<div class="m2"><p>آن کار چه کارست آن کو تازه کند جانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گرچه نتوان گفتن می خوردن و خوش خفتن</p></div>
<div class="m2"><p>در زیر درخت گل با چون تو گلستانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کآنجا ز هوا نبود در طبع تقاضایی</p></div>
<div class="m2"><p>وآنجا ز حیا نبود بر بوسه نگهبانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عاشق ز لب جانان خمری چو عسل نوشد</p></div>
<div class="m2"><p>زاهد بترش رویی با سرکه خورد نانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یک روز مرا گفتی کامت بدهم یک شب</p></div>
<div class="m2"><p>سیراب کجا گردد این تشنه ببارانی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>صاحب هوسان هریک نسبت بکسی دارند</p></div>
<div class="m2"><p>چون من مگسی دارد چون تو شکرستانی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در خانه نشین ورنی بر روی فگن برقع</p></div>
<div class="m2"><p>کآشفته شود ناگه شهر از چو تو سلطانی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>با دشمن بذ گویم شد دوست رقیب تو</p></div>
<div class="m2"><p>بهر تو روا دارم زاغی بزمستانی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر سیف سر خود را اندر قدمت مالد</p></div>
<div class="m2"><p>پای ملخی بخشد موری بسلیمانی</p></div></div>