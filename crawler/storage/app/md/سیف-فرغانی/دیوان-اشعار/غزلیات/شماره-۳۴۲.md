---
title: >-
    شمارهٔ ۳۴۲
---
# شمارهٔ ۳۴۲

<div class="b" id="bn1"><div class="m1"><p>ای سجده کرده پیش جمالت بت چگل</p></div>
<div class="m2"><p>مطلوب خلق عالم ومحبوب اهل دل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هم شهد از حلاوت گفتار تو برشک</p></div>
<div class="m2"><p>هم حسن از طراوت رخسارتو خجل</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>علم ازل تواتر انوار تو بجان</p></div>
<div class="m2"><p>ملک ابد تعلق غمهای تو بدل</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خاصیتی است عشق ترا بر خلاف رسم</p></div>
<div class="m2"><p>ینجی لمن یعذب یهدی لمن یضل</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زین عام را خبر نه که خاص از برای تست</p></div>
<div class="m2"><p>تأثیرلطف صنع یدالله در آب وگل</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>باشد وجود تو قلم صنع را مداد</p></div>
<div class="m2"><p>یک یک حروف کون بهم گشت متصل</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مانند تو در انجم وافلاک کس ندید</p></div>
<div class="m2"><p>مجموعه یی بر انفس وآفاق مشتمل</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مصباح ماه را شده چون شمع آفتاب</p></div>
<div class="m2"><p>مشکاة نور روشن از آن روی مشتعل</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از مکتب فقیر تو گردون خوهد زکات</p></div>
<div class="m2"><p>با نعمت گدای تو قارون بود مقل</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گر وصل دوست می طلبی زینهار سیف</p></div>
<div class="m2"><p>پیوند هر دو کون ز خاطر فرو گسل</p></div></div>