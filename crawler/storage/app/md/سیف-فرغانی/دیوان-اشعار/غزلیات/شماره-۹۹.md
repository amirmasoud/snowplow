---
title: >-
    شمارهٔ ۹۹
---
# شمارهٔ ۹۹

<div class="b" id="bn1"><div class="m1"><p>در سمن با آن طراوت حسن این رخسار نیست</p></div>
<div class="m2"><p>در شکر با آن حلاوت ذوق این گفتار نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ژاله بر برگ سمن همچون عرق بر روی او</p></div>
<div class="m2"><p>لاله در صحن چمن مانند آن رخسار نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دوش گفتم از لبش جانم بکام دل رسد</p></div>
<div class="m2"><p>چون کنم او خفته و بخت رهی بیدار نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای بشیرینی ز شکر در جهان معروف تر</p></div>
<div class="m2"><p>شهد با چندان حلاوت چون تو شیرین کار نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون تو روزی مرهم وصلی نهی بر جان من</p></div>
<div class="m2"><p>گر بتیغ هجر مجروحم کنی آزار نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر دل تنگم اگر کوهی نهی کاهی بود</p></div>
<div class="m2"><p>کآنچه جز هجر تو باشد بر دل من بار نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا درآید اندرو غمهای تو هر سو درست</p></div>
<div class="m2"><p>خانه دلرا که جز نقش تو بر دیوار نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مستی و دیوانگی از چون منی نبود عجب</p></div>
<div class="m2"><p>کز شراب عشق تو در من رگی هشیار نیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر همه جانست اندر وی نباشد زندگی</p></div>
<div class="m2"><p>چون کسی را دل ز درد عشق تو بیمار نیست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در سخن هر لفظ کندر وی نباشد نام تو</p></div>
<div class="m2"><p>صورتش گر جان بود آن لفظ معنی دار نیست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هرکه عاشق نیست از وصلت نیابد بهره یی</p></div>
<div class="m2"><p>هرکه او نبود بهشتی لایق دیدار نیست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>سیف فرغانی چو روی دوست دیدی ناله کن</p></div>
<div class="m2"><p>عندلیبی و ترا جز روی او گلزار نیست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چون مدد از غیر نبود صبر کن تا حل شود</p></div>
<div class="m2"><p>« ای که گفتی هیچ مشکل چون فراق یار نیست »</p></div></div>