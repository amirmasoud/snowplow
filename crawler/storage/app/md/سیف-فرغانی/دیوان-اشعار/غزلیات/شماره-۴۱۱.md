---
title: >-
    شمارهٔ ۴۱۱
---
# شمارهٔ ۴۱۱

<div class="b" id="bn1"><div class="m1"><p>آن دوست که ما ازآن اوییم</p></div>
<div class="m2"><p>در زمره عاشقان اوییم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این بخت نگر که جمله مردم</p></div>
<div class="m2"><p>آن خود وما ازآن اوییم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>وین دولت بین که از دو عالم</p></div>
<div class="m2"><p>آزاد چو بندگان اوییم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر مرده همه بدرد عشقیم</p></div>
<div class="m2"><p>ور زنده همه بجان اوییم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>او گلشن بلبلان عشقست</p></div>
<div class="m2"><p>ما بلبل گلستان اوییم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ما کرده نشان خویشتن محو</p></div>
<div class="m2"><p>وندر طلب نشان اوییم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ما همچو زبان بهر دهان در</p></div>
<div class="m2"><p>بهر لب بی دهان اوییم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جبریل زما مگس نراند</p></div>
<div class="m2"><p>چون از مگسان خوان اوییم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سلطان نبود چو ما توانگر</p></div>
<div class="m2"><p>اکنون که گدای نان اوییم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شیران همه کاسه لیس مایند</p></div>
<div class="m2"><p>تاما سگ استخوان اوییم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گرچه چو در ازپی گشایش</p></div>
<div class="m2"><p>پیوسته برآستان اوییم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>برما در این قفس گشادست</p></div>
<div class="m2"><p>تا بسته ریسمان اوییم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ماراتو کسی مدان که چون سیف</p></div>
<div class="m2"><p>ما هیچ کسان کسان اوییم</p></div></div>