---
title: >-
    شمارهٔ ۸۶
---
# شمارهٔ ۸۶

<div class="b" id="bn1"><div class="m1"><p>یار من خسرو خوبان ولبش شیرینست</p></div>
<div class="m2"><p>خبرش نیست که فرهاد وی این مسکینست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نکنم رو ترش ار تیز شود کز لب او</p></div>
<div class="m2"><p>سخن تلخ چو جان در دل من شیرینست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دید خورشید رخش وز سر انصاف بماه</p></div>
<div class="m2"><p>گفت من سایه او بودم وخورشید اینست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با رخ او که در او صورت خود نتوان دید</p></div>
<div class="m2"><p>هرکه در آینه یی می نگرد خود بینست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پای در بستر راحت نکنم وز غم او</p></div>
<div class="m2"><p>شب نخسبم که مرا درد سر از بالینست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خار مهرش چو برآورد سر از پای کسی</p></div>
<div class="m2"><p>رویش از خون جگر چون رخ گل رنگینست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دلستان تر نبود از شکن طره او</p></div>
<div class="m2"><p>آن خم وتاب که در گیسوی حورالعینست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در ره عشق که از هر دوجهانست برون</p></div>
<div class="m2"><p>دنیی ای دوست زمن رفت وسخن دردینست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر کسی ماه ندیدست که خندید آنست</p></div>
<div class="m2"><p>ورکسی سرو ندیدست که رفتست اینست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سیف فرغانی تا ازتو سخن می گوید</p></div>
<div class="m2"><p>مرغ روح از سخنش طوطی شکر چینست</p></div></div>