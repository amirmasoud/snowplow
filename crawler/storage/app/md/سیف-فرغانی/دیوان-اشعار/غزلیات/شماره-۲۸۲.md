---
title: >-
    شمارهٔ ۲۸۲
---
# شمارهٔ ۲۸۲

<div class="b" id="bn1"><div class="m1"><p>ای چو شیرین بدهان پسته بگفتارشکر</p></div>
<div class="m2"><p>در حدیث آی وازآن پسته فروبار شکر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دردهانت صنما غیر شکر چیزی نیست</p></div>
<div class="m2"><p>اینت تنگی که دروهست بخروار شکر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شهد راکرد زخجلت چو نبات ای دلبر</p></div>
<div class="m2"><p>پیش حلوای لبت کاسه نگو سار شکر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اندرآن رو که زقرص مه وخو را فزونست</p></div>
<div class="m2"><p>نمکی هست که کم نیست زبسیار شکر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شعرمن بنده مخوان تا بلبانت نرسد</p></div>
<div class="m2"><p>بارها گفتمت ازآب نگه دار شکر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همچو خسرو که بجان در طلب شیرین بود</p></div>
<div class="m2"><p>لب شیرین ترا هست طلب کار شکر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>با نبات لب لعلت زکساد بازار</p></div>
<div class="m2"><p>تا بامروز مگس داشت خریدار شکر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کام جانم نشود تلخ بمرگ ار روزی</p></div>
<div class="m2"><p>ازلب تو بدهانم رسد ای یار شکر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گلبن ار درچمن آب ازلب لعل تو خورد</p></div>
<div class="m2"><p>عوض غنچه برآید زسر خار شکر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نام و ننگم مبر ای جان ومرا دور مکن</p></div>
<div class="m2"><p>ازبرخود که ندارد زمگس عار شکر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گر زلعل تو خوهم بوسه مزن از سرکبر</p></div>
<div class="m2"><p>بانگ برمن که نباشد مگس آزار شکر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>پای دیوار چوتو گل رخ اگر بوسه نهم</p></div>
<div class="m2"><p>برمن افشاند خارازسر دیوار شکر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>در مقامی که شود با شکرآن شیرین جمع</p></div>
<div class="m2"><p>تو ازو بوسه خوه ای عاشق وبگذار شکر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>یار با آن لب شیرین سخن تلخم گفت</p></div>
<div class="m2"><p>در دوا کرد طبیب ازپی بیمار شکر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>سیف فرغانی با ذکر لب او عجبست</p></div>
<div class="m2"><p>گر ترشح نکند ازتو عرق وار شکر</p></div></div>