---
title: >-
    شمارهٔ ۵۸۱
---
# شمارهٔ ۵۸۱

<div class="b" id="bn1"><div class="m1"><p>گر خوش کند لب تو دهانم به بوسه‌ای</p></div>
<div class="m2"><p>خرم شود زلعل تو جانم به بوسه‌ای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خواهم ز جور تو گله کردن به پادشاه</p></div>
<div class="m2"><p>گر عاقلی بگیر دهانم به بوسه‌ای</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در حسرت کنار تو جانم به لب رسید</p></div>
<div class="m2"><p>زین ورطه لطف کن برهانم به بوسه‌ای</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای جان مکن گرانی و یک بار بر لب آی</p></div>
<div class="m2"><p>باشد که من ترا برسانم به بوسه‌ای</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفتم که جان من بستان بوسه‌ای بده</p></div>
<div class="m2"><p>گفتی هزار جان نستانم به بوسه‌ای</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر آن لب و دهان که تو داری مرا بود</p></div>
<div class="m2"><p>ملک دو کون را بستانم به بوسه‌ای</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دیدی که من زبان شکایت خوهم گشاد</p></div>
<div class="m2"><p>کردی فسون و بست زبانم به بوسه‌ای</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زنده کنند مرده به آب دهان من</p></div>
<div class="m2"><p>گر با تو کام خویش برانم به بوسه‌ای</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>یک بار دیگرم به کنار و لبت رسان</p></div>
<div class="m2"><p>ای برده حاصل دو جهانم به بوسه‌ای</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گویی لب من از شکر و قند خوش‌ترست</p></div>
<div class="m2"><p>من لذت لب تو چه دانم به بوسه‌ای</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>در کار عشق جان و دلی داشتم چو سیف</p></div>
<div class="m2"><p>اینم به عشوه‌ای شد و آنم به بوسه‌ای</p></div></div>