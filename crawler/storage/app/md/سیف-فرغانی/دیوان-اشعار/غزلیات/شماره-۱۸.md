---
title: >-
    شمارهٔ ۱۸
---
# شمارهٔ ۱۸

<div class="b" id="bn1"><div class="m1"><p>گر چه وصلت نفسی می ندهد دست مرا</p></div>
<div class="m2"><p>جز بیادت نزنم تا نفسی هست مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من چو وصل تو کسی را ندهم آسان دست</p></div>
<div class="m2"><p>چون بدست آوری آسان مده از دست مرا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون تو هشیار بدم، نرگس مخمورت کرد</p></div>
<div class="m2"><p>از می عشق بیک جرعه چنین مست مرا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مردمم شیفته خوانند و از آن بی خبرند</p></div>
<div class="m2"><p>که چنین شیفته سودای تو کردست مرا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گو نگهدار کنون جام نکونامی خویش</p></div>
<div class="m2"><p>آنکه او سنگ ملامت زدو بشکست مرا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا من ابروی کمان شکل تو دیدم چون صید</p></div>
<div class="m2"><p>تیر مژگانت ز هر سو بزد و خست مرا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ناوک غمزه وتیر مژه آید بر دل</p></div>
<div class="m2"><p>از کمان خانه ابروی تو پیوست مرا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دوش بر آتش شوقت همه شب از دیده</p></div>
<div class="m2"><p>آب می ریختم وسوز تو ننشست مرا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سیف فرغانی بی روی بهار آیینش</p></div>
<div class="m2"><p>همچو بلبل بخزان نطق فروبست مرا</p></div></div>