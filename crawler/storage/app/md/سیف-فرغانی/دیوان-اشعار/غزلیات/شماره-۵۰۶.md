---
title: >-
    شمارهٔ ۵۰۶
---
# شمارهٔ ۵۰۶

<div class="b" id="bn1"><div class="m1"><p>ای عشق تو داده روح را می</p></div>
<div class="m2"><p>مستان تو از تو دور تا کی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عشق تو شعار ماست در دین</p></div>
<div class="m2"><p>روی تو بهار ماست در دی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خورشید رخی و یک جهان خلق</p></div>
<div class="m2"><p>چون سایه ترا فتاده در پی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>وز عکس رخ چو لاله ریزان</p></div>
<div class="m2"><p>چون آب بقم ز عارضت خوی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اندر لب لعل تو حلاوت</p></div>
<div class="m2"><p>آمیخته همچو شهد بامی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جز عشق تو هرچه هست لاخیر</p></div>
<div class="m2"><p>جز وصل تو هرچه هست لاشی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>وصف تو ز طبع من عجب نیست</p></div>
<div class="m2"><p>چون در ز صدف چو شکر از نی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هرکو نه بعشق زنده شد سیف</p></div>
<div class="m2"><p>ای زنده بدو و مرده بی وی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر خود پدر قبیله باشد</p></div>
<div class="m2"><p>رو نسبت خود ببر از آن حی</p></div></div>