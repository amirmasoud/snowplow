---
title: >-
    شمارهٔ ۲۶۵
---
# شمارهٔ ۲۶۵

<div class="b" id="bn1"><div class="m1"><p>ای ترا پای بر سر خورشید</p></div>
<div class="m2"><p>سایه تست افسر خورشید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گرچه سایه ترا زمین بوسد</p></div>
<div class="m2"><p>زآسمان بگذرد سر خورشید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نیکوان جمله چاکران تواند</p></div>
<div class="m2"><p>ذرها جمله لشکر خورشید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سوی تو هر شبی که جامه چرخ</p></div>
<div class="m2"><p>در گریبان کشد سر خورشید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نامهای نیاز هر ذره است</p></div>
<div class="m2"><p>زیر بال کبوتر خورشید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در غم تست استخوان هلال</p></div>
<div class="m2"><p>ضلع پهلوی لاغر خورشید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای چو مه از شعاع چهره تو</p></div>
<div class="m2"><p>یافته نور پیکر خورشید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بر درتو زمن نماند اثر</p></div>
<div class="m2"><p>محو شد سایه بر در خورشید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>من نیم در خور تو وچه عجب</p></div>
<div class="m2"><p>سایه گر نیست در(خور) خورشید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>جز بنامت نمی زند در کان</p></div>
<div class="m2"><p>سکه بر سیم زرگر خورشید</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بیخ مهرتو کاشته در دل</p></div>
<div class="m2"><p>دایه تخم پرور خورشید</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>پیش روی تو ماه آینه ییست</p></div>
<div class="m2"><p>پیش روی منور خورشید</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>پرتوی بر زمین فتد چو نهند</p></div>
<div class="m2"><p>آیینه در برابر خورشید</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گر کند سایه تو تربیتش</p></div>
<div class="m2"><p>ذره گردد برادر خورشید</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>مدح تو گفت سیف فرغانی</p></div>
<div class="m2"><p>ذره یی شد ثناگر خورشید</p></div></div>