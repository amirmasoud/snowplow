---
title: >-
    شمارهٔ ۱۸۸
---
# شمارهٔ ۱۸۸

<div class="b" id="bn1"><div class="m1"><p>این حسن و آن لطافت در حور عین نباشد</p></div>
<div class="m2"><p>وین لطف و آن حلاوت در ترک چین نباشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ماهی اگر چه مه را بر روی گل نروید</p></div>
<div class="m2"><p>جانی اگر چه جان را صورت چنین نباشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از جان و دل فزونی وز آب و گل برونی</p></div>
<div class="m2"><p>کین آب (و) لطف هرگز در ما و طین نباشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای خدمت تو کردن بهتر ز دین و دنیا</p></div>
<div class="m2"><p>آنرا که تو نباشی دنیا و دین نباشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مشتاق وصلت ای جان دل در جهان نبندد</p></div>
<div class="m2"><p>انگشتری جم را زآهن نگین نباشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون دامن تو گیرد در پای تو چه ریزد</p></div>
<div class="m2"><p>بیچاره یی که جانش در آستین نباشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هان تا گدا نخوانی درویش را اگرچه</p></div>
<div class="m2"><p>اندر طریق عشقش دنیا معین نباشد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اندر روش نشاید شه را پیاده گفتن</p></div>
<div class="m2"><p>گر بر بساط شطرنج اسبی بزین نباشد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مرده شناس دل را کز عشق نیست جانی</p></div>
<div class="m2"><p>عقرب شمر مگس را کش انگبین نباشد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آن کو بعشق میرد اندر لحد نخسبد</p></div>
<div class="m2"><p>گور شهید دریا اندر زمین نباشد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>الا بعشق جانان مسپار سیف دل را</p></div>
<div class="m2"><p>کز بهر این امانت جبریل امین نباشد</p></div></div>