---
title: >-
    شمارهٔ ۱۴
---
# شمارهٔ ۱۴

<div class="b" id="bn1"><div class="m1"><p>ای بچشم دل ندیده روی یار خویش را</p></div>
<div class="m2"><p>کرده ای بی عشق ضایع روزگار خویش را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کعبه رو سوی تو دارد همچو تو رو سوی او</p></div>
<div class="m2"><p>گر تو روزی قبله سازی روی یار خویش را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عشق دعوی می کنی، بار بلا بر دوش نه</p></div>
<div class="m2"><p>نقد خود بر سنگ زن بنگر عیار خویش را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یا چو زن در خانه بنشین عاشق کار تو نیست</p></div>
<div class="m2"><p>عشق نیکو می شناسد مرد کار خویش را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عاشق آن قومند کندر حضرت سلطان عشق</p></div>
<div class="m2"><p>برگرفتند از ره خدمت غبار خویش را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>روز اول چون بدولت خانه عشق آمدند</p></div>
<div class="m2"><p>زآستان بیرون نهادند اختیار خویش را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بارگیر نفس شان در هر قدم جانی بداد</p></div>
<div class="m2"><p>تا بدین منزل رسانیدند بار خویش را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دیگران از ترک جان در راه جانان قاصرند</p></div>
<div class="m2"><p>زین شتر دل همرهان بگسل مهار خویش را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>وندرین ره دام ساز از جان و جانان صید کن</p></div>
<div class="m2"><p>پای بگشا باشه عنقا شکار خویش را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چون صدف گر در میان دارد در مهرش دلت</p></div>
<div class="m2"><p>زآب چشم چون گهر پر کن کنار خویش را</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ای توانگر سوی درویشان نظر کن ساعتی</p></div>
<div class="m2"><p>تا بخدمت عرضه دارند افتخار خویش را</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هر زمان در حلقه زنجیر زلفت می کشند</p></div>
<div class="m2"><p>یک جهان عاشق دل دیوانه سار خویش را</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>سیف فرغانی ز هجرانت بکام دشمنست</p></div>
<div class="m2"><p>چند دشمنکام داری دوستدار خویش را</p></div></div>