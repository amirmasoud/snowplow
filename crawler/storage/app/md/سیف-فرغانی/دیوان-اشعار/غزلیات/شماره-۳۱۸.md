---
title: >-
    شمارهٔ ۳۱۸
---
# شمارهٔ ۳۱۸

<div class="b" id="bn1"><div class="m1"><p>ای بت پسته دهن وقت تو چون نامت خوش</p></div>
<div class="m2"><p>عیش تلخ من ازآن چشم چو بادامت خوش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بگل روی خود ایام مرا خوش کردی</p></div>
<div class="m2"><p>باد چون موسم گل جمله ایامت خوش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>می کنم ناله زعشق تو چو بلبل که مرا</p></div>
<div class="m2"><p>همچو اندام گل آمد گل اندامت خوش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با رهی گربکنی سرکشی و نازت خوب</p></div>
<div class="m2"><p>در سماع ار بروی جنبش وآرامت خوش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دوش بخت من شوریده درآمد از خواب</p></div>
<div class="m2"><p>مژده یی داد بمن راست چوپیغامت خوش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کز می عشق اگر چند دهانت تلخ است</p></div>
<div class="m2"><p>عاقبت همچو لب خویش کند کامت خوش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گرچه در عشق بسی رنج کشیدی زآغاز</p></div>
<div class="m2"><p>رو که چون قصه یوسف شود انجامت خوش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بود باید زبرای شب وصلش امروز</p></div>
<div class="m2"><p>با غم هجر وی ای خواجه بناکامت خوش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ای زانعام تو خلقی شده بی اندیشه</p></div>
<div class="m2"><p>چند باشیم باندیشه انعامت خوش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سیف فرغانی از ذکر تو ایام مرا</p></div>
<div class="m2"><p>می کند همچو دل خویش بدشنامت خوش</p></div></div>