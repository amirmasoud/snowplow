---
title: >-
    شمارهٔ ۲۹۷
---
# شمارهٔ ۲۹۷

<div class="b" id="bn1"><div class="m1"><p>ای رخ خوب تو آفتاب جهان سوز</p></div>
<div class="m2"><p>عشق تو چون آتش وفراق تو جان سوز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شوق لقاء تو باده طرب انگیز</p></div>
<div class="m2"><p>عشق جمال تو آتشی است جهان سوز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دردل مجنون چه سوز بود زلیلی</p></div>
<div class="m2"><p>هست مرااز تو ای نگار همان سوز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خلق جهان مختلف شدند نگارا</p></div>
<div class="m2"><p>پرده برانداز ازآن یقین گمان سوز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کرد سیه دل مرا بدود ملامت</p></div>
<div class="m2"><p>عقل که چون هیزم تراست گران سوز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>رو غم آن ماه رو مخور که ندارد</p></div>
<div class="m2"><p>هر دهنی تاب آن طعام دهان سوز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در ره سودای او مباش کم از شمع</p></div>
<div class="m2"><p>گرنکشندت برو بمیر درآن سوز</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>با که توان گفت سر عشق چو با خود</p></div>
<div class="m2"><p>دم نتوان زد ازین حدیث زبان سوز</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در سخن ارگرم گشت سیف ازآن گشت</p></div>
<div class="m2"><p>تا بدلی درفتد ازین سخنان سوز</p></div></div>