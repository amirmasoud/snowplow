---
title: >-
    شمارهٔ ۳۰۶
---
# شمارهٔ ۳۰۶

<div class="b" id="bn1"><div class="m1"><p>گرچه جان می دهم از آرزوی دیدارش</p></div>
<div class="m2"><p>جان نو داد بمن صورت معنی دارش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بنگر آن دایره روی و برو نقطه خال</p></div>
<div class="m2"><p>دست تقدیر بصد لطف زده پرگارش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بوستانیست که قدر شکر و گل بشکست</p></div>
<div class="m2"><p>ناردان لب و رخساره چون گلنارش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ملک خسرو برود در هوس بندگیش</p></div>
<div class="m2"><p>آب شیرین ببرد لعل شکر گفتارش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نقد جان رفت درین کار خریدارش را</p></div>
<div class="m2"><p>برو ای حسن و دگر تیز مکن بازارش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از پی نصرت سلطان جمالش جمعست</p></div>
<div class="m2"><p>لشکر حسن بزیر علم دستارش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا غم تلخ گوارش نخوری یکچندی</p></div>
<div class="m2"><p>کام شیرین نکنی از لب شکربارش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عشق دردیست که چون کرد کسی را بیمار</p></div>
<div class="m2"><p>گر بمیرد نخوهد صحت خود بیمارش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>لوح ما از قلم دوست نه آن نقش گرفت</p></div>
<div class="m2"><p>کآب بر وی گذرد محو کند آثارش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آنچه داری بکف و آنچه نداری جز دوست</p></div>
<div class="m2"><p>گر نیاید مطلب ور برود بگذارش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سیف فرغانی نزدیک همه زنده دلان</p></div>
<div class="m2"><p>مرده یی باشی اگر جان ندهی در کارش</p></div></div>