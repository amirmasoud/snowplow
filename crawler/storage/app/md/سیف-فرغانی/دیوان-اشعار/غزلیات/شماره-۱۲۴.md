---
title: >-
    شمارهٔ ۱۲۴
---
# شمارهٔ ۱۲۴

<div class="b" id="bn1"><div class="m1"><p>جانم از عشقت پریشانی گرفت</p></div>
<div class="m2"><p>کارم از هجر تو ویرانی گرفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وصل تو دشوار یابد چون منی</p></div>
<div class="m2"><p>مملکت نتوان بآسانی گرفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر سعادت یار باشد بنده را</p></div>
<div class="m2"><p>سهل باشد ملک و سلطانی گرفت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دست در زلفت بنادانی زدم</p></div>
<div class="m2"><p>مار را کودک بنادانی گرفت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دوست بی همت نگردد ملک کس</p></div>
<div class="m2"><p>ملک بی شمشیر نتوانی گرفت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حسن رویت ای صنم آفاق را</p></div>
<div class="m2"><p>راست چون دین مسلمانی گرفت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر سر بالین عشاقت بشب</p></div>
<div class="m2"><p>خواب چون بلبل سحرخوانی گرفت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گفتمت کامم بده، گفتی بطنز</p></div>
<div class="m2"><p>من بدادم گر تو بتوانی گرفت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دربهای وصل اگر جان میخوهی</p></div>
<div class="m2"><p>راضیم چون نرخش ارزانی گرفت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>اینچنین ملکی که سلطان را نبود</p></div>
<div class="m2"><p>چون تواند سیف فرغانی گرفت</p></div></div>