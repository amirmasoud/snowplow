---
title: >-
    شمارهٔ ۳۲
---
# شمارهٔ ۳۲

<div class="b" id="bn1"><div class="m1"><p>ای سعادت زپی زینت وزیبایی را</p></div>
<div class="m2"><p>بافته بر قد تو کسوت رعنایی را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عشق رویت چو مرا حلقه بزد بر در دل</p></div>
<div class="m2"><p>شوق از خانه بدر کرد شکیبایی را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر ببینم رخ چون شمع تو ای جان بیمست</p></div>
<div class="m2"><p>کآب چشمم بکشد آتش بینایی را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ذرها گر همه خورشید شود بی رویت</p></div>
<div class="m2"><p>نبود روز وشب عاشق سودایی را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>من شوریده سر کوی ترا ترک کنم</p></div>
<div class="m2"><p>گر مگس ترک کند صحبت حلوایی را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در دهان طمعم چون ترشی کند کند</p></div>
<div class="m2"><p>لب شیرین تو دندان شکر خایی را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دهن تنگ تو چون ذره در سایه نهان</p></div>
<div class="m2"><p>نفی کرده است زخود تهمت پیدایی را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>صبر با غمزه غارت گرت افگند سپر</p></div>
<div class="m2"><p>دفع شمشیر کند لشکر یغمایی را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هوس نرگس شیر افگن تو در کویت</p></div>
<div class="m2"><p>با سگان انس دهد آهوی صحرایی را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بهرتو گوهر دین ترک همی باید کرد</p></div>
<div class="m2"><p>زآنکه تو خاک شماری زر دنیایی را</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سعدی ار شعرمن وحسن تو دیدی گفتی</p></div>
<div class="m2"><p>غایت اینست جمال وسخن آرایی را</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>سیف فرغانی چون شمع خیالش با تست</p></div>
<div class="m2"><p>چه غم ار روز نباشد شب تنهایی را</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>مرد نادان زغم آسوده بود چون کودک</p></div>
<div class="m2"><p>خیز وچون تخته بشو دفتر دانایی را</p></div></div>