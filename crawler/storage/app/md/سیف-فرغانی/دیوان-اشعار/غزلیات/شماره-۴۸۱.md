---
title: >-
    شمارهٔ ۴۸۱
---
# شمارهٔ ۴۸۱

<div class="b" id="bn1"><div class="m1"><p>بگشادی رو که رنگی بستست بر رخ تو</p></div>
<div class="m2"><p>حسن آنچنانکه خود را گل بر بهار بسته</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خسرو بقصد جانم آهنگ کرده ومن</p></div>
<div class="m2"><p>امید در تو شیرین فرهاد وار بسته</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من چون گدا که نانم از تست حاصل و تو</p></div>
<div class="m2"><p>سگ را گشاده وآنگه در استوار بسته</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر در دهانم آید جز ذکر تو حدیثی</p></div>
<div class="m2"><p>گردد زبان نطقم بی اختیار بسته</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای لطف حق زخوبی صد در گشاده بر تو</p></div>
<div class="m2"><p>بر سیف در چه داری در روز بار بسته</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اکنون که شد دل من در عشق یار بسته</p></div>
<div class="m2"><p>یارب در وصالش برمن مدار بسته</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا صید او شدستم زنجیر می درانم</p></div>
<div class="m2"><p>همچون سگی که باشد وقت شکار بسته</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بگشاد دی بپیشم آن زلف چون رسن را</p></div>
<div class="m2"><p>من تنگدل بماندم زآن دم چو بار بسته</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>وامروز بهر کشتن بربست هر دودستم</p></div>
<div class="m2"><p>دیدم بروز ماتم دست نگار بسته</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زآن ساعتی که عاشق بویی شنوده از تو</p></div>
<div class="m2"><p>ای ازرخ تو رنگی گل برعذار بسته</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>پیوند نسبت خود از غیر تو بریده</p></div>
<div class="m2"><p>تا بر تو خویشتن را برگل چو خار بسته</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بیهوده گوی داند همچون درآیم آنکس</p></div>
<div class="m2"><p>کو اشتری ندارد با این قطار بسته</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>تا تو بحسن خود را بازار تیز کردی</p></div>
<div class="m2"><p>شد آفتاب ومه رادکان کار بسته</p></div></div>