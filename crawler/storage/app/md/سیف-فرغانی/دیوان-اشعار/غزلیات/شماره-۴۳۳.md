---
title: >-
    شمارهٔ ۴۳۳
---
# شمارهٔ ۴۳۳

<div class="b" id="bn1"><div class="m1"><p>با سر زلف تو صعبست مسلمان بودن</p></div>
<div class="m2"><p>با رخت خود نتوان بسته ایمان بودن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من چو اندر سر گیسوی تو بستم دل خویش</p></div>
<div class="m2"><p>پس مرا چاره نباشد زپریشان بودن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گل چو اندام تو می خواست که باشد بنگر</p></div>
<div class="m2"><p>کآرزو میکند او را همه تن جان بودن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>غرقه بحر فراق توام (و)تشنه وصل</p></div>
<div class="m2"><p>وینچنین تا بابد بهر تو بتوان بودن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کز پی دانه در همچو صدف می شاید</p></div>
<div class="m2"><p>غرق دریا شدن و تشنه باران بودن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بگدایی درت فخر کنم درهر کوی</p></div>
<div class="m2"><p>من که عار آیدم ازخسرو و سلطان بودن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گرچه با آتش سودای تو باید چون شمع</p></div>
<div class="m2"><p>هر شب از سوز دل سوخته گریان بودن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>روز با درد تو از غیر تو مرگی دگرست</p></div>
<div class="m2"><p>دل بیمار مرا طالب درمان بودن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بی رخ لیلی اگر کوه گرفتم چه عجب</p></div>
<div class="m2"><p>من خوکرده چو مجنون ببیابان بودن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>عشق میدان و درو هست قدم جان بازی</p></div>
<div class="m2"><p>با چنین پای توان بر سر میدان بودن؟</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سیف فرغانی از خود برو ار مرد رهی</p></div>
<div class="m2"><p>تا بخود باشی نتوانی از ایشان بودن</p></div></div>