---
title: >-
    شمارهٔ ۱۳۶
---
# شمارهٔ ۱۳۶

<div class="b" id="bn1"><div class="m1"><p>زکوی دوست بادی بر من افتاد</p></div>
<div class="m2"><p>چه بادست این که رحمتها بروباد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بمن آورد از آن دلبر پیامی</p></div>
<div class="m2"><p>چنان شیرین که شوری در من افتاد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بدو گفتم اگر آنجا روی باز</p></div>
<div class="m2"><p>دل غمگین ما را شادکن شاد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بگویش بی تو او را نیم جانیست</p></div>
<div class="m2"><p>وگر در دست بودی می فرستاد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دل چون مومش از مهرت جدا نیست</p></div>
<div class="m2"><p>چو نقش از خاتم و جوهر ز پولاد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>وگر آن آب اینجامی نیاید</p></div>
<div class="m2"><p>برو این خاک را آنجا بر ای باد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>که یاد بی فراموشیست اینجا</p></div>
<div class="m2"><p>وزآن جانب فراموشیست بی یاد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو جان او دل شهری خرابست</p></div>
<div class="m2"><p>ز جور هجرت ای سلطان بی داد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نگویم کز پری زادی ولیکن</p></div>
<div class="m2"><p>بدین خوبی نباشد آدمی زاد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>اگر چشمت بغمزه دل همی برد</p></div>
<div class="m2"><p>لب لعلت ببوسه جان همی داد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مرا شیرینی تو کشته وتو</p></div>
<div class="m2"><p>چو خسرو شادمان از مرگ فرهاد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بسوی کوی عشقت عاشقان را</p></div>
<div class="m2"><p>ز خود رفتن رهست و بیخودی زاد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بیاد سیف فرغانی بسی کرد</p></div>
<div class="m2"><p>دل غمگین خود را خرم آباد</p></div></div>