---
title: >-
    شمارهٔ ۱۴۷
---
# شمارهٔ ۱۴۷

<div class="b" id="bn1"><div class="m1"><p>مرا در دل غم جان می نگنجد</p></div>
<div class="m2"><p>درو جز عشق جانان می نگنجد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چنان پر شد دلم از شادی عشق</p></div>
<div class="m2"><p>که اندر وی غم جان می نگنجد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نگارا عشق تو زآن عقل من برد</p></div>
<div class="m2"><p>که در ملکی دوسلطان می نگنجد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>غم تو گردن هستیم بشکست</p></div>
<div class="m2"><p>دو سر در یک گریبان می نگنجد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دل عاشق زشادی بی نصیب است</p></div>
<div class="m2"><p>فرح در بیت احزان می نگنجد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>درون عاشقان زآن سان پر از تست</p></div>
<div class="m2"><p>که دل نیز اندر ایشان می نگنجد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مرا عشق تو با دنیا و عقبی</p></div>
<div class="m2"><p>دو نانم بر یکی خوان می نگنجد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>برویت نسبتی کردیم گل را</p></div>
<div class="m2"><p>زشادی در گلستان می نگنجد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چوآمد عشق تو من رفتم از دست</p></div>
<div class="m2"><p>بهرجا کین نشست آن می نگنجد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دل تنگ احتمال عشق نکند</p></div>
<div class="m2"><p>سریر شه در ارمان می نگنجد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>برو خیمه مزن در خانه آن را</p></div>
<div class="m2"><p>که خرگه در بیابان می نگنجد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>درین ره سیف فرغانی نگنجید</p></div>
<div class="m2"><p>وزغ در آب حیوان می نگنجد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>زمین را جا کجا باشد برآن اوج</p></div>
<div class="m2"><p>که دروی چرخ گردان می نگنجد</p></div></div>