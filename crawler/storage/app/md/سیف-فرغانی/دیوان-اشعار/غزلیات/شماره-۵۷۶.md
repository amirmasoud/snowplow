---
title: >-
    شمارهٔ ۵۷۶
---
# شمارهٔ ۵۷۶

<div class="b" id="bn1"><div class="m1"><p>در گلشن حسن چون تو کس نیست</p></div>
<div class="m2"><p>معروف چو گل بخوب رویی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من تنگ دلم چو غنچه و تو</p></div>
<div class="m2"><p>لاله رخی و بنفشه مویی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>موی تو بر آن زمین که افتاد</p></div>
<div class="m2"><p>از خاک نرفت مشک بویی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ما آن توایم وجمله گویند</p></div>
<div class="m2"><p>تو آن کئی همی نگویی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گم شد دل صد هزار عاشق</p></div>
<div class="m2"><p>تو خود دل عاشقی نجویی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دستت نرسد بدامن سیف</p></div>
<div class="m2"><p>تا دست زخویشتن نشویی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای چون گل نو بتازه رویی</p></div>
<div class="m2"><p>بر روی تو ختم شد نکویی</p></div></div>