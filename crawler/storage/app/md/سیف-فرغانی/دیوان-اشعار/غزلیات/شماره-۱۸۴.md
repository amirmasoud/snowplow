---
title: >-
    شمارهٔ ۱۸۴
---
# شمارهٔ ۱۸۴

<div class="b" id="bn1"><div class="m1"><p>بسی گفتم ترا گر یاد باشد</p></div>
<div class="m2"><p>که دم بی یاد جانان باد باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل ویران بعشق تست معمور</p></div>
<div class="m2"><p>جهان ای جان بعدل آباد باشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خلاف عشق کردن کار نفس است</p></div>
<div class="m2"><p>خلاف هود کار عاد باشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همه کس را نباشد جوهر عشق</p></div>
<div class="m2"><p>همه آهن کجا پولاد باشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کسی کو نیست عاشق آدمی نیست</p></div>
<div class="m2"><p>چو شاهین صید نکند خاد باشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تویی سلطان حسن و بنده تست</p></div>
<div class="m2"><p>کسی کز هر دو کون آزاد باشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ترا عاشق بسی و من از ایشان</p></div>
<div class="m2"><p>چنانم کز الوف آحاد باشد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عجب نبود که شیرین شکر را</p></div>
<div class="m2"><p>بهر خانه مگس فرهاد باشد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زمن گر زر ستانی نیست بی داد</p></div>
<div class="m2"><p>وگر جانم ستانی داد باشد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>درین ره ترک نان آب حیوتست</p></div>
<div class="m2"><p>که خون خویش خوردن زاد باشد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کتب شویم چو کودک تخته خویش</p></div>
<div class="m2"><p>مرا گر عشق تو استاد باشد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بر من آیت قرآن عشقست</p></div>
<div class="m2"><p>حدیثی کش بتو اسناد باشد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بحضرت سیف فرغانی سخن برد</p></div>
<div class="m2"><p>ببذل زر سخی معتاد باشد</p></div></div>