---
title: >-
    شمارهٔ ۳۴۴
---
# شمارهٔ ۳۴۴

<div class="b" id="bn1"><div class="m1"><p>ایا سلطان عشق تو نشسته بر سریر دل</p></div>
<div class="m2"><p>بلشکرهای خود کرده تصرف در ضمیر دل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رئیس عقل را گفتم سر خود گیر ای مسکین</p></div>
<div class="m2"><p>چو شاه عشق او بنهاد پایی بر سریر دل</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ترا از حسن لشکرهاست ای سلطان سلطانان</p></div>
<div class="m2"><p>که می گیرند ملک جان و می آرند اسیر دل</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز سوزن بگذرد بی شک تن چون ریسمان من</p></div>
<div class="m2"><p>اگر مقراض غم رانی ازین سان بر حریر دل</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>درین ملکی که من دارم خراب از دولت عشقت</p></div>
<div class="m2"><p>غمت گو کار خود می کن که معزولست امیر دل</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بدان رخسار چون آتش تنور عشق بر کردی</p></div>
<div class="m2"><p>من ناپخته از خامی درو بستم خمیر دل</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو مالک بی زیان باشد ز دوزخ هرکه او دارد</p></div>
<div class="m2"><p>ز رضوان خیال تو بهشتی در سعیر دل</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اگر عیسی عشق تو نکردی چاره چشمش</p></div>
<div class="m2"><p>بنور آتش موسی ندیدی ره ضریر دل</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در آ در خانقاه ای جان و در ده صوفیانش را</p></div>
<div class="m2"><p>می عشقت، که بیرون برد ازو سجاده پیر دل</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چو جان را آسیای شوق در چرخست از وصلت</p></div>
<div class="m2"><p>بده آبی که در افلاک آتش زد اثیر دل</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز عشقت خاتمی گر هست جان چون سلیمان را</p></div>
<div class="m2"><p>نگین مهر غیر تو بخود نگرفت قیر دل</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مداد از خاطر و کاغذ کند از دفتر فکرت</p></div>
<div class="m2"><p>برای ذکر تو، وز جان قلم سازد دبیر دل</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>دلم بی غم غمت بی دل نبودستند واکنون شد</p></div>
<div class="m2"><p>دل من ناگزیر غم غم تو ناگزیر دل</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>وراورا نیز دریابی چنان مگذار (و) مستش کن</p></div>
<div class="m2"><p>که بی آن بدرقه در ره خطر دارد خطیر دل</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>هنوز این دایه گیتی نزاد از مادر فطرت</p></div>
<div class="m2"><p>که نوزادان اندوهت همی خوردند شیر دل</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ز دست هستی خود سر نبردی سیف فرغانی</p></div>
<div class="m2"><p>اگر زنجیر عشق تو نبودی پای گیر دل</p></div></div>