---
title: >-
    شمارهٔ ۳۵۴
---
# شمارهٔ ۳۵۴

<div class="b" id="bn1"><div class="m1"><p>عاشقان راسوی خود هم خود بود جانان دلیل</p></div>
<div class="m2"><p>کعبه وصل و زاد غم، وز خویشتن رفتن سبیل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای بقال وقیل عالم بی خبر از عشق تو</p></div>
<div class="m2"><p>هر که معلومش تو باشی فارغست از قال و قیل</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گرد خجلت می فشاند نور رویت بر قمر</p></div>
<div class="m2"><p>آب حیوان می چکاند تیغ عشقت بر قتیل</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هرکه را زین سیم وزین زر کرد مستغنی غمت</p></div>
<div class="m2"><p>زر بنزد آن توانگر چون گدا باشد ذلیل</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا بدیدم شمع روی تو، چنان با خود گرفت</p></div>
<div class="m2"><p>آتش عشق ترا جانم که روغن را فتیل</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>با بلا هم خانه باشد عاشق اندر کوی تو</p></div>
<div class="m2"><p>وز سلامت دور باشد پشه زیر پای پیل</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>طبع شورانگیز را بر جان عاشق حکم نیست</p></div>
<div class="m2"><p>آتش نمرود را تاثیر نبود در خلیل</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از برای وصل جانان گر زعاشق جان خوهد</p></div>
<div class="m2"><p>همچنان باشد که آب از جوی خواهد سلسبیل</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>یوسفان حسن را جاه وجمال از روی تست</p></div>
<div class="m2"><p>چون شکر از خاک مصر وچون نهنگ از آب نیل</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>عاشقان را چه زیان گر عقلشان نکند مدد</p></div>
<div class="m2"><p>در خلافت چه خلل گر با علی نبود عقیل</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گر پیمبر وار عاشق وارهد از خویشتن</p></div>
<div class="m2"><p>وحیها آید بدو واندر میان نی جبرئیل</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>سیف فرغانی زغم بر عاشقان تکلیف نیست</p></div>
<div class="m2"><p>حمل کوه بیستون فرهاد را نبود ثقیل</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>از پی تعریف جانان را مکن در شعر ذکر</p></div>
<div class="m2"><p>بهر شهرت در چمن گل را مکش بر روی بیل</p></div></div>