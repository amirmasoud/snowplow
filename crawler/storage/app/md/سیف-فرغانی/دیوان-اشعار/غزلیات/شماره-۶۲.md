---
title: >-
    شمارهٔ ۶۲
---
# شمارهٔ ۶۲

<div class="b" id="bn1"><div class="m1"><p>دل چون بجان نظر فگند جای عشق تست</p></div>
<div class="m2"><p>دل جان شود درو چو تمنای عشق تست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سیمرغ وار خود نتوان یافتن نشان</p></div>
<div class="m2"><p>زآن دل که آشیانه عنقای عشق تست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جانم فدای تو که دل مرده رهی</p></div>
<div class="m2"><p>از زنده کردگان مسیحای عشق تست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای نور دیده درتن مشکات شکل ما</p></div>
<div class="m2"><p>مصباح روح زنده باحیای عشق تست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون جان بزندگی ابد شادمان بود</p></div>
<div class="m2"><p>آن دل که درحمایت غمهای عشق تست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>فرهاد وار در پس هر سنگ بی دلیست</p></div>
<div class="m2"><p>بیگانه خسروست که شیدای عشق تست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>من خام کیستم که پزم دیگ این هوس</p></div>
<div class="m2"><p>هرجا سریست مطبخ سودای عشق تست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گردن کش خرد که هوا را نداد دست</p></div>
<div class="m2"><p>سرمست و پای کوب زصهبای عشق تست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>افراز و شیب کون و مکان زیر پای کرد</p></div>
<div class="m2"><p>دل منزلی ندید که بالای عشق تست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ما خامشیم و مهر ادب بر زبان چو سیف</p></div>
<div class="m2"><p>این جمله گفت و گو بتقاضای عشق تست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>موج غم تو از صدف دل برون فگند</p></div>
<div class="m2"><p>این نظم را که گوهر دریای عشق تست</p></div></div>