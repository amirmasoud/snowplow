---
title: >-
    شمارهٔ ۷۱
---
# شمارهٔ ۷۱

<div class="b" id="bn1"><div class="m1"><p>بداد باز مراصحبت نگاری دست</p></div>
<div class="m2"><p>وگرچه داشته بودم زعشق باری دست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چنین نگار که امروز دست داد مرا</p></div>
<div class="m2"><p>نمی دهد دگران را بروزگاری دست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>میسرم شد ناگاه صحبت یاری</p></div>
<div class="m2"><p>که وصل او ندهد جز بانتظاری دست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر بپای رقیبش سری نهم شاید</p></div>
<div class="m2"><p>که می زنم زبرای گلی بخاری دست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو پای در ره مهرش نهاد جان زآن پس</p></div>
<div class="m2"><p>نرفت بیش دلم را بهیچ کاری دست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مرا گرفت گریبان و برد پای از جای</p></div>
<div class="m2"><p>ازآستین مدد پنجه یی برآر ای دست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ایا چو لعل نگین نام دار در خوبی</p></div>
<div class="m2"><p>چو خاتم ار دهدم چون تو نامداری دست،</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بصد نگار منقش بخلق بنمایم</p></div>
<div class="m2"><p>درخت وار مزین بهر بهاری دست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>درین مصاف ازو دل برد نه جان ای دوست</p></div>
<div class="m2"><p>چو برپیاده بیابد چوتو سواری دست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چو درکمند تو بی اختیار افتادم</p></div>
<div class="m2"><p>زدامن تو ندارم باختیاری دست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تو خاک پای خود ای دوست درکف من نه</p></div>
<div class="m2"><p>اگر بنزد تو دارم فقیرواری دست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>غریب شهر توام از خودم مکن نومید</p></div>
<div class="m2"><p>کنون که در تو زدم چون امیدواری دست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بود که جان ببرم از میان بحر فراق</p></div>
<div class="m2"><p>اگر شبی بزنم باتو در کناری دست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>مگیر خانه درین کوی سیف فرغانی</p></div>
<div class="m2"><p>اگر ترا ندهد دلبری و یاری دست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>برو برو که به جز استخوان درین بازار</p></div>
<div class="m2"><p>نمی دهد سگ قصاب را شکاری دست</p></div></div>