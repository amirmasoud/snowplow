---
title: >-
    شمارهٔ ۳۷۱
---
# شمارهٔ ۳۷۱

<div class="b" id="bn1"><div class="m1"><p>بیک نظر دل خلقی همی برد یارم</p></div>
<div class="m2"><p>بمن نگر که بدان یک نظر گرفتارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مرا خود از خبرش بود حال شوریده</p></div>
<div class="m2"><p>کنون بیک نظر او تمام شد کارم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ورا اگر دگری یافت و از طلب بنشست</p></div>
<div class="m2"><p>من آن کسم که پس از یافتن طلب کارم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شبی بخدمت او خلوتی خوهم تا روز</p></div>
<div class="m2"><p>که او ز لب شکر و من ز دیده دربارم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چراغ وارم از آن پس اگر کشند رواست</p></div>
<div class="m2"><p>ستاره وار چو با مه شبی بروز آرم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>امیر ملک ورا طالب است و من در عشق</p></div>
<div class="m2"><p>نمی خوهم که فرومایه یی بود یارم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تو همت من مسکین نگر که چون فرهاد</p></div>
<div class="m2"><p>برای شیرین با خسرو است پیکارم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>من از مدام املهای خویش بودم مست</p></div>
<div class="m2"><p>شراب عشقش از آن سکر کرد هشیارم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کنون نخسبم جز بر درش چو سگ همه روز</p></div>
<div class="m2"><p>که شب روان رهش کرده اند بیدارم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بدین گنه که دلم قدر وصل تو نشناخت</p></div>
<div class="m2"><p>گرم بهجر عقوبت کند سزاوارم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>اگر چنانکه زدی لاف سیف فرغانی</p></div>
<div class="m2"><p>که من ببذل درم سرخ رو چو دینارم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>میان خلق تفاوت بسیست در گوهر</p></div>
<div class="m2"><p>که دوست را تو بزر من بجان خریدارم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>طریق اهل دل اینست کاین امانت جان</p></div>
<div class="m2"><p>که دوست داد بمن من بدوست بسپارم</p></div></div>