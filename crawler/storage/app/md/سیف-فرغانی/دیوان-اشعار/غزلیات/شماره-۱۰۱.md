---
title: >-
    شمارهٔ ۱۰۱
---
# شمارهٔ ۱۰۱

<div class="b" id="bn1"><div class="m1"><p>مرا بغیر تو اندر جهان دگر کس نیست</p></div>
<div class="m2"><p>بجز تو دل ندهم کز تو خوبتر کس نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بچشم حال چو ما را خبر معاینه شد</p></div>
<div class="m2"><p>بعین چون تو ندیدیم و در خبر کس نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مرا که دیده دل از تو روشنی دارد</p></div>
<div class="m2"><p>بهر کجا نگرم جز تو در نظر کس نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بگرد کوی تو هر عاشقی که کشته شود</p></div>
<div class="m2"><p>شهید عشق بود خون بهاش بر کس نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جهان بجمله خرابست و نیست آبادان</p></div>
<div class="m2"><p>بجز دلی که درو غیر تو دگر کس نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جهانیان اگر از حسن روی تو خبری</p></div>
<div class="m2"><p>شنوده اند چرا طالب اثر کس نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ره تو معنی و این خلق صورت آرایند</p></div>
<div class="m2"><p>ز بهر کار تو اندر جهان مگر کس نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دهان بیاد تو گر خوش نمی کنند این خلق</p></div>
<div class="m2"><p>ز بی کسی مگس اینچنین شکر کس نیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز بهر صبح وصالت که کی زند نفسی</p></div>
<div class="m2"><p>شبی ز شوق تو بیدار تا سحر کس نیست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چو عقل بر سر کویت گذر کند داند</p></div>
<div class="m2"><p>که راه عشق تو ای جان بپای هر کس نیست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>اگر ز پرده برونست سیف فرغانی</p></div>
<div class="m2"><p>چو آستانه به جز وی مقیم در کس نیست</p></div></div>