---
title: >-
    شمارهٔ ۱۳۵
---
# شمارهٔ ۱۳۵

<div class="b" id="bn1"><div class="m1"><p>ای زروی تو مه و خور را مدد</p></div>
<div class="m2"><p>از ازل دوران حسنت تا ابد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حسن را از عاشقان باشد کمال</p></div>
<div class="m2"><p>پادشاه از لشکری دارد مدد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در کتاب ما نمی گنجد حروف</p></div>
<div class="m2"><p>درحساب ما نمی آید عدد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>معنی اسما همه در ذات تو</p></div>
<div class="m2"><p>مضمر ست ای دوست چون نه در نود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کشته عشقت نمیرد در مصاف</p></div>
<div class="m2"><p>مرده شوقت نخسبد در لحد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>صعب باشد در دل شوریده عشق</p></div>
<div class="m2"><p>گرم باشد آفتاب اندر اسد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آدمی بی عشق تو دل مرده ییست</p></div>
<div class="m2"><p>ورفرشته جان خود دروی دمد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در ره عشقت براق همتم</p></div>
<div class="m2"><p>می زند بر توسن گردون لگد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>وصف حسنت کی توان گفتن بشعر</p></div>
<div class="m2"><p>کسب دولت چون توان کردن بکد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خامشی بهتر که نتوانم گرفت</p></div>
<div class="m2"><p>خیمه گردون چو خرگه در نمد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نفس اسرار ترا نبود امین</p></div>
<div class="m2"><p>دزد بر جوهر نباشد معتمد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>عاشق از چرخ و ز انجم برترست</p></div>
<div class="m2"><p>اختر عاشق نیاید در رصد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>از کلام او خلایق بی خبر</p></div>
<div class="m2"><p>وز مقام او ملایک در حسد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ترک گفته جان او ملک دو کون</p></div>
<div class="m2"><p>محو کرده روح او رسم جسد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>سیف فرغانی بتو جان تحفه داد</p></div>
<div class="m2"><p>تحفه درویش نتوان کرد رد</p></div></div>