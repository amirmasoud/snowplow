---
title: >-
    شمارهٔ ۳۹۸
---
# شمارهٔ ۳۹۸

<div class="b" id="bn1"><div class="m1"><p>گر از ره تو بود خاک را گهر دانم</p></div>
<div class="m2"><p>وراز کف تو بود زهر را شکر دانم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کسی که سیر درین ره (کند) اگر شترست</p></div>
<div class="m2"><p>بسوی کعبه قرب تو راهبر دانم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ورم بکعبه قرب تو راهبر نبود</p></div>
<div class="m2"><p>اگر چه قبله بود روی ازو بگردانم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گرم خبر نکند از مقام ابراهیم</p></div>
<div class="m2"><p>دلیل را شتر وکعبه را حجر دانم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بتیغ قهرم اگر عشق تو زند گردن</p></div>
<div class="m2"><p>نه مست شوقم اگر پای رازسر دانم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر آسمان بودم مملکت، سپاه انجم</p></div>
<div class="m2"><p>بدست عشق شکسته شدن ظفر دانم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>حق ثنای ترا یک زبان ادا نکند</p></div>
<div class="m2"><p>بصد زبان بستایم ترا اگر دانم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بسی لطیفه به جز حسن در تو موجودست</p></div>
<div class="m2"><p>بجز شکوفه چه داری بر شجر دانم(؟)</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بروی حاجت من بسته باد چون دیوار</p></div>
<div class="m2"><p>بجز در تو اگر من دری دگر دانم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نماز خدمت تن قصر کردم وگشتم</p></div>
<div class="m2"><p>مقیم کوی تو، از خود شدن سفر دانم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بکوی دوست دورنگی روز وشب نبود</p></div>
<div class="m2"><p>زکوی تو نیم ار شام (و)ار سحر دانم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>میان ماوشما پرده سیف فرغانیست</p></div>
<div class="m2"><p>اگر چه بی خبرم ازتو این قدر دانم</p></div></div>