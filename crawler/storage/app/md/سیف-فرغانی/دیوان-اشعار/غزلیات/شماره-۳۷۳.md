---
title: >-
    شمارهٔ ۳۷۳
---
# شمارهٔ ۳۷۳

<div class="b" id="bn1"><div class="m1"><p>نگارا با رخ خوبت نه من تنها هوس دارم</p></div>
<div class="m2"><p>برای شکری چون تو چو خود چندین مگس دارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سعادت درد عشق تو بصاحب دولتان بخشد</p></div>
<div class="m2"><p>مرا آن بخت خود نبود ولیکن این هوس دارم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بهای گوهر وصلت نباشد پادشاهانرا</p></div>
<div class="m2"><p>مرا آن کی شود حاصل که جانی دست رس دارم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نظر کردم بهر کویی سگی چندین کسان دارد</p></div>
<div class="m2"><p>بنزد آنکه کس باشد سگم گر جز تو کس دارم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ببوسه وعده یی کردی برآمد سالها تا من</p></div>
<div class="m2"><p>ببوی آن شکرجانی چو طوطی در قفس دارم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>میان وصل و هجر امروز چون صبحست حال من</p></div>
<div class="m2"><p>ز پیشم شب چه می بینی که خورشیدی ز پس دارم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عجب نبود گر از شعرم بمردم در فتد شوری</p></div>
<div class="m2"><p>دلم با عشق تو گرمست و سوزی در نفس دارم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز عشقت خاک اران را بآب چشم تر کردم</p></div>
<div class="m2"><p>من مسکین ندانستم که در دیده ارس دارم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بذکر سیف فرغانی سخن را گر بیالایم</p></div>
<div class="m2"><p>سزد تا مستمع داند که من در آب خس دارم</p></div></div>