---
title: >-
    شمارهٔ ۳۹۰
---
# شمارهٔ ۳۹۰

<div class="b" id="bn1"><div class="m1"><p>ای عارض و خط تو شده صبح و شام چشم</p></div>
<div class="m2"><p>دل گشته خاص تو زنظرهای عام چشم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا عشق تو درین دل تاریک ره برد</p></div>
<div class="m2"><p>شد نور شمع تعبیه در پیه خام چشم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زآن ساعت خجسته که هندوی چشم کرد</p></div>
<div class="m2"><p>دل را غلام روی تو ای من غلام چشم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ابرو مثال لعبت بی جان دیده خواست</p></div>
<div class="m2"><p>کز شوق دیدن تو برآید ببام چشم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون ازدحام حاجی تشنه بود برآب</p></div>
<div class="m2"><p>بر آفتاب چشمه رویت زحام چشم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون چشم تو خراب دلم مست شد ازآنک</p></div>
<div class="m2"><p>هردم می مشاهده نوشد بجام چشم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون حسم دام دیده گشادست بنده را</p></div>
<div class="m2"><p>تا درفتد خیال تو دل را بدام چشم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر روی تو ببیند ازین پس بخون خویش</p></div>
<div class="m2"><p>برسیم اشک سکه زند دل بنام چشم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ترک خیال تو چو برفتن کند نشاط</p></div>
<div class="m2"><p>گلگون الاغ اشک ستاند ز یام چشم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چشمم زگریه گر برود هست در سرم</p></div>
<div class="m2"><p>نور خیال روی تو قایم مقام چشم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ای دل زدیده آب روان دار تا برد</p></div>
<div class="m2"><p>روزی بسوی خاک در او سلام چشم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ابر فراق چون مه رویش زتو نهفت</p></div>
<div class="m2"><p>رو اشک چون ستاره ببار از غمام چشم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>روزی بسر درآورد اسب نظر ترا</p></div>
<div class="m2"><p>زآن رخ اگر کشیده نداری زمام چشم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>این رمز راز دیده توان گوش ساختن</p></div>
<div class="m2"><p>زیرا اشارتست سراسر کلام چشم</p></div></div>