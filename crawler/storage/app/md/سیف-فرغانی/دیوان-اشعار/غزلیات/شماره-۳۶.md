---
title: >-
    شمارهٔ ۳۶
---
# شمارهٔ ۳۶

<div class="b" id="bn1"><div class="m1"><p>ما را دلیست سوخته آتش طلب</p></div>
<div class="m2"><p>آتش که دید پرتو او آب را سبب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زاشکم مدام سوزش دل در زیاد تست</p></div>
<div class="m2"><p>این آب هست هیزم آن آتش طلب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر عاشقی بمیل سهر در دو چشم کش</p></div>
<div class="m2"><p>کحل کلام هر سحر از سرمه دان شب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای غافلان ز عشق کفرتم بذنبکم</p></div>
<div class="m2"><p>وی عاشقان دوست اتیتم بما وجب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در وصف حسن دوست چو خواهی دهن گشود</p></div>
<div class="m2"><p>اول زبان عشق بیار و لب ادب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>وآنگه هزار خوشه معنی طب ز جان</p></div>
<div class="m2"><p>کندر درون سنبله صورتست حب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای سحر غمزهای ترا سامری غلام</p></div>
<div class="m2"><p>وی شکر حدیث ترا خامشی قصب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>وی پایه ولای تو بالاترین مقام</p></div>
<div class="m2"><p>وی نسبت هوای تو عالیترین نسب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نارالله است عشق تو چون کوره جحیم</p></div>
<div class="m2"><p>شهرالله است روی تو همچون مه رجب</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در آرزوی میوه باغ وصال تو</p></div>
<div class="m2"><p>هرگز نگشت غوره اومید ما عنب</p></div></div>