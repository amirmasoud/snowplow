---
title: >-
    شمارهٔ ۵۳۹
---
# شمارهٔ ۵۳۹

<div class="b" id="bn1"><div class="m1"><p>هم دلبر من با من دلدار شود روزی</p></div>
<div class="m2"><p>هم گلشن بخت من بی خار شود روزی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اندر ره او نبود جان کندن من ضایع</p></div>
<div class="m2"><p>آنکس که دلم بستد دلدار شود روزی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خود را بامید آن دلشاد همی دارم</p></div>
<div class="m2"><p>کآنکس که غمش خوردم غمخوار شود روزی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>باشد که شبی ما را شکری بود از وصلش</p></div>
<div class="m2"><p>ور نی گله از هجرش ناچار شود روزی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بنمود مرا عشقش آسان و ندانستم</p></div>
<div class="m2"><p>کین کار بدین غایت دشوار شود روزی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همچون نفس عیسی در مرده دمد روحی</p></div>
<div class="m2"><p>آنکس که ز عشق او بیمار شود روزی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از سکه مهر او مهر ار چو درم گیری</p></div>
<div class="m2"><p>از نقد تو هریک جو دینار شود روزی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دزدیده دل خود را بر خنجر اوزن سیف</p></div>
<div class="m2"><p>کآن مرغ که نکشندش مردار شود روزی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بی ماه رخش بختم شبهاست که می خسبد</p></div>
<div class="m2"><p>دولت بود ار ناگه بیدار شود روزی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آن غم که همی آمد از هر طرفی ده ده</p></div>
<div class="m2"><p>گر کم نکند یک یک بسیار شود روزی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>با محنت عشق تو امید همی دارم</p></div>
<div class="m2"><p>کین دولت سرمستم هشیار شود روزی</p></div></div>