---
title: >-
    شمارهٔ ۱۰۸
---
# شمارهٔ ۱۰۸

<div class="b" id="bn1"><div class="m1"><p>گشت روی زمین چو صحن بهشت</p></div>
<div class="m2"><p>از رخ خوب یار حور سرشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دیده از دل کن وببین دیدار</p></div>
<div class="m2"><p>ای قصارای همت تو بهشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر گل از روی لاله رخ که نمود</p></div>
<div class="m2"><p>بر مه از مشک سوده خط که نوشت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حسن رویش زخط نگردد کم</p></div>
<div class="m2"><p>رخ ماه از کلف نگردد زشت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یار من در میانه خوبان</p></div>
<div class="m2"><p>همچو لاله است در میانه کشت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر رخ لاله رنگ او خالیست</p></div>
<div class="m2"><p>همچو نقطه بر آتش از انگشت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عشق او در دل آن اثر دارد</p></div>
<div class="m2"><p>کآب در خاک و آتش اندر خشت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون منی ذکر غیر او نکند</p></div>
<div class="m2"><p>کرم قز ریسمان نداند رشت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دل نگهدار سیف فرغانی</p></div>
<div class="m2"><p>زآنکه در کعبه بت نشاید هشت</p></div></div>