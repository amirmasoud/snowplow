---
title: >-
    شمارهٔ ۴۹۳
---
# شمارهٔ ۴۹۳

<div class="b" id="bn1"><div class="m1"><p>تویی از اهل معنی بازمانده</p></div>
<div class="m2"><p>چنین از دین بدنیی بازمانده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بدین صورت که جانی نیست دروی</p></div>
<div class="m2"><p>مدام از راه معنی بازمانده</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زمعنی بی خبر چون اهل صورت</p></div>
<div class="m2"><p>چرایی ای بدعوی بازمانده</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ازآن دلبر که شیرین تر زجانست</p></div>
<div class="m2"><p>چو مجنونی ز لیلی بازمانده</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زیارانی که ازتو پیش رفتند</p></div>
<div class="m2"><p>بران تا بگذری ای بازمانده</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو طور ازنور رویش بهره دارد</p></div>
<div class="m2"><p>چرایی از تجلی بازمانده</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو پر همتت کنده است ازآنی</p></div>
<div class="m2"><p>از آن درگاه اعلی بازمانده</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>میان اینچنین دجال فعلان</p></div>
<div class="m2"><p>تو چون مریم زعیسی بازمانده</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زیاران سیف فرغانی درین ره</p></div>
<div class="m2"><p>چو هارونی زموسی بازمانده</p></div></div>