---
title: >-
    شمارهٔ ۳۵۷
---
# شمارهٔ ۳۵۷

<div class="b" id="bn1"><div class="m1"><p>آنی که کس بخوبی تو من ندیده ام</p></div>
<div class="m2"><p>خورشید را چو روی تو روشن ندیده ام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یا خود چو روی خوب تو رو نیست در جهان</p></div>
<div class="m2"><p>یا هست و زاشتغال بتو من ندیده ام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رنگی ز حسن در گل رویت نهاده اند</p></div>
<div class="m2"><p>کندر شکوفهای ملون ندیده ام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>روی تو گلستان و دهان غنچه یی کزو</p></div>
<div class="m2"><p>الا بوقت خنده شکفتن ندیده ام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>روی ترا بزینت وزیب احتیاج نیست</p></div>
<div class="m2"><p>من احتیاج شمع بروغن ندیده ام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گویی بتن که آب روان زو خجل شود</p></div>
<div class="m2"><p>جان مجسمی که چنین تن ندیده ام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از کشتنم بتیغ تو ای دوست حاصلست</p></div>
<div class="m2"><p>ذوقی که در هزیمت دشمن ندیده ام</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خود را بکام خویش شبی از سر رضا</p></div>
<div class="m2"><p>با چون تو دوست دست بگردن ندیده ام</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زآن سان که سیف بر کوی تو خوار ماند</p></div>
<div class="m2"><p>خاشاک راه بر در گلخن ندیده ام</p></div></div>