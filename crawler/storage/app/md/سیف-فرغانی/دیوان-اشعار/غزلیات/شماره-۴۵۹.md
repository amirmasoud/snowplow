---
title: >-
    شمارهٔ ۴۵۹
---
# شمارهٔ ۴۵۹

<div class="b" id="bn1"><div class="m1"><p>چو هیچ می نکنی التفات با ما تو</p></div>
<div class="m2"><p>چه فایده است درین التفات ما با تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>برای چیست تکاپوی من بهر طرفی</p></div>
<div class="m2"><p>چو در میانه مسافت همین منم تا تو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز بس که خلعت عشق تو جان من پوشید</p></div>
<div class="m2"><p>خیالم است که در جامه این منم یا تو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بچشم معنی چندانکه باز می نگرم</p></div>
<div class="m2"><p>ز روی نسبت ما قطره ایم و دریا تو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پس این تویی و منی در میانه چندانست</p></div>
<div class="m2"><p>که قطره بحر ببیند تو ما شوی ما تو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ترا ببردن دلهای خلق معجزه ییست</p></div>
<div class="m2"><p>که دلبران همه سحرند و دست بیضا تو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اجل بکشتن من قصد داشت، عشقت گفت</p></div>
<div class="m2"><p>که این وظیفه از آن منست فرما تو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شب وصال دهان بر لبم نهادی و گفت</p></div>
<div class="m2"><p>منم بلب شکر و طوطی شکر خا تو</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بدانکه هست ترا با دهان من نسبت</p></div>
<div class="m2"><p>که در جهان بسخن می شوی هویدا تو</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>فدا کند پس ازین جان و دل بدست آرد</p></div>
<div class="m2"><p>چو دید بنده که در دل همی کنی جا تو</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز فرقت تو چو مرده است سیف فرغانی</p></div>
<div class="m2"><p>توی بوصل خود این مرده را مسیحا تو</p></div></div>