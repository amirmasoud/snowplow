---
title: >-
    شمارهٔ ۱۱۰
---
# شمارهٔ ۱۱۰

<div class="b" id="bn1"><div class="m1"><p>کسی کو غم عشق جانان نداشت</p></div>
<div class="m2"><p>چو زنده نفس می زد و جان نداشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گدای توام ای توانگر بحسن</p></div>
<div class="m2"><p>چنین مملکت هیچ سلطان نداشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تویی آن شفایی که بیمار دل</p></div>
<div class="m2"><p>بجز درد تو هیچ بیمار نداشت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دلی را که اندوه تو جمع کرد</p></div>
<div class="m2"><p>غم هر دو کونش پریشان نداشت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از آن مشتغل شد بشیرین خود</p></div>
<div class="m2"><p>که خسرو چو تو شکرستان نداشت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بملک ارسکندر بود مفلس است</p></div>
<div class="m2"><p>که همچون خضر آب حیوان نداشت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بخارست جانی که عاشق نشد</p></div>
<div class="m2"><p>دخانست ابری که باران نداشت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>غم عشق خور سیف اگر زنده ای</p></div>
<div class="m2"><p>هر آنکس که این غم نخورد آن نداشت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مرو بی محبت که مفتی عشق</p></div>
<div class="m2"><p>چنین مؤمنی را مسلمان نداشت</p></div></div>