---
title: >-
    شمارهٔ ۴
---
# شمارهٔ ۴

<div class="b" id="bn1"><div class="m1"><p>چنان بوصل تو میلیست خاطر ما را</p></div>
<div class="m2"><p>که دل بصحبت یوسف کشد زلیخا را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیا بیا که بشب چون چراغ درخوردست</p></div>
<div class="m2"><p>بروز شمع جمال تو مجلس ما را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ترا بصحبت ما هیچ رغبتی باشد</p></div>
<div class="m2"><p>اگر بود بنمک احتیاج حلوا را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز خاک درگهت ابرام دور می دارم</p></div>
<div class="m2"><p>که آب درنفزاید ز سیل دریا را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بوصف حسنت اگر دم نمی زنم شاید</p></div>
<div class="m2"><p>که نیست حاجت مشاطه روی زیبا را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جفا و ناز بیکبارگی مکن امروز</p></div>
<div class="m2"><p>ذخیره کن قدری زین متاع فردا را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زلعل خود شکری، من گشاده می گویم،</p></div>
<div class="m2"><p>بده وگرنه میان بسته ایم یغما را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مرا ز لعل تو یک بوسه آرزو کردن</p></div>
<div class="m2"><p>سزد که عرصه فراخست مر تمنا را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز جام عشق تو مستم چنانکه بر رویت</p></div>
<div class="m2"><p>بوقت بوسه فراموش می کنم جا را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بوصل خویشم دی وعده کرده ای و امروز</p></div>
<div class="m2"><p>چنین غزل زرهی بس بود تقاضا را</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز بهر تاج وصال تو سیف فرغانی</p></div>
<div class="m2"><p>(شب فراق نخواهد دواج دیبا را)</p></div></div>