---
title: >-
    شمارهٔ ۳۴۰
---
# شمارهٔ ۳۴۰

<div class="b" id="bn1"><div class="m1"><p>ای که در باغ جمالست رخ تو چون گل</p></div>
<div class="m2"><p>گل تو در سخن آورد مرا چون بلبل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر ببستان نگری ور بگلستان گذری</p></div>
<div class="m2"><p>باد بر خاک نهد پیش تو رخساره گل</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نیست با عز تو در کوی تو درویشی عار</p></div>
<div class="m2"><p>هست از بند غلامی تو آزادی ذل</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عشق تو تاختن آورد و مرا کرد شکار</p></div>
<div class="m2"><p>چیست عصفور که سیمرغ درو زد چنگل</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عجمی وار مرا سلسله در گردن کرد</p></div>
<div class="m2"><p>هندوی زلف تو ای ترک تتاری کاکل</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تشنه وصل ترا چند سبو زد بر سنگ</p></div>
<div class="m2"><p>طاق ابروی تو آن آب لطافت را پل</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر که بی راه بر عشق تو ره رفت او را</p></div>
<div class="m2"><p>زلف تو راه زد ای روی تو هادی سبل</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تو بدین حسن در ایام فزودی فتنه</p></div>
<div class="m2"><p>من بدین شعر در آفاق فگندم غلغل</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بسخن مرد ز عشاق تو نتواند شد</p></div>
<div class="m2"><p>همچو شاهین نشود خاد بزرین زنگل</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ادبم گفت خمش باش و دگر شعر مگو</p></div>
<div class="m2"><p>نتوانم که مرا شوق (تو) می گوید قل</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سیف فرغانی در زمره عشاق تو نیست</p></div>
<div class="m2"><p>اسب شطرنج بمیدان نرود با دلدل</p></div></div>