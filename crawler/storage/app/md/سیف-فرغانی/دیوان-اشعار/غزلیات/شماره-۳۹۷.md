---
title: >-
    شمارهٔ ۳۹۷
---
# شمارهٔ ۳۹۷

<div class="b" id="bn1"><div class="m1"><p>گر دست رسد روزی در پات سرافشانم</p></div>
<div class="m2"><p>هر چند نثارت را لایق نبود جانم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پیش گل سیمینت چون شاخ خزان دیده</p></div>
<div class="m2"><p>با این همه بی برگی از باد زرافشانم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفتم که بجمعیت چون آب روانم کن</p></div>
<div class="m2"><p>بادی که همی داری چون خاک پریشانم؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شکر از تو بدین نعمت ذکریست که کم گویم</p></div>
<div class="m2"><p>صبر از تو بدین طاقت کاریست که نتوانم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در کار تو از یاران هیچم مددی ناید</p></div>
<div class="m2"><p>ای جمله مدد از تو مگذار بدیشانم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر عشق بیک بازی صد جان ببرد از من</p></div>
<div class="m2"><p>دست آن منست ای جان چون با تو همی مانم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زآن صورت جان پرور یادم دهد ای دلبر</p></div>
<div class="m2"><p>هر نقش که می بینم هر حرف که می خوانم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون ابر بسی بودم گریان ز فراق تو</p></div>
<div class="m2"><p>ای گل بوصال خود چون غنچه بخندانم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شاهین جهانگیری از دام برون رفته</p></div>
<div class="m2"><p>با دست نمی آیی چندانت که می خوانم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>من بلبلم و هرگز زین شهره نوانکند</p></div>
<div class="m2"><p>بی برگی شاخ گل خامش بزمستانم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>من در طلب وصلت چون سیف نیم خاکی</p></div>
<div class="m2"><p>ریگم، نتوان کردن سیراب ببارانم</p></div></div>