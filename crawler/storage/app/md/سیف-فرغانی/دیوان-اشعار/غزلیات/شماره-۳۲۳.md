---
title: >-
    شمارهٔ ۳۲۳
---
# شمارهٔ ۳۲۳

<div class="b" id="bn1"><div class="m1"><p>سزد که صبر کنم بر فراق دلبر خویش</p></div>
<div class="m2"><p>ازآنکه وصلش ما را ندید در خور خویش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بلطف خواندن از خدمتش ندارم چشم</p></div>
<div class="m2"><p>چو راضیم که نراند بعنفم از بر خویش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بود بآب دهانش نیاز و خاک درش</p></div>
<div class="m2"><p>مرا برای لب خشک و دیده تر خویش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرا قلاده امید کرد در گردن</p></div>
<div class="m2"><p>زبس که همچو سگانم بداشت بر در خویش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز بهر بوسه پایش که دست می ندهد</p></div>
<div class="m2"><p>مرا بسی بسخن دفع کرد از سر خویش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دهانم ار بلب او رسد چه غم باشد</p></div>
<div class="m2"><p>ازآنکه طوطی خود پرورد بشکر خویش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دهد بنرخ سفال شکسته سیم درست</p></div>
<div class="m2"><p>کسی که سکه مهرش نگاشت بر زر خویش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خبر نداشت ز خوبی خویش تا اکنون</p></div>
<div class="m2"><p>که شد بمیل من آگه ز حسن منظر خویش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ببوستان شد و لایق ندید ریحان را</p></div>
<div class="m2"><p>بخادمی خط و زلف همچو عنبر خویش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>اگر فدای تو کردند هرکسی مالی</p></div>
<div class="m2"><p>بزر و سیم نمودند جمله جوهر خویش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چو مال نیست مرا جان همی دهم بپذیر</p></div>
<div class="m2"><p>که شرمسارم ازین تحفه محقر خویش</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بسان سعدی راضی است سیف فرغانی</p></div>
<div class="m2"><p>گرش قبول کنی ور برانی از بر خویش</p></div></div>