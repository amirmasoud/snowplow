---
title: >-
    شمارهٔ ۱۳۲
---
# شمارهٔ ۱۳۲

<div class="b" id="bn1"><div class="m1"><p>زهی با لعل تو شهد و شکر هیچ</p></div>
<div class="m2"><p>خهی با روی تو شمس و قمر هیچ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>لبم را بر لبت نه تا ببینم</p></div>
<div class="m2"><p>که با او نسبتی دارد شکر هیچ</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دهانت دیدم وآن گشت باطل</p></div>
<div class="m2"><p>که می گفتم نیاید در نظر هیچ</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>وزین معنی عجب دارم که چون من</p></div>
<div class="m2"><p>جهانی دل نهادستند بر هیچ</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز دندان تو نیز اندر شگفتم</p></div>
<div class="m2"><p>که چندین در نهان چونست در هیچ</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>درین مدت که از روی تو دورم</p></div>
<div class="m2"><p>که چون عمرت ندیدم برگذر هیچ</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شکیبایی و دل آبند و روغن</p></div>
<div class="m2"><p>ندارند الفتی با یکدگر هیچ</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تو مست حسن و من مست تو ونیست</p></div>
<div class="m2"><p>ترا از من مرا از خود خبر هیچ</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>همی ترسم که عشق سیف با تو</p></div>
<div class="m2"><p>شود چون کار دنیا سربسر هیچ</p></div></div>