---
title: >-
    شمارهٔ ۲۴۴
---
# شمارهٔ ۲۴۴

<div class="b" id="bn1"><div class="m1"><p>هرکه او نام تو گوید دم او نور شود</p></div>
<div class="m2"><p>ظلمت غیر تو از جان ودلش دور شود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آفتاب ارنبود از رخ چون خورشیدت</p></div>
<div class="m2"><p>سربسر عرصه آفاق پر از نور شود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ماه روی تو مدد گر نکند هر روزش</p></div>
<div class="m2"><p>روی خورشید سیه چون شب دیجور شود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نفس اگر زنده بود نفحه عشقش نکشد</p></div>
<div class="m2"><p>وردلی مرده بود زنده بدین صور شود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هجرت جان زتن خود نبود بر ما مرگ</p></div>
<div class="m2"><p>مردن آنست که عاشق زتو مهجور شود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر کسی عشق نورزد بچه گردد کامل</p></div>
<div class="m2"><p>ور کسی خمر ننوشد بچه مخمور شود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مرد شیرین شد چون عشق در او شور فگند</p></div>
<div class="m2"><p>برهد از ترشی غوره چو انگور شود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جهد آن کن که ترا طعمه خود سازد عشق</p></div>
<div class="m2"><p>شهد گردد گل چون طعمه زنبور شود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>عشق بر هر که تجلی کند آن کس همه جای</p></div>
<div class="m2"><p>بکرامت چو کلیم وبشرف طور شود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هر که را عشق تو بیمار کند همچو مسیح</p></div>
<div class="m2"><p>نفس او سبب صحت رنجور شود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سیف فرغانی اگر مست می عشق شوی</p></div>
<div class="m2"><p>صد خرابی بیکی بیت تو معمور شود</p></div></div>