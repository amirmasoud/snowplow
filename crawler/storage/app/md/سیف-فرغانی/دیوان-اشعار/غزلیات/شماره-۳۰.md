---
title: >-
    شمارهٔ ۳۰
---
# شمارهٔ ۳۰

<div class="b" id="bn1"><div class="m1"><p>عاشق روی توام از من مپوش آن روی را</p></div>
<div class="m2"><p>پرده بردار از رخ و بر رو میفگن موی را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا بروز وصل تو چشمش نبیند روی خواب</p></div>
<div class="m2"><p>هر که یک شب همچو من در خواب دید آن روی را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گرد میدان زمین سرگشته گردم همچو گوی</p></div>
<div class="m2"><p>من چو در میدان عشق تو فگندم گوی را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همتی دارم که گر دستم رسد هر ساعتی</p></div>
<div class="m2"><p>طوق زر در گردن اندازم سگ آن کوی را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عشق سری بود پنهان رنگ رو پیداش کرد</p></div>
<div class="m2"><p>مشک اگر پنهان بود پنهان ندارد بوی را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>من ز مشتاقان آن رویم ازیرا خوش بود</p></div>
<div class="m2"><p>با رخ نیکوی گل مر بلبل خوش گوی را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تیر باران غمش را پیشوا رفتم به صبر</p></div>
<div class="m2"><p>جز سپر نکند تحمل تیغ رویاروی را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دل همی جوید نگارم تا ستاند جان ز من</p></div>
<div class="m2"><p>دل به ترک جان بجوی آن دلبر دلجوی را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سبزه مژگان بماند بر کنار جوی چشم</p></div>
<div class="m2"><p>کآب هر دم جو شود آن چشم همچون جوی را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سیف فرغانی برو تصدیق سعدی کن که گفت</p></div>
<div class="m2"><p>«من بدین خوبی و زیبایی ندیدم روی را»</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بنده گر نیکست و گر بد در سخن نیکت ستود</p></div>
<div class="m2"><p>نزد نیکویان جزا بد نیست نیکوگوی را</p></div></div>