---
title: >-
    شمارهٔ ۵۷۵
---
# شمارهٔ ۵۷۵

<div class="b" id="bn1"><div class="m1"><p>تو قبله دل و جانی چو روی بنمایی</p></div>
<div class="m2"><p>بطوع سجده کنندت بتان یغمایی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو آفتابی واین هست حجتی روشن</p></div>
<div class="m2"><p>که در تو خیره شود دیده تماشایی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بوصف حسن تو لایق نباشد ار گویم</p></div>
<div class="m2"><p>بنفشه زلفی و گل روی و سرو بالایی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز روی پرده برانداز تا جهانی را</p></div>
<div class="m2"><p>بهار وار بگل سربسر بیارایی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چگونه با تو دگر عشق من کمی گیرد</p></div>
<div class="m2"><p>که لحظه لحظه تو در حسن می بیفزایی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بدست عشق درافگند همچو مرغ بدام</p></div>
<div class="m2"><p>کمند عشق تو هر جا دلیست سودایی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>برآستان تو هستند عاشقان چندان</p></div>
<div class="m2"><p>که پای بر سر خود می نهم ز بی جایی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بلطف بر سر وقت من آ که در طلبت</p></div>
<div class="m2"><p>ز پا درآمدم و تو بدست می نایی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بهجر دور نیم از تو زآنکه هر نفسم</p></div>
<div class="m2"><p>چو فکر در دل و در دیده ای چو بینایی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>اگر چه ملک نخواهد شریک، نتوانم</p></div>
<div class="m2"><p>که روز و شب غم تو من خورم بتنهایی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>در آمدن ز در دوست سیف فرغانی</p></div>
<div class="m2"><p>میسرت نشود تا ز خود برون نایی</p></div></div>