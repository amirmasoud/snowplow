---
title: >-
    شمارهٔ ۲۷۶
---
# شمارهٔ ۲۷۶

<div class="b" id="bn1"><div class="m1"><p>ای نامه نو رسیده از یار</p></div>
<div class="m2"><p>بی گوش سخن شنیده از یار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در طی تو گر هزار قهرست</p></div>
<div class="m2"><p>لطفیست بمن رسیده از یار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>این جان جفا کشیده از تو</p></div>
<div class="m2"><p>ای بوی وفا شنیده از یار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>وی دیده هر آنچه گفته از دوست</p></div>
<div class="m2"><p>وی گفته هر آنچه دیده از یار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هرگز باشد که چون سوادت</p></div>
<div class="m2"><p>پر نور کنیم دیده از یار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اندر شب هجر مطلع تو</p></div>
<div class="m2"><p>صبحیست ولی دمیده از یار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای حظ نظر گرفته از دوست</p></div>
<div class="m2"><p>وی ذوق سخن چشیده از یار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر باز روی ز من بگویش</p></div>
<div class="m2"><p>کای بی سببی رمیده از یار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>انصاف بده که چون بود سیف</p></div>
<div class="m2"><p>پیوسته چنین بریده از یار</p></div></div>