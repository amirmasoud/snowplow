---
title: >-
    شمارهٔ ۸۹
---
# شمارهٔ ۸۹

<div class="b" id="bn1"><div class="m1"><p>دوست سلطان و دل ولایت اوست</p></div>
<div class="m2"><p>خرم آن دل که در حمایت اوست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر کرا دل بعشق اوست گرو</p></div>
<div class="m2"><p>از ازل تا ابد ولایت اوست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پس نماند ز سابقان در راه</p></div>
<div class="m2"><p>هر کرا پیش رو هدایت اوست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عرش بر آستانش سر بنهد</p></div>
<div class="m2"><p>هر کرا تکیه بر عنایت اوست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در دو عالم ز کس ندارد خوف</p></div>
<div class="m2"><p>هرکه در مأمن رعایت اوست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون ز غایات کون در گذرد</p></div>
<div class="m2"><p>این قدم در رهش بدایت اوست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>منتها اوست طالب او را</p></div>
<div class="m2"><p>مقبل آنکس که او نهایت اوست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>با خود از بهر او جهاد کند</p></div>
<div class="m2"><p>اسدالله که شیر رایت اوست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گو مکن وقف هیچ جا گر چه</p></div>
<div class="m2"><p>مصحف کون پر زآیت اوست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خود عبارت نمی توان کردن</p></div>
<div class="m2"><p>زآنچه آن انتها و غایت اوست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سیف فرغانی ار سخن شنود</p></div>
<div class="m2"><p>اندکی زین نمط کفایت اوست</p></div></div>