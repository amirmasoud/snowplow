---
title: >-
    شمارهٔ ۳۳۰
---
# شمارهٔ ۳۳۰

<div class="b" id="bn1"><div class="m1"><p>ای ز عشقت مهر و مه سرگشته در گردون خویش</p></div>
<div class="m2"><p>وی ببویت روز و شب آواره در هامون خویش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در هوای عشق تو چون ذره زآن گردان شدم</p></div>
<div class="m2"><p>کآفتاب حسن تو می تابد از گردون خویش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در پس جلباب شب هر صبح روشن رو کنی</p></div>
<div class="m2"><p>آفتاب تیره ار از ماه روزافزون خویش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از گریبان افق پیداست کز عشقت مدام</p></div>
<div class="m2"><p>آسمان دامن کشان می گردد اندر خون خویش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یار صاحب حسن و ما در دست او چون آینه</p></div>
<div class="m2"><p>چون ببیند آینه شاهد بود مفتون خویش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تند باشد شاهدی کآگه بود از حسن خود</p></div>
<div class="m2"><p>صعب باشد عشق چون لیلی شود مجنون خویش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عاشقان را در درون شمع است و شاهد رو ولیک</p></div>
<div class="m2"><p>چون توان کردن صفت از شاهد بیچون خویش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کفر باشد مرد را بیرون شدن از اندرون</p></div>
<div class="m2"><p>گر هزاران شمع و شاهد بیند از بیرون خویش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نور گیرد دم بدم هر ذره از خورشید خود</p></div>
<div class="m2"><p>فیض یابد بی گمان هر قطره از جیحون خویش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>روی او کآفاق روشن زوست، هرشب می کند</p></div>
<div class="m2"><p>چشم را خیره ز پرتوهای گوناگون خویش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چون غلام عشق گشتی و شد آزاد از دو کون</p></div>
<div class="m2"><p>پس مبارک بنده یی در خدمت میمون خویش</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>عاشق رویش اگر موزون نباشد گو مباش</p></div>
<div class="m2"><p>زآنکه ناموزون او بودن به از موزون خویش</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>مر ترا فرعون او بودن به از موسی خود</p></div>
<div class="m2"><p>مر ترا هامان او بودن به از هارون خویش</p></div></div>