---
title: >-
    شمارهٔ ۵۰۲
---
# شمارهٔ ۵۰۲

<div class="b" id="bn1"><div class="m1"><p>تا نمودی روی و دیدم گرد چشمت آن مژه</p></div>
<div class="m2"><p>می کنم در حسرت چشم تو خون باران مژه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گرخوهی تا بنگری بیمار تیرانداز را</p></div>
<div class="m2"><p>بنگر اندر روی خود وآن چشم بین وآن مژه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تاکمان ابرو اندر قبضه حکمش فتاد</p></div>
<div class="m2"><p>چشم شوخت کرد تیر غمزه را پیکان مژه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرغ دل برآتش سودای تو کردم کباب</p></div>
<div class="m2"><p>زآنکه شد چشم جگرخوار ترا دندان مژه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دلربای وخوب چون ابروست بر بالای چشم</p></div>
<div class="m2"><p>زیر ابروی تو ای بر نیکوان سلطان مژه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شورم اندر جان شیرین اوفتد فرهادوار</p></div>
<div class="m2"><p>هرکجا بر هم زنی ای خسرو خوبان مژه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دیده خون بارست تا ز آن چشم شیرافگن شدست</p></div>
<div class="m2"><p>گوسپند صبر ما را پنجه گرگان مژه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بر زمین خشک بارد ابر رحمت هر کجا</p></div>
<div class="m2"><p>عاشقی راتر شود از دیده گریان مژه</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تا ترا ز آن پسته خندان شکر بارست لب</p></div>
<div class="m2"><p>بنده را زین چشم گریانست خون افشان مژه</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نسبت چشمت بنرگس کرد نتوانم چنان</p></div>
<div class="m2"><p>کز برای چشم نرگس ساختن نتوان مژه</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خوش بخسبد فتنه چون در قند ز روسی کشد</p></div>
<div class="m2"><p>چشم هندوی ترا ای ماه ترکستان مژه</p></div></div>