---
title: >-
    شمارهٔ ۵۷۷
---
# شمارهٔ ۵۷۷

<div class="b" id="bn1"><div class="m1"><p>ز دلبران همه شهر دل پذیر تویی</p></div>
<div class="m2"><p>مرا ز جمله گزیر است و ناگزیر تویی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز دیگران سخنی بر زبان رود هر وقت</p></div>
<div class="m2"><p>ولی مدام چو اندیشه در ضمیر تویی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پیاده اند نکویان ز نطع دل بیرون</p></div>
<div class="m2"><p>کنون چو شاه درین خانه جایگیر تویی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سمن بران همه با کثرتی که ایشانراست</p></div>
<div class="m2"><p>ترا رعیت فرمان برند امیر تویی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همه روایت منظومه حکایاتند</p></div>
<div class="m2"><p>ترا چه شرح دهم جامع کبیر تویی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بنام حسن تو از بهر شعر چون زر خرد</p></div>
<div class="m2"><p>زدیم سکه که سلطان این سریر تویی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بدین جمال (چو) خورشید می توانی گفت</p></div>
<div class="m2"><p>که آفتاب منم ذره حقیر تویی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زمین بدور تو چون آسمان شد و در وی</p></div>
<div class="m2"><p>مه تمام بدان روی مستدیر تویی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اگر سراج منیرست بر فلک بر ما</p></div>
<div class="m2"><p>قمر بلمعه چراغی بود منیر تویی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>لبت بنکته بسی آب و خمر در هم ریخت</p></div>
<div class="m2"><p>مگر ز جوی بهشت انگبین و شیر تویی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز بعد آن همه الفاظ مدح در حق تو</p></div>
<div class="m2"><p>که از معانی آن یک بیک خبیر تویی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>رقیب بی نمکت را سزد اگر گویم</p></div>
<div class="m2"><p>که بهر کوفتن ای ترش روی سیر تویی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>مرا هوای تو دی گفت سیف فرغانی</p></div>
<div class="m2"><p>ز قید ما دگران مطلقند اسیر تویی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>برای وقت جوانان کنون که سعدی رفت</p></div>
<div class="m2"><p>سخن بگو که درین خانقان پیر تویی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ملک مجیر و ملک دم بدم ظهیرت باد</p></div>
<div class="m2"><p>که زیر چرخ نخستین دوم اثیر تویی</p></div></div>