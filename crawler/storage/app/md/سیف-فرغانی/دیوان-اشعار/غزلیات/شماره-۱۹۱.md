---
title: >-
    شمارهٔ ۱۹۱
---
# شمارهٔ ۱۹۱

<div class="b" id="bn1"><div class="m1"><p>هرچند دیده هرگز رویت ندیده باشد</p></div>
<div class="m2"><p>جز روی تو نبیند آنراکه دیده باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در خوبی رخ تو من تیره دل چه گویم</p></div>
<div class="m2"><p>کآیینه همچو رویت رویی ندیده باشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر روی تو زبستان روزی خراج خواهد</p></div>
<div class="m2"><p>گل از میانه جان زر برکشیده باشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون عارض تو بیند نرگس بلاله گوید</p></div>
<div class="m2"><p>هرگز بنفشه بر گل زین سان دمیده باشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای در عرق ز خوبی رخسار لاله رنگت</p></div>
<div class="m2"><p>همچون گلی که بر وی باران چکیده باشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حال دل حزینم زآنکس بپرس کو را</p></div>
<div class="m2"><p>دل از درون و آرام از دل رمیده باشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر بوی وصل هجران آنکس کند تحمل</p></div>
<div class="m2"><p>کو بر امید شکر زهری چشیده باشد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آنکس نکو شناسد حال دل زلیخا</p></div>
<div class="m2"><p>کو از برای یوسف دستی بریده باشد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گفتی بصبر می کن با هجر سازگاری</p></div>
<div class="m2"><p>بی وصل دوست عاشق چون آرمیده باشد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بر دامگاه عشقت مرغی فرو نیاید</p></div>
<div class="m2"><p>کز طبل باز هجرت بانگی شنیده باشد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سیف ار غزل سراید در وصف صورت تو</p></div>
<div class="m2"><p>یک بیت او بمعنی چندین قصیده باشد</p></div></div>