---
title: >-
    شمارهٔ ۳۸۰
---
# شمارهٔ ۳۸۰

<div class="b" id="bn1"><div class="m1"><p>از عشق دل افروزم چون شمع همی سوزم</p></div>
<div class="m2"><p>چون شمع همی سوزم ازعشق دل افروزم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ازگریه وسوز من اوفارغ ومن هر شب</p></div>
<div class="m2"><p>چون شمع زهجر او می گریم و می سوزم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>درخانه گرم هر شب ازماه بود شمعی</p></div>
<div class="m2"><p>بی روی چو خورشیدت چون شب گذرد روزم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در عشق که مردم رااز پوست برون آرد</p></div>
<div class="m2"><p>ازشوق شود پاره هر جامه که بر دوزم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر چند فقیرم من گر دوست مرا باشد</p></div>
<div class="m2"><p>چون گنج غنی باشم گر مال بیندوزم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دانش نکند یاری در خدمت او کس را</p></div>
<div class="m2"><p>من خدمت او کردن از عشق وی آموزم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون سیف اگر باشم در صحبت آن شیرین</p></div>
<div class="m2"><p>خسرو نزند پنجه با دولت پیروزم</p></div></div>