---
title: >-
    شمارهٔ ۲۱۰
---
# شمارهٔ ۲۱۰

<div class="b" id="bn1"><div class="m1"><p>مه و خورشید اگرچه رخ نیکو دارند</p></div>
<div class="m2"><p>پیش آن روی نکو صورت بر دیوارند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گل رخسار ترا میوه جمال و حسنست</p></div>
<div class="m2"><p>وین نکویان همه بی میوه چو اسپیدارند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آیت محکم این سوره تویی و دگران</p></div>
<div class="m2"><p>چون حروفند و ندانم که چه معنی دارند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حلقه زلف ترا هست بسی دل دربند</p></div>
<div class="m2"><p>بهر زنجیر تو دیوانه چومن بسیارند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دی یکی گفت که عشاق بنزد معشوق</p></div>
<div class="m2"><p>راست چون در دل دین دار چو دنیا دارند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفتم ایشان را چون چشم همی دارد دوست</p></div>
<div class="m2"><p>می نبینی که چو چشمش همگان بیمارند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>حذر از دیده مردم که ترا ومارا</p></div>
<div class="m2"><p>مردم دیده چو نیکو نگری اغیارند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شاید ار بر سر کوی تو بخسبند بروز</p></div>
<div class="m2"><p>که چو سگ بر در وبام تو بشب بیدارند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در ره خدمت اگر مال خوهی در بازند</p></div>
<div class="m2"><p>وز سر رغبت اگر جان طلبی بسپارند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>پاس امر تو چو روزه است ببایدشان داشت</p></div>
<div class="m2"><p>کار عشقت چو نمازست چرا نگزارند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از گل وخار نگوییم که عشاقت را</p></div>
<div class="m2"><p>خارها جمله گل اندر ره وگلها خارند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گر چونرگس همه چشمند نبینند ترا</p></div>
<div class="m2"><p>کور بختان که بر آیینه خود زنگارند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نگذارم که زمن فوت شود همچو نفس</p></div>
<div class="m2"><p>گرمرا باتو بیکجا نفسی بگذارند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گرچه در خدمت تو عمر بپایان نرسد</p></div>
<div class="m2"><p>عمر آنست که با دوست بپایان آرند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>سیف فرغانی هرکس که درین کار افتاد</p></div>
<div class="m2"><p>کار او دارد وچون تو دگران بی کارند</p></div></div>