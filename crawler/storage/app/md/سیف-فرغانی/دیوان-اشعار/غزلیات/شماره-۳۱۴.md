---
title: >-
    شمارهٔ ۳۱۴
---
# شمارهٔ ۳۱۴

<div class="b" id="bn1"><div class="m1"><p>آن دلارامی که آرامی نباشد با منش</p></div>
<div class="m2"><p>کرد شام عاشقان چون صبح روی روشنش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آستین از رو چو برگیرد ترا روشن شود</p></div>
<div class="m2"><p>کآفتاب حسن دارد مطلع از پیراهنش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زآن بحبل شعله خود همچو دلو از چاه آب</p></div>
<div class="m2"><p>روز و شب بر می کشد خورشید نور از روزنش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از گریبانش گلستان می برآید عیب نیست</p></div>
<div class="m2"><p>عاشقی گر همچو خار آویزد اندر دامنش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جامه بر خود می درم چون غنچه زآن دلبر که هست</p></div>
<div class="m2"><p>خرمنی گل در قبا و عالمی جان در تنش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر ز من بستد دلی آن دوست باطل جوی نیست</p></div>
<div class="m2"><p>زآنکه گر صد جان خوهد حقی است واجب بر منش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در کمان ابرو آورد و بسوی من فگند</p></div>
<div class="m2"><p>یار آهو چشم تیر غمزه شیر افگنش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>داشت پیش آتش رویش فتیلی از نظر</p></div>
<div class="m2"><p>زآن چراغ دیده را از آب و خون شد روغنش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>باد با تو چون شبی گر سوی بستان بگذرد</p></div>
<div class="m2"><p>همچو بلبل در نوا آید زبان سوسنش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خسروان او را غلامند این زمان در ملک روم</p></div>
<div class="m2"><p>همچو شیرین صد کنیزک عاشق اندر ارمنش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بر امید وصل او چون سیف فرغانی که دید</p></div>
<div class="m2"><p>طوطیی اندر قفس یا بلبلی در گلشنش</p></div></div>