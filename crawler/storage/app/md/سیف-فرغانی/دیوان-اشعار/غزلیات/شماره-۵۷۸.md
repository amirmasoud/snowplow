---
title: >-
    شمارهٔ ۵۷۸
---
# شمارهٔ ۵۷۸

<div class="b" id="bn1"><div class="m1"><p>عشق تو دردست و درمانش تویی</p></div>
<div class="m2"><p>هست عاشق صورت و جانش تویی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آنچه در درمان نیابد دردمند</p></div>
<div class="m2"><p>هست در دردی که درمانش تویی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سالک راه تو زاول واصلست</p></div>
<div class="m2"><p>کین ره از سر تا بپایانش تویی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عاشقت کی گنجد اندر پیرهن</p></div>
<div class="m2"><p>چون ز دامن تا گریبانش تویی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ما و تو این هر دو یک معنی بود</p></div>
<div class="m2"><p>کآشکارش ما و پنهانش تویی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عاشق روی ترا در دین عشق</p></div>
<div class="m2"><p>غیر تو کفرست و ایمانش تویی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دل بتو زنده است همچو تن بجان</p></div>
<div class="m2"><p>این خضر را آب حیوانش تویی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خوشه خوشه کشت هستی جوبجو</p></div>
<div class="m2"><p>زرع بی آبست و بارانش تویی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>این غزل شطح است و قوالش منم</p></div>
<div class="m2"><p>وین سخن حق است (و) برهانش تویی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>منطق الطیر سخنهای مرا</p></div>
<div class="m2"><p>کس نمی داند سلیمانش تویی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سیف فرغانی از آن بر نقد شعر</p></div>
<div class="m2"><p>سکه زین سان زد که سلطانش تویی</p></div></div>