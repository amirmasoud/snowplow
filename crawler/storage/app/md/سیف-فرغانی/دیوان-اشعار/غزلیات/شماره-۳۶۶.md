---
title: >-
    شمارهٔ ۳۶۶
---
# شمارهٔ ۳۶۶

<div class="b" id="bn1"><div class="m1"><p>نگارا تا ترا دیدم دل اندر کس نمی بندم</p></div>
<div class="m2"><p>ز خوبان منقطع کردی بری از خویش و پیوندم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بجز تو گر دل و جان را بود آرام و پیوندی</p></div>
<div class="m2"><p>دگر با دل نیارامم دگر با جان نپیوندم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تو داری روی همچون گل من شوریده چون بلبل</p></div>
<div class="m2"><p>برنگی از تو خشنودم ببویی از تو خرسندم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تو خورشیدی ز من پنهان و من با اشک چون باران</p></div>
<div class="m2"><p>گهی چون ابر می گریم گهی چون برق می خندم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چنان از آب چشمم تر که همچون عود در مجمر</p></div>
<div class="m2"><p>نسوزم گر بیندازی در آتش همچو اسپندم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>درخت صبر بنشاندم، چو دیدم مرغ دل بی تو</p></div>
<div class="m2"><p>بشاخ او تعلق کرد، از آنش بیخ برکندم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بلطف و حسن و زیبایی و عشق و صبر و شیدایی</p></div>
<div class="m2"><p>ترا شیرین نباشد مثل و خسرو نیست مانندم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اگر چون دوستان بر من کنی امری بجان (و تن)</p></div>
<div class="m2"><p>ز تو ای دلستان بر من چه حکم آید که نپسندم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مگر خورشید روی تو شعاعی بر من اندازد</p></div>
<div class="m2"><p>که بر خاک درت خود را بسی چون سایه افگندم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز بخت این چشم می دارم کزین پس شاخ نومیدی</p></div>
<div class="m2"><p>نیارد تخم امیدی که اندر دل پراگندم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز استاد و پدر میراث و علمم هست عشق تو</p></div>
<div class="m2"><p>اگر نااهل شاگردم و گر ناجنس فرزندم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>همه دیوانگان را بند زنجیرست و این طرفه</p></div>
<div class="m2"><p>که در زنجیر عشق تو دل دیوانه شد بندم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>درین عشقی ز جان خوشتر مرا از صد جهان خوشتر</p></div>
<div class="m2"><p>عدوی جان ستان خوشتر زیاری کو دهد پندم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بکوی سیف فرغانی اگر آیی بصد ناز آ</p></div>
<div class="m2"><p>خرامان از درم باز آ کت از جان آرزومندم</p></div></div>