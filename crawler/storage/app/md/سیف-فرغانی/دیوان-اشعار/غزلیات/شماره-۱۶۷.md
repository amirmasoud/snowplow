---
title: >-
    شمارهٔ ۱۶۷
---
# شمارهٔ ۱۶۷

<div class="b" id="bn1"><div class="m1"><p>گرترا بی عشق ازین سان زندگانی بگذرد</p></div>
<div class="m2"><p>وینچنین بی حاصل ایام جوانی بگذرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زآن همی ترسم که با صد تیرگی مانند سیل</p></div>
<div class="m2"><p>در جهان خاکت آب زندگانی بگذرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ازجهان عشق مگذر چون رسی آنجا ازآنک</p></div>
<div class="m2"><p>درخرابی میرد آن کز آبدانی بگذرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خویشتن درآتش عشقی فگن تا نفس تو</p></div>
<div class="m2"><p>در شرف زین مردم آبی ونانی بگذرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون همایان ملایک زین شترمرغان دهر،</p></div>
<div class="m2"><p>کز طبیعت چون خروسانند، رانی، بگذرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ازبرای قوت جان در دست میر اشکار عشق</p></div>
<div class="m2"><p>گوشت بیند زین سگان استخوانی بگذرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>راحت تن ترک کردن کار مرد زاهدست</p></div>
<div class="m2"><p>عاشق آن باشد که از راحات جانی بگذرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون جو حظ تو کم شد زآخر سنگین خاک</p></div>
<div class="m2"><p>اسب سیرت زین خران کاهدانی بگذرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آتش شوقت چو در دل تیز گردد جان تو</p></div>
<div class="m2"><p>همچو روح القدس ازین چرخ دخانی بگذرد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ترک دام و دانه کن تا روح چون شهباز تو</p></div>
<div class="m2"><p>آید اندر طیر و از سیر مکانی بگذرد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ثقل هستی دور کن از دوش جان کامید نیست</p></div>
<div class="m2"><p>کز سبک روحان کسی با این گرانی بگذرد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ای که گر در کویت آید سیف فرغانی دمی</p></div>
<div class="m2"><p>بی درنگ از حد ایام زمانی بگذرد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چون جهان سیارراهت یک هنر دارد که تو</p></div>
<div class="m2"><p>از مقامات آنچنانکش بگذرانی بگذرد</p></div></div>