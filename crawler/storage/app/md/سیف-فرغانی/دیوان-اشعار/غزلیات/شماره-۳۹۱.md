---
title: >-
    شمارهٔ ۳۹۱
---
# شمارهٔ ۳۹۱

<div class="b" id="bn1"><div class="m1"><p>آن توانگر بمعالی که منش درویشم</p></div>
<div class="m2"><p>کنه وصفش نه چنانست که می اندیشم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گل من مایه زخاک (سر) کویش دارد</p></div>
<div class="m2"><p>بگهر محتشمم گرچه بزر درویشم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من چو در دل ننشاندم به جز او چیزی را</p></div>
<div class="m2"><p>دوست برخاست وبنشاند بجای خویشم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هرچه آن دشمن بود چو افگندم پس</p></div>
<div class="m2"><p>اندرین راه جزآن دوست نیامد پیشم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا تونبض من بیمار نگیری ای دوست</p></div>
<div class="m2"><p>درد من دارو ومرهم نپذیرد ریشم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>من همان روز که روی تو بدیدم گفتم</p></div>
<div class="m2"><p>کآشنایی تو بیگانه کند با خویشم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فتنه دی تیز همی رفت کمان زه کرده</p></div>
<div class="m2"><p>گفت جز ابروی او تیر ندارد کیشم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از کسانی که درین کوی چو سگ نان خواهند</p></div>
<div class="m2"><p>کم توان یافت گدایی که من ازوی بیشم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سیف فرغانی ازین سان که گدا کرد ترا؟</p></div>
<div class="m2"><p>آن توانگر بمعالی که منش درویشم</p></div></div>