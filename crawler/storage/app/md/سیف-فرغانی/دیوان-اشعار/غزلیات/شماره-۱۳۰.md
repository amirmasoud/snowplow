---
title: >-
    شمارهٔ ۱۳۰
---
# شمارهٔ ۱۳۰

<div class="b" id="bn1"><div class="m1"><p>ای مه و خور بروی تو محتاج</p></div>
<div class="m2"><p>بر سر چرخ خاک پای تو تاج</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چه کنم وصف تو که مستغنیست</p></div>
<div class="m2"><p>مه ز گلگونه گل ز اسپیداج</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هرکه جویای تو بود همه روز</p></div>
<div class="m2"><p>همه شبهای او بود معراج</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پادشاهان که زر همی بخشند</p></div>
<div class="m2"><p>بگدایان کوی تو محتاج</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ندهد عاشق تو دل بکسی</p></div>
<div class="m2"><p>بکسی چون دهد خلیفه خراج</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عیب نبود تصلف از عاشق</p></div>
<div class="m2"><p>کفر نبود اناالحق از حلاج</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عشق را باک نیست از خون ریز</p></div>
<div class="m2"><p>ترک را رحم نیست در تاراج</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چاره با عشق نیست جز تسلیم</p></div>
<div class="m2"><p>خوف جانست با ملوک لجاج</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دل نیاید بتنگ از غم عشق</p></div>
<div class="m2"><p>کعبه ویران نگردد از حجاج</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دل بتو داد سیف فرغانی</p></div>
<div class="m2"><p>از نمد پاره دوخت بر دیباج</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سخن اهل ذوق می گوید</p></div>
<div class="m2"><p>بانگ بلبل همی کند دراج</p></div></div>