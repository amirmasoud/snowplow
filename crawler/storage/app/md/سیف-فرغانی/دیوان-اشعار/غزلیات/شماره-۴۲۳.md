---
title: >-
    شمارهٔ ۴۲۳
---
# شمارهٔ ۴۲۳

<div class="b" id="bn1"><div class="m1"><p>خوبان رعیت اند وتویی پادشاهشان</p></div>
<div class="m2"><p>ایشان همه ستاره و روی تو ماهشان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بی آفتاب روی تو همرنگ شب بود</p></div>
<div class="m2"><p>روز سپید خلق ز چشم سیاهشان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ایشان بتیر غمزه صف عقل بشکنند</p></div>
<div class="m2"><p>اکنون که گشت روی تو پشت سپاهشان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بالای این چه مرتبه باشد دگر که هست</p></div>
<div class="m2"><p>خورشید و ماه جمع بزیر کلاهشان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یوسف رخند و هر که چو یعقوب مستمند</p></div>
<div class="m2"><p>پوشیده چشم نیست درافتد بچاهشان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در دعوی هوای تو عشاق صادقند</p></div>
<div class="m2"><p>زیرا که هست شاهد رویت گواهشان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جان باختند با تو که بر نطع دلبری</p></div>
<div class="m2"><p>خوبان پیاده اند ورخ تست شاهشان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خال تو دیده اند و بزلف تو داده دل</p></div>
<div class="m2"><p>آن دانه در فگند درین دامگاهشان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>عشاق سوی کوی تو ره می نیافتند</p></div>
<div class="m2"><p>روی تو شعله یی زد و بنمود راهشان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>فردا که خلق را بعملها جزا دهند</p></div>
<div class="m2"><p>حیران شوند و کس نبود عذر خواهشان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گر هست عاشقان ترا صد چو سیف جرم</p></div>
<div class="m2"><p>ایزد بروی خوب تو بخشد گناهشان</p></div></div>