---
title: >-
    شمارهٔ ۲۰۴
---
# شمارهٔ ۲۰۴

<div class="b" id="bn1"><div class="m1"><p>دلی کز وصل جانان بازماند</p></div>
<div class="m2"><p>تنی باشد که از جان باز ماند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نگارینا منم بی روی خوبت</p></div>
<div class="m2"><p>شبی کز ماه تابان باز ماند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چه باشد حال آن بیچاره عاشق</p></div>
<div class="m2"><p>که از وصلت بهجران باز ماند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چه گردد ذره سرگشته راحال</p></div>
<div class="m2"><p>که از خورشید رخشان باز ماند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگر خورشید رخسار تو بیند</p></div>
<div class="m2"><p>درآن رخساره حیران بازماند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>وگر بار فراقت بروی افتد</p></div>
<div class="m2"><p>ز دور این چرخ گردان باز ماند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>غم تو قوت جان عاشقانست</p></div>
<div class="m2"><p>روا نبود کز ایشان بازماند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نه دست خلق راشاید عصایی</p></div>
<div class="m2"><p>که از موسی عمران باز ماند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کسی را دست آن خاتم نباشد</p></div>
<div class="m2"><p>کز انگشت سلیمان بازماند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>باسکندر کجا خواهد رسیدن</p></div>
<div class="m2"><p>گر از خضرآب حیوان بازماند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بزیر ران هر مردی نیاید</p></div>
<div class="m2"><p>چو رخش از پور دستان باز ماند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نگارا سیف فرغانیست بی تو</p></div>
<div class="m2"><p>چو بلبل کز گلستان بازماند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>زر اشعار او در روم گنجیست</p></div>
<div class="m2"><p>که زیر خاک پنهان بازماند</p></div></div>