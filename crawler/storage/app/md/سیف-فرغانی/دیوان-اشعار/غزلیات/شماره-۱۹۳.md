---
title: >-
    شمارهٔ ۱۹۳
---
# شمارهٔ ۱۹۳

<div class="b" id="bn1"><div class="m1"><p>دلبرم عزم سفر کرد و روان خواهد شد</p></div>
<div class="m2"><p>در دلم آنچه همی‌گشت چنان خواهد شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>او چو آب است و من سوخته با دیده تر</p></div>
<div class="m2"><p>خشک لب ماندم و آن آب روان خواهد شد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بود بیگانه ز من چون بر من نامده بود</p></div>
<div class="m2"><p>از برم چون برود باز همان خواهد شد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از پی گریه مرا چشم شود جمله مسام</p></div>
<div class="m2"><p>وز پی ناله مرا موی زبان خواهد شد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>می‌رود نیستش اندیشه که مردم گویند</p></div>
<div class="m2"><p>که فلان کشته هجران فلان خواهد شد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دلبرا در دل من از غم تو سودایی‌ست</p></div>
<div class="m2"><p>که مرا جان سر اندر سر آن خواهد شد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جان من بودی و چون نیست رفتن کردی</p></div>
<div class="m2"><p>از من دلشده شک نیست که جان خواهد شد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دیده چون روی تو می‌دید دلم فارغ بود</p></div>
<div class="m2"><p>چون ز چشمم بروی دل نگران خواهد شد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از برای دل تو غصه هجرت بخورم</p></div>
<div class="m2"><p>ور چه جانیم درین غصه زیان خواهد شد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بر من از بار فراقت چو عنان برتابی</p></div>
<div class="m2"><p>دل من همچو رکاب تو گران خواهد شد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دل که در حوصله انده تو یک لقمه است</p></div>
<div class="m2"><p>تا غم تو بخورد جمله دهان خواهد شد</p></div></div>