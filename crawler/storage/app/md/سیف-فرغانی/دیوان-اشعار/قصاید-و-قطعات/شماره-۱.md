---
title: >-
    شمارهٔ ۱
---
# شمارهٔ ۱

<div class="b" id="bn1"><div class="m1"><p>دیده تحمل نمی کند نظرت را</p></div>
<div class="m2"><p>پرده برافگن رخ چو ماه و خورت را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نزد من ای از جهان یگانه بخوبی</p></div>
<div class="m2"><p>ملک دو عالم بهاست یک نظرت را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مشکلم است این که چون همی نکند حل</p></div>
<div class="m2"><p>آب سخن آن لبان چون شکرت را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عشق تو داده است در ولایت جان حکم</p></div>
<div class="m2"><p>هجر ستم کار و وصل داد گرت را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>منتظرم لیک نیست وقت معین</p></div>
<div class="m2"><p>همچو قیامت وصال منتظرت را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>میل ندارد بآفتاب و بروزش</p></div>
<div class="m2"><p>هرکه بشب دید روی چون قمرت را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پرده برافگن زدورو گرنه ببادی</p></div>
<div class="m2"><p>گرد بهر سو بریم خاک درت را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پر زلآلی شود چو بحر کنارش</p></div>
<div class="m2"><p>کوه اگر در میان رود کمرت را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مصحف آیات خوبیی و باخلاص</p></div>
<div class="m2"><p>فاتحه خوانیم جمله سورت را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خوب چو طاوسی و بچشم تعشق</p></div>
<div class="m2"><p>ما نگرانیم حسن جلوه گرت را</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مشک چه باشد بنزد تو که چو عنبر</p></div>
<div class="m2"><p>زلف تو خوش بو کند کنار و برت را</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چون سخن اینجا رسید دوست مرا گفت</p></div>
<div class="m2"><p>سیف شنودیم شعرهای ترت را</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>مس ترا حکم کیمیاست ازین پس</p></div>
<div class="m2"><p>سکه اگر از قبول ماست زرت را</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>وقت شد اکنون که ما حدیث تو گوییم</p></div>
<div class="m2"><p>فاش کنیم اندرین جهان خبرت را</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بر سر بازار روزگار بریزیم</p></div>
<div class="m2"><p>بر طبق عرض حقه گهرت را</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گرچه زره وار رخنه کرد بیک تیر</p></div>
<div class="m2"><p>قوس دو ابروم صبر چون سپرت را</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>پای چو هیزم شکسته دار و مزن نیز</p></div>
<div class="m2"><p>بیهده بر سنگ دیگران تبرت را</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بر در ما کن اقامت و بسگان ده</p></div>
<div class="m2"><p>بر سر این کو زواده سفرت را</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بر سر خرمن چو کاه زبل مپنداز</p></div>
<div class="m2"><p>گر که و دانه فزون کنند خرت را</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>تا نرسد گردنت بتیغ زمانه</p></div>
<div class="m2"><p>از کله او نگاه دار سرت را</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>جان تو از بحر وصلم آب نیابد</p></div>
<div class="m2"><p>تا جگرت خون و خون کنم جگرت را</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>گر تو بر این اوج چون فرشته برآیی</p></div>
<div class="m2"><p>جمله ببینند از آسمان گذرت را</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>تا بنشان قبول مات رساند</p></div>
<div class="m2"><p>بر سر تیر نیاز بند پرت را</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>رو قدم همت از دو کون برون نه</p></div>
<div class="m2"><p>بیخ برآور ازین و آن شجرت را</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ور نه چو شاخ درخت از کف هرکس</p></div>
<div class="m2"><p>سنگ خور ار میوه یی بود زهرت را</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>زنده شود مرده از مساس تو گر تو</p></div>
<div class="m2"><p>ذبح بتیغ فنا کنی بقرت را</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>قصر ملوکست جسم تو و معنا نیست</p></div>
<div class="m2"><p>این همه دیوارهای پر صورت را</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>دفتر اسرار حکمتی و یدالله</p></div>
<div class="m2"><p>جلد تو کردست جسم مختصرت را</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>مریم بکر است روح تو بطهارت</p></div>
<div class="m2"><p>ای مدد از جان دم مسیح اثرت را</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>در شکم مادر ضمیر چو خواهم</p></div>
<div class="m2"><p>عیسی انجیل خوان کنم پسرت را</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>کعبه زوار فیض مایی و از عشق</p></div>
<div class="m2"><p>یمن یمین الله است هر حجرت را</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>چون حرم قدس عشق ماست مقامت</p></div>
<div class="m2"><p>زمزم مکه است تشنه آبخورت را</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>و از اثر حکم بارقات تجلی</p></div>
<div class="m2"><p>فعل یکی دان بصیرت و بصرت را</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>تا ز تو باقیست ذره یی نبود امن</p></div>
<div class="m2"><p>منزل پر خوف و راه پرخطرت را</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>چون تو ز هستی خویش وا برهی سیف</p></div>
<div class="m2"><p>زشت شمر خوب و عیب دان هنرت را</p></div></div>