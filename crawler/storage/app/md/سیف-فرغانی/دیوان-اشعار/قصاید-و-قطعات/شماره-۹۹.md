---
title: >-
    شمارهٔ ۹۹
---
# شمارهٔ ۹۹

<div class="b" id="bn1"><div class="m1"><p>ای جمالت آیتی از صنع رب العالمین</p></div>
<div class="m2"><p>باد بر روی تو از ایزد هزاران آفرین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو چنان شاهی که در منشور دولت درج کرد</p></div>
<div class="m2"><p>عشق تو عشاق را انتم علی الحق المبین</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ماه با خورشید جمشید و سپاه اختران</p></div>
<div class="m2"><p>پیش روی خوب تو چون آسمان بوسد زمین</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شکر از پسته روان و سحر در نرگس عیان</p></div>
<div class="m2"><p>ماه طالع در رخ و خورشید تابان در جبین</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در رخ خوب تو پیوسته قمر با آفتاب</p></div>
<div class="m2"><p>در لب لعل تو آغشته شکر با انگبین</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>صورتی در لطف همچون روح (و) هر دم می نهد</p></div>
<div class="m2"><p>از معانی گنجها در چشم او جان آفرین</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آنکه با نقاش کن بر لوح هستی زد قلم</p></div>
<div class="m2"><p>در نگارستان دنیا صورتی یابد چنین</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عالم از وی همچو جنت جنت از وی پر ز حور</p></div>
<div class="m2"><p>خانه از رویش پر از گل جامه زو پر یاسمین</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>حورگاه بوسه در جنت در دندان خود</p></div>
<div class="m2"><p>در لب لعلش نشاند همچو نقش اندر نگین</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دست قدرت بر نیاورد از برای جان و دل</p></div>
<div class="m2"><p>مثل او اعجوبه یی در کارگاه ما و طین</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ناوکی از غمزه دارد ابروی او در کمان</p></div>
<div class="m2"><p>لشکری از فتنه دارد چشم او اندر کمین</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گیسوی عنبرفشان تاری تر از ظلمات شک</p></div>
<div class="m2"><p>روی خوبش بی گمان روشن تر از نور یقین</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چون گریبان افق وقت طلوع آفتاب</p></div>
<div class="m2"><p>پای او در عطف دامن دست او در آستین</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>در سخن معنی لفظش مایه آب حیات</p></div>
<div class="m2"><p>گرد رخ مضمون خطش نزهت للناظرین</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>در لفظش را گهرچین گوش جان عاشقان</p></div>
<div class="m2"><p>روی خوبش را مگس ران شهپر روح الامین</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>پرتو انوار رویش آفت عقلست و هوش</p></div>
<div class="m2"><p>سکه دینار حسنش رحمت للعالمین</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>لعل او شهد شفا و نطق او شمع هدی</p></div>
<div class="m2"><p>روی او نور مبین و زلف او حبل المتین</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>عطر زلف عنبرینش رشک بوی مشک ناب</p></div>
<div class="m2"><p>پشت پای نازنینش به ز روی حور عین</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چون لب معشوق از شیرینی نامش گزد</p></div>
<div class="m2"><p>در کتابت مرزبان خامه را دندان شین</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>طوطی جان را بگو منقار از دل کن بیا</p></div>
<div class="m2"><p>چون نگار بی دهان از لب شکر بارد بچین</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ترک تازی گو که پشت پا ز عشق او زدند</p></div>
<div class="m2"><p>مشک مویان ختن بر روی بت رویات چین</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>در لطافت چون عرق بر جسم او نبود اگر</p></div>
<div class="m2"><p>زآب حیوان شبنم افشاند هوا بر یاسمین</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>عقل در بازار او در کیسه دارد نقد قلب</p></div>
<div class="m2"><p>کفر اندر راه او بر پشت دارذ بار دین</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بر سمند کامرانی می خرامد شاه وار</p></div>
<div class="m2"><p>گاه در بستان مهر و گاه در میدان کین</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ماه رویان چاکران و پادشاهان بندگان</p></div>
<div class="m2"><p>عشق بازان بر یسار و جان فشانان بر یمین</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>با چنین ملک و ولایت با چنین خیل و حشم</p></div>
<div class="m2"><p>با گدایان هم وثاق و با فقیران همنشین</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>صورتی دارد که در وی خیره گردد چشم عقل</p></div>
<div class="m2"><p>دیده معنی خود روشن کن و رویش ببین</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بر در او باش و می دان زیر پای خود بهشت</p></div>
<div class="m2"><p>در ره او پوی و می خوان نعم اجرالعاملین</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>عاشقان روی خوبش از نعیم دار خلد</p></div>
<div class="m2"><p>چون فرشته فارغند از خوردن عجل سمین</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>دستشان افلاک را چون نعل دارد زیر پای</p></div>
<div class="m2"><p>حکمشان اجرام را چون مرکب آرد زیر زین</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>عاشقان را قال نبود، حال باشد نقد وقت</p></div>
<div class="m2"><p>زردیی بر رخ عیان واندهی در دل دفین</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>آفتاب گرم رو در خانه دارد چون خوهد</p></div>
<div class="m2"><p>شیر گردون از برای دفع سرما پوستین</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>سیف فرغانی سنائی وار ازین پس نزد ما</p></div>
<div class="m2"><p>«چون سخن زآن زلف و رخ گویی مگو از کفر و دین »</p></div></div>