---
title: >-
    شمارهٔ ۸۶
---
# شمارهٔ ۸۶

<div class="b" id="bn1"><div class="m1"><p>عشق و دولت اگر بود باهم</p></div>
<div class="m2"><p>بتو نزدیکتر شود راهم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>محنت و عشق هر دو هم زادند</p></div>
<div class="m2"><p>عشق و دولت کجا بود باهم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هست بخت آنکه تو مرا خواهی</p></div>
<div class="m2"><p>هست عشق آنکه من ترا خواهم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عشق خواند مرا بدرگه تو</p></div>
<div class="m2"><p>لیک دولت برد بدرگاهم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هست ارزان بمحنت همه عمر</p></div>
<div class="m2"><p>دولتی کز تو کرد آگاهم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بسوی خیمه تو می نگرد</p></div>
<div class="m2"><p>ترک جان از تن چو خرگاهم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سوزن گم شده است در ره هجر</p></div>
<div class="m2"><p>این تن همچو رشته یکتاهم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>که ز خورشید اگر چراغ کنی</p></div>
<div class="m2"><p>نتوان یافتن بیک ماهم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در غم تست ناله هم نفسم</p></div>
<div class="m2"><p>در ره تست سایه همراهم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مردم از من ترا همی طلبند</p></div>
<div class="m2"><p>که من از تو ترا همی خواهم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بدو زلف تو عشق قیدم کرد</p></div>
<div class="m2"><p>رسن تو فگند در چاهم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>عشق تو سوخت خرمن خردم</p></div>
<div class="m2"><p>باد تو برد دانه و کاهم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>رخ تو دید مست شد عقلم</p></div>
<div class="m2"><p>در همین خانه مات شد شاهم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>سخنم چون بسمع تو نرسید</p></div>
<div class="m2"><p>کز تو همچون سخن در افواهم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ای چو شب دل سیاه کرده، مباش</p></div>
<div class="m2"><p>ایمن از ناله سحرگاهم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گر تو از روشنی چو آینه یی</p></div>
<div class="m2"><p>عاقبت تیره گردی از آهم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>سر نهم زیر پای تا برسد</p></div>
<div class="m2"><p>بدرخت تو دست کوتاهم</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گر همه رنگها بیامیزی</p></div>
<div class="m2"><p>ای دو زلف دراز و بالا هم</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بجز از رنگ عشق تو رنگی</p></div>
<div class="m2"><p>نپذیرم که صبغت اللهم</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>من نه آن عاشقم که در پی خود</p></div>
<div class="m2"><p>هم چو سعدی بری باکراهم</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>گر چه در خانه خفته ام بی کار</p></div>
<div class="m2"><p>بتو مشغول و با تو همراهم</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>زین گلستان بسیف فرغانی</p></div>
<div class="m2"><p>خاردادی مدام و خرما هم</p></div></div>