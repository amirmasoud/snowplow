---
title: >-
    شمارهٔ ۷۱
---
# شمارهٔ ۷۱

<div class="b" id="bn1"><div class="m1"><p>گر باد خاک کوی تو سوی چمن برد</p></div>
<div class="m2"><p>بینند نور باصره در چشم عبهرش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جان چون بتو رسید تن اینجا چه میکند</p></div>
<div class="m2"><p>زآنجا که لطف تست ازینجا برون برش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عیسی خود ار بجنت مأویست کی سزد</p></div>
<div class="m2"><p>کندر ظلال سدره و طوبی بود خرش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آن سروری که چون کمر کوه و طرف کان</p></div>
<div class="m2"><p>ترصیع کرده اند جواهر در افسرش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر بندگی تو دهذش دست و، روی خود</p></div>
<div class="m2"><p>بر خاک پای تو ننهد، خاک بر سرش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عشقت چو در حریم دلی پای در نهاد</p></div>
<div class="m2"><p>گشتند سرکشان طبیعت مسخرش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بت را نمازگاه عبادت مقام داد</p></div>
<div class="m2"><p>بانگ نماز و هیبت الله اکبرش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دل مجمریست آتش اندوه عشق را</p></div>
<div class="m2"><p>تا کرده ای بعود محبت معطرش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>عاشق که آبخورده عشقست خاک او</p></div>
<div class="m2"><p>کانون شوق بوده دل همچو مجمرش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گر چه کمال یافت کجا منقطع شود</p></div>
<div class="m2"><p>نسبت ازین جناب و تعلق ازین درش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>عیسی اگر چه رتبت روح اللهی بیافت</p></div>
<div class="m2"><p>حق کی برید نسبت او را ز مادرش</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>از بهر این غزل که بوصف تو گفته شد</p></div>
<div class="m2"><p>گر چه قول زور ندارند باورش</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>در بزم خویش زخمه ز اظفار حور کرد</p></div>
<div class="m2"><p>ناهید رود ساز بر او تار مزهرش</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گر مطرب این ترانه سراید حزین بود</p></div>
<div class="m2"><p>چون بانگ چنگ ناله مزمار حنجرش</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بلبل چو پیش برگ گل خود نوا برد</p></div>
<div class="m2"><p>طوطی خجل بماند ز سجع مکررش</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>در موسم بهار روا کی بود که زاغ</p></div>
<div class="m2"><p>با عندلیب دم زند از صوت منکرش</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ما را ببوسه چون بگرفتیم در برش</p></div>
<div class="m2"><p>آب حیات داد لب همچو شکرش</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گردیم هر دو مست شراب نیاز و ناز</p></div>
<div class="m2"><p>او دست در بر من و من دست در برش</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>در وصف او اگر چه اشارات کرده اند</p></div>
<div class="m2"><p>ما وصف می کنیم بقانون دیگرش</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بسیار خلق چون شکر و عود سوختند</p></div>
<div class="m2"><p>زآن روی همچو آتش و خط چو عنبرش</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بفروخت در زجاجه تاریک کاینات</p></div>
<div class="m2"><p>مصباح نور اشعه خورشید منظرش</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بر لشکر نجوم کشد آفتاب تیغ</p></div>
<div class="m2"><p>در سایه حمایت روی منورش</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>سلطان حسن او و یکی از سپاه اوست</p></div>
<div class="m2"><p>این مه که مفردات نجومند لشکرش</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>طاوس حسنش ار بگشاید جناح خویش</p></div>
<div class="m2"><p>جبریل آشیانه کند زیر شهپرش</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>آرایش عروس جمالش مکن که نیست</p></div>
<div class="m2"><p>با آن کمال حسن نیازی بزیورش</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>خورشید کیمیایی گر خاک زر کند</p></div>
<div class="m2"><p>با صنعتی چنین عرض اوست جوهرش</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>دل سست گشت آینه سخت روی را</p></div>
<div class="m2"><p>هرگه که داشت روی خود اندر برابرش</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>هرکو چو من بوصف جمالش خطی نوشت</p></div>
<div class="m2"><p>شد جمله حسن چون رخ گل روی دفترش</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>آب حیات یافت خضروار بی خلاف</p></div>
<div class="m2"><p>لب تشنه یی که می طلبد چون سکندرش</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>آن را که آبخور می عشقست حاصلست</p></div>
<div class="m2"><p>بر هر کنار جوی لب حوض کوثرش</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>صافی درون چو شیشه و روشن شود چو می</p></div>
<div class="m2"><p>هرکو شراب عشق درآمد بساغرش</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>آن دلبری که جمله جمالست نعت او</p></div>
<div class="m2"><p>نام آوریست کاسم جمیل است مصدرش</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>در دل نهفت همچو صدف اشک قطره را</p></div>
<div class="m2"><p>هر در که یافت گوش ز لعل سخن ورش</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>رو مستقیم باش اگر خوض می کنی</p></div>
<div class="m2"><p>در بحر عشق او که صراطست معبرش</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>بی داروی طبیب غم او بسی بمرد</p></div>
<div class="m2"><p>بیمار دل که هست امانی مزورش</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>هر ذره یی که از پی خورشید روی او</p></div>
<div class="m2"><p>یکشب بروز کرد مهی گشت اخترش</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>بر فرق خویش تاج حیات ابد نهاد</p></div>
<div class="m2"><p>آنکس که بازیافت بسر نیش خنجرش</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>وآن را که نور عشق ازل پیش رو نبود</p></div>
<div class="m2"><p>ننموده ره بشمع هدایت پیمبرش</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>ای دلبری که هر که ترا خواست، وصل تو</p></div>
<div class="m2"><p>جز در فراق خویش نگردد میسرش</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>نبود بهیچ باغ چو تو سرو میوه دار</p></div>
<div class="m2"><p>باغ ار بهشت باشد و رضوان (کدیورش)</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>نه خارج و نه داخل عالم بود چو روح</p></div>
<div class="m2"><p>آن معدن جمال که هستی تو گوهرش</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>فردا که نفخ صور اعادت خوهند کرد</p></div>
<div class="m2"><p>مرده سری برآورد از خاک محشرش</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>در بوته جحیم گدازند هرکرا</p></div>
<div class="m2"><p>بی سکه غم تو بود جان چون زرش</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>پیوستگان عشق تو از خود بریده اند</p></div>
<div class="m2"><p>آن کو خلیل تست چه نسبت بآزرش</p></div></div>