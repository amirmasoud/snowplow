---
title: >-
    شمارهٔ ۷۵
---
# شمارهٔ ۷۵

<div class="b" id="bn1"><div class="m1"><p>ایا ندیده ز قرآن دلت ورای حروف</p></div>
<div class="m2"><p>بچشم جان رخ معنی نگر بجای حروف</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بگرد حرف چو اعراب تا بکی گردی</p></div>
<div class="m2"><p>بملک عالم معنی نگر ورای حروف</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مدبرات امورند در مصالح خلق</p></div>
<div class="m2"><p>ستارگان معانیش بر سمای حروف</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عروس معنی او بهر چشم نامحرم</p></div>
<div class="m2"><p>فرو گذاشته بر روی پردهای حروف</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خلیفه وار بدیدی امام قرآن را</p></div>
<div class="m2"><p>لباس خویش سیه کرده از کسای حروف</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زوجد پاره کنی جامه گر برون آید</p></div>
<div class="m2"><p>برهنه شاهد اسرارش از قبای حروف</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عزیز قرآن در مصر جامع مصحف</p></div>
<div class="m2"><p>فراز مسند الفاظ و متکای حروف</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شراب معنی رخشان چو طلعت یوسف</p></div>
<div class="m2"><p>نمود از دل جام جهان نمای حروف</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>حدیث گنج معانی همی کند با تو</p></div>
<div class="m2"><p>زبان قرآن در کام اژدهای حروف</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دل صدف صفتت بر امید در ثواب</p></div>
<div class="m2"><p>ز بحر قرآن قانع بقطرهای حروف</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بکام جان برو آب حیات معنی نوش</p></div>
<div class="m2"><p>ز عین چشمه الفاظ وز انای حروف</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مکن بجهل تناول، که خوان قرآن را</p></div>
<div class="m2"><p>پر از حلاوه علم است کاسهای حروف</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>قمطرهای نباتست پر ز شهد شفا</p></div>
<div class="m2"><p>نهاده خازن رحمت برو غطای حروف</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>عرب اگر چه بگفتار سحر می کردند</p></div>
<div class="m2"><p>از ابتدای الف تا بانتهای حروف</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>حبال دعوی برداشتند چون بفگند</p></div>
<div class="m2"><p>کلیم لفظ وی اندر میان عصای حروف</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بدوستانش فرستاد نامه ایزد</p></div>
<div class="m2"><p>که ره برند بمضمونش از سخای حروف</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>پس آمده ز کتب بوده پیشوای همه</p></div>
<div class="m2"><p>چنانکه حرف الف هست پیشوای حروف</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بآفتاب هدایت مگر توانی دید</p></div>
<div class="m2"><p>که ذرهای معانیست در هوای حروف</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>اگر مرکب گردد چو صورت و بیند</p></div>
<div class="m2"><p>بسیط عالم معنی ز تنگنای حروف</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ببارگاه سلیمان روح هدهد عقل</p></div>
<div class="m2"><p>خبر ز عرش عظیم آرد از سبای حروف</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بدین قصیده که گفتم، درو بیان کردم</p></div>
<div class="m2"><p>که ترک علم معانی مکن برای حروف</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>تو در حروف هجا خوانده ای کجا دانی</p></div>
<div class="m2"><p>که مدح معنی شد گفته بی هجای حروف</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>گمان مبر که برد راه سایر معنی</p></div>
<div class="m2"><p>بسوی منزل فهم تو جز بپای حروف</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چو نای بلبل خواننده گشت تیز آهنگ</p></div>
<div class="m2"><p>تو ره بپرده معنی براز نوای حروف</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بسوی شاه معانی بسان حجابند</p></div>
<div class="m2"><p>معرفان نقط بر در سرای حروف</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>سماع چون کنی از زخمه زبان باصول</p></div>
<div class="m2"><p>بدست دل نزنی بر چهارتای حروف</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ورآب لفظ نباشد کجا برون آید</p></div>
<div class="m2"><p>دقیق معنی از زیر آسیای حروف</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>تو کوردل نکنی رو بدان طرف که بود</p></div>
<div class="m2"><p>جمال معنی، اگر نشنوی ندای حروف</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>اگر تو مدحت قرآن کنی چنانکه سزاست</p></div>
<div class="m2"><p>کی احتمال معانی کند وعای حروف</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بجمع کردن الفاظ و نظم دادن آن</p></div>
<div class="m2"><p>نه مدح معنی گفتیم و نی ثنای حروف</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>ز روی علم معانی همچو مو باریک</p></div>
<div class="m2"><p>چو زلفهاست گره بسته در قفای حروف</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>الهی ار چه ز قرآن ندارم آگاهی</p></div>
<div class="m2"><p>مرا عطا کن فهمی گره گشای حروف</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>ادای حق معانی آن ببخش مرا</p></div>
<div class="m2"><p>چنانکه عاصم و بوعمر ورا ادای حروف</p></div></div>