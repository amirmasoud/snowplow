---
title: >-
    شمارهٔ ۱۲۹
---
# شمارهٔ ۱۲۹

<div class="b" id="bn1"><div class="m1"><p>قرآن چه بود مخزن اسرار الهی</p></div>
<div class="m2"><p>گنج حکم و حکمت آن نامتناهی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در صورت الفاظ معانیش کنوزست</p></div>
<div class="m2"><p>وین حرف طلسمیست بر آن گنج الهی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>لفظش بقراآت بخوانی و ندانی</p></div>
<div class="m2"><p>معنی وی، ای حاصلت از حرف سیاهی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گلهای معانیش نبویند چو هستند</p></div>
<div class="m2"><p>آن مردم بی علم ستوران گیاهی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بحریست درو گوهر علم و در حکمت</p></div>
<div class="m2"><p>غواص شو و در طلب از بحر نه ماهی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز اعراب و نقط هست پس و پیش حروفش</p></div>
<div class="m2"><p>آراسته چون درگه سلطان بسپاهی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>قرآن رهاننده ز دوزخ چو بهشتست</p></div>
<div class="m2"><p>زیرا که بیابی تو درو هرچه بخواهی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا پرده صورت نگشایی ننماید</p></div>
<div class="m2"><p>اسرار و معانیش بتو (روی کماهی)</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آنها که یکی حرف بدانند ز قرآن</p></div>
<div class="m2"><p>بر جمله کتب مفتخرانند (و مباهی)</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بی معرفتی بر لب دریای حروفند</p></div>
<div class="m2"><p>چون تشنه بی دلو و رسن بر سر چاهی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>حق است که گویند همه کاتب (او را)</p></div>
<div class="m2"><p>کای بر سر کتاب (ترا منصب شاهی)</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هر سو که برد نفس ندا از چپ و از راست</p></div>
<div class="m2"><p>گر پشت بقرآن بکنی روی (سیاهی)</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>در محکمه دین کتب منزله یک یک</p></div>
<div class="m2"><p>داده همه بر محضر صدق تو گواهی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>سرمست می موعظتت بهر شکستن</p></div>
<div class="m2"><p>بر سنگ ندامت بزند جام ملاهی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بر لوح که از خلق نهان در شب غیب است</p></div>
<div class="m2"><p>آن جمله کتب همچو ستاره تو چو ماهی</p></div></div>