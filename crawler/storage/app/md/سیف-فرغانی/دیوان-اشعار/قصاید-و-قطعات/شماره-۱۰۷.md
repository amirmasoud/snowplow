---
title: >-
    شمارهٔ ۱۰۷
---
# شمارهٔ ۱۰۷

<div class="b" id="bn1"><div class="m1"><p>ایا درویش رعناوش چو مطرب با سماعت خوش</p></div>
<div class="m2"><p>بنزد ره روان بازیست رقص خرس وار تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چه گویی نی روش اینجا بخرقه است آب روی تو</p></div>
<div class="m2"><p>چه گویی همچو گل تنها برنگست اعتبار تو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بهانه بر قدر چه نهی قدم در راه نه، گرچه</p></div>
<div class="m2"><p>ز دست جبر در بندست پای اختیار تو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>باسب همت عالی توانی ره بسر بردن</p></div>
<div class="m2"><p>گر آید در رکاب جهد پای اقتدار تو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بدرویشی بکنجی در برو بنشین و پس بنگر</p></div>
<div class="m2"><p>جهانداران غلام تو جهان ملک و عقار تو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ترا عاری بود زآن پس شراب از جام جم خوردن</p></div>
<div class="m2"><p>چو شد در جشن درویشی ز خرسندی عقار تو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز تلخی ترش رویان شد آخر کام شیرینت</p></div>
<div class="m2"><p>چو شور آب قناعت شد شراب خوش گوار تو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ترا در گلستان جان هزارانند چون بلبل</p></div>
<div class="m2"><p>وزین باب ار سخن گویی بود فصل بهار تو</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سخن مانند بستانست و ذکر دوست دروی گل</p></div>
<div class="m2"><p>چو بلبل صد نوا دارد درین بستان هزار تو</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تو چنگی در کنار دهر و صاحب دل کند حالت</p></div>
<div class="m2"><p>چو زین سان در نوا آید بریشم وار تار تو</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چو تیز آهنگ شد قولت، نباشد سیف فرغانی</p></div>
<div class="m2"><p>غزل سازی درین پرده که باشد دستیار تو</p></div></div>