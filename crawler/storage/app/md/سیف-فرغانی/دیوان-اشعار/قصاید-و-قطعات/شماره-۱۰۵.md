---
title: >-
    شمارهٔ ۱۰۵
---
# شمارهٔ ۱۰۵

<div class="b" id="bn1"><div class="m1"><p>ایا قاضی حیلت گر، حرام آشام رشوت خور</p></div>
<div class="m2"><p>که بی دینی است دین تو و بی شرعی شعار تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل بیچاره یی راضی نباشد از قضای تو</p></div>
<div class="m2"><p>زن همسایه یی آمن نبوده در جوار تو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز بی دینی تو چون گبری و زند تو سجل تو</p></div>
<div class="m2"><p>ز بی علمی تو چون گاوی و نطق تو خوار تو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو باطل را دهی قوت ز بهر ضعف دین حق</p></div>
<div class="m2"><p>تو دجالی درین ایام و جهل تو حمار تو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگر خوی زمان گیری و گر ملک جهان گیری</p></div>
<div class="m2"><p>مسیحی هم پدید آید کزو باشد دمار تو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ترا در سر کلهداریست چون کافر از آن هرشب</p></div>
<div class="m2"><p>ببندد عقد با فتنه سر دستار دار تو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو زر قلب مردودست و تقویم کهن باطل</p></div>
<div class="m2"><p>درین ملکی که ما داریم یرلیغ تتار تو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کنی دین دار را خواری و دنیادار را عزت</p></div>
<div class="m2"><p>عزیز تست خوار ما عزیز ماست خوار تو</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دل مشغولت از غفلت قبول موعظت نکند</p></div>
<div class="m2"><p>تو این دانه کجا خواهی که که دارد غرار تو</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ترا بینند در دوزخ بدندان سگان داده</p></div>
<div class="m2"><p>زبان لغوگوی تو، دهان رشوه خوار تو</p></div></div>