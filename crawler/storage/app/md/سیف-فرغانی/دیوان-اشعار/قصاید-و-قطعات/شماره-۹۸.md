---
title: >-
    شمارهٔ ۹۸
---
# شمارهٔ ۹۸

<div class="b" id="bn1"><div class="m1"><p>دلا از آستین عشق دست کار بیرون کن</p></div>
<div class="m2"><p>ز ملک خویش دشمن را بعون یار بیرون کن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حریم دوستست این دل اگر نه دشمن خویشی</p></div>
<div class="m2"><p>بغیر از دوست چیزی را درو مگذار بیرون کن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تو چون گنجی و حب مال مارست ای پسر در تو</p></div>
<div class="m2"><p>سخن بشنو برو از خود بافسون مار بیرون کن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر از دست حکم دوست تیغ آید ترا بر سر</p></div>
<div class="m2"><p>سپر در رو مکش جوشن درین پیکار بیرون کن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تو در کعبه بتان داری ازین پندارها در دل</p></div>
<div class="m2"><p>ز کعبه بت برون افگن ز دل پندار بیرون کن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو در مسجد سگان یابی مسلمان وار بیرون ران</p></div>
<div class="m2"><p>چو در کعبه بتان بینی برو زنهار بیرون کن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سرت را در فسار حکم خویش آورد نفس تو</p></div>
<div class="m2"><p>گر از عقل افسری داری سر از افسار بیرون کن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو کار عشق خواهی کرد دست افزار یک سونه</p></div>
<div class="m2"><p>چو اندر کعبه خواهی رفت پای افزار بیرون کن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گرت در دل نیامد عشق و عاشق نیستی باری</p></div>
<div class="m2"><p>برو با عاشقان او ز دل انکار بیرون کن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تو می گویی که هشیارم ولیکن از می غفلت</p></div>
<div class="m2"><p>هنوز اندر سرت مستیست ای هشیار بیرون کن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گل و خارست پایت را درین ره هرچه پیش آید</p></div>
<div class="m2"><p>هم از گل پا برون آور هم از پا خار بیرون کن</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تو اندر خویشتن دایم چو بو در گل چه ماندستی</p></div>
<div class="m2"><p>چو برگ از شاخ و چون میوه سر از ازهار بیرون کن</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>برو گر عاشق از جانی برو ای سیف فرغانی</p></div>
<div class="m2"><p>گرت در دل جز او چیزیست عاشق وار بیرون کن</p></div></div>