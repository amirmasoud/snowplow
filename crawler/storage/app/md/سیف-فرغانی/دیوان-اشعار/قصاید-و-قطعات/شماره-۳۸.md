---
title: >-
    شمارهٔ ۳۸
---
# شمارهٔ ۳۸

<div class="b" id="bn1"><div class="m1"><p>هرکه همچون من و تو از عدم آمد بوجود</p></div>
<div class="m2"><p>همه دانند که از بهر سجود آمد وجود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا بسی محنت خدمت نکشد همچو ایاز</p></div>
<div class="m2"><p>مرد همکاسه نعمت نشود با محمود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هرکه مانند خضر آب حیات دین یافت</p></div>
<div class="m2"><p>بهر دنیا بر او نیست سکندر محسود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای (که) بر خلق حقت دست و ولایت دادست</p></div>
<div class="m2"><p>خلق آزرده مدار از خود و حق ناخشنود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آتش اندر بنه خویش زدی ای ظالم</p></div>
<div class="m2"><p>که بظلم از دل درویش برآوردی دود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گرچه داری رخ چون آتش و اندام چو آب</p></div>
<div class="m2"><p>زیر این خاک از آن آتش و آب افتد زود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ور چه در کبر بنمرود رسیدی و گذشت</p></div>
<div class="m2"><p>من همی گویمت از پشه بترس ای نمرود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز بر و زیر مکن کار جهانی چون عاد</p></div>
<div class="m2"><p>که بیک صیحه شوی زیر و زبر همچو ثمود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تا گریبان تو از دست اجل بستانند</p></div>
<div class="m2"><p>ای که از بهر تو آفاق گرفتند جنود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>پیش ازین بی دگران با تو بسی بود جهان</p></div>
<div class="m2"><p>پس ازین با دگران بی تو بسی خواهد بود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گر چه عمر تو درازست، چو روزی چندست</p></div>
<div class="m2"><p>هم بآخر رسد آن چیز که باشد معدود</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ورچه خوش نایدت از دنیی فانی رفتن</p></div>
<div class="m2"><p>نه تویی باقی (و) خالد نه جهان جای خلود</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نرم بالای زمین رو که بزیر خاکست</p></div>
<div class="m2"><p>سرو سیمین قد و رو و گل رنگین خدود</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>این زر سرخ که روی تو ز عشقش زردست</p></div>
<div class="m2"><p>هست همچون درم قلب و مس سیم اندوذ</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>عمر اندر طلبش صرف (شود،) آنت زیان!</p></div>
<div class="m2"><p>دگری بعد تو زآن مایه کند، اینت سود!</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>رو هوا گیر چو آتش که ز بهر نان مرد</p></div>
<div class="m2"><p>تا درین خاک بود آب خورد خون آلود</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>عاقبت بذ بجزای عمل خود برسید</p></div>
<div class="m2"><p>خار می کاشت از آن گل نتوانست درود</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>نیک بختان را مقصود رضای حقست</p></div>
<div class="m2"><p>بخت خود بد مکن و باز ممان از مقصود</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>گر درم داری با خلق کرم کن زیرا</p></div>
<div class="m2"><p>«شرف نفس بجودست و کرامت بسجود»</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>سیف فرغانی در وعظ چو سعدی زین سان</p></div>
<div class="m2"><p>سخنی گفت و بود دولت آنکس که شنود</p></div></div>