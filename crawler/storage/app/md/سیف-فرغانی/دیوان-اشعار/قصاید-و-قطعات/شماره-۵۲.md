---
title: >-
    شمارهٔ ۵۲
---
# شمارهٔ ۵۲

<div class="b" id="bn1"><div class="m1"><p>چند گفتم که فراموش کنم صحبت یار</p></div>
<div class="m2"><p>یاد او می دهدم رنگ گل و بوی بهار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بلبل از وصلت گل بانگ برآورده چنانک</p></div>
<div class="m2"><p>در چمن ناله کند مرغ جدا مانده ز یار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون ز چنگ غمش آهنگ فغان پست کنم</p></div>
<div class="m2"><p>خاصه این لحظه که صد ناله برآمد ز هزار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>من چرا باشم خاموش چو بلبل کاکنون</p></div>
<div class="m2"><p>حسن رخسار گل افزود جمال گلزار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>باغ را آب فزوده لب جوی از سبزه</p></div>
<div class="m2"><p>دم طاوس نموده سر شاخ از اشجار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زآتش لاله علمدار شده دامن طور</p></div>
<div class="m2"><p>شاخ چون جیب کلیمست محل انوار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دست قدرت که ورا نامیه چون انگشتست</p></div>
<div class="m2"><p>بر سر شاخ گل از غنچه نهاده دستار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آب روی چمن افزوده بنزد مردم</p></div>
<div class="m2"><p>شبنم قطره صفت بر گل آتش رخسار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>لاله بر دامن سبزه است بدان سان گویی</p></div>
<div class="m2"><p>که بشنگرف کسی نقطه زند بر زنگار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>رعد تا صور دمیدست و زمین زنده شده</p></div>
<div class="m2"><p>همبر سدره و طوبیست درخت از ازهار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>راست چون مرده مبعوث دگر باره بیافت</p></div>
<div class="m2"><p>کسوه نو ز ریاحین چمن کهنه شعار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>حوریانند ریاحین و بساتین چو بهشت</p></div>
<div class="m2"><p>وقت آنست که جانان بنماید دیدار</p></div></div>