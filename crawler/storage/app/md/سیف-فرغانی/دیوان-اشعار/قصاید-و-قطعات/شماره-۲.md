---
title: >-
    شمارهٔ ۲
---
# شمارهٔ ۲

<div class="b" id="bn1"><div class="m1"><p>بباغی در بدیدم پار گل را</p></div>
<div class="m2"><p>مگر گفتم تویی ای یار گل را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خطای خویشتن امسال دیدم</p></div>
<div class="m2"><p>که نسبت با تو کردم پار گل را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>وگر بویت ز دیوارش درآید</p></div>
<div class="m2"><p>ز در بیرون کند گلزار گل را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ترا من با رقیبت دیدم و گفت</p></div>
<div class="m2"><p>چه خوش می پرورد این خار گل را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو مشکین زلف تو خوش بو نباشد</p></div>
<div class="m2"><p>وگر عنبر بود در بار گل را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اگر این سرخ روی اسپید دیدی</p></div>
<div class="m2"><p>برفتی زردی از رخسار گل را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز شوق خوب رویانش بدر کن</p></div>
<div class="m2"><p>چه رختست اندرین بازار گل را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بخود مشغول می دارد مرا گل</p></div>
<div class="m2"><p>چو خار از راه من بردار گل را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نسیم صبح را گفتم سحرگه</p></div>
<div class="m2"><p>ز حبس غنچه بیرون آر گل را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>جوابم داد و گفتا پیش رویش</p></div>
<div class="m2"><p>چو پیش گل گیا پندار گل را</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چو با آن گلستان در گلشن آیی</p></div>
<div class="m2"><p>نظر بروی کن و بگذار گل را</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بخوبی تو کلهداری و، خاری</p></div>
<div class="m2"><p>بسر بربسته چو دستار گل را</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>تو سلطانی و گل همچون رعیت</p></div>
<div class="m2"><p>بدست این و آن مگذار گل را</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>غریبست آمده وز ره رسیده</p></div>
<div class="m2"><p>بلطف خویشتن خوش دار گل را</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بجز خارش کسی اندر قفا نیست</p></div>
<div class="m2"><p>بروی خویش کن تیمار گل را</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ز حسنت مایه ده ای جان و منشان</p></div>
<div class="m2"><p>ببازار چمن بی کار گل را</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ز خجلت پای او از جای رفتست</p></div>
<div class="m2"><p>بدست لطف خود باز آر گل را</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بصد دستان ثناگوی تو گردد</p></div>
<div class="m2"><p>چو بلبل گر بود گفتار گل را</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چو او رنگی ز رخسار تو دارد</p></div>
<div class="m2"><p>دگر زین پس ندارم خوار گل را</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>جهانی خوب را لطف تو نبود</p></div>
<div class="m2"><p>که باشد میوه کم بسیار گل را</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>قبای تور اندام تو دایم</p></div>
<div class="m2"><p>بتنگ آورده صد خروار گل را</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ز عشق روی تو زین پس برآید</p></div>
<div class="m2"><p>چو بلبل نالهای زار گل را</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>عجب نبود که همچون نرگس خود</p></div>
<div class="m2"><p>ز عشق خود کنی بیمار گل را</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>مرا این شعرها گل میدهد گفت</p></div>
<div class="m2"><p>که کرد آگه ازین اسرار گل را</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>درین اشعار من ذکر تو کردم</p></div>
<div class="m2"><p>علم کردم برین اسحار گل را</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>مباد از سیف فرغانی ترا عار</p></div>
<div class="m2"><p>که از بلبل نباشد عار گل را</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>من و تو هر دو از هم ناگزیریم</p></div>
<div class="m2"><p>که از خاری بود ناچار گل را</p></div></div>