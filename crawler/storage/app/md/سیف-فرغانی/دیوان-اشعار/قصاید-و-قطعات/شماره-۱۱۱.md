---
title: >-
    شمارهٔ ۱۱۱
---
# شمارهٔ ۱۱۱

<div class="b" id="bn1"><div class="m1"><p>ای که در حسن عمل زامسال بودی پار به</p></div>
<div class="m2"><p>مردم بی خیر را دست عمل بی کار به</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چند گویی من بهم در کار دنیا یا فلان</p></div>
<div class="m2"><p>چون ز دین بی بهره باشد سگ ز دنیا دار به</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دین ترا در دل به از دنیا که در دستت بود</p></div>
<div class="m2"><p>گل بدست باغبان از خار بر دیوار به</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نفس اگر چه مرده باشد آمنی زو شرط نیست</p></div>
<div class="m2"><p>دزد اگر چه خفته باشد پاسبان بیدار به</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نفس سرکش بهر دین مالیده بهتر زیر پای</p></div>
<div class="m2"><p>بهر سلطان مرد لشکر کشته در پیکار به</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نفست از بهر تنعم میخوهد مال حرام</p></div>
<div class="m2"><p>سگ چو مردارست باشد قوت او مردار به</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زر خالص نزد تو از دین خالص بهترست</p></div>
<div class="m2"><p>گلخنی را خار بی گل از گل بی خار به</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بر سر نیکان چو بد را از تو باشد دست حکم</p></div>
<div class="m2"><p>تو ازو بسیار بدتر او ز تو بسیار به</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آن جهانجویی که نزد حق بدین نبود عزیز</p></div>
<div class="m2"><p>در جهان چون اهل باطل بهر دنیا خوار به</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دین بنزد مؤمن از دینار و دیبا بهترست</p></div>
<div class="m2"><p>کافری گر نزد تو از دین بود دینار به</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نزد چون تو بی خبر از فقر به باشد غنا</p></div>
<div class="m2"><p>نزد طفل بی خرد از مهره باشد مار به</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>عقل نیک اندیش در تو بهتر از طبع لئیم</p></div>
<div class="m2"><p>غله خاصه در غلا از موش در انبار به</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>جهل رهزن را مگو از علم رهبر نیک تر</p></div>
<div class="m2"><p>ظلمت شب را مدان از روز پرانوار به</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>از سخن چون کار باید کرد بهتر خامشی</p></div>
<div class="m2"><p>وز کله چون راه باید رفت پای افزار به</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>عیب پنهان را چو می بینی و پنهان می کنی</p></div>
<div class="m2"><p>آن دو چشم عیب بین پوشیده چون اسرار به</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>هرکرا پندار نیکویی نباشد در درون</p></div>
<div class="m2"><p>گرچه بد باشد برو او را ز خود پندار به</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>جرم مستغفر بسی از طاعت معجب بهست</p></div>
<div class="m2"><p>گرچه اندر شرع نبود ذنب از استغفار به</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>تا ز چشم بد امان یابد جمال نیکوان</p></div>
<div class="m2"><p>آبله بر روی خوب از خال بر رخسار به</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>در طریق ار یار جویی از غنی بهتر فقیر</p></div>
<div class="m2"><p>ور بگرما سایه خواهی بید از اسپیدار به</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>هر کرا درویش نبود خواجه نیکوتر بنفس</p></div>
<div class="m2"><p>هرکرا بلبل نباشد زاغ را گفتار به</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>او بجان از تو نکوتر تو بجامه زوبهی</p></div>
<div class="m2"><p>هست ای بی مغز او را سر ترا دستار به</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>عرصه دنیا بدوریشان صاحب دل خوشست</p></div>
<div class="m2"><p>ای بر تو دوخیار از یک جهان اخیار به</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>با وجود خار کز وی خسته گردد آدمی</p></div>
<div class="m2"><p>گل چو در گلشن نباشد گلخن از گلزار به</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>سیف فرغانی دلت بیمار حرصست و طمع</p></div>
<div class="m2"><p>گر نه تیمارش کنی کی گردد این بیمار به</p></div></div>