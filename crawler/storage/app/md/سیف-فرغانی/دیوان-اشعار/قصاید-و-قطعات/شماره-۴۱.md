---
title: >-
    شمارهٔ ۴۱
---
# شمارهٔ ۴۱

<div class="b" id="bn1"><div class="m1"><p>حسن هرجا که در جهان برود</p></div>
<div class="m2"><p>عشق در پی چوبی‌دلان برود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حسن هرجا به دل‌ستانی رفت</p></div>
<div class="m2"><p>عشق بر کف نهاده جان برود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حسن لیلی‌صفت چو حکمی کرد</p></div>
<div class="m2"><p>عشق مجنون سلب بر آن برود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در پی حسن دلربا هر روز</p></div>
<div class="m2"><p>عشق بی‌بال جان‌فشان برود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر تو شرح کتاب حسن کنی</p></div>
<div class="m2"><p>مهر و مه چون ورق در آن برود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هرچه در مکتب خبر علم است</p></div>
<div class="m2"><p>جمله بر تخته عیان برود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نقطه عشق اگر پذیرد بسط</p></div>
<div class="m2"><p>بت به مسجد فغان‌کنان برود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عشق خورشید و بود ما سایه است</p></div>
<div class="m2"><p>هرکجا این بیاید آن برود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سر عشقم چو بر زبان آمد</p></div>
<div class="m2"><p>گر بگویم مرا زبان برود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ره نورد بیان چو سر بکشد</p></div>
<div class="m2"><p>ترسم از دست من عنان برود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به سخن گفتم از دل تنگم</p></div>
<div class="m2"><p>انده حسن دل‌ستان برود</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بر من این داغ از آتش عشقست</p></div>
<div class="m2"><p>که به آب از من این نشان برود</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>دل که فرمانش بر جهان برود</p></div>
<div class="m2"><p>کرد حکمی که جان بر آن برود</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گرد میدان انفس و آفاق</p></div>
<div class="m2"><p>همچو گویی به سر دوان برود</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>از نشان‌های او دلست آگاه</p></div>
<div class="m2"><p>هرکجا دل دهد نشان برود</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>طالب دوست در پی رنگی</p></div>
<div class="m2"><p>راست چون سگ به بوی نان برود</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>مرکب شوق را چو بستی نعل</p></div>
<div class="m2"><p>برکند میخ و آنچنان برود</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>که تو (تا) از مکان شوی بیرون</p></div>
<div class="m2"><p>او به سرحد لامکان برود</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بر براق طلب چو بنشینی</p></div>
<div class="m2"><p>با تو مطلوب هم‌عنان برود</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>از زمین خودی چو برخیزی</p></div>
<div class="m2"><p>در رکاب تو آسمان برود</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>آن زمان در کنار وصل آیی</p></div>
<div class="m2"><p>که تویی تو از میان برود</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>آفتابی که عرش ذره اوست</p></div>
<div class="m2"><p>در دل تنگت آن زمان برود</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>این ره کعبه نیست کندروی</p></div>
<div class="m2"><p>کس بیاری کاروان برود</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>مرد بازاد ناتوانی خویش</p></div>
<div class="m2"><p>تا به جایی که می‌توان برود</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>هرکه در سر هوای او دارد</p></div>
<div class="m2"><p>پایش ار بشکنی به جان برود</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>طلبی می‌کند و گر نرسد</p></div>
<div class="m2"><p>مقبل آن کس که اندر آن برود</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>پای اگر زآستان درون بنهد</p></div>
<div class="m2"><p>همچنین سر بر آستان برود</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>هم درین درد جان به دوست دهد</p></div>
<div class="m2"><p>هم درین کار از جهان برود</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>مرد این ره ز چشم نامحرم</p></div>
<div class="m2"><p>روی پوشیده چون زنان برود</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>سیف فرغانی آن رود این راه</p></div>
<div class="m2"><p>کآشکار آید و نهان برود</p></div></div>