---
title: >-
    شمارهٔ ۵۴
---
# شمارهٔ ۵۴

<div class="b" id="bn1"><div class="m1"><p>من بلبلم و رخ تو گلزار</p></div>
<div class="m2"><p>تو خفته من از غم تو بیدار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جانا تو بنیکویی فریدی</p></div>
<div class="m2"><p>وین زلف چو عنبر تو عطار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفتم که چو روی گل ببینم</p></div>
<div class="m2"><p>کمتر کنم این فغان بسیار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شوق گل روی تو چو بلبل</p></div>
<div class="m2"><p>هر لحظه در آردم بگفتار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>من در طلب تو گم شدستم</p></div>
<div class="m2"><p>خود گم شده چون بود طلب کار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر من همه دوستان بگریند</p></div>
<div class="m2"><p>هرگه که بنالم از غمت زار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دل خسته نگردد از غم تو</p></div>
<div class="m2"><p>هرگز نبود ز مرهم آزار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از دانه خال تو دل من</p></div>
<div class="m2"><p>در دام هوای تو گرفتار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بسیار تنم بجان بکوشید</p></div>
<div class="m2"><p>تا دل ندهد بچون تو دلدار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>با یوسف حسن تو نرستم</p></div>
<div class="m2"><p>زین عشق چو گرگ آدمی خوار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چون جان بفنای تن نمیرد</p></div>
<div class="m2"><p>آن دل که ز عشق گشت بیمار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چون کرد بنای آبگیری</p></div>
<div class="m2"><p>بر خاک در تو اشک گل کار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>وقتست کنون که که رباید</p></div>
<div class="m2"><p>رنگ رخ من ز روی دیوار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>در دست غم تو من چو چنگم</p></div>
<div class="m2"><p>واسباب حیات همچو او تار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چنگی غم تو ناخن جور</p></div>
<div class="m2"><p>گوسخت مزن که بگسلد تار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ای لعل تو شهد مستی انگیز</p></div>
<div class="m2"><p>وی چشم تو مست مردم آزار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>در یاب که تا تو آمدی، رفت</p></div>
<div class="m2"><p>کارم از دست و دستم از کار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>اندوه فراخ رو بصد دست</p></div>
<div class="m2"><p>بر تنگ دلم همی نهد بار</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>دور از تو هر آنکسی که زنده است</p></div>
<div class="m2"><p>بی روی تو زنده ییست مردار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>در دایره وجود گشتم</p></div>
<div class="m2"><p>با مرکز خود شدم دگربار</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بر نقطه مهرت ایستادم</p></div>
<div class="m2"><p>تا پای ز سر کنم چو پرگار</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>افتاد از آن زمان که دیدیم</p></div>
<div class="m2"><p>ناگه رخ چون تو شوخ عیار</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>هم خانه ما بدست نقاب</p></div>
<div class="m2"><p>هم کیسه ما بدست طرار</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>در دوستی تو و ره تو</p></div>
<div class="m2"><p>مرد اوست که ثابتست و سیار</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>گر بر در تو مقیم باشد</p></div>
<div class="m2"><p>سگ سکه بدل کند در آن غار</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>آن شب که بهم نشسته باشیم</p></div>
<div class="m2"><p>در خلوت قرب یار با یار</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>هم بیم بود ز چشم مردم</p></div>
<div class="m2"><p>هم مردم چشم باشد اغیار</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>پرنور چو روی روز کرده</p></div>
<div class="m2"><p>شب را بفروغ شمع رخسار</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>در صحبت دوست دست داده</p></div>
<div class="m2"><p>من سوخته را بهشت دیدار</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>در پرسش ما شکر فشانده</p></div>
<div class="m2"><p>از پسته تنگ خود بخروار</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>کای در چمن امید وصلم</p></div>
<div class="m2"><p>چیده ز برای گل بسی خار</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>جام طرب و هوای خود را</p></div>
<div class="m2"><p>در مجلس ما بگیر و بگذار</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>آن دم بامید مستی وصل</p></div>
<div class="m2"><p>بر بنده رگی نماند هشیار</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>بیرون شده طبع آرزو جوی</p></div>
<div class="m2"><p>بی خود شده عقل خویشتن دار</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>بر صوفی روح چاک گشته</p></div>
<div class="m2"><p>در رقص دل از سماع اسرار</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>در چشم ازو فزوده نوری</p></div>
<div class="m2"><p>در خانه ز من نمانده دیار</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>چون از افق قبای عاشق</p></div>
<div class="m2"><p>سر بر زده آفتاب انوار</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>او وحدت خویش کرده اثبات</p></div>
<div class="m2"><p>اندر دل او بمحو آثار</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>ای از درمی بدانگی کم</p></div>
<div class="m2"><p>خرم بزیادتی دینار</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>مشتی گل تست در کشیده</p></div>
<div class="m2"><p>در چشم هوای تو چو گلنار</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>دلشاد بعالمی که در وی</p></div>
<div class="m2"><p>کس سر نشود مگر بدستار</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>دستت نرسد بدو چو درپاش</p></div>
<div class="m2"><p>این هر دو نیفگنی بیکبار</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>تا پر هوا ز دل نریزد</p></div>
<div class="m2"><p>جانت نشود چو مرغ طیار</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>ای طالب علم عاشقی ورز</p></div>
<div class="m2"><p>خود را نفسی بعشق بسپار</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>کندر درجات فضل پیش است</p></div>
<div class="m2"><p>عشق از همه علمها بمقدار</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>در مدرسه هوای او کس</p></div>
<div class="m2"><p>عالم نشود ببحث و تکرار</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>گر طالب علم این حدیثی</p></div>
<div class="m2"><p>بشکن قلم و بسوز طومار</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>چون عشق لجام بر سرت کرد</p></div>
<div class="m2"><p>دیگر نروی گسسته افسار</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>تو مؤمن و مسلمی و داری</p></div>
<div class="m2"><p>یک خانه پر از بتان پندار</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>در جنب تو دشمنان کافر</p></div>
<div class="m2"><p>در جیب تو سروران کفار</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>تو با همه متحد بسیرت</p></div>
<div class="m2"><p>تو با همه متفق بکردار</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>دایم ز شراب نخوت علم</p></div>
<div class="m2"><p>سرمست روی بگرد بازار</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>جهل تو تویی تست وزین علم</p></div>
<div class="m2"><p>تو بی خبر ای امام مختار</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>تا تو تویی ای بزرگ خود را</p></div>
<div class="m2"><p>با آن همه علم جاهل انگار</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>رو تفرقه دور کن ز خاطر</p></div>
<div class="m2"><p>رو آینه پاک کن ز زنگار</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>کاری می کن که ننگ نبود</p></div>
<div class="m2"><p>از کار جهان پر و تو بی کار</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>وین نیز بدان که من درین شعر</p></div>
<div class="m2"><p>تنبیه تو کرده ام نه انکار</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>گر یوسف دلربای ما را</p></div>
<div class="m2"><p>هستی بعزیز جان خریدار</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>ما یوسف خود نمی فروشیم</p></div>
<div class="m2"><p>تو جان عزیز خود نگهدار</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>مقصود من از سخن جزو نیست</p></div>
<div class="m2"><p>جز مهره چه سود باشد از مار</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>من روی غرض نهفته دارم</p></div>
<div class="m2"><p>در برقع رنگ پوش اشعار</p></div></div>