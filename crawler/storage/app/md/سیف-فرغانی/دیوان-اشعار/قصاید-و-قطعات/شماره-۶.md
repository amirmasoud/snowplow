---
title: >-
    شمارهٔ ۶
---
# شمارهٔ ۶

<div class="b" id="bn1"><div class="m1"><p>حسن آن صورت از صفت بدرست</p></div>
<div class="m2"><p>که بمعنی ز جمله خوبترست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روی حرف ز حسن او دیدم</p></div>
<div class="m2"><p>از معانی درو بسی صورست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سور مصحف نکویی را</p></div>
<div class="m2"><p>همچو الحمد سرور سورست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای ز روی تو حسن را زینت</p></div>
<div class="m2"><p>حسن از آن روی در جهان سمرست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از دو عالم مرا تویی مقصود</p></div>
<div class="m2"><p>غرض آدمی ز کان گهرست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زین صدفها امید من لولوست</p></div>
<div class="m2"><p>زین قصبها مراد من شکرست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از تجلی تو منم آگاه</p></div>
<div class="m2"><p>ذره(را) از طلوع خور خبرست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نظری بر چو من گدا انداز</p></div>
<div class="m2"><p>که نه هر عاشقی توانگرست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ذره گر چند هست خوار و حقیر</p></div>
<div class="m2"><p>هم درو آفتاب را نظرست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گرچه خفتن طریق عاشق نیست</p></div>
<div class="m2"><p>کو بشب همچو روز در سهرست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آستان تو عاشقان ترا</p></div>
<div class="m2"><p>همچو گردن مدام زیر سرست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>خانه عشق حصن ربانیست</p></div>
<div class="m2"><p>هر که در وی نشست بی خطرست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بخورد همچو گوسپندش گرگ</p></div>
<div class="m2"><p>هرکه زین خانه همچو سگ بدرست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>عاشق تو بپای همت رفت</p></div>
<div class="m2"><p>طیران مرغ را ببال و پرست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>وقت بی ناله نیست عاشق را</p></div>
<div class="m2"><p>شب و روز این خروس را سحرست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>خویشتن را بعشق بشکستم</p></div>
<div class="m2"><p>که هزیمت درین وغا ظفرست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>هرکجا عشق تو نزول کند</p></div>
<div class="m2"><p>چان عاشق کمیته ما حضرست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>مرد کز خوان عشق قوت نیافت</p></div>
<div class="m2"><p>بهر نان همچو گاو برزگرست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>در رهت بهر خون شدن جانا</p></div>
<div class="m2"><p>همه احشای اهل دل جگرست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>از برای سرادق عشقت</p></div>
<div class="m2"><p>عرصه هر دو کون مختصرست</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>هر کجا عشق تو تویی آنجا</p></div>
<div class="m2"><p>عشق تو چون تو میوه را زهرست</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>دایم از حسرت گل رویت</p></div>
<div class="m2"><p>میوه نالان چو مرغ بر شجرست</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ملک از سوز عشق بهره نداشت</p></div>
<div class="m2"><p>کین نمک در خمیر بوالبشرست</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>تا نفس هست سیف فرغانی</p></div>
<div class="m2"><p>جهد می کن که جهد را اثرست</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>هیچ بی ذکر دوست شعر مگوی</p></div>
<div class="m2"><p>سکه بر روی زر جمال زرست</p></div></div>