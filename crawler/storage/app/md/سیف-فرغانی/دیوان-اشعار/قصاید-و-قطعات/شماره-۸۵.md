---
title: >-
    شمارهٔ ۸۵
---
# شمارهٔ ۸۵

<div class="b" id="bn1"><div class="m1"><p>ای شهنشه چون غلامانت به در باز آمدم</p></div>
<div class="m2"><p>عیب مشمر کز درت من بی‌هنر باز آمدم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دی برفتم کز پی فردا مگر کاری کنم</p></div>
<div class="m2"><p>چون گدا امروز ناگاهان به در باز آمدم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>صولجان ارجعی زد در قفای من چو گوی</p></div>
<div class="m2"><p>رو نهادم سوی این میدان به سر باز آمدم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هرکجا رفتم غمت پیش از من آنجا رفته بود</p></div>
<div class="m2"><p>گفتم از دست غمت این المفر باز آمدم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گرد بازار جهان دکان به دکان همچو سیم</p></div>
<div class="m2"><p>گشتم و آخر به معدن همچو زر باز آمدم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اندر آن جانب مرا گلزار نزهت خشک گشت</p></div>
<div class="m2"><p>من به بوی اینچنین گل‌های تر باز آمدم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از جوار تو که خورشید از تو دارد نور روی</p></div>
<div class="m2"><p>چون هلالی رفته بودم چون قمر باز آمدم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>من ازین دریا که موجش گوهر افشاند چو ابر</p></div>
<div class="m2"><p>چون بخاری رفته بودم چون مطر باز آمدم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بهر ادراک معانی در نگارستان دهر</p></div>
<div class="m2"><p>یک‌به‌یک کردم تماشای صور باز آمدم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گنج حکمت بود در هر ذره زآن خورشیدوار</p></div>
<div class="m2"><p>اندر آن ویرانه‌ها کردم نظر باز آمدم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بچه عنقا بدم آنجا شکسته پر و بال</p></div>
<div class="m2"><p>چون دگر بارم برآمد بال و پر باز آمدم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مدتی در دامگاه خاک بودم دانه چین</p></div>
<div class="m2"><p>یاد(م) آمد لذت این آبخور باز آمدم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چون مگس آنجا بسی کردم دهان در تلخ و شور</p></div>
<div class="m2"><p>خوشتر از شیرین ندیدم وز شکر باز آمدم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>شکرستان ترا چون من مگس درخور بود</p></div>
<div class="m2"><p>زین سوم راندی من از سوی دگر باز آمدم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>همچو منج انگبین در کنج بودم منزوی</p></div>
<div class="m2"><p>چون گلی دیدم برافراز شجر باز آمدم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>نحل بی‌برگم مرا بوی بهار آمد ز تو</p></div>
<div class="m2"><p>تا بسازم انگبین سوی زهر باز آمدم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>در سفر با دیگران کردم زیان و نزد تو</p></div>
<div class="m2"><p>در اقامت سود دیدم از سفر باز آمدم</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>روزگاری بر سر این کوه بودم ابروار</p></div>
<div class="m2"><p>رفتم و بر بحر و بر کردم گذر باز آمدم</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>حامل در بود از مهر تو دل همچون صدف</p></div>
<div class="m2"><p>قطره‌ای بودم که رفتم چون گهر باز آمدم</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بهر یعقوبان نابینای هجران چون بشیر</p></div>
<div class="m2"><p>سوی کنعان بردم از یوسف خبر باز آمدم</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>این که می‌گویم سراسر وصف حال کاملست</p></div>
<div class="m2"><p>هرچه گفتم بهر خویش از خیر و شر باز آمدم</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>من چو مجنونان بسوی کوی لیلی می‌شدم</p></div>
<div class="m2"><p>تا دوای خود کنم دیوانه‌تر باز آمدم</p></div></div>