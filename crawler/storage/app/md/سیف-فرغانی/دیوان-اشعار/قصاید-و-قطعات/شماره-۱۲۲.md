---
title: >-
    شمارهٔ ۱۲۲
---
# شمارهٔ ۱۲۲

<div class="b" id="bn1"><div class="m1"><p>ای تن‌آرامی که خون جان به گردن می‌بری</p></div>
<div class="m2"><p>راحت جان ترک کرده زحمت تن می‌بری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تن‌پرستی ترک کن چون عشق کردی اختیار</p></div>
<div class="m2"><p>رو به کار دوست داری بار دشمن می‌بری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با یزید انبازی اندر خون شاهی چون حسین</p></div>
<div class="m2"><p>چون تو در حرب از برای شمر جوشن می‌بری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رنج بردن در طریق عاشقی بیهوده نیست</p></div>
<div class="m2"><p>در نکویان بدگمانی گر چنین ظن می‌بری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر ز عشقت محنت آید صبر کن کز وصل دوست</p></div>
<div class="m2"><p>راحتی بینی اگر رنجی درین فن می‌بری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هم عصایی هم صفورایی به دست آرد کلیم</p></div>
<div class="m2"><p>ور به چوپانی ز مصرش سوی مدین می‌بری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر چو خسرو چند روز از دست دادی ملک پارس</p></div>
<div class="m2"><p>همچو شیرین شکرستانی زار من می‌بری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نیستی شاکر که خشنودی شیرین حاصلست</p></div>
<div class="m2"><p>رنج اگر در سنگ چون فرهاد کُه‌کن می‌بری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>همچو رستم سهل گردد راه توران بر دلت</p></div>
<div class="m2"><p>چون سوی ایران سپهداری چو بیژن می‌بری</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به ز بیداری بود جای دگر سگ را و گر</p></div>
<div class="m2"><p>بر در اصحاب کهفش بهر خفتن می‌بری</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دوست چون گل جلوه رخسار خود کرده است و تو</p></div>
<div class="m2"><p>همچو بلبل روزگار خود به گفتن می‌بری</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گر بقو الان آن حضرت فرستی شعر خود</p></div>
<div class="m2"><p>نزد آواز جلاجل بانگ هاون می‌بری</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>شعر خود نزدیک او آگه نه‌ای ای زنده‌دل</p></div>
<div class="m2"><p>کز چراغ مرده پیش شمع روغن می‌بری</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>سیف فرغانی ازین کشت ار نچیدی خوشه‌ای</p></div>
<div class="m2"><p>شکر کن چون دانه‌ای زاطراف خرمن می‌بری</p></div></div>