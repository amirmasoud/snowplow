---
title: >-
    شمارهٔ ۳۹
---
# شمارهٔ ۳۹

<div class="b" id="bn1"><div class="m1"><p>کم خور غم تنی که حیاتش بجان بود</p></div>
<div class="m2"><p>چیزی طلب که زندگی جان بآن بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هیچش ز تخم عشق معطل روا مدار</p></div>
<div class="m2"><p>تا در زمین جسم تو آب روان بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آنکس رسد بدولت وصی که مرورا</p></div>
<div class="m2"><p>روح سبک ز بار محبت گران بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون استخوان مرده نیاید بهیچ کار</p></div>
<div class="m2"><p>عشقی که زنده یی چو تواش در میان بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>معشوق روح بخش باول قدم چو مرگ</p></div>
<div class="m2"><p>از هفت عضو هستی تو جان ستان بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آخر بعشق زنده کند مر ترا که اوست</p></div>
<div class="m2"><p>کآب حیات از آتش عشقش روان بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از تو چه نقشهاست در آیینه مثال</p></div>
<div class="m2"><p>دیدند و گر تو نیز ببینی چنان بود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>این حرف خوانده ای تو که بر دفتر وجود</p></div>
<div class="m2"><p>لفظیست صورت تو که معنیش جان بود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>با نور چشم فهم تو پنهان لطیفه ییست</p></div>
<div class="m2"><p>جان تو آیتیست که تفسیرش آن بود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ای دل ازین حدیث زبان در کشیده به</p></div>
<div class="m2"><p>خود شرح این حدیث چه کار زبان بود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خود را مکن میان دل و خلق ترجمان</p></div>
<div class="m2"><p>تا سر میان عشق و دلت ترجمان بود</p></div></div>