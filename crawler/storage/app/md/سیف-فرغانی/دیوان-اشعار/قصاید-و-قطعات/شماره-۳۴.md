---
title: >-
    شمارهٔ ۳۴
---
# شمارهٔ ۳۴

<div class="b" id="bn1"><div class="m1"><p>پیوستگان عشق تو از خود بریده‌اند</p></div>
<div class="m2"><p>الفت گرفته با تو و از خود رمیده‌اند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پیغمبران نیند ولیکن چو جبرئیل</p></div>
<div class="m2"><p>بی‌واسطه کلام تو از تو شنیده‌اند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون چشم روشنند و ازین روی دیده‌وار</p></div>
<div class="m2"><p>بسیار چیز دیده و خود را ندیده‌اند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون سایه بر زمین و از آن سوی آسمان</p></div>
<div class="m2"><p>مانند آفتاب علم برکشیده‌اند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دامن بخار عشق درآویختست شان</p></div>
<div class="m2"><p>در وجد از آن چو غنچه گریبان دریده‌اند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از زادگان مادر فطرت چو بنگری</p></div>
<div class="m2"><p>این قوم بالغ و دگران نارسیده‌اند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>وز مثنوی روز و شب و نظم کاینات</p></div>
<div class="m2"><p>ارکان یکی رباعی وایشان قصیده‌اند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سری که کس نگفت از ایشان شنیده‌ایم</p></div>
<div class="m2"><p>کآنجا که کس نمی‌رسد ایشان رسیده‌اند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آن عاشقان صادق کانفاس گرم خویش</p></div>
<div class="m2"><p>چون صبح هر سحر به جهان دردمیده‌اند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>محتاج نه به خلق و خلایق فقیرشان</p></div>
<div class="m2"><p>نی آفریدگار و نه نیز آفریده‌اند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>اندر جریده‌ای که ز خاصان برند نام</p></div>
<div class="m2"><p>این پابرهنگان گدا سرجریده‌اند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>حلاج‌وار مست کند کاینات را</p></div>
<div class="m2"><p>یک جرعه زآن شراب که ایشان چشیده‌اند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>با کس کدورتی نه ازیرا به جان و دل</p></div>
<div class="m2"><p>روشن چو چشم و پاک‌تر از آب دیده‌اند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>دنیا اگرچه دشمن ایشان بود ولیک</p></div>
<div class="m2"><p>در وی گمان مبر که به جز دوست دیده‌اند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>اندر غزل به حسن کنم ذکرشان از آنک</p></div>
<div class="m2"><p>هریک چو شاه‌بیت به نیکی فریده‌اند</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>با خلق در نماز و تواضع برای حق</p></div>
<div class="m2"><p>پیوسته در رکوع چو ابرو خمیده‌اند</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>در شوق آن گروه که از اطلس و نسیج</p></div>
<div class="m2"><p>برخود چو کرم پیله بریشم تنیده‌اند</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>با غیر دوست بیع و شری کرده منقطع</p></div>
<div class="m2"><p>خود را بدو فروخته و او را خریده‌اند</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>زآن خانه مشاهده‌شان پر ز شهد شد</p></div>
<div class="m2"><p>کز گلشن مشاهده گل‌ها چریده‌اند</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>مرغان اوج قرب که اندر هوای او</p></div>
<div class="m2"><p>بی‌پای همچو باد به هرجا پریده‌اند</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>سرپای کرده در طلب خاک کوی دوست</p></div>
<div class="m2"><p>بی‌بال همچو آب به هرسو دویده‌اند</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>در سیر و گردشند به جان همچو آسمان</p></div>
<div class="m2"><p>گرچه به چشم همچو زمین آرمیده‌اند</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>در راحتند خلق از ایشان مدام سیف</p></div>
<div class="m2"><p>اینان مگر ز رحمت محض آفریده‌اند</p></div></div>