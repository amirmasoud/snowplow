---
title: >-
    شمارهٔ ۲۸
---
# شمارهٔ ۲۸

<div class="b" id="bn1"><div class="m1"><p>دین و دولت قرین یکدگرند</p></div>
<div class="m2"><p>همچنین بود و همچنین باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دولتی را که دین کند بنیاد</p></div>
<div class="m2"><p>همچو بنیاد دین متین باشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بقرانها زوال ممکن نیست</p></div>
<div class="m2"><p>دولتی را که دین قرین باشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هست ای شاه دولت بی دین</p></div>
<div class="m2"><p>خاتمی کش خزف نگین باشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مهربانی طمع مدار ز خلق</p></div>
<div class="m2"><p>دین چو با دولتت بکین باشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نیست حاجت بعون و نصرت کس</p></div>
<div class="m2"><p>دولتی را که دین معین باشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دنیی و آخرت ببردی اگر</p></div>
<div class="m2"><p>دولت تو معین دین باشد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>توأمانند ملک و دین باهم</p></div>
<div class="m2"><p>شمع همزاد انگبین باشد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>صاحب دولت ار بود دین دار،</p></div>
<div class="m2"><p>خصمش ار شاه روم و چین باشد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دست و دولت ورا بود بمثل</p></div>
<div class="m2"><p>گر علمدارش آستین باشد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>همنشین باش با نکوکاران</p></div>
<div class="m2"><p>مرد نیکو بهمنشین باشد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بزرش همچو گلشکر بخرند</p></div>
<div class="m2"><p>خار چون با ترانگبین باشد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>سرکه چون با عسل درآمیزد</p></div>
<div class="m2"><p>نام نیکش سکنجبین باشد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بشنو پند سیف فرغانی</p></div>
<div class="m2"><p>که سخنهای او گزین باشد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بتوانگر که ملک دارد و مال</p></div>
<div class="m2"><p>تحفه اهل فقر این باشد</p></div></div>