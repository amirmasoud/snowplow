---
title: >-
    شمارهٔ ۴۴
---
# شمارهٔ ۴۴

<div class="b" id="bn1"><div class="m1"><p>ای مقبل ار سعادت دنیات رو نماید</p></div>
<div class="m2"><p>وآن زشت رو بچشم بد تو نکو نماید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از برقعی که تازه بود رنگ او بخوبی</p></div>
<div class="m2"><p>این کهنه گنده پیر بتو روی نو نماید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>امروزه غره ای تو بدین خوب رو و بی شک</p></div>
<div class="m2"><p>فردا عروس زشتی خود را بشو نماید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون پای بست سلسله مهر او شدستی</p></div>
<div class="m2"><p>اصلع سری بچشم تو زنجیر مو نماید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هرکس که خواستار وی آمد بدست عشوه</p></div>
<div class="m2"><p>چشم دلش ببندد و خود را بدو نماید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اندک بقاست چون گل و نزد تو هست خارش</p></div>
<div class="m2"><p>چون تازه سبزه یی که بر اطراف جو نماید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عزلت کند اگرچه عمل دار ملک باشی</p></div>
<div class="m2"><p>امروز رنگ دیدی فردات بو نماید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آیینه یی است موی سپید تو ای سیه دل</p></div>
<div class="m2"><p>هرگه که اندرو نگری مرگ رو نماید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در چشم اهل عقل برافراز تخت هستی</p></div>
<div class="m2"><p>مانند بیذقی که بچاهی فرو نماید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>امروز ظالم ار چو توانگر عزیز باشد</p></div>
<div class="m2"><p>فردات خوارتر ز گدایان کو نماید</p></div></div>