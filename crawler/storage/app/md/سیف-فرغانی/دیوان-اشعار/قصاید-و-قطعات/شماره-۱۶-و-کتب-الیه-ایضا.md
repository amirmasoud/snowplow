---
title: >-
    شمارهٔ ۱۶ - و کتب الیه ایضا
---
# شمارهٔ ۱۶ - و کتب الیه ایضا

<div class="b" id="bn1"><div class="m1"><p>دلم از کار این جهان بگرفت</p></div>
<div class="m2"><p>راست خواهی دلم ز جان بگرفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مدح سعدی نگفته بیتی چند</p></div>
<div class="m2"><p>طوطی نطق را زبان بگرفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آفتابیست آسمان بارش</p></div>
<div class="m2"><p>که زمین بستد و زمان بگرفت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پادشاه سخن بتیغ زبان</p></div>
<div class="m2"><p>تا بجایی که می توان بگرفت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سخن او که هست آب حیات</p></div>
<div class="m2"><p>چون سکندر همه جهان بگرفت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دیگری جای او نگیرد و او</p></div>
<div class="m2"><p>بسخن جای دیگران بگرفت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بلبل طبع او صفیری زد</p></div>
<div class="m2"><p>همه آفاق گلستان بگرفت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دم سرد چمن ز خجلت آن</p></div>
<div class="m2"><p>آمد و غنچه را دهان بگرفت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دست صیتش که در جهان علمست</p></div>
<div class="m2"><p>دامن آخرالزمان بگرفت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بحر معنیست او وزین سبب است</p></div>
<div class="m2"><p>که چو بحر از جهان کران بگرفت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>عرضه کردند بر دلش دو جهان</p></div>
<div class="m2"><p>همتش این بداد و آن بگرفت</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>سخن او بسمع من چو رسید</p></div>
<div class="m2"><p>مر مرا شوق او چنان بگرفت</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>که دل من ز خاتم مهرش</p></div>
<div class="m2"><p>همچو شمع از نگین نشان بگرفت</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>طوطی طبعش از سخن شکری</p></div>
<div class="m2"><p>بدهان شکرفشان بگرفت</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>زهره از بهر نقل خویش آنرا</p></div>
<div class="m2"><p>در طبقهای آسمان بگرفت</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ای بزرگی که طبع وقادت</p></div>
<div class="m2"><p>خرده بر عقل خرده دان بگرفت</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>وقت تقریر مدحت تو مرا</p></div>
<div class="m2"><p>این معانی ره بیان بگرفت</p></div></div>