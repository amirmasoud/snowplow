---
title: >-
    شمارهٔ ۹۲
---
# شمارهٔ ۹۲

<div class="b" id="bn1"><div class="m1"><p>در شب زلف تو قمر دیدن</p></div>
<div class="m2"><p>خوش بود خاصه هر سحر دیدن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا بکی همچو سایه خانه</p></div>
<div class="m2"><p>آفتاب از شکاف در دیدن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پرده بردار از آن رخ پرنور</p></div>
<div class="m2"><p>که ملولم ز ماه و خور دیدن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گرچه کس را نمی شود حاصل</p></div>
<div class="m2"><p>لذت شکر از شکر دیدن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هست دشوار دیدن تو چنان</p></div>
<div class="m2"><p>که ز خود مشکلست سر دیدن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>روی منما بهر ضعیف دلی</p></div>
<div class="m2"><p>گرچه ناید ز بی بصر دیدن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>که چو سیماب مضطرب گردد</p></div>
<div class="m2"><p>دل مسکین ز روی زر دیدن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>میوه یی ده ز باغ وصل مرا</p></div>
<div class="m2"><p>که دلم خون شد از زهر دیدن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آشنای ترا سزد زین باغ</p></div>
<div class="m2"><p>همچو بیگانگان شجر دیدن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>طالب رؤیت مؤثر شد</p></div>
<div class="m2"><p>چون کلیم الله از اثر دیدن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گرچه صبرم گرفته است کمی</p></div>
<div class="m2"><p>شوقم افزون شود بهر دیدن</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>زخم چوگان شوق می باید</p></div>
<div class="m2"><p>بر دل از بهر ره نور دیدن</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گرد میدان عشق می نتوان</p></div>
<div class="m2"><p>بسر خود چو گوی گردیدن</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ای دل ای دل ترا همه چیزی</p></div>
<div class="m2"><p>شد میسر ازو مگر دیدن</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بفروغ چراغ عشق توان</p></div>
<div class="m2"><p>هر دو عالم بیک نظر دیدن</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>جان معنی و معنی جان را</p></div>
<div class="m2"><p>در پس پرده صور دیدن</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>اوست پیش و پس همه چیزی</p></div>
<div class="m2"><p>چون غلط می کنی تو در دیدن</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>علم رسمیت منع کرد از عشق</p></div>
<div class="m2"><p>بصدف ماندی از گهر دیدن</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>مرد این ره نظر بخود نکند</p></div>
<div class="m2"><p>از عجایب درین سفر دیدن</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گر سر این رهت بود شرطست</p></div>
<div class="m2"><p>پای طاوس را چو پر دیدن</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>نزد ما از خواص این ره هست</p></div>
<div class="m2"><p>در یکی گام صد خطر دیدن</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چند خود را خلاف باید کرد</p></div>
<div class="m2"><p>در مقامات خیر و شر دیدن</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>تا دل و دیده اتفاق کنند</p></div>
<div class="m2"><p>روی او را بیکدگر دیدن</p></div></div>