---
title: >-
    شمارهٔ ۷۳
---
# شمارهٔ ۷۳

<div class="b" id="bn1"><div class="m1"><p>چو کرد نرگس مستش ز تیر مژگان تیغ</p></div>
<div class="m2"><p>چو چشمم آب ز خون جگر خورد آن تیغ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سپر فگندم از آن دلبر کمان ابرو</p></div>
<div class="m2"><p>که بهر کشتن من کرد تیر مژگان تیغ</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز جان نشانه کن و از نشانه ساز سپر</p></div>
<div class="m2"><p>که تیر غمزه آن ترک راست پیکان تیغ</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بگرد نرگس مخمور او خدنگ مژه است</p></div>
<div class="m2"><p>بدست ترکان تیر و بچنگ مستان تیغ</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو روی خوبش در باغ نیست خندان گل</p></div>
<div class="m2"><p>چو خوی تیزش در جنگ نیست بران تیغ</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کسی که با من شمشیر آشکار زدی</p></div>
<div class="m2"><p>چو تیر غمزه او دید کرد پنهان تیغ</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بتیغم از دهن او جدا نگردد لب</p></div>
<div class="m2"><p>وگر چو اره برآرد هزار دندان تیغ</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ایا ز روی تو اسلام کرده پشت قوی</p></div>
<div class="m2"><p>مکش چو لشکر کافر بر اهل ایمان تیغ</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بحسن مملکت مصر حاصلست ترا</p></div>
<div class="m2"><p>چو یوسف ار نزنی با عزیز ریان تیغ</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>میان زمره خوبان تو حجت الحقی</p></div>
<div class="m2"><p>ز بهر منکر آن حجتست برهان تیغ</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز دست عشق تو از بس که خورده ام شمشیر،</p></div>
<div class="m2"><p>و گرچه از کف تو در در است درمان تیغ،</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چنان شدم که چو در گردن افگنم جامه</p></div>
<div class="m2"><p>بجای من بدر آرد سر از گریبان تیغ</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>خط تو دیدم و از بنده دل برفت که هست</p></div>
<div class="m2"><p>برای فتح سبا نامه سلیمان تیغ</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ز بهر کار تو با نفس خویش کردم صلح</p></div>
<div class="m2"><p>بجزیه باز گرفتم ز کافرستان تیغ</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>که تا بطاعت و خدمت سری فرو نارد</p></div>
<div class="m2"><p>خلیفه باز نگیرد ز اهل طغیان تیغ</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>میان ما و مخالف برای تو جنگست</p></div>
<div class="m2"><p>کشیده از دو طرف یک بیک دلیران تیغ</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>پی عروس خلافت که در کنار آید</p></div>
<div class="m2"><p>میان لشکر بومسلم است و مروان تیغ</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بوصل تو نرسد بنده چون رقیبانت</p></div>
<div class="m2"><p>کشیده اند چپ و راست چون غلامان تیغ</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>کجا سریر بخارا رسد با یلک خان</p></div>
<div class="m2"><p>سبکتکین چو زند بهر آل سامان تیغ</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>دو چشم راست چو مردم بهم رسیدندی</p></div>
<div class="m2"><p>ز بینی ار نبدی در میان ایشان تیغ</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ز کاسه سر لشکر بریزد آب حیات</p></div>
<div class="m2"><p>دو پادشا چو زنند از برای یک نان تیغ</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>برای چون توپری صورتی عجب نبود</p></div>
<div class="m2"><p>که با سپاه شیاطین زنند انسان تیغ</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>نظر کنیم بدزدی سوی تو و ترسیم</p></div>
<div class="m2"><p>که هست گرد تو از غیرت رقیبان تیغ</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ز بیم و هیبت خنجر بمرد ناکشته</p></div>
<div class="m2"><p>چو دزد دید که جلاد زد بسوهان تیغ</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ز آفتاب جمال تو رو نگردانم</p></div>
<div class="m2"><p>وگر ز ابر ببارد بجای باران تیغ</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ز عشق گل نرود عندلیب جای دگر</p></div>
<div class="m2"><p>وگرچه خار کشیدست در گلستان تیغ</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>منم قتیل تو ای جان و آن اثر دارد</p></div>
<div class="m2"><p>غم تو در دل عاشق که در شهیدان تیغ</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>اگر چه حکم روانی ولی مران و مزن</p></div>
<div class="m2"><p>بقوتی که تو داری برین ضعیفان تیغ</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>که بهر حفظ ولایت دعای درویشان</p></div>
<div class="m2"><p>چو از وزیر قلم باشد و ز سلطان تیغ</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>مبارزان همه بر تن زنند و این مردان</p></div>
<div class="m2"><p>بدست صفدر همت زنند بر جان تیغ</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>تو آب دیده درویش را گزاف مدان</p></div>
<div class="m2"><p>که ابر گریان دارد ز برق خندان تیغ</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>چو دردمند هوای توایم هر ساعت</p></div>
<div class="m2"><p>فرو مبر بجراحات دردمندان تیغ</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>گرش ز سنگ بود پشت همت ایشان</p></div>
<div class="m2"><p>فرو برد شتر کوه را بکوهان تیغ</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>ز جمله خلق بقیمت بهند عشاقت</p></div>
<div class="m2"><p>کجا بود ببها همچو سوزن ارزان تیغ</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>بسوی روی تو از چشم ناوک اندازت</p></div>
<div class="m2"><p>مبارزان نظر کرده اند پنهان تیغ</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>ز نیکوان جهان کس ترا منازع نیست</p></div>
<div class="m2"><p>که با تو چون سپر افگنده اند خوبان تیغ</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>نه در مقابله رویهای خوب آید</p></div>
<div class="m2"><p>بسان آینه گر روشنست و تابان تیغ</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>مقیم کوی ترا از رقیب (تو) چه غمست</p></div>
<div class="m2"><p>که بر کسی نزند در بهشت رضوان تیغ</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>پی سرور دل تنگ بنده چون شادی</p></div>
<div class="m2"><p>همی زند غم تو با سپاه احزان تیغ</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>ز غیر تو غم عشق تو جان و دل راهست</p></div>
<div class="m2"><p>چو مال را قلم و ملک را نگهبان تیغ</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>ایا بزهد مشهر ز عشق لاف مزن</p></div>
<div class="m2"><p>که نیست لایق آن دست سبحه گردان تیغ</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>ز خنجر ملک الموت بیم نیست مرا</p></div>
<div class="m2"><p>چو در کفش نبود از فراق جانان تیغ</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>مرا سپاه حوادث ز پای در نارد</p></div>
<div class="m2"><p>چو دست او نزند بر سرم ز هجران تیغ</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>چو عاشقی نکند سنگ به بود ز آن دل</p></div>
<div class="m2"><p>چو دشمنی نکشد چوب به بود زآن تیغ</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>چو راه می نروی خرقه یی مپوش چو من</p></div>
<div class="m2"><p>چو گردنی نزنی گرد سر مگردان تیغ</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>کمال نفس بعشق است مرد طالب را</p></div>
<div class="m2"><p>چه ضبط ملک کنی چون گرفت نقصان تیغ</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>تمام همچو سپر چون شود کمال هلال</p></div>
<div class="m2"><p>اگر برو نزند آفتاب رخشان تیغ</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>برای دوست بکش نفس را که با کافر</p></div>
<div class="m2"><p>چو انبیا ز پی دین زند مسلمان تیغ</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>بهر چه دوست کند اعتراض نتوان کرد</p></div>
<div class="m2"><p>که بر خلیفه و سلطان کشید نتوان تیغ</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>بگاه صلح ز ما طاعت وز جانان حکم</p></div>
<div class="m2"><p>بوقت حرب ز ما گردن و ازیشان تیغ</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>برای نان بود اندر میان شاهان جنگ</p></div>
<div class="m2"><p>ز بهر جان نبود در میان یاران تیغ</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>رعیتی، مکن ای خواجه با سلاطین حرب</p></div>
<div class="m2"><p>پیاده ای، مزن ای شاه با سواران تیغ</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>چو زال زر برو ایران زمین نگه می دار</p></div>
<div class="m2"><p>چو رستم ار نزنی در بلاد توران تیغ</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>بعشق قمع توان کرد نفس را، که زدند</p></div>
<div class="m2"><p>عرب بقوت دین با ملوک ساسان تیغ</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>کند ز هستی خود مرد را مجرد عشق</p></div>
<div class="m2"><p>ز خوان ملک بود شاه را مگس ران تیغ</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>ز بهر دوست بکن صد مجاهدت با خود</p></div>
<div class="m2"><p>برای ملک بزن همچو پادشاهان تیغ</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>بترس از سرت آنجا که عشق پای نهاد</p></div>
<div class="m2"><p>بپوش جوشن آنجا که گشت عریان تیغ</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>چو عشق مالک امر تو شد از آن پس ملک</p></div>
<div class="m2"><p>بده بهر که خوهی وز ملوک بستان تیغ</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>چو بوسعید خراسان بآل سلجق داد</p></div>
<div class="m2"><p>نراند سلطان مسعود در خراسان تیغ</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>شود بخصم تو بر باد آتش افشان خاک</p></div>
<div class="m2"><p>شود بدست تو در آب گوهرافشان تیغ</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>بدست همت بر صفدران جوشن پوش</p></div>
<div class="m2"><p>چو برق از پس خفتان ابر می ران تیغ</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>اگرچه علمت باشد برای خرق حجب</p></div>
<div class="m2"><p>ببایدت ز مقالات اهل عرفان تیغ</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>که بهر نصرت سلطان شرع در خوردست</p></div>
<div class="m2"><p>ز سنت نبوی با لوای قرآن تیغ</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>ز راه راحت تن پای سعی باز گرفت</p></div>
<div class="m2"><p>چو دست همت دل راند بر سر جان تیغ</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>بعمر اگر خضری از فنا همی اندیش</p></div>
<div class="m2"><p>که مرگ تعبیه دارد در آب حیوان تیغ</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>بچشم تیز نظر دل بنیکوان مسپار</p></div>
<div class="m2"><p>بمرد معرکه جویی بده نه چوپان تیغ</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>مدام فکر بترکیب شعر صرف مکن</p></div>
<div class="m2"><p>بدست خویش مده بعد ازین بخصمان تیغ</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>زبان بخامشی اندر دهان نگه می دار</p></div>
<div class="m2"><p>ز بند یابی امان گر کنی بزندان تیغ</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>بعارفان نرسد کس بشاعری هرگز</p></div>
<div class="m2"><p>کجا رساند مریخ را بکیوان تیغ</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>براه عشق نشاید ز شعر کرد دلیل</p></div>
<div class="m2"><p>بگاه حرب نزیبد ز بیل دهقان تیغ</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>کجا سماع کند بانگ کوس فتح و ظفر</p></div>
<div class="m2"><p>سپهبدی که دهد در وغا بکوران تیغ</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>بوقت حمله سپهدار وصف شکن نشود</p></div>
<div class="m2"><p>وگر چه برزگری یافت در بیابان تیغ</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>برین نهج که تویی با چنین بلاغت شعر</p></div>
<div class="m2"><p>تو حیدری نزند با تو هر سخن دان تیغ</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>ز صفدران سخن پیش ازین نپندارم</p></div>
<div class="m2"><p>که کس کشیده بود غیر تو ازین سان تیغ</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>سخن وران جهان گر بشعر سحر کنند</p></div>
<div class="m2"><p>درین قصیده بیاشامدش چو ثعبان تیغ</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>بنزد دوست مبر شعر سیف فرغانی</p></div>
<div class="m2"><p>بروز رزم مکش پیش پوردستان تیغ</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>بغیر حق نشود مشتغل بکس عارف</p></div>
<div class="m2"><p>بجز علی نبود مفتخر ز مردان تیغ</p></div></div>