---
title: >-
    شمارهٔ ۱۱۵
---
# شمارهٔ ۱۱۵

<div class="b" id="bn1"><div class="m1"><p>عروس چمن راست زیور شکوفه</p></div>
<div class="m2"><p>سر شاخ راهست افسر شکوفه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کنون بر (سر) شاخ فرقی ندارد</p></div>
<div class="m2"><p>شکوفه ز زیور ز زیور شکوفه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بفصل خزان بود صفراش غالب</p></div>
<div class="m2"><p>کنون باغ را هست در خور شکوفه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بصد پرده بلبل نواساز گردد</p></div>
<div class="m2"><p>چو بگشاد بر شاخ صد در شکوفه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در آن دم که شاخ آستین برفشاند</p></div>
<div class="m2"><p>همی آر دامان و می بر شکوفه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یکی عاشق نازنین است بلبل</p></div>
<div class="m2"><p>یکی شاهدی نازپرور شکوفه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو آگه شد از بی نوایی بلبل</p></div>
<div class="m2"><p>ز دوری خویش آن سمن بر شکوفه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>درختان بی برگ را کرد آنک</p></div>
<div class="m2"><p>بسیم و زر خود توانگر شکوفه</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>برغم زمستان ممسک بهر سو</p></div>
<div class="m2"><p>گل سیمتن می کند زر شکوفه</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بیک هفته چون گل جهانگیر گردد</p></div>
<div class="m2"><p>که سلطان بهارست و لشکر شکوفه</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>درختست طوبی صفت زآنکه بستان</p></div>
<div class="m2"><p>بهشتست از آن حور پیکر شکوفه</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ز نامحرم و مست چون باغ پر شد</p></div>
<div class="m2"><p>زاستار غیب آن مستر شکوفه</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>برون آمد و مادر خویشتن (را)</p></div>
<div class="m2"><p>در آورد در زیر چادر شکوفه</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>شراب از کجا خورد مطرب که بودش</p></div>
<div class="m2"><p>که شاخست سرمست و ساغر شکوفه</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چو نقاش قدرت روان کرد خامه</p></div>
<div class="m2"><p>قلم راند بر نقش آزر شکوفه</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ز نفخ لواحق شود همچو عیسی</p></div>
<div class="m2"><p>بروح نباتی مصور شکوفه</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ازین پس کند شاخ همچون عصا را</p></div>
<div class="m2"><p>چو دست کلیم پیمبر شکوفه</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>زمین مدتی بود چون خارپشتی</p></div>
<div class="m2"><p>کشیده درون چون کشف سر شکوفه</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>کنون زینت بال طاوس یابد</p></div>
<div class="m2"><p>چو بگشاد در گلستان پر شکوفه</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ازین پیش با خار و خس بود ملحق</p></div>
<div class="m2"><p>که در شاخ تر بود مضمر شکوفه</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>کنون سبزه را خفته در زیر سایه</p></div>
<div class="m2"><p>در آغوش گل بین و در بر شکوفه</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>جهان آنچنان شد که هرجا که باشد</p></div>
<div class="m2"><p>کند مست پیوسته قی بر شکوفه</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>چو آوازه روی آن سروگل رخ</p></div>
<div class="m2"><p>بگیرد همی هفت کشور شکوفه</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ببستان درآی و ببین بامدادان</p></div>
<div class="m2"><p>بیاد گل روی دلبر شکوفه</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>سهی سرو باغ جمال آن نگاری</p></div>
<div class="m2"><p>که از حسن باغیست یکسر شکوفه</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>زهی روی تو خوشتر از هر شکوفه</p></div>
<div class="m2"><p>نباشد چو تو خوب منظر شکوفه</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ورقهای گل را یکایک بدیدم</p></div>
<div class="m2"><p>ز حسن تو جزویست در هر شکوفه</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بآب رخت گر برآید نبیند</p></div>
<div class="m2"><p>از آتش زیان چون سمندر شکوفه</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بباغ جمالت که فردوس جان شد</p></div>
<div class="m2"><p>صنوبر دهد میوه عرعر شکوفه</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>تو آن آهوی مشک مویی که گردد</p></div>
<div class="m2"><p>چو نافه ببویت معطر شکوفه</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>اگر باد بویت بآتش رساند</p></div>
<div class="m2"><p>کند عود در عین مجمر شکوفه</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>بیاد تو در قعر دوزخ بروید</p></div>
<div class="m2"><p>از آتش بنفشه از اخگر شکوفه</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>اگر پرتوی از رخت بر گل آید</p></div>
<div class="m2"><p>مه و آفتاب آورد بر شکوفه</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>ور از روی خوبت عرق بر وی افتد</p></div>
<div class="m2"><p>برون آید از شاخ شکر شکوفه</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>وگر بوی زلفت ببستان درآید</p></div>
<div class="m2"><p>چو موی تو گردد معنبر شکوفه</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>مدد گر ز رویت نیابد برآید</p></div>
<div class="m2"><p>چو برگ خزانی مزعفر شکوفه</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>صفا گر از آن رخ نگیرد بماند</p></div>
<div class="m2"><p>چو آب بهاری مکدر شکوفه</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>اگر تو بنزدیک این عاشق آیی</p></div>
<div class="m2"><p>از آن روی تا پا نهی بر شکوفه</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>ز خاک سر کوی ما گل بروید</p></div>
<div class="m2"><p>بدان سان که از شاخه تر شکوفه</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>تو چون بگذری از برت گل بریزد</p></div>
<div class="m2"><p>صبا بر زمین گو مگستر شکوفه</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>گر از خاک کوی تو صابون نسازد</p></div>
<div class="m2"><p>نشوید رخ خود منور شکوفه</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>بکافور برفی شود زنگی آسا</p></div>
<div class="m2"><p>سیه روی چون داغ گازر شکوفه</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>بباغ ار درآیی ز بهر نثارت</p></div>
<div class="m2"><p>ایا مر رخت را ثناگر شکوفه</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>کند میوه را همچو باران نیسان</p></div>
<div class="m2"><p>صدف وار در سینه گوهر شکوفه</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>رخ از پرده بنمود وز شرم رویت</p></div>
<div class="m2"><p>ایا گل ترا بنده چاکر شکوفه</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>بیکبار چون مه فرو رفت اگرچه</p></div>
<div class="m2"><p>که یک یک برآمد چو اختر شکوفه</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>نظر کرد و در گلستان پرتوی دید</p></div>
<div class="m2"><p>از آن روی همچون مه و خور شکوفه</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>بوصف گل روی تو داستانی</p></div>
<div class="m2"><p>شنود و نمی داشت باور شکوفه</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>ببادی چنان از هوا اندر آمد</p></div>
<div class="m2"><p>که با خاک ره شد برابر شکوفه</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>خوهد از پی آنکه روی تو بیند</p></div>
<div class="m2"><p>همه چشم خود را چو عبهر شکوفه</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>مه و خور بحسنند همشیره تو</p></div>
<div class="m2"><p>از آن سان که گل را برادر شکوفه</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>درین فصل گل با چو تو لاله رویی</p></div>
<div class="m2"><p>چو زنبورم افتاده اندر شکوفه</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>ز وصف گل روی تو گشت شعرم</p></div>
<div class="m2"><p>چو باغ از بهاران سراسر شکوفه</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>زمستان عشقت درین موسم گل</p></div>
<div class="m2"><p>زمن کس نکردست خوشتر شکوفه</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>بدل گفت حسنت که در باغ مهرم</p></div>
<div class="m2"><p>اگر میوه داری بیاور شکوفه</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>ببست این چنین نخل و گفتا ازین پس</p></div>
<div class="m2"><p>ببازار دوران برآور شکوفه</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>درخت عبارت که شاخش بلندست</p></div>
<div class="m2"><p>یکی میوه دارد معبر شکوفه</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>بر چون تو سروی که از خاک پایت</p></div>
<div class="m2"><p>همی روید از گل نکوتر شکوفه</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>درخت گل آور بود نخلبندی</p></div>
<div class="m2"><p>که از موم سازد مزور شکوفه</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>چو اجزای شعر مرا برفشانی</p></div>
<div class="m2"><p>بریزد ز اوراق دفتر شکوفه</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>ز لب انگبین می دهی عاشقان را</p></div>
<div class="m2"><p>ازین شعر چون نحل می خور شکوفه</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>گر از قامتت برنخوردیم شاید</p></div>
<div class="m2"><p>نیاید ز سرو و صنوبر شکوفه</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>نگارا اگر شاخ وصل تو پنهان</p></div>
<div class="m2"><p>ز من می کند جای دیگر شکوفه</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>بدین شعر بوسی طمع دارم از تو</p></div>
<div class="m2"><p>طلب می کنم میوه را در شکوفه</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>مرا کام خوش کن بآب دهانت</p></div>
<div class="m2"><p>که خوش نبود ای دوست بی بر شکوفه</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>کبوتر بوقتی که دلجوی گردد</p></div>
<div class="m2"><p>کند در دهان کبوتر شکوفه</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>نظر کن زمانی بباغ ضمیرم</p></div>
<div class="m2"><p>که بی حد برآورد و بی مر شکوفه</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>درخت افگن دعوی شاعران شد</p></div>
<div class="m2"><p>زبان من آن تیغ جوهر شکوفه</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>ز بستان خاطر برند این جماعت</p></div>
<div class="m2"><p>برای علف نزد هر خر شکوفه</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>نه خوش بوی گردد بسعی کس ار چه</p></div>
<div class="m2"><p>کند در دهان غضنفر شکوفه</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>تو می نشنوی ورنه در گوش عارف</p></div>
<div class="m2"><p>چو طوطیست دایم سخن ور شکوفه</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>بسی بی زبان از دل پاک هردم</p></div>
<div class="m2"><p>همی گوید الله اکبر شکوفه</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>ز دیوان فطرت خط نور دارد</p></div>
<div class="m2"><p>نوشته بر آن روی انور شکوفه</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>که عالم همه محضر حسن یارست</p></div>
<div class="m2"><p>یکی از گواهان محضر شکوفه</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>برافراز اغصان شهابیست ثاقب</p></div>
<div class="m2"><p>مشعشع بروی منور شکوفه</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>دل پاک را چون صفات مقدس</p></div>
<div class="m2"><p>مذکر ز ذات مطهر شکوفه</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>کتابیست عالم ز افعال و اسما</p></div>
<div class="m2"><p>وزآن جمله جزویست ابتر شکوفه</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>اگر درس معنی بخوانی بدانی</p></div>
<div class="m2"><p>که فعلی دگر راست مصدر شکوفه</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>وگر علم باطن بدانی ببینی</p></div>
<div class="m2"><p>که ظاهر جمالست و مظهر شکوفه</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>چو تو عندلیب گلستان عشقی</p></div>
<div class="m2"><p>ترا گل میسر مسخر شکوفه</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>مربع نشین در چمن چون برآمد</p></div>
<div class="m2"><p>ز شاخ مطول مدور شکوفه</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>مثلث خور از جام عشق و مثنی</p></div>
<div class="m2"><p>سخن می سرابر گل و بر شکوفه</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>بذین شعر دیوان من هست باغی</p></div>
<div class="m2"><p>بهر فصل در وی میسر شکوفه</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>هرآنکس که محرور عشقست اورا</p></div>
<div class="m2"><p>شراب گلست این مکرر شکوفه</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>درخت ضمیرت که بارش زرآمد</p></div>
<div class="m2"><p>کند شاخ او در و گوهر شکوفه</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>ز شعر تو عارف ملالت نبیند</p></div>
<div class="m2"><p>بهشتی کند ز آب کوثر شکوفه</p></div></div>