---
title: >-
    شمارهٔ ۵۵
---
# شمارهٔ ۵۵

<div class="b" id="bn1"><div class="m1"><p>گفتند ماه و خور که چو پیدا شد آن نگار</p></div>
<div class="m2"><p>ما در حنای عزل گرفتیم دست کار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در چشم همتست خیال تو چون عروس</p></div>
<div class="m2"><p>بر دست قدرتست جمال تو چون نگار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر خوانده بود بلبل لحان طبع من</p></div>
<div class="m2"><p>لفظ حدیث وصف تو چندین هزار بار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از نقطهای خال تو اعراب راست کرد</p></div>
<div class="m2"><p>نحوی ناطقه چو خطت دید بر عذار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از پشت عقل بالغ کز باغ شرع هست</p></div>
<div class="m2"><p>چون شاخ به ز عالم تکلیف باردار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا صورتی پذیرد زیبا بوصف تو</p></div>
<div class="m2"><p>معنی بکر جمله شکم گشت چون انار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از عشق چهره تو چو زر زرد گشت گل</p></div>
<div class="m2"><p>وز شرم عارض تو چو گل سرخ شد بهار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر باد خاک کوی تو سوی چمن برد</p></div>
<div class="m2"><p>بر صفحهای گل خط ریحان شود غبار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بوسه که یابد از لب چون انگبین تو؟</p></div>
<div class="m2"><p>کز عصمت است لعل تو چون موم مهردار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در راه گلستان رخت زیر پای دل</p></div>
<div class="m2"><p>سر تیز کرده اند موانع بسان خار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ماریست هجر و مهره مطلوب وصل ازو</p></div>
<div class="m2"><p>از دیده امید نهان گشته گنج وار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بیچاره زهر خورده تریاک جوی را</p></div>
<div class="m2"><p>صعب است مهره کرد برون از دهان مار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ای بخت سر کشیده بنه پای در میان</p></div>
<div class="m2"><p>تا من بباغ وصل ز گل پر کنم کنار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>تا قطره سحاب غمت در دلم چکید</p></div>
<div class="m2"><p>چشمم ز اشک حامل در شد چو گوشوار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>از بس که گرد کوی تو بودیم تا بصبح</p></div>
<div class="m2"><p>از بهر روز وصل تو شبها در انتظار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>سوزی فتاده در دل و بر رخ فشانده اشک</p></div>
<div class="m2"><p>آتش نهاده بر سر و چون شمع پایدار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>زآن آتشی که کرده ای اندر تنور دل</p></div>
<div class="m2"><p>ترسم که با دمم بگلو پر شود شرار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>دعوی عشق کردم و آگه نبود از آن</p></div>
<div class="m2"><p>کین لفظ اندکست و معانیش بی شمار</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>وین اصطلاح باشد از آن سان که عندلیب</p></div>
<div class="m2"><p>گر چه یکی بود لغوی خواندش هزار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ما از کجا و دولت عشق تو از کجا</p></div>
<div class="m2"><p>هرگز مگس که دید که عنقا کند شکار</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>من از خواص عشق چگویم سخن که هست</p></div>
<div class="m2"><p>اسرار او نهفته و آثارش آشکار</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>آنجا که عشق سلطنت خود کند پدید</p></div>
<div class="m2"><p>نی شرع حکم دارد و نی عقل اعتبار</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>عشقت چو پای بسته خود را رسن زند</p></div>
<div class="m2"><p>حلاج شد بعالم بالا ز زیر دار</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>وصل تو خواستم بدعا عزت تو گفت</p></div>
<div class="m2"><p>دولت بجهد نیست چو محنت باختیار</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>هیهات سیف تیغ سخن در نیام کن</p></div>
<div class="m2"><p>یارایت کلام برنگی دگر برآر</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>همچون سر شتر سوی بالا چه می روی</p></div>
<div class="m2"><p>یکدم بسوی زیر کش این ناقه را مهار</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>گر چه ز عشق مرکب تو تازیانه خورد</p></div>
<div class="m2"><p>این راه مشکلست عنان درکش ای سوار</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>قانون شعر و معنی بکر تو نزهه ییست</p></div>
<div class="m2"><p>خوش نغمه چون بریشم باریک همچو تار</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>گویندگان وقت ازین پرده خارجند</p></div>
<div class="m2"><p>آهنگ ارغنون سخن تیز برمدار</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>رو فضل آن کتاب کن این فضل را که نیست</p></div>
<div class="m2"><p>دلسوزتر ز قصه وصل و فراق یار</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>وآنگه بدست لطف بتضمین این دو بیت</p></div>
<div class="m2"><p>در سلک نظم این شبه کش در شاهوار</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>تو گوشمال خورده هجران چو بربطی</p></div>
<div class="m2"><p>مانند چنگ ناله برآور چو زیر زار</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>کای وصل ناگزیر تو برده ز من شکیب</p></div>
<div class="m2"><p>وی هجر پرقرار تو برده ز من قرار</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>گر یک نفس فراق تو اندیشه کردمی</p></div>
<div class="m2"><p>گشتی ز بیم هجر دل و جان من فگار</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>اکنون تو دوری از من و من بی تو زنده ام</p></div>
<div class="m2"><p>سختا که آدمیست در احداث روزگار</p></div></div>