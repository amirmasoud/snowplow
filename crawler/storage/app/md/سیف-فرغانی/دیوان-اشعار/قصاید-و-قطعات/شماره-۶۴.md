---
title: >-
    شمارهٔ ۶۴
---
# شمارهٔ ۶۴

<div class="b" id="bn1"><div class="m1"><p>ای پسر از مردم زمانه حذر گیر</p></div>
<div class="m2"><p>بگذر ازین کوی و خانه جای دگر گیر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در تو نظرهای خلق تیر عدو دان</p></div>
<div class="m2"><p>تیغ بیفگن برای دفع سپر گیر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون تو ندانی طریق غوص درین بحر</p></div>
<div class="m2"><p>حشو صدف ممتلی بدر و گهر گیر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون تو نه آنی که ره بری بمعانی</p></div>
<div class="m2"><p>جمله جهان نیکوان خوب صور گیر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر بجهد آتشی ززند عنایت</p></div>
<div class="m2"><p>سوخته دل بپیش او برو در گیر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یار اگرت از نگین خویش کند مهر</p></div>
<div class="m2"><p>نام ازو همچو شمع و مهر چو زر گیر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پای بنه برفراز چرخ و چو خورشید</p></div>
<div class="m2"><p>جمله آفاق را بزیر نظر گیر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>باز دلت چون بدام عشق در افتاد</p></div>
<div class="m2"><p>خیل ملک را چو مرغ سوخته پر گیر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مرغ سعادت بشام چون بگرفتی</p></div>
<div class="m2"><p>دام تضرع بنه بوقت سحر گیر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>جان شریف تو مغز دانه نفس است</p></div>
<div class="m2"><p>سنگ بزن مغز را ز دانه بدر گیر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چون سر تو زیر دست راهبری نیست</p></div>
<div class="m2"><p>جمله اعضای خویش پای سفر گیر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بگذر ازین پستی از بلندی همت</p></div>
<div class="m2"><p>وین همه بالا و شیب زیر و زبر گیر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>صدق ابوبکر را علم کن و با خود</p></div>
<div class="m2"><p>تیغ علی وار زن جهان چو عمر گیر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>سیف برو جان بباز و نصرت دل کن</p></div>
<div class="m2"><p>دامن معشوق را بدست ظفر گیر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>عیب عملهای خویشتن چو ببینی</p></div>
<div class="m2"><p>بحر دلت را چو علم کان هنر گیر</p></div></div>