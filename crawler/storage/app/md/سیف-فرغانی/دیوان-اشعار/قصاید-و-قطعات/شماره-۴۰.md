---
title: >-
    شمارهٔ ۴۰
---
# شمارهٔ ۴۰

<div class="b" id="bn1"><div class="m1"><p>در عجبم تا خود آن زمان چه زمان بود</p></div>
<div class="m2"><p>کآمدن من بسوی ملک جهان بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بهر عمارت سعود را چه خلل شد</p></div>
<div class="m2"><p>بهر خرابی نحوس را چه قران بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر سرخاکی که پایگاه من و تست</p></div>
<div class="m2"><p>خون عزیزان بسان آب روان بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا کند از آدمی شکم چو لحد پر</p></div>
<div class="m2"><p>پشت زمین همچو گور جمله دهان بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>این تن آواره هیچ جای نمی رفت</p></div>
<div class="m2"><p>بهرامان، کندرو نه خوف بجان بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آب بقا از روان خلق گریزان</p></div>
<div class="m2"><p>باد فنا از مهب قهر وزان بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ظلم بهر خانه لانه کرده چون خطاف</p></div>
<div class="m2"><p>عدل چو عنقا ز چشم خلق نهان بود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>رایت اسلام سرشکسته ازیرا</p></div>
<div class="m2"><p>دولت دین پیرو بخت کفر جوان بود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بر سر قطب صلاح کار نمی گشت</p></div>
<div class="m2"><p>چرخ که گویی مدبرش دبران بود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مردم بی عقل و دین گرفته ولایت</p></div>
<div class="m2"><p>حال بره چون بود چو گرگ شبان بود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بنگر و امروز بین کزآن کیانست</p></div>
<div class="m2"><p>ملک که دی و پریر از آن کیان بود</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>قوت شبانه نیافت هر که کتب خواند</p></div>
<div class="m2"><p>ملک سلاطین بخورد هر که عوان بود</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ملک شیاطین شده بظلم و تعدی</p></div>
<div class="m2"><p>آنچه بمیراث از آن آدمیان بود</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>آنک بسربار تاج خود نکشیدی</p></div>
<div class="m2"><p>گرد جهان همچو پای کفش کشان بود</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گشته زبون چون اسیر هیچ کسان را</p></div>
<div class="m2"><p>هر که باصل و نسب امیر کسان بود</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>نفس نکو ناتوان و در حق مردم</p></div>
<div class="m2"><p>نیک نمی کرد هرکرا که توان بود</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>هرکه صدیقی گزید دوستی او</p></div>
<div class="m2"><p>سود نمی کرد و دشمنیش زیان بود</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>تجربه کردیم تا بدیش یقین شد</p></div>
<div class="m2"><p>هرکه کسی را بنیکوییش گمان بود</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>سر که کند مردمی فتاده ز گردن</p></div>
<div class="m2"><p>نان که خورد آدمی بدست سگان بود</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>دل ز جهان سیر گشته چون وزغ از آب</p></div>
<div class="m2"><p>خون جگر خورده هرکرا غم نان بود</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>همچو مرض عمر رنج خلق ولکن</p></div>
<div class="m2"><p>مرگ ز راحت بخلق مژده رسان بود</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>زر و درم چون مگس ملازم هر خس</p></div>
<div class="m2"><p>در و گهر چون جرس حلی خران بود</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>من بزمانی که در ممالک گیتی</p></div>
<div class="m2"><p>هر که بتر پیشوای اهل زمان بود</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>شرع الاهی و سنت نبوی را</p></div>
<div class="m2"><p>هرکه نکرد اعتبار معتبر آن بود</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>نیک نظر کردم و بهر که ز مردم</p></div>
<div class="m2"><p>چشم وفا داشتم بوعذه زبان بود</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ناخلف و جلف و خلف عادت ایشان</p></div>
<div class="m2"><p>مادر ایام را چنین پسران بود</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>آب سخاشان چو یخ فسرده و هردم</p></div>
<div class="m2"><p>جام طربشان بلهو جرعه فشان بود</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>کرده باقلام بسط ظلم ولیکن</p></div>
<div class="m2"><p>دست همه بهر قبض همچو بنان بود</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>زاستدن نان و آب خلق چو آتش</p></div>
<div class="m2"><p>سرخ بروی و سیاه دل چو دخان بود</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>شعر که نقد روان معدن طبعست</p></div>
<div class="m2"><p>بر دل این ممسکان بنسیه گران بود</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>بوده جهان همچو باغ وقت بهاران</p></div>
<div class="m2"><p>ما چو بباغ آمدیم فصل خزان بود</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>از پی آیندگان ز ماضی (و) حالی</p></div>
<div class="m2"><p>گفتم و تاریخ آن فساد زمان بود</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>هفتصد و سه سال بر گذشته ز هجرت</p></div>
<div class="m2"><p>روز نگفتیم و لیل، مه رمضان بود</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>مسکن من ملک روم مرکز محنت</p></div>
<div class="m2"><p>آقسراشهر و خانه دار هوان بود</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>حمد خداوند گوی سیف و همی کن</p></div>
<div class="m2"><p>شکر که نیک و بد جهان گذران بود</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>سغبه ملکی مشو که پیشتر از تو</p></div>
<div class="m2"><p>همچو زن اندر حباله دگران بود</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>همچو پیمبر نظر نکرد بدنیا</p></div>
<div class="m2"><p>دیده ور(ی) کو بآخرت نگران بود</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>در نظر اهل دل چگونه بود مرد</p></div>
<div class="m2"><p>آنکه بدنیاش میل همچو زنان بود</p></div></div>