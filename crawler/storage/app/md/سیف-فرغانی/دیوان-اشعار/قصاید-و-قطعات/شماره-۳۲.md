---
title: >-
    شمارهٔ ۳۲
---
# شمارهٔ ۳۲

<div class="b" id="bn1"><div class="m1"><p>زنده نبود آن دلی کز عشق جانان باز ماند</p></div>
<div class="m2"><p>مرده دان چون دل ز عشق و جسم از جان باز ماند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جای نفس و طبع شد کز عشق خالی گشت دل</p></div>
<div class="m2"><p>ملک دیوان شد ولایت کز سلیمان باز ماند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جان چو کار عشق نکند بار تن خواهد کشید</p></div>
<div class="m2"><p>گاو آخر شد چو رخش از پور دستان باز ماند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>این عجب نبود که اندردست خصمان اوفتد</p></div>
<div class="m2"><p>ملک سلطانی که از پیکار خصمان باز ماند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عاشقان را نفرتست از لقمه دنیا طلب</p></div>
<div class="m2"><p>خوان سلطان را نشاید چون ز سگ نان باز ماند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آن جوانمردان که از همت نه از سیری کنند</p></div>
<div class="m2"><p>پشت برنانی کزین اشکم پرستان باز ماند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اسب دل چون در قفای گوی همت راندند</p></div>
<div class="m2"><p>چرخ چوگانی از ایشان چند میدان باز ماند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آن زمان کز خویشتن رفتند و در سیر آمدند</p></div>
<div class="m2"><p>جبرئیل تیز پر در راه از ایشان باز ماند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>عشق باقی کی گذارد با تو از تو ذره یی</p></div>
<div class="m2"><p>گر تویی تو برفت و پاره یی زآن باز ماند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آن نمی بینی که از گرمای تابستان گداخت</p></div>
<div class="m2"><p>همچو یخ در آب برفی کز زمستان باز ماند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ای پسر برخیز و با این قوم بنشین زینهار</p></div>
<div class="m2"><p>کین جهان پر دشمنست از دوست نتوان باز ماند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گر ز دنیا باز مانی ملک عقبی آن تست</p></div>
<div class="m2"><p>شد عزیز مصر یوسف چون ز کنعان باز ماند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>من نپندارم که تأثیری کند در حال تو</p></div>
<div class="m2"><p>خرقه یی با تو گر از آثار مردان باز ماند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>دیگران ثعبان سحرآشام نتوانند کرد</p></div>
<div class="m2"><p>آن عصایی را که از موسی عمران باز ماند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>سیف فرغانی ز مردم منقطع شو بهر دوست</p></div>
<div class="m2"><p>قدر یوسف آنگه افزون شد که زاخوان باز ماند</p></div></div>