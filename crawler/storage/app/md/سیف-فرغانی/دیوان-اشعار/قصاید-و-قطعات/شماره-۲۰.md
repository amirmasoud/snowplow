---
title: >-
    شمارهٔ ۲۰
---
# شمارهٔ ۲۰

<div class="b" id="bn1"><div class="m1"><p>ای مرد فقر، هست ترا خرقه تو تاج</p></div>
<div class="m2"><p>سلطان تویی که نیست بسلطانت احتیاج</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو داد بندگی خداوند خود بده</p></div>
<div class="m2"><p>وآنگاه از ملوک جهان می ستان خراج</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر طاعتی کنی مکنش فاش نزد خلق</p></div>
<div class="m2"><p>چون بیضه یی نهی مکن آواز چون دجاج</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>محبوب حق شدن بنماز و بروزه نیست</p></div>
<div class="m2"><p>این آرزوت اگرچه کند در دل اختلاج</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون هرچه غیر اوست بدل ترک آن کنی</p></div>
<div class="m2"><p>بر فرق جان تو نهد از حب خویش تاج</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در نصرت خرد که هوا دشمن ویست</p></div>
<div class="m2"><p>با نفس خود جدل کن و با طبع خود لجاج</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر در مصاف آن دو مخالف شوی شهید</p></div>
<div class="m2"><p>بیمار را بدم چو مسیحا کنی علاج</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون نفس تند گشت بسختیش رام کن</p></div>
<div class="m2"><p>سردی دهد طبیب چو گر می کند مزاج</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>با او موافقت مکن اندر خلاف عقل</p></div>
<div class="m2"><p>محتاج نیست شب که سیاهش کنی بزاج</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مردانه گنده پیر جهان را طلاق ده</p></div>
<div class="m2"><p>کز عشق بست با دل تو عقد ازدواج</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هستی تو چوزیت بسوزد گرت فتد</p></div>
<div class="m2"><p>بر دل شعاع عشق چو مصباح در زجاج</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>زاندوه او چو مشعله ماه روشن است</p></div>
<div class="m2"><p>شمع دلت، که زنده بروغن بود سراج</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>مر فقر را امین نبود هیچ جاه جوی</p></div>
<div class="m2"><p>چون تخت شه نشین نشود هیچ پیل عاج</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گوید گلیم پوش گدارا کسی امیر؟</p></div>
<div class="m2"><p>خواند هوید پوش شتر را کسی دواج</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گر در رهش زنی قدمی، بر جبین گل</p></div>
<div class="m2"><p>از خاک ره چو قطره شبنم فتد عجاج</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>خود کام را چنین سخن از طبع هست دور</p></div>
<div class="m2"><p>محموم را بود عسل اندر دهان اجاج</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>گر دوستی حق طلبی ترک خلق کن</p></div>
<div class="m2"><p>در یک مکان دو ضد نکند باهم امتزاج</p></div></div>