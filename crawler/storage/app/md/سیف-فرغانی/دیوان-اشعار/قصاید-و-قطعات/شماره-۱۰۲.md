---
title: >-
    شمارهٔ ۱۰۲
---
# شمارهٔ ۱۰۲

<div class="b" id="bn1"><div class="m1"><p>ایا سلطان لشکرکش بشاهی چون علم سرکش</p></div>
<div class="m2"><p>که هرگز دوست با دشمن ندیده کارزار تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ملک شمشیرزن باید، چو تو تن می زنی ناید</p></div>
<div class="m2"><p>ز تیغی بر میان بستن مرادی در کنار تو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نه دشمن را بریده سر چو خوشه تیغ چون داست</p></div>
<div class="m2"><p>نه خصمی را چو خرمن کوفت گرز گاوسار تو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عیالان رعیت را بحسبت کدخدایی کن</p></div>
<div class="m2"><p>چو کدبانوی دنیا شد برغبت خواستار تو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مروت کن، یتیمی را بچشم مردمی بنگر</p></div>
<div class="m2"><p>که مروارید اشک اوست در گوشوار تو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خری شد پیشکار تو که دروی نیست یک جو دین</p></div>
<div class="m2"><p>دل خلقی ازو تنگست اندر روز بار تو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو آتش برفروزی تو بمردم سوختن هردم</p></div>
<div class="m2"><p>ازان کان خس نهد خاشاک دایم بر شرارتو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو تو بی رای و بی تدبیر او را پیروی کردی</p></div>
<div class="m2"><p>تو در دوزخ شوی پیشین و از پس پیشکار تو</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بباطل چون تو مشغولی ز حق و خلق بی خشیت</p></div>
<div class="m2"><p>نه خوفی در درون تو نه امنی در دیار تو</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نه ترسی نفس ظالم را ز بیم گوشمال تو</p></div>
<div class="m2"><p>نه بیمی اهل باطل را ز عدل حق گزار تو</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بشادی می کنی جولان درین میدان، نمی دانم</p></div>
<div class="m2"><p>در آن زندان غم خواران که باشد غمگسار تو</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بپای کژروت روزی درآیی ناگهان در سر</p></div>
<div class="m2"><p>وگر سم بر فلک ساید سمند راهوار تو</p></div></div>