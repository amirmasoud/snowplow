---
title: >-
    شمارهٔ ۶۲ - فی التوحید الباری تعالی
---
# شمارهٔ ۶۲ - فی التوحید الباری تعالی

<div class="b" id="bn1"><div class="m1"><p>دانستم از صفات که ذاتت منزهست</p></div>
<div class="m2"><p>از شرکت مشابه و از شبهت نظیر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در دست من که قاصرم از شکر نعمتت</p></div>
<div class="m2"><p>ذکر تو می کند بزبان قلم صریر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هرچند غافلم ز تو لکن ز ذکر تو</p></div>
<div class="m2"><p>در و کر سینه مرغ دلم می زند صفیر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اندر هوای وصف تو پرواز خواست کرد</p></div>
<div class="m2"><p>از پر خویش طایر اندیشه خورد تیر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>منظومه ثنای تو تألیف می کنم</p></div>
<div class="m2"><p>باشد که نافع آیدم این نظم دل پذیر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تو هادیی، بفضلت تنبیه کن مرا</p></div>
<div class="m2"><p>تا از هدایه تو شوم جامع کبیر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کس را سزای ذات تو مدحی نداد دست</p></div>
<div class="m2"><p>گر بنده حق آن نگزارد بر او مگیر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر کس حق ثنای تو هرگز گزاردی</p></div>
<div class="m2"><p>لا احصی از چه گفتی پیغمبر بشیر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در آرزوی فقر بسی بود جان من</p></div>
<div class="m2"><p>عشق از رواق غیب ندا کرد کای فقیر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>رو ترک سر بگیر و ازین حبیب سر برآر</p></div>
<div class="m2"><p>رو ترک زر بگو وا زین سکه نام گیر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گر زندگی خواهی چو شهیدان پس از حیات</p></div>
<div class="m2"><p>بر بستر مجاهده پیش از اجل بمیر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ای جان بنفس مرده شو و از فنا مترس</p></div>
<div class="m2"><p>وی دل بعشق زنده شو و تا ابد ممیر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>روزی که حکمت از پی تحقیق وعدها</p></div>
<div class="m2"><p>تغییر کاینات بفرماید ای قدیر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گهواره زمین چو بجنبد بامر تو</p></div>
<div class="m2"><p>گردد در آن زمان ز فزع شیرخواره پیر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>با اهل رحمت تو برانگیز بنده را</p></div>
<div class="m2"><p>کان قوم خورده اند ز پستان فضل شیر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>من جمع کرده هیزم افعال بد بسی</p></div>
<div class="m2"><p>وآنگه گذر بر آتش قهر تو ناگزیر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>از بهر صید ماهی عفو تو در دعا</p></div>
<div class="m2"><p>از دست دام دارم و از چشم آبگیر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>نومید نیستم ز در رحمتت که هست</p></div>
<div class="m2"><p>کشت امید تشنه و ابر کرم مطیر</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>تو عالمی که حاصل ایام عمر من</p></div>
<div class="m2"><p>جرمی است، رحمتم کن و؛ عذریست، درپذیر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>فردا که هیچ حکم نباشد بدست کس</p></div>
<div class="m2"><p>ای دستگیر جمله مرا نیز دست گیر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ای پادشاه عالم، ای عالم خبیر</p></div>
<div class="m2"><p>یک وصف تست قدرت و یک اسم تو قدیر</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>فضل تو بر تواتر و فیض تو بر دوام</p></div>
<div class="m2"><p>حکم تو بی منازع و ملک تو بی وزیر</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بر چهره کواکب از صنع تست نور</p></div>
<div class="m2"><p>بر گردن طبایع از حکم تست نیر</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چون آفتاب بر دل هر ذره روشن است</p></div>
<div class="m2"><p>کز زیت فیض تست چراغ قمر منیر</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>از آفتاب قدرت تو سایه پرتویست</p></div>
<div class="m2"><p>کورست آنکه می نگرد ذره را حقیر</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>از طشت آبگون فلک بر مثال برق</p></div>
<div class="m2"><p>در روز ابر شعله زند آتش اثیر</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>با امر نافذ تو چو سلطان آفتاب</p></div>
<div class="m2"><p>نبود عجب که ذره ز گردون کند سریر</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>برخوان نان جود تو عالم بود طفیل</p></div>
<div class="m2"><p>بهر تنور صنع تو آدم بود خمیر</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>در پیش صولجان قضای تو همچو گوی</p></div>
<div class="m2"><p>میدان بسر همی سپرد چرخ مستدیر</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>علم ترا خبر که ز بهر چه منزویست</p></div>
<div class="m2"><p>خلوت نشین فکر بیغوله ضمیر</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>اجزای کاینات همه ذاکر تواند</p></div>
<div class="m2"><p>این گویدت که مولی، وآن گویدت نصیر</p></div></div>