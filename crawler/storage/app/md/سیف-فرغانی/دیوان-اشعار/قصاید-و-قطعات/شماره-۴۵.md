---
title: >-
    شمارهٔ ۴۵
---
# شمارهٔ ۴۵

<div class="b" id="bn1"><div class="m1"><p>نگارا کار عشق از من نیاید</p></div>
<div class="m2"><p>ز بلبل جز سخن گفتن نیاید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خرد اسرار عشقت فهم نکند</p></div>
<div class="m2"><p>ز نابینا گهر سفتن نیاید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ننالد بهر تو جز زنده جانی</p></div>
<div class="m2"><p>ز مرده دل چنین شیون نیاید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نباشد عشق کار مرد دنیا</p></div>
<div class="m2"><p>ملک در حکم آهرمن نیاید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>که از عقد ثریا دانه خوردن</p></div>
<div class="m2"><p>ز گنجشکان این خرمن نیاید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دل عاشق بکس پیوند نکند</p></div>
<div class="m2"><p>ز مردم مریم آبستن نیاید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>که اینجا زآستان خویش عنقا</p></div>
<div class="m2"><p>ز بهر خوردن ارزن نیاید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ایا مسکین مکن دعوی این کار</p></div>
<div class="m2"><p>که هرگز کار مرد از زن نیاید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز سوز عشق بگدازد تن مرد</p></div>
<div class="m2"><p>از آتش هیمه پروردن نیاید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>محبت کار چون تو کوردل نیست</p></div>
<div class="m2"><p>سرشک از چشم پرویزن نیاید</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چو دستار ریاست بر سر تست</p></div>
<div class="m2"><p>ترا این طوق در گردن نیاید</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دل ناپاک و نور عشق؟ هیهات</p></div>
<div class="m2"><p>سریر شاه در گلخن نیاید</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چمن آرامگاه عندلیب است</p></div>
<div class="m2"><p>کلاغ بیشه در گلشن نیاید</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>تو شمعی از طلب در خانه برکن</p></div>
<div class="m2"><p>گرت مهتاب در روزن نیاید</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>خلاف نفس کردن کار تو نیست</p></div>
<div class="m2"><p>که از خر بنده خر کشتن نیاید</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>برو از دست محنت کوب می خور</p></div>
<div class="m2"><p>که این دولت بنان خوردن نیاید</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>کجا در چشم مردم باشدش جای</p></div>
<div class="m2"><p>چو سنگ سرمه در هاون نیاید</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گر آیینه شوی بی صیقل عشق</p></div>
<div class="m2"><p>دل تاریک تو روشن نیاید</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>نگردد چشم روشن تا بیعقوب</p></div>
<div class="m2"><p>ز یوسف بوی پیراهن نیاید</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>دل سخت تو چون مردار سنگست</p></div>
<div class="m2"><p>چنان جوهر ازین معدن نیاید</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ترا باید چو من معلوم باشد</p></div>
<div class="m2"><p>که این کار از تو و از من نیاید</p></div></div>