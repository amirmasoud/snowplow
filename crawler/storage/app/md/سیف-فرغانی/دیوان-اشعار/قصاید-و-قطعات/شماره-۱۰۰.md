---
title: >-
    شمارهٔ ۱۰۰
---
# شمارهٔ ۱۰۰

<div class="b" id="bn1"><div class="m1"><p>ای ترا در کار دنیا بوده دست افزار دین</p></div>
<div class="m2"><p>وی تو از دین گشته بیزار و ز تو بیزار دین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای بدستار و بجبه گشته اندر دین امام</p></div>
<div class="m2"><p>ترک دنیا کن که نبود جبه و دستار دین</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای لقب گشته فلان الدین والدنیا ترا</p></div>
<div class="m2"><p>ننگ دنیایی و از نام تو دارد عار دین</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نفس مکارت کجا بازار زرقی تیز کرد</p></div>
<div class="m2"><p>کز پی دنیا درو نفروختی صد بار دین</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>قدر دنیا را تو می دانی که گر دستت دهد</p></div>
<div class="m2"><p>یک درم از وی بدست آری بصد دینار دین</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>قیمت او هم تو بشناسی که گر یابی کنی</p></div>
<div class="m2"><p>یک جو او را خریداری بده خروار دین</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خویشتن باز آر ازین دنیا خریدن زینهار</p></div>
<div class="m2"><p>چون خریداران زر مفروش در بازار دین</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کز برای سود دنیا ای زیان تو ز تو</p></div>
<div class="m2"><p>بهر مال ارزان فروشد مرد دنیادار دین</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از پی مالی که امسالت مگر حاصل شود</p></div>
<div class="m2"><p>در پی این سروران از دست دادی پار دین</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مصر دنیا را که دروی سیم و زر باشد عزیز</p></div>
<div class="m2"><p>تو زلیخایی از آن نزد تو باشد خوار دین</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دیو نفست گر مسخر شد مسلم باشدت</p></div>
<div class="m2"><p>این که در دنیا نگه داری سلیمان وار دین</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>حق دین ضایع کنی هر روز بهر حظ نفس</p></div>
<div class="m2"><p>آه از آن روزی که گوید حق من بگزار دین</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>کار تو چون جاهلان شد برگ دنیا ساختن</p></div>
<div class="m2"><p>خود درخت علم تو روزی نیارد بار دین</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بحث و تکرار از برای دین بود در مدرسه</p></div>
<div class="m2"><p>وز تو آنجا فوت شد ای عالم مختار دین</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>آرزوی مسند تدریس بیرون کن ز دل</p></div>
<div class="m2"><p>تا ترا حاصل شود بی بحث و تکرار دین</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چشم جان از دیدن رخسار این رعنا ببند</p></div>
<div class="m2"><p>تاگشاید بر دلت گنجینه اسرار دین</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>دست حکم طبع بیرون ناورد از دایره</p></div>
<div class="m2"><p>نقطه دل را که زد بر گرد او پرگار دین</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>کار من گویی همه دینست و من بیدار دل</p></div>
<div class="m2"><p>خواب غفلت کی گمارد بر دل بیدار دین</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>نزد تو کز مال دنیا خانه رنگین کرده ای</p></div>
<div class="m2"><p>پرده بیرون در نقشیست بر دیوار دین</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بیم درویشی اعمالست اندر آخرت</p></div>
<div class="m2"><p>آن توانگر را که در دنیا نباشد یار دین</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>در دل دنیاپرست تو قضا چون بنگریست</p></div>
<div class="m2"><p>گفت ناپاکست یارب اندرو مگذار دین</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>با چو تو کم عقل از دین گفت نتوان زآنکه هست</p></div>
<div class="m2"><p>اندکی دنیا بر تو بهتر از بسیار دین</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>دین چو مقداری ندارد بهر دنیا نزد تو</p></div>
<div class="m2"><p>آخرت نیکو بدست آری بدین مقدار دین!</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>کار برعکس است اگر دین می خوهی دنیا مجوی</p></div>
<div class="m2"><p>همچنین ای خواجه گر دنیا خوهی بگذار دین</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>در چراگاه جهان خوش خوش همی کن گاولیس</p></div>
<div class="m2"><p>چون خر نفس ترا بر سر نکرد افسار دین</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>اندرین دوری که نزد سروران اهل کفر</p></div>
<div class="m2"><p>زین مسلمان مرتد می کند زنهار دین</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>سیف فرغانی برو آثار دین داران بجان</p></div>
<div class="m2"><p>در کتب می جو، قوی می کن بدان آثار دین</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>خلق در دنیای باطل راه حق گم کرده اند</p></div>
<div class="m2"><p>چون نمی جویند در قرآن و در اخبار دین</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>مجلس علمی طلب کز پرده های نقل او</p></div>
<div class="m2"><p>دم بدم اندر نوا آید چو موسیقار دین</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>گرچه گفتار نکو از دین برون نبود ولیک</p></div>
<div class="m2"><p>نزد حق کردار نیکست ای نکوگفتار دین</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>ورچه شعر از علم دین بیرون بود، چون عارفان</p></div>
<div class="m2"><p>تا توانی درج کن در ضمن این اشعار دین</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>ای خروس تاجور چون ماکیان بر تخم خویش</p></div>
<div class="m2"><p>خامش اندر گوشه یی بنشین، نگه می دار دین</p></div></div>