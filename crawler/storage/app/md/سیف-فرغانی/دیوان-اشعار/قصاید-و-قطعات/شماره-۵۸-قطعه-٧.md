---
title: >-
    شمارهٔ ۵۸ - قطعه ٧
---
# شمارهٔ ۵۸ - قطعه ٧

<div class="b" id="bn1"><div class="m1"><p>روی در زیر سمش کرد بساطی، چو فگند</p></div>
<div class="m2"><p>بار ابریشم خود بر خر چوبین طنبور</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر بکام تو رسد تلخ، مکن روی ترش</p></div>
<div class="m2"><p>کآب شیرین نخورد تشنه ازین مشرب شور</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جهد کن تا بسلامت بکناری برسی</p></div>
<div class="m2"><p>این جهان بحر عمیقست و کنارش لب گور</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ترک دنیا بارادت نکند جاهل ازآنک</p></div>
<div class="m2"><p>بکراهت بدر آید ز علف زار ستور</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سیف فرغانی این قطعه برای خود گفت</p></div>
<div class="m2"><p>کای شده از پی جامه ز لباس دین عور</p></div></div>