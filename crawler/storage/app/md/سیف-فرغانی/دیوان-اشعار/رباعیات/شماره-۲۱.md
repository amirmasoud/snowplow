---
title: >-
    شمارهٔ ۲۱
---
# شمارهٔ ۲۱

<div class="b" id="bn1"><div class="m1"><p>آنی که منورست آفاق از تو</p></div>
<div class="m2"><p>محروم بماندم من مشتاق از تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این محنت نو نگر که در خلوت وصل</p></div>
<div class="m2"><p>تو با دگری جفتی و من طاق از تو</p></div></div>