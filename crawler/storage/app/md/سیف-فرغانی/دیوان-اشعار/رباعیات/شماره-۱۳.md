---
title: >-
    شمارهٔ ۱۳
---
# شمارهٔ ۱۳

<div class="b" id="bn1"><div class="m1"><p>ای من همه بد کرده و دیده ز تو نیک</p></div>
<div class="m2"><p>بد گفته همه عمر و شنیده ز تو نیک</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حد بدی و غایت نیکی اینست</p></div>
<div class="m2"><p>کز من بتوبد بمن رسیده ز تو نیک</p></div></div>