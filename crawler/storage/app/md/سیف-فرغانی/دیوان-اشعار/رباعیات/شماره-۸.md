---
title: >-
    شمارهٔ ۸
---
# شمارهٔ ۸

<div class="b" id="bn1"><div class="m1"><p>کردم همه عمر آنچه نمی باید کرد</p></div>
<div class="m2"><p>از کرده او حذر نمی شاید کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>امروز چنینم و ندانم فردا</p></div>
<div class="m2"><p>تا با من بیچاره چه فرماید کرد</p></div></div>