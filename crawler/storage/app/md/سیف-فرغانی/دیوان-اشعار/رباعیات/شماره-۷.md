---
title: >-
    شمارهٔ ۷
---
# شمارهٔ ۷

<div class="b" id="bn1"><div class="m1"><p>جعفر که ز رخ ماه تمامی دارد</p></div>
<div class="m2"><p>در شهر بلطف و حسن نامی دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با لشکر حسن در میان خوبان</p></div>
<div class="m2"><p>زآنست مظفر که حسامی دارد</p></div></div>