---
title: >-
    بخش ۲ - فی شکایة اهل الزمان
---
# بخش ۲ - فی شکایة اهل الزمان

<div class="b" id="bn1"><div class="m1"><p>اندرین عصر بوالفضولی چند</p></div>
<div class="m2"><p>کرده از بر دو فصلک از ترفند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هیچ نادیده از علوم اثر</p></div>
<div class="m2"><p>هیچ نایافته ز حال خبر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همچو خر مانده عاجز معلف</p></div>
<div class="m2"><p>کرده عمر عزیز خویش تلف</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همه در بند لقمه‌ای و جماع</p></div>
<div class="m2"><p>همه را خون حلال بر اجماع</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همه چون گاو و خر کشندهٔ بار</p></div>
<div class="m2"><p>همه اشتر صفت اسیر مهار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بی‌خبر جمله از حقیقت کار</p></div>
<div class="m2"><p>همه از علم دین شده ناهار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به گه لقمه چون سبع تازان</p></div>
<div class="m2"><p>به گه شهوه همچو خر یازان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در غضب همچو شیر درّنده</p></div>
<div class="m2"><p>در طلب همچو مرغ پرّنده</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شهوت آن را که گشت مستولی</p></div>
<div class="m2"><p>هردو یکسان امام و مستملی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>حسد و حقد و خشم و شهوت و آز</p></div>
<div class="m2"><p>گردشان اندر آمده چو پیاز</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نز خدا ترس و نه ز مردم شرم</p></div>
<div class="m2"><p>یکسو انداخته ره آزرم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>همه در جستجوی دانگانه</p></div>
<div class="m2"><p>از شریعت به جمله بیگانه</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>شرع را جمله پشت پای زده</p></div>
<div class="m2"><p>هریک از رای خویش رای زده</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>کرده منسوخ شرع را احکام</p></div>
<div class="m2"><p>همه پیش مراد خویش غلام</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ای رسول خدای بی‌همتای</p></div>
<div class="m2"><p>از پی امتت ز بهر خدای</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>در مدینه ز روضه سر بردار</p></div>
<div class="m2"><p>تا ببینی که کیست بر سرِ دار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>دین فروشان گرفته منبر تو</p></div>
<div class="m2"><p>زار گشته شبیر و شبّر تو</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>باد بدرود شرع و سنّت تو</p></div>
<div class="m2"><p>وان پسندیده راه امّت تو</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>باد بدرود دین و شرع رسول</p></div>
<div class="m2"><p>گشت پیدا به جای فضل فضول</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>باد بدرود صدق بوبکری</p></div>
<div class="m2"><p>فارغ از عیب و ریب و پر مکری</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>باد بدرود هیبت عمری</p></div>
<div class="m2"><p>منهزم گشته جمع دیو و پری</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>باد بدرود سیرت عثمان</p></div>
<div class="m2"><p>آنکه بود او مرتّب قرآن</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>باد بدرود زخم تیغ علی</p></div>
<div class="m2"><p>آنکه او را خدای خواند ولی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>وان گزیده جماعت اصحاب</p></div>
<div class="m2"><p>همه در راه دین اولوالالباب</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>وان ستوده مهاجر و انصار</p></div>
<div class="m2"><p>همه در راه شرع نیکوکار</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>و اهل صفّه موافقان رسول</p></div>
<div class="m2"><p>همه فارغ ز عیب و ریب و فضول</p></div></div>