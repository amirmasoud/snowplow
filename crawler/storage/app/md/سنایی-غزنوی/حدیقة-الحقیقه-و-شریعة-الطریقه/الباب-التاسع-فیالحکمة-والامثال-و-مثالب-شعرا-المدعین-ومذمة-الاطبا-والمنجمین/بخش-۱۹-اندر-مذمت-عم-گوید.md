---
title: >-
    بخش ۱۹ - اندر مذمّت عمّ گوید
---
# بخش ۱۹ - اندر مذمّت عمّ گوید

<div class="b" id="bn1"><div class="m1"><p>آنکه عمّ تو و آنکه خال تواند</p></div>
<div class="m2"><p>همه در قصد خون و مال تواند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عمّ که بدگوی و پر ستم باشد</p></div>
<div class="m2"><p>عم نباشد که درد و غم باشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در مهی خویشتن پدر کرده</p></div>
<div class="m2"><p>به گه پرورش به در کرده</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در کن و در مکن مه خانه</p></div>
<div class="m2"><p>در بیار و بده چو بیگانه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون عقاب و چو باز وقت گرفت</p></div>
<div class="m2"><p>همچو گنجشک وعکه خوار گرفت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همچو کیر جوان به وقت بگیر</p></div>
<div class="m2"><p>باز وقت بیار خایهٔ پیر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دیدی ار دست و پای بلعم را</p></div>
<div class="m2"><p>دردسر آن عمامهٔ عم را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گرت بخشد عمامه عم مستان</p></div>
<div class="m2"><p>کان بود چون عطای بدمستان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کان عمامه نه بهر آن دادست</p></div>
<div class="m2"><p>کز وجود تو خوشدل و شادست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تا ندیده است پای را هنجار</p></div>
<div class="m2"><p>ندهد دست عم ترا دستار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>انده خال و غمّ عم بگذار</p></div>
<div class="m2"><p>تا بوی شاد خوار و برخوردار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ورنه جان کن که دل ستم نکشد</p></div>
<div class="m2"><p>عاقل اندوه خال و عم نکشد</p></div></div>