---
title: >-
    بخش ۳ - فی‌المعذرة والتقصیر
---
# بخش ۳ - فی‌المعذرة والتقصیر

<div class="b" id="bn1"><div class="m1"><p>تا به دل بر گنه دلیر شدم</p></div>
<div class="m2"><p>زین حیات ذمیم سیر شدم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زین حیات ذمیم بی‌مقصود</p></div>
<div class="m2"><p>بهتر آید مرا عدم ز وجود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من ز بار گنه چو کوه شدم</p></div>
<div class="m2"><p>وز تن و جان خود ستوده شدم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرگ بهتر ز زندگانی بد</p></div>
<div class="m2"><p>نیست کاره ز مرگ خود بخرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سال و مه بر گناهها مُصرم</p></div>
<div class="m2"><p>روز و شب بر گناه خود مُقرم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای خداوند فرد بی‌همتای</p></div>
<div class="m2"><p>حرمت این رسول راه‌نمای</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>که مرا زین گروه برهانی</p></div>
<div class="m2"><p>تا گذارم جهان به آسانی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گرچه دارم گناه بسیاری</p></div>
<div class="m2"><p>نیستم در زمانه بازاری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دو سبب را امید می‌دارم</p></div>
<div class="m2"><p>گرچه آلوده و گنه کارم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>که نجاتم دهی بدین دو سبب</p></div>
<div class="m2"><p>زین چنین جمع بی‌خبر یاری</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آن یکی حبّ خاندان رسول</p></div>
<div class="m2"><p>حبّ آن شیرمرد جفت بتول</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>وآن دگر بغض آل بوسفیان</p></div>
<div class="m2"><p>که از ایشان بدو رسید زیان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>مر مرا زین سبب نجات دهی</p></div>
<div class="m2"><p>وز جهنّم مرا برات دهی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>مایهٔ من به روز حشر این است</p></div>
<div class="m2"><p>ظن چنان آیدم که این دین است</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>شکر ایزد که بنده چون دگران</p></div>
<div class="m2"><p>نیست اندر شمار بی‌خبران</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ای سنا داده مر سنایی را</p></div>
<div class="m2"><p>تا بدیدم ره رهایی را</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>که تو بر ظالمان نبخشایی</p></div>
<div class="m2"><p>ظالمان را جزا بفرمایی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>خاصه بر ظالمان آل رسول</p></div>
<div class="m2"><p>آنکه دارند جای فضل فضول</p></div></div>
<div class="b2" id="bn19"><p>* * *</p></div>
<div class="b" id="bn20"><div class="m1"><p>ختم این بیتها درود رسید</p></div>
<div class="m2"><p>اجل اندر قفا و عقل بدید</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>دید چشمم به خواب در یک شب</p></div>
<div class="m2"><p>که ز گفتارها ببستم لب</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>عقل دانست وقت رفتن جان</p></div>
<div class="m2"><p>آمد و رفت خواهد او ز میان</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>آمدم پیش با خطر سفری</p></div>
<div class="m2"><p>بو که یابم بر این خطر گذری</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چون نصیبم ز دهر این آمد</p></div>
<div class="m2"><p>این مرا بیت واپسین آمد</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ناقص آمد کتاب از آنکه اجل</p></div>
<div class="m2"><p>جان ربود و سپر تن به وجل</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>گرچه این بیتها تمام نشد</p></div>
<div class="m2"><p>تیغ گفتار در نیام نشد</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>آنچه گفتم نظام او به کمال</p></div>
<div class="m2"><p>هست چون شمس و ماه و آب زلال</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>اگر اندر جهان مقام بُدی</p></div>
<div class="m2"><p>گفت من تا ابد تمام بُدی</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>چون برفتم به عذر معذورم</p></div>
<div class="m2"><p>پیش استاد دین چو مزدورم</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>یارب این عذر گفته‌ها بپذیر</p></div>
<div class="m2"><p>به خطاها و کردهام مگیر</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>تو که خواننده‌ای دعاگو باش</p></div>
<div class="m2"><p>دین نگهدار و جای جان جو باش</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>چون ترا جای جان برون از جاست</p></div>
<div class="m2"><p>پاک جان‌دار اگر نه بیم عناست</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>که چو خاکی تنت به خاک شود</p></div>
<div class="m2"><p>پاک باید که جای پاک شود</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>من ز کردار و گفت ترسانم</p></div>
<div class="m2"><p>بو که گیرند هردو آسانم</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>لیکن ای دوست رفتنم زین‌سان</p></div>
<div class="m2"><p>گر بخواهی تو هم کنون آسان</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>کرد خود گِرد خود درآوردم</p></div>
<div class="m2"><p>آنچه کردم ز دهر آن بُردم</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>تو چنان دان که همچنین باشی</p></div>
<div class="m2"><p>جهد کن تا مرید دین باشی</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>چون ترا دین بود مرید لحد</p></div>
<div class="m2"><p>یافتی خلعت ثنای احد</p></div></div>