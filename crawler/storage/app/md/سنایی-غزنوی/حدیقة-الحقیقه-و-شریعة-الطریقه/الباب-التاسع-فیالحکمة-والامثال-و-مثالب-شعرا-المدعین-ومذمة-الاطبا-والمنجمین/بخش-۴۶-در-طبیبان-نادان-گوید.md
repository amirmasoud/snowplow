---
title: >-
    بخش ۴۶ - در طبیبان نادان گوید
---
# بخش ۴۶ - در طبیبان نادان گوید

<div class="b" id="bn1"><div class="m1"><p>این نمودیم حدّ این پنجاه</p></div>
<div class="m2"><p>کرد باید کنون سخن کوتاه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حکما جمله حدّ این امراض</p></div>
<div class="m2"><p>این نهادند بر سواد و بیاض</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از اطباء عام این ایّام</p></div>
<div class="m2"><p>گر بپرسی از این همه یک نام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به خدا ار شناسد و داند</p></div>
<div class="m2"><p>ور هزارن کتاب برخواند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همه از جهل پر شر و شورند</p></div>
<div class="m2"><p>همه کناس و اکمه و کورند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>صدهزاران مریض را هر سال</p></div>
<div class="m2"><p>بکشند از تباهی افعال</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>همه هستند یار عزرائیل</p></div>
<div class="m2"><p>قاتل ایشان و خلق جمله قتیل</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>وای آنکس که هست حاجتمند</p></div>
<div class="m2"><p>به چنین قوم کور بی‌در و بند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ای خداوند از این چنین حکما</p></div>
<div class="m2"><p>خلق را کن به فضل خویش رها</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>که جهان شد ز فعلشان ویران</p></div>
<div class="m2"><p>خلق را زین بدان به جان برهان</p></div></div>