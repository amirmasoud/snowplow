---
title: >-
    بخش ۵۵ - صفة مقادیر ابروج والکواکب السیّارة
---
# بخش ۵۵ - صفة مقادیر ابروج والکواکب السیّارة

<div class="b" id="bn1"><div class="m1"><p>غافلند این منجّمان از کار</p></div>
<div class="m2"><p>نیست در کارشان دل بیدار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همه را زرق و حیلت است آلت</p></div>
<div class="m2"><p>نیست از علم و حلمشان عدّت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شمس کز کرّه هست در مقدار</p></div>
<div class="m2"><p>ز صد و بیست و چار بار شمار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خانه او را اسد نهادستند</p></div>
<div class="m2"><p>دور دور از خرد فتادستند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زُهره کز ربع کرّه بیگانه‌ست</p></div>
<div class="m2"><p>ثور و میزان ورا چرا خانه‌ست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نیست تیر از کره یکی اجزا</p></div>
<div class="m2"><p>با دو خانه است سنبله و جوزا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نیست در کارشان بسی تمییز</p></div>
<div class="m2"><p>خیز و بر ریش آن منجم تیز</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>می نویسند خیره بر تقویم</p></div>
<div class="m2"><p>نیک و بد بر عموم اینت حکیم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بس تبجّح کنند بر دانش</p></div>
<div class="m2"><p>هیچ دانش نداده یزدانش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نیست فرقی میان مردم دهر</p></div>
<div class="m2"><p>همه یکسان بود طوالع شهر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>همه بادست حکم باد انگار</p></div>
<div class="m2"><p>تو ز احکام خیره دست بدار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نیست جز هرزه مندل و تنجیم</p></div>
<div class="m2"><p>زن بود سغبهٔ چنین تعلیم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>سخن فال گو ندارد سود</p></div>
<div class="m2"><p>باد پیمود کآسمان پیمود</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نیست الّا به قدرت یزدان</p></div>
<div class="m2"><p>نیک و بد در طبایع و ارکان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بی‌قضا خلق یک نفس نزند</p></div>
<div class="m2"><p>مرد عاقل چنین جرس نزند</p></div></div>