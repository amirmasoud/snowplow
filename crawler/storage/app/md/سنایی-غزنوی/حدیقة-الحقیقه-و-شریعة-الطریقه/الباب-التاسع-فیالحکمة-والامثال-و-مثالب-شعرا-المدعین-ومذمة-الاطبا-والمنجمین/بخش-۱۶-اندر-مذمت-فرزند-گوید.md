---
title: >-
    بخش ۱۶ - اندر مذمّت فرزند گوید
---
# بخش ۱۶ - اندر مذمّت فرزند گوید

<div class="b" id="bn1"><div class="m1"><p>بود فرزند بد بود به دو باب</p></div>
<div class="m2"><p>زنده مالت برند و مرده ثواب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جهل باشد عدوت پروردن</p></div>
<div class="m2"><p>از پی رنج دل جگر خوردن</p></div></div>