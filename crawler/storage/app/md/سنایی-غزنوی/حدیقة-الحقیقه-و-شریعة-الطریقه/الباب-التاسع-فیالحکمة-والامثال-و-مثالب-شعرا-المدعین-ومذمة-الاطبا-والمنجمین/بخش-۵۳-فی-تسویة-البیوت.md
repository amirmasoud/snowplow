---
title: >-
    بخش ۵۳ - فی تسویة البیوت
---
# بخش ۵۳ - فی تسویة البیوت

<div class="b" id="bn1"><div class="m1"><p>اختراعی چنین هرآنکه نهاد</p></div>
<div class="m2"><p>راه در داد و لیک در نگشاد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خلق را جمله کرد سرگردان</p></div>
<div class="m2"><p>وآنچه کرد از عمل تبه کرد آن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شخص گاهی که در شمار آید</p></div>
<div class="m2"><p>مادرش اوّلین به کار آید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بعد از آن خانهٔ نحوس و سعود</p></div>
<div class="m2"><p>که درآمد وی از عدم به وجود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خواهران و برادران پس از آن</p></div>
<div class="m2"><p>پس پدر تا بداریش چون جان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خانهٔ رنجها و بیماری</p></div>
<div class="m2"><p>نکبات و بلای و دشخواری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بعد از آن خانهٔ مناکح و جفت</p></div>
<div class="m2"><p>به در آید بدان زمان ز نهفت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون بجَست از نهیب بند و کمند</p></div>
<div class="m2"><p>پس ورا نِه تو خانهٔ فرزند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خانهٔ دوست و خانهٔ دشمن</p></div>
<div class="m2"><p>بعد از این خانه‌ها تو پی بفکن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ورنه بیهوده زین نمط کم گوی</p></div>
<div class="m2"><p>ژاژ کم خای و پر بهانه مجوی</p></div></div>