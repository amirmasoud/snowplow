---
title: >-
    بخش ۱۸ - فی مذمّة الختن
---
# بخش ۱۸ - فی مذمّة الختن

<div class="b" id="bn1"><div class="m1"><p>کیست این هست مر مرا داماد</p></div>
<div class="m2"><p>کرده حمدان ز بهر زن پر باد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گه و بیگه درآید از درِ تو</p></div>
<div class="m2"><p>کام و ناکام گشته همسر تو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گشته معروف هرکه و هرجای</p></div>
<div class="m2"><p>کیست این مر مراست خواهر گای</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گادن آنگه کند که گیرد زر</p></div>
<div class="m2"><p>کس خواهر به زر درد آن خر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>وان زمانی که سیم نستاند</p></div>
<div class="m2"><p>ای بسا گاو و خر که برراند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر تجمل که دارد از پی کیر</p></div>
<div class="m2"><p>بدهد وان دنس نگردد سیر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون نماند درم طلاق دهد</p></div>
<div class="m2"><p>چک بیزاری و فراق دهد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سال و مه گادن به زر کند او</p></div>
<div class="m2"><p>چون نماند درم به در کند او</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خاک بر فرق خواهر و داماد</p></div>
<div class="m2"><p>که نگردد کسی از ایشان شاد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هرکه خواهد جماع سیم دهد</p></div>
<div class="m2"><p>زر به معشوق خود سلیم دهد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>زانکه داماد تا نیابد سیم</p></div>
<div class="m2"><p>نکند فرج خواهرت به دو نیم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>آنکه او خواهرت همی گاید</p></div>
<div class="m2"><p>مرگ بابات را همی پاید</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>دور باد ای برادر از ما دور</p></div>
<div class="m2"><p>خواهر و دختر ار چه بس مستور</p></div></div>