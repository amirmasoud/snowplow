---
title: >-
    بخش ۴۰ - التمثّل فی‌القناعة و ترک الحاجة
---
# بخش ۴۰ - التمثّل فی‌القناعة و ترک الحاجة

<div class="b" id="bn1"><div class="m1"><p>بود بقراط را خُمی مسکن</p></div>
<div class="m2"><p>بودش آن خُم به جای پیراهن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روزی از اتفاق سرما یافت</p></div>
<div class="m2"><p>از سوی خم به سوی دشت شتافت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پادشاه زمان برو بگذشت</p></div>
<div class="m2"><p>دیدش او را چنان برهنه به دشت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شد برِ او فراز و گفت ای تن</p></div>
<div class="m2"><p>کر بخواهی سبک سه حاجه ز من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر سه حالی روا کنم تو بخواه</p></div>
<div class="m2"><p>که منم بر زمانه شاهنشاه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفت بقراط حاجت اوّل</p></div>
<div class="m2"><p>عملم هست یک به یک به خلل</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گنهم محو کن بیامرزم</p></div>
<div class="m2"><p>کز گرانی چو کوه البرزم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گفت ویحک خدای بتواند</p></div>
<div class="m2"><p>مزد بدهد گناه بستاند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گفت برگوی حاجت دومین</p></div>
<div class="m2"><p>که منم پادشاه روی زمین</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گفت پیرم مرا جوان گردان</p></div>
<div class="m2"><p>عجز و ضعف از نهاد من بستان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گفت این از خدای باید خواست</p></div>
<div class="m2"><p>از من این خواستن نیاید راست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>زود پیش آر حاجت سومین</p></div>
<div class="m2"><p>از من این آرزو مخواه چنین</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گفت روزی من فزون گردان</p></div>
<div class="m2"><p>جانم از چنگ مرگ باز رهان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گفت این نیز کرد نتوانم</p></div>
<div class="m2"><p>مَلِکم بر جهان نه یزدانم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گفت برتر شو از برِ خورشید</p></div>
<div class="m2"><p>که رطب خیره بار نارد بید</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>حاجت از کردگار خواهم من</p></div>
<div class="m2"><p>وز تو حالی بدو پناهم من</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>تو چو من عاجزی و مجبوری</p></div>
<div class="m2"><p>وز بزرگی و برتری دوری</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>برتری مر خدای را زیباست</p></div>
<div class="m2"><p>که به ملکت همیشه بی‌همتاست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>یارب ای سیّدی به حق رسول</p></div>
<div class="m2"><p>دور گردان دل مرا ز فضول</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ای خداوند فرد بی‌همتا</p></div>
<div class="m2"><p>جسم را همچو اسم بخش سنا</p></div></div>