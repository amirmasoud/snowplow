---
title: >-
    بخش ۹ - فی ذکرالعوام و اهل السوق والجهال
---
# بخش ۹ - فی ذکرالعوام و اهل السوق والجهال

<div class="b" id="bn1"><div class="m1"><p>تا توانی به گرد عامه مگرد</p></div>
<div class="m2"><p>عامه از نام تو برآرد گرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زان کجا عامه بی‌خرد باشد</p></div>
<div class="m2"><p>صحبت بی‌خردت بد باشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به همه حال چون خودت خواهد</p></div>
<div class="m2"><p>صحبت او روان همی کاهد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چه نکو گفت آن خردمندی</p></div>
<div class="m2"><p>که سخنهای اوست چون پندی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عامه نبود ز کارها آگاه</p></div>
<div class="m2"><p>عامه را گوش کرّ و دیده تباه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>صحبت عامه اسب و خر باشد</p></div>
<div class="m2"><p>هر دوان ضد یکدگر باشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خر تگ از اسب خود نگیرد تیز</p></div>
<div class="m2"><p>لیک اسب از خران بگیرد تیز</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>صحبت عامه هرکه هشیارست</p></div>
<div class="m2"><p>مثل حدّاد و مثل عطّارست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گرچه عطّار ندهدت مشک او</p></div>
<div class="m2"><p>رسد از ناف مشک او به تو بوی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مرد حدّاد اگر به سور آید</p></div>
<div class="m2"><p>جامه ز انگشت او بیالاید</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>با بهان لحظه‌ای چو بشتابی</p></div>
<div class="m2"><p>نام نیکو ازو بسی یابی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>صحبت عامه هرکرا دیدست</p></div>
<div class="m2"><p>سخت زشت است و ناپسندیدست</p></div></div>