---
title: >-
    بخش ۱۰ - در ذمّ عوام و بازاریان و جهال گوید ذکر مساوی العوان للخواص نفع عام
---
# بخش ۱۰ - در ذمّ عوام و بازاریان و جهال گوید ذکر مساوی العوان للخواص نفع عام

<div class="b" id="bn1"><div class="m1"><p>عامه تا در جهانِ اسبابند</p></div>
<div class="m2"><p>همه در کشتی‌اند و در خوابند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل عامی چو دیدهٔ یار است</p></div>
<div class="m2"><p>نیم بیمار و نیم بیدار است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گنده و بی‌مزه است صحبت عام</p></div>
<div class="m2"><p>چون سگ پخته و چو مردم خام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زان گروهی که سوی درویشان</p></div>
<div class="m2"><p>نفرت آرد همی خرد زیشان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از دل عامی و بخیل و حسود</p></div>
<div class="m2"><p>کینه آید ولیک ناید جود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مگس و گزدمند مردم دون</p></div>
<div class="m2"><p>نیشی اندر دهان یکی در کون</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نه به دل بر نهد جهان پلید</p></div>
<div class="m2"><p>بر سر دیو چتر مروارید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز آفت نیش یک جهان گزدم</p></div>
<div class="m2"><p>چشم من پر مژه‌ست چون گندم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>روی چون ابر از آن دژم دارند</p></div>
<div class="m2"><p>که چو ابر آب در شکم دارند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چون خُره زان سزای قربانند</p></div>
<div class="m2"><p>که خره‌وار مغ مسلمانند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چون مگس روی بهر نان شویند</p></div>
<div class="m2"><p>در چو گربه برای خوان جویند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مزد باشد برای خندیدن</p></div>
<div class="m2"><p>سبلت زن به مزدشان ریدن</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گاه شوخی پلید چون مگس‌اند</p></div>
<div class="m2"><p>گاه صحبت به غیض چون دنس‌اند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>شوخ همچون مگس ولی‌بانان</p></div>
<div class="m2"><p>طعمهٔ عنکبوت بی‌سامان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بهر پیوند جان مهمان را</p></div>
<div class="m2"><p>روزه فرموده سال و مه جان را</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گر یکی میهمان بخوان رسدش</p></div>
<div class="m2"><p>کارد گویی به استخوان رسدش</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>گر دهند این گره به کوه آوا</p></div>
<div class="m2"><p>نکند کُه بزرگشان به صدا</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>از پی یک دو لقمه خرد به هیچ</p></div>
<div class="m2"><p>کرده بسیار گونه راه بسیچ</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>مردم عامه همچو زنبورست</p></div>
<div class="m2"><p>که صلاح از وجودشان دورست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>هوس دخلشان چودوزخشان</p></div>
<div class="m2"><p>دفتر خرجشان چو مطبخشان</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>از پی یک دو لقمهٔ تر و شور</p></div>
<div class="m2"><p>بام و دیوار خز چو گربه و مور</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ریششان سال و مه ببردن چیز</p></div>
<div class="m2"><p>از شره مانده بر گذر گه تیز</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>حاصل سفله چیست جز غم و رنج</p></div>
<div class="m2"><p>قفص تیز چیست جز قولنج</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>یک دم ار تخته در بغل گیرند</p></div>
<div class="m2"><p>خانهٔ خویش در تبل گیرند</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>یک دم ار گوش سوی رود آرند</p></div>
<div class="m2"><p>به دو گوز آسمان فرود آرند</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>شکر ایشان بخواهم ارچه به روز</p></div>
<div class="m2"><p>بشکند زوبه ساعتی صد گوز</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ذکرش بر هجاش شیر گواست</p></div>
<div class="m2"><p>ریش مادر غرش بکن که رواست</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>آمد از چنگشان ز سبلت حیز</p></div>
<div class="m2"><p>در تظلم میان درکهٔ تیز</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>چون نعامه به گاه نان خوردن</p></div>
<div class="m2"><p>لیک چون مرغ وقت اه کردن</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>اه کنند از دریغ اه کردن</p></div>
<div class="m2"><p>خه کنند از جواب خه کردن</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>عامه مانند گردباد بُوَد</p></div>
<div class="m2"><p>که سبک خیز همچو باد بُوَد</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>به یکی باد خوش شود ناچیز</p></div>
<div class="m2"><p>صورت مرد دارد و تن حیز</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>عرض عامه بسان نار بُوَد</p></div>
<div class="m2"><p>گرچه بی‌مال و بی‌تبار بُوَد</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>کرده مجروح چون دد از بیداد</p></div>
<div class="m2"><p>که نه دندان نه ناخنش ماناد</p></div></div>