---
title: >-
    بخش ۱۴ - اندر مذمّت خواهران گوید
---
# بخش ۱۴ - اندر مذمّت خواهران گوید

<div class="b" id="bn1"><div class="m1"><p>ور ترا خواهر آورد مادر</p></div>
<div class="m2"><p>شود از وی سیاه روی پدر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو ز میراث ربعی او را ده</p></div>
<div class="m2"><p>فحلی آور ورا سبک مسته</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر تو ناری خود آورد بی‌شک</p></div>
<div class="m2"><p>بنویسند بی‌حضور تو چک</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نشناسد ز هیچ مرد گریز</p></div>
<div class="m2"><p>نکند خود ز مرد و زن پرهیز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هم ز ده سالگی گرد در سر</p></div>
<div class="m2"><p>شوهر و مال و چیز و زرّ و گهر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زان هوسش خیره لعبت آراید</p></div>
<div class="m2"><p>کیر و کالای را همی باید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جامه بر تن درد همی به ستیز</p></div>
<div class="m2"><p>مانده در انتظار مال و جهیز</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ور کنی در جهیز او تأخیر</p></div>
<div class="m2"><p>همه توفیر تو شود تقصیر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نام و ننگت به باد بردهد او</p></div>
<div class="m2"><p>بر سرت زود خاک برنهد او</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مرد بیگانه گردد از خانه</p></div>
<div class="m2"><p>خانه‌ات پر شود ز بیگانه</p></div></div>