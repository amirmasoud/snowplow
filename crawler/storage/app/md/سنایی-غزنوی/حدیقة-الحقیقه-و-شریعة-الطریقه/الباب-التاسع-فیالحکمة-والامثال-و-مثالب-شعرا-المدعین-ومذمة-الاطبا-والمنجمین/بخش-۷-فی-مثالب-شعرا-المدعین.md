---
title: >-
    بخش ۷ - فی مثالب شعراء المدّعین
---
# بخش ۷ - فی مثالب شعراء المدّعین

<div class="b" id="bn1"><div class="m1"><p>چون ستودی بسی عدولان را</p></div>
<div class="m2"><p>سخنی گوی بوالفضولان را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آنکه بی‌آلتند و بی‌مایه</p></div>
<div class="m2"><p>همه عریان چو کیر بی‌خایه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یا طلبکار زرق و تزویرند</p></div>
<div class="m2"><p>یا جهان را به حسبه می‌گیرند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شعر برده به گازر و جولاه</p></div>
<div class="m2"><p>خواسته زو بهای کفش و کلاه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همچو خلقانیان کهن پیرای</p></div>
<div class="m2"><p>کرده یک شعر را دو گرده بهای</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همچو سگ در به در به دریوزه</p></div>
<div class="m2"><p>خوانده مر زهر را شکر بوزه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مدح شاهان به عامیان برده</p></div>
<div class="m2"><p>دیو را هوش خویش بسپرده</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>یک رمه ناحفاظ و نابینا</p></div>
<div class="m2"><p>در عبارت فرخچ و نازیبا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جای خلخال تاج بنهاده</p></div>
<div class="m2"><p>شعرشان همچو ریششان ساده</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هیچ نشناخته معانی را</p></div>
<div class="m2"><p>بد زبانی ز خوش زبانی را</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تابه از آفتابه نشناسند</p></div>
<div class="m2"><p>شکل چرخ از ذوابه نشناسند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نزد ایشان کراسه با کاسه</p></div>
<div class="m2"><p>هست یکسان چو تاس با تاسه</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>شاه را مدحت امیر برند</p></div>
<div class="m2"><p>میر را در علو به تیر برند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>عامیان را خدایگان خوانند</p></div>
<div class="m2"><p>مهتران را به پاسبان خوانند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>مدح و ذم نزدشان چو یکسانست</p></div>
<div class="m2"><p>کس زنشان چو خانه ویرانست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>همه محتاج لقمهٔ نانند</p></div>
<div class="m2"><p>همه بی‌آلتند و حیرانند</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>همه ناشسته روی و منحوسند</p></div>
<div class="m2"><p>همه تطفیل خوی و جاسوسند</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>همه با روی و طلعت شومند</p></div>
<div class="m2"><p>زان همه ساله خوار و محرومند</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بی‌زبانی ورا زبانی کرد</p></div>
<div class="m2"><p>آلت خویش بی‌زبانی کرد</p></div></div>