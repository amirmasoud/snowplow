---
title: >-
    بخش ۲۳ - در معنی زناشویی گوید
---
# بخش ۲۳ - در معنی زناشویی گوید

<div class="b" id="bn1"><div class="m1"><p>از غلام آنکه زی عیال آمد</p></div>
<div class="m2"><p>او ز دنبه بپوستکال آمد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نیست کدبانویی و گادن را</p></div>
<div class="m2"><p>زن بد جز طلاق دادن را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بندهٔ زن شدن به شهوت و مال</p></div>
<div class="m2"><p>پس براو حکم کردن اینت محال</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زشت باشد که در زناشویی</p></div>
<div class="m2"><p>بنده باشی و خواجگی جویی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بندهٔ زن مشو حرام و حلال</p></div>
<div class="m2"><p>تا نگرداندت عیال عیال</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جفت در حکم شوی خود باشد</p></div>
<div class="m2"><p>لیک در حکم بنده بد باشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تو چو انگشت گشته از تشویش</p></div>
<div class="m2"><p>زن چو ناخن‌کنان به ناخن ریش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نفقه بر ریش خواجه خط کرده</p></div>
<div class="m2"><p>سبلت او چو کون بط کرده</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سیم کابین چو طوق در گردن</p></div>
<div class="m2"><p>زرنه بر طاق و خیره غم خوردن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کرد باید زن ای ستوده سیر</p></div>
<div class="m2"><p>لیکن از خان و مان خویش به در</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>زیرک آنست کو نگاید زن</p></div>
<div class="m2"><p>ننهد در سرای خود شیون</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>اشتقاقش ز چیست دانی زن</p></div>
<div class="m2"><p>یعنی آن قحبه را به تیر بزن</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>پس اگر والعیاذباللّٰه باز</p></div>
<div class="m2"><p>بچه در سقف کس کند پرواز</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>کس ببینی گرفته از سر کین</p></div>
<div class="m2"><p>ریش بابا ز ناز در سرگین</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>پس چه گویم که هرکه عاقل‌تر</p></div>
<div class="m2"><p>پیش سحبان کیر باقل تر</p></div></div>