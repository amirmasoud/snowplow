---
title: >-
    بخش ۵۱ - در صفت بروج دوازده‌گانه
---
# بخش ۵۱ - در صفت بروج دوازده‌گانه

<div class="b" id="bn1"><div class="m1"><p>حمل و ثور و پیکر جوزا</p></div>
<div class="m2"><p>سرطان و اسد دلیل بقا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خوشهٔ خاک و کفّهٔ میزان</p></div>
<div class="m2"><p>عقرب مائی و زنار کمان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جدی خاکی و دلو و حوت بهم</p></div>
<div class="m2"><p>از هوا و ز آب داده رقم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>برّه و شیر ناریست و کمان</p></div>
<div class="m2"><p>گاو و خوشه و بز ز خاک گران</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>باز دو پیکر و ترازو و دول</p></div>
<div class="m2"><p>از هوا یافت بهره بیش ممول</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هست خرچنگ و گزدم و ماهی</p></div>
<div class="m2"><p>که بر آبستشان شهنشاهی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>حمل و عقربست از این تاریخ</p></div>
<div class="m2"><p>که شدستند خانهٔ مریخ</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ثور و میزان ز زهره دارد بهر</p></div>
<div class="m2"><p>زهره چون شاه و ثور و میزان شهر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>پس از این هست خوشه و جوزا</p></div>
<div class="m2"><p>کز عطارد گرفته‌اند بها</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سرطان خانهٔ قمر گویند</p></div>
<div class="m2"><p>شمس را جز اسد کجا جویند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>قوس و حوتست خانهٔ هرمزد</p></div>
<div class="m2"><p>جدی و دلو از زحل بجوید مزد</p></div></div>