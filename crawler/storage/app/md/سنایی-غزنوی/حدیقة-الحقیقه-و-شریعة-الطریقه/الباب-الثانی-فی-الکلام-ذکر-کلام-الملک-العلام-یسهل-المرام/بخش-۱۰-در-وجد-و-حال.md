---
title: >-
    بخش ۱۰ - در وجد و حال
---
# بخش ۱۰ - در وجد و حال

<div class="b" id="bn1"><div class="m1"><p>در طریقی که شرط جان سپریست</p></div>
<div class="m2"><p>نعرهٔ بیهده خری و تریست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مردِ دانا به جان سماع کند</p></div>
<div class="m2"><p>حرف و ظرفش همه وداع کند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جان ازو حظِّ خویش برگیرد</p></div>
<div class="m2"><p>کارها جملگی ز سر گیرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با مرید جوان سرود و شفق</p></div>
<div class="m2"><p>همچنان دان که مردِ عاشق و دق</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حال کان از مراد و زرق بُوَد</p></div>
<div class="m2"><p>همچو فرعون و بانگ غرق بُوَد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بانگ او حال غرق سود نکرد</p></div>
<div class="m2"><p>آتش آشتیش دود نکرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>الامان ای مخنّث ملعون</p></div>
<div class="m2"><p>بهر میویز باد دادی کون</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هرکه در مجلسی سه بانگ کند</p></div>
<div class="m2"><p>دان کز اندیشهٔ دو دانگ کند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ور نه آه مرید عشق‌الفنج</p></div>
<div class="m2"><p>همچو ماریست خفته بر سر گنج</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>اژدها کو ز گنج برخیزد</p></div>
<div class="m2"><p>مُهرهٔ کامش آتش انگیزد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کخ کخ اندر فقر چیست خری</p></div>
<div class="m2"><p>چک چک اندر چراغ چیست تری</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>آب و روغن چو درهم آمیزد</p></div>
<div class="m2"><p>نور در صفو روغن آویزد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>تف چو روغن ز پیش برگیرد</p></div>
<div class="m2"><p>نم بیگانه بانگ درگیرد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>آه رعنایی طبیعت تست</p></div>
<div class="m2"><p>راه بینایی شریعت تست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>آینه روشنست راه شما</p></div>
<div class="m2"><p>پردهٔ آینه است آه شما</p></div></div>