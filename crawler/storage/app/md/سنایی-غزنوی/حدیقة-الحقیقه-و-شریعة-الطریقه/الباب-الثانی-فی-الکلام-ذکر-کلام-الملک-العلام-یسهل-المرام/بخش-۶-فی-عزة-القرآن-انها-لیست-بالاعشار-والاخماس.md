---
title: >-
    بخش ۶ - فی عزّة‌القرآن انّها لیست بالاعشار والاخماس
---
# بخش ۶ - فی عزّة‌القرآن انّها لیست بالاعشار والاخماس

<div class="b" id="bn1"><div class="m1"><p>بهر یک مشت کودک از وسواس</p></div>
<div class="m2"><p>نامش اعشار کرده‌ای و اخماس</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کرده منسوخ حکم هر ناسخ</p></div>
<div class="m2"><p>نشده در علوم آن راسخ</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>متشابه ترا شده محکم</p></div>
<div class="m2"><p>کرده بر محکمش معوّل کم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تو رها کرده نور قرآن را</p></div>
<div class="m2"><p>وز پی عامه صورت آنرا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ساخته دست موزهٔ سالوس </p></div>
<div class="m2"><p>بهر یک من جو و دو کاسه سبوس</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گه سرودش کنی و گاه مثل</p></div>
<div class="m2"><p>گاه سازی ازو سلاح جدل</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گه زنی در همش به بی‌ادبی</p></div>
<div class="m2"><p>گه شمارش کنی به بوالعجبی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گه ز پایان به سر بری به خیال</p></div>
<div class="m2"><p>گه درونش برون کنی به محال</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گه کنی بر قیاس خود تأویل</p></div>
<div class="m2"><p>گه کنی حکم را برین تحویل</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گه به رای خودش کنی تفسیر</p></div>
<div class="m2"><p>گه به علم خودش کنی تقریر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>می نگردی مگر به بیغاره</p></div>
<div class="m2"><p>گرد صندوقهای سی پاره</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گاه گویی رفیق جاهل را</p></div>
<div class="m2"><p>یا نه کرباس‌باف کاهل را</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>که نویسم ترا یکی تعویذ</p></div>
<div class="m2"><p>پاک‌دار ای جوان مدار پلیذ</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>لیک هدیه پگاه می‌باید</p></div>
<div class="m2"><p>خون مرغ سیاه می‌باید</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>این همه حیله بهر یک دو درم</p></div>
<div class="m2"><p>شام تا چاشتی ز بهر شکم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>عمر بر داده‌ای به خیره به باد</p></div>
<div class="m2"><p>من چه گویم برو که شرمت باد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>در یکی مسجدی خزی به هوس</p></div>
<div class="m2"><p>حلق پر بانگ همچو نای و جرس</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>زین هوس شرم شرع و دینت باد</p></div>
<div class="m2"><p>یا خرد یا اجل قرینت باد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>با چنین خود و فضل و فرهنگت</p></div>
<div class="m2"><p>شرم بادا که نیست خود ننگت</p></div></div>