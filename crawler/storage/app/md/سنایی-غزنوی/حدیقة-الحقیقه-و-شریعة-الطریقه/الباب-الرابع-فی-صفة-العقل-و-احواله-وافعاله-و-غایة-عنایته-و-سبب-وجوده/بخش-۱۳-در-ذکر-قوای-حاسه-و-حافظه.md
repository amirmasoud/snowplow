---
title: >-
    بخش ۱۳ - در ذکر قوای حاسّه و حافظه
---
# بخش ۱۳ - در ذکر قوای حاسّه و حافظه

<div class="b" id="bn1"><div class="m1"><p>نفس کو مر ترا چو جان دارست</p></div>
<div class="m2"><p>بی‌ تو در جسم تو بسی کارست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گرچه آن پنج شحنه بی‌کارند</p></div>
<div class="m2"><p>سه وکیل از درونت بیدارند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آن کند هضم و این کند قسمت</p></div>
<div class="m2"><p>آن برد ثفل و این نهد نعمت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آن نماید ره این کند تدبیر</p></div>
<div class="m2"><p>این شود حافظ آن کند تعبیر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آن نبینی که چون به خواب شوی</p></div>
<div class="m2"><p>فارغ از زحمت و عذاب شوی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از برای فراغت و خوابت</p></div>
<div class="m2"><p>وز برای صلاح و اسبابت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اندرین خاکدان ز آتش و باد</p></div>
<div class="m2"><p>ز آب روی تو برد خاک نژاد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا ترا بر سریر سرِّ خرد</p></div>
<div class="m2"><p>بنشاند ز بهرِ راحت خود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تو بر آسوده و خرد بر کار</p></div>
<div class="m2"><p>تو بخفته درونت او بیدار</p></div></div>