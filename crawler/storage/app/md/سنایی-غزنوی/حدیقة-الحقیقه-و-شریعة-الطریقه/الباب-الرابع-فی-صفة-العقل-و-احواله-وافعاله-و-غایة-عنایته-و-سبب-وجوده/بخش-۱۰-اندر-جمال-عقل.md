---
title: >-
    بخش ۱۰ - اندر جمال عقل
---
# بخش ۱۰ - اندر جمال عقل

<div class="b" id="bn1"><div class="m1"><p>سبب امت و رسولی او</p></div>
<div class="m2"><p>علت صورت و هیولی او</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>او نهادست هم به امر قدم</p></div>
<div class="m2"><p>صورت اندر هیولی عالم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کان وجودی که بی‌زبان باشد</p></div>
<div class="m2"><p>از هیولی عقل و جان باشد</p></div></div>