---
title: >-
    بخش ۱۴ - اندر جمع بین عقل و شرع
---
# بخش ۱۴ - اندر جمع بین عقل و شرع

<div class="b" id="bn1"><div class="m1"><p>عقل چشم و پیمبری نورست</p></div>
<div class="m2"><p>آن ازین این از آن نه بس دورست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اینکه در دست شهوت و خشمند</p></div>
<div class="m2"><p>چشم بی‌نور و نور بی‌چشمند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نور بی‌چشم شاخ بی‌بر دان</p></div>
<div class="m2"><p>چشم بی‌نور جسم بی‌سر دان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>این تواضع نمای پر تلبیس</p></div>
<div class="m2"><p>وآن تکبر فزای چون ابلیس</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>این ز دست امیر چیز دهد</p></div>
<div class="m2"><p>وآن ز کون رئیس تیز دهد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نیست جز شرع و عقل و جان و دماغ</p></div>
<div class="m2"><p>خلق را در دو خطه چشم و چراغ</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون ترا از خرد هوا بدلست</p></div>
<div class="m2"><p>خنده‌ت آید ز هرچه جز جدلست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون خرد سوی هر دلی پوید</p></div>
<div class="m2"><p>وز دل هرکسی سخن گوید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از پی مصلحت درین بنیاد</p></div>
<div class="m2"><p>کاوّلش آتش است و آخر باد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>قهرمانِ امین یزدانیست</p></div>
<div class="m2"><p>بهرمانِ نگین انسانیست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>عقل جز داد و جز کرم نکند</p></div>
<div class="m2"><p>که اولوالامر خود ستم نکند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>عقل چون برگشاد زاغ هوس</p></div>
<div class="m2"><p>درکشد چون تذرو سر در خس</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>راکبی کز خرد عنان دارد</p></div>
<div class="m2"><p>اسب انجام زیر ران دارد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چهره‌ای را که روز بد نبود</p></div>
<div class="m2"><p>هیچ مشاطه چون خرد نبود</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>از خرد بدگهر نگیرد فرّ</p></div>
<div class="m2"><p>کی شود سنگ بدگهر گوهر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>مده ای پور روز نیک به بد</p></div>
<div class="m2"><p>با خرد روز آن نه با دل خود</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>با خرد باش و از هوا بگریز</p></div>
<div class="m2"><p>که هوا علّتیست رنگ آمیز</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>کَون بی‌تجربت فساد بود</p></div>
<div class="m2"><p>تجربت عقل مستفاد بود</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>خرد از بهر عاطفت باشد</p></div>
<div class="m2"><p>ختم عمرش بر این صفت باشد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>خرد از بهر برّ و احسانست</p></div>
<div class="m2"><p>زانکه خود خلقتش ازین سانست</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>حرف بد بر زبان بون باشد</p></div>
<div class="m2"><p>هرکه با دین بود نه دون باشد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ملک عقل از عقود کانی به</p></div>
<div class="m2"><p>پادشاهی ز پاسبانی به</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>عقل را هیچ مدح نتوان گفت</p></div>
<div class="m2"><p>جز بدو دُرّ مدح نتوان سفت</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>شو رها کن جهان فانی را</p></div>
<div class="m2"><p>تا بدانی جمال باقی را</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>آن کسی کو به ملک عقل رسید</p></div>
<div class="m2"><p>دو جهان را چنانکه هست بدید</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>از برای حصول نعمت دل</p></div>
<div class="m2"><p>در دل آویز خاک بر سرِ گل</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ای خداوند خالق سبحان</p></div>
<div class="m2"><p>من رهی را به ملک عقل رسان</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>سخن عقل چون تمام آمد</p></div>
<div class="m2"><p>علم را در جهان نظام آمد</p></div></div>