---
title: >-
    بخش ۱۱ - در آفرینش جهان
---
# بخش ۱۱ - در آفرینش جهان

<div class="b" id="bn1"><div class="m1"><p>از برای تناهی اندر کرد</p></div>
<div class="m2"><p>عالم جسم گویی آمد گرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>متساوی نهاد چون گویی</p></div>
<div class="m2"><p>متفاوت نه سویی از سویی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هست ممتد جهان و اندر حد</p></div>
<div class="m2"><p>متناهی جهت بود ممتد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بعد از آن در ولایت تصویر</p></div>
<div class="m2"><p>مرتبه نقش دان و نقش‌پذیر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز اوّل جان و آخر مرجان</p></div>
<div class="m2"><p>فاعل و منفعل در این دو میان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در سرای صفت‌پذیر فنا</p></div>
<div class="m2"><p>از پی رفعت قصور و بنا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عقل در بند امر بنشسته</p></div>
<div class="m2"><p>نفس در شوق عقل دل خسته</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>صورت از بهر مایه اندر بند</p></div>
<div class="m2"><p>نه فلک را به دست هفت کمند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>وز درون فلک چهار گهر</p></div>
<div class="m2"><p>همه در بند و خصم یکدیگر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سه موالید از این چهار ارکان</p></div>
<div class="m2"><p>چون نبات و معادن و حیوان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چون نباتی غذای حیوان شد</p></div>
<div class="m2"><p>حیوان هم غذای انسان شد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نطق انسان چو شد غذای ملک</p></div>
<div class="m2"><p>تا بدین روی باز شد به فلک</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ورنه در عالم یقین و گمان</p></div>
<div class="m2"><p>خر همان بودی و حکیم همان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نطق زیبا ز خامشی بهتر</p></div>
<div class="m2"><p>ورنه در جان فرامشی بهتر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>در سخن دُر ببایدت سفتن</p></div>
<div class="m2"><p>ورنه گُنگی به از سخن گفتن</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گنگ اندر حدیث کم آواز</p></div>
<div class="m2"><p>به که بسیار گوی بیهده تاز</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>کرد عقلت نصیحتی محکم</p></div>
<div class="m2"><p>که نکو گوی باش یا ابکم</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گر نصیحت قبول کردی تو</p></div>
<div class="m2"><p>فضل را کی فضول کردی تو</p></div></div>