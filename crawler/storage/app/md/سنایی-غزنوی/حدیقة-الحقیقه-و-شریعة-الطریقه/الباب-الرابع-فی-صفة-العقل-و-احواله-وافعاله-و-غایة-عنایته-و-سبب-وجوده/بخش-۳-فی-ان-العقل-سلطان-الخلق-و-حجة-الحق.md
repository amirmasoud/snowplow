---
title: >-
    بخش ۳ - فی اَنّ العقل سلطان الخلق و حجّة الحق
---
# بخش ۳ - فی اَنّ العقل سلطان الخلق و حجّة الحق

<div class="b" id="bn1"><div class="m1"><p>عقل سلطان قادر خوش خوست</p></div>
<div class="m2"><p>آنکه سایهٔ خداش گویند اوست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سایه با ذات آشنا باشد</p></div>
<div class="m2"><p>سایه از ذات کی جدا باشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سایه جز بنده‌وار کی باشد</p></div>
<div class="m2"><p>سایه را اختیار کی باشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عقل کُل تخته زیر گُل دارد</p></div>
<div class="m2"><p>هرکجا امر امر قل دارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عقل تا پیش گوی فرمانست</p></div>
<div class="m2"><p>سخنش هم قرین قرآنست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هرچه از بارگاه فرمان نیست</p></div>
<div class="m2"><p>آن همه درد تست درمان نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عقل برتر ز وهم و حسّ و قیاس</p></div>
<div class="m2"><p>برترست از فلک ستاره‌شناس</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در مصالح مدبّر جان اوست</p></div>
<div class="m2"><p>در ممالک دبیر یزدان اوست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>عقل را از عقیله باز شناس</p></div>
<div class="m2"><p>نبود همچو فربهی آماس</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>عقل کل مر ترا رهاند زود</p></div>
<div class="m2"><p>از قرینی دیو و آتش و دود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>رحمة‌اللّٰه نهاد عالم را</p></div>
<div class="m2"><p>حجّة‌الحق سرای آدم را</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>عقل اندر سرای پردهٔ کن</p></div>
<div class="m2"><p>از برای قبول کن و مکن</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>مُقبلی بود مُدبری شد باز</p></div>
<div class="m2"><p>باز اقبال یافت از پی راز</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>قابل نور امر شد به همه</p></div>
<div class="m2"><p>درخور خود نه درخور کلمه</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>هرکه او را مخالف از خود خَست</p></div>
<div class="m2"><p>وانکه او را متابع از بد رست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>با خرد کن چو مشتری تدبیر</p></div>
<div class="m2"><p>چون قمر دین ز بهر غلبه مگیر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>نفس روینده در رعایت اوست</p></div>
<div class="m2"><p>نفس گوینده در هدایت اوست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>اوست از جود کاشف الغمّة</p></div>
<div class="m2"><p>حضرت او نهایة الهمة</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>عقل داند اسامی هرچیز</p></div>
<div class="m2"><p>او کند در به و بتر تمییز</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>کدخدای تن بشر عقلست</p></div>
<div class="m2"><p>از همه حال با خبر عقلست</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>پاک و مردار بر یکی خوانست</p></div>
<div class="m2"><p>جز به عقل این کجا توان دانست</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>هرکه با عقل آشنا باشد</p></div>
<div class="m2"><p>از همه عیبها جدا باشد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>یافت عاقل ز روی فوز و فلاح</p></div>
<div class="m2"><p>در سرای فساد عین صلاح</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>سخن عاقل از طریق قیاس</p></div>
<div class="m2"><p>دُرِّ دین است و ذهن او الماس</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>گرچه مرد هنر بیابانیست</p></div>
<div class="m2"><p>جان او لوح سرّ ربّانیست</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>هنر از مرد همچو روح از تن</p></div>
<div class="m2"><p>بی‌هنر مرده جان و زنده بدن</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>شربت عقل بردبار چشد</p></div>
<div class="m2"><p>خر چو بی‌عقل بود بار کشد</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>عقل چون ابجدِ حق از بر کرد</p></div>
<div class="m2"><p>جامهٔ باطل از سرش بر کرد</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>هرکه با عقل خویش نااهلست</p></div>
<div class="m2"><p>حلم او زور و علم او جهلست</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>هرکه در بند قیلها افتاد</p></div>
<div class="m2"><p>عقل او در عقیله‌ها افتاد</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>مرد بی‌عقل جز خیالی نیست</p></div>
<div class="m2"><p>بید بی‌بَر ز دیو خالی نیست</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>مغز عقل است و اختران ثقلند</p></div>
<div class="m2"><p>پیر عقل است و خاکیان طفلند</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>دایه عقل آمد از برای سخن</p></div>
<div class="m2"><p>مجتهد را به گاهوارهٔ ظن</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>عقل هم قادرست و هم مقدور</p></div>
<div class="m2"><p>عقل هم آمرست و هم مأمور</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>برتر از صورت و مکان و محل</p></div>
<div class="m2"><p>درِ دروازهٔ جهانِ ازل</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>عقل شاهست و دیگران حشمند</p></div>
<div class="m2"><p>زانکه در مرتبت ز عقل کمند</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>همه تشریف عقل ز اللّٰه است</p></div>
<div class="m2"><p>ورنه بیچاره است و گمراهست</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>عقل کل را بسان بام شناس</p></div>
<div class="m2"><p>نردبان پایه سوی بام حواس</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>عقل تخته است و نفس نقش‌نمای</p></div>
<div class="m2"><p>نقش امرست و نقشبند خدای</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>عقل را داد کردگار این عزّ</p></div>
<div class="m2"><p>ورنه کی دیدی این شرف هرگز</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>عقل در کوی عشق نابیناست</p></div>
<div class="m2"><p>عاقلی کار بوعلی سیناست</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>سوی تو عقل صلح یا کین است</p></div>
<div class="m2"><p>اینت ریش ار سوی تو عقل این است</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>عقل کان رهنمای حیلت تست</p></div>
<div class="m2"><p>آن نه عقل است کان عقیلت تست</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>از برای صلاح دشمن را</p></div>
<div class="m2"><p>عقل خوانده حواس روشن را</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>منگر آن روشنی که هم به غرور</p></div>
<div class="m2"><p>کشت پروانه را چراغ از نور</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>عقل را هرکه با بدی آمیخت</p></div>
<div class="m2"><p>لاجرم عقل جست و او آویخت</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>آنچه عقلت نمود آن ره گیر</p></div>
<div class="m2"><p>رخ و اسبت چو شد کمِ شه گیر</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>آشنا نیست هرکه بیگانه است</p></div>
<div class="m2"><p>هرکرا عقل نیست دیوانه است</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>گنگ باید مرید پیر نیاز</p></div>
<div class="m2"><p>تا شود عقل او سخن پرداز</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>چون سخن گوی گشت عقل مُرید</p></div>
<div class="m2"><p>مرده بر در بمانده دیو مَرید</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>هرکه در عقل همچو سلمان شد</p></div>
<div class="m2"><p>دان که دیو دلش مسلمان شد</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>لاجرم چون ز عقل یافت کمال</p></div>
<div class="m2"><p>سه بیابان بُرد به سیصد سال</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>هرکرا رای و روی سلمانیست</p></div>
<div class="m2"><p>آخرین منزلش مسلمانیست</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>نیست از عقل در سرای غرور</p></div>
<div class="m2"><p>تبش و تابش از دم انگور</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>وز خرد نیست در خیال سوای</p></div>
<div class="m2"><p>می و شطرنج و نرد و بربط و نای</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>خرد از بهر امن و امر آمد</p></div>
<div class="m2"><p>نز پی خمر و زمر قمر آمد</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>عقل فرمان پادشاهی راست</p></div>
<div class="m2"><p>نز پی لاهی و ملاهی راست</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>زاجر زمر و ناهی خمر اوست</p></div>
<div class="m2"><p>وآنکه بشنیده‌ای اولواالامر اوست</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>وین سلاطین که نز ره دین‌اند</p></div>
<div class="m2"><p>نه سلاطین که آن شیاطین‌اند</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>عقل کز بهر مال و جاه و دهست</p></div>
<div class="m2"><p>دان که عطّار نیست ناک دهست</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>عقل طرّار و حیله‌گر نبود</p></div>
<div class="m2"><p>عقل دو روی و کینه‌ور نبود</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>عقل از اشعار عار دارد عار</p></div>
<div class="m2"><p>عقل را با دروغ و هرزه چکار</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>عقل بر هیچ دل ستم نکند</p></div>
<div class="m2"><p>به طمع قصد مدح و ذم نکند</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>عقل جز خواجهٔ محقق نیست</p></div>
<div class="m2"><p>عقل صوفیچهٔ مبقبق نیست</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>آنکه او آب ریز و نان طلبست</p></div>
<div class="m2"><p>وانکه ناشی وانکه بوالعجبست</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>وانکه از بهر مجمع رندان</p></div>
<div class="m2"><p>کرد تفِّ تموز در زندان</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>وانکه سرمای دی مهی را باز</p></div>
<div class="m2"><p>بند بر می‌نهد ز روی نیاز</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>وانکه داهی و آنکه سالوسیست</p></div>
<div class="m2"><p>وانکه غماز وآنکه ناموسیست</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>وانکه از سنگ شیشه پردازد</p></div>
<div class="m2"><p>وانکه در حقّه مُهره می‌بازد</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>وانکه او بر زمین هزاران بار</p></div>
<div class="m2"><p>پای بر سر نهاد چنبروار</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>هست بسیار زین نسق به جهان</p></div>
<div class="m2"><p>که حساب و شمار آن نتوان</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>این همه عقلهای عاریتی است</p></div>
<div class="m2"><p>کز پی جاه و مال و بد نیتیست</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>این همه زرنمای خاک دهند</p></div>
<div class="m2"><p>همه عطار شکل و ناک دهند</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>هر دهایی که ناپسندیده است</p></div>
<div class="m2"><p>حِسّ انسان ز عقل دزدیدست</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>هرچه نیکوست گر بِدست بَدست</p></div>
<div class="m2"><p>آن او نیست گم شدهٔ خردست</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>عقل را جز صلاح نبوَد کار</p></div>
<div class="m2"><p>عقل را در صلاح هرزه مدار</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>عقل خود کارهای بد نکند</p></div>
<div class="m2"><p>هرچه آن ناپسند خود نکند</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>عقل در دست یک رمه خود رای</p></div>
<div class="m2"><p>چون چراغی است در طهارت جای</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>خردی بوده اصل و دانش و مزد</p></div>
<div class="m2"><p>زشت نامی اوست مشتی دزد</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>عقل هرگز به کذب راضی نیست</p></div>
<div class="m2"><p>عقل هرگز وکیل قاضی نیست</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>عقل جز راست گوی و لمتر نیست</p></div>
<div class="m2"><p>حیله سازنده و گلو بر نیست</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>عقل هرگز خطا نیندیشد</p></div>
<div class="m2"><p>با من و تو بلا نیندیشد</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>عقل دمساز زور و بهتان نیست</p></div>
<div class="m2"><p>پرده‌پوش فلان و بهمان نیست</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>کرده چون در نهاد پای به قیل</p></div>
<div class="m2"><p>دست حیدر سزای عقل عقیل</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>درد ایتام و اندُه اطفال</p></div>
<div class="m2"><p>آوریدش طمع به بیت‌المال</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>داد چون خواست از علی داروش</p></div>
<div class="m2"><p>آهنی تافته سوی پهلوش</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>زور او چون نداشت گاه مقیل</p></div>
<div class="m2"><p>نه بنالید زار عقل عقیل</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>تا بدانی براستی نه به روی</p></div>
<div class="m2"><p>که دل از پشت چشم بیند روی</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>زانکه اندر نگارخانهٔ جان</p></div>
<div class="m2"><p>از پی پنج حس و چار ارکان</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>عقل از این کارها کرانه کند</p></div>
<div class="m2"><p>عقل کی قصد دام و دانه کند</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>کرم کردار گرد خویش تنند</p></div>
<div class="m2"><p>زانکه در بند جهل خویشتنند</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>گرچه از زرق و خدعه و تلبیس</p></div>
<div class="m2"><p>وز پی شادی دل ابلیس</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>از گل تو بنفشه رویانند</p></div>
<div class="m2"><p>تیره‌رایان و خیره رویانند</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>آنکه زیشان حکیم‌تر در کار</p></div>
<div class="m2"><p>در نهان گزدمست و پیدا یار</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>در سخا کند و در جفا تیزند</p></div>
<div class="m2"><p>همچو بهمان بهمن انگیزند</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>تا ترا عقل دوربین چکند</p></div>
<div class="m2"><p>خویشتن را به تو جز این چه کند</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>عقل جایی جمال بنماید</p></div>
<div class="m2"><p>که مرّفه شود برآساید</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>ننماید ترا ز خویش نشان</p></div>
<div class="m2"><p>تا تو او را مکان کنی زندان</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>مر ترا عقل چهره ننموده است</p></div>
<div class="m2"><p>ور بننمود چهره بر سودست</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>این کزین روی عقل مرد و زنست</p></div>
<div class="m2"><p>این نه عقل استراق اهرمنست</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>ذهن قلاب و کاهن و ساحر</p></div>
<div class="m2"><p>رای دزد و مشعبد و شاعر</p></div></div>
<div class="b" id="bn102"><div class="m1"><p>این همه فطنت و دها و حیل</p></div>
<div class="m2"><p>از عطای عطاردست و زحل</p></div></div>
<div class="b" id="bn103"><div class="m1"><p>خود پدیدست تا به مکّاری</p></div>
<div class="m2"><p>چه دهد هندویی و طرّاری</p></div></div>
<div class="b" id="bn104"><div class="m1"><p>دهش تیر و بخشش کیوان</p></div>
<div class="m2"><p>گوشه کشتت کنند همچو کمان</p></div></div>
<div class="b" id="bn105"><div class="m1"><p>دیو از این عقل گشت با شر و شور</p></div>
<div class="m2"><p>تا به مخراق لعنتی شد کور</p></div></div>
<div class="b" id="bn106"><div class="m1"><p>بگذر از عقل و خدعه و تلبیس</p></div>
<div class="m2"><p>که عزازیل ازین شدست ابلیس</p></div></div>
<div class="b" id="bn107"><div class="m1"><p>خردی را که آن دلیل بدیست</p></div>
<div class="m2"><p>لعنتش کن که بی‌خرد خردیست</p></div></div>
<div class="b" id="bn108"><div class="m1"><p>عقل دانست خوی بخل از جود</p></div>
<div class="m2"><p>عقل بشناخت بوی بید از عود</p></div></div>
<div class="b" id="bn109"><div class="m1"><p>درگذر زین کیاست اوباش</p></div>
<div class="m2"><p>عقل دین جو و پس روِ او باش</p></div></div>
<div class="b" id="bn110"><div class="m1"><p>عقل دین مر ترا نکو یاریست</p></div>
<div class="m2"><p>گر بیابی نه سرسری کاریست</p></div></div>
<div class="b" id="bn111"><div class="m1"><p>عقل دین مر ترا چو تیر کند</p></div>
<div class="m2"><p>بر همه آفریده میر کند</p></div></div>
<div class="b" id="bn112"><div class="m1"><p>عقل دین جز هدی عطا نکند</p></div>
<div class="m2"><p>تا نبردت به حق رها نکند</p></div></div>
<div class="b" id="bn113"><div class="m1"><p>نفس بی‌عقل احمقی باشد</p></div>
<div class="m2"><p>نوح بی‌روح زورقی باشد</p></div></div>
<div class="b" id="bn114"><div class="m1"><p>عقل مردان رسیده تا در حق</p></div>
<div class="m2"><p>شده از بند نیک و بد مطلق</p></div></div>
<div class="b" id="bn115"><div class="m1"><p>سوی عاقل چو دیو و دد باشد</p></div>
<div class="m2"><p>هرکه در بند نیک و بد باشد</p></div></div>
<div class="b" id="bn116"><div class="m1"><p>زانکه خود نیست عاقلان را برخ</p></div>
<div class="m2"><p>از چه از هفت میر و از نه چرخ</p></div></div>
<div class="b" id="bn117"><div class="m1"><p>چون همه نیک دید بد نکند</p></div>
<div class="m2"><p>زانکه بد والی خرد نکند</p></div></div>
<div class="b" id="bn118"><div class="m1"><p>والی چرخ و دهر کیست خرد</p></div>
<div class="m2"><p>عالم شرع و داد چیست خرد</p></div></div>
<div class="b" id="bn119"><div class="m1"><p>نیست اندر مقام راحت و رنج</p></div>
<div class="m2"><p>بر سرِ گنج به ز مار شکنج</p></div></div>
<div class="b" id="bn120"><div class="m1"><p>دایه‌ای زیر این کهن بنیاد</p></div>
<div class="m2"><p>نیست کس را چو عقل مادرزاد</p></div></div>
<div class="b" id="bn121"><div class="m1"><p>عقل تو روز و شب چو طوّافان</p></div>
<div class="m2"><p>بر سر چارسوی صرّافان</p></div></div>
<div class="b" id="bn122"><div class="m1"><p>خیره می‌گردد و همی گوید</p></div>
<div class="m2"><p>که فلان کون به نیک می‌شوید</p></div></div>
<div class="b" id="bn123"><div class="m1"><p>این فلان خوب و آن فلان زشتست</p></div>
<div class="m2"><p>این زمین شوره و آن زمین کشتست</p></div></div>
<div class="b" id="bn124"><div class="m1"><p>گل این خار و آب آن پست است</p></div>
<div class="m2"><p>دل این خفته عقل آن مست است</p></div></div>
<div class="b" id="bn125"><div class="m1"><p>این یکی عیسی آن دگر خرِ سول</p></div>
<div class="m2"><p>این سیم خضر و آن چهارم غول</p></div></div>
<div class="b" id="bn126"><div class="m1"><p>این بلندست و آن دگر کوتاه</p></div>
<div class="m2"><p>سرخ این شد از آن سپید و سیاه</p></div></div>
<div class="b" id="bn127"><div class="m1"><p>این همه بیهده است بگذر ازین</p></div>
<div class="m2"><p>شاه جان را لقب مکن فرزین</p></div></div>
<div class="b" id="bn128"><div class="m1"><p>تو ندانی طریق هشیاری</p></div>
<div class="m2"><p>تو خرد را دروغ‌زن داری</p></div></div>
<div class="b" id="bn129"><div class="m1"><p>پرده از روی عقل برتر کش</p></div>
<div class="m2"><p>چه زنی دست خیره بر ترکش</p></div></div>
<div class="b" id="bn130"><div class="m1"><p>چون نه‌ای مرد کار روز مصاف</p></div>
<div class="m2"><p>شب روی را بمان و خیره ملاف</p></div></div>
<div class="b" id="bn131"><div class="m1"><p>مرد درمان درد نی ز خرد</p></div>
<div class="m2"><p>دیر یابد ولیک زود خرد</p></div></div>
<div class="b" id="bn132"><div class="m1"><p>صفت عاقلان درین نو باغ</p></div>
<div class="m2"><p>کهنه نو کردنست پیش چراغ</p></div></div>
<div class="b" id="bn133"><div class="m1"><p>ز اول خلقت و بآخر عمر</p></div>
<div class="m2"><p>بوده در کار عقل جاهل و غمر</p></div></div>
<div class="b" id="bn134"><div class="m1"><p>کرد باید ز بهر کسب معاد</p></div>
<div class="m2"><p>کاسه چون کیسهٔ خرد پر داد</p></div></div>
<div class="b" id="bn135"><div class="m1"><p>بر درِ غیب ترجمان خردست</p></div>
<div class="m2"><p>شاه تن جان و شاه جان خردست</p></div></div>
<div class="b" id="bn136"><div class="m1"><p>هرکه بهر هوا خرد را راند</p></div>
<div class="m2"><p>از دو خر تا ابد پیاده بماند</p></div></div>
<div class="b" id="bn137"><div class="m1"><p>گرچه بر بی‌خرد هوا چیرست</p></div>
<div class="m2"><p>بر درِ خانه هر سگی شیرست</p></div></div>
<div class="b" id="bn138"><div class="m1"><p>بی‌خرد را بَدست فضل و هنر</p></div>
<div class="m2"><p>زانکه باشد هلاک مور از پر</p></div></div>
<div class="b" id="bn139"><div class="m1"><p>مار را چوت اجل فراز آید</p></div>
<div class="m2"><p>به سرِ ره ورا جواز آید</p></div></div>
<div class="b" id="bn140"><div class="m1"><p>دهد ایزد گه سؤال و جواب</p></div>
<div class="m2"><p>هرکسی را به قدر عقل ثواب</p></div></div>
<div class="b" id="bn141"><div class="m1"><p>دیل در جان خویشتن داری</p></div>
<div class="m2"><p>گر خرد را دروغ‌زن داری</p></div></div>
<div class="b" id="bn142"><div class="m1"><p>ور نداریم باور، از قرآن</p></div>
<div class="m2"><p>ویل والمرسلات بر خود خوان</p></div></div>
<div class="b" id="bn143"><div class="m1"><p>عقل کردن به خوی رویی هست</p></div>
<div class="m2"><p>مسخ گشت آنکه مسح عقل شکست</p></div></div>
<div class="b" id="bn144"><div class="m1"><p>عقل را چون بیافتی بنواز</p></div>
<div class="m2"><p>از دل خویش جای او بر ساز</p></div></div>