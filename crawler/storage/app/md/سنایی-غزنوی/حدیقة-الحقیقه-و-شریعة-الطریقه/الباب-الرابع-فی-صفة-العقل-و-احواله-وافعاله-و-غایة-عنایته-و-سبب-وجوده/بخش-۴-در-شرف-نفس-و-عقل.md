---
title: >-
    بخش ۴ - در شرف نفس و عقل
---
# بخش ۴ - در شرف نفس و عقل

<div class="b" id="bn1"><div class="m1"><p>پدر و مادر جهان لطیف</p></div>
<div class="m2"><p>نفس گویا شناس و عقل شریف</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زین دو جفت شریف طاق مباش</p></div>
<div class="m2"><p>واندرین هر دو اصل عاق مباش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بندگی کن همیشه ایشان را</p></div>
<div class="m2"><p>مده از دست در پریشان را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گرشان بعد امر بپرستند</p></div>
<div class="m2"><p>این دو گوهر سزای آن هستند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پدر و مادری که نازارند</p></div>
<div class="m2"><p>حکما عقل و نفس را دارند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مایه بخش سپهر و ارکانند</p></div>
<div class="m2"><p>پیشکاران عالم جانند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سبب جسمت این دو جسمانیست</p></div>
<div class="m2"><p>علت روحت آن دو روحانیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آن دوت از آرزو سپرده به خاک</p></div>
<div class="m2"><p>وآن دوت از قدر برده بر افلاک</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>حق آن دو شریف را بگذار</p></div>
<div class="m2"><p>حق این هر دو هم فرو مگذار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>وانکه در راهِ کعبه از سرِ داد</p></div>
<div class="m2"><p>اشتر این داد اگرت زاد آن داد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خرد از تو تویی برد جاوید</p></div>
<div class="m2"><p>آب را در هوا کشد خورشید</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>خرد آمد مشاطهٔ جانت</p></div>
<div class="m2"><p>خرد آمد چراغ ایمانت</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>حقّهٔ حق دراین جهان خردست</p></div>
<div class="m2"><p>سر به مُهرست و پایدار خودست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>عقل در کارگاه کن‌فیکون</p></div>
<div class="m2"><p>از پی جلوهٔ قرار و سکون</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>در ازل چون حدیث با خود راند</p></div>
<div class="m2"><p>تا ابد همچو کرم پیله بماند</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>سوی بازار دین چو جستی راه</p></div>
<div class="m2"><p>رستی ار جستی از ملامت گاه</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>از کژی دور باش و کاژ مباش</p></div>
<div class="m2"><p>چون نه‌ای عود خیره ناژ مباش</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>که کژی نفس عشوه آگین راست</p></div>
<div class="m2"><p>راستی عقل عافیت بین راست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>خرد از بد ترا نجات دهد</p></div>
<div class="m2"><p>خرد از دوزخت برات دهد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>جاهلی کفر و عاقلی دین است</p></div>
<div class="m2"><p>عیب‌جو آن و غیب‌گو این است</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>کشد این را هوا سوی سجین</p></div>
<div class="m2"><p>برد آنرا خرد به علّیّین</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>منگر آن تات بد چه فرماید</p></div>
<div class="m2"><p>آن نگر کت خرد چه فرماید</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>کند ار عاقلت به حق در خشم</p></div>
<div class="m2"><p>به از آن کت ببندد ابله چشم</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>همه کار تو باد با عقلا</p></div>
<div class="m2"><p>دور بادی ز صحبت جهلا</p></div></div>