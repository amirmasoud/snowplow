---
title: >-
    بخش ۵ - حکایت در داد و ستد خردمند
---
# بخش ۵ - حکایت در داد و ستد خردمند

<div class="b" id="bn1"><div class="m1"><p>معن دادی خمی درم به دمی</p></div>
<div class="m2"><p>باز کردی مُکاس در درمی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفت این خوی نزد من نه بدست</p></div>
<div class="m2"><p>جود مال و بخیلی خردست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مال بدهم پی جوانمردی</p></div>
<div class="m2"><p>عقل ندهم به کس به نامردی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در سخاوت چنانکه خواهی ده</p></div>
<div class="m2"><p>لیکن اندر معاملت بسته</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ستد و داد را مباش زبون</p></div>
<div class="m2"><p>مرده بهتر که زنده و مغبون</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مرد باشی به گاه بیع و شری</p></div>
<div class="m2"><p>از ثریّا نیوفتی به ثری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عقل دست و زبان کوته دان</p></div>
<div class="m2"><p>آرزو رأس مال ابله دان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ای خرد کرده سرفراز ترا</p></div>
<div class="m2"><p>سر نگونسار کرده آز ترا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مرد گرد درِ خرد گردد</p></div>
<div class="m2"><p>تنگ میدان به گرد خود گردد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هرکجا رخ نهادی ای عاقل</p></div>
<div class="m2"><p>تو به آیی چو بد نداری دل</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هرکه تدبیر رای بد نکند</p></div>
<div class="m2"><p>ستد و داد بی‌خرد نکند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بی‌خرد را ز خود نباشد سود</p></div>
<div class="m2"><p>بود او آتش است و سودش دود</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>که ازو تیره تیرگی آرد</p></div>
<div class="m2"><p>چشم را خیره خیرگی آرد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>حاکم عقل را در این بنیاد</p></div>
<div class="m2"><p>کارها محکم است و دلها شاد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>زانکه در مکتب علوم ازل</p></div>
<div class="m2"><p>از پی راندن رسوم عمل</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>نُتف او در آسمانهٔ نقل</p></div>
<div class="m2"><p>نکتش در کتابخانهٔ عقل</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>از خرد خواجه شو که سنگ سپید</p></div>
<div class="m2"><p>لعل شد زیر دامنِ خورشید</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>اوست بهر بقای جاویدان</p></div>
<div class="m2"><p>دفتر نفش و خامهٔ فرمان</p></div></div>