---
title: >-
    بخش ۱۲ - یمدح الشیخ الامام جمال‌الدّین فخرالاسلام تاج الخطباء احمدبن محمّدالملقّب بالحذور
---
# بخش ۱۲ - یمدح الشیخ الامام جمال‌الدّین فخرالاسلام تاج الخطباء احمدبن محمّدالملقّب بالحذور

<div class="b" id="bn1"><div class="m1"><p>خلق از این خانه بر حذر باشد</p></div>
<div class="m2"><p>خواجه احمد حذورتر باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آنکه خامه‌ش ز سحر بر قرطاس</p></div>
<div class="m2"><p>شب و روزی نگاشت از انقاس</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>معنی اندر میان خط سیاه</p></div>
<div class="m2"><p>درج کرده چو دین میان گناه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گرنه آن سحر کردی اندر دم</p></div>
<div class="m2"><p>آب کاغذ ببردی آب از نم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جگر گرم را خطش چو شمال</p></div>
<div class="m2"><p>نم پذیرفته چون ادیم زلال</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اوست فهرست و سرجریدهٔ علم</p></div>
<div class="m2"><p>اوست بنیاد جود و مایهٔ حلم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آسمان قدر و مشتری دیدار</p></div>
<div class="m2"><p>منتجب خلق منتخب گفتار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خاطرش تیزرو بسان شهاب</p></div>
<div class="m2"><p>کَون را با دلش نمانده حجاب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شربت شرع باغ دین خدای</p></div>
<div class="m2"><p>از غبار خیال کرده جُدای</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>همچو شرع از مخالفت دور است</p></div>
<div class="m2"><p>در همه کار خویش معذور است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>فیلسوف و حکیم و دیندارست</p></div>
<div class="m2"><p>راست چون چشم عقل بیدارست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نیست از اهل روزگار چنو</p></div>
<div class="m2"><p>آب کاغذ نگاه‌دار چنو</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نکند ظرف حرف را به اثر</p></div>
<div class="m2"><p>آتش و آب او نه خشک و نه تر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نطق او در ره جواب و سؤال</p></div>
<div class="m2"><p>تازه و خوش چو در بهار شمال</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>تازیان را شکال بر بسته</p></div>
<div class="m2"><p>لاشکان را فسار بگسسته</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گرچه خود نیست لایق قایل</p></div>
<div class="m2"><p>قابل قول او شود باقل</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>از بزرگان کفایت او دارد</p></div>
<div class="m2"><p>راست خواهی ولایت او دارد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>تا بود بر زخانش دولت و فرّ</p></div>
<div class="m2"><p>بوسه زن همچو کاغذ و دفتر</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>حفظ او آب روی شرع آرد</p></div>
<div class="m2"><p>اصل او اصلها به فرع آرد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>منبرش چرخ و او چو خورشید است</p></div>
<div class="m2"><p>مجلسش قصر و او چو جمشید است</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>هرچه گوید همه بدیع بُوَد</p></div>
<div class="m2"><p>هر شریفی برش وضیع بُوَد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>همچو آب روان بود سخنش</p></div>
<div class="m2"><p>سر نپیچد کسی ز کن مکنش</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>لفظ او خلق را جواب دهد</p></div>
<div class="m2"><p>هم براندازه‌ها ثواب دهد</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>نبود همچو گفت او گفتار</p></div>
<div class="m2"><p>راحت روح خود از آن گفت آر</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>هرگهی کو به درس بنشیند</p></div>
<div class="m2"><p>عقل در مجلسش دُرر چیند</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>عقل گردد ز لفظ او مدهوش</p></div>
<div class="m2"><p>نفس گوید که یک زمان خاموش</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>تا سماع حدیث خوب کنیم</p></div>
<div class="m2"><p>روح را پاک و بی‌عیوب کنیم</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>هرچه گوید همه نکو باشد</p></div>
<div class="m2"><p>گفتهٔ او همه چنو باشد</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بخت و دولت هوای او دارند</p></div>
<div class="m2"><p>خواجه و شاه رای او دارند</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>برتر از هفت چرخ همّت اوست</p></div>
<div class="m2"><p>بر کریمان اثر ز نعمت اوست</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>آب عذبست نکته بر نامه</p></div>
<div class="m2"><p>آتش باد پیکرش خامه</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>بینی آنگه که خواجه کلک ربود</p></div>
<div class="m2"><p>تا کند عقل را ز جان خشنود</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>هندوی مشک خانه عنبر فام</p></div>
<div class="m2"><p>بر درِ روم کرده رایت رام</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>در فصاحت زبان چو بگشاید</p></div>
<div class="m2"><p>بسته گیرد زمانه را شاید</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>زانکه آنکس که خواجهٔ دل شد</p></div>
<div class="m2"><p>زود و عالم چو شاه عادل شد</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>شد مسلّم ولایت جاهش</p></div>
<div class="m2"><p>قبلهٔ عقل گشت درگاهش</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>لب من باد بر ستانهٔ او</p></div>
<div class="m2"><p>اندرین جان فروز خانهٔ او</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>باد تا روز محشر اقبالش</p></div>
<div class="m2"><p>که مهنّاست قدر و اقبالش</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>باد تا هست ماه و مهر و سپهر</p></div>
<div class="m2"><p>جاه او چون سپهر و رخ چون مهر</p></div></div>