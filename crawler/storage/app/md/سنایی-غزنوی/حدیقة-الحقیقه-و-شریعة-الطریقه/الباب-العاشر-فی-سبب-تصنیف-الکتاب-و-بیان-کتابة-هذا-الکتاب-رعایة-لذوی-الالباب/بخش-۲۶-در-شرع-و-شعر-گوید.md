---
title: >-
    بخش ۲۶ - در شرع و شعر گوید
---
# بخش ۲۶ - در شرع و شعر گوید

<div class="b" id="bn1"><div class="m1"><p>ای سنایی چو شرع دادت بار</p></div>
<div class="m2"><p>دست ازین شاعری و شعر بدار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شرع دیدی ز شعر دل بگسل</p></div>
<div class="m2"><p>که گدایی نگارد اندر دل</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شعر بر حسب طبع و جان سره‌ئیست</p></div>
<div class="m2"><p>چون به سنّت رسیده مسخره‌ئیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شعرت اوّل که شاه تن باشد</p></div>
<div class="m2"><p>نور صبح دروغ‌زن باشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون مرا پیر عقل بپذیرفت</p></div>
<div class="m2"><p>کردگارم به فضل بپذیرفت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مدد ناحفاظ و خس بُوَد اوی</p></div>
<div class="m2"><p>غلط مؤذن و عسس بُوَد اوی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سخن شاعران همه غمز است</p></div>
<div class="m2"><p>نکتهٔ انبیا همه رمز است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آن بدین غمز خواجگی جوید</p></div>
<div class="m2"><p>وین بدین رمز راه دین پوید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شرع چون صبح صادق آمد راست</p></div>
<div class="m2"><p>که فزون شد به نور و هیچ نکاست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دردمندی به گرد عیسی گرد</p></div>
<div class="m2"><p>داروی ره‌نشین چه خواهی کرد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هرکجا شرع انبیا باشد</p></div>
<div class="m2"><p>شعر اندوه بر کیا باشد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>حکما طبع آسمان دانند</p></div>
<div class="m2"><p>انبیا روح این و آن خوانند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>آنکه سی‌روزه راه ماه بُوَد</p></div>
<div class="m2"><p>شرع را زان فلک چه جاه بُوَد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>اینک اقلیم بیم و امیدست</p></div>
<div class="m2"><p>خود یکی روزه راه خورشیدست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گر زیم بعد از این نگویم من</p></div>
<div class="m2"><p>در جهان بیش و کم به نظم سخن</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>نا تمامی عقل بودستم</p></div>
<div class="m2"><p>خویشتن را بیازمودستم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ای کسانیکه اهل غزنینید</p></div>
<div class="m2"><p>بر سرِ خاک چون که بنشنید</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>هرزه و بیهُده مپردازید</p></div>
<div class="m2"><p>نفط در خرمنم میندازید</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ظاهر آنچه گفته‌های منست</p></div>
<div class="m2"><p>وصف نقش خط خدای منست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>تو مخوانش غزل که توحیدست</p></div>
<div class="m2"><p>باطنش وحی و حمد و تمجیدست</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>گر توانید گه گهم به دعا</p></div>
<div class="m2"><p>یاد دارید مهتر و برنا</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>که بیامرزش ای خدای خبیر</p></div>
<div class="m2"><p>عذر تقصیرها ازو بپذیر</p></div></div>