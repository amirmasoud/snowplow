---
title: >-
    بخش ۱۳ - در قناعت و انزوای خویش گوید
---
# بخش ۱۳ - در قناعت و انزوای خویش گوید

<div class="b" id="bn1"><div class="m1"><p>ای که در زیر طبع گردونی</p></div>
<div class="m2"><p>چند گویی مرا که از دونی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با چنین گنج در چنین گنجی</p></div>
<div class="m2"><p>چه گنه گنج را تو ناگنجی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رنج با گنج و زحمت نااهل</p></div>
<div class="m2"><p>چون بریدی طمع ترا شد سهل</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زحمت خود ز اهل عصر بکاه</p></div>
<div class="m2"><p>هرچه خواهی ز خالق خودخواه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خلق را جمله صورتی انگار</p></div>
<div class="m2"><p>هیچی از هیچ خلق طمع مدار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جُرم من اندرین چه می‌دانی</p></div>
<div class="m2"><p>چون بدیدی کمال نادانی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نرسد در ولایت دل خویش</p></div>
<div class="m2"><p>هیچ بی‌حوصله به حاصل خویش</p></div></div>