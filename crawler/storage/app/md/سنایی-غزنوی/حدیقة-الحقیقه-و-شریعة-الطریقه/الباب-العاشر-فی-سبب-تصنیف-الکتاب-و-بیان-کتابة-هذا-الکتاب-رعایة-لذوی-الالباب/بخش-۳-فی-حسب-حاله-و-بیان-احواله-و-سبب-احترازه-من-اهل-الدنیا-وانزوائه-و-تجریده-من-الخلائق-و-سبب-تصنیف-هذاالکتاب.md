---
title: >-
    بخش ۳ - فی حسب حاله و بیان احواله و سبب احترازه من اهل الدّنیا وانزوائه و تجریده من‌الخلائق و سبب تصنیف هذاالکتاب
---
# بخش ۳ - فی حسب حاله و بیان احواله و سبب احترازه من اهل الدّنیا وانزوائه و تجریده من‌الخلائق و سبب تصنیف هذاالکتاب

<div class="b" id="bn1"><div class="m1"><p>حسب حال آنکه دیو آز مرا</p></div>
<div class="m2"><p>داشت یک چند در گداز مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گرد آفاق گشته چون پرگار</p></div>
<div class="m2"><p>گرد گردان ز حرص دایره‌وار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شاه خرسندیم جمال نمود</p></div>
<div class="m2"><p>جمع و منع و طمع محال نمود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شدم اندر طلاب مال ملول</p></div>
<div class="m2"><p>از جهان و جهانیان معزول</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بود طبعم ز نظم و نثر نفور</p></div>
<div class="m2"><p>چون ز اسکندر مظفّر فور</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا در این حضرتم خرد تلقین</p></div>
<div class="m2"><p>کرد این نامهٔ بدیع آیین</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یادگاری طرازم از پی شاه</p></div>
<div class="m2"><p>جان فزای از معانی دلخواه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>روش روز را بُوَد وادی</p></div>
<div class="m2"><p>مهتدی را ازو بُوَد هادی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>عقلا را بُوَد نکو دستور</p></div>
<div class="m2"><p>نخورد زان سپس شراب غرور</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>رستگاری وی درین باشد</p></div>
<div class="m2"><p>یادگار خرد چنین باشد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هرزه ناورده‌ام من این تصنیف</p></div>
<div class="m2"><p>جان و دل کرده‌ام در این تألیف</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ریسمان کرده‌ام تن و جان را</p></div>
<div class="m2"><p>تا به سوزن بکنده‌ام کان را</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گرچه هرگز نبود وقت سخن</p></div>
<div class="m2"><p>در غریبی غریب شعر چو من</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گرچه مولد مرا ز غزنین است</p></div>
<div class="m2"><p>نظم شعرم چو نقش ما چین است</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>خاک غزنین چو من نزاد حکیم</p></div>
<div class="m2"><p>آتشی باد خوار و آب ندیم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بهر حکمت برغم انجمنی</p></div>
<div class="m2"><p>مر ترا کی گریزد از چو منی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>لیکن از روی حکمت لقمان</p></div>
<div class="m2"><p>رقم لقمه ماند بر انبان</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>از تو پرسم حکیم‌وار جواب</p></div>
<div class="m2"><p>بازده بر طریق صدق و صواب</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>در همه عالم از دو قاف زمین</p></div>
<div class="m2"><p>تا به کاف سماک و تا پروین</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>از پی شعر کو سخن دانی</p></div>
<div class="m2"><p>بهر سیمرغ کو سلیمانی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>همه مرغی ز شاخ بسراید</p></div>
<div class="m2"><p>لیک طوطی شکر همی خاید</p></div></div>