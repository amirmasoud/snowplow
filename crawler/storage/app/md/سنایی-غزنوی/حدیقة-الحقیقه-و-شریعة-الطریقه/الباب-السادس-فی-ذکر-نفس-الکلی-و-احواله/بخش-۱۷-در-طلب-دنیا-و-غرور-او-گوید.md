---
title: >-
    بخش ۱۷ - در طلب دنیا و غرور او گوید
---
# بخش ۱۷ - در طلب دنیا و غرور او گوید

<div class="b" id="bn1"><div class="m1"><p>زینة اللّٰه نه اسب و زین باشد</p></div>
<div class="m2"><p>زینة‌اللّٰه جمالِ دین باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مرده ای زان شدی اسیرِ هوس</p></div>
<div class="m2"><p>دیده از مردگان کشد کرگس</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در جهان منگر از پی رازش</p></div>
<div class="m2"><p>چکنی رنگ و بوی غمّازش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>که تو اندر جهان بدسازان</p></div>
<div class="m2"><p>همچو رازی به دست غمازان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نیست مهر زمانه بی‌کینه</p></div>
<div class="m2"><p>سیر دارد میان لوزینه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کی سزای جهان جان باشد</p></div>
<div class="m2"><p>هرکرا روی دل به کان باشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سرنگون خیزد از سرای معاد</p></div>
<div class="m2"><p>هرکه روی از خرد نهد به جماد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هرکه اکنون درین کلوخین گوی</p></div>
<div class="m2"><p>از نَبیّ و نُبی بتابد روی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون قیامت برآید از کویش</p></div>
<div class="m2"><p>روی باشد قفا قفا رویش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ای سنایی برای دین و صلاح</p></div>
<div class="m2"><p>وز پی جُستن نجات و فلاح</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>همچو دریا چو نیست اینجا حُر</p></div>
<div class="m2"><p>کام پر زهر باش و دل پر دُر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>زانکه در جان به واسطهٔ اسباب</p></div>
<div class="m2"><p>زفتی از خاک رُست و ترّی از آب</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>آدمی چون غلام راتبه شد</p></div>
<div class="m2"><p>ژاژِ طیان به خط کاتبه شد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گرچه جان همچو آب پاک آمد</p></div>
<div class="m2"><p>زر نگهدارتر ز خاک آمد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ورنه ارکان ز خاک پاکستی</p></div>
<div class="m2"><p>زر نگهدارتر ز خاکستی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>معطیان زفت و دل زحیر زده</p></div>
<div class="m2"><p>دایه بیمار و طفل شیر زده</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>هرکه در زندگی بخیل بُوَد</p></div>
<div class="m2"><p>چون بمیرد چو سگ ذلیل بُوَد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بیش از این بهر خواجه و مزدور</p></div>
<div class="m2"><p>نتوان رفت در جوال غرور</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>تو مشو غرّه کو سیه چرده‌ست</p></div>
<div class="m2"><p>کان سیاهه سپید بر کرده‌ست</p></div></div>