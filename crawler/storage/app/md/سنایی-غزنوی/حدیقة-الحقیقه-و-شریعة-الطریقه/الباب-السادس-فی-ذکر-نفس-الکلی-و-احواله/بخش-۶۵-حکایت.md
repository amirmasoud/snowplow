---
title: >-
    بخش ۶۵ - حکایت
---
# بخش ۶۵ - حکایت

<div class="b" id="bn1"><div class="m1"><p>آن شنیدی که با سکندر راد</p></div>
<div class="m2"><p>گفت در پیش مردمان استاد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کی شده فتنه بر جهانگیری</p></div>
<div class="m2"><p>غافل از روز مرگ وز پیری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>باز عمر تو چون کند پرواز</p></div>
<div class="m2"><p>نبود با هیچ کس دمساز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هرکسی گوشه‌ای دگر گیرد</p></div>
<div class="m2"><p>ورچه شاهی به بنده نپذیرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در جهان بهتر از کم آزاری</p></div>
<div class="m2"><p>هیچ کاری تو تا نپنداری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عمر کرکس از آن بُوَد بسیار</p></div>
<div class="m2"><p>که نبیند کسی از او آزار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا از او جانور نیازارد</p></div>
<div class="m2"><p>جز به مردار سر فرو نارد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>باز اگر کبک را نکشتی زار</p></div>
<div class="m2"><p>سال عمرش فزون شدی ز هزار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زانکه از کرکس او ضعیف‌ترست</p></div>
<div class="m2"><p>طعمه و جای او لطیف‌ترست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هرکه خون ریختن کند آغاز</p></div>
<div class="m2"><p>زود میرد بسان باشه و باز</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چون نمودم در این سخن برهان</p></div>
<div class="m2"><p>سخن آغاز کردم از نسیان</p></div></div>