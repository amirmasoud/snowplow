---
title: >-
    بخش ۳۶ - فی ذکر انساب البشر من ارکان البشر
---
# بخش ۳۶ - فی ذکر انساب البشر من ارکان البشر

<div class="b" id="bn1"><div class="m1"><p>آدمی گرچه بر زمانه مهست</p></div>
<div class="m2"><p>ز آدمی خام دیو پخته بهست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نیست خامی مگر کم اندر کم</p></div>
<div class="m2"><p>چون ره رومیان خم اندر خم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کآدمی‌زاده تا نشد مردم</p></div>
<div class="m2"><p>گه پری گه ددست گه گزدم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در زمانه ز هرچه جانورست</p></div>
<div class="m2"><p>تا نشد پخته آدمی بترست</p></div></div>