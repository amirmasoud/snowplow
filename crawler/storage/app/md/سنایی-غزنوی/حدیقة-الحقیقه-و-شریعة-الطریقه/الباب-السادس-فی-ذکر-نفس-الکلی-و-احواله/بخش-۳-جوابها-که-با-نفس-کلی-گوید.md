---
title: >-
    بخش ۳ - جوابها که با نفس کلّی گوید
---
# بخش ۳ - جوابها که با نفس کلّی گوید

<div class="b" id="bn1"><div class="m1"><p>گفت من دست گرد لاهوتم</p></div>
<div class="m2"><p>قائد و رهنمای ناسوتم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در جهانی که بخت جای منست</p></div>
<div class="m2"><p>این جهان جمله زیر پای منست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اوّل خلق در جهان ماییم</p></div>
<div class="m2"><p>نه همه جای چهره بنماییم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>برِ نااهل و سفله کم گردیم</p></div>
<div class="m2"><p>در جبلّت ز خلقها فردیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نظر حق به ماست از همه خلق</p></div>
<div class="m2"><p>خلقت ما جداست از همه خلق</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تربتم گوهرست کانها را</p></div>
<div class="m2"><p>موضعم مرجعست جانها را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>من ز اقلیمی آمدم ایدر</p></div>
<div class="m2"><p>چون قلم کرده پای تارک سر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آن زمین کاندر آن مبارک جاست</p></div>
<div class="m2"><p>همچو خورشید آسمان شماست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سنگ او گوهرست و خاکش زر</p></div>
<div class="m2"><p>بحر او انگبین و کُه عنبر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>قصرهایی درو بلند و شگرف</p></div>
<div class="m2"><p>پاک چون آتش و سپید چو برف</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بامشان چون فلک مسیح پذیر</p></div>
<div class="m2"><p>بومشان همچو نقطه قارون گیر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>وآن گروهی که اندرین جایند</p></div>
<div class="m2"><p>گوهرین سر زُمردین پایند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>پل جیحونشان سرِ ظالم</p></div>
<div class="m2"><p>وحش‌گه پایه‌شان دل عالم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>سر بسان سران سرافرازان</p></div>
<div class="m2"><p>قد چو اومید ابلهان یازان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>همه مستغرق جمال قدم</p></div>
<div class="m2"><p>فارغ از نقش عالم و آدم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گاوشان از برای دفع الم</p></div>
<div class="m2"><p>نیزه‌بازی کند چو شیر علَم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>کشورش روز و شب فزاینده</p></div>
<div class="m2"><p>او و هرچ اندروست پاینده</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>همه از روی بی‌غمی جاوید</p></div>
<div class="m2"><p>بی‌خبر همچو سایه و خورشید</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>اندران باغ هریکی زیشان</p></div>
<div class="m2"><p>از برای قبول درویشان</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>صاحب صدره سدرهٔ ازلیست</p></div>
<div class="m2"><p>مونس فاطمه جمال علی است</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چه صفت گویم آن گُره را من</p></div>
<div class="m2"><p>همه اندر یقین جان بی‌ظن</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>عندلیبان روضهٔ انسند</p></div>
<div class="m2"><p>ساکنان حظیرهٔ قدسند</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>عالمی سر به سر بدیع‌الحال</p></div>
<div class="m2"><p>نقش او رهنمای خیرالفال</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بینی آن روضه را اگر خواهی</p></div>
<div class="m2"><p>کنی از جان و دیده همراهی</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بی‌عقوبت زمینش از ذل و غم</p></div>
<div class="m2"><p>بی‌عفونت هوایش از تف و نم</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>من زمینش ز کوه و از گو دور</p></div>
<div class="m2"><p>هم هواش از حوادث الجو دور</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>سنگ ریز و گیاش عالم و حی</p></div>
<div class="m2"><p>حشرات زمینش خسرو و کی</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>هرچه در صحن او مکان دارد</p></div>
<div class="m2"><p>تا به سنگ و کلوخ جان دارد</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>من ز درگاه خازن ملکوت</p></div>
<div class="m2"><p>حجّتم در خزینهٔ ناسوت</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>گفتم آخر کجاست آن کشور</p></div>
<div class="m2"><p>گفت کز کی و از کجا برتر</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>جای کی گویمش که شهر خدای</p></div>
<div class="m2"><p>جای جانست و جان ندارد جای</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>این چنین نکته‌ها چو گفت مرا</p></div>
<div class="m2"><p>خرد اندر بصر بخفت مرا</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>زانکه اندر جمال آن زیبا</p></div>
<div class="m2"><p>مانده بودم چو نقش بر دیبا</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>اجل از دست آن لبِ خندان</p></div>
<div class="m2"><p>سرِ انگشت مانده در دندان</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>چشم کز صورتش ندارد برخ</p></div>
<div class="m2"><p>دیده زو برکشد دو کرگس چرخ</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>مرکبی کو به زیر ران دارد</p></div>
<div class="m2"><p>آخر از راه کهکشان دارد</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>جان ما واله از جلالت او</p></div>
<div class="m2"><p>مدرک کس نگشته حالت او</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>عشق در کوی غیب حالت او</p></div>
<div class="m2"><p>صدق در راه دین مقالت او</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>هیچ بیهوده را بدو ره نیست</p></div>
<div class="m2"><p>زانکه در خلقها چنو شه نیست</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>در و درگاه او چو مرئی نیست</p></div>
<div class="m2"><p>مرو آنجا به جای خویش بایست</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>پیش درگاه او ز اهل هوس</p></div>
<div class="m2"><p>مُل سوارست و گُل پیاده و بس</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>او امیریست کاندرین بنیاد</p></div>
<div class="m2"><p>از پی عزّ شرع و دادن داد</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>بر درش لشکر هوس نبود</p></div>
<div class="m2"><p>وز سوار و پیاده کس نبود</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>روح را کرده از جواهر نور</p></div>
<div class="m2"><p>گوش و گردن چو گوش و گردن حور</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>پرده‌ها باشد از هدایت او</p></div>
<div class="m2"><p>حرف و آواز در ولایت او</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>روز کوری ترا به خود پذرفت</p></div>
<div class="m2"><p>کت در این لافگاه غرجه گرفت</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>تا نُبی و نَبی ز چون تو سقط</p></div>
<div class="m2"><p>این درآمد به صورت آن در خط</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>جهل تو بهر قال و قیلی را</p></div>
<div class="m2"><p>دحیه کردست جبرئیلی را</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>گرد این پیر گرد تا از چاه</p></div>
<div class="m2"><p>پایت آرد ز چاه بر سرِ گاه</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>طفل کو بر گرد کسی گردد</p></div>
<div class="m2"><p>تخم کو پرورد بسی گردد</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>زانکه از قوّت قوایم او</p></div>
<div class="m2"><p>بهر جاوید نفس سایم او</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>کس چنود کم شنود در سلفوت</p></div>
<div class="m2"><p>برزگر در مزارع ملکوت</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>جان من بهر این حدیث چو نوش</p></div>
<div class="m2"><p>چشم بنهاده بر دریچهٔ گوش</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>نشدم سیر از آن سخن‌دان زیر</p></div>
<div class="m2"><p>تشنه ز آب نمک نگردد سیر</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>جان ز دیدار دوست پروردن</p></div>
<div class="m2"><p>هست چون شهد و گلشکر خوردن</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>معده از علم زان نگردد پست</p></div>
<div class="m2"><p>که طعام و شره بود هم دست</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>گفتمش با تو روزگار خوش است</p></div>
<div class="m2"><p>با تو خواهم که با تو کار خوش است</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>بی‌چنو پیر در جوانی خویش</p></div>
<div class="m2"><p>که خورد بر ز زندگانی خویش</p></div></div>