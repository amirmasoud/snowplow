---
title: >-
    بخش ۱۶ - اندر مذمّت کسانی که به جامه و لقمه مغرور باشند
---
# بخش ۱۶ - اندر مذمّت کسانی که به جامه و لقمه مغرور باشند

<div class="b" id="bn1"><div class="m1"><p>جامه از بهر عورت عامه است</p></div>
<div class="m2"><p>خاصگان را برهنگی جامه است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مر زنان راست جامه اندر خور</p></div>
<div class="m2"><p>حیدر و مرد و جوشن اندر بر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جامه بر عورتان پسندیدست</p></div>
<div class="m2"><p>جامهٔ دیبه آفت دیدست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرد را در لباس خُلقان جوی</p></div>
<div class="m2"><p>گنج در کُنجهای ویران جوی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مر زنان راست جامه تو بر تو</p></div>
<div class="m2"><p>مرد را روز نو و روزی نو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون نباشد ملامت و اتعاظ</p></div>
<div class="m2"><p>بس بود جامهٔ برهنه حفاظ</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مر زنان را برهنگی جامه‌است</p></div>
<div class="m2"><p>خاصه آن را که شوخ و خودکامه است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نیست زن را به جامه خانهٔ هوش</p></div>
<div class="m2"><p>به ز عریانی ایچ عورت پوش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>عورتانند جاهلان کِه و مِه</p></div>
<div class="m2"><p>هرکه پوشیده‌تر ز عورت به</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>باقیی در بقای معنی کوش</p></div>
<div class="m2"><p>پنبه رو بازده به پنبه فروش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چکند عقل جامهٔ زیبا</p></div>
<div class="m2"><p>نقش دیبا چه داند از دیبا</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چه کُشی از پی هوس تن را</p></div>
<div class="m2"><p>گرمی عشق جامه بس تن را</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>دین به زیر کلاه داری تو</p></div>
<div class="m2"><p>زان هوای گناه داری تو</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>با کلاه از هوای تن نَجهی</p></div>
<div class="m2"><p>سر پدید آید ار کُله بنهی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چون سرآمد پدید در شبگیر</p></div>
<div class="m2"><p>پای ذر نه عمارت از سر گیر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>یک شبی رو به وقت شبگیران</p></div>
<div class="m2"><p>با حذر در نهان ز خر گیران</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>سر خود را پدید کن ز کلاه</p></div>
<div class="m2"><p>توبه این است از گذشته گناه</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چه شد ار بر سر تو افسر نیست</p></div>
<div class="m2"><p>خرد اندر سرست و بر سر نیست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>نقش آنها کز اهل محرابند</p></div>
<div class="m2"><p>در جریدهٔ مجرّدان یابند</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>آنکه نقش کلاه و سر دارند</p></div>
<div class="m2"><p>زن و زنبیل و زور و زر دارند</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>متأهّل دو پای خود در بست</p></div>
<div class="m2"><p>سر خود را به دست خود بشکست</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>گر زید ور بمیرد آن بدبخت</p></div>
<div class="m2"><p>رخت و بختش بماند زیر درخت</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>همچنین ژنده جامه باید بود</p></div>
<div class="m2"><p>در خورِ عقل عامه باید بود</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>کانکه از عقل عامه دور افتاد</p></div>
<div class="m2"><p>آب عمرش بداد خاک به باد</p></div></div>