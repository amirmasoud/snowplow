---
title: >-
    بخش ۴۰ - اندر بیان ظلومی و جهولی انسان  کما قال‌اللّٰه تعالی: وحملها الانسان انّه کان ظلوما جهولا
---
# بخش ۴۰ - اندر بیان ظلومی و جهولی انسان  کما قال‌اللّٰه تعالی: وحملها الانسان انّه کان ظلوما جهولا

<div class="b" id="bn1"><div class="m1"><p>هیچ بدنامی آدمی را پیش</p></div>
<div class="m2"><p>از ظلومی و از جهولی خویش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چه حدیثست هرچه پیش آید</p></div>
<div class="m2"><p>همه از ظلم و جهل خویش آید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حق پسندست عالم و عادل</p></div>
<div class="m2"><p>بنده‌گه ظالمست و گه جاهل</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آدمی با گنه شکسته‌ترست</p></div>
<div class="m2"><p>پای طاوس چشم زخم پرست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کانکه گوید منم شده معصوم</p></div>
<div class="m2"><p>اوست بر نفس خویشتن میشوم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کانکه خود را شکسته دل بیند</p></div>
<div class="m2"><p>خویشتن را به دل خجل بیند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اوست شایستهٔ خدای کریم</p></div>
<div class="m2"><p>ایمن است از عذاب نار جحیم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گفت داود را خدای جهان</p></div>
<div class="m2"><p>که منم یاور شکسته دلان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جان پاکان خزانهٔ فلکست</p></div>
<div class="m2"><p>جسم نیکان نشیمن ملکست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>جسم تو گرچه ناپسندیده است</p></div>
<div class="m2"><p>شوخ چشمست لیک خوش دیده است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آدمی سر به سر همه آهوست</p></div>
<div class="m2"><p>ظن چنان آیدش که بس نیکوست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>عیب دارد دو صد هزاران بیش</p></div>
<div class="m2"><p>هنرش آنکه از بهایم پیش</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گر بود زینتش ز عقل و ادب</p></div>
<div class="m2"><p>ورنه هم با بهایم است نسب</p></div></div>