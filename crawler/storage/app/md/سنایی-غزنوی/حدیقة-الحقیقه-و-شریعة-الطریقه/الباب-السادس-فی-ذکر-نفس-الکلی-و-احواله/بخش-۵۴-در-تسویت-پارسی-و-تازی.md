---
title: >-
    بخش ۵۴ - در تسویت پارسی و تازی
---
# بخش ۵۴ - در تسویت پارسی و تازی

<div class="b" id="bn1"><div class="m1"><p>فضل دین در رهِ مسلمانیست</p></div>
<div class="m2"><p>هنر ملک ره فرادانیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هست محتاج کارسازی ملک</p></div>
<div class="m2"><p>چه کند پارسی و تازی ملک</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از پی دین و شغل پردازی</p></div>
<div class="m2"><p>هیچ در بسته نیست در تازی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا عمر شمع تازیان بفروخت</p></div>
<div class="m2"><p>کسری اندر عجم چو هیمه بسوخت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ملک عدلست و دین دلِ پر درد</p></div>
<div class="m2"><p>تازی و پارسی چه خواهد کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پارسی بهر کارسازی تست</p></div>
<div class="m2"><p>تازی از بهر کرّه تازی تست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر به تازی کسی ملک بودی</p></div>
<div class="m2"><p>بوالحکم خواجهٔ فلک بودی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تازی ار شرع را پناهستی</p></div>
<div class="m2"><p>بولهب آفتاب و ماهستی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مرد را چون هنر نباشد کم</p></div>
<div class="m2"><p>چه ز اهل عرب چه ز اهل عجم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بهر معنیست قدر تازی را</p></div>
<div class="m2"><p>نز پی صورت مجازی را</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هرکه شد جان مصطفی را اهل</p></div>
<div class="m2"><p>چکند ریش و سبلت بوجهل</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بهر معنیست صورت تازی</p></div>
<div class="m2"><p>نه بدان تا تو خواجگی سازی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>روح با عقل و علم داند زیست</p></div>
<div class="m2"><p>روح را پارسی و تازی چیست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>این چنین جلف و بی‌ادب زانی</p></div>
<div class="m2"><p>که تو تازی ادب همی خوانی</p></div></div>