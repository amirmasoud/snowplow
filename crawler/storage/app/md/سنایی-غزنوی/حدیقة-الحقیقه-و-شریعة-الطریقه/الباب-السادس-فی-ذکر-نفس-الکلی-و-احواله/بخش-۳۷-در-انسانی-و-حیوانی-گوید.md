---
title: >-
    بخش ۳۷ - در انسانی و حیوانی گوید
---
# بخش ۳۷ - در انسانی و حیوانی گوید

<div class="b" id="bn1"><div class="m1"><p>هست ترکیب نفسِ انسانی</p></div>
<div class="m2"><p>نفسی و عقلی و هیولانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از دل و جان و نیروی فایت</p></div>
<div class="m2"><p>حدِّ او حیّ ناطق مایت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گِل و دل دان سرشتهٔ آدم</p></div>
<div class="m2"><p>این برآن آن برین شده درهم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هرچه جز مردمند یک رنگند</p></div>
<div class="m2"><p>یا همه صلح یا همه جنگند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>روح انسان عجایبیست عظیم</p></div>
<div class="m2"><p>آدم از روح یافت این تعظیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بلعجب روح روح انسانیست</p></div>
<div class="m2"><p>که در این دیوخانه زندانیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گاه با امر سوی حق یازد</p></div>
<div class="m2"><p>گاه با خلق خالکی بازد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>فلکی زیر دست او پیوست</p></div>
<div class="m2"><p>او خود از دست خویش هفت بدست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>پایی اندر تن و یکی در جان</p></div>
<div class="m2"><p>متحیّر بمانده چون مرجان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گِل و دل آدمی چو نخچیر است</p></div>
<div class="m2"><p>هم زبونست و هم زبون‌گیرست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گاه عاجز ضعیف تن ز تبی</p></div>
<div class="m2"><p>گاه همچون سبع پر از شغبی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تن ضعیف و قوی دل آدمی است</p></div>
<div class="m2"><p>آفریده تن از گِل آدمی است</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>لیک دارد میان گِل گوهر</p></div>
<div class="m2"><p>نیست از خلق مر ورا همسر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>اعتقاد ترا به خیر و به شر</p></div>
<div class="m2"><p>جز قیامت مباد قیمت گر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نیست از بهر طامع و خایف</p></div>
<div class="m2"><p>هیچ قیمت گری چنو منصف</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>نفخهٔ صور سورِ مردانست</p></div>
<div class="m2"><p>هرکه زان سور خورد مرد آنست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>راز اگر چون زمین نگهداری</p></div>
<div class="m2"><p>آسمان‌وار بهره برداری</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>روی دل را خرد روان آمد</p></div>
<div class="m2"><p>بهرهٔ چار طبع جان آمد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>روح چون رفت خانه پاک بماند</p></div>
<div class="m2"><p>کالبد در مغاک خاک بماند</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>هرکه زین جرعه طافح و ثملست</p></div>
<div class="m2"><p>عقل او شب چراغ روز دلست</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>در شبِ وصل پرده‌گر باشد</p></div>
<div class="m2"><p>راز در روز پرده‌در باشد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>روز باشد قوی دل و غمّاز</p></div>
<div class="m2"><p>با ضعیفان به شب به آید راز</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>زانکه مقلوب روز زور بود</p></div>
<div class="m2"><p>مرغ عیسی به روز کور بود</p></div></div>