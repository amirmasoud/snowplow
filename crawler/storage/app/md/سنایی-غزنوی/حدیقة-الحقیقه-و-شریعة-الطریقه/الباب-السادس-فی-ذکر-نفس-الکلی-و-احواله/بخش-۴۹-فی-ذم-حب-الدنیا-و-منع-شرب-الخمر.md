---
title: >-
    بخش ۴۹ - فی ذمِ حبّ الدّنیا و منع شرب الخمر
---
# بخش ۴۹ - فی ذمِ حبّ الدّنیا و منع شرب الخمر

<div class="b" id="bn1"><div class="m1"><p>می همی خور کنون به بوی بهار</p></div>
<div class="m2"><p>باش تا بردمد ز گور تو خار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای چو فرعون شوم گردنکش</p></div>
<div class="m2"><p>از ره آب رفته در آتش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چکنی در میان رنج خمار</p></div>
<div class="m2"><p>کار آبی که آتش آرد بار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زان چنان خون که از لگد ریزند</p></div>
<div class="m2"><p>پس ز تابوت خم برانگیزند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نه که زنده شوی گزنده شوی</p></div>
<div class="m2"><p>از لگد مرده‌ای چه زنده شوی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون چو شیران به کرد خود نچری</p></div>
<div class="m2"><p>همچو روباه خون رز چه خوری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عشق بیرون برد ترا ز خودی</p></div>
<div class="m2"><p>بی‌خودی را بدان ز بیخردی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>با خرد میل سوی مُل چکنی</p></div>
<div class="m2"><p>سپر خار برگ گل چکنی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آنکه دارد خرد نخواهد مُل</p></div>
<div class="m2"><p>وانکه باشد حزین نبوید گُل</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از پی هوش برمگردان میل</p></div>
<div class="m2"><p>خاصه مستی و خانه بر ره سیل</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چون براتی ندارد اندر ره</p></div>
<div class="m2"><p>لاشه خر را به دست دزد مده</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به بُوَد خواجه را در این بازار</p></div>
<div class="m2"><p>وندرین گلشن و درین گلزار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>کیسه خالی و شهر پر ماتم</p></div>
<div class="m2"><p>شرع خصم و ندیم نامحرم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>کوی پر دزد و زو بعست و پری</p></div>
<div class="m2"><p>تو همی کوک و کو کنار خوری</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>حزم خود کن که دزدت از خانه است</p></div>
<div class="m2"><p>خازنت خاینست و بیگانه است</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>تا کی از خویشتن کمی بودن</p></div>
<div class="m2"><p>دلت نگرفت ز آدمی بودن</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>اندرین سور پر ز شور و شغب</p></div>
<div class="m2"><p>دل پر از غم نشین و مُهر به لب</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>باده خوردی ولیک باهی نه</p></div>
<div class="m2"><p>دوغ خوردی ولیک با کینه</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چون شدی مست هستی ای ساده</p></div>
<div class="m2"><p>خیک باده چو خاک افتاده</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چکنی باده کاندرین فرسنگ</p></div>
<div class="m2"><p>بار شیشه است و ره یخ و خر لنگ</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>خرِ لنگ ضعیف و بار گران</p></div>
<div class="m2"><p>منزلت سنگلاخ و تو حیران</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>راه تاری چراغ بی‌روغن</p></div>
<div class="m2"><p>باد صرصر تو بادخانه شکن</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>سر بی‌مغز و پای محکم نی</p></div>
<div class="m2"><p>مال همدست و یار محرم نی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>خوابگه ساخته ز شاخ درخت</p></div>
<div class="m2"><p>تا نهاده قدم به جایی سخت</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>تا ترا اندرین سفر ز گزاف</p></div>
<div class="m2"><p>باشد اندر خیال خانهٔ لاف</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>شب سر خواب و روز عزم شراب</p></div>
<div class="m2"><p>چه کند جز که دین و ملک خراب</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>تو به می شاد و آدم اندر بند</p></div>
<div class="m2"><p>اینت بد مهر و ناخلف فرزند</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>گرچه شادی نمای اوّل اوست</p></div>
<div class="m2"><p>کیسهٔ گم شده معوّل اوست</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>کو بدین خاکدان و ویران دِه</p></div>
<div class="m2"><p>کیسه لاغر کنست و تن فربه</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>داند آن کو گشاده رگ باشد</p></div>
<div class="m2"><p>که میان بسته تیزتگ باشد</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>مرد فربه چو پنج گام برفت</p></div>
<div class="m2"><p>هفت عضوش ز چار طبع نهفت</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>تو به چُستی حساب از او برگیر</p></div>
<div class="m2"><p>چون بپوشند جامهٔ شبگیر</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>که گریبان چو دامن و تیریز</p></div>
<div class="m2"><p>عطسه و خوی گرفت و سرفه و تیز</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>او سرت را گرفته زیر دو پای</p></div>
<div class="m2"><p>تو ز جان ساخته تنش را جای</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>تو بدو دین و بخردی داده</p></div>
<div class="m2"><p>او به تو دیوی و ددی داده</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>تو ازو آن خوری که پستی تست</p></div>
<div class="m2"><p>و او ز تو آن بَرد که هستی تست</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>عمر دادی به باد از پی می</p></div>
<div class="m2"><p>غافلی زین شمار عزّ علی</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>به نشاط و سماع مشغولی</p></div>
<div class="m2"><p>وز سرای بقا تو معزولی</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>فارغ از مرگ و ایمن از گوری</p></div>
<div class="m2"><p>من چه گویم ترا به دل کوری</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>چنگ در دنیی زبون زده‌ای</p></div>
<div class="m2"><p>دل پاکیزه را به خون زده‌ای</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>حبه‌ای نزد تست کوه اُحد</p></div>
<div class="m2"><p>سیم باید که باشدت لابد</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>ور نباشد خدا و دین باشد</p></div>
<div class="m2"><p>دیو دنیی چنینت فرماید</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>هیچ خصمت بتر ز دنیا نیست</p></div>
<div class="m2"><p>با که گویم که چشم بینا نیست</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>گل به نزد تو زان فراز آمد</p></div>
<div class="m2"><p>که گلو را به گل نیاز آمد</p></div></div>