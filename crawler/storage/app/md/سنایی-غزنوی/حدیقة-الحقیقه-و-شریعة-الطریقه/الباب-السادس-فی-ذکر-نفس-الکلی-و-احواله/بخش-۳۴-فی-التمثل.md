---
title: >-
    بخش ۳۴ - فی التّمثّل
---
# بخش ۳۴ - فی التّمثّل

<div class="b" id="bn1"><div class="m1"><p>در طمع زین سگان مزبله پوی</p></div>
<div class="m2"><p>ای کم از گربه دست و روی بشوی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گربه هم روی شوی و هم دزدست</p></div>
<div class="m2"><p>لاجرم زان سرای بی‌مزدست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>موش را موی هست چون سنجاب</p></div>
<div class="m2"><p>لیک پاکی نیاید از دریاب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نپذیرد دباغت ار چه نکوست</p></div>
<div class="m2"><p>نشود پاک همچو دیگر پوست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نای و چنگی که گربکان دارند</p></div>
<div class="m2"><p>موش را خود به رقص نگذارند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بی‌رسن دزد خانه‌کن باشد</p></div>
<div class="m2"><p>مور هم دزد و هم رسن باشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا تن تست چون دلِ کفتار</p></div>
<div class="m2"><p>لت و لوتش یکیست در گفتار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مانده در پیش این و آن به فسوس</p></div>
<div class="m2"><p>خایه کن نه و خانه کن چو خروس</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون به شهر آن کسان که خرسندند</p></div>
<div class="m2"><p>کمر از بهر خواجگی بندند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تو نصیحت مدار خوار که غمر</p></div>
<div class="m2"><p>کرد ضایع به طمع عمّان عمر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هان قناعت گزین که طامع دون</p></div>
<div class="m2"><p>در دو گیتی است با عذاب الهون</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>طالع آنکه دین به حرص فروخت</p></div>
<div class="m2"><p>در و بالش به احتراق بسوخت</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>دست بردی نیافت از پی بند</p></div>
<div class="m2"><p>پای حرص تو از قناعت بند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گر بهی خود روانت نهراسد</p></div>
<div class="m2"><p>ور بدی زاهدت بنشناسد</p></div></div>