---
title: >-
    بخش ۳۵ - ذکر اظهار حال آن سرای  فی یوم‌القیامة فلا انساب بینهم یومئذ ولایتسائلون
---
# بخش ۳۵ - ذکر اظهار حال آن سرای  فی یوم‌القیامة فلا انساب بینهم یومئذ ولایتسائلون

<div class="b" id="bn1"><div class="m1"><p>روز دین دستِ دست رس نبود</p></div>
<div class="m2"><p>نسب کس شفیع کس نبود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نقد تو چون ترا برانگیزد</p></div>
<div class="m2"><p>همه در گردن تو آویزد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بوته خود گویدت چو پالودی</p></div>
<div class="m2"><p>که زری یا مس زراندودی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر بدی آتشت بپالاید</p></div>
<div class="m2"><p>ور بدی صافی از تو آساید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون رسیدی به آتش موعود</p></div>
<div class="m2"><p>خود بگوید که چندنی یا عود</p></div></div>