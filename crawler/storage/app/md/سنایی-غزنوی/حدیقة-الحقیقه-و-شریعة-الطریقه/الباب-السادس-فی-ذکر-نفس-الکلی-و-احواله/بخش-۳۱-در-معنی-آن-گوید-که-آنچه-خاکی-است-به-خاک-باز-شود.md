---
title: >-
    بخش ۳۱ - در معنی آن گوید که آنچه خاکی است به خاک باز شود
---
# بخش ۳۱ - در معنی آن گوید که آنچه خاکی است به خاک باز شود

<div class="b" id="bn1"><div class="m1"><p>تنت از چرخ و طبع دارد ساز</p></div>
<div class="m2"><p>این و آن ساز خویش خواهد باز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جان حق داد جاودان ماند</p></div>
<div class="m2"><p>زانکه حق داده باز نستاند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>معرفت در دلت نهادهٔ اوست</p></div>
<div class="m2"><p>باز کی گیرد آنچه دادهٔ اوست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کانکه او خود سرشت خاک نکرد</p></div>
<div class="m2"><p>وانکه او خود نگاشت پاک نکرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زانکه حکمت بد اقتضا نکند</p></div>
<div class="m2"><p>هرچه حکمت کند هبا نکند</p></div></div>