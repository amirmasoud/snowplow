---
title: >-
    بخش ۱۵ - اندر طلب دنیا
---
# بخش ۱۵ - اندر طلب دنیا

<div class="b" id="bn1"><div class="m1"><p>هرکه جست از خدای خود دنیی</p></div>
<div class="m2"><p>مرحبا لیک نبودش عقبی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هردو نبود به هم یکی بگذار</p></div>
<div class="m2"><p>زان سرای نفیس دست مدار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هست بی‌قدر دنیی غدّار</p></div>
<div class="m2"><p>مر سگان راست این چنین مردار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>وانکه از کردگار عقبی خواست</p></div>
<div class="m2"><p>گر مر او را دهیم جمله رواست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زانکه گشتای خوب کاران راست</p></div>
<div class="m2"><p>جمله عقبی حلال خواران راست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>وانکه دعوی دوستی ما کرد</p></div>
<div class="m2"><p>از تن و جان او برآرم گرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هیچ اگر بنگرد سوی اغیار</p></div>
<div class="m2"><p>زنده او را برآورم بردار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دانی از بهر چیست رنج و عنا</p></div>
<div class="m2"><p>زانکه اللّٰه اَغیَرُ منّا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تن خود از دین به کام دارد مرد</p></div>
<div class="m2"><p>هرچه جز حق حرام دارد مرد</p></div></div>