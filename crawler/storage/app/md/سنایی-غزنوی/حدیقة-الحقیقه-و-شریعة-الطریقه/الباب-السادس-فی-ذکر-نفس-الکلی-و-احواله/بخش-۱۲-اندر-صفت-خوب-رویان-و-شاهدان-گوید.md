---
title: >-
    بخش ۱۲ - اندر صفت خوب رویان و شاهدان گوید
---
# بخش ۱۲ - اندر صفت خوب رویان و شاهدان گوید

<div class="b" id="bn1"><div class="m1"><p>آن نگاری که سوی او نگری</p></div>
<div class="m2"><p>او دل از تو برد تو درد بری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روی اگر هیچ بی‌نقاب کند</p></div>
<div class="m2"><p>راه پر ماه و آفتاب کند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ور کند هردو بند گیسو باز</p></div>
<div class="m2"><p>سه شب قدر برگشاید راز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دایگان زلف او چو تاب دهند</p></div>
<div class="m2"><p>چینیان نقش خود به آب دهند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دُرج دُرّش چو نطق بشکافد</p></div>
<div class="m2"><p>شرمش از گل نقابها بافد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شکن زلفش از درون سرای</p></div>
<div class="m2"><p>مشک دست آمد و جلاجل پای</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گرچه در پرده‌ها تواند شد</p></div>
<div class="m2"><p>ز ایچ عاشقان نهان نداند شد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بوی او عقل را کند سرمست</p></div>
<div class="m2"><p>روی او مرگ را کند پسِ دست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>حلقهٔ زلف او معما گوی</p></div>
<div class="m2"><p>نقش سودای او سویدا جوی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از لبش جان کور کوثر نوش</p></div>
<div class="m2"><p>وز خطش چشم عور دیبا پوش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دیو همچون ملک شد از رویش</p></div>
<div class="m2"><p>روز شب گشته زان سیه مویش</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>روی و مویش به از شب و روزست</p></div>
<div class="m2"><p>شادی افزای و مجلس افروزست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>مرد از بوی او حیات برد</p></div>
<div class="m2"><p>ماه از حسن او برات برد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چشم صورت ز رفتنش جان بین</p></div>
<div class="m2"><p>دست معنی ز دامنش گل چین</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گاه پیدا و گاه ناپیدا</p></div>
<div class="m2"><p>همچو نقطه به چشم نابینا</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>خط و خالش چو خط و عجم نُبی</p></div>
<div class="m2"><p>زیر هریک جهان جهان معنی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>روی و زلفش گر آشکارستی</p></div>
<div class="m2"><p>شب و روز این که دوست چارستی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>در تماشای آن دوتا گلنار</p></div>
<div class="m2"><p>مرد برهم فتد چو دانهٔ نار</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چشم گوشی شود چو سازد چنگ</p></div>
<div class="m2"><p>گوش چشمی شود چو آرد رنگ</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>زان خط مشک رنگ لعل فروش</p></div>
<div class="m2"><p>مردمِ دیده گشته دیباپوش</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>روز حیران شود همی ز شبش</p></div>
<div class="m2"><p>بوسه ره گم کند همی ز لبش</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>وهم عاشق سوی لبش بشتافت</p></div>
<div class="m2"><p>لب او جز به خنده باز نیافت</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بوسهٔ عاشق روان پرداز</p></div>
<div class="m2"><p>دهنش را به خنده یابد باز</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>نه ز غنچه دو دیده باز کند</p></div>
<div class="m2"><p>نه ز خنده دو لب فراز کند</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بند زلفش چو زیر تاب آمد</p></div>
<div class="m2"><p>بندِ قندیل آفتاب آمد</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>خرمن مشک توده بر توده</p></div>
<div class="m2"><p>خوشه‌چینان ازو برآسوده</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>صورت قهر و لطف خال و لبش</p></div>
<div class="m2"><p>عالم قبض و بسط روز و شبش</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>لعل او دلگشای و جان آویز</p></div>
<div class="m2"><p>جزع او لعل پاش و مرجان ریز</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>کارخانهٔ رخش بهار شکن</p></div>
<div class="m2"><p>ناردانهٔ لبش خمار شکن</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>شمع رخ چون ز شرم بفروزد</p></div>
<div class="m2"><p>آهوان را کرشمه آموزد</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>جعد او عقل و روح را خرگه</p></div>
<div class="m2"><p>چشم او چشم را تماشاگه</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>هرکجا زلف او مصاف زند</p></div>
<div class="m2"><p>زشت باشد که نافه لاف زند</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>از زمین بوی مُشک برخیزد</p></div>
<div class="m2"><p>خون عاشق چو زلف او ریزد</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>جعدش از تاب بر رخ دلخواه</p></div>
<div class="m2"><p>راست چون خال بی بسم‌اللّٰه</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>دیده زان چشمها که بردارد</p></div>
<div class="m2"><p>جز کسی کآفت بصر دارد</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>چشم کز دیدنش ندارد نور</p></div>
<div class="m2"><p>باشد از روی خوب فایده دور</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>قد او در دو دیدهٔ دلجوی</p></div>
<div class="m2"><p>همچو سرو بلند بر لب جوی</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>بتوان دید از لطیفی کوست</p></div>
<div class="m2"><p>استخوان در تنش چو خون از پوست</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>هم گهر با دهان او ارزان</p></div>
<div class="m2"><p>هم سُرین بر میان او لزران</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>جان جانست نور بر قمرش</p></div>
<div class="m2"><p>نور عقلست لعل پر شکرش</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>گر برو عنکبوتکی بتند</p></div>
<div class="m2"><p>در زمان حد زانیانش زند</p></div></div>