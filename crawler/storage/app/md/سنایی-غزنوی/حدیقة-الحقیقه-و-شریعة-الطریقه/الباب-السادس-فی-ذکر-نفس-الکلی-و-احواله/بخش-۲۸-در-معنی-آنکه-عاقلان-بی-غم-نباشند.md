---
title: >-
    بخش ۲۸ - در معنی آنکه عاقلان بی‌غم نباشند
---
# بخش ۲۸ - در معنی آنکه عاقلان بی‌غم نباشند

<div class="b" id="bn1"><div class="m1"><p>معرفت را شرفت پناهِ شماست</p></div>
<div class="m2"><p>مغفرت را علف گناه شماست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آدمی بهر بی‌غمی را نیست</p></div>
<div class="m2"><p>پای در گِل جز آدمی را نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همه مقصود آفرینش اوست</p></div>
<div class="m2"><p>اهل تکلیف و عقل و بینش اوست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عرش و فرش و زمان برای ویست</p></div>
<div class="m2"><p>وین تبه خاکدان نه جای ویست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>او در این خاک توده بیگانه است</p></div>
<div class="m2"><p>زانکه با عقل یار و همخانه است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خنده و گریه آدمی داند</p></div>
<div class="m2"><p>زانکه او رنج و بی‌غمی داند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شادی از اهل عقل بیگانه است</p></div>
<div class="m2"><p>آدمی را خود اندُه از خانه است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>غم در آنست کز تن آسانی</p></div>
<div class="m2"><p>بی‌غمی را تو غم هی خوانی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>غم تو را می‌خورد ز بی‌خطری</p></div>
<div class="m2"><p>تو چنان کس نه‌ای که غم نخوری</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چون ترا خورد و گشت فربه غم</p></div>
<div class="m2"><p>غم تو شد فزون و مردی کم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>علف غم تویی در این عالم</p></div>
<div class="m2"><p>چون تو رفتی علف نیابد غم</p></div></div>