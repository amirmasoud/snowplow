---
title: >-
    بخش ۲ - صفت کلماتی که با نفس کلّی رود و جوابها که او گوید
---
# بخش ۲ - صفت کلماتی که با نفس کلّی رود و جوابها که او گوید

<div class="b" id="bn1"><div class="m1"><p>گفتم ای ایزد سرشته ز نور</p></div>
<div class="m2"><p>وی ز عکس رخ تو دیو چو حور</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای زمان از تو عید و آدینه</p></div>
<div class="m2"><p>وی زمین از رخ تو آیینه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>صفتت برتر از نفس باشد</p></div>
<div class="m2"><p>وصف کردن ترا هوس باشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پس بدیعی به صورت و پیکر</p></div>
<div class="m2"><p>نیست در کل کَون چون تو دگر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از صفت صورت معاینه‌ای</p></div>
<div class="m2"><p>زانکه هم رویی و هم آینه‌ای</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اندر اقلیم دین تویی هموار</p></div>
<div class="m2"><p>از پی راه عذر و شکر شکار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>طوبی مایه‌بخش باغ ارم</p></div>
<div class="m2"><p>کعبهٔ پادشاه خاک حرم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بس بهی نفس و بس قوی نفسی</p></div>
<div class="m2"><p>عقل و جانی سری دلی چه کسی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>حبّذا صورتت که بس خوبی</p></div>
<div class="m2"><p>خرّما شوکتت نه معیوبی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>برتر از جوهری و از عَرَضی</p></div>
<div class="m2"><p>جملهٔ کاینات را غرضی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گوهری کز تو قابل قوتست</p></div>
<div class="m2"><p>برج خورشید و دُرج یاقوتست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>خورده‌ای شربها ز دست ملک</p></div>
<div class="m2"><p>همچو پیغمبران هنیئاً لک</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چه کنی پیش مُدبری پر درد</p></div>
<div class="m2"><p>در چنین کنج گنج بادآورد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>کلبه‌ای همچو دیو درگه دود</p></div>
<div class="m2"><p>کردی از عکس روی نور اندود</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>من سهایی ندیده‌ام در چاه</p></div>
<div class="m2"><p>با دو خورشیدم این زمان و دو ماه</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بلی اندر سرای جسمانی</p></div>
<div class="m2"><p>تو ز من این حدیث به دانی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>این بود خلق و فعل پیران را</p></div>
<div class="m2"><p>که امیران کنند اسیران را</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>این چه جای چو تو جهان‌بین است</p></div>
<div class="m2"><p>گفت خود جایم از جهان این است</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>که عمارت سرای رنج بُوَد</p></div>
<div class="m2"><p>در خرابی مقام گنج بُوَد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>جای گنج است موضع ویران</p></div>
<div class="m2"><p>سگ بود سگ به جای آبادان</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>عرش و فرشت سرای و بارگه است</p></div>
<div class="m2"><p>آفرینش ترا چه کارگه است</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>تیرگی با عمارتست انباز</p></div>
<div class="m2"><p>نور گرد خراب گردد باز</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>نبود زین سرای رنج و تعب</p></div>
<div class="m2"><p>ماه و خورشید جز خراب طلب</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>که به خانهٔ درست درنایند</p></div>
<div class="m2"><p>رخنه یابند و روی بنمایند</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>زیرک از زخم دهر خسته بهست</p></div>
<div class="m2"><p>پوست پر مغز خود شکسته بهست</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>دل زیرک میان لوز بود</p></div>
<div class="m2"><p>دل نادان چو پوست گوز بود</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>مغز تا نازکست پوست نکوست</p></div>
<div class="m2"><p>چون قوی شد حجاب گردد پوست</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>سنگ باید چو مرد کاهل شد</p></div>
<div class="m2"><p>مغز نغزت ز سنگ حاصل شد</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>گفتم ای جان پر از نکویی تو</p></div>
<div class="m2"><p>از کجایی مرا نگویی تو</p></div></div>