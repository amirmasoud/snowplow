---
title: >-
    بخش ۲۱ - حکایت
---
# بخش ۲۱ - حکایت

<div class="b" id="bn1"><div class="m1"><p>گفت مردی ز ابلهی رازی</p></div>
<div class="m2"><p>با یکی بدفعال غمّازی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مرد غمّاز پیش هر اوباش</p></div>
<div class="m2"><p>راز آن مرد کرد یکسر فاش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>طیره گشت ابله از چنان غمّاز</p></div>
<div class="m2"><p>گفت با مرد کای بدِ بدساز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رازِ من فاش کردی ای نادان</p></div>
<div class="m2"><p>همچو پرخاش پتک بر سندان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دل من کرد قصد پاداشن</p></div>
<div class="m2"><p>افگنم در سرای تو شیون</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نوحه دانم یکی به شست درم</p></div>
<div class="m2"><p>وآنِ هفتاد نیز دانم هم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ضایع این رنج را بنگذارم</p></div>
<div class="m2"><p>حق سعیت بوجه بگزارم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بی‌سبب مر مرا بیازردی</p></div>
<div class="m2"><p>آنچه ناکردنی بُوَد کردی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به مکافات آن شوم مشغول</p></div>
<div class="m2"><p>تا که از سر برون کنی تو فضول</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>رفت ناگه برو و زخمی زد</p></div>
<div class="m2"><p>مرد غمّاز گشت کارش بد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مرد غمّاز کشته شد ناگاه</p></div>
<div class="m2"><p>کار ابله ز خشم گشت تباه</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>پادشه مر ورا سبک بگرفت</p></div>
<div class="m2"><p>عوض وی بکشت اینت شگفت</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بی‌سبب خیره کشته گشت دو مرد</p></div>
<div class="m2"><p>زانکه ناکردنی به جهل بکرد</p></div></div>