---
title: >-
    بخش ۴۷ - التمثّل فی ترک الدّنیا و قصّة روح‌اللّٰه و تجریده
---
# بخش ۴۷ - التمثّل فی ترک الدّنیا و قصّة روح‌اللّٰه و تجریده

<div class="b" id="bn1"><div class="m1"><p>روح را چون ببرد روح امین</p></div>
<div class="m2"><p>چرخِ چارم فزود ازو تزیین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>داد مر جبرئیل را فرمان</p></div>
<div class="m2"><p>خالق و کردگار هر دو جهان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>که بجویید مر ورا همه جای</p></div>
<div class="m2"><p>تا چه دارد ز نعمت دنیای</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون بجُستند سوزنی دیدند</p></div>
<div class="m2"><p>بر زِه دلق او بپرسیدند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کز پی چیست با تو این سوزن</p></div>
<div class="m2"><p>گفت کز بهر ستر عورت من</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>که به خُلقان ز زینت خَلقان</p></div>
<div class="m2"><p>قانعم ورچه نیستم خاقان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا بُوَد زنده ژنده پیراهن</p></div>
<div class="m2"><p>هست محتاج رشته و سوزن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جمله گفتند خالق مایی</p></div>
<div class="m2"><p>بر همه حالها تو دانایی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بر زِه دلق سوزنی است ورا</p></div>
<div class="m2"><p>نیست زین بیش چیزی از دنیا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ندی آمد بدو ز ربّ رئوف</p></div>
<div class="m2"><p>که کنیدش در آن مکان موقوف</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بوی دنیی همی دمَد زین تن</p></div>
<div class="m2"><p>چرخ چارم بُوَد ورا مسکن</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گرنه این سوزنش بُدی همراه</p></div>
<div class="m2"><p>برسیدی به زیر عرش اِله</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>سوزنی روح را چو مانع گشت</p></div>
<div class="m2"><p>به مکانی شریف قانع گشت</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>باز ماند از مکان قرب و جلال</p></div>
<div class="m2"><p>سوزنی گشت روح را به وبال</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ای جوانمرد پند من بپذیر</p></div>
<div class="m2"><p>دل ز دنیا و زینتش برگیر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>تا مرّفه بدان سرای رسی</p></div>
<div class="m2"><p>به سرور و عز و بهای رسی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ورنه با خاک راه گردی راست</p></div>
<div class="m2"><p>راه عقبی ز راه هزل جداست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>زهر قاتل شناس دنیی را</p></div>
<div class="m2"><p>رَو تو پازهر ساز عقبی را</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>زانکه دنیی‌پرست بر خیره</p></div>
<div class="m2"><p>هست چون بت‌پرست دل تیره</p></div></div>