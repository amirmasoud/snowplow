---
title: >-
    بخش ۵ - اندر عذر انبساط گوید
---
# بخش ۵ - اندر عذر انبساط گوید

<div class="b" id="bn1"><div class="m1"><p>چون خرد در لبت به جان نگرم</p></div>
<div class="m2"><p>چون قلم بر خطت به جان گذرم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آینهٔ روشنی به دست خرد</p></div>
<div class="m2"><p>کس در آن روی دم نیارد زد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پیش تو چون سنان کمر بندم</p></div>
<div class="m2"><p>خون همی گریم و همی خندم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همچو چنگ ار درِ هوات زنم</p></div>
<div class="m2"><p>رسن اندر گلو نوات زنم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خواجه آگه که راز مطلق گفت</p></div>
<div class="m2"><p>رسن اندر گلو اناالحق گفت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کانکه از بیم نفس بگریزد</p></div>
<div class="m2"><p>عشق با خونِ دل درآمیزد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>حرز خواجه پس از فراق تنش</p></div>
<div class="m2"><p>وصل حق بود جملهٔ سخنش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پشت را روی باش و خیره مجه</p></div>
<div class="m2"><p>بر سرِ دار دست و پای منه</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زانکه در کلمن رموز ازل</p></div>
<div class="m2"><p>خاصه آنگه که جان شنید غزل</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>حق چو مر خواجه را پدید آمد</p></div>
<div class="m2"><p>پرهٔ رمز را کلید آمد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از نخست آوریده این پیغام</p></div>
<div class="m2"><p>به پسین آفریده این خود کام</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>باز پس مانده‌ای ز پیش اجل</p></div>
<div class="m2"><p>پس و پیشت گرفته حرص و امل</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>از پی نان و آب ماندی باز</p></div>
<div class="m2"><p>در حجاب نیاز تن چو پیاز</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چه افگنی تخم حرص و آز و نیاز</p></div>
<div class="m2"><p>در گِل دل که آز نارد ناز</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>کاندرین خرسرای پویی تو</p></div>
<div class="m2"><p>به چه مانی مرا نگویی تو</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گر به آب و به نان بماندی باز</p></div>
<div class="m2"><p>چکنی تخم خشم و شهوت و آز</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>کانچه شوری ز نخ کند محلوج</p></div>
<div class="m2"><p>وآنچه تری ترا کند مفلوج</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>تو بپرهیز از این غرور فلک</p></div>
<div class="m2"><p>که نداری سرِ مرور فلک</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>کاندرین حجره بر تن و دل و جان</p></div>
<div class="m2"><p>آب از آن گشت بر خرد تاوان</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>کنجدی گر دهد ترا گردون</p></div>
<div class="m2"><p>دبه‌ای بنددت سبک بر کون</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>که تواند که دانهٔ کنجد</p></div>
<div class="m2"><p>در دبهٔ روغنی دو من گنجد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>جان و دینت به قهر بستاند</p></div>
<div class="m2"><p>گر ترا دل به خویشتن خواند</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>نیست بی‌رنج راحتِ دنیا</p></div>
<div class="m2"><p>خنک آنکس که کرد هر دو رها</p></div></div>