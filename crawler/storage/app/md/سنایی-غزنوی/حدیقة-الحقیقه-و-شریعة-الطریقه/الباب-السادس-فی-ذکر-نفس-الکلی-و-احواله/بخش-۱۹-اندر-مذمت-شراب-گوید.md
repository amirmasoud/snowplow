---
title: >-
    بخش ۱۹ - اندر مذمّت شراب گوید
---
# بخش ۱۹ - اندر مذمّت شراب گوید

<div class="b" id="bn1"><div class="m1"><p>مرد دینی شراب تا چکند</p></div>
<div class="m2"><p>بط چینی سراب تا چکند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چیست حاصل سوی شراب شدن</p></div>
<div class="m2"><p>اوّلش شرّ و آخرش آب شدن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کرده‌ای تو به خاک کوی گرو</p></div>
<div class="m2"><p>نرخ عمرش چو باد خویش دو جو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تو بدان آب دل مگردان خوش</p></div>
<div class="m2"><p>کو از آن آب رفت در آتش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همچو فرعون شوم گردن کش</p></div>
<div class="m2"><p>کز ره آب رفت در آتش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>وآتشی کان بودت لونالون</p></div>
<div class="m2"><p>تکیه بر آب روی چون فرعون</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گرچه بر روی قلزم از رشتی</p></div>
<div class="m2"><p>بر سر بحر می‌رود کشتی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مثل خمر خوارهٔ پیوست</p></div>
<div class="m2"><p>نزد عاقل کزین میانه بجَست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هست چون حقّه‌باز بی‌آزار</p></div>
<div class="m2"><p>کرده هنگامه بر سرِ بازار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در دل از سرِّ او سروری نه</p></div>
<div class="m2"><p>هرچه او داد جز غروری نه</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چون کند عربده ولی شکنست</p></div>
<div class="m2"><p>ور سخاوت کند دروغ زنست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مست کو را دو خوش سخن باشد</p></div>
<div class="m2"><p>نور صبح دروغ‌زن باشد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>مست چون صبح کاذبست به فعل</p></div>
<div class="m2"><p>روز و شب همچو جاذبست به فعل</p></div></div>