---
title: >-
    بخش ۷ - حکایة فی‌العجز والسکوت
---
# بخش ۷ - حکایة فی‌العجز والسکوت

<div class="b" id="bn1"><div class="m1"><p>شبلی از پیر روزگار جُنید</p></div>
<div class="m2"><p>کرد نیکو سؤالی از پی صید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفت پیرا نهادِ جمله علوم</p></div>
<div class="m2"><p>مر مرا کن درین زمان معلوم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا بدانم که راه عقبی چیست</p></div>
<div class="m2"><p>مرد این راه زین خلایق کیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفت برگیر خواجه زود قلم</p></div>
<div class="m2"><p>تا بگویم ترا ز سرّ قدم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شبلی اندر زمان قلم برداشت</p></div>
<div class="m2"><p>وانچه او گفت یک به یک بنگاشت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفت بنویس ازین قلم اللّٰه</p></div>
<div class="m2"><p>چونکه بنوشت شد سخن کوتاه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گفت دیگر چه پیر گفت جز این</p></div>
<div class="m2"><p>خود همین است کردمت تلقین</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>علمها جمله زیر این کلمه‌ست</p></div>
<div class="m2"><p>هست صورت یکی ولیک همه‌ست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>علم جمله جهان جزین مشناس</p></div>
<div class="m2"><p>بشنو فرق فربهی ز اماس</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>این بدان و ز قیل و قال گریز</p></div>
<div class="m2"><p>جمله این است و زان دگر پرهیز</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>رهروانی که چشم سرّ دارند</p></div>
<div class="m2"><p>دیده بر پشتِ راهبر دارند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>روی در خلق مقتدا نه رواست</p></div>
<div class="m2"><p>که نه راه خدای راه هواست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>تو بدو داده‌ای و او به تو روی</p></div>
<div class="m2"><p>هردو همره چو حلقه‌ها در موی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>به هوا او ترا تو او را دوست</p></div>
<div class="m2"><p>بت‌پرستی تو بت‌پرستی اوست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>آنکه هرگز نبود با خود یار</p></div>
<div class="m2"><p>اوست از رنج علم برخوردار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>نیک و بد میل تو نه از خوابست</p></div>
<div class="m2"><p>بد و نیک تو همچو جلّابست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>کی دهد مر بخار را تسکین</p></div>
<div class="m2"><p>کاتش اندر دلست ای مسکین</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>آتش دل ز حکمت چپ و راست</p></div>
<div class="m2"><p>نشود جز به بادبیزن کاست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>دل تهی کن ز آتش پنداشت</p></div>
<div class="m2"><p>که کفی خاک باد و آب نداشت</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ساخته راه را همه اسباب</p></div>
<div class="m2"><p>سوی منزل رسیده در تک و تاب</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بی‌رفیق این چنین ره هایل</p></div>
<div class="m2"><p>رفته و کرده جسم را بسمل</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>همه در باخته ز خود الوان</p></div>
<div class="m2"><p>نفس رفته بمانده جان و روان</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>کرده این نفسها بجمله فدی</p></div>
<div class="m2"><p>ساخته از قالب و نفوس غدی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>روح صافی بمانده تن رفته</p></div>
<div class="m2"><p>صدق مانده به جای و فن رفته</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>معنی کار را جُهینه شده</p></div>
<div class="m2"><p>عین ارواح را بُثینه شده</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>چون شدم فارغ از طریق جواز</p></div>
<div class="m2"><p>عشق را زین سپس کنم آغاز</p></div></div>