---
title: >-
    بخش ۲ - التمثیل فی وضع الشی بغیر موضعه
---
# بخش ۲ - التمثیل فی وضع الشی بغیر موضعه

<div class="b" id="bn1"><div class="m1"><p>آن شنیدی که ابلهی برخاست</p></div>
<div class="m2"><p>سرگذشت از مخنثی درخواست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که بگو سرگذشتی ای بهمان</p></div>
<div class="m2"><p>گفت رَو رَو مَزَح مکن هله هان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کسی از حیز سرگذشت نجست</p></div>
<div class="m2"><p>حیز را کون گذشت باید گفت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گوش سوی همه سخنها دار</p></div>
<div class="m2"><p>هرچه زان به درون جان بنگار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هرچه مایه صفا بدان ده روی</p></div>
<div class="m2"><p>هرچه مایه کدر گذر کن زوی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حجّت ایزدست در گردن</p></div>
<div class="m2"><p>خواندن علم و کار ناکردن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کرده‌ای همچو گوز بُن گردن</p></div>
<div class="m2"><p>از چه از عشوه و قفا خوردن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مخر آن عشوه کاندرین بنیاد</p></div>
<div class="m2"><p>عشوه تن پر کند ولیک از باد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مشک پر بادی از سر و دل و تن</p></div>
<div class="m2"><p>ریسمانی شوی به یک سوزن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در جهان خراب بی‌فریاد</p></div>
<div class="m2"><p>کس گرفتار بادِ عشوه مباد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>قُبله اول ز قِبله باز شناس</p></div>
<div class="m2"><p>تا بدانی تو فربهی ز اماس</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چند ازین در نفاق و محتالی</p></div>
<div class="m2"><p>چشمها درد و لاف کحّالی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>هرکه مغرور بانگ غولانست</p></div>
<div class="m2"><p>اجلش زیر ام‌غیلانست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>علمت از جان و مالت از تن تست</p></div>
<div class="m2"><p>آن دو معشوقه این دو دشمن تست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>پاک شو تا ز اهل دین گردی</p></div>
<div class="m2"><p>آن چنان باش تا چنین گردی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>رهروان را ز نطق نبود ساز</p></div>
<div class="m2"><p>پیل فربه بود ضعیف آواز</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>علمدان کدخدای دو جهانست</p></div>
<div class="m2"><p>وآنکه نادان حقیر و حیرانست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>حکما بار جمله بربستند</p></div>
<div class="m2"><p>همه رفتند و زین هوس رستند</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>تو گل و دین درین جهان بستی</p></div>
<div class="m2"><p>ای نه هشیار چون چنین مستی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>علم دان خاصهٔ خدا آمد</p></div>
<div class="m2"><p>علم خوان شوخ و نر گدا آمد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بهر دین با سفیه رای مزن</p></div>
<div class="m2"><p>رگ قیفال بهر پای مزن</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بد ز نیکان سلامتی نشود</p></div>
<div class="m2"><p>که ز بیجاده قیمتی نشود</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>در دونی برای زر نزنند</p></div>
<div class="m2"><p>باسلیق از برای سر نزنند</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>آنکه را علّتی بود در پشت</p></div>
<div class="m2"><p>چون بنالد ز پنجه و انگشت</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چون تو بر سر نهی ورا مرهم</p></div>
<div class="m2"><p>نفزاید ز مرهمش مرهم</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>آن حکیمان که روی بنمایند</p></div>
<div class="m2"><p>بر گل و بر دلت نبخشایند</p></div></div>