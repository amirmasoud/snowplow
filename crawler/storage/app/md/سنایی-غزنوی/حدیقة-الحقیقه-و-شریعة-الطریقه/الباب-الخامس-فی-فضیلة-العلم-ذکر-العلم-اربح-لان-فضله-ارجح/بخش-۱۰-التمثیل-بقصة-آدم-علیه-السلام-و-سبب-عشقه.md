---
title: >-
    بخش ۱۰ - التمثیل بقصّة آدم علیه‌السلام و سبب عشقه
---
# بخش ۱۰ - التمثیل بقصّة آدم علیه‌السلام و سبب عشقه

<div class="b" id="bn1"><div class="m1"><p>دل خریدار نیست جز غم را</p></div>
<div class="m2"><p>آن بنشنیده ای که آدم را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عزِّ علمش سوی جنان آورد</p></div>
<div class="m2"><p>دل عشقش به خاکدان آورد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون ره علم رفت سلطان شد</p></div>
<div class="m2"><p>چون ره دل گرفت عریان شد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون همه لطفها بدید از حق</p></div>
<div class="m2"><p>عشق جانش ندا شنید از حق</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای که ذاتت چو عقل فرزانه‌ست</p></div>
<div class="m2"><p>عشق مگذار کو هم از خانه‌ست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زیرکی دیو و عاشقی آدم</p></div>
<div class="m2"><p>این بمان تا بدان رسی دردم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عشق در پیش گیر و دل بگذار</p></div>
<div class="m2"><p>که ز دل خیره بر نیاید کار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مرد را عشق تاجِ سر باشد</p></div>
<div class="m2"><p>عشق بهتر ز هر هنر باشد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>عاشقی بستهٔ خرد نبود</p></div>
<div class="m2"><p>علّت عشق نیک و بد نبود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آدم از عشق اهبِطوُا منها</p></div>
<div class="m2"><p>آمد اندر جهان جان تنها</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>عقل عزم اِحاطت وی کرد</p></div>
<div class="m2"><p>غیرت عشق پای او پی کرد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>برگزیده دو مرغ بهر دو کار</p></div>
<div class="m2"><p>عقل طوطی و عشق بوتیمار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>قدم عقل نقدِ حالی جوی</p></div>
<div class="m2"><p>شعلهٔ عشق لاابالی گوی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>باشهٔ عقل صعوه‌گیر بُوَد</p></div>
<div class="m2"><p>کرکس عشق بازگیر بُوَد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>در ره عشق ما همه طفلیم</p></div>
<div class="m2"><p>عاشقان صافیند و ما ثفلیم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بالغ عقلها بسی یابی</p></div>
<div class="m2"><p>بالغ عشق کم کسی یابی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>در جهانی که عشق گوید راز</p></div>
<div class="m2"><p>عقل باشد در آن جهان غمّاز</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>تا تو به مانده‌ای و عقل توباز</p></div>
<div class="m2"><p>تو چو کبکی و عشق همچون باز</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>حق پژوهان که راه دل سپرند</p></div>
<div class="m2"><p>عقل را لاشهٔ دبر شمرند</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>محدث از خلقت قدم که بُوَد</p></div>
<div class="m2"><p>روز کور از سپیده‌دم که بُوَد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>عشق را جان بلعجب داند</p></div>
<div class="m2"><p>زانکه تفسیر شهد لب داند</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>صورت عشق پوست باشد پوست</p></div>
<div class="m2"><p>عشق بی عین و شین و قاف نکوست</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>در ره عاشقی سلامت نیست</p></div>
<div class="m2"><p>اضطرابست و استقامت نیست</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>صفتِ عاشقان ز من بشنو</p></div>
<div class="m2"><p>ور نداری مرا برو به دو جو</p></div></div>