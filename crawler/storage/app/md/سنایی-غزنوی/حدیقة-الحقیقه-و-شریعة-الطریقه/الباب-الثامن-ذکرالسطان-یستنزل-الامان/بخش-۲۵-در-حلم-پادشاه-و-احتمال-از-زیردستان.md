---
title: >-
    بخش ۲۵ - در حلم پادشاه و احتمال از زیردستان
---
# بخش ۲۵ - در حلم پادشاه و احتمال از زیردستان

<div class="b" id="bn1"><div class="m1"><p>بشنو تا ابوحنیفه چه گفت</p></div>
<div class="m2"><p>صفّهٔ عقل خویش را چون رُفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که سفیهی چو داد دشنامش</p></div>
<div class="m2"><p>گشت خامش ز گفتن خامش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفت از این ژاژ او چه آزارم</p></div>
<div class="m2"><p>آنچه او گفت بیش بنگارم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر چنانم بشویم آن از خود</p></div>
<div class="m2"><p>ورنه‌ام با بدی چه گویم بَد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زو بهم چونکه عیب خود جویم</p></div>
<div class="m2"><p>ورنه چه او چه من که بد گویم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مرد دین‌دار همچنین باشد</p></div>
<div class="m2"><p>کز برون وز درونش دین باشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نه خرد جُستن مراد خودست</p></div>
<div class="m2"><p>از دو بد به گزین کنی خردست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گرچه با خام طبع تو نپزد</p></div>
<div class="m2"><p>تو چنان زی برو که از تو سزد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر کسی عیب تو کند بشنو</p></div>
<div class="m2"><p>وآنچه عیبست جملگی بدرو</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>باغ دل را تو از بدی کن پاک</p></div>
<div class="m2"><p>تا برآید نهال تو چالاک</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گر کند عیب از دو بیرون نیست</p></div>
<div class="m2"><p>یا بُوَد یا نه بر دو رای مایست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گر بود عیب آن ز خود بدرَو</p></div>
<div class="m2"><p>ور نباشدت آن سخن به دو جو</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گر تو معیوبی آن بشو از هوش</p></div>
<div class="m2"><p>ور نه‌ای ژاژ او میار به گوش</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>خلق اگر در تو خست ناگه خار</p></div>
<div class="m2"><p>تو گل خویش ازو دریغ مدار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>آنکه دشنام دادت از سرِ خشم</p></div>
<div class="m2"><p>خاک پایش گزین چو سرمهٔ چشم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>وانکه بد گفت نیکویی گویش</p></div>
<div class="m2"><p>ور نجوید ترا تو می‌جویش</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>آنکه زَهرت دهد بدو ده قند</p></div>
<div class="m2"><p>وانکه از تو بُرد درو پیوند</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>وانکه سیمت نداد زر بخشش</p></div>
<div class="m2"><p>وانکه پایت برید سر بخشش</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>همه را در محلّ خویش بدار</p></div>
<div class="m2"><p>هیچ‌کس را ز خوی بد مازار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>تا بوی در کنار وصل و فراق</p></div>
<div class="m2"><p>دفتری از مکارم الاخلاق</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>هست در دین و ملک ظلم و محال</p></div>
<div class="m2"><p>همچو در جسم و جان و با و وبال</p></div></div>