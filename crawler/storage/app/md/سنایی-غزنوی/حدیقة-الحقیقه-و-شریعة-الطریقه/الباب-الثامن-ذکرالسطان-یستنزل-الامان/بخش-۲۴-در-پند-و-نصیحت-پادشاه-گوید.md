---
title: >-
    بخش ۲۴ - در پند و نصیحت پادشاه گوید
---
# بخش ۲۴ - در پند و نصیحت پادشاه گوید

<div class="b" id="bn1"><div class="m1"><p>همه خلق آنچه ماده وانچه نرند</p></div>
<div class="m2"><p>از درون خازنان یکدگرند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر دهی نیک نیک پیش آرند</p></div>
<div class="m2"><p>ور کنی بد بدی نگهدارند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زانکه از کوزه بهر عادت و خو</p></div>
<div class="m2"><p>بترابد گلاب و سرکه درو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خویشتن را همی نکو خواهی</p></div>
<div class="m2"><p>وز بد دیگران نه آگاهی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تو که از کرمکی بیازاری</p></div>
<div class="m2"><p>چه کنی با دگر کسی ماری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>صبر کن بر سفاهت جاهل</p></div>
<div class="m2"><p>تا شوی سایس ولایت دل</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پند عاقل به آخر کارت</p></div>
<div class="m2"><p>کند ار کند تیز بازارت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هست پندت نگاه دارنده</p></div>
<div class="m2"><p>همچو می ناخوش و گوارنده</p></div></div>