---
title: >-
    بخش ۱۹ - در کفایت و رای پادشاهی
---
# بخش ۱۹ - در کفایت و رای پادشاهی

<div class="b" id="bn1"><div class="m1"><p>شاه شاهان یمینِ دین محمود</p></div>
<div class="m2"><p>که جهان را به عدل بُد مقصود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شاه غازی یمین دین خدای</p></div>
<div class="m2"><p>که بُد او در زمانه بار خدای</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یافته دین احمد تازی</p></div>
<div class="m2"><p>سرفرازی بدان شه غازی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>روزی اندر دلش فتاد هوس</p></div>
<div class="m2"><p>که سوی رومیان فرستد کس</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ملک روم را کند آگاه</p></div>
<div class="m2"><p>که منم بر زمانه شاهنشاه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفت بر درگهم کدام کس است</p></div>
<div class="m2"><p>کهمر این کار را به علم بس است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اختیار اوفتادش از فضلا</p></div>
<div class="m2"><p>خواجه بوبکر سیّدالندما</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آن به هر علم حیدر ثانی</p></div>
<div class="m2"><p>آنکه خوانی ورا قهستانی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کرد حاضر ورا و حال بگفت</p></div>
<div class="m2"><p>راز خود زان نکو سیر ننهفت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گفت خواهم که سوی روم شوی</p></div>
<div class="m2"><p>برِ آن خیره رای شوم شوی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بگزاری ز من یکی پیغام</p></div>
<div class="m2"><p>برسانی به شرط خویش سلام</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>پس بگویی که حمل ما بفرست</p></div>
<div class="m2"><p>زر و دیبا و دُر بدین فهرست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ورنه جنگ ترا بسیچم زود</p></div>
<div class="m2"><p>از تو و ملک تو برآرم دود</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گفت بوبکر بنده فرمانم</p></div>
<div class="m2"><p>باد برخی جان تو جانم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گفتنی گفته شد بدو یکسر</p></div>
<div class="m2"><p>همه پیغامها ز خیر و ز شر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>کس فرستاد پس شبی سلطان</p></div>
<div class="m2"><p>که برو خواجه را برِ من خوان</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>کرد حاضر ورا و پیش نشاند</p></div>
<div class="m2"><p>سخن از هر نمط برش می‌راند</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>پس بگفتش که گر در آن محفل</p></div>
<div class="m2"><p>رومیان آورند با تو جدل</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>گوید ای مرد تا کی این هذیان</p></div>
<div class="m2"><p>شرم ناید ترا ز شاه جهان</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>در چنین بارگاه وین دیهیم</p></div>
<div class="m2"><p>ظالمی را همی نهی تعظیم</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بنده زادی خود آن محل دارد</p></div>
<div class="m2"><p>که ز وی شاه ما خلل دارد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ظالمی خیره رای هر جایی</p></div>
<div class="m2"><p>چون ورا پیش شاه بستایی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>پیش این تخت با بزرگی جفت</p></div>
<div class="m2"><p>سخن ظالمان نباید گفت</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>تو چه گویی جواب این گفتار</p></div>
<div class="m2"><p>از سرِ لطف نز سرِ پیکار</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>خواجه بوبکر گفت سلطان را</p></div>
<div class="m2"><p>کای به حق سایه گشته یزدان را</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>این سخن گر بُدی ز خصم بی‌آب</p></div>
<div class="m2"><p>دادمی گفته را به شرط جواب</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>لیکن اکنون سخن تو آرایی</p></div>
<div class="m2"><p>هم تو این را جواب فرمایی</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>گفت سلطان که گر رود این حال</p></div>
<div class="m2"><p>تو بده مر ورا جواب سؤال</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>که چنین است و حق به دست شماست</p></div>
<div class="m2"><p>لیکن این از جواب گردد راست</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بنده زاده است و ظالمست بلی</p></div>
<div class="m2"><p>نیست با تو مرا بدین جدلی</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>لیکن اندر ممالک این مرد</p></div>
<div class="m2"><p>ظلم جز وی کسی نیارد کرد</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>کس ندارد به ملک او زَهره</p></div>
<div class="m2"><p>که فزون‌تر خورد وی از بهره</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>جز ازو ظلم کایناً من کان</p></div>
<div class="m2"><p>نرود هیچ آشکار و نهان</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>ز اتفاق این سخن برفت به روم</p></div>
<div class="m2"><p>خواجه گفت این سخن بُوَد معلوم</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>هم بر آن سان جواب ایشان داد</p></div>
<div class="m2"><p>صد در از رنج بر ملک بگشاد</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>چون سخن جملگی مکرّر گشت</p></div>
<div class="m2"><p>رومیان را بیان مقرّر گشت</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>چون شنید این سخن عظیم‌الرّوم</p></div>
<div class="m2"><p>کرد دستور خویش را معلوم</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>کین سخن باز هم از آن نمطست</p></div>
<div class="m2"><p>نه چو دیگر سخن حدیث بطست</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>شد خجل زان جواب و گشت خموش</p></div>
<div class="m2"><p>گشت در گوش او چو حلقهٔ گوش</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>شاه باید که وقت خلوت و بار</p></div>
<div class="m2"><p>در همه کارها بود بیدار</p></div></div>