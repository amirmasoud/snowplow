---
title: >-
    بخش ۴۶ - در مدح اقضی‌القضاة جمال‌الدیّن ابوالقاسم محمودبن محمّدِالاثیری
---
# بخش ۴۶ - در مدح اقضی‌القضاة جمال‌الدیّن ابوالقاسم محمودبن محمّدِالاثیری

<div class="b" id="bn1"><div class="m1"><p>چون از این طایفه گذر کردی</p></div>
<div class="m2"><p>به دگر طایفه نظر کردی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عالم عدل بینی و انصاف</p></div>
<div class="m2"><p>همه معنی محض و دور از لاف</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پیشوای امم مرّفه جمع</p></div>
<div class="m2"><p>نور اقضی القضاة تابان شمع</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مفتی اصل و فرع و وارث جود</p></div>
<div class="m2"><p>شمع شرع محمّدی محمود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آنکه در صدر شرع تا بنشست</p></div>
<div class="m2"><p>پای فتنه دو دست ظلم ببست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گشته در راه دین ز بهر ثبات</p></div>
<div class="m2"><p>خاک درگاه او چو آب حیات</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از غبار غرور عالم خاک</p></div>
<div class="m2"><p>دامن و جیب او چو ایمان پاک</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>قفل احکام را ستوده کلید</p></div>
<div class="m2"><p>پرّه و حلقه بی‌عمود که دید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون ستونی که هست بی‌افسون</p></div>
<div class="m2"><p>خیمهٔ شرع را طناب و ستون</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دیده بی‌زحمت خیال و غرور</p></div>
<div class="m2"><p>علم نزدیک او به عالم دور</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از فرازش نبرده سوی نشیب</p></div>
<div class="m2"><p>مگر این گنده پیر غرچه فریب</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دل او سال و ماه مسکن شرع</p></div>
<div class="m2"><p>گوش او شاهراه مَکمن شرع</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>دین ایزد ز بود او شادان</p></div>
<div class="m2"><p>خانهٔ شرع ازوست آبادان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>دل پاکش چو قبلهٔ ایمان</p></div>
<div class="m2"><p>عزم و حزمش همه دلیل و بیان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>روز حکمش بری ز جبر و قدر</p></div>
<div class="m2"><p>میل بر وی ندیده هیچ ظفر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>میل هرگز نکرده در احکام</p></div>
<div class="m2"><p>کرده در دین به شرط خویش قیام</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ظاهر و باطنش ز رشوت پاک</p></div>
<div class="m2"><p>میل در طبع او نه در افلاک</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گر بُدی زنده یوسف‌القاضی</p></div>
<div class="m2"><p>به نیابت ازو شدی راضی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>روز حشر و تغابن و زلزال</p></div>
<div class="m2"><p>او دهد زین قضا جواب سؤال</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>نامهٔ او به روز حشر و قضا</p></div>
<div class="m2"><p>نامهٔ یحیی است پاک و خلا</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>گر ز حشر است هرکسی را بیم</p></div>
<div class="m2"><p>وز مکافات و از عذاب الیم</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>او بُوَد ایمن از همه نکبات</p></div>
<div class="m2"><p>نبود در فریق حشر قضات</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>مهتر خلق و سیّد سادات</p></div>
<div class="m2"><p>گفت باشند از سه نوع قضات</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>دو بود هالک و یکی ناجی</p></div>
<div class="m2"><p>مژده کاندر بهشت با تاجی</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>آنکه نارد چنو صنایع دهر</p></div>
<div class="m2"><p>نیز در هیچ شهر قاضی شهر</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>علم دین تا بدو سپرد قضا</p></div>
<div class="m2"><p>جهل رحلت گزید سوی فنا</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>پیشش آن سر که در خزینه بُوَد</p></div>
<div class="m2"><p>چون چراغ اندر آبگینه بُوَد</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>اندرین حضرت بزرگ چو جان</p></div>
<div class="m2"><p>معنی او پدید و او پنهان</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>جان او را برای عالم غیب</p></div>
<div class="m2"><p>کرده خالی ز رسم و سیرت عیب</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>کرده پاک از میان جمع امم</p></div>
<div class="m2"><p>صفوت او کدورت از عالم</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>تازه کرده ز بهر یزدان را</p></div>
<div class="m2"><p>جان بی‌عقل و عقل بی‌جان را</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>نظرش همچو جان پاک مسیح</p></div>
<div class="m2"><p>بوده در شرح علم و شرع فصیح</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>کرده دست عنایت دینش</p></div>
<div class="m2"><p>متحلّی به عقد تمکینش</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>شمع دین صورت و بصیرت او</p></div>
<div class="m2"><p>عقل جان سیرت و سریرت او</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>گاه فتوی چو کلک بردارد</p></div>
<div class="m2"><p>چتر حق بر فراز سر دارد</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>بی‌حقیقت قلم نگیرد هیچ</p></div>
<div class="m2"><p>تو ز باد هوا ناله مپیچ</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>نه به کس میل و نه ز کسب ملول</p></div>
<div class="m2"><p>چون پیمبر به علم دین مشغول</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>زان به بیهوده می‌نپردازد</p></div>
<div class="m2"><p>که همی شغل آخرت سازد</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>بینی از هیچ چشم جان و خرد</p></div>
<div class="m2"><p>بگشایی که تا بدو نگرد</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>گر شناسی مقدم از تالی</p></div>
<div class="m2"><p>نیست این‌جا ز حیلتی خالی</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>فحل بودست در همه احوال</p></div>
<div class="m2"><p>چه به افعال دین چه در اقوال</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>در رضا دین به نفس نسپارد</p></div>
<div class="m2"><p>خشم را در نهاد نگذارد</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>هست چون حوض کوثر از انعام</p></div>
<div class="m2"><p>مشرب عَذب او ز زحمت عام</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>اهل دین را معین و دلسوز اوست</p></div>
<div class="m2"><p>مفتی شرق و غرب امروز اوست</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>زین جهان از پی سرای معاد</p></div>
<div class="m2"><p>شده مشغول در کشیدن زاد</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>تا عنان چون بدان جهان تابد</p></div>
<div class="m2"><p>عاقبت را چو نام خود یابد</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>متناسب نهاد او با حلم</p></div>
<div class="m2"><p>متشابه سواد او با علم</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>چون قدر در سخا ریا نکند</p></div>
<div class="m2"><p>چون قضا در عطا خطا نکند</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>هرچه اندر نقاب قوّت بود</p></div>
<div class="m2"><p>خاطرش را خرد به فعل نمود</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>رای بیدارش از طرسق صواب</p></div>
<div class="m2"><p>یک جهان خصم را کند در خواب</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>فضل را بحر بود و عزّ را کان</p></div>
<div class="m2"><p>شرع را دایه بود و دین را جان</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>روی او چون ز رای او بفروخت</p></div>
<div class="m2"><p>آفتابی به آفتاب آموخت</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>همچو اقبالش از دو عالم جای</p></div>
<div class="m2"><p>لاجرم هست پیر ملک خدای</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>دل او همچو موی اوست سپید</p></div>
<div class="m2"><p>باد در باغ شرع تا جاوید</p></div></div>