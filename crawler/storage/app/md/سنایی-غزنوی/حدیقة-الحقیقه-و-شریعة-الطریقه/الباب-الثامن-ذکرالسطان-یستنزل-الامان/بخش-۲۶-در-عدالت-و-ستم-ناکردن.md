---
title: >-
    بخش ۲۶ - در عدالت و ستم ناکردن
---
# بخش ۲۶ - در عدالت و ستم ناکردن

<div class="b" id="bn1"><div class="m1"><p>شاه چون بستد از رعیّت نان</p></div>
<div class="m2"><p>نقد شد کلّ من علیها فان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از رعیّت شهی که مایه ربود</p></div>
<div class="m2"><p>بُن دیوار کَند و بام اندود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نان خشکار را ز من ببری</p></div>
<div class="m2"><p>میده گردانی و تو میده خوری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>برّهٔ خوان که وجه بابزنست</p></div>
<div class="m2"><p>از بهای فروخ بیوه زنست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ملک ویران و گنج آبادان</p></div>
<div class="m2"><p>نبود جز طریق بیدادان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سخت بیخی درخت از بادست</p></div>
<div class="m2"><p>گنج پُر زر ز ملک آبادست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ملک آباد به ز گنج روان</p></div>
<div class="m2"><p>شادی دل ندارد ایچ روان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ابر چون زُفت گشت در باران</p></div>
<div class="m2"><p>شد ستم‌کش روانِ بیوه‌زنان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون ستد شه عوامل از دهقان</p></div>
<div class="m2"><p>ده ازو رفت و ماند بر وی قان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هرکه امسال آب وَرز ببرد</p></div>
<div class="m2"><p>سال دیگر گرسنه باید مرد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گرگ چون خورد گوسفند همه</p></div>
<div class="m2"><p>چه بُوَد سود از کلاب رمه</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گر نخواهی برهنه عورت تن</p></div>
<div class="m2"><p>در گریبان مزن ز بُن دامن</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>شاه را از رعیّت است اسباب</p></div>
<div class="m2"><p>کام دریا ز جوی جوید آب</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>آب جوی ار ز بحر بازگری</p></div>
<div class="m2"><p>بحر را زان سپس شَمر شمری</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بس به کار آمده است و بس دلخواه</p></div>
<div class="m2"><p>سرخی سیب را سپیدی ماه</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>هرکه جز شاه کالبدشان دان</p></div>
<div class="m2"><p>شاه جانست و خفته نبود جان</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>مثل شه سر و رعیّت تن</p></div>
<div class="m2"><p>هردو از یکدگر فزود ثمن</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>تن بی‌سر غذای زنبورست</p></div>
<div class="m2"><p>سر بی‌تن سزای تنّورست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>رونق جان ز عدل شاه بُوَد</p></div>
<div class="m2"><p>مُلک بی‌عدل برگ کاه بُوَد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ترک و ایرانی و عرابی و کرد</p></div>
<div class="m2"><p>هرکه عادلتر است دست او برد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>شاه را خواب خوش نباید خفت</p></div>
<div class="m2"><p>فتنه بیدار شد چو شاه بخفت</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>شاه را خواب غفلتست آفت</p></div>
<div class="m2"><p>همچو بیداریش بُوَد رأفت</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بالش کودکان ز خفتن دان</p></div>
<div class="m2"><p>بالش مرد سایهٔ خفتان</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>فلک از همّت ار چه زه دارد</p></div>
<div class="m2"><p>روز شمشیر و شب زره دارد</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>شب فلک دارد از ستاره حشر</p></div>
<div class="m2"><p>روز دارد ز آفتاب سپر</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>کم ز نرگس مباش اندر حزم</p></div>
<div class="m2"><p>چون کنی عزم رزم و مجلس بزم</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>نرگس از خواب از آن حذر دارد</p></div>
<div class="m2"><p>کههمی پاس تاج زر دارد</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>شه چو غوّاص و ملک چون دریاست</p></div>
<div class="m2"><p>خفتنش در درون آب خطاست</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>چون سیه روی بود نیلوفر</p></div>
<div class="m2"><p>شب چو ماهی در آب دارد سر</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>شه چو در بحر یار خواب شود</p></div>
<div class="m2"><p>تخت او زود تاج آب شود</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>چون برون شد ز کالبد غم نام</p></div>
<div class="m2"><p>خانه ویران شمار و زن بدنام</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>کور دل همچو کوز می باشد</p></div>
<div class="m2"><p>تیز مغز و ضعیف پی باشد</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>لیک محرور را دماغ قوی</p></div>
<div class="m2"><p>تو ز تأثیر کوز می‌شنوی</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>گور پی بند کیسه پندارد</p></div>
<div class="m2"><p>کور می را هریسه پندارد</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>عجز رای دلست و قدرت و جاه</p></div>
<div class="m2"><p>خشم و کین و دروغ و بخل از شاه</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>هرکه بر خشم و آز قاهرتر</p></div>
<div class="m2"><p>اوست بر خصم خویش قادرتر</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>شاه را در دماغ و بازوی چیر</p></div>
<div class="m2"><p>حزم بد دل بهست و عزم دلیر</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>اوّل حزم چیست رای زدن</p></div>
<div class="m2"><p>بعد از آن عزم دست و پای زدن</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>شاه را در خورست حزم درست</p></div>
<div class="m2"><p>ورنه عزمش بود ز غفلت سست</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>دل و زهره چو نور وام کند</p></div>
<div class="m2"><p>شمس را تیغ در نیام کند</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>زانکه در کارگاه دولت و دین</p></div>
<div class="m2"><p>عقل بیند به جان حقیقت این</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>مردی از شاه و خدعه از بدخواه</p></div>
<div class="m2"><p>حمله از شیر و حیله از روباه</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>حمله با شیر مرد همراهست</p></div>
<div class="m2"><p>حیله کار زنست و روباهست</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>همچو دریاست شاه خس‌پرور</p></div>
<div class="m2"><p>گهرش زیر پای و خس بر سر</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>بد نو کشته گنده نیک کُهن</p></div>
<div class="m2"><p>خار باشد به جای خرما بُن</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>همه روز از برای لقمهٔ نان</p></div>
<div class="m2"><p>این حدیثست و دوکدان زنان</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>میل ندهم به بد اگرچه نوست</p></div>
<div class="m2"><p>علف خر سبوس و کاه و جوست</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>خار بُن گرچه رست و بالا کرد</p></div>
<div class="m2"><p>سر او را سپهر والا کرد</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>تو طمع زو مدار میوه و گل</p></div>
<div class="m2"><p>یار بد هست بابت سر پل</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>نه ازو میوه خوب و نه سایه</p></div>
<div class="m2"><p>نه ازو سود به نه سرمایه</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>عامیان صف کشنده همچو کلنگ</p></div>
<div class="m2"><p>لیک زیشان چو باز ناید جنگ</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>هست در جنگ نیروی عامه</p></div>
<div class="m2"><p>همچو ارزیز گرم بر جامه</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>کودکان و زنان و حشو سپاه</p></div>
<div class="m2"><p>دل و صف را کنند هر سه تباه</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>زود خیزاست و خوش گریز حشر</p></div>
<div class="m2"><p>زود زایست و زود میر شرر</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>شرر تیز تگ جز ابله نیست</p></div>
<div class="m2"><p>زادهٔ او ز عمرش آگه نیست</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>زیرکانی که زیر کان دلند</p></div>
<div class="m2"><p>گوهر تخم را چو آب و گِلند</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>در میادین دین و ملک ملوک</p></div>
<div class="m2"><p>از برای نجات و هلک ملوک</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>یار دل به ز صبر ننهادند</p></div>
<div class="m2"><p>ظفر و صبر هردو همزادند</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>شه که دون را بلند و والا کرد</p></div>
<div class="m2"><p>مر بلا را بلندبالا کرد</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>آتشی کاب را بلند کند</p></div>
<div class="m2"><p>بر تن خویش ریشخند کند</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>از تف آتش گرش برد به فراز</p></div>
<div class="m2"><p>از کف خویش بکشد آبش باز</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>زشت زشت است در ولایت شاه</p></div>
<div class="m2"><p>گرگ بر گاه و یوسف اندر چاه</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>لشکری و رعیتی که سرند</p></div>
<div class="m2"><p>نفع را تیغ و دفع را سپرند</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>شاه بی‌بخشش آفت سپهست</p></div>
<div class="m2"><p>بی‌نیازی سپاه ذل شهست</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>ای بیاموخته به خاطر دون</p></div>
<div class="m2"><p>تاجداری ز گزدم گردون</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>چاکرت گر بدست و گر بد نیست</p></div>
<div class="m2"><p>بد و نیکش ز تست از خود نیست</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>چاکر مرد بد نکو نبود</p></div>
<div class="m2"><p>آب خاکی جز از سبو نبود</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>هست در دست تو چو تیغ و چونی</p></div>
<div class="m2"><p>تو بدی عیب خود منه بر وی</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>لشکر از جاه و مال شد بد دل</p></div>
<div class="m2"><p>رعیت از بی‌زریست بی‌حاصل</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>رعیت از تو چو با یسار شود</p></div>
<div class="m2"><p>از برای تو جان سپار شود</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>چون نیابد یسار بگریزد</p></div>
<div class="m2"><p>با عدوی تو برنیاویزد</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>تن که لاغر بُوَد بُوَد منبل</p></div>
<div class="m2"><p>پس چو فربه شود شود کاهل</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>مردمی با کسی که بی‌اصل است</p></div>
<div class="m2"><p>همچو شمشیر دسته با وصل است</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>سوی او دل چو خاک در دیگست</p></div>
<div class="m2"><p>نزد او جان چو آب در ریگست</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>چه به بی‌اصل زر و زور دهی</p></div>
<div class="m2"><p>چه چراغی به دست کور دهی</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>ای که با دین و ملک داری کار</p></div>
<div class="m2"><p>در شره خوی خرس و خوک مدار</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>که نکو ناید ار ز من پرسی</p></div>
<div class="m2"><p>خوک بر تخت و خرس بر کرسی</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>شاه شهری که بی‌خرد باشد</p></div>
<div class="m2"><p>نیک لشکر به نرخ بد باشد</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>لهو چون مرگ جان ملک برد</p></div>
<div class="m2"><p>ظلم چون ریگ آب ملک خورد</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>خاک بر باد کینه‌ور باشد</p></div>
<div class="m2"><p>ریگ بر آب تشنه‌تر باشد</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>شه چو بنشست بر دریچهٔ هزل</p></div>
<div class="m2"><p>ملک بیرون پرد ز روزن عزل</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>هزل با شاه اگر مقیم شود</p></div>
<div class="m2"><p>خاطرش در هنر عقیم شود</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>اول نور هست باد هبات</p></div>
<div class="m2"><p>آخر ظلمت است آب حیات</p></div></div>