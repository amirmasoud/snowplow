---
title: >-
    بخش ۴۳ - در مدح نظام‌الملک ابونصر محمّدبن عبدالحمیدالمستوفی
---
# بخش ۴۳ - در مدح نظام‌الملک ابونصر محمّدبن عبدالحمیدالمستوفی

<div class="b" id="bn1"><div class="m1"><p>خواجه بونصر نائب دستور</p></div>
<div class="m2"><p>چشم بد زان جمال و دانش دور</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خُلق او هست بی‌ریا و نفاق</p></div>
<div class="m2"><p>خلق او هست بی‌خلاف و شقاق</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هم نکو خلق و هم نکو گفتار</p></div>
<div class="m2"><p>هم نکو خط و هم نکو دیدار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آنچه گوش از کمال خواجه شنید</p></div>
<div class="m2"><p>چشم از او صد هزار چندان دید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جان و دل را حدیقه و مونس</p></div>
<div class="m2"><p>عقل و گل را شمامه و مجلس</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کانچه دارد ز خلق او اطراف</p></div>
<div class="m2"><p>آهوی چین ندارد اندر ناف</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>روح دیدار و عقل گفتارست</p></div>
<div class="m2"><p>دولت ایثار و ملّت آثارست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>فضل او در زمان چنان فاشست</p></div>
<div class="m2"><p>که ادب بر درش چو فرّاشست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از پی جاه و خدمت سلطان</p></div>
<div class="m2"><p>نه برای فلانک و بهمان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>قبلهٔ فاضلان ستانهٔ اوست</p></div>
<div class="m2"><p>سرمهٔ عقل گرد خانهٔ اوست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مال خود چون خیال بگذارد</p></div>
<div class="m2"><p>وآنِ سلطان چو جان نگه دارد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>صورتش ابتدای قوّت روح</p></div>
<div class="m2"><p>سیرتش انتهای سورهٔ نوح</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>کرده از بهر حق بکرد و بگفت</p></div>
<div class="m2"><p>عادتش عُدّت وفا را جفت</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>در ره شاکری فریشته وش</p></div>
<div class="m2"><p>راست محنت کن است و محنت کش</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>پیش او از برای سود و زیان</p></div>
<div class="m2"><p>صدهزاران دلست و یک فرمان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>همچو عقل از کی و که و چه و چون</p></div>
<div class="m2"><p>فکرتش پی برد درون و برون</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>از پی آفتاب دهرآرای</p></div>
<div class="m2"><p>زو برد مشتری اصابت رای</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>رای او قطب دولت مردان</p></div>
<div class="m2"><p>ملک و دین گرد رای او گردان</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>همچو عقل از ورای چرخ کبود</p></div>
<div class="m2"><p>دیده نابوده هرچه خواهد بود</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>پیش رایش نماند پوشیده</p></div>
<div class="m2"><p>بر فلک هیچ روی پوشیده</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>فهمش از جام جم نیاید کم</p></div>
<div class="m2"><p>که همه بودنی بدید چو جم</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>دل او از برای به دانی</p></div>
<div class="m2"><p>هست مشکات نور ربّانی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>اثر لطف او چو آب زُلال</p></div>
<div class="m2"><p>خاک‌روب درش اثیر جلال</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>نیست در کارگاه صُنع خدای</p></div>
<div class="m2"><p>کار بندی چو خواجه کارگشای</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چون سرانگشت او قلم گیرد</p></div>
<div class="m2"><p>چار طبع عدو الم گیرد</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>عقدی از دُر کشد ز نوک قلم</p></div>
<div class="m2"><p>چون ز سر بر بیاض ساخت قدم</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>پست بالاست پیش عزّش عرش</p></div>
<div class="m2"><p>تنگ پهناست پیش فرّش فرش</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>ابر گریان ز دست و دست گهش</p></div>
<div class="m2"><p>صبح خندان ز خاک بوس رهش</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>هست در رشک آن کف و گفتار</p></div>
<div class="m2"><p>آب دریا و لؤلؤ شهوار</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>برده آب بهار و آوازه‌ش</p></div>
<div class="m2"><p>لب خندان و چهرهٔ تازه‌ش</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>پیش سرِّ خدایگان از هوش</p></div>
<div class="m2"><p>هر زمان حلقه‌ای کند در گوش</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>گر فلک نیست کلک او هرگاه</p></div>
<div class="m2"><p>از گریبان چرا برآرد ماه</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>در یکی فضل او تأمّل کن</p></div>
<div class="m2"><p>عقل را مال و روح را مل کن</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>تا نبینی به چشم عقل و یقین</p></div>
<div class="m2"><p>در دو خط صد نگارخانهٔ چین</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>درج کرده چو سایه و خورشید</p></div>
<div class="m2"><p>در شب و روز نام بیم و امید</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>از خط او که دنیی و دینست</p></div>
<div class="m2"><p>دیده گل‌بین و عقل گل چینست</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>همّتش آسمان و خُلق ملک</p></div>
<div class="m2"><p>خاطرش آفتاب و کلک فلک</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>خط او در هوای گلبن راز</p></div>
<div class="m2"><p>پشت طاوس‌دان و سینهٔ باز</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>زاده از روح کلک و نور یقین</p></div>
<div class="m2"><p>شب و روز جهان دولت و دین</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>زردهٔ عقل زردی خامه‌ش</p></div>
<div class="m2"><p>ادهم دین سیاهی نامه‌ش</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>هرکرا نیست چون قلم رایش</p></div>
<div class="m2"><p>قلم او قلم کند پایش</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>خط او خط جان اسرافیل</p></div>
<div class="m2"><p>کلک او کیل رزق میکائیل</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>صورت خط او که در نامه‌ست</p></div>
<div class="m2"><p>چون نسیم بهارخوش جامه‌ست</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>کلک او همچو نوک دیده‌کشان</p></div>
<div class="m2"><p>خط او همچو غمزه‌های خوشان</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>شحنهٔ راه دین صلابت او</p></div>
<div class="m2"><p>روح قدسی کمین مثابت او</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>نیست پوشیده زو قلیل و کثیر</p></div>
<div class="m2"><p>نز نقیر ایچ چیز و نز قطمیر</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>جاه او همچو ماه ملک نگار</p></div>
<div class="m2"><p>کلک او همچو تیغ کارگذار</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>به امان و به خلق حور و پری</p></div>
<div class="m2"><p>در تباشیر بشر او بشری</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>برده بیخ سخاش تا عیّوق</p></div>
<div class="m2"><p>میوه و برگ و شاخ و نرد و عروق</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>طیب ذکرش غذای روح ملک</p></div>
<div class="m2"><p>طول عمرش مدار دور فلک</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>باد امرش چو امر روح ملک</p></div>
<div class="m2"><p>باد عمرش چو عمر نوح و لمک</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>عقل با وی نشسته در مکتب</p></div>
<div class="m2"><p>علم از وی گرفته علم و ادب</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>روح بر مرکب عنایت اوست</p></div>
<div class="m2"><p>عقل در مکتب هدایت اوست</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>به گه ضبط مال و عقد حسیب</p></div>
<div class="m2"><p>ساحران را زند به علم آسیب</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>کرده از بر به قدرت خلّاق</p></div>
<div class="m2"><p>حاجت آید مطالعت به کتاب</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>او ز حالی که شاه از او جوید</p></div>
<div class="m2"><p>همه از بر به جمله برگوید</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>ملک عالم برش معاینه شد</p></div>
<div class="m2"><p>دل او بر مثال آینه شد</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>حبّذا رای روشن و پاکش</p></div>
<div class="m2"><p>که فلک گشت تختهٔ خاکش</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>خامه اندر بنان او گه سیر</p></div>
<div class="m2"><p>بگشاید به خلق بر در خیر</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>بر سر انگشت وی چو گشت سوار</p></div>
<div class="m2"><p>آن لطیف نحیف زرد و نزار</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>دوستان را کند دو رخ چون لعل</p></div>
<div class="m2"><p>دشمنان را کند سیاه چو نعل</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>اندُه دشمن است و شادی دوست</p></div>
<div class="m2"><p>خیر و شر بسته در زبانهٔ اوست</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>شب آبستنست خامهٔ او</p></div>
<div class="m2"><p>گشت مُضمر ز فتح نامهٔ او</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>زان زبان سیاه و شخص سپید</p></div>
<div class="m2"><p>گشته دشمن ز جان خود نومید</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>تن سپید و سیاه منقارش</p></div>
<div class="m2"><p>همه ساله غذا شده قارش</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>در شود هر زمان به بحر سیاه</p></div>
<div class="m2"><p>برکشد دُر ز بهر تاج و کلاه</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>هست همواره با دلِ بیدار</p></div>
<div class="m2"><p>در همه کار عاقل و هشیار</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>مال دنیا اگر ورا باشد</p></div>
<div class="m2"><p>همه بر زایرانش بر پاشد</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>چیز را در دلش نماند محل</p></div>
<div class="m2"><p>زان ورا نیست در زمانه بدل</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>گرچه رنگش گناه را ماند</p></div>
<div class="m2"><p>به گه سیر ماه را ماند</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>ساعتی با دلش چو رهبر شد</p></div>
<div class="m2"><p>سایه‌بان زمانه جانور شد</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>خیمهٔ عمر او هزار طناب</p></div>
<div class="m2"><p>ماه خیمه‌ش برابر مهتاب</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>تا ورا شاه شرق تمکین داد</p></div>
<div class="m2"><p>ملک را صدهزار تزیین داد</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>کار ملکت به کاردان فرمود</p></div>
<div class="m2"><p>لاجرم رونق دول بفزود</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>چیست بهتر در این جهان جهان</p></div>
<div class="m2"><p>مرد را کار و کار را مردان</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>این هم از بخت شاه مشرق بود</p></div>
<div class="m2"><p>که بدو رونق عمل بفزود</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>لاجرم عالمی برآسودند</p></div>
<div class="m2"><p>به حیات و به مال بر سودند</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>که کسی را گماشت شه به جهان</p></div>
<div class="m2"><p>که نخواهد به هیچ خلق زیان</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>به قلم قسم کرد هفت اقلیم</p></div>
<div class="m2"><p>هیچ ناکرده ظلم دانگی سیم</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>حاکم مملکت چنین باید</p></div>
<div class="m2"><p>تا ز عدلش جهان برآساید</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>تا جهانست عمر خسرو باد</p></div>
<div class="m2"><p>که مر او را چنین مثابت داد</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>باد تا باد ملک را بازار</p></div>
<div class="m2"><p>شاه از او او ز شاه برخوردار</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>باد تا باد شکل خط همه طول</p></div>
<div class="m2"><p>به خدای و خدایگان مشغول</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>شاه را باد عمر تا جاوید</p></div>
<div class="m2"><p>خواجگانش چو ماه و چون خورشید</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>صاحب عادل آن صفی وفی</p></div>
<div class="m2"><p>صدر دیوان و خواجه مستوفی</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>چشم بد دور ازین چنین دو وزیر</p></div>
<div class="m2"><p>که ندارند در زمانه نظیر</p></div></div>