---
title: >-
    بخش ۲۲ - اندر معنی بیداری ملوک و سلاطین و حفظ و بخشش ایشان
---
# بخش ۲۲ - اندر معنی بیداری ملوک و سلاطین و حفظ و بخشش ایشان

<div class="b" id="bn1"><div class="m1"><p>شاه محمود زاولی به شکار</p></div>
<div class="m2"><p>رفت روزی ز روزگار بهار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با گروهی ز خاصگان سپاه</p></div>
<div class="m2"><p>کرد نخچیر شاه داد پناه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از برِ شاه آهویی برخاست</p></div>
<div class="m2"><p>که به جستن تو گفتیی که صباست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گرم کرد اسب شاه از پی صید</p></div>
<div class="m2"><p>تا کند مر ورا سبکتر قید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بارهٔ شاه هرچه بیش شتافت</p></div>
<div class="m2"><p>گرد صید دونده کمتر یافت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا جدا گشت شه ز لشکر خویش</p></div>
<div class="m2"><p>پی آهو ندید در برِ خویش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در پی صید چونکه شد حیران</p></div>
<div class="m2"><p>سوی لشکر ز ره بتافت عنان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بود بیران دهی به ره اندر</p></div>
<div class="m2"><p>از عمارت درو نمانده اثر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شاه را آبدست حاجت کرد</p></div>
<div class="m2"><p>سوی بیرانه ده ارادت کرد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>راند باره در آن ده ویران</p></div>
<div class="m2"><p>چون سوی صید آهوان شیران</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آمد از بارگی فرو چون باد</p></div>
<div class="m2"><p>اسب دربست و بند خویش گشاد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چونکه فارغ شد از مراد برفت</p></div>
<div class="m2"><p>تا به لشکر رود چو باد بتفت</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>پس چو نزدیک باره آمد شاه</p></div>
<div class="m2"><p>سوی دیوار باره کرد نگاه</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>رخنه‌ای دید اندر آن دیوار</p></div>
<div class="m2"><p>خرقه‌ای اندر آن سیاه چو قار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گوشهٔ خرقه از شکاف به در</p></div>
<div class="m2"><p>باد می‌برد زیر و گاه زبر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>سر تازانه خسرو اندر آخت</p></div>
<div class="m2"><p>خرقه زان جایگه برون انداخت</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>خرقهٔ کهنه بر زمین افتاد</p></div>
<div class="m2"><p>بود پوسیده بند او بگشاد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>پنج دینار بُد در او موزون</p></div>
<div class="m2"><p>مُهر او کرده نام افریدون</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>شاه از آن گشت شاد و داشت به فال</p></div>
<div class="m2"><p>با همه خسروی و عزّ و جلال</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>برگرفت و نهاد اندر جیب</p></div>
<div class="m2"><p>زان گرفتنش هیچ نامد عیب</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>سیم را چون خدای کرد عزیز</p></div>
<div class="m2"><p>پس تو لابد عزیز دارش نیز</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>مر عزیزی که یار داری تو</p></div>
<div class="m2"><p>خوار گردی چو خوار داری تو</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>اندر آن جایگاه بیش نماند</p></div>
<div class="m2"><p>باره را بر نشست و تیز براند</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>به سلامت بسوی لشکرگاه</p></div>
<div class="m2"><p>باز شد با مراد خرّم شاد</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>خواست دینار شاه پنج هزار</p></div>
<div class="m2"><p>کرد با آن دُرست یافته یار</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>جمله را شه به سایلان بخشید</p></div>
<div class="m2"><p>از چنان شه چنین طریق سزید</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>شاه از آن پس چو زی شکار شدی</p></div>
<div class="m2"><p>هوس آن وطنش یار شدی</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>اسب راندی در آن خرابه چو باد</p></div>
<div class="m2"><p>کردی آن روزگار و آن زر یاد</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>هرکه او خرّمی ز جایی دید</p></div>
<div class="m2"><p>طبعش آن جایگاه را بگزید</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>چون بدان جایگاه باز رسید</p></div>
<div class="m2"><p>خرّمی در دلش فراز رسید</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>تا نبیند دلش نیارامد</p></div>
<div class="m2"><p>زانکه دل با مراد یار آمد</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>خواجه این خرده را مگردانی</p></div>
<div class="m2"><p>خو پذیر است نفس انسانی</p></div></div>