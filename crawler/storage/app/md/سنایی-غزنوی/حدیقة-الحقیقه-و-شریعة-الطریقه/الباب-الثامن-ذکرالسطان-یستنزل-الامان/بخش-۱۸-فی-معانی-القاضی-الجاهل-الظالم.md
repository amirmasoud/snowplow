---
title: >-
    بخش ۱۸ - فی معانی القاضی الجاهل الظالم
---
# بخش ۱۸ - فی معانی القاضی الجاهل الظالم

<div class="b" id="bn1"><div class="m1"><p>آن شنیدی که در دهی پیری</p></div>
<div class="m2"><p>خورد ناگه ز شحنه‌ای تیری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رفت در پیش قاضی آن درویش</p></div>
<div class="m2"><p>گفت بنگر مرا چه آمد پیش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شحنه سرمست بود در میدان</p></div>
<div class="m2"><p>تیری افکند و زد مرا بر جان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>قاضی او را بگفت از سرِ خشم</p></div>
<div class="m2"><p>قلتبانا نگه نداری چشم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تیر شحنه به خون بیالودی</p></div>
<div class="m2"><p>تا مرا درد سر بیفزودی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جفت گاوت به شحنهٔ ده ده</p></div>
<div class="m2"><p>وز چنین دردسر به نفس بجه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا دل شحنه بر تو گردد خوش</p></div>
<div class="m2"><p>ورنه اندر زند به جانت آتش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گفت گشتم به حکم تو راضی</p></div>
<div class="m2"><p>چون بُوَد خشم شحنه و قاضی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ای ملک سیرت ملک سیما</p></div>
<div class="m2"><p>ملک دنیا به تست درد و دوا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زین چنین قاضیان هرزه درای</p></div>
<div class="m2"><p>خلق را گوش کن ز بهر خدا</p></div></div>