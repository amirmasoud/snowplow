---
title: >-
    بخش ۲۳ - فی حفظ اسرارالملک و کفایته و کتمانه
---
# بخش ۲۳ - فی حفظ اسرارالملک و کفایته و کتمانه

<div class="b" id="bn1"><div class="m1"><p>با سلاطین چو گفت خواهی راز</p></div>
<div class="m2"><p>وقت آنرا بدان چو وقت نماز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کن مراعات شاهِ بدخو را</p></div>
<div class="m2"><p>چون زن زشت شوی نیکو را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شه چو بر داردت فکندش باش</p></div>
<div class="m2"><p>چون ترا خواجه خواند بنده‌ش باش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دستت از داد پایگاه بنه</p></div>
<div class="m2"><p>ور ترا سر دهد کلاه بنه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر سری کو ز شه کله جوید</p></div>
<div class="m2"><p>پای خود زان میان ره جوید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پادشاه ار ترا برادر خواند</p></div>
<div class="m2"><p>دان که در قعر دوزخت بنشاند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون بگفت این ملوک‌وار سخن</p></div>
<div class="m2"><p>پس به خود گفت هوش‌دار ای تن</p></div></div>