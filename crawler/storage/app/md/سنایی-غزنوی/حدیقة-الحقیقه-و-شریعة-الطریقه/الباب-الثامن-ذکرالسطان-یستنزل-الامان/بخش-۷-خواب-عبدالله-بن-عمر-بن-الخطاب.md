---
title: >-
    بخش ۷ - خواب عبداللّٰه بن عمر بن الخطّاب
---
# بخش ۷ - خواب عبداللّٰه بن عمر بن الخطّاب

<div class="b" id="bn1"><div class="m1"><p>دید یک شب به خواب عبداللّٰه</p></div>
<div class="m2"><p>پدر خویش را عُمر ناگاه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفت یا میر عادل خوش خوی</p></div>
<div class="m2"><p>حال خود با من این زمان برگوی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با تو ایزد چه کرد بر گو حال</p></div>
<div class="m2"><p>بعد از این مدّت دوازده سال</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفت از آن روز باز تا امروز</p></div>
<div class="m2"><p>در حسابم کنون شدم پیروز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کار من صعب بود با غم و درد</p></div>
<div class="m2"><p>عاقبت عفو کرد و رحمت کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گوسفندی ضعیف در بغداد</p></div>
<div class="m2"><p>رفت بر پول و ناگهان بفتاد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گشت رنجور و پای وی بشکست</p></div>
<div class="m2"><p>صاحب وی به دامنم زد دست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گفت انصاف من بده بتمام</p></div>
<div class="m2"><p>که تو بودی امیر بر اسلام</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تا به امروز من دوازده سال</p></div>
<div class="m2"><p>بوده‌ام مانده در جواب سؤال</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ای ستوده شه نکو کردار</p></div>
<div class="m2"><p>باز پرسند از تو این مقدار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چون چنین بُد خطاب با عمری</p></div>
<div class="m2"><p>چه رود روز حشر با دگری</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هان و هان تاز خود نگردی مست</p></div>
<div class="m2"><p>ورنه گردی به روز محشر پست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ای ز انصاف ملک دلکش‌تر</p></div>
<div class="m2"><p>همه کس بر تو خوش رهی خوشتر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>آنت خواهم که هرکجا پویند</p></div>
<div class="m2"><p>همه نیکان ترا نکو گویند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بهر رغم ستم‌گرایان را</p></div>
<div class="m2"><p>الکنی کن عمرستایان را</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>عدل عمّر چو ظلم با عدلت</p></div>
<div class="m2"><p>بذل حاتم چو بخل با بذلت</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>عدل تایید جاه شاه بُوَد</p></div>
<div class="m2"><p>غبغب اندر گلو چه جاه بود</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>آن چنان داد کن که از پی داد</p></div>
<div class="m2"><p>کس ز عدل عُمر نیارد باد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>خوش بود خاصه از جهانگیران</p></div>
<div class="m2"><p>رحمت طفل و حرمت پیران</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>آن چنان باد پادشاهی تو</p></div>
<div class="m2"><p>که نخواهد عدوت و خواهی تو</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>دولتت با دوام مقرون باد</p></div>
<div class="m2"><p>سایل درگه تو قارون باد</p></div></div>