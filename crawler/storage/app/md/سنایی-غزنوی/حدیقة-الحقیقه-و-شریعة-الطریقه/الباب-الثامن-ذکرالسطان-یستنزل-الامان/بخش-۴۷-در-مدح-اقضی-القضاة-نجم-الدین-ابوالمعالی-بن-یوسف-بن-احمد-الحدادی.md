---
title: >-
    بخش ۴۷ - در مدح اقضی‌القضاة نجم‌الدّین ابوالمعالی‌بن یوسف‌بن احمد الحدّادی
---
# بخش ۴۷ - در مدح اقضی‌القضاة نجم‌الدّین ابوالمعالی‌بن یوسف‌بن احمد الحدّادی

<div class="b" id="bn1"><div class="m1"><p>نام او در عمل صحیح‌الجهد</p></div>
<div class="m2"><p>لقبش در وفا کریم‌ العهد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همت او ورای جزو و کلست</p></div>
<div class="m2"><p>که همه آبها به زیر پلست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر بخواهی تو جانش از معنی</p></div>
<div class="m2"><p>کرم و خلق او نگوید نی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سایل آز را چو قاورن کرد</p></div>
<div class="m2"><p>پنبه از گوش بخل بیرون کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خواجه ابلیس کز پی دم غیر</p></div>
<div class="m2"><p>لیف او لاف زد چو گفت انا خیر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کردی ار دیدی این مکارم و جود</p></div>
<div class="m2"><p>در سرای وجود رای سجود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بیند آنکس که هست بینا دل</p></div>
<div class="m2"><p>وانکه از گِل دل آورد حاصل</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سمع آنکو به مجلسش بنشست</p></div>
<div class="m2"><p>شمع دارد تو گویی اندر دست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جامهٔ عزمش از صیانت پاک</p></div>
<div class="m2"><p>عرصهٔ جانش از خیانت پاک</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دم او همچو عیسی آدم جان</p></div>
<div class="m2"><p>عهد او همچو خضر محکم جان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>عهد او چون پیمبر اندر عهد</p></div>
<div class="m2"><p>شخص او همچو عیسی اندر مهد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چون ز خورشید قابل قوتست</p></div>
<div class="m2"><p>لاجرم عهد او چو یاقوتست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نکتهٔ او بَرِ صلاح و وفاق</p></div>
<div class="m2"><p>گوش ساره‌ست و مژدهٔ اسحق</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چون تنور زمانه آتش یافت</p></div>
<div class="m2"><p>گردن چرخ سیلی خوش یافت</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>خود نراندست در شفا و الم</p></div>
<div class="m2"><p>جز به املای شرع و عقل قلم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>لفظ و نطقش ز عقل و جان مملیست</p></div>
<div class="m2"><p>کو ز امر خدای مستملیست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>جود او چون بهار خوش سلبست</p></div>
<div class="m2"><p>بود او چون حیات حق طلبست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>مایهٔ فرش رسم تحفهٔ اوست</p></div>
<div class="m2"><p>سایهٔ عرش طاق صفّهٔ اوست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>هست از روی رتبت و اجلال</p></div>
<div class="m2"><p>پشت اسلام و شرع را ز کمال</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>در نظر چون عبارت آراید</p></div>
<div class="m2"><p>جبرئیلش به طبع بستاند</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>کلک او کز ره جفا دورست</p></div>
<div class="m2"><p>همچو انگشت حور پر نورست</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>در کف نقشبند سرِّ ازّل</p></div>
<div class="m2"><p>در خلاء جلال او چه خلل</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>هست در بادیه در آز و نیاز</p></div>
<div class="m2"><p>گرچه راهست دور و زشت و دراز</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>زین سبب نیست در نشیمن جود</p></div>
<div class="m2"><p>لاجرم هست در سرای وجود</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>آسمان سخا و احسان اوست</p></div>
<div class="m2"><p>ابر انعام و غیث انسان اوست</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>چاکر گفت اوست گفتارم</p></div>
<div class="m2"><p>شاکر دست اوست دستارم</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>به دو لفظ نکو که بشنودم</p></div>
<div class="m2"><p>یک در اندر فلک بیفزودم</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>مر مرا آب شد ز حیرانی</p></div>
<div class="m2"><p>آتش دیگ روح حیوانی</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>گرچه با ما هم از قرونست او</p></div>
<div class="m2"><p>از قرون و قران برونست او</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>زاغ را چون همای فر دادست</p></div>
<div class="m2"><p>پشه را همچو باشه پر دادست</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>قلم او ز سهوهاست مصون</p></div>
<div class="m2"><p>برِ علمش علوم گشته زبون</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>زو امیر ولایتی گشتم</p></div>
<div class="m2"><p>وز قبول وی آیتی گشتم</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>علم او دستگیر دینداران</p></div>
<div class="m2"><p>قلمش چون ربیع با باران</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>عالم از فتویش بر آسوده</p></div>
<div class="m2"><p>وز ضلالت جهان بیزدوده</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>کرده برهانش بر جهان آسان</p></div>
<div class="m2"><p>متشابه که هست در قرآن</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>گر تبجح کند روا باشد</p></div>
<div class="m2"><p>این چنین علمها کرا باشد</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>نیست مانند او به علم اندر</p></div>
<div class="m2"><p>متواضع به علم و حلم اندر</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>او تواند نمود مرجان را</p></div>
<div class="m2"><p>بی‌نقاب حروف قرآن را</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>زانکه در تربه سیّد آسوده‌ست</p></div>
<div class="m2"><p>تا نیابت به شیخ فرموده‌ست</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>مرد چون کار را بود در خورد</p></div>
<div class="m2"><p>هرچه وی گفت شیخ چونان کرد</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>هر خبر کز رسول نقل افتاد</p></div>
<div class="m2"><p>شیخ در شرح آن بدادش داد</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>معنی هریکی برون آورد</p></div>
<div class="m2"><p>جمله زیبا و نیکو و در خورد</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>مشکلات کلام ایزد بار</p></div>
<div class="m2"><p>متشابه که هست در اخبار</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>همه را کرده حل به شکل و بیان</p></div>
<div class="m2"><p>لفظهائی که هست در قرآن</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>ابن‌عبّاس روزگارست او</p></div>
<div class="m2"><p>با معانی بی‌شمارست او</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>هست با دانش معاذ جبل</p></div>
<div class="m2"><p>ایزدش برگزیده عزّوجل</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>سخنش همچو روضهٔ نورست</p></div>
<div class="m2"><p>نیک نزدیک لیک بس دورست</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>همچو عقل اندک فراوان شو</p></div>
<div class="m2"><p>صلح افکن ولیک پنهان شو</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>هم گران هم سبک لقاست چو کان</p></div>
<div class="m2"><p>هم سبک هم گران‌بهاست چو جان</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>گر دواند مرا به پیش الم</p></div>
<div class="m2"><p>پیش حکمش به سر دوَم چو قلم</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>ور مرا گوید ای سنایی رو</p></div>
<div class="m2"><p>بندم از دیده با شمال گرو</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>ور بخواند مرا ز بهر عتاب</p></div>
<div class="m2"><p>همه تن دل شوم بسان حباب</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>قدر او بام آسمان برین</p></div>
<div class="m2"><p>خوی او دام جبرئیل امین</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>کام چون بر بساط نطق آرد</p></div>
<div class="m2"><p>گنگ را در نشاط نطق آرد</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>گر کند ز الکن التماس سخن</p></div>
<div class="m2"><p>در حدیث آید از نشاط الکن</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>سنگ بر وی به مدح جود کند</p></div>
<div class="m2"><p>فلک از نطق او سجود کند</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>سخنش عذب چون نتیجهٔ صبر</p></div>
<div class="m2"><p>با بطر چون سرشک دیدهٔ ابر</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>خلق و خلقش لطیف چون حورا</p></div>
<div class="m2"><p>لفظ و معنی دو مغزه چون جوزا</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>نفس او نقش زندگانی بود</p></div>
<div class="m2"><p>که دو مغز و یک استخوانی بود</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>خوی او جان تشنه را مشرب</p></div>
<div class="m2"><p>سحر او مر پیاده را مرکب</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>کرده از نکته‌های عقل‌انگیز</p></div>
<div class="m2"><p>طبع یاران و چشم خاطر تیز</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>در تصفّح چو حلم به بردار</p></div>
<div class="m2"><p>در تخلّص چو علم برخوردار</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>در خرد صفو را مبانی اوست</p></div>
<div class="m2"><p>در سخن روح را معانی اوست</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>سیرت پاک او حکیم اوصاف</p></div>
<div class="m2"><p>صورت علم او کریم انصاف</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>همه ابرام و ناز بتوان کرد</p></div>
<div class="m2"><p>شعر چون هست بکر و معطی مرد</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>باد پیوسته چیره در هر کار</p></div>
<div class="m2"><p>وز همه علم خویش برخوردار</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>باد باقی بقای روح و ملک</p></div>
<div class="m2"><p>تا بود در مدار چرخ و فلک</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>تا جهانست عزّ و جاهش باد</p></div>
<div class="m2"><p>حکمت و شرع در پناهش باد</p></div></div>