---
title: >-
    بخش ۱۵ - در عدل پادشاه و صفت آن
---
# بخش ۱۵ - در عدل پادشاه و صفت آن

<div class="b" id="bn1"><div class="m1"><p>عدل کن زانکه در ولایت دل</p></div>
<div class="m2"><p>دَرِ پیغمبری زند عادل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در شبانی چو عدل کرد کلیم</p></div>
<div class="m2"><p>داد پیغمبریش اله کریم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا شبانی نکرد بر حیوان</p></div>
<div class="m2"><p>کی شبان گشت بر سرِ انسان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عدل در دست آنکه دادگرست</p></div>
<div class="m2"><p>ناوک مرگ را قوی سپرست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مرگ را هیچ ناید از عادل</p></div>
<div class="m2"><p>زانکه دارد ز عدل عادل دل</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شاه پُر دل ستیزه کار بود</p></div>
<div class="m2"><p>شاه بد دل همیشه خوار بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شاه عادل میان نیک و بدست</p></div>
<div class="m2"><p>تیز و ظالم هلاک خلق و خودست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بر میانه بود شه عادل</p></div>
<div class="m2"><p>نبود شیرخو نه اشتر دل</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ملک را شاه ظالم پُر دل</p></div>
<div class="m2"><p>به ز سلطان عاجز عادل</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>داد کس شاه عاجز با داد</p></div>
<div class="m2"><p>نتواند ستد نه یارد داد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شاه جایر ز ملک و دین تنهاست</p></div>
<div class="m2"><p>جان به انصاف طبع در تنهاست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دل شه چون ز عجز خونابه‌ست</p></div>
<div class="m2"><p>او نه شاهست نقش گرمابه‌ست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>عدل شه نعمت خداوندست</p></div>
<div class="m2"><p>جور او پای خلق را بندست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>شاه عادل چو کشتی نوحست</p></div>
<div class="m2"><p>که ازو امن و راحت روحست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>شاه جایر چو موج طوفانست</p></div>
<div class="m2"><p>زو خرابی خانه و جانست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>باشد اندر خراب و آبادان</p></div>
<div class="m2"><p>عدل شه غیث و جور شه طوفان</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>طالب شاه عادلست جهان</p></div>
<div class="m2"><p>تو نیت خوب کن جهان بستان</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>هرکه دارد به داد و دین عالَم</p></div>
<div class="m2"><p>به خدا ار بود ز مَهدی کم</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>کو نه مهدی به سست عهدی شد</p></div>
<div class="m2"><p>او بدین و به داد مَهدی شد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>تو بری شو ز جور و بدعهدی</p></div>
<div class="m2"><p>کافرم گر نخوانمت مَهدی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>فرّ انصاف و زیب شید یکیست</p></div>
<div class="m2"><p>بیخ بیداد و شاخ بید یکیست</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ساختن راست شید بر گردون</p></div>
<div class="m2"><p>سوختن راست بید بر هامون</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>با ستم سور مملکت شوریست</p></div>
<div class="m2"><p>بی‌الف نقش داوری دوریست</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>پادشاه مسلّط و مغرور</p></div>
<div class="m2"><p>از خدای و ز خلق باشد دور</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>از خدای و اجل بی‌آگاهی</p></div>
<div class="m2"><p>ایمن از ناوک سحرگاهی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ای بسا تاج و تخت مرجومان</p></div>
<div class="m2"><p>لخت لخت از دعای مظلومان</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ای بسا رایت عدو شکنان</p></div>
<div class="m2"><p>سرنگون از دعای پیرزنان</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>ای بسا تیرهای گنجوران</p></div>
<div class="m2"><p>شاخ شاخ از دعای رنجوران</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>ای بسا نیزه‌های جبّاران</p></div>
<div class="m2"><p>پار پار از دعای غم خواران</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>ای بسا باد و بوش تکسینان</p></div>
<div class="m2"><p>ترت و مرت از دعای مسکینان</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>ای بسا بادگیر و طارم و تیم</p></div>
<div class="m2"><p>زیر و بالا ز آب چشم یتیم</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>ای بسا رفته ملک پر هنران</p></div>
<div class="m2"><p>زار زار از دعای بی‌پدران</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>آنچه یک پیرزن کند به سحر</p></div>
<div class="m2"><p>نکند صد هزار تیر و تبر</p></div></div>