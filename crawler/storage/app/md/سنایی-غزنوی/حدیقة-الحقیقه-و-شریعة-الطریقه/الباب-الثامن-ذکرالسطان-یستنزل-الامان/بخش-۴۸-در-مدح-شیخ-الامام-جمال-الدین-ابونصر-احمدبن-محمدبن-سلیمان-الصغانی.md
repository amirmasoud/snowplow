---
title: >-
    بخش ۴۸ - در مدح شیخ‌الامام جمال‌الدّین ابونصر احمدبن محمّدبن سلیمان الصغانی
---
# بخش ۴۸ - در مدح شیخ‌الامام جمال‌الدّین ابونصر احمدبن محمّدبن سلیمان الصغانی

<div class="b" id="bn1"><div class="m1"><p>بعد او خواجهٔ امام امین</p></div>
<div class="m2"><p>مَفخر شرع و یار و ناصر دین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تازه از لفظ او مسلمانی</p></div>
<div class="m2"><p>به نژاد و نسب سلیمانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>صدر اسلام و دین بدو تازه</p></div>
<div class="m2"><p>هنر و علم او بی‌اندازه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>علم او همچو آب شوینده</p></div>
<div class="m2"><p>نام او همچو باد پوینده</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>علم او وعدهٔ سماعیلی</p></div>
<div class="m2"><p>جمع او شمع طارم نیلی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هرکه از عقل رنگ دارد و بوی</p></div>
<div class="m2"><p>بستهٔ اوست همچو دستنبوی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ذوق او جان فروز اقرانست</p></div>
<div class="m2"><p>پند او بند سوز دیوانست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سیّما در ره حقیقت و شرع</p></div>
<div class="m2"><p>نیست اصلی قدیم‌تر زین فرع</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>علمشان را ندیده‌ام به یقین</p></div>
<div class="m2"><p>وارثی حق‌تر از جمال‌الدّین</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آنکه تا یافت ز آسمان مسند</p></div>
<div class="m2"><p>یک زمینست اجمد و احمد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شربت شرع دین ز باغ رسول</p></div>
<div class="m2"><p>از نسیم قبول کرده قبول</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>همچو دین وعده‌ش از تخلّف دور</p></div>
<div class="m2"><p>چون خرد لطفش از تکلّف دور</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>عالم علم را گشاده دری</p></div>
<div class="m2"><p>که جز او کم تواند آن دگری</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>شد حرام از برای دُر سفتن</p></div>
<div class="m2"><p>جز ورا برملا سخن گفتن</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>جان قرآن همی بیفروزد</p></div>
<div class="m2"><p>تا ازو نکته‌ای درآموزد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>عشق پنهان ز زحمت خاطر</p></div>
<div class="m2"><p>گفته با ذوق مغز جانش سِر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>آن بگفته دل از زبان سروش</p></div>
<div class="m2"><p>واین چشیده تن از ولایت گوش</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>سخنش اندک و ملیح ملیح</p></div>
<div class="m2"><p>همچو توقیع دوربین فصیح</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>با بد و نیک بی‌ریا و شکی</p></div>
<div class="m2"><p>اوّل و آخرش یکی چو یکی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>وقت آن کو کمان به خاطر خویش</p></div>
<div class="m2"><p>زه کند از برای ده درویش</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>زه کند تیر چرخ بر گردون</p></div>
<div class="m2"><p>زه کند سنگ خاره بر هامون</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>اشهب نطق او چو بشتابد</p></div>
<div class="m2"><p>یارب این نکته‌ها که در یابد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>کانگهی کو بیان یاسین کرد</p></div>
<div class="m2"><p>جبرئیلش ز سدره تحسین کرد</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>شاد باش ای امام هردو فریق</p></div>
<div class="m2"><p>دیر زی ای گزین هر دو طریق</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>تا تو بر منبری فلک دونست</p></div>
<div class="m2"><p>من نگویم که استوا چونست</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>دست معنی چو گرد معنی تاخت</p></div>
<div class="m2"><p>زال زر دید و زال زار شناخت</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ای که می‌پرسی از طریق مری</p></div>
<div class="m2"><p>نکند این سخن جواب کَری</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>که چه گوید همی براین کرسی</p></div>
<div class="m2"><p>باز گویم اگر ز من پرسی</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>تا چراغ سخاش تابان گشت</p></div>
<div class="m2"><p>همچو پروانه جان شتابان گشت</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>جان آن کو چراغ جودش دید</p></div>
<div class="m2"><p>زار می‌سوخت و خوش همی خندید</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>گردد از بهر رتبت و جاهش</p></div>
<div class="m2"><p>وز پی خاک‌روب درگاهش</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>فلک هفتم از زحل خالی</p></div>
<div class="m2"><p>چارارکان ز پنج حس حالی</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>چندگویی که وصف خواجه بگوی</p></div>
<div class="m2"><p>پای در نه به وصف و دست بشوی</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>در دو بیتت به مختصر کاری</p></div>
<div class="m2"><p>باز گویم که مرد هشیاری</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>خواجه در راه عقل و جان ز قیاس</p></div>
<div class="m2"><p>در سرای غرور و جمع اناس</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>به سخن هم کمان و هم تیرست</p></div>
<div class="m2"><p>به صفت هم مرید و هم پیرست</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>آن کمان پدید و تیر نهان</p></div>
<div class="m2"><p>آن مرید خدا و پیر جهان</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>خاک جسمش ز مرتبت صلصال</p></div>
<div class="m2"><p>آب چشمش ز معرفت سلسال</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>نطق او از جهان جاویدست</p></div>
<div class="m2"><p>دور و نزدیک همچو خورشیدست</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>زادهٔ ذهن او به صفوت نور</p></div>
<div class="m2"><p>حلقه و عقد گوش و گردن حور</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>همچو اندر خیال عامی حور</p></div>
<div class="m2"><p>سخن سهل او هم ایدر و دور</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>تا چو تو میزبان نو دارد</p></div>
<div class="m2"><p>عیسی و خر غذا و جو دارد</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>جان پاکش سخن گشاده برو</p></div>
<div class="m2"><p>جان درو معنیی نهاده نه او</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>صیت او در عراق و مصر و دمشق</p></div>
<div class="m2"><p>هست غمّاز دوست روی چو عشق</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>چون در اعراب اسم حرف شود</p></div>
<div class="m2"><p>واندر احکام فعل صرف شود</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>ور به بصره حدیث نحو کند</p></div>
<div class="m2"><p>بصره از اهل نحو محو کند</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>گشت در باغ برّ یزدانی</p></div>
<div class="m2"><p>از برای دل مسلمانی</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>غذی بیخ شرع گفتارش</p></div>
<div class="m2"><p>میوهٔ شاخ عقل کردارش</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>دل مر او را نموده راه صواب</p></div>
<div class="m2"><p>دین مر او را جمال داده خطاب</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>تا ابد زانکه جانش کان دارد</p></div>
<div class="m2"><p>روغن اندر چراغدان دارد</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>با امل عمر او چو پیمان بست</p></div>
<div class="m2"><p>ز انتقال زوال حال برست</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>از پی باغ شرع چون حیدر</p></div>
<div class="m2"><p>آب در جوی اوست از کوثر</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>هست خوی رسول دلجویش</p></div>
<div class="m2"><p>هست آب خدای در جویش</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>رنگ او بهر نکهت طیبش</p></div>
<div class="m2"><p>کرده تهذیب عشق تذهیبش</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>هرکه یک شب به کوی او بگذشت</p></div>
<div class="m2"><p>در سخن مقتدای عالم گشت</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>هرکه روزی به دست دل درماند</p></div>
<div class="m2"><p>نسخهٔ دلبری ز رویش خواند</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>چون به مجلس نشاط گفت کند</p></div>
<div class="m2"><p>طاق خورشید چرخ جفت کند</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>از پی چشم بد به روضهٔ نور</p></div>
<div class="m2"><p>دل به جای سپند سوخته حور</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>او همی سرّ رمز به داند</p></div>
<div class="m2"><p>قاصد از حال راه به داند</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>گویی آمد ز خانه و کویش</p></div>
<div class="m2"><p>خوی خوش بر نظارهٔ رویش</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>لب چون لاله خشک و تر نرگس</p></div>
<div class="m2"><p>بینی آنگه که ختم شد مجلس</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>عقلا بازگشته طوطی‌وار</p></div>
<div class="m2"><p>خلق چون حلق بلبل از گفتار</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>چشم پُر دُر ز درّ سفتهٔ او</p></div>
<div class="m2"><p>گوشها پر گهر ز گفتهٔ او</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>عیسی جان مرده خاک درش</p></div>
<div class="m2"><p>ملک‌الموت قهر زنده فرش</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>گاه تقریر و وقت تدبیرش</p></div>
<div class="m2"><p>صبح خوش خندد از تباشیرش</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>شد برای امید جان و خرد</p></div>
<div class="m2"><p>آنکه او را به جان و دیده خرد</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>دل ز دینش همیشه در ارمست</p></div>
<div class="m2"><p>چه ارم زیر گلبن کرمست</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>باغ ایمانش را ز چشمهٔ روی</p></div>
<div class="m2"><p>تا ابد آب رویش اندر جوی</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>خود چه دیدند اهل غزنی ازو</p></div>
<div class="m2"><p>چه شنیدند اهل معنی ازو</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>که خود او زان نکت که در دل اوست</p></div>
<div class="m2"><p>وز ره لطف غیب حاصل اوست</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>از هزاران هزار دُرّ نهفت</p></div>
<div class="m2"><p>چکنم من که خود یکی بنگفت</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>در خور عقل عامه می‌گوید</p></div>
<div class="m2"><p>به سخن گَرد نامه می‌شوید</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>سخنش با نوا و زینت و برگ</p></div>
<div class="m2"><p>خاص بندیست عام‌گیر چو مرگ</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>وارث مصطفی به علم و وفا</p></div>
<div class="m2"><p>نایب مرتضی به علم و سخا</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>رنج ما را از آن دل خوش خوی</p></div>
<div class="m2"><p>داده ابر سخا به عشرت خوی</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>برگرفته به قوّت ایمان</p></div>
<div class="m2"><p>دو گروهی ز عالم تن و جان</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>شده در راه حکمت و تدریس</p></div>
<div class="m2"><p>برتر از یونس و ارسطالیس</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>یافته فلسفه شریعت و ره</p></div>
<div class="m2"><p>از پی فرّ دین و فلّ سفه</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>برگرفته به عقل از امکان</p></div>
<div class="m2"><p>فتنه از پنج حس و چار ارکان</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>خاک شوره کند شراب از خلق</p></div>
<div class="m2"><p>آب دریا کند گلاب از خلق</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>آری آنکس که صبر پیشه کند</p></div>
<div class="m2"><p>پیشهٔ شیر زیر تیشه کند</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>از بسی صبر کرده آتش صبر</p></div>
<div class="m2"><p>عذب همچون سرشک دیدهٔ ابر</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>از دورن تو هست از پی دین</p></div>
<div class="m2"><p>صدهزار آسمان فزون ز زمین</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>خلق را شرط شرع او ابدیست</p></div>
<div class="m2"><p>زانکه با عزّ پردهٔ احدیست</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>داد و دین با خلل نکرده ز کبر</p></div>
<div class="m2"><p>دال احمد بدل نکرده ز کبر</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>ای امامی که از پی زینت</p></div>
<div class="m2"><p>منبر تست قابِ قوسینت</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>پردهٔ چرخ را پدید آورد</p></div>
<div class="m2"><p>قفل احکام را کلید آور</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>سرِ صندوق صدق را بگشای</p></div>
<div class="m2"><p>خلق را سرّ لطف حق بنمای</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>از سخا و فصاحت از سرِ دین</p></div>
<div class="m2"><p>پای برنه به فرق علّیّین</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>معنیی بخش معن زائده را</p></div>
<div class="m2"><p>قسم ده جان قُس ساعده را</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>تا به انفاس اوش سر کاریست</p></div>
<div class="m2"><p>مر سخن را چه تیز بازاریست</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>هر سخن را که نقش جان دیدم</p></div>
<div class="m2"><p>داغ نطقش به زیر ران دیدم</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>همه گویندگان روی زمین</p></div>
<div class="m2"><p>پیش نطق تو ای جمال‌الدّین</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>بی‌غرض پندم ار بهش باشند</p></div>
<div class="m2"><p>چو نکو باشد ار خمش باشند</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>هرچه اندر جهان سخن کوشند</p></div>
<div class="m2"><p>نزد رمز تو حلقه در گوشند</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>در زمان تو ای امیر سخن</p></div>
<div class="m2"><p>شوخ چشمی بُوَد سخن گفتن</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>گرچه الماس نطق می‌سفتند</p></div>
<div class="m2"><p>با بیان تو مفتیان زفتند</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>ظرف حرف تو مخّ تفسیرست</p></div>
<div class="m2"><p>هرچه جز آن مگر تف سیرست</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>تا که در سرّ ضمیر ارکانست</p></div>
<div class="m2"><p>شمع جمع تو شه ره جانست</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>روح را تازه میزبانی تو</p></div>
<div class="m2"><p>غذی صدهزار جانی تو</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>قالبست این جهان و جانش تویی</p></div>
<div class="m2"><p>همچو شخصست دین روانش تویی</p></div></div>
<div class="b" id="bn102"><div class="m1"><p>به وجود تو خلق از آن شادست</p></div>
<div class="m2"><p>عمر با دانش تو همزادست</p></div></div>
<div class="b" id="bn103"><div class="m1"><p>حالت از اصل سوز فرع آمد</p></div>
<div class="m2"><p>قالت از درد ساز شرع آمد</p></div></div>
<div class="b" id="bn104"><div class="m1"><p>دوستان را صبوح روحی تو</p></div>
<div class="m2"><p>جان جان را همه فتوحی تو</p></div></div>
<div class="b" id="bn105"><div class="m1"><p>جود اگر نام تو نبردستی</p></div>
<div class="m2"><p>زود همچون عدوت مُردستی</p></div></div>
<div class="b" id="bn106"><div class="m1"><p>میزبان دشمنانت را مرگست</p></div>
<div class="m2"><p>با چنین دعوتی کرا برگست</p></div></div>
<div class="b" id="bn107"><div class="m1"><p>تن که یکدم خلاف تو پذرفت</p></div>
<div class="m2"><p>جانش گوید دلت ز من بگرفت</p></div></div>
<div class="b" id="bn108"><div class="m1"><p>تف آن دم نرفته تا لب او</p></div>
<div class="m2"><p>مرگ در جل کشیده مرکب او</p></div></div>
<div class="b" id="bn109"><div class="m1"><p>مرگ خوردست بد سگالش را</p></div>
<div class="m2"><p>تا نبیند کمال حالش را</p></div></div>
<div class="b" id="bn110"><div class="m1"><p>چون خرد عمر دوستانش باز</p></div>
<div class="m2"><p>در لقا و بقاش باد دراز</p></div></div>
<div class="b" id="bn111"><div class="m1"><p>گوشت عالم به زهر اگر خبرست</p></div>
<div class="m2"><p>لیکن آنِ تو آزموده‌ترست</p></div></div>
<div class="b" id="bn112"><div class="m1"><p>هرکه در سر چراغ دین افروخت</p></div>
<div class="m2"><p>سبلت پف کنانش پاک بسوخت</p></div></div>
<div class="b" id="bn113"><div class="m1"><p>سخت بسیار کس بکوشیدند</p></div>
<div class="m2"><p>کسوت صورتت نپوشیدند</p></div></div>
<div class="b" id="bn114"><div class="m1"><p>خلعت هرکه آن سری باشد</p></div>
<div class="m2"><p>حسد ای خواجه از خری باشد</p></div></div>
<div class="b" id="bn115"><div class="m1"><p>به ثناهاش بُد سنا منسوب</p></div>
<div class="m2"><p>لیک نامحرمان شده محجوب</p></div></div>
<div class="b" id="bn116"><div class="m1"><p>همه مستورگان عالم راز</p></div>
<div class="m2"><p>با ضمیر تو رخ پر آب نیاز</p></div></div>
<div class="b" id="bn117"><div class="m1"><p>هرکسی اسب رمز با تو بتاخت</p></div>
<div class="m2"><p>چون نبد مرد مرد را چه شناخت</p></div></div>
<div class="b" id="bn118"><div class="m1"><p>پرده‌داری سرای غیرت را</p></div>
<div class="m2"><p>حیرت افتاد از تو حیرت را</p></div></div>
<div class="b" id="bn119"><div class="m1"><p>خصم از آن آمدند هر خامت</p></div>
<div class="m2"><p>نیست کس واقف از الف لامت</p></div></div>
<div class="b" id="bn120"><div class="m1"><p>در کمال حدود و لطف و نواخت</p></div>
<div class="m2"><p>بکر ماندی و کس ترا نشناخت</p></div></div>
<div class="b" id="bn121"><div class="m1"><p>هرکه او با یزید نفس بساخت</p></div>
<div class="m2"><p>حالت بایزید را چه شناخت</p></div></div>
<div class="b" id="bn122"><div class="m1"><p>در سخا مرد با خطیری تو</p></div>
<div class="m2"><p>در سخن فرد بی‌‌نظیری تو</p></div></div>
<div class="b" id="bn123"><div class="m1"><p>از کمالت فزوده‌ای دین را</p></div>
<div class="m2"><p>شادی جان اهل غزنین را</p></div></div>
<div class="b" id="bn124"><div class="m1"><p>گرچه بر نقش حرف غزنین است</p></div>
<div class="m2"><p>چون قدم سای تست عزبین است</p></div></div>
<div class="b" id="bn125"><div class="m1"><p>حضرت شه بهشت خلد ارزد</p></div>
<div class="m2"><p>بی‌وجود تو حبّه‌ای نرزد</p></div></div>
<div class="b" id="bn126"><div class="m1"><p>با لقای تو ای جمال‌الدّین</p></div>
<div class="m2"><p>نیست غزنین بهشت نقدست این</p></div></div>
<div class="b" id="bn127"><div class="m1"><p>مثل تو با تو در جهان ضمیر</p></div>
<div class="m2"><p>خود قیاسیست به ز سوسن و سیر</p></div></div>
<div class="b" id="bn128"><div class="m1"><p>زادهٔ نثر تست برهانم</p></div>
<div class="m2"><p>شکر این موهبت نکو دانم</p></div></div>
<div class="b" id="bn129"><div class="m1"><p>نظم من بهر نثر تو بودست</p></div>
<div class="m2"><p>جان جانها از آن برآسودست</p></div></div>
<div class="b" id="bn130"><div class="m1"><p>خرده نبود بضاعت زیره</p></div>
<div class="m2"><p>سوی کرمان بریم برخیره</p></div></div>
<div class="b" id="bn131"><div class="m1"><p>گهر مدحت تو دانم سُفت</p></div>
<div class="m2"><p>همه دانم ولی نیارم گفت</p></div></div>
<div class="b" id="bn132"><div class="m1"><p>دوستان در نشاط لطف مست</p></div>
<div class="m2"><p>دشمنان بر بساط قهرت پست</p></div></div>
<div class="b" id="bn133"><div class="m1"><p>تن همت به جود تو کامل</p></div>
<div class="m2"><p>جان حکمت به جدّ تو حامل</p></div></div>
<div class="b" id="bn134"><div class="m1"><p>ای وجودت ز لطف حق اثری</p></div>
<div class="m2"><p>باز جودت ز حسن او خبری</p></div></div>
<div class="b" id="bn135"><div class="m1"><p>هرکه از حق به سوی او نظریست</p></div>
<div class="m2"><p>در دل او ز مهر تو اثریست</p></div></div>
<div class="b" id="bn136"><div class="m1"><p>تو طبیب مفسّری دگرست</p></div>
<div class="m2"><p>تو حبیبی مذکّری دگرست</p></div></div>
<div class="b" id="bn137"><div class="m1"><p>محرم سرِّ انبیایی تو</p></div>
<div class="m2"><p>مدد قوت اصفیایی تو</p></div></div>
<div class="b" id="bn138"><div class="m1"><p>ای ترا حق نموده راه صواب</p></div>
<div class="m2"><p>ای ترا دین جمال کرده خطاب</p></div></div>
<div class="b" id="bn139"><div class="m1"><p>حکمتت اهل استقامت گشت</p></div>
<div class="m2"><p>حجّتت حالی قیامت گشت</p></div></div>
<div class="b" id="bn140"><div class="m1"><p>نزد نطقت سخن یتیم بماند</p></div>
<div class="m2"><p>پیش جودت سخا عقیم بماند</p></div></div>
<div class="b" id="bn141"><div class="m1"><p>هرکه نشنید از تو او چه شنید</p></div>
<div class="m2"><p>دیده‌ای کو ترا ندید چه دید</p></div></div>
<div class="b" id="bn142"><div class="m1"><p>منزل رمزها بریدم من</p></div>
<div class="m2"><p>چون تو و چون خودی ندیدم من</p></div></div>
<div class="b" id="bn143"><div class="m1"><p>حاسدان را تو گو زنخ می‌زن</p></div>
<div class="m2"><p>ختم شد نظم و نثر بر تو و من</p></div></div>
<div class="b" id="bn144"><div class="m1"><p>راز را مستمع بیان تو باد</p></div>
<div class="m2"><p>آز را مصطنع بنان تو باد</p></div></div>
<div class="b" id="bn145"><div class="m1"><p>باد تا هست اختران را سیر</p></div>
<div class="m2"><p>عرض تو عرصهٔ عوارض خیر</p></div></div>