---
title: >-
    بخش ۳۶ - صفة اصرار الاعداء والباغین لعنهم‌اللّٰه
---
# بخش ۳۶ - صفة اصرار الاعداء والباغین لعنهم‌اللّٰه

<div class="b" id="bn1"><div class="m1"><p>آدمی چون بداشت دست از صیت</p></div>
<div class="m2"><p>هرچه خواهی بکن که فاصنع شیت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هرکه راضی شود به کردهٔ زشت</p></div>
<div class="m2"><p>نزد آنکس چه دوزخ و چه بهشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مرد عاقل برآن کسی خندد</p></div>
<div class="m2"><p>کز پی خویش نار بپسندد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دین به دنیا بخیره بفروشد</p></div>
<div class="m2"><p>نکند نیک و در بدی کوشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خیره راضی شود به خون حسین</p></div>
<div class="m2"><p>که فزون بود وقعش از ثقلین</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آنکه را این خبیث خال بُوَد</p></div>
<div class="m2"><p>مؤمنان را کی ابن خال بُوَد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>من از این ابن‌خال بیزارم</p></div>
<div class="m2"><p>کز پدر نیز هم دل آزارم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پس تو گویی یزید میر منست</p></div>
<div class="m2"><p>عمروعاص پلید پیرِ منست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آنکه را عمروعاص باشد پیر</p></div>
<div class="m2"><p>با یزید پلید باشد میر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مستحق عذاب و نفرین است</p></div>
<div class="m2"><p>بد ره و بد فعال و بد دین است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>لعنت دادگر برآنکس باد</p></div>
<div class="m2"><p>که مر او را کند به نیکی یاد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>من نیم دوستدار شمر و یزید</p></div>
<div class="m2"><p>زان قبیله منم به عهد بعید</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>هرکه راضی شود به بد کردن</p></div>
<div class="m2"><p>لعنتش طوق گشت در گردن</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>از سنایی به جان میر حسین</p></div>
<div class="m2"><p>صد هزاران ثناست دایم دَین</p></div></div>