---
title: >-
    بخش ۲ - اندر بدایت کمال نبوّت
---
# بخش ۲ - اندر بدایت کمال نبوّت

<div class="b" id="bn1"><div class="m1"><p>آدم و آنکه شمّت جان داشت</p></div>
<div class="m2"><p>پای دامانش بر گریبان داشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آدم از مادر عدم زاده</p></div>
<div class="m2"><p>او چراغی بدو فرستاده</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>غیب یزدان نهاده در دل او</p></div>
<div class="m2"><p>آب حیوان سرشته در گِل او</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دیدهٔ او به گاه منزل خواب</p></div>
<div class="m2"><p>تا سوی عرش برگرفته نقاب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جان او بوده در طریقت حق</p></div>
<div class="m2"><p>گوهر حضرت حقیقت حق</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دیده از چشم دل به نور احد</p></div>
<div class="m2"><p>از دریچهٔ ازل سرای ابد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کرده از بر به مکتب مردی</p></div>
<div class="m2"><p>سورتِ سیرتِ جوانمردی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>من نگویم که غیب‌دان بُد او</p></div>
<div class="m2"><p>گرچه از چشمها نهان بُد او</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>غیب‌دان در مشیمهٔ کن و کان</p></div>
<div class="m2"><p>نیست جز خالق زمین و زمان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نه زبانش به وقت نشر حکم</p></div>
<div class="m2"><p>گفت لو تعلمون ما اعلم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>زانکه بنمود حق به جان و دلش</p></div>
<div class="m2"><p>رمزهای حقیقت ازلش</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>رفته از اقتداش تا عیّوق</p></div>
<div class="m2"><p>زشت و نیکو و لاحق و مسبوق</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>پادشا بر جهان آدم اوست</p></div>
<div class="m2"><p>راهبر سوی ملک اعظم اوست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>طینتش زینتِ جهان آمد</p></div>
<div class="m2"><p>ساحتش راحتِ روان آمد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چون زبان را به نامه کرد روان</p></div>
<div class="m2"><p>تا شود کسری آرمیده روان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>به شقاوت چو رشد کرد رها</p></div>
<div class="m2"><p>از سعادت به غی بماند جدا</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چونکه عینش فتاد بر عنوان</p></div>
<div class="m2"><p>زهر شد نوش جان نوشروان</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>شرع او چون نشست بر عیّوق</p></div>
<div class="m2"><p>شد گسسته عنان عزّ یعوق</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>شد ز تابش نشانهٔ کسری</p></div>
<div class="m2"><p>سر ایوان طارم کسری</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>پای کوبان عروس عشق ازل</p></div>
<div class="m2"><p>سرنگون اوفتاده لات و هبل</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>داده دادش همه خلایق را</p></div>
<div class="m2"><p>عزّ معشوق و ذلّ عاشق را</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ملک تن را خرابی از کینش</p></div>
<div class="m2"><p>ملک جان را عمارت از دینش</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>جزع و لعلش ز بهر عزّ و شرف</p></div>
<div class="m2"><p>گوشها کرده همچو گوش صدف</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>روز تا روشنست و شب سیهست</p></div>
<div class="m2"><p>زلف و رویش شفیع هر گنهست</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>از پی زقّه دادن از لب او</p></div>
<div class="m2"><p>وز پی زادگان مَرکب او</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>وز پی صورت و دل و جانش</p></div>
<div class="m2"><p>پیش حکم خطاب و فرمانش</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>عقل کل بوده در دبستانش</p></div>
<div class="m2"><p>نفس کل گاهواره جنبانش</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>جوهر این سرای را عرض او</p></div>
<div class="m2"><p>لیک عرض بهشت را غرض او</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>دیو را بوده روز بدر و حنین</p></div>
<div class="m2"><p>صورتش سورهٔ معوّذتین</p></div></div>