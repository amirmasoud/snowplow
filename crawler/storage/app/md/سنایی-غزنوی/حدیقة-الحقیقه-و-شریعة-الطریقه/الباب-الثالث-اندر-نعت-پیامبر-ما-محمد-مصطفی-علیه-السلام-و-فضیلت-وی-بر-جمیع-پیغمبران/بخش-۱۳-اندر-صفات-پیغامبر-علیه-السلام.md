---
title: >-
    بخش ۱۳ - اندر صفات پیغامبر علیه‌السلام
---
# بخش ۱۳ - اندر صفات پیغامبر علیه‌السلام

<div class="b" id="bn1"><div class="m1"><p>برده بر بام آسمان رختش</p></div>
<div class="m2"><p>سایهٔ بخت و پایهٔ تختش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صورتی را که بود اهل قبول</p></div>
<div class="m2"><p>کردش از صورت طلب مشغول</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نسبت از عقل آن جهانی داشت</p></div>
<div class="m2"><p>هم معالی و هم معانی داشت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دنیا آورده در قدم او بود</p></div>
<div class="m2"><p>غرض حکمت قِدم او بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کعبهٔ بادیه عدم او بود</p></div>
<div class="m2"><p>عالم علم را علم او بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در جبلت جلالت او را بود</p></div>
<div class="m2"><p>با رسالت بسالت او را بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در رسالت تمام بود تمام</p></div>
<div class="m2"><p>در کرامت امام بود امام</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چمنی با کمال بی‌شرکی</p></div>
<div class="m2"><p>شجری پر ز برگ بی‌برگی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>روی او خوب و رای او ثاقب</p></div>
<div class="m2"><p>ازلش خوانده حاشر و عاقب</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>صحن او شرع و عقل او صاحی</p></div>
<div class="m2"><p>خوانده محیی اعظمش ماحی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>صیت صوتش برفته در عالم</p></div>
<div class="m2"><p>نه پرش بوده در روش نه قدم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>وصف این حال مصطفی دارد</p></div>
<div class="m2"><p>بوی خوش بال و پر کجا دارد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>صاد و دال آب داد صادق را</p></div>
<div class="m2"><p>عین و شین عشوه داد عاشق را</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>هرچه از تر و خشک بوی آورد</p></div>
<div class="m2"><p>این سپید سیاه روی آورد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>کشته و زاده‌اند ارکانش</p></div>
<div class="m2"><p>پدر عقل و مادر جانش</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>مایه و سایهٔ زمین او بود</p></div>
<div class="m2"><p>گوهر شب چراغ دین او بود</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>مفخر جمله انبیا او بود</p></div>
<div class="m2"><p>خسر میر مرتضی او بود</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>از دورن رفتنش نداشته باز</p></div>
<div class="m2"><p>پرده‌دار سرای پردهٔ راز</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چون برآمد ز شاهراه عدم</p></div>
<div class="m2"><p>نو رهی خواست مصطفی ز آدم</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>آدمش نورهی چو پیش کشید</p></div>
<div class="m2"><p>جان او جان اصفیا بخشید</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>منهج صدق در دو ابرو داشت</p></div>
<div class="m2"><p>مدرج عشق در دو گیسو داشت</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>دید آدم که مایه‌دار قدم</p></div>
<div class="m2"><p>مردمی می‌زند ز عشق دم</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>عقل کل زو گرفته حکمت و رای</p></div>
<div class="m2"><p>سایه از آفتاب یابد پای</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>پیش آن کو ز اصل بد خود بود</p></div>
<div class="m2"><p>بسته چشم و گشاده ابرو بود</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>شرع رادست عقل کی سنجد</p></div>
<div class="m2"><p>عشق در ظرف حرف کی گنجد</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>آنکه شب را سپید داند کرد</p></div>
<div class="m2"><p>از تن عقل برنیارد گرد</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>چیست جز شرع او به خانهٔ راز</p></div>
<div class="m2"><p>بر قبای بقا طِراز طَراز</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>رخ او میزبان صادق بود</p></div>
<div class="m2"><p>زلفش اجری ده منافق بود</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بود بهتر ز جملهٔ عالم</p></div>
<div class="m2"><p>بود خشنود ازو بدو آدم</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>رخ و زلفش صلاح عالم بود</p></div>
<div class="m2"><p>خَلق و خُلقش وجود آدم بود</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>غرض او بُد ز گردش عالم</p></div>
<div class="m2"><p>خوانده او و طفیل او آدم</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>یافت تشریف سجدهٔ ملکوت</p></div>
<div class="m2"><p>نیز تشریف بذر قوت به قوت</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>زان دل زنده و زبان فصیح</p></div>
<div class="m2"><p>دل یارانش چون وثاق مسیح</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>جمله یاران او ز دانش و علم</p></div>
<div class="m2"><p>کیسه‌ها دوخته ز حکمت و حلم</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>تا نبیند ز سائلان تشویر</p></div>
<div class="m2"><p>همه پیش از نیاز گفته بگیر</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>زان درختی که بیخ تبجیلست</p></div>
<div class="m2"><p>شاخ تنزیل و میوه تاویلست</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>مولدش بر دعای مظلومان</p></div>
<div class="m2"><p>موردش بر ندای معصومان</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>زاد کم توشگان قناعت او</p></div>
<div class="m2"><p>قوّت امتان شفاعت او</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>ملتبس درد انبیا ز گلش</p></div>
<div class="m2"><p>مقتبس نور اولیا ز دلش</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>اول روز دین شهنشاه او</p></div>
<div class="m2"><p>آخر روز جان دلخواه او</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>خلقتش بر صلاح بخل و نثار</p></div>
<div class="m2"><p>خلق را نیش بخش و نوش گوار</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>زو فلک‌وار مسجد و مؤمن</p></div>
<div class="m2"><p>زو کنشت و کلیسیا ایمن</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>همه سادات دین ازو مرحوم</p></div>
<div class="m2"><p>همه نامحرمان ازو محروم</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>مرشد طبع سوی عقل از می</p></div>
<div class="m2"><p>داعی عقل سوی رشد از غی</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>چون محمّد بگفتی ای درویش</p></div>
<div class="m2"><p>شو به نزدیک عقل دوراندیش</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>تا ترا عقل هم ز روی صواب</p></div>
<div class="m2"><p>پشت پایی زند مگر در خواب</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>گویدت معنی محمّد راست</p></div>
<div class="m2"><p>محو و مدّست و هر دو برّ و عطاست</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>محو کفر از سرای پردهٔ دین</p></div>
<div class="m2"><p>مدّ اطناب شرع تا پروین</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>هم ستاننده از که از احمق</p></div>
<div class="m2"><p>هم دهنده به که به صاحب حق</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>آنکه را از غذای او نورست</p></div>
<div class="m2"><p>از غذای زمانه مهجورست</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>نقش نامش به گاه دانش و رای</p></div>
<div class="m2"><p>از در غیب و ریب قفل گشای</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>خلق بندهٔ خدای و چاکر او</p></div>
<div class="m2"><p>قبله‌شان او و قُبله بر در اوی</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>هرکه یک دم نبوده بر خوانش</p></div>
<div class="m2"><p>عقل او خون گرسته بر جانش</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>طینتی نه ازو مخمّرتر</p></div>
<div class="m2"><p>سالکی نه ازو مشمّرتر</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>اوست بر کفر چون گرفت شتاب</p></div>
<div class="m2"><p>نور توزی گداز چون مهتاب</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>ملک دین را معین و ناصر اوست</p></div>
<div class="m2"><p>تخت اشراف را عناصر اوست</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>در ره مصلحت مکرّم اوست</p></div>
<div class="m2"><p>در طریق خدا معظّم اوست</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>هرگز از بهر ملک و ملک نجس</p></div>
<div class="m2"><p>پای بند هوا نبوده چو خس</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>از همه خلق و از همه اغیار</p></div>
<div class="m2"><p>چشم بردوخته چو باز شکار</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>از پی شرع در جهان خدای</p></div>
<div class="m2"><p>جان خاموش او زبان خدای</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>نه زبانی که گوشتین باشد</p></div>
<div class="m2"><p>بل زبانی که گوش تین باشد</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>نطق در گوش عاریت باشد</p></div>
<div class="m2"><p>قلب تین چیست کو نیت باشد</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>نیت پاک چون ز دل خیزد</p></div>
<div class="m2"><p>نقطهٔ شرک را برانگیزد</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>معنی گل ز تین چو حاصل شد</p></div>
<div class="m2"><p>اندرونش چو جان همه دل شد</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>چون همه دل گرفت و شافی شد</p></div>
<div class="m2"><p>گوش او پر ز شیر صافی شد</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>روی او چون به قلب تین باشد</p></div>
<div class="m2"><p>رای او در عمل متین باشد</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>جان گل پیر می‌شود ز قعود</p></div>
<div class="m2"><p>خون دل شیر می‌شود به صعود</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>بازگشتم به نعت سید قاب</p></div>
<div class="m2"><p>برگرفتم ز روی دعد نقاب</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>تو ازو همچو شیر در بیشه</p></div>
<div class="m2"><p>من ازو همچو دل در اندیشه</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>دل ز اندیشه روشن و عالیست</p></div>
<div class="m2"><p>بیشهٔ دین ز شیر شر خالیست</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>فکرت اندر صنایع صمدی</p></div>
<div class="m2"><p>در نبوت ودایع احدی</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>گرچه در خَلق شکل گوساله است</p></div>
<div class="m2"><p>به ز تکرار و ذکر صد ساله است</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>اخترش قهرمان راه ملک</p></div>
<div class="m2"><p>عصمتش پاسبان شاه فلک</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>دست گرد جهان برآورده</p></div>
<div class="m2"><p>هرچه جز حق همه هدر کرده</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>فهمش اندر بصیرت امکان</p></div>
<div class="m2"><p>برتر است از قیاس و استحسان</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>منبع رعب درد و بازو داشت</p></div>
<div class="m2"><p>منهج صدق در دو ابرو داشت</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>هرکه بگرفت پای اهل بصر</p></div>
<div class="m2"><p>هرگز از ذلّ نیاید اندر سر</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>چون سوی راه بیخودی پوید</p></div>
<div class="m2"><p>نقش خود زاب روی خود شوید</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>نزد آن خواجهٔ جهان نهفت</p></div>
<div class="m2"><p>رفتم و دید و بازگشت و بگفت</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>نه چنان رو که شیر در بیشه</p></div>
<div class="m2"><p>آن چنان رو که دل در اندیشه</p></div></div>