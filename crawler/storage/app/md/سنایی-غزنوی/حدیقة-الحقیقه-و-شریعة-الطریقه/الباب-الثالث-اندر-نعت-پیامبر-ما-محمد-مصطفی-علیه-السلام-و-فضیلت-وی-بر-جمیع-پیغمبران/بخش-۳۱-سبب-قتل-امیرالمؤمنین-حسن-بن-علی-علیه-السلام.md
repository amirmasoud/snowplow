---
title: >-
    بخش ۳۱ - سبب قتل امیرالمؤمنین حسن‌بن علی علیه‌السّلام
---
# بخش ۳۱ - سبب قتل امیرالمؤمنین حسن‌بن علی علیه‌السّلام

<div class="b" id="bn1"><div class="m1"><p>کرد خصمان برو جهان فراخ</p></div>
<div class="m2"><p>تنگ همچون درونگه درواخ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بی‌سبب خصم قصد جانش کرد</p></div>
<div class="m2"><p>او بدانست و زان امانش کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بار دیگر به قصد او برخاست</p></div>
<div class="m2"><p>بی‌گناهی ورا بکشتن خواست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پس سیم بار عزم کرد درست</p></div>
<div class="m2"><p>شربت زهر همچو بار نخست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>راست کرد و بداد آن ناپاک</p></div>
<div class="m2"><p>که جهان باد از چنان زن پاک</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>صد و هفتاد و اَند پاره جگر</p></div>
<div class="m2"><p>به در انداخت زان لب چو شکر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جان بداد اندر آن غم و حسرت</p></div>
<div class="m2"><p>باد بر جام خصم او لعنت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گفت با او ستوده میر حسین</p></div>
<div class="m2"><p>آن مرا اشراف را چو زینت و زین</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زهر جان مر ترا که داد بگوی</p></div>
<div class="m2"><p>گفت غمز از حسن بود نه نکو</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>جدِّ من مصطفی امان زمان</p></div>
<div class="m2"><p>پدرم مرتضی امین جهان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>جدِّهٔ من خدیجه زَین زمان</p></div>
<div class="m2"><p>مادرم فاطمه چراغ جنان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>جمله بودند از خیانت و غمز</p></div>
<div class="m2"><p>پاک و پاکیزه خاطر و دل و مغز</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>من هم از بطن و ظَهر ایشانم</p></div>
<div class="m2"><p>گرچه جمع از غم پریشانم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نه کنم غمز و نه بُوَم غمّاز</p></div>
<div class="m2"><p>خود خدا داند آخر و آغاز</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>هست دانا به باطن و ظاهر</p></div>
<div class="m2"><p>چون توانا به اوّل و آخر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>آنکه فرمود و آنکه داد رضا</p></div>
<div class="m2"><p>خود جزا یابد او به روز جزا</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ور مرا روز حشر ایزد بار</p></div>
<div class="m2"><p>بدهد در جوارِ جنّت بار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>نروم در بهشت جز آنگاه</p></div>
<div class="m2"><p>که نهد در کفم کفِ بدخواه</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>از چه گویم به رمز وصف‌الحال</p></div>
<div class="m2"><p>کاندرین شرح نیست جای مقال</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>حق بگویم من از که اندیشم</p></div>
<div class="m2"><p>آنچه باشد یقین شده پیشم</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>جعدهٔ بنت اشعث آن بد زن</p></div>
<div class="m2"><p>که ورا جام زهر داد به فن</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>که فرستاد مر ورا بر گوی</p></div>
<div class="m2"><p>بر زمین زن سبوی بر لب جوی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>آن که بودش که یافت این فرصت</p></div>
<div class="m2"><p>که برو باد تا ابد لعنت</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>که پذیرفت ازو درم به الوف</p></div>
<div class="m2"><p>زر و گوهر که نیست جای وقوف</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>لؤلؤ هند و عقدِ مروارید</p></div>
<div class="m2"><p>که ز میراثهای هند رسید</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>کین نکو عقد مرا ترا دادم</p></div>
<div class="m2"><p>به تو بخشیدم و فرستادم</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>گر تو این شغل را تمام کنی</p></div>
<div class="m2"><p>خویشتن را تو نیک‌نام کنی</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>به پسر مر ترا دهم به زنی</p></div>
<div class="m2"><p>مر مرا دختری و جان و تنی</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>تا بکرد آنچه کردنی بودش</p></div>
<div class="m2"><p>لیک زان فعل بد نبُد سودش</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>آنچه پذرفته بود هیچ نداد</p></div>
<div class="m2"><p>مر ورا در دهان نار نهاد</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>چون پدر گفت با پسر که زنت</p></div>
<div class="m2"><p>جعده باید که هست رای زنت</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>گفت، آن زن که با حسن ده بار</p></div>
<div class="m2"><p>نخورد بر روان او زنهار</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>به دروغی دهد سرش بر باد</p></div>
<div class="m2"><p>از خدا و رسول نارد یاد</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>من برو دل بگو چگونه نهم</p></div>
<div class="m2"><p>به زنی‌اش رضا چگونه دهم</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>با چو او کس چو کژ هوا باشد</p></div>
<div class="m2"><p>با منش راستی کجا باشد</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>جان بیهوده کرد در سرِ کار</p></div>
<div class="m2"><p>تا ابد ماند در جهنّم و نار</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>رفت و با خود ببرد بدنامی</p></div>
<div class="m2"><p>چه بتر در جهان ز خود کامی</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>صدهزار آفرین بار خدا</p></div>
<div class="m2"><p>بر حسن باد تا به روز جزا</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>خبرِ آن دلِ پر آذر او</p></div>
<div class="m2"><p>نشنوی جز مه از برادر او</p></div></div>