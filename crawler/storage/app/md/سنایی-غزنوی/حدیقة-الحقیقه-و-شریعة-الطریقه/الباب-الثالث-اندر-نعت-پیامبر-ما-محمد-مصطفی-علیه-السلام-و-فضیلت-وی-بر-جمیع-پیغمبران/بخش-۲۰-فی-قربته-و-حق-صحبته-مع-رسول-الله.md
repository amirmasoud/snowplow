---
title: >-
    بخش ۲۰ - فی قربته و حق صحبته مع رسول‌اللّٰه
---
# بخش ۲۰ - فی قربته و حق صحبته مع رسول‌اللّٰه

<div class="b" id="bn1"><div class="m1"><p>چون زدی کوس شرع روح امین</p></div>
<div class="m2"><p>چشم بر گوش او نهادی دین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به نبی او ز جان شایسته</p></div>
<div class="m2"><p>در دهان دل نمود چون پسته</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>قد او در رضای یزدانی</p></div>
<div class="m2"><p>جست پیراهن مسلمانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بوده چندان کرامت و فضلش</p></div>
<div class="m2"><p>که لوالفضل خوانده ذوالفضلش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>داده قرض از نهادهٔ دل و دین</p></div>
<div class="m2"><p>هست من ذاالذی گواه براین</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حکم من ذاالذی شنیده به گوش</p></div>
<div class="m2"><p>زده در پیش حکم خانه فروش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در یکی دفعه گاه ایثارش</p></div>
<div class="m2"><p>داده وی چل هزار دینارش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>داده اسباب و ملک و سهل و سلیم</p></div>
<div class="m2"><p>کرده بهر خود اختیار گلیم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از دریچهٔ مشبّک ایمان</p></div>
<div class="m2"><p>در تماشای روضهٔ رضوان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>صدق او نقشبند زیب و فرش</p></div>
<div class="m2"><p>درد او هرهم دل و جگرش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گشته پشمینه پوش روح امین</p></div>
<div class="m2"><p>از پی حلق او به حلقهٔ دین</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تخته شسته ز بهر شرع رسول</p></div>
<div class="m2"><p>از الف با و تای عقل فضول</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>قفصی بوده سینهٔ صدّیق</p></div>
<div class="m2"><p>عندلیبی درو به نام عتیق</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>دل خود چون به شرع او بربست</p></div>
<div class="m2"><p>به نخستین دم آن قفص بشکست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گشت حاصل هرآنچه او مسؤول</p></div>
<div class="m2"><p>نام کُل بر دلش نهاد رسول</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>عندلیب دلش چو بالا جَست</p></div>
<div class="m2"><p>در درازای شرع پهنا گشت</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>عرش و شرع محمدی برِ او</p></div>
<div class="m2"><p>هم در آن سینهٔ منوّر او</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>طول و عرضش چو شرع معلومست</p></div>
<div class="m2"><p>زانکه مقلوب موم هم مومست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چون کمال و جمال او بشناخت</p></div>
<div class="m2"><p>همهٔ خویش در رهش درباخت</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>دایهٔ دین بلایجوز و یجوز</p></div>
<div class="m2"><p>سیر شیرش نکرده بود هنوز</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>که همی کرد بهر دمسازی</p></div>
<div class="m2"><p>جان او با صفاش دل بازی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>دین چو شمعی و مصطفی جانش</p></div>
<div class="m2"><p>جان بوبکر بود پروانه‌اش</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>برده در دین حق خبر بر از او</p></div>
<div class="m2"><p>یافته روز کین ظفر فر از او</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>کرده منشور را به خط بدیع</p></div>
<div class="m2"><p>حق لیستخلفنهم توقیع</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>به خلافت چو دست بیرون کرد</p></div>
<div class="m2"><p>رودهٔ اهل ردّه را خون کرد</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>خرد خویش را ز روی نیاز</p></div>
<div class="m2"><p>قبلهٔ راز کرد و جای نماز</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>آن یکی و همه چو جوهر عقل</p></div>
<div class="m2"><p>آن خداوند و بنده چون سرِِ عقل</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>سال و مه بوده در مرافقتش</p></div>
<div class="m2"><p>جان فدا کرده در موافقتش</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>جد بوبکر بود دین را جاه</p></div>
<div class="m2"><p>دین ز بوبکر یافت تاج و کلاه</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>صدق او میزبان ایمان بود</p></div>
<div class="m2"><p>مصطفی هرچه خواست او آن بود</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>سوخت شاخ اعادت عادت</p></div>
<div class="m2"><p>کند بیخ ارادت ردّت</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>ملک افتاده را به پای آورد</p></div>
<div class="m2"><p>ملت رفته باز جای آورد</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>چون جدا خواست شد زکوة و نماز</p></div>
<div class="m2"><p>بهم آورد هر دوان را باز</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>تازه زو شد زکوة و فرض صلوة</p></div>
<div class="m2"><p>رکن اسلام شد مصون ز افات</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>برگرفت او به قوّت ایمان</p></div>
<div class="m2"><p>شرک و شک را ز کسوت ایمان</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>عالمی قصد کافری کرده</p></div>
<div class="m2"><p>او به نوبت پیامبری کرده</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>صورت و سیرتش همه جان بود</p></div>
<div class="m2"><p>زان ز چشم عوام پنهان بود</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>دل عاقل به نام شد نه به نان</p></div>
<div class="m2"><p>چشم عامی به تن شده نه به جان</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>چشم عاقل درون جان بیند</p></div>
<div class="m2"><p>گوهر لعل چشم کان بیند</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>دست هر ناکسی بدو نرسد</p></div>
<div class="m2"><p>پای هر سفله‌ای درو نرسد</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>چشم ایمان جمال او بیند</p></div>
<div class="m2"><p>کور کی چهرهٔ نکو بیند</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>ای ندانسته حدق بوبکری</p></div>
<div class="m2"><p>تو چه دانش ز صدق بوبکری</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>جان پر کبر و عقل پر مکرت</p></div>
<div class="m2"><p>کی نماید جمال بوبکرت</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>تو بدین چشم مختصر بینش</p></div>
<div class="m2"><p>چون توانی بدیدن آن دینش</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>چشم بوبکر بین ز دین خیزد</p></div>
<div class="m2"><p>نه ز مکر و هوا و کین خیزد</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>حور صدر قیامتش خواند</p></div>
<div class="m2"><p>رافضی قیمتش کجا داند</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>کرد بوبکر کار بوبکری</p></div>
<div class="m2"><p>تو نه مرد عیار بوبکری</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>دشمنش را اجل دوان آرد</p></div>
<div class="m2"><p>که هوا مر ورا هوان دارد</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>تا هوا هاویه نگار تو شد</p></div>
<div class="m2"><p>مار و موش امل شکار تو شد</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>سر بریده چو بیند از خود سیر</p></div>
<div class="m2"><p>گوید او با هزار شر اناخیر</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>رافضی را محلّ آن نبود</p></div>
<div class="m2"><p>وانچه او ظن برد چنان نبود</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>تو نه مرد علی و عبّاسی</p></div>
<div class="m2"><p>مصلحت را ز جهل نشناسی</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>کانکه ابلیس‌وار تن بیند</p></div>
<div class="m2"><p>همه را همچو خویشتن بیند</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>او چه داند که تابش جان چیست</p></div>
<div class="m2"><p>چه شناسد که درد ایمان چیست</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>آنکه جان بهر خاندان خواهد</p></div>
<div class="m2"><p>کی علی را به جان زیان خواهد</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>از برای فضول و جاهلیی</p></div>
<div class="m2"><p>ناز خواهد ز بغض چون علیی</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>آنکه نستد ز حق حلال فلک</p></div>
<div class="m2"><p>کی به خود ره دهد حرام فدک</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>گر نه جانش اضافتی بودی</p></div>
<div class="m2"><p>ورنه صدقش خلافتی بودی</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>مصطفی کی بدو سپردی ملک</p></div>
<div class="m2"><p>یا ز حیدر چگونه بردی ملک</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>آنکه جان را ز صخر بستاند</p></div>
<div class="m2"><p>کی ز بیم عدو فرو ماند</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>علیی کو کشد ز دشمن پوست</p></div>
<div class="m2"><p>با چنین دشمنی نباشد دوست</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>تو بدین ترّهات و هزل و فضول</p></div>
<div class="m2"><p>مر علی را همی کنی معزول</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>گر مداهن بود روا نبود</p></div>
<div class="m2"><p>به خلافت تنش سزا نبود</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>ور بود عاجز و خبیر بود</p></div>
<div class="m2"><p>پس منافق بود نه میر بود</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>مصلحت بود آنچه کرد علی</p></div>
<div class="m2"><p>تو چرا سال و ماه پر جدلی</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>مکر و کبر و هوا برون انداز</p></div>
<div class="m2"><p>تا دهد جانش مر ترا آواز</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>شد چو شیر خدای حرز نویس</p></div>
<div class="m2"><p>رخت بر گاو بر نهد ابلیس</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>تا علی را چو تو ولی چکند</p></div>
<div class="m2"><p>در هوا و هوس علی چکند</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>زین بد و نیک به گزین کردن</p></div>
<div class="m2"><p>زشت باشد حدیث دین کردن</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>برگذشت او ز مبتدای قدم</p></div>
<div class="m2"><p>در رسید او به منتهای همم</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>پیش او روفتند تا درگاه</p></div>
<div class="m2"><p>حور و غلمان به جعد و گیسو راه</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>رافضی را بماند در گردن</p></div>
<div class="m2"><p>جکجک و مرگ و جسک و جان کندن</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>بر براقی که مصطفی پرورد</p></div>
<div class="m2"><p>رافضی رایضی چه داند کرد</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>بود بوبکر با علی همراه</p></div>
<div class="m2"><p>تو زبان فضول کن کوتاه</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>آفرین خدای بی‌همتا</p></div>
<div class="m2"><p>بر ابوبکر باد و شیر خدا</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>صورت صدقش از دریچهٔ فضل</p></div>
<div class="m2"><p>دیده فاروق را به علم و به عدل</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>هر دو مهتر برای دین بودند</p></div>
<div class="m2"><p>در سیادت سزای دین بودند</p></div></div>