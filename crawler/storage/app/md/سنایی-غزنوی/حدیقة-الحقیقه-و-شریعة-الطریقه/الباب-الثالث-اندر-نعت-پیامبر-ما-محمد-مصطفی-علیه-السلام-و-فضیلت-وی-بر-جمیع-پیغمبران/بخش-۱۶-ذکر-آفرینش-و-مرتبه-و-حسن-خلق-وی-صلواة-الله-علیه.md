---
title: >-
    بخش ۱۶ - ذکر آفرینش و مرتبه و حسن خلق وی صلواة الله علیه
---
# بخش ۱۶ - ذکر آفرینش و مرتبه و حسن خلق وی صلواة الله علیه

<div class="b" id="bn1"><div class="m1"><p>عندلیبان باغ آن خوشبوی</p></div>
<div class="m2"><p>در ترنّم تبارک‌الله گوی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر زمان حکم چون شهان کرده</p></div>
<div class="m2"><p>بر زمین نان چو بندگان خورده</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نان جو خورده همچو مختصران</p></div>
<div class="m2"><p>پس کشیده ز حلم بارِ گران</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خَلق را خُلق او نویدگرست</p></div>
<div class="m2"><p>نور ماه از جمال جرم خورست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گنج همسایه بُد دل پاکش</p></div>
<div class="m2"><p>رنج سایه نبود بر خاکش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>صد هزار آه زو شنیده حری</p></div>
<div class="m2"><p>نه الف بود در میانه نه هی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جبرئیل آمده ز سدره برش</p></div>
<div class="m2"><p>بوده سوگند صعب حق به سرش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جز از او کس نبود در بشری</p></div>
<div class="m2"><p>در طلب گریه خند خنده گری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خُلق او زیر این سراپرده</p></div>
<div class="m2"><p>رحمها کرده زخمها خورده</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سالها پیش چرخ با ندمی</p></div>
<div class="m2"><p>ناگواریده خورد جانش همی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گلشکر داشت با خو از دل خود</p></div>
<div class="m2"><p>زان نشد ایچ ناگوارش بد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>خود کسی را که آن زبان دارد</p></div>
<div class="m2"><p>ناگوارندگی زیان دارد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چون زبان از زیان خلق ببست</p></div>
<div class="m2"><p>رفت و بر فوق فرق عرش نشست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>قامتش چون خم رکوع آورد</p></div>
<div class="m2"><p>عرش در پیش او خشوع آورد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بتشهّد دمی چو بنشستی</p></div>
<div class="m2"><p>کمرهِ کوهِ قاف بگسستی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بهره داده وجود را به تمام</p></div>
<div class="m2"><p>زان لب و دیده‌ها به سین سلام</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بوده بحری همیشه محرابش</p></div>
<div class="m2"><p>آتش عشقِ لم‌یزل آبش</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>اندر آن بیکرانه دریا بار</p></div>
<div class="m2"><p>صدهزاران نهنگ مردم‌خوار</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چون دم از حضرت سجود زدی</p></div>
<div class="m2"><p>آتش اندر همه وجود زدی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>خود جهان جملگی طفیلش بود</p></div>
<div class="m2"><p>انس و جن کمترین خیلش بود</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ماه راهش خسوف نپذیرد</p></div>
<div class="m2"><p>شمس شرعش کسوف نپذیرد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>برتر از فرش و عرش قدرش بود</p></div>
<div class="m2"><p>قمهٔ عرش زیر صدرش بود</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>در ره مصطفی نژندی نیست</p></div>
<div class="m2"><p>برتر از قدر او بلندی نیست</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>در ره او همه صعود بود</p></div>
<div class="m2"><p>درگه او سرشت عود بود</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>تا ابد نور و حور در مهدش</p></div>
<div class="m2"><p>پای بسته بمانده در عهدش</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>گر گشایند چنبرِ افلاک</p></div>
<div class="m2"><p>شرع او را از آن نیاید باک</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>اسب گردون بمانده از آورد</p></div>
<div class="m2"><p>مفرش شرع او نگیرد گرد</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>نفسی کز هوای عشقش خاست</p></div>
<div class="m2"><p>طاقت آن نفس ز خلق کراست</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>شود از تفّ آن نفس چو نمود</p></div>
<div class="m2"><p>موج دریا چو آتش نمرود</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>راه پیدا بود پر از آکفت</p></div>
<div class="m2"><p>راه او جز نهفته نتوان رفت</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>از پی جان آن سرِ سادات</p></div>
<div class="m2"><p>اشتر بارکش بداده زکات</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>ای دریغا که در جهان سخن</p></div>
<div class="m2"><p>سر در انگشت می‌کشد ناخن</p></div></div>