---
title: >-
    بخش ۴ - در ذکر آنکه پیغمبر ما رحمةً للعالمین است
---
# بخش ۴ - در ذکر آنکه پیغمبر ما رحمةً للعالمین است

<div class="b" id="bn1"><div class="m1"><p>زحمت آب و گِل در این عالم</p></div>
<div class="m2"><p>رحمتش نام کرده فضل قِدم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قدر شبهای قدر از گل او</p></div>
<div class="m2"><p>نورِ روز قیامت از دل او</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حلقهٔ حلقه‌ها به حلقهٔ موی</p></div>
<div class="m2"><p>شحنهٔ شرعها به صفحهٔ روی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رازِ حق پردهٔ محارم او</p></div>
<div class="m2"><p>نفس کل صورت مکارم او</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عرش عشقش بر آسمان جلال</p></div>
<div class="m2"><p>اصل و فرعش پُر از فنون کمال</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>غرض کُن ز حکم در ازل او</p></div>
<div class="m2"><p>اول الفکر و آخر العمل او</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بوده اول به خلقت و صورت</p></div>
<div class="m2"><p>و آمده آخر از پی دعوت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بوده در روضهٔ حظیرهٔ انس</p></div>
<div class="m2"><p>مادرش امر و دایه روح‌القدس</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>قد او هرکه از مهی و بهی</p></div>
<div class="m2"><p>سخره کردی به قدّ سرو سهی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>لون او ماه را چو گل کردی</p></div>
<div class="m2"><p>بوی او مُشک را خجل کردی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>حلق خلق از برای طوق فرش</p></div>
<div class="m2"><p>خلق خلق نسیم خاک درش</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>فرش نو بارِ فرع او گشته</p></div>
<div class="m2"><p>عرش مغلوب شرع او گشته</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>منتصب قد چو سرو آزاده</p></div>
<div class="m2"><p>شمسهٔ عقل آدمی زاده</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>صبح صادق چنو ندیده به راه</p></div>
<div class="m2"><p>آفتابی به زیر گنبد ماه</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>شرع و دین چار طبع و شش سوی او</p></div>
<div class="m2"><p>عقل و جان گوهر دو گیسوی او</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>هفده تاموی چون ستاره به باغ</p></div>
<div class="m2"><p>وآنِ دیگر سیاه چون پرِ زاغ</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>اندران گیسوی سیاه و سپید</p></div>
<div class="m2"><p>دوخته عقل کیسه‌های امید</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>کرده همزاد با ازل نسبش</p></div>
<div class="m2"><p>گشته همراز با ابد ادبش</p></div></div>