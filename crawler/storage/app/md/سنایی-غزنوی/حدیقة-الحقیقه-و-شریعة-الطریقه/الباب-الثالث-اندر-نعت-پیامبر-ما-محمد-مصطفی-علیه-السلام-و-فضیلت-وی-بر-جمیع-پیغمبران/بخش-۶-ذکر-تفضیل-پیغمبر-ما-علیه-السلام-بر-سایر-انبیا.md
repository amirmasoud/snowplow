---
title: >-
    بخش ۶ - ذکر تفضیل پیغمبر ما علیه‌السّلام بر سایر انبیاء
---
# بخش ۶ - ذکر تفضیل پیغمبر ما علیه‌السّلام بر سایر انبیاء

<div class="b" id="bn1"><div class="m1"><p>از همه انبیا چو بخشش رب</p></div>
<div class="m2"><p>یک تنست و همه‌ست اینت عجب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خلق او از نفیس‌تر موکب</p></div>
<div class="m2"><p>عِرق او در شریفتر منصب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از پی صورتِ دل و جانش</p></div>
<div class="m2"><p>پیش حکم خطاب و فرمانش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نقش پر چشم همچو نرگس‌تر</p></div>
<div class="m2"><p>عقل پر گوش همچو سیسنبر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سیل نامد نهال کن‌تر از او</p></div>
<div class="m2"><p>مرغ نامد قفص شکن‌تر ازو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همتش الرفیق الاعلی جوی</p></div>
<div class="m2"><p>عزّتش لانبیّ بعدی گوی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شیخ را ساز و سوز داده چو شاب</p></div>
<div class="m2"><p>خاک را آبروی داده چو آب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>او ورا بنده بوده از سرِ جد</p></div>
<div class="m2"><p>همه عالم ز پای او مسجد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>رو که تا دامن ابد نیکو</p></div>
<div class="m2"><p>کس نبیند به چشم خود چون او</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در جهانی فگنده آوازه</p></div>
<div class="m2"><p>با خود آورده سنّتی تازه</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گشته ادیان خلق سیرت او</p></div>
<div class="m2"><p>نیست ادراک بر بصیرت او</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>رشد قومی برای حق جویان</p></div>
<div class="m2"><p>اهد قومی ز خوی خوش گویان</p></div></div>