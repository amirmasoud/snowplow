---
title: >-
    بخش ۷ - فی اتباعه صلوات الله علیه
---
# بخش ۷ - فی اتباعه صلوات الله علیه

<div class="b" id="bn1"><div class="m1"><p>خرد و جام او به هر دو سرای</p></div>
<div class="m2"><p>واسطه در میان خلق و خدای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تیغ و قرآن ورا شده معجز</p></div>
<div class="m2"><p>نشود شرع او خلق هرگز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>او چو موسی علی ورا هارون</p></div>
<div class="m2"><p>هر دو یک رنگ از درون و برون</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هرکه از در درآمده بر او</p></div>
<div class="m2"><p>تاج زدنی نهاده بر سر او</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از پی جود نز برای سجود</p></div>
<div class="m2"><p>صدر او آب کعبه برده ز جود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>او مدینهٔ علوم و باب علی</p></div>
<div class="m2"><p>او خدا را نبی علیش ولی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>حرف کاغذ همی سیاه کند</p></div>
<div class="m2"><p>کی دل تیره را چو ماه کند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آن بنان کو میان چو ماه زدی</p></div>
<div class="m2"><p>کی دم از خامهٔ سیاه زدی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ضرب کردی میان ماه تمام</p></div>
<div class="m2"><p>کی شدی بار گیر خامهٔ خام</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آن بنانی که کرد مه به دو نیم</p></div>
<div class="m2"><p>کی کشیدی ز خامه حلقهٔ میم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آنکه هر حرف را دلش بُد ظرف</p></div>
<div class="m2"><p>کی شدی در زمانه بستهٔ حرف</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>آنکه شب را سپید موی کند</p></div>
<div class="m2"><p>کی سخن را سیاه‌روی کند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>کی توان دید نور جان نبی</p></div>
<div class="m2"><p>از دریچهٔ مشبّک عنبی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>کی شد ادراکش از بلندی نور</p></div>
<div class="m2"><p>از زجاجی و از جلیدی دور</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>او همه است از جلال با ما یار</p></div>
<div class="m2"><p>همچو جان از تن و یکی به شمار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چون فرو تاخت آسمان قدم</p></div>
<div class="m2"><p>فلک المستقیم زیر قدم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>آتش کسری از تفش بگریخت</p></div>
<div class="m2"><p>جان خود زیر پای اسبش ریخت</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>پیش شاخی که نور بار آرد</p></div>
<div class="m2"><p>نار زردشت جان نثار آرد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>خدمتش را ز بارگاه بلند</p></div>
<div class="m2"><p>خواجهٔ سدره شد جلاجل بند</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گرچه موسی به سوی نیل شدی</p></div>
<div class="m2"><p>نیل چون پرّ جبرئیل شدی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>سفل نیل آب داد تا سرِِ او</p></div>
<div class="m2"><p>از نشان سفال چاکر او</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>مصلحت را ز بهر عالم داد</p></div>
<div class="m2"><p>هرچه گوشش ستد زبانش داد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>چرخ تا شد جدا ز گوهر او</p></div>
<div class="m2"><p>هست از آنگاه باز گوهر جوی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>آسمان از جمال او ز زمین</p></div>
<div class="m2"><p>خاک بیزی شده‌ست گوهر چین</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>نطق او هرچه در عقول نهاد</p></div>
<div class="m2"><p>روح بر دیدهٔ قبول نهاد</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>یک سخن زو و عالمی معنی</p></div>
<div class="m2"><p>یک نظر زو و یک جهان تقوی</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>نام او هم تکست با تقدیر</p></div>
<div class="m2"><p>کام او همرهست با تیسیر</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>وصف او روح در زبان دارد</p></div>
<div class="m2"><p>یاد او آب در دهان دارد</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>محو گشت از هدایتش گبری</p></div>
<div class="m2"><p>قدری شد به سعی او جبری</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>خلق او آمد از نکو عهدی</p></div>
<div class="m2"><p>روح عیسی و قالب مهدی</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>یافته دین حق بدو تعظیم</p></div>
<div class="m2"><p>خُلق او را خدای خوانده عظیم</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>چون درآمد صدف گشای ازل</p></div>
<div class="m2"><p>پر گهر شد دهان علم و عمل</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>دین بدو یافت زینت و رونق</p></div>
<div class="m2"><p>زانکه زو یافت خلق راه به حق</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>رهروان را ز احمدِ مختار</p></div>
<div class="m2"><p>آنکه دی نار بُد شدی دین‌دار</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>تا گروهی که دی چو دینارند</p></div>
<div class="m2"><p>پیشش امروز جمله دین آرند</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>تا بنگشاد لعل او کان را</p></div>
<div class="m2"><p>سمعها شمعدان نشد جان را</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>نرگسش چون ز آب تر گشتی</p></div>
<div class="m2"><p>زُهره در حال نوحه‌گر گشتی</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>چون ز جهال رخ نهان کردی</p></div>
<div class="m2"><p>خانه بر خود چو بوستان کردی</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>چون شدی تنگ دل ز اهل مجاز</p></div>
<div class="m2"><p>به تماشا شدی به باغ نماز</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>چون ز اشغال خلق درماندی</p></div>
<div class="m2"><p>به ارحنا بلال را خواندی</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>کای بلال اسب دولتم زین کن</p></div>
<div class="m2"><p>خاک بر فرق آن کن و این کن</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>که شدم سیر از آدم و عالم</p></div>
<div class="m2"><p>هان سیاها سپید مهره بدم</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>آدم خویش را به پردهٔ راز</p></div>
<div class="m2"><p>بوده ادهم جنیبت اشهب تاز</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>کرده بی‌گرم و سرد و بی‌تر و خشک</p></div>
<div class="m2"><p>تربتش تربت عرب را مشک</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>گاه گفتی جهان مراست تیغ</p></div>
<div class="m2"><p>گاه گفتی اجوع و گه اشبع</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>یک شکم نان جو نخوردی سیر</p></div>
<div class="m2"><p>نزدی جز برای دین شمشیر</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>مهرش ادریس را بداده نوید</p></div>
<div class="m2"><p>لطفش ابلیس را نکرده نُمید</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>سایه پروردگان پردهٔ غیب</p></div>
<div class="m2"><p>از پی شک و رشک و شبهت و ریب</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>رفته زو بر عطا چو چرخ کبود</p></div>
<div class="m2"><p>تا به گردن در آفتاب فرود</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>ذوق و شوقش ز نیک و بد کوتاه</p></div>
<div class="m2"><p>چشم جسمش ز روح روح آگاه</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>همه خلق و وفا و بسط و فرح</p></div>
<div class="m2"><p>شرط این نعتها الم نشرح</p></div></div>