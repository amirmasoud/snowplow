---
title: >-
    بخش ۵۶ - من اَمَنَ بطاعته فقد خَسَر خُسراناً مبینا
---
# بخش ۵۶ - من اَمَنَ بطاعته فقد خَسَر خُسراناً مبینا

<div class="b" id="bn1"><div class="m1"><p>روبهی پیر روبهی را گفت</p></div>
<div class="m2"><p>کای تو با عقل و رای و دانش جفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چابکی کن دو صد درم بستان</p></div>
<div class="m2"><p>نامهٔ ما بدین سگان برسان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفت اجرت فزون ز دردسر است</p></div>
<div class="m2"><p>لیک کاری عظیم با خطر است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زین زیان چونکه جان من فرسود</p></div>
<div class="m2"><p>درمت آنگهم چه دارد سود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ایمنی از قضایت ای الله</p></div>
<div class="m2"><p>هست نزدیک عقل عین گناه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ایمنی کرد هر دو را بدنام</p></div>
<div class="m2"><p>آن عزازیل و آن دگر بلعام</p></div></div>