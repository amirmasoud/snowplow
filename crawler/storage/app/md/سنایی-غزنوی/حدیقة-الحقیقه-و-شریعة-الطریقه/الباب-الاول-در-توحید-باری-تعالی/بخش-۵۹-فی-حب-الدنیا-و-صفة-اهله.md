---
title: >-
    بخش ۵۹ - فی حُبّ الدُّنیا و صفة اهله
---
# بخش ۵۹ - فی حُبّ الدُّنیا و صفة اهله

<div class="b" id="bn1"><div class="m1"><p>هست شهری بزرگ در حدِِ روم</p></div>
<div class="m2"><p>باز بسیار اندر آن بر و بوم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نام آن شهر شهره فسطاطست</p></div>
<div class="m2"><p>ساحلش تا به حد دمیاطست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اندرو مرغ خانگی نپرد</p></div>
<div class="m2"><p>زانکه باز از هوا ورا شکرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>واندران شهر مرغ نگذارد</p></div>
<div class="m2"><p>زآنکه در ساعتش بیوبارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همچو فسطاط شد زمانه کنون</p></div>
<div class="m2"><p>علما همچو مرغ ‌خوار و زبون</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>من به دست آوریدم این بالا</p></div>
<div class="m2"><p>تا شوم ایمن از بدِ دنیا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گفت دانا که با تو اینجا کیست</p></div>
<div class="m2"><p>بر سرِ کوهپایه حالت چیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گفت زاهد که نفس من با من</p></div>
<div class="m2"><p>هست روز و شب اندرین مسکن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گفت دانا که پس نکردی هیچ</p></div>
<div class="m2"><p>بیهده راه زاهدان مپیسیچ</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گفت زاهد که نفس دوخته‌اند</p></div>
<div class="m2"><p>در من و زی ویم فروخته‌اند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نتوانم ز وی جدا گشتن</p></div>
<div class="m2"><p>چکنم چارهٔ رها گشتن</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گفت با زاهد آن ستوده حکیم</p></div>
<div class="m2"><p>نفس افعالِ بد کند تعلیم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گفت زاهد که من بساخته‌ام</p></div>
<div class="m2"><p>زانکه من نفس را شناخته‌ام</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>هست بیمار نفس و من چو طبیب</p></div>
<div class="m2"><p>من‌کنم روز و شب ورا ترتیب</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>به مداوای نفس مشغولم</p></div>
<div class="m2"><p>زآنکه گوید همی که معلولم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گه ورا قصد فصد فرمایم</p></div>
<div class="m2"><p>اکحل از دیدگانش بگشایم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چون تصعد کند فرو بارد</p></div>
<div class="m2"><p>فصد تسکینی اندرو آرد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گه ورا مُسهلی بفرمایم</p></div>
<div class="m2"><p>علل از جسم او بپالایم</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>حبّ دنیا و حقد و بغض و حسد</p></div>
<div class="m2"><p>غل و غشش برون شود ز جسد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گاه نهیش کنم من از شهوات</p></div>
<div class="m2"><p>تا مگر باز ماند از لذّات</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>از خورش خوی خویش باز کند</p></div>
<div class="m2"><p>درِ شهوت به خود فراز کند</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>قُوتش از باقلی دودانه کنم</p></div>
<div class="m2"><p>خانه بر وی چو گورخانه کنم</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ساعتی نفس چون شود در خواب</p></div>
<div class="m2"><p>من کنم یک دو رکعتی بشتاب</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>پیش از آن کو ز خواب برخیزد</p></div>
<div class="m2"><p>همچو بیمار در من آویزد</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>یک دو رکعت بی او چو بگذارم</p></div>
<div class="m2"><p>بعد از آن نفس گشت بیدارم</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>مردِ دانا چو این سخن بشنید</p></div>
<div class="m2"><p>جامه بر تن ز وجد آن بدرید</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>گفت للّه درّک ای زاهد</p></div>
<div class="m2"><p>بارک الله عمرک ای عابد</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>این سخن جز ترا مسلّم نیست</p></div>
<div class="m2"><p>ملک تو کم ز ملکت جم نیست</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>هرچت امروز هست آرایش</p></div>
<div class="m2"><p>دان که فردات باشد آلایش</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>نیست آلوده کز گنه خیزد</p></div>
<div class="m2"><p>آن کز اندوه آه و أه خیزد</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>زن کند بهر میهمانی پاک</p></div>
<div class="m2"><p>موی ابرو و موی رخ چالاک</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>دل بدین‌جا غریب و نادانست</p></div>
<div class="m2"><p>تا به بندِ چهار ارکانست</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>خرد اینجا تهی کند جعبه</p></div>
<div class="m2"><p>که تحرّی بد است در کعبه</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>پیش کعبه مگر که بوالهوسی</p></div>
<div class="m2"><p>بشنود علم سمت قبله بسی</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>هرکه در کعبه با تحرّی مرد</p></div>
<div class="m2"><p>زیرهٔ تر بسوی کرمان برد</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>در سه زندان غل و حقد و حسد</p></div>
<div class="m2"><p>عقل را بسته‌ای به بندِ جسد</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>پنج حس کز چهار ارکانند</p></div>
<div class="m2"><p>پنج غمّاز این سه زندانند</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>دل شده محرم خزانهٔ راز</p></div>
<div class="m2"><p>چکند ننگ مُنهی و غمّاز</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>بی‌زبانان زبان او گویند</p></div>
<div class="m2"><p>بی‌نشانان نشانِ او جویند</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>هرچه جز دوست آتش اندر زن</p></div>
<div class="m2"><p>آنگه از آب عشق سر بر زن</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>که نه یارند و یار می‌بینی</p></div>
<div class="m2"><p>همه زنهارخوار می‌بینی</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>گلبن باغ خویشتن بینان</p></div>
<div class="m2"><p>شده چون دُلم دُلم بدبینان</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>نیک معلوم کن که در محشر</p></div>
<div class="m2"><p>نشود هیچ حال خلق دگر</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>پیشش آید هرآنچه بگزیند</p></div>
<div class="m2"><p>هرچه زینجا برد همان بیند</p></div></div>