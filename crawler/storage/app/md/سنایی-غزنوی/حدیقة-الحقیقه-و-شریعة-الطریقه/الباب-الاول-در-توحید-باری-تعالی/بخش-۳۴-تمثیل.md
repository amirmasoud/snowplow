---
title: >-
    بخش ۳۴ - تمثیل
---
# بخش ۳۴ - تمثیل

<div class="b" id="bn1"><div class="m1"><p>زالکی کرد سر برون ز نهفت</p></div>
<div class="m2"><p>کشتک خویش خشک دید و بگفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کای هم آنِ نو و هم آنِ کُهن</p></div>
<div class="m2"><p>رزق بر تست هرچه خواهی کن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>علت رزق تو به خوب و به زشت</p></div>
<div class="m2"><p>گریهٔ ابر نی و خندهٔ کِشت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از هزاران هزار به یک تو</p></div>
<div class="m2"><p>زانک اندک نباشد اندک تو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شعله‌ای زو و صدهزار اختر</p></div>
<div class="m2"><p>قطره‌ای زو و صد هزار اخضر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بی‌سبب رازقی یقین دانم</p></div>
<div class="m2"><p>همه از تست نانم و جانم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مرد نبود کسی که در غمِ خور</p></div>
<div class="m2"><p>در یقین باشد از زنی کمتر</p></div></div>