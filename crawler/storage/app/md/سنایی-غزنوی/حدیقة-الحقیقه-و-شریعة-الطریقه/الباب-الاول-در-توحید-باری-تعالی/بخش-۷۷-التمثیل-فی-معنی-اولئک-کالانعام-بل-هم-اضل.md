---
title: >-
    بخش ۷۷ - التمثیل فی معنی اولئک کالانعام بل هم اضل
---
# بخش ۷۷ - التمثیل فی معنی اولئک کالانعام بل هم اضل

<div class="b" id="bn1"><div class="m1"><p>کرهّ‌ای را که شد سه سال تمام</p></div>
<div class="m2"><p>رائضش درکشد به زخم لگام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مر ورا در هنر بفرهنجد</p></div>
<div class="m2"><p>توسنی از تنش بیاهنجد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کرّه را بر لگام رام کند</p></div>
<div class="m2"><p>نام او اسب خوش لگام کند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بارگیر ملوک را شاید</p></div>
<div class="m2"><p>به زر و زیورش بیاراید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون نیابد ریاضتی در خور</p></div>
<div class="m2"><p>باشد آن کرّه از خری کمتر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بابت بارِ آسیا باشد</p></div>
<div class="m2"><p>دایم از بار در عنا باشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گاه بارِ جهود و گه ترسا</p></div>
<div class="m2"><p>می‌کشد در عنا و رنج و بلا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آدمی نیز کش ریاضت نیست</p></div>
<div class="m2"><p>پیش دانا ورا افاضت نیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>علف دوزخ است و ترسانست</p></div>
<div class="m2"><p>با حجر در جحیم یکسانست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مر ورا هست جای خوف و هراس</p></div>
<div class="m2"><p>خوانده در نص هم وقودالناس</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نفس فرمان‌پذیر و فرمانده</p></div>
<div class="m2"><p>عقل ایمان‌شناس و ایمان‌ده</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>عکس خور زاب بر جدار شود</p></div>
<div class="m2"><p>سقف از نقش او نگار شود</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>آنهم از عکسِ آفتاب شمار</p></div>
<div class="m2"><p>آن دوم عکس آب بر دیوار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>جان نروبد ز بیم مهجوری</p></div>
<div class="m2"><p>خاک درگاه جز به دستوری</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>آنِ اویند در مکان و زمان</p></div>
<div class="m2"><p>از کُن امر تا دریچهٔ جان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گفته از بهر خدمتِ درگاه</p></div>
<div class="m2"><p>امر با عقلها اطیعوالله</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>نفس روینده تا به گوینده</p></div>
<div class="m2"><p>همه چون بنده‌اند جوینده</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>سوی آن کفر زشت و دین نیکوست</p></div>
<div class="m2"><p>که ز دین نقش بیند از خر پوست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>گرچه بی‌اوت قصد و نیرو نه</p></div>
<div class="m2"><p>کار دین بی تو نی و بی‌او نه</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>کار دین خود نه سرسری کاریست</p></div>
<div class="m2"><p>دینِ حق را همیشه بازاریست</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>دین حق تاج و افسر مردست</p></div>
<div class="m2"><p>تاج نامرد را چه در خوردست</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>دین نگهدار تا به ملک رسی</p></div>
<div class="m2"><p>ورنه بی‌دین بدان که هیچ کسی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>راه دین رو که راهِ دین چو روی</p></div>
<div class="m2"><p>همچو شاخ از برهنگی ننوی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ای خوشا راه دین و امر خدای</p></div>
<div class="m2"><p>از گِل تیره رو برآر دو پای</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>در ره جبر و اختیار خدای</p></div>
<div class="m2"><p>بی تو و با تو نیست کار خدای</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>همه از کار کرد الله است</p></div>
<div class="m2"><p>نیکبخت آن کسی که آگاه است</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>اندرین ره ز داد و دانش خویش</p></div>
<div class="m2"><p>ره رو و رهبری کن و مندیش</p></div></div>