---
title: >-
    بخش ۷۹ - فی الحذر عن‌القدر
---
# بخش ۷۹ - فی الحذر عن‌القدر

<div class="b" id="bn1"><div class="m1"><p>بندگان را که از قدر حذر است</p></div>
<div class="m2"><p>آن نه زیشان که آن هم از قدر است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قدر و تقدیر او نهاد چو چنگ</p></div>
<div class="m2"><p>که شناسد همی ز نام و ز ننگ</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زان چو بربط به هر خیال همی</p></div>
<div class="m2"><p>خفته نالد ز گوشمال همی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پیش دیوانِ حکم او جز مرد</p></div>
<div class="m2"><p>شکر سیلی حق که داند کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سنگ خواران حکم چو سندان</p></div>
<div class="m2"><p>نزنند از برای جان دندان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>که کند با قضای او آهی</p></div>
<div class="m2"><p>جز فرومایه‌ای و گمراهی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آه تو با قضای او باد است</p></div>
<div class="m2"><p>با قضایش دل تو ناشاد است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>با قضا مر ترا چو نیست رضا</p></div>
<div class="m2"><p>نشناسی خدای را به خدای</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کو در این راه کردنی کردن</p></div>
<div class="m2"><p>که تواند قفای او خوردن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کردنی بایدت عزازیلی</p></div>
<div class="m2"><p>تا زند دست لعنتش سیلی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سیلی کز دو دست دوست خوری</p></div>
<div class="m2"><p>همچو بادام بی دو پوست خوری</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گردنانی که با خدای خوشند</p></div>
<div class="m2"><p>حکم را بُختیان بارکشند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چون چراغند اگرچه در بندند</p></div>
<div class="m2"><p>زانکه جان می‌کنند و می‌خندند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>هر بلایی که دل نماید از او</p></div>
<div class="m2"><p>گر یکی ور هزار شاید از او</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>حکم و تقدیر او بلا نبوَد</p></div>
<div class="m2"><p>هرچه آید به جز عطا نبوَد</p></div></div>