---
title: >-
    بخش ۵۸ - فی صفة‌الزهد و الزّاهد
---
# بخش ۵۸ - فی صفة‌الزهد و الزّاهد

<div class="b" id="bn1"><div class="m1"><p>زاهدی از میان قوم بتاخت</p></div>
<div class="m2"><p>به سرِِ کوه رفت و صومعه ساخت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روزی از اتّفاق دانایی</p></div>
<div class="m2"><p>عالمی پُر خرد توانایی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>برگذشت و بدید زاهد را</p></div>
<div class="m2"><p>آن چنان پارسای عابد را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفت ویحک چرا براین بالای</p></div>
<div class="m2"><p>ساختستی مقام و مسکن و جای</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفت زاهد که اهل دنیا پاک</p></div>
<div class="m2"><p>در طلب کردنش شدند هلاک</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بازِ دنیا شده است در پرواز</p></div>
<div class="m2"><p>در فکنده به هر دیار آواز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به زبانی فصیح می‌گوید</p></div>
<div class="m2"><p>در جهان صیدِِ خویش می‌جوید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هر زمان گوید اهل دنیی را</p></div>
<div class="m2"><p>جفت بلوی و فرد مولی را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>وای آن کو ز من حذر نکند</p></div>
<div class="m2"><p>در طلب کردنم نظر نکند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تا نگردد چنانکه در فسطاط</p></div>
<div class="m2"><p>اندکی مرغ و باز بر افراط</p></div></div>