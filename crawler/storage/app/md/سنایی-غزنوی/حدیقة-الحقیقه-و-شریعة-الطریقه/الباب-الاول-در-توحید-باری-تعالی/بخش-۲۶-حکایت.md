---
title: >-
    بخش ۲۶ - حکایت
---
# بخش ۲۶ - حکایت

<div class="b" id="bn1"><div class="m1"><p>ثوری از بایزید بسطامی</p></div>
<div class="m2"><p>از پی طاعت و نکونامی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کرد نیکو سؤالی و بگریست</p></div>
<div class="m2"><p>گفت پیرا بگو که ظالم کیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پیرِ وی مر ورا جواب بداد</p></div>
<div class="m2"><p>شربت وی هم از کتاب بداد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفت ظالم کسیست بدروزی</p></div>
<div class="m2"><p>که یکی لحظه در شبانروزی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کند از غافلی فراموشش</p></div>
<div class="m2"><p>نبوَد بنده حلقه در گوشش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر فراموش کردیش نفسی</p></div>
<div class="m2"><p>ظالمی هرزه نیست چون تو کسی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ور بوی حاضر و بری نامش</p></div>
<div class="m2"><p>نیست کردی ز جرم احکامش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آنچنان یاد کن که از دل و جان</p></div>
<div class="m2"><p>بشوی غایب از زمین و زمان</p></div></div>