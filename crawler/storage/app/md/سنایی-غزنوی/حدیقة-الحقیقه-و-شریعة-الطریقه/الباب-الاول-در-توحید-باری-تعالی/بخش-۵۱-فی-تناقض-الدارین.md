---
title: >-
    بخش ۵۱ - فی تناقض الدارین
---
# بخش ۵۱ - فی تناقض الدارین

<div class="b" id="bn1"><div class="m1"><p>علّت روز و شب خور است و زمین</p></div>
<div class="m2"><p>چون گذشتی نه آنت ماند و نه این</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای دو بر زعم تو مراد و مرید</p></div>
<div class="m2"><p>دویی از عقل دان نه از توحید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در چنین حضرت ار ز من شنوی</p></div>
<div class="m2"><p>چون همه شد یکی مجوی دویی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>که بر این در اگرچه پر شورست</p></div>
<div class="m2"><p>زال زر همچو زال بی زورست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در دویی دان مشقت و تمییز</p></div>
<div class="m2"><p>در یکیئی یکیست رستم و حیز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در مصاف صفا و ساحت دل</p></div>
<div class="m2"><p>بر فراز روان و تارکِ گل</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تیغ تا نفکنی سپر نشوی</p></div>
<div class="m2"><p>تا بننهی کلاه سر نشوی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا دلت بندهٔ کلاه بود</p></div>
<div class="m2"><p>فعل تو سال و مه گناه بود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون شدی فارغ از کلاه و کمر</p></div>
<div class="m2"><p>بر سران زمانه گشتی سر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ترک ترکیب رخش توفیق‌ست</p></div>
<div class="m2"><p>نفی ترتیب محض تحقیق‌ست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مردنِ دل هلاکِ جان باشد</p></div>
<div class="m2"><p>مردنِ جان ورا امان باشد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>صُدرهٔ صدر پادشاه سخن</p></div>
<div class="m2"><p>فارغ آمد ز سوزن و ناخن</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>اندرین ره به هیچ روی مایست</p></div>
<div class="m2"><p>نیست گرد و ز نیست گشتن نیست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گرم رو گرچه فی‌المثل تنهاست</p></div>
<div class="m2"><p>نیست یکتن که عالمی برپاست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چون تو برخاستی ز نفس و ز عقل</p></div>
<div class="m2"><p>این جهانت بدان جهان شد نقل</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>هر سری کز تو رست هم در دم</p></div>
<div class="m2"><p>سر بزن چون چراغ و شمع و قلم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>زانکه هر سر که دیدنی باشد</p></div>
<div class="m2"><p>در طریقت بریدنی باشد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بی‌سری پیش گردنان ادبست</p></div>
<div class="m2"><p>زانکه پیوسته سر کله طلبست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بی‌سری مر ترا سر آرد بار</p></div>
<div class="m2"><p>دُرج پر دُر ز بی‌سریست انار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>سرّ کل را کله پناه بُوَد</p></div>
<div class="m2"><p>با چنین سر کله گناه بُوَد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>تو بزیر کلاه غش داری</p></div>
<div class="m2"><p>لاجرم جسر نار نگذاری</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>آدمی را ز جاه بهتر چاه</p></div>
<div class="m2"><p>کل فضولی شود چو یافت کلاه</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>جاه یوسف ز چاه پیدا شد</p></div>
<div class="m2"><p>نفس دانا ز عقل گویا شد</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>زانکه در بارگاه ربّانی</p></div>
<div class="m2"><p> من بگویم اگر نمیدانی</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>آن نکوتر که اندرین معراج</p></div>
<div class="m2"><p>دست بر سر کنی نیابی تاج</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>کز پی غیب مرده ره پوید</p></div>
<div class="m2"><p>وز پی عیب کل کله جوید</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>چون سلیمان کمال ره را داد</p></div>
<div class="m2"><p>همچو یوسف جمال چه را داد</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>تا نشد نقش صورتت چاهی</p></div>
<div class="m2"><p>نشود نقش سرّت اللّهی</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>با کلاهت اگر زیان باشد</p></div>
<div class="m2"><p>قلب او خود هلاک جان باشد</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>در طریقت سر و کلاه مدار</p></div>
<div class="m2"><p>ورنه داری چو شمع دل پر نار</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>گر همی یوسفیت باید و جاه</p></div>
<div class="m2"><p>پیش حق باشگونه پال چو چاه</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>سر که آن بندهٔ کلاه بُوَد</p></div>
<div class="m2"><p>همچو بیژن اسیر چاه بود</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>ور کله بایدت همی ناچار</p></div>
<div class="m2"><p>همچو شمع آن کلاه از آتش دار</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>کانکه در عشق شمع ره باشد</p></div>
<div class="m2"><p>همچو شمع آتشین کله باشد</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>ای ز صورت چنانکه جان از جسم</p></div>
<div class="m2"><p>دل ز وحدت چنانکه مرد از اسم</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>کوشش از تن کشش ز جان خیزد</p></div>
<div class="m2"><p>چشش از ترک این و آن خیزد</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>تا ابد با قدم حدث طفل است</p></div>
<div class="m2"><p>وانکه صافی برون ازین ثفل است</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>تا زمین جای آدمی زایست</p></div>
<div class="m2"><p>خیمهٔ روزگار برپایست</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>این زمین میهمان‌سرایی دان</p></div>
<div class="m2"><p>آدمی را چو کدخدایی دان</p></div></div>