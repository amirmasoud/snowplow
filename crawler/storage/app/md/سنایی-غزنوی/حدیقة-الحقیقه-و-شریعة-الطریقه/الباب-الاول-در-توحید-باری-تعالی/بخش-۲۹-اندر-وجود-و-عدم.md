---
title: >-
    بخش ۲۹ - اندر وجود و عدم
---
# بخش ۲۹ - اندر وجود و عدم

<div class="b" id="bn1"><div class="m1"><p>جهد کن تا زنیست هست شوی</p></div>
<div class="m2"><p>وز شراب خدای مست شوی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>باشد آنرا که دین کند هستش</p></div>
<div class="m2"><p>گوی و چوگان دهر در دستش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون ازاین جرعه گشت جان تو مست</p></div>
<div class="m2"><p>بر بلندی هست گردی پست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هرکه آزاد کرد آنجایست</p></div>
<div class="m2"><p>حلقه در گوش و بند برپایست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>لیکن آن بند به که مرکب بخت</p></div>
<div class="m2"><p>لیکن آن حلقه به که حلقهٔ تخت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بند کو برنهد تو تاج شمر</p></div>
<div class="m2"><p>ور پلاست دهد دواج شمر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زانکه هم محسنست و هم مُجمل</p></div>
<div class="m2"><p>زانکه هم مُکرمست و هم مُفضل</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چه کنی بهر بی‌نوایی را</p></div>
<div class="m2"><p>شادی و زیرک و بهایی را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شاد ازو باش و زیرک از دینش</p></div>
<div class="m2"><p>تا بیابی رضا و تمکینش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زیرک آنست کوش بردارد</p></div>
<div class="m2"><p>شادی آنست کوش نگذارد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نیکبخت آن کسی که بندهٔ اوست</p></div>
<div class="m2"><p>در همه کارها بسنده بر اوست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چون از این شاخها شدی بی‌برگ</p></div>
<div class="m2"><p>دستها در کمر کنی با مرگ</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نشوی مرگ را دگر منکر</p></div>
<div class="m2"><p>یابی از عالم حیات خبر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>دست تو چون به شاخ مرگ رسید</p></div>
<div class="m2"><p>پای تو گرد کاخ برگ دوید</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>پای کز طارم هدی دورست</p></div>
<div class="m2"><p>نیست پای آن دماغ مخمورست</p></div></div>