---
title: >-
    بخش ۶۷ - فی تأدیب صببان المکتب و صفة الجنّة والنّار
---
# بخش ۶۷ - فی تأدیب صببان المکتب و صفة الجنّة والنّار

<div class="b" id="bn1"><div class="m1"><p>از پی راه حق کم از کودک</p></div>
<div class="m2"><p>نتوان بودن ای کم از یک یک</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر در آموختن کند تقصیر</p></div>
<div class="m2"><p>هرچه خواهد سبک زوی بپذیر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به تلطّف بدار و بنوازش</p></div>
<div class="m2"><p>خیره در انتظار مگدازش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در کنارش نه آن زمان کاکا</p></div>
<div class="m2"><p>تا شود راضی و مکنش جفا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در نمازش نه آن زمان کانجا</p></div>
<div class="m2"><p>تا شود سرخ چهره‌اش چو لکا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ور نخواند بخواه زود دوال</p></div>
<div class="m2"><p>گوشهایش بگیر و سخت بمال</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به معلم نمای تهدیدش</p></div>
<div class="m2"><p>تا بود گوشمال تمهیدش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بند و حبسش کند به خانهٔ موش</p></div>
<div class="m2"><p>میر موشان کند فشرده گلوش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در ره آخرت ز بهر شنود</p></div>
<div class="m2"><p>کمتر از کودکی نشاید بود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خلد کاکای تست هان بشتاب</p></div>
<div class="m2"><p>به دو رکعت بهشت را دریاب</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ورنه شد موشخانه دوزخِ تو</p></div>
<div class="m2"><p>در ره آن سرای برزخِ تو</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>رو به کتّاب انبیا یک چند</p></div>
<div class="m2"><p>بر خود این جهل و این ستم مپسند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>لوحی از شرح انبیا برخوان</p></div>
<div class="m2"><p>چون ندانی برو بخوان و بدان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>تا مگر یار انبیا گردی</p></div>
<div class="m2"><p>زین جهالت مگر جدا گردی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>در جهان خراب پر ز ضرر</p></div>
<div class="m2"><p>از جهالت مدان تو هیچ بتر</p></div></div>