---
title: >-
    بخش ۳۵ - حکایت مرغ با گبر
---
# بخش ۳۵ - حکایت مرغ با گبر

<div class="b" id="bn1"><div class="m1"><p>آن بنشنیده‌ای که بی‌نم ابر</p></div>
<div class="m2"><p>مرغ روزی بیافت از درِ گبر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گبر را گفت پس مسلمانی</p></div>
<div class="m2"><p>زین هنرپیشه‌ای سخن دانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کز تو این مکرمت بنپذیرند</p></div>
<div class="m2"><p>مرغکان گرچه دانه برگیرند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گبر گفت ار مرا بگزیند</p></div>
<div class="m2"><p>آخر این رنج من همی بیند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زانکه او مکرمست و با احسان</p></div>
<div class="m2"><p>نکند بخل با کرم یکسان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دست در باخت در رهش جعفر</p></div>
<div class="m2"><p>داد ایزد به جای دستش پر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>داد به فعل و فضول خلق مبند</p></div>
<div class="m2"><p>دل در او بند رستی از غم و بند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کار تو جز خدای نگشاید</p></div>
<div class="m2"><p>به خدای ار ز خلق هیچ آید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تا توانی جز او به یار مگیر</p></div>
<div class="m2"><p>خلق را هیچ در شمار مگیر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خلق را هیچ تکیه‌گاه مساز</p></div>
<div class="m2"><p>جز به درگاه او پناه مساز</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کین همه تکیه جایها هوس است</p></div>
<div class="m2"><p>تکیه‌گه رحمت خدای بس است</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تا بقای شماست نان شماست</p></div>
<div class="m2"><p>اِلف آلای او و جان شماست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>هردو را در جهان عشق و طلب</p></div>
<div class="m2"><p>پارسی باب‌دان و تازی اَب</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چون نداری خبر ز راه نیاز</p></div>
<div class="m2"><p>در حجابی بسان مغز پیاز</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>تا جدایی ز نور موسی تو</p></div>
<div class="m2"><p>روز کوری چو مرغ عیسی تو</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>اول از بهر عشق دل جویش</p></div>
<div class="m2"><p>سر قدم کن چو کلک و می جویش</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>تا بدانجا رسی بچست درست</p></div>
<div class="m2"><p>که بدانی که می نباید جست</p></div></div>