---
title: >-
    بخش ۸۱ - فی الکرامة
---
# بخش ۸۱ - فی الکرامة

<div class="b" id="bn1"><div class="m1"><p>از درونش چوبوی جان یابند</p></div>
<div class="m2"><p>بی‌زبانان همه زبان یابند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دلش از بند ملک بربایند</p></div>
<div class="m2"><p>ملکوت جهانش بنمایند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا کند عقلش از پی رازی</p></div>
<div class="m2"><p>گرد میدان عشق پروازی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دل و جانش نهفته شد حق جوی</p></div>
<div class="m2"><p>شد زبانش به حق اناالحق گوی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>راه دین صنعت و عبارت نیست</p></div>
<div class="m2"><p>جز خرابی در او عمارت نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون تو گشتی خموش منطیقی</p></div>
<div class="m2"><p>ور بگویی بسان بطریقی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مرد باید که چون خلیل بُوَد</p></div>
<div class="m2"><p>تا ز حق ظّلِ او ظلیل بُوَد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زَهره دارد زمانه از بیمش</p></div>
<div class="m2"><p>یک نفس بر زند به تعلیمش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>موسئی را که خفتهٔ کونست</p></div>
<div class="m2"><p>فرّ عونش هلاک فرعونست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>عرش چون فرش زیر پای آرد</p></div>
<div class="m2"><p>جغد باشد ولی همای آرد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خواجهٔ این و آن سرای شود</p></div>
<div class="m2"><p>بندهٔ مخلص خدای شود</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مر ورا عقل روی بنماید</p></div>
<div class="m2"><p>تنش از نور خود بیاراید</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>لطف حق سایه‌ش افکند بر دل</p></div>
<div class="m2"><p>بس بگوید که کیف مدالظل</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چون ز ظل جان او بیابد لمس</p></div>
<div class="m2"><p>روی بنمایدش جعلنا الشمس</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>هرکرا توبه زین شراب دهند</p></div>
<div class="m2"><p>بوی و رنگش به باد و آب دهند</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بیش بنمایدش به حِس زبون</p></div>
<div class="m2"><p>فلک و طبع و رنگ بوقلمون</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>راه دور از دل درنگی تست</p></div>
<div class="m2"><p>کفر و دین از پی دو رنگی توست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>لقب رنگها مجازی کن</p></div>
<div class="m2"><p>خور ز دریای بی‌نیازی کن</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>تا از آن نعره‌ها به گوش نوی</p></div>
<div class="m2"><p>وحده لا شریک له شنوی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بیش سودای رنگها نپزی</p></div>
<div class="m2"><p>گر کند عیسی تو رنگ‌رزی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>هرچه خواهی ز رنگ برداری</p></div>
<div class="m2"><p>در یکی خم زنی برون آری</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>به حقیقت شنو نه از سرِ جهل</p></div>
<div class="m2"><p>نیست این نکته بابت نااهل</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>کین همه رنگهای پر نیرنگ</p></div>
<div class="m2"><p>خم وحدت کند همه یک رنگ</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>پس چو یک رنگ شد همه او شد</p></div>
<div class="m2"><p>رشته باریک شد چو یک تو شد</p></div></div>