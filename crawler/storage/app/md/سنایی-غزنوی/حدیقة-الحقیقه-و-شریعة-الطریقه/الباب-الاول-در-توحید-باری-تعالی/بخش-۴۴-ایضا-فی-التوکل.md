---
title: >-
    بخش ۴۴ - ایضاً فی‌التوکّل
---
# بخش ۴۴ - ایضاً فی‌التوکّل

<div class="b" id="bn1"><div class="m1"><p>ربع مسکون چو از طریق شمار</p></div>
<div class="m2"><p>شد به فرسنگ بیست و چار هزار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو اگر واقفی به صرف و صروف</p></div>
<div class="m2"><p>بدلش کن به بیست و چار حروف</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ساعت شب چو ضم کنی با روز</p></div>
<div class="m2"><p>خود بود بیست و چار آدم سوز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>قاف قول شهادتین ترا</p></div>
<div class="m2"><p>بی‌ریا و نفاق و کیف و مِرا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از همه عالمت برون آرد</p></div>
<div class="m2"><p>نه به آلت به کاف و نون آرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از ورای خرد سخن زو گو</p></div>
<div class="m2"><p>وردت این بس که لاهو الّا هو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کلمهٔ حق چو در شمار آمد</p></div>
<div class="m2"><p>عدد حرف بیست و چار آمد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نیمی از بحر جان دوازده دُرج</p></div>
<div class="m2"><p>نیمی از چرخ دین دوازده بُرج</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دُرجها پر ز دُرّ امید است</p></div>
<div class="m2"><p>برجها پر ز ماه و خورشید است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>درِّ دریای این جهانی نه</p></div>
<div class="m2"><p>ماه و خورشید آسمانی نه</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>درِّ دریای عالم جبروت</p></div>
<div class="m2"><p>ماه و خورشید آسمان سکوت</p></div></div>