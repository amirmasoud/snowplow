---
title: >-
    بخش ۵۳ - قصهٔ قیس‌بن عاصم رضی‌الله عنه
---
# بخش ۵۳ - قصهٔ قیس‌بن عاصم رضی‌الله عنه

<div class="b" id="bn1"><div class="m1"><p>آن زمان کز خدای نزد رسول</p></div>
<div class="m2"><p>حکم من ذاالذی نمود نزول</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هرکسی آنقدر که دست رسید</p></div>
<div class="m2"><p>پیش مهتر کشید و سر نکشید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گوهر و زر ستور و بنده و مال</p></div>
<div class="m2"><p>هرچه در وسع بودشان در حال</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>قیس عاصم ضعیف حالی بود</p></div>
<div class="m2"><p>که نکردی طلب ز دنیا سود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رفت در خانه با عیال بگفت</p></div>
<div class="m2"><p>زانچه بشنید هیچیک ننهفت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کاینچنین آیت آمده است امروز</p></div>
<div class="m2"><p>خیز و ما را در انتظار مسوز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آنچه در خانه حاضر است بیار</p></div>
<div class="m2"><p>تا کنم پیش سیّد آن ایثار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گفت زن چیز نیست در خانه</p></div>
<div class="m2"><p>تو نه‌ای زین سرای بیگانه</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گفتش آخر بجوی آن مقدار</p></div>
<div class="m2"><p>هرچه یابی سبک به نزد من آر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>رفت و خانه بجُست بسیاری</p></div>
<div class="m2"><p>تا برآید مگر ورا کاری</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>یافت در خانه صاعی از خرما</p></div>
<div class="m2"><p>دقل و خشک گشته تا بنوا</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>پیش قیس آورید زن در حال</p></div>
<div class="m2"><p>گفت زین بیش نیست مارا مال</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>قیس خرما به آستین در کرد</p></div>
<div class="m2"><p>شادمانه برِ رسول آورد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چون درون رفت قیس در مسجد</p></div>
<div class="m2"><p>نز سرِ هزل بلکه از سرِ جد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گفت با وی منافقی بدکار</p></div>
<div class="m2"><p>تا چه آورده‌ای سبک پیش آر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گوهر است این متاع یا زر و سیم</p></div>
<div class="m2"><p>پیش مهتر چه می‌کنی تسلیم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>زان سخن قیس گشت خوار و خجل</p></div>
<div class="m2"><p>بنگر تا چه آمدش حاصل</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>رفت و در گوشه‌ای به غم بنشست</p></div>
<div class="m2"><p>بر نهاده ز شرم دست به دست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>آمد از سِدره جبرئیل امین</p></div>
<div class="m2"><p>گفت کای سیّد زمان و زمین</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>مرد را اندر انتظار مدار</p></div>
<div class="m2"><p>وآنچه آورده است خوار مدار</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>مصطفی را ز حال کرد آگاه</p></div>
<div class="m2"><p>یلمزون المطوّعین ناگاه</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>مرد را انتظار چون دارند</p></div>
<div class="m2"><p>ملکوت آمده به نظّارند</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>زلزله اوفتاده در ملکوت</p></div>
<div class="m2"><p>نیست جای قرار و جای سکوت</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>حق تعالی چنین همی گوید</p></div>
<div class="m2"><p>دل او را به لطف می‌جوید</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>کای سرافراز، وی گزیده رسول</p></div>
<div class="m2"><p>اینقدر زود کن ز قیس قبول</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>که به نزد من این دقل بعیان</p></div>
<div class="m2"><p>بهتر از زرّ و گوهر دگران</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>زو پذیرفتم این متاع قلیل</p></div>
<div class="m2"><p>زانکه دستش رسید نیست بخیل</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>از همه چیزهای بگزیده</p></div>
<div class="m2"><p>هست جهدالمقل پسندیده</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>قیس را زان سبب برآمد کار</p></div>
<div class="m2"><p>زان منافق به فعل بد گفتار</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>گشت رسوا منافق اندر حال</p></div>
<div class="m2"><p>قیس را کار گشت از آن به کمال</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>تا بدانی که هرکه پیش آمد</p></div>
<div class="m2"><p>هم برآنسان که بود بیش آمد</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>با خدای آنکه تو دو دل باشد</p></div>
<div class="m2"><p>از همه فعل خود خجل باشد</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>راستی بهتر از همه کاری</p></div>
<div class="m2"><p>خوانده باشی تو اینقدر باری</p></div></div>