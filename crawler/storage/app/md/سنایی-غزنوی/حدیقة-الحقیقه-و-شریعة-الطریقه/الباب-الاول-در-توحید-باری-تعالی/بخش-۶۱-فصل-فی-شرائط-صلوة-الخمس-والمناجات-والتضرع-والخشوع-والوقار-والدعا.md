---
title: >-
    بخش ۶۱ - فصل فی شرائط صلوة الخمس والمناجات والتضرّع والخشوع والوقار والدعاء
---
# بخش ۶۱ - فصل فی شرائط صلوة الخمس والمناجات والتضرّع والخشوع والوقار والدعاء

<div class="n" id="bn1"><p>قال‌الله تعالی: الذین یؤمنون بالغیب و یقیمون الصلوة، و قال النّبی علیه‌السلام عند نزعه: و ما ملکت ایمانکم، و قال النّبی صلی اللّه علیه و سلم حبب الی من دنیاکم ثلاث: الطبیب والنساء و قرّة عینی فی‌الصلوة، و قال علیه‌السلام: من ترک الصلوة متعمداً فقد کفر و بین‌الاسلام والکفر ترک‌الصلوة، و قال: المصلی یناجی ربه، و قال علیه‌السلام لو علم المصلی من یناجی ما التفت و قال علیه‌السلام کن فی صلوتک خاشعا و قال علیه‌السلام: الصلوة نورالمؤمن، من اقام الصلوة اعطی الجنّة بالصلوة.</p></div>
<div class="b" id="bn2"><div class="m1"><p>بنده تا از حدث برون ناید</p></div>
<div class="m2"><p>پردهٔ عزّ نماز نگشاید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون کلید نماز پاکی تست</p></div>
<div class="m2"><p>قفل آن دان که عیبناکی تست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پای کی بر نهی به بام فلک</p></div>
<div class="m2"><p>باده کی در کشی ز جام ملک</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تات چون خر در این سرای خراب</p></div>
<div class="m2"><p>شکم از نان پرست و پشت از آب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا به زیر چهار و پنج و ششی</p></div>
<div class="m2"><p>باده جز از خم هوس نچشی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کی ترا حق به لطف برگیرد</p></div>
<div class="m2"><p>یا نمازت به طوع بپذیرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون دوم کرد امر یزدانت</p></div>
<div class="m2"><p>چار تکبیر بر سه ارکانت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>فوطه‌بافان عالم ازلت</p></div>
<div class="m2"><p>بر تو خوانند نکته و غزلت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>روی سلطان شرع کی بینی</p></div>
<div class="m2"><p>کون در آب و در آسمان بینی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>لقمه و خرقه هردو باید پاک</p></div>
<div class="m2"><p>ورنه گردی میان خاک هلاک</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چونت نبود طعام و کسوت پاک</p></div>
<div class="m2"><p>چه نمازت بود چه مشتی خاک</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به رعونت سوی نماز مپای</p></div>
<div class="m2"><p>شرم‌ دار و بترس تو ز خدای</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>سوی خود هرکه نیست بار خدای</p></div>
<div class="m2"><p>دهدش در نماز بار خدای</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>سگ به دُم جای خود بروبد و باز</p></div>
<div class="m2"><p>تو نروبی به آه جای نماز</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>از پی جاه و خدمت یزدان</p></div>
<div class="m2"><p>دار پاکیزه جای و جامه و جان</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>قبلهٔ جان ستانهٔ صمدست</p></div>
<div class="m2"><p>اُحد سینه کعبهٔ اَحدست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>در اُحد حمزه‌وار جان درباز</p></div>
<div class="m2"><p>تا بیابی مزه ز بانگ نماز</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>هرچه جز حق بسوز و غارت کن</p></div>
<div class="m2"><p>هرچه جز دین از آن طهارت کن</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>با نیازت به لطف برگیرند</p></div>
<div class="m2"><p>بی‌نیازت نماز نپذیرند</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بی‌نیاز ار غم نماز خوری</p></div>
<div class="m2"><p>از جگر قلیهٔ پیاز خوری</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>باز اگر هست با نماز نیاز</p></div>
<div class="m2"><p>برگِرَد دست لطف پردهٔ راز</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>هرکه در بارگاه لطف شتافت</p></div>
<div class="m2"><p>دادنی داد و جستنی دریافت</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ورنه ابلیس در درون نماز</p></div>
<div class="m2"><p>گوش گیرد برونت آرد باز</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>تو لئیم آمدی نماز کریم</p></div>
<div class="m2"><p>تو حدیث آمدی نماز قدیم</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>هفده رکعت نماز از دل و جان</p></div>
<div class="m2"><p>ملک هشده هزار عالم دان</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>پس مگو کین حساب باریکست</p></div>
<div class="m2"><p>زآنکه هفده به هشده نزدیکست</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>هرکه او هفده رکعه بگذارد</p></div>
<div class="m2"><p>ملک هشده هزار او دارد</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>حسد و خشم و بخل و شهوت و آز</p></div>
<div class="m2"><p>به خدای از گذاردت به نماز</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>تا حسد را ز دل برون ننهی</p></div>
<div class="m2"><p>از عملهای زشت او نرهی</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>چون نبیند ز دین غنیمت تو</p></div>
<div class="m2"><p>نکند هم نماز قیمت تو</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>قیمت تو عنان چو برتابد</p></div>
<div class="m2"><p>والله ار جبرئیل دریابد</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>طالب اوّل ز غسل درگیرد</p></div>
<div class="m2"><p>کز جُنُب حق نماز نپذیرد</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>تا ترا غل و غش درون باشد</p></div>
<div class="m2"><p>غسل ناکرده‌ای تو چون باشد</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>غسل ناکرده از صفات ذمیم</p></div>
<div class="m2"><p>نپذیرد نماز ربّ عظیم</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>گرچه پاکست هرچه بابت تست</p></div>
<div class="m2"><p>همه در جنب حق جنابت تست</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>اصل و فرع نماز غسل و وضوست</p></div>
<div class="m2"><p>صحت داء معضل از داروست</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>تا به جاروب لا نروبی راه</p></div>
<div class="m2"><p>نرسی در سرای الّا الله</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>چون ترا از تو دل برانگیزد</p></div>
<div class="m2"><p>پس نماز از نیاز برخیزد</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>ندهد سوی حق نماز جواز</p></div>
<div class="m2"><p>چون طهارت نکرده‌ای به نیاز</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>زاری و بی‌خودی طهارت تست</p></div>
<div class="m2"><p>کشتن نفس تو کفارت تست</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>چون بکشتی تو نفس را در راه</p></div>
<div class="m2"><p>روی بنمود زود فضل اِله</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>با نیاز آی تا بیابی بار</p></div>
<div class="m2"><p>ورنه یابی سبک طلاق سه بار</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>کان نمازی که در حضور بُوَد</p></div>
<div class="m2"><p>از تری آب روی دور بُوَد</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>تن چو در خاک رفت و جان به فلک</p></div>
<div class="m2"><p>روح خود در نماز بین چو مَلَک</p></div></div>