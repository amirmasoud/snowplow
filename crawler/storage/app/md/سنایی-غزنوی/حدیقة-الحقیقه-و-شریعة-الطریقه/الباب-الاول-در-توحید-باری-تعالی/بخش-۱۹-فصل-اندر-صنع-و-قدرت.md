---
title: >-
    بخش ۱۹ - فصل اندر صُنع و قدرت
---
# بخش ۱۹ - فصل اندر صُنع و قدرت

<div class="b" id="bn1"><div class="m1"><p>نقش بند برون گلها اوست</p></div>
<div class="m2"><p>نقش‌دان درون دلها اوست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صُنع او را مقدّمست عدم</p></div>
<div class="m2"><p>ذات او را مسلّم است قدم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا ترا کبر تیز خشم نکرد</p></div>
<div class="m2"><p>تا ترا چشم تو به چشم نکرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پای طاووس اگر چو پر بودی</p></div>
<div class="m2"><p>در شب و روز جلوه‌گر بودی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>که تواند نگاشت در آدم</p></div>
<div class="m2"><p>نقش‌بند قلم نگار قدم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عقل را کرده قایل سورت</p></div>
<div class="m2"><p>مایه را کرده قابل صورت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مبدع هست و آنچ ناهست او</p></div>
<div class="m2"><p>صانع دست و آنچ در دست او</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>قبلهٔ عقل صنع بی‌خللش</p></div>
<div class="m2"><p>کعبهٔ شوق، ذات بی بدلش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>عقل را داده راه بیداری</p></div>
<div class="m2"><p>تو همی عقل را چه پنداری</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سگ و سنگست گلخنی و رهی</p></div>
<div class="m2"><p>تو چو لعل از برون حقّه بهی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سیم بهر هزینه دارد شاه</p></div>
<div class="m2"><p>لعل بهر خزینه دارد شاه</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>سیم زار از نهاد وارونست</p></div>
<div class="m2"><p>لعل شاد از درون پرخونست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ساخت دولابی از زبرجد ناب</p></div>
<div class="m2"><p>کوزه سیمین ببست بر دولاب</p></div></div>