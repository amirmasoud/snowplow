---
title: >-
    بخش ۷۱ - فی‌الاخلاص والمخلصون علی خطر عظیم
---
# بخش ۷۱ - فی‌الاخلاص والمخلصون علی خطر عظیم

<div class="b" id="bn1"><div class="m1"><p>چون ز درگاهِ تست گو می‌مال</p></div>
<div class="m2"><p>خواب را زیر پای خیل خیال</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همچو شمع آنکه را نماند منی</p></div>
<div class="m2"><p>در تو خندد چو گردنش بزنی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با تو با جاه و عقل و زر چکنم</p></div>
<div class="m2"><p>دین و دنیا تویی دگر چکنم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تو مرا دل ده و دلیری بین</p></div>
<div class="m2"><p>رو به خویش خوان و شیرین بین</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر ز تیر تو پُر کنم ترکش</p></div>
<div class="m2"><p>کمر کوه قاف گیرم کش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یار آنی که بی‌خرد نبوَد</p></div>
<div class="m2"><p>وان آنی که آنِ خود نبوَد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>من چو درمانده‌ام درم بگشای</p></div>
<div class="m2"><p>ره چو گم کرده‌ام رهم بنمای</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هیچ خودبین خدای بین نبود</p></div>
<div class="m2"><p>مرد خود دیده مرد دین نبود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر تو مرد شریعت و دینی</p></div>
<div class="m2"><p>یک زمان دور شو ز خود بینی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ای خداوند کردگار غفور</p></div>
<div class="m2"><p>بنده را از درت مگردان دور</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بستهٔ خویش کن ببُر خوابم</p></div>
<div class="m2"><p>تشنهٔ خویش کن مده آبم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دل از این و از آن چه باید جست</p></div>
<div class="m2"><p>درد خود رهنمای مقصد تست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>عمر ضایع همی کنی در کار</p></div>
<div class="m2"><p>همچو خر پیش سبزه بی‌افسار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گرد هر شهر هرزه می‌گردی</p></div>
<div class="m2"><p>خر در آن ره طلب که گم کردی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>خر اگر در عراق دزدیدند</p></div>
<div class="m2"><p>پس ترا چون به یزد و ری دیدند</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>پُل بود پیش تا نگردی کل</p></div>
<div class="m2"><p>چون شدی کل ترا چه بحر و چه پُل</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>اندرین ره ز داد و دانش خویش</p></div>
<div class="m2"><p>بار ساز و ز هیچ پل مندیش</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>قصد کشتی مکن که پر خطرست</p></div>
<div class="m2"><p>مرد کشتی ز بحر بی‌خبرست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>گرچه نو خیز و نو گرفت بود</p></div>
<div class="m2"><p>بط کشتی طلب شگفت بود</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بچهٔ بط اگر چه دینه بود</p></div>
<div class="m2"><p>آب دریاش تا به سینه بود</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>تو چو بط باش و دنیی آب روان</p></div>
<div class="m2"><p>ایمن از قعر بحر بی‌پایان</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بچهٔ بط مین بحر عمان</p></div>
<div class="m2"><p>خربطی باز گشته کشتی‌بان</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>یارب این خربطان عالم را</p></div>
<div class="m2"><p>کم کن از بهر عزّ آدم را</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>قدم ار در ره قِدم داری</p></div>
<div class="m2"><p>قُلزمی را ز دست نگذاری</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>قدمی را که با قِدم بقل‌ست</p></div>
<div class="m2"><p>سطح بیرونی محیط پل‌ست</p></div></div>