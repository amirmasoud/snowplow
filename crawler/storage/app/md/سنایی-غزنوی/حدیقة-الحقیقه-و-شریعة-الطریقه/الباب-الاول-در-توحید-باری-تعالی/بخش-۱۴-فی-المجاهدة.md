---
title: >-
    بخش ۱۴ - فی‌المُجاهدة
---
# بخش ۱۴ - فی‌المُجاهدة

<div class="b" id="bn1"><div class="m1"><p>چون تو از بود خویش گشتی نیست</p></div>
<div class="m2"><p>کمر جهد بند و در ره ایست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون کمر بسته ایستادی تو</p></div>
<div class="m2"><p>تاج بر فرق دل نهادی تو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تاج اقبال بر سرِ دل نه</p></div>
<div class="m2"><p>پای ادبار بر خور و گل نه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گرت باید که سست گردد زه</p></div>
<div class="m2"><p>اولا پوستین به گازر ده</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گرچه غافل برین عمل خندد</p></div>
<div class="m2"><p>لیک عاقل جز این بنپسندد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پوستین باز کن که تا در شاه</p></div>
<div class="m2"><p>پوستین در بسی است اندر راه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به نخستین قدم که زد آدم</p></div>
<div class="m2"><p>پوستینش درید گرگ ستم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نه چو قابیل تشنه شد به جفا</p></div>
<div class="m2"><p>داد هابیل پوستین به فنا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نه چو ادریس پوستین بفکند</p></div>
<div class="m2"><p>درِ فردوس را ندید به بند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چون خلیل از ستاره و مه و خور</p></div>
<div class="m2"><p>پوستینها درید بی‌غم خور</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شب او همچو روز روشن شد</p></div>
<div class="m2"><p>نار نمرود تازه گلشن شد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به سلیمان نگر که از سرِ داد</p></div>
<div class="m2"><p>پوستین امل به گازر داد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>جن و انس و طیور و مور و ملخ</p></div>
<div class="m2"><p>در بُن آب قلزم و سرِ شخ</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>روی او را همه رفیع شدند</p></div>
<div class="m2"><p>رای او را همه مطیع شدند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ز آتش دل چو سوخت آب نهاد</p></div>
<div class="m2"><p>خاک بر دوش باد چرخ نهاد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چون کلیم کریم غم پرورد</p></div>
<div class="m2"><p>رخ به مدین نهاد با غم و درد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>پوستین را ز روی مزدوری</p></div>
<div class="m2"><p>برکشید از نهاد رنجوری</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>کرده ده سال چاکری شعیب</p></div>
<div class="m2"><p>تا گشادند بر دلش درِ غیب</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>دست او همچو چشم بینا شد</p></div>
<div class="m2"><p>پای او تاج فرق سینا شد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>روح چون دم ز بحر روحانی</p></div>
<div class="m2"><p>زد و پذ رفت لطف ربانی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>پوستین را به اولین منزل</p></div>
<div class="m2"><p>بفرستید سوی گازر دل</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>دل چو او را فر آلهی داد</p></div>
<div class="m2"><p>هم به خردیش پادشاهی داد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>گشت بی‌او به قدرت ازلی</p></div>
<div class="m2"><p>از ثناء خفی و لطف جلی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>تن ابرص از او چو سایهٔ فرش</p></div>
<div class="m2"><p>چشم اکمه ازو چو پایهٔ عرش</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>هرکه چون او به نام جوید ننگ</p></div>
<div class="m2"><p>از یکی خُم برآورد ده رنگ</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>پشک با او چو مشک شد بویا</p></div>
<div class="m2"><p>زنده کردار مردگان گویا</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>گل دل را زلطف جان سر کرد</p></div>
<div class="m2"><p>دل گل را ز دست جان درکرد</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>چون دکان را به مهر کرد قضا</p></div>
<div class="m2"><p>دست تقدیر در نشیب فنا</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>ماند عالم پر از هوا و هوس</p></div>
<div class="m2"><p>گشت بازار پر عوان و عسس</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>شحنه‌ای را ز بهر دفع ستم</p></div>
<div class="m2"><p>بفرستاد اندرین عالم</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>چون شد از آسمان دل ظاهر</p></div>
<div class="m2"><p>هم به جان مست و هم به تن ظاهر</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>پوستین خود نداشت در ره دین</p></div>
<div class="m2"><p>پس چه دادی به گازران زمین</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>از فنا چون سوی بقا آمد</p></div>
<div class="m2"><p>زینت و زیب این فنا آمد</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>هرکه گشت از برای او خاموش</p></div>
<div class="m2"><p>سخن او حیات باشد و نوش</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>گر نگوید ز کاهلی نبود</p></div>
<div class="m2"><p>ور بگوید ز جاهلی نبود</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>دیدی ای خواجهٔ سخن فربه</p></div>
<div class="m2"><p>که ترا در دل از سخن فر به</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>در خموشی نبوده لهو اندیش</p></div>
<div class="m2"><p>گاه گفتن نبوده لغو پریش</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>بسته از جد و جهد و عشق و طلب</p></div>
<div class="m2"><p>بر گریبان روز دامن شب</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>روز و شب را به مسطر انصاف</p></div>
<div class="m2"><p>تسویت داده به ز هرچه گزاف</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>از درونش چوبوی جان یابند</p></div>
<div class="m2"><p>بی‌زبانان همه زبان یابند</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>تو در این گفت من مدار شکی</p></div>
<div class="m2"><p>باز کن دیده بر گمار یکی</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>در رهش خوانده عاشقان بر جان</p></div>
<div class="m2"><p>آیهٔ کُلُّ مَن عَلَیها فاَن</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>آن سفیهان که دزد و طرّارند</p></div>
<div class="m2"><p>عقل را بهر ره زدن دارند</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>صُنع او عدل و حکمتست و جلی</p></div>
<div class="m2"><p>ملک او قهر و عزتست و خفی</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>پیکر آب و گل ز قهرش عور</p></div>
<div class="m2"><p>لعبت چشم و دل ز کنهش کور</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>عقل آلوده از پی دیدار</p></div>
<div class="m2"><p>اَرَنی گوی گشته موسی‌وار</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>چون برون آمد از تجلی پیک</p></div>
<div class="m2"><p>گفت در گوش او که تبت اِلیک</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>صفت ذات او به علم بدان</p></div>
<div class="m2"><p>نام پاکش هزار و یک برخوان</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>وصف او زیر علم نیکو نیست</p></div>
<div class="m2"><p>هرچه در گوشت آمد آن او نیست</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>نقطه و خط و سطح با صفتش</p></div>
<div class="m2"><p>هست چون جسم و بُعد و شش جهتش</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>مُبدع آن سه از ورای مکان</p></div>
<div class="m2"><p>خالق این سه از درون زمان</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>هیچ عاقل درو نبیند عیب</p></div>
<div class="m2"><p>او بداند درون عالم غیب</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>مطّلع بر ضمائر و اسرار</p></div>
<div class="m2"><p>نوز ناکرده بر دل تو گذار</p></div></div>