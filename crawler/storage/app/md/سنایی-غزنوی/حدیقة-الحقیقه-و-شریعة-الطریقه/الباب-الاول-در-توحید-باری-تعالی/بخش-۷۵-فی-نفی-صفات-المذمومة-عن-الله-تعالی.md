---
title: >-
    بخش ۷۵ - فی نفی صفات المذمومِة عن‌الله تعالی
---
# بخش ۷۵ - فی نفی صفات المذمومِة عن‌الله تعالی

<div class="b" id="bn1"><div class="m1"><p>در حق حقّ غضب روا نبود</p></div>
<div class="m2"><p>زانکه صاحب غضب خدا نبود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غضب و حقد هر دو مجبورند</p></div>
<div class="m2"><p>وین صفت هردو از خدا دورند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>غضب و خشم و کین و حقد و حسد</p></div>
<div class="m2"><p>نیست اندر صفات فرد اَحد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همه رحمت بود ز خالق بار</p></div>
<div class="m2"><p>هست بر بندگان خود ستّار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>می‌دهد مر ترا به رحمت پند</p></div>
<div class="m2"><p> به خودت می‌کشد به لطف کمند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر نیایی بخواندت سوی خویش</p></div>
<div class="m2"><p>به تلطّف بهشت آرد پیش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زانکه هستی بدین سرای دریغ</p></div>
<div class="m2"><p>تو گرفته ز جهل راه گُریغ</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دُرّ توحید را تویی چو صدف</p></div>
<div class="m2"><p>آدم تازه را شدی تو خلف</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر کنی ضایع آن در توحید</p></div>
<div class="m2"><p>شوی از مفلسی ز مایه فرید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ور تو آن درّ را نگهداری</p></div>
<div class="m2"><p>سر ز هفت و چهار بگذاری</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به سرور ابد رسی پس از آن</p></div>
<div class="m2"><p>نرسد مر ترا ز خلق زیان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>در زمانه تو سرفراز شوی</p></div>
<div class="m2"><p>در فضای ازل چو باز شوی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>دست شاهان ترا شود منزل</p></div>
<div class="m2"><p>هر دو پایت برآید از بُن گِل</p></div></div>