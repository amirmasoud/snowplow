---
title: >-
    بخش ۶ - التمثیل فی شأن من کان فی هذه اعمی فهو فی‌الآخرة اعمی  جماعة العمیان و احوال الفیل
---
# بخش ۶ - التمثیل فی شأن من کان فی هذه اعمی فهو فی‌الآخرة اعمی  جماعة العمیان و احوال الفیل

<div class="b" id="bn1"><div class="m1"><p>بود شهری بزرگ در حدِ غور</p></div>
<div class="m2"><p>واندر آن شهر مردمان همه کور</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پادشاهی در آن مکان بگذشت</p></div>
<div class="m2"><p>لشکر آورد و خیمه زد بر دشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>داشت پیلی بزرگ با هیبت</p></div>
<div class="m2"><p>از پی جاه و حشمت و صولت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مردمان را ز بهر دیدن پیل</p></div>
<div class="m2"><p>آرزو خاست زانچنان تهویل</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چند کور از میان آن کوران</p></div>
<div class="m2"><p>برِ پیل آمدند از آن عوران</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا بدانند شکل و هیآت پیل</p></div>
<div class="m2"><p>هریکی تازیان در آن تعجیل</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آمدند و به دست می‌سودند</p></div>
<div class="m2"><p>زانکه از چشم بی‌بصر بودند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هریکی را به لمس بر عضوی</p></div>
<div class="m2"><p>اطلاع اوفتاد بر جزوی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هریکی صورت محالی بست</p></div>
<div class="m2"><p>دل و جان در پی خیالی بست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چون برِ اهل شهر باز شدند</p></div>
<div class="m2"><p>برشان دیگران فراز شدند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آرزو کرد هریکی زیشان</p></div>
<div class="m2"><p>آنچنان گمرهان و بدکیشان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>صورت و شکل پیل پرسیدند</p></div>
<div class="m2"><p>وآنچه گفتند جمله بشنیدند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>آنکه دستش بسوی گوش رسید</p></div>
<div class="m2"><p>دیگری حال پیل ازو پرسید</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گفت شکلیست سهمناک عظیم</p></div>
<div class="m2"><p>پهن و صعب و فراخ همچو گلیم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>وانکه دستش رسیدی زی خرطوم</p></div>
<div class="m2"><p>گفت گشتست مر مرا معلوم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>راست چون ناودان میانه تهیست</p></div>
<div class="m2"><p>سهمناکست و مایهٔ تبهیست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>وانکه را بُد ز پیل ملموسش</p></div>
<div class="m2"><p>دست و پای سطبر پر بوسش</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گفت شکلش چنانکه مضبوط است</p></div>
<div class="m2"><p>راست همچون عمود مخروط است</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>هریکی دیده جزوی از اجزا</p></div>
<div class="m2"><p>همگان را فتاده ظن خطا</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>هیچ دل را ز کلی آگه نی</p></div>
<div class="m2"><p>علم با هیچ کور همره نی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>جملگی را خیالهای محال</p></div>
<div class="m2"><p>کرده مانند غتفره به جوال</p></div></div>
<div class="b" id="bn22"><div class="m1"><p> از خدایی خلایق آگه نیست</p></div>
<div class="m2"><p>عقلا را در این سخن ره نیست</p></div></div>