---
title: >-
    بخش ۴ - فصل تنزیه قِدم
---
# بخش ۴ - فصل تنزیه قِدم

<div class="b" id="bn1"><div class="m1"><p>دهر بی‌قالب قدیمی او</p></div>
<div class="m2"><p>طبع بی باعث کریمی او</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نشود دهر و طبع بی‌قولش</p></div>
<div class="m2"><p>همچو جان از نهاد بی‌طولش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>این و آن هردو ناقص و ابتر</p></div>
<div class="m2"><p>این و آن هردو ابله و بی‌بر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مادت او زکهنه و نو نیست</p></div>
<div class="m2"><p>اوست کز هستها به جز او نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بنهایت نه ملک او معروف</p></div>
<div class="m2"><p>به بدایت نه ذات او موصوف</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زرق و تلبیس ومخرقه نخرد</p></div>
<div class="m2"><p>سوی توحید و صدق به نگرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دیدهٔ عقل‌بین گزیند حق</p></div>
<div class="m2"><p>دیدهٔ رنگ‌بین نبیند حق</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>باطل است آنچه دیده آراید</p></div>
<div class="m2"><p>حق در اوهام آب و گل ناید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>عقل باشد به خلط و وهم محیط</p></div>
<div class="m2"><p>هر دوان لیک بر بساط بسیط</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خلق را ذات چون نماید او</p></div>
<div class="m2"><p>در کدام آینه درآید او</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>جای و جان هر دو پیشکار تواند</p></div>
<div class="m2"><p>کوتوال و نفس شمار تواند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چون برون آمدی ز جان و ز جای</p></div>
<div class="m2"><p>پس ببینی خدای را به خدای</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بار توحید هرکسی نکشد</p></div>
<div class="m2"><p>طعم توحید هر خسی نچشد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>هست در هر مکان خدا معبود</p></div>
<div class="m2"><p>نیست معبود در مکان محدود</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>مرد جسمی ز راه گمراهست</p></div>
<div class="m2"><p>کفر و تشبیه هر دو همراهست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>در ره صدق نفس را بگذار</p></div>
<div class="m2"><p>خیز و زین نفس شوم دست بدار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>از درونت نگاشت صنع آله</p></div>
<div class="m2"><p>نه ز زرد و سپید و سرخ و سیاه</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>وز برونت نگاشتهٔ افلاک</p></div>
<div class="m2"><p>از چه از باد و آب و آتش و خاک</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>فعل و ذاتش برون ز آلت و سوست</p></div>
<div class="m2"><p>بس که هویتش پر از کن و هوست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>کن دو حرفست بی‌نوا هر دو</p></div>
<div class="m2"><p>هر دو بحر است بی‌هوا هر دو</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ذات او سوی عارف و عالم</p></div>
<div class="m2"><p>برتر از اَیْن و کَیف وز هل و لم</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>دادهٔ خود سپهر بستاند</p></div>
<div class="m2"><p>نقش الله جاودان ماند</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>آنکه بی‌رنگ زد ترا نیرنگ</p></div>
<div class="m2"><p>باز نستاند از تو هرگز رنگ</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>نگذارد به تو فلک جاوید</p></div>
<div class="m2"><p>رنگ زرد و سیاه و سرخ و سپید</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>جمع کرد از پی تو بیش از تو</p></div>
<div class="m2"><p>آنچه اسباب تست پیش از تو</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>آفریدت ز صنع در تکلیف</p></div>
<div class="m2"><p>کرد فضلش ترا به خود تعریف</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>گفت گنجی بُدم نهانی من</p></div>
<div class="m2"><p>خُلق الخلق تا بدانی من</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>کرده از کاف و نون به درّ ثمین</p></div>
<div class="m2"><p>دیده را یک دهان پر از یاسین</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>زیر گردون به امر و صنع خدای</p></div>
<div class="m2"><p>ساخته چار خصم بر یک جای</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>جمع ایشان دلیل قدرت اوست</p></div>
<div class="m2"><p>قدرتش نقشبند حکمت اوست</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>همه اضداد و لیک ز امر آله</p></div>
<div class="m2"><p>همه با یکدگر شده همراه</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>همه را ابد به امر قدم</p></div>
<div class="m2"><p>زده نیرنگ در سرای عدم</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>چار گوهر به سعی هفت اختر</p></div>
<div class="m2"><p>شده بیرنگ را گزارشگر</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>آنکه بی‌خامه زد ترا نیرنگ</p></div>
<div class="m2"><p>هم تواند گزاردن بی‌رنگ</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>نیست گویی جهان ز زشت و نکو</p></div>
<div class="m2"><p>جز از او و بدو و بلکه خود او</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>همه زو یافته نگار و صوَر</p></div>
<div class="m2"><p>هم هیولانی اصل و هم پیکر</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>عنصر و مادهٔ هیولانی</p></div>
<div class="m2"><p>طبع و الوان چار ارکانی</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>همه را غایت تناهی دان</p></div>
<div class="m2"><p>نردبان پایهٔ آلهی دان</p></div></div>