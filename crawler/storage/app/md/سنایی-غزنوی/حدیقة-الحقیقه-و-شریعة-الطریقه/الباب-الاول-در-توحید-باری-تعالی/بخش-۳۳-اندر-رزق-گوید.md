---
title: >-
    بخش ۳۳ - اندر رزق گوید
---
# بخش ۳۳ - اندر رزق گوید

<div class="b" id="bn1"><div class="m1"><p>جانور را چو خوانش پیش نهاد</p></div>
<div class="m2"><p>خوردنی از خورنده بیش نهاد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همه را روح و روز و روزی از اوست</p></div>
<div class="m2"><p>نیک‌بختی و نیک روزی از اوست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>روزی هریکی پدید آورد</p></div>
<div class="m2"><p>در انبارخانه مُهر نکرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کافر و مؤمن و شقی و سعید</p></div>
<div class="m2"><p>همه را روزی و حیات جدید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حاء حاجت هنوزشان در حلق</p></div>
<div class="m2"><p>جیم جودش بداده روزی خلق</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جز بنان نیست پرورش ما را</p></div>
<div class="m2"><p>جز شره نیست نان خورش ما را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>او ز توجیه بندگان نجهد</p></div>
<div class="m2"><p>نان خورش داد نان همو بدهد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نان و جان تو در خزینهٔ اوست</p></div>
<div class="m2"><p>تو نداری خبر دفینهٔ اوست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>روزی تو اگر به چین باشد</p></div>
<div class="m2"><p>اسب کسب تو زیر زین باشد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>یا ترا نزد او برد به شتاب</p></div>
<div class="m2"><p>ورنه او را برِ تو، تو در خواب</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نه ترا گفت رازق تو منم</p></div>
<div class="m2"><p>عالمِ سرّ و عالمِ علنم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>جان بدادم وجوه نان بدهم</p></div>
<div class="m2"><p>هرچه خواهی تو در زمان بدهم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>کار روزی چو روز دان بدرست</p></div>
<div class="m2"><p>که ره آورد روز روزی تست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>سفله دارد ز بهر روزی بیم</p></div>
<div class="m2"><p>نخورد دیگ گرده حکیم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نخورد شیر صید خود تنها</p></div>
<div class="m2"><p>چون شود سیر مانده کرد رها</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>با تو زانجا که لطف یزدانست</p></div>
<div class="m2"><p>گرو نان به دست تو جانست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>غم جان خور که آنِ نان خورده است</p></div>
<div class="m2"><p>تا لب گور گرده بر گرده است</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>جان بی‌نان به کس نداد خدای</p></div>
<div class="m2"><p>زانکه از نان بماند جان بر جای</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>این گرو سخت‌دار و نان می‌خور</p></div>
<div class="m2"><p>چون گرو رفت قوت جان می‌خور</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>مر زنان راست کهنه تو بر تو</p></div>
<div class="m2"><p>مرد را روز نو و روزی نو</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>روزی تست بر علیم و قدیر</p></div>
<div class="m2"><p>تو ز میر و وکیل خشم مگیر</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>آن زمانی که جان ز تن برمید</p></div>
<div class="m2"><p>به یقین دان که روزیت برسید</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>روزیت از درِ خدای بُوَد</p></div>
<div class="m2"><p>نه ز دندان و حلق و نای بُوَد</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>کدخدایی خدایی است برنج</p></div>
<div class="m2"><p>خاصه آنرا که نیست حکمت و گنج</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>کدخدایی همه غم و هوس است</p></div>
<div class="m2"><p>کد رها کن ترا خدای بس است</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>اعتماد تو در همه احوال</p></div>
<div class="m2"><p>بر خدا به که بر خراس و جوال</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ابر اگر نم نداد یک سالت</p></div>
<div class="m2"><p>سخت شوریده بینم احوالت</p></div></div>