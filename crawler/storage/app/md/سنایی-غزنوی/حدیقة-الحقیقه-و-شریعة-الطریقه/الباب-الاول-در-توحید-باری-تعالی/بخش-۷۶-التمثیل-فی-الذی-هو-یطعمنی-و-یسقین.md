---
title: >-
    بخش ۷۶ - التمثیل فی‌الذی هو یُطعمنی و یَسقین
---
# بخش ۷۶ - التمثیل فی‌الذی هو یُطعمنی و یَسقین

<div class="b" id="bn1"><div class="m1"><p>باز را چون ز بیشه صید کنند</p></div>
<div class="m2"><p>گردن و هردو پاش قید کنند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر دو چشمش سبک فرو دوزند</p></div>
<div class="m2"><p>صید کردن ورا بیاموزند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خو ز اغیار و عاده باز کند</p></div>
<div class="m2"><p>چشم از آن دیگران فراز کند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اندکی طعمه را شود راضی</p></div>
<div class="m2"><p>یاد نارد ز طعمهٔ ماضی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>باز دارش ز خود پیاده کند</p></div>
<div class="m2"><p>گوشهٔ چشم او گشاده کند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا همه بازدار را بیند</p></div>
<div class="m2"><p>خلق بر بازدار نگزیند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زو ستاند همه طعام و شراب</p></div>
<div class="m2"><p>نرود ساعتی بی‌او در خواب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بعد آن برگشایدش یک چشم</p></div>
<div class="m2"><p>در رضا بنگرد درو نه به خشم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از سرِِ رسم و عاده برخیزد</p></div>
<div class="m2"><p>با دگر کس به طبع نامیزد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بزم و دستِ ملوک را شاید</p></div>
<div class="m2"><p>صیدگه را بدو بیاراید</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چون ریاضت نیافت وحشی ماند</p></div>
<div class="m2"><p>هرکه دیدش ز پیش خویش براند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بی‌ریاضت نیافت کس مقصود</p></div>
<div class="m2"><p>تا نسوزی ترا چه بید و چه عود</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>فرخ آنکو همه طعام و شراب</p></div>
<div class="m2"><p>از مسبّب ستد نه از اسباب</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>رو ریاضت‌کش ارت باید باز</p></div>
<div class="m2"><p>ورنه راه جحیم را می‌ساز</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>دیگران غافلند تو هُش‌دار</p></div>
<div class="m2"><p>واندرین ره زبانت خامش‌دار</p></div></div>