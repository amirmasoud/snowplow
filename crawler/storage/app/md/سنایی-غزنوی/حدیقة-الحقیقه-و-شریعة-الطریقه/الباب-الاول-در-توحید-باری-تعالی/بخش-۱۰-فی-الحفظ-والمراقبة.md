---
title: >-
    بخش ۱۰ - فی‌الحفظ والمراقبة
---
# بخش ۱۰ - فی‌الحفظ والمراقبة

<div class="b" id="bn1"><div class="m1"><p>هرکه را عون حق حصار شود</p></div>
<div class="m2"><p>عنکبوتیش پرده‌دار شود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سوسماری ثنای او گوید</p></div>
<div class="m2"><p>اژدهائی رضای او جوید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نعل او فرق عرش را ساید</p></div>
<div class="m2"><p>لعل او زیب فرش را شاید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زهر در کام او شکر گردد</p></div>
<div class="m2"><p>سنگ در دست او گهر گردد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هرکه او سر برین ستانه نهد</p></div>
<div class="m2"><p>پای بر تارک زمانه نهد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عقل درمانده را بدین درخواند</p></div>
<div class="m2"><p>زانکه درماند هرکه زین درماند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ترسم از جاهلی و نادانی</p></div>
<div class="m2"><p>ناگهان بر صراط درمانی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جاهلی مر ترا به نار دهد</p></div>
<div class="m2"><p>تا ترا کوک و کوکنار دهد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>لقمه دیدی که مرد می‌خاید</p></div>
<div class="m2"><p>گندمی زان میان برون آید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بوده پیش جراد و مرغ و ستور</p></div>
<div class="m2"><p>دیده تاب خراس و تف تنور</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>داشته زیر آسیای تو پای</p></div>
<div class="m2"><p>که نگه داشتش خدای خدای</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>از پیِ حفظ مال و نفس و نفس</p></div>
<div class="m2"><p>او ترا بس تو کرده‌ای زو بس</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>من بگویم ترا به عقل و به هوش</p></div>
<div class="m2"><p>گر ببندی تو پند من در گوش</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>سگ و زنجیر چون به دست آری</p></div>
<div class="m2"><p>آهوی دشت را شکست آری</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>پس بدین اعتقاد و این اخلاص</p></div>
<div class="m2"><p>از برای معاش و کسب خلاص</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>اعتماد تو بر سگ و زنجیر</p></div>
<div class="m2"><p>بیش بینم که بر سمیع و بصیر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>نور ایمانت را در این بنیاد</p></div>
<div class="m2"><p>آهنی و سگی به غارت داد</p></div></div>