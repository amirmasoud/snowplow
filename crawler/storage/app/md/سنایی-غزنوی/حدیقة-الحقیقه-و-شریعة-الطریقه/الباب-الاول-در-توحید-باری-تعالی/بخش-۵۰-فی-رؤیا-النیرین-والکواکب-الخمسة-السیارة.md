---
title: >-
    بخش ۵۰ - فی رؤیا النّیّرین والکواکب الخمسة السیّارة
---
# بخش ۵۰ - فی رؤیا النّیّرین والکواکب الخمسة السیّارة

<div class="b" id="bn1"><div class="m1"><p>دیدن آفتاب را در خواب</p></div>
<div class="m2"><p>پادشه گفته‌اند از هر باب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ماه مانند رایزن باشد</p></div>
<div class="m2"><p>دگری گفت نه که زن باشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جرم مریخ یا زحل در خواب</p></div>
<div class="m2"><p>صاحب محنتست و رنج و عذاب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تیر مانندهٔ دبیر آمد</p></div>
<div class="m2"><p>مشتری خازن و وزیر آمد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زهره خود هست مایهٔ آرامش</p></div>
<div class="m2"><p>مایهٔ عیش و کام و آرامش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>وآن دگر کوکبان برادر دان</p></div>
<div class="m2"><p>گاه تعبیرشان برادر خوان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>همچو یعقوب کین طریق نهاد</p></div>
<div class="m2"><p>راز این علم بر پسر تقریر بگشاد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مهر و ماهش پدر بُد و مادر</p></div>
<div class="m2"><p>کوکبان چون برادران در خور</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بس کن از فال و زجر وز تعبیر</p></div>
<div class="m2"><p>در گذر زین که کرده‌ای </p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کس چو ما دید خیره غمخواران</p></div>
<div class="m2"><p>می‌گذاریم خواب بیداران</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خفته بیدار کردن آسانست</p></div>
<div class="m2"><p>غافل و مرده هر دو یکسانست</p></div></div>