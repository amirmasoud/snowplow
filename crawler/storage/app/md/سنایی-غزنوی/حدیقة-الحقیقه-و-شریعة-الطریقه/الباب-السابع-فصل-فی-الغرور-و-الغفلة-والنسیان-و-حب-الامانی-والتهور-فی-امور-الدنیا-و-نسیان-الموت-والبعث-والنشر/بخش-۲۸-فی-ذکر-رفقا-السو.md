---
title: >-
    بخش ۲۸ - فی ذکر رفقاء السّوء
---
# بخش ۲۸ - فی ذکر رفقاء السّوء

<div class="b" id="bn1"><div class="m1"><p>دوستی با مُقامر و قلّاش</p></div>
<div class="m2"><p>یا مکن یا چو کردی آن را باش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دوستی کز پی پیاله کنند</p></div>
<div class="m2"><p>ندهی پوست پوست کاله کنند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دوست خواهی که تا بماند دوست</p></div>
<div class="m2"><p>آن طلب زو که طبع و خاطر اوست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بد کسی دان که دوست کم دارد</p></div>
<div class="m2"><p>زو بتر چون گرفت بگذارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دوست گرچه دو صد دو یار بُوَد</p></div>
<div class="m2"><p>دشمن ارچه یکی هزار بُوَد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مر ترا خصم و دشمن دانا</p></div>
<div class="m2"><p>بهتر از دوستان همه کانا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از تقی دین طلب ز رعنا لاف</p></div>
<div class="m2"><p>از صدف دُر طلب ز آهو ناف</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آستین ار ز هیچ خواهی پُر</p></div>
<div class="m2"><p>از صدف مشک جو وز آهو دُر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آنچه از حس چشم و بینی و گوش</p></div>
<div class="m2"><p>زین ببین زان ببوی و زان بنیوش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ناید از گوشها جهان‌بینی</p></div>
<div class="m2"><p>نچشد چشم و نشنود بینی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از حواس ار بجویی این همه ساز</p></div>
<div class="m2"><p>آن ازین این از آن نیابی باز</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>که پدیدست در جهان باری</p></div>
<div class="m2"><p>کار هر مرد و مرد هر کاری</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گر نخواهی دل از ندامت پر</p></div>
<div class="m2"><p>به بدی از رفیق نیک مبر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گرچه صد بار باز گردد یار</p></div>
<div class="m2"><p>سوی او باز گرد چون طومار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>زین بدان رخ همی بگردانی</p></div>
<div class="m2"><p>باش تا قدر این بدان دانی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>دوستان گنج‌خانهٔ رازند</p></div>
<div class="m2"><p>رنج‌بردار و گنج پردازند</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>با نفایه و سره به خفت و به خیز</p></div>
<div class="m2"><p>نه درآمیز چُست و نه بگریز</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>نه طلب زین ستوده دان نه هرب</p></div>
<div class="m2"><p>که چنین آمد از حکیم عرب</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>صفت دوست از ره تحقیق</p></div>
<div class="m2"><p>از علی بشنو ار نه‌ای زندیق</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>دوست نادان بود نباید سوخت</p></div>
<div class="m2"><p>باید این حکمت از علی آموخت</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>خلق دشمن شود چو بگریزی</p></div>
<div class="m2"><p>بد قرین گردی ار درآمیزی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چون ترا دوستی پدید آید</p></div>
<div class="m2"><p>عقل باید که زود نستاید</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>وقت عشرت از او به کم دیدن</p></div>
<div class="m2"><p>کم شنیدن به از پسندیدن</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>مطلب گرچه جزم فرمانی</p></div>
<div class="m2"><p>پیکی از مقعدان زندانی</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>آن طلب کن که داند و دارد</p></div>
<div class="m2"><p>تا تو از وی وی از تو نازارد</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>دوستی با مزاج بی‌خردی</p></div>
<div class="m2"><p>دور دور و هم ایدرست خودی</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>تا نباشی حریف بی‌خردان</p></div>
<div class="m2"><p>که نکو کار بد شود ز بدان</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>باد کز لطف اوست جان بر کار</p></div>
<div class="m2"><p>زهر گردد همی به صحبت مار</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>یار بد همچو خاردان بدرست</p></div>
<div class="m2"><p>که همی دامنت بگیرد چُست</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>زرد رویی زر از قریت بدست</p></div>
<div class="m2"><p>ورنه سرخست تا قرین خودست</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>صحبت باغها به فصل بهار</p></div>
<div class="m2"><p>باد را هر زمان کند عطبار</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>روغن کنجدی که بودی عام</p></div>
<div class="m2"><p>شد ز گلها عزیز و نیکو نام</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>چون به گلها سپرد نفس و نفس</p></div>
<div class="m2"><p>روغن کنجدش نخواند کس</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>این برست از سبو و آن از ذُل</p></div>
<div class="m2"><p>گل از او نیکنام و از گُل</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>با بدان کم نشین که بد مانی</p></div>
<div class="m2"><p>خو پذیر است نفس انسانی</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>خوش خو از بدخوان سترگ شود</p></div>
<div class="m2"><p>میش چون گرگ خورد گرگ شود</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>صحبت نیک را ز دست مده</p></div>
<div class="m2"><p>که مِه و به شوی ز صحبت مه</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>اسب توسن ز اسب ساکن رگ</p></div>
<div class="m2"><p>گشت هم خو اگر نشد هم تگ</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>گر بدی صورتت شود مسته</p></div>
<div class="m2"><p>بد دانا ز نیک نادان به</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>هیچ صحبت مباد با عامت</p></div>
<div class="m2"><p>که چو خود مختصر کند نامت</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>صحبت عام آتش و پنبه است</p></div>
<div class="m2"><p>زشت نام و تباه و استنبه است</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>صحبت عام در بهشت آباد</p></div>
<div class="m2"><p>مرگ باشد که مرگ عامی باد</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>با دو عاقل هوا نیامیزد</p></div>
<div class="m2"><p>یک هوا از دو عقل بگریزد</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>با بد و نیک جسم داند زیست</p></div>
<div class="m2"><p>جان شناسد که دوست و دشمن کیست</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>دوستی را که نیست با تو مجال</p></div>
<div class="m2"><p>که بگوید حرام نیست حلال</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>با تو تا لقمه دید جان و دلست</p></div>
<div class="m2"><p>چون شدت لقمه تیز و تیغ و شلست</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>شکمش چون دل پیاله ببین</p></div>
<div class="m2"><p>وز دهانش دل چو لاله ببین</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>با کُله کی بُوَد اخوّت پاک</p></div>
<div class="m2"><p>زانکه گفتند اخوک من و اساک</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>جامهٔ خون و گوشت پوست بود</p></div>
<div class="m2"><p>عیبهٔ عیب دوست دوست بود</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>نیست در هیچ یار صدق و صفا</p></div>
<div class="m2"><p>نیست با هیچ دوست مهر و وفا</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>چون به علت کند سلام علیک</p></div>
<div class="m2"><p>از بد و نیک تو شود بد و نیک</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>دوست و دشمن برای جان باید</p></div>
<div class="m2"><p>تن بود کش غذای نان باید</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>گر کنی چشم جفت بی‌خوابی</p></div>
<div class="m2"><p>دوستی با خلاص کم یابی</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>مر ترا زو وفا نخواهد خواست</p></div>
<div class="m2"><p>که تنوریست با ترازو راست</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>پس تو اکنون مه به مه بد را باش</p></div>
<div class="m2"><p>دامن خویش گیر و خود را باش</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>که بود عهد و عشق لقمه زنان</p></div>
<div class="m2"><p>بی‌مدد چون چراغ بیوه زنان</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>صلح دشمن چو جنگ دوست بُوَد</p></div>
<div class="m2"><p>که از آن مغز آن چو پوست بُوَد</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>دل در ایشان مبند کز گیهان</p></div>
<div class="m2"><p>همه آدم دمند و مرجان جان</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>نیک را از بدان چه جاه بُوَد</p></div>
<div class="m2"><p>زانکه عقرب هبوط ماه بُوَد</p></div></div>