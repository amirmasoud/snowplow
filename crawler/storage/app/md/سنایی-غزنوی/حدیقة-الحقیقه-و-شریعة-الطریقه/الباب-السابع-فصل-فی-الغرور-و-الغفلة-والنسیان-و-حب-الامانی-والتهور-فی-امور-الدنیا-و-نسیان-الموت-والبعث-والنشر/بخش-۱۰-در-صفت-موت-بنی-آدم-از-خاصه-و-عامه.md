---
title: >-
    بخش ۱۰ - در صفت موت بنی‌آدم از خاصّه و عامه
---
# بخش ۱۰ - در صفت موت بنی‌آدم از خاصّه و عامه

<div class="b" id="bn1"><div class="m1"><p>زان بنی‌آدم از صغار و کبار</p></div>
<div class="m2"><p>که برآورده شد ز جمله دمار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زان به جان اندرون خلیدن نیش</p></div>
<div class="m2"><p>بچه را در کنار مادر خویش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زان بریدن به منزل و به سفر</p></div>
<div class="m2"><p>حلق برنای تازه پیش پدر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زان ربودن فکندن اندر نار</p></div>
<div class="m2"><p>مرد را از دکان و از بازار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زان خصال سران سمر کردن</p></div>
<div class="m2"><p>زان کلاه کیان کمر کردن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زان همه ملک با خلل کردن</p></div>
<div class="m2"><p>زان همه خطبه‌ها بدل کردن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زان بناگاه بردن از سرِ تخت</p></div>
<div class="m2"><p>پای بسته کشان دو صد بدبخت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا چو بشنودی از غرور مهی</p></div>
<div class="m2"><p>دل براین عمر بی‌وفا ننهی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>این همه قصّه‌ها از او بشنو</p></div>
<div class="m2"><p>نازنینی مکن بدو بگرو</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زین قفاهای نرم و شیرین کار</p></div>
<div class="m2"><p>گردن اندر مدزد مسخره‌وار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تو ز روی هوا و پر هوسی</p></div>
<div class="m2"><p>وز پی فعل ناکسی و خسی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>آنچنان با غرور گشتی جفت</p></div>
<div class="m2"><p>پیش تو مرگ خود که یارد گفت</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چه حدیثست شاه کی میرد</p></div>
<div class="m2"><p>کی اجل حلق پادشا گیرد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>کی بود خاصه از درون حصار</p></div>
<div class="m2"><p>با امیر اجل اجل را کار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>از توام خوشتر آنکه پیش اجل</p></div>
<div class="m2"><p>از برای نفاق و زرق و دغل</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>پیش بیمار همنفس با مرگ</p></div>
<div class="m2"><p>گشته ریزان ز شاخ عمرش برگ</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>او کشیده ز هفت عضوش جان</p></div>
<div class="m2"><p>تو همی گویی هفت کُه به میان</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>کرده ابلیس بهر طنّازی</p></div>
<div class="m2"><p>زین سخن بر بروت تو بازی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>در میان از هزار کُه باشد</p></div>
<div class="m2"><p>مرگ یک دم چو خاک بر پاشد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>زین ترش بودنت در این زندان</p></div>
<div class="m2"><p>مرگ را کُند کی شود دندان</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>مِه ز تو کِه ز تو به پیش تو مُرد</p></div>
<div class="m2"><p>تو بزی خوش ترا که یارد بُرد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>مردگان را به گِل سپردی تو</p></div>
<div class="m2"><p>تو نمیری نه مرد خردی تو</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>خود ترا مرگ بسته کی گیرد</p></div>
<div class="m2"><p>تو امیری امیر کی میرد</p></div></div>