---
title: >-
    بخش ۳۳ - التمثّل فی‌الانسان و عمله
---
# بخش ۳۳ - التمثّل فی‌الانسان و عمله

<div class="b" id="bn1"><div class="m1"><p>آن نبینی که پادشه‌زاده</p></div>
<div class="m2"><p>که ورا ملکتست آماده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>باشد اندر سرای و حجرهٔ خاص</p></div>
<div class="m2"><p>بر سرش خادمان با اخلاص</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا به بازی فراش نگذارند</p></div>
<div class="m2"><p>سال و مه پاس او همی دارند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آن وشاقان پر فغان و فضول</p></div>
<div class="m2"><p>شده بر لهو یکدگر مشغول</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در سرایی که بارگه باشد</p></div>
<div class="m2"><p>زحمت و انبُه سپه باشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همه را بر فلک رسیده خروش</p></div>
<div class="m2"><p>بارگاه از فغانشان پر جوش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>وآن ملک‌زاده ساعتی بی‌کار</p></div>
<div class="m2"><p>نبود بی‌رقیب و بی‌کردار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا نپوید به راه ناواجب</p></div>
<div class="m2"><p>نبود بی‌اتابک و حاجب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نه به بازی و لهو پردازد</p></div>
<div class="m2"><p>نه نپرسیده گفتن آغازد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آن چنانش نگاه می‌دارد</p></div>
<div class="m2"><p>که یکی دم به هرزه برنارد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سرّ این چیست خود تو می‌دانی</p></div>
<div class="m2"><p>زانکه مقصود کار دو جهانی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مر ترا تخت ملک منتظرست</p></div>
<div class="m2"><p>از عبث جمله بخت بر حذرست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>تو کز از نسل آدمی به نسب</p></div>
<div class="m2"><p>پاک‌دار از عبث همیشه حسب</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>کار کن رنج کش بسان پدر</p></div>
<div class="m2"><p>بازگردد ترا گهر به گهر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ورنه از آدمی ز شیطانی</p></div>
<div class="m2"><p>هرچه خواهی بکن تو به دانی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ای دریغا که قیمت تن خویش</p></div>
<div class="m2"><p>می‌ندانی سخن نگویم پیش</p></div></div>