---
title: >-
    بخش ۳۷ - التمثیل فی اعتقادالسوء والخوف من قلّة الرزق
---
# بخش ۳۷ - التمثیل فی اعتقادالسوء والخوف من قلّة الرزق

<div class="b" id="bn1"><div class="m1"><p>بود مردی مُعیل بس رنجور</p></div>
<div class="m2"><p>شده از عمر و عیش خویش نفور</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مرد را ده عیال و کسب قلیل</p></div>
<div class="m2"><p>گشت بیچاره زار مرد معیل</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از عیال و طفول رخ برتافت</p></div>
<div class="m2"><p>به دگر ناحیت سبک بشتافت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>وآن عیالان به شهر دربگذاشت</p></div>
<div class="m2"><p>راحت خویشتن در آن پنداشت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به سر چاهساری آمد مرد</p></div>
<div class="m2"><p>بخت بنگر که با معیل چه کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دید مردی نشسته بر سر چاه</p></div>
<div class="m2"><p>دلو با حبل برنهاده به راه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مرغکی بس ضعیف و بس کوچک</p></div>
<div class="m2"><p>که ز گنجشک بود او ده یک</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گفت مردا سبک بکن کاری</p></div>
<div class="m2"><p>تا برآید مگرت بازاری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از من ای خواجه صد درم بستان</p></div>
<div class="m2"><p>مرغ را زآب تشنگی بنشان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دلو و حبل اینک و چهی پر ز آب</p></div>
<div class="m2"><p>آب ده مرغ را سبک بشتاب</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مرد گفتا که بخت روی نمود</p></div>
<div class="m2"><p>به از این کار خود نخواهد بود</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به یکی دلو سیر گردد مرغ</p></div>
<div class="m2"><p>صد درم مر مرا شود آمرغ</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>دلو بگرفت و رفت زی سرِ چاه</p></div>
<div class="m2"><p>خود ز سرِّ فلک نبود آگاه</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>تا به گاه زوال آب کشید</p></div>
<div class="m2"><p>مرغ سیری از آب هیچ ندید</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>خسته شد مرد و گفت چتوان بود</p></div>
<div class="m2"><p>که تن من در این عنا فرسود</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>مر ورا گفت مرد کای نادان</p></div>
<div class="m2"><p>امتحان توام من از یزدان</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>تو مر این مرغ را ز چاه پر آب</p></div>
<div class="m2"><p>نتوانی ز آب داد اسباب</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ده عیال ضعیف چون داری</p></div>
<div class="m2"><p>طفل را خیر خیر بگذاری</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>رازقم من تو در میان سببی</p></div>
<div class="m2"><p>پس چرا با فغان و با شغبی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>رو سوی خانه باز شو بشتاب</p></div>
<div class="m2"><p>کار اطفال خویش را دریاب</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>من که روزی دهم توانایم</p></div>
<div class="m2"><p>راه ارزق بر تو بگشایم</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>جان بدادم همی دهم روزی</p></div>
<div class="m2"><p>در غم نان چرا تو دل سوزی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>جان بدادم دهمت نان هر دم</p></div>
<div class="m2"><p>جان مدار از برای نان در غم</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>زین هوسها چرا نگردی دور</p></div>
<div class="m2"><p>چند دارد جهان ترا مغرور</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>آن جهان در غرور نتوان یافت</p></div>
<div class="m2"><p>نرسید آنکه سالها بشتافت</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>حج مپندار گفت لبّیکی</p></div>
<div class="m2"><p>جامه مفکن بر آتش از کیکی</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>نه برِ اوستاد دین‌پرور</p></div>
<div class="m2"><p>نه به بازار و نه برِ مادر</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>پیش من قصّهٔ هنر برخوان</p></div>
<div class="m2"><p>به کدامی تره‌ات نهم بر خوان</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>نه بدینجات زر نه آنجا زور</p></div>
<div class="m2"><p>نز پی گلشنی نه از پی گور</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>با حریف دغا مباز ای کور</p></div>
<div class="m2"><p>نبری زو نه از مجاهز عور</p></div></div>