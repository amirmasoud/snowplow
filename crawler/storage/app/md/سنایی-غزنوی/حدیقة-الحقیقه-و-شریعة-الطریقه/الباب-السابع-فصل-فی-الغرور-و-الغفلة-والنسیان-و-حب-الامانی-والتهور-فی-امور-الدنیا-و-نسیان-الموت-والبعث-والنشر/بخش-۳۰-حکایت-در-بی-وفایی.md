---
title: >-
    بخش ۳۰ - حکایت در بی‌وفایی
---
# بخش ۳۰ - حکایت در بی‌وفایی

<div class="b" id="bn1"><div class="m1"><p>قصه‌ای یاد دارم از پدران</p></div>
<div class="m2"><p>زان جهان‌دیدگان پرهنران</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>داشت زالی به روستای تگاو</p></div>
<div class="m2"><p>مهستی نام دختری و سه گاو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نوعروسی چو سرو تر بالان</p></div>
<div class="m2"><p>گشت روزی ز چشم بد نالان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گشت پدرش چو ماه نو باریک</p></div>
<div class="m2"><p>شد جهان پیش پیر زن تاریک</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دلش آتش گرفت و سوخت جگر</p></div>
<div class="m2"><p>که نیازی جز او نداشت دگر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زال گفتی همیشه با دختر</p></div>
<div class="m2"><p>پیش تو باد مردنِ مادر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از قضا گاو زالک از پی خورد</p></div>
<div class="m2"><p>پوز روزی به دیگش اندر کرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ماند چون پای مقعد اندر ریگ</p></div>
<div class="m2"><p>آن سر مرده ریگش اندر دیگ</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گاو مانند دیوی از دوزخ</p></div>
<div class="m2"><p>سوی آن زال تاخت از مطبخ</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زال پنداشت هست عزرائیل</p></div>
<div class="m2"><p>بانگ برداشت از پی تهویل</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کای مقلموت من نه مهستیم</p></div>
<div class="m2"><p>من یکی زال پیر محنتیم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تندرستم من و نیم بیمار</p></div>
<div class="m2"><p>از خدا را مرا بدو مشمار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گر ترا مهستی همی باید</p></div>
<div class="m2"><p>آنک او را ببر مرا شاید</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>دخترم اوست من نه بیمارم</p></div>
<div class="m2"><p>تو او منت رخت بردارم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>من برفتم تو دانی و دختر</p></div>
<div class="m2"><p>سوی او رو ز کار من بگذر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>تا بدانی که وقت پیچاپیچ</p></div>
<div class="m2"><p>هیچ‌کس مر ترا نباشد هیچ</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بی‌بلا نازنین شمرد او را</p></div>
<div class="m2"><p>چون بلا دید در سپرد او را</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>به جمال نکو ازو بُد شاد</p></div>
<div class="m2"><p>به خیال بدش ز دست بداد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>یار نبود که بر درِ زندان</p></div>
<div class="m2"><p>چشم گریان و لب بود خندان</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>یارت آن باشد ار نیاری خشم</p></div>
<div class="m2"><p>که ز سر بفکند برای تو چشم</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>گیرد ار پرسیش پسندیده</p></div>
<div class="m2"><p>گفته ناگفته دیده نادیده</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>هرکه وقت بلا ز تو بگریخت</p></div>
<div class="m2"><p>به حقیقت بدانکه رنگ آمیخت</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>صحبتش را مجو مرو بَرِ او</p></div>
<div class="m2"><p>رَو ز روزن بجه نه از درِ او</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>من وفایی ندیده‌ام ز خسان</p></div>
<div class="m2"><p>گر تو دیدی سلام من برسان</p></div></div>