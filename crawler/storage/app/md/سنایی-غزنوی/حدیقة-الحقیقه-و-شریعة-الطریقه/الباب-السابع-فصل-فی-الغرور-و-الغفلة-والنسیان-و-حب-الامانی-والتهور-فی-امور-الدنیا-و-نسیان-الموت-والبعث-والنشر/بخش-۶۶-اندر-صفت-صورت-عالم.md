---
title: >-
    بخش ۶۶ - اندر صفت صورت عالم
---
# بخش ۶۶ - اندر صفت صورت عالم

<div class="b" id="bn1"><div class="m1"><p>تو به گوهر ورای دو جهانی</p></div>
<div class="m2"><p>چکنم قدرِ خود نمی‌دانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با زنان نیک هم نبردی تو</p></div>
<div class="m2"><p>با چنین زور مردِ مردی تو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چه کمست ای بزرگ زاده ترا</p></div>
<div class="m2"><p>در گشاده‌ست و خوان نهاده ترا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر تو خود را در این سرای غرور</p></div>
<div class="m2"><p>از سرِ جهل و بخل داری دور</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پنج نوبت زنی چو عقل و چو جان</p></div>
<div class="m2"><p>بر سرِ هفت چرخ و چار ارکان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ور قبای فنا بیندازی</p></div>
<div class="m2"><p>به قبیلهٔ بقای حق تازی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر سه جانت بکن به شبگیری</p></div>
<div class="m2"><p>دو سلام و چهار تکبیری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آخشیجان و گنبد دوّار</p></div>
<div class="m2"><p>مردگانند زندگانی‌خوار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چه کنی در جهان بیم آرش</p></div>
<div class="m2"><p>زانکه بی‌پرسش است بیمارش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>برگذر کین سرای پر وحلست</p></div>
<div class="m2"><p>نردبان پایه عمر و بام اجلست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هرکه بر متن این سرای رسید</p></div>
<div class="m2"><p>باز دستش به دیدگان بکشید</p></div></div>