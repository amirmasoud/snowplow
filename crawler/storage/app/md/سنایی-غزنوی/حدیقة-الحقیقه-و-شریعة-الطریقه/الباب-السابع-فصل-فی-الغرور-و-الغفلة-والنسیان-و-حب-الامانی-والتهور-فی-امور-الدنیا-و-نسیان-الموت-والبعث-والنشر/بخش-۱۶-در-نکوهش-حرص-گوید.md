---
title: >-
    بخش ۱۶ - در نکوهش حرص گوید
---
# بخش ۱۶ - در نکوهش حرص گوید

<div class="b" id="bn1"><div class="m1"><p>حرص بگذار و ز آز دست بدار</p></div>
<div class="m2"><p>حرص و آزست مایهٔ تیمار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حرص را ز آنکه قهر خواند اله</p></div>
<div class="m2"><p>عاقل از وی بدان نساخت پناه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هرکه او حرص را امام کند</p></div>
<div class="m2"><p>خواب و خور جملگی حرام کند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نقش رنگین و هیچ جان نه درو</p></div>
<div class="m2"><p>خوان زرین و هیچ نان نه برو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حرص نقشیست هیچش اندر زیر</p></div>
<div class="m2"><p>نکند هیچ هیچ کس را سیر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هرکه را دیو حرص مهمان بُرد</p></div>
<div class="m2"><p>تو حقیقت شنو که گرسنه مرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آز پر باد چون درو پیچی</p></div>
<div class="m2"><p>کدخداییست خانه پر هیچی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هرکه او آز را متابع گشت</p></div>
<div class="m2"><p>بگذشت از ثلاث و رابع گشت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به غروری ببرده خواب همه</p></div>
<div class="m2"><p>نان نداده ببرده آب همه</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خلق ازین گردخوان دیرینه</p></div>
<div class="m2"><p>دیده سیلی و هیچ سیری نه</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تا قیامت نخورده مهمانش</p></div>
<div class="m2"><p>یک شکم نان سیر بر خوانش</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>این دو در دوزخ از درون تو باز</p></div>
<div class="m2"><p>صورتش سوی عقل شهوت و آز</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>زین دو گر در فنا نپرهیزی</p></div>
<div class="m2"><p>در بقا از درونشان خیزی</p></div></div>