---
title: >-
    بخش ۵۶ - اندر صفت بیابان گوید
---
# بخش ۵۶ - اندر صفت بیابان گوید

<div class="b" id="bn1"><div class="m1"><p>تنگی راه را صفت بشنو</p></div>
<div class="m2"><p>در رهی نازموده خیره مرو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ره چو سوفار و خار چون پیکان</p></div>
<div class="m2"><p>مار رنگین درو چو توز کمان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تیز و گریان کنندت از گرما</p></div>
<div class="m2"><p>امّ‌غیلان او چو ابن‌ذکا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خاره در تفّ او چو خاره سبک</p></div>
<div class="m2"><p>شوره بر سنگ او چو شاره تُنُک</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مرده خاکش ز هجر بی‌آبی</p></div>
<div class="m2"><p>کفنش کرده شوره سیمابی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مهمهش با مهابت ارقم</p></div>
<div class="m2"><p>چون دم ابیض و دل بلعم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شده از تفّ شورهٔ بدرنگ</p></div>
<div class="m2"><p>همچو سیماب ریزه در وی سنگ</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سایه یک دم درو نیاسوده</p></div>
<div class="m2"><p>غول و خضرش سراب پیموده</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نابسوده برِ هلاکش را</p></div>
<div class="m2"><p>ادهم روزگار خاکش را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>پیش چشم و خیال پر کینه</p></div>
<div class="m2"><p>خاک سرمه سراب آیینه</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ابر بهمن درو سموم شده</p></div>
<div class="m2"><p>مار بر خاک او چو موم شده</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بوده هامون او چو هاویه راست</p></div>
<div class="m2"><p>خاک همچون دل معاویه راست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>که نرفتی ز سهم آن هامون</p></div>
<div class="m2"><p>خضر بی‌آب و بی‌دلیل برون</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>خضر بی‌رهبر اندران صحرا</p></div>
<div class="m2"><p>نتوانست رفت برعمیا</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>زانکه از روی حقد و پر کینی</p></div>
<div class="m2"><p>راه چون پشت آینهٔ چینی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>قمر آنجا طریق گم کرده</p></div>
<div class="m2"><p>شمس در وی شعاع بسپرده</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>جزع در چشمهاش خوان آرای</p></div>
<div class="m2"><p>غول بر گوشها فقاع گشای</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>از پی قوت و قوّت مردم</p></div>
<div class="m2"><p>گندمش پر ز نیش چون گزدم</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>نرگس اندر خیال بود چنین</p></div>
<div class="m2"><p>آفتابی میانهٔ پروین</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چشمهٔ آفتاب ابر آلود</p></div>
<div class="m2"><p>تشت شمعی میان تودهٔ دود</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>گرچه از بهر مهر دل داری</p></div>
<div class="m2"><p>شش درم ساخت کرد دیناری</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>قلزم قیر و قار تا ابراج</p></div>
<div class="m2"><p>برفشانده تلاطم امواج</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>صحن بی‌امن او چو خانهٔ بیم</p></div>
<div class="m2"><p>مانده بی‌آب همچو روی یتیم</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>باد سردش ز دل بریده امید</p></div>
<div class="m2"><p>ریگ گرمش به مرگ داده نوید</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>تا سمومش صمام گوش آمد</p></div>
<div class="m2"><p>دست او پای‌بند هوش امد</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>گزدم از خار او کند مسواک</p></div>
<div class="m2"><p>مار افعی درو نیابد خاک</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>خاک او روی آب نادیده</p></div>
<div class="m2"><p>گل او پشت مردم دیده</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>نان ندید آنکه ز آب او شد شاد</p></div>
<div class="m2"><p>جان نبرد آنکه دل برو بنهاد</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>تب زردست رشتهٔ چَه اوی</p></div>
<div class="m2"><p>مرگ سرخست رفتن ره اوی</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>زین بیابان بسی ترا بهتر</p></div>
<div class="m2"><p>خانه و آب سرد و دیگ کبر</p></div></div>