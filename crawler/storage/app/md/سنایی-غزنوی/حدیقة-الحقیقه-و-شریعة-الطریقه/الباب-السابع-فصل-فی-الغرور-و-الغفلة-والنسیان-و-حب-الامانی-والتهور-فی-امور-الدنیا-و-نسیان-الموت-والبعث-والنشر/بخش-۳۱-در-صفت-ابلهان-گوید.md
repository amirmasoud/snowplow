---
title: >-
    بخش ۳۱ - در صفت ابلهان گوید
---
# بخش ۳۱ - در صفت ابلهان گوید

<div class="b" id="bn1"><div class="m1"><p>صحبت ابلهان چو دیگ تهیست</p></div>
<div class="m2"><p>از درون خالی از برون سیهیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دوستی ابلهان ز تقلید است</p></div>
<div class="m2"><p>نز ره عقل و دین و توحیدست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ببُر از دوستی خلق سبک</p></div>
<div class="m2"><p>دوستی خلق سنگ و شیشه تُنُک</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سنگ در ظرف شیشه نتوان برد</p></div>
<div class="m2"><p>نبود دوست با عرابی کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چنگ و نایست در صفت نادان</p></div>
<div class="m2"><p>تنگدل باشد و فراخ دهان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زانکه ابله چو باشدت دلجوی</p></div>
<div class="m2"><p>آب تهمت دواند اندر جوی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا بوی تندرست و حکم روان</p></div>
<div class="m2"><p>داردت خویش‌و دوست چون تن‌و جان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون شود مویی از تو دیگرگون</p></div>
<div class="m2"><p>آن شود موسی این دگر قارون</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سوز بی‌نور بینی از خویشان</p></div>
<div class="m2"><p>راست همچون چراغ درویشان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>یار دانا چو شد ترا همراه</p></div>
<div class="m2"><p>بس درازی راه شد کوتاه</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چون کم آید به راه توشهٔ تو</p></div>
<div class="m2"><p>ننگرد در کلاه گوشهٔ تو</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نه برادر بود به نرم و درشت</p></div>
<div class="m2"><p>که برای شکم بود هم پشت</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>دلِ تو با خدای و خلق ای خر</p></div>
<div class="m2"><p>چون جوست ای ز نیم جو کمتر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>که یکی دانه بهر زر باشد</p></div>
<div class="m2"><p>بار یک خانه بهر خر باشد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>از خریّ خران تبرّا کن</p></div>
<div class="m2"><p>دل خود با خدای یکتا کن</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>تا دلت معدن نیاز کند</p></div>
<div class="m2"><p>درِ دل پیش جانت باز کند</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>نه همی گویدت فلک ز فراز</p></div>
<div class="m2"><p>کز خرد نردبان کن و بر تاز</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>لیک می‌نشنوی که کر شده‌ای</p></div>
<div class="m2"><p>عقل بگذاشتی چو خر شده‌ای</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>گر ترا گوش عقل بودی باز</p></div>
<div class="m2"><p>بشنیدی چو عاقلان آواز</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>در تو زیرا سخن مؤثر نیست</p></div>
<div class="m2"><p>که ترا زن جهان مبشّر نیست</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>در جهان خدا بر آی از خاک</p></div>
<div class="m2"><p>چکنی کلبه‌ای که آن کاواک</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چون کتابی است صورت عالم</p></div>
<div class="m2"><p>کاندرویست بند و پند بهم</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>صورتش بر تن لئیمان بند</p></div>
<div class="m2"><p>صفتش در دل حکیمان پند</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>صورتش خامش و سخن در وی</p></div>
<div class="m2"><p>تن او نو و جان کهن در وی</p></div></div>