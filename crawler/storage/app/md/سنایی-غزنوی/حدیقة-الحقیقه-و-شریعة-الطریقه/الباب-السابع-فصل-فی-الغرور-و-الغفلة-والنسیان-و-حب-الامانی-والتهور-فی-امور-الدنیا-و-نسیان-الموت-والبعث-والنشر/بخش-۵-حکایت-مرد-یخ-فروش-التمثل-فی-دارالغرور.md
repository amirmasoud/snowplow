---
title: >-
    بخش ۵ - حکایت مرد یخ فروش التمثّل فی دارالغرور
---
# بخش ۵ - حکایت مرد یخ فروش التمثّل فی دارالغرور

<div class="b" id="bn1"><div class="m1"><p>مثلت هست در سرای غرور</p></div>
<div class="m2"><p>مثل یخ فروش نیشابور</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در تموز آن یخک نهاده به پیش</p></div>
<div class="m2"><p>کس خریدار نی و او درویش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هرچه زر داشت او به یخ درباخت</p></div>
<div class="m2"><p>آفتاب تموز یخ بگداخت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یخ‌گدازان شده ز گرمی و مرد</p></div>
<div class="m2"><p>با دلی دردناک و با دمِ سرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زانکه عمر گذشته باقی داشت</p></div>
<div class="m2"><p>آفتاب تموزش نگذاشت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>این همی گفت و اشک می‌بارید</p></div>
<div class="m2"><p>که بسی مان نماند و کس نخرید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>قیمت روزگار آسانی</p></div>
<div class="m2"><p>به سرِ روزگار اگر دانی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چیست عقل اوّل این جهان دیدن</p></div>
<div class="m2"><p>پس به حسبت برین جهان ریدن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>برگ دنیا خرد نبپسندد</p></div>
<div class="m2"><p>مرگ بر برگ این جهان خندد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چون نترسی تو از اجل خُردی</p></div>
<div class="m2"><p>آن ز غفلت شمر نه از مردی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تو نه‌ای بر اجل دلیر هنوز</p></div>
<div class="m2"><p>گور گور است و شیر شیر هنوز</p></div></div>