---
title: >-
    بخش ۱۳ - اندر طلب بهشت به سالوسی
---
# بخش ۱۳ - اندر طلب بهشت به سالوسی

<div class="b" id="bn1"><div class="m1"><p>مرغ و حور از بهشت ابدانست</p></div>
<div class="m2"><p>حکمت و دین بهشت یزدانست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نبود جز جمال ایزد قوت</p></div>
<div class="m2"><p>عاشقان را به جنت ملکوت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تو چه دانی که می چه گیری قوت</p></div>
<div class="m2"><p>در چنین دل کجا رسد ملکوت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ملکوت از پی گدایی را</p></div>
<div class="m2"><p>جان دهد از پی رضایی را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آنکه در بند حور و غلمانست</p></div>
<div class="m2"><p>نیست خواجه که از غلامانست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آنکه در صفّ بارگاه ازل</p></div>
<div class="m2"><p>می‌سراید چو عندلیب غزل</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون گرفت از صفای صفوت قوت</p></div>
<div class="m2"><p>ملک را باز داند از ملکوت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون نداری مناهی اندر پیش</p></div>
<div class="m2"><p>ز احتساب خرد بجومندیش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تو چه دانی بهشت یزدان چیست</p></div>
<div class="m2"><p>چه شناسی که جنّت جان چیست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کی برد شهوتت ترا به بهشت</p></div>
<div class="m2"><p>تات حور و قصور باید و کشت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>همچو بربط ز فسق و سیرت زشت</p></div>
<div class="m2"><p>چشمتان هشت بهر هشت بهشت</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ای به دل کرده دین به نامردی</p></div>
<div class="m2"><p>چند ازین نان و چند ازین خوردی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>دلی آخر به دست کن روزی</p></div>
<div class="m2"><p>که درو باشدت ز دین سوزی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گیرم این جا ز دیوی و زوشی</p></div>
<div class="m2"><p>عیب خود بر همه همی پوشی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چون رسی در جهان بی‌چونی</p></div>
<div class="m2"><p>عیب گوید من اینکم چونی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>تو همی پوش همچو عامهٔ خلق</p></div>
<div class="m2"><p>عیب خود بهر بارنامهٔ خلق</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>پس بدان تا هوا شود خشنود</p></div>
<div class="m2"><p>عذر می نه که عقلم این فرمود</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گرچه بر خود بپوشی از پی فرع</p></div>
<div class="m2"><p>از درون شرم‌دار شرم از شرع</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>این همه طمطراق بیهودست</p></div>
<div class="m2"><p>عقل جز راستی نفرمودست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>وآن کسانی که مرد این راهند</p></div>
<div class="m2"><p>از نهادِ زمانه آگاهند</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ستم دوست را چو از درِ اوست</p></div>
<div class="m2"><p>دوست دارند که دوست دارد دوست</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>خشم را از درون محمّد وار</p></div>
<div class="m2"><p>جز برای شکار شرع مدار</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>حرص را سر بزن به تیغ وفا</p></div>
<div class="m2"><p>بخل را پی کن از صفای رضا</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>وآن خرانی که بارِ گل بکشند</p></div>
<div class="m2"><p>شربت صرف کار دل بچشند</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>همه را بینی اندرین بنیاد</p></div>
<div class="m2"><p>ز آتش دل دماغها پر باد</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>چون براین در نه‌ای سپهداری</p></div>
<div class="m2"><p>کم ز سگ‌بانئی مکن باری</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>گر نمیرد چنین سگی در تو</p></div>
<div class="m2"><p>از سگی کم بوی به محشر تو</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>از صفات سگی تهی کن رگ</p></div>
<div class="m2"><p>ورنه در رستخیز خیزی سگ</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>کمتر از سگ مباش و حق بشناس</p></div>
<div class="m2"><p>که به یک لقمه دارد از تو سپاس</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>خشم را دل مده به جاه ویسار</p></div>
<div class="m2"><p>سگ دیوانه بر درد هشیار</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>برِ عاقل که یافت عقل و بصر</p></div>
<div class="m2"><p>فربهی دیگر و ورم دیگر</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>نبود چون بصیر مرد ضریر</p></div>
<div class="m2"><p>نیست حاجت مرا بدین تقریر</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>گرچه آبستنی ز دور زمن</p></div>
<div class="m2"><p>او هم از مرگ تست آبستن</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>جسم فربه مکن به لقمهٔ خوش</p></div>
<div class="m2"><p>کاسب فربه چو شد شود سرکش</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>روده کز باد گشت فربه و تر</p></div>
<div class="m2"><p>بدو سوزن شود سبک لاغر</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>ابلهان مانده‌اند بر سر پل</p></div>
<div class="m2"><p>پای در گِل دو دست اندر غُل</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>همه در آب این دو روزه نهاد</p></div>
<div class="m2"><p>تازه و تر چو رودهٔ پر باد</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>تو در این خطهٔ فساد و فجور</p></div>
<div class="m2"><p>از دل شاد مانده‌ای رنجور</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>گر تو هستی ز نسبت آدم</p></div>
<div class="m2"><p>هم ز خود زای با کمر چو قلم</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>اصل را هم به اصل باز رسان</p></div>
<div class="m2"><p>خوش‌به‌خوش بخش‌وناخوشی به‌خسان</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>عقل و علمست آفت منحوس</p></div>
<div class="m2"><p>پر و بالست فتنهٔ طاوس</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>هرچه گویی نه در ره آدم</p></div>
<div class="m2"><p>دیو و دد دیده گیرد اندر دم</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>کبک سنّت به بوستان نیاز</p></div>
<div class="m2"><p>کی درآید چو در خرامد باز</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>گو سبک روح نیست دختر دین</p></div>
<div class="m2"><p>هست اندر جهان گران کابین</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>نشود دل تهی ز پر گویی</p></div>
<div class="m2"><p>پس تو خون را به خون چرا شویی</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>زان ترا گوشمال داد فلک</p></div>
<div class="m2"><p>زیر چرخ کیان فراز سمک</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>تا نگویی جواب بوالحکمان</p></div>
<div class="m2"><p>ور بگویی چو کوه گوی همان</p></div></div>