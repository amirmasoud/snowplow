---
title: >-
    بخش ۵۵ - اندر موعظت و نصیحت گوید
---
# بخش ۵۵ - اندر موعظت و نصیحت گوید

<div class="b" id="bn1"><div class="m1"><p>صحبت زیرکان چو بوی از گُل</p></div>
<div class="m2"><p>عظت ناصحان چو طعم از مُل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بی‌غرض پند همچو قند بُوَد</p></div>
<div class="m2"><p>با غرض پند پای بند بُوَد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در مشام خرد چه زشت آید</p></div>
<div class="m2"><p>هر نسیمی که نز بهشت آید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بهر اندام دادن اوباش</p></div>
<div class="m2"><p>دل چو سندان زبان چو سوهان باش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بشناسی ز راه دیدهٔ روح</p></div>
<div class="m2"><p>فاتحهٔ دین چو روی داد فتوح</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>وسعت آنجا که راه یزدانست</p></div>
<div class="m2"><p>تنگی اینجا که بندِ انسانست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>انّ فی دیننا بخوان و بمان</p></div>
<div class="m2"><p>فی کبد را برآن و تیز بران</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>راه یزدان رهِ فراخ آمد</p></div>
<div class="m2"><p>گلشن و بوستان و کاخ آمد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هر مشامی کزین نسیم بهشت</p></div>
<div class="m2"><p>نتواند شنید باشد زشت</p></div></div>