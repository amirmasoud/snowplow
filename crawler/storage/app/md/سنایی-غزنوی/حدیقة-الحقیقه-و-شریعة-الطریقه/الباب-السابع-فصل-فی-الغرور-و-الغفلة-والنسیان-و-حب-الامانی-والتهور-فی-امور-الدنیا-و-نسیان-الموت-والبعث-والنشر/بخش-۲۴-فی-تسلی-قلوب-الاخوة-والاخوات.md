---
title: >-
    بخش ۲۴ - فی تسلّی قلوب الاخوة والاخوات
---
# بخش ۲۴ - فی تسلّی قلوب الاخوة والاخوات

<div class="b" id="bn1"><div class="m1"><p>شوی خود را زنی بدید دژم</p></div>
<div class="m2"><p>تنگ دل شد به شوی گفت این غم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر برای منست بادی شاد</p></div>
<div class="m2"><p>ور برای دل است پیشت باد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از پی نان مریز آب از روی</p></div>
<div class="m2"><p>بوحبیشی ز بوغیاث مجوی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آبروی از برای نان برود</p></div>
<div class="m2"><p>طمع نان بود که جان برود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون نه نیکی نه قابل نیکی</p></div>
<div class="m2"><p>تو و کاکا و کوکو و کی کی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زهد عیسی و حرص قارون بین</p></div>
<div class="m2"><p>گفته در شأن آن و در حق این</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>و رفعنا به نردبان نیاز</p></div>
<div class="m2"><p>فخسفنا ز سر نشیبی آز</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آن به زهد آسمان گرفته به ناز</p></div>
<div class="m2"><p>وین شده خاک خورده از پی آز</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>عقل و جان گفته از پی زر و سیم</p></div>
<div class="m2"><p>انّ ربّی بکیدهّن علیم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آفت آدمی ز دنیی دان</p></div>
<div class="m2"><p>راحت جان و تن ز عقبی دان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مرد خرسند میر کوی بود</p></div>
<div class="m2"><p>که طمع زنگ آب روی بود</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>در نگر بی‌مزاج و خاطر دون</p></div>
<div class="m2"><p>زین دو معنی به عیسی و قارون</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>قصّهٔ یوسف ار ندانی تو</p></div>
<div class="m2"><p>چون ز قرآن همی نخوانی تو</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چون ز زن بود آفت و المش</p></div>
<div class="m2"><p>راند قرآن به کام او قلمش</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>مرد دنیی کرامتی نبود</p></div>
<div class="m2"><p>قیمتی جز قیامتی نبود</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گر ترا خشم و آز بگذارد</p></div>
<div class="m2"><p>بر زمین موری از تو نازارد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ار چنانی مبارکت باد آن</p></div>
<div class="m2"><p>ورنه این کن وزو جهان بستان</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ورنه از حرص گندمی پی خورد</p></div>
<div class="m2"><p>گرد خود همچو آسیا می‌گرد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>حرص را بر نه از قناعت بند</p></div>
<div class="m2"><p>وانگه از دور او گری و تو خند</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>باب نسیان تمام گشت سخن</p></div>
<div class="m2"><p>سخن آرم ز دوست و ز دشمن</p></div></div>