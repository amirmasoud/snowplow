---
title: >-
    بخش ۳۵ - التمثیل فی شکر هدایة‌الاسلام
---
# بخش ۳۵ - التمثیل فی شکر هدایة‌الاسلام

<div class="b" id="bn1"><div class="m1"><p>بود عمّر نشسته روزی فرد</p></div>
<div class="m2"><p>گردش اصحاب صفّه با غم و درد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هریک از شادی ره اسلام</p></div>
<div class="m2"><p>یاد می‌کرد بر گشاده کلام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هم کهن پیر و هم جوان تازه</p></div>
<div class="m2"><p>برده آوازه تا به دروازه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>منّتی جمله یاد می‌کردند</p></div>
<div class="m2"><p>فوت ایام کفر می‌خوردند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بود عبداللّٰه عُمر حاضر</p></div>
<div class="m2"><p>لیک زان درد و رنج بُد قاصر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>منتی کرد نیز بر خود یاد</p></div>
<div class="m2"><p>زود عمّر برو زبان بگشاد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گفت ویحک چه لاف پاشی تو</p></div>
<div class="m2"><p>خود مرین درد را چه باشی تو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>درد دین تو تا کجا باشد</p></div>
<div class="m2"><p>مر ترا درد کی روا باشد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تو در اسلام زاده و دیده</p></div>
<div class="m2"><p>تلخی کفر هیچ نچشیده</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>درد ایام کفر خورده نه‌ای</p></div>
<div class="m2"><p>خویشتن را ذلیل کرده نه‌ای</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>این چنین درد و زخم ما دانیم</p></div>
<div class="m2"><p>زان به دین رسول شادانیم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ناچشیده تو درد و منّت و عار</p></div>
<div class="m2"><p>هیچ نابرده ذُل و استحقار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نشناسی تو لذّت ایمان</p></div>
<div class="m2"><p>قدر ایمان چه دانی و احسان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ما شناسیم کان چه ذُلّی بود</p></div>
<div class="m2"><p>وان چه بندی و آن چه غُلّی بود</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>شکر اسلام کرد مادانیم</p></div>
<div class="m2"><p>کین زمان مرد راه ایمانیم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>شیر مردان عناء ره بردند</p></div>
<div class="m2"><p>به تو نامرد راه بسپردند</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>تو به نامردی این ره دین را</p></div>
<div class="m2"><p>جمله کردی خراب آیین را</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>به چه بنهم ترا به یار جواب</p></div>
<div class="m2"><p>ای ز تو دین و شرع گشته خراب</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>نه زنی در ره صواب و نه مرد</p></div>
<div class="m2"><p>نه مختّث از آنت نبود درد</p></div></div>