---
title: >-
    بخش ۴۴ - در کاهلی گوید
---
# بخش ۴۴ - در کاهلی گوید

<div class="b" id="bn1"><div class="m1"><p>بشنو از بارگاه مصطفوی</p></div>
<div class="m2"><p>تا چه دانی ز نکتهٔ نبوی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صفت کاهلان دین در راه</p></div>
<div class="m2"><p>هست لفظ من استوی یوماه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اسب کودن به غز و نیست دوان</p></div>
<div class="m2"><p>ورنه چون خر نداردی پالان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر تن خود نه‌ای مغفّل بار</p></div>
<div class="m2"><p>زانکه باشد سیاه بدکردار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شرع ورزی نیاید از منبل</p></div>
<div class="m2"><p>حق گزاری نیاید از کاهل</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آنکه او شرع را شود منقاد</p></div>
<div class="m2"><p>نرود چون خران به راه عناد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بندهٔ شرع باش تا برهی</p></div>
<div class="m2"><p>ورنه گشتی به پیش دیو رهی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مر ترا گر به سوی خانه برد</p></div>
<div class="m2"><p>اشهب و ادهم زمانه برد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خام و گم‌نام رفته از خانه</p></div>
<div class="m2"><p>که بود جز جنین و افگانه</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گام زن همچو روز روشن باش</p></div>
<div class="m2"><p>نه فسرده چو بام و روزن باش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آب در گشتن است خوش چو گلاب</p></div>
<div class="m2"><p>چو نگردد بگندد از تف و تاب</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دم به دم طوف کن به هر کویی</p></div>
<div class="m2"><p>تا ببینی مگر نکورویی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ور نکو گویی و نکو رایی</p></div>
<div class="m2"><p>همچو اقبال باش هرجایی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>با همه خلق رای نیکو دار</p></div>
<div class="m2"><p>خو نکودار و رای چون خو دار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>تنگ خویی نشان ادبیرست</p></div>
<div class="m2"><p>خوی بد روبه و نکو شیرست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>خوی نیکو ترا چو شیر کند</p></div>
<div class="m2"><p>خوی بد عالم از تو سیر کند</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>نیست در خورد مر مرا دل و جان</p></div>
<div class="m2"><p>یارب از هر دوام تو باز رهان</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چیست لذّت ز عمر با تکلیف</p></div>
<div class="m2"><p>همه با هم رقیب و خصم و حریف</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>زین همه خلق و زین همه بنیاد</p></div>
<div class="m2"><p>بار تکلیف خویش بر تو نهاد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گشت زین کاینات جمله خصوص</p></div>
<div class="m2"><p>احسن‌الصوره مر ترا مخصوص</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>گرد هزل و عبث چرا گردی</p></div>
<div class="m2"><p>عمر خود در عبث هبا کردی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>که ترا غرهّ کرد بر دنیی</p></div>
<div class="m2"><p>تا بدادی ز دست خود عقبی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>کار خود دیر و زود دریابی</p></div>
<div class="m2"><p>لیک اکنون هنوز در خوابی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>غافلی زین زمانهٔ غدّار</p></div>
<div class="m2"><p>از وجود زمانه دست بدار</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>کین امانی نه پایدار بُوَد</p></div>
<div class="m2"><p>حسرت‌افزای و عمر خوار بُوَد</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>چون من و چون تو صد هزاران کشت</p></div>
<div class="m2"><p>ناشده سرخ یک سر انگشت</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>تو در این راه کودکی طفلی</p></div>
<div class="m2"><p>نه شراب مروّقی ثفلی</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>مرد راهی درآی و مردی کن</p></div>
<div class="m2"><p>ورنه ره گیر و رو مه سردی کن</p></div></div>