---
title: >-
    بخش ۱۷ - در شهوت و آز گوید
---
# بخش ۱۷ - در شهوت و آز گوید

<div class="b" id="bn1"><div class="m1"><p>چیست دنیا و خلق و استظهار</p></div>
<div class="m2"><p>خاکدانی پر از سگ و مردار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بهر یک خامش این همه فریاد</p></div>
<div class="m2"><p>بهر یک خاک توده این همه باد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هست مهر زمانه با کینه</p></div>
<div class="m2"><p>سیر دارد میان لوزینه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از پی گندمی درین عالم</p></div>
<div class="m2"><p>چند باشی برهنه چون آدم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بهر گندم تو روح رنجه مدار</p></div>
<div class="m2"><p>کادم از بهر گندمی شد خوار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در جهان بنگر از پی رازش</p></div>
<div class="m2"><p>چه کنی رنگ و بوی غمازش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>این جهان زان جهان نمودارست</p></div>
<div class="m2"><p>لیکن آن زنده اینت مردارست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون یکی بحر دانش آن به شرف</p></div>
<div class="m2"><p>آخرش دُرج درّ و اوّل کف</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خانه‌ای دان شکسته زیر و زبر</p></div>
<div class="m2"><p>نقش دیوار پر درخت و سپر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نه درختیش میوه آرنده</p></div>
<div class="m2"><p>نه سپر مرگ باز دارنده</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>راز دل هر دو بر تو بنموده</p></div>
<div class="m2"><p>تو به غفلت زهر دو نشنوده</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مانده اندر غرور او شب و روز</p></div>
<div class="m2"><p>همچو آدینه کودکان از گَوز</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>صفت عُمر و مرگ و دولت و زیست</p></div>
<div class="m2"><p>زیر دورِ زمانه دانی چیست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>شاهد ابله و رقیب بهش</p></div>
<div class="m2"><p>می شیرین و میزبان ترش</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>میزبان بی‌حفاظ و بی‌آزرم</p></div>
<div class="m2"><p>خوردنی جمله سرد و آبش گرم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>پس مریز ارت چرب باید دیگ</p></div>
<div class="m2"><p>آب در دیگ و روغن اندر ریگ</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>راز این کلبه نفس غمّازست</p></div>
<div class="m2"><p>عقل کل باز خانهٔ رازست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>نچنی برگش ار چه با برگست</p></div>
<div class="m2"><p>پس دویدنش حسرت و مرگست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>به درِ عقل گرد تا برهی</p></div>
<div class="m2"><p>از بلاها و زشتی و تبهی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>مرد را عقل به بود دستور</p></div>
<div class="m2"><p>ورنه ماند چو ابلهان مغرور</p></div></div>