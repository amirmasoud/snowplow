---
title: >-
    بخش ۴۲ - فی صفة‌النّفس واحواله
---
# بخش ۴۲ - فی صفة‌النّفس واحواله

<div class="b" id="bn1"><div class="m1"><p>دزد خانه است نفس حالی بین</p></div>
<div class="m2"><p>زو نگه‌دار خانهٔ دل و دین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دزد ناگه خسیس دزد بُوَد</p></div>
<div class="m2"><p>دزد خانه نفیس دزد بُوَد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون ظفر یافت دزد بیگانه</p></div>
<div class="m2"><p>نبرد جز که خردهٔ خانه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>باز چون دزد خانه در نگرد</p></div>
<div class="m2"><p>همه کالای دور دست برد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تو خوشی زانکه پیش تست قماش</p></div>
<div class="m2"><p>زان دگرها خبر نداری باش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا کنی دست زی خزینه فراز</p></div>
<div class="m2"><p>آنچه به بایدت نبینی باز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از درونت پلنگ و موش بهم</p></div>
<div class="m2"><p>تو همی خسبی اینت جهل و ستم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>غافل از کید و حیلت شیطان</p></div>
<div class="m2"><p>کرده شیطان ز مکر قصد به جان</p></div></div>