---
title: >-
    شمارهٔ ۱۴۱
---
# شمارهٔ ۱۴۱

<div class="b" id="bn1"><div class="m1"><p>چنگری ای پارسا در عاشق مسکین به کین</p></div>
<div class="m2"><p>تا ز بد فعلی چه داری بر مسلمانان یقین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من گنه کارم تو طاعت کن چه جویی جرم من</p></div>
<div class="m2"><p>زان که من گویم بتر از من نیاید بر زمین</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>باز خواهد دست شاه و شیر جوید بیشه را</p></div>
<div class="m2"><p>بوم را ویرانه سازد همچو سگ را پارگین</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آنکه نشنیدست عدل عمر عبدالعزیز</p></div>
<div class="m2"><p>لاجرم حجاج را خواند امیرالمومنین</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مصطفا را یار بوبکرست اندر غار و بس</p></div>
<div class="m2"><p>بولهب را باز بوجهلست یار و همنشین</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>«الخبیثات» و «خبیثین» گفت ایزد در نبی</p></div>
<div class="m2"><p>تا بپرهیزند اهل «طیبات» و «طیبین»</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عاجز آمد از مشیت زلت و عصیان تو</p></div>
<div class="m2"><p>دفترت در دوده می‌مالد کرام‌الکاتبین</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کس ز صوف و فوطه بی‌طاعت نیابد پایگاه</p></div>
<div class="m2"><p>کی بجایی می‌رسد مردم ز ریش و پوستین</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گوی برد از جمله مردم فوطه باف و نیل گر</p></div>
<div class="m2"><p>عالمی را موی تابی گرددت زیر نگین</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>روی بنماید عروس دین ترا گر هیچ تو</p></div>
<div class="m2"><p>با قناعت چون سنایی غزنوی گردی قرین</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ای به دعوی بر شده بر آسمان هفتمین</p></div>
<div class="m2"><p>وز ره معنی بمانده تا به حلق اندر زمین</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>آنکه را همت ز اجزای زمین بر نگذرد</p></div>
<div class="m2"><p>چون سخن گوید ز کل آسمان هفتمین</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چند از این دعوی درویشی و لاف عاشقی</p></div>
<div class="m2"><p>ناچشیده شربت آن نازموده درد این</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>با هوای جسم رفتن در ره روحانیان</p></div>
<div class="m2"><p>در لباس دیو جستن رتبت روح‌الامین</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>سر قلاشی ندانی راه قلاشان مرو</p></div>
<div class="m2"><p>دیدهٔ بینا نداری راه درویشان مبین</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>کم سگال ار نیستی عاشق کزان در آز تن</p></div>
<div class="m2"><p>مانده معنی را بجای و کرده صورت را گزین</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ای برادر قصد ضحاک جفا پیشه مکن</p></div>
<div class="m2"><p>تا نبینی خویشتن همبر به پور آبتین</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>جنت باقی کجا یابی و راه بی‌هوان</p></div>
<div class="m2"><p>تا تو باشی در هوای جوی شیر و انگبین</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>باز ماندن بهتر آمد در سعیر سفلی آنک</p></div>
<div class="m2"><p>جنت اعلا نخواهد جز برای حور عین</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>تا نگردی فانی از اوصاف این فانی صفت</p></div>
<div class="m2"><p>بی نیازی را نبینی در بهشت راستین</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>پایت اندر طین دل بر نار باشد تا ترا</p></div>
<div class="m2"><p>دیو نخوت گفت خواهد نار به باشد ز طین</p></div></div>