---
title: >-
    شمارهٔ ۱۸
---
# شمارهٔ ۱۸

<div class="b" id="bn1"><div class="m1"><p>آن تو کوری نه جهان تاریکست</p></div>
<div class="m2"><p>آن تو کری نه سخن باریکست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر سر این سخنت نیست برو</p></div>
<div class="m2"><p>روی دیوار و سرت نزدیک‌ست</p></div></div>