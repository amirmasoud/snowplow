---
title: >-
    شمارهٔ ۱۷۲
---
# شمارهٔ ۱۷۲

<div class="b" id="bn1"><div class="m1"><p>پسری دیدم پوشیده قبای</p></div>
<div class="m2"><p>گفتم او را که به نزدیک من آی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفت من دیر بمانم نایم</p></div>
<div class="m2"><p>گفتم او را که بیا ژاژ مخای</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دیر کی مانی جایی که بود</p></div>
<div class="m2"><p>سیم در دست و گروگان در پای</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>من اگر ایستاده‌ام مسته</p></div>
<div class="m2"><p>خویشتن گر نشسته‌ای مستای</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زان که تو فتنه‌ای و من علمم</p></div>
<div class="m2"><p>تو نشسته بهی و من بر پای</p></div></div>