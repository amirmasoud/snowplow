---
title: >-
    شمارهٔ ۱۷۵
---
# شمارهٔ ۱۷۵

<div class="b" id="bn1"><div class="m1"><p>نکند دانا مستی نخورد عاقل می</p></div>
<div class="m2"><p>ننهد مرد خردمند سوی مستی پی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چه خوری چیزی کز خوردن آن چیز ترا</p></div>
<div class="m2"><p>نی چون سرو نماید به مثل سرو چو نی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر کنی بخشش گویند که می کرد نه او</p></div>
<div class="m2"><p>ور کنی عربده گویند که او کرد نه می</p></div></div>