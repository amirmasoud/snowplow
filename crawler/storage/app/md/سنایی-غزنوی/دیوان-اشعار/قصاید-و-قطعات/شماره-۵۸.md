---
title: >-
    شمارهٔ ۵۸ 
---
# شمارهٔ ۵۸ 

<div class="b" id="bn1"><div class="m1"><p>شکر ایزد را که تا من بوده‌ام</p></div>
<div class="m2"><p>حرص و آزم ساعتی رنجه نکرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هیچ خلق از من شبی غمگین نخفت</p></div>
<div class="m2"><p>هیچ کس روزی ز من خشمی نخورد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از طمع هرگز ندادم پشت خم</p></div>
<div class="m2"><p>وز حسد هرگز نکردم روی زرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نیستم آزاد مرد ار کرده‌ام</p></div>
<div class="m2"><p>یا کنم من قصد هیچ آزاد مرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با سلامت قانعم در گوشه‌ای</p></div>
<div class="m2"><p>خالی از غش فارغ از ننگ و نبرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چند چیزک دوست دارم زین جهان</p></div>
<div class="m2"><p>چون گذشتی زین حدیث اندر نورد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جامهٔ نو جای خرم بوی خوش</p></div>
<div class="m2"><p>روی خوب و کتب حکمت تخت نرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>یار نیک و بانگ رود و جام می</p></div>
<div class="m2"><p>دیگ چرب و نان گرم آب سرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>برنگردم زین سخن تا زنده‌ام</p></div>
<div class="m2"><p>گر خرد داری تو زین هم بر نگرد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گرد غم بنشان به می خوردن ز عمر</p></div>
<div class="m2"><p>پیش از آن کز تو برآرد چرخ گرد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نسیه را بر نقد مگزین و بکوش</p></div>
<div class="m2"><p>تا نباشی یک زمان از عیش فرد</p></div></div>