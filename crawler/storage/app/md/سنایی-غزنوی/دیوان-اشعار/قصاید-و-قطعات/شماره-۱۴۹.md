---
title: >-
    شمارهٔ ۱۴۹
---
# شمارهٔ ۱۴۹

<div class="b" id="bn1"><div class="m1"><p>خواهد که شاعران جهان بی صله همی</p></div>
<div class="m2"><p>باشند پیش خوانش دایم مدیح خوان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>الحق بزرگوار خردمند مهتریست</p></div>
<div class="m2"><p>کورا کسی مدیح برد خاصه رایگان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مدحش چرا کنم که بیالایدم خرد</p></div>
<div class="m2"><p>هجوش چرا کنم که بفرسایدم زبان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>باشد دروغ مدح در آن خر فراخ کون</p></div>
<div class="m2"><p>باشد دریغ هجو از آن خام قلتبان</p></div></div>