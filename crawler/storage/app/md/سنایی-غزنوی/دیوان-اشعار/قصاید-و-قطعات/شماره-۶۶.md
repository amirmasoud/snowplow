---
title: >-
    شمارهٔ ۶۶
---
# شمارهٔ ۶۶

<div class="b" id="bn1"><div class="m1"><p>خواجگانی که اندرین حضرت</p></div>
<div class="m2"><p>خویشتن محتشم همی دارند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن نکوتر که خادمان نخرند</p></div>
<div class="m2"><p>حرم اندرحرم همی دارند</p></div></div>