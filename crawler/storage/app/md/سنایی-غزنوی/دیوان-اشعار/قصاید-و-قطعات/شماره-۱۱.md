---
title: >-
    شمارهٔ ۱۱
---
# شمارهٔ ۱۱

<div class="b" id="bn1"><div class="m1"><p>مال هست از درون دل چون مار</p></div>
<div class="m2"><p>وز برون یار همچو روز و چو شب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>او چنانست کاب کشتی را</p></div>
<div class="m2"><p>از درون مرگ و از برون مرکب</p></div></div>