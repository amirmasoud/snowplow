---
title: >-
    شمارهٔ ۱۳۴ - این قطعه را بر گور نظام الملک محمد نوشتند
---
# شمارهٔ ۱۳۴ - این قطعه را بر گور نظام الملک محمد نوشتند

<div class="b" id="bn1"><div class="m1"><p>ما فرش بزرگی به جهان باز کشیدیم</p></div>
<div class="m2"><p>صد گونه شراب از کف اقبال چشیدیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن جای که ابرار نشستند نشستیم</p></div>
<div class="m2"><p>وان راه که احرار گزیدند گزیدیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گوش خود و گوش همه آراسته کردیم</p></div>
<div class="m2"><p>از بس سخن خوب که گفتیم و شنیدیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از روی سخا حاصل ده ملک بدادیم</p></div>
<div class="m2"><p>با اسب شرف منزل نه چرخ بریدیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ناگاه به زد مقرعهٔ مرگ زمانه</p></div>
<div class="m2"><p>ما نای روان رو سوی عقبی بدمیدیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دیدیم که در عهدهٔ صد گونه وبالیم</p></div>
<div class="m2"><p>خود را به یکی جان ز همه باز خریدیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پس جمله بدانید که در عالم پاداش</p></div>
<div class="m2"><p>آنها که درین راه بدادیم بدیدیم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دادند مجازات به بندی که گشادیم</p></div>
<div class="m2"><p>کردند مکافات به رنجی که کشیدیم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ما را همه مقصود به بخشایش حق بود</p></div>
<div class="m2"><p>المنةالله که به مقصود رسیدیم</p></div></div>