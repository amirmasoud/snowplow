---
title: >-
    شمارهٔ ۱۲۳
---
# شمارهٔ ۱۲۳

<div class="b" id="bn1"><div class="m1"><p>چو بر قناعت ازین گونه دسترس دارم</p></div>
<div class="m2"><p>چرا ازین و از آن خویشتن ز پس دارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خدای داند کز هر چه جز خدای بود</p></div>
<div class="m2"><p>ازو طمع چو ندارم گرش به کس دارم</p></div></div>