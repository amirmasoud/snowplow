---
title: >-
    شمارهٔ ۸۹
---
# شمارهٔ ۸۹

<div class="b" id="bn1"><div class="m1"><p>نمی‌داند مگر آنکس مراد از کشف حال آید</p></div>
<div class="m2"><p>که کشف حال را در حال بی‌حالی زوال آید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زوال حال آن باشد کمال حال بی‌حالان</p></div>
<div class="m2"><p>که درگاه زوال حال بی‌حالان مجال آید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگر چه هر که در کوی هدی باشد به شرع اندر</p></div>
<div class="m2"><p>چو در کول جلال آید همه خویش جلال آید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز حال آنگه شود صافی دل بدحال مردی را</p></div>
<div class="m2"><p>که از کوی هدی بی‌حال در کوی ضلال آید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نهان گشتست حال کشف در دلهای مشتاقان</p></div>
<div class="m2"><p>تو آوازی بر آر از دل چنان دل کز خیال آید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به جامی عذر یکسان شد سنایی را به هر حالی</p></div>
<div class="m2"><p>ز تلخی عیش او دایم همی بوی زلال آید</p></div></div>