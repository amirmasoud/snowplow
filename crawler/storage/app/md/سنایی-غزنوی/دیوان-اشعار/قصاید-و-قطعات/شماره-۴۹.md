---
title: >-
    شمارهٔ ۴۹
---
# شمارهٔ ۴۹

<div class="b" id="bn1"><div class="m1"><p>چه ممسکی که ز جود تو قطره‌ای نچکد</p></div>
<div class="m2"><p>اگر در آب کسی جامهٔ تو برتابد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به مجلسی که تو باشی ز بخل نگذاری</p></div>
<div class="m2"><p>که رادمردی از آن صدر نیکویی یابد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به ابر برشده مانی بلند و بی‌باران</p></div>
<div class="m2"><p>کدام زایر و شاعر سوی تو بشتابد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کو خود نباری و بر هیچ خلق نگذاری</p></div>
<div class="m2"><p>مر آفتاب فلک را که بر کسی تابد</p></div></div>