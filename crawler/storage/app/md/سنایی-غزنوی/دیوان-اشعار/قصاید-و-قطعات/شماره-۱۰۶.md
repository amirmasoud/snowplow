---
title: >-
    شمارهٔ ۱۰۶
---
# شمارهٔ ۱۰۶

<div class="b" id="bn1"><div class="m1"><p>هر که زین پیش بود امیر سخن</p></div>
<div class="m2"><p>از امیر سخا شدند عزیز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو همه روز گرد آن گردی</p></div>
<div class="m2"><p>که به نزدیکشان زرست و پشیز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دستهٔ گل بر کسی چه بری</p></div>
<div class="m2"><p>که فروشد به کویها گشنیز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پیرهن زان طمع مکن که ز حرص</p></div>
<div class="m2"><p>دزدد از جامهٔ پدر تیریز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بهر دهلیزبان چگویی شعر</p></div>
<div class="m2"><p>که بمانی چو کفش در دهلیز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بوسه بر لب دهی شکر یابی</p></div>
<div class="m2"><p>بوسه بر کون دهی چه یابی تیز</p></div></div>