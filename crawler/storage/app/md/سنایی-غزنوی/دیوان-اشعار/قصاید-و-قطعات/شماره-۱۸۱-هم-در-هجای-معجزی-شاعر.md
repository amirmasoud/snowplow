---
title: >-
    شمارهٔ ۱۸۱ - هم در هجای معجزی شاعر
---
# شمارهٔ ۱۸۱ - هم در هجای معجزی شاعر

<div class="b" id="bn1"><div class="m1"><p>معجز معجزی پدید آمد</p></div>
<div class="m2"><p>چون فرورید قوم او پسری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بی‌نهادی پلید و پر هوسی</p></div>
<div class="m2"><p>بی‌زمانی دراز و بی‌خبری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هم ازو بود و از کفایت او</p></div>
<div class="m2"><p>که بهر کار دارد او هنری</p></div></div>