---
title: >-
    شمارهٔ ۱۱۰
---
# شمارهٔ ۱۱۰

<div class="b" id="bn1"><div class="m1"><p>ای مرد سفر در طلب زاد سفر باش</p></div>
<div class="m2"><p>بشکن شبهٔ شهوت و غواص درر باش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از سیرت سلمان چه خوری حسرت و راهش</p></div>
<div class="m2"><p>بپذیر و تو خود بوذر و سلمان دگر باش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر چند که طوطی دلت کشتهٔ زهرست</p></div>
<div class="m2"><p>آن زهر دهان را تو همه شهد و شکر باش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون تو به دل زهر شکر داری از خود</p></div>
<div class="m2"><p>زهر تن او گردد تو مرد عبر باش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در مکهٔ دین ابرههٔ نفس علم زد</p></div>
<div class="m2"><p>تو طیر ابابیل ورا زخم حجر باش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نمرود هوای خانهٔ باطن و ز بت آگند</p></div>
<div class="m2"><p>او رفت سوی عید تو در عیش نظر باش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر خلق جهان ابرههٔ دین تو باشد</p></div>
<div class="m2"><p>تو بر فلک سیرت ایشان چو قمر باش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آن کس که مر ایوب ترا گرم غم آورد</p></div>
<div class="m2"><p>تو دیدهٔ یعقوب ورا بوی پسر باش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ور دیو ز لا حول تو خواهی که گریزد</p></div>
<div class="m2"><p>از زرق تبرا کن و با دلق عمر باش</p></div></div>