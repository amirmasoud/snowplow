---
title: >-
    شمارهٔ ۱۴۳
---
# شمارهٔ ۱۴۳

<div class="b" id="bn1"><div class="m1"><p>پای بلقاسم ز پای بلحکم بشناس نیک</p></div>
<div class="m2"><p>نیستی ایوب فرمان از دم کرمان مکن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تیغ شرع از تارک بدخواه دین داری دریغ</p></div>
<div class="m2"><p>شرط مردان این نباشد ای برادر آن مکن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عزم داری تا که خود بزغاله را بریان کنی</p></div>
<div class="m2"><p>پس چو ابراهیم رو فرزند را قربان مکن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>این ترا معلوم گردد لیکن اکنون وقت نیست</p></div>
<div class="m2"><p>کیست هر کو گر تواند گفت این کن آن مکن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر کجا مردی بد اکنون همچو تو تردامنند</p></div>
<div class="m2"><p>چند گویی مرد هستم یاد نامردان مکن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اهل را در کوی معنی همچو مردان دستگیر</p></div>
<div class="m2"><p>یار نااهلان مباش و یاد نا اهلان مکن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ناقد نقدی ولیکن نقد را آماده کن</p></div>
<div class="m2"><p>کم بضاعت تاجری تو قصد در عمان مکن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خواجه را این آیت اندر سمع کمتر می‌شود</p></div>
<div class="m2"><p>بشنو این آیت که کل من علیها فان مکن</p></div></div>