---
title: >-
    شمارهٔ ۲۹
---
# شمارهٔ ۲۹

<div class="b" id="bn1"><div class="m1"><p>پسر هند اگر چه خال منست</p></div>
<div class="m2"><p>دوستی ویم به کاری نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ور نوشت او خطی ز بهر رسول</p></div>
<div class="m2"><p>به خطش نیز افتخاری نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در مقامی که شیر مردانند</p></div>
<div class="m2"><p>در خط و خال اعتباری نیست</p></div></div>