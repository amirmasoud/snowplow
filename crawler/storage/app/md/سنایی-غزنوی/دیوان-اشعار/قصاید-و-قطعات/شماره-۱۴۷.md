---
title: >-
    شمارهٔ ۱۴۷
---
# شمارهٔ ۱۴۷

<div class="b" id="bn1"><div class="m1"><p>یک روز بپرسید منوچهر ز سالار</p></div>
<div class="m2"><p>کاندر همه عالم چه به ای سام نریمان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>او داد جوابش که در این عالم فانی</p></div>
<div class="m2"><p>گفتار حکیمان به و کردار ندیمان</p></div></div>