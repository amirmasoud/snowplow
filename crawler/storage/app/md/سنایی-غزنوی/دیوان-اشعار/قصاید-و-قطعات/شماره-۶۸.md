---
title: >-
    شمارهٔ ۶۸
---
# شمارهٔ ۶۸

<div class="b" id="bn1"><div class="m1"><p>خادمان را ز بهر آن بخرند</p></div>
<div class="m2"><p>تا به رخسارشان فرو نگرند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>«لا الی هولاء» نه مرد و نه زن</p></div>
<div class="m2"><p>بین ذالک نه ماده و نه نرند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جای ایشان شدست هند و عجم</p></div>
<div class="m2"><p>لاجرم هر دو جا به دردسرند</p></div></div>