---
title: >-
    شمارهٔ ۱۶۷
---
# شمارهٔ ۱۶۷

<div class="b" id="bn1"><div class="m1"><p>این چه قرنست اینکه در خوابند بیداران همه</p></div>
<div class="m2"><p>وین چه دورست اینکه سرمستند هشیاران همه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>طوق منت یابم اندر حلق حق گویان دین</p></div>
<div class="m2"><p>خواب غفلت بینم اندر چشم بیداران همه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در لباس مصلحت رفتند رزاقان دهر</p></div>
<div class="m2"><p>بر بساط صایبی خفتند طراران همه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در لحد خفتند بیداران دین مصطفا</p></div>
<div class="m2"><p>بر فلک بردند غیو و نعره میخواران همه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حیز متواری بدی زین پیش اکنون شد پدید</p></div>
<div class="m2"><p>زان که بی‌ننگند و بی‌عارند عیاران همه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>غارتی را عادتی کردند بزازان ما</p></div>
<div class="m2"><p>در دکان دارند ازین معنی به خرواران همه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بی‌خبر گشته‌ست گوش عقل حق‌گویان دین</p></div>
<div class="m2"><p>بی‌بصر گشته‌ست گویی چشم نظاران همه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ای جهان دیده کجااند آن جهانداران کجا</p></div>
<div class="m2"><p>وی ستمدیده کجااند آن ستمگاران همه</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آنکه از من زاد کو و آنکه زو زادم کجاست</p></div>
<div class="m2"><p>آن رفیقان نکو و آن مهربان یاران همه</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>و آن سمن رویان گل بویان حوراپیکران</p></div>
<div class="m2"><p>آنکه گل بودی خجل زان روی گلناران همه</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مرگشان هم قهر کرد آخر به امر کردگار</p></div>
<div class="m2"><p>ای برادر مرگ دان قهار قهاران همه</p></div></div>