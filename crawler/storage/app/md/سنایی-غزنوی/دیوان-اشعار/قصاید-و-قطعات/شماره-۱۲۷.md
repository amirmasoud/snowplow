---
title: >-
    شمارهٔ ۱۲۷
---
# شمارهٔ ۱۲۷

<div class="b" id="bn1"><div class="m1"><p>از زهر به مغزم رسید بویی</p></div>
<div class="m2"><p>بفگند هم اندر زمان ز پایم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زهری که به بویی بیازمودم</p></div>
<div class="m2"><p>آن به که به خوردن نیازمایم</p></div></div>