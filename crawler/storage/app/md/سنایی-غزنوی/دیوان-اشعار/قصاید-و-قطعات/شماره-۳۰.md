---
title: >-
    شمارهٔ ۳۰
---
# شمارهٔ ۳۰

<div class="b" id="bn1"><div class="m1"><p>ای ماه صیام ار چه مرا خود خطری نیست</p></div>
<div class="m2"><p>حقا که مرا همچو تو مهمان دگری نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از درد تو ای رفته به ناگه ز بر ما</p></div>
<div class="m2"><p>یک زاویه‌ای نیست که پر خون جگری نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آن کیست که از بهر تو یک قطره ببارید</p></div>
<div class="m2"><p>کان قطره کنون در صدف دین گهری نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای وای بر آن کز غم وقت سحر تو</p></div>
<div class="m2"><p>او را به جز از وقت صبوحی سحری نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بسیار تو آیی و نبینی همه را زانک</p></div>
<div class="m2"><p>ما برگذریم از تو ترا خود خبری نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آن دل که همی ترسد از شعلهٔ آتش</p></div>
<div class="m2"><p>والله که به جز روزه مر او را سپری نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بس کس که چو ما روزه همی داشت ازین پیش</p></div>
<div class="m2"><p>امروز به جز خاک مر او را مقری نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ای داده به باد این مه با برکت و با خیر</p></div>
<div class="m2"><p>مانا کت ازین آتش در دل شرری نیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بسیار کسا کو بر عیدی چو تو می‌خواست</p></div>
<div class="m2"><p>امروز جز از حسرت از آنش ثمری نیست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>اشکی دو سه امروز درین بقعه فرو بار</p></div>
<div class="m2"><p>کاندر چمن عمر تو زین به مطری نیست</p></div></div>