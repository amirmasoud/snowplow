---
title: >-
    شمارهٔ ۵۲
---
# شمارهٔ ۵۲

<div class="b" id="bn1"><div class="m1"><p>ای خواجه اگر قامت اقبال تو امروز</p></div>
<div class="m2"><p>مانند الف هیچ خم و پیچ ندارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بسیار تفاخر مکن امروز که فردا</p></div>
<div class="m2"><p>معلوم تو گردد که الف هیچ ندارد</p></div></div>