---
title: >-
    شمارهٔ ۱۳۳ - در مدح عمادالدین محمدبن منصور
---
# شمارهٔ ۱۳۳ - در مدح عمادالدین محمدبن منصور

<div class="b" id="bn1"><div class="m1"><p>ای محمد نام و احمد خلق و محمودی شیم</p></div>
<div class="m2"><p>محمدت را همچنان چون ملک را تیغ و قلم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بذل بی‌دستت نباشد همچو دانش بی‌خرد</p></div>
<div class="m2"><p>مال با جودت نماند همچو شادی با ستم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>روح را از رنجهای دل تهی کردی کنار</p></div>
<div class="m2"><p>آز را از گنجهای جود پر کردی شکم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر همی یک چند بی‌کام تو گردد دور چرخ</p></div>
<div class="m2"><p>تا نباشی همچو ابر ای نایب دریا دژم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در وجود غم چنین بد دل چه باشی بهر آنک</p></div>
<div class="m2"><p>کار اقبال تو می‌سازند در پردهٔ عدم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>می‌کند از خانهٔ فضل الاهی بهر تو</p></div>
<div class="m2"><p>تختهٔ تقدیر ایزد را ز تاییدت رقم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>منگر این حال غم و اندیشه کز روی خرد</p></div>
<div class="m2"><p>شادی صد ساله زاید مادر یک روزه غم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>باش تا سر برزند خورشید اقبالت ز چرخ</p></div>
<div class="m2"><p>تا جهانی را ببینی پیش خود چون من خرم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تا ببینی دشمنانت را به طوع و اختیار</p></div>
<div class="m2"><p>پیش روی چون مهت چون چرخ داده پشت خم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>باش تا دریای جودت در فشاند تا شود</p></div>
<div class="m2"><p>صدهزاران شاعر از جود تو چون من محتشم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ای دو گوشت بر صحیفهٔ فضل فهرست خرد</p></div>
<div class="m2"><p>وی دو دستت در کتاب جود سرباب کرم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>با چنین فضلی که کردم قصد در گاهت ز بیم</p></div>
<div class="m2"><p>خشک شد خون در تن امید چون شاخ بقم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>آمدم سوی تو از بهر وعدهٔ بخششت</p></div>
<div class="m2"><p>از عرقهای خجالت عرقها را داده نم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چون علم کی بود می پیشت چنین لیک از سخا</p></div>
<div class="m2"><p>هم تو کردی بنده را اندر چنان مجلس علم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>حلقه شد بر من جهان چون عقد سیصد در امید</p></div>
<div class="m2"><p>تا درین سی روز دارم طمع آن سیصد درم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ریش در وعده مجنبان از سر حری بگوی</p></div>
<div class="m2"><p>از پی دوری ره من زود یا «لا» یا «نعم»</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>تا بود مر بد سگالان را به طاعتها خلل</p></div>
<div class="m2"><p>تا بود مر نیکمردان را به زلتها ندم</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>تا در آب و خاک و باد و آتش از بهر صلاح</p></div>
<div class="m2"><p>گرمی و خشکی و سردی و تری باشد به هم</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>در هنرمندی چو سرو اندر چمن گاه نشاط</p></div>
<div class="m2"><p>گاه از نزهت به بال و گاه از شادی به چم</p></div></div>