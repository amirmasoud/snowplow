---
title: >-
    شمارهٔ ۱۲۵
---
# شمارهٔ ۱۲۵

<div class="b" id="bn1"><div class="m1"><p>عمر دو نیمه‌ست و ازین بیش نیست</p></div>
<div class="m2"><p>اول و آخر، چو همی بنگرم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نیمی از آن کردم در مدح تو</p></div>
<div class="m2"><p>نیمی در وعده به پایان برم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عمر چو در وعده و مدح تو شد</p></div>
<div class="m2"><p>صله مگر روز قیامت خورم</p></div></div>