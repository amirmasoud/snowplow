---
title: >-
    شمارهٔ ۱۷۳
---
# شمارهٔ ۱۷۳

<div class="b" id="bn1"><div class="m1"><p>به هفت کشور تا شکر پنج و ده گویم</p></div>
<div class="m2"><p>نبود خواهم ساکن دو روز در یک جای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دو پای دارم چار دگر بباید از آنک</p></div>
<div class="m2"><p>به هفت کشور نتوان رسید بی‌شش پای</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چنان زندگانی کن ای نیک رای</p></div>
<div class="m2"><p>از آن پس که توفیق دادت خدای</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>که خایند ز اندوهت انگشت دست</p></div>
<div class="m2"><p>چو اندر زمینت آید انگشت پای</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مکن در جهان زندگانی چنانک</p></div>
<div class="m2"><p>جهانی به مرگ تو دارند رای</p></div></div>