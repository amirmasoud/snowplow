---
title: >-
    شمارهٔ ۶۵
---
# شمارهٔ ۶۵

<div class="b" id="bn1"><div class="m1"><p>چرا نه مردم دانا چنان زید که به غم</p></div>
<div class="m2"><p>چو سرش درد کند دشمنان دژم گردند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چنان نباید بودن که گر سرش ببرند</p></div>
<div class="m2"><p>به سر بریدن او دوستان خرم گردند</p></div></div>