---
title: >-
    شمارهٔ ۵۹
---
# شمارهٔ ۵۹

<div class="b" id="bn1"><div class="m1"><p>آنچه دی کرد به من آن پسر سر گرغر</p></div>
<div class="m2"><p>اندر آفاق ندیدم که یکی لمتر کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتمش پوتی و لوتی کنی امروز مرا</p></div>
<div class="m2"><p>دست بر سر زد و پس پای سبک در سر کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دست در گردنم آورد پس او از سر لطف</p></div>
<div class="m2"><p>گوش و آغوش مرا پر گهر و زیور کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا تو آبی خوری آن جان جهان بی‌مکری</p></div>
<div class="m2"><p>پشتم از آب تهی و شکم از نان پر کرد</p></div></div>