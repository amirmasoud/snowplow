---
title: >-
    شمارهٔ ۱۱۸
---
# شمارهٔ ۱۱۸

<div class="b" id="bn1"><div class="m1"><p>طلوع مهر سعادت به ساحت اقبال</p></div>
<div class="m2"><p>ظهور ماه معالی بر آسمان جلال</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نتیجهٔ کرم و مردمی و فضل و هنر</p></div>
<div class="m2"><p>طلیعهٔ اثر لطف ایزد متعال</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خجسته باد و همایون مبارک و میمون</p></div>
<div class="m2"><p>به سعد طالع و بخت جوان و نیکوفال</p></div></div>