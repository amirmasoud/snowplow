---
title: >-
    شمارهٔ ۱۵ - در مذمت دنیا
---
# شمارهٔ ۱۵ - در مذمت دنیا

<div class="b" id="bn1"><div class="m1"><p>گنده پیریست تیره روی جهان</p></div>
<div class="m2"><p>خرد ما بدو نظر کردست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به سپیدی رخانش غره مشو</p></div>
<div class="m2"><p>کان سیاهی سپید برکردست</p></div></div>