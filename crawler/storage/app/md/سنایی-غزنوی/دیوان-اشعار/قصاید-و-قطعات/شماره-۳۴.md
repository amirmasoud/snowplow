---
title: >-
    شمارهٔ ۳۴
---
# شمارهٔ ۳۴

<div class="b" id="bn1"><div class="m1"><p>آمد آن حور و دست من بربست</p></div>
<div class="m2"><p>زده استادوار نیش به دست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زنخ او به دست بگرفتم</p></div>
<div class="m2"><p>چون رگ دست من ز نیش بخست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفت هشیار باش و آهسته</p></div>
<div class="m2"><p>دست هر جا مزن چون مردم مست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفتمش گر به دست بگرفتم</p></div>
<div class="m2"><p>زنخ سادهٔ تو عذرم هست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زان که هنگام رگ زدن شرطست</p></div>
<div class="m2"><p>گوی سیمین گرفتن اندر دست</p></div></div>