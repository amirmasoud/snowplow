---
title: >-
    شمارهٔ ۸
---
# شمارهٔ ۸

<div class="b" id="bn1"><div class="m1"><p>ای برآراسته از لطف و سخا معدن خویش</p></div>
<div class="m2"><p>همچو گوهر که بیاراید مر معدن را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دفتری ساختم از بهر تو پر مدح و هجا</p></div>
<div class="m2"><p>هر چه مدحست ترا هر چه هجا دشمن را</p></div></div>