---
title: >-
    شمارهٔ ۱۷۱
---
# شمارهٔ ۱۷۱

<div class="b" id="bn1"><div class="m1"><p>اگر پای تو از خط خطا گامی بعیدستی</p></div>
<div class="m2"><p>بر تخت تو اندر دین بر از عرش مجیدستی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وگر امروز طبع تو ز طراری نه طاقستی</p></div>
<div class="m2"><p>وگر غفلت ز رزاقی زر فرد آفریدستی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز عشق آن یکی سلطان طاعت شادمان بودی</p></div>
<div class="m2"><p>ز رشک آن دگر شیطان شهوت مستزیدستی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تو مستی زان نیاری رفت در بازار عشاقان</p></div>
<div class="m2"><p>اگر زر بودیی بر سنگ صرافان پدیدستی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همیشه این همی خوانی که دست من درین عالم</p></div>
<div class="m2"><p>گشاده‌تر ز دست و تیغ سلطان عمیدستی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همیشه خواب این بینی که یارب کاشکی دانم</p></div>
<div class="m2"><p>سر انگشت من صندوق خلقان را کلیدستی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اگر بخت و رضا در تحت رای بلحکم بودی</p></div>
<div class="m2"><p>وگر لوح و قلم در دست شاگرد یزیدستی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نباشد آنکه تو خواهی و گر نه این چنین بودی</p></div>
<div class="m2"><p>همه رو سالکان خواهند گر هر روز عیدستی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اگر بودی دلت مشتاق در گفتار بسم‌الله</p></div>
<div class="m2"><p>ترا هر دم هزاران نعرهٔ «هل من مزید» ستی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>وگر عاجز سنایی نیستی در دست نااهلان</p></div>
<div class="m2"><p>ز سر سامری عالم پر از پیک و بریدستی</p></div></div>