---
title: >-
    شمارهٔ ۱۸۸
---
# شمارهٔ ۱۸۸

<div class="b" id="bn1"><div class="m1"><p>خسرو از مازندران آید همی</p></div>
<div class="m2"><p>یا مسیح از آسمان آید همی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یا ز بهر مصلحت روح‌الامین</p></div>
<div class="m2"><p>سوی دنیا زان جهان آید همی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یا سکندر با بزرگان عراق</p></div>
<div class="m2"><p>سوی شرق از قیروان آید همی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>«ریگ آموی و درازی راه او</p></div>
<div class="m2"><p>زیر پامان پرنیان آید همی»</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>«آب جیحون از نشاط روی دوست</p></div>
<div class="m2"><p>اسب ما را تا میان آید همی»</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>رنج غربت رفت و تیمار سفر</p></div>
<div class="m2"><p>«بوی یار مهربان آید همی»</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>این از آن وزنست گفته رودکی</p></div>
<div class="m2"><p>«یاد جوی مولیان آید همی»</p></div></div>