---
title: >-
    شمارهٔ ۱۹۷
---
# شمارهٔ ۱۹۷

<div class="b" id="bn1"><div class="m1"><p>چونت نپرسم بگویی اینت کراهت</p></div>
<div class="m2"><p>چونت بخوانم نیایی اینت گرانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دعوی دانش کنی همیشه ولیکن</p></div>
<div class="m2"><p>هیچ ندانی ورا که هیچ ندانی</p></div></div>