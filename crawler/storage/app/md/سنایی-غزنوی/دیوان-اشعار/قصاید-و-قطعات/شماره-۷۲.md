---
title: >-
    شمارهٔ ۷۲
---
# شمارهٔ ۷۲

<div class="b" id="bn1"><div class="m1"><p>با دلی رفته به استسقا</p></div>
<div class="m2"><p>که معاصیش هیچ غم نکند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با چنین دل چه جای بارانست</p></div>
<div class="m2"><p>کابر بر تو کمیز هم نکند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با همه خلق جهان گر چه از آن</p></div>
<div class="m2"><p>بیشتر بی‌ره و کمتر به رهند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تو چنان زی که بمیری برهی</p></div>
<div class="m2"><p>نه چنان چون تو بمیری برهند</p></div></div>