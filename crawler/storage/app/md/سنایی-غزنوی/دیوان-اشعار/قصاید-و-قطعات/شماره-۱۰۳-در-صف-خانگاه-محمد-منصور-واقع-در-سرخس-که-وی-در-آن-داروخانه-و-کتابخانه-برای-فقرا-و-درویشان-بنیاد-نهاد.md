---
title: >-
    شمارهٔ ۱۰۳ - در صف خانگاه محمد منصور واقع در سرخس که وی در آن داروخانه و کتابخانه برای فقرا و درویشان بنیاد نهاد
---
# شمارهٔ ۱۰۳ - در صف خانگاه محمد منصور واقع در سرخس که وی در آن داروخانه و کتابخانه برای فقرا و درویشان بنیاد نهاد

<div class="b" id="bn1"><div class="m1"><p>لب روح الله ست یا دم صور</p></div>
<div class="m2"><p>خانگاه محمد منصور</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که ز درس و کتاب و دارو هست</p></div>
<div class="m2"><p>از سه سو دین و جان و تن را سور</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زین بنا ایمن از دو چیز سه چیز</p></div>
<div class="m2"><p>تن و جان و دل از قبور و فتور</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تعبیه در صدای هر خم اوست</p></div>
<div class="m2"><p>لحن داوود با ادای زبور</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از تحلیش تیره چهرهٔ تیر</p></div>
<div class="m2"><p>وز تجلیش طیره تودهٔ طور</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در تن ار علتی‌ست اینجا خواه</p></div>
<div class="m2"><p>حب مرطوب و شربت محرور</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در دل ار شبهتیست اینجا خوان</p></div>
<div class="m2"><p>لوح محفوظ و دفتر مسطور</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کتب اینجاست ای دل طالب</p></div>
<div class="m2"><p>دارو اینجاست ای تن رنجور</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>عیسی اینجاست ای هوای عفن</p></div>
<div class="m2"><p>خضر اینجاست ای سراب غرور</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>پس ازین زین ستانه خواهد بود</p></div>
<div class="m2"><p>دولت و رحمت و قصور و حبور</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>صفت و صورتش گه ادراک</p></div>
<div class="m2"><p>برتر از گوش روح و دیدهٔ حور</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چون بدو چشم نیک درنرسد</p></div>
<div class="m2"><p>چونش گویم که چشم بد ز تو دور</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>مجد او داشت مر سنایی را</p></div>
<div class="m2"><p>در نثای سنای خود معذور</p></div></div>