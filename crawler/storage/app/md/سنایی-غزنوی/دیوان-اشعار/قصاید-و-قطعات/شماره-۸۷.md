---
title: >-
    شمارهٔ ۸۷ 
---
# شمارهٔ ۸۷ 

<div class="b" id="bn1"><div class="m1"><p>چون شکرم در آب دو چشم و دلم فلک</p></div>
<div class="m2"><p>در جام کینه خوشتر از آب و شکر کشید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گردون زبان عقل مرا قفل برفگند</p></div>
<div class="m2"><p>و ایام چشم بخت مرا میل در کشید</p></div></div>