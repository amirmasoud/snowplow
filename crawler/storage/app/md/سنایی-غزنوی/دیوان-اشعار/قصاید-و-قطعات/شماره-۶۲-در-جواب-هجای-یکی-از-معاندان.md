---
title: >-
    شمارهٔ ۶۲ - در جواب هجای یکی از معاندان
---
# شمارهٔ ۶۲ - در جواب هجای یکی از معاندان

<div class="b" id="bn1"><div class="m1"><p>سرخ گویی همیشه غر باشد</p></div>
<div class="m2"><p>شبه از لعل پاکتر باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این چنین ژاژ نزد هر عاقل</p></div>
<div class="m2"><p>سخنی سخت مختصر باشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>لعل مصنوع آفتاب بود</p></div>
<div class="m2"><p>شیشه مصنوع شیشه‌گر باشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سرخ اگر نیست پس بر هر عقل</p></div>
<div class="m2"><p>سخن مرتضا دگر باشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون به یک جای رسته سرخ و سیاه</p></div>
<div class="m2"><p>سرخ پیوسته بر زبر باشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>من چه گویم که خود به هر مکتب</p></div>
<div class="m2"><p>کودکان را ازین خبر باشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون که سرخ‌ست اصل عمر به دوست</p></div>
<div class="m2"><p>جایش اندر دل و جگر باشد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون سیه گشت هم درین دو مکان</p></div>
<div class="m2"><p>اصل دیوانگی و شر باشد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زیر لعلست لاله را سیهی</p></div>
<div class="m2"><p>دودکی خوشتر از شرر باشد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>علم صبح سرخ آمد از آنک</p></div>
<div class="m2"><p>بر سپاه شبش ظفر باشد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سیهی بی‌نهاد و بی‌معنی</p></div>
<div class="m2"><p>زان ز تو خلق بر حذر باشد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نزد ما این چنین سیه که تویی</p></div>
<div class="m2"><p>مرد نبود که ... خر باشد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>روز کزین فعل زشت روز قضا</p></div>
<div class="m2"><p>نامت از تو سیاه‌تر باشد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>پشک چون تو بود چو خشک شود</p></div>
<div class="m2"><p>مشک چون من بود چو تر باشد</p></div></div>