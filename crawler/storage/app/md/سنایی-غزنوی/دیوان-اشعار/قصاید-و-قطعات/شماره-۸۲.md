---
title: >-
    شمارهٔ ۸۲ 
---
# شمارهٔ ۸۲ 

<div class="b" id="bn1"><div class="m1"><p>با بقای پدر پسر ناید</p></div>
<div class="m2"><p>شادی مهتری به سر ناید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شمس در غرب تا فرو نشود</p></div>
<div class="m2"><p>از سوی شرق بدر برناید</p></div></div>