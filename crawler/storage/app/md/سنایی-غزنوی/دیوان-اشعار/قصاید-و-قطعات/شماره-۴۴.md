---
title: >-
    شمارهٔ ۴۴
---
# شمارهٔ ۴۴

<div class="b" id="bn1"><div class="m1"><p>یک نیمه عمر خویش به بیهودگی به باد</p></div>
<div class="m2"><p>دادیم و هیچ گه نشدیم از زمانه شاد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از گشت آسمان و ز تقدیر ایزدی</p></div>
<div class="m2"><p>بر کس چنین نباشد و بر کس چنین مباد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یا روزگار کینه کش از مرد دانشست</p></div>
<div class="m2"><p>یا قسم من ز دانش من کمتر اوفتاد</p></div></div>