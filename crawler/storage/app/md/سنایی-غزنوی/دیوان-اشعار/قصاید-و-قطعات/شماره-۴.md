---
title: >-
    شمارهٔ ۴
---
# شمارهٔ ۴

<div class="b" id="bn1"><div class="m1"><p>ای که اطفال به گهواره درون از ستمت</p></div>
<div class="m2"><p>سور نادیده بجویند همی ماتم را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قفسی شد ز تو عالم به همه عالمیان</p></div>
<div class="m2"><p>اینت زحمت ز وجود تو بنی‌آدم را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>وه که تا روز قیامت پی آلایش ملک</p></div>
<div class="m2"><p>طاهری از تو نجس‌تر نبود عالم را</p></div></div>