---
title: >-
    شمارهٔ ۱۱۷
---
# شمارهٔ ۱۱۷

<div class="b" id="bn1"><div class="m1"><p>گفت بر دوخته مرا شعری</p></div>
<div class="m2"><p>خواجه خیاطی از سر فرهنگ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>معنی او چو ریسمان باریک</p></div>
<div class="m2"><p>قافیت همچو چشم سوزن تنگ</p></div></div>