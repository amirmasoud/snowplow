---
title: >-
    شمارهٔ ۸۰ 
---
# شمارهٔ ۸۰ 

<div class="b" id="bn1"><div class="m1"><p>اگر معمار جاه او نباشد</p></div>
<div class="m2"><p>بنای مملکت ویران نماید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جهان را از امانی دل بگیرد</p></div>
<div class="m2"><p>به قدر همت ار احسان نماید</p></div></div>