---
title: >-
    شمارهٔ ۴۱
---
# شمارهٔ ۴۱

<div class="b" id="bn1"><div class="m1"><p>گفتی که بترسد ز همه خلق سنایی</p></div>
<div class="m2"><p>پاسخ شنو ار چند نه‌ای در خور پاسخ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جغد ار که بترسد بنترسد ز پی جنس</p></div>
<div class="m2"><p>آن مرغ که دارند شهانش همه فرخ</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آن مست ز مستی بنترسد نه ز مردی</p></div>
<div class="m2"><p>ور نه بخرد نیزهٔ خطی شمرد لخ</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در بند بود رخ همه از اسب و پیاده</p></div>
<div class="m2"><p>هر چند همه نطع بود جایگه رخ</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نز روی عزیزیست که چون مرکب شاهان</p></div>
<div class="m2"><p>رایض نکند بر سر خر کره همی مخ</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گویی که نترسم ز همه دیوان آری</p></div>
<div class="m2"><p>از میخ چه ترسد که مر او را نبود مخ</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بیدار نه‌ای فارغی از بانگ تکاتک</p></div>
<div class="m2"><p>بیمار نه‌ای فارغی از بند اخ و اخ</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ایمن بود از چشم بد آن را که ز زشتی</p></div>
<div class="m2"><p>در چشم کسان چون رخ شطرنج بود رخ</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زان ایمنی از دیدن هر کس که بگویند</p></div>
<div class="m2"><p>اندر مثل عامه که کخ را نبرد کخ</p></div></div>