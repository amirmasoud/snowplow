---
title: >-
    شمارهٔ ۱۰۴
---
# شمارهٔ ۱۰۴

<div class="b" id="bn1"><div class="m1"><p>اگر چون زر نخواهی روی عاشق</p></div>
<div class="m2"><p>منه بر گردن چون سیم سنگور</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جهان از زشت قوادان تهی شد</p></div>
<div class="m2"><p>که حمال فقع باید همی حور</p></div></div>