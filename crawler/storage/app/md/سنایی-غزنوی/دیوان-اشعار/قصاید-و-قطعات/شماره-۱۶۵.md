---
title: >-
    شمارهٔ ۱۶۵
---
# شمارهٔ ۱۶۵

<div class="b" id="bn1"><div class="m1"><p>ای تیر غم و رنج بسی خورده و برده</p></div>
<div class="m2"><p>واقف شده بر معرفت خرقه و خورده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر ظاهر خود نقش شریعت بگشادم</p></div>
<div class="m2"><p>در باطن خود حرف حقیقت بسترده</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با هستی خود نرد فنا باخته بسیار</p></div>
<div class="m2"><p>صد دست فزون مانده و یک دست نبرده</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در آرزوی کوی خرابات همه سال</p></div>
<div class="m2"><p>اول قدم از راه خرابی بسپرده</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ایمن شده از عمر خود و گشت شب و روز</p></div>
<div class="m2"><p>در بی‌خردی کیسه به طرار سپرده</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ور زان که ترا نیستی ای خواجه تمناست</p></div>
<div class="m2"><p>هان تا نکنی تکیه بر انفاس شمرده</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زان پیش که نوبت به سر آید تو در آن کوش</p></div>
<div class="m2"><p>تا مردهٔ زنده شوی ای زندهٔ مرده</p></div></div>