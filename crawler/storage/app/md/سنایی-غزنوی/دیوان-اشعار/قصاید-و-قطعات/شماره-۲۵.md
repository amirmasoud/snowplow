---
title: >-
    شمارهٔ ۲۵
---
# شمارهٔ ۲۵

<div class="b" id="bn1"><div class="m1"><p>به همه وقت دلیری نکنند</p></div>
<div class="m2"><p>هر کرا از خرد و هش یاریست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زان که هر جای به جز در صف حرب</p></div>
<div class="m2"><p>بد دلی بیش بود هشیاریست</p></div></div>