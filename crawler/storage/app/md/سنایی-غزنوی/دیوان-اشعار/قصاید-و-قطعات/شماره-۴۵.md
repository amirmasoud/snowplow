---
title: >-
    شمارهٔ ۴۵
---
# شمارهٔ ۴۵

<div class="b" id="bn1"><div class="m1"><p>گر چه شمشیر حیدر کرار</p></div>
<div class="m2"><p>کافران کشت و قلعه‌ها بگشاد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا سه تا نان نداد در حق او</p></div>
<div class="m2"><p>هفده آیت خدای نفرستاد</p></div></div>