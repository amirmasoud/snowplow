---
title: >-
    شمارهٔ ۳۱
---
# شمارهٔ ۳۱

<div class="b" id="bn1"><div class="m1"><p>زین پسم با دیو مردم پیکر و پیکار نیست</p></div>
<div class="m2"><p>گر بمانم زنده دیگر با غرورم کار نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یافتم در بی‌قراری مرکزی کز راه دین</p></div>
<div class="m2"><p>جز نشاط عقل و جانش مرکز پرگار نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یافتم بازاری اندر عالم فارغ دلان</p></div>
<div class="m2"><p>کاندران بازار خوی خواجه را بازار نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در سرای ضرب او الا به نام شاه عقل</p></div>
<div class="m2"><p>بر جمال چهرهٔ آزادگان دینار نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر گل حکمت شنوده باده گلگون حکم</p></div>
<div class="m2"><p>گاه اسراف خماری بر گلی کش خار نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زیر این موکب گذر کن بر جهان کز روی حکم</p></div>
<div class="m2"><p>جز به شمشیر نبوت کس برو سالار نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>واندر آن موکب سوارانند کاندر رزمشان</p></div>
<div class="m2"><p>رستم و اسفندیار و زال را مقدار نیست</p></div></div>