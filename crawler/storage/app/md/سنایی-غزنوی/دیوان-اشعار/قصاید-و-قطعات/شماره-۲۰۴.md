---
title: >-
    شمارهٔ ۲۰۴
---
# شمارهٔ ۲۰۴

<div class="b" id="bn1"><div class="m1"><p>ملکا ذکر تو گویم که تو پاکی و خدایی</p></div>
<div class="m2"><p>نروم جز به همان ره که توام راه نمایی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همه درگاه تو جویم همه از فضل تو پویم</p></div>
<div class="m2"><p>همه توحید تو گویم که به توحید سزایی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تو زن و جفت نداری تو خور و خفت نداری</p></div>
<div class="m2"><p>احد بی زن و جفتی ملک کامروایی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نه نیازت به ولادت نه به فرزندت حاجت</p></div>
<div class="m2"><p>تو جلیل الجبروتی تو نصیر الامرایی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تو حکیمی تو عظیمی تو کریمی تو رحیمی</p></div>
<div class="m2"><p>تو نمایندهٔ فضلی تو سزاوار ثنایی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بری از رنج و گدازی بری از درد و نیازی</p></div>
<div class="m2"><p>بری از بیم و امیدی بری از چون و چرایی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بری از خوردن و خفتن بری از شرک و شبیهی</p></div>
<div class="m2"><p>بری از صورت و رنگی بری از عیب و خطایی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نتوان وصف تو گفتن که تو در فهم نگنجی</p></div>
<div class="m2"><p>نتوان شبه تو گفتن که تو در وهم نیایی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نبد این خلق و تو بودی نبود خلق و تو باشی</p></div>
<div class="m2"><p>نه بجنبی نه بگردی نه بکاهی نه فزایی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>همه عزی و جلالی همه علمی و یقینی</p></div>
<div class="m2"><p>همه نوری و سروری همه جودی و جزایی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>همه غیبی تو بدانی همه عیبی تو بپوشی</p></div>
<div class="m2"><p>همه بیشی تو بکاهی همه کمی تو فزایی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>احد لیس کمثله صمد لیس له ضد</p></div>
<div class="m2"><p>لمن الملک تو گویی که مر آن را تو سزایی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>لب و دندان سنایی همه توحید تو گوید</p></div>
<div class="m2"><p>مگر از آتش دوزخ بودش روی رهایی</p></div></div>