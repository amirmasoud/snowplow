---
title: >-
    شمارهٔ ۱۵۵ - در مدح بهرامشاه
---
# شمارهٔ ۱۵۵ - در مدح بهرامشاه

<div class="b" id="bn1"><div class="m1"><p>خواجه غلط کرده است در چه؟ در ابروی او</p></div>
<div class="m2"><p>زان که نسازد همی قبلهٔ دل سوی او</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قبلهٔ عقلست و نقل پیچ و خم زلف او</p></div>
<div class="m2"><p>دایهٔ حورست و روح بوی خوش و خوی او</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شیر فلک را شدست از پی کسب شرف</p></div>
<div class="m2"><p>مسجد حاجت روا خاک سر کوی او</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تاز دو عید و یکی قدر چه خیزد ترا</p></div>
<div class="m2"><p>عید همی بین و قدر در شکن موی او</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر سر کوی دل آی تا یابد یک دمی</p></div>
<div class="m2"><p>رحمت درمان این زحمت داروی او</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جادو اگر در بهشت نبود پس در رخش</p></div>
<div class="m2"><p>از چه بهشتی شدست نرگس جادوی او</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سایهٔ گیسوش را دار غنیمت که دل</p></div>
<div class="m2"><p>کیسه بسی دوختست در خم گیسوی او</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شیر فلک شد به شرط روبه بازی از آنک</p></div>
<div class="m2"><p>تا به کف آرد مگر چشم چو آهوی او</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>قبله اگر چه بسی‌ست از پی احرام دل</p></div>
<div class="m2"><p>چشم سنایی نساخت قبله جز ابروی او</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شد ز پی دین و جاه چون سم شبدیز شاه</p></div>
<div class="m2"><p>سجده‌گه و قبله‌گاه دایرهٔ روی او</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سلطان بهرامشاه آنکه گه زور هست</p></div>
<div class="m2"><p>گردن گردان زدن بازی بازوی او</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>از پی تشریف خویش در همه چین و ختا</p></div>
<div class="m2"><p>بچهٔ یک ترک نیست ناشده هندوی او</p></div></div>