---
title: >-
    شمارهٔ ۱۳۸
---
# شمارهٔ ۱۳۸

<div class="b" id="bn1"><div class="m1"><p>چند گویی که زحمتت کردم</p></div>
<div class="m2"><p>تا نگردی ز من گران گران</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به سر تو که دوستر دارم</p></div>
<div class="m2"><p>زحمت تو ز رحمت دگران</p></div></div>