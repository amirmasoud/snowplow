---
title: >-
    شمارهٔ ۷۴ - در هجای «معجزی» شاعر
---
# شمارهٔ ۷۴ - در هجای «معجزی» شاعر

<div class="b" id="bn1"><div class="m1"><p>معجزی خود ز معجز ادبار</p></div>
<div class="m2"><p>نزد هر زیرکی کم از خر بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خود همه کس برو همی خندید</p></div>
<div class="m2"><p>زان که عقلش ز جهل کمتر بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زین چنین کون دریده مادر و زن</p></div>
<div class="m2"><p>ریشخندش نیز درخور بود</p></div></div>