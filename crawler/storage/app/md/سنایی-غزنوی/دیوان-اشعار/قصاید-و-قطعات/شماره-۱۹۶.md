---
title: >-
    شمارهٔ ۱۹۶
---
# شمارهٔ ۱۹۶

<div class="b" id="bn1"><div class="m1"><p>هم اکنون از هم اکنون داد بستان</p></div>
<div class="m2"><p>که اکنونست بیشک زندگانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مکن هرگز حوالت سوی فردا</p></div>
<div class="m2"><p>که حال و قصهٔ فردا ندانی</p></div></div>