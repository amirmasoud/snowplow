---
title: >-
    شمارهٔ ۳۹ - در ذم مردم بلخ
---
# شمارهٔ ۳۹ - در ذم مردم بلخ

<div class="b" id="bn1"><div class="m1"><p>از بس غر و غر زن که به بلخند ادیبانش</p></div>
<div class="m2"><p>می باز ندانند مذکر ز مونث</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بلخی که کند از گه خردی پسران را</p></div>
<div class="m2"><p>برکان دهی و دف زنی و ذلت لت حث</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زان قبه لقب گشت مر او را که نیابی</p></div>
<div class="m2"><p>در قبه به جز مسخره و رند و مخنث</p></div></div>