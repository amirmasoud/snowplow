---
title: >-
    شمارهٔ ۶۰
---
# شمارهٔ ۶۰

<div class="b" id="bn1"><div class="m1"><p>آنکه تدبیر ظفر گستر او گر خواهد</p></div>
<div class="m2"><p>عقدهٔ نفی ز دیباچهٔ لا برگیرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تیغ را در سخن ملک زبان کنده شود</p></div>
<div class="m2"><p>هر کجا او قلم کامروا برگیرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در هوایی که در او پای سمند تو رسد</p></div>
<div class="m2"><p>تشنه از عین سراب آب بقا برگیرد</p></div></div>