---
title: >-
    شمارهٔ ۹۵ - در رثای زکی‌الدین بلخی
---
# شمارهٔ ۹۵ - در رثای زکی‌الدین بلخی

<div class="b" id="bn1"><div class="m1"><p>ای برادر زکی بمرد و بشد</p></div>
<div class="m2"><p>تا یکی به ز ما قرین جوید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا ز آب حیات آن عالم</p></div>
<div class="m2"><p>تن و جان از عدم فرو شوید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من ز غم مرده‌ام که کی بود او</p></div>
<div class="m2"><p>باز از آنجا به سوی من پوید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پس تو گویی که مرثیت گویش</p></div>
<div class="m2"><p>زنده را مرده مرثیت گوید</p></div></div>