---
title: >-
    شمارهٔ ۳۳
---
# شمارهٔ ۳۳

<div class="b" id="bn1"><div class="m1"><p>هر که در خطهٔ مسلمانیست</p></div>
<div class="m2"><p>متلاشی چو نفس حیوانیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر که عیسی‌ست او ز مریم زاد</p></div>
<div class="m2"><p>هر که او یوسفست کنعانیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فرق باشد میان لام و الف</p></div>
<div class="m2"><p>این چه آشوب و حشو و لامانیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چه گرانی کنی ز کافهٔ کاف</p></div>
<div class="m2"><p>این گرانی ز بهر ارزانیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تن خود را عمارتی فرمای</p></div>
<div class="m2"><p>کاین عمارت نصیب دهقانیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا سنایی ز خاک سر بر زد</p></div>
<div class="m2"><p>در خراسان همه تن آسانیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فتنهٔ روزگار او شده‌اند</p></div>
<div class="m2"><p>گر عراقی و گر خراسانیست</p></div></div>