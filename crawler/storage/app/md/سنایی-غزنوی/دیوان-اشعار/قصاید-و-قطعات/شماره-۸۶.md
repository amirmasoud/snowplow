---
title: >-
    شمارهٔ ۸۶
---
# شمارهٔ ۸۶

<div class="b" id="bn1"><div class="m1"><p>اگر رای رحمت شود با دلم</p></div>
<div class="m2"><p>دمی بو که بی‌زای زحمت زید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مگس را کند در زمان نامزد</p></div>
<div class="m2"><p>که تا بر سر رای رحمت رید</p></div></div>