---
title: >-
    شمارهٔ ۲۰۵
---
# شمارهٔ ۲۰۵

<div class="b" id="bn1"><div class="m1"><p>در شهر مرد نیست ز من نابکارتر</p></div>
<div class="m2"><p>مادر پسر نزاد ز من خاکسارتر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مغ با مغان به طوع ز من راست‌گوی تر</p></div>
<div class="m2"><p>سگ با سگان به طبع ز من سازگارتر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از مغ هزار بار منم زشت کیش‌تر</p></div>
<div class="m2"><p>وز سگ هزار بار منم زشت کارتر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر چند دانم این به یقین کز همه جهان</p></div>
<div class="m2"><p>کس راز حال من نبود کارزارتر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اینست جای شکر که در موقف جلال</p></div>
<div class="m2"><p>نومیدتر کسی بود امیدوارتر</p></div></div>