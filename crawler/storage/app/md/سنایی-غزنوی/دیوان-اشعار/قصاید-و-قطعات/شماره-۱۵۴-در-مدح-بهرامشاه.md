---
title: >-
    شمارهٔ ۱۵۴ - در مدح بهرامشاه
---
# شمارهٔ ۱۵۴ - در مدح بهرامشاه

<div class="b" id="bn1"><div class="m1"><p>خواجه سلام علیک کو لب چون نوش او</p></div>
<div class="m2"><p>پستهٔ دربار او لعل گهر پوش او</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کی به اشارت ز دور چشم ببیند لبش</p></div>
<div class="m2"><p>زان که نداند همی شکل لبش هوش او</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چشم کجا بیندش از ره صورت از آنک</p></div>
<div class="m2"><p>هست نهان جای عقل در لب خاموش او</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جای فرشتست و دیو چشم قوی خشم او</p></div>
<div class="m2"><p>حجلهٔ عقلست و جان گوش سخن کوش او</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گشت پر از ابرویم چشم جهانی از آنک</p></div>
<div class="m2"><p>خرمن مهرست و ماه قند ز شب پوش او</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مایه قهرست و لطف ناوک دلدوز او</p></div>
<div class="m2"><p>پایهٔ کفرست و دین جوشن و شب پوش او</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از سر شوخی و ناز برکشد او چشم تو</p></div>
<div class="m2"><p>گر تو ز زور و دروغ بر نکشی گوش او</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دی چو سناییش دید نیک بر بندگیش</p></div>
<div class="m2"><p>تا به ابد مانده گیر غاشیه بر دوش او</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در هوس هجر او دوزخیانند خلق</p></div>
<div class="m2"><p>شاه بهشتست و بس از بر و آغوش او</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سلطان بهرامشاه آنکه بود روز صید</p></div>
<div class="m2"><p>کرکس و شیر فلک پشه و خرگوش او</p></div></div>