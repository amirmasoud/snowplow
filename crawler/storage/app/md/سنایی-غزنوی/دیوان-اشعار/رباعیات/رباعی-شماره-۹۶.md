---
title: >-
    رباعی شمارهٔ ۹۶
---
# رباعی شمارهٔ ۹۶

<div class="b" id="bn1"><div class="m1"><p>ای دیدهٔ روشن سنایی ز غمت</p></div>
<div class="m2"><p>تاریک شد این دو روشنایی ز غمت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با این همه یک ساعت و یک لحظه مباد</p></div>
<div class="m2"><p>این جان و دل مرا جدایی ز غمت</p></div></div>