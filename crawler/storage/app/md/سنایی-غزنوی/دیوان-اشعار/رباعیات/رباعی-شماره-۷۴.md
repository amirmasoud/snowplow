---
title: >-
    رباعی شمارهٔ ۷۴
---
# رباعی شمارهٔ ۷۴

<div class="b" id="bn1"><div class="m1"><p>آنکس که سرت برید غمخوار تو اوست</p></div>
<div class="m2"><p>وان کت کلهی نهاد طرار تو اوست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آنکس که ترا بار دهد بار تو اوست</p></div>
<div class="m2"><p>وآنکس که ترا بی تو کند یار تو اوست</p></div></div>