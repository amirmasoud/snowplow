---
title: >-
    رباعی شمارهٔ ۱۴۰
---
# رباعی شمارهٔ ۱۴۰

<div class="b" id="bn1"><div class="m1"><p>چون پوست کشد کارد به دندان گیرد</p></div>
<div class="m2"><p>آهن ز لبش قیمت مرجان گیرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>او کارد به دست خویش میزان گیرد</p></div>
<div class="m2"><p>تا جان گیرد هر آنچه با جان گیرد</p></div></div>