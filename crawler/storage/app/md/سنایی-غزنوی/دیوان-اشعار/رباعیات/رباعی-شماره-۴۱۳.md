---
title: >-
    رباعی شمارهٔ ۴۱۳
---
# رباعی شمارهٔ ۴۱۳

<div class="b" id="bn1"><div class="m1"><p>تا مخرقه و راندهٔ هر در نشوی</p></div>
<div class="m2"><p>نزد همه کس چو کفر و کافر نشوی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حقا که بدین حدیث همسر نشوی</p></div>
<div class="m2"><p>تا هر چه کمست ازو تو کمتر نشوی</p></div></div>