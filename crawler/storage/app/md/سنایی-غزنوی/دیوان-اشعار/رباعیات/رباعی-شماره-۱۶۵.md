---
title: >-
    رباعی شمارهٔ ۱۶۵
---
# رباعی شمارهٔ ۱۶۵

<div class="b" id="bn1"><div class="m1"><p>عشق و غم تو اگر چه بی‌دادانند</p></div>
<div class="m2"><p>جان و دل من زهر دو آبادانند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نبود عجب ار ز یکدیگر شادانند</p></div>
<div class="m2"><p>چون جان من و عشق تو همزادانند</p></div></div>