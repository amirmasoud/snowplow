---
title: >-
    رباعی شمارهٔ ۲۱۸
---
# رباعی شمارهٔ ۲۱۸

<div class="b" id="bn1"><div class="m1"><p>ای تن وطن بلای آن دلکش باش</p></div>
<div class="m2"><p>ای جان ز غمش همیشه در آتش باش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای دیده به زیر پای او مفرش باش</p></div>
<div class="m2"><p>ای دل نه همه وصال باشد خوش باش</p></div></div>