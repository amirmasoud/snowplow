---
title: >-
    رباعی شمارهٔ ۳۱۵
---
# رباعی شمارهٔ ۳۱۵

<div class="b" id="bn1"><div class="m1"><p>گه بردوزی به دامنم بر دامن</p></div>
<div class="m2"><p>گه نگذاری که گردمت پیرامن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گه دوست همی شماریم گه دشمن</p></div>
<div class="m2"><p>تا من کیم از تو ای دریغا تو به من</p></div></div>