---
title: >-
    رباعی شمارهٔ ۲۴
---
# رباعی شمارهٔ ۲۴

<div class="b" id="bn1"><div class="m1"><p>از روی تو و زلف تو روز آمد و شب</p></div>
<div class="m2"><p>ای روز و شب تو روز و شب کرده عجب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا عشق مرا روز و شبت هست سبب</p></div>
<div class="m2"><p>چون روز و شبت کنم شب و روز طلب</p></div></div>