---
title: >-
    رباعی شمارهٔ ۳۳
---
# رباعی شمارهٔ ۳۳

<div class="b" id="bn1"><div class="m1"><p>ای خواجه محمد ای محامد سیرت</p></div>
<div class="m2"><p>ای در خور تاج هر دو هم نام و سرت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پیدا به شما دو تن سه اصل فطرت</p></div>
<div class="m2"><p>ز آن روی سخا از تو و علم از پدرت</p></div></div>