---
title: >-
    رباعی شمارهٔ ۱۲۶
---
# رباعی شمارهٔ ۱۲۶

<div class="b" id="bn1"><div class="m1"><p>من چون تو نیابم تو چو من یابی صد</p></div>
<div class="m2"><p>پس چون کنمت بگفت هر ناکس زد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کودک نیم این مایه شناسم بخرد</p></div>
<div class="m2"><p>پای از سر و آب از آتش و نیک از بد</p></div></div>