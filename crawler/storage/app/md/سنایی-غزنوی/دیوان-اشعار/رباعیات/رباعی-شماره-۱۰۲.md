---
title: >-
    رباعی شمارهٔ ۱۰۲
---
# رباعی شمارهٔ ۱۰۲

<div class="b" id="bn1"><div class="m1"><p>آتش در زن ز کبریا در کویت</p></div>
<div class="m2"><p>تا ره نبرد هیچ فضولی سویت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن روی نکو ز ما بپوش از مویت</p></div>
<div class="m2"><p>زیرا که به ما دریغ باشد رویت</p></div></div>