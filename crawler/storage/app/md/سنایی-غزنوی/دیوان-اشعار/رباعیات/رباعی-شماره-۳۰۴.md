---
title: >-
    رباعی شمارهٔ ۳۰۴
---
# رباعی شمارهٔ ۳۰۴

<div class="b" id="bn1"><div class="m1"><p>عقلی که خلاف تو گزیدن نتوان</p></div>
<div class="m2"><p>دینی که ز شرط تو بریدن نتوان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وهمی که به ذات تو رسیدن نتوان</p></div>
<div class="m2"><p>دهری که ز دام تو رهیدن نتوان</p></div></div>