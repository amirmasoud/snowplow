---
title: >-
    رباعی شمارهٔ ۲۹۳
---
# رباعی شمارهٔ ۲۹۳

<div class="b" id="bn1"><div class="m1"><p>ما شربت هجر تو چشیدیم و شدیم</p></div>
<div class="m2"><p>هجران تو بر وصل گزیدیم و شدیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در جستن وصل تو ز نایافتنت</p></div>
<div class="m2"><p>دل رفت و طمع ز جان بریدیم و شدیم</p></div></div>