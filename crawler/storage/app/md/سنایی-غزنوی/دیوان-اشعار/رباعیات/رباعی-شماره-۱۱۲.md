---
title: >-
    رباعی شمارهٔ ۱۱۲
---
# رباعی شمارهٔ ۱۱۲

<div class="b" id="bn1"><div class="m1"><p>گردی که ز دیوار تو برباید باد</p></div>
<div class="m2"><p>جز در چشمم از آن نشان نتوان داد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای در غم تو طبع خردمندان شاد</p></div>
<div class="m2"><p>هر کو به تو شاد نیست شادیش مباد</p></div></div>