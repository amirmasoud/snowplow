---
title: >-
    رباعی شمارهٔ ۱۹۸
---
# رباعی شمارهٔ ۱۹۸

<div class="b" id="bn1"><div class="m1"><p>هرگز دل من به آشکارا و به راز</p></div>
<div class="m2"><p>با مردم بی خرد نباشد دمساز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من یار عیار خواهم و خاک انداز</p></div>
<div class="m2"><p>کورا نشود ز عالمی دیده فراز</p></div></div>