---
title: >-
    رباعی شمارهٔ ۳۱۲
---
# رباعی شمارهٔ ۳۱۲

<div class="b" id="bn1"><div class="m1"><p>گر شاد نخواهی این دلم شاد مکن</p></div>
<div class="m2"><p>ور یاد نیایدت ز من یاد مکن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>لیکن به وفا بر تو که این خسته دلم</p></div>
<div class="m2"><p>از بند غم عشق خود آزاد مکن</p></div></div>