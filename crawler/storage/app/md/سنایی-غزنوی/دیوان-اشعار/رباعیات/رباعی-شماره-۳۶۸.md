---
title: >-
    رباعی شمارهٔ ۳۶۸
---
# رباعی شمارهٔ ۳۶۸

<div class="b" id="bn1"><div class="m1"><p>چون نار اگرم فروختن فرمایی</p></div>
<div class="m2"><p>چون باد بزان شوم ز ناپروایی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زیر قدم خود ار چو خاکم سایی</p></div>
<div class="m2"><p>چون آب روانه گردم از مولایی</p></div></div>