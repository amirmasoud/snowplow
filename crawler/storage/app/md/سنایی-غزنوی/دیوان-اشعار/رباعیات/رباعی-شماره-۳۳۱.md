---
title: >-
    رباعی شمارهٔ ۳۳۱
---
# رباعی شمارهٔ ۳۳۱

<div class="b" id="bn1"><div class="m1"><p>دلها همه آب گشت و جانها همه خون</p></div>
<div class="m2"><p>تا چیست حقیقت از پس پرده و چون</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای بر علمت خرد رد و گردون دون</p></div>
<div class="m2"><p>از تو دو جهان پر و تو از هر دو برون</p></div></div>