---
title: >-
    رباعی شمارهٔ ۱۱۵
---
# رباعی شمارهٔ ۱۱۵

<div class="b" id="bn1"><div class="m1"><p>آن را شایی که باشم از عشق تو شاد</p></div>
<div class="m2"><p>و آن را شایم که از منت ناید یاد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با این همه چشم زخم ای حورنژاد</p></div>
<div class="m2"><p>در راه تو بنده با خود و بی خود باد</p></div></div>