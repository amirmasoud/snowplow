---
title: >-
    رباعی شمارهٔ ۳۲۴
---
# رباعی شمارهٔ ۳۲۴

<div class="b" id="bn1"><div class="m1"><p>ای شاه چو لاله دارد از تو دشمن</p></div>
<div class="m2"><p>دل تیره و چاک دامن و خاک وطن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون چرخ چراست خصمت ای گرد افگن</p></div>
<div class="m2"><p>نالنده و گردان و رسن در گردن</p></div></div>