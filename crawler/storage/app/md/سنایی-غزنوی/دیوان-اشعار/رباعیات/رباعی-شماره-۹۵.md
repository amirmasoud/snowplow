---
title: >-
    رباعی شمارهٔ ۹۵
---
# رباعی شمارهٔ ۹۵

<div class="b" id="bn1"><div class="m1"><p>در خاک بجستمت چو خور یافتمت</p></div>
<div class="m2"><p>بسیار عزیزتر ز زر یافتمت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جایی اگر امروز خبر یافتمت</p></div>
<div class="m2"><p>جان تو که نیک عشوه گر یافتمت</p></div></div>