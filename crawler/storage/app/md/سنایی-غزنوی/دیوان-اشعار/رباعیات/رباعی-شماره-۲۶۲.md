---
title: >-
    رباعی شمارهٔ ۲۶۲
---
# رباعی شمارهٔ ۲۶۲

<div class="b" id="bn1"><div class="m1"><p>هر بار ز دیده از تو در تیمارم</p></div>
<div class="m2"><p>تا بهره ز دیدار تو چون بردارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای یار چو ماه اگر دهی دیدارم</p></div>
<div class="m2"><p>چون چرخ هزار دیده در وی دارم</p></div></div>