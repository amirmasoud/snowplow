---
title: >-
    رباعی شمارهٔ ۵۸
---
# رباعی شمارهٔ ۵۸

<div class="b" id="bn1"><div class="m1"><p>هر روز مرا با تو نیازی دگرست</p></div>
<div class="m2"><p>با دو لب نوشین تو رازی دگرست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر روز ترا طریق و سازی دگرست</p></div>
<div class="m2"><p>جنگی دگر و عتاب و نازی دگرست</p></div></div>