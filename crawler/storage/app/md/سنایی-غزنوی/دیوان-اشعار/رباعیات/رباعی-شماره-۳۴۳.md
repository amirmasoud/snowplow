---
title: >-
    رباعی شمارهٔ ۳۴۳
---
# رباعی شمارهٔ ۳۴۳

<div class="b" id="bn1"><div class="m1"><p>چون موی شدم ز رشک پیراهن تو</p></div>
<div class="m2"><p>وز رشک گریبان تو و دامن تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کاین بوسه همی دهد قدمهای ترا</p></div>
<div class="m2"><p>وآنرا شب و روز دست در گردن تو</p></div></div>