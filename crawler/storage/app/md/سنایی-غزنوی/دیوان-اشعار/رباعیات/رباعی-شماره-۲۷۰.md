---
title: >-
    رباعی شمارهٔ ۲۷۰
---
# رباعی شمارهٔ ۲۷۰

<div class="b" id="bn1"><div class="m1"><p>روز آمد و برکشید خورشید علم</p></div>
<div class="m2"><p>شب کرد ازو هزیمت و برد حشم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گویی ز میان آن دو زلفین به خم</p></div>
<div class="m2"><p>پیدا کردند روی آن شهره صنم</p></div></div>