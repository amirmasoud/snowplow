---
title: >-
    رباعی شمارهٔ ۳۰۸
---
# رباعی شمارهٔ ۳۰۸

<div class="b" id="bn1"><div class="m1"><p>اندر دریا نهنگ باید بودن</p></div>
<div class="m2"><p>واندر صحرا پلنگ باید بودن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مردانه و مرد رنگ باید بودن</p></div>
<div class="m2"><p>ورنه به هزار ننگ باید بودن</p></div></div>