---
title: >-
    رباعی شمارهٔ ۱۸۴
---
# رباعی شمارهٔ ۱۸۴

<div class="b" id="bn1"><div class="m1"><p>اکنون که سیاهی ای دل چون خورشید</p></div>
<div class="m2"><p>بیشت باید ز عشق من داد نوید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کاندر چشمی تو از عزیزی جاوید</p></div>
<div class="m2"><p>چون دیدهٔ دیده‌ای سیه به که سفید</p></div></div>