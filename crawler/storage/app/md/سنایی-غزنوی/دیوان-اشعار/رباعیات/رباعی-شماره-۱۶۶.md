---
title: >-
    رباعی شمارهٔ ۱۶۶
---
# رباعی شمارهٔ ۱۶۶

<div class="b" id="bn1"><div class="m1"><p>آنها که اسیر عشق دلدارانند</p></div>
<div class="m2"><p>از دست فلک همیشه خونبارانند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هرگز نشود بخت بد از عشق جدا</p></div>
<div class="m2"><p>بدبختی و عاشقی مگر یارانند</p></div></div>