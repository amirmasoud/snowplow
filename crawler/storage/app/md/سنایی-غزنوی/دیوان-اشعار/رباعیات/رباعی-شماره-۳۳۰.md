---
title: >-
    رباعی شمارهٔ ۳۳۰
---
# رباعی شمارهٔ ۳۳۰

<div class="b" id="bn1"><div class="m1"><p>از عشوهٔ چرخ در امانم ز تو من</p></div>
<div class="m2"><p>و آزاد ز بند این و آنم ز تو من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر چند ز غم جامه‌درانم ز تو من</p></div>
<div class="m2"><p>والله که نمانم ار بمانم ز تو من</p></div></div>