---
title: >-
    رباعی شمارهٔ ۳۸۵
---
# رباعی شمارهٔ ۳۸۵

<div class="b" id="bn1"><div class="m1"><p>ای رفته و دل برده چنین نپسندی</p></div>
<div class="m2"><p>من می‌گریم ز درد و تو می‌خندی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نشگفت که ببریدی و دل برکندی</p></div>
<div class="m2"><p>تو هندویی و برنده باشد هندی</p></div></div>