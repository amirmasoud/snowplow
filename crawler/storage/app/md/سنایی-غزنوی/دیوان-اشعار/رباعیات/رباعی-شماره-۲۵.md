---
title: >-
    رباعی شمارهٔ ۲۵
---
# رباعی شمارهٔ ۲۵

<div class="b" id="bn1"><div class="m1"><p>تا دیده‌ام آن سیب خوش دوست فریب</p></div>
<div class="m2"><p>کو بر لب نوشین تو می‌زد آسیب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اندیشهٔ آن خود از دلم برد شکیب</p></div>
<div class="m2"><p>تا از چه گرفت جای شفتالو سیب</p></div></div>