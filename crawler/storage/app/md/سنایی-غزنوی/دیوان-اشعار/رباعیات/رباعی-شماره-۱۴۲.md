---
title: >-
    رباعی شمارهٔ ۱۴۲
---
# رباعی شمارهٔ ۱۴۲

<div class="b" id="bn1"><div class="m1"><p>گبری که گرسنه شد به نانی ارزد</p></div>
<div class="m2"><p>سگ زان تو شد به استخوانی ارزد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اظهار نهانی به جهانی ارزد</p></div>
<div class="m2"><p>آسایش زندگی به جانی ارزد</p></div></div>