---
title: >-
    رباعی شمارهٔ ۳۲۰
---
# رباعی شمارهٔ ۳۲۰

<div class="b" id="bn1"><div class="m1"><p>پندی دهمت اگر پذیری ای تن</p></div>
<div class="m2"><p>تا سور ترا به دل نگردد شیون</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عضوی ز تو گر صلح کند با دشمن</p></div>
<div class="m2"><p>دشمن دو شمر تیغ دو کش زخم دو زن</p></div></div>