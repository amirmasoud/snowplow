---
title: >-
    رباعی شمارهٔ ۲۵۲
---
# رباعی شمارهٔ ۲۵۲

<div class="b" id="bn1"><div class="m1"><p>ناید به کف آن زلف سمن مال به مال</p></div>
<div class="m2"><p>نی رقص کند بر آن رخان خال به خال</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای چون گل نو که بینمت سال به سال</p></div>
<div class="m2"><p>گردنده چو روزگاری از حال به حال</p></div></div>