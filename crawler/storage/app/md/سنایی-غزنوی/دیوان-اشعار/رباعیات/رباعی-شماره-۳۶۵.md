---
title: >-
    رباعی شمارهٔ ۳۶۵
---
# رباعی شمارهٔ ۳۶۵

<div class="b" id="bn1"><div class="m1"><p>هر چند به دلبری کنون آمده‌ای</p></div>
<div class="m2"><p>در بردن دل تو ذوفنون آمده‌ای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آلوده همه جامه به خون آمده‌ای</p></div>
<div class="m2"><p>گویی که ز چشم من برون آمده‌ای</p></div></div>