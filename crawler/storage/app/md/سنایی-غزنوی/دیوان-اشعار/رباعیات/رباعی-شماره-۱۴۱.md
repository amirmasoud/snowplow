---
title: >-
    رباعی شمارهٔ ۱۴۱
---
# رباعی شمارهٔ ۱۴۱

<div class="b" id="bn1"><div class="m1"><p>این اسب قلندری نه هر کس تازد</p></div>
<div class="m2"><p>وین مهرهٔ نیستی نه هر کس بازد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مردی باید که جان برون اندازد</p></div>
<div class="m2"><p>چون جان بشود عشق ترا جان سازد</p></div></div>