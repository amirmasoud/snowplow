---
title: >-
    رباعی شمارهٔ ۲۵۴
---
# رباعی شمارهٔ ۲۵۴

<div class="b" id="bn1"><div class="m1"><p>ای عهد تو عهد دوستان سر پل</p></div>
<div class="m2"><p>از وصل تو هجر خیزد از عز تو دل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پر مشغله و میان تهی همچو دهل</p></div>
<div class="m2"><p>ای یک شبه همچو شمع و یک روزه چو گل</p></div></div>