---
title: >-
    رباعی شمارهٔ ۱۴۴
---
# رباعی شمارهٔ ۱۴۴

<div class="b" id="bn1"><div class="m1"><p>ای آنکه برت مردم بد، دد باشد</p></div>
<div class="m2"><p>وز نیکی تو یک هنرت صد باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دانی تو و آنکه چون تو بخرد باشد</p></div>
<div class="m2"><p>گر مردم نیک بد کند بد باشد</p></div></div>