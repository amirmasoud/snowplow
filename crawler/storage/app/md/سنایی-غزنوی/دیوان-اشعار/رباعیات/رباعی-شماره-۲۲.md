---
title: >-
    رباعی شمارهٔ ۲۲
---
# رباعی شمارهٔ ۲۲

<div class="b" id="bn1"><div class="m1"><p>نیلوفر و لاله هر دو بی‌هیچ سبب</p></div>
<div class="m2"><p>این پوشد نیل و آن به خون شوید لب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می‌شویم و می‌پوشم ای نوشین لب</p></div>
<div class="m2"><p>در هجر تو رخ به خوان و از نیل سلب</p></div></div>