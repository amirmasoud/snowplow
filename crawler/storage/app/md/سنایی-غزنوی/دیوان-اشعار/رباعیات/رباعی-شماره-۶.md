---
title: >-
    رباعی شمارهٔ ۶
---
# رباعی شمارهٔ ۶

<div class="b" id="bn1"><div class="m1"><p>هر چند بسوختی به هر باب مرا</p></div>
<div class="m2"><p>چون می‌ندهد آب تو پایاب مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زین بیش مکن به خیره در تاب مرا</p></div>
<div class="m2"><p>دریافت مرا غم تو، دریاب مرا</p></div></div>