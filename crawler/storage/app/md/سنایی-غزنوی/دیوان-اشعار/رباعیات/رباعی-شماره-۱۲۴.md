---
title: >-
    رباعی شمارهٔ ۱۲۴
---
# رباعی شمارهٔ ۱۲۴

<div class="b" id="bn1"><div class="m1"><p>ای صورت تو سکون دلها چو خرد</p></div>
<div class="m2"><p>وی سیرت تو منزه از خصلت بد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دارم ز پی عشق تو یک انده صد</p></div>
<div class="m2"><p>از بیم تو هیچ دم نمی‌یارم زد</p></div></div>