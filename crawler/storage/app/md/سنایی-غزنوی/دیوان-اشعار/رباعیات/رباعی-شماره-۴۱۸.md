---
title: >-
    رباعی شمارهٔ ۴۱۸
---
# رباعی شمارهٔ ۴۱۸

<div class="b" id="bn1"><div class="m1"><p>تا کی ز غم جهان امانی خواهی</p></div>
<div class="m2"><p>تا کی به مراد خود جهانی خواهی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون در خور خویشتن تمنا نکنی</p></div>
<div class="m2"><p>زین مسجد و زان میکده نانی خواهی</p></div></div>