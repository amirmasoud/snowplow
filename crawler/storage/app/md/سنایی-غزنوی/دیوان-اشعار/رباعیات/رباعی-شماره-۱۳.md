---
title: >-
    رباعی شمارهٔ ۱۳
---
# رباعی شمارهٔ ۱۳

<div class="b" id="bn1"><div class="m1"><p>در دل کردی قصد بداندیشی ما</p></div>
<div class="m2"><p>ظاهر کردی عیب کمابیشی ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای جسته به اختیار خود خویشی ما</p></div>
<div class="m2"><p>بگرفت ملالتت ز درویشی ما</p></div></div>