---
title: >-
    رباعی شمارهٔ ۳۹۵
---
# رباعی شمارهٔ ۳۹۵

<div class="b" id="bn1"><div class="m1"><p>چون بلبل داریم برای بازی</p></div>
<div class="m2"><p>چون گل که ببوییم برون اندازی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شمعم که چو برفروزیم بگدازی</p></div>
<div class="m2"><p>چنگم که ز بهر زدنم می‌سازی</p></div></div>