---
title: >-
    رباعی شمارهٔ ۶۳
---
# رباعی شمارهٔ ۶۳

<div class="b" id="bn1"><div class="m1"><p>تا این دل من همیشه عشق اندیش‌ست</p></div>
<div class="m2"><p>هر روز مرا تازه بلایی پیش ست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عیبم مکنید اگر دل من ریش‌ست</p></div>
<div class="m2"><p>کز عشق مراد خانه ویران بیشست</p></div></div>