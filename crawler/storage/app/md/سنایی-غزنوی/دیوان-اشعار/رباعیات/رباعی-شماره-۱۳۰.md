---
title: >-
    رباعی شمارهٔ ۱۳۰
---
# رباعی شمارهٔ ۱۳۰

<div class="b" id="bn1"><div class="m1"><p>از دور مرا بدید لب خندان کرد</p></div>
<div class="m2"><p>و آن روی چو مه به یاسمین پنهان کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن جان جهان کرشمهٔ خوبان کرد</p></div>
<div class="m2"><p>ور نه به قصب ماه نهان نتوان کرد</p></div></div>