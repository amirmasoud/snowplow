---
title: >-
    رباعی شمارهٔ ۱۱۷
---
# رباعی شمارهٔ ۱۱۷

<div class="b" id="bn1"><div class="m1"><p>ما را به جز از تو عالم افروز مباد</p></div>
<div class="m2"><p>بر ما سپه هجر تو پیروز مباد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اندر دل ما ز هجر تو سوز مباد</p></div>
<div class="m2"><p>چون با تو شدم بی‌تو مرا روز مباد</p></div></div>