---
title: >-
    رباعی شمارهٔ ۲۴۰
---
# رباعی شمارهٔ ۲۴۰

<div class="b" id="bn1"><div class="m1"><p>بر سین سریر سر سپاه آمد عشق</p></div>
<div class="m2"><p>بر میم ملوک پادشاه آمد عشق</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر کاف کمال کل، کلاه آمد عشق</p></div>
<div class="m2"><p>با اینهمه یک قدم ز راه آمد عشق</p></div></div>