---
title: >-
    رباعی شمارهٔ ۳۶۹
---
# رباعی شمارهٔ ۳۶۹

<div class="b" id="bn1"><div class="m1"><p>گفتم که ببرم از تو ای بینایی</p></div>
<div class="m2"><p>گفتی که بمیر تا دلت بربایی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتار ترا به آزمایش کردم</p></div>
<div class="m2"><p>می بشکیبم کنون چه میفرمایی</p></div></div>