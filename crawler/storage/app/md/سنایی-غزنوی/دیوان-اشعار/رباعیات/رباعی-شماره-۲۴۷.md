---
title: >-
    رباعی شمارهٔ ۲۴۷
---
# رباعی شمارهٔ ۲۴۷

<div class="b" id="bn1"><div class="m1"><p>خورشید سما بسوزد از سایهٔ عشق</p></div>
<div class="m2"><p>پس چون شده‌ای دلا تو همسایهٔ عشق</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جز آتش عشق نیست پیرایهٔ عشق</p></div>
<div class="m2"><p>اینست بتا مایه و سرمایهٔ عشق</p></div></div>