---
title: >-
    رباعی شمارهٔ ۲۰۷
---
# رباعی شمارهٔ ۲۰۷

<div class="b" id="bn1"><div class="m1"><p>آسیمه سران بی‌نواییم هنوز</p></div>
<div class="m2"><p>با شهوتها و با هواییم هنوز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زین هر دو پی هم بگراییم هنوز</p></div>
<div class="m2"><p>از دوست بدین سبب جداییم هنوز</p></div></div>