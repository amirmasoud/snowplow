---
title: >-
    رباعی شمارهٔ ۳۹۳
---
# رباعی شمارهٔ ۳۹۳

<div class="b" id="bn1"><div class="m1"><p>راهی که به اندیشهٔ دل می‌سپری</p></div>
<div class="m2"><p>خواهی که به هر دو عالم اندر نگری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در سرت همیشه سیرت گردون دار</p></div>
<div class="m2"><p>کانجا که همی ترسی ازو می‌گذری</p></div></div>