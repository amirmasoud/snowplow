---
title: >-
    رباعی شمارهٔ ۳۸۸
---
# رباعی شمارهٔ ۳۸۸

<div class="b" id="bn1"><div class="m1"><p>زان چشم چو نرگس که به من در نگری</p></div>
<div class="m2"><p>چون نرگس تیر ماه خوابم ببری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نرگس چشمی چو نرگس ای رشک پری</p></div>
<div class="m2"><p>هر چند شکفته‌تر شوی شوخ‌تری</p></div></div>