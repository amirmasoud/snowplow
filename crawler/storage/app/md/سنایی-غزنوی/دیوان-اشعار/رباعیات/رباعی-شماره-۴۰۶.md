---
title: >-
    رباعی شمارهٔ ۴۰۶
---
# رباعی شمارهٔ ۴۰۶

<div class="b" id="bn1"><div class="m1"><p>گر من سر ناز هر خسی داشتمی</p></div>
<div class="m2"><p>معشوقه درین شهر بسی داشتمی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ور بر دل خود دست رسی داشتمی</p></div>
<div class="m2"><p>در هر نفسی همنفسی داشتمی</p></div></div>