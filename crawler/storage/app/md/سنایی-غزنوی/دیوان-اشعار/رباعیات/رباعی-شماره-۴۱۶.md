---
title: >-
    رباعی شمارهٔ ۴۱۶
---
# رباعی شمارهٔ ۴۱۶

<div class="b" id="bn1"><div class="m1"><p>با هر تاری سوخته چون پود شوی</p></div>
<div class="m2"><p>یا جمله همه زیان بی سود شوی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در دیدهٔ عهد دوستان دود شوی</p></div>
<div class="m2"><p>زینگونه به کام دشمنان زود شوی</p></div></div>