---
title: >-
    رباعی شمارهٔ ۳۹۴
---
# رباعی شمارهٔ ۳۹۴

<div class="b" id="bn1"><div class="m1"><p>هست از دم من همیشه چرخ اندر دی</p></div>
<div class="m2"><p>وز شرم جمالت آفتاب اندر خوی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر روز چو مه به منزلی داری پی</p></div>
<div class="m2"><p>آخر چو ستاره شوخ چشمی تا کی</p></div></div>