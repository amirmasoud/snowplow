---
title: >-
    رباعی شمارهٔ ۹۴
---
# رباعی شمارهٔ ۹۴

<div class="b" id="bn1"><div class="m1"><p>تا کی باشم با غم هجران تو جفت</p></div>
<div class="m2"><p>زرقیست حدیثان تو پیدا و نهفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون از تو نخواهدم گل و مل بشکفت</p></div>
<div class="m2"><p>دست از تو بشستم و به ترک تو گفت</p></div></div>