---
title: >-
    رباعی شمارهٔ ۳۶۳
---
# رباعی شمارهٔ ۳۶۳

<div class="b" id="bn1"><div class="m1"><p>ای آنکه تو رحمت خدایی شده‌ای</p></div>
<div class="m2"><p>در چشم بجای روشنایی شده‌ای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از رندی سوی پارسایی شده‌ای</p></div>
<div class="m2"><p>اندر خور صحبت سنایی شده‌ای</p></div></div>