---
title: >-
    رباعی شمارهٔ ۴۱۵
---
# رباعی شمارهٔ ۴۱۵

<div class="b" id="bn1"><div class="m1"><p>گیرم که مقدم مقالات شوی</p></div>
<div class="m2"><p>پیش شمن صفات خود لات شوی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جز جمع مباش تا مگر ذات شوی</p></div>
<div class="m2"><p>کانگه که پراکنده شوی مات شوی</p></div></div>