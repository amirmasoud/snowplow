---
title: >-
    رباعی شمارهٔ ۲۲۲
---
# رباعی شمارهٔ ۲۲۲

<div class="b" id="bn1"><div class="m1"><p>بر طرف قمر نهاده مشک و شکرش</p></div>
<div class="m2"><p>چکند که فقاع خوش نبندد به درش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در کعبهٔ حسن گشت و در پیش درش</p></div>
<div class="m2"><p>عشاق همه بوسه‌زنان بر حجرش</p></div></div>