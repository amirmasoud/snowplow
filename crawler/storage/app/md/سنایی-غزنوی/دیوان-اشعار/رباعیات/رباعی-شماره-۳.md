---
title: >-
    رباعی شمارهٔ ۳
---
# رباعی شمارهٔ ۳

<div class="b" id="bn1"><div class="m1"><p>عشقا تو در آتشی نهادی ما را</p></div>
<div class="m2"><p>درهای بلا همه گشادی ما را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صبرا به تو در گریختم تا چکنی</p></div>
<div class="m2"><p>تو نیز به دست هجر دادی ما را</p></div></div>