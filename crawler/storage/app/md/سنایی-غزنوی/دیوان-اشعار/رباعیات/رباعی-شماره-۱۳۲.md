---
title: >-
    رباعی شمارهٔ ۱۳۲
---
# رباعی شمارهٔ ۱۳۲

<div class="b" id="bn1"><div class="m1"><p>روزی که سر از پرده برون خواهی کرد</p></div>
<div class="m2"><p>آنروز زمانه را زبون خواهی کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر حسن و جمال ازین فزون خواهی کرد</p></div>
<div class="m2"><p>یارب چه جگرهاست که خون خواهی کرد</p></div></div>