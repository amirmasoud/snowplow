---
title: >-
    رباعی شمارهٔ ۴۷
---
# رباعی شمارهٔ ۴۷

<div class="b" id="bn1"><div class="m1"><p>ای چون گل و مل در به در و دست به دست</p></div>
<div class="m2"><p>هر جا ز تو خرمی و هر کس ز تو مست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آنرا که شبی با تو بود خاست و نشست</p></div>
<div class="m2"><p>جز خار و خمار از تو چه برداند بست</p></div></div>