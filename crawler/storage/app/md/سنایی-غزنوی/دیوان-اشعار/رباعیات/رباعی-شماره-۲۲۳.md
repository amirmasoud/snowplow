---
title: >-
    رباعی شمارهٔ ۲۲۳
---
# رباعی شمارهٔ ۲۲۳

<div class="b" id="bn1"><div class="m1"><p>چون نزد رهی درآیی ای دلبر کش</p></div>
<div class="m2"><p>پیراهن چرب را تو از تن درکش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زیرا که چو گیرمت به شادی در کش</p></div>
<div class="m2"><p>در پیرهن چرب تو افتد آتش</p></div></div>