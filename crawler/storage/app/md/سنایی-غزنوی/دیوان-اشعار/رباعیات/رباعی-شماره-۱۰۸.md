---
title: >-
    رباعی شمارهٔ ۱۰۸
---
# رباعی شمارهٔ ۱۰۸

<div class="b" id="bn1"><div class="m1"><p>زلفینانت همیشه خم در خم باد</p></div>
<div class="m2"><p>واندوهانت همیشه دم در دم باد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شادان به غم منی غمم بر غم باد</p></div>
<div class="m2"><p>عشقی که به صد بلا کم آید کم باد</p></div></div>