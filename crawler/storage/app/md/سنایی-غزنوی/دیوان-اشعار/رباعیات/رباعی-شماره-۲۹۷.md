---
title: >-
    رباعی شمارهٔ ۲۹۷
---
# رباعی شمارهٔ ۲۹۷

<div class="b" id="bn1"><div class="m1"><p>مانندهٔ باد اگر چه بی‌پا و سریم</p></div>
<div class="m2"><p>پیوسته چو آتش ره بالا سپریم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زان پیش که رخت ما سوی خاک کشند</p></div>
<div class="m2"><p>ما خاک فروشیم و بدان آب خوریم</p></div></div>