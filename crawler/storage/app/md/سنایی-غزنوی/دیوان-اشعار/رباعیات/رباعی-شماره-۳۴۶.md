---
title: >-
    رباعی شمارهٔ ۳۴۶
---
# رباعی شمارهٔ ۳۴۶

<div class="b" id="bn1"><div class="m1"><p>ای همت صد هزار کس در پی تو</p></div>
<div class="m2"><p>وی رنگ گل و بوی گلاب از خوی تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای تعبیه جان عاشقان در پی تو</p></div>
<div class="m2"><p>ای من سر خویش کشته‌ام در پی تو</p></div></div>