---
title: >-
    رباعی شمارهٔ ۱۳۳
---
# رباعی شمارهٔ ۱۳۳

<div class="b" id="bn1"><div class="m1"><p>چون چهرهٔ تو ز گریه باشد پر درد</p></div>
<div class="m2"><p>زنهار به هیچ آبی آلوده مگرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اندر ره عاشقی چنان باید مرد</p></div>
<div class="m2"><p>کز دریا خشک آید از دوزخ سرد</p></div></div>