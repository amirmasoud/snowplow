---
title: >-
    رباعی شمارهٔ ۵۲
---
# رباعی شمارهٔ ۵۲

<div class="b" id="bn1"><div class="m1"><p>امروز ببر زانچه ترا پیوندست</p></div>
<div class="m2"><p>کانها همه بر جان تو فردا بندست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سودی طلب از عمر که سرمایهٔ عمر</p></div>
<div class="m2"><p>روزی چندست و کس نداند چندست</p></div></div>