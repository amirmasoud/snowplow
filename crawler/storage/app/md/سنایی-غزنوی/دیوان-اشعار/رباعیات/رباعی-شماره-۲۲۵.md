---
title: >-
    رباعی شمارهٔ ۲۲۵
---
# رباعی شمارهٔ ۲۲۵

<div class="b" id="bn1"><div class="m1"><p>با سینهٔ این و آن چه گویی غم خویش</p></div>
<div class="m2"><p>از دیدهٔ این و آن چه جویی نم خویش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر ساز تو عالمی ز بیش و کم خویش</p></div>
<div class="m2"><p>آنگاه بزی به ناز در عالم خویش</p></div></div>