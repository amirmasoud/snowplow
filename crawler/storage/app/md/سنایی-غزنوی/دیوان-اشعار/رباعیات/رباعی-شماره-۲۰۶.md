---
title: >-
    رباعی شمارهٔ ۲۰۶
---
# رباعی شمارهٔ ۲۰۶

<div class="b" id="bn1"><div class="m1"><p>ای گلبن نابسوده او باش هنوز</p></div>
<div class="m2"><p>وی رنگ تو نامیخته نقاش هنوز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بوی تو نکردست صبا فاش هنوز</p></div>
<div class="m2"><p>تا بر تو وزد باد صبا باش هنوز</p></div></div>