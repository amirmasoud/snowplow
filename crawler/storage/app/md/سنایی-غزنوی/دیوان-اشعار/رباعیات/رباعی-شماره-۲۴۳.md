---
title: >-
    رباعی شمارهٔ ۲۴۳
---
# رباعی شمارهٔ ۲۴۳

<div class="b" id="bn1"><div class="m1"><p>جز تیر بلا نبود در ترکش عشق</p></div>
<div class="m2"><p>جز مسند عشق نیست در مفرش عشق</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جز دست قضا نیست جنیبت کش عشق</p></div>
<div class="m2"><p>جان باید جان سپند بر آتش عشق</p></div></div>