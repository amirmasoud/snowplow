---
title: >-
    رباعی شمارهٔ ۵۰
---
# رباعی شمارهٔ ۵۰

<div class="b" id="bn1"><div class="m1"><p>گیرم که چو گل همه نکویی با تست</p></div>
<div class="m2"><p>چون بلبل راه خوبگویی با تست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون آینه خوی عیب جویی با تست</p></div>
<div class="m2"><p>چه سود که شیمت دورویی با تست</p></div></div>