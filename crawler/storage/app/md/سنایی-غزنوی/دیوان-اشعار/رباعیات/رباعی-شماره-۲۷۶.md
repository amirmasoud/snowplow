---
title: >-
    رباعی شمارهٔ ۲۷۶
---
# رباعی شمارهٔ ۲۷۶

<div class="b" id="bn1"><div class="m1"><p>پر شد ز شراب عشق جانا جامم</p></div>
<div class="m2"><p>چون زلف تو درهم زده شد ایامم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از عشق تو این نه بس مراد و کامم</p></div>
<div class="m2"><p>کز جملهٔ بندگان نویسی نامم</p></div></div>