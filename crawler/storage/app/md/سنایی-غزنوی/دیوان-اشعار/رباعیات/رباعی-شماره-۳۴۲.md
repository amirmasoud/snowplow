---
title: >-
    رباعی شمارهٔ ۳۴۲
---
# رباعی شمارهٔ ۳۴۲

<div class="b" id="bn1"><div class="m1"><p>ای بی تو دلیل اشهب و ادهم تو</p></div>
<div class="m2"><p>اقبال فرو شد که برآمد دم تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دیوانه شدست عقل در ماتم تو</p></div>
<div class="m2"><p>جان چیست که خون نگرید اندر غم تو</p></div></div>