---
title: >-
    رباعی شمارهٔ ۲۵۷
---
# رباعی شمارهٔ ۲۵۷

<div class="b" id="bn1"><div class="m1"><p>ای عمر عزیز داده بر باد ز جهل</p></div>
<div class="m2"><p>وز بی‌خبری کار اجل داشته سهل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اسباب دوصد ساله سگالنده ز پیش</p></div>
<div class="m2"><p>نایافته از زمانه یک ساعت مهل</p></div></div>