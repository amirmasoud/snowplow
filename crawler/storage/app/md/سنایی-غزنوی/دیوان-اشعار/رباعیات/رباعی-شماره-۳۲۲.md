---
title: >-
    رباعی شمارهٔ ۳۲۲
---
# رباعی شمارهٔ ۳۲۲

<div class="b" id="bn1"><div class="m1"><p>گر کرده بدی تو آزمون دل من</p></div>
<div class="m2"><p>دل بسته نداری تو بدون دل من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر آگاهی از اندرون دل من</p></div>
<div class="m2"><p>زینگونه نکوشی تو به خون دل من</p></div></div>