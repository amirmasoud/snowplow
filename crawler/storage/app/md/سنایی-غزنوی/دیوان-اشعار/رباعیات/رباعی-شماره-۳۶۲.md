---
title: >-
    رباعی شمارهٔ ۳۶۲
---
# رباعی شمارهٔ ۳۶۲

<div class="b" id="bn1"><div class="m1"><p>در جامه و فوطه سخت خرم شده‌ای</p></div>
<div class="m2"><p>کاشوب جهان و شور عالم شده‌ای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در خواب ندانم که چه دیدستی دوش</p></div>
<div class="m2"><p>کامروز چو نقش فوطه در هم شده‌ای</p></div></div>