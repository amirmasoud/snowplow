---
title: >-
    رباعی شمارهٔ ۱۳۶
---
# رباعی شمارهٔ ۱۳۶

<div class="b" id="bn1"><div class="m1"><p>رو گرد سراپردهٔ اسرار مگرد</p></div>
<div class="m2"><p>شوخی چکنی که نیستی مرد نبرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مردی باید زهر دو عالم شده فرد</p></div>
<div class="m2"><p>کو درد به جای آب و نان داند خورد</p></div></div>