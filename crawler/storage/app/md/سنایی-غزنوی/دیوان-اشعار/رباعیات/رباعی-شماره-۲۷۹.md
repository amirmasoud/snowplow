---
title: >-
    رباعی شمارهٔ ۲۷۹
---
# رباعی شمارهٔ ۲۷۹

<div class="b" id="bn1"><div class="m1"><p>گیرم ز غمت جان و خرد پیر کنم</p></div>
<div class="m2"><p>خود را ز هوس ناوک تقدیر کنم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر هر دو جهان چهار تکبیر کنم</p></div>
<div class="m2"><p>شایستهٔ تو نیم، چه تدبیر کنم</p></div></div>