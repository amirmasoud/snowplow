---
title: >-
    رباعی شمارهٔ ۱۶۸
---
# رباعی شمارهٔ ۱۶۸

<div class="b" id="bn1"><div class="m1"><p>دیده ز فراق تو زیان می‌بیند</p></div>
<div class="m2"><p>بر چهره ز خون دل نشان می‌بیند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با این همه من ز دیده ناخشنودم</p></div>
<div class="m2"><p>تا بی رخ تو چرا جهان می‌بیند</p></div></div>