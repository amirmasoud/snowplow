---
title: >-
    رباعی شمارهٔ ۲۱۰
---
# رباعی شمارهٔ ۲۱۰

<div class="b" id="bn1"><div class="m1"><p>درد دلم از طبیب بیهوده مپرس</p></div>
<div class="m2"><p>رنج تنم از حریف آسوده مپرس</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نالودهٔ پاک را از آلوده مپرس</p></div>
<div class="m2"><p>در بوده همی نگر ز نابوده مپرس</p></div></div>