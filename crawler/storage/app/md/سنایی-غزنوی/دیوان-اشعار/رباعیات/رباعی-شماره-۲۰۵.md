---
title: >-
    رباعی شمارهٔ ۲۰۵
---
# رباعی شمارهٔ ۲۰۵

<div class="b" id="bn1"><div class="m1"><p>شب گشت ز هجران دل فروزم روز</p></div>
<div class="m2"><p>شب تیز شد از آه جهانسوزم روز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شد روشنی و تیرگی از روز و شبم</p></div>
<div class="m2"><p>اکنون نه شبم شبست و نه روزم روز</p></div></div>