---
title: >-
    رباعی شمارهٔ ۳۷۷
---
# رباعی شمارهٔ ۳۷۷

<div class="b" id="bn1"><div class="m1"><p>روشن‌تر از آفتاب و ماهی گویی</p></div>
<div class="m2"><p>پدرام‌تر از مسند و گاهی گویی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آراسته از لطف الاهی گویی</p></div>
<div class="m2"><p>تا خود به کجا رسید خواهی گویی</p></div></div>