---
title: >-
    رباعی شمارهٔ ۱۷۶
---
# رباعی شمارهٔ ۱۷۶

<div class="b" id="bn1"><div class="m1"><p>یک روز دلت به مهر ما نگراید</p></div>
<div class="m2"><p>دیوت همه جز راه بلا ننماید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا لاجرم اکنون که چنینت باید</p></div>
<div class="m2"><p>می‌گوید من همی نگویم شاید</p></div></div>