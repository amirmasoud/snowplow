---
title: >-
    رباعی شمارهٔ ۴۱۹
---
# رباعی شمارهٔ ۴۱۹

<div class="b" id="bn1"><div class="m1"><p>از خلق ز راه تیز گوشی نرهی</p></div>
<div class="m2"><p>وز خود ز سر سخن‌فروشی نرهی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زین هر دو بدین دو گر بکوشی نرهی</p></div>
<div class="m2"><p>از خلق و ز خود جز به خموشی نرهی</p></div></div>