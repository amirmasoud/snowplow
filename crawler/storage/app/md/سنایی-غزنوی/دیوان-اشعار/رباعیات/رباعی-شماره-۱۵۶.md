---
title: >-
    رباعی شمارهٔ ۱۵۶
---
# رباعی شمارهٔ ۱۵۶

<div class="b" id="bn1"><div class="m1"><p>این بی‌ریشان که سغبهٔ سیم و زرند</p></div>
<div class="m2"><p>در سبلت تو به شاعری که نگرند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زر باید زر که تا غم از دل ببرند</p></div>
<div class="m2"><p>ترانهٔ خشک خوبرویان نخرند</p></div></div>