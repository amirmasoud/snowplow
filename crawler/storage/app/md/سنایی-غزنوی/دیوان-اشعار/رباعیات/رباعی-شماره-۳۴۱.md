---
title: >-
    رباعی شمارهٔ ۳۴۱
---
# رباعی شمارهٔ ۳۴۱

<div class="b" id="bn1"><div class="m1"><p>ای مفلس ما ز مجلس خرم تو</p></div>
<div class="m2"><p>دل مرد رهی را که برآمد دم تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شد بر دو کمان سنایی پر غم تو</p></div>
<div class="m2"><p>یا ماتم دل دارد یا ماتم تو</p></div></div>