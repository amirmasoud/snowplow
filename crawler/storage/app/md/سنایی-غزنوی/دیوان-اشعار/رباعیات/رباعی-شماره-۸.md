---
title: >-
    رباعی شمارهٔ ۸
---
# رباعی شمارهٔ ۸

<div class="b" id="bn1"><div class="m1"><p>در منزل وصل توشه‌ای نیست مرا</p></div>
<div class="m2"><p>وز خرمن عشق خوشه‌ای نیست مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر بگریزم ز صحبت نااهلان</p></div>
<div class="m2"><p>کمتر باشد که گوشه‌ای نیست مرا</p></div></div>