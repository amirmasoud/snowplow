---
title: >-
    رباعی شمارهٔ ۱۱۹
---
# رباعی شمارهٔ ۱۱۹

<div class="b" id="bn1"><div class="m1"><p>آب از اثر عارض تو می گردد</p></div>
<div class="m2"><p>آتش زد و رخسار تو پر خوی گردد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر عاشق تو چو خاک لاشی گردد</p></div>
<div class="m2"><p>چون باد به گرد زلف تو کی گردد</p></div></div>