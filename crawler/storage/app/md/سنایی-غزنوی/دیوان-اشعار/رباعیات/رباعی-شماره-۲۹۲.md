---
title: >-
    رباعی شمارهٔ ۲۹۲
---
# رباعی شمارهٔ ۲۹۲

<div class="b" id="bn1"><div class="m1"><p>از دیده درم خرید روی تو شدیم</p></div>
<div class="m2"><p>وز گوش غلام های و هوی تو شدیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بی روی تو بر مثال روی تو شدیم</p></div>
<div class="m2"><p>بازیچهٔ کودکان کوی تو شدیم</p></div></div>