---
title: >-
    رباعی شمارهٔ ۳۵۵
---
# رباعی شمارهٔ ۳۵۵

<div class="b" id="bn1"><div class="m1"><p>گر بدگویی ترا بدی گفت ای ماه</p></div>
<div class="m2"><p>هرگز نشود بر تو دل بنده تباه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از گفتهٔ بدگوی ز ما عذر مخواه</p></div>
<div class="m2"><p>کایینه سیه نگردد از روی سیاه</p></div></div>