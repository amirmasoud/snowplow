---
title: >-
    رباعی شمارهٔ ۲۱۵
---
# رباعی شمارهٔ ۲۱۵

<div class="b" id="bn1"><div class="m1"><p>اندر طلبت هزار دل کرد هوس</p></div>
<div class="m2"><p>با عشق تو صد هزار جان باخت نفس</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>لیکن چو همی می‌نگرم از همه کس</p></div>
<div class="m2"><p>با نام تو پیوست جمال همه کس</p></div></div>