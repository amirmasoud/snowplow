---
title: >-
    رباعی شمارهٔ ۳۵۹
---
# رباعی شمارهٔ ۳۵۹

<div class="b" id="bn1"><div class="m1"><p>گر تو به صلاح خویش کم نازی به</p></div>
<div class="m2"><p>با حالت نقد وقت در سازی به</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در صومعه سر ز زهد نفرازی به</p></div>
<div class="m2"><p>بتخانه اگر ز بت بپردازی به</p></div></div>