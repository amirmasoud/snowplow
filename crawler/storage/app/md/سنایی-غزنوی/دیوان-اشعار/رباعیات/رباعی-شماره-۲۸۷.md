---
title: >-
    رباعی شمارهٔ ۲۸۷
---
# رباعی شمارهٔ ۲۸۷

<div class="b" id="bn1"><div class="m1"><p>چوبی بودم بود به گل در پایم</p></div>
<div class="m2"><p>در خدمت مختار فلک شد جایم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در خدمت او چنان قوی شد رایم</p></div>
<div class="m2"><p>کامروز ستون آسمان را شایم</p></div></div>