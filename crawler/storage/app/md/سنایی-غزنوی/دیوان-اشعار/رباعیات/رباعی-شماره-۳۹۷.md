---
title: >-
    رباعی شمارهٔ ۳۹۷
---
# رباعی شمارهٔ ۳۹۷

<div class="b" id="bn1"><div class="m1"><p>در هجر تو گر دلم گراید به خسی</p></div>
<div class="m2"><p>در بر نگذارمش که سازم هوسی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ور دیده نگه کند به دیدار کسی</p></div>
<div class="m2"><p>در سر نگذارمش که ماند نفسی</p></div></div>