---
title: >-
    رباعی شمارهٔ ۳۷
---
# رباعی شمارهٔ ۳۷

<div class="b" id="bn1"><div class="m1"><p>روز از طلبت پردهٔ بیکاری ماست</p></div>
<div class="m2"><p>شبها ز غمت حجرهٔ بیداری ماست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هجران تو پیرایهٔ غمخواری ماست</p></div>
<div class="m2"><p>سودای تو سرمایهٔ هشیاری ماست</p></div></div>