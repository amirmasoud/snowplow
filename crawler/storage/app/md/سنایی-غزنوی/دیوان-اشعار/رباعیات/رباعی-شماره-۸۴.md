---
title: >-
    رباعی شمارهٔ ۸۴
---
# رباعی شمارهٔ ۸۴

<div class="b" id="bn1"><div class="m1"><p>عقلی که ز لطف دیدهٔ جان پنداشت</p></div>
<div class="m2"><p>بر دل صفت ترا به خوبی بنگاشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جانی که همی با تو توان عمر گذاشت</p></div>
<div class="m2"><p>عمری که دل از مهر تو بر نتوان داشت</p></div></div>