---
title: >-
    رباعی شمارهٔ ۲۳۴
---
# رباعی شمارهٔ ۲۳۴

<div class="b" id="bn1"><div class="m1"><p>از یار وفا مجوی کاندر هر باغ</p></div>
<div class="m2"><p>بی هیچ نصیبه عشق میبازد زاغ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا با خودی از عشق منه بر دل داغ</p></div>
<div class="m2"><p>پروانه شو آنگاه تو دانی و چراغ</p></div></div>