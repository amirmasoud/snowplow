---
title: >-
    رباعی شمارهٔ ۴۱۷
---
# رباعی شمارهٔ ۴۱۷

<div class="b" id="bn1"><div class="m1"><p>بر خاک نهم پیش تو سر گر خواهی</p></div>
<div class="m2"><p>وان خاک کنم ز دیده‌تر گر خواهی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای جان چو به یاد تو مرا کار نکوست</p></div>
<div class="m2"><p>جان نیز دل انگار و ببر گر خواهی</p></div></div>