---
title: >-
    رباعی شمارهٔ ۲۴۴
---
# رباعی شمارهٔ ۲۴۴

<div class="b" id="bn1"><div class="m1"><p>گویند که کرده‌ای دلت بردهٔ عشق</p></div>
<div class="m2"><p>وین رنج تو هست از دل آوردهٔ عشق</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر بر دارم ز پیش دل پردهٔ عشق</p></div>
<div class="m2"><p>بینند دلی به نازپروردهٔ عشق</p></div></div>