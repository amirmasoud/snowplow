---
title: >-
    رباعی شمارهٔ ۱
---
# رباعی شمارهٔ ۱

<div class="b" id="bn1"><div class="m1"><p>عشقست مرا بهینه‌تر کیش بتا</p></div>
<div class="m2"><p>نوشست مرا ز عشق تو نیش بتا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من می‌باشم ز عشق تو ریش بتا</p></div>
<div class="m2"><p>نه پای تو گیرم نه سر خویش بتا</p></div></div>