---
title: >-
    رباعی شمارهٔ ۲۲۹
---
# رباعی شمارهٔ ۲۲۹

<div class="b" id="bn1"><div class="m1"><p>هر چند بود مردم دانا درویش</p></div>
<div class="m2"><p>صد ره بود از توانگر نادان بیش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این را بشود جاه چو شد مال از پیش</p></div>
<div class="m2"><p>و آن شاد بود مدام از دانش خویش</p></div></div>