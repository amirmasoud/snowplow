---
title: >-
    رباعی شمارهٔ ۱۹۶
---
# رباعی شمارهٔ ۱۹۶

<div class="b" id="bn1"><div class="m1"><p>آن کس که چو او نبود در دهر دگر</p></div>
<div class="m2"><p>در خاک شد از تیر اجل زیر و زبر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>واکنون که همی ز خاک برنارد سر</p></div>
<div class="m2"><p>شاید که به خون دل کنم مژگان تر</p></div></div>