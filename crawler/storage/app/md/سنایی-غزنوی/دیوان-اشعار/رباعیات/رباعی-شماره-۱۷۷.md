---
title: >-
    رباعی شمارهٔ ۱۷۷
---
# رباعی شمارهٔ ۱۷۷

<div class="b" id="bn1"><div class="m1"><p>آنی که فدای تو روان می‌باید</p></div>
<div class="m2"><p>پیش رخ تو نثار جان می‌باید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من هیچ ندانم که کرا مانی تو</p></div>
<div class="m2"><p>ای دوست چنانی که چنان می‌باید</p></div></div>