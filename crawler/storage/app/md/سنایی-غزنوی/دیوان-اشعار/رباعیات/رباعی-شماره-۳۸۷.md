---
title: >-
    رباعی شمارهٔ ۳۸۷
---
# رباعی شمارهٔ ۳۸۷

<div class="b" id="bn1"><div class="m1"><p>در هر خم زلف مشکبیزی داری</p></div>
<div class="m2"><p>در هر سر غمزه رستخیزی داری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رو گر چه ز عاشقان گریزی داری</p></div>
<div class="m2"><p>روزی داری از آنکه ریزی داری</p></div></div>