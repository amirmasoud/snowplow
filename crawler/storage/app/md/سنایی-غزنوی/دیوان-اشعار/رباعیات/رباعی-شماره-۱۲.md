---
title: >-
    رباعی شمارهٔ ۱۲
---
# رباعی شمارهٔ ۱۲

<div class="b" id="bn1"><div class="m1"><p>گردی نبرد ز بوسه از افسر ما</p></div>
<div class="m2"><p>گر بوسه به نام خود زنی بر سر ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تازان خودی مگرد گرد در ما</p></div>
<div class="m2"><p>یا چاکر خویش باش یا چاکر ما</p></div></div>