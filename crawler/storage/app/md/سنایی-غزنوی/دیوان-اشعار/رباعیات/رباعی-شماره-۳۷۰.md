---
title: >-
    رباعی شمارهٔ ۳۷۰
---
# رباعی شمارهٔ ۳۷۰

<div class="b" id="bn1"><div class="m1"><p>ای سوسن آزاد ز بس رعنایی</p></div>
<div class="m2"><p>چون لاله ز خنده هیچ می‌ناسایی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پشتم چو بنفشه گشت ای بینایی</p></div>
<div class="m2"><p>زیرا که چو گل زود روی، دیر آیی</p></div></div>