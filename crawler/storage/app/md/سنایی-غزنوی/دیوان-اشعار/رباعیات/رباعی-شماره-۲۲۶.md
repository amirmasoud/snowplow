---
title: >-
    رباعی شمارهٔ ۲۲۶
---
# رباعی شمارهٔ ۲۲۶

<div class="b" id="bn1"><div class="m1"><p>می بر کف گیر و هر دو عالم بفروش</p></div>
<div class="m2"><p>بیهوده مدار هر دو عالم به خروش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر هر دو جهان نباشدت در فرمان</p></div>
<div class="m2"><p>در دوزخ مست به که در خلد به هوش</p></div></div>