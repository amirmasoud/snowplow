---
title: >-
    رباعی شمارهٔ ۳۱۷
---
# رباعی شمارهٔ ۳۱۷

<div class="b" id="bn1"><div class="m1"><p>گه یار شوی تو با ملامت‌گر من</p></div>
<div class="m2"><p>گه بگریزی ز بیم خصم از بر من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بگذار مرا چو نیستی در خور من</p></div>
<div class="m2"><p>تو مصلح و من رند نداری سر من</p></div></div>