---
title: >-
    شمارهٔ ۳ - ترکیب بند در مدح مکین‌الدین
---
# شمارهٔ ۳ - ترکیب بند در مدح مکین‌الدین

<div class="b" id="bn1"><div class="m1"><p>ای سنایی بگذر از جان در پناه تن مباش</p></div>
<div class="m2"><p>چون فرشته یار داری جفت اهریمن مباش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همچو شانه بستهٔ هر تارهٔ مویی مشو</p></div>
<div class="m2"><p>همچو آیینه درون تاری برون روشن مباش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر زمان از قیل و قال هر کسی از جا مشو</p></div>
<div class="m2"><p>گر زمانه همچو سندان شد تو چون ارزن مباش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همچو طوطی هر زمانی صدرهٔ دیبا مپوش</p></div>
<div class="m2"><p>پیش ناکس همچو قمری طوق در گردن مباش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر سر نیکی نداری پایت از بدها بکش</p></div>
<div class="m2"><p>تاج را گر زر نباشی بند را آهن مباش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پیش دانگانه همه سر چشم چون سوزن مشو</p></div>
<div class="m2"><p>بندهٔ هر بنده نام آزاد چون سوسن مباش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عاشق جانی به گرد حجرهٔ جانان مگرد</p></div>
<div class="m2"><p>با جعل خو کرده‌ای رو، طالب گلشن مباش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>صحبت آن سینه خواهی نرم شو همچون حریر</p></div>
<div class="m2"><p>طاقت پیکان نداری سخت چون جوشن مباش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مکمن قرآن به جز صدر مکین الدین مدان</p></div>
<div class="m2"><p>تا همی ممکن شود جز در پی ممکن مباش</p></div></div>
<div class="b2" id="bn10"><p>سید آل نظیری آن امام راستین</p>
<p>پیشوای راستان صاحب کلام راستین</p></div>
<div class="b" id="bn11"><div class="m1"><p>ای دل اندر راه عشق عاشقی هشیار باش</p></div>
<div class="m2"><p>عقل را یکسو نه و مر یار خود را یار باش</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چند گویی از قلندر وز طریق و رسم او</p></div>
<div class="m2"><p>یا حدیث او فرونه یا قلندروار باش</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>یا بسان بلبل و قمری همه گفتار شو</p></div>
<div class="m2"><p>یا چنان چون باز و شاهین سر به سر کردار باش</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>یا بیا کن دل ز خون چون نار و نفع خلق شو</p></div>
<div class="m2"><p>ورنه رخ را رنگ ده بی نفع چون گلنار باش</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گرت خوی شیر و زور پیل و سهم مار نیست</p></div>
<div class="m2"><p>همچو مور و پشه و روباه کم آزار باش</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ور همی خواهی که دو عالم مسلم باشدت</p></div>
<div class="m2"><p>یک زمان بر وفق صاحب عور و صاحب عار باش</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>با صفای دل چه اندیشی ز حس و طبع و نفس</p></div>
<div class="m2"><p>یار در غارست با تو غار گو پر مار باش</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>سینهٔ فرزانگان را کین چه گردی مهر گرد</p></div>
<div class="m2"><p>دیدهٔ دیوانگان را گل چه باشی، خار باش</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ای سنایی گرت قصد آسمان چارمست</p></div>
<div class="m2"><p>همچو عیسا پیش دشمن یک زمان بر دار باش</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>مدح خواجه‌ست این قصیده اندرین دعوی مکن</p></div>
<div class="m2"><p>خواجه این معنی نکو داند تو زیرک‌سار باش</p></div></div>
<div class="b2" id="bn21"><p>آفتاب اهل فضل و آسمان شاعری</p>
<p>قرة العین جهان صاحب قران شاعری</p></div>
<div class="b" id="bn22"><div class="m1"><p>ای دل ار بند جانانی حدیث جان مکن</p></div>
<div class="m2"><p>صحبت رضوان گزیدی خدمت دربان مکن</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>زلف او دیدی صفات ظلمت کفران مگوی</p></div>
<div class="m2"><p>روی او دیدی حدیث لذت ایمان مکن</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>کفر و ایمان هر دو از راهند جانان مقصدست</p></div>
<div class="m2"><p>بر در کعبه حدیث عقبهٔ شیطان مکن</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چون عطارد گر نخواهی هر زمانی احتراق</p></div>
<div class="m2"><p>چون بنات النعش جز در گرد خود جولان مکن</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>گر زحیزی خیره گردی روی زی نادان میار</p></div>
<div class="m2"><p>چون بضاعت زیره داری روی زی کرمان مکن</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>سر این معنی ندانی گرد این دعوی مگرد</p></div>
<div class="m2"><p>راستی بوذر نداری دوستی سلمان مکن</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>مل چو زان لب خواستی جز سینه مجلسگه مساز</p></div>
<div class="m2"><p>گل چو زان رخ یافتی جز دیده نرگسدان مکن</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بر یمین و بر یسار تو دو دیو کافرند</p></div>
<div class="m2"><p>چون فرشته خو شدی این هر دو را فرمان مکن</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>اندرین ره با تو همراه ست پیری راست گوی</p></div>
<div class="m2"><p>هر چه گوید آن مکن، ز نهار زنهار آن مکن</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>صحبت حور ارت باید کینهٔ رضوان مجوی</p></div>
<div class="m2"><p>تخت ری خواهی خلاف تاج اصفاهان مکن</p></div></div>
<div class="b2" id="bn32"><p>تا چنو تاجی بود بر فرق اصفاهان مدام</p>
<p>چون خرد در سر، درو سازند پس شاهان مقام</p></div>
<div class="b" id="bn33"><div class="m1"><p>آنکه مر صدر عرب را اوست اکنون کدخدای</p></div>
<div class="m2"><p>آنکه مر اهل عجم را اوست حالی رهنمای</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>هست هم خلق کسی کز مهر او آمد به دست</p></div>
<div class="m2"><p>هست هم نام کسی کز بهر او دارد به پای</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>هشت خلد و هفت کوکب شش جهات و پنج حس</p></div>
<div class="m2"><p>چار طبع و هر سه نفس و هر دو عالم یک خدای</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>زو گزیده‌تر نبیند هیچ کس معنی گزین</p></div>
<div class="m2"><p>زو ستوده‌تر نیابد هیچ کس مردم‌ستای</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>شعر او پرورده باشد همچو ابروی چگل</p></div>
<div class="m2"><p>قافیتها دلربای و تنگ همچون چشم فای</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>مادح و ممدوح را چون او ندیدم در جهان</p></div>
<div class="m2"><p>در سخن معنی طراز و در سخا معنی فزای</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>نیست گردد بی گمان از خاطر او حشو و لحن</p></div>
<div class="m2"><p>آب گردد استخوان ناچار در حلق همای</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>شعر او بینی جهانی آید اندر چشم تو</p></div>
<div class="m2"><p>همچنین بودست آن جامی که بد گیتی نمای</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>معنی و الفاظ او همچون کبابست و شراب</p></div>
<div class="m2"><p>این یکی قوت فزای و آن یکی انده زدای</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>خوش نباشد با تکلف شعر ناخوش چون دواج</p></div>
<div class="m2"><p>شعر او بس چابکست و بی تکلف چون قبای</p></div></div>
<div class="b2" id="bn43"><p>شعرهای ما نه شعرست ار چنان کان شاعریست</p>
<p>شاعری دیگر بود نزدیک من آن ساحریست</p></div>
<div class="b" id="bn44"><div class="m1"><p>دی در آن تصنیف خواجه ساعتی کردم نظر</p></div>
<div class="m2"><p>لفظها دیدم فصیح و نکته‌ها دیدم غور</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>عالمی آمد به چشم من مزین وندر او</p></div>
<div class="m2"><p>لشکر تازی و دهقان در جدل با یکدگر</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>در یکی رو رودکی و عنصری با طعن و ضرب</p></div>
<div class="m2"><p>وز دگر سو بو تمام و بحتری در کر و فر</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>اخطل و اعشی در آن جانب شده صاحب نفیر</p></div>
<div class="m2"><p>شاکر و جلاب ازین جانب شده صاحب نفر</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>از قفای بحتری از حله در تا قیروان</p></div>
<div class="m2"><p>بر وفای رودکی از دجله در تا کاشغر</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>مرکبانش وافر و کامل، سریع و منسرح</p></div>
<div class="m2"><p>ساختهاشان وافر و سالم، صحیح و معتبر</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>معنی اندر جوشن لفظ آمده پیش مصاف</p></div>
<div class="m2"><p>خود بر سر همچو کیوان تیغ در کف همچو خور</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>از نهیب شوکت ایشان ز چرخ آبگون</p></div>
<div class="m2"><p>زهره و مریخ مانده کام خشک و دیده تر</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>هر زمان گفتی خرد زین دو سپاه بیکران</p></div>
<div class="m2"><p>مر کرا باشد ظفر یا خود که دارد زین خبر</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>مر خرد را خاطر من در زمان دادی جواب</p></div>
<div class="m2"><p>من ندانم خواجه داند تا کرا باشد ظفر</p></div></div>
<div class="b2" id="bn54"><p>آنکه اندر هر دو صف دارد مجال سروری</p>
<p>بیش ازین هرگز کرا باشد کمال سروری</p></div>
<div class="b" id="bn55"><div class="m1"><p>شعر او همچون سلامت عالم آراید همی</p></div>
<div class="m2"><p>نکتهٔ او چون سعادت شادی افزاید همی</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>نکته و معنی که از انشاء و طبع او رود</p></div>
<div class="m2"><p>گویی از فردوس اعلا جبرییل آید همی</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>مادر بد مهر گفتستند عالم را و من</p></div>
<div class="m2"><p>این نگویم ز آنکه چونین من خلف زاید همی</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>کس نیدی اندر سخن شیرین سخنتر زو ولیک</p></div>
<div class="m2"><p>هجو او چون زهر افعی زود بگزاید همی</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>هر که مدح او ببیند گر چه خصم او بود</p></div>
<div class="m2"><p>از میان جان و دل گوید چنین باید همی</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>سر فرازان جماعت گر چه بدگوی منند</p></div>
<div class="m2"><p>مر مرا باری بدیشان دل ببخشاید همی</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>آب روی و آتش طبع مرا زان چه زیان</p></div>
<div class="m2"><p>گر به خیره بادپایی خاک پیماید همی</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>زین شگفتی من خود از اندیشه حیران مانده‌ام</p></div>
<div class="m2"><p>تا چرا معنی بدینسان روی بنماید همی</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>گر مرا نادان بنستاید چه عیب آید از آن</p></div>
<div class="m2"><p>چون به عالم هر که دانایست بستاید همی</p></div></div>
<div class="b2" id="bn64"><p>در سعادت همچنین آسوده بادی سال و ماه</p>
<p>از بزرگان و ز بزرگی مر ترا اقبال و جاه</p></div>