---
title: >-
    شمارهٔ ۴۰۶
---
# شمارهٔ ۴۰۶

<div class="b" id="bn1"><div class="m1"><p>ای راه ترا دلیل دردی</p></div>
<div class="m2"><p>فردی تو و آشنات فردی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از دام تو دانه‌ای و مرغی</p></div>
<div class="m2"><p>در جام تو قطره‌ای و مردی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بی روی تو روح چیست بادی</p></div>
<div class="m2"><p>با زلف تو شخص کیست گردی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خارست همه جهان و آنگه</p></div>
<div class="m2"><p>روی تو در آن میانه وردی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در کوی تو نیست تشنگان را</p></div>
<div class="m2"><p>جز خاک در تو آبخوردی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در راه تو نیست عاشقان را</p></div>
<div class="m2"><p>جز داعیهٔ تو ره‌نوردی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در تو که رسد به دستمزدی</p></div>
<div class="m2"><p>تا از تو نبود پایمردی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در عشق تو خود وفا کی آید</p></div>
<div class="m2"><p>از خشک و تری و گرم و سردی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نیک‌ست که آینه نداری</p></div>
<div class="m2"><p>تا هست شفات نیست دردی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از آینه‌ای بدی به دستت</p></div>
<div class="m2"><p>چشم تو ترا به چشم کردی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>در شهر تو نیست جز سنایی</p></div>
<div class="m2"><p>بی‌وصل تو جز که یاوه گردی</p></div></div>