---
title: >-
    شمارهٔ  ۱۹۲
---
# شمارهٔ  ۱۹۲

<div class="b" id="bn1"><div class="m1"><p>ای پسر میخواره و قلاش باش</p></div>
<div class="m2"><p>در میان حلقهٔ اوباش باش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>راه بر پوشیدگی هرگز مرو</p></div>
<div class="m2"><p>بر سر کویی که باشی فاش باش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مهر خوبان بر دل و جان نقش کن</p></div>
<div class="m2"><p>سال و مه این نقش را نقاش باش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کم زنان را غاشیه بر دوش گیر</p></div>
<div class="m2"><p>مجلس میخواره را فراش باش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر نداری رو ز درگاه قدر</p></div>
<div class="m2"><p>چاکر اینانج یا بکتاش باش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>میر میران گر نباشی باک نیست</p></div>
<div class="m2"><p>چون سنایی بندهٔ یکتاش باش</p></div></div>