---
title: >-
    شمارهٔ  ۲۰۱
---
# شمارهٔ  ۲۰۱

<div class="b" id="bn1"><div class="m1"><p>از فلک در تاب بودم دی و دوش</p></div>
<div class="m2"><p>وز غمت بی تاب بودم دی و دوش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با لب خشک از سرشک دیدگان</p></div>
<div class="m2"><p>در میان آب بودم دی و دوش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گاه می‌خوردم گه از بحر دعا</p></div>
<div class="m2"><p>روی در محراب بودم دی و دوش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بی رخ تو در میان بحر آب</p></div>
<div class="m2"><p>با نبید ناب بودم دی و دوش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از کمال هجر در صحرای درد</p></div>
<div class="m2"><p>تیر در پرتاب بودم دی و دوش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>صحبت دیدار تو جستم همی</p></div>
<div class="m2"><p>گر چه با اصحاب بودم دی و دوش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بی تو لرزان و طپان بر روی خاک</p></div>
<div class="m2"><p>راست چون سیماب بودم دی و دوش</p></div></div>