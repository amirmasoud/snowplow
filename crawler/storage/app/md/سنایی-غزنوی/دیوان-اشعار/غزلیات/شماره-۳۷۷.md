---
title: >-
    شمارهٔ ۳۷۷
---
# شمارهٔ ۳۷۷

<div class="b" id="bn1"><div class="m1"><p>ای سنایی خیز و بشکن زود قفل میکده</p></div>
<div class="m2"><p>بازخر ما را زمانی زین غمان بیهده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جام جمشیدی بیار از بهر این آزادگان</p></div>
<div class="m2"><p>درد می درده برای درد این محنت زده</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>درد صافی درده ای ساقی درین مجلس همی</p></div>
<div class="m2"><p>تا زمانی می خوریم آسوده دل در میکده</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>محتسب را گو ترا با مست کوی ما چکار</p></div>
<div class="m2"><p>می چه خواهی ای جوان زین عاشقان دل زده</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>می‌ندانی کادم از کتم عدم سوی وجود</p></div>
<div class="m2"><p>از برای مهربازان خرابات آمده</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا ترا روشن شود در کافری در ثمین</p></div>
<div class="m2"><p>بت پرستی پیشه گیر اندر میان بتکده</p></div></div>