---
title: >-
    شمارهٔ  ۱۵
---
# شمارهٔ  ۱۵

<div class="b" id="bn1"><div class="m1"><p>من کیم کاندیشهٔ تو هم نفس باشد مرا</p></div>
<div class="m2"><p>یا تمنای وصال چون تو کس باشد مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر بود شایستهٔ غم خوردن تو جان من</p></div>
<div class="m2"><p>این نصیب از دولت عشق تو بس باشد مرا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر نه عشقت سایهٔ من شد چرا هر گه که من</p></div>
<div class="m2"><p>روی بر تابم ازو پویان ز پس باشد مرا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هرنفس کانرا بیاد روزگار تو زنم</p></div>
<div class="m2"><p>جملهٔ عالم طفیل آن نفس باشد مرا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هز رمان ز امید وصل تو دل خود خوش کنم</p></div>
<div class="m2"><p>باز گویم نه چه جای این هوس باشد مرا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون خیال خاکپایت می‌نبیند چشم من</p></div>
<div class="m2"><p>بر وصال تو چگونه دست رس باشد مرا</p></div></div>