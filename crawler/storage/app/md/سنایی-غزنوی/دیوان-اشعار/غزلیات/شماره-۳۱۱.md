---
title: >-
    شمارهٔ ۳۱۱
---
# شمارهٔ ۳۱۱

<div class="b" id="bn1"><div class="m1"><p>ای به راه عشق خوبان گام بر میخواره زن</p></div>
<div class="m2"><p>نور معنی را ز دعوی در میان زنار زن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر سر کوی خرابات از تن معشوق هست</p></div>
<div class="m2"><p>صدهزاران بوسه بر خاک در خمار زن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>قیل و قال لایجوز از کوی دل بیرون گذار</p></div>
<div class="m2"><p>بر در همت ز هستی پس قوی مسمار زن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا تویی با تو نیایی خویشتن رنجه مدار</p></div>
<div class="m2"><p>بر در نادیده معنی خیمهٔ اسرار زن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نوش شهد از پیش آن در زهر قاتل بار کن</p></div>
<div class="m2"><p>طمع از روی حقیقت پیش زهر مار زن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون به نامحرم رسی بدروز و کافر رنگ باش</p></div>
<div class="m2"><p>بر طراز رنگ ظاهر نام را طرار زن</p></div></div>