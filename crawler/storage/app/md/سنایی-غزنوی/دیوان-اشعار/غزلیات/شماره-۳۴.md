---
title: >-
    شمارهٔ  ۳۴
---
# شمارهٔ  ۳۴

<div class="b" id="bn1"><div class="m1"><p>تا هلاک عاشقان از طرهٔ شبرنگ تست</p></div>
<div class="m2"><p>وای مسکین عاشقی کو را دل اندر چنگ تست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عاشق مسکین چه داند کرد با نیرنگ تو</p></div>
<div class="m2"><p>جادوی بلبل اسیر چشم پر نیرنگ تست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نافهٔ آهو غلام زلف عنبر بوی تست</p></div>
<div class="m2"><p>عنبر سارا رهین خط سبز از رنگ تست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا نهفته مشک باشد مر ترا در زیر سیم</p></div>
<div class="m2"><p>دستهای عاشقان یکباره زیر سنگ تست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>من به رنگ تو ندیدم هیچ کس را در جهان</p></div>
<div class="m2"><p>بر تو عاشق باد هر کو در جهان همرنگ تست</p></div></div>