---
title: >-
    شمارهٔ ۳۶۷
---
# شمارهٔ ۳۶۷

<div class="b" id="bn1"><div class="m1"><p>ای قوم مرا رنجه مدارید علی‌الله</p></div>
<div class="m2"><p>معشوق مرا پیش من آرید علی‌الله</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گز هیچ زیاری نهمی بر لب او بوس</p></div>
<div class="m2"><p>یک بوسه به من صد بشمارید علی‌الله</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ور هیچ به دست آرید از صورت معشوق</p></div>
<div class="m2"><p>بر قبلهٔ زهاد نگارید علی‌الله</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آن خم که بر او مهر مغانست نهاده</p></div>
<div class="m2"><p>الا به من مغ مسپارید علی‌الله</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از دین مسلمانی چون نام شمار است</p></div>
<div class="m2"><p>از دین مغان شرم مدارید علی‌الله</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گشتست سنایی مغ بی‌دولت و بی‌دین</p></div>
<div class="m2"><p>از دیدهٔ خود خون بمبارید علی‌الله</p></div></div>