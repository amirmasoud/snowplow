---
title: >-
    شمارهٔ  ۱۶۱
---
# شمارهٔ  ۱۶۱

<div class="b" id="bn1"><div class="m1"><p>زینهار ای یار گلرخ زینهار</p></div>
<div class="m2"><p>بی گنه بر من مکن تیزی چو خار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>لالهٔ خود رویم از فرقت مکن</p></div>
<div class="m2"><p>حجرهٔ من ز اشک خون چون لاله‌زار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون شکوفه گرد بدعهدی مگرد</p></div>
<div class="m2"><p>تا مگر باقی بمانی چون چنار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون بنفشه خفته‌ام در خدمتت</p></div>
<div class="m2"><p>پس مدارم چون بنفشه سوگوار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زان که جانها را فراقت چون سمن</p></div>
<div class="m2"><p>یک دو هفته بیش ندهد زینهار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>باش با من تازه چون شاه اسپرم</p></div>
<div class="m2"><p>تا نگردم همچو خیری دلفگار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از سر لطف و ظریفی خوش بزی</p></div>
<div class="m2"><p>همچو سوسن تازه‌ای آزاده‌وار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>همچو سیسنبر بپژمردم ز غم</p></div>
<div class="m2"><p>یک ره از ابر وفا بر من ببار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون نخوردم بادهٔ وصلت چو گل</p></div>
<div class="m2"><p>همچو نرگس پس مدارم در خمار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ای همیشه تازه و تر همچو سرو</p></div>
<div class="m2"><p>اشکم از هجران مکن چون گل انار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>حوضها کن گلبنان را از عرق</p></div>
<div class="m2"><p>تا چو نیلوفر در او گیرم قرار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>زان که از بهر سنایی هر زمان</p></div>
<div class="m2"><p>بر فراز سرو و طرف جویبار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بلبل و قمری همی گویند خوش</p></div>
<div class="m2"><p>زینهار ای یار گلرخ زینهار</p></div></div>