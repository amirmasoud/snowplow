---
title: >-
    شمارهٔ  ۵۶
---
# شمارهٔ  ۵۶

<div class="b" id="bn1"><div class="m1"><p>ماه رویا گرد آن رخ زلف چون زنجیر چیست</p></div>
<div class="m2"><p>وندران زنجیر چندان پیچ و تاب از قیر چیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر بود زنجیر جانان از پی دیوانگان</p></div>
<div class="m2"><p>خود منم دیوانه بر عارض ترا زنجیر چیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر شراب و شیر خواهی مضمر اندر یاسمین</p></div>
<div class="m2"><p>تودهٔ عنبر فگنده بر شراب و شیر چیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>قبلهٔ جان ای نگار از صورت و روی تو نیست</p></div>
<div class="m2"><p>از خیالت روز و شب در چشم من تصویر چیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>قد من گر چون کمان از عشق تو شد پس چرا</p></div>
<div class="m2"><p>گرد آن دو نرگس بیمار چندان تیر چیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آیتی کز فال عشق تو برآید مر مرا</p></div>
<div class="m2"><p>اندر آن آیت به جز اندوه و غم تفسیر چیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در ازل رفته‌ست تقدیری ز عشقت بر سرم</p></div>
<div class="m2"><p>جز رضا دادن نگارا حیله و تدبیر چیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ای سنایی چون مقصر نیستی در عشق او</p></div>
<div class="m2"><p>در وفا و عهد تو چندین ازو تقصیر چیست</p></div></div>