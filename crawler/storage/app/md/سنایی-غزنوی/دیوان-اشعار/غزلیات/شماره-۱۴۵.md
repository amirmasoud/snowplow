---
title: >-
    شمارهٔ  ۱۴۵
---
# شمارهٔ  ۱۴۵

<div class="b" id="bn1"><div class="m1"><p>هر که در کوی خرابات مرا بار دهد</p></div>
<div class="m2"><p>به کمال و کرمش جان من اقرار دهد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بار در کوی خرابات مرا هیچ کسی</p></div>
<div class="m2"><p>ندهد ور دهد آن یار وفادار دهد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در خرابات بود یار من و من شب و روز</p></div>
<div class="m2"><p>به سر کوی همی گردم تا بار دهد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای خوشا کوی خرابات که پیوسته در او</p></div>
<div class="m2"><p>مر مرا دوست همی وعدهٔ دیدار دهد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر که او حال خرابات بداند به درست</p></div>
<div class="m2"><p>هر چه دارد همه در حال به بازار دهد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در خرابات نبینی که ز مستی همه سال</p></div>
<div class="m2"><p>راهب دیر ترا کشتی و زنار دهد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آنکه چون باشد هشیار به فرزند عزیز</p></div>
<div class="m2"><p>در می سیم به صد زاری دشخوار دهد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هر دو عالم را چون مست شود از دل و جان</p></div>
<div class="m2"><p>به بهای قدح می دهد و خوار دهد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آنکه بیرون خرابات به قطمیر و نقیر</p></div>
<div class="m2"><p>چون در آید به خرابات به قنطار دهد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آنکه نانی همه آفاق بود در چشمش</p></div>
<div class="m2"><p>در خرابات به می جبه و دستار دهد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آنکه او کیسه ز طرار نگهدارد چون</p></div>
<div class="m2"><p>به خرابات شود کیسه به طرار دهد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ای تو کز کوی خرابات نداری گذری</p></div>
<div class="m2"><p>زان سناییت همی پند به مقدار دهد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>تو برو زاویهٔ زهد نگهدار و مترس</p></div>
<div class="m2"><p>که خداوند سزا را به سزاوار دهد</p></div></div>