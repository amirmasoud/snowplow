---
title: >-
    شمارهٔ  ۷۳
---
# شمارهٔ  ۷۳

<div class="b" id="bn1"><div class="m1"><p>گل به باغ آمده تقصیر چراست</p></div>
<div class="m2"><p>ساقیا جام می لعل کجاست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به چنین وقت و چنین فصل عزیز</p></div>
<div class="m2"><p>کاهلی کردن و سستی نه رواست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای سنایی تو مکن توبه ز می</p></div>
<div class="m2"><p>که ترا توبه درین فصل خطاست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عاشقی خواهی و پس توبه کنی</p></div>
<div class="m2"><p>توبه و عشق بهم ناید راست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>روزکی چند بود نوبت گل</p></div>
<div class="m2"><p>روزه و توبه همه روز بجاست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جز از آن نیست که گویند مرا</p></div>
<div class="m2"><p>یار بود آنکه نه از مجمع ماست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شد به بد مردی و میخانه گزید</p></div>
<div class="m2"><p>نیک مردی را با زهد نخواست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>من به بد مردی خرسند شدم</p></div>
<div class="m2"><p>هر قضایی که بود خود ز قضاست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ای بدا مرد که امروز منم</p></div>
<div class="m2"><p>ای خوشا عیش که امروز مراست</p></div></div>