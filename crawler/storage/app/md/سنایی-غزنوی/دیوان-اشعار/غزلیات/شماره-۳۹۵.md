---
title: >-
    شمارهٔ ۳۹۵
---
# شمارهٔ ۳۹۵

<div class="b" id="bn1"><div class="m1"><p>بتا پای این ره نداری چه پویی</p></div>
<div class="m2"><p>دلا جان آن بت ندانی چه گویی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ازین رهروان مخالف چه چاره</p></div>
<div class="m2"><p>که بر لافگاه سر چار سویی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگر عاشقی کفر و ایمان یکی دان</p></div>
<div class="m2"><p>که در عقل رعناست این تندخویی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تو جانی و انگاشتی که شخصی</p></div>
<div class="m2"><p>تو آبی و پنداشتستی سبویی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همه چیز را تا نجویی نیابی</p></div>
<div class="m2"><p>جز این دوست را تا نیابی نجویی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یقین دان که تو او نباشی ولیکن</p></div>
<div class="m2"><p>چو تو در میانه نباشی تو اویی</p></div></div>