---
title: >-
    شمارهٔ ۴۰۱
---
# شمارهٔ ۴۰۱

<div class="b" id="bn1"><div class="m1"><p>اگر در کوی قلاشی مرا یکبار بارستی</p></div>
<div class="m2"><p>مرا بر دل درین عالم همه دشخوار خوارستی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ار این ناسازگار ایام با من سازگارستی</p></div>
<div class="m2"><p>سرو کارم همیشه با می و ورد و قمارستی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگر نه محنت این نامساعد روزگارستی</p></div>
<div class="m2"><p>مرا با زهد و قرایی و مستوری چکارستی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر در پارسایی خود مرا او را دوستارستی</p></div>
<div class="m2"><p>سنایی را به ماه نو نسیم نوبهارستی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هرانکو در دلست او را کنون اندر کنارستی</p></div>
<div class="m2"><p>دلش همواره شادستی و کارش چون نگارستی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دلیل صدق او دایم سنایی را بهارستی</p></div>
<div class="m2"><p>نهان وصل او دایم بر او آشکارستی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اگر از غم دل مسکین عاشق را قرارستی</p></div>
<div class="m2"><p>جهنم پیش چشم سر سریر شهریارستی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گل از هجران اقطارش میان کارزارستی</p></div>
<div class="m2"><p>دل از امید دیدارش میان مرغزارستی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مرا هفتم درک با او بدان دارالقرارستی</p></div>
<div class="m2"><p>سماوات العلی بی او حمیم هفت نارستی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چرا گویی سنایی این گر او را خود شکارستی</p></div>
<div class="m2"><p>ز دست سینهٔ کبک دری او را در آرستی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>اگر شخص سنایی را جهان سفله یارستی</p></div>
<div class="m2"><p>چو دیگر مدبران دایم به گردون بر سوارستی</p></div></div>