---
title: >-
    شمارهٔ  ۱۴۴
---
# شمارهٔ  ۱۴۴

<div class="b" id="bn1"><div class="m1"><p>روزی بت من مست به بازار برآمد</p></div>
<div class="m2"><p>گرد از دل عشاق به یک بار بر آمد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صد دلشده را از غم او روز فرو شد</p></div>
<div class="m2"><p>صد شیفته را از غم او کار برآمد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رخسار و خطش بود چو دیبا و چو عنبر</p></div>
<div class="m2"><p>باز آن دو بهم کرد و خریدار برآمد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در حسرت آن عنبر و دیبای نو آیین</p></div>
<div class="m2"><p>فریاد ز بزاز و ز عطار برآمد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رشک ست بتان را ز بناگوش و خط او</p></div>
<div class="m2"><p>گویند که بر برگ گلش خار برآمد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آن مایه بدانید که ایزد نظری کرد</p></div>
<div class="m2"><p>تا سوسن و شمشاد ز گلزار برآمد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>و آن شب که مرا بود به خلوت بر او بار</p></div>
<div class="m2"><p>پیش از شب من صبح ز کهسار برآمد</p></div></div>