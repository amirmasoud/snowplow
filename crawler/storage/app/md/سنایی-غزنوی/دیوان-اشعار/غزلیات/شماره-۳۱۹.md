---
title: >-
    شمارهٔ ۳۱۹
---
# شمارهٔ ۳۱۹

<div class="b" id="bn1"><div class="m1"><p>ای نگار دلبر زیبای من</p></div>
<div class="m2"><p>شمع شهرافروز شهرآرای من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جز برای دیدنت دیده مباد</p></div>
<div class="m2"><p>روشنایی دیدهٔ بینای من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جان و دل کردم فدای مهر تو</p></div>
<div class="m2"><p>خاک پایت باد سر تا پای من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از همه خلقان دلارامم تویی</p></div>
<div class="m2"><p>ای لطیف چابک زیبای من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون قضیب خیزران گشتم نزار</p></div>
<div class="m2"><p>در غمت ای خیزران بالای من</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>رحمت آری بر من و دستم گری</p></div>
<div class="m2"><p>گر نیاری رحم بر من وای من</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زار می‌نالم ز درد عشق زار</p></div>
<div class="m2"><p>زان که تا تو نشنوی آوای من</p></div></div>