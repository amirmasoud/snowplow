---
title: >-
    شمارهٔ  ۱۰۸
---
# شمارهٔ  ۱۰۸

<div class="b" id="bn1"><div class="m1"><p>ناز را رویی بباید همچو ورد</p></div>
<div class="m2"><p>چون نداری گرد بدخویی مگرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یا بگستر فرش زیبایی و حسن</p></div>
<div class="m2"><p>یا بساط کبر و ناز اندر نورد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نیکویی و لطف گو با تاج و کبر</p></div>
<div class="m2"><p>کعبتین و مهره گو با تخته نرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در سرت بادست و بر رو آب نیست</p></div>
<div class="m2"><p>پس میان ما دو تن زین‌ست گرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زشت باشد روی نازیبا و ناز</p></div>
<div class="m2"><p>صعب باشد چشم نا بینا و درد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جوهرت ز اول نبودست این چنین</p></div>
<div class="m2"><p>با تو ناز و کبر کرد این کار کرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زر ز معدن سرخ روی آید برون</p></div>
<div class="m2"><p>صحبت ناجنس کردش روی زرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کی کند ناخوب را بیداد خوب</p></div>
<div class="m2"><p>چون کند نامرد را کافور مرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تو همه بادی و ما را با تو صلح</p></div>
<div class="m2"><p>ما ترا خاک و ترا با ما نبرد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>لیکن از یاد تو ما را چاره نیست</p></div>
<div class="m2"><p>تا دین خاکست ما را آب خورد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ناز با ما کن که درباید همی</p></div>
<div class="m2"><p>این نیاز گرم را آن ناز سرد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ور ثنا خواهی که باشد جفت تو</p></div>
<div class="m2"><p>با سنایی چون سنایی باش فرد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>در جهان امروز بردار برد اوست</p></div>
<div class="m2"><p>باردی باشد بدو گفتن که برد</p></div></div>