---
title: >-
    شمارهٔ ۳۲۷
---
# شمارهٔ ۳۲۷

<div class="b" id="bn1"><div class="m1"><p>بند ترکش یک زمان ای ترک زیبا باز کن</p></div>
<div class="m2"><p>با رهی یک دم بساز و خرمی را ساز کن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جامهٔ جنگ از سر خود برکش و خوش طبع باش</p></div>
<div class="m2"><p>خانهٔ لهو و طرب را یک زمان در باز کن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چند گه در رزم شه پرواز کردی گرد خصم</p></div>
<div class="m2"><p>گرد جام می کنون در بزم ما پرواز کن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یک زمان با عشق خود می خور و دلشاد زی</p></div>
<div class="m2"><p>ترکی و مستی مکن چندان که خواهی ناز کن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ناز ترکان خوش بود چندان که در مستی شود</p></div>
<div class="m2"><p>چون شوی مست و خراب آنگاه ناز آغاز کن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ناز و مستی دلبران بر عاشقان زیبا بود</p></div>
<div class="m2"><p>ناز را با مستی اندر دلبری دمساز کن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر شکار خویش خواهی کرد جملهٔ خلق را</p></div>
<div class="m2"><p>زلف را گه چون کمند و گه چو چنگ باز کن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مهر تو گردنکشان را صید تو کرد آنگهی</p></div>
<div class="m2"><p>پادشه امروز گشتی در جهان آواز کن</p></div></div>