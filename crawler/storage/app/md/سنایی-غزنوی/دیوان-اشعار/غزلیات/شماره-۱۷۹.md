---
title: >-
    شمارهٔ  ۱۷۹
---
# شمارهٔ  ۱۷۹

<div class="b" id="bn1"><div class="m1"><p>ساقیا می ده و نمی کم گیر</p></div>
<div class="m2"><p>وز سر زلف خود خمی کم گیر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر به یک دم بمانده‌ای در دام</p></div>
<div class="m2"><p>جستی از دام پس دمی کم گیر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رو که عیسی دلیل و همره تست</p></div>
<div class="m2"><p>ره همی رو تو مریمی کم گیر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عالمی علم بر تو جمع شدست</p></div>
<div class="m2"><p>علم باقیست عالمی کم گیر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز کما بیش بر تو نقصان نیست</p></div>
<div class="m2"><p>چون تو بیشی ز کم کمی کم گیر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بم گسسته ست زیر و زار خوشست</p></div>
<div class="m2"><p>زحمت زخمه را به می کم گیر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر سنایی غمی‌ست بر دل تو</p></div>
<div class="m2"><p>یا غمی باش یا غمی کم گیر</p></div></div>