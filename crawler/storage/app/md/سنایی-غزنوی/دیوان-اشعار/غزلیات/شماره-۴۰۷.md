---
title: >-
    شمارهٔ ۴۰۷
---
# شمارهٔ ۴۰۷

<div class="b" id="bn1"><div class="m1"><p>تا معتکف راه خرابات نگردی</p></div>
<div class="m2"><p>شایستهٔ ارباب کرامات نگردی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از بند علایق نشود نفس تو آزاد</p></div>
<div class="m2"><p>تا بندهٔ رندان خرابات نگردی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در راه حقیقت نشوی قبلهٔ احرار</p></div>
<div class="m2"><p>تا قدوهٔ اصحاب لباسات نگردی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا خدمت رندان نگزینی به دل و جان</p></div>
<div class="m2"><p>شایستهٔ سکان سماوات نگردی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا در صف اول نشوی فاتحهٔ «قل»</p></div>
<div class="m2"><p>اندر صف ثانی چو تحیات نگردی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شه پیل نبینی به مراد دل معشوق</p></div>
<div class="m2"><p>تا در کف عشق شه او مات نگردی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا نیست نگردی چو سنایی ز علایق</p></div>
<div class="m2"><p>نزد فضلا عین مباهات نگردی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>محکم نشود دست تو در دامن تحقیق</p></div>
<div class="m2"><p>تا سوخته راه ملامات نگردی</p></div></div>