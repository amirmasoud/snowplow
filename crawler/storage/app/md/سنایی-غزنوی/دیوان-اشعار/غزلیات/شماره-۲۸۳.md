---
title: >-
    شمارهٔ ۲۸۳
---
# شمارهٔ ۲۸۳

<div class="b" id="bn1"><div class="m1"><p>آمد گه آنکه ساغر آریم</p></div>
<div class="m2"><p>آواز چو عاشقان برآریم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر پشت چمن سمن برآمد</p></div>
<div class="m2"><p>ما روی بر آن سمنبر آریم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در باغ چو بنگریم رویش</p></div>
<div class="m2"><p>جانها به نثار بتگر آریم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اندر ره عاشقی ز باده</p></div>
<div class="m2"><p>گر از سر لاف خود برآریم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با همت خود به عون دردی</p></div>
<div class="m2"><p>از عالم عشق پر برآریم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یک مر صلاح را مگر ما</p></div>
<div class="m2"><p>در ره روش قلندر آریم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون مرکب عاشقی به معنی</p></div>
<div class="m2"><p>اندر صف کم زنان در آریم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر جان و جهان و دین ببازیم</p></div>
<div class="m2"><p>سرپوش زمانه در سر آریم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در خاک بسیط چون سنایی</p></div>
<div class="m2"><p>نعت فلک مدور آریم</p></div></div>