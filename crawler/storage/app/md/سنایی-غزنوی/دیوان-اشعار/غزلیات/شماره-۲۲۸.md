---
title: >-
    شمارهٔ ۲۲۸
---
# شمارهٔ ۲۲۸

<div class="b" id="bn1"><div class="m1"><p>از همت عشق بافتوحم</p></div>
<div class="m2"><p>پا بستهٔ عشق بلفتوحم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بربود ز بوی زلف عقلم</p></div>
<div class="m2"><p>بفزود ز آب روی روحم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از موی سیاه اوست شامم</p></div>
<div class="m2"><p>وز روی نکوی او صبوحم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یک بوسه ازو بیافتم بس</p></div>
<div class="m2"><p>آن بود ز عشق او فتوحم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زان بوسهٔ همچو آب حیوان</p></div>
<div class="m2"><p>اکنون نه سناییم که نوحم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نی نی که برفت نوح آخر</p></div>
<div class="m2"><p>من نوح نیم که روح روحم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آن روز گریخت از سنایی</p></div>
<div class="m2"><p>آن توبه که گفت من نصوحم</p></div></div>