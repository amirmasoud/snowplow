---
title: >-
    شمارهٔ  ۸۹
---
# شمارهٔ  ۸۹

<div class="b" id="bn1"><div class="m1"><p>روزی دل من مرا نشان داد</p></div>
<div class="m2"><p>وز ماه من او خبر به جان داد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتا بشنو نشان ماهی</p></div>
<div class="m2"><p>کو نامهٔ عشق در جهان داد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خورشید رهی او نزیبد</p></div>
<div class="m2"><p>مه بوسه ورا بر آستان داد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یک روز مرا بخواند و بنواخت</p></div>
<div class="m2"><p>و آنگاه به وصل من زبان داد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>برداشت پیاله و دمادم</p></div>
<div class="m2"><p>می داد مرا و بی کران داد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>من دانستم که می بلاییست</p></div>
<div class="m2"><p>لیکن چه کنم مرا چو ز آن داد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از باده چنان مرا بیازرد</p></div>
<div class="m2"><p>کز سر بگرفت و در میان داد</p></div></div>