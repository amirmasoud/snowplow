---
title: >-
    شمارهٔ ۳۴۳
---
# شمارهٔ ۳۴۳

<div class="b" id="bn1"><div class="m1"><p>اسب را باز کشیدی در زین</p></div>
<div class="m2"><p>راه را کردی بر خانه گزین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>راه بیداری آوردی پیش</p></div>
<div class="m2"><p>دل من کردی گمراه و حزین</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بدل و شق بپوشیدی درع</p></div>
<div class="m2"><p>بدل جام گرفتی زوبین</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دست بردی به سوی تیر و کمان</p></div>
<div class="m2"><p>روی دادی به سوی حرب و کمین</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نه براندیشی از کرب زمان</p></div>
<div class="m2"><p>نه ببخشایی بر خلق زمین</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا نبینم رخ چون ماه ترا</p></div>
<div class="m2"><p>بارم از دیده به رخ بر پروین</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون بخسبم ز فراق تو مرا</p></div>
<div class="m2"><p>غم بود بستر و حیرت بالین</p></div></div>