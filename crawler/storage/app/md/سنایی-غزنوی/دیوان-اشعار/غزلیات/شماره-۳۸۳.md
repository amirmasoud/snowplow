---
title: >-
    شمارهٔ ۳۸۳
---
# شمارهٔ ۳۸۳

<div class="b" id="bn1"><div class="m1"><p>گر بگویی عاشقی با ما هم از یک خانه‌ای</p></div>
<div class="m2"><p>با همه کس آشنا با ما چرا بیگانه‌ای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ما چو اندر عشق تو یکرویه چون آیینه‌ایم</p></div>
<div class="m2"><p>تو چرا در دوستی با ما دو سر چون شانه‌ای</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شمع خود خوانی همی ما را و ما در پیش تو</p></div>
<div class="m2"><p>پس ترا پروای جان از چیست گر پروانه‌ای</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جز به عمری در ره ما راست نتوان رفت از آنک</p></div>
<div class="m2"><p>همچو فرزین کجروی در راه نافرزانه‌ای</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عاشقی از بند عقل و عافیت جستن بود</p></div>
<div class="m2"><p>گر چنینی عاشقی ور نیستی دیوانه‌ای</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زان ز وصل ما نداری یکدم آسایش که تو</p></div>
<div class="m2"><p>روز و شب سودای خود رانی دمی مارا نه‌ای</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یارت ای بت صدر دارد زان عزیزست و تو زان</p></div>
<div class="m2"><p>در لگد کوب همه خلقی که در استانه‌ای</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هر کجا صحراست گرم و روشنست از آفتاب</p></div>
<div class="m2"><p>تو از آن در سایه ماندستی که اندر خانه‌ای</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تو برای ما به گرد دام ما گردی ولیک</p></div>
<div class="m2"><p>دام ما را دانه‌ای هست و تو مرد دانه‌ای</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بر خودی عاشق نه بر ما ای سنایی بهر آنک</p></div>
<div class="m2"><p>روز و شب مرد فسون و شعبده و افسانه‌ای</p></div></div>