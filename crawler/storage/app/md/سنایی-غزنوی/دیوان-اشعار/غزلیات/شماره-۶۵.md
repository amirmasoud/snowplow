---
title: >-
    شمارهٔ  ۶۵
---
# شمارهٔ  ۶۵

<div class="b" id="bn1"><div class="m1"><p>کار دل باز ای نگارینا ز بازی در گذشت</p></div>
<div class="m2"><p>شد حقیقت عشق و از حد مجازی در گذشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر به بازی بازی از عشقت همی لافی زدم</p></div>
<div class="m2"><p>کار بازی بازی از لاف و بازی در گذشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اندک اندک دل به راه عشقت ای بت گرم شد</p></div>
<div class="m2"><p>چون ز من پیشی گرفت از اسب تازی در گذشت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سودکی دارد کنون گر گوید ای غازی بدار</p></div>
<div class="m2"><p>تیر چون از شست شد از دست غازی در گذشت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چشم خونخوار تو از قتال سجزی دست برد</p></div>
<div class="m2"><p>زلف دلدوز تو از طرار رازی در گذشت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر چه کشمیریست آن سیمین صنم از حسن خویش</p></div>
<div class="m2"><p>از بت چینی و ماچین و طرازی در گذشت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بی‌نیاز ار داشتی خوشدل سنایی را کنون</p></div>
<div class="m2"><p>این نیاز و خوشدلی و بی‌نیازی در گذشت</p></div></div>