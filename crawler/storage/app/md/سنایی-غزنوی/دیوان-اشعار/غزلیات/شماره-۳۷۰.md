---
title: >-
    شمارهٔ ۳۷۰
---
# شمارهٔ ۳۷۰

<div class="b" id="bn1"><div class="m1"><p>من نه ارزیزم ز کان انگیخته</p></div>
<div class="m2"><p>من عزیزم از فلک بگریخته</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چرخ در بالام گوهر تافته</p></div>
<div class="m2"><p>طبع در پهنام عنبر بیخته</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آسمان رنگم ولیک از روی شکل</p></div>
<div class="m2"><p>آفتابی از هلال آویخته</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از برای کسب آب روی خویش</p></div>
<div class="m2"><p>آبروی خود به عمدا ریخته</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از برای خدمت آزادگان</p></div>
<div class="m2"><p>با همه کس همچو آب آمیخته</p></div></div>