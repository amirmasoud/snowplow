---
title: >-
    شمارهٔ  ۱۱۷
---
# شمارهٔ  ۱۱۷

<div class="b" id="bn1"><div class="m1"><p>معشوق که او چابک و چالاک نباشد</p></div>
<div class="m2"><p>آرام دل عاشق غمناک نباشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از چرخ ستمکاره نباشد به غم و بیم</p></div>
<div class="m2"><p>آن را که چو تو دلبر بی باک نباشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در مرتبه از خاک بسی کم بود آن جان</p></div>
<div class="m2"><p>کو زیر کف پای تو چون خاک نباشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نادان بود آنکس که ترا دید و از آن پس</p></div>
<div class="m2"><p>از مهر دگر خوبان دل پاک نباشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>روی تو و موی تو بسنده‌ست جهان را</p></div>
<div class="m2"><p>گو روز و شب و انجم و افلاک نباشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دامن نزند شادی با جان سنایی</p></div>
<div class="m2"><p>روزی که دلش از غم تو چاک نباشد</p></div></div>