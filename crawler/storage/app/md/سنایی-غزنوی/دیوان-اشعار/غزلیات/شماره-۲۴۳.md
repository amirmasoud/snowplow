---
title: >-
    شمارهٔ ۲۴۳
---
# شمارهٔ ۲۴۳

<div class="b" id="bn1"><div class="m1"><p>ای یار سر مهر و مراعات تو دارم</p></div>
<div class="m2"><p>ای دولت دل خدمت و طاعات تو دارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>طاعات و مراعات ترا فرض شناسم</p></div>
<div class="m2"><p>جان و دل و دین وقف مراعات تو دارم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حاجات تو گر هست به جان و دل و دینم</p></div>
<div class="m2"><p>جان و دل و دین از پی حاجات تو دارم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یک بار مناجات تو در وصل شنیدم</p></div>
<div class="m2"><p>بار دگر امید مناجات تو دارم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر چند به بد قصد کنی جان و سر تو</p></div>
<div class="m2"><p>گر هیچ به بد قصد مکافات تو دارم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر صومعهٔ خویش خرابات کنی تو</p></div>
<div class="m2"><p>من روی همه سوی خرابات تو دارم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ششدر کن و شهمات ببر جان و دل من</p></div>
<div class="m2"><p>کاین هر دو بر ششدر و شهمات تو دارم</p></div></div>