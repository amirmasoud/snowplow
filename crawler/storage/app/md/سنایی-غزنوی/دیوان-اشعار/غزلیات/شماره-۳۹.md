---
title: >-
    شمارهٔ  ۳۹
---
# شمارهٔ  ۳۹

<div class="b" id="bn1"><div class="m1"><p>هر زمان از عشق جانانم وفایی دیگرست</p></div>
<div class="m2"><p>گر چه او را هر نفس بر من جفایی دیگرست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من برو ساعت به ساعت فتنه زانم کز جمال</p></div>
<div class="m2"><p>هر زمان او را به من از نو عنایی دیگرست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر قضا مستولی و قادر شود بر هر کسی</p></div>
<div class="m2"><p>بر من بیچاره عشق او قضایی دیگرست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>باد زلفش از خوشی می‌آورد بوی عبیر</p></div>
<div class="m2"><p>خاک پایش از عزیزی توتیایی دیگرست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از لطیفی آفتاب دیگرست آن دلفریب</p></div>
<div class="m2"><p>از ضعیفی عاشقش گویی هبایی دیگرست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یک زمان از رنج هجرانش دلم خالی مباد</p></div>
<div class="m2"><p>کو مرا جز وصل او راحت فزایی دیگرست</p></div></div>