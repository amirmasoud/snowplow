---
title: >-
    شمارهٔ ۲۲۴
---
# شمارهٔ ۲۲۴

<div class="b" id="bn1"><div class="m1"><p>برندارم دل ز مهرت دلبرا تا زنده‌ام</p></div>
<div class="m2"><p>ور چه آزادم ترا تا زنده‌ام من بنده‌ام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مهر تو با جان من پیوسته گشت اندر ازل</p></div>
<div class="m2"><p>نیست روی رستگاری زو مرا تا زنده‌ام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از هوای هر که جز تو جان و دل بزدوده‌ام</p></div>
<div class="m2"><p>وز وفای تو چو نار از ناردان آگنده‌ام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عشق تو بر دین و دنیا دلبرا بگزیده‌ام</p></div>
<div class="m2"><p>خواجگی در راه تو در خاک راه افگنده‌ام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا بدیدم درج مروارید خندان ترا</p></div>
<div class="m2"><p>بس عقیقا کز دریغ از دیده بپراکنده‌ام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا به من بر لشگر اندوه تو بگشاد دست</p></div>
<div class="m2"><p>از صلاح و نیکنامی دستها بفشانده‌ام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دست دست من بد از اول که در عشق آمدم</p></div>
<div class="m2"><p>کم زدم تا لاجرم در ششدره درمانده‌ام</p></div></div>