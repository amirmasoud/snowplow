---
title: >-
    شمارهٔ  ۱۴۲
---
# شمارهٔ  ۱۴۲

<div class="b" id="bn1"><div class="m1"><p>اقتدا بر عاشقان کن گر دلیلت هست درد</p></div>
<div class="m2"><p>ور نداری درد گرد مذهب رندان مگرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ناشده بی عقل و جان و دل درین ره کی شوی</p></div>
<div class="m2"><p>محرم درگاه عشقی با بت و زنار گرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر که شد مشتاق او یکبارگی آواره شد</p></div>
<div class="m2"><p>هر که شد جویای او در جان و دل منزل نکرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرد باید پاکباز و درد باید مرد سوز</p></div>
<div class="m2"><p>کان نگارین روی عاشق می نخواهد کرد مرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خاکپای خادمان درگه معشوق شو</p></div>
<div class="m2"><p>بوسه را بر خاک ده چون عاشقان از بهر درد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر کرا سودای وصل آن صنم در سر فتاد</p></div>
<div class="m2"><p>اندرین ره سر هم آخر در سر این کار کرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای سنایی رنگ و بویی اندرین ره بیش نیست</p></div>
<div class="m2"><p>اندرین ره رو همی چون رنگ و بو خواهند کرد</p></div></div>