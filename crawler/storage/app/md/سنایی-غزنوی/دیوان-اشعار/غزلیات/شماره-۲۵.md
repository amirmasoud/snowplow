---
title: >-
    شمارهٔ  ۲۵
---
# شمارهٔ  ۲۵

<div class="b" id="bn1"><div class="m1"><p>ای لعبت صافی صفات ای خوشتر از آب حیات</p></div>
<div class="m2"><p>هستی درین آخر زمان این منکران را معجزات</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هم دیده داری هم قدم هم نور داری هم ظلم</p></div>
<div class="m2"><p>در هزل وجد ای محتشم هم کعبه گردی هم منات</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حسن ترا بینم فزون خلق ترا بینم زبون</p></div>
<div class="m2"><p>چون آمد از جنت برون چون تو نگاری بی برات</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در نارم از گلزار تو بیزارم از آزار تو</p></div>
<div class="m2"><p>یک دیدن از دیدار تو خوشتر ز کل کاینات</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر گه که بگشایی دهن گردد جهان پر نسترن</p></div>
<div class="m2"><p>بر تو ثنا گوید چو من ریگ و مطر سنگ و نبات</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عالی چو کعبه کوی تو نه خاکپای روی تو</p></div>
<div class="m2"><p>بر دو لب خوشبوی تو جان را به دل دارد حیات</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>برهان آن نوشین لبت چون روز گرداند شبت</p></div>
<div class="m2"><p>وان خالها بر غبغبت تابان چو از گردون بنات</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بر ما لبت دعوت کنی بر ما سخن حجت کنی</p></div>
<div class="m2"><p>وقتی که جان غارت کنی چون صوفیان در ده صلات</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>باز ار بکشتی عاجزی بنمای از لب معجزی</p></div>
<div class="m2"><p>چون از عزی نبود عزی لا را بزن بر روی لات</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>غمهات بر ما جمله شد بغداد همچون حله شد</p></div>
<div class="m2"><p>یک دیده اینجا دجله شد یک دیده آنجا شد فرات</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>جان سنایی مر ترا از وی حذر کردن چرا</p></div>
<div class="m2"><p>از تو گذر نبود ورا هم در حیات و هم ممات</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ای چون ملک گه سامری وی چون فلک گه ساحری</p></div>
<div class="m2"><p>تا بر تو خوانم یک سری «الباقیات الصالحات»</p></div></div>