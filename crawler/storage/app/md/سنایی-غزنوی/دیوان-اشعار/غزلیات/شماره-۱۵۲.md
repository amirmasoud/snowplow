---
title: >-
    شمارهٔ  ۱۵۲
---
# شمارهٔ  ۱۵۲

<div class="b" id="bn1"><div class="m1"><p>میر خوبان را کنون منشور خوبی در رسید</p></div>
<div class="m2"><p>مملکت بر وی سهی شد ملک بر وی آرمید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نامه آن نامه‌ست کاکنون عاشقی خواهد نوشت</p></div>
<div class="m2"><p>پرده آن پرده‌ست کاکنون عاشقی خواهد درید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دلبران را جان همی بر روی او باید فشاند</p></div>
<div class="m2"><p>نوخطان را می همی بر یاد او باید چشید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آفت جانهای ما شد خط دلبندش ولیک</p></div>
<div class="m2"><p>آفت جان را ز بت رویان به جان باید خرید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گویی اکنون راست شد «والشمس» اندر آسمان</p></div>
<div class="m2"><p>آیت «واللیل» کرد و «الضحاش» اندر کشید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر ز مرد گرد بیجاده‌ش پدید آمد چه شد</p></div>
<div class="m2"><p>خرمی باید که اندر سبزه زیباتر نبید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر چه عمرش بیش گردد بیش گرداند زمان</p></div>
<div class="m2"><p>چون غزلهای سنایی تری اندر وی پدید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کی تبه گرداندش هرگز به دست روزگار</p></div>
<div class="m2"><p>صورتی کایزد برای عشقبازی آفرید</p></div></div>