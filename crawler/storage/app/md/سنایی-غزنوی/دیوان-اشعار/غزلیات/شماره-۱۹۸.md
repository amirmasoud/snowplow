---
title: >-
    شمارهٔ  ۱۹۸
---
# شمارهٔ  ۱۹۸

<div class="b" id="bn1"><div class="m1"><p>الا ای دلربای خوش بیا کامد بهاری خوش</p></div>
<div class="m2"><p>شراب تلخ ما را ده که هست این روزگاری خوش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سزد گر ما به دیدارت بیاراییم مجلس را</p></div>
<div class="m2"><p>چو شد آراسته گیتی به بوی نوبهاری خوش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همی بوییم هر ساعت همی نوشیم هر لحظه</p></div>
<div class="m2"><p>گل اندر بوستانی نو مل اندر مرغزاری خوش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گهی از دست تو گیریم چون آتش می صافی</p></div>
<div class="m2"><p>گهی در وصف تو خوانیم شعر آبداری خوش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کنون در انتظار گل سراید هر شبی بلبل</p></div>
<div class="m2"><p>غزلهای لطیف خوش به نغمه‌های زاری خوش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شود صحرا همه گلشن شود گیتی همه روشن</p></div>
<div class="m2"><p>چو خرم مجلس عالی و باد مشکباری خوش</p></div></div>