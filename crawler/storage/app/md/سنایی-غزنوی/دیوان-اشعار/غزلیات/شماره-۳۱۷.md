---
title: >-
    شمارهٔ ۳۱۷
---
# شمارهٔ ۳۱۷

<div class="b" id="bn1"><div class="m1"><p>خیز ای بت و در کوی خرابی قدمی زن</p></div>
<div class="m2"><p>با شیفتگان سر این راه دمی زن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر عالم تجرید ز تفرید رهی ساز</p></div>
<div class="m2"><p>در بادیهٔ هجر ز حیرت علمی زن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر هر چه ترا نیست ز بهرش مبر انده</p></div>
<div class="m2"><p>وز هر چه ترا هست ز اسباب کمی زن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جمع آر همه تفرقهٔ خویش به جهدت</p></div>
<div class="m2"><p>بر ذات دعاوی ز معانی رقمی زن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از علم و اشارات و عبارات حذر کن</p></div>
<div class="m2"><p>وز زهد و کرامات گذشته بر می زن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از کفر و ز توحید مگو هیچ سخن نیز</p></div>
<div class="m2"><p>پیرامن خود زین دو خطرها حرمی زن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون فرد شدی زین همه احوال به تصدیق</p></div>
<div class="m2"><p>در شاهره فقر و حقیقت قدمی زن</p></div></div>