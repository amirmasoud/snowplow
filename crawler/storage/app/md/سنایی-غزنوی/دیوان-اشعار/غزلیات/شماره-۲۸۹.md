---
title: >-
    شمارهٔ ۲۸۹
---
# شمارهٔ ۲۸۹

<div class="b" id="bn1"><div class="m1"><p>ما همه راه لب آن دلبر یغما زنیم</p></div>
<div class="m2"><p>شکر او را به بوسه هر شبی یغما زنیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هم توان از دو لبش شکر زدن یغما ولیک</p></div>
<div class="m2"><p>هر شبی راه لب آن دلبر یغما زنیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ما چو وامق او چو عذرا ما چو رامین او چو ویس</p></div>
<div class="m2"><p>رطل زیبد در چنین حالی اگر صهبا زنیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شخص را می وار هر شب در بر ویس افگنیم</p></div>
<div class="m2"><p>بوسه وامق‌وار هر دم بر لب عذرا زنیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر بخفتن گاه صحبت در بر ما افگند</p></div>
<div class="m2"><p>لب به بوسه گاه عشرت بر لب او ما زنیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خوش بدست امروز و دی با آن نگارین عیش ما</p></div>
<div class="m2"><p>خوشتر از امروز و دی فردا و پس فردا زنیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر وصال او به جور از ما ستاند روزگار</p></div>
<div class="m2"><p>دست در عدل غیاث‌الدین والدنیا زنیم</p></div></div>