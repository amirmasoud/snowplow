---
title: >-
    شمارهٔ  ۶۲
---
# شمارهٔ  ۶۲

<div class="b" id="bn1"><div class="m1"><p>جام می پر کن که بی جام میم انجام نیست</p></div>
<div class="m2"><p>تا به کام او شوم این کار جز ناکام نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ساقیا ساغر دمادم کن مگر مستی کنم</p></div>
<div class="m2"><p>زان که در هجر دلارامم مرا آرام نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای پسر دی رفت و فردا خود ندانم چون بود</p></div>
<div class="m2"><p>عاشقی ورزیم و زین به در جهان خودکام نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دام دارد چشم ما دامی نهاده بر نهیم</p></div>
<div class="m2"><p>کیست کو هم بسته و پا بستهٔ این دام نیست</p></div></div>