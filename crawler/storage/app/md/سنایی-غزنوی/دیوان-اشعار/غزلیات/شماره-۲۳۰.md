---
title: >-
    شمارهٔ ۲۳۰
---
# شمارهٔ ۲۳۰

<div class="b" id="bn1"><div class="m1"><p>تا من به تو ای بت اقتدی کردم</p></div>
<div class="m2"><p>بر خویش به بی دلی ندی کردم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از بهر دو چشم پر ز سحر تو</p></div>
<div class="m2"><p>دین و دل خویش را فدی کردم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آن وقت بیا که من ز مستوری</p></div>
<div class="m2"><p>در شهر ز خویش زاهدی کردم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همچون تو شدم مغ از دل صافی</p></div>
<div class="m2"><p>خود را ز پی تو ملحدی کردم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در طمع وصال تو به نادانی</p></div>
<div class="m2"><p>مال و تن خویش را سدی کردم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کز رفق سنایی اندرین حالت</p></div>
<div class="m2"><p>از راه مغان ره هدی کردم</p></div></div>