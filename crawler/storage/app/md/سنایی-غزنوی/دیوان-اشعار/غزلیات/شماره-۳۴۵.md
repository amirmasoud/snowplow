---
title: >-
    شمارهٔ ۳۴۵
---
# شمارهٔ ۳۴۵

<div class="b" id="bn1"><div class="m1"><p>چون سخن زان زلف و رخ گویی مگو از کفر و دین</p></div>
<div class="m2"><p>زان که هر جای این دو رنگ آمد نه آن ماند نه این</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نیست با زلفین او پیکار دارالضرب کفر</p></div>
<div class="m2"><p>نیست با رخسان او بی‌شاه دارالملک دین</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خود ز رنگ زلف و نور روی او برساختند</p></div>
<div class="m2"><p>کفر خالی از گمان و دین جمالی از یقین</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خاکپای و خار راهش دیده را و دست را</p></div>
<div class="m2"><p>توده توده سنبلست و دسته دسته یاسمین</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون به کوی اندر خرامد آن چنان باشد ز لطف</p></div>
<div class="m2"><p>پای آن بت ز آستان چون دست موسی ز آستین</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون نقاب از رخ براندازد ز خاتونان خلد</p></div>
<div class="m2"><p>بانگ برخیزد که: هین ای آفرینش آفرین</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>لعبت چین خواندم او را و بد خواندم نه نیک</p></div>
<div class="m2"><p>لاجرم زین شرم شد رویم چو زلفش پر ز چین</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>لعبت چین چون توان خواند آن نگاری را که هست</p></div>
<div class="m2"><p>زیر یک چین از دو زلفش صدهزار ار تنگ چین</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خود حدیث عاشقی بگذار و انصافم بده</p></div>
<div class="m2"><p>کافری نبود چنانی را صفت کردن چنین</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خط او را گر تو خط خوانی خطا باشد که نیست</p></div>
<div class="m2"><p>آن مگر دولت گیای خطهٔ روح‌الامین</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آسمان آن خط بر آن عارض نه بهر آن نوشت</p></div>
<div class="m2"><p>تا من و تو رنجه دل گردیم و آن بت شرمگین</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>لیک چون دید آسمان کز حسن او چون آفتاب</p></div>
<div class="m2"><p>رامش و آرامش و آرایشست اندر زمین</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>حسن را بر چهرهٔ او بنده کرد و بر نوشت</p></div>
<div class="m2"><p>آسمان از مشک بر گردش صلاح‌المسلمین</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>از دو یاقوتش دو چیز طرفه یابم در دو حال</p></div>
<div class="m2"><p>چون بگوید حلقه باشد چون خمش گردد نگین</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>دل چو ز آن لب دور ماند گر بسوزد گو بسوز</p></div>
<div class="m2"><p>موم را ز آتش چه چاره چون جدا شد ز انگبین</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>هر زمان گویی سنایی کیست خیز اندر نگر</p></div>
<div class="m2"><p>هم سنا و هم سنایی را در آن صورت ببین</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>خود سنایی او بود چون بنگری زیرا بر اوست</p></div>
<div class="m2"><p>لب چو باقامت الف ابرو چو نون دندان چو سین</p></div></div>