---
title: >-
    شمارهٔ ۳۳۷
---
# شمارهٔ ۳۳۷

<div class="b" id="bn1"><div class="m1"><p>ای ماه ماهان چند ازین ای شاه شاهان چند ازین</p></div>
<div class="m2"><p>پندت سزای بند گشت آخر نگیری پند ازین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گشتی تو سلطان از کشی تا کی بود این سرکشی</p></div>
<div class="m2"><p>عادت مکن عاشقی کشی توبه بکن یکچند ازین</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با روی خوب و خوی بد از تو کسی کی برخورد</p></div>
<div class="m2"><p>این خوی بد در تو رسد بگریز ای دلبند ازین</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا کی کنی کبر آوری چون عاقبت را بنگری</p></div>
<div class="m2"><p>ترسم پشیمانی خوری ای یار بد پیوند ازین</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اول که نامت برده‌ام صد ضربه از غم خورده‌ام</p></div>
<div class="m2"><p>زان صد یکی نشمرده‌ام آخر شوی خرسند ازین</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای هوش و جان بی‌هشان جان و دل عاشق کشان</p></div>
<div class="m2"><p>از جان ما چد هی نشان روزی اگر پرسند ازین</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از جور تست اندر دعا دست سنایی بر هوا</p></div>
<div class="m2"><p>از وی وفا از تو جفا آخر نگویی چند ازین</p></div></div>