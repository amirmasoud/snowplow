---
title: >-
    شمارهٔ  ۳۶
---
# شمارهٔ  ۳۶

<div class="b" id="bn1"><div class="m1"><p>بر دوزخ هم کفر و هم ایمان تراست</p></div>
<div class="m2"><p>بر دو لب هم درد و هم درمان تراست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر دو صد یعقوب داری زیبدت</p></div>
<div class="m2"><p>کانچه یوسف داشت صد چندان تراست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خندهٔ تو چون دم عیساست کو</p></div>
<div class="m2"><p>هر چه در لب داشت در دندان تراست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چند گویی کان و کان یک ره ببین</p></div>
<div class="m2"><p>کانچه در کانست در ارکان تراست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چند گویی جان و جان یک دم بخند</p></div>
<div class="m2"><p>کانچه در جانست در مرجان تراست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از لطیفی آنت جان خواند از آنک</p></div>
<div class="m2"><p>هر چه آنرا خواند جان بتوان تراست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر زمان گویی همی چوگان من</p></div>
<div class="m2"><p>گوی از آن کیست گر چوگان تراست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون همی دانی که میدان آن تست</p></div>
<div class="m2"><p>گوی هم می دان که در میدان تراست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بنده گر خوبست گر زشت آن تست</p></div>
<div class="m2"><p>عاشق ار دانا و گر نادان تراست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>صورت ار با تو نباشد گو مباش</p></div>
<div class="m2"><p>خاک بر سر جسم را چون جان تراست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>من ترا هرگز بنگذارم ولیک</p></div>
<div class="m2"><p>گر تو بگذاری مرا فرمان تراست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هیچ مرغ آسان سنایی را نیافت</p></div>
<div class="m2"><p>دولتی مرغی که این آسان‌تر است</p></div></div>