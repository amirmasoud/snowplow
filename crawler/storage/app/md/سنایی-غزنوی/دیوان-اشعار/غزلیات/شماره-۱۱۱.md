---
title: >-
    شمارهٔ  ۱۱۱
---
# شمارهٔ  ۱۱۱

<div class="b" id="bn1"><div class="m1"><p>عشق آن معشوق خوش بر عقل و بر ادراک زد</p></div>
<div class="m2"><p>عشق بازی را بکرد و خاک بر افلاک زد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر جمال و چهرهٔ او عقلها را پیرهن</p></div>
<div class="m2"><p>نعرهٔ عشق از گریبان تا به دامن چاک زد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حسن او خورشید و ماه و زهره بر فتراک بست</p></div>
<div class="m2"><p>لطف او در چشم آب و باد و آتش خاک زد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آتش عشقش جنیبتهای زر چون در کشید</p></div>
<div class="m2"><p>آب حیوانش به خدمت چنگ در فتراک زد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شاه عشقش چون یکی بر کد خدای روم تاخت</p></div>
<div class="m2"><p>گفتی افریدون در آمد گرز بر ضحاک زد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زهر او آب رخ تریاک برد و پاک برد</p></div>
<div class="m2"><p>درد او بر لشکر درمان زد و بی‌باک زد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>درد او دیده چو افسر بر سر درمان نهاد</p></div>
<div class="m2"><p>زهر او چون تیغ دل بر تارک تریاک زد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جادوی استاد پیش خاک پای او بسی</p></div>
<div class="m2"><p>بوسه‌های سرنگون بر پایش از ادراک زد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>عقل و جان را همچو شمع و مشعله کرد آنگهی</p></div>
<div class="m2"><p>آتش بی باک را در عقل و جان پاک زد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>می سنایی را همو داد و همو زان پس به جرم</p></div>
<div class="m2"><p>سرنگون چون خوشه کرد و حدبه چوب تاک زد</p></div></div>