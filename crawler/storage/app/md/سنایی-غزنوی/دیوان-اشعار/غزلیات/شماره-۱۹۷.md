---
title: >-
    شمارهٔ  ۱۹۷
---
# شمارهٔ  ۱۹۷

<div class="b" id="bn1"><div class="m1"><p>برخیز و برو باده بیار ای پسر خوش</p></div>
<div class="m2"><p>وین گفت مرا خوار مدار ای پسر خوش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>باده خور و مستی کن و دلداری و عشرت</p></div>
<div class="m2"><p>و اندوه جهان باد شمار ای پسر خوش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رنج و غم بیهوده منه بر دل و بر جان</p></div>
<div class="m2"><p>و آن چت بنخارد بمخار ای پسر خوش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خواهی که بود خاک درت افسر عشاق</p></div>
<div class="m2"><p>در باده فزون کن تو خمار ای پسر خوش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ناموس خرد بشکن و سالوس طریقت</p></div>
<div class="m2"><p>وز هر دو برآور تو دمار ای پسر خوش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زهد و گنه و کفر و هدی را همه در هم</p></div>
<div class="m2"><p>در باز به یک داو قمار ای پسر خوش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا زنده شود مجلس ما از رخ و زلفت</p></div>
<div class="m2"><p>در مجلس ما مشک و گل آر ای پسر خوش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از جان و جوانی نبود شاد سنایی</p></div>
<div class="m2"><p>تا دل نکند بر تو نثار ای پسر خوش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>صد سجدهٔ شکر از دل و از جان به تو آرد</p></div>
<div class="m2"><p>او را ز چه داری تو فگار ای پسر خوش</p></div></div>