---
title: >-
    شمارهٔ  ۳۸
---
# شمارهٔ  ۳۸

<div class="b" id="bn1"><div class="m1"><p>ای صنم در دلبری هم دست و هم دستان تراست</p></div>
<div class="m2"><p>بر دل و جان پادشاهی هم دل و هم جان تراست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هم حیات از لب نمودن هم شفا از رخ چو حور</p></div>
<div class="m2"><p>با دم عیسی و دست موسی عمران تراست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در سر زلف نشان از ظلمت اهریمنست</p></div>
<div class="m2"><p>بر دو رخ از نور یزدان حجت و برهان تراست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای چراغ دل نمی‌دانی که اندر وصل و هجر</p></div>
<div class="m2"><p>دوزخ بی مالک و فردوس بی رضوان تراست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در میان اهل دین و اهل کفر این شور چیست</p></div>
<div class="m2"><p>گر مسلم بر دو رخ هم کفر و هم ایمان تراست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از جمال و از بهایت خیره گردد سرو و مه</p></div>
<div class="m2"><p>سرو بستانی تو داری ماه بی کیوان تراست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آنچه بت‌گر کرد و جادو دید جانا باطل است</p></div>
<div class="m2"><p>در دو مرجان و دو نرگس کار این و آن تراست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر من از حواری جنت یاد نارم شایدم</p></div>
<div class="m2"><p>کانچه حورالعین جنت داشت صد چندان تراست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از همه خوبان عالم گوی بردی شاد باش</p></div>
<div class="m2"><p>داوری حاجت نیاید ای صنم فرمان تراست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در همه جایی سنایی چاکر و مولای تست</p></div>
<div class="m2"><p>گر برانی ور بخوانی ای صنم فرمان تراست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>این چنین صیدی که در دام تو آمد کس ندید</p></div>
<div class="m2"><p>گوی گردون بس که اکنون نوبت میدان تراست</p></div></div>