---
title: >-
    شمارهٔ  ۱۵۴
---
# شمارهٔ  ۱۵۴

<div class="b" id="bn1"><div class="m1"><p>عاشق مشوید اگر توانید</p></div>
<div class="m2"><p>تا در غم عاشقی نمانید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این عشق به اختیار نبود</p></div>
<div class="m2"><p>دانم که همین قدر بدانید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هرگز مبرید نام عاشق</p></div>
<div class="m2"><p>تا دفتر عشق بر نخوانید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آب رخ عاشقان مریزید</p></div>
<div class="m2"><p>تا آب ز چشم خود نرانید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>معشوقه وفا به کس نجوید</p></div>
<div class="m2"><p>هر چند ز دیده خون چکانید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اینست رضای او که اکنون</p></div>
<div class="m2"><p>بر روی زمین یکی نمانید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اینست سخن که گفته آمد</p></div>
<div class="m2"><p>گر نیست درست بر مخوانید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بسیار جفا کشید آخر</p></div>
<div class="m2"><p>او را به مراد او رسانید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اینست نصیحت سنایی</p></div>
<div class="m2"><p>عاشق مشوید اگر توانید</p></div></div>