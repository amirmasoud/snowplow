---
title: >-
    شمارهٔ ۴۲۶
---
# شمارهٔ ۴۲۶

<div class="b" id="bn1"><div class="m1"><p>ای زبدهٔ راز آسمانی</p></div>
<div class="m2"><p>وی حلهٔ عقل پر معانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای در دو جهان ز تو رسیده</p></div>
<div class="m2"><p>آوازهٔ کوس «لن ترانی»</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای یوسف عصر همچو یوسف</p></div>
<div class="m2"><p>افتاده به دست کاروانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>لعل تو به غمزه کفر و دین را</p></div>
<div class="m2"><p>پرداخته مخزن امانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>لعل تو به بوسه عقل و جان را</p></div>
<div class="m2"><p>برساخته عقل جاودانی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>با آفت زلف تو که بیند</p></div>
<div class="m2"><p>یک لحظه زعمر شادمانی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>با آتش عشق تو که یابد</p></div>
<div class="m2"><p>یک قطره ز آب زندگانی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>موسی چکند که بی‌جمالت</p></div>
<div class="m2"><p>نکشد غم غربت شبانی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>فرعون که بود که با کمالت</p></div>
<div class="m2"><p>کوبد در ملک جاودانی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>«آن» گویم «آن» چو صوفیانت</p></div>
<div class="m2"><p>نی نی که تو پادشاه آنی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>جان خوانم جان چو عاشقانت</p></div>
<div class="m2"><p>نی نی که تو کدخدای جانی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>از جملهٔ عاشقان تو نیست</p></div>
<div class="m2"><p>یکتن چو سنایی و تو دانی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>زیبد که سبک نداری او را</p></div>
<div class="m2"><p>گر گه گهکی کند گرانی</p></div></div>