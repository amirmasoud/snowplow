---
title: >-
    شمارهٔ ۳۰۲
---
# شمارهٔ ۳۰۲

<div class="b" id="bn1"><div class="m1"><p>خانهٔ طاعات عمارت مکن</p></div>
<div class="m2"><p>کعبهٔ آفاق زیارت مکن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>امهٔ تلبیس نهفته مخوان</p></div>
<div class="m2"><p>جامهٔ ناموس قضاوت مکن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>قاعدهٔ کار زمانه بدان</p></div>
<div class="m2"><p>هر چه کنی جز به بصارت مکن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سر به خرابات خرابی در آر</p></div>
<div class="m2"><p>صومعه را هیچ عمارت مکن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون همه سرمایهٔ تو مفلسی‌ست</p></div>
<div class="m2"><p>در ره افلاس تجارت مکن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون تو مخنث شدی اندر روش</p></div>
<div class="m2"><p>قصهٔ معراج عبارت مکن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا نشوی در دین قلاش‌وار</p></div>
<div class="m2"><p>خرقهٔ قلاشان غارت مکن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عمر به شادی چو سنایی گذار</p></div>
<div class="m2"><p>کار به سستی و حقارت مکن</p></div></div>