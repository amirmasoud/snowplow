---
title: >-
    شمارهٔ ۲۷۳
---
# شمارهٔ ۲۷۳

<div class="b" id="bn1"><div class="m1"><p>ما را میفگنید که ما اوفتاده‌ایم</p></div>
<div class="m2"><p>در کار عشق تن به بلاها نهاده‌ایم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آهستگی مجوی تو از ماورای هوش</p></div>
<div class="m2"><p>کاکنون به شغل بی دلی اندر فتاده‌ایم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ما بی‌دلیم و بی‌دل هر چه کند رواست</p></div>
<div class="m2"><p>دل را به یادگار به معشوق داده‌ایم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از ما بهر حدیث به آزار چون کشد</p></div>
<div class="m2"><p>ما مردمان بی دل و بی مکر و ساده‌ایم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خصمان ما اگر در خوبی ببسته‌اند</p></div>
<div class="m2"><p>ما در وفاش چندین درها گشاده‌ایم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر بد کنند با ما ما نیکویی کنیم</p></div>
<div class="m2"><p>زیرا که پاک نسبت و آزاده زاده‌ایم</p></div></div>