---
title: >-
    شمارهٔ  ۴۲
---
# شمارهٔ  ۴۲

<div class="b" id="bn1"><div class="m1"><p>توبهٔ من جزع و لعل و زلف و رخسارت شکست</p></div>
<div class="m2"><p>دی که بودم روزه‌دار امروز هستم بت‌پرست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از ترانهٔ عشق تو نور نبی موقوف گشت</p></div>
<div class="m2"><p>وز مغابهٔ جام تو قندیلها بر هم شکست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رمزهای لعل تو دست جوانمردان گشاد</p></div>
<div class="m2"><p>حلقه‌های زلف تو پای خردمندان ببست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ابروی مقرونت ای دلبر کمان اندر کشید</p></div>
<div class="m2"><p>ناوک مژگانت ای جانان دل و جانم بخست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با چنان مژگان و ابرو با چنان رخسار و لب</p></div>
<div class="m2"><p>بود نتوان جز صبور و عاشق و مخمور و مست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پارسایی را بود در عشق تو بازار سست</p></div>
<div class="m2"><p>پادشاهی را بود در وصل تو مقدار پست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جز برای تو نسازم من ز فرق خویش پای</p></div>
<div class="m2"><p>جز به یاد تو نیارم سوی رطل و جام دست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شادی و آرام نبود هر کرا وصل تو نیست</p></div>
<div class="m2"><p>هر کرا وصل تو باشد هر چه باید جمله هست</p></div></div>