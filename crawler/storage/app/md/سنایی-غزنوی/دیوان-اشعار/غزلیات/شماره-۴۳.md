---
title: >-
    شمارهٔ  ۴۳
---
# شمارهٔ  ۴۳

<div class="b" id="bn1"><div class="m1"><p>زان چشم پر از خمار سرمست</p></div>
<div class="m2"><p>پر خون دارم دو دیده پیوست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اندر عجبم که چشم آن ماه</p></div>
<div class="m2"><p>ناخورده شراب چون شود مست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یا بر دل خسته چون زند تیر</p></div>
<div class="m2"><p>بی دست و کمان و قبضه و شست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بس کس که ز عشق غمزهٔ او</p></div>
<div class="m2"><p>زنار چهار کرد بر بست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>برد او دل عاشقان آفاق</p></div>
<div class="m2"><p>پیچند بر آن دو زلف چون شست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون دانست او که فتنه بر خاست</p></div>
<div class="m2"><p>متواری شد به خانه بنشست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یک شهر ازو غریو دارند</p></div>
<div class="m2"><p>زان نیست شگفت جای آن هست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دارند به پای دل ازو بند</p></div>
<div class="m2"><p>دارند به فرق سر ازو دست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تا عزم جفا درست کرد او</p></div>
<div class="m2"><p>دست همه عاشقانش بشکست</p></div></div>