---
title: >-
    شمارهٔ  ۱۱۵
---
# شمارهٔ  ۱۱۵

<div class="b" id="bn1"><div class="m1"><p>چه رنگهاست که آن شوخ دیده نامیزد</p></div>
<div class="m2"><p>که تا مگر دلم از صحبتش بپرهیزد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گهی ز طیره گری نکته‌ای دراندازد</p></div>
<div class="m2"><p>گهی به بلعجبی فتنه‌ای برانگیزد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به هیچ وقت به بازی کرشمه‌ای نکند</p></div>
<div class="m2"><p>که صد هزار دل از غمزه درنیاویزد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گهی کزو به نفورم بر من آید زود</p></div>
<div class="m2"><p>گهش چو خوانم با من به قصد بستیزد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز بهر خصم همی سرمه سازد از دیده</p></div>
<div class="m2"><p>چو دود یافت ز بهر سنایی آمیزد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خبر ندارد از آن کز بلاش نگریزم</p></div>
<div class="m2"><p>که هیچ تشنه ز آب فرات نگریزد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هزار شربت زهر ار ز دست او بخورم</p></div>
<div class="m2"><p>ز عشق نعرهٔ «هل من مزید» برخیزد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نه از غمست که چشمم همی ز راه مژه</p></div>
<div class="m2"><p>هزار دریا پالونه‌وار می‌بیزد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به هر که مردم چشمم نگه کند جز از او</p></div>
<div class="m2"><p>جنایتی شمرد آب ازان سبب ریزد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>جواب آن غزل خواجه بو سعید است این</p></div>
<div class="m2"><p>«مرا دلیست که با عافیت نیامیزد»</p></div></div>