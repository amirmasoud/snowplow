---
title: >-
    شمارهٔ  ۱۵۹
---
# شمارهٔ  ۱۵۹

<div class="b" id="bn1"><div class="m1"><p>زهی حسن و زهی عشق و زهی نور و زهی نار</p></div>
<div class="m2"><p>زهی خط و زهی زلف و زهی مور و زهی مار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به نزدیک من از شق زهی شور و زهی شر</p></div>
<div class="m2"><p>به درگاه تو از حسن زهی کار و زهی بار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به بالا و کمرگاه به زلفین و به مژگان</p></div>
<div class="m2"><p>زهی تیر و زهی تار و زهی قیر و زهی قار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یکی گلبنی از روح گلت عقل و گلت عشق</p></div>
<div class="m2"><p>زهی بیخ و زهی شاخ و زهی برگ و زهی بار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بهشت از تو و گردون حواس از تو و ارکان</p></div>
<div class="m2"><p>زهی هشت و زهی هفت زهی پنج و زهی چار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>برین فرق و برین دست برین روی و برین دل</p></div>
<div class="m2"><p>زهی خاک و زهی باد زهی آب و زهی نار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>میان خرد و روح دو زلفین و دو چشمت</p></div>
<div class="m2"><p>زهی حل و زهی عقد زهی گیر و زهی دار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>همه دل سوختگان را از سر زلف و زنخدانت</p></div>
<div class="m2"><p>زهی جاه و زهی چاه زهی بند و زهی بار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به نزدیک سناییست ز عشق تو و غیرت</p></div>
<div class="m2"><p>زهی نام و زهی ننگ زهی فخر و زهی عار</p></div></div>