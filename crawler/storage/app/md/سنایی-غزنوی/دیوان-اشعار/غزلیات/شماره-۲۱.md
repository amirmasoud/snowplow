---
title: >-
    شمارهٔ  ۲۱
---
# شمارهٔ  ۲۱

<div class="b" id="bn1"><div class="m1"><p>ای از بنفشه ساخته بر گل مثال‌ها</p></div>
<div class="m2"><p>در آفتاب کرده ز عنبر کلال‌ها</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هاروت تو ز معجزه دارد دلیل‌ها</p></div>
<div class="m2"><p>ماروت تو ز شعبده دارد مثال‌ها</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هرروز بامداد برآیی و بر زنی</p></div>
<div class="m2"><p>از مشک سوده بر سمن تازه‌خال‌ها</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای کاشکی ز خواسته مفلس نبودمی</p></div>
<div class="m2"><p>تا کردمی فدای جمال تو مال‌ها</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نی بر امید فضل گذارم همی جهان</p></div>
<div class="m2"><p>آخر کند خدای دگرگونه حال‌ها</p></div></div>