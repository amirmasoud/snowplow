---
title: >-
    شمارهٔ  ۱۳۷
---
# شمارهٔ  ۱۳۷

<div class="b" id="bn1"><div class="m1"><p>هر زمان از عشقت ای دلبر دل من خون شود</p></div>
<div class="m2"><p>قطره‌ها گردد ز راه دیدگان بیرون شود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر ز بی صبری بگویم راز دل با سنگ و روی</p></div>
<div class="m2"><p>روی را تن آب گردد سنگ را دل خون شود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز آتش و درد فراقت این نباشد بس عجب</p></div>
<div class="m2"><p>گر دل من چون جحیم و دیده چون جیحون شود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بار اندوهان من گردون کجا داند کشید</p></div>
<div class="m2"><p>خاصه چون فریادم از بیداد بر گردون شود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در غم هجران و تیمار جدایی جان من</p></div>
<div class="m2"><p>گاه چون ذوالکفل گردد گاه چون ذوالنون شود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در دل از مهرت نهالی کشته‌ام کز آب چشم</p></div>
<div class="m2"><p>هر زمانی برگ و شاخ و بیخ او افزون شود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا تو در حسن و ملاحت همچنان لیلی شدی</p></div>
<div class="m2"><p>عاشق مسکینت ای دلبر همی مجنون شود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خاک درگاه تو ای دلبر اگر گیرد هوا</p></div>
<div class="m2"><p>توتیای حور و چتر شاه سقلاطون شود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ای شده ماه تمام از غایت حسن و جمال</p></div>
<div class="m2"><p>چاکر از هجران رویت «عادکالعرجون» شود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آن دلی کز خلق عالم دارد امیدی به تو</p></div>
<div class="m2"><p>چون ز تو نومید گردد ماهرویا چون شود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چون سنایی مدحتت گوید ز روی تهنیت</p></div>
<div class="m2"><p>لفظ اسرار الاهی در دلش معجون شود</p></div></div>