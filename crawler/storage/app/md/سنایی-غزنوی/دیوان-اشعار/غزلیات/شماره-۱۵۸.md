---
title: >-
    شمارهٔ  ۱۵۸
---
# شمارهٔ  ۱۵۸

<div class="b" id="bn1"><div class="m1"><p>مارا مدار خوار که ما عاشقیم و زار</p></div>
<div class="m2"><p>بیمار و دلفگار و جدا مانده از نگار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ما را مگوی سرو که ما رنج دیده‌ایم</p></div>
<div class="m2"><p>از گشت آسمان وز آسیب روزگار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زین صعبتر چه باشد زین بیشتر که هست</p></div>
<div class="m2"><p>بیماری و غریبی و تیمار و هجر یار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رنج دگر مخواه و برین بر فزون مجوی</p></div>
<div class="m2"><p>ما را بسست اینکه برو آمدست کار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر ما حلال گشت غم و ناله و خروش</p></div>
<div class="m2"><p>چونان که شد حرام می نوش خوشگوار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ما را به نزد هیچ کسی زینهار نیست</p></div>
<div class="m2"><p>خواهیم زینهار به روزی هزار بار</p></div></div>