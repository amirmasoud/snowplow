---
title: >-
    شمارهٔ ۴۲۰
---
# شمارهٔ ۴۲۰

<div class="b" id="bn1"><div class="m1"><p>لولو خوشاب من از چنگ شد یکبارگی</p></div>
<div class="m2"><p>لالهٔ سیراب من بی‌رنگ شد یکبارگی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دلبری را من به چنگ آورده بودم در جهان</p></div>
<div class="m2"><p>ای دریغا دلبرم کز چنگ شد یکبارگی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جنگها بودی میان ما و گاهی آشتی</p></div>
<div class="m2"><p>آشتی این بار الحق جنگ شد یکبارگی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بود نام و ننگ ما را پیش ازین هر جایگاه</p></div>
<div class="m2"><p>این بتر کامروز نامم ننگ شد یکبارگی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با رخ و اشکی چو زر سیماب و من چون موم نرم</p></div>
<div class="m2"><p>کز دل چون سنگ آن بت سنگ شد یکبارگی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>این جهان روشن اندر هجر آن زیبا پسر</p></div>
<div class="m2"><p>بر سنایی تیره گشت و تنگ شد یکبارگی</p></div></div>