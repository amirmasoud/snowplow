---
title: >-
    شمارهٔ  ۱۹۴
---
# شمارهٔ  ۱۹۴

<div class="b" id="bn1"><div class="m1"><p>ای سنایی جان ده و در بند کام دل مباش</p></div>
<div class="m2"><p>راه رو چون زندگان چون مرده بر منزل مباش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون نپاشی آب رحمت نار زحمت کم فروز</p></div>
<div class="m2"><p>ور نباشی خاک معنی آب بی حاصل مباش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رافت یاران نباشی آفت ایشان مشو</p></div>
<div class="m2"><p>سیرت حق چون نباشی صورت باطل مباش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در میان عارفان جز نکتهٔ روشن مگوی</p></div>
<div class="m2"><p>در کتاب عاشقان جز آیت مشکل مباش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در منای قرب یاران جان اگر قربان کنی</p></div>
<div class="m2"><p>جز به تیغ مهر او در پیش او بسمل مباش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر همی خواهی که با معشوق در هودج بوی</p></div>
<div class="m2"><p>با عدو و خصم او همواره در محمل مباش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر شوی جان جز هوای دوست رامسکن مشو</p></div>
<div class="m2"><p>ور شوی دل جز نگار عشق را قابل مباش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>روی چون زی کعبه کردی رای بتخانه مکن</p></div>
<div class="m2"><p>دشمنان دوست را جز حنظل قاتل مباش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در نهاد تست با تو دشمن معشوق تو</p></div>
<div class="m2"><p>مانع او گر نه‌ای باری بدو مایل مباش</p></div></div>