---
title: >-
    شمارهٔ  ۵۷
---
# شمارهٔ  ۵۷

<div class="b" id="bn1"><div class="m1"><p>عشق بازیچه و حکایت نیست</p></div>
<div class="m2"><p>در ره عاشقی شکایت نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حسن معشوق را چو نیست کران</p></div>
<div class="m2"><p>درد عشاق را نهایت نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مبر این ظن که عشق را به جهان</p></div>
<div class="m2"><p>جز به دل بردنش ولایت نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رایت عشق آشکارا به</p></div>
<div class="m2"><p>زان که در عشق روی و رایت نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عالم علم نیست عالم عشق</p></div>
<div class="m2"><p>رویت صدق چون روایت نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر که عاشق شناسد از معشوق</p></div>
<div class="m2"><p>قوت عشق او به غایت نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر چه داری چو دل بباید باخت</p></div>
<div class="m2"><p>عاشقی را دلی کفایت نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به هدایت نیامدست از کفر</p></div>
<div class="m2"><p>هر کرا کفر چون هدایت نیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کس به دعوی به دوستی نرسد</p></div>
<div class="m2"><p>چون ز معنی درو سرایت نیست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نیک بشناس کانچه مقصودست</p></div>
<div class="m2"><p>بجز از تحفه و عنایت نیست</p></div></div>