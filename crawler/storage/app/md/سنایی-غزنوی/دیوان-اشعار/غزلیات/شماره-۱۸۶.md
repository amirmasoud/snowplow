---
title: >-
    شمارهٔ  ۱۸۶
---
# شمارهٔ  ۱۸۶

<div class="b" id="bn1"><div class="m1"><p>ای من غریب کوی تو از کوی تو بر من عسس</p></div>
<div class="m2"><p>حیلت چه سازم تا مگر با تو برآرم یک نفس</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر من به کویت بگذرم بر آب و آتش بسترم</p></div>
<div class="m2"><p>ترسم ز خصمت چون پرم گیتی بود بر من قفس</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در جستنش روز و شبان گشتم قرین اندهان</p></div>
<div class="m2"><p>پایم ببوسد این جهان گر بر تو یابم دسترس</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از عشق تو قارون منم غرقه در آب و خون منم</p></div>
<div class="m2"><p>لیلی تویی مجنون منم در کار تو بسته هوس</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آن شب که ما پنهان دو تن سازیم حالی ز انجمن</p></div>
<div class="m2"><p>باشیم در یک پیرهن ما را کجا گیرد عسس</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خواهی همی دیدن چنین با تو بوم دایم قرین</p></div>
<div class="m2"><p>بینم ز بخت همنشین وصلت ز پیش و هجر پس</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون در کنار آرم ترا از دست نگذارم ترا</p></div>
<div class="m2"><p>چون جان و دل دارم ترا این آرزویم نیست بس</p></div></div>