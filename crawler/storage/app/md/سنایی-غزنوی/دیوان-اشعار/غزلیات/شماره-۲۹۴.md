---
title: >-
    شمارهٔ ۲۹۴
---
# شمارهٔ ۲۹۴

<div class="b" id="bn1"><div class="m1"><p>تماشا را یکی بخرام در بستان جان ای جان</p></div>
<div class="m2"><p>ببین در زیر پای خویش جان افشان جان ای جان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نخواهد جان دگر جانی اگر صد جان برافشاند</p></div>
<div class="m2"><p>که بس باشد قبول تو بقای جان جان ای جان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ترا یارست بس در جان ز بهر آنکه نشناسد</p></div>
<div class="m2"><p>ز خوبان جز تو در عالم همی درمان جان ای جان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز بهر چشم خوب تو برای دفع چشم بد</p></div>
<div class="m2"><p>کمال عافیت باشد همه قربان جان ای جان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از آن تا در دل و دیده گهر جز عشق تو نبود</p></div>
<div class="m2"><p>برون روید گهر هر دم ز بحر و کان جان ای جان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همه عالم چو حرف «ن» از آن در خدمتت مانده</p></div>
<div class="m2"><p>که از کل نکورویان تویی خاص آن جان ای جان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز بهر سرخ رویی جان چه باشد گر به یک غمزه</p></div>
<div class="m2"><p>ز خوبان جان براندایی تو در میدان جان ای جان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به نور روی تست اکنون همه توحید عقل من</p></div>
<div class="m2"><p>به کفر زلف تست اکنون همه ایمان جان ای جان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سنایی وار در عالم ز بهر آبروی خود</p></div>
<div class="m2"><p>سنایی خاکپای تست سر دیوان جان ای جان</p></div></div>