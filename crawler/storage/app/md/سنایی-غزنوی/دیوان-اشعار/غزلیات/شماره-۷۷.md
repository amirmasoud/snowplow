---
title: >-
    شمارهٔ  ۷۷
---
# شمارهٔ  ۷۷

<div class="b" id="bn1"><div class="m1"><p>دوش رفتم به سر کوی به نظارهٔ دوست</p></div>
<div class="m2"><p>شب هزیمت شده دیدم ز دو رخسارهٔ دوست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از پی کسب شرف پیش بناگوش و لبش</p></div>
<div class="m2"><p>ماه دیدم رهی و زهره سما کارهٔ دوست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گوشها گشته شکر چین که همی ریخت ز نطق</p></div>
<div class="m2"><p>حرفهای شکرین از دو شکر پارهٔ دوست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چشمهای همه کس گشته تماشاگه جان</p></div>
<div class="m2"><p>نز پی بلعجبی از پی نظارهٔ دوست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پیش یکتا مژهٔ چشم چو آهوش ز ضعف</p></div>
<div class="m2"><p>شده شیران جهان ریشه‌ای از شارهٔ دوست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کرده از شکل عزب خانهٔ زنبور از غم</p></div>
<div class="m2"><p>دل عشاق جهان غمزهٔ خونخوارهٔ دوست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر زمان مدعی را ز غرور دل خویش</p></div>
<div class="m2"><p>تازه خونی حذر اندر خم هر تارهٔ دوست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون به سیاره شدی از پی چندین چو فلک</p></div>
<div class="m2"><p>از ستاره شده آراسته سیارهٔ دوست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>لب نوشینش بهم کرده بر نظم بقاش</p></div>
<div class="m2"><p>داد نوشروان با چشم ستمگارهٔ دوست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دوش روزیم پدید آمده از تربیتش</p></div>
<div class="m2"><p>بازم امروز شبی از غم بی‌غارهٔ دوست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چه کند قصه سنایی که ز راه لب و زلف</p></div>
<div class="m2"><p>یک جهان دیده پر آوازهٔ آوارهٔ دوست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هست پروارهٔ او را رهی از بام فلک</p></div>
<div class="m2"><p>همت شاه جهان ساکن پروارهٔ دوست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>شاه بهرامشه آن شه که همیشه کف او</p></div>
<div class="m2"><p>سبب آفت دشمن بود و چارهٔ دوست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>زخم و رحم و بد و نیکش ز ره کون و فساد</p></div>
<div class="m2"><p>تا ابد رخنهٔ دشمن بود و یارهٔ دوست</p></div></div>