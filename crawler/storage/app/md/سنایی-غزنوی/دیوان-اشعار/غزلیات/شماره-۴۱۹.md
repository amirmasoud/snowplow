---
title: >-
    شمارهٔ ۴۱۹
---
# شمارهٔ ۴۱۹

<div class="b" id="bn1"><div class="m1"><p>ای سنایی چو تو در بند دل و جان باشی</p></div>
<div class="m2"><p>کی سزاوار هوای رخ جانان باشی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در دریا تو چگونه به کف آری که همی</p></div>
<div class="m2"><p>به لب جوی چو اطفال هراسان باشی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون به ترک دل و جان گفت نیاری آن به</p></div>
<div class="m2"><p>که شوی دور ازین کوی و تن آسان باشی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا تو فرمانبر چوگان سواران نشوی</p></div>
<div class="m2"><p>نیست ممکن که تو اندر خور میدان باشی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کار بر بردن چوگان نبود صنعت تو</p></div>
<div class="m2"><p>تو همان به که اسیر خم چوگان باشی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به عصایی و گلیمی که تو داری پسرا</p></div>
<div class="m2"><p>تو همی خواهی چون موسی عمران باشی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خواجهٔ ما غلطی کردست این راه مگر</p></div>
<div class="m2"><p>خود نه بس آنکه نمیری و مسلمان باشی</p></div></div>