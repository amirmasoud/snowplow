---
title: >-
    شمارهٔ ۲۶۸
---
# شمارهٔ ۲۶۸

<div class="b" id="bn1"><div class="m1"><p>بی صحبت تو جهان نخواهم</p></div>
<div class="m2"><p>بی خشنودیت جان نخواهم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر جان و روان من بخواهی</p></div>
<div class="m2"><p>یک دم زدنت امان نخواهم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جان را بدهم به خدمت تو</p></div>
<div class="m2"><p>من خدمت رایگان نخواهم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رضوان و بهشت و حور و عین را</p></div>
<div class="m2"><p>بی روی تو جاودان نخواهم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر من تو نشان خویش کردی</p></div>
<div class="m2"><p>حقا که جز این نشان نخواهم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بیگانه بود میان ما جان</p></div>
<div class="m2"><p>بیگانه درین میان نخواهم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>من عشق تو کردم آشکارا</p></div>
<div class="m2"><p>عشق چو تویی نهان نخواهم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هر گه که مرا تو یار باشی</p></div>
<div class="m2"><p>من یاری این و آن نخواهم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تو سودی و دیگران زیانند</p></div>
<div class="m2"><p>تا سود بود زیان نخواهم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>اکنون که مرا عیان یقین شد</p></div>
<div class="m2"><p>زین پس به جز از عیان نخواهم</p></div></div>