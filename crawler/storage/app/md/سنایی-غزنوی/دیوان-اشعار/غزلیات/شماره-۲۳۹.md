---
title: >-
    شمارهٔ ۲۳۹
---
# شمارهٔ ۲۳۹

<div class="b" id="bn1"><div class="m1"><p>من نصیب خویش دوش از عمر خود برداشتم</p></div>
<div class="m2"><p>کز سمن بالین و از شمشاد بستر داشتم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>داشتم در بر نگاری را که از دیدار او</p></div>
<div class="m2"><p>پایهٔ تخت خود از خورشید برتر داشتم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نرگس و شمشاد و سوسن مشک و سیم و ماه و گل</p></div>
<div class="m2"><p>تا به هنگام سحر هر هفت در بر داشتم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر نهاده بر بر چون سیم و سوسن داشتم</p></div>
<div class="m2"><p>لب نهاده بر لب چون شیر و شکر داشتم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دست او بر گردن من همچو چنبر بود و من</p></div>
<div class="m2"><p>دست خود در گردن او همچو چنبر داشتم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بامدادان چون نگه کردم بسی فرقی نبود</p></div>
<div class="m2"><p>چنیر از زر داشت او سوسن ز عنبر داشتم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون موذن گفت یک الله اکبر کافرم</p></div>
<div class="m2"><p>گر امید آن دگر الله اکبر داشتم</p></div></div>