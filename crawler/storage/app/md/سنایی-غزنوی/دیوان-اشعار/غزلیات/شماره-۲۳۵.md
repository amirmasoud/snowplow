---
title: >-
    شمارهٔ ۲۳۵
---
# شمارهٔ ۲۳۵

<div class="b" id="bn1"><div class="m1"><p>پسرا خیز تا صبوح کنیم</p></div>
<div class="m2"><p>راح را همنشین روح کنیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مفلسانیم یک زمان بگذار</p></div>
<div class="m2"><p>از شرابی دو تا فتوح کنیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>باده نوشیم بی ریا از آنک</p></div>
<div class="m2"><p>با ریا توبهٔ نصوح کنیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حال با شعر فرخی آریم</p></div>
<div class="m2"><p>رقص بر شعر بلفتوح کنیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ور بود زحمتی ز ناجنسی</p></div>
<div class="m2"><p>به نیازی دعای نوح کنیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ور سنایی هنوز خواهد خفت</p></div>
<div class="m2"><p>پیش ازو ما همی صبوح کنیم</p></div></div>