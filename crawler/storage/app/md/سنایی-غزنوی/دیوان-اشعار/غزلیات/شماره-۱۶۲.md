---
title: >-
    شمارهٔ  ۱۶۲
---
# شمارهٔ  ۱۶۲

<div class="b" id="bn1"><div class="m1"><p>ای نهاده بر گل از مشک سیه پیچان دو مار</p></div>
<div class="m2"><p>هین که از عالم برآورد آن دو مار تو دمار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روی تو در هر دلی افروخته شمع و چراغ</p></div>
<div class="m2"><p>زلف تو در هر تنی جان سوخته پروانه‌وار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر کجا بوییست خطت تاخته آنجا سپاه</p></div>
<div class="m2"><p>هر کجا رنگیست خالت ساخته آنجا قرار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آتش عشقت ببرده عالمی را آبروی</p></div>
<div class="m2"><p>باد هجرانت نشانده کشوری را خاکسار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا ترا بر یاسمین رست از بنفشه برگ مورد</p></div>
<div class="m2"><p>عاشقان را زعفران رست از سمن بر لاله‌زار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یوسف عصر ار نه‌ای پس چون که اندر عشق تو</p></div>
<div class="m2"><p>خونفشان یعقوب بینم هر زمانی صدهزار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ماه را مانی غلط کردم که مر خورشید را</p></div>
<div class="m2"><p>نورمند از خاک پای تست نورانی عذار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>قیروان عشوه بگذارند غواصان دهر</p></div>
<div class="m2"><p>گر نهنگ عشق تو بخرامد از دریای قار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر براندازی نقاب از روی روح افزای خود</p></div>
<div class="m2"><p>رخت بردارد ز کیهان زحمت لیل و نهار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هر که بر روی تو باشد عاشق ای جان جهان</p></div>
<div class="m2"><p>با جهان جان نباشد بود او را هیچ کار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>عالم کون و فساد از کفر و دین آراسته‌ست</p></div>
<div class="m2"><p>عالم عشق از دل بریان و چشم اشکبار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>در جهان عشق ازین رمز و حکایت هیچ نیست</p></div>
<div class="m2"><p>کاین مزخرف پیکران گویند بر سرهای دار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>وای اگر دستی برآرد در جهان انصاف تو</p></div>
<div class="m2"><p>در همه صحرای جان یک تن نماند پایدار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بر تو کس در می‌نگنجد تالی الا الله چو لا</p></div>
<div class="m2"><p>حاجبی دارد کشیده تیغ در ایوان نار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>لاف گویان اناالله را ببین در عشق خویش</p></div>
<div class="m2"><p>بر بساط عشق بنهاده جبین اختیار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>من نه تنها عاشقم بر تو که بر هفت آسمان</p></div>
<div class="m2"><p>کشته هست از عشق تو چندان که ناید در شمار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>من شناسم مر ترا کز هفتمین چرخ آمدم</p></div>
<div class="m2"><p>بچهٔ عشق ترا پرورده بر دوش و کنار</p></div></div>