---
title: >-
    شمارهٔ  ۲۳
---
# شمارهٔ  ۲۳

<div class="b" id="bn1"><div class="m1"><p>فریاد از آن دو چشمک جادوی دلفریب</p></div>
<div class="m2"><p>فریاد از آن دو کافر غازی با نهیب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این همبر دو ترکش دلگیر جان ستان</p></div>
<div class="m2"><p>وان پیش دو شمامهٔ کافور یا دو سیب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بردوش غایه کش او زهره می‌رود</p></div>
<div class="m2"><p>چون کیقباد و قیصر پانصدش در رکیب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یوسف نبود هرگز چون او به نیکویی</p></div>
<div class="m2"><p>چون سامری هزارش چاکر گه فریب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آسیب عاشقی و غم عشق و گمرهی</p></div>
<div class="m2"><p>تا روی او بدید پس آن طرفه‌ها و زیب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>غمخانه برگزید و ره عشق و گمرهی</p></div>
<div class="m2"><p>هر روز می برآرد نوعی دگر ز جیب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بسترد و گفت چون که سنایی همه ز جهل</p></div>
<div class="m2"><p>بنبشت در هوای غم عشق صد کتیب</p></div></div>