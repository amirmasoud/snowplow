---
title: >-
    شمارهٔ  ۲۰۰
---
# شمارهٔ  ۲۰۰

<div class="b" id="bn1"><div class="m1"><p>چه رسمست آن نهادن زلف بر دوش</p></div>
<div class="m2"><p>نمودن روز را در زیر شب پوش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گه از بادام کردن جعبهٔ نیش</p></div>
<div class="m2"><p>گه از یاقوت کردن چشمهٔ نوش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>برآوردن برای فتنهٔ خلق</p></div>
<div class="m2"><p>هزاران صبحدم از یک بناگوش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تو خورشیدی از آن پیش تو آرند</p></div>
<div class="m2"><p>فلک را از مه نو حلقه در گوش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پری و سرو و خورشیدی ولیکن</p></div>
<div class="m2"><p>قدح گیر و کمربند و قباپوش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گل و مه پیش تو بر منبر حسن</p></div>
<div class="m2"><p>همه آموخته کرده فراموش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سنایی را خریدستی دل و جان</p></div>
<div class="m2"><p>اگر صد جان دهندت باز مفروش</p></div></div>