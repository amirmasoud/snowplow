---
title: >-
    شمارهٔ  ۳
---
# شمارهٔ  ۳

<div class="b" id="bn1"><div class="m1"><p>بندهٔ یک دل منم بند قبای ترا</p></div>
<div class="m2"><p>چاکر یکتا منم زلف دو تای ترا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خاک مرا تا به باد بر ندهد روزگار</p></div>
<div class="m2"><p>من ننشانم ز جان باد هوای ترا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کاش رخ من بدی خاک کف پای تو</p></div>
<div class="m2"><p>بوسه مگر دادمی من کف پای ترا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر بود ای شوخ چشم رای تو بر خون من</p></div>
<div class="m2"><p>بر سر و دیده نهم رایت رای ترا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تیر جفای تو هست دلکش جان دوز من</p></div>
<div class="m2"><p>جعبه ز سینه کنم تیر جفای ترا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بار نیامد دلم در شکن زلف تو</p></div>
<div class="m2"><p>گر نه به گردن کشم بار بلای ترا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بنده سنایی ترا بندگی از جان کند</p></div>
<div class="m2"><p>گوی کلاه ترا بند قبای ترا</p></div></div>