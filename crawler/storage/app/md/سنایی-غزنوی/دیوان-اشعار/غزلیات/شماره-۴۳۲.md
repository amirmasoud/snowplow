---
title: >-
    شمارهٔ ۴۳۲
---
# شمارهٔ ۴۳۲

<div class="b" id="bn1"><div class="m1"><p>ربی و ربک‌الله ای ماه تو چه ماهی</p></div>
<div class="m2"><p>کافزون شوی ولیکن هرگز چنو نکاهی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مه نیستی که مهری زیرا که هست مه را</p></div>
<div class="m2"><p>گاه از برونش زردی گاه از درون سیاهی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با مایهٔ جمالت ناید ز مهر شمعی</p></div>
<div class="m2"><p>در سایهٔ سلیمان ناید ز دیو شاهی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آنجا که قدت آید ناید ز سر و سروی</p></div>
<div class="m2"><p>آنجا که خدت آید ناید ز ماه ماهی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از جزع عقل عقلی و ز لعل شمع شمعی</p></div>
<div class="m2"><p>از خنده جان جانی وز غمزه جاه جاهی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر روز صبح صادق از غیرت جمالت</p></div>
<div class="m2"><p>بر خود همی بدرد پیراهن پگاهی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گرد سم سمندت بر گلشن سمایی</p></div>
<div class="m2"><p>در زلف جعد حوران مشکیست جایگاهی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>حقا و ثم حقا آنگه که بزم سازی</p></div>
<div class="m2"><p>روح‌الامین نوازد در مجلست ملاهی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خوش‌خوتر از تو خویی روح‌القدس ندیدست</p></div>
<div class="m2"><p>از قایل الاهی تا قابل گیاهی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آویختی به عمدا از بهر بند دل‌ها</p></div>
<div class="m2"><p>زنجیر بی‌گناهان از جای بی‌گناهی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>در جنب آبرویت آدم که بود؟ خاکی</p></div>
<div class="m2"><p>با قدر قد و مویت یوسف که بود چاهی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>فراش خاک کویت پاکان آسمانی</p></div>
<div class="m2"><p>قلاش آبرویت پیران خانقاهی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>در تاب‌های زلفت بنگر به خط ابرو</p></div>
<div class="m2"><p>ترغیب اگر ندیدی در صورت مناهی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>عقلم همی‌نداند تفسیر خطت آری</p></div>
<div class="m2"><p>نامحرمی چه داند شرح خط الاهی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>در ملک خوبرویی بس نادری ولیکن</p></div>
<div class="m2"><p>نادرتر آنکه داری ملکی به بی‌کلاهی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>با خنده و کرشمه آنجا که روی آری</p></div>
<div class="m2"><p>هم ماه و هم سپهری هم شاه و هم سپاهی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>آهم شکست در بر ز آن دم که دید چشمم</p></div>
<div class="m2"><p>آن حسن بی‌تباهی و آن لطف بی‌تناهی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ز آن آه بر نیارد زیرا که هست پنهان</p></div>
<div class="m2"><p>آه از درون جانش تو در میان آهی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>در جل کشید جان را در خدمتت سنایی</p></div>
<div class="m2"><p>خواهی کنون بر آن را خواه آن زمان که خواهی</p></div></div>