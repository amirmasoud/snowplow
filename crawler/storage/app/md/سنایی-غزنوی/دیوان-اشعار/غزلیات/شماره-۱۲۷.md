---
title: >-
    شمارهٔ  ۱۲۷
---
# شمارهٔ  ۱۲۷

<div class="b" id="bn1"><div class="m1"><p>گر سال عمر من به سر آید روا بود</p></div>
<div class="m2"><p>اندی که سال عیش همیشه به جا بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پایان عاشقی نه پدیدست تا ابد</p></div>
<div class="m2"><p>پس سال و ماه و وقت در او از کجا بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای وای و حسرتا که اگر عشق یک نفس</p></div>
<div class="m2"><p>در سال و ماه عمر ز جانم جدا بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای آمده به طمع وصال نگار خویش</p></div>
<div class="m2"><p>نشنیده‌ای که عشق برای بلا بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پروانهٔ ضعیف کند جان فدای شمع</p></div>
<div class="m2"><p>تا پیش شمع یک نظرش را سنا بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دیدار وی همان بود و سوختن همان</p></div>
<div class="m2"><p>گویی فنای وی همه اندر بقا بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آن را که زندگیش به عشق‌ست مرگ نیست</p></div>
<div class="m2"><p>هرگز گمان مبر که مر او را فنا بود</p></div></div>