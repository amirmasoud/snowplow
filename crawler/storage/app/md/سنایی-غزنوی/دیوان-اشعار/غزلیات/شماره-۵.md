---
title: >-
    شمارهٔ  ۵
---
# شمارهٔ  ۵

<div class="b" id="bn1"><div class="m1"><p>باز تابی در ده آن زلفین عالم سوز را</p></div>
<div class="m2"><p>باز آبی بر زن آن روی جهان افروز را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>باز بر عشاق صوفی طبع صافی جان گمار</p></div>
<div class="m2"><p>آن دو صف جادوی شوخ دلبر جان دوز را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>باز بیرون تاز در میدان عقل و عافیت</p></div>
<div class="m2"><p>آن سیه پوشان کفر انگیز ایمان‌سوز را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سر برآوردند مشتی گوشه گشته چون کمان</p></div>
<div class="m2"><p>باز در کار آر نوک ناوک کین توز را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>روزها چون عمر بد خواه تو کوتاهی گرفت</p></div>
<div class="m2"><p>پاره‌ای از زلف کم کن مایه‌ای ده روز را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آینه بر گیر و بنگر گر تماشا بایدت</p></div>
<div class="m2"><p>در میان روی نرگس بوستان افروز را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>لب ز هم بردار یک دم تا هم اندر تیر ماه</p></div>
<div class="m2"><p>آسمان در پیشت اندر جل کشد نوروز را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نوگرفتان را ببوسی بسته گردان بهر آنک</p></div>
<div class="m2"><p>دانه دادن شرط باشد مرغ نو آموز را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بر شکن دام سنایی ز آن دو تا بادام از آنک</p></div>
<div class="m2"><p>دام را بادام تو چون سنگ باشد گوز را</p></div></div>