---
title: >-
    شمارهٔ  ۱۶۴
---
# شمارهٔ  ۱۶۴

<div class="b" id="bn1"><div class="m1"><p>چون رخ به سراب آری ای مه به شراب اندر</p></div>
<div class="m2"><p>اقبال گیا روید در عین سراب اندر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ور رای شکار آری او شکر شکارت را</p></div>
<div class="m2"><p>الحمد کنان آید جانش به کباب اندر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جلاب خرد باشد هر گه که تو در مجلس</p></div>
<div class="m2"><p>از شرم برآمیزی شکر به گلاب اندر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>راز «ارنی ربی» در سینه پدید آید</p></div>
<div class="m2"><p>گر زخم زند ما را چشم تو به خواب اندر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جانها به شتاب آرد لعلت به درنگ اندر</p></div>
<div class="m2"><p>دلها به درنگ آرد لعلت به شتاب اندر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر لحظه یکی عیسی از پرده برون آری</p></div>
<div class="m2"><p>مریم کده‌ها داری گویی به حجاب اندر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مهر تو برآمیزد پاکی به گناه اندر</p></div>
<div class="m2"><p>قهر تو درانگیزد دیوی به شهاب اندر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ما و تو و قلاشی چه باک همی با تو</p></div>
<div class="m2"><p>راند پسر مریم خر را به خلاب اندر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هر روز بهشتی نو ما را بدهی زان لب</p></div>
<div class="m2"><p>دندان نزنی هرگز با ما و ثواب اندر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دانی که خراباتیم از زلزلهٔ عشقت</p></div>
<div class="m2"><p>کم رای خراج آید شه را به خراب اندر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ما را ز میان ما چون کرد برون عشقت</p></div>
<div class="m2"><p>اکنون همه خود خوان خود ما را به خطاب اندر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ما گر تو شدیم ای جان نشگفت که از قوت</p></div>
<div class="m2"><p>دراج عقابی شد چون شد به عقاب اندر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ای جوهر روح ما در هم شده با عشقت</p></div>
<div class="m2"><p>چون بوی به باد اندر چون رنگ به آب اندر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>یارب چه لبی داری کز بهر صلاح ما</p></div>
<div class="m2"><p>جز آب نمی‌باشد با ما به شراب اندر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>از دل چکنی وقتی در عشق سوال او را</p></div>
<div class="m2"><p>در گوش طلب جان را چون شد به جواب اندر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>شعری به سجود آید اشعار سنایی را</p></div>
<div class="m2"><p>هر گه که تو بسرایی شعرش به رباب اندر</p></div></div>