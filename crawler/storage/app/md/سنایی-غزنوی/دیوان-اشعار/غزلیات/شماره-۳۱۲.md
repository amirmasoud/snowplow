---
title: >-
    شمارهٔ ۳۱۲
---
# شمارهٔ ۳۱۲

<div class="b" id="bn1"><div class="m1"><p>ای سنایی در ره ایمان قدم هشیار زن</p></div>
<div class="m2"><p>در مسلمانی قدم با مرد دعوی‌دار زن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ور تو از اخلاص خواهی تا چو زر خالص شوی</p></div>
<div class="m2"><p>دیدهٔ اخلاص را چون طوق بر زنار زن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پی به قلاشی فرو نه فرد گرد از عین ذات</p></div>
<div class="m2"><p>آتش قلاشی اندر ننگ و نام و عار زن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>درد سوز سینه را وقت سحر بنشان ز درد</p></div>
<div class="m2"><p>وز پی دردی قدم با مرد دردی خوار زن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عالم سفلی که او جز مرکز پرگار نیست</p></div>
<div class="m2"><p>چون درین کوی آمدی تو پای بر پرگار زن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خانهٔ خمار اگر شد کعبه پیش چشم تو</p></div>
<div class="m2"><p>لاف از لبیک او در خانهٔ خمار زن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ورت ملک و ملک باید پای در تحقیق نه</p></div>
<div class="m2"><p>ورت جاه و مال باید دست در اسرار زن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ور نخواهی تا چو فرعون لعین گردی تو خوار</p></div>
<div class="m2"><p>پس چو ابراهیم پیغمبر قدم در نار زن</p></div></div>