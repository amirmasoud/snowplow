---
title: >-
    شمارهٔ ۳۰۸
---
# شمارهٔ ۳۰۸

<div class="b" id="bn1"><div class="m1"><p>نی‌نی به ازین باید با دوست وفا کردن</p></div>
<div class="m2"><p>یا نی کم ازین باید آهنگ جفا کردن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یا زشت بود گویی در کیش نکورویان</p></div>
<div class="m2"><p>یک عهد به سر بردن یک قول وفا کردن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هم گفتن و هم کردن از سوختگان آید</p></div>
<div class="m2"><p>باز از چه شما خامان ناگفتن و ناکردن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>باور نکنم قولت زیرا که ترا در دل</p></div>
<div class="m2"><p>یک بادیه ره فرقست از گفتن تا کردن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حاصل نبود کس را از عشق تو در دنیا</p></div>
<div class="m2"><p>جز نامه سیه کردن جز عمر هبا کردن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خود یاد ندارد کس از زلف تو و چشمت</p></div>
<div class="m2"><p>یک تار عطا دادن یک تیر خطا کردن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از بلطمعی تا کی بوسی به رهی دادن</p></div>
<div class="m2"><p>وز بلعجبی تا کی گوشی به ریا کردن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا چند به طراری ما را به زبان و دل</p></div>
<div class="m2"><p>یک باره بلی گفتن صد باره بلا کردن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تا چند به چالاکی ما را به قبول و رد</p></div>
<div class="m2"><p>یک ماه رهی خواندن یکسال رها کردن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گر فوت شود روزی بد عهدی یک روزه</p></div>
<div class="m2"><p>واجب شمری او را چون فرض قضا کردن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گر بوسه‌ای اندیشم بر خاک سر کویت</p></div>
<div class="m2"><p>صد شهر طمع داری در وقت بها کردن</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>در مجمع بت رویان تو بوسه دریغی خود</p></div>
<div class="m2"><p>یا رسم بتان نبود از بوسه سخا کردن</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>یا خوب نباید شد تا هم تو رهی هم ما</p></div>
<div class="m2"><p>ورنه چو شدی باری خوبی به سزا کردن</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>یا فتنه نباید شد تا کس نشود فتنه</p></div>
<div class="m2"><p>ورنه چو شدی جانا این قاعده نا کردن</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>هر لحظه یکی دون را صد «طال بقا» گویی</p></div>
<div class="m2"><p>زیشان چه به کف داری زین «طال بقا» کردن</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چون هست سنایی را اقبال و سنا از تو</p></div>
<div class="m2"><p>واجب نبود او را مهجور سنا کردن</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>با این ادب و حرمت حقا که روا نبود</p></div>
<div class="m2"><p>سودای شما پختن صفرای شما کردن</p></div></div>