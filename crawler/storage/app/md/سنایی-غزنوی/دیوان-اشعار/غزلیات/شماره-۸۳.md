---
title: >-
    شمارهٔ  ۸۳
---
# شمارهٔ  ۸۳

<div class="b" id="bn1"><div class="m1"><p>ساقیا می ده که جز می عشق را پدرام نیست</p></div>
<div class="m2"><p>وین دلم را طاقت اندیشهٔ ایام نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پختهٔ عشقم شراب خام خواهی زان کجا</p></div>
<div class="m2"><p>سازگار پخته جانا جز شراب خام نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با فلک آسایش و آرام چون باشد ترا</p></div>
<div class="m2"><p>چون فلک را در نهاد آسایش و آرام نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عشق در ظاهر حرامست از پی نامحرمان</p></div>
<div class="m2"><p>زان که هر بیگانه‌ای شایستهٔ این نام نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خوردن می نهی شد زان نیز در ایام ما</p></div>
<div class="m2"><p>کاندرین ایام هر دستی سزای جام نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا نیفتد بر امید عشق در دام هوا</p></div>
<div class="m2"><p>کاین ره خاصست اندر وی مجال عام نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هست خاص و عام نی نزدیک هر فرزانه‌ای</p></div>
<div class="m2"><p>دانهٔ دام هوا جز جام جان انجام نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جاهلان را در چراگه دام هست و دانه نی</p></div>
<div class="m2"><p>عاشقان را باز در ره دانه هست و دام نیست</p></div></div>