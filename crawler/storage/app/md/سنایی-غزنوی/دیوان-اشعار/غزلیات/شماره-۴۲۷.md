---
title: >-
    شمارهٔ ۴۲۷
---
# شمارهٔ ۴۲۷

<div class="b" id="bn1"><div class="m1"><p>تو آفت عقل و جان و دینی</p></div>
<div class="m2"><p>تو رشک پری و حور عینی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا چشم تو روی تو نبیند</p></div>
<div class="m2"><p>تو نیز چو خویشتن نبینی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای در دل و جان من نشسته</p></div>
<div class="m2"><p>یک جال دو جای چون نشینی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سروی و مهی عجایب تو</p></div>
<div class="m2"><p>نه بر فلک و نه بر زمینی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بی روی تو عقل من نه خوبست</p></div>
<div class="m2"><p>در خاتم عقل من نگینی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر مهر تو دل نهاد نتوان</p></div>
<div class="m2"><p>تو اسب فراق کرده زینی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گه یار قدیم را برانی</p></div>
<div class="m2"><p>گه یار نوآمده گزینی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>این جور و جفات نه کنونست</p></div>
<div class="m2"><p>دیریست بتا که تو چنینی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ای بوقلمون کیش و دینم</p></div>
<div class="m2"><p>گه کفر منی و گاه دینی</p></div></div>