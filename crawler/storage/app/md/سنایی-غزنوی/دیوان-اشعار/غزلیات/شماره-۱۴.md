---
title: >-
    شمارهٔ  ۱۴
---
# شمارهٔ  ۱۴

<div class="b" id="bn1"><div class="m1"><p>ساقیا دانی که مخموریم در ده جام را</p></div>
<div class="m2"><p>ساعتی آرام ده این عمر بی آرام را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>میر مجلس چون تو باشی با جماعت در نگر</p></div>
<div class="m2"><p>خام در ده پخته را و پخته در ده خام را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>قالب فرزند آدم آز را منزل شدست</p></div>
<div class="m2"><p>انده پیشی و بیشی تیره کرد ایام را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نه بهشت از ما تهی گردد نه دوزخ پر شود</p></div>
<div class="m2"><p>ساقیا در ده شراب ارغوانی فام را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>قیل و قال بایزید و شبلی و کرخی چه سود</p></div>
<div class="m2"><p>کار کار خویش دان اندر نورد این نام را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا زمانی ما برون از خاک آدم دم زنیم</p></div>
<div class="m2"><p>ننگ و نامی نیست بر ما هیچ خاص و عام را</p></div></div>