---
title: >-
    شمارهٔ  ۴۱
---
# شمارهٔ  ۴۱

<div class="b" id="bn1"><div class="m1"><p>ای پر در گوش من ز چنگت</p></div>
<div class="m2"><p>وی پر گل چشم من زرنگت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هنگام سماع بر توان چید</p></div>
<div class="m2"><p>تنگ شکر از دهان تنگت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون چنگ به چنگ بر نهادی</p></div>
<div class="m2"><p>آید ز هزار زهره ننگت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون شوخ نه ای بسان نرگس</p></div>
<div class="m2"><p>کی باده دهد چو باد رنگت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هم صورت آهویی به دیده</p></div>
<div class="m2"><p>زنیست تکبر پلنگت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در صلح چگونه‌ای که باری</p></div>
<div class="m2"><p>شهریست پر از شکر ز جنگت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای چشم خوشت مرا چو دیده</p></div>
<div class="m2"><p>یک روز مباد آژرنگت</p></div></div>