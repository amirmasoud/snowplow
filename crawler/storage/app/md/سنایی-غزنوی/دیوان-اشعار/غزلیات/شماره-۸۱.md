---
title: >-
    شمارهٔ  ۸۱
---
# شمارهٔ  ۸۱

<div class="b" id="bn1"><div class="m1"><p>ای سنایی خواجگی در عشق جانان شرط نیست</p></div>
<div class="m2"><p>جان اسیر عشق گشته دل به کیوان شرط نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>«رب ارنی» بر زبان راندن چو موسی روز شوق</p></div>
<div class="m2"><p>پس به دل گفتن «انا الا علی» چو هامان شرط نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از پی عشق بتان مردانگی باید نمود</p></div>
<div class="m2"><p>گر چو زن بی همتی پس لاف مردان شرط نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون اناالله در بیابان هدی بشنیده‌ای</p></div>
<div class="m2"><p>پس هراسیدن ز چوبی همچو ثعبان شرط نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از پی مردان اگر خواهی که در میدان شوی</p></div>
<div class="m2"><p>صف کشیدن گرداوبی گوی و چوگان شرط نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ور همی دعوی کنی گویی که «لی صبر جمیل»</p></div>
<div class="m2"><p>پس فغان و زاری اندر بیت احزان شرط نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون جمال یوسفی غایب شدست از پیش تو</p></div>
<div class="m2"><p>پس نشستن ایمن اندر شهر کنعان شرط نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ور همی دانی که منزلگاه حق جز عرش نیست</p></div>
<div class="m2"><p>پس مهار اشتر کشیدن در بیابان شرط نیست</p></div></div>