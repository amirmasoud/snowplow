---
title: >-
    شمارهٔ  ۷۵
---
# شمارهٔ  ۷۵

<div class="b" id="bn1"><div class="m1"><p>رازی ز ازل در دل عشاق نهانست</p></div>
<div class="m2"><p>زان راز خبر یافت کسی را که عیانست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>او را ز پس پردهٔ اغیار دوم نیست</p></div>
<div class="m2"><p>زان مثل ندارد که شهنشاه جهانست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گویند ازین میدان آن را که درآمد</p></div>
<div class="m2"><p>کی خواجه دل و روح و روانت ز روانست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر ماه هلال آید در نعت کسوفست</p></div>
<div class="m2"><p>ور تیر وصال آید بر بسته کمانست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کاین کوی دو صد بار هزار از سر معنی</p></div>
<div class="m2"><p>گشتست کز ایشان تف انگشت نشانست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آنکس که ردایی ز ریا بر کتف افگند</p></div>
<div class="m2"><p>آن نیست ردا آن به صف دان طلسانست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر چند نگونست درین پرده دل ما</p></div>
<div class="m2"><p>میدان به حقیقت که ز اقبال ستانست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>قاف از خبر هیبت این خوف به تحقیق</p></div>
<div class="m2"><p>چون سین سلامت ز پی خواجه روانست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گویی که مگر سینهٔ پر آتش دارد</p></div>
<div class="m2"><p>یا دیدهٔ او بر صفت بحر عمانست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>این چیست چنین باید اندر ره معنی</p></div>
<div class="m2"><p>آن کس که چنین نیست یقین دان که چنانست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نظم گهر معنی در دیدهٔ دعوی</p></div>
<div class="m2"><p>چون مردمک دیده درین مقله نهانست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>در راه فنا باید جانهای عزیزان</p></div>
<div class="m2"><p>کاین شعر سنایی سبب قوت جانست</p></div></div>