---
title: >-
    شمارهٔ ۲۷۰
---
# شمارهٔ ۲۷۰

<div class="b" id="bn1"><div class="m1"><p>ای به رخسار کفر و ایمان هم</p></div>
<div class="m2"><p>وی به گفتار درد و درمان هم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زلف پر تاب تو چو قامت من</p></div>
<div class="m2"><p>چنبرست ای نگار چوگان هم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خیره ماند از لب تو بیجاده</p></div>
<div class="m2"><p>به سر تو که لعل و مرجان هم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از رخ تو دلیل اثباتست</p></div>
<div class="m2"><p>عالم عقل را و برهان هم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در ره تو ز رنج کهسارست</p></div>
<div class="m2"><p>بی کناره ز غم بیابان هم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر سر کوی عاشقی صبرست</p></div>
<div class="m2"><p>ایستاده ذلیل و حیران هم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر دل و جان بنده حکم تراست</p></div>
<div class="m2"><p>ای شهنشاه حسن فرمان هم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چند گویی که از تو برگردم</p></div>
<div class="m2"><p>با همه بازیست و با جان هم؟</p></div></div>