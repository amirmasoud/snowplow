---
title: >-
    شمارهٔ  ۱۱۶
---
# شمارهٔ  ۱۱۶

<div class="b" id="bn1"><div class="m1"><p>دگر گردی روا باشد دلم غمگین چرا باشد</p></div>
<div class="m2"><p>جهان پر خوبرویانند آن کن کت روا باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ترا گر من بوم شاید وگر نه هم روا باشد</p></div>
<div class="m2"><p>ترا چون من فراوانند مرا چون تو کجا باشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جفاهای تو نزد من مکافاتش به جا باشد</p></div>
<div class="m2"><p>ولیکن آن کند هر کس که از اصلش سزا باشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نگویند ای مسلمانان هرانکو مبتلا باشد</p></div>
<div class="m2"><p>نباشد مبتلا الا خداوند بلا باشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چنین گیرم که این عالم همه یکسر ترا باشد</p></div>
<div class="m2"><p>نه آخر هر فرازی را نشیبی در قفا باشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سنایی از غم عشقت سنایی گشت ای دلبر</p></div>
<div class="m2"><p>مگویید ای مسلمانان خطا باشد خطا باشد</p></div></div>