---
title: >-
    شمارهٔ  ۱۹۹
---
# شمارهٔ  ۱۹۹

<div class="b" id="bn1"><div class="m1"><p>بر من از عشقت شبیخون بود دوش</p></div>
<div class="m2"><p>آب چشمم قطرهٔ خون بود دوش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در دل از عشق تو دوزخ می‌نمود</p></div>
<div class="m2"><p>در کنار از دیده جیحون بود دوش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای توانگر همچو قارون از جمال</p></div>
<div class="m2"><p>عاشق از عشق تو قارون بود دوش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای به رخ ماه زمین بی روی تو</p></div>
<div class="m2"><p>مونس من ماه گردون بود دوش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بی تو دوش از عمر نشمردم همی</p></div>
<div class="m2"><p>کز شمار عمر بیرون بود دوش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون شب دوشین شبی هرگز مباد</p></div>
<div class="m2"><p>کز همه شبها غم افزون بود دوش</p></div></div>