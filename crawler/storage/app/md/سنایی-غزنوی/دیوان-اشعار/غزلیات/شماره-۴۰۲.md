---
title: >-
    شمارهٔ ۴۰۲
---
# شمارهٔ ۴۰۲

<div class="b" id="bn1"><div class="m1"><p>دلا تا کی سر گفتار داری</p></div>
<div class="m2"><p>طریق دیدن و کردار داری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ظهور ظاهر احوال خود را</p></div>
<div class="m2"><p>ظهور ظاهر اظهار داری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگر مشتاق دلداری و دایم</p></div>
<div class="m2"><p>امید دیدن دلدار داری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز دیدارت نپوشیدست دلدار</p></div>
<div class="m2"><p>ببین دلدار اگر دیدار داری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مسلمان نیستی تا همچو گبران</p></div>
<div class="m2"><p>ز هستی بر میان زنار داری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دلا تا چون سنایی در ره دین</p></div>
<div class="m2"><p>طریق زهد و استغفار داری</p></div></div>