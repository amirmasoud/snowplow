---
title: >-
    شمارهٔ  ۵۹
---
# شمارهٔ  ۵۹

<div class="b" id="bn1"><div class="m1"><p>هر کرا درد بی نهایت نیست</p></div>
<div class="m2"><p>عشق را پس برو عنایت نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عشق شاهیست پا به تخت ازل</p></div>
<div class="m2"><p>جز بدو مرد را ولایت نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عشق در عقل و علم درماند</p></div>
<div class="m2"><p>عشق را عقل و علم رایت نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عشق را بوحنیفه درس نکرد</p></div>
<div class="m2"><p>شافعی را در او روایت نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عشق حی است بی بقا و فنا</p></div>
<div class="m2"><p>عاشقان را ازو شکایت نیست</p></div></div>