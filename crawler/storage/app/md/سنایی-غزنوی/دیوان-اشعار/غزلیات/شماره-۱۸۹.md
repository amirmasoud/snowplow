---
title: >-
    شمارهٔ  ۱۸۹
---
# شمارهٔ  ۱۸۹

<div class="b" id="bn1"><div class="m1"><p>ای ز خوبی مست هان هشیار باش</p></div>
<div class="m2"><p>ور ز مستی خفته‌ای بیدار باش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از شراب شوق رویت عالمی</p></div>
<div class="m2"><p>گشته مستانند هان هشیار باش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر مه میخواره خوانندت رواست</p></div>
<div class="m2"><p>می به شادی نوش و بی تیمار باش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خویشتن‌داری کن اندر کارها</p></div>
<div class="m2"><p>خصم بر کارست هان بر کار باش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گاه بزم افروز عاشق سوز باش</p></div>
<div class="m2"><p>گاه صاحب درد و دردی خوار باش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زینهاری دارم اندر گردنت</p></div>
<div class="m2"><p>زینهار ای بت بران زنهار باش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون ز خصمان خویشتن داری کنی</p></div>
<div class="m2"><p>دستبردی بر جهان سالار باش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هم چنین از خویشتن داری مدام</p></div>
<div class="m2"><p>تا توانی سر کش و عیار باش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بر در دیوار خود ایمن مباش</p></div>
<div class="m2"><p>بر حذر هان از در و دیوار باش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کار تو باید که باشد بر نظام</p></div>
<div class="m2"><p>کارهای عاشقان گو زار باش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گر سنایی از تو برخوردار نیست</p></div>
<div class="m2"><p>تو ز بخت خویش برخوردار باش</p></div></div>