---
title: >-
    شمارهٔ  ۱۲
---
# شمارهٔ  ۱۲

<div class="b" id="bn1"><div class="m1"><p>مرد بی حاصل نیابد یار با تحصیل را</p></div>
<div class="m2"><p>جان ابراهیم باید عشق اسماعیل را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر هزاران جان لبش را هدیه آرم گویدم</p></div>
<div class="m2"><p>نزد عیسا تحفه چون آری همی انجیل را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زلف چون پرچین کند خواری نماید مشک را</p></div>
<div class="m2"><p>غمزه چون بر هم زند قیمت فزاید نیل را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون وصال یار نبود گو دل و جانم مباش</p></div>
<div class="m2"><p>چون شه و فرزین نباشد خاک بر سر فیل را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از دو چشمش تیز گردد ساحری ابلیس را</p></div>
<div class="m2"><p>وز لبانش کند گردد تیغ عزراییل را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر چه زمزم را پدید آورد هم نامش به پای</p></div>
<div class="m2"><p>او به مویی هم روان کرد از دو چشمم نیل را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جان و دل کردم فدای خاکپایش بهر آنک</p></div>
<div class="m2"><p>از برای کعبه چاکر بود باید میل را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آب خورشید و مه اکنون برده شد کو بر فروخت</p></div>
<div class="m2"><p>در خم زلف از برای عاشقان قندیل را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ای سنایی گر هوای خوبرویان می‌کنی</p></div>
<div class="m2"><p>از نخستت ساخت باید دبه و زنبیل را</p></div></div>