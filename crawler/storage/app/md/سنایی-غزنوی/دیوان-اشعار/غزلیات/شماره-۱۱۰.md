---
title: >-
    شمارهٔ  ۱۱۰
---
# شمارهٔ  ۱۱۰

<div class="b" id="bn1"><div class="m1"><p>صحبت معشوق انتظار نیرزد</p></div>
<div class="m2"><p>بوی گل و لاله زخم خار نیرزد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وصل نخواهم که هجر قاعدهٔ اوست</p></div>
<div class="m2"><p>خوردن می محنت خمار نیرزد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز آن سوی دریای عشق گر همه سودست</p></div>
<div class="m2"><p>آنهمه نسود آفت گذار نیرزد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>این دو سه روز غم وصال و فراقت</p></div>
<div class="m2"><p>اینهمه آشوب کار و بار نیرزد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>روز شود در شمارم از غم جانان</p></div>
<div class="m2"><p>خود عمل عاشقی شمار نیرزد</p></div></div>