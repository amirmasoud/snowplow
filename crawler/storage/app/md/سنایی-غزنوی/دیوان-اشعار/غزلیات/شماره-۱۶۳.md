---
title: >-
    شمارهٔ  ۱۶۳
---
# شمارهٔ  ۱۶۳

<div class="b" id="bn1"><div class="m1"><p>هر کرا در دل بود بازار یار</p></div>
<div class="m2"><p>عمر و جان و دل کند در کار یار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خاصه آن بی دل که چون من یک زمان</p></div>
<div class="m2"><p>بر زمین نشکیبد از دیدار یار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کبک را بین تا چگونه شد خجل</p></div>
<div class="m2"><p>زان کرشمه کردن و رفتار یار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بنگر اندر گل که رشوت چون دهد</p></div>
<div class="m2"><p>خون شود لعل از پی رخسار یار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در جهان فردوس اعلا دارد آنک</p></div>
<div class="m2"><p>یک نفس بودست در پندار یار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در همه عالم ندیدم لذتی</p></div>
<div class="m2"><p>خوشتر و شیرین‌تر از گفتار یار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>همچو سنگ آید مرا یاقوت سرخ</p></div>
<div class="m2"><p>بی لب یاقوت شکر بار یار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>باد نوشین دوش گفتی ناگهان</p></div>
<div class="m2"><p>چین زلف آشفت بر گلنار یار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زان قبل امروز مشک آلود گشت</p></div>
<div class="m2"><p>خانه و بام و در و دیوار یار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>رشک لعل و لولو اندر کوه و بحر</p></div>
<div class="m2"><p>زان عقیق و لولو شهوار یار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شد دلم مسکین من در غم نژند</p></div>
<div class="m2"><p>من ندانم پیش ازین هنجار یار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دست بر سر ماند چون کژدم دلم</p></div>
<div class="m2"><p>زان دو زلفین سیه چون مار یار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>هوش و عقلم برده‌اند از دل تمام</p></div>
<div class="m2"><p>آن دو نرگس بر رخ چون نار یار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>مر سنایی را فتاد این نادره</p></div>
<div class="m2"><p>چون معزی گفت از اخبار یار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>آنچه من می‌بینم از آزار یار</p></div>
<div class="m2"><p>گر بگویم بشکنم بازار یار</p></div></div>