---
title: >-
    شمارهٔ ۲۸۸
---
# شمارهٔ ۲۸۸

<div class="b" id="bn1"><div class="m1"><p>گرچه از جمع بی نیازانیم</p></div>
<div class="m2"><p>عاشق عشق و عشقبازانیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>منصف منصف خراباتیم</p></div>
<div class="m2"><p>کعبهٔ کعبتین بازانیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گاه سوزان در آتش عشقیم</p></div>
<div class="m2"><p>گاه از سوز رود سازانیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همچو مرغ از قفس شکسته شدیم</p></div>
<div class="m2"><p>همچو شمع از هوس گدازانیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر چه کبکیم در ممالک خویش</p></div>
<div class="m2"><p>مانده در جستجوی بازانیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مرغزار وصال یافته‌ایم</p></div>
<div class="m2"><p>چون سنایی درو گرازانیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زاهدا خیز و در نماز آویز</p></div>
<div class="m2"><p>زان که ما خاک بی‌نیازانیم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر تو از طوع و طاعه می‌نازی</p></div>
<div class="m2"><p>ما همیشه ز شوق نازانیم</p></div></div>