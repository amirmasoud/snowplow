---
title: >-
    شمارهٔ  ۱۷
---
# شمارهٔ  ۱۷

<div class="b" id="bn1"><div class="m1"><p>ای به بر کرده بی وفایی را</p></div>
<div class="m2"><p>منقطع کرده آشنایی را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر ما امشبی قناعت کن</p></div>
<div class="m2"><p>بنما خلق انبیایی را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای رخت بستده ز ماه و ز مهر</p></div>
<div class="m2"><p>خوبی و لطف و روشنایی را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زود در گردنم فگن دلقی</p></div>
<div class="m2"><p>برکش این رومی و بهایی را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چنگی و بربطی به گاه نشاط</p></div>
<div class="m2"><p>جمله یاری دهند نایی را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>با چنان روی و با چنان زلفین</p></div>
<div class="m2"><p>منهزم کرده‌ای ختایی را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آتشی نزد ماست خیز و بیار</p></div>
<div class="m2"><p>آبی و خاکی و هوایی را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بار ندهند نزد ما به صبوح</p></div>
<div class="m2"><p>هیچ بیگانهٔ مرایی را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون بود یار زشت پر معنی</p></div>
<div class="m2"><p>چکنم جور هر کجایی را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چو شدی مست جای خواب بساز</p></div>
<div class="m2"><p>وز میان بانگ زن سنایی را</p></div></div>