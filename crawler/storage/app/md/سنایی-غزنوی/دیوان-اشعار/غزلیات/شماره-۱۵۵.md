---
title: >-
    شمارهٔ  ۱۵۵
---
# شمارهٔ  ۱۵۵

<div class="b" id="bn1"><div class="m1"><p>هر که او معشوق دارد گو چو من عیار دار</p></div>
<div class="m2"><p>خوش لب و شیرین زبان خوش عیش و خوش گفتار دار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یار معنی دار باید خاصه اندر دوستی</p></div>
<div class="m2"><p>تا توانی دوستی با یار معنی دار دار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از عزیزی گر نخواهی تا به خواری اوفتی</p></div>
<div class="m2"><p>روی نیکو را عزیز و مال و نعمت خوار دار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ماه ترکستان بسی از ماه گردون خوبتر</p></div>
<div class="m2"><p>مه ز ترکستان گزین و ز ماه گردون دون عار دار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زلف عنبر بار گیر و جام مالامال کش</p></div>
<div class="m2"><p>دوستی با جام و با زلفین عنبر بار دار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ور همی خواهی که گردد کار تو همچون نگار</p></div>
<div class="m2"><p>چون سنایی خویشتن در عشق او بر کار دار</p></div></div>