---
title: >-
    شمارهٔ ۳۳۸
---
# شمارهٔ ۳۳۸

<div class="b" id="bn1"><div class="m1"><p>ای چون تو ندیده جم آخر چه جمالست این</p></div>
<div class="m2"><p>وی چون تو به عالم کم آخر چه کمالست این</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو با من و من پویان هر جای ترا جویان</p></div>
<div class="m2"><p>ای شمع نکورویان آخر چه وصالست این</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زان گلبن انسانی هر دم گلی افشانی</p></div>
<div class="m2"><p>ای میوهٔ روحانی آخر چه نهالست این</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در وصف تو عقل و جان چون من شده سرگردان</p></div>
<div class="m2"><p>ای وهم ز تو حیران آخر چه جمالست این</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفتی که چو من دلبر داری وز من بهتر</p></div>
<div class="m2"><p>ای جادوی صورت گر آخر چه خیالست این</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای از پی داغ ما آرایش باغ ما</p></div>
<div class="m2"><p>ای چشم و چراغ ما آخر چه مثالست این</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر روز نپویی تو جز عشق نجویی تو</p></div>
<div class="m2"><p>ای ماه نکویی تو آخر چه خصالست این</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هر روز مرا نرمک بکشی تو به آزرمک</p></div>
<div class="m2"><p>ای شوخک بی‌شرمک آخر چه وبالست این</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>پرسی: چو منی دلبر بینی تو به عالم در</p></div>
<div class="m2"><p>ای ماه نکو منظر آخر چه سوالست این</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ما را نه بدین سستی زین بیش همی جستی</p></div>
<div class="m2"><p>ای خسته از آن خستی آخر چه ملالست این</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گفتی همه جا با تو وصلست مرا با تو</p></div>
<div class="m2"><p>ای بی خود و با ما تو آخر چه دلالست این</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گفتی که سنایی خود داریم و ازو به صد</p></div>
<div class="m2"><p>ای ناقد نیک و بد آخر چه محالست این</p></div></div>