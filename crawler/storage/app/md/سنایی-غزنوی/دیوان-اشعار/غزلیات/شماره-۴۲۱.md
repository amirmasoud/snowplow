---
title: >-
    شمارهٔ ۴۲۱
---
# شمارهٔ ۴۲۱

<div class="b" id="bn1"><div class="m1"><p>به درگاه عشقت چه نامی چه ننگی</p></div>
<div class="m2"><p>به نزد جلالت چه شاهی چه شنگی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جهان پر حدیث وصال تو بینم</p></div>
<div class="m2"><p>زهی نارسیده به زلف تو چنگی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همانا به صحرا نظر کرده‌ای تو</p></div>
<div class="m2"><p>که صحرا ز رویت گرفتست رنگی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز عکس رخ تو به هر مرغزاری</p></div>
<div class="m2"><p>ز دیبای چینی گشادست تنگی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شگفت آهوی تو که صید تو سازد</p></div>
<div class="m2"><p>به هر چشم زخمی دلاور پلنگی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز جعدت کمندی و شهری پیاده</p></div>
<div class="m2"><p>جهانی سوار و ز چشمت خدنگی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اگر خواهی ارواح مرغان علوی</p></div>
<div class="m2"><p>فرود آری از شاخ طوبا به سنگی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به تو کی رسد هرگز از راه گفتی</p></div>
<div class="m2"><p>بر نار و نورت که دارد درنگی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کیم من که از نوش وصل تو گویم</p></div>
<div class="m2"><p>نپوید پی شیر روباه لنگی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>من آن عاشقم کز تو خشنود باشم</p></div>
<div class="m2"><p>ز نوشی به زهری ز صلحی به جنگی</p></div></div>