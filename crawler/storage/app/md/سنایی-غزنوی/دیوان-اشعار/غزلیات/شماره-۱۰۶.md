---
title: >-
    شمارهٔ  ۱۰۶
---
# شمارهٔ  ۱۰۶

<div class="b" id="bn1"><div class="m1"><p>سوال کرد دل من که دوست با تو چه کرد</p></div>
<div class="m2"><p>چرات بینم با اشک سرخ و با رخ زرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دراز قصه نگویم حدیث جمله کنم</p></div>
<div class="m2"><p>هر آنچه گفت نکرد و هر آنچه کشت نخورد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جفا نمود و نبخشود و دل ربود و نداد</p></div>
<div class="m2"><p>وفا بگفت و نکرد و جفا نگفت و بکرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو پیشم آمد کردم سلام روی بتافت</p></div>
<div class="m2"><p>چو آستینش گرفتم گفت بردا برد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نه چاره‌ای که دل از دوستیش برگیرم</p></div>
<div class="m2"><p>نه حیله‌ای که توانمش باز راه آورد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر انتظار میان دو حال ماندستم</p></div>
<div class="m2"><p>کشید باید رنج و چشید باید درد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ایا سنایی لولو ز دیدگانت مبار</p></div>
<div class="m2"><p>که در عقیلهٔ هجران صبور باید مرد</p></div></div>