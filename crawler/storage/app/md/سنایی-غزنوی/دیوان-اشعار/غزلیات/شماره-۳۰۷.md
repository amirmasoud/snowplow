---
title: >-
    شمارهٔ ۳۰۷
---
# شمارهٔ ۳۰۷

<div class="b" id="bn1"><div class="m1"><p>تخم بد کردن نباید کاشتن</p></div>
<div class="m2"><p>پشت بر عاشق نباید داشتن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای صنم ار تو بخواهی بنده را</p></div>
<div class="m2"><p>زین سپس دانی نکوتر داشتن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چند ازین آیات نخوت خواندن</p></div>
<div class="m2"><p>چند ازین رایات عجب افراشتن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نقش چین باید ز سینه محو کرد</p></div>
<div class="m2"><p>صورت مهر و وفا بنگاشتن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چند ازین شاخ وفاها سوختن</p></div>
<div class="m2"><p>چند ازین تخم جفاها کاشتن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خوب نبود بر چو من بیچاره‌ای</p></div>
<div class="m2"><p>لشکر جور و جفا بگماشتن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زشت باشد با چو من درمانده‌ای</p></div>
<div class="m2"><p>شرط و رسم مردمی نگذاشتن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در صف رندان و قلاشان خویش</p></div>
<div class="m2"><p>کمترین کس بایدم پنداشتن</p></div></div>