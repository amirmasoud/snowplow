---
title: >-
    شمارهٔ  ۱۰۵
---
# شمارهٔ  ۱۰۵

<div class="b" id="bn1"><div class="m1"><p>عاشقی تا در دل ما راه کرد</p></div>
<div class="m2"><p>اغلب انفاس ما را آه کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بود هر باری دلم عاشق به طوع</p></div>
<div class="m2"><p>برد و زیر پای عشق اکراه کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عیش چون نوش مرا چون زهر کرد</p></div>
<div class="m2"><p>صبر چون کوه مرا چون کاه کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>باز در شهر مسلمانان مغی</p></div>
<div class="m2"><p>کرد ما را بسته و ناگاه کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از تن باریک من زنار ساخت</p></div>
<div class="m2"><p>وز دل سنگینم آتشگاه کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>با همه محنت که دیدم من به عشق</p></div>
<div class="m2"><p>کو مرا بی قدر و آب و جاه کرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نیک خواهم عشق را گر چه مرا</p></div>
<div class="m2"><p>او به کام دشمن و بدخواه کرد</p></div></div>