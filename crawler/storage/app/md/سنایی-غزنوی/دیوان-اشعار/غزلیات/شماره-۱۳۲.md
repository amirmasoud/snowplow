---
title: >-
    شمارهٔ  ۱۳۲
---
# شمارهٔ  ۱۳۲

<div class="b" id="bn1"><div class="m1"><p>روی او ماهست اگر بر ماه مشک افشان بود</p></div>
<div class="m2"><p>قد او سروست اگر بر سرو لالستان بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر روا باشد که لالستان بود بالای سرو</p></div>
<div class="m2"><p>بر مه روشن روا باشد که مشک افشان بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل چو گوی و پشت چون چوگان بود عشاق را</p></div>
<div class="m2"><p>تا زنخدانش چو گوی و زلف چون چوگان بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر ز دو هاروت او دلها نژند آید همی</p></div>
<div class="m2"><p>درد دلها را ز دو یاقوت او درمان بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>من به جان مرجان و لولو را خریداری کنم</p></div>
<div class="m2"><p>گر چو دندان و لب او لولو و مرجان بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>راز او در عشق او پنهان نماند تا مرا</p></div>
<div class="m2"><p>روی زرد و آه سرد و دیدهٔ گریان بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زان که غمازان من هستند هر سه پیش خلق</p></div>
<div class="m2"><p>هر کجا غماز باشد راز کی پنهان بود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بر کنار خویش رضوان پرورد او را به ناز</p></div>
<div class="m2"><p>حور باشد هر که او پروردهٔ رضوان بود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هر زمان گویم به شیرینی و پاکی در جهان</p></div>
<div class="m2"><p>چون لب و دندان او یارب لب و دندان بود</p></div></div>