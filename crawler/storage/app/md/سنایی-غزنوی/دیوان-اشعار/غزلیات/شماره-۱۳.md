---
title: >-
    شمارهٔ  ۱۳
---
# شمارهٔ  ۱۳

<div class="b" id="bn1"><div class="m1"><p>ساقیا دل شد پر از تیمار پر کن جام را</p></div>
<div class="m2"><p>بر کف ما نه سه باده گردش اجرام را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا زمانی بی زمانه جام می بر کف نهیم</p></div>
<div class="m2"><p>بشکنیم اندر زمانه گردش ایام را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جان و دل در جام کن تا جان به جام اندر نهیم</p></div>
<div class="m2"><p>همچو خون دل نهاده ای پسر صد جام را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دام کن بر طرف بام از حلقه‌های زلف خویش</p></div>
<div class="m2"><p>چون که جان در جام کردی تنگ در کش جام را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کاش کیکاووس پر کن زان سهیل شامیان</p></div>
<div class="m2"><p>زیر خط حکم درکش ملک زال و سام را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چرخ بی آرام را اندر جهان آرام نیست</p></div>
<div class="m2"><p>بند کن در می پرستی چرخ بی آرام را</p></div></div>