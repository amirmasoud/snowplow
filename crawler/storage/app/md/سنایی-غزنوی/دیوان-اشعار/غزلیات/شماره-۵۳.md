---
title: >-
    شمارهٔ  ۵۳
---
# شمارهٔ  ۵۳

<div class="b" id="bn1"><div class="m1"><p>ای ساقی می بیار پیوست</p></div>
<div class="m2"><p>کان یار عزیز توبه بشکست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>برخاست ز جای زهد و دعوی</p></div>
<div class="m2"><p>در میکده با نگار بنشست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بنهاد ز سر ریا و طامات</p></div>
<div class="m2"><p>از صومعه ناگهان برون جست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بگشاد ز پای بند تکلیف</p></div>
<div class="m2"><p>زنار مغانه بر میان بست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>می خورد و مرا بگفت می خور</p></div>
<div class="m2"><p>تا بتوانی مباش جز مست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اندر ره نیستی همی رو</p></div>
<div class="m2"><p>آتش در زن بهر چه زی هست</p></div></div>