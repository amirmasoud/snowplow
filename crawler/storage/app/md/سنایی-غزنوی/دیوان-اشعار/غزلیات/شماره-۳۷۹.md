---
title: >-
    شمارهٔ ۳۷۹
---
# شمارهٔ ۳۷۹

<div class="b" id="bn1"><div class="m1"><p>از عشق آن دو نرجس وز مهر آن دو لاله</p></div>
<div class="m2"><p>بی خواب و بی‌قرارم چون بر گلت کلاله</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خدمت کنم به پیشت همچون صراحی از جان</p></div>
<div class="m2"><p>تا برنهی لبم را بر لبت چون پیاله</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا روز ژاله بارد از چشم همچو رودم</p></div>
<div class="m2"><p>آری نکو نماید بر روی لاله ژاله</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دارم هزار بوسه بر روی و چشم تو من</p></div>
<div class="m2"><p>گر میدهی وگرنه بیرون کنم قباله</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مهمان حسن داری سیر از پی خرد را</p></div>
<div class="m2"><p>مر تشنگان خود را ندهی یک پیاله</p></div></div>