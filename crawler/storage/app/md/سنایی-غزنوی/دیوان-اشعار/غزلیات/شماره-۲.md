---
title: >-
    شمارهٔ  ۲
---
# شمارهٔ  ۲

<div class="b" id="bn1"><div class="m1"><p>جمالت کرد جانا هست ما را</p></div>
<div class="m2"><p>جلالت کرد ماها پست ما را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل آرا ما نگارا چون تو هستی</p></div>
<div class="m2"><p>همه چیزی که باید هست ما را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شراب عشق روی خرمت کرد</p></div>
<div class="m2"><p>بسان نرگس تو مست ما را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر روزی کف پایت ببوسم</p></div>
<div class="m2"><p>بود بر هر دو عالم دست ما را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تمنای لبت شوریده دارد</p></div>
<div class="m2"><p>چو مشکین زلف تو پیوست ما را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو صیاد خرد لعل تو باشد</p></div>
<div class="m2"><p>سر زلف تو شاید شست ما را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زمانه بند شستت کی گشاید</p></div>
<div class="m2"><p>چو زلفین تو محکم بست ما را</p></div></div>