---
title: >-
    شمارهٔ ۴۱۵
---
# شمارهٔ ۴۱۵

<div class="b" id="bn1"><div class="m1"><p>عشق و شراب و یار و خرابات و کافری</p></div>
<div class="m2"><p>هر کس که یافت شد ز همه اندهان بری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از راه کج به سوی خرابات راه یافت</p></div>
<div class="m2"><p>کفرش همه هدی شد و توحید کافری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بگذاشت آنچه بود هم از هجر و هم ز وصل</p></div>
<div class="m2"><p>برخاست از تصرف و از راه داوری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بیزار شد ز هر چه به جز عشق و باده بود</p></div>
<div class="m2"><p>بست او میان به پیش یکی بت به چاکری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>برخیز ای سنایی باده بخواه و چنگ</p></div>
<div class="m2"><p>اینست دین ما و طریق قلندری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مرد آن بود که داند هر جای رای خویش</p></div>
<div class="m2"><p>مردان به کار عشق نباشند سر سری</p></div></div>