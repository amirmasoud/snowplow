---
title: >-
    شمارهٔ ۲۴۰
---
# شمارهٔ ۲۴۰

<div class="b" id="bn1"><div class="m1"><p>ترا دل دادم ای دلبر شبت خوش باد من رفتم</p></div>
<div class="m2"><p>تو دانی با دل غمخور شبت خوش باد من رفتم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگر وصلت بگشت از من روا دارم روا دارم</p></div>
<div class="m2"><p>گرفتم هجرت اندر بر شبت خوش باد من رفتم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ببردی نور روز و شب بدان زلف و رخ زیبا</p></div>
<div class="m2"><p>زهی جادو زهی دلبر شبت خوش باد من رفتم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به چهره اصل ایمانی به زلفین مایهٔ کفری</p></div>
<div class="m2"><p>ز جور هر دو آفتگر شبت خوش باد من رفتم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>میان آتش و آبم ازین معنی مرا بینی</p></div>
<div class="m2"><p>لبان خشک و چشم تر شبت خوش باد من رفتم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بدان راضی شدم جانا که از حالم خبر پرسی</p></div>
<div class="m2"><p>ازین آخر بود کمتر شبت خوش باد من رفتم</p></div></div>