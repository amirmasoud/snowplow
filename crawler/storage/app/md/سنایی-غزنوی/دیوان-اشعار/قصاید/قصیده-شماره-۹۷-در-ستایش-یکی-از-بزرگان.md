---
title: >-
    قصیدهٔ شمارهٔ ۹۷ - در ستایش یکی از بزرگان
---
# قصیدهٔ شمارهٔ ۹۷ - در ستایش یکی از بزرگان

<div class="b" id="bn1"><div class="m1"><p>ای به آرام تو زمین را سنگ</p></div>
<div class="m2"><p>وی به اقبال تو زمان را ننگ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای به نزد کفایت تو کفایت</p></div>
<div class="m2"><p>باد پیمای و کژ چو نای و چو چنگ</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای دو عالم گرفته اندر دست</p></div>
<div class="m2"><p>به کمال و صیانت و فرهنگ</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با مجال سخات هفت اقلیم</p></div>
<div class="m2"><p>تنگ میدان بسان هفتو رنگ</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پر و بال ا زتو یافته رادی</p></div>
<div class="m2"><p>فروهنگ ا زتو یافته فرهنگ</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از بزرگیست در دماغ تو کبر</p></div>
<div class="m2"><p>وز کریمیست در نهاد تو هنگ</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نه به کبرست حلم تو چو جبال</p></div>
<div class="m2"><p>نه به طبعست کبر تو چو پلنگ</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ای گهر زای بی‌نشیب زوال</p></div>
<div class="m2"><p>وی درر پاش بی‌نهیب نهنگ</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>درد دو عالم همی نگنجی از آنک</p></div>
<div class="m2"><p>تو بزرگی و هر دو عالم تنگ</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به تن و طبع تازه‌ای نه به روح</p></div>
<div class="m2"><p>به دل و نام زنده‌ای نه به رنگ</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نام تو در ازل نشانه نهاد</p></div>
<div class="m2"><p>خوشدلی در مزاج مردم زنگ</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دور از آن مجلس از حرارت دل</p></div>
<div class="m2"><p>آن چنانم که نار با نارنگ</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گه خروشان چو در نبرد تو نای</p></div>
<div class="m2"><p>گاه نالان چو در نبرد تو چنگ</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گاه در خوی چو اسبت اندر تک</p></div>
<div class="m2"><p>گاه در خون چو تیغت اندر جنگ</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>کرده شیران حضرت تو مرا</p></div>
<div class="m2"><p>سر زده همچو گاو آب آهنگ</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گر نیایم به مجلس تو همی</p></div>
<div class="m2"><p>از سر عجزدان نه از سر ننگ</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>خود به تو چون رسد رهی که تویی</p></div>
<div class="m2"><p>از سنا و بلندی و اورنگ</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>روی تو آفتاب و چشمم درد</p></div>
<div class="m2"><p>صدر تو آسمان و پایم لنگ</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>خود شگفتست از آنکه بشکیبد</p></div>
<div class="m2"><p>از چنان طلعت و چنان فرهنگ</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>کز پی ضعف دیدگان خفاش</p></div>
<div class="m2"><p>نکند با جمال صبح درنگ</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>مرغ عیسی کدام سگ باشد</p></div>
<div class="m2"><p>که کند سوی جبرئیل آهنگ</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>کز چنان قلزم آنک روی بتافت</p></div>
<div class="m2"><p>چشم بر پشت یافت چون خرچنگ</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>لعل در دست تست خوش می‌باش</p></div>
<div class="m2"><p>سنگ اگر نیست خاک بر سنگ</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چکنی ریش و سبلت مانی</p></div>
<div class="m2"><p>چون بدیدی عجایب ارتنگ</p></div></div>