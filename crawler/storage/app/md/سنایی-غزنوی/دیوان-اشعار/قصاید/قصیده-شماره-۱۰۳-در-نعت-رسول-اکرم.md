---
title: >-
    قصیدهٔ شمارهٔ ۱۰۳ - در نعت رسول اکرم
---
# قصیدهٔ شمارهٔ ۱۰۳ - در نعت رسول اکرم

<div class="b" id="bn1"><div class="m1"><p>چون به صحرا شد جمال سید کون از عدم</p></div>
<div class="m2"><p>جاه کسرا زد به عالم‌های عزل اندر قدم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون نقاب از چهرهٔ ایمان براندازد زند</p></div>
<div class="m2"><p>خیمهٔ ادبار خود کفر از خجالت در ظلم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کوس دعوت چون بزد در خاک بطحا در زمان</p></div>
<div class="m2"><p>بر کنار عرش بر زد رایت ایمان علم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آفتاب کل مخلوقات آن کز بهر جاه</p></div>
<div class="m2"><p>یاد کرد ایزد به جان او به قرآن در قسم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نیست اندر هشت جنت کس چنو با قدر و جاه</p></div>
<div class="m2"><p>نیست در هفت آسمان دیگر چنو یک محتشم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر سر این چرخ گردان جاه او بینی نشان</p></div>
<div class="m2"><p>بر نهاد عرش یزدان نام او بینی رقم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از سعادات جمال و جاه و اقبالش همی</p></div>
<div class="m2"><p>شد به صحرا آفتاب نور و ایمان از ظلم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>رایت «نصر من الله» چون برآمد از عرب</p></div>
<div class="m2"><p>آتش اندر زد به جان شهریاران عجم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خاک پای بوذرش از یک جهان نوذر بهست</p></div>
<div class="m2"><p>درز نعلین بلال او به از صد روستم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>همچو لا شد سرنگون آن کس که او را گفت «لا»</p></div>
<div class="m2"><p>وز سعادت با نعم شد آنکه گفت او را «نعم»</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چرخ اعظم آمده پیش قیامش در رکوع</p></div>
<div class="m2"><p>طارم کسرا از او کسر و ز جاه او به خم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تا بیان شرع و دینش را خداوند جهان</p></div>
<div class="m2"><p>یاد کرد اندر کلام خود نه افزون و نه کم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>«صادقین» بوبکر بود و «قانتین» فرخ عمر</p></div>
<div class="m2"><p>«منفقین» عثمان علی «مستغفرین» آمد بهم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>هر کرا جاهیست زیر جایگاهش چاه‌دان</p></div>
<div class="m2"><p>اندرین معنی مگو هرگز حدیث «لا» و «لم»</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>کافرانی کش ندیدند و نپذرفتند دین</p></div>
<div class="m2"><p>چشم و گوش عقل ایشان بود اعما و اصم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>سرفرازان قریش از زخم تیغش دیده‌اند</p></div>
<div class="m2"><p>هر یکی در حربگاه اندازهٔ خود لاجرم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بر سما دارد چو میکاییل و چون جبریل دوست</p></div>
<div class="m2"><p>بر زمین دارد چو صدیقی و فاروقی خدم</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>عالم ار هجده هزار و صد هزارست از قیاس</p></div>
<div class="m2"><p>نیست اندر کل عالم‌ها چنو یک محتشم</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>با قلم باید علم تا کارها گیرد نظام</p></div>
<div class="m2"><p>او علم بفراخت اندر کل عالم بی قلم</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>از ریاحین سعادات و گل تحقیق و انس</p></div>
<div class="m2"><p>صدهزاران جان به دعوت کرد چون باغ ارم</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>از دم صمصام و رمح چاکران خویش کرد</p></div>
<div class="m2"><p>هم عجم را بی‌ملوک و هم عرب را بی‌صنم</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>مهتر اولاد آدم خواجهٔ هر دو جهان</p></div>
<div class="m2"><p>آنکه یزدانش امات داد بر کل امم</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>از جلال و جاه و اقبالش خدای ذوالجلال</p></div>
<div class="m2"><p>نام او پیش از ازل با نام خود کرده رقم</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>او جدا کرد آن کسانی را سر از تن بی‌خلاف</p></div>
<div class="m2"><p>کز جفا بی حرمتی کردند در بیت‌الحرام</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>آب روی مومنان را کرد او با قدر و جاه</p></div>
<div class="m2"><p>آب چشم کافران را کرد چون آب به قم</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>سرور هر دو جهان و کارساز حشر و نشر</p></div>
<div class="m2"><p>آفتاب دین محمد سید عالی همم</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>مصطفا و مجتبا آن کز برای خیر حال</p></div>
<div class="m2"><p>در ادای وحی جبریلش ندیدی متهم</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>در سخن جز نام او گفتن خطا باشد خطا</p></div>
<div class="m2"><p>در هنر جز نعت او گفتن ستم باشد ستم</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>پیش علم و حلم وجود او کجا دارند پای</p></div>
<div class="m2"><p>عالمان عالمین و کوه قاف و ابرویم</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>ای سنایی جز مدیح این چنین سید مگوی</p></div>
<div class="m2"><p>تا توانی جز به نام نیک او مگشای دم</p></div></div>