---
title: >-
    قصیدهٔ شمارهٔ ۱۶ - در مدح بهرامشاه
---
# قصیدهٔ شمارهٔ ۱۶ - در مدح بهرامشاه

<div class="b" id="bn1"><div class="m1"><p>او کیست مرا یارب او کیست مرا یارب</p></div>
<div class="m2"><p>رویش خوش و مویش خوش باز از همه خوشتر لب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>داده لب و خال او را بی‌خدمت کفر و دین</p></div>
<div class="m2"><p>کرده رخ و زلف او را بی‌منت روز و شب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>منزلگه خورشیدست بی‌نور رخش تیره</p></div>
<div class="m2"><p>دولتکدهٔ چرخ است از قدر و قدش مرکب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از بهر دلفروزی جان گهر و ارکان</p></div>
<div class="m2"><p>وز بهر جانسوزی دست فلک و کوکب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر هر مژهٔ چشمش بنبشته که: لا تعجل</p></div>
<div class="m2"><p>در هر شکن زلفش برخوانده که: لا تعجب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بی بوالعجبی زلفش کاشنید که سر بر زد</p></div>
<div class="m2"><p>مهر از گلوی تنین ماه از دهن عقرب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>میگون لب شیرینش بر ما ترشست آری</p></div>
<div class="m2"><p>می سرکه بخواهد شد چندان نمک اندر لب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دیدی رسن مشکین بر گرد چه سیمین</p></div>
<div class="m2"><p>کو آب گره بندد مانند حباب و حب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ورنه برو و بنگر از دیدهٔ روحانی</p></div>
<div class="m2"><p>در باغ جمال او زلف و زنخ و غبغب</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کافر مژگانش از بت بر ساخت مرا قبله</p></div>
<div class="m2"><p>نازک لب او در تب بگداخت مرا قالب</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>در پنجرهٔ جزعین موسی چکند با بت</p></div>
<div class="m2"><p>در حجرهٔ یاقوتین عیسی چکند با تب</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>جزعش همه دل سوزد لعلش همه جان سوزد</p></div>
<div class="m2"><p>شوخی و خوشی را خود این ملک بود یارب</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>مژگانش همی از ما قربان دل و جان خواهد</p></div>
<div class="m2"><p>های ای دل و هان ای جان من یرغب من یرغب</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>مدح ملک مشرق بهرامشه مسعود</p></div>
<div class="m2"><p>آن بدر فلک رتبت و آن ماه ملک مشرب</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گاو ز می از لطفش چو گاو فلک در تک</p></div>
<div class="m2"><p>شیر فلک از قهرش چون شیر زمین در تب</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>عدل از در او گویان با ظلم که: لا تامن</p></div>
<div class="m2"><p>جود از کف او گویان با بخل که:لا تقرب</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بخل و ستم کلی از درگه و از صدرش</p></div>
<div class="m2"><p>جز این دود گر هرچت آن هست هوالمطلب</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گر عدل عمر خواهی آنک در او بنشین</p></div>
<div class="m2"><p>ور جود علی جویی اینک کف او اشرب</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>در جمله سنایی را در دولت حسن او</p></div>
<div class="m2"><p>در دست بهین سنت مدحست مهین مذهب</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بر آخور او بادا دوبارگی عالم</p></div>
<div class="m2"><p>در دولت و پیروزی هم ادهم و اشهب</p></div></div>