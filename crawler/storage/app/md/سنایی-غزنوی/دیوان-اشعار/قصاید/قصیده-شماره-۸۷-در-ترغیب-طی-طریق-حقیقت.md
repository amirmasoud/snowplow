---
title: >-
    قصیدهٔ شمارهٔ ۸۷ - در ترغیب طی طریق حقیقت
---
# قصیدهٔ شمارهٔ ۸۷ - در ترغیب طی طریق حقیقت

<div class="b" id="bn1"><div class="m1"><p>ای دل به کوی فقر زمانی قرار گیر</p></div>
<div class="m2"><p>بیکار چند باشی دنبال کار گیر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر همچو روح راه نیابی بر آسمان</p></div>
<div class="m2"><p>اصحاب کهف‌وار برو راه غار گیر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا کی حدیث صومعه و زهد و زاهدی</p></div>
<div class="m2"><p>لختی طریق دیر و شراب و قمار گیر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خواهی که ران گور خوری راه شیر رو</p></div>
<div class="m2"><p>خواهی که گنج در شمری دنب مار گیر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خواهی که همچو جعفر طیار بر پری</p></div>
<div class="m2"><p>رو دلبر قناعت اندر کنار گیر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تسلیم کن به صدق و مسلم همی خرام</p></div>
<div class="m2"><p>وین قلب را به بوتهٔ معنی عیار گیر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون طیلسان و منبر وقف از تو روی تافت</p></div>
<div class="m2"><p>زنار و دیر جوی و ره پای دار گیر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از حرص و آز و شهوت دل را یگانه کن</p></div>
<div class="m2"><p>با نفس جنگجوی ره کارزار گیر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>یا چون عمر به دره جهان را قرار ده</p></div>
<div class="m2"><p>یا چون علی به تیغ فراوان حصار گیر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گه یزدجرد مال و گهی ذوالخمار کش</p></div>
<div class="m2"><p>گه زخم دره دارو گهی ذوالفقار گیر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خواهی که بار عسکر بندی ز کان دهر</p></div>
<div class="m2"><p>خرما خمارت آرد سودای خار گیر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چندین هزار سجده بکردی ز غافلی</p></div>
<div class="m2"><p>بنشین یکی و سجدهٔ خود را شمار گیر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>یک سجده کن چو سحرهٔ فرعون بیریا</p></div>
<div class="m2"><p>و آن گه میان جنت ماوی قرار گیر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ای بی بصر حکایت بختنصر مگوی</p></div>
<div class="m2"><p>وز سامری هزار سمر یادگار گیر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بغداد را به طرفهٔ بغداد باز ده</p></div>
<div class="m2"><p>وندر کمین بصره نشین و طرار گیر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>در جوی شهر گوهر معنی طلب مکن</p></div>
<div class="m2"><p>غواص وار گوشهٔ دریا کنار گیر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ای کمزن مقامر بد باز بی‌هنر</p></div>
<div class="m2"><p>خواهی که کم نبازی یاد نگار گیر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>از زخم هفت و هشت نیابی مراد دل</p></div>
<div class="m2"><p>یکبار پنج رود و سه تار و چهار گیر</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>گر چو خلیل سوخته‌ای از غم خلیل</p></div>
<div class="m2"><p>در گلستان مگرد و در آتش قرار گیر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ماهی ز آب نازد و گنجشک از هوا</p></div>
<div class="m2"><p>زین هر دو بط به جوی و کنار بحار گیر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>دست نگار گر نرسد زی نگار چین</p></div>
<div class="m2"><p>ماهی به تابه صید مکن در شکار گیر</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>گر از جهان حرص نگیری ولایتی</p></div>
<div class="m2"><p>سالار آن ولایت تو خاکسار گیر</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>با یک سوار غز و کنی نیست جای نام</p></div>
<div class="m2"><p>باری چو کشته گردی ره بر هزار گیر</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>یا همچو باز ساکن دست ملوک شو</p></div>
<div class="m2"><p>یا همچو زاغ گوشهٔ شاخ کنار گیر</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>زین روزگار هیچ نخیزد مکوش بیش</p></div>
<div class="m2"><p>از روزگار دست بشو روز کار گیر</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>چون ماه علم از فلک فقر بر تو تافت</p></div>
<div class="m2"><p>طاووس وار جلوه به باغ و بهار گیر</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بی‌رنج بادیه نرسی مشعرالحرام</p></div>
<div class="m2"><p>در تاز و تاکباز و هوا را مهار گیر</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>چندین هزار مرد مبارز درین مصاف</p></div>
<div class="m2"><p>کردند حمله‌ها و نمودند دار گیر</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>با صدق و با شهادت رفتند مردوار</p></div>
<div class="m2"><p>گر ره روی تو نیز ره آن قطار گیر</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>چون سوز کار و درد غم دین نداردت</p></div>
<div class="m2"><p>زین راه «برد» و گوشهٔ زرع و شیار گیر</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>زین خواجگان مرتبه جویان بی‌سخا</p></div>
<div class="m2"><p>زین فعل نامشان شرف ننگ و عار گیر</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>زین مال بی نهایت دشمن گرت نصیب</p></div>
<div class="m2"><p>خود را چهار خشت ز دنیا شمار گیر</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>گفت سنایی ار چه محالست نزد تو</p></div>
<div class="m2"><p>تو شکر حال گوی و در کردگار گیر</p></div></div>