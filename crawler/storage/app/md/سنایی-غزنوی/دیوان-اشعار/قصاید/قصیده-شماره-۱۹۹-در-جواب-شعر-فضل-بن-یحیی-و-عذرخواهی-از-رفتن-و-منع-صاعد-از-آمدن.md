---
title: >-
    قصیدهٔ شمارهٔ ۱۹۹ - در جواب شعر فضل بن یحیی و عذرخواهی از رفتن و منع صاعد از آمدن
---
# قصیدهٔ شمارهٔ ۱۹۹ - در جواب شعر فضل بن یحیی و عذرخواهی از رفتن و منع صاعد از آمدن

<div class="b" id="bn1"><div class="m1"><p>فضل یحیاست بر ضعیف و قوی</p></div>
<div class="m2"><p>فضل یحیای صاعد هروی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پادشاه قضات و خواجهٔ شرع</p></div>
<div class="m2"><p>که چو صدرست و دیگران چو روی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از صعود حیات و فضل دلش</p></div>
<div class="m2"><p>نیست جز صورت صراط سوی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پیش ادراک خاطر علویش</p></div>
<div class="m2"><p>محو شد نحو بوعلی نسوی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شعر و خطش ز نور و از ظلمت</p></div>
<div class="m2"><p>قلب شیعی و قالب اموی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شعر و خطش بدیدم و گفتم</p></div>
<div class="m2"><p>تن یزیدی چراست جان علوی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر نبودی بیان او که شدست</p></div>
<div class="m2"><p>فلک و کوکب و رشید غوی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ورنه از رنگ خط و معنی شعر</p></div>
<div class="m2"><p>شدمی هم در آن زمان ثنوی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>یکی او ببرد ازین خادم</p></div>
<div class="m2"><p>پنجی و چاری و سه‌ای و دوی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ای که سنگ هنگ نیست ترا</p></div>
<div class="m2"><p>چون خس از باد خوی یافه دوی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به زیارت به سوی مشتی دون</p></div>
<div class="m2"><p>کعبهٔ کعبتین نه ای چه شوی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به هوا سوی کس نشاید رفت</p></div>
<div class="m2"><p>از پی دین روا بود که روی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نخرامد به خاصه در معراج</p></div>
<div class="m2"><p>سوی قارون رکاب مصطفوی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>کی شوم چون تو گرچه گویم شعر</p></div>
<div class="m2"><p>کی رسد زال در کمال زوی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گر چه با زر و زندگی بشود</p></div>
<div class="m2"><p>آهن از آهنی و جو ز جوی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>تا بود نطق جبرییل به جای</p></div>
<div class="m2"><p>چون کند پشه‌ای در آب دوی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>من به گرد تو خود نیارم گشت</p></div>
<div class="m2"><p>زان که من چشم دردم و تو ضوی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گفتی آیم میا که گر آیی</p></div>
<div class="m2"><p>سوی من با تواضع نبوی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ندی ینزل الله اندر شهر</p></div>
<div class="m2"><p>حنبلی‌وار در دهم بنوی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>که دریغست گوش و چشم کرام</p></div>
<div class="m2"><p>به هوا بینی و هوس شنوی</p></div></div>