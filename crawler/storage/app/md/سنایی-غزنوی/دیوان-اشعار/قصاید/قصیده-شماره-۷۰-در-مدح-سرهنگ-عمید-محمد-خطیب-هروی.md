---
title: >-
    قصیدهٔ شمارهٔ ۷۰ - در مدح سرهنگ عمید محمد خطیب هروی
---
# قصیدهٔ شمارهٔ ۷۰ - در مدح سرهنگ عمید محمد خطیب هروی

<div class="b" id="bn1"><div class="m1"><p>مرد کی گردد به گرد هفت کشور نامور</p></div>
<div class="m2"><p>تا بود زین هشت حرف اوصاف دانش بی‌خبر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مهر جود و حرص فضل و ملک عقل و دست عدل</p></div>
<div class="m2"><p>خلق خوب و طبع پاک و یار نیک و بذل زر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>میم و حا و میم و دال خا و طا و یا و باء</p></div>
<div class="m2"><p>آنکه چون نامش مرکب ازین صورت سیر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>صورت این حرفها نبود چو نیکو بنگری</p></div>
<div class="m2"><p>جز خصال و نام سرهنگ و عمید نامور</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آنکه همچون عقل و دولت رای او را بود و هست</p></div>
<div class="m2"><p>هم بر گفتن صواب و هم بر رفتن ظفر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آنکه آن ساعت که او را چرخ آبستن بزاد</p></div>
<div class="m2"><p>شد عقیم سرمدی از زادن چون او پسر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کرده وهمش عرصهٔ گردون قدرت را مقام</p></div>
<div class="m2"><p>کرده فهمش تختهٔ قانون قسمت را ز بر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سخت کوش از عون بختش دوستان سست زور</p></div>
<div class="m2"><p>سست پای از سهم تیغش دشمنان سخت سر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>غاشیهٔ تمکین او بر دوش دارند آن کسانک</p></div>
<div class="m2"><p>عیبها کردند پیش از آفرینش بر بشر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چارسوی و پنج حس بخت بگرفت آن چنانک</p></div>
<div class="m2"><p>حادثه نه چرخ را از شش جهت بر بست در</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هر که در کانون خصمش آتش کینه فروخت</p></div>
<div class="m2"><p>گر چه با رفعت بود کم عمر گردد چون شرر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>شمس رایش گر فتد ناگاه بر راس و ذنب</p></div>
<div class="m2"><p>گردد از تاثیر آن نور آسمان زرین کمر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ذره‌ای از برق قهرش گر برافتد بر سما</p></div>
<div class="m2"><p>نه فلک چون هفت مرکز باز ماند از مدر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>سایه‌ای از کوه حزمش گر بیفتد بر زمین</p></div>
<div class="m2"><p>بر نگیرد آفتابش تا به حشر از جای بر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ذره‌ای از باد عزمش گر بیابد آفتاب</p></div>
<div class="m2"><p>یک قدم باشد ز خاور سیر او تا باختر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ساحت گردون اگر چون همتش باشد به طول</p></div>
<div class="m2"><p>صدهزاران سال ناید ماه زیر نور خور</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>اعتمادی دارد او بر نصرت بخت آن چنانک</p></div>
<div class="m2"><p>هر سلاحی در خزانهٔ او بیابی جز سپر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ای به صحرا شتابت باد صرصر همچو کوه</p></div>
<div class="m2"><p>وی به شاهین درنگت کوه ثهلان همچو زر</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>گر مقنع ماهی از چاهی برآورد از حیل</p></div>
<div class="m2"><p>پس خدایی کرد دعوی گو بیا اندر نگر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>در تو کز گردون ملکت صدهزاران آفتاب</p></div>
<div class="m2"><p>می برون آری و هستی و هر زمانی بنده‌تر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بود دارالملک بو یحیا هوای آن زمین</p></div>
<div class="m2"><p>کاندرو امروز دارد عرض پاکت مستقر</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>لیک تا والی شدی در وی ز شرم لطف تو</p></div>
<div class="m2"><p>اسب بو یحیا نیفگندست آنجا رهگذر</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>از عفونت در هوای او اگر دهقان چرخ</p></div>
<div class="m2"><p>زندگانی کاشتی مرگ آمدی در وقت بر</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>شد ز اقبال و ز فرت در لطافت آن چنانک</p></div>
<div class="m2"><p>زهر قاتل گر غذا سازی نیابی زو ضرر</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>مایهٔ آتش برو غالب چنان شد کز تفش</p></div>
<div class="m2"><p>آب گشتی ابر بهمن در هوا همچون مطر</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>شد ز سعیت گاه پاکی ز اعتدال اینک چنانک</p></div>
<div class="m2"><p>باد نپذیرد غبار و آب نگذارد شکر</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>شاد باش ای از تو عقل محتشم را احتشام</p></div>
<div class="m2"><p>دیر زی ای از تو چرخ محترم را مفتخر</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>روزگاری گاه حل و عقد اندر دو صفت</p></div>
<div class="m2"><p>همچنین چون اصل نفعی نیست خالی ز ضر</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>از پی نادیدن سهمت چو اندازی تو تیر</p></div>
<div class="m2"><p>دشمن از بیم تو بر پیکان برافشاند بصر</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>از تو و خشم تو بینا دل هراسد بهر آنک</p></div>
<div class="m2"><p>چون نبیند کی هراسد مور کور از مار گر</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>میخ کردار ار جهد دشمن ز پیشت پای او</p></div>
<div class="m2"><p>بی خبر او را کشد سوی تو بر کردار خر</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>دولتی داند که یابد سایه گاهی چون جحیم</p></div>
<div class="m2"><p>دشمنی کز بیم شمشیر تو باشد با خطر</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>دیدهٔ دشمن کند تیرت چو نقش چشم بند</p></div>
<div class="m2"><p>گر چه در ظلمت عدو چون دیده‌ها سازد مقر</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>گر هدف سازد قمر را تیر اختر دوز تو</p></div>
<div class="m2"><p>تا قیامت جز قران نبود زحل را با قمر</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>اندر آن روزی که پیدا گردد از جنگ یلان</p></div>
<div class="m2"><p>تیرهای دیده دوز و تیغهای سینه در</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>تیغها گردد ز حلق زردرویان سرخ رو</p></div>
<div class="m2"><p>نیزه‌ها گردد ز فرق تاجداران تاجور</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>گرز بندد پرده‌ای بی جامه بر راه قضا</p></div>
<div class="m2"><p>تیغ سازد خندقی بی عبره بر راه قدر</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>از نهیب تیر و بانگ کوس بگذارند باز</p></div>
<div class="m2"><p>چشمهای سر عیان و گوشهای حس خبر</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>نای روئین گویی آنجا نفخ صور اولست</p></div>
<div class="m2"><p>کز یکی بانگش روان از تن رمد زنگ از صور</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>روی داده جان بی تن سوی بالا چون دعا</p></div>
<div class="m2"><p>رای کرده جسم بی جان سوی پستی چون قدر</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>همچو هامون قیامت گرد میدان جوق جوق</p></div>
<div class="m2"><p>زمره‌ای اندر عنا و مجمعی اندر بطر</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>کرده خالی پیش از آسیب سنان و گرز تو</p></div>
<div class="m2"><p>روح نفسانی دماغ و نفس حیوانی جگر</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>ناگهی باشد برون تازی چو بر چرخ آفتاب</p></div>
<div class="m2"><p>سایه‌وار از بیم جان بگریزد از پشت حشر</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>نیزه‌ای اندر بنان اختر کن و جیحون مصاف</p></div>
<div class="m2"><p>باره‌ای در زیر ران هامون برو گردون سیر</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>باره‌ای کز حرص رفتن خواهدی کش باشدی</p></div>
<div class="m2"><p>همچو جیحون جمله پای و همچو صرصر جمله پر</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>راکبش گر سوی مشرق تازد از مغرب بر او</p></div>
<div class="m2"><p>گر چه در روزه‌ست مفتی کی نهد حکم سفر</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>سم او سنبد حجر را در زمان الماس وار</p></div>
<div class="m2"><p>پس بزودی زو برون آید چو آتش از حجر</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>هر که نامت بر زبان راند از بدی در یک زمان</p></div>
<div class="m2"><p>خضروارش حاضر آرد نزد ایشان ما حضر</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>گوهری در کف تو زاده ز دریای اجل</p></div>
<div class="m2"><p>آفت سنگین دلان وز آهن و سنگش گهر</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>بر و بحر ار ز آتش و آبش بیابد بهره‌ای</p></div>
<div class="m2"><p>بر گردد همچو بحر و بحر گردد همچو بر</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>هیزم دوزخ بود گر آتش شمشیر تو</p></div>
<div class="m2"><p>می‌فزاید هر زمان صد ساله هیزم در سقر</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>آتش ار هیزم کند کم در طبیعت طرفه نیست</p></div>
<div class="m2"><p>آتشی کو هیزم افزاید همی این طرفه‌تر</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>با چنین اسبی و تیغی قلعهٔ دشمن شده</p></div>
<div class="m2"><p>همچو شارستان لوط از کوششت زیر و زبر</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>جنگها کردی چنان چون گفت مختاری به شعر</p></div>
<div class="m2"><p>بسکه از تیغ تو مجبورند اعدا و کفر</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>ای چو عثمان و چو حیدر شرم روی و زورمند</p></div>
<div class="m2"><p>وی چو بکر و چو عمر راست گوی و دادگر</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>جبرییل از سدره گویان گشته کز اقبال و روز</p></div>
<div class="m2"><p>نعمت حق را سر آل خطیبی قد شکر</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>خون اعدا از چه ریزی کز برای نصرتت</p></div>
<div class="m2"><p>مویشان در عرقشان گشته‌ست همچون نیشتر</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>با چنان بت کش علایی و صت کرد اندر غزل</p></div>
<div class="m2"><p>خانهٔ غم پست کرد آن کامران و نوش خور</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>باز چون در بحر فکرت غوطه‌خوردی بهر نظم</p></div>
<div class="m2"><p>گوهرین گردد ز بویهٔ فضل تو در دل فکر</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>هیچ فاضل در جان بی‌نثر و بی‌نظمت نراند</p></div>
<div class="m2"><p>بر زبان معنی بکر و در بیان لفظ غرر</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>آب از آتش گر نزاید هرگز و هرگز نزاد</p></div>
<div class="m2"><p>ز آتش طبعت چرا زاده‌ست چندین شعر تر</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>شعرها پیشت چنان باشد که از شهر حجاز</p></div>
<div class="m2"><p>با یکی خرما کسی هجرت کند سوی هجر</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>گر چه صدرت منشاء شعرست و جای شاعران</p></div>
<div class="m2"><p>گفتمت من نیز شعری بی تکلف ماحضر</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>بوحنیفه گر چه بود اندر شریعت مقتدا</p></div>
<div class="m2"><p>کس نشست از آب منسوخی سخنهای ز فر</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>زاغ را با لحن بد هم بر شجر جایست از آنک</p></div>
<div class="m2"><p>آشیانهٔ بلبل تنها نباشد یک شجر</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>گر چه استادان هنرمندند من شاگرد را</p></div>
<div class="m2"><p>یک هنر باشد که پوشد هر چه باشد از هنر</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>آب دریا گرچه بسیارست چو تلخست و شور</p></div>
<div class="m2"><p>هرکرا تشنه‌ست لابد رفت باید زی شمر</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>شیر از آهو گرچه افزونست لیکن گاه بوی</p></div>
<div class="m2"><p>ناف آهو فضل دارد بر دهان شیر نر</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>گر چه استادان من گفتند پیش از من ثنات</p></div>
<div class="m2"><p>لیک پیدا نبود از پیش و پس اصل خیر و شر</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>خانهٔ آحاد پیشست از الوف اندر حساب</p></div>
<div class="m2"><p>در نگر در پیشتر تا بیشتر یابی خطر</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>یافتم تاثیر اقبال از برای آنکه کرد</p></div>
<div class="m2"><p>اختر مدح تو اندر طالع شعرم نظر</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>بیش از این تاثیر چبود کز ثناهای تو شد</p></div>
<div class="m2"><p>شاه را گفت من پیش از قبولت پر درر</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>ور خود از صدر تو یابم هیچ توقیع قبول</p></div>
<div class="m2"><p>یافت طبعم ملک حر و شخص ملک شوشتر</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>تا ز روی مایه مردم را نه از روی نسب</p></div>
<div class="m2"><p>چار عنصر مادر آمد هفت سیاره پدر</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>باد صبح ناصحت چون روز عقبا بی‌مسا</p></div>
<div class="m2"><p>باد شام حاسدت تا روز محشر بی‌سحر</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>بر تو فرخ باد و شایان و مبارک این سه چیز:</p></div>
<div class="m2"><p>خلعت سلطان و شعر بنده و ماه صفر</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>باد امرت در زمین چون چار عنصر پیش رو</p></div>
<div class="m2"><p>باد نامت در زمان چون هفت سیاره سمر</p></div></div>