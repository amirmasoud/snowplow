---
title: >-
    قصیدهٔ شمارهٔ ۱۲۰
---
# قصیدهٔ شمارهٔ ۱۲۰

<div class="b" id="bn1"><div class="m1"><p>تا کی دم از علایق و طبع فلک زنیم</p></div>
<div class="m2"><p>تا کی مثل ز جوهر دیو و ملک زنیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا کی غم امام و خلیفهٔ جهان خوریم</p></div>
<div class="m2"><p>تا کی دم از علی و عتیق و فلک زنیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دوریم از سماع و قرینیم با صداع</p></div>
<div class="m2"><p>تا ما همی سقف به نوای سلک زنیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هرگز نبوده دفتر و دف در مصاف عشق</p></div>
<div class="m2"><p>تیر امید کی چو شهان بر دفک زنیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا کی ز راه رشک برین و بر آن رویم</p></div>
<div class="m2"><p>بهر گل و کلالهٔ خوبان کلک زنیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا کی به زیر دور فلک چون مقامران</p></div>
<div class="m2"><p>از بهر برد خویش دم لی و لک زنیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دست حریف خوبتر آید که در قمار</p></div>
<div class="m2"><p>شش پنج نقش ماست همین ما دو یک زنیم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>یک دم شویم همچو دم آدم و چنو</p></div>
<div class="m2"><p>اندر سرای عشق دمی مشترک زنیم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آن به که همچو شعر سنای گه سنا</p></div>
<div class="m2"><p>میخ طناب خیمه برون از فلک زنیم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بر یاد روی و موی صنم صد هزار بوس</p></div>
<div class="m2"><p>بر دامن یقین و گریبان شک زنیم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گر چه ستد زمانه چک و چاک را ز ما</p></div>
<div class="m2"><p>آتش نخست در شکن چاک و چک زنیم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>طوفان عام تا چکند چون بسان سام</p></div>
<div class="m2"><p>خر پشته در سفینهٔ نوح و ملک زنیم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ای ما ز لعل پر نمکت چون نمک در آب</p></div>
<div class="m2"><p>هرگز بود که زیور ما بر محک زنیم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>زین جوهر و عرض غرض ما همین یکیست</p></div>
<div class="m2"><p>گر چه همی ز قهر سما بر سمک زنیم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ما را طعام خوان خدا آرزو شدست</p></div>
<div class="m2"><p>یک دم به پای تا دو سخن بر نمک زنیم</p></div></div>