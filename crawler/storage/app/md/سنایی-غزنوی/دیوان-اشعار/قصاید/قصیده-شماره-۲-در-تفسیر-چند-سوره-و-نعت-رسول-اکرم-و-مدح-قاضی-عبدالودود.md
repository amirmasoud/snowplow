---
title: >-
    قصیدهٔ شمارهٔ ۲ - در تفسیر چند سوره و نعت رسول اکرم و مدح قاضی عبدالودود
---
# قصیدهٔ شمارهٔ ۲ - در تفسیر چند سوره و نعت رسول اکرم و مدح قاضی عبدالودود

<div class="b" id="bn1"><div class="m1"><p>کفر و ایمان را هم اندر تیرگی هم در صفا</p></div>
<div class="m2"><p>نیست دارالملک جز رخسار و زلف مصطفا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>موی و رویش گر به صحرا نا وریدی مهر و لطف</p></div>
<div class="m2"><p>کافری بی‌برگ ماندستی و ایمان بی‌نوا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نسخهٔ جبر و قدر در شکل روی و موی اوست</p></div>
<div class="m2"><p>این ز «واللیل» ت شود معلوم آن از «والضحا»</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر قسیم کفر و ایمان نیستی آن زلف و رخ</p></div>
<div class="m2"><p>کی قسم گفتی بدان زلف و بدان رخ پادشا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کی محمد: این جهان و آن جهانی نیستی</p></div>
<div class="m2"><p>لاجرم اینجا نداری صدر و آنجا متکا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>رحمتت زان کرده‌اند این هر دو تا از گرد لعل</p></div>
<div class="m2"><p>این جهان را سرمه بخشی آن جهان را توتیا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اندرین عالم غریبی، زان همی گردی ملول</p></div>
<div class="m2"><p>تا «ارحنا یا بلالت» گفت باید برملا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عالمی بیمار بودند اندرین خرگاه سبز</p></div>
<div class="m2"><p>قاید هر یک وبال و سایق هر یک وبا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زان فرستادیمت اینجا تا ز روی عاطفت</p></div>
<div class="m2"><p>عافیت را همچو استادان درآموزی شفا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گر ز داروخانه روزی چند شاگردت به امر</p></div>
<div class="m2"><p>شربتی ناوردشان این جا به حکم امتلا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گر ترا طعنی کنند ایشان مگیر از بهر آنک</p></div>
<div class="m2"><p>مردم بیمار باشد یافه گوی و هرزه لا</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تابش رخسار تست آن را که می‌خوانی صباح</p></div>
<div class="m2"><p>سایهٔ زلفین تست آنجا که می‌گویی مسا</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>روبروی تو کز آنجا جانت را «ما و دعک»</p></div>
<div class="m2"><p>شو به زلف تو کزین آتش دلت را «ما قلا»</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>در دو عالم مر ترا باید همی بودن پزشک</p></div>
<div class="m2"><p>لیکن آنجا به که آنجا، به بدست آید دوا</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>هر که اینجا به نشد آنجا برو داروش کن</p></div>
<div class="m2"><p>کاین چنین معلول را به سازد آن آب و هوا</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>لاجرم چندان شرابت بخشم از حضرت که تو</p></div>
<div class="m2"><p>از عطا خشنود گردی و آن ضعیفان از خطا</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>دیو از دیوی فرو ریزد همی در عهد تو</p></div>
<div class="m2"><p>آدمی را خاصه با عشق تو کی ماند جفا</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>پس بگفتش: ای محمد منت از ما دار از آنک</p></div>
<div class="m2"><p>نیست دارالملک منتهای ما را منتها</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>نه تو دری بودی اندر بحر جسمانی یتیم</p></div>
<div class="m2"><p>فضل ما تاجیت کرد از بهر فرق انبیا</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>نی تو راه شهر خود گم کرده بودی ز ابتدا</p></div>
<div class="m2"><p>ما ترا کردیم با همشهریانت آشنا</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>غرقهٔ دریای حیرت خواستی گشتن ولیک</p></div>
<div class="m2"><p>آشنایی ما برونت آورد ازو بی‌آشنا</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بی نعمت خواست کردن مر ترا تلقین حرص</p></div>
<div class="m2"><p>پیش از آن کانعام ما تعلیم کردت کیمیا</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>با تو در فقر و یتیمی ما چه کردیم از کرم</p></div>
<div class="m2"><p>تو همان کن ای کریم از خلق خود با خلق ما</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>مادری کن مر یتیمان را بپرورشان به لطف</p></div>
<div class="m2"><p>خواجگی کن سایلان را طعمشان گردان وفا</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>نعمت از ما دان و شکر از فضل ما کن تا دهیم</p></div>
<div class="m2"><p>مر ترا زین شکر نعمت نعمتی دیگر جزا</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>از زبان خود ثنایی گوی ما را در عرب</p></div>
<div class="m2"><p>تا زبان ما ترا اندر عجم گوید ثنا</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>آفتاب عقل و جان اقضی القضاة دین که هست</p></div>
<div class="m2"><p>چون قضای آسمان اندر زمین فرمانروا</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>آن سر اصحاب نعمان کز پی کسب شرف</p></div>
<div class="m2"><p>هر زمانی قبله بر پایش دهد قبله دعا</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>با بقای عدل او نشگفت اگر در زیر چرخ</p></div>
<div class="m2"><p>شخص حیوان همچو نوع و جنس نپذیرد فنا</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>تا نسیم او بر بوستان دین نجست</p></div>
<div class="m2"><p>شاخ دین نشو بود و بیخ سنت بی‌نما</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>در حریم عدل او تا او پدید آید به حکم</p></div>
<div class="m2"><p>خاصیت بگذاشت گاه که ربودن کهربا</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>تا بگفت او جبریان را ماجرای امر و نهی</p></div>
<div class="m2"><p>تا بگفت او عدلیان را رمز تسلیم و رضا</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>باز رستند از بیان واضحش در امر و حکم</p></div>
<div class="m2"><p>جبری از تعطیل شرع و عدی از نفی قضا</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>این کمر ز «ایاک نعبد» بست در فرمان شرع</p></div>
<div class="m2"><p>وان دگر تاجی نهاد از «یفعل الله مایشا»</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>ای بنانت حاجب اندر شاهراه مصطفا</p></div>
<div class="m2"><p>وی زبانت نایب اندر زخم تیغ مرتضا</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>هر کجا گام تو آمد افتخار آرد زمین</p></div>
<div class="m2"><p>هر کجا عدل تو آمد انقیاد آرد سما</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>سیف حقی از پی آن سیف حق آمد روان</p></div>
<div class="m2"><p>مفتی شرقی از آن مشرق شدست اصل ضیا</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>مفتی شرقت نه زان خواند همی سلطان که هست</p></div>
<div class="m2"><p>جز تو در مغرب دیگر مفتی و دگر مقتدا</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>بلکه سلطان مفتی شرقت بدان خواند همی</p></div>
<div class="m2"><p>هر کجا مفتی تو باشی غرب خود نبود روا</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>همقرینی علم دین را همچو فکرت را خرد</p></div>
<div class="m2"><p>همنشینی ظلم و کین را همچو فطنت را ذکاء</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>چون تو موسی وار بر کرسی برآیی گویدت</p></div>
<div class="m2"><p>عیسی از چرخ چهارم کی محمد مرحبا</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>جان پاکان گرسنهٔ علم تواند از دیرباز</p></div>
<div class="m2"><p>سفره اندر سفره بنهادی و در دادی صلا</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>لطف لفظت کی شناسد مرد ژاژ و ترهات</p></div>
<div class="m2"><p>«من و سلوی» را چه داند مرد سیر و گندنا</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>هر که از آزار تو پرهیز کرد از درد رست</p></div>
<div class="m2"><p>راست گفتند این مثل «الا حتما اقوی الدوا»</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>مالش دشمن ترا حاجت نیفتد بهر آنک</p></div>
<div class="m2"><p>چاکری داری چو گردون کش همی درد قفا</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>هر شقی کز آتش خشم تو گردد کام خشک</p></div>
<div class="m2"><p>بر لب دریا به جانش آب نفروشد سقا</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>لاف «نحن الغالبون» بسیار کس گفتند لیک</p></div>
<div class="m2"><p>«غالبون» شان گشت «آمنا» چو ثعبان شد عصا</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>زرق سیماب و رسن هرگز کجا ماندی بجای</p></div>
<div class="m2"><p>چون برآید ناگه از دریای قدرت اژدها</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>گه طلب کن بی سراج ماه در صحرای خوف</p></div>
<div class="m2"><p>گه طلب کن بی‌مزاج زهره در باغ رجا</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>ماه را آنجا نبود کو ترا گوید که چون</p></div>
<div class="m2"><p>زهره را آن زهر نبود کو ترا گوید چرا</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>رو که نیکو جلوه کردت روزگار اندر خلا</p></div>
<div class="m2"><p>شو که زیبا پروریدت کردگار اندر ملا</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>ای ز تو اعقاب تو طاهر، چو سادات از نبی</p></div>
<div class="m2"><p>وی ز تو اسلاف تو ظاهر چو ز آصف بر خیا</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>باز یابی آنچه ایزد کرد با تو نیکویی</p></div>
<div class="m2"><p>هم درین صورت که گفتی صورت این ماجرا</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>این نه بس کاندر ادای شکر حق بر جان تو</p></div>
<div class="m2"><p>دعوی انعام او را «واضحی» باشد گوا</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>روز و شب در عالم اسلام، علم و حلم تست</p></div>
<div class="m2"><p>آن یکی از آل عباس این دگر ز آل عبا</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>گر چه روزی چند گشتی گرد این مشکین بساط</p></div>
<div class="m2"><p>گر چه روزی چند بودی گرد این نیلی غطا</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>همچنان کاندر فضای آسمان مطلقی</p></div>
<div class="m2"><p>صورتست این دار و گیر و حبس و بند اندر قضا</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>نی به علم و حلم تو سوگند خوردست آفتاب</p></div>
<div class="m2"><p>کز تو هرگز لطف یزدانی نخواهد شد جدا</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>ای همه اعدای دین را اندرین نیلی خراس</p></div>
<div class="m2"><p>آس کرده زیر پر فطنت و فر و دها</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>بازتاب اکنون عنان هم سوی آن اقلیم از آنک</p></div>
<div class="m2"><p>آرد چون شد کرده اکنون خانه بهتر کاسیا</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>تا همه آن بینی آنجا کت کند چشم آرزو</p></div>
<div class="m2"><p>تا همه آن یابی آنجا کت کند رای اقتضا</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>نی ز قصد حاسدانت در بدایت شهر تو</p></div>
<div class="m2"><p>بر تو چونان بود چون بر آل یاسین کربلا</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>نی ز اول دوستانت را نبودی با تو الف</p></div>
<div class="m2"><p>نی چنان گشتی کنون کز خطبهٔ چین و ختا</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>از برای مهر چهر جانفزایت را همی</p></div>
<div class="m2"><p>بر دو چشم مردمان غیرت بود مردم گیا</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>نی کنون از لطف ربانی همه اقلیم شرع</p></div>
<div class="m2"><p>از تو خرم شد چه بر داوودیان شهر سبا</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>نی تو حیران مانده بودی در تماشاگه عجب</p></div>
<div class="m2"><p>نی تو ره گم کرده بودی در بیابان ریا</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>آن چنانت ره نمود ایزد به پاکی تا شدند</p></div>
<div class="m2"><p>خرقه‌پوشان فلک در جنب تو ناپارسا</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>نی تو در زندان چاه حاسدان بودی ببند</p></div>
<div class="m2"><p>هم‌نشین ذل و غریبی هم عنان رنج و عنا</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>نی خدا از چاه و بند حاسدانت از روی فضل</p></div>
<div class="m2"><p>بر کشید و برنشاندت بر بساط کبریا</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>بی‌پدر بودی ولیک اکنون چنانی کز شرف</p></div>
<div class="m2"><p>پادشاه دین همی در دین پدر خواند ترا</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>آن چنان گشتی که بد گویت کنون بی‌روی تو</p></div>
<div class="m2"><p>نه همی در دل بهی بیند نه اندر جان بها</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>ای یتیمی دیده اکنون با یتیمان لطف کن</p></div>
<div class="m2"><p>وی غریبی کرده اکنون با غریبان کن وفا</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>«الفلق» می‌خوان و می‌دان قصد این چندین حسود</p></div>
<div class="m2"><p>«والضحی» می‌خوان و می‌کن شکر این چندین عطا</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>ای مرا از یک نعم پیوسته با چندین نعم</p></div>
<div class="m2"><p>وی مرا از یک بلی ببریده از چندین بلا</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>شکرت ار بر کوه برخوانم به یک آواز، من</p></div>
<div class="m2"><p>از برای حرص مدحت صد همی گردد صدا</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>شعر من نیک از عطای نیک تست ایرا که مرغ</p></div>
<div class="m2"><p>هر کجا به برگ بیند به برون آرد نوا</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>قربت تو باز هستم کرد در صحرای انس</p></div>
<div class="m2"><p>شربت تو باز مستم کرد در باغ صفا</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>گر غنی شد جان و عقل از تو عجب نبود از آنک</p></div>
<div class="m2"><p>آمدست این از پیمبر «طائف الحج الغنا»</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>ور چه تن را این غرض حاصل نیامد زان مدیح</p></div>
<div class="m2"><p>ای بداگر جان ما را افتد از مدحت بدا</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>مانده‌ام مخمور آن شربت هنوز از پار باز</p></div>
<div class="m2"><p>پای سست و سر گران این از طمع آن از حیا</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>دی به دل گفتم که این را چیست دار و نزد تو</p></div>
<div class="m2"><p>گفت دل: داروی این نزدیک من «منهابها»</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>تا کلاه از روح دارد عامل کون و فساد</p></div>
<div class="m2"><p>تا قبا از عقل دارد قابل علم و بقا</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>فرق و شخص دشمنت پوشیده بادا تا ابد</p></div>
<div class="m2"><p>هم به مقلوب کلاه و هم به تصحیف قبا</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>باد برخوان وجودت روز و شب تصحیف صیف</p></div>
<div class="m2"><p>باد بر جان حسودت سال و مه قلب شتا</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>عالم از علم تو چونان باد کز مادر صبی</p></div>
<div class="m2"><p>خلقت از خلق تو چونان باد کز گلبن صفا</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>خلعت و احسان شاعر سنت هم نام تست</p></div>
<div class="m2"><p>باد ز احسان تو زین سنت سنایی را سنا</p></div></div>