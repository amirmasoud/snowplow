---
title: >-
    قصیدهٔ شمارهٔ ۱۸۳ - در مدح بهرامشاه
---
# قصیدهٔ شمارهٔ ۱۸۳ - در مدح بهرامشاه

<div class="b" id="bn1"><div class="m1"><p>گرد رخت صف زده لشکر دیو و پری</p></div>
<div class="m2"><p>ملک سلیمان تراست گم مکن انگشتری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پردهٔ خوبی بساز امشب و بیرون خرام</p></div>
<div class="m2"><p>زهرهٔ زهره بسوز زان رخ چون مشتری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از پی موی تو شد بر سر کوی خرد</p></div>
<div class="m2"><p>دیدهٔ اسلامیان سجده‌گه کافری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کفر ممکن شدی در سر زلفین تو</p></div>
<div class="m2"><p>گر بنکردی لبت دعوی پیغمبری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عشق تو آورد خوی خستن بی مرهمی</p></div>
<div class="m2"><p>هجر تو آورد رسم کشتن بی داوری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هجر تو مانند وصل هست روا بهر آنک</p></div>
<div class="m2"><p>بر سر بازار نیز کور بود مشتری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>صلح جدا کن ز جنگ زان که نه نیکو بود</p></div>
<div class="m2"><p>دستگه شیشه‌گر پایگه گازری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عقل در دل بکوفت عشق تو گفت اندر آی</p></div>
<div class="m2"><p>صدر سرای آن تست گر به حرم ننگری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>عشق تو همچون فلک خرمن شادی بداد</p></div>
<div class="m2"><p>صد کس را یک ققیز یک کس را صد گری</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>باشم گستاخ وار با تو که لاشی کند</p></div>
<div class="m2"><p>صد گنه این سری یک نظر آن سری</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چشم تو هر دم به طعن گوید با چشم من</p></div>
<div class="m2"><p>مهره بدست تو بود کم زده‌ای خون گری</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>حسن تو جاوید باد تا که ز سودای تو</p></div>
<div class="m2"><p>طبع سنایی به شعر ختم کند شاعری</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چون تو ز دل برنخورد باری بر آب کار</p></div>
<div class="m2"><p>خدمت خسرو گزین تا تو ز خود برخوری</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>خسرو خسرو نسب سلطان بهرامشاه</p></div>
<div class="m2"><p>آنکه چو بهرام هست خاک درش مشتری</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>هست سنایی به شعر بندهٔ درگاه او</p></div>
<div class="m2"><p>زان که مر او راست بس خوی ثنا پروری</p></div></div>