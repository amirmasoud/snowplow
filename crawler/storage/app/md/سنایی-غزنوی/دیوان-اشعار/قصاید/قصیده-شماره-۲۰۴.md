---
title: >-
    قصیدهٔ شمارهٔ ۲۰۴
---
# قصیدهٔ شمارهٔ ۲۰۴

<div class="b" id="bn1"><div class="m1"><p>ای خواجه ترا در دل اگر هست صفایی</p></div>
<div class="m2"><p>بر هستی آن چون که ترا نیست گوایی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر باطنت از نور یقینست منور</p></div>
<div class="m2"><p>بر ظاهر تو چون که عیان نیست صفایی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آری چو بود صورت تحقیق چو تلبیس</p></div>
<div class="m2"><p>بیدار شو از هرچه صوابی و خطایی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دعوی که مجرد بود از شاهد معنی</p></div>
<div class="m2"><p>باطل شودش اصل به چونی و چرایی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر شاهد وقت تو بود حشمت و نعمت</p></div>
<div class="m2"><p>بیمار دلت را نبود هیچ شفایی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کاین حشمت و نعمت دو حجایند یقین‌دان</p></div>
<div class="m2"><p>کاندر دو جهان زین دو بتر نیست بلایی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>این هست وجودش متعلق به مجازی</p></div>
<div class="m2"><p>و آن هست حصولش متولد ز ریایی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا این دو رفیق بد همراه تو باشند</p></div>
<div class="m2"><p>هرگز نبود خواجه ترا راه به جایی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تو بسته شده در گره آز شب و روز</p></div>
<div class="m2"><p>وز دست هوا خورده به ناکام قفایی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بفروخته دین را به یکی گرده و کرده</p></div>
<div class="m2"><p>پوشیده تن خویش به رنگی و عبایی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بویی نرسید به مشامت ز حقیقت</p></div>
<div class="m2"><p>همچون سگ دیوانه به هر گرد سرایی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>در دعوی مطلق چو رسولی شده مرسل</p></div>
<div class="m2"><p>در لفظ به هر ساعت چونی و چرایی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>تا جسم و دلت هست به هم هر دو مرکب</p></div>
<div class="m2"><p>نایدت زد و برد قبایی و کلایی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>تا زین تن آلوده برون ناید کبرت</p></div>
<div class="m2"><p>حاصل نشود بهر خدا هیچ رضایی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بیرون کن ازین خانهٔ خاکی دل خود را</p></div>
<div class="m2"><p>وآن گه ز دلت ساز تو ارضی و سمایی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گر خاطر اوهام برنده شود از خلق</p></div>
<div class="m2"><p>بر خالق خود گوید بی مثل ثنایی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ار حق به جز از حق نکند هیچ قبولی</p></div>
<div class="m2"><p>وندر خور خود خواهد ملکی و عطایی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>آن دل که بدین سان بود اندر ره توحید</p></div>
<div class="m2"><p>حقا که بود موقن و باقی به بقایی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>در حوصلهٔ تنگ تو زین بیش نگنجد</p></div>
<div class="m2"><p>این هدیه چو دادند نخواهند جزایی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>کاین فضل الاهی بود اندر ره توحید</p></div>
<div class="m2"><p>وندر ره توحید چنین جوی بهایی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>شونیست شو از خویش و میندیش کزان پس</p></div>
<div class="m2"><p>یکسان شمری هر دو: جفایی و وفایی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>اندر صفتت نیست چه نامی و چه ننگی</p></div>
<div class="m2"><p>بر بام خرابات چه جغدی چه همایی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>گر نزد سنایی بشدی خلقت اول</p></div>
<div class="m2"><p>از دیده نمودی ره تحقیق سنایی</p></div></div>