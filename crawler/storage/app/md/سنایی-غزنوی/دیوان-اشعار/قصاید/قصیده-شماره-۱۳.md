---
title: >-
    قصیدهٔ شمارهٔ ۱۳
---
# قصیدهٔ شمارهٔ ۱۳

<div class="b" id="bn1"><div class="m1"><p>ای ازل دایه بوده جان ترا</p></div>
<div class="m2"><p>وی خرد مایه داده کان ترا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای جهان کرده آستین پر جان</p></div>
<div class="m2"><p>از پی نثر آستان ترا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سالها بهر انس روح‌القدس</p></div>
<div class="m2"><p>بلبلی کرده بوستان ترا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شسته از آب زندگانی روح</p></div>
<div class="m2"><p>از پی فتنه ارغوان ترا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کرده ایزد ز کارخانهٔ عقل</p></div>
<div class="m2"><p>سیرت و خوی و طبع و سان ترا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تیرهای یقین به شاگردی</p></div>
<div class="m2"><p>چون کمان بوده مر گمان ترا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کرده بر روی آفتاب فلک</p></div>
<div class="m2"><p>نقش دستان و داستان ترا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نور روی از سیاهی مویت</p></div>
<div class="m2"><p>کرده مغزول پاسبان ترا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از برای خمار مستانت</p></div>
<div class="m2"><p>نوش دان کرده بوسه‌دان ترا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از برون تن تو بتوان دید</p></div>
<div class="m2"><p>از لطیفی درون جان ترا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>پرده داری به داد گویی طبع</p></div>
<div class="m2"><p>از پی مغز استخوان ترا</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>از نحیفی همی نبیند هیچ</p></div>
<div class="m2"><p>چشم سر صورت دهان ترا</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>از لطیفی همی نیابد باز</p></div>
<div class="m2"><p>چشم سر سیرت نهان ترا</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>در میانست هر کرا هستی‌ست</p></div>
<div class="m2"><p>از پی نیستی میان ترا</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>هیچ باکی مدار گر زه نیست</p></div>
<div class="m2"><p>آن کمان شکل ابروان ترا</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>زان که تیر فلک همی هر دم</p></div>
<div class="m2"><p>زه کند در ثنا کمان ترا</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>تا چسان دو لبت رها کرده</p></div>
<div class="m2"><p>ناتوان نرگس توان ترا</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>زان دو تا عیسی و دو تا بیمار</p></div>
<div class="m2"><p>شرم ناید همی روان ترا</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>از پی چه معالجت نکنند</p></div>
<div class="m2"><p>آن دو عیسی دو ناتوان ترا</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ای وفا همعنان عنای ترا</p></div>
<div class="m2"><p>وی بقا همنشین نشان ترا</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>نافرید آفریدگار مگر</p></div>
<div class="m2"><p>جز زیان مرا زبان ترا</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چند زیر لبم دهی دشنام</p></div>
<div class="m2"><p>تا ببندم میان زیان ترا</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>می بدان آریم که برخیزم</p></div>
<div class="m2"><p>بوسه باران کنم لبان ترا</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>به بیمم دهی به زخم سنان</p></div>
<div class="m2"><p>کی گذارم بدین عنان ترا</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>تو سنان تیز کن از دل و چشم</p></div>
<div class="m2"><p>شد سنایی سپر سنان ترا</p></div></div>