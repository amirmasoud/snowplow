---
title: >-
    قصیدهٔ شمارهٔ ۳۴ - در دل نبستن به مهر دنیا
---
# قصیدهٔ شمارهٔ ۳۴ - در دل نبستن به مهر دنیا

<div class="b" id="bn1"><div class="m1"><p>مسلمانان سرای عمر، در گیتی دو در دارد</p></div>
<div class="m2"><p>که خاص و عام و نیک و بد بدین هر دو گذر دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دو در دارد حیات و مرگ کاندر اول و آخر</p></div>
<div class="m2"><p>یکی قفل از قضا دارد، یکی بند از قدر دارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو هنگام بقا باشد قضا این قفل بگشاید</p></div>
<div class="m2"><p>چو هنگام فنا آید قدر این بند بردارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اجل در بند تو دایم تو در بند امل آری</p></div>
<div class="m2"><p>اجل کار دگر دارد، امل کار دگر دارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر آن عالم که در دنیا به این معنی بیندیشد</p></div>
<div class="m2"><p>جهان را پر خطر بیند روان را پر خطر دارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر آنکس کو گرفتارست، اندر منزل دنیا</p></div>
<div class="m2"><p>نه درمان اجل دارد نه سامان حذر دارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کمر گیرد اجل آنرا که در شاهی و جباری</p></div>
<div class="m2"><p>زحل، مهر نگین دارد قمر طرف کمر دارد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اگر طبع تو از فرهنگ دارد فر کیخسرو</p></div>
<div class="m2"><p>وگر شخص تو اندر جنگ زور زال زر دارد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اگر تو فی‌المثل ماهی و از گردون سپر داری</p></div>
<div class="m2"><p>بسر عمر ترا لابد زمانه پی سپر دارد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ایا، سرگشتهٔ دنیا مشو غره به مهر او</p></div>
<div class="m2"><p>که بس سرکش که اندر گور خشتی زیر سر دارد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>طمع در سیم و زر چندین مکن گردین و دل خواهی</p></div>
<div class="m2"><p>که دین و دل تبه کرد آن که دل در سیم و زر دارد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>جهان پر آتش آزست و بیچاره دل آنکس</p></div>
<div class="m2"><p>که او اندر صمیم دل از آن آتش شرر دارد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چه نوشی شربت نوشین و آخر ضربت هجران</p></div>
<div class="m2"><p>همه رنجت هبا گردد همه کارت هدر دارد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>تو اندر وقت بخشیدن جهانی مختصر داری</p></div>
<div class="m2"><p>جهان از روی بخشیدن ترا هم مختصر دارد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>سنایی را مسلم شد که گوید زهد پرمعنی</p></div>
<div class="m2"><p>نداند قیمت نظمش، هر آن کو گوش کر دارد</p></div></div>