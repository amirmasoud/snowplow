---
title: >-
    قصیدهٔ شمارهٔ ۹ - در توحید
---
# قصیدهٔ شمارهٔ ۹ - در توحید

<div class="b" id="bn1"><div class="m1"><p>آراست جهاندار دگرباره جهان را</p></div>
<div class="m2"><p>چو خلد برین کرد، زمین را و زمان را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فرمود که تا چرخ یکی دور دگر کرد</p></div>
<div class="m2"><p>خورشید بپیمود مسیر دوران را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ایدون که بیاراست مر این پیر خرف را</p></div>
<div class="m2"><p>کاید حسد از تازگیش تازه جوان را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر روز جهان خوشتر از آنست چو هر شب</p></div>
<div class="m2"><p>رضوان بگشاید همه درهای جنان را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گویی که هوا غالیه آمیخت بخروار</p></div>
<div class="m2"><p>پر کرد از آن غالیه‌ها غالیه‌دان را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گنجی که به هر کنج نهان بود ز قارون</p></div>
<div class="m2"><p>از خاک برآورد مر آن گنج نهان را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ابری که همی برف ببارید ببرید</p></div>
<div class="m2"><p>شد غرقهٔ بحری که ندید ایچ کران را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آن ابر درر بار ز دریا که برآید</p></div>
<div class="m2"><p>پر کرده ز در و درم و دانه دهان را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از بس که ببارید به آب اندر لولو</p></div>
<div class="m2"><p>چون لولو تر کرد همه آب روان را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>رنجی که همی باد فزاید ز بزیدن</p></div>
<div class="m2"><p>بر ما بوزید از قبل راحت جان را</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کوه آن تل کافور بدل کرد به سیفور</p></div>
<div class="m2"><p>شادی روان داد مر آن شاد روان را</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بر کوه از آن تودهٔ کافور گرانبار</p></div>
<div class="m2"><p>خورشید سبک کرد مر آن بار گران را</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>خاکی که همه ژاله ستد از دهن ابر</p></div>
<div class="m2"><p>تا بر کند آن لالهٔ خوش خفته ستان را</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چندان ز هوا ژاله ببارید بدو ابر</p></div>
<div class="m2"><p>تا لاله ستان کرد همه لاله ستان را</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>از رنگ گل و لاله کنون باز بنفشه</p></div>
<div class="m2"><p>چون نیل شود خیره کند گوهر کان را</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>شبگیر زند نعره کلنگ از دل مشتاق</p></div>
<div class="m2"><p>وز نعره زدن طعنه زند نعره زنان را</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>آن لکلک گوید که: لک الحمد، لک الشکر</p></div>
<div class="m2"><p>تو طعمهٔ من کرده‌ای آن مار دمان را</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>قمری نهد از پشت قبای خز و قاقم</p></div>
<div class="m2"><p>اکنون که بتابید و بپوشید کتان را</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>طاووس کند جلوه چو از دور به بیند</p></div>
<div class="m2"><p>بر فرق سر هدهد، آن تاج کیان را</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>موسیجه همی گوید: یا رازق رزاق</p></div>
<div class="m2"><p>روزی ده جانبخش تویی انسی و جان را</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>زاغ از شغب بیهده بربندد منقار</p></div>
<div class="m2"><p>چون فاخته بگشاده به تسبیح زبان را</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>پیوسته هما گوید: یکیست یگانه</p></div>
<div class="m2"><p>تا در طرب آرد به هوا بر ورشان را</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>گنجشک بهاری صفت باری گوید</p></div>
<div class="m2"><p>کز بوم به انگیزد اشجار نوان را</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>هر گوید هو صد بدمی سرخ کبوتر</p></div>
<div class="m2"><p>در گفتن هو دارد پیوسته لسان را</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چرغان به سر چنگ درآورده تذروان</p></div>
<div class="m2"><p>تسبیح شده از دهن مرغ مر آن را</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>شارک چو موذن به سحر حلق گشاده</p></div>
<div class="m2"><p>آن ژولک و آن صعوه از آن داده اذان را</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>آن شیشککان شاد ازین سنگ به آن سنگ</p></div>
<div class="m2"><p>پاینده و پوینده مر آن پیک دوان را</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>آن کبک مرقع سلب برچده دامن</p></div>
<div class="m2"><p>از غالیه غل ساخته از بهر نشان را</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بنگر به هوا بر به چکاوک که چه گوید</p></div>
<div class="m2"><p>خیر و حسنت بادا خیرات و حسان را</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>نازیدن ناز و نواهای سریچه</p></div>
<div class="m2"><p>ناطق کند آن مردهٔ بی‌نطق و بیان را</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>آن کرکی گوید که: توی قادر قهار</p></div>
<div class="m2"><p>از مرگ همی قهر کنی مر حیوان را</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>پیوسته همی گوید آن سر شب تشنه</p></div>
<div class="m2"><p>بی‌آب ملک صبر دهد مر عطشان را</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>مرغابی سرخاب که در آب نشیند</p></div>
<div class="m2"><p>گوید که خدایی و سزایی تو جهان را</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>در خوید چنین گوید کرک که: خدایا</p></div>
<div class="m2"><p>تو خالق خلقانی صد قرن قران را</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>گویند تذروان که تو آنی که بدانی</p></div>
<div class="m2"><p>راز تن بی‌قوت و بی‌روح و روان را</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>آن باز چنین گوید یارب تو نگهدار</p></div>
<div class="m2"><p>بر امت پیغمبر، ایمان و امان را</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>آن کرکس با قوت گوید که به قدرت</p></div>
<div class="m2"><p>جبار نگهدار، این کون و مکان را</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>بنگر که عقاب از پس تسبیح چه گوید</p></div>
<div class="m2"><p>آراسته دارید مر این سیرت و سان را</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>بلبل چه مذکر شده و قمری قاری</p></div>
<div class="m2"><p>برداشته هر دو شغب و بانگ و فغان را</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>آید به تو هر پاس خروشی ز خروسی</p></div>
<div class="m2"><p>کی غافل، بگذار جهان گذران را</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>آوازه برآورد که: ای قوم تن خویش</p></div>
<div class="m2"><p>دوزخ مبرید از پی بهمان و فلان را</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>دنیا چو یکی بیشه شمارید و ژیان شیر</p></div>
<div class="m2"><p>در بیشه مشورید مر آن شیر ژیان را</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>در جستن نان آب رخ خویش مریزید</p></div>
<div class="m2"><p>در نار مسوزید روان از پی نان را</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>ایزد چو به زنار نبستست میانتان</p></div>
<div class="m2"><p>در پیش چو خود خیره مبندید میان را</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>زان پیش که جانتان بستاند ملک الموت</p></div>
<div class="m2"><p>از قبضهٔ شیطان بستانید عنان را</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>مجدود بدینحال تو نزدیکتری زانک</p></div>
<div class="m2"><p>پیریت به نهمار فرستاده خزان را</p></div></div>