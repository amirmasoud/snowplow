---
title: >-
    قصیدهٔ شمارهٔ ۱۳۰ - در مدح بهرامشاه
---
# قصیدهٔ شمارهٔ ۱۳۰ - در مدح بهرامشاه

<div class="b" id="bn1"><div class="m1"><p>چون من و چون تو شد ای دوست چمن</p></div>
<div class="m2"><p>یک چمانه من و تو بی تو و من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>توی بی‌تو چو بهار اندر بت</p></div>
<div class="m2"><p>من بی من به بهار تو شمن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>توبهٔ سست بروتان شده‌است</p></div>
<div class="m2"><p>شکن زلفک تو توبه شکن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حسن اندر حسن اندر حسنم</p></div>
<div class="m2"><p>تو حسن خلق و حسن بنده حسن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بی سر و پای یکی چنبروار</p></div>
<div class="m2"><p>خر ما جسته و بگسسته رسن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تو چو نرگس کله زر بر سر</p></div>
<div class="m2"><p>من چو گل کرده قبا پیراهن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پشت من پیش تو شاخ سمنی</p></div>
<div class="m2"><p>پیش من روی تو صد دسته سمن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شاخ چون روی تو پر لعل و درر</p></div>
<div class="m2"><p>آب چون زلف تو پر پیچ و شکن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بر گریبان پر از ماه تو شاخ</p></div>
<div class="m2"><p>انجم افشانان دامن دامن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شکفه پر زر و پر سیم گلو</p></div>
<div class="m2"><p>یاسمین پر می و پر شیر دهن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بسته بر ساعد گل عقد گهر</p></div>
<div class="m2"><p>سوده در کام سمن مشک ختن</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>سر به سر شاخ پر از عارض و زلف</p></div>
<div class="m2"><p>لب به لب جوی پر از خط و ذقن</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>زیر سرو چو الف با خوی و می</p></div>
<div class="m2"><p>گشته یک تن الف دار دو تن</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>غنچه همچون دل من با لب تو</p></div>
<div class="m2"><p>لاله همچون رخ تو در دل من</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>عندلیب آمده در مدحت شاه</p></div>
<div class="m2"><p>رایگان همچو سنایی به سخن</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>شاه بهرامشه آن کو بدو زخم</p></div>
<div class="m2"><p>جرم بهرام کند شش چو پرن</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>آن شهی کز صفت گرز و سنانش</p></div>
<div class="m2"><p>که شود آرد فلک پرویزن</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>پوستها بر تنشان گردد نیست</p></div>
<div class="m2"><p>هر که اندر کنفش نیست کفن</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>او چه ماند به فلان و به همان</p></div>
<div class="m2"><p>او و تایید و جهانی دشمن</p></div></div>