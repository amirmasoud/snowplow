---
title: >-
    قصیدهٔ شمارهٔ ۹۸ - در مدح سرهنگ امیر محمد هروی
---
# قصیدهٔ شمارهٔ ۹۸ - در مدح سرهنگ امیر محمد هروی

<div class="b" id="bn1"><div class="m1"><p>ای سنایی نشود کار تو امروز چو چنگ</p></div>
<div class="m2"><p>تا به خدمت نشوی و نکنی قامت چنگ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سر سرهنگان سرهنگ محمد هروی</p></div>
<div class="m2"><p>که سر آهنگان خوانند مر او را سرهنگ</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آنکه روی همه هشیاران آمد به شتاب</p></div>
<div class="m2"><p>آنکه پشت همه بیداران آمد به درنگ</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نزد دیدارش که بوده بهای بهمن</p></div>
<div class="m2"><p>پیش گفتارش جهل آمده هوش هوشنگ</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر بسقلاب برد باد نهیبش نشگفت</p></div>
<div class="m2"><p>که سیه روی شود مردم سقلاب چو زنگ</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>باد لطفش بوزد گر بحد چین نه عجب</p></div>
<div class="m2"><p>که از خاکش پس از آن زنده برآید سترنگ</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر پلنگ ار بنهد دست ز روی شفقت</p></div>
<div class="m2"><p>نجم سیاره نماید نقط از پشت پلنگ</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ای به علم و به سخا مفخر اهل غزنین</p></div>
<div class="m2"><p>غزنی از فخر تو بر چرخ برآرد اورنگ</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بنگ و افیون شود از بوی تو سرمایهٔ عقل</p></div>
<div class="m2"><p>گر در آن کو که توباشی بود افیون یا بنگ</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گر بسنجید به شاهین خرد حلم ترا</p></div>
<div class="m2"><p>دایرهٔ مرکز و دریا بود آن را پا سنگ</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دست جود تو چو جان ساخته با هفت اقلیم</p></div>
<div class="m2"><p>پای قدر تو چو دل تاخته با هفتو رنگ</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>آنچه در وقعهٔ قنوج تو کردی از زور</p></div>
<div class="m2"><p>و آنچه در پیش شهنشاه نمودی از جنگ</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>سود یک لشکر دین بود که آنروز چو شیر</p></div>
<div class="m2"><p>کردی از کین سوی آن گاو زیان کار آهنگ</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>مار مردم‌کش در بحر نکرد آن از کام</p></div>
<div class="m2"><p>شیر مردم‌کش در بیشه نکرد آن از چنگ</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>تاختی راست چو خورشید و بکندیش آن شاخ</p></div>
<div class="m2"><p>که به آسانی سفتی سر او آهن و سنگ</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بودی آن روز به کردار چو خورشید به ثور</p></div>
<div class="m2"><p>هستی امروز به مقدار چو مه در خرچنگ</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>روز مردان بود آنجا که تو باشی بازی</p></div>
<div class="m2"><p>جنگ ترکان بود آنجا که تو باشی نیرنگ</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>آنچه تنها تو به یک تیغ کنی صد یک از آن</p></div>
<div class="m2"><p>نکند لشکری از ترک به صد تیر خدنگ</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چو بنات‌النعش گردند پراکنده چو تو</p></div>
<div class="m2"><p>دشمنان را کنی از نیزه چو پروین آونگ</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>عقل هر ترک در آن روز همی گوید هین</p></div>
<div class="m2"><p>ترکش ای ترک به یکسو فکن و جامهٔ جنگ</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بره بسیار در آویختی از چنگ و کنون</p></div>
<div class="m2"><p>دشمن شاه درآویز چو مسلوخ از چنگ</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چون حمایل به زر اندر کنف افگنی راست</p></div>
<div class="m2"><p>همچو پیلی که کند گردن در کام نهنگ</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>پس خرامی سوی میدان و به جانت که شود</p></div>
<div class="m2"><p>زردی روی عدویت چو حمایل از رنگ</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>تو چو خورشیدی و آن زرد ترا هست سزا</p></div>
<div class="m2"><p>بر کتف پرور کز بچه ندارد کس ننگ</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>گر حسودی سخنی گوید ازین روی فراخ</p></div>
<div class="m2"><p>پشت منمای و زان ژاژ مکن دل را تنگ</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>که ببینی پس از این از قبل خدمت تو</p></div>
<div class="m2"><p>پشت‌اعدای تو چون پشت حمایل شده گنگ</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>آهنین گوهر شد روی من از آتش دل</p></div>
<div class="m2"><p>همچو آبی که برو باد وزد از آژنگ</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>روشنست آینهٔ فضلم چون زنگ ولیک</p></div>
<div class="m2"><p>آینهٔ بختم تاریک همی دارد زنگ</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>قدر چون بینم چون نیستم از گوهر هیز</p></div>
<div class="m2"><p>صدر چون یابم چون نیستم از شوخی شنگ</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>دولت آن راست درین وقت که آبست از که</p></div>
<div class="m2"><p>صلت آن راست درین شهر که نانست از سنگ</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>آب و قدر شعرا نزد تو ز آنست بزرگ</p></div>
<div class="m2"><p>که نخوردستی در خردی نان بشتالنگ</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>مدح بی‌صلت آن راد نمی‌آید چست</p></div>
<div class="m2"><p>شعر بی‌جامهٔ آن مرد نمی‌گیرد هنگ</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>جامه‌ای بخش مرا خاص خود ار سرو قدم</p></div>
<div class="m2"><p>تا ز فر تو شود کار من امسال چو چنگ</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>شوم از شکر ثناهات چو قمری در دم</p></div>
<div class="m2"><p>چو بوم من ز لباس تو چو طوطی بارنگ</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>من از آن رنگ جهان را کنم آگاه ز شکر</p></div>
<div class="m2"><p>همچو اشتر که دهد آگهی از رنگارنگ</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>ای عزیزی اگر این باد که اندر سر هست</p></div>
<div class="m2"><p>راه یابد سوی خانه کندم تنگ ز ننگ</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>چون کبوتر نشوم بهرهٔ کس بهر شکم</p></div>
<div class="m2"><p>گردن افراشته ز آنم همالان چو کلنگ</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>تا سپهرست و فلک پایهٔ ماه و خورشید</p></div>
<div class="m2"><p>تا به هندست و به چین معدن گنگ و ار تنگ</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>باد افراخته رای تو چو خورشید و چو ماه</p></div>
<div class="m2"><p>باد آراسته جان تو چو ارتنگ و چو گنگ</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>روی زردان همه اعدای تو مانند ترنج</p></div>
<div class="m2"><p>روی سرخان همه احباب تو همچون نارنگ</p></div></div>