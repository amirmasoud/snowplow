---
title: >-
    قصیدهٔ شمارهٔ ۶۹ - در وحدانیت ذات باری
---
# قصیدهٔ شمارهٔ ۶۹ - در وحدانیت ذات باری

<div class="b" id="bn1"><div class="m1"><p>ای ذات تو ناشده مصور</p></div>
<div class="m2"><p>اثبات تو کرده عقل باور</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اسم تو ز حد و رسم بیزار</p></div>
<div class="m2"><p>ذات تو ز جنس و نوع برتر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>محمول نه‌ای چنانکه اعراض</p></div>
<div class="m2"><p>موضوع نه‌ای چنانکه جوهر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>فعلت نه به قصد آمر خیر</p></div>
<div class="m2"><p>قولت نه به لفظ ناهی شر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حکم تو به رقص قرص خورشید</p></div>
<div class="m2"><p>انگیخته سایه‌های جانور</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>صنع تو به دور دور گردون</p></div>
<div class="m2"><p>آمیخته رنگهای دلبر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ببریده در آشیان تقدیس</p></div>
<div class="m2"><p>وصف تو ز جبرییل شهپر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بگشاده به شه نمای تنزیه</p></div>
<div class="m2"><p>حسنت ز عروس عرش زیور</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هم بر قدمت حدوث شاهد</p></div>
<div class="m2"><p>هم بر ازلت ابد مجاور</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ای گشته چو آفتاب تابان</p></div>
<div class="m2"><p>در سایهٔ نور خود مستر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>معشوق جهانی و نداری</p></div>
<div class="m2"><p>یک عاشق با ساز و در خور</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بنهفته به حر گنج قارون</p></div>
<div class="m2"><p>یک در تو در دو دانه گوهر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>عالم پس ازین دو گشت پیدا</p></div>
<div class="m2"><p>آدم هم ازین دو برد کیفر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>عالم چو یکی رونده دریا</p></div>
<div class="m2"><p>سیاره سفینه طبع لنگر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>آبش چو نبات و سنگ حیوان</p></div>
<div class="m2"><p>درش چو حقیقت سخن‌ور</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>غواص چه چیز؟ عقل فعال</p></div>
<div class="m2"><p>زینسان که به بحر دین پیمبر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>علت چو سیاست فرودین</p></div>
<div class="m2"><p>از دست چو حرص خصم بی مر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>آخر چه هر آنچه بود اول</p></div>
<div class="m2"><p>مقصود چه آنچه بود بهتر</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بنگر به صواب اگر نه‌ای کور</p></div>
<div class="m2"><p>بنشو به حقیقت ار نه‌ای کر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ای باز هوات در ربوده</p></div>
<div class="m2"><p>از دام زمانه چون کبوتر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ای پنجهٔ حرص در کشیده</p></div>
<div class="m2"><p>ناگه چو رسن سرت به چنبر</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>در قشر بمانده کی توان دید</p></div>
<div class="m2"><p>مقصود خلاصهٔ مقشر</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>از توبه و از گناه آدم</p></div>
<div class="m2"><p>خود هیچ ندانی ای برادر</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>سربسته بگویم ار توانی</p></div>
<div class="m2"><p>بردار به تیغ فکرتش سر</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>درویش کند ز راه ترتیب</p></div>
<div class="m2"><p>نزدیکی تو به سوی داور</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>در خلد چگونه خورد گندم</p></div>
<div class="m2"><p>آنجا که نبود شخص نان خور</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بل گندمش آن گهٔ ببایست</p></div>
<div class="m2"><p>کز خلد نهاد پای بر در</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>این جمله همه بدیده آدم</p></div>
<div class="m2"><p>ابلیس نیامده ز مادر</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>در سجده نکردنش چه گویی</p></div>
<div class="m2"><p>مجبور بدست یا مخیر</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>گر قادر بد خدای عاجز</p></div>
<div class="m2"><p>ور عاجز بد خدا ستمگر</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>کاری که نه کار تست مسگال</p></div>
<div class="m2"><p>راهی که نه راه تست مسپر</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>بیهوده مجوی آب حیوان</p></div>
<div class="m2"><p>در ظلمت خویش چون سکندر</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>کن چشمه که خضر یافت آنجا</p></div>
<div class="m2"><p>با دیو فرشته نیست همبر</p></div></div>