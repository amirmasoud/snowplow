---
title: >-
    قصیدهٔ شمارهٔ ۱۸۷
---
# قصیدهٔ شمارهٔ ۱۸۷

<div class="b" id="bn1"><div class="m1"><p>ای سنایی چند لاف از خواجه و مهتر زنی</p></div>
<div class="m2"><p>دار قلابان نهی بی مهر سلطان زر زنی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رایتت بر چرخ سر دارد همی چون آفتاب</p></div>
<div class="m2"><p>خیمه‌ات از چرخ چو می بگذرد بر تر زنی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با یجوز و لایجوز اندر مشو در کوی عشق</p></div>
<div class="m2"><p>رخت دل در خانه نه تا کی چو دربان در زنی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مصر اگر اقطاع داری دست از کنعان بدار</p></div>
<div class="m2"><p>از علی بیزار گردی دست در قنبر زنی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>معرفت خواهی و در معروف کرخی ننگری</p></div>
<div class="m2"><p>ای جنب شرمی نداری با جنیدی در زنی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بار سازی بر خرت آلت نمی‌بینی همی</p></div>
<div class="m2"><p>از چه معنا بگذری تو آتش اندر خر زنی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آتش اندر کشور اندازی و می سوزی همی</p></div>
<div class="m2"><p>باز لاف از آبروی صاحب کشور زنی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از هوای آدمیت سینه را معزول کن</p></div>
<div class="m2"><p>گرد همت گرد تا بر اوج گردون پر زنی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مطربی جلدی بدان هر ساعتی بی زیر و بم</p></div>
<div class="m2"><p>پردهٔ دیگر نوازی زخمهٔ دیگر زنی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گر یکی دم بر تو افتد باز پرس از باد فقه</p></div>
<div class="m2"><p>قال قالی پیش گیری چنگ در دفتر زنی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>باز اگر در صدر فقهت مفتیی لازم کند</p></div>
<div class="m2"><p>فقه را منکر شوی با شیخ شبلی بر زنی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>امر اذقال الله اردانی صلیب از کف بنه</p></div>
<div class="m2"><p>تا کی از عیساکران جویی و لاف از خر زنی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>تا برین خاکی کزو با دست کار جاه و مال</p></div>
<div class="m2"><p>شاید ار آتش به آب و جاه و مال اندر زنی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>پای پیری گیر اگر خواهی که پروازی کنی</p></div>
<div class="m2"><p>چون شکستی بت روا باشد که بر بتگر زنی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>جامه مومن سینه کافر رستم ترسایان بود</p></div>
<div class="m2"><p>روی چون بوذر نمایی راه چون آزر زنی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>سنگ با معنی به از یاقوت با دعوی چرا</p></div>
<div class="m2"><p>از گریبان پاره برداری به دامن بر زنی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>اینهمه رنگست و نیرنگست زینجا سر بتاب</p></div>
<div class="m2"><p>عاشقی شو تا مفاجا چنگ در دلبر زنی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گر ازین دعوی بی‌معنی قدم یکسو نهی</p></div>
<div class="m2"><p>پای بر کیوان نهی و خیمه بر اختر زنی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>نکته‌های خوب من چون شکر آید مر ترا</p></div>
<div class="m2"><p>پس چنان باید که نار از رشگ بر عسکر زنی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>عاشقان این زمانه از زه خود عاجزند</p></div>
<div class="m2"><p>منکرند این قوم شاید گر دمی منکر زنی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ای سنایی راست می‌گویی ز کج گویان مترس</p></div>
<div class="m2"><p>تا قدم چون دم به راه دین پیغمبر زنی</p></div></div>