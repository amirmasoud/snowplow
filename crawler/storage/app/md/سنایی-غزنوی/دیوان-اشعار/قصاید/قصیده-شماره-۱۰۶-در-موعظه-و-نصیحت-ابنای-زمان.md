---
title: >-
    قصیدهٔ شمارهٔ ۱۰۶ - در موعظه و نصیحت ابنای زمان
---
# قصیدهٔ شمارهٔ ۱۰۶ - در موعظه و نصیحت ابنای زمان

<div class="b" id="bn1"><div class="m1"><p>کجایی ای همه هوشت به سوی طبل و علم</p></div>
<div class="m2"><p>چرا نباری بر رخ ز دیده آب ندم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چرا غرور دهی تنت را به مال و به ملک</p></div>
<div class="m2"><p>چرا فروشی دین را به ساز و اسب و درم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تمام شد که ترا خواجگی لقب دادند</p></div>
<div class="m2"><p>کمال یافت همه کار تو به باد و بدم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به ذات ایزد اگر دست گیردت فردا</p></div>
<div class="m2"><p>غلام و اسب و سلاح و سوار و خیل و حشم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو بر زنند بر آن طبل عزل خواجه دوال</p></div>
<div class="m2"><p>تو خواه میر عرب باش و خواه شام عجم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به گوش خواجه فرو گوید زان زمان معنی</p></div>
<div class="m2"><p>کجا شد آنهمه دعوی و لاف تو هر دم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ازین غرور تو تا کی ایا زبون قضا</p></div>
<div class="m2"><p>وزین نشاط تو تا کی ایا سرشته به غم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کمر به دست تو آید همی سلیمان‌وار</p></div>
<div class="m2"><p>ترا طمع که در انگشت تو کند خاتم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز کردگار نترسی و پس خراب کنی</p></div>
<div class="m2"><p>هزار خانهٔ درویش را به نوک قلم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>امین دینت لقب گشت پس چرا دزدی</p></div>
<div class="m2"><p>گلیم موسی عمران و چادر مریم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز بهر ده درم قلب را نداری باک</p></div>
<div class="m2"><p>که بر کنی و بسوزی هزار بیت حرم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>شراب جنت و حور و قصور می طلبی</p></div>
<div class="m2"><p>بدین مروت و حلم و بدین سخا و کرم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بدین عمل که تو داری مگر ترا ندهند</p></div>
<div class="m2"><p>به حشر هیچی و ز هیچ نیز چیزی کم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بدین قصیده ز من خواجگان بپرهیزند</p></div>
<div class="m2"><p>چنانکه اهل شیاطین ز توبهٔ آدم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>سنایی ار تو خدا ترسی و خدای شناس</p></div>
<div class="m2"><p>ترا ز میر چه باک و ترا ز شاه چه غم</p></div></div>