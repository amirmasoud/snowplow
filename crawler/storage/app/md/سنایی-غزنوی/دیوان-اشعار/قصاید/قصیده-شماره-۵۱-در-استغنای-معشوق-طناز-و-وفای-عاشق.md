---
title: >-
    قصیدهٔ شمارهٔ ۵۱ - در استغنای معشوق طناز و وفای عاشق
---
# قصیدهٔ شمارهٔ ۵۱ - در استغنای معشوق طناز و وفای عاشق

<div class="b" id="bn1"><div class="m1"><p>عاشقانت سوی تو تحفه اگر جان آرند</p></div>
<div class="m2"><p>به سر تو که همی زیره به کرمان آرند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ور خرد بر تو فشانند همی دان که همی</p></div>
<div class="m2"><p>عرق سنگ سوی چشمهٔ حیوان آرند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ور دل و دین به تو آرند عجب نبود از آنک</p></div>
<div class="m2"><p>رخت خر بنده به بنگاه شتربان آرند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر چه هستیست همه ملک لب و خال تواند</p></div>
<div class="m2"><p>چیست کن نیست ترا تا سوی تو آن آرند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نوک مژگانت بهر لحظه همی در ره عشق</p></div>
<div class="m2"><p>آدم کافر و ابلیس مسلمان آرند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چینهٔ دام لبان تو زمان تا به زمان</p></div>
<div class="m2"><p>روح را از قفس سدره به مهمان آرند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زلف و خالت ز پی تربیت فتنهٔ ما</p></div>
<div class="m2"><p>عقل را کاج زنان بر در زندان آرند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چشمهامان ز پی تقویت حسن تو باز</p></div>
<div class="m2"><p>فتنه را رقص‌کنان در قفس جان آرند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>طوبی و سدره به باغ تو و پس مشتی خس</p></div>
<div class="m2"><p>دستهٔ مجلس تو خار مغیلان آرند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هدیه‌شان رد مکن انگار که پای ملخی</p></div>
<div class="m2"><p>گلهٔ مور همی پیش سلیمان آرند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خاکپای تو اگر دیده سوی روح برد</p></div>
<div class="m2"><p>روح پندارد کز خلد همی خوان آرند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>از پی چشم بدو چشم نکوی تو همی</p></div>
<div class="m2"><p>مردمان مردمک دیده به قربان آرند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بوستان از خجلی پوست بیندازد از آنک</p></div>
<div class="m2"><p>صورت روی تو در دیدهٔ بستان آرند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>عاشقان از خم زلف تو چه دیدند هنوز</p></div>
<div class="m2"><p>باش تا تاب در آن زلف پریشان آرند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>باش تا سلطنت و کبر تو مشتی دون را</p></div>
<div class="m2"><p>از در دین به هوس خانهٔ شیطان آرند</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>باش تا خار سر کوی ترا نرگس وار</p></div>
<div class="m2"><p>دسته بندند و سوی مجلس سلطان آرند</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ای بسا بیخ که در چین و ختن کنده شود</p></div>
<div class="m2"><p>تا چو تو مهر گیاهی به خراسان آرند</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>باش تا خط بناگوش و خم زلف تو باز</p></div>
<div class="m2"><p>عقل را گوش گرفته به دبستان آرند</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>کی به آسانی عشاق ز دستت بدهند</p></div>
<div class="m2"><p>که نه در دست همی چون تویی آسان آرند</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>عقد پروین بخمد چون دم عقرب در حال</p></div>
<div class="m2"><p>چون سخن زان دو رده لولو مرجان آرند</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>کافران گمره از آنند که در زلف تواند</p></div>
<div class="m2"><p>یک ره آن زلف ببر تا همه ایمان آرند</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>یک ره آن پرده برانداز که تا مشتی طفل</p></div>
<div class="m2"><p>رخت جان سوی سراپردهٔ قرآن آرند</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>هردم از غیرت یاری تو اجرام سپهر</p></div>
<div class="m2"><p>بر سنایی غم و اندوه فراوان آرند</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>هر زمان لعل و در و سرو و بنفشهٔ تو همی</p></div>
<div class="m2"><p>دل و دین و خرد و صبر دگر سان آرند</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>خود چو پروین که مه و مهر همی سجدهٔ عشق</p></div>
<div class="m2"><p>سر دندان ترا از بن دندان آرند</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>قدر چوگانت ندانند از آن خامی چند</p></div>
<div class="m2"><p>باش تا سوختگان گوی به میدان آرند</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>شکل دندان و سر زلف تو زودا که برو</p></div>
<div class="m2"><p>سین و نون و الف و یا همه تاوان آرند</p></div></div>