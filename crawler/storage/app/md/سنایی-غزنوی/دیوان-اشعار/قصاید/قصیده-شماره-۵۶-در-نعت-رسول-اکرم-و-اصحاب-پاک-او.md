---
title: >-
    قصیدهٔ شمارهٔ ۵۶ - در نعت رسول اکرم و اصحاب پاک او
---
# قصیدهٔ شمارهٔ ۵۶ - در نعت رسول اکرم و اصحاب پاک او

<div class="b" id="bn1"><div class="m1"><p>روشن آن بدری که کمتر منزلش عالم بود</p></div>
<div class="m2"><p>خرم آن صدری که قبله‌ش حضرت اعظم بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این جهان رخسار او دارد از آن دلبر شدست</p></div>
<div class="m2"><p>و آن جهان انوار او دارد از آن خرم بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حاکمی کاندر مقام راستی هر دم که زد</p></div>
<div class="m2"><p>بر خلاف آندم اگر یک دم زنی آندم بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>راه عقل عاقلان را مهر او مرشد شدست</p></div>
<div class="m2"><p>درد جان عاشقان را نطق او مرهم بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>صدهزاران جان فدای آنسواری کز جلال</p></div>
<div class="m2"><p>غاشیه‌ش بر دوش پاک عیسی مریم بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از رخش گردد منور گر همه جنت بود</p></div>
<div class="m2"><p>وز لبش یابد طهارت گر همه زمزم بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فرش اگر سر برکشد تا عرش را زیر آورد</p></div>
<div class="m2"><p>دست آن دارد که از زلفش بر اوریشم بود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>طلعت جنت ز شوق حضرتش پر خوی شدست</p></div>
<div class="m2"><p>دیدهٔ دوزخ ز رشک غیبتش پر نم بود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از گریبان زمین گر صبح او سر بر زند</p></div>
<div class="m2"><p>تا شب حشر از جمالش صد سپیده دم بود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>با «لعمرک» انیبا را فکرت رجحان کیست</p></div>
<div class="m2"><p>با «عفاالله» اولیا را زهرهٔ یک دم بود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>با «الم نشرح» چگویی مشکلی ماند ببند</p></div>
<div class="m2"><p>با «فترضی» هیچ عاصی در مقام غم بود</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>خوش سخن شاهی کز اقبال کفش در پیش او</p></div>
<div class="m2"><p>کشتهٔ بریان زبان یابد که در وی سم بود</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>خاک را در صدر جنت آبرویش جاه داد</p></div>
<div class="m2"><p>آتش ابلیس را از خاک او ماتم بود</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چرخ را از کاف «لولاک»ش کمر زرین بود</p></div>
<div class="m2"><p>خاکرا با حاء احمامش قبا معلم بود</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>خاک زاید گوهری کز گوهران برتر شود</p></div>
<div class="m2"><p>بچه زاید آدمی کو خواجهٔ عالم بود</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>هر که در میدان مردی پیش او یکدم زند</p></div>
<div class="m2"><p>رخش او گوساله گردد گر همه رستم بود</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>در شبی کو عذر «اخطانا» همی خواهد ز حق</p></div>
<div class="m2"><p>جبرئیل آنجا چو طفل ابکم و الکن بود</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>حکم الالله بر فرق رسول الله بین</p></div>
<div class="m2"><p>راستی زین تکیه‌گاهی آدمی را کم بود</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ماه بر چرخ فلک چون حلقهٔ زلف و رخش</p></div>
<div class="m2"><p>گاه چون سیمین سپر گه پارهٔ معصم بود</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>شاه انجم موذن وی گشته اندر شرق ملک</p></div>
<div class="m2"><p>زان جمال وی شعار شرع را معلم بود</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بادوشان فلک را دور او همره شدست</p></div>
<div class="m2"><p>خاکپاشان زمین را نعل او ملحم بود</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>سدرهٔ طاووس یک پر کز همای دولتش</p></div>
<div class="m2"><p>بر پر خود بست از آن مر وحی را محرم بود</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>خضر گرد چشمهٔ حیوان از آن می‌گشت دیر</p></div>
<div class="m2"><p>تا مگر اندر زمین با وی دمی همدم بود</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>تا نهنگش در عجم گرد زمین چون عمرست</p></div>
<div class="m2"><p>تا هزبرش در عرب غرنده ابن عم بود</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>نی در آن آثار گرز و ناچخ عنتر بود</p></div>
<div class="m2"><p>نه در آن اسباب ملک کیقباد و جم بود</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>با خرد گفتم که فرعی برتر از اصلی شود</p></div>
<div class="m2"><p>گفت: آری چون بر آن فرع اتفاقی ضم بود</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>گفت: ای بوبکر با احمد چرا یکتا شدی</p></div>
<div class="m2"><p>گفت:هر حرفی که ضعفی یافت آن مدغم بود</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>گفتم: ای عمر تو دیدی بوالحکم بس چون برید</p></div>
<div class="m2"><p>گفت: زمرد کی سزای دیدهٔ ارقم بود</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>گفتم: ای عثمان بنا گه کشتهٔ غوغا شدی</p></div>
<div class="m2"><p>گفت: خلخال عروس عاشقان ز آندم بود</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>گفتم: ای حیدر میی از ساغر شیران بخور</p></div>
<div class="m2"><p>گفت: فتح ما ز فتح زادهٔ ملجم بود</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>باد را گفتم: سلیمان را چرا خدمت کنی</p></div>
<div class="m2"><p>گفت: از آن کش نام احمد نقش بر خاتم بود</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>ای سنایی از ره جان گوی مدح مصطفا</p></div>
<div class="m2"><p>تا ترا سوی سپهر برترین سلم بود</p></div></div>