---
title: >-
    قصیدهٔ شمارهٔ ۱۹۵
---
# قصیدهٔ شمارهٔ ۱۹۵

<div class="b" id="bn1"><div class="m1"><p>از خانه برون رفتم من دوش به نادانی</p></div>
<div class="m2"><p>تو قصهٔ من بشنو تا چون به عجب مانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از کوه فرود آمد زین پیری نورانی</p></div>
<div class="m2"><p>پیداش مسلمانی در عرصهٔ بلسانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون دید مرا گفت او داری سر مهمانی</p></div>
<div class="m2"><p>گفتم که بلی دارم بی سستی و کسلانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفتا که هلاهین رو گر بر سر پیمانی</p></div>
<div class="m2"><p>دانم که مرا زین پس نومید نگردانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رفتم به سرایی خوش پاکیزه و سلطانی</p></div>
<div class="m2"><p>نه عیب ز همسایه نه بیم ز ویرانی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در وی نفری دیدم پیران خراباتی</p></div>
<div class="m2"><p>قومی همه قلاشان چون دیو بیابانی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>معروف به بی سیمی مشهور به بی نانی</p></div>
<div class="m2"><p>همچون الف کوفی از عوری و عریانی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>این باخته دراعه و آن باخته بارانی</p></div>
<div class="m2"><p>این گفته که بستانی وان گفته که نستانی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>می گفت یکی رستم زان ظلمت نفسانی</p></div>
<div class="m2"><p>می گفت یکی دیگر ما «اعظم برهانی»</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>این گفت «انا الاول» کس نیست مرا ثانی</p></div>
<div class="m2"><p>و آن گفت «انا آلاخر» تا خلق شود فانی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ماندم متحیر من زان حال ز حیرانی</p></div>
<div class="m2"><p>گفتم که چو قومند این ای خواجهٔ روحانی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گفت: اهل خراباتند این قوم نمی‌دانی</p></div>
<div class="m2"><p>آنها که تو ایشان را قلاش همی دانی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>هان تا نکنی انکار گر بر سر پیمانی</p></div>
<div class="m2"><p>کایشان هذیان گویند از مستی و نادانی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ار این گنهی منکر در مذهب ایشانی</p></div>
<div class="m2"><p>باید که تو این اسار از خلق بپوشانی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>زنهار از این معنی بر خلق سخنرانی</p></div>
<div class="m2"><p>پندار که نشنیدی اندر حد نسیانی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ای آنکه ز قلاشی بر خلق تو ترسانی</p></div>
<div class="m2"><p>در زهد عبادت آر چون بوذر و سلمانی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>در خدمت این مردم تا تن به نرنجانی</p></div>
<div class="m2"><p>حقا که تو بر هیچی چون زاهد او ثانی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چون شاد نباشم من از رحمت یزدانی</p></div>
<div class="m2"><p>دیدار چنین قومی دارد به من ارزانی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>تا دید سنایی را در مجلس روحانی</p></div>
<div class="m2"><p>با دست به دست او زین زهد به سامانی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>امروز بدانست او کان صدر مسلمانی</p></div>
<div class="m2"><p>چون گفت ز بی خویشی سبحانی و سبحانی</p></div></div>