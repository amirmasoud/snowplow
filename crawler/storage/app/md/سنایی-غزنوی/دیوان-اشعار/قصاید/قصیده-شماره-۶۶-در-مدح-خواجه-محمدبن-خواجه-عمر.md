---
title: >-
    قصیدهٔ شمارهٔ ۶۶ - در مدح خواجه محمدبن خواجه عمر
---
# قصیدهٔ شمارهٔ ۶۶ - در مدح خواجه محمدبن خواجه عمر

<div class="b" id="bn1"><div class="m1"><p>دوش سرمست نگارین من آن طرفه پسر</p></div>
<div class="m2"><p>با یکی پیرهن زورقئی طرفه به سر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از سر کوی فرود آمد متواری وار</p></div>
<div class="m2"><p>کرده از غایت دلتنگی ازین گونه خطر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ماه غماز شده از دو لبش بوسه ربای</p></div>
<div class="m2"><p>باد عطار شده بر دو رخش حلقه شمر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کوه از آن کله بگشاده و از غایت لطف</p></div>
<div class="m2"><p>ماه بر چرخ شده بستهٔ آن سینه و بر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چست بنشسته بر اندام لطیف چو خورش</p></div>
<div class="m2"><p>از لطیفی و تری پیرهن توزی تر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خط مشکین بر آن عارض کافور نهاد</p></div>
<div class="m2"><p>چون بدیدم جگرم خون شد و خونم چو جگر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر چه بس نادره کاریستکه خون گردد مشک</p></div>
<div class="m2"><p>لیک مشکی که جگر خون کند این نادره‌تر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سرگران از می و چون باد همی رفت و جز او</p></div>
<div class="m2"><p>من سبک پای ندیدم که گران دارد سر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جعد ژولیده و پرورده ز سیکی لاله</p></div>
<div class="m2"><p>زلف شوریده و پژمرده ز مستی عبهر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>می نمود از سر مستی و طرب هر ساعت</p></div>
<div class="m2"><p>سی و دو تابش پروین ز سهیل و ز قمر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خواست کز پیش درم بگذرد از بی خبری</p></div>
<div class="m2"><p>چون چنان دید ز غم شد دل من زیر و زبر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بانگ برداشتم از غایت نومیدی و عشق</p></div>
<div class="m2"><p>گفتم: ای عشوه فروشندهٔ انگارده خر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>از خداوند نترسی که بدین حال مرا</p></div>
<div class="m2"><p>بگذاری و کنی از در من بنده گذر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چون شنید این ز نکو عهدی و از گوهر پاک</p></div>
<div class="m2"><p>آمد و کرد درین چهرهٔ من نیک نظر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>پشت خم داد و نهاد از قبل خدمت و عذر</p></div>
<div class="m2"><p>روی افروخته از شرم بر آستانهٔ در</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گفت: معذور همی دار که گر نیستی</p></div>
<div class="m2"><p>از پی بیم ولی نعمت و تهدید پدر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>همچنان چون پدر از زر کمری بست مرا</p></div>
<div class="m2"><p>کردمی گرد تو از دست خود از سیم کمر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>شادمان گشتم از آن عذر و گرفتمش کنار</p></div>
<div class="m2"><p>همچو تنگ شکر و خرمن گل تنگ به بر</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>جان و دل زیر قدمهاش نشاندم زین شکر</p></div>
<div class="m2"><p>خود بر آن چهره هزاران دل و جان را چه خطر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>اندرین بود که از نازکی و مستی و شرم</p></div>
<div class="m2"><p>خواب مستانه در آن لحظه در آورد حشر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>سر بر آنجای نهاد آن سمن تازه که بود</p></div>
<div class="m2"><p>صد شب اندر غمش از اشک دو چشمم چو شمر</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>او چو تنگ شکر و گشته سراسیمه ز خواب</p></div>
<div class="m2"><p>من چون طوطی شده بی خواب در اندیشهٔ خور</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>او شده طاق به آرام و من از بوسه زدن</p></div>
<div class="m2"><p>بر دو چشم و دو لبش تا به سحر جفت سهر</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>خواب زاید اگر از شکر و بادام چرا</p></div>
<div class="m2"><p>خوابم از دیده ببرد از در بادام و شکر</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>خود که داند که در آن نیم‌شب از مستی او</p></div>
<div class="m2"><p>تا چه برداشتم از بوسه و هر چیزی بر</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>نرم نرم از سمن آن نرگس پر خواب گشاد</p></div>
<div class="m2"><p>ژاله ژاله عرق از لالهٔ او کرد اثر</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>رویش از خاک چو برداشتم از خوی شده بود</p></div>
<div class="m2"><p>لاله برگش چو گل نم زده در وقت سحر</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بوسه بر دو لب من داد همی از پی عذر</p></div>
<div class="m2"><p>آنت شرمنده نگار آنت شکر بوسه پسر</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>آنت خوش خرمی و عیش که من دیدم دوش</p></div>
<div class="m2"><p>چه حدیثی‌ست که امروزم از آن خرم‌تر</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>دوش از یار بدم خرم و امروز شدم</p></div>
<div class="m2"><p>از رخ خواجه محمد پسر خواجه عمر</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>آنکه تا دست سخا بر همه عالم بگشاد</p></div>
<div class="m2"><p>به بدی بسته شدست ساحت ما پای قدر</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>آن سخن سنج شهی کو چو دو بسد بگشاد</p></div>
<div class="m2"><p>خانهٔ عقل دو صد کله ببندد ز درر</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>مایه‌ور گشته ز اسباب دلش خرد و بزرگ</p></div>
<div class="m2"><p>سودها کرده ز تاثیر کفش ماده و نر</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>پایهٔ مرتبتش را چو ملک نیست قیاس</p></div>
<div class="m2"><p>عرصهٔ مکرمتش را چو فلک نیست عبر</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>خاطرش سر ملک در فلک آینه‌گون</p></div>
<div class="m2"><p>همچنان بیند چون دیده در آیینه صور</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>جنیان زان همه از شرم نهانند که هیچ</p></div>
<div class="m2"><p>به ز خود روی ندیدند چنو ز اهل بشر</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>جزوی از خشم وی ار بر فلک افتد به خطا</p></div>
<div class="m2"><p>نار کلی شود از هیبت او خاکستر</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>آتش عزمش اگر قصد کند سوی هوا</p></div>
<div class="m2"><p>چنبر چرخ بسوزد به یک آسیب شرر</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>شمت حزمش اگر باد برد تحفه به ابر</p></div>
<div class="m2"><p>در شود در شکم ابر هوا قطره مطر</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>ای بهی روی ز سعی تو گه بزم سخا</p></div>
<div class="m2"><p>وی قوی پشت ز عون تو گه رزم ظفر</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>پسری چون تو نزادند درین شش روزن</p></div>
<div class="m2"><p>هفت سیاره و نه دایره و چار گهر</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>هرگز از جود تو نگرفت کس اندازهٔ آز</p></div>
<div class="m2"><p>هرگز از خیر تو نشنید کس آوازهٔ شر</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>کلک و گفتار تو پیرایهٔ فضلست و محل</p></div>
<div class="m2"><p>لفظ و دیدار تو سرمایهٔ سمعست و بصر</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>شبهی دارد کلک تو به شحنهٔ تقدیر</p></div>
<div class="m2"><p>که چنو عنصر نفع آمد و ارکان ضرر</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>عرض او چون عرض جوهر صفرا گه رنگ</p></div>
<div class="m2"><p>فرق او چون عرض جوهر سودا به فکر</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>گر نه سالار هنرمندی بودی هرگز</p></div>
<div class="m2"><p>نزد سالار شهنشاه نبودیش خطر</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>خاطری داری و فهمی که به یک لحظه کنند</p></div>
<div class="m2"><p>تختهٔ قسمت تقدیر خداوند از بر</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>ای جوان بخت نبینی که برین فضل مرا</p></div>
<div class="m2"><p>به چسان این فلک پیر گرفته‌ست به حر</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>مدح گوییم که در تربیت خاطر و طبع</p></div>
<div class="m2"><p>در همه عالم امروز چو من نیست دگر</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>طوق دارند عدو پیش درم فاخته‌وار</p></div>
<div class="m2"><p>تام دیدند ز خاطر شجر پر ز ثمر</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>غوک را جامه بهری جوی و من از شرم عدو</p></div>
<div class="m2"><p>روزها گشته چو خفاش مرا خانه ستر</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>لیک بی‌برگ و نوا مانده‌ام از گردش چرخ</p></div>
<div class="m2"><p>همچو طوق گلوی فاخته و شاخ شجر</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>روی من شد چو زر و دیده چو سیم از پی اشک</p></div>
<div class="m2"><p>گر بخواهی شود از سیم توام کار چو زر</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>پیش خورشید سخای تو به تعجیل کرم</p></div>
<div class="m2"><p>کوه کوه انده من بنده هبا باد و هدر</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>بادی از بخت تو تا از اثر جوهر طبع</p></div>
<div class="m2"><p>در جهان آدمی از پای رود مرغ به پر</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>مرغ بر شاخ تو از مدح تو بگشاد گلو</p></div>
<div class="m2"><p>آدمی پیش تو از مهر تو بربسته کمر</p></div></div>