---
title: >-
    قصیدهٔ شمارهٔ ۵۵ - در مدح بهرامشاه
---
# قصیدهٔ شمارهٔ ۵۵ - در مدح بهرامشاه

<div class="b" id="bn1"><div class="m1"><p>روز بر عاشقان سیاه کند</p></div>
<div class="m2"><p>مست چون قصد خوابگاه کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>راه بر عقل و عافیت بزند</p></div>
<div class="m2"><p>ز آنچه او در میان راه کند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گاه چون نعل اندر آذر بست</p></div>
<div class="m2"><p>یوسفان را اسیر چاه کند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گاه چون زلف را ز هم بگشاد</p></div>
<div class="m2"><p>تنگ بر آفتاب و ماه کند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گاه بیجاده را بطوع و بطبع</p></div>
<div class="m2"><p>در سر رنگ برگ کاه کند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گه چو دندان سپید کرد بطمع</p></div>
<div class="m2"><p>ملک الموت را سیاه کند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گه بیندازد از سمن بستر</p></div>
<div class="m2"><p>گاه بالین گل گیاه کند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گاه زلف شکسته را بر دل</p></div>
<div class="m2"><p>حلقهٔ حضرت الاه کند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گاه خط دمیده را بر جان</p></div>
<div class="m2"><p>نسخهٔ توبهٔ گناه کند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گاه بر جبرئیل صومعه را</p></div>
<div class="m2"><p>چار دیوار خانقاه کند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گاه بر دیو هم ز سایهٔ خویش</p></div>
<div class="m2"><p>شش سوی صحن خوابگاه کند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بوی او کش عدم نبوییدی</p></div>
<div class="m2"><p>گاهش از قهر در پناه کند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>لب او را که بوسه گه بودی</p></div>
<div class="m2"><p>گاهش از لطف بوسه خواه کند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>عشق را گه دلی نهد در بر</p></div>
<div class="m2"><p>تا دل اندر برش سیاه کند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>عقل را گه کله نهد بر سر</p></div>
<div class="m2"><p>تا سر اندر سر کلاه کند</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>پیشهٔ آفتاب خود اینست</p></div>
<div class="m2"><p>چون کسی نیک تر نگاه کند</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>جامهٔ گازر ار سپید کند</p></div>
<div class="m2"><p>روز گازر همو سیاه کند</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>اینهمه می‌کند ولیک از بیم</p></div>
<div class="m2"><p>آه را زهره نی که آه کند</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>از پی آنکه رویش آینه است</p></div>
<div class="m2"><p>آه آیینه را تباه کند</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>من غلام کسی که هر چه کند</p></div>
<div class="m2"><p>چون سنایی به جایگاه کند</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>همه کردار او به جایگه است</p></div>
<div class="m2"><p>خاصه وقتی که مدح شاه کند</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>شاه بهرامشاه آنکه همی</p></div>
<div class="m2"><p>دین و دولت بدو پناه کند</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>گور با شرزه شیر از عدلش</p></div>
<div class="m2"><p>در میان شعر شناه کند</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>صعوه در چشم باز از امنش</p></div>
<div class="m2"><p>از پی بیضه جایگاه کند</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>تارح و زلف دلبران وصاف</p></div>
<div class="m2"><p>به گل و مشک اشتباه کند</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>چاه صد باز را اگر خواهد</p></div>
<div class="m2"><p>تاج سیصد هزار جاه کند</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>محترز باد ظلم از در او</p></div>
<div class="m2"><p>تا چو نحل آرزوی شاه کند</p></div></div>