---
title: >-
    قصیدهٔ شمارهٔ ۱۲ - در مدح بهرامشاه
---
# قصیدهٔ شمارهٔ ۱۲ - در مدح بهرامشاه

<div class="b" id="bn1"><div class="m1"><p>دیده نبیند همی، نقش نهان ترا</p></div>
<div class="m2"><p>بوسه نیابد همی، شکل دهان ترا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حسن بدان تا کند جلوه گهت بر همه</p></div>
<div class="m2"><p>پیرهن هست و نیست، ساخت نهان ترا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در همهٔ هست و نیست، از تری و تازگی</p></div>
<div class="m2"><p>نیست نهانخانه‌ای ثروت جان ترا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زان لب تو هر دمی گردد باریک‌تر</p></div>
<div class="m2"><p>کز شکر و آب کرد روح لبان ترا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هیچ اگر بینمی شکل میانت به چشم</p></div>
<div class="m2"><p>جان نهمی بر میان شکل میان ترا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بوسه دهد خلد و حور، پای و رکیب ترا</p></div>
<div class="m2"><p>سجده کند عقل و روح دست و عنان ترا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون تو به آماج‌گاه تیر نهی بر کمان</p></div>
<div class="m2"><p>تیر فلک زه کند تیر و کمان ترا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پرده‌زنان روز و شب حلقهٔ زلف ترا</p></div>
<div class="m2"><p>غاشیه کش چرخ پیر بخت جوان ترا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>برد دل و گوش و هوش بهر جواز لبت</p></div>
<div class="m2"><p>نام شکر گر شدست کام و زبان ترا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>قبلهٔ خود ساخت عشق از پی ایمان و کفر</p></div>
<div class="m2"><p>زلف نگون ترا روی ستان ترا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>فتنه جان کرد صنع نرگس شوخ ترا</p></div>
<div class="m2"><p>انس روان ساخت طبع سرو روان ترا</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>پیشروان بهشت بر پر و بال خرد</p></div>
<div class="m2"><p>نسخهٔ دین خوانده‌اند سیرت و سان ترا</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>دیدهٔ جانها بخورد نوک سنانت ولیک</p></div>
<div class="m2"><p>جان سنایی کند شکر سنان ترا</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>از پی ضعف میان حرز چه جویی ز من</p></div>
<div class="m2"><p>خدمت خسرو نه بس حرز میان ترا</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>سلطان بهرامشاه آنکه به تایید حق</p></div>
<div class="m2"><p>هست بحق پاسبان خانه و جان ترا</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>هیبتش ار نیستی شحنه وجود ترا</p></div>
<div class="m2"><p>جان ز عدم جویدی نام و نشان ترا</p></div></div>