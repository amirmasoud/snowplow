---
title: >-
    قصیدهٔ شمارهٔ ۱۳۴
---
# قصیدهٔ شمارهٔ ۱۳۴

<div class="b" id="bn1"><div class="m1"><p>برگ بی‌برگی نداری لاف درویشی مزن</p></div>
<div class="m2"><p>رخ چو عیاران نداری جان چو نامردان مکن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یا برو همچون زنان رنگی و بویی پیش گیر</p></div>
<div class="m2"><p>یا چو مردان اندر آی و گوی در میدان فگن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر چه بینی جز هوا آن دین بود بر جان نشان</p></div>
<div class="m2"><p>هر چه یابی جز خدا آن بت بود در هم شکن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون دل و جان زیر پایت نطع شد پایی بکوب</p></div>
<div class="m2"><p>چون دو کون اندر دو دستت جمع شد دستی بزن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سر بر آر از گلشن تحقیق تا در کوی دین</p></div>
<div class="m2"><p>کشتگان زنده بینی انجمن در انجمن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در یکی صف کشتگان بینی به تیغی چون حسین</p></div>
<div class="m2"><p>در دگر صف خستگان بینی به زهری چون حسن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>درد دین خود بوالعجب دردیست کاندر وی چو شمع</p></div>
<div class="m2"><p>چون شوی بیمار بهتر گردی از گردن زدن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اندرین میدان که خود را می دراندازد جهود</p></div>
<div class="m2"><p>وندرین مجلس که تن را می‌بسوزد برهمن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اینت بی همت شگرفی کو برون ناید ز جان</p></div>
<div class="m2"><p>و آنت بی دولت سواری کو برون ناید ز تن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هر خسی از رنگ گفتاری بدین ره کی رسد</p></div>
<div class="m2"><p>درد باید عمر سوز و مرد باید گام زن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سالها باید که تا یک سنگ اصلی ز آفتاب</p></div>
<div class="m2"><p>لعل گردد در بدخشان یا عقیق اندر یمن</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ماهها باید که تا یک پنبه دانه ز آب و خاک</p></div>
<div class="m2"><p>شاهدی را حله گردد یا شهیدی را کفن</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>روزها باید که تا یک مشت پشم از پشت میش</p></div>
<div class="m2"><p>زاهدی را خرقه گردد یا حماری را رسن</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>عمرها باید که تا یک کودکی از روی طبع</p></div>
<div class="m2"><p>عالمی گردد نکو یا شاعری شیرین سخن</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>قرنها باید که تا از پشت آدم نطفه‌ای</p></div>
<div class="m2"><p>بوالوفای کرد گردد یا شود ویس قرن</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چنگ در فتراک صاحبدولتی زن تا مگر</p></div>
<div class="m2"><p>برتر آیی زین سرشت گوهر و صرف ز من</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>روی بنمایند شاهان شریعت مر ترا</p></div>
<div class="m2"><p>چون عروسان طبیعت رخت بندند از بدن</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>تا تو در بند هوایی از زر و زن چاره نیست</p></div>
<div class="m2"><p>عاشقی شو تا هم از زر فارغ آیی هم ز زن</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>نفس تو جویای کفرست و خردجویای دین</p></div>
<div class="m2"><p>گر بقا خواهی بدین آی ار فنا خواهی به تن</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>جان‌فشان و پای کوب و راد زی و فرد باش</p></div>
<div class="m2"><p>تا شوی باقی چو دامن برفشانی زین دمن</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>کز پی مردانگی پاینده ذات آمد چنار</p></div>
<div class="m2"><p>وز پی تر دامنی اندک حیات آمد سمن</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>راه رو تا دیو بینی با فرشته در مصاف</p></div>
<div class="m2"><p>ز امتحان نفس حسی چند باشی ممتحن</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>چون برون رفت از تو حرص آن گه در آمد در تو دین</p></div>
<div class="m2"><p>چون در آمد در تو دین آن گه برون شد اهرمن</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>گر نمی‌خواهی که پرها رویدت زین دامگاه</p></div>
<div class="m2"><p>همچو کرم پیله جز گرد نهاد خود متن</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بار معنی بند ازینجا زان که در صحرای حشر</p></div>
<div class="m2"><p>سخت کاسد بود خواهد تیز بازار سخن</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>باش تا طومار دعویها فرو شوید خرد</p></div>
<div class="m2"><p>باش تا دیوان معنیها بخواند ذوالمنن</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>باش تا از پیش دلها پرده بردارد خدای</p></div>
<div class="m2"><p>تا جهانی بوالحسن بینی به معنی بوالحزن</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>ای جمال حال مردان بی‌اثر باشد مکان</p></div>
<div class="m2"><p>وز شعاع شمع تابان بی‌خبر باشد لگن</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بارنامهٔ ما و من در عالم حس‌ست و بس</p></div>
<div class="m2"><p>چون ازین عالم برون رفتی نه ما بینی نه من</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>از برون پرده بینی یک جهان پر شاه و بت</p></div>
<div class="m2"><p>چون درون پرده رفتی این رهی گشت آن شمن</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>پوشش از دین ساز تا باقی بمانی بهر آنک</p></div>
<div class="m2"><p>گر برین پوشش نمیری هم تو ریزی هم کفن</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>این جهان و آن جهانت را به یک دم در کشد</p></div>
<div class="m2"><p>چون نهنگ درد دین ناگاه بگشاید دهن</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>با دو قبله در ره توحید نتوان رفت راست</p></div>
<div class="m2"><p>یا رضای دوست باید یا هوای خویشتن</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>سوی آن حضرت نپوید هیچ دل با آرزو</p></div>
<div class="m2"><p>با چینن گلرخ نخسبد هیچ کس با پیرهن</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>پردهٔ پرهیز و شرم از روی ایمان بر مدار</p></div>
<div class="m2"><p>تا به زخم چشم نااهلان نگردی مفتتن</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>گرد قرآن گرد زیرا هر که در قرآن گریخت</p></div>
<div class="m2"><p>آن جهان رست از عقوبت این جهان جست از فتن</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>چون همی دانی که قرآن را رسن خواندست حق</p></div>
<div class="m2"><p>پس تو در چاه طبیعت چند باشی با وسن</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>چرخ گردان این رسن را می‌رساند تا به چاه</p></div>
<div class="m2"><p>گر همی صحرات باید چنگ در زن در رسن</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>گرد سم اسب سلطان شریعت سرمه کن</p></div>
<div class="m2"><p>تا شود نور الاهی با دو چشمت مقترن</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>گر عروس شرع را از رخ براندازی نقاب</p></div>
<div class="m2"><p>بی خطا گردد خطا و بی‌خطر گردد ختن</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>سنی دین‌دار شو تا زنده مانی زان که هست</p></div>
<div class="m2"><p>هر چه جز دین مردگی و هر چه جز سنت حزن</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>مژه در چشم سنایی چون سنانی باد تیز</p></div>
<div class="m2"><p>گر سنایی زندگی خواهد زمانی بی‌سنن</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>با سخنهای سنایی خاصه در زهد و مثل</p></div>
<div class="m2"><p>فخر دارد خاک بلخ امروز بر بحر عدن</p></div></div>