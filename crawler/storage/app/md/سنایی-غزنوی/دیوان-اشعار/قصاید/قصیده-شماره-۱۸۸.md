---
title: >-
    قصیدهٔ شمارهٔ ۱۸۸
---
# قصیدهٔ شمارهٔ ۱۸۸

<div class="b" id="bn1"><div class="m1"><p>عشق تو بربود ز من مایهٔ مایی و منی</p></div>
<div class="m2"><p>خود نبود عشق ترا چاره ز بی‌خویشتنی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دست کسی بر نرسد به شاخ هویت تو</p></div>
<div class="m2"><p>تا رگ نخلیت او ز بیخ و بن بر نکنی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با لب تو باد بود، سیرت نیکی و بدی</p></div>
<div class="m2"><p>با رخ تو خاک بود صورت مردی و زنی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خنجر تیزیست برو حنجر هر کس که بری</p></div>
<div class="m2"><p>حلقه به گوشیست درو حلقهٔ هر در که زنی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پردهٔ نزهت گه تو روی بلال حبشی</p></div>
<div class="m2"><p>عود سراپردهٔ تو جان اویس قرنی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جان مرا مست کنی مست چو بر من گذری</p></div>
<div class="m2"><p>عقل مرا پست کنی زلف چو در هم شکنی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>راست چو دیوانه شوم بند مرا برگسلی</p></div>
<div class="m2"><p>باز چو هشیار شوم سلسله درهم فگنی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چند کشی جان مرا در طلب بی طلبی</p></div>
<div class="m2"><p>چند زنی عقل مرا از حزن بی حزنی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ایزدی و اهرمنی کرد مرا زلف و رخت</p></div>
<div class="m2"><p>باز رهان جان مرا زیزدی و اهرمنی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از ره شیرین سخنی بس ترشم در ره تو</p></div>
<div class="m2"><p>جان مرا پاک بشوی از خوشی و خش سخنی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چون تو بیایی برود هم دل و هم تن ز برم</p></div>
<div class="m2"><p>دل که بود تا تو دلی تن چه بود تا تو تنی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>از من و من سیر شدم بر در تو زان که همی</p></div>
<div class="m2"><p>من چو بیایم تو نه‌ای من چو نمانم تو منی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بر در و در مجلس تو تا تو بوی من نبوم</p></div>
<div class="m2"><p>خود نبود در ره تو هم صنمی هم شمنی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بوالحسنم گشت لقب از بس تکرار کنم</p></div>
<div class="m2"><p>پیش خیال تو همی از سخن بوالحسنی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>شرقنی غربنی اخرجنی من وطنی</p></div>
<div class="m2"><p>اذا تغیبت بدا وان بدا غیبنی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>کی رهم از خوف و رجا تا کند از منع و عطا</p></div>
<div class="m2"><p>غمزهٔ تو عمر هبا خندهٔ تو عیش هنی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>کی شود ای جان جهان با لب و با غمزهٔ تو</p></div>
<div class="m2"><p>عشق سنایی و فنا عقل سنایی و سنی</p></div></div>