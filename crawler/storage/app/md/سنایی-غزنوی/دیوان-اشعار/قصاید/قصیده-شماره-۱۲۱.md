---
title: >-
    قصیدهٔ شمارهٔ ۱۲۱
---
# قصیدهٔ شمارهٔ ۱۲۱

<div class="b" id="bn1"><div class="m1"><p>خیز تا خود ز عقل باز کنیم</p></div>
<div class="m2"><p>در میدان عشق باز کنیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یوسف چاه را به دولت دوست</p></div>
<div class="m2"><p>در چه صد هزار باز کنیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در قمار وقار بنشینیم</p></div>
<div class="m2"><p>خویشتن جبرییل ساز کنیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر چه شیب و فراز پردهٔ ماست</p></div>
<div class="m2"><p>خاک بر شیب و بر فراز کنیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز بر و زیر چرخ هرزه زنیم</p></div>
<div class="m2"><p>آن به از هر دو احتراز کنیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جان کبکی برون کنیم از تن</p></div>
<div class="m2"><p>خویشتن جان شاهباز کنیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به خرابات روح در تازیم</p></div>
<div class="m2"><p>در به روی خرد فراز کنیم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آه را از برای زنده دلی</p></div>
<div class="m2"><p>ملک‌الموت جان آز کنیم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ناز را از برای پخته شدن</p></div>
<div class="m2"><p>هیزم آتش نیاز کنیم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>با نیازیم تا همه ماییم</p></div>
<div class="m2"><p>چون همه او شدیم ناز کنیم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آلت عشرت ظریفان را</p></div>
<div class="m2"><p>آفت عقل عشوه ساز کنیم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>خم زلفین خوبرویان را</p></div>
<div class="m2"><p>حجرهٔ روز های راز کنیم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>در زمین بی زمین سجود بریم</p></div>
<div class="m2"><p>در جهان بی‌جهان نماز کنیم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>سه شراب حقیقتی بخوریم</p></div>
<div class="m2"><p>چار تکبیر بر مجاز کنیم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>از سنایی مگر سنایی را</p></div>
<div class="m2"><p>به یکی باده درد باز کنیم</p></div></div>