---
title: >-
    قصیدهٔ شمارهٔ ۱۶۸
---
# قصیدهٔ شمارهٔ ۱۶۸

<div class="b" id="bn1"><div class="m1"><p>دلی از خلق عالم بی‌غمی کو</p></div>
<div class="m2"><p>برون از عالم دل عالمی کو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>درین عالم دم و غم جفت باید</p></div>
<div class="m2"><p>مرا غم هست باری همدمی کو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نگویی تا که درد عاشقی را</p></div>
<div class="m2"><p>بجز مرگ از دواها مرهمی کو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به عشق اندر ز بیم هجر بنمای</p></div>
<div class="m2"><p>که از خلق عالم خرمی کو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگر مردان عالم کمزنانند</p></div>
<div class="m2"><p>ترا زان کمزدن آخر کمی کو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حکایت چند از ابلیس و آدم</p></div>
<div class="m2"><p>همه ابلیس گشتند آدمی کو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جهان دیو طبیعت جمله بگرفت</p></div>
<div class="m2"><p>دریغا از حقیقت رستمی کو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اگر دعوی کنی در ملک بنمای</p></div>
<div class="m2"><p>که در انگشت ملکت خاتمی کو</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سلیمان‌وار اگر خواهی همی ملک</p></div>
<div class="m2"><p>ز بادت خنگ و ز ابرت ادهمی کو</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چو در دین بر خلاف امر و نهیی</p></div>
<div class="m2"><p>ز کامت نالهٔ زیر و بمی کو</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>همه سور هوای نفس سازند</p></div>
<div class="m2"><p>ز آه و درد دینشان ماتمی کو</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به شرع اندر ز بهر طوف کعبه</p></div>
<div class="m2"><p>ز چینی و ز زنگی محرمی کو</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بجز در عالم تسلیم و تحقیق</p></div>
<div class="m2"><p>دلی پر غم و پشت پر خمی کو</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ز بهر عدت گور و قیامت</p></div>
<div class="m2"><p>ترا در چشم دل نار و نمی کو</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چو در نی بست تن ایمن نشستی</p></div>
<div class="m2"><p>ز دل در جان جانت طارمی کو</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>همه گویندهٔ فسق و فجوریم</p></div>
<div class="m2"><p>ز هزل و ژاژ گفتن با کمی کو</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>براهیمان بسی بودند لیکن</p></div>
<div class="m2"><p>بگو تا چون خلیل و ادهمی کو</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>به عالم در فراوان سنگ و چاهست</p></div>
<div class="m2"><p>ولی چون عیسی‌بن مریمی کو</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>سنایی‌وار در عالم تو بنگر</p></div>
<div class="m2"><p>ز بهرش ارحمی و ترحمی کو</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>اگر فارغ شدی در دین ز دنیا</p></div>
<div class="m2"><p>بست رخ بی ریا دل بی غمی کو</p></div></div>