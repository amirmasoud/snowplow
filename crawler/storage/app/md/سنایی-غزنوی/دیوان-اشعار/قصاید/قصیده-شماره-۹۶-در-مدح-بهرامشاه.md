---
title: >-
    قصیدهٔ شمارهٔ ۹۶ - در مدح بهرامشاه
---
# قصیدهٔ شمارهٔ ۹۶ - در مدح بهرامشاه

<div class="b" id="bn1"><div class="m1"><p>مست گشتم ز لطف دشنامش</p></div>
<div class="m2"><p>یارب آن می بهست یا جامش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عنبرش خلق و زلف هم خلقش</p></div>
<div class="m2"><p>حسنش نام و روی هم نامش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل به چین رفت و بازگشت و ندید</p></div>
<div class="m2"><p>به ز اندام ترکه اندامش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سوی آن کو بخیل‌تر در عصر</p></div>
<div class="m2"><p>زر پختست نقرهٔ خامش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>لب و چشمم بماند پیوسته</p></div>
<div class="m2"><p>بستهٔ کوی و فتنهٔ نامش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون به زلف و به عارضش نگری</p></div>
<div class="m2"><p>به گه خوشخویی و آرامش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>صبح بینی همه گریبان باز</p></div>
<div class="m2"><p>بسته بر زیر دامن شامش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>لام گردد چو دید ماه او را</p></div>
<div class="m2"><p>با الف سان قدی به اندامش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>راست خواهی به پیش او مه را</p></div>
<div class="m2"><p>سخت پژمرده گشت الف لامش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>پسته‌ها خوش توان شکست از بوس</p></div>
<div class="m2"><p>بر یکی پسته و دو بادامش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>همه راهش خراب کرد وخلاب</p></div>
<div class="m2"><p>چشمم از بهر غیرت کامش</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هم به روی نکوش اگر هستم</p></div>
<div class="m2"><p>از پی دانه بستهٔ دامش</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>هست یک رنگ نزد من در عشق</p></div>
<div class="m2"><p>دیدهٔ توسن و لب رامش</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>هیچ کامم نماند جز یک کام</p></div>
<div class="m2"><p>چیست آن کام جستن کامش</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>زیر فامم به صد هزاران جان</p></div>
<div class="m2"><p>از پی عارض سمن فامش</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چون تقاضاگر اوست باکی نیست</p></div>
<div class="m2"><p>گردن ما و منت وامش</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>زان که در راه عشق گاه به گاه</p></div>
<div class="m2"><p>دوست دارم جفا و دشنامش</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>خواهم از وی به قصد شفتالو</p></div>
<div class="m2"><p>بهر دشنام خسته بادامش</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>کرد عشقش دل سنایی خوش</p></div>
<div class="m2"><p>باد خوش چون دل شه ایامش</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>شاه بهرام شاه آنک او را</p></div>
<div class="m2"><p>خاک پایست جرم بهرامش</p></div></div>