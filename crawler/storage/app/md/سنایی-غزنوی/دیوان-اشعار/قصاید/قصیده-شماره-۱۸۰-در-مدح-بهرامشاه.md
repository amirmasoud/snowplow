---
title: >-
    قصیدهٔ شمارهٔ ۱۸۰ - در مدح بهرامشاه
---
# قصیدهٔ شمارهٔ ۱۸۰ - در مدح بهرامشاه

<div class="b" id="bn1"><div class="m1"><p>این چه بود ای جان که ناگه آتش اندر من زدی</p></div>
<div class="m2"><p>دل ببردی و چو بوبکر ربابی تن زدی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا مرا دیدی ز خلق از عشق رویت سوخته</p></div>
<div class="m2"><p>سنگ و آهن بودت از دل سنگ بر آهن زدی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>قامتم چون لام و نون کردی چو موسی در امید</p></div>
<div class="m2"><p>پس مرا در گلبن غیرت نوای «لن» زدی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر زمان از جای سری روید همی بر تن چو شمع</p></div>
<div class="m2"><p>تا مرا از دست خود چون شمع خود گردن زدی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چشمهای من چو چشم ابر کردی تا تو شوخ</p></div>
<div class="m2"><p>ناگه از عنبر به گرد قرص مه خرمن زدی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جوشن صبر و شکیباییم خون نو شد ز زخم</p></div>
<div class="m2"><p>تا ز زلف چون زره تیغی بر آن جوشن زدی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کی فرو زد مر ترا قندیل دلداری چو تو</p></div>
<div class="m2"><p>آب بر آتش گرفتی خاک در روغن زدی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کی شود پیراهنت هم قدر قد تو چو تو</p></div>
<div class="m2"><p>از گریبان کاست کردی آنچه در دامن زدی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>روزنی بود از برای روز رویت بر دلم</p></div>
<div class="m2"><p>از بخیلی گل بیاوردی و بر روزن زدی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شد جهان بر چشم من چون چشم سوزن تنگ و تار</p></div>
<div class="m2"><p>از پی رغم مرا شمشاد بر سوسن زدی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از برون آفرینش گلشنی بر ساختی</p></div>
<div class="m2"><p>برکشیدی نردبان و خیمه در گلشن زدی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>رشتهٔ تو کس نداند تافت کز شوخی و کبر</p></div>
<div class="m2"><p>سوزنی کردی مرا پس کوه بر سوزن زدی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>از سنایی دل ربودی شکر چون کردی ز غیر</p></div>
<div class="m2"><p>جان ز یزدان یافتی چو لاف ز اهریمن زدی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>زخم داری بهر دشمن رحم داری بهر دوست</p></div>
<div class="m2"><p>دوست بودم از چه بر من زخم چون دشمن زدی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>پس چو هست از زخم شاه ما همی گردد چو نیست</p></div>
<div class="m2"><p>آنچه شه بر دشمن خود زد چرا بر من زدی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>شاه ما بهرامشه آن شه که گوید دولتش</p></div>
<div class="m2"><p>زه که چون گردون جهانی خصم را گردن زدی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چرخ چندان بر زمین کی زد به صد دوران که تو</p></div>
<div class="m2"><p>زان سنان چرخ دوز و گرز کوه افگن زدی</p></div></div>