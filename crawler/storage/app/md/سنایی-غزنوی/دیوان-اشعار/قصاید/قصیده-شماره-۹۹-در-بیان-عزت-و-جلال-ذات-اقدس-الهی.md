---
title: >-
    قصیدهٔ شمارهٔ ۹۹ - در بیان عزت و جلال ذات اقدس الهی
---
# قصیدهٔ شمارهٔ ۹۹ - در بیان عزت و جلال ذات اقدس الهی

<div class="b" id="bn1"><div class="m1"><p>مقدسی که قدیمست از صفات کمال</p></div>
<div class="m2"><p>منزهی که جلیل ست بر نعوت جلال</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به ذات لم یزلی هست واحد اندر مجد</p></div>
<div class="m2"><p>بعز وحدت پیدا از او سنا و کمال</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>صفات قدس کمالش بری ز علت کون</p></div>
<div class="m2"><p>نمای بحر لقایش بداده فیض وصال</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به هستی جبروتی نیاید اندر وهم</p></div>
<div class="m2"><p>به عزت ملکوتی بری ز شکل و مثال</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جلال و عز قدیمش نبوده مدرک خلق</p></div>
<div class="m2"><p>نه عقل یابد بروی سبیل مثل و مثال</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نه اولیت او را بود گه اول</p></div>
<div class="m2"><p>نه آخریت او را نهایتست و مآل</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زحیر حد ثانی ورا بود منزل</p></div>
<div class="m2"><p>نه در مشاهد قربی جلال اوست جدال</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به قدرت صمدیت لطایف صنعش</p></div>
<div class="m2"><p>بداده هر صفتی را هزار حسن و جمال</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به ساحت قدمش نگذرد قیام فهوم</p></div>
<div class="m2"><p>نهاده قهر قدیمش به پای عقل عقال</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چه یافت خاطر ادراک او به جز حیرت</p></div>
<div class="m2"><p>چه گفت وهم مزور به جز فضول و فضال</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به ذات پاک نماند به هیچ صورت و جسم</p></div>
<div class="m2"><p>منزهست به وصف از حلول حالت و حال</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>جلال وحدت او در قدم به سرمد بود</p></div>
<div class="m2"><p>صفات عزت او باقیست در آزال</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به وحدت ازلی انقسام نپذیرد</p></div>
<div class="m2"><p>به عزت ابدی نیست شبه هر اشکال</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>به کنه ذاتش غفلت عقول را از غیب</p></div>
<div class="m2"><p>نه در سرادق مجدش علوم راست مجال</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نه قهر باشد او را تغیر اندر وصف</p></div>
<div class="m2"><p>نه در صنایع لطفش بود فتور و زوال</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>هر آنکه در صفتش شبه و مثل اندیشد</p></div>
<div class="m2"><p>بود دل سیهش نقش گیر کفر و ضلال</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>هر آنکه کرد اشارت به ذات بی چونش</p></div>
<div class="m2"><p>بود به صرف حقیقت چو عابد تمثال</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>برای جلوه‌گری از سرادق عرشی</p></div>
<div class="m2"><p>کند منور مغرب بروی خوب هلال</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>به صبحدم کشد او شمس از دریچهٔ شرق</p></div>
<div class="m2"><p>نهد به قبهٔ چرخ بلند وقت زوال</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ز نور چرخ منور کند طلایهٔ سیم</p></div>
<div class="m2"><p>کند ز بیضهٔ کافور صبح ارض و جبال</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ز قطره ابر کند در صدف به حکمت در</p></div>
<div class="m2"><p>ز عین قدرت آرد هزار نهر زلال</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>هزار نافهٔ مشک ازل دهد هر شب</p></div>
<div class="m2"><p>برای نفخهٔ عشاق بر جنوب و شمال</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ز چاه شرق برآرد به صبحدم خورشید</p></div>
<div class="m2"><p>کند منور از نور او وهاد و تلال</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ز سبغ حکمت رنگین کند به که لاله</p></div>
<div class="m2"><p>نهد به چهرهٔ خوبان چین به قدرت خال</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>نهاده در دل خورشید آتشین گوهر</p></div>
<div class="m2"><p>بداده چهرهٔ مه را هزار نور و نوال</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بریده است به مقراض عزت و تقدیس</p></div>
<div class="m2"><p>زبان تیغ خلیقت ز مدحتش در قال</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>خورنده لقمهٔ جودش ز عرش تا به ثری</p></div>
<div class="m2"><p>به درگه صمدی عاجزند جمله عیال</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>چو خاک گشته به درگاه او مه و خورشید</p></div>
<div class="m2"><p>شده‌ست بندهٔ درگاه او دهور و طوال</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>کند سجود وی از جان همه مکین و مکان</p></div>
<div class="m2"><p>کند خضوع کمالش همه جبال و رمال</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>به عزتش بشتابد بهار در جوشش</p></div>
<div class="m2"><p>به امر اوست روان سیل دجلهٔ سیال</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>کند ثنای جلالش زبان رعدا زخوف</p></div>
<div class="m2"><p>مسبح ست مر او را چو ابر و برق ثقال</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>گشاده‌اند زبان در ثنای او مرغان</p></div>
<div class="m2"><p>چو عندلیب و چکاوک چو طوطی و چو دال</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>مدبری که ندارد شریک در عزت</p></div>
<div class="m2"><p>معطلی ست بر او وجود عقل فعال</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>ز قهر او شده کوه گران چو حلقهٔ میم</p></div>
<div class="m2"><p>ز خدمتش شده پشت فلک چو حلقهٔ دال</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>نهاده در دل عشاق سرهای قدم</p></div>
<div class="m2"><p>چگونه گوید سر ازل زبان کلال</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>هر آنکه شربت سبحانی وانالحق خورد</p></div>
<div class="m2"><p>به تیغ غیرت او کشته در هزار قتال</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>ز آهوان طریقت هر آنکه شیر آمد</p></div>
<div class="m2"><p>نهاده‌است به پایش هزارگونه شکال</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>زمازم ملکوتش کند دلم چون خون</p></div>
<div class="m2"><p>مراست جام وصالش همیشه مالامال</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>به نغمه‌های مزامیر عشق او هستم</p></div>
<div class="m2"><p>شراب وصلش دایم مرا شدست حلال</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>چو بوی گلبن او بشنوم به باغ ازل</p></div>
<div class="m2"><p>شوم چو حور جنانی به حسن و غنج و دلال</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>ز خاک معصیت ار بر رخم بودی گردی</p></div>
<div class="m2"><p>چو خاک درگه اویم نباشد ایچ وبال</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>ز رهروان معارف منم درین عالم</p></div>
<div class="m2"><p>بود مرا ز خصایص درین هزار خصال</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>به جان جان دهم از جان و دل همه شب و روز</p></div>
<div class="m2"><p>صلاتها و تحیات بر محمد و آل</p></div></div>