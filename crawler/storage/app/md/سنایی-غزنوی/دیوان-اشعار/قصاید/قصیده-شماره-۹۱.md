---
title: >-
    قصیدهٔ شمارهٔ ۹۱
---
# قصیدهٔ شمارهٔ ۹۱

<div class="b" id="bn1"><div class="m1"><p>یکی بهتر ببینید ایها الناس</p></div>
<div class="m2"><p>که می دیگر شود عالم به هر پاس</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دمی از گردش حالات عالم</p></div>
<div class="m2"><p>نمی‌یابم نجات از بند وسواس</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو دل در عقدهٔ وسواس باشد</p></div>
<div class="m2"><p>چه دانم دیدن از انواع و اجناس</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کجا ماند جهان را روشنایی</p></div>
<div class="m2"><p>چو خورشید افتد اندر عقدهٔ راس</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو سود از آرزو چون نیست روزی</p></div>
<div class="m2"><p>دهش ماند دهش جز یافه مشناس</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یکی بین آرمیده در غنا غرق</p></div>
<div class="m2"><p>یکی پویان و سرگشته ز افلاس</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بدور طاس کس نتوان رسیدن</p></div>
<div class="m2"><p>توان دور فلک پیمودن از طاس</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ترا ندهند هرچ از بهر تو نیست</p></div>
<div class="m2"><p>بهر کار این سخن را دار مقیاس</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سکندر جست لیکن یافت بهره</p></div>
<div class="m2"><p>ز آب زندگانی خضر و الیاس</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بسی فربه نماید آنکه دارد</p></div>
<div class="m2"><p>نمای فربهی از نوع آماس</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به ریواس ار توان لعبت روان کرد</p></div>
<div class="m2"><p>روان نتوان بدو دادن به ریواس</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>خلایق بر خلافند از طبایع</p></div>
<div class="m2"><p>یکی عطار ودیگر باز کناس</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چو رومی گوید از پوشش نپوشم</p></div>
<div class="m2"><p>بجز ابریشمین پاک بی‌لاس</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>برهنهٔ زنگی بی غم بر افسوس</p></div>
<div class="m2"><p>همی گوید: چه گردی گرد کرباس</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ز سر بر کردن این کشت از دل و خاک</p></div>
<div class="m2"><p>چه سودش چون کند سر در سر داس</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چو دانه دیدی اندر خوشه رسته</p></div>
<div class="m2"><p>ببین هم گشته زیر آسیا آس</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>سخن کز روی حکمت گفت خواهی</p></div>
<div class="m2"><p>جدا کن ناس را اول ز نسناس</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چو ناس آمد بگو حق ای سنایی</p></div>
<div class="m2"><p>به حق گفتم ز هر نسناس مهراس</p></div></div>