---
title: >-
    قصیدهٔ شمارهٔ ۱۸۶ - در مدح شرف الملک امیر زنگی محسن
---
# قصیدهٔ شمارهٔ ۱۸۶ - در مدح شرف الملک امیر زنگی محسن

<div class="b" id="bn1"><div class="m1"><p>با چشم چو بحرم ز گهر خنده نگاری</p></div>
<div class="m2"><p>با عیش چو زهرم به شکر بوسه شکاری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>برگرد بناگوش چو عاجش خط مشکین</p></div>
<div class="m2"><p>چون دای رخ کز شب بکشی گرد نهاری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خورشید نماینده بتی ماه جبینی</p></div>
<div class="m2"><p>کافور بناگوش مهی مشک عذاری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خوبی خطش بین که بر آن روی چو لاله</p></div>
<div class="m2"><p>کرده ز ره غالیه آساش حصاری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از تیر مژهٔ کوه گذارش دل عاشق</p></div>
<div class="m2"><p>خسته شده و پر خون همچون گل ناری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>با دو لب چون باده و با چشم چو نرگس</p></div>
<div class="m2"><p>با دو رخ چون لاله و با زلف چو قاری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در زلفش از آن دو رخ چون لاله نشاطی</p></div>
<div class="m2"><p>در چشمش از آب دو لب چون باده خماری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زین عشوه فروشندهٔ پیوسته دروغی</p></div>
<div class="m2"><p>زین بیهده اندیشهٔ بگسسته فساری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون آبی و چون سیب ازین صد تنه حوری</p></div>
<div class="m2"><p>چون نار و چو نارنگ ازین ده له یاری</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آتش به تن و جان جهانی زده و آن گه</p></div>
<div class="m2"><p>چون آب نبینیش به یک جای قراری</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>اینجای ز بی رحمی دلسوخته قومی</p></div>
<div class="m2"><p>و آنجای ز بی شرمی بر ساخته کاری</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هم جان سر او که از آن ماه نخواهم</p></div>
<div class="m2"><p>جز بوس و کناری و حدیثی و نظاری</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ور خواهم ازو بوس و کناری ز بخیلی</p></div>
<div class="m2"><p>چون صبر من از من کند آن ماه کناری</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>اینک که یکی هفتست کان ماه دو هفته</p></div>
<div class="m2"><p>کردست کناره ز پی بوس و کناری</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>امروز بدیدمش به نومیدی گفتم</p></div>
<div class="m2"><p>کز ریش منت شرم همی ناید باری</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>دو لعل ز هم باز گشاد از سر طعنه</p></div>
<div class="m2"><p>افروخت درین دل ز سر شوخی ناری</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>گفتا که برو بیش مکن خواجه سنایی</p></div>
<div class="m2"><p>با ما چه حسابت ترا یا چه شماری</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>سیمای تو حقا که چو زر باشد بی سیم</p></div>
<div class="m2"><p>گلزار نیابی تو مشو در گلزاری</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بی سیم ازین باغ بر آراسته دانم</p></div>
<div class="m2"><p>والله که نیابی تو ازین گلبن خاری</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گفتم که ندارم چکنم گفت نگارم</p></div>
<div class="m2"><p>خواهی که شود کار تو ناگه چو نگاری</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>در پردهٔ اندیشه بیارای عروسی</p></div>
<div class="m2"><p>پس جلوه کنش پیش مهی شاه تباری</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>آن آیت احسان و شرف زنگی محسن</p></div>
<div class="m2"><p>کاسوده شده از رستهٔ احسانش دیاری</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>آن بحر گهر پاش که نسرشت طبایع</p></div>
<div class="m2"><p>همچون گهر اندر گهرش عیب و عواری</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>آن شمس عطابخش که ننهاد عناصر</p></div>
<div class="m2"><p>همچون فلک اندر گهرش دود و بخاری</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>دوزخ شود از آتش سعیش چو بهشتی</p></div>
<div class="m2"><p>گلبن شود از قوت عونش چو چناری</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>حزمش کند اندر شکم خاک مقامی</p></div>
<div class="m2"><p>حلمش کند اندر گهر باد قراری</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>حقا که به یک لحظه ازین هر دو برآید</p></div>
<div class="m2"><p>در آتش و در آب قراری و وقاری</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>ای زاده ز تو طبع تو از سور سروری</p></div>
<div class="m2"><p>وی داده به تو بخت تو از مهر مهاری</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>در روی سخا از دل چون بحر تو آبی</p></div>
<div class="m2"><p>وندر دل بخل از کف چون ابر تو ناری</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>چون ذات هنر نیست در اوصاف تو عیبی</p></div>
<div class="m2"><p>چون فعل خردنیست در اعمال تو عاری</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>نه دایره یک لحظه کناره کند از سیر</p></div>
<div class="m2"><p>گر بروزد از موکب عزم تو غباری</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>چون لعل فسرده شود آب همه دریا</p></div>
<div class="m2"><p>گر تاب دهد آتش عزم تو شراری</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>ای مرحکما را ز یسار تو یمینی</p></div>
<div class="m2"><p>وی مر شعرا را ز یمنین تو یساری</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>بر اسب امید آمده مجدود سنایی</p></div>
<div class="m2"><p>در زیر پی از بهر کفت راهگذاری</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>زیرا که ز بی‌پیرهنی از قبل شرم</p></div>
<div class="m2"><p>در خانه چو خفاش بدو مانده بشاری</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>از بهر چه گویند فضولان به یکی کنج</p></div>
<div class="m2"><p>چون شپرکی ساخته از روز حصاری</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>ای خواجهٔ با جود بدان از قبل آنک</p></div>
<div class="m2"><p>دارم طمع از جود تو زین شعر شعاری</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>کاین سینه و پستان چو دو خرمن لاله</p></div>
<div class="m2"><p>گشتست ز سرما چو یکی شاخ چناری</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>چون قله دو پستانگه و چون شیر یکی ناف</p></div>
<div class="m2"><p>چون ماه یکی خفته و چون زهره زهاری</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>چون گردهٔ پیه تنک آن کون چو دنبه</p></div>
<div class="m2"><p>از پارهٔ شلوار برون آمده پاری</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>از پارهٔ شلوار همی تابد لعلش</p></div>
<div class="m2"><p>چون از تنکی شیشه بتابد گل ناری</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>از نازکی و تازگی و فربهی او</p></div>
<div class="m2"><p>گوی چو نگاری که نگنجد به کناری</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>بی موی و در و دوغ فرود آمده مشکی</p></div>
<div class="m2"><p>چون شیر و درو موی پدید آمده تاری</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>وندر بن این سفجهٔ سیمین کفیده</p></div>
<div class="m2"><p>نابوده و نامیخته آهخته خیاری</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>ناداده یکی بوسه چنان کاید ازین لب</p></div>
<div class="m2"><p>این فربه ما بر لب و بر فرق نزاری</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>ارزد برت ای کون همه خوبان دیده</p></div>
<div class="m2"><p>این شخص به دراعه و این کون به ازاری</p></div></div>