---
title: >-
    قصیدهٔ شمارهٔ ۳ - این قصیدهٔ را عارف زرگر در مدح سنایی گفته
---
# قصیدهٔ شمارهٔ ۳ - این قصیدهٔ را عارف زرگر در مدح سنایی گفته

<div class="b" id="bn1"><div class="m1"><p>ای نهاده پای همت بر سر اوج سما</p></div>
<div class="m2"><p>وی گرفته ملک حکمت گشته در وی مقتدا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر سریر حکمت اندر خطهٔ کون و فساد</p></div>
<div class="m2"><p>از تو عادل‌تر نبد هرگز سخن را پادشا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مشرق و مغرب ز راه صلح بگرفتی بکلک</p></div>
<div class="m2"><p>ناکشیده تیغ جنگی روز کین اندر وغا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>لاجرم ز انصاف تو، روی ز من شد پر درر</p></div>
<div class="m2"><p>همچو از اوصاف تو، چشم زمانه پر ضیا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گوی همت باختی با خلق در میدان عقل</p></div>
<div class="m2"><p>باز پس ماندند و بردی و برین دارم گوا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نی غلط کردم که رای صایبت با اهل عصر</p></div>
<div class="m2"><p>کی پسندد از تو بازی یا کجا دارد روا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون زر و طاعت عزیزی در دو عالم زان که تو</p></div>
<div class="m2"><p>با قناعت همنشینی با فراغت آشنا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سیم نااهلان نجویی زان که نپسندد خرد</p></div>
<div class="m2"><p>خاکروبی کردن آن کس را که داند کیمیا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شعر تو روحانیان گر بشنوند از روی صدق</p></div>
<div class="m2"><p>بانگ برخیزد ازیشان کای سنایی مرحبا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>حجتی بر خلق عالم زان دو فعل خوب خویش</p></div>
<div class="m2"><p>شاعری بی‌ذل طمع و پارسایی بی‌ریا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>عیسی عصری که از انفاس روحانیت هست</p></div>
<div class="m2"><p>مردگان آز و معلولان غفلت را شفا</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بس طبیب زیرکی زیرا که بی‌نبض و علیل</p></div>
<div class="m2"><p>درد هر کس را ز راه نطق می‌سازی دوا</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نظم گوهربار عقل افزای جان افروز تو</p></div>
<div class="m2"><p>کرد شعر شاعران بوده را یکسر هبا</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>معجز موسی نمایست این و آنها سحر و کی</p></div>
<div class="m2"><p>ساحری زیبا نماید پیش موسی و عصا</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>هر که او شعر ترا گوید جواب از اهل عصر</p></div>
<div class="m2"><p>نزد عقل آنکس نماید یافه گوی و هرزه لا</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>زان که بشناسند بزازان زیرک روز عرض</p></div>
<div class="m2"><p>اطلس رومی و شال ششتری از بوریا</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>شاعران را پایه بی‌شرمی بود تا زان قبل</p></div>
<div class="m2"><p>حاصل و رایج کنند از مدح ممدوحان عطا</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>صورت شرمی تو اندر سیرت پاکی بلی</p></div>
<div class="m2"><p>با چنان ایمان کامل، این چنین باید حیا</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>شعر و سحر و شرع و حکمت آمدت اندر خبر</p></div>
<div class="m2"><p>ره برد اسرار او چون بنگرد عین‌الرضا</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>کاین چهارست ای سنایی چار حرف و یافتند</p></div>
<div class="m2"><p>زین چهار آن هر چهار از نظم و نثر اوستا</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>تا حریم کعبه باشد قبلهٔ اهل سنن</p></div>
<div class="m2"><p>تا نعیم سدره باشد طعمهٔ اهل بقا</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>سدره بادت دستگاه بخشش دارالبقا</p></div>
<div class="m2"><p>کعبه بادت پایگاه کوشش دارالفنا</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>کعبه و سدره مبادت مقصد همت که نیست</p></div>
<div class="m2"><p>جز «و یبقی وجه ربک» مر ترا کام و هوا</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>نظم عشق‌آمیز عارف را ز راه لطف و بر</p></div>
<div class="m2"><p>برگذر از عیبهاش و در گذر از وی خطا</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>تا که باشد عارف اندر سال و ماه و روز و شب</p></div>
<div class="m2"><p>شاکر افضال تو اندر خلا و اندر ملا</p></div></div>