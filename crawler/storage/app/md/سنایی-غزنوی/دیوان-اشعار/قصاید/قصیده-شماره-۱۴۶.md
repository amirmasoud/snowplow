---
title: >-
    قصیدهٔ شمارهٔ ۱۴۶
---
# قصیدهٔ شمارهٔ ۱۴۶

<div class="b" id="bn1"><div class="m1"><p>ای مسافر اندرین ره گام عاشق‌وار زن</p></div>
<div class="m2"><p>فرش لاف اندر نورد و گفت از کردار زن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر نسیم مشک معنی نیست اندر جیب تو</p></div>
<div class="m2"><p>دست همت باری اندر دامن عطار زن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هرکت از زر باز گوید اوست دقیانوس تو</p></div>
<div class="m2"><p>گر همی دین بایدت خیمه میان غار زن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دیو طرارست پیش آهنگ حرب وی تویی</p></div>
<div class="m2"><p>سوزن تمهید را در چشم این طرار زن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پیش از آن کز غدر عالم لال گردد جان تو</p></div>
<div class="m2"><p>آتش درویشی اندر عالم غدار زن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>منزلی کآنجا نشان خیمهٔ معشوق تست</p></div>
<div class="m2"><p>خاک اندر سرمه ساز و بوسه بر دیوار زن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر نثار پای معشوقان بود در راه وصل</p></div>
<div class="m2"><p>با دو دیده در بپاش و با دو رخ ایثار زن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون سوار راهبر گشتی تو در میدان عشق</p></div>
<div class="m2"><p>شو پیاده آتش آندر زین و زین‌افزار زن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هوشیار از باده و مست از می دنیا چه سود</p></div>
<div class="m2"><p>طیلسان فقر و بر فرق چنین هشیار زن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در خرابات خرابی همچو مستان گوشه‌گیر</p></div>
<div class="m2"><p>خیمهٔ قلاشی اندر خانهٔ خمار زن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>پای در میدان مهر کمزنان ملک نه</p></div>
<div class="m2"><p>نرد بازیدی ز مستی حصل بر اسرار زن</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>جان و دل را در قبالهٔ عاشقی اقرار کن</p></div>
<div class="m2"><p>پس به نام عاشقی مهری بر آن اقرار زن</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گر همه دعوی کنی در عاشقی و مفلسی</p></div>
<div class="m2"><p>چون سنایی دم درین عالم قلندروار زن</p></div></div>