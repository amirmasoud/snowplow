---
title: >-
    قصیدهٔ شمارهٔ ۲۰۱ - در مدح بهرامشاه
---
# قصیدهٔ شمارهٔ ۲۰۱ - در مدح بهرامشاه

<div class="b" id="bn1"><div class="m1"><p>چرا چو روز بهار ای نگار خرگاهی</p></div>
<div class="m2"><p>بر این غریب نه بر یک نهاد و یک راهی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گهی به لطف چو عیسا مرا کنی فلکی</p></div>
<div class="m2"><p>گهی به قهر چو یوسف کنی مرا چاهی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گهی به بوسه امیرم کنی به راهبری</p></div>
<div class="m2"><p>گهی به غمزه اسیرم کنی به گمراهی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گه از مسافت با روغنی کنی آبی</p></div>
<div class="m2"><p>گه از لطافت با کهربا کنی کاهی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به دست رد و قبول تو چون به دست کریم</p></div>
<div class="m2"><p>عزیز و خوارم چون سیم قل هو الاهی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به مار ماهی مانی نه این تمام و نه آن</p></div>
<div class="m2"><p>منافقی چکنی مار باش یا ماهی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ندیده میوه‌ای از شاخ نیکوییت وز غم</p></div>
<div class="m2"><p>شکوفه‌وار شدم پیر وقت برناهی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به نوک غمزهٔ ساحر مباش غره چنین</p></div>
<div class="m2"><p>که هست خصم ستم ناوک سحرگاهی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از این شعار برون آی تا سوی دلها</p></div>
<div class="m2"><p>بسان شعر سنایی شوی به دلخواهی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>حدیث کوته کردم که این حدیث ترا</p></div>
<div class="m2"><p>چو عمر دشمن سلطان نکوست کوتاهی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>یمین دولت بهرامشاه بن مسعود</p></div>
<div class="m2"><p>که هست چست بر او خلعت شهنشاهی</p></div></div>