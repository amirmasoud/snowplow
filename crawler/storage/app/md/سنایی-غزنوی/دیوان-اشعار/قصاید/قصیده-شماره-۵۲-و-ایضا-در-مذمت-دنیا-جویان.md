---
title: >-
    قصیدهٔ شمارهٔ ۵۲ - و ایضا در مذمت دنیا جویان
---
# قصیدهٔ شمارهٔ ۵۲ - و ایضا در مذمت دنیا جویان

<div class="b" id="bn1"><div class="m1"><p>مرحبا بحری که از آب و گلش گوهر برند</p></div>
<div class="m2"><p>حبذا کانی کزو پاکیزه سیم و زر برند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نی ز هر کانی که بینی سیم و زر آید پدید</p></div>
<div class="m2"><p>نی ز هر بحری که بینی گوهر احمر برند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در میان صدهزاران نی یکی نی بیش نیست</p></div>
<div class="m2"><p>کز میان او به حاصل شاکران شکر برند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در میان صد هزاران نحل جز یک نحل نیست</p></div>
<div class="m2"><p>کز لعابش انگبین ناب جان‌پرور برند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جانور بسیار دیدستم به دریاها ولیک</p></div>
<div class="m2"><p>چون صدف نبود که غواصان ازو گوهر برند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گاو آبی در جزیره سنبل و سوسن چرد</p></div>
<div class="m2"><p>لاجرم هر جا که خفت از خاک او عنبر برند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>همچو آهو شو تو نیز از سنبل و سوسن بچر</p></div>
<div class="m2"><p>تا بهر جایی ز نافت نافهٔ اذفر برند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>باغشان از شوخ چشمی گشت شورستان خار</p></div>
<div class="m2"><p>طمع آن دارند کز وی سوسن و عنبر برند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سوسن و عنبر کجا آید به دست ار روضه‌ای</p></div>
<div class="m2"><p>کاندرو تخم سپست و سیر و سیسنبر برند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هر چه کاری بدروی و هر چه گویی بشنوی</p></div>
<div class="m2"><p>این سخن حقست اگر نزد سخن گستر برند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خواب ناید مرزنی را کاندر آن باشد نیت</p></div>
<div class="m2"><p>هفتهٔ دیگر مر او را خانهٔ شوهر برند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ای بهمت از زنی کم چند خسبی چون ترا</p></div>
<div class="m2"><p>هم کنون زی کردگار قادر اکبر برند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ور همی گویی که من در آرزوی ایزدم</p></div>
<div class="m2"><p>کو نشانی تا ترا باری سوی دلبر برند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>این جهان دریا و ما کشتی و زنهار اندرو</p></div>
<div class="m2"><p>تا نه پنداری که کشتیها همه همبر برند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>کشتیی را پیش باد امروز در تازان کنند</p></div>
<div class="m2"><p>کشتیی را باز از پیش بلا لنگر برند</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>کشتیی را غرق گردانند در دریای غیب</p></div>
<div class="m2"><p>کشتیی را هم ز صرصر تا در معبر برند</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>مر یکی را گل دهد تا او به بویش جان دهد</p></div>
<div class="m2"><p>و آن دگر را باز جانش ز آتشین خنجر برند</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>مر یکی را سر فرازانند ز آتش از جحیم</p></div>
<div class="m2"><p>مر یکی را باز از گوهر همه افسر برند</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>خنده آید مر مرا ز آنها که از سیم ربا</p></div>
<div class="m2"><p>درگه رفتن کفن از دیبه شوشتر برند</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>مرد آن مردست که چون پهلو نهد اندر لحد</p></div>
<div class="m2"><p>هم به ساعت از بهشتش بالش و بستر برند</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>مرد را باید شهادت چونکه باشد باک نیست</p></div>
<div class="m2"><p>گرو را اندر به چین سوی لحد میزر برند</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>تا نباشی غافل و دایم همی ترسی ز حق</p></div>
<div class="m2"><p>گر همی خواهی که چون ایمان ترا بر سر برند</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>گر ندادی حق خبر هرگز کرا بودی گمان</p></div>
<div class="m2"><p>کز جهان چون بلعمی را نزد حق کافر برند</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>عالم آمد این سخن مخصوص فردا روز حشر</p></div>
<div class="m2"><p>عالمان بی‌عمل از کرد خود کیفر برند</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>یک پرستار و یکی عالم که در دوزخ برند</p></div>
<div class="m2"><p>همچنان باشد که از جاهل دوصد کشور برند</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>حسرت آن را کی بود کز دخمه زی دوزخ رود</p></div>
<div class="m2"><p>حسرت آن را کش به دوزخ از سر منبر برند</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>منظر و کاشانه پر نقش و نگارست مر ترا</p></div>
<div class="m2"><p>چون بمیری هم بر آن کاشانه و منظر برند</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>اشتر و استر فزون کردن سزاوار است اگر</p></div>
<div class="m2"><p>بار عصیان ترا بر اشتر و استر برند</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>مضمر آمدن مردن هر یک ولی وقت شدن</p></div>
<div class="m2"><p>نسخهٔ قسمت همه یکبارگی مظهر برند</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>مرد عالم را سوی دوزخ شدن چونان بود</p></div>
<div class="m2"><p>چونکه ترکی را به سوی خوان خنیاگر برند</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>مضمر آمد مردن هر یک ولی مضمر بهست</p></div>
<div class="m2"><p>بانگ خیزد از جهان گر جان ما مضمر برند</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>مرد نابینا اگر در ره بساود با کسی</p></div>
<div class="m2"><p>عیب دارند و ورا خصمان سوی داور برند</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>باز اگر بینا بساود منکری باشد درو</p></div>
<div class="m2"><p>شاید این معروف رازی جبر آن منکر برند</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>این سخن بر ما پدید آید به ما بر آن زمان</p></div>
<div class="m2"><p>کز برای حشرمان فردا سوی محشر برند</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>عاصیا هین زار بگری زان که فردا روز حشر</p></div>
<div class="m2"><p>عاصیان را سوی فردوس برین کمتر برند</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>ظالمان را حشر گردانند با آب نیاز</p></div>
<div class="m2"><p>عادلان را زی امیرالمومنین عمر برند</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>عالمان را در جنان با غازیان سازند جای</p></div>
<div class="m2"><p>ساقیان را در سقر نزدیک رامشگر برند</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>ای سنایی این چنین غافل مباش و باز گرد</p></div>
<div class="m2"><p>کآفتابت را به زودی هم سوی خاور برند</p></div></div>