---
title: >-
    قصیدهٔ شمارهٔ ۴۸ - در مدح بهرامشاه
---
# قصیدهٔ شمارهٔ ۴۸ - در مدح بهرامشاه

<div class="b" id="bn1"><div class="m1"><p>عقل کل در نقش روی دلبرم حیران بماند</p></div>
<div class="m2"><p>جان ز جانی توبه کرد آنک بر جانان بماند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جان ز جان کردست شست آن گه ز خاک پای او</p></div>
<div class="m2"><p>جان پیوندیش رفت و جان جاویدان بماند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>صبح پیش روی او خندید و بر خورشید چرخ</p></div>
<div class="m2"><p>نور صادق بی لب و دندان از آن خندان بماند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نقش بند عقل و جان را پیش نقش روی او</p></div>
<div class="m2"><p>دست در زیر زنخ انگشت در دندان بماند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عشق چون دولت به پیش روی او بی غم نشست</p></div>
<div class="m2"><p>کفر چون ایمان به پیش روی او عریان بماند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کفر و ایمان از نشان زلف و رخسار ویست</p></div>
<div class="m2"><p>زان نشان روز و شب در کفر و در ایمان بماند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عقل با آن سراندازی به میدان رخش</p></div>
<div class="m2"><p>در خم زلفین او چون گوی در چوگان بماند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از برای رغم من گویی ازین میدان حسن</p></div>
<div class="m2"><p>عیسی مریم برفت و موسی عمران بماند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آتش جانان گریبان‌گیر جان آمد از آنک</p></div>
<div class="m2"><p>آنهمه تر دامنی در چشمهٔ حیوان بماند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گفتمی کن رنگ با مرجان چه ماند با لبش</p></div>
<div class="m2"><p>نی غلط کردم ز خجلت رنگ با مرجان بماند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نیست صبرم از میانش تا چو ذات خود مگر</p></div>
<div class="m2"><p>بر میانم چون میانش والله ار همیان بماند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>زخم خوار خویش را بی زخم خود مگذار از آنک</p></div>
<div class="m2"><p>خوار گردد پتک کوبنده که از سندان بماند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>عاقبت از دشنهٔ مژگانش روی اندر کشید</p></div>
<div class="m2"><p>عافیت در سلسلهٔ زلفینش در زندان بماند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بهتر آن تا خاکپایش را به دست آرد مگر</p></div>
<div class="m2"><p>چرخ را هرچند جنبش بود سرگردان بماند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>عقل و جان در خدمت آن بارگه رفتند لیک</p></div>
<div class="m2"><p>عقل کارافزای رفت و جان جان افشان بماند</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>هر چه خواهی گو همی فرمای کاندر ذات ما</p></div>
<div class="m2"><p>قایل فرمان برفت و قابل فرمان بماند</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>گر قماری کرد جان با او بجانی هم ز جان</p></div>
<div class="m2"><p>لاجرم در ما ز دانش مایه صد چندان بماند</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گوهر جان و جهان ذات سنایی را ازوست</p></div>
<div class="m2"><p>گرد می زو ماند ذاتش بی مکان و کان بماند</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>تا نگیرد مرغ مر مرغ سنایی را ز بیم</p></div>
<div class="m2"><p>لاجرم چون مرغ عیسی روز از آن پنهان بماند</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>تا جمال قهر و لطفش سایه بر عالم فکند</p></div>
<div class="m2"><p>شیر در بستان فنا شد شیر در پستان بماند</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>زلف شیطانیش گر دل برد گو بر باک نیست</p></div>
<div class="m2"><p>منت ایزد را که جان در مدحت سلطان بماند</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>خسرو خسرو نسب بهرامشه سلطان شرق</p></div>
<div class="m2"><p>آنکه بهرام فلک در سطوتش حیران بماند</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ملک علت ناکرا خوش خوش ازین عیسی پاک</p></div>
<div class="m2"><p>درد رفت الحمدلله و آنچه درمان آن بماند</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>تا شدش معلوم حکم آیت احسان و عدل</p></div>
<div class="m2"><p>شد نهان چون جور بخل و عدل چون احسان بماند</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بر فلک بینی که کیوان رتبتی دارد ولیک</p></div>
<div class="m2"><p>از پی ایوان این شه چرخ خود کیوان بماند</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>به گراید رایت رایش بسوی عاطفت</p></div>
<div class="m2"><p>زین سبب را خان و خوان خانه بر اخوان بماند</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>چون گشاید دست و دل در عدل و در احسان به خلق</p></div>
<div class="m2"><p>بستهٔ احسان و عدلش جملهٔ انسان بماند</p></div></div>