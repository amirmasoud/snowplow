---
title: >-
    قصیدهٔ شمارهٔ ۱۳۲ - در نکوهش حرص و هوی و هوس
---
# قصیدهٔ شمارهٔ ۱۳۲ - در نکوهش حرص و هوی و هوس

<div class="b" id="bn1"><div class="m1"><p>ای همیشه دل به حرص و آز کرده مرتهن</p></div>
<div class="m2"><p>داده یکباره عنان خود به دست اهرمن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هیچ نندیشی که آخر چون بود فرجام کار</p></div>
<div class="m2"><p>اندر آن روزی که خواهد بود عرض ذوالمنن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر پی حاجت نگردی بر پی حجت مپوی</p></div>
<div class="m2"><p>ور سر میدان نداری طعنه بر مردان مزن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یا ز بی آبی چو خار از خیرگی دیده مدوز</p></div>
<div class="m2"><p>یا ز رعنایی چو گل بر تن بدران پیرهن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر کلیمی سحر فرعون هوا را نیست کن</p></div>
<div class="m2"><p>ور خلیلی غیرت اغیار را در هم شکن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همت عالی بباید مرد را در هر دو کون</p></div>
<div class="m2"><p>تا کند قصر مشید ربع و اطلال و دمن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بگذر از گفتار ما و من که لهوست و مجاز</p></div>
<div class="m2"><p>عاشق مجبور را زیبا نباشد ما و من</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>باز را دست ملوک از همت عالی‌ست جای</p></div>
<div class="m2"><p>جغد را بوم خراب از طبع دون شد مستکن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کی شناسد قیمت و مقدار در بی معرفت</p></div>
<div class="m2"><p>کی شناسد قدر مشک آهوی خر خیز و ختن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ناسزایان را ستودن بیکران از بهر طمع</p></div>
<div class="m2"><p>گسترانیدی به جد و هزل طومار سخن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از پی آن تا یکی گوهر به دست آرد مگر</p></div>
<div class="m2"><p>ننگری تا چند مایه رنج بیند کوهکن</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نه ز رنج کوه کندن رنج طاعت هست بیش</p></div>
<div class="m2"><p>نه کمست از کان که گنج بهشت ذوالمنن</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>در ازل خلاق چون تن را و دل را آفرید</p></div>
<div class="m2"><p>راحت و آرام دل ننهاد جز در رنج تن</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>دعوی ایمان کنی و نفس را فرمان بری</p></div>
<div class="m2"><p>با علی بیعت کنی و زهر پاشی بر حسن</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گر خداجویی چرا باشی گرفتار هوا</p></div>
<div class="m2"><p>گر صمد خواهی چرا باشی طلبکار وثن</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>هیچ کس نستود و نپرستید دو معبود را</p></div>
<div class="m2"><p>هیچ کس نشنود روز و شب قرین در یک وطن</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>خرمن خود را به دست خویشتن سوزیم ما</p></div>
<div class="m2"><p>کرم پیله هم به دست خویشتن دوزد کفن</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ناز دنیا کی شود با آز عقبا مجتمع</p></div>
<div class="m2"><p>رنج حرث و زرع چه بود پیش نسرین و سمن</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>از پی محنت گرفتاریم در حبس ابد</p></div>
<div class="m2"><p>نز پی راحت بود محبوس روح اندر بدن</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>صدق و معنی گر همی خواهی که بینی هر دوان</p></div>
<div class="m2"><p>سوز دل بنگر یکی مر شمع را اندر لگن</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>نیست جز اخلاص مر درد قطیعت را دوا</p></div>
<div class="m2"><p>نیست جز تسلیم مر تیر بلیت را مجن</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>از صف هستی گریز اندر مصاف نیستی</p></div>
<div class="m2"><p>در مصاف نیستی هرگز نبیند کس شکن</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ور همی خواهی که پوشی تن به تشریف هدی</p></div>
<div class="m2"><p>دام خود کامی چو گمراهان به گرد خود متن</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>صدق و معنی باش و از آواز و دعوی باز گرد</p></div>
<div class="m2"><p>رایض استاد داند شیههٔ زاغ از زغن</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>آنکه در باغ بلا سرو رضا کارد همی</p></div>
<div class="m2"><p>چون من و تو کی کند دل بسته در سرو چمن</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>با سر پر فضله گویی فضل خود قسم منست</p></div>
<div class="m2"><p>خویشتن را نیک دیدستی به چشم خویشتن</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>باش تا ظن خبر عین عیان گردد ترا</p></div>
<div class="m2"><p>باش تا ثعبان مرگت باز بگشاید دهن</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>در دیار تو نتابد ز آسمان هرگز سهیل</p></div>
<div class="m2"><p>گر همی باید سهیلت قصد کن سوی یمن</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>ایمنی از نازکی باشد تنی را کو بود</p></div>
<div class="m2"><p>با لبی چون ناردانه قامتی چون نارون</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>باش تا اعضای خود بر خود گوا یابی به حق</p></div>
<div class="m2"><p>باش تا در کف نهندت نامهٔ سر و علن</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>دانی آن گه کاین رعونت بود خواب بی‌هشان</p></div>
<div class="m2"><p>دانی آن گه کاین ترفع بود باد بادخن</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>هست اجل چون چنبر و ما چون رسن سر تافته</p></div>
<div class="m2"><p>گر چه باشد بس دراز آید سوی چنبر رسن</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>تا ترا در دل چو قارون گنجها باشد ز آز</p></div>
<div class="m2"><p>چند گویی از اویس و چند پویی در قرن</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>ای سنایی بر سنای عافیت بی ناز باش</p></div>
<div class="m2"><p>چند بر گفتار بی کردار باشی مفتتن</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>گر کنی زین پس به جز توحید و جز وعظ امتحان</p></div>
<div class="m2"><p>ز امتحان اخروی بی شک بمانی ممتحن</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>در نمایش و آزمایش چون نکوتر بنگری</p></div>
<div class="m2"><p>اندر آن شیر عرینی و درین اسب عرن</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>قوت معنی نداری حلقهٔ دعوی مگیر</p></div>
<div class="m2"><p>طاعت زیبا نداری تکیه بر عقبا مزن</p></div></div>