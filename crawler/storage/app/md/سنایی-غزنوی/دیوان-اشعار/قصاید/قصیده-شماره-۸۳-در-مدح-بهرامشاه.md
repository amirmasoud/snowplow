---
title: >-
    قصیدهٔ شمارهٔ ۸۳ - در مدح بهرامشاه
---
# قصیدهٔ شمارهٔ ۸۳ - در مدح بهرامشاه

<div class="b" id="bn1"><div class="m1"><p>ای خنده زنان بوس تو بر تنگ شکر بر</p></div>
<div class="m2"><p>وی طنز کنان نوش تو بر رنگ گهر بر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جان تو که باشد ز در خندهٔ او باش</p></div>
<div class="m2"><p>کز خنده شیرینت بخندد به شکر بر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر مردمک دیدهٔ عشاق زنی گام</p></div>
<div class="m2"><p>هر گه که ملک وار خرامی به گذر بر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نظارگیان رخ زیبای تو بر راه</p></div>
<div class="m2"><p>افتاده چو زلف سیهت یک به دگر بر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تو بوسه همی باری از آن لعل شکر بار</p></div>
<div class="m2"><p>در بوسه چدن دیده و جانها به اثر بر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آمیخته صورتگر خوبان بر فتنه</p></div>
<div class="m2"><p>از نطق و دهان تو عیان را به خبر بر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بنشانده به خواری خرد عافیتی را</p></div>
<div class="m2"><p>زنجیر دلاویز تو چون حلقه به در بر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ای زلف تو از آتش رخسار تو پرتاب</p></div>
<div class="m2"><p>من فتنه بر آن تافته و تافته گر بر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دیوانه بسی دارد در هر شکن و پیچ</p></div>
<div class="m2"><p>آن سلسلهٔ مشک تو بر طرف قمر بر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>یارب که همی تا چه بلا بارد هر دم</p></div>
<div class="m2"><p>ای جان پدر زلف تو بر جان پدر بر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>اندر شب و روز سر زلفین و رخ تو</p></div>
<div class="m2"><p>عمری به سر آوردم بر «بوک» و «مگر» بر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گر با خبرستی ز پی روی تو هر شب</p></div>
<div class="m2"><p>غیرت بزمی بر فلک خیره نگر بر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>سرو و گل تو تازه بدانند که هستند</p></div>
<div class="m2"><p>آن جسته و این رستهٔ این دیدهٔ تر بر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>آتش زده‌ای در دل عشاق ز خشکی</p></div>
<div class="m2"><p>آبی نه کسی را ز تو بر روی جگر بر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>مانند دل سخت سیاه تو از آنست</p></div>
<div class="m2"><p>هم بوسه و هم گریهٔ حاجی به حجر بر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ای نقش دل انگیز ترا از قبل انس</p></div>
<div class="m2"><p>بنگاشته روح‌القدس از عشق به پر بر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>در زینت و در رنگ کلاه و کمر خویش</p></div>
<div class="m2"><p>زحمت چه کشی در طلب گوهر و زر بر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>از اشک من و رنگ رخ من ببر ای ترک</p></div>
<div class="m2"><p>بعضی به کله بر زن و بعضی به کمر بر</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>سحر تو اگر چه ز سحر سست شود سحر</p></div>
<div class="m2"><p>خندید چو صبح آمد بر نور سحر بر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چندان چه نمایی شر از آن چشم چو آهو</p></div>
<div class="m2"><p>خیرالبشر اینجا و تو مشغول به شر بر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>هان آهو کا جور مکن تا بنگویم</p></div>
<div class="m2"><p>این جور تو بر عدل شه شیر شکر بر</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>سلطان همه مشرق بهرامشه آنکو</p></div>
<div class="m2"><p>بهرام سپهرش نسزد بنده به در بر</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>فرخنده یمینی و امینی که بخندد</p></div>
<div class="m2"><p>یمنش به قضای بد و امنش به قدر بر</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>شیر فلک از بیلک او برطرف کون</p></div>
<div class="m2"><p>زانگونه گریزنده که آهو به کمر بر</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>خو کرده زبانش به در جنگ و سر گنج</p></div>
<div class="m2"><p>اندر صف مجلس به «بگیر» و به «ببر» بر</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>در بارگه حکم تقاضای یقینش</p></div>
<div class="m2"><p>آتش زده در نفس شک و نقش اگر بر</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>لفظش برسیدست بسان خرد و جان</p></div>
<div class="m2"><p>بر ذروهٔ عرش و فلک و ذره به در بر</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>صاحب خبر غیر نخواندست به سدره</p></div>
<div class="m2"><p>چون سیرت نیکوش به فهرست سیر بر</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>نظاره اگر روح ندیدست به دیده</p></div>
<div class="m2"><p>چون چهرهٔ زیباش به صحرای صور بر</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>فتنه‌ست چو خورشید پی فتنه نشانیش</p></div>
<div class="m2"><p>بهرام فلک به شه ناهید نظر بر</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>هر کس که کند قصد که تا سر بکشد زو</p></div>
<div class="m2"><p>سر گمشده بیند چو کشد دست به سر بر</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>ای تکیه گه دولت و تایید تو در ملک</p></div>
<div class="m2"><p>بر سو به خداوند و فرو سو به هنر بر</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>چون رعب تو خود نایب حشرست درین ربع</p></div>
<div class="m2"><p>کی دل دهدت تا تو نهی دل به حشر بر</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>چون عصمت و تایید الاهی سپر تست</p></div>
<div class="m2"><p>کی تکیه کنی بر زره و خود و سپر بر</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>گر رشگ برد خصم تو نشگفت گه سوز</p></div>
<div class="m2"><p>از آتش شمشیر تو بر عمر شرر بر</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>زیرا که به از عمر بود مرگ مر آنرا</p></div>
<div class="m2"><p>کز سهم دلاشوب تو باشد به خطر بر</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>هر چند که بودی ز پس پردهٔ ادبار</p></div>
<div class="m2"><p>بدخواه ترا میل به کبر و به بطر بر</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>اکنون که ترا دید ز سهم و خطر تو</p></div>
<div class="m2"><p>بارست بطر بر عدوی روز بتر بر</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>این قوت بازوی ظفر از پی آنست</p></div>
<div class="m2"><p>کز نعت تو حرزست به بازوی ظفر بر</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>ای از کف چون ابر بهاریت گه جود</p></div>
<div class="m2"><p>آن آمده بر بخل که از وی به حضر بر</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>گر ابر مدد یکدم از انگشت تو گیرد</p></div>
<div class="m2"><p>هرگز نکند بیش بخیلی به مطر بر</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>ای ذات ترا از قبل قبلهٔ دلها</p></div>
<div class="m2"><p>تدبیرگر چرخ بپرورده ببر بر</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>چون قطب تو اندر وطن خویش به نیکی</p></div>
<div class="m2"><p>آوازهٔ نام تو چو انجم به سفر بر</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>خور جود تو جوینده چو انجم به فلک بر</p></div>
<div class="m2"><p>گل مدح تو گوینده چو بلبل به شجر بر</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>رحمت شده بی امر تو زحمت به خرد بر</p></div>
<div class="m2"><p>فتنه شده بی امر تو فتنه به سهر بر</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>در کعبهٔ انصاف تو محراب دگر شد</p></div>
<div class="m2"><p>نقش سم شبدیز تو بر ماده و نر بر</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>تا حرز نفر داد تو و یاد تو باشد</p></div>
<div class="m2"><p>هرگز نرسد هیچ نفیری به نفر بر</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>امروز درین دور دریغی نخورد هیچ</p></div>
<div class="m2"><p>از عدل تو یک سوخته بر عدل عمر بر</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>بنگاشت تو گویی همه را از قلم مهر</p></div>
<div class="m2"><p>نقاش ازل نقش تو بر حسن بصر بر</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>انگشت گزان آمده نزد تو حسودت</p></div>
<div class="m2"><p>برده سر انگشت کز آتش به سقر بر</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>دولت نتواند که گشاید ز سر زور</p></div>
<div class="m2"><p>ار بند نهد دست تو بر پای قدر بر</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>گور و ملک الموت بهم بیندی از تو</p></div>
<div class="m2"><p>گر گرز زنی بر عدوی تیره گهر بر</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>در بحر گر آواز دهی جانورانش</p></div>
<div class="m2"><p>لبیک زنان پیش تو آیند به سر بر</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>هر دم فلک الاعظم ز اوج شرف خویش</p></div>
<div class="m2"><p>احسنت کند بر شرف چون تو پسر بر</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>تا نقش کند از قبل رمز حکیمان</p></div>
<div class="m2"><p>جاه خطر و چاه خطر را به سمر بر</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>بر رهگذر حاسد تو چاه و خطر باد</p></div>
<div class="m2"><p>تا ناصحت آساید با جاه و خطر بر</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>بر پشت تو بادا زره عصمت ایزد</p></div>
<div class="m2"><p>تا باد زره سازد بر روی شمر بر</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>خاک در تو باد سپهر همه شاهان</p></div>
<div class="m2"><p>تا خاک و سپهرست بزیر و به زبر بر</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>روی تو چنان تازه که گوید خرد و جان</p></div>
<div class="m2"><p>ای تازه‌تر از برگ گل تازه به بربر</p></div></div>