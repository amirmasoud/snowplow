---
title: >-
    قصیدهٔ شمارهٔ ۹۳ - در نکوهش اصحاب دعوا
---
# قصیدهٔ شمارهٔ ۹۳ - در نکوهش اصحاب دعوا

<div class="b" id="bn1"><div class="m1"><p>ای جوان زیر چرخ پیر مباش</p></div>
<div class="m2"><p>یا ز دورانش در نفیر مباش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یا برون شو ز چرخ چون مردان</p></div>
<div class="m2"><p>ورنه با ویل و وای و ویر مباش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اثر دوزخ ار نمی‌خواهی</p></div>
<div class="m2"><p>ساکن گنبد اثیر مباش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر سعیدیت آرزوست به عدن</p></div>
<div class="m2"><p>در سراپردهٔ سعیر مباش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تو ورای چهار و پنج و ششی</p></div>
<div class="m2"><p>در کف هفت و هشت اسیر مباش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در سرا ضرب عقل و نفس و فلک</p></div>
<div class="m2"><p>ناقدی باش و جز بصیر مباش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در میان غرور و وهم و خیال</p></div>
<div class="m2"><p>بستهٔ دیو بسته گیر مباش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هر دمی با گشاد نامهٔ عقل</p></div>
<div class="m2"><p>گر تو سلطان نه‌ای سفیر مباش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>منی انداز باش چون مردان</p></div>
<div class="m2"><p>گر نه‌ای زن منی پذیر مباش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گر ترا جان به وزر آلودست</p></div>
<div class="m2"><p>داروی وزر کن وزیر مباش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از برای خلاف و استبداد</p></div>
<div class="m2"><p>به سرو دنب جز بگیر مباش</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ای به گوهر و رای طبع و فلک</p></div>
<div class="m2"><p>بهر آز این چنین حقیر مباش</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>مار قانع بسی زید تو به حرص</p></div>
<div class="m2"><p>گر نه‌ای مور زود میر مباش</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>از پی خرس حرص و موش طمع</p></div>
<div class="m2"><p>گاه گوز و گهی پنیر مباش</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>«من» و «سلوی» چو هست اندر تیه</p></div>
<div class="m2"><p>در نیاز پیاز و سیر مباش</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>از کمان یافت دور گشتن تیر</p></div>
<div class="m2"><p>تو ز کژ دور شو چو تیر مباش</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>گر همی در و عنبرت باید</p></div>
<div class="m2"><p>بحرها هست در غدیر مباش</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گر خطر بایدت خطر کن جان</p></div>
<div class="m2"><p>ورنه ایمن بزی خطیر مباش</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چون ترا خاک تخت خواهد بود</p></div>
<div class="m2"><p>گو کنون تخت اردشیر مباش</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>تا ز یک وصف خلق متصفی</p></div>
<div class="m2"><p>شو فقیهی گزین فقیر مباش</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>فقه خوان لیک در جهنم جاه</p></div>
<div class="m2"><p>همچو قابوس وشمگیر مباش</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چون زفر درس و ترس با هم خوان</p></div>
<div class="m2"><p>ورنه بیهوده در زفیر مباش</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>در ره دین چو بو حنیفه ز علم</p></div>
<div class="m2"><p>چون چراغی به جز منیر مباش</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چون تو طفلی و شرع دایهٔ تست</p></div>
<div class="m2"><p>جز ازین دایه سیر شیر مباش</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>مجمع اکبر ار نخواهد بود</p></div>
<div class="m2"><p>طالب جامع کبیر مباش</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ور کنون سوی کعبه خواهی رفت</p></div>
<div class="m2"><p>ره مخوفست بی‌خفیر مباش</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>با چنین غافلان نذر شکن</p></div>
<div class="m2"><p>جز چو پیغمبران نذیر مباش</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>از پی ذکر بر صحیفهٔ عمر</p></div>
<div class="m2"><p>چون نکو نه‌ای دبیر مباش</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>با تو در گورتست علم و عمل</p></div>
<div class="m2"><p>منکر «منکر» و «نکیر» مباش</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>پاس پیوسته دار بر در حق</p></div>
<div class="m2"><p>کاهلانه «بجه» «بگیر» مباش</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>خار خارت چو نیست در ره او</p></div>
<div class="m2"><p>پس در آن کوی خیر خیر مباش</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>همه دل باش و آگهی نیاز</p></div>
<div class="m2"><p>بی‌خبر بر در خبیر مباش</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>زیر بی‌آگهی کند زاری</p></div>
<div class="m2"><p>پس تو گر آگهی چو زیر مباش</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>چون قلم هر دمی فدا کن سر</p></div>
<div class="m2"><p>لیک از بن شکر بی‌صریر مباش</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>چون به پیش تو نیست یوسف تو</p></div>
<div class="m2"><p>پس چو یعقوب جز ضریر مباش</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>ای سنایی تو بر نظارهٔ خلق</p></div>
<div class="m2"><p>در سخن فرد و بی‌نظیر مباش</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>در زحیری ز سغبهٔ گفتن</p></div>
<div class="m2"><p>گفت بگذار و در زحیر مباش</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>در هوای صفا چو بوتیمار</p></div>
<div class="m2"><p>دردت ار هست گو صفیر مباش</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>با قرارست نور دیدهٔ سر</p></div>
<div class="m2"><p>چشم سر گو: برو قریر مباش</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>شکر کن زان که شرع و شعرت هست</p></div>
<div class="m2"><p>خرت ار نیست گو شعیر مباش</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>گر چه خصمت فرزدق ست به هجو</p></div>
<div class="m2"><p>تو به پاداش او جریر مباش</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>خود نقیریست کل عالم و تو</p></div>
<div class="m2"><p>در نقار از پی نقیر مباش</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>از پی یوسف کسان به غرض</p></div>
<div class="m2"><p>گاه بشرا و گه بشیر مباش</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>همه بر کشتهای تشنه ز قحط</p></div>
<div class="m2"><p>ابر باش و به جز مطیر مباش</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>هر کجا پای عاشقی‌ست روان</p></div>
<div class="m2"><p>باد کشتیش باش و قیر مباش</p></div></div>