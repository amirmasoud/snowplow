---
title: >-
    قصیدهٔ شمارهٔ ۱۳۸ - در مدح محمد ترکین بغراخان
---
# قصیدهٔ شمارهٔ ۱۳۸ - در مدح محمد ترکین بغراخان

<div class="b" id="bn1"><div class="m1"><p>چرخ نارد به حکم صدر دوران</p></div>
<div class="m2"><p>جان نزاید به سعی چار ارکان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در زمین از سخا و فضل و هنر</p></div>
<div class="m2"><p>چون محمد تکین بغراخان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آنکه شد تا سخاش پیدا گشت</p></div>
<div class="m2"><p>بخل در دامن فنا پنهان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آنکه از بیم خنجرش دشمن</p></div>
<div class="m2"><p>همچو خنجر شدست گنگ زبان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آنکه تا باد امن او بوزید</p></div>
<div class="m2"><p>غرق عفوست کشتی عصیان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آنکه بر شید و شیر نزد کفش</p></div>
<div class="m2"><p>جود بخلست و پردلی بهتان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در یمینش نهادهٔ دعوی</p></div>
<div class="m2"><p>در یقینش نیتجهٔ برهان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مرده با زخم پای او زفتی</p></div>
<div class="m2"><p>زنده با جود دست او احسان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از پی چشم زخم بر در جود</p></div>
<div class="m2"><p>کرده شخص نیاز را قربان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ای ز تاثیر حرمت گهرت</p></div>
<div class="m2"><p>یافته از زمانه خلق امان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>فلک جود را کفت انجم</p></div>
<div class="m2"><p>نامهٔ جاه را دلت عنوان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>زیر امر تو نقش چار گهر</p></div>
<div class="m2"><p>زیر قدر تو جرم هفت ایوان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>دل کفیده ز فکرت تو یقین</p></div>
<div class="m2"><p>دم بریده ز خاطر تو گمان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ابرو تیری به بخشش و کوشش</p></div>
<div class="m2"><p>شید و شیری به مجلس و میدان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>تا بپیوست نهی تو بر عقل</p></div>
<div class="m2"><p>عقلها را گسسته شد فرمان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>از پی کین نحس سخت بکوفت</p></div>
<div class="m2"><p>پای قدر تو تارک کیوان</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>دید چون کبر و همتت بگذاشت</p></div>
<div class="m2"><p>کبر و همت پلنگ شیر ژیان</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بر یک انگشت همتت تنگست</p></div>
<div class="m2"><p>خاتم نه سپهر سرگردان</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>به مکانی رسید همت تو</p></div>
<div class="m2"><p>کز پس آن پدید نیست مکان</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>شمت جودت ار بر ابر عقیم</p></div>
<div class="m2"><p>بوزد خیزد از گهر طوفان</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>باد حزم تو گر بر ابر زند</p></div>
<div class="m2"><p>بر زمین ناید از هوا باران</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>آب عزم تو گر به کوه رسد</p></div>
<div class="m2"><p>بر هوا بر رود چو نار و دخان</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>هر که در فر سایهٔ کف تست</p></div>
<div class="m2"><p>ایمنست از نوائب حدثان</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>رو که روشن بتست جرم فلک</p></div>
<div class="m2"><p>رو که خرم بتست طبع جهان</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چه عجب گر ز گوهر تو کند</p></div>
<div class="m2"><p>فخر بر شام و مکه ترکستان</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>گر چه زین پیش بر طوایف ترک</p></div>
<div class="m2"><p>کرد رستم ز پردلی دستان</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>گر بدیدیت بوسها دادی</p></div>
<div class="m2"><p>بر ستانهٔ تو رستم دستان</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>ای ز دل سود حرص را مایه</p></div>
<div class="m2"><p>وی ز کف درد آز را درمان</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>عورتی ام بکرده از شنگی</p></div>
<div class="m2"><p>تیغ بسیار مرد را افسان</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بر همه مهتران فگنده رکاب</p></div>
<div class="m2"><p>وز همه لیتکان کشیده عنان</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>با مهان بوده همچو ماه قرین</p></div>
<div class="m2"><p>وز کهان همچو گبر کرده کران</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>هر که زین طایفه مرا دیدی</p></div>
<div class="m2"><p>شدی از لرزه همچو باد وزان</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>آخر این لیتک کتاب فروش</p></div>
<div class="m2"><p>برسانیده کار بنده به جان</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>آنچنان کون فروش کاون بخش</p></div>
<div class="m2"><p>و آنچنان گنده ریش گنده دهان</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>و آنچنان سرد پوز گنده بروت</p></div>
<div class="m2"><p>و آنچنان کون فراخک کشخان</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>آنچنان بادسار خاک انبوی</p></div>
<div class="m2"><p>آنچنان باد ریش و خاک افشان</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>آن درم سنگکی که برناید</p></div>
<div class="m2"><p>از گرانی به یک جهان میزان</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>بی‌نواتر ز ابرهای تموز</p></div>
<div class="m2"><p>سرد دم‌تر ز بادهای خزان</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>در همه دیده‌ها چو کاه سبک</p></div>
<div class="m2"><p>بر همه طبعها چو کوه گران</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>بی‌خرد لیتکی و بد خصلت</p></div>
<div class="m2"><p>بی‌ادب مردکی و بی‌سامان</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>باد بی‌حمیتانه در سبلت</p></div>
<div class="m2"><p>نام بی‌دولتانه در دیوان</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>جای عقلش گرفته باد و بروت</p></div>
<div class="m2"><p>آب رویش بخورده خاک هوان</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>چون سگ و گره برده از غمری</p></div>
<div class="m2"><p>آبروی از برای پارهٔ نان</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>دل و تن چون تن و دل غربال</p></div>
<div class="m2"><p>سر و بن چون بن و سر و بنگان</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>کرده بر کون خویش سیم سره</p></div>
<div class="m2"><p>کرده بر کیر خویش عمر زیان</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>بی‌زبان بوده و شده تازی</p></div>
<div class="m2"><p>خوشه‌چین بوده و شده دهقان</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>سخت بیهوده گوی چون فرعون</p></div>
<div class="m2"><p>نیک بسیار خوار چون ثعبان</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>زده جامه برای من صابون</p></div>
<div class="m2"><p>کرده سبلت ز عشق من سوهان</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>چنگ در دل چو عاشق مفلس</p></div>
<div class="m2"><p>دست بر کون چو مفلس عریان</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>در شکمش ز نوعها علت</p></div>
<div class="m2"><p>در دو چشمش ز جنسها یرقان</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>پر کدو دانه گردد ار بنهی</p></div>
<div class="m2"><p>کپه بر کون او چو با تنگان</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>تیز سیصد قرابه در ریشش</p></div>
<div class="m2"><p>با چنین عشق و با چنین پیمان</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>گاه گوید دعات گویم من</p></div>
<div class="m2"><p>اوفتم زان حدیث در خفقان</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>زان که هرگز نخواست کس از کس</p></div>
<div class="m2"><p>به دعا گادن ای مسلمانان</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>نکنم بی‌درم جماعش اگر</p></div>
<div class="m2"><p>دهد ایزد بهشت بی‌ایمان</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>درم آمد علاج عشق درم</p></div>
<div class="m2"><p>کوه ریشا چه سود ازین و از آن</p></div></div>