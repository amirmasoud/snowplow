---
title: >-
    قصیدهٔ شمارهٔ ۳۸
---
# قصیدهٔ شمارهٔ ۳۸

<div class="b" id="bn1"><div class="m1"><p>تا بت من قصد خرابات کرد</p></div>
<div class="m2"><p>نفی مرا شاهد اثبات کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با قدح و بلبله تسبیح کرد</p></div>
<div class="m2"><p>با دف و طنبور مناجات کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آن خدمات من دل سوخته</p></div>
<div class="m2"><p>مستی او دوش مکافات کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نغمهٔ او هست مرا نیست کرد</p></div>
<div class="m2"><p>بیدق او شاه مرا مات کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا که به من داد و گفت:«خذ»</p></div>
<div class="m2"><p>اغلب انفاس مرا هات کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آنکه همی دعوی بر هر کسی</p></div>
<div class="m2"><p>روز و شب از راه کرامات کرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>حال سنایی دل اهل خرد</p></div>
<div class="m2"><p>خاک گمان بر سر طامات کرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>با دل و با دیدهٔ چرخ فلک</p></div>
<div class="m2"><p>دال دل خویش مباهات کرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دیدهٔ بردوخته چون برگشاد</p></div>
<div class="m2"><p>راز دل خویش مقامات کرد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بحر محیط او به یکی دم بخورد</p></div>
<div class="m2"><p>پس بشد و قصد سماوات کرد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دست به هم بر زد و ناگه به شوق</p></div>
<div class="m2"><p>زان همه شب دوش لباسات کرد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بست در صومعه و خویش را</p></div>
<div class="m2"><p>چاکر و شاگرد خرابات کرد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>کشف که داند که کند آنکه او</p></div>
<div class="m2"><p>فضل برو سید سادات کرد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ماند سنایی را در دل هوس</p></div>
<div class="m2"><p>صومعه پر هزل و خرافات کرد</p></div></div>