---
title: >-
    قصیدهٔ شمارهٔ ۲۳ - در جواب قصیدهٔ علی‌بن هیصم
---
# قصیدهٔ شمارهٔ ۲۳ - در جواب قصیدهٔ علی‌بن هیصم

<div class="b" id="bn1"><div class="m1"><p>سنایی کنون با ضیا و سناست</p></div>
<div class="m2"><p>که بر وی ز سلطان سنت ثناست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بدین مدح بر وی ز روح‌القدس</p></div>
<div class="m2"><p>همه تهنیت مرحبا مرحباست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگر خاطرش را به خط خطیر</p></div>
<div class="m2"><p>همی عالم عقل خواند سزاست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>که جز عالم عقل نبود بلی</p></div>
<div class="m2"><p>که بر وی چنو خواجه‌ای پادشاست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>علی‌بن هیصم که این هفت حرف</p></div>
<div class="m2"><p>سه روح و چهار اسطقسات ماست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سه حرفست نامش که در مرتبت</p></div>
<div class="m2"><p>سه روحست آن نطق و حس و نماست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زه‌ای واعظ صلب همچون کلیم</p></div>
<div class="m2"><p>که وعظ تو کوران دین را عصاست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به وعظت اگر مبتدع نگرود</p></div>
<div class="m2"><p>همان وعظ بر جان او اژدهاست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کسی کو الف نیست با آل تو</p></div>
<div class="m2"><p>همه ساله چون لام پشتش دوتاست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در اقلیم ادراک احیای او</p></div>
<div class="m2"><p>خرد را و جان را ریاست ریاست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تو فوق همه عالمانی به علم</p></div>
<div class="m2"><p>که این فوق در علم بی‌منتهاست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>خصال و جمال تو در چشم عقل</p></div>
<div class="m2"><p>همه صورت و سیرت مصطفاست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>همه صیت و صوت امامان دین</p></div>
<div class="m2"><p>به پیش کمال و کلامت صداست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>تو از فوق و جسم و جهت برتری</p></div>
<div class="m2"><p>که فوق تو نقش خیالات ماست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ز دیوان خلق تو مر خلق را</p></div>
<div class="m2"><p>همه کنیت و طبعشان بوالوفاست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>به تصحیف آن مذهبم کرده‌ای</p></div>
<div class="m2"><p>که تصحیف آن مصحف اصفیاست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>مرا ماه خواندی درستست از آنک</p></div>
<div class="m2"><p>تو مهری و از مهر مه را ضیاست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چگویم که کار همه خلق را</p></div>
<div class="m2"><p>همه منشا از حضرت «من تشا»ست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>تو دانی که بر درگه لایزال</p></div>
<div class="m2"><p>در برترین الاهی رضاست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>به من مقعد صدق گفتی هری ست</p></div>
<div class="m2"><p>هری کیست کاین نام بر من سزاست</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>که جان و تنم معدن مدح تست</p></div>
<div class="m2"><p>گرش مقعد صدق خوانی رواست</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>خط و شعر تو دید چشم و دلم</p></div>
<div class="m2"><p>چه جای خط و شعر چین و ختاست</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>نفسهای روحانیان را کسی</p></div>
<div class="m2"><p>اگر شعر و خط خواند از وی خطاست</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ز جزو تو آن شربها خورد جان</p></div>
<div class="m2"><p>که خود عقل کلی از آن ناشتاست</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>فلک در شگفت از تو گر چند او</p></div>
<div class="m2"><p>بر از آتش و آب و خاک و هواست</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>که در فضل و در لفظ و در رزم و بزم</p></div>
<div class="m2"><p>علی هیصم‌ست و علی مرتضاست</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>قضای ثنای چو تو مهتری</p></div>
<div class="m2"><p>مرا هم ز تایید رسم و قضاست</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>مرا این تفضل که خلق تو کرد</p></div>
<div class="m2"><p>ز افضال فضل بن یحیا عطاست</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>ز سیاره‌دان آنکه سیاره‌وار</p></div>
<div class="m2"><p>به ممدود و مقصود از وی رواست</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>گرم جان ندادی به تشریف خویش</p></div>
<div class="m2"><p>مرا این شرف از کجا خواست خاست</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>که چون من خسی را ز چون تو کسی</p></div>
<div class="m2"><p>چنین زینت و رتبت و کبریاست</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>اگر چند باران ز ابرست لیک</p></div>
<div class="m2"><p>ز دریا فراموش کردن خطاست</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>ثنا و ثواب جزیل و جمیل</p></div>
<div class="m2"><p>برو بیش ازیرا که او مقتداست</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>تو دانی که از حضرت مصطفا</p></div>
<div class="m2"><p>برین گفتهٔ من فرشته گواست</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>تو شرعی و او دین و در راه حق</p></div>
<div class="m2"><p>نه آن زین نه این زان زمانی جداست</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>تو و او چنانید کن صدر گفت</p></div>
<div class="m2"><p>دو دست‌ست الله را هر دو راست</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>من ار آیم ار نی همی دان که جان</p></div>
<div class="m2"><p>ز خاک درت با قبای بقاست</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>چه تشویر دارم چو دانم که این</p></div>
<div class="m2"><p>ز تقدیر قادر نه تقصیر ماست</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>چه ترسم چو از جان و ایمان تو</p></div>
<div class="m2"><p>به «ما لم یشا» «لم یکن» عذر خواست</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>محالست اینجا دعا کز محل</p></div>
<div class="m2"><p>زمین تو خود آسمان دعاست</p></div></div>