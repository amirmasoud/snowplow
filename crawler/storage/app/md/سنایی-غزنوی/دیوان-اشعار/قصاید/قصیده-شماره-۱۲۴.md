---
title: >-
    قصیدهٔ شمارهٔ ۱۲۴
---
# قصیدهٔ شمارهٔ ۱۲۴

<div class="b" id="bn1"><div class="m1"><p>بر بساز کم زنان خود را بر آن مهتر نهیم</p></div>
<div class="m2"><p>گر دغا بازد کسی ما مهره در ششدر نهیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پاکبازانیم ما را نه جهاز و نه گرو</p></div>
<div class="m2"><p>گر حریفی زر نهد ما جان به جای زر نهیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در دو کونم نیست از معلوم حالی یک درم</p></div>
<div class="m2"><p>با چنین افلاس خود را نام سر دفتر نهیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون خطا از سامری بینیم در هنگام کار</p></div>
<div class="m2"><p>غایت سستی بود گر جرم بر آزر نهیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر سراندازی کند با ما درین ره یار ما</p></div>
<div class="m2"><p>ما ز سر بنهیم سودا بر خط او سر نهیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همتی داریم عالی در ره دیوانگی</p></div>
<div class="m2"><p>درد چون از علم زاید جهل را بر در نهیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فتنهٔ خویشیم هر یک در طریق عاشقی</p></div>
<div class="m2"><p>جامه‌مان گازر درد تاوانش بر زرگر نهیم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کی پسندد عاقل از ما در مقام زیرکی</p></div>
<div class="m2"><p>کاسب تازی مانده بی که جو به پیش خر نهیم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر یکی دیگ از هوای هستی خود بشکنیم</p></div>
<div class="m2"><p>از طریق نیستی صد دیگ دیگر برنهیم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز آتش معنی مگر مردان ره را خوی دهیم</p></div>
<div class="m2"><p>تا ز روی تربیت تر دامنان را تر نهیم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گر حریفان زان مکان لامکان پی برگرند</p></div>
<div class="m2"><p>ما برین معلوم نامعلوم دستی بر نهیم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>آیت غم از برای عاشقان منزل شدست</p></div>
<div class="m2"><p>دست بر حنظل زنیم و پای بر شکر نهیم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>مصر اگر فرعون دارد ما به کنعان بس کنیم</p></div>
<div class="m2"><p>سیم گر سلمان رباید دیده در بوذر نهیم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>دست همت چنبر گردون خرسندی کنیم</p></div>
<div class="m2"><p>پای خرسندی ز حکمت بر سر اختر نهیم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>پای رای نفس را از تیغ شرعی پی کنیم</p></div>
<div class="m2"><p>پای معنی از سپهر و اختران برتر نهیم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ماه اگر نیکو نتابد ابر در پیشش کشیم</p></div>
<div class="m2"><p>رهبر ار گمراه گردد سنگها رهبر نهیم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>گوش زی فرمان صاحب حرمت و دولت نهیم</p></div>
<div class="m2"><p>پای را بر شاهراه شرع پیغمبر نهیم</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>عقل را اگر نقل باید گو چو مردان کسب کن</p></div>
<div class="m2"><p>گر گنه از کور زاید جرم چون بر کر نهیم</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>خواجهٔ جانیم از آن از خودپرستی رسته‌ایم</p></div>
<div class="m2"><p>نفس اگر میزر بجوید حکمش از معجر نهیم</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>هر خسی واقف نگردد بر نهاد کار ما</p></div>
<div class="m2"><p>غایب و حاضر چه داند ما کجا محضر نهیم</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>تا بدین دلق ای برادر در سنایی ننگری</p></div>
<div class="m2"><p>عطر از عود آن گهی آید که بر آذر نهیم</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>دیدهٔ بیدار باید تا بینند نظم او</p></div>
<div class="m2"><p>تیر همت را به پای عقل کافی بر نهیم</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بر سر معلوم خود خاک قناعت گستریم</p></div>
<div class="m2"><p>راه چون معلوم باشد نک به دیده بر نهیم</p></div></div>