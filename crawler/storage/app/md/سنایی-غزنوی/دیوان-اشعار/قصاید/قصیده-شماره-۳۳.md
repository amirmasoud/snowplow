---
title: >-
    قصیدهٔ شمارهٔ ۳۳
---
# قصیدهٔ شمارهٔ ۳۳

<div class="b" id="bn1"><div class="m1"><p>ای چو عقل از کل موجودات فرد</p></div>
<div class="m2"><p>وی جوان از تو سپهر سالخورد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خاکبوسان سر کوی تواند</p></div>
<div class="m2"><p>روشنان کارگاه لاجورد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پاسبانان در و بام تواند</p></div>
<div class="m2"><p>چرخ و خورشید و مه گیتی نورد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا سنایی کیست کاید بر درت</p></div>
<div class="m2"><p>مجد کو تا گویدش کز راه برد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای همه دریا چه خواهی کردنم</p></div>
<div class="m2"><p>وی همه گردون چه خواهی کرد گرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نام او میدان و نقش او بسی</p></div>
<div class="m2"><p>کز حکیمان او زیاد اندر نبرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زان به خدمت نامدم زیرا بود</p></div>
<div class="m2"><p>پیش بینا مرد عریان روی زرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کز ضعیفی دیدگان شب پره‌ست</p></div>
<div class="m2"><p>کو بماندست از رخ خورشید فرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ساختم جلابی از جان جانت را</p></div>
<div class="m2"><p>وز دم خرسندی آنرا کرده سرد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چون بزرگان نوش کن جلاب جان</p></div>
<div class="m2"><p>می بخردان مان و گرد می‌مگرد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ورد جوید روز مجلس مرد عقل</p></div>
<div class="m2"><p>بوالهوس جوید به مجلس خارورد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>زان که مقلوب سنایی یانس است</p></div>
<div class="m2"><p>گر نگیرم انس با من بد مگرد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>انس گیرم باژگونه خوانیم</p></div>
<div class="m2"><p>خویشتن را باژگونه کس نکرد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گر تن و جانم به خدمت نامدند</p></div>
<div class="m2"><p>عذرشان بپذیر کمتر کن نبرد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>صدر تو چرخست و تن را بال سست</p></div>
<div class="m2"><p>روی تو مهرست و جان را چشم درد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>جان من آزاد کن تا عقل من</p></div>
<div class="m2"><p>هر زمان گوید: زهی آزادمرد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>تازه گردانم بنا جستن که باد</p></div>
<div class="m2"><p>تازه از جان بیخ و شاخ و برگ و ورد</p></div></div>