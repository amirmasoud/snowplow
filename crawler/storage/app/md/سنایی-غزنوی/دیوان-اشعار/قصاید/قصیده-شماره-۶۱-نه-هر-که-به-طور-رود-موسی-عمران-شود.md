---
title: >-
    قصیدهٔ شمارهٔ ۶۱ - نه هر که به طور رود موسی عمران شود
---
# قصیدهٔ شمارهٔ ۶۱ - نه هر که به طور رود موسی عمران شود

<div class="b" id="bn1"><div class="m1"><p>تا بد و نیک جهان پیش تو یکسان نشود</p></div>
<div class="m2"><p>کفر در دیدهٔ انصاف تو پنهان نشود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا چو بستان نشوی پی سپر خلق ز شوق</p></div>
<div class="m2"><p>دلت از شوق ملک روضه و بستان نشود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا مهیا نشوی حال تو نیکو نشود</p></div>
<div class="m2"><p>تا پریشان نشوی کار به سامان نشود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا تو در دایرهٔ فقر فرو ناری سر</p></div>
<div class="m2"><p>خانهٔ حرص تو و آز تو ویران نشود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا تو خوشدل نشوی در پی دلبر نرسی</p></div>
<div class="m2"><p>تا که از جان نبری جفت تو جانان نشود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر که در مصر شود یوسف چاهی نبود</p></div>
<div class="m2"><p>و آنکه بر طور شود موسی عمران نشود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تو چنان والهٔ نانی ز حریصی که اگر</p></div>
<div class="m2"><p>جان شود خالی از جسم تو یک نان نشود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>صد نمازت بشود باک نداری به جوی</p></div>
<div class="m2"><p>چست می‌باشی تا خدمت سلطان نشود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>راه مخلوقان گیری و نیندیشی هیچ</p></div>
<div class="m2"><p>دیو بر تخت سلیمان چو سلیمان نشود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دامن عشق نگهدار که در دیدهٔ عقل</p></div>
<div class="m2"><p>سرو آزاد تو جز خار مغیلان نشود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مرد باید که سخندان بود و نکته شناس</p></div>
<div class="m2"><p>تا چو می‌گوید از آن گفته پشیمان نشود</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گر فرشته بزند راه تو شیطان تو اوست</p></div>
<div class="m2"><p>دیو دیوان تو با دیو به زندان نشود</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بی خود از هیچ به کفر آیی و این نیست عظیم</p></div>
<div class="m2"><p>با خود از هیچ به دین آیی و درمان نشود</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>دست بتگر ببر و زینت بتخانه بسوز</p></div>
<div class="m2"><p>گر بت نفس و هوای تو مسلمان نشود</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>کم زن بد دل یک لخت به عذرا نزند</p></div>
<div class="m2"><p>عاشق مصلح در مصلحت جان نشود</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>خانهٔ سودا ویران کن و آسان بنشین</p></div>
<div class="m2"><p>حامل عاقل با زیره به کرمان نشود</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>خواجه گر مردی زین نکته برون آی و مپای</p></div>
<div class="m2"><p>صوفی صافی در خدمت دهقان نشود</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گر تو رنگ آوری و طیره شوی غم نخورم</p></div>
<div class="m2"><p>سنگ اگر لعل شود جز به بدخشان نشود</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>در سراپردهٔ فقر آی و ز اوباش مترس</p></div>
<div class="m2"><p>سینهٔ جاهل جز غارت شیطان نشود</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>شربت از دست سنایی خور و ایمن می‌باش</p></div>
<div class="m2"><p>زان که گاه طمع او بر در خصمان نشود</p></div></div>