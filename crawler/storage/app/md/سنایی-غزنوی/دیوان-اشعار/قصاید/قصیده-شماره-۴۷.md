---
title: >-
    قصیدهٔ شمارهٔ ۴۷
---
# قصیدهٔ شمارهٔ ۴۷

<div class="b" id="bn1"><div class="m1"><p>کرد رفت از مردمان اندر جهان اقوال ماند</p></div>
<div class="m2"><p>همعنان شوخ چشمی در جهان آمال ماند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از فصیحان و ظریفان پاک شد روی زمین</p></div>
<div class="m2"><p>در جهان مشتی بخیل کور و کر و لال ماند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در معنی در بن دریای عزلت جای ساخت</p></div>
<div class="m2"><p>وز پی دعوی به روی آبها آخال ماند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>صدرها از عالمان و منصفان یکسر تهیست</p></div>
<div class="m2"><p>صدر در دست بخیل و ظالم و بطال ماند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عدل گم گشت و نمی‌یابد کسی از وی نشان</p></div>
<div class="m2"><p>ظلم جای وی گرفت و چند ماه و سال ماند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عدل نوشروان و جور معتصم افسانه شد</p></div>
<div class="m2"><p>وز بزرگیشان به چشم مردمان تمثال ماند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>رفت سید از جهان و چند مشکل کرد حل</p></div>
<div class="m2"><p>بوحنیفه رفت و زو در گرد عالم قال ماند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نیست گویی در جهان جز فیلی از اصحاب فیل</p></div>
<div class="m2"><p>شد نجاشی وز فسونش چند گون اشکال ماند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شد ملک محمود و ماند اندر زبانها مدح اوی</p></div>
<div class="m2"><p>عنصری رفت و ازو گرد جهان امثال ماند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خاک شد کسری و از هر دل برون شد مهر او</p></div>
<div class="m2"><p>در مداین از بنای قصر او اطلال ماند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هر گهی بانگی برآید گرد شهر از مردمان</p></div>
<div class="m2"><p>آه و دردا و دریغا خواجه رفت و مال ماند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>رفت کدبانو کلید اندر کف نوروز داد</p></div>
<div class="m2"><p>رفت خواجه ده به دست زیرک جیپال ماند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>یک گره را خانه‌ها در غیبت و وزر و بزه</p></div>
<div class="m2"><p>یک گره را گنجها بر طاعت و اهمال ماند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>زین سپس شاید سنایی گر نگویی هیچ مدح</p></div>
<div class="m2"><p>زان کجا ممدوح تو خوالی پز و بقال ماند</p></div></div>