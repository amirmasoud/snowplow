---
title: >-
    قصیدهٔ شمارهٔ ۱۳۷ - معروفی بود زن سلیطه‌ای داشت او را به قاضی برده بود و رنج می‌نمود در حق وی گوید
---
# قصیدهٔ شمارهٔ ۱۳۷ - معروفی بود زن سلیطه‌ای داشت او را به قاضی برده بود و رنج می‌نمود در حق وی گوید

<div class="b" id="bn1"><div class="m1"><p>ویحک ای پردهٔ پرده‌در در ما نگران</p></div>
<div class="m2"><p>بیش از این پردهٔ ما پیش هر ابله مدران</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یا مدر یا چو دریدی چو لئیمان بمدوز</p></div>
<div class="m2"><p>یا مخوان یا چو بخواندی چو بخیلان بمران</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جای نوری تو و ما از تو چو تاریک دلان</p></div>
<div class="m2"><p>آب گویی تو و ما از تو پر آتش جگران</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ماهت ار نور دهد تری آبست درو</p></div>
<div class="m2"><p>مشک ار بوی دهد خشکی نارست در آن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شیشهٔ بادهٔ روشن ندهی تا نکنی</p></div>
<div class="m2"><p>روز ما تیره‌تر از کارگه شیشه‌گران</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شرم دار ای فلک آخر مکن این بی رسمی</p></div>
<div class="m2"><p>تا کی از پرورش و تربیت بد سیران</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از تو و گردش چرخت چه هنر باشد پس</p></div>
<div class="m2"><p>چون تهی دست بوند از تو همه پر هنران</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عمر ما طعمهٔ دوران تو شد بس باشد</p></div>
<div class="m2"><p>نیز هر ساعتمان شربت هجران مخوران</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هر که یکشب ز بر زن بود از روی مراد</p></div>
<div class="m2"><p>سالی از نو شود از جلمهٔ زیر و زبران</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خواستم از پی راحت زنی آخر از تو</p></div>
<div class="m2"><p>آن بدیدم که نبینند همه بی‌خبران</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>این ز تو در خورد ای مادر زندانی زای</p></div>
<div class="m2"><p>ما به زندان و تو از دور به ما در نگران</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مر پسر را به تو امید کجا ماند پس</p></div>
<div class="m2"><p>همه چون فعل تو این باشد بر بی‌پدران</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چون به زن کردنی این رنج همی باید دید</p></div>
<div class="m2"><p>اینت اقبال که دارند پس امروز غران</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ما غلام کف دستیم بس اکنون که ز عجز</p></div>
<div class="m2"><p>مانده‌اند از پس یک ماده برینگونه بران</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نه تویی یوسف یعقوب مکن قصه دراز</p></div>
<div class="m2"><p>یوسفان را نبود چاره ازین بد گهران</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>یوسف مصری ده سال ز زن زندان دید</p></div>
<div class="m2"><p>پس ترا کی خطری دارند این بی‌خطران</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>آنکه با یوسف صدیق چنین خواهد کرد</p></div>
<div class="m2"><p>هیچ دانی چکند صحبت او با دگران</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>حجرهٔ عقل ز سودای زنان خالی کن</p></div>
<div class="m2"><p>تا به جان پند تو گیرند همه پر عبران</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بند یک ماده مشو تا بتوانی چو خروس</p></div>
<div class="m2"><p>تا بوی تاجور و پیش رو تاجوران</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>خاصه اکنون که جهان بی‌خردان بگرفتند</p></div>
<div class="m2"><p>بی‌خرد وار بزی تا نبوی سرد و گران</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>کار چون بی‌خردی دارد و بی‌اصلی و جهل</p></div>
<div class="m2"><p>وای پس بر تو و آباد برین مختصران</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>طالع فاجری و ماجری امروز قویست</p></div>
<div class="m2"><p>هر که امروز بر آنست بر آنست برآن</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>مر که پستان میان پای نداد او را شیر</p></div>
<div class="m2"><p>نیست امروز میان جهلا او ز سران</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>هر که لوزینهٔ شهوت نچشیدست ز پس</p></div>
<div class="m2"><p>نیست در مجلس این طایفه از پیشتران</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>آنکه بودست چو گردون به گه خردی کوژ</p></div>
<div class="m2"><p>لاجرم هست درین وقت ز گردون سپران</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بی‌نفیرست کسی کش نفر از جهل و خطاست</p></div>
<div class="m2"><p>جهد کن تا نبوی از نفر بی‌نفران</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>روزگاریست که جز جهل و خیانت نخرند</p></div>
<div class="m2"><p>داری این مایه و گر نه خر ازین کلبه بران</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>سپر تیر زمان دیدهٔ شوخست و فساد</p></div>
<div class="m2"><p>جهد کن تات نبیند فلک از پی سپران</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>شاید ار دیدهٔ آزاده گهر بار شود</p></div>
<div class="m2"><p>چون شدستند همه بی‌گهران با گهران</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>باز دانش چو همی صید نگیرد ز اقبال</p></div>
<div class="m2"><p>پیشش از خشم در اطراف ممالک مپران</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>معنی اصل و وفایش مجوی از همه کس</p></div>
<div class="m2"><p>زان که هستند ز بستان وفا بی‌ثمران</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>اندرین وقت ز کس راه صیانت مطلب</p></div>
<div class="m2"><p>که سر راه برانند همه راهبران</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>بی‌خبروار در این عصر بزی کز پی بخت</p></div>
<div class="m2"><p>گوی اقبال ربودند همه بی‌خبران</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>با چنین قول و چنین فعل که این دونان راست</p></div>
<div class="m2"><p>رشک بر می‌آیدم ای خواجه ز کوران و کران</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>چون سرشت همه رعنایی و بر ساختگیست</p></div>
<div class="m2"><p>مذهب خانه خدادار تو چون مستقران</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>پس چو از واقعهٔ حادثه کس نیست مصون</p></div>
<div class="m2"><p>همچو بی‌اصل تو دون باش نه از مشتهران</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>عاجزیت از شرف با پدری بود ار نه</p></div>
<div class="m2"><p>دهر و ایام کیت دیدی چون بی‌ظفران</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>هر که چون بی‌بصران صحبت دونان طلبد</p></div>
<div class="m2"><p>سخت بسیار بلاها کشد از بی‌بصران</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>پای کی دارد با صحبت تو سفلهٔ دون</p></div>
<div class="m2"><p>چون نه ای خیره سر و در نسب خیره سران</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>مردمی را چو نگیرد همی این تازی اسب</p></div>
<div class="m2"><p>یارب ای بار خداییت جهانی ز خران</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>وقت آنست که در پیشگه میخانه</p></div>
<div class="m2"><p>ترس و لاباس بسازی چو همه بی‌فکران</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>اسب شادی و طرب در صف ایام در آر</p></div>
<div class="m2"><p>مگر از زحمت اسبت برمند این گذران</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>مرکب امر خدایست چو ترکیب تنت</p></div>
<div class="m2"><p>بخرابیش درین مرتع خاکی مچران</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>ای دل ای دل چو ز فضل و ز شرف حیرانیست</p></div>
<div class="m2"><p>ز اهل فضل و شرف و عقل گران گیر گران</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>دست در گردن ایام در آریم از عقل</p></div>
<div class="m2"><p>پای برداریم از سیرت نیکو نظران</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>دین فروشیم چو این قوم جزین می‌نخرند</p></div>
<div class="m2"><p>مایه سازیم هم از همت و خوی دگران</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>کام جوییم و نبندیم دل اندر یک بند</p></div>
<div class="m2"><p>زان که اینست همه ره روش با خطران</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>همت خویش ورای فلک و عقل نهیم</p></div>
<div class="m2"><p>که برون فلکند از ما فرزانه تران</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>خود که باشد فلک بادرو آب نهاد</p></div>
<div class="m2"><p>خود که باشند درو اینهمه صاحب سفران</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>کار حکم ازلی دارد و نقش تقدیر</p></div>
<div class="m2"><p>که نوشتست همه بوده و نابوده در آن</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>جرم از اجرام ندانند به جز کوردلان</p></div>
<div class="m2"><p>طمع از چرخ ندارند مگر خیره‌سران</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>زان که از قاعدهٔ قسمت در پردهٔ راز</p></div>
<div class="m2"><p>چرخ پیمایان دورند و ستاره شمران</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>همه بادست حدیث فلک و سیر نجوم</p></div>
<div class="m2"><p>باده دارد همه خوشی و دگر باده‌خوران</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>دولت نو چو همی می‌ندهد چرخ کهن</p></div>
<div class="m2"><p>ما و بادهٔ کهن و مطرب و نو خط پسران</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>گرچه با زیب و فریم از خرد و اصل و وفا</p></div>
<div class="m2"><p>گرد میخانه در آییم چو بی زیب و فران</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>عیش خود تلخ چه داریم به سودای زنان</p></div>
<div class="m2"><p>ما و سیمین زنخان خوش و زرین کمران</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>جان ببخشیم به یاران نکو از سر عشق</p></div>
<div class="m2"><p>سیم خوردن چه خطر دارد با سیمبران</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>خام باشد ترشی در رخ و شهوت در دل</p></div>
<div class="m2"><p>چون بود کیسه پر از سیم و جهان پر شکران</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>رنگ آن قوم نگیریم به یک صحبت از آنک</p></div>
<div class="m2"><p>پشت اسلام نکردند بنا بر عمران</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>همه اندر طلب مستی بی‌عقل و دلان</p></div>
<div class="m2"><p>همه اندر طرب هستی بی‌سیم و زران</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>آنچنان قاعده سازیم ز شادی که شود</p></div>
<div class="m2"><p>از پس ما سمر خوشتر صاحب سمران</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>هیچ تاوان نبود در دو جهان بر من و تو</p></div>
<div class="m2"><p>چون برین گونه گذاریم جهان گذران</p></div></div>