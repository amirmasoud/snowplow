---
title: >-
    قصیدهٔ شمارهٔ ۱۷۷ - در مدح خواجه ایرانشاه
---
# قصیدهٔ شمارهٔ ۱۷۷ - در مدح خواجه ایرانشاه

<div class="b" id="bn1"><div class="m1"><p>ای ز آواز و جمال تو جهان پر طربی</p></div>
<div class="m2"><p>وز پی هر دو شده جان و دلم در طلبی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چشم و گوش همه از لحن و رخت پر در و گل</p></div>
<div class="m2"><p>پس چرا قسمتم از هر دو عنا و تعبی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر ز آهن دل من در کف تو گشت چو موم</p></div>
<div class="m2"><p>ور چو یعقوب ز عشق تو کنم واهربی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ناید از خود عجبم زان که به آواز و به روی</p></div>
<div class="m2"><p>داری از یوسف و داوود پیمبر نسبی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آنچه با این دل من چشم چو بادام تو کرد</p></div>
<div class="m2"><p>نکند هرگز با مهره کف بوالعجبی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پس دل خون شدهٔ تافتهٔ تیرهٔ من</p></div>
<div class="m2"><p>کو همی در دو صفت داشت ز زلفت حسبی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شد مگر حلقه‌ای از زلف تو و شاید از آنک</p></div>
<div class="m2"><p>خون اگر مشک شود طبع ندارد عجبی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>صد دل خون به در یک شکن زلف تو هست</p></div>
<div class="m2"><p>همچو عناب در آویخته اندر عنبی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تا همی رقص کند در چمن عشرت و عیش</p></div>
<div class="m2"><p>ماه رقاص نهادست سپهرت لقبی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شدم از طمع وصال تو چو یک برگ از کاه</p></div>
<div class="m2"><p>تا بر آن سیم تو دیدم زد و بیجاده لبی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بند بندم همه بگشاد چو تو زی از ماه</p></div>
<div class="m2"><p>تا تو بر تارک خورشید ببستی قصبی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چاک ماندست دلم چون دل خرما تا تو</p></div>
<div class="m2"><p>چاک داری ز پس و پیش ببسته سلبی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>جان بابا مکن این کبر مبادا که به عدل</p></div>
<div class="m2"><p>روزگارت کند از رنج دل من ادبی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ابلهم خوانی و گویی که به باغ آر زرم</p></div>
<div class="m2"><p>خار ندهند تو بی‌سیم چه جویی رطبی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ابله اکنون تویی ای جان جهان کز پی زر</p></div>
<div class="m2"><p>طعنه بر من زنی اکنون و بسازی شغبی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>تو بدین پایه ندانی که چو این شعر برم</p></div>
<div class="m2"><p>از سخا کار مرا خواجه بسازد سببی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ناصح ملک شه ایران ایرانشاه آن</p></div>
<div class="m2"><p>که نزاد از نجبا دهر چنو منتجبی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>آن بزرگی که ز بس فضل و کریمی نگذاشت</p></div>
<div class="m2"><p>در مزاج فضلا از کرم خود اربی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>آن کریمی کاثر سورت خمش در کون</p></div>
<div class="m2"><p>همچو نار آمد و ارواح حسودش حطبی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>آن خطیبی که به هر لحظه خطیبان فلک</p></div>
<div class="m2"><p>جمع سازند ز آثار خصالش خطبی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ای سخا از گهر چون تو پسر با شرفی</p></div>
<div class="m2"><p>وی سپهر از شرف چون تو بشر با طربی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>شجر همت تو بیخ چنان زد که نمود</p></div>
<div class="m2"><p>برترین چرخ بدان بیخ فروتر شعبی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>گر فتد قطره‌ای از رای تو بر دامن روز</p></div>
<div class="m2"><p>نگشاید پس از آن چرخ گریبان شبی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>تا دو نوک قلمت فایده دارد در ملک</p></div>
<div class="m2"><p>چرخ با چار زن از عجز بود چون عزبی</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>کسب کردی به کریمی و سخا نام نکو</p></div>
<div class="m2"><p>که نبوده به دو گیتی به ازین مکتسبی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>تا ضمیر تو سوی کلک تو راهی بگشاد</p></div>
<div class="m2"><p>بسته شد مصلحت ملک هری در قصبی</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>نردها بازد با نطع امیدت با دهر</p></div>
<div class="m2"><p>جانی از بنده و اقبال ز دستت ندبی</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>هر که او مرد بود باک ندارد ز غمی</p></div>
<div class="m2"><p>هر که او شیر بود سست نگردد به تبی</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>هر که آوازهٔ کوس و دو کری یافت به گوش</p></div>
<div class="m2"><p>کی به چشم آید او را ز یکی حبه حبی</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>به کهان جامه بسی داده‌ای این اولاتر</p></div>
<div class="m2"><p>کاین فریضه به مهان به ز چنان مستحبی</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>ای خداوند یقین دان که بر مدحت تو</p></div>
<div class="m2"><p>نیست در شاعری بنده ریا و ریبی</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>فکرت بنده چو معنی خوش آورد به دست</p></div>
<div class="m2"><p>طبع زودش بر مدح تو کند منتخبی</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>هر که را دین شود از دوستی او موجود</p></div>
<div class="m2"><p>چه زیان داردش از دشمنی بولهبی</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>حاسدان دارد و بدگوی بسی لیک همی</p></div>
<div class="m2"><p>کی مقاسات کشد بحر دمان از مهبی</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>تا حیات آید از آمیزش جانی و تنی</p></div>
<div class="m2"><p>تا تناسل بود از صحب امی و ابی</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>سببی سازش تا شاعر صدر تو بود</p></div>
<div class="m2"><p>که همی شعر مرکب نبود بی سببی</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>تا ز پیش دو ربیع آید هر گه صفری</p></div>
<div class="m2"><p>تا پس از هر دو جماد آید هر گه رجبی</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>باد حظ ولی تو ز سعادت لطفی</p></div>
<div class="m2"><p>باد قسم عدوی تو ز شقاوت غضبی</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>پای احباب تو بگشاده ز بند از شرفی</p></div>
<div class="m2"><p>دست اعدای تو بر بسته به دار از کنبی</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>تا چو تمساح بود راس و ذنب بر گردون</p></div>
<div class="m2"><p>راس عز تو مبیناد ز گردون ذنبی</p></div></div>