---
title: >-
    قصیدهٔ شمارهٔ ۱۵۱ - دعوت به آزادگی و عدالت‌خواهی
---
# قصیدهٔ شمارهٔ ۱۵۱ - دعوت به آزادگی و عدالت‌خواهی

<div class="b" id="bn1"><div class="m1"><p>ای سنایی خویشتن را بی سر و سامان مکن</p></div>
<div class="m2"><p>مایهٔ انفاس را بر عمر خود تاوان مکن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از برای آنکه تا شیطان ز تو شادان شود</p></div>
<div class="m2"><p>دیدهٔ رضوان و شخص خویش را گریان مکن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دینت را نیکو نداری دیو را دعوت مساز</p></div>
<div class="m2"><p>عقل را چاکر نباشی نفس را فرمان مکن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از برای آنکه تا شاهین شود همکاسه‌ات</p></div>
<div class="m2"><p>سینهٔ صد صعوهٔ بیچاره را بریان مکن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یونسان تنت را خلعت نمی‌بخشی مبخش</p></div>
<div class="m2"><p>یوسفان وقت را در چاه و در زندان مکن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از برای کرکسان باطن اماره را</p></div>
<div class="m2"><p>سینهٔ صالح مسوز و اشترش قربان مکن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از پی آن تا خر لنگ ترا پالان بود</p></div>
<div class="m2"><p>مر براق خلد را ازین خود عریان مکن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر به شیطان می‌فروشی یوسف صدیق را</p></div>
<div class="m2"><p>چون ز چاهش برکشیدی قیمتش ارزان مکن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>یوسف کنعان تن را می‌خری امروز تو</p></div>
<div class="m2"><p>یوسف ایمان خود را بیع با شیطان مکن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تا مرض را دارویی بخشی شفا را سر مبر</p></div>
<div class="m2"><p>تا عرض را جسم بخشی جسم را بی‌جان مکن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>در بلا چون روز قهر نفس روباهیت نیست</p></div>
<div class="m2"><p>در خلا دعوی ز فر رستم دستان مکن</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>صلح کردستیم با تو این بگیر و آن مبخش</p></div>
<div class="m2"><p>بیت مقدس بر میار و کعبه را ویران مکن</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>سر به سر کردیم با تو نی ز ما و نی ز تو</p></div>
<div class="m2"><p>چادر مریم مدزد و شیث را مهمان مکن</p></div></div>