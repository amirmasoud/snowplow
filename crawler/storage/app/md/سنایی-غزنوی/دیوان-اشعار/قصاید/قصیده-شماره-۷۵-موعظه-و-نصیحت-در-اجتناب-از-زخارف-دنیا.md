---
title: >-
    قصیدهٔ شمارهٔ ۷۵ - موعظه و نصیحت در اجتناب از زخارف دنیا
---
# قصیدهٔ شمارهٔ ۷۵ - موعظه و نصیحت در اجتناب از زخارف دنیا

<div class="b" id="bn1"><div class="m1"><p>طلب ای عاشقان خوش رفتار</p></div>
<div class="m2"><p>طرب ای شاهدان شیرین‌کار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا کی از خانه هین ره صحرا</p></div>
<div class="m2"><p>تا کی از کعبه هین در خمار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زین سپس دست ما و دامن دوست</p></div>
<div class="m2"><p>بعد از این گوش ما و حلقهٔ یار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در جهان شاهدی و ما فارغ</p></div>
<div class="m2"><p>در قدح جرعه‌ای و ما هشیار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خیز تا ز آب روی بنشانیم</p></div>
<div class="m2"><p>گرد این خاک تودهٔ غدار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پس به جاروب «لا» فرو روبیم</p></div>
<div class="m2"><p>کوکب از صحن گنبد دوار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ترکتازی کنیم و در شکنیم</p></div>
<div class="m2"><p>نفس رنگی مزاج را بازار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>وز پی آنکه تا تمام شویم</p></div>
<div class="m2"><p>پای بر سر نهیم دایره‌وار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تا ز خود بشنود نه از من و تو</p></div>
<div class="m2"><p>لمن الملک واحد القهار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ای هواهای تو هوا انگیز</p></div>
<div class="m2"><p>وی خدایان تو خدای آزار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>قفس تنگ چرخ و طبع و حواس</p></div>
<div class="m2"><p>پر و بالت گسست از بن و بار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گرت باید کزین قفس برهی</p></div>
<div class="m2"><p>باز ده وام هفت و پنج و چهار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>آفرینش نثار فرق تو اند</p></div>
<div class="m2"><p>بر مچین خون خسان ز راه نثار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چرخ و اجرام ساکنان تو اند</p></div>
<div class="m2"><p>تو از ایشان طمع مدار مدار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>حلقه در گوش چرخ و انجم کن</p></div>
<div class="m2"><p>تا دهندت به بندگی اقرار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ورنه بر چارسوی کون و فساد</p></div>
<div class="m2"><p>گاه بیمار بین و گه تیمار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>گاهت اندر مزارعت فکند</p></div>
<div class="m2"><p>جرم کیوان چو خوک در شد یار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گه کند اورمزدت از سر زهد</p></div>
<div class="m2"><p>زین جهان سیر و زان جهان ناهار</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>گاه بر بنددت به تهمت تیغ</p></div>
<div class="m2"><p>دست بهرام چون قلم زنار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گاه مهرت نماید از سر کین</p></div>
<div class="m2"><p>مر ترا در خیال زر عیار</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>گاه ناهید لولی رعنا</p></div>
<div class="m2"><p>کندت باد سار و باده گسار</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>گه کند تیر چرخت از سر امن</p></div>
<div class="m2"><p>چون کمان گوشه کشته و زه‌وار</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>گه کند ماه نقشت اندر دل</p></div>
<div class="m2"><p>در خزر هندو در حبش بلغار</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>گه ترا بر کند اثیر از تو</p></div>
<div class="m2"><p>تا تهی زو شوی چو دود شرار</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>گاه بادت کند ز آز و نیاز</p></div>
<div class="m2"><p>روح پر نار و روی چون گلنار</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>گاه آب لئیم دون همت</p></div>
<div class="m2"><p>جاهل و کاهلت کند به بحار</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>گاه خاک فسرده از تاثیر</p></div>
<div class="m2"><p>بر تو ویران کند ده و آثار</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>با چنین چار پای‌بند بود</p></div>
<div class="m2"><p>سوی هفت آسمان شدن دشوار</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>چند از این آب و خاک و آتش و باد</p></div>
<div class="m2"><p>این دی و تیر و آن تموز و بهار</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بسکه نامرد و خشک مغزت کرد</p></div>
<div class="m2"><p>بوی کافور و مشک و لیل و نهار</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>عمر امسال و پار ضایع کرد</p></div>
<div class="m2"><p>هر که در بند یار ماند و دیار</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>دولتی مردی ار نپریدست</p></div>
<div class="m2"><p>مرغ امسالت از دریچهٔ پار</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>شیب گردی به لفظ تازی ریش</p></div>
<div class="m2"><p>قیر گردی به لفظ ترکی قار</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>برگذر زین جهان غرچه فریب</p></div>
<div class="m2"><p>در گذر زین رباط مردم‌خوار</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>کلبه‌ای کاندرو نخواهی ماند</p></div>
<div class="m2"><p>سال عمرت چه ده چه صد چه هزار</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>رخت برگیر ازین خراب که هست</p></div>
<div class="m2"><p>بام سوراخ و ابر طوفان بار</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>از ورای خرد مگوی سخن</p></div>
<div class="m2"><p>وز فرود فلک مجوی قرار</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>خویشتن را به زیر پی بسپر</p></div>
<div class="m2"><p>چون سپردی به دست حق بسپار</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>بود بگذار زان که در ره فقر</p></div>
<div class="m2"><p>تن حصارست و بود قفل حصار</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>نشود در گشاده تا تو به دم</p></div>
<div class="m2"><p>بر نیاری ز قفل و پره دمار</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>بود تو شرع بر تواند داشت</p></div>
<div class="m2"><p>زان که آن روشنست و بود تو تار</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>دین نیاید به دست تابودت</p></div>
<div class="m2"><p>بر یمین و یسار یمین و یسار</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>نه فقیری چو دین به دنیا کرد</p></div>
<div class="m2"><p>مر ترا پایمزد و دست افزار</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>نه فقیهی چو حرص و شهوت کرد</p></div>
<div class="m2"><p>مر ترا فرع جوی و اصل گذار</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>ره رها کرده‌ای از آنی گم</p></div>
<div class="m2"><p>عز ندانسته‌ای از آنی خوار</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>مشک و پشکت یکیست تا تو همی</p></div>
<div class="m2"><p>ناک ده را ندانی از عطار</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>دل به صد پاره همچو ناری از آنک</p></div>
<div class="m2"><p>خلق را سر شمرده‌ای چو انار</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>کار اگر رنگ و بوی دارد و بس</p></div>
<div class="m2"><p>حبذا چین و فرخا فرخار</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>دعوی دل مکن که جز غم حق</p></div>
<div class="m2"><p>نبود در حریم دل دیار</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>ده بود آن نه دل که اندر وی</p></div>
<div class="m2"><p>گاو و خر باشد و ضیاع و عقار</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>نیست اندر نگارخانهٔ امر</p></div>
<div class="m2"><p>صورت و نقش مومن و کفار</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>زان که در قعر بحرالاالله</p></div>
<div class="m2"><p>لا نهنگی ست کفر و دین او بار</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>چه روی با کلاه بر منبر</p></div>
<div class="m2"><p>چه شوی با زکام در گلزار</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>تر مزاجی مگرد در سقلاب</p></div>
<div class="m2"><p>خشک مغزی مپوی در تاتار</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>خود کلاه و سرت حجاب تو اند</p></div>
<div class="m2"><p>چه فزایی تو بر کله دستار</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>کله آن گه نهی که در فتدت</p></div>
<div class="m2"><p>سنگ در کفش و کیک در شلوار</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>علم کز تو ترا بنستاند</p></div>
<div class="m2"><p>جهل از آن علم به بود صدبار</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>آب حیوان چو شد گره در حلق</p></div>
<div class="m2"><p>زهر گشت ار چه بود نوش و گوار</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>نه بدان لعنت‌ست بر ابلیس</p></div>
<div class="m2"><p>کو نداند همی یمین ز یسار</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>بل بدان لعنت‌ست کاندر دین</p></div>
<div class="m2"><p>علم داند به علم نکند کار</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>دوری از علم تا ز شهوت و خشم</p></div>
<div class="m2"><p>جانت پر پیکرست و پر پیکار</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>نبرند از تو تشنگی و کنند</p></div>
<div class="m2"><p>این دهان گنده و آن جگر افگار</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>تشنهٔ جاه و زر مباش که هست</p></div>
<div class="m2"><p>جاه و زر آب پار گین و بحار</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>کی درآید فرشته تا نکنی</p></div>
<div class="m2"><p>سگ ز در دور و صورت از دیوار</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>کی در احمد رسی در صدیق</p></div>
<div class="m2"><p>عنکبوتی تنیده بر در غار</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>پرده بردار تا فرود آید</p></div>
<div class="m2"><p>هودج کبریا به صفهٔ بار</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>با بخیلی مجوی ره که نبود</p></div>
<div class="m2"><p>هیچ دینار مالکی دین دار</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>مالک دین نشد کسی که نشد</p></div>
<div class="m2"><p>از سر جود مالک دینار</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>سرخرویی ز آب جوی مجوی</p></div>
<div class="m2"><p>زان که زردند اهل دریا بار</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>گر چه از مال و گندم و یونجه</p></div>
<div class="m2"><p>هم خزینه‌ت پرست و هم انبار</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>بس تفاخر مکن که اندر حشر</p></div>
<div class="m2"><p>گندمت گژدمست و مالت مار</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>مال دادی به باد چون تو همی</p></div>
<div class="m2"><p>گل به گوهری خری و خر به خیار</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>دولت آن را مدان که دادندت</p></div>
<div class="m2"><p>بیش از ابنای جنس استظهار</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>تا تو را یار دولتست نه‌ای</p></div>
<div class="m2"><p>در جهان خدای دولت یار</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>چون ترا از تو پاک بستانند</p></div>
<div class="m2"><p>دولت آن دولتست و کار آن کار</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>چون دو گیتی دو نعل پای تو شد</p></div>
<div class="m2"><p>بر سر کوی هر دو را بگذار</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>در طریق رسول دست آویز</p></div>
<div class="m2"><p>بر بساط خدای پای افشار</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>پاک شو بر سپهر همچو مسیح</p></div>
<div class="m2"><p>گشته از جان و عقل و تن بیزار</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>همچو نمرود قصد چرخ مکن</p></div>
<div class="m2"><p>با دوتا کرکس و دوتا مردار</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>کز دو بال سریش کرده نشد</p></div>
<div class="m2"><p>هیچ طرار جعفر طیار</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>عقل در کوی عشق ره نبرد</p></div>
<div class="m2"><p>تو از آن کور چشم چشم مدار</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>کاندر اقلیم عشق بی‌کارند</p></div>
<div class="m2"><p>عقلهای تهی رو پر کار</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>کی توان گفت سر عشق به عقل</p></div>
<div class="m2"><p>کی توان سفت سنگ خاره به خار</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>گر نخواهی که بر تو خندد خلق</p></div>
<div class="m2"><p>نقد خوارزم در عراق میار</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>راه توحید را به عقل مپوی</p></div>
<div class="m2"><p>دیدهٔ روح را به خار مخار</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>زان که کردست قهر الاالله</p></div>
<div class="m2"><p>عقل را بر دو شاخ لا بردار</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>به خدای ار کسی تواند بود</p></div>
<div class="m2"><p>بی‌خدا از خدای برخوردار</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>هر که از چوب مرکبی سازد</p></div>
<div class="m2"><p>مرکب آسوده‌دان و مانده سوار</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>نشود دل چو تیر تا نشوی</p></div>
<div class="m2"><p>بی‌زبان چون دهانهٔ سوفار</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>تا زبانت خمش نشد از قول</p></div>
<div class="m2"><p>ندهد بار نطقت ایزد بار</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>تا ز اول خمش نشد مریم</p></div>
<div class="m2"><p>در نیامد مسیح در گفتار</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>گرت باید که مرکزی گردی</p></div>
<div class="m2"><p>زیر این چرخ دایره کردار</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>پای بر جای باش و سرگردان</p></div>
<div class="m2"><p>چون سکون و تحرک پرگار</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>در هوای زمانه مرغی نیست</p></div>
<div class="m2"><p>چمن عشق را چو بوتیمار</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>زو کس آواز او بنشنودی</p></div>
<div class="m2"><p>گر نبودی میان تهی مزمار</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>قاید و سایق صراط‌الله</p></div>
<div class="m2"><p>به ز قرآن مدان و به ز اخبار</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>جز به دست و دل محمد نیست</p></div>
<div class="m2"><p>حل و عقد خزانهٔ اسرار</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>چون دلت بر ز نور احمد بود</p></div>
<div class="m2"><p>به یقین دان که ایمنی از نار</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>خود به صورت نگر که آمنه بود</p></div>
<div class="m2"><p>صدف در احمد مختار</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>ای به دیدار فتنه چون طاووس</p></div>
<div class="m2"><p>وی به گفتار غره چون کفتار</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>عالمت غافلست و تو غافل</p></div>
<div class="m2"><p>خفته را خفته کی کند بیدار</p></div></div>
<div class="b" id="bn102"><div class="m1"><p>همه زنهار خوار دین تو اند</p></div>
<div class="m2"><p>دین به زنهارشان مده زنهار</p></div></div>
<div class="b" id="bn103"><div class="m1"><p>غول باشد نه عالم آنکه ازو</p></div>
<div class="m2"><p>بشنوی گفت و نشنوی کردار</p></div></div>
<div class="b" id="bn104"><div class="m1"><p>بر خود آنرا که پادشاهی نیست</p></div>
<div class="m2"><p>بر گیاهیش پادشا مشمار</p></div></div>
<div class="b" id="bn105"><div class="m1"><p>افسری کن نه دین نهد بر سر</p></div>
<div class="m2"><p>خواهش افسر شمار و خواه افسار</p></div></div>
<div class="b" id="bn106"><div class="m1"><p>باش وقت معاشرت با خلق</p></div>
<div class="m2"><p>همچو عفو خدای پذرفتار</p></div></div>
<div class="b" id="bn107"><div class="m1"><p>هر چه نز راه دین خوری و بری</p></div>
<div class="m2"><p>در شمارت کنند روز شمار</p></div></div>
<div class="b" id="bn108"><div class="m1"><p>بره و مرغ را بدان ره کش</p></div>
<div class="m2"><p>که به انسان رسند در مقدار</p></div></div>
<div class="b" id="bn109"><div class="m1"><p>جز بدین ظلم باشد ار بکشد</p></div>
<div class="m2"><p>بی‌نمازی مسبحی را زار</p></div></div>
<div class="b" id="bn110"><div class="m1"><p>نکند عشق نفس زنده قبول</p></div>
<div class="m2"><p>نکند باز موش مرده شکار</p></div></div>
<div class="b" id="bn111"><div class="m1"><p>راه عشاق کسپرد عاشق</p></div>
<div class="m2"><p>آه بیمار کشنود بیمار</p></div></div>
<div class="b" id="bn112"><div class="m1"><p>از ره ذوق عشق بشناسی</p></div>
<div class="m2"><p>آه موسا ز راه موسیقار</p></div></div>
<div class="b" id="bn113"><div class="m1"><p>بیخ کنرا نشاند خرسندی</p></div>
<div class="m2"><p>شاخ او بی‌نیاز آرد بار</p></div></div>
<div class="b" id="bn114"><div class="m1"><p>عاشقان را ز عشق نبود رنج</p></div>
<div class="m2"><p>دیدگان را ز نور نبود نار</p></div></div>
<div class="b" id="bn115"><div class="m1"><p>جان عاشق نترسد از شمشیر</p></div>
<div class="m2"><p>مرغ محبوس نشکهد ز اشجار</p></div></div>
<div class="b" id="bn116"><div class="m1"><p>زان که بر دست عشق بازانند</p></div>
<div class="m2"><p>ملک‌الموت گشته در منقار</p></div></div>
<div class="b" id="bn117"><div class="m1"><p>گر شعار تو شعر آمده شرع</p></div>
<div class="m2"><p>چکنی صبح کاذب اشعار</p></div></div>
<div class="b" id="bn118"><div class="m1"><p>روی بنمود صبح صادق شرع</p></div>
<div class="m2"><p>خاک زن بر جمال شعر و شعار</p></div></div>
<div class="b" id="bn119"><div class="m1"><p>بر سر دار دان سر سرهنگ</p></div>
<div class="m2"><p>در بن چاه بین تن بندار</p></div></div>
<div class="b" id="bn120"><div class="m1"><p>تا نه بس روزگار خواهی دید</p></div>
<div class="m2"><p>هم سپه مرده هم سپهسالار</p></div></div>
<div class="b" id="bn121"><div class="m1"><p>وارهان خویش را که وارسته‌ست</p></div>
<div class="m2"><p>خر وحشی ز نشتر بیطار</p></div></div>
<div class="b" id="bn122"><div class="m1"><p>هیچ بی‌چشم دیدی از سر عشق</p></div>
<div class="m2"><p>طالب شمع زیر و آینه دار</p></div></div>
<div class="b" id="bn123"><div class="m1"><p>بهر مشتی مهوس رعنا</p></div>
<div class="m2"><p>رنج بر جان و دین و دل مگمار</p></div></div>
<div class="b" id="bn124"><div class="m1"><p>ای توانگر به کنج خرسندی</p></div>
<div class="m2"><p>زین بخیلان کناره‌گیر کنار</p></div></div>
<div class="b" id="bn125"><div class="m1"><p>یک زمان زین خسان ناموزون</p></div>
<div class="m2"><p>از پی سختن تو با معیار</p></div></div>
<div class="b" id="bn126"><div class="m1"><p>ریش و دامن به دستشان چه دهی</p></div>
<div class="m2"><p>چون نه‌ای خصم و نه پذیر رفتار</p></div></div>
<div class="b" id="bn127"><div class="m1"><p>خواجگان بوده‌اند پیش از ما</p></div>
<div class="m2"><p>در عطا سخت مهر و سست مهار</p></div></div>
<div class="b" id="bn128"><div class="m1"><p>این نجیبان وقت ما همه باز</p></div>
<div class="m2"><p>راح خوارند مستراح انبار</p></div></div>
<div class="b" id="bn129"><div class="m1"><p>جمله از بخل و مبخلی سرمست</p></div>
<div class="m2"><p>همه از شر و ناکسی هشیار</p></div></div>
<div class="b" id="bn130"><div class="m1"><p>ای سنایی ازین سگان بگریز</p></div>
<div class="m2"><p>گوشه‌ای گیر ازین جهان هموار</p></div></div>
<div class="b" id="bn131"><div class="m1"><p>زین چنین خواجگان بی معنی</p></div>
<div class="m2"><p>رد افلاک و گفت بی‌کردار</p></div></div>
<div class="b" id="bn132"><div class="m1"><p>دامن عافیت بگیر و بپوش</p></div>
<div class="m2"><p>مر گریبان آز را رخسار</p></div></div>
<div class="b" id="bn133"><div class="m1"><p>میوه‌ای کان به تیر ماه رسد</p></div>
<div class="m2"><p>چه طمع داری از مه آزار</p></div></div>
<div class="b" id="bn134"><div class="m1"><p>دل ازینان ببر که بی دریا</p></div>
<div class="m2"><p>نکشد بار گیر چوبین بار</p></div></div>
<div class="b" id="bn135"><div class="m1"><p>همچنین در سرای حکمت و شرع</p></div>
<div class="m2"><p>آدمی سیر باش و مردم سار</p></div></div>
<div class="b" id="bn136"><div class="m1"><p>هان و هان تا ترا چو خود نکنند</p></div>
<div class="m2"><p>مشتی ابلیس ریزهٔ طرار</p></div></div>
<div class="b" id="bn137"><div class="m1"><p>چون تو از خمر هیچ کس نخوری</p></div>
<div class="m2"><p>کی ترا درد سر دهد خمار</p></div></div>
<div class="b" id="bn138"><div class="m1"><p>طیرهٔ چون گردی و فسرده و کج</p></div>
<div class="m2"><p>طیره از طیر گرد و از طیار</p></div></div>
<div class="b" id="bn139"><div class="m1"><p>نشود شسته جز به بی‌طمعی</p></div>
<div class="m2"><p>نقشهای گشاد نامهٔ عار</p></div></div>
<div class="b" id="bn140"><div class="m1"><p>ملک دنیا مجوی و حکمت جوی</p></div>
<div class="m2"><p>زان که این اندکست و آن بسیار</p></div></div>
<div class="b" id="bn141"><div class="m1"><p>خدمتی کز تو در وجود آمد</p></div>
<div class="m2"><p>هم ثناگوی و هم گنه پندار</p></div></div>
<div class="b" id="bn142"><div class="m1"><p>در طریقت همین دو باید ورد</p></div>
<div class="m2"><p>اول الحمد و آخر استغفار</p></div></div>
<div class="b" id="bn143"><div class="m1"><p>گر سنایی ز یار ناهموار</p></div>
<div class="m2"><p>گله‌ای کرد ازو شگفت مدار</p></div></div>
<div class="b" id="bn144"><div class="m1"><p>آبرا بین که چون همی نالد</p></div>
<div class="m2"><p>هردم از همنشین ناهموار</p></div></div>
<div class="b" id="bn145"><div class="m1"><p>بر زمین مست همچو من بنشین</p></div>
<div class="m2"><p>تا سمایی شوی سنایی وار</p></div></div>