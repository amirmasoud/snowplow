---
title: >-
    قصیدهٔ شمارهٔ ۱۱۴ - در آرزوی مرگ
---
# قصیدهٔ شمارهٔ ۱۱۴ - در آرزوی مرگ

<div class="b" id="bn1"><div class="m1"><p>کی باشد کین قفس بپردازم</p></div>
<div class="m2"><p>در باغ الاهی آشیان سازم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با روی نهفتگان دل یک دم</p></div>
<div class="m2"><p>در پردهٔ غیب عشقها بازم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کش در چمن رسول بخرامم</p></div>
<div class="m2"><p>خوش در حرم خدای بگرازم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>این چار غریب ناموافق را</p></div>
<div class="m2"><p>خشنود به سوی خانه‌ها تازم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>این حلهٔ نیمکار آدم را</p></div>
<div class="m2"><p>در کارگه کمال بطرازم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>وین دیو سرای استخوانی را</p></div>
<div class="m2"><p>در پیش سگان دوزخ اندازم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>این بام و سرای بی‌وفایان را</p></div>
<div class="m2"><p>از شحنه و شش عسس بپردازم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>باغند ولی کرام طینت را</p></div>
<div class="m2"><p>از میوه و مرغ و جوز بنوازم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کوفی و قریشی طبیعت را</p></div>
<div class="m2"><p>در بوتهٔ لطف و مهر بگدازم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>با این همه رهبران و رهرو من</p></div>
<div class="m2"><p>محرومم اگر چه محرم رازم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>با این همه دل چه مرد این کوژم</p></div>
<div class="m2"><p>با این همه پر چه مرغ این بازم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بنهم کله از سر و پس از غیرت</p></div>
<div class="m2"><p>بر هر که سرست گردن افرازم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>از جان جهول دل فرو شویم</p></div>
<div class="m2"><p>وز عقل فضول سر بپردازم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چون بال شکسته گشت بر پرم</p></div>
<div class="m2"><p>چون دست بریده گشت دریازم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گر ناز کنم بر آفرینش من</p></div>
<div class="m2"><p>فرزند خلیفه‌ام رسد نازم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چون رفت سنایی از میان بیرون</p></div>
<div class="m2"><p>آن گه سخن از سنایی آغازم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>تا کار شود مگر چو چنگ آندم</p></div>
<div class="m2"><p>کامروز چو نای بادی آوازم</p></div></div>