---
title: >-
    قصیدهٔ شمارهٔ ۷۸ - در مدح خواجه ابو نصر منصور سعید
---
# قصیدهٔ شمارهٔ ۷۸ - در مدح خواجه ابو نصر منصور سعید

<div class="b" id="bn1"><div class="m1"><p>تا چرخ برگشاد گریبان نوبهار</p></div>
<div class="m2"><p>از لاله بست دامن کهپایه‌ها ازار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چونان نمود کل اثیری اثر به کوه</p></div>
<div class="m2"><p>کاجزای او گرفت همه طرف جویبار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از اعتدال و تقویت طبع او ز خاک</p></div>
<div class="m2"><p>صد برگ گل بزاد ز یک نوک تیز خار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اکنون که پر ز برگ زمرد شد از صبا</p></div>
<div class="m2"><p>شاخی که بد چو هیکل افعی تهی ز بار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زان می‌کفد ز دیدن او دیده‌های شاخ</p></div>
<div class="m2"><p>کز خاصیت کفد ز زمرد دو چشم مار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از هجر نالش آرد بس بلبل از درخت</p></div>
<div class="m2"><p>با وصل گل برو چکند ناله‌های زار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زاید همی هوا به لطافت ز سعی چرخ</p></div>
<div class="m2"><p>آن قوتی که داد عناصر به کوهسار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>با آفتاب اگر بنتابد بروز نجم</p></div>
<div class="m2"><p>بیواسطه اگر چه نپاید بر آب نار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر به سما بهشت نهانست تا به حشر</p></div>
<div class="m2"><p>بی حشر چونکه کرد زمینش پس آشکار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بر دشت و باغ چیست پس از یاسمین و گل</p></div>
<div class="m2"><p>گردون پر ستاره و دریای پر شرار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گلزار بین سبزه پر از آب نارگون</p></div>
<div class="m2"><p>کهسار بین ز لاله پر از نار آبدار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بر شبه چنگ باز سر غنچه‌های گل</p></div>
<div class="m2"><p>بر شکل پای شیر شده پنجهٔ چنار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گر دشت خرمست چرا گرید از فراز</p></div>
<div class="m2"><p>این پردهٔ کثیف لطیف اصل تند بار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>زینجا نفیر ریزد ز آنجا نوای نای</p></div>
<div class="m2"><p>زینجا خروش عاشق و ز آنجا نشاط یار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>خلقی پر از نشاط ز دشتی تهی ز برف</p></div>
<div class="m2"><p>طبعی تهی ز غم ز درختان پر ز بار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>آن لاله فام باده‌خوران زیر شاخ گل</p></div>
<div class="m2"><p>و آن گلرخان نشاط کنان گرد لاله‌زار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بیخ زمین چو افسر شاهان پر از گهر</p></div>
<div class="m2"><p>شاخ شجر چون گوش عروسان ز گوشوار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بر هر طرف بهشتی در هر بهشت حور</p></div>
<div class="m2"><p>بر هر چمن کناری و در هر کنار یار</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>مرغی بهر درخت و چراغی بهر چمن</p></div>
<div class="m2"><p>شاهی بهر طریق و عروسی بهر کنار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گر چه ز هر درخت خوشی دید هر دماغ</p></div>
<div class="m2"><p>ور چه درین بهار بها یافت هر دیار</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>لیک از بهار خرمیی نیستی به طبع</p></div>
<div class="m2"><p>چون خلق و طبع خواجه اگر نیستی بهار</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>منصوربن سعیدبن احمد که از کرم</p></div>
<div class="m2"><p>چون نصرت و سعادت و حمدست نامدار</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>آن کز مزاج گوهر و تاثیر علم او</p></div>
<div class="m2"><p>بر نه فلک چهار گهر می‌کند نثار</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>آن خواجه‌ای که گشت ز تعجیل جود خویش</p></div>
<div class="m2"><p>چون شخص سل گرفته سوال از کفش نزار</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>یک فکر تند از پی مدحش همه سخن</p></div>
<div class="m2"><p>یک منزلند از تک جودش همه قفار</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>کرد از تف سخاوت خود همچو چوب خشک</p></div>
<div class="m2"><p>در کامهای خلق زبانهای افتخار</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>چشمی که نشر سیرت او بیند از مدیح</p></div>
<div class="m2"><p>آن چشم ایمنست بهر حال از انتشار</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>گر بنگرد به خشم سوی چرخ و آفتاب</p></div>
<div class="m2"><p>در ساعتی دو لیل بخیزد ز یک نهار</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>ای دایرهٔ نجات ز جود تو مستدیر</p></div>
<div class="m2"><p>وی مرکز حیات ز عون تو مستدار</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>رویی که یافت گرد ستانهٔ درت ز لطف</p></div>
<div class="m2"><p>هرگز شکن نگیرد چون پشت سوسمار</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>خاکی که یافت سایهٔ حزم تو زان سپس</p></div>
<div class="m2"><p>از باد کوه کن نبرد در هوا غبار</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>آبی که یافت آتش عزمت کند چو وهم</p></div>
<div class="m2"><p>در نیم لحظه چنبر افلاک را گذار</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>هرگز سپاه مرگ نیابد بدو ظفر</p></div>
<div class="m2"><p>آن کس که دارد از علم و علم تو حصار</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>مدحست طبع و فعل ترا سال و مه خورش</p></div>
<div class="m2"><p>شکرست باز عمر ترا روز شب شکار</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>شد فرش پای قدر تو گردون مستقیم</p></div>
<div class="m2"><p>شد غرق بحر دست تو کشتی انتظار</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>گویی که هست بر بشره نزد خاطرت</p></div>
<div class="m2"><p>آنها که در عروق مفاصل بود نثار</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>زنده شود به علم و به احسانت هر زمان</p></div>
<div class="m2"><p>آنرا که کشت بوالحسن از زخم ذوالفقار</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>آخر گشاد تیر علوم تو از علاج</p></div>
<div class="m2"><p>بر مرگ سوی شخص فروبست رهگذار</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>از لطف و بخشش تو چو شمس ای فلک محل</p></div>
<div class="m2"><p>وز جود و بر یافت همه خلق بر و بار</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>پرمایه‌ای چو گوهر و پر سایه‌ای چو ماه</p></div>
<div class="m2"><p>پس چونکه هست روی عدو از تو همچو قار</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>نی نی مه و گهر چه خوانم ترا چو هست</p></div>
<div class="m2"><p>هر نکته صد سپهر و هر انگشت صد بحار</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>ای چرخ را به بذل یمینت همه یمین</p></div>
<div class="m2"><p>وی خلق را به جود یسارت همه یسار</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>هستم من آن بلند که گشتم ز چرخ پست</p></div>
<div class="m2"><p>هستم من آن عزیز که ماندم ز دهر خوار</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>از جور این زمان و زمانه نهاد من</p></div>
<div class="m2"><p>یک لحظه می‌نیابد همچون زمین قرار</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>از جهل عار باشد حظم ازوست فخر</p></div>
<div class="m2"><p>وز شعر فخر زاید قسمم ازوست عار</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>هرگز نیافتم به چنین شعرهای نغز</p></div>
<div class="m2"><p>از هیچ رادمرد به صد شعر یک شعار</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>تا پنجگانه‌ایم دهند از دویست شعر</p></div>
<div class="m2"><p>روزی هزار بار دو چشمم شود چهار</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>چشمم همی ستاره از آن بارد از مژه</p></div>
<div class="m2"><p>زیرا که چون شبست برو روزگار تار</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>هستی سخن چه سود کسی را که نیستی</p></div>
<div class="m2"><p>از سر همی برآرد هر ساعتی دمار</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>شوخیست مایهٔ طمع اشعار خوش چه سود</p></div>
<div class="m2"><p>کامروز فرق کس نکند افسر از فسار</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>آنراست یمن و یسر که با قوت تمیز</p></div>
<div class="m2"><p>نشناسد او ز جهل یمین خود از یسار</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>گر کارها چنانکه بباید چنان بدی</p></div>
<div class="m2"><p>در پستی آب کی بدی و در هوا بخار</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>شاید که خاکپای تو بوسم که خود تویی</p></div>
<div class="m2"><p>مداح را به جود و به انصاف دستیار</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>مجبور بخت بد بدم از روی چاکری</p></div>
<div class="m2"><p>زان مر ترا چو دولت تو کردم اختیار</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>نشکفت اگر ز روی تو والا شوم از آنک</p></div>
<div class="m2"><p>نه تو کم از مهی و نه من کمتر از خیار</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>تخمیم بر دهنده ز مدح و ثنا و شکر</p></div>
<div class="m2"><p>در بوستان عمر خود از حکمتم به کار</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>در زینهار خویش نگهدارم از بلا</p></div>
<div class="m2"><p>ای خلق را به علم تو از مرگ زینهار</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>بودم صبور تا برسیدم به صدر تو</p></div>
<div class="m2"><p>گر چه ز خلق بود روان و دلم فگار</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>آری به زخم ماری ابوبکر صبر کرد</p></div>
<div class="m2"><p>تا لاجرم وزیر نبی گشت و یار غار</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>تا ز آتش و ز آب و ز خاک و هوا بود</p></div>
<div class="m2"><p>مر خلق را ز حکمت باری همی نگار</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>بادی چو آب و آتش و بادی چو باد و خاک</p></div>
<div class="m2"><p>در صفوت و بلندی و در لطف و در وقار</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>بادت ز سعی بخت همیشه تهی و پر</p></div>
<div class="m2"><p>از رنج تن روان و ز مقصود دل کنار</p></div></div>