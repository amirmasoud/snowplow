---
title: >-
    قصیدهٔ شمارهٔ ۲۶ - در مدح قاضی عبدالودود غزنوی
---
# قصیدهٔ شمارهٔ ۲۶ - در مدح قاضی عبدالودود غزنوی

<div class="b" id="bn1"><div class="m1"><p>آن طبع را که علم و سخاوت شعار نیست</p></div>
<div class="m2"><p>از عالمیش فخر و ز زفتیش عار نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جز چشم زخم امت و تعویذ بخل نیست</p></div>
<div class="m2"><p>جز رد چرخ و آب کش روزگار نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آن دست و آن زبان که درو نیست نفع خلق</p></div>
<div class="m2"><p>جز چون زبان سوسن و دست چنار نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>باشد چو ابر بی‌مطر و بحر بی‌گهر</p></div>
<div class="m2"><p>آن را که با جمال نکو خوی یار نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در پیش جوهری چو سفالست آن صدف</p></div>
<div class="m2"><p>کاندر میان او گهری شاهوار نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>منت خدای را که مر این هر دو وصف را</p></div>
<div class="m2"><p>جر در مزاج پیشرو دین قرار نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>قاضی‌القضاة غزنین عبدالودود آنک</p></div>
<div class="m2"><p>مر علم وجود را جز ازو پیشکار نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چرخست علم او که مر او را فساد نیست</p></div>
<div class="m2"><p>بحرست جود او که مر او را کنار نیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در بر و بحر نیست یکی صنعت از سخا</p></div>
<div class="m2"><p>کاندر بنان و طبعش از آن صدهزار نیست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>با سیرتش در آتش و آب و هوا و خاک</p></div>
<div class="m2"><p>قدر بلند و صفوت و لطف و وقار نیست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ای قدر تو رسیده بدان پرده کز علو</p></div>
<div class="m2"><p>زان پرده ز استر اثر صنع بار نیست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>آن چیست کز یقین تو آنرا مزاج نیست</p></div>
<div class="m2"><p>و آن کیست کز یمین تو آنرا یسار نیست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>دین از تو و زبانت چرا می‌شود قوی</p></div>
<div class="m2"><p>گر تو علی نه‌ای و زبان ذوالفقار نیست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>در هفت بخش عالم یک مبتدع نماند</p></div>
<div class="m2"><p>کز ذوالفقار حجت تو دلفگار نیست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>جز در چمن ولی تو چون گل پیاده کیست</p></div>
<div class="m2"><p>جز بر اجل حسود تو چون جان سوار نیست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>نزدیک علم و رای تو مه نورمند نیست</p></div>
<div class="m2"><p>در پیش حلم و سنگ تو که بردبار نیست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>آن کیست کو ندارد با تو چو تیر دل</p></div>
<div class="m2"><p>کو از سنان سنت تو سوگوار نیست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>یک تن نماند در چمن جود تو که او</p></div>
<div class="m2"><p>چون فاخته ز منت تو طوقدار نیست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ای شمس طبع کز تو جهان را گزیر نیست</p></div>
<div class="m2"><p>ای ابر دست کز تو زمین را غبار نیست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>امیدوار باز سوی صدرت آمدم</p></div>
<div class="m2"><p>از ابر و شمس کیست که امیدوار نیست</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>جز شاعران کوته‌بین را درین دیار</p></div>
<div class="m2"><p>بر بارگاه جود کریمیت بار نیست</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>آری ز نوش آتش و از لطف آب پاک</p></div>
<div class="m2"><p>رفعت به جز نصیب دخان و بخار نیست</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>لیکن زمانه ای تو و بر من ز بخت بد</p></div>
<div class="m2"><p>هر چه از زمانه آید حقا که عار نیست</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>والله که از لباس جز از روی عاریت</p></div>
<div class="m2"><p>بر فرق من عمامه و بر پا آزار نیست</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>کارم بساز از کرم امروز ای کریم</p></div>
<div class="m2"><p>هر چند کارساز به جز کردگار نیست</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>گر چه دهی وگر ندهی صله در دو حال</p></div>
<div class="m2"><p>جز گوهر ثنای من اینجا نثار نیست</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>باشد کریمی ار بدهی ورنه رای تست</p></div>
<div class="m2"><p>مر بنده را به هیچ صفت اختیار نیست</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>دانی که از زمانه جز احسان و نام نیک</p></div>
<div class="m2"><p>حقا که هر چه هست به جز مستعار نیست</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>نام نکو بمان چو کریمان ز دستگاه</p></div>
<div class="m2"><p>چون شد یقین که عمر دول پایدار نیست</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>تا دوزخ و بهشت کم از هفت و هشت نیست</p></div>
<div class="m2"><p>تا حس و طبع بیش ز پنج و چهار نیست</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>چندانت قدر باد که آن را کرانه نیست</p></div>
<div class="m2"><p>چندانت عمر باد که آن را شمار نیست</p></div></div>