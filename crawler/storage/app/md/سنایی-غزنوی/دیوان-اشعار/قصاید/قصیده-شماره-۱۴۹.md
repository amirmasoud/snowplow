---
title: >-
    قصیدهٔ شمارهٔ ۱۴۹
---
# قصیدهٔ شمارهٔ ۱۴۹

<div class="b" id="bn1"><div class="m1"><p>رحل بگذار ای سنایی رطل مالامال کن</p></div>
<div class="m2"><p>این زبان را چون زبان لاله یک دم لال کن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یک زمان از رنگ و بوی باده روح‌القدس را</p></div>
<div class="m2"><p>در ریاض قدس عنبر مغز و مرجان بال کن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زهد و صفوت یک زمان از عشق در دوزخ فگن</p></div>
<div class="m2"><p>حال و وقتت ساعتی در کار زلف و خال کن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در میان زهد کوشان خویشتن قلاش ساز</p></div>
<div class="m2"><p>در جهان می‌فروشان خویشتن ابدال کن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شاهد شیرین نخواهد زاهدان تلخ را</p></div>
<div class="m2"><p>شاهدی چون شهد خواهی رطل مالامال کن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سرو خود را گوی ای سرو از پی گلزار رخ</p></div>
<div class="m2"><p>خون روان در جویبار اکحل و قیفال کن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تو به کژی ما به خدمت چون دو دالیم از صفت</p></div>
<div class="m2"><p>یک الف را بهر الفت ردف جفتی دال کن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خاک جسم و آب چشم ما به دست عشق تست</p></div>
<div class="m2"><p>خاک را صلصال کردی آب را سلسال کن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>باز صیاد اجل را آتشین منقاردار</p></div>
<div class="m2"><p>چرخ گیرای امل را کاغذین چنگال کن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دامن تر دامنان عقل در آخال کش</p></div>
<div class="m2"><p>ساعد هودج کشان عشق پر خلخال کن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>عاشق مالست حرص و دشمن مالست می</p></div>
<div class="m2"><p>مال دشمن را به سعی باده دشمن مال کن</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>خال خود در چشم ما زن صبحهامان شام کن</p></div>
<div class="m2"><p>زلف خود بر دوش خود نه روزهامان سال کن</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>عشق یک رویست او را بر در عیسی نشان</p></div>
<div class="m2"><p>عقل یک چشمست او را در صف دجال کن</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>عشق را روز عزیمت باد بر فتراک بند</p></div>
<div class="m2"><p>عقل را وقت هزیمت خاک در دنبال کن</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ای سنایی خویش را چون طبع خرم وقت کن</p></div>
<div class="m2"><p>روح را چون خود همایون بخت و فرخ فال کن</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>خرقه و حالت به هشیاری محال و مخرقه‌ست</p></div>
<div class="m2"><p>چون ز خود بی خود شدی در خرقهٔ دل حال کن</p></div></div>