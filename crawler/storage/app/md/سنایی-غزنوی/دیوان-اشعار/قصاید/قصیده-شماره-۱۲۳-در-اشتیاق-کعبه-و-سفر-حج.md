---
title: >-
    قصیدهٔ شمارهٔ ۱۲۳ - در اشتیاق کعبه و سفر حج
---
# قصیدهٔ شمارهٔ ۱۲۳ - در اشتیاق کعبه و سفر حج

<div class="b" id="bn1"><div class="m1"><p>گاه آن آمد که با مردان سوی میدان شویم</p></div>
<div class="m2"><p>یک ره از ایوان برون آییم و بر کیوان شویم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>راه بگذاریم و قصد حضرت عالی کنیم</p></div>
<div class="m2"><p>خانه‌پردازیم و سوی خانهٔ یزدان شویم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>طبل جانبازی فرو کوبیم در میدان دل</p></div>
<div class="m2"><p>بی‌زن و فرزند و بی‌خان و سر و سامان شویم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گاه با بار مذلت سوی آن مسجد دویم</p></div>
<div class="m2"><p>گاه با رخت غریبی نزد آن ویران شویم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گاه در صحن بیابان با خران همره بویم</p></div>
<div class="m2"><p>گاه در کنج خرابی با سگان هم خوان شویم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گاه چون بی دولتان از خاک و خس بستر کنیم</p></div>
<div class="m2"><p>گاه چون ارباب دولت نقش شادروان شویم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گاه از ذل غریبی بار هر ناکس کشیم</p></div>
<div class="m2"><p>گاه در حال ضرورت یار هر نادان شویم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گاه بر فرزندگان چون بیدلان واله شویم</p></div>
<div class="m2"><p>گه ز عشق خانمان چون عاشقان پژمان شویم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از فراق شهر بلخ اندر عراق از چشم و دل</p></div>
<div class="m2"><p>گاه در آتش بویم و گاه در طوفان شویم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گه بعون همرهان چون آتش اندر دی بویم</p></div>
<div class="m2"><p>گه به دست ملحدان چون آب در آبان شویم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ملحدان گر جادوی فرعونیان حاضر کنند</p></div>
<div class="m2"><p>ما به تکبیری عصای موسی عمران شویم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>غم نباشد بیش ما را زان سپس روزی که ما</p></div>
<div class="m2"><p>از نشابور و ز طوس و مرو زی همدان شویم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>از پی بغداد و کرخ و کوفه و انطاکیه</p></div>
<div class="m2"><p>زهرمان حلوا شود آنشب که در حلوان شویم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چون بدارالملک عباسی امامی آمدیم</p></div>
<div class="m2"><p>تازه رخ چون برگ و شاخ از قطرهٔ باران شویم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>از برای حق صاحب مذهب اندر تهنیت</p></div>
<div class="m2"><p>جان قدم سازیم و سوی تربت نعمان شویم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>با شیاطین کین کشیم از خنجر توفیق حق</p></div>
<div class="m2"><p>چون ز قادسیه سوی عقبهٔ شیطان شویم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>پای چون در بادیهٔ خونین نهادیم از بلا</p></div>
<div class="m2"><p>همچو ریگ نرم پیش باد سرگردان شویم</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>زان یتیمان پدر گم کرده یاد آریم باز</p></div>
<div class="m2"><p>چون یتیمان روز عید از درد دل گریان شویم</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>از پدر وز مادر و فرزند و زن یاد آوریم</p></div>
<div class="m2"><p>ز آرزوی آن جگر بندان جگر بریان شویم</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>در تماشاشان نیابیم ار گهی خوش دل بویم</p></div>
<div class="m2"><p>گرد بالینشان نبینم ار دمی نالان شویم</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>در غریبی درد اگر بر جان ما غالب شود</p></div>
<div class="m2"><p>چون نباشد این عزیزان سخت بی‌درمان شویم</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>غمگساری نه که اشگی بارد از غمگین بویم</p></div>
<div class="m2"><p>مهربانی نی که آبی آرد ار عطشان شویم</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>نه پدر بر سر که ما در پیش او نازی کنیم</p></div>
<div class="m2"><p>نی پسر در بر که ما از روی او شادان شویم</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چون رخ پیری ببینیم از پدر یاد آوریم</p></div>
<div class="m2"><p>همچو یعقوب پسر گم کرده با احزان شویم</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>باشد امیدی هنوز ار زندگی باشد ولیک</p></div>
<div class="m2"><p>آه اگر در منزلی ما صید گورستان شویم</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>حسرت آن روز چون بر دل همی صورت کنیم</p></div>
<div class="m2"><p>ناچشیده هیچ شربت هر زمان حیران شویم</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>آه اگر یک روز در کنج رباطی ناگهان</p></div>
<div class="m2"><p>بی‌جمال دوستان و اقربا مهمان شویم</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>همرهان حج کرده باز آیند با طبل و علم</p></div>
<div class="m2"><p>ما به زیر خاک ره با خاک ره یکسان شویم</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>قافله باز آید اندر شهر بی‌دیدار ما</p></div>
<div class="m2"><p>ما به تیغ قهر حق کشتهٔ غریبستان شویم</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>همرهان با سرخ رویی چون به پیش ماه شب</p></div>
<div class="m2"><p>ما به زیر خاک چون در پیش مه کتان شویم</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>دوستان گویند حج کردیم و می‌آییم باز</p></div>
<div class="m2"><p>ما به هر ساعت همی طعمهٔ دگر کرمان شویم</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>نی که سالی صدهزار آزاده گردد منقطع</p></div>
<div class="m2"><p>هم دریغی نیست گر ما نیز چون ایشان شویم</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>گر نهنگ حکم حق بر جان ما دندان زند</p></div>
<div class="m2"><p>ما به پیش خدمت او از بن دندان شویم</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>رو که هر تیری که از میدان حکم آمد به ما</p></div>
<div class="m2"><p>هدیه جان سازیم و استقبال آن پیکان شویم</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>چون بدو باقی شدیم از جسم خود فانی شویم</p></div>
<div class="m2"><p>چون بدو دانا شدیم از علم خود نادان شویم</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>گر نباشد حج و عمره ور می و قربان گو مباش</p></div>
<div class="m2"><p>این شرف ما را نه بس کز تیغ او قربان شویم</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>این سفر بستان عیاران راه ایزدست</p></div>
<div class="m2"><p>ما ز روی استقامت سرو این بستان شویم</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>حاجیان خاص مستان شراب دولتند</p></div>
<div class="m2"><p>ما به بوی جرعه‌ای مولای این مستان شویم</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>نام و ننگ و لاف و اصل و فضل در باقی کنیم</p></div>
<div class="m2"><p>تا سزاوار قبول حضرت قرآن شویم</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>بادیه بوته‌ست و ما چون زر مغشوشیم راست</p></div>
<div class="m2"><p>چون بپالودیم ازو خالص چو زر کان شویم</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>بادیه میدان مردانست و ما نیز از نیاز</p></div>
<div class="m2"><p>خوی این مردان گریم و گوی این میدان شویم</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>گر چه در ریگ روان عاجز شویم از بی‌دلی</p></div>
<div class="m2"><p>چون پدید آید جمال کعبه جان افشان شویم</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>یا به دست آریم سری یا برافشانیم سر</p></div>
<div class="m2"><p>یا به کام حاسدان گردیم یا سلطان شویم</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>یا پدید آییم در صحرای مردان همچو کوه</p></div>
<div class="m2"><p>یا به زیر پشتهٔ ریگ روان پنهان شویم</p></div></div>