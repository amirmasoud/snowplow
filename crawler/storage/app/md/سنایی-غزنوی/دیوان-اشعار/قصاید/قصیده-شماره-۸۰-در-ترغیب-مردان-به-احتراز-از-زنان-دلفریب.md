---
title: >-
    قصیدهٔ شمارهٔ ۸۰ - در ترغیب مردان به احتراز از زنان دلفریب
---
# قصیدهٔ شمارهٔ ۸۰ - در ترغیب مردان به احتراز از زنان دلفریب

<div class="b" id="bn1"><div class="m1"><p>زیبد ار بی مایه عطاری کند پیوسته یار</p></div>
<div class="m2"><p>زان که هر تاری ز زلفش نافه دارد صد هزار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صد جگر بریان کند روزی ز حسنش ای شگفت</p></div>
<div class="m2"><p>هر که چندان مشک دارد با جگر او را چکار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مایهٔ عنبر فروشان بوی گرد زلف اوست</p></div>
<div class="m2"><p>هیچ دانی تا چه باشد یمن زلفش از یسار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بارنامهٔ چشم آهو از دو دیده کرد پست</p></div>
<div class="m2"><p>کارنامهٔ ناف آهو از دو جعدش ماند خوار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عارض زلفش ز بند کاسدی آن گه برست</p></div>
<div class="m2"><p>کاروان مشک و کافور از ریاح و از تتار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مشکشان در نافهاشان چون جگرشان خون شده</p></div>
<div class="m2"><p>از چه؟ ا زتشویر و شرم آن دو زلف مشکبار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>روی خوبش چو نگری فتنهٔ جهانی بین ازو</p></div>
<div class="m2"><p>فتنه فتنه‌ست ای برادر خواه منبر خواه دار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شمت زلفین او کردست چون باد بهشت</p></div>
<div class="m2"><p>خاک را عنبر نسیم و باد را مشکین به خار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>حسن و خلق و لطف و ملح آمد اصول جوهرش</p></div>
<div class="m2"><p>با اصول جوهر ما باد و خاک و آب و نار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>روی او اندر صفا و روشنی چون آینه‌ست</p></div>
<div class="m2"><p>باز روی من ز آب دیدگان باشد بحار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>من بدو چون بنگرم یا او به من چون بنگرد</p></div>
<div class="m2"><p>من همی او گردم و او من به روزی چند بار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>از لبم باد خزان خیزد که از تاثیر عشق</p></div>
<div class="m2"><p>چون از آن دندان کژ مژ خود بخندد چون بهار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>در مثل گویند مروارید کژ نبود چرا</p></div>
<div class="m2"><p>کژ همی بینم چو زلف نیکوان دندان یار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>لیک چندان زیب دارد کژ مژی دندان او</p></div>
<div class="m2"><p>کن نیابی در هزاران کوکب گردون گذار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>در لبش چون بنگرم از غایت لعلی شود</p></div>
<div class="m2"><p>چشمم از عکس لبان چون می او پر خمار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>هر که روزی بی رضایش چهرهٔ زیباش دید</p></div>
<div class="m2"><p>بی خلاف از وی برآرد داغ بی صبری دمار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>او همی کاهد ز نیکو عهدی و از خوشخویی</p></div>
<div class="m2"><p>هر چه بر رویش طبیعت می‌بیفزاید نگار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>هست بسیاری نکوتر زیب امروزش ز دی</p></div>
<div class="m2"><p>هست بسیاری تبه‌تر عهد امسالش ز پار</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ای دریغ از هیچ سنگستی درو بر راه او</p></div>
<div class="m2"><p>کشتگان عشق یابندی قطار اندر قطار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>لیک طبع عامیان را ماند از ساده دلی</p></div>
<div class="m2"><p>هر که دامی راست کرد او را درو بینی شکار</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>گه برین هم جفت باشد همچو بی دین با دروغ</p></div>
<div class="m2"><p>گه بر آن همخوابه گردد همچو بد خو با نقار</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>من که جان و عمر و دل درباختم در عشق او</p></div>
<div class="m2"><p>من که جاه و مال و دین در عشق او کردم نثار</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بر چو من کس نا کسی را برگزیند هر زمان</p></div>
<div class="m2"><p>اینت بی معنی نگاری وه که یارب زینهار</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>جان من آتش همی گیرد که از دون همتی</p></div>
<div class="m2"><p>هرکرا بیند، همی گیرد چو آب اندر کنار</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>غیرت آنرا که چون نارنگ ده دل بینمش</p></div>
<div class="m2"><p>گر به سینه صد دلستی خون شدستی چون انار</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بنده از وی آمنم زیرا که روزی بیشک‌ست</p></div>
<div class="m2"><p>در طویلهٔ عشوهٔ او صد کس اندر انتظار</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>در حرم هر کس در آید لیک از روی شرف</p></div>
<div class="m2"><p>نیست یک کس را مسلم در حرم کردن شکار</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>باز اگر چند این چنین ست او ولیک این به بود</p></div>
<div class="m2"><p>کاش اندر سنگ باشد پنبه‌ای در پنبه‌زار</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بید باری ایمنست از زحمت هر کس ولی</p></div>
<div class="m2"><p>سنگ نااهلان خورد شاخی که دارد میوه بار</p></div></div>