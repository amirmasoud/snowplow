---
title: >-
    قصیدهٔ شمارهٔ ۱۹۶
---
# قصیدهٔ شمارهٔ ۱۹۶

<div class="b" id="bn1"><div class="m1"><p>زیر دام عشوه تا چند ای سنایی دم زنی</p></div>
<div class="m2"><p>گاه آن آمد یکی کاین دام و دم بر هم زنی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از دم خویشی تو دایم مانده اندر دام دیو</p></div>
<div class="m2"><p>گر برون آیی ملک گردی و جام جم زنی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با تو اندر پوست باشد بی‌گمان ابلیس تو</p></div>
<div class="m2"><p>تا تو اندر عشق دم در خانهٔ آدم زنی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون نگفتی لا مگو الله و اثباتی مکن</p></div>
<div class="m2"><p>گر قدم در کوی نفی خود نهی محکم زنی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گویی الاالله و آنگاهی ز کوته دیدگی</p></div>
<div class="m2"><p>گه رقم بر علم و گاهی تکیه بر عالم زنی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در نهاد تو دو صد فرعون با دعوی هنوز</p></div>
<div class="m2"><p>تو همی خواهی که چون موسا عصا بر یم زنی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از مراد خود تبرا کن اگر خواهی که تو</p></div>
<div class="m2"><p>در میان بی‌مرادان یک نفس بی غم زنی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون ولایتها گرفت اندر تنت دیو سپید</p></div>
<div class="m2"><p>رستم راهی گر او را ضربت رستم زنی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کی دهد عیسا ترا از جوی عین‌السلوی آب</p></div>
<div class="m2"><p>چون تو عمدا آتش اندر چادر مریم زنی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نشنود گوش تو هرگز صوت موسیقار عشق</p></div>
<div class="m2"><p>تا تو در بزم مراد خویش زیر و بم زنی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>پای بیرون نه ز گلزار و به گلزار اندر آی</p></div>
<div class="m2"><p>تا به دست نیستی با پاکبازان کم زنی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>عشق خرگه کی زند اندر هوای سر تو</p></div>
<div class="m2"><p>تا تو خرگه زیر جعد زلف خم در خم زنی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>حال را با قال همره کن تو اندر راه عشق</p></div>
<div class="m2"><p>ورنه چون بی‌مایگان تا کی دم مبهم زنی</p></div></div>