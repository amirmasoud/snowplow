---
title: >-
    قصیدهٔ شمارهٔ ۷۴ - در مدح علی بن محمد طبیب
---
# قصیدهٔ شمارهٔ ۷۴ - در مدح علی بن محمد طبیب

<div class="b" id="bn1"><div class="m1"><p>ای گردن احرار به شکر تو گرانبار</p></div>
<div class="m2"><p>تحقیق ترا همره و توفیق ترا یار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای خواجهٔ فرزانه علی‌بن محمد</p></div>
<div class="m2"><p>وی نایب عیسا به دو صد گونه نمودار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چندان که ترا جود و معالی‌ست به دنیا</p></div>
<div class="m2"><p>نه نقطه سکون دارد و نه دایره رفتار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ذهن تو و سنگ تو به مقدار حقیقت</p></div>
<div class="m2"><p>بر سخت همه فایدهٔ روح به معیار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مر جاه تو و علم ترا از سر معنی</p></div>
<div class="m2"><p>آباء و سطقسات غلامند و پرستار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نخرید کسی جان بهایی به زر و سیم</p></div>
<div class="m2"><p>تا نامدش اسراسر علوم تو پدیدار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>برگ اجل از شاخ امل پاک فرو ریخت</p></div>
<div class="m2"><p>تا شاخ علومت عمل آورد چنین بار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شد طبع جهان معتدل از تو که نیابی</p></div>
<div class="m2"><p>در شهر یکی ذات گرانجان و سبکبار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از غایت آزادگی و فر بزرگیت</p></div>
<div class="m2"><p>گشتند غلامان ستانهٔ درت احرار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گفتار فزونست ز هر چیز ولیکن</p></div>
<div class="m2"><p>جود تو و مدح تو فزونست ز گفتار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>عقلی که ز داروت مدد یافت به تحقیق</p></div>
<div class="m2"><p>در تختهٔ تقدیر بخواند همه اسرار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>شخصی که تر از شربت تو شد جگر او</p></div>
<div class="m2"><p>لب خشک نماند به همه عمر چو سوفار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>از عقل تو ای ناقد صراف طبیعت</p></div>
<div class="m2"><p>شد عنصر ترکیب همه خلق چو طیار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>آنکس که یکی مسهل و داروی تو خوردست</p></div>
<div class="m2"><p>مانند فرشته نشود هرگز بیمار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>هر چشم که از خاک درت سرمهٔ او بود</p></div>
<div class="m2"><p>ز آوردن هر آب که آرد نشود تار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>آنها که یکی حبه ز حب تو بخوردند</p></div>
<div class="m2"><p>در دام اجل هیچ نگردند گرفتار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>حذق تو چنانست که بی‌نبض و دلیلی</p></div>
<div class="m2"><p>می باز نمایی غرض روح به هنجار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گر باد بفرخار بر دشمت داروت</p></div>
<div class="m2"><p>از قوت او روح پذیرد بت فرخار</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بر کار ز داروی تو شد شخص معطل</p></div>
<div class="m2"><p>مانده ملک الموت ز داروی تو بیکار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ای طبع و علوم تو شفا بخش و سخاورز</p></div>
<div class="m2"><p>وی دست و زبان تو درر پاش و گهربار</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>از مال تو جز خانهٔ تو کیست تهی‌دست</p></div>
<div class="m2"><p>وز دست تو جز کیسهٔ تو کیست زیان‌کار</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>آراسته‌ای از شرف و جود همیشه</p></div>
<div class="m2"><p>چون شاخ ز طیار و چو افلاک ز سیار</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>فعل تو چنانست که دیگر ز معاصی</p></div>
<div class="m2"><p>واجب نشود بر تو یکی روز ستغفار</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چون مردمک دیده عزیزی بر ما ز آنک</p></div>
<div class="m2"><p>در چشم تو سیم و زر ما هست چنین خوار</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چون نقطهٔ نقش‌ست دل آنکه ابا تو</p></div>
<div class="m2"><p>دو روی و دو سر باشد چون کاغذ پرگار</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ادیان به علی راست شد ابدان به تو زیراک</p></div>
<div class="m2"><p>تو نافع مومن شدی او قامع کفار</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>تو دیگری و حاسد تو دیگر از آن کو</p></div>
<div class="m2"><p>خار آمده بی‌گلبن تو گلبن بی‌خار</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>کی گردد مه مردم بد اصل به دعوی</p></div>
<div class="m2"><p>کی گردد نو پیرهن کهنه به آهار</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>یک شهر طبیبند ولی از سر دعوی</p></div>
<div class="m2"><p>کو چون تو یکی خواجهٔ دانندهٔ هشیار</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>عالم همه پر موسی و چوبست ولیکن</p></div>
<div class="m2"><p>یک موسی از آن کو که ز چوبی بکند مار</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>کار چو تو کس نیست شدن نزد هر ابله</p></div>
<div class="m2"><p>تا بار دهد یا ندهد حاجب و سالار</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>کز حشمت و جاه تو همی پیش نیاید</p></div>
<div class="m2"><p>نور قمر و شمس به درگاه تو بی‌یار</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>خود دیده کنان جمله می‌آیند سوی تو</p></div>
<div class="m2"><p>دیدار ترا از دل و جان گشته خریدار</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>تو کعبهٔ مایی و به یک جای بیاسای</p></div>
<div class="m2"><p>این رفتن هر جای به هر بیهده بگذار</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>زوار سوی خانهٔ کعبه شده از طمع</p></div>
<div class="m2"><p>هرگز نشود کعبه سوی خانهٔ زوار</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>دیدیم طبیبان و بدین مایه شناسیم</p></div>
<div class="m2"><p>ما جعفر طیار ز بو جعفر طرار</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>بر چشمهٔ حیوان ز پی چون تو طبیبی</p></div>
<div class="m2"><p>شاید که کند فخر شهنشاه جهاندار</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>کز جود تو و علم تو غزنین چو بهشتست</p></div>
<div class="m2"><p>زیرا که درو نیست نه بیمار و نه تیمار</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>ای مرد فلک حشمت و فرزانهٔ مکرم</p></div>
<div class="m2"><p>وی پیر جوان دولت مردانهٔ غیار</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>هستیم بر آنسان ز حکیمی که نگوید</p></div>
<div class="m2"><p>اندر همه عالم ز من امروز کس اشعار</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>لیک آمده‌ام سیر ز افعال زمانه</p></div>
<div class="m2"><p>هر چند هنوز از غرض خویشم ناهار</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>آن سود همی بینم از اشعار که هر شب</p></div>
<div class="m2"><p>هش را ببرد سوش بماند بر من عار</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>خواریم از آنست که زین شهرم ازیرا</p></div>
<div class="m2"><p>در بحر و صدف خوار بود لولو شهوار</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>هدهد کلهی دارد و طاووس قبایی</p></div>
<div class="m2"><p>من بلبل و خواهان یکی درعه و دستار</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>زین محتشمانند درین شهر که همت</p></div>
<div class="m2"><p>بر هیچ کسی می‌نتوان دوخت به مسمار</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>ای درت ز بی‌برگان چون شاخ در آذر</p></div>
<div class="m2"><p>وی دلت ز بخشیدن چون باغ در آزار</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>از مکرمت تست که پیوسته نهفته‌ست</p></div>
<div class="m2"><p>این شخص به دراعه و این پای به شلوار</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>پس چون تنم آراستهٔ پیرهن تست</p></div>
<div class="m2"><p>این فرق مرا نیز بیارای به دستار</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>سود از تو بدان جویم کز مایهٔ طبعم</p></div>
<div class="m2"><p>خود را بر تو دیده‌ام این قیمت و بازار</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>آثار نکو به که بماند چو ز مردم</p></div>
<div class="m2"><p>می هیچ نماند ز پس مرگ جز آثار</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>تا جوهر دریا نبود چون گهر باد</p></div>
<div class="m2"><p>تا مایهٔ مرکز نبود چون فلک نار</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>چون چار گهر فعل تو و ذات تو بادا</p></div>
<div class="m2"><p>از محکمی و لطف و توانایی و مقدار</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>در عافیت خیر و سخا باد همیشه</p></div>
<div class="m2"><p>اسباب بقای تو چو خیرات تو بسیار</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>جبار ترا از قبل نفع طبیبان</p></div>
<div class="m2"><p>تا دیر برین مکرمت و جود نگهدار</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>جبار ترا باد نگهبان به کریمی</p></div>
<div class="m2"><p>از مادح بدگوی و ز ممدوح جگرخوار</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>از فضل ملک باد به هر حال و به هر وقت</p></div>
<div class="m2"><p>امروز تو از دی به و امسال تو از پار</p></div></div>