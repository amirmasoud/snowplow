---
title: >-
    قصیدهٔ شمارهٔ ۱۱۵
---
# قصیدهٔ شمارهٔ ۱۱۵

<div class="b" id="bn1"><div class="m1"><p>بخ بخ اگر این علم برافرازم</p></div>
<div class="m2"><p>در تفرقه سوی جمع پردازم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>باشد بینم رخان معشوقم</p></div>
<div class="m2"><p>وز صحبت خود دری کند بازم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از راهبران عشق ره پسرم</p></div>
<div class="m2"><p>با پاک بران دو کون در بازم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شطرنج به شاهمات بر بندم</p></div>
<div class="m2"><p>در ششدره مهره‌ای در اندازم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر فرش فنا به قعده ننشینم</p></div>
<div class="m2"><p>در باغ بقا چو سرو بگرازم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>این عشوهٔ اوست خاک آدم را</p></div>
<div class="m2"><p>با صحبت جان و دل بدل سازم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>این گنج که تو ختم من از هستی</p></div>
<div class="m2"><p>در بوتهٔ نیستیش بگدازم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>این بربط غم گداز در وصلت</p></div>
<div class="m2"><p>در بهر نهم و بشرط بنوازم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هر بیت که از سماع او گویم</p></div>
<div class="m2"><p>اول سخنی ز عشق آغازم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>این است جواب آن کجا گفتم</p></div>
<div class="m2"><p>«کی باشد کاین قفس بپردازم»</p></div></div>