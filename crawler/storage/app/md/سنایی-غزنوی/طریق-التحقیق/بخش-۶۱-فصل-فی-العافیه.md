---
title: >-
    بخش ۶۱ - فصل فی العافیه
---
# بخش ۶۱ - فصل فی العافیه

<div class="b" id="bn1"><div class="m1"><p>در جهان هر چه هست عاریت است</p></div>
<div class="m2"><p>بهترین نعمتیش عافیت است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p> هست اندر جهان جسمانی</p></div>
<div class="m2"><p> عافیت ملکت سلیمانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p> هر که در عافیت بداند پست</p></div>
<div class="m2"><p> قدر این مملکت شناسد چیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p> خشک نانی به عافیت زجهان</p></div>
<div class="m2"><p> نزد من به زملکت خاقان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p> فرخ آن کو دل از جهان برکند</p></div>
<div class="m2"><p> ببرید از جهانیان پیوند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p> فرخ آن کو به گوشه‌ای بنشست</p></div>
<div class="m2"><p> گشت فارغ زگفت وگوی‌برست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p> هرکه را این غرض میسر شد</p></div>
<div class="m2"><p> از شرف با ملک برابر شد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p> شاه ایوان غلام او باشد</p></div>
<div class="m2"><p> جرعه خواران جام او باشد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون ترا عافیت نماید روی</p></div>
<div class="m2"><p>پس از آن بر طریق آزمپوی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p> آز بگذار تا نیاز آری</p></div>
<div class="m2"><p> کاز آرد به رویها خواری</p></div></div>
<div class="b" id="bn11"><div class="m1"><p> طمع و آز را مرید مباش</p></div>
<div class="m2"><p> بایزیدی کن و یزید مباش</p></div></div>
<div class="b" id="bn12"><div class="m1"><p> از پی ملک او گزید سفر</p></div>
<div class="m2"><p> دو جهان پیش او نداشت خطر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بزن ای پیرو جوانمردان</p></div>
<div class="m2"><p> بر جهان پشت پای چون مردان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p> تا ترا بر جهان و جان نظر است</p></div>
<div class="m2"><p> هر چه هستی توست در خطراست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p> برفشان آستین زجان و جهان</p></div>
<div class="m2"><p> التفاتی مکن بدین و بدان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p> شاخ حرص و هوا ز بیخ بکن</p></div>
<div class="m2"><p> کردن آز و آرزو بشکن</p></div></div>
<div class="b" id="bn17"><div class="m1"><p> هر چه یابی زنعمت دنیا</p></div>
<div class="m2"><p> برفشان بهر عزت عقبا</p></div></div>
<div class="b" id="bn18"><div class="m1"><p> چون الف آن‌کسی‌که هیچ نداشت</p></div>
<div class="m2"><p> اندر آن هیچ بند و پیچ نداشت</p></div></div>
<div class="b" id="bn19"><div class="m1"><p> دم زتجرید، آن تواند زد</p></div>
<div class="m2"><p> که لگد بر جهان‌، تواند زد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p> در روش چون بدین مقام بود</p></div>
<div class="m2"><p> دان که در عاشقی تمام بود</p></div></div>
<div class="b" id="bn21"><div class="m1"><p> مرد این ره چو راهرو باشد</p></div>
<div class="m2"><p> هر زمان قربتیش نو باشد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p> نقش کژ محو کن زتخته دل</p></div>
<div class="m2"><p> تا شود کشف بر تو هر مشکل</p></div></div>
<div class="b" id="bn23"><div class="m1"><p> هر مرادی که از تو روی بتافت </p></div>
<div class="m2"><p> نتوان جز براستی دریافت </p></div></div>