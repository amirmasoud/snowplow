---
title: >-
    بخش ۴۹ - الذین یذکرون الله قیاماً وقعوداً و علی جنوبهم‌
---
# بخش ۴۹ - الذین یذکرون الله قیاماً وقعوداً و علی جنوبهم‌

<div class="b" id="bn1"><div class="m1"><p>باش پیوسته با خضوع و بکا</p></div>
<div class="m2"><p>روز و شب در میان خوف و رجا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p> باش با نفس و قهر خود قاهر</p></div>
<div class="m2"><p> دار یک رنگ باطن و ظاهر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p> از برای قبول خاصه و عام</p></div>
<div class="m2"><p> به پا باشدت قعود و قیام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p> بی ریا در ره طلب نه پای</p></div>
<div class="m2"><p> خالصا مخلصاً برای خدای</p></div></div>
<div class="b" id="bn5"><div class="m1"><p> چابک وچست رو، نه‌کاهل و سست</p></div>
<div class="m2"><p>تا بدانجا رسی‌که مقصد توست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p> مقصد ت عالم الهی دان </p></div>
<div class="m2"><p> بی تباهی و بی تناهی دان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>رو به کونین سر فرود میار</p></div>
<div class="m2"><p>تا بر آن آستان بیابی بار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p> چون لگد بر سر دوکون زنی</p></div>
<div class="m2"><p> رخت خود در جهان «‌هو» فکنی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p> گرتواینجا به خویش مشغولی</p></div>
<div class="m2"><p> دان که زان کارگاه معزولی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p> وربگردد از این نسق صفتت</p></div>
<div class="m2"><p> حاصل آید کمال معرفتت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p> هر کمالی که آن سری نبود</p></div>
<div class="m2"><p> جز که نقصان و سرسری نبود</p></div></div>
<div class="b" id="bn12"><div class="m1"><p> گر کمالی طلب کنی آنجا</p></div>
<div class="m2"><p> که زنقصان بری بود فردا،</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>راست بشنو اگر به تنگی حال </p></div>
<div class="m2"><p>بی نیازی زخلق اینت کمال‌! </p></div></div>