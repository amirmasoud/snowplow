---
title: >-
    بخش ۱۷ - در سؤال از عقل کل و جواب او
---
# بخش ۱۷ - در سؤال از عقل کل و جواب او

<div class="b" id="bn1"><div class="m1"><p>خردم دوش اندپن معنی</p></div>
<div class="m2"><p>نکته‌ای چند نغز کرد املی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p> گفت شهری‌که جا و مبین ماست</p></div>
<div class="m2"><p> صحن او سقف‌گنبد اعلا‌ست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p> خاک او راست نکهت عنبر</p></div>
<div class="m2"><p>آب او راست‌، لذت شکر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p> نز برودت در او اثر بینی‌ </p></div>
<div class="m2"><p> نز حرارت در او شرر بینی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اندر آن شهر ما گلستانهاست</p></div>
<div class="m2"><p>که چمنهاش نزهت جانهاست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p> طوطیان بینی ندر آن بستان</p></div>
<div class="m2"><p> همه را ذکر حق بود الحان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون کند لطف او تعلمشان</p></div>
<div class="m2"><p>«‌ربی الله» بود ترنمّشان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p> در چمنهاش بلبلان ، گویا</p></div>
<div class="m2"><p> نغمه‌شان جمله «‌ربنا الاعلی»</p></div></div>
<div class="b" id="bn9"><div class="m1"><p> «‌مقعد صدق‌» ازو ولایت ماست</p></div>
<div class="m2"><p> هرکه آنجاست در حمایت ماست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p> همگان خاص حضرت سلطان</p></div>
<div class="m2"><p> جسته از بند انجم و ارکان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p> رهروان بینی از سر غیرت</p></div>
<div class="m2"><p> همه اوفتاده در ره حیرت</p></div></div>
<div class="b" id="bn12"><div class="m1"><p> ساکنان بینی از سر اخلاص</p></div>
<div class="m2"><p> چشم بگشاده بر سرادق خاص</p></div></div>
<div class="b" id="bn13"><div class="m1"><p> چون بدان شهر جان فرود آیی</p></div>
<div class="m2"><p> پن همه دردسر بیاسایی‌</p></div></div>
<div class="b" id="bn14"><div class="m1"><p> مسکن و جایگاه ما بینی</p></div>
<div class="m2"><p> مجلس خاص شاه ما بینی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p> خلعت شاه‌، بی بدن پوشی</p></div>
<div class="m2"><p> بادهٔ شوق‌، بی دهن نوشی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p> نغمهٔ بلبلان ره شنوی </p></div>
<div class="m2"><p> «‌وحده لاشریک له» شنوی </p></div></div>