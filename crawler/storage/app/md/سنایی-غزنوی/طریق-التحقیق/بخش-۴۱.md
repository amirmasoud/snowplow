---
title: >-
    بخش ۴۱ - **‌*
---
# بخش ۴۱ - **‌*

<div class="b" id="bn1"><div class="m1"><p>تا جهان است کار او این است</p></div>
<div class="m2"><p>نوش او نیش و مهر اوکین است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p> اندر این خاکدان افسرده</p></div>
<div class="m2"><p> هیچ کس نیست از غم آسوده</p></div></div>
<div class="b" id="bn3"><div class="m1"><p> آنچنان زی درو که وقت رحیل</p></div>
<div class="m2"><p> بیش باشد به رفتنت تعجیل</p></div></div>
<div class="b" id="bn4"><div class="m1"><p> رخت بیرون فکن زدار غرور</p></div>
<div class="m2"><p> چه نشینی میان دیو شرور؟‌</p></div></div>
<div class="b" id="bn5"><div class="m1"><p> حسد و حرص را به گور مبر</p></div>
<div class="m2"><p>دشمنان را به راه دور مبر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p> دو رفیقند هر دو ناخوش و زشت </p></div>
<div class="m2"><p> باز دارندت این و آن زبهشت </p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پیشتر زآنکه مرگ پیش آید</p></div>
<div class="m2"><p>از چنین مرگ زندگی زاید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p> به چنین مرگ هر که بشتابد</p></div>
<div class="m2"><p>از چنین مرگ زندگی یابد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p> تا از این زندگی نمیری تو</p></div>
<div class="m2"><p>در کف دیو خود اسیری تو</p></div></div>
<div class="b" id="bn10"><div class="m1"><p> نفس تو تابدیش عادت و خوست</p></div>
<div class="m2"><p> به حقیقت بدان‌که دیو تو اوست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p> مرده دل گشتی و پراکنده </p></div>
<div class="m2"><p> کوش تا جمع باشی و زنده </p></div></div>