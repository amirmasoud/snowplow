---
title: >-
    بخش ۲۳ - و لقد کرمنا بنی آدم
---
# بخش ۲۳ - و لقد کرمنا بنی آدم

<div class="b" id="bn1"><div class="m1"><p>در نگر تا که آفرید ترا ؟</p></div>
<div class="m2"><p>از برای چه برگزید ترا ؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p> خاک بودی ترا مکرم کرد</p></div>
<div class="m2"><p> زان پست جلوهٔ دو عالم کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p> از همه مهتر آفرید ترا</p></div>
<div class="m2"><p> هر چه هست از همه گزید ترا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p> در نظر از همه لطیف‌تری</p></div>
<div class="m2"><p> به صفت از همه شریف‌تری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p> خوبتر از تو نقشبند ازل</p></div>
<div class="m2"><p> هیچ نقشی نبست در اول</p></div></div>
<div class="b" id="bn6"><div class="m1"><p> قدرتش بهترین صفت به تو داد</p></div>
<div class="m2"><p> شرف نور معرفت به تو داد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p> گوهر مردمی شعار تو کرد</p></div>
<div class="m2"><p> کرم و لطف خود نثار تو کرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p> باطنت را به لطف خود پرورد</p></div>
<div class="m2"><p> ظاهرت قبهٔ ملایک کرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p> آن یکی گنج نامهٔ عصمت</p></div>
<div class="m2"><p> این یکی کارنامهٔ حکمت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p> اختر آسمان معرفتی</p></div>
<div class="m2"><p> زبدهٔ چار طبع و شش جهتی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p> قاری سورهٔ مجاهده‌ای</p></div>
<div class="m2"><p> قابل لذت مشاهده‌ای</p></div></div>
<div class="b" id="bn12"><div class="m1"><p> خلقتت برد کوی استکمال</p></div>
<div class="m2"><p> همتت راست سو‌ی استدلال</p></div></div>
<div class="b" id="bn13"><div class="m1"><p> خاطرت مدرک وجود خودست</p></div>
<div class="m2"><p> عنصرت مستعد نیک و بدست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p> با تو بودست در الست خطاب</p></div>
<div class="m2"><p> با تو باشد به روز حشر حساب</p></div></div>
<div class="b" id="bn15"><div class="m1"><p> گفته اسم جملهٔ اشیاء </p></div>
<div class="m2"><p> در حق توست «‌علم الاسماء»‌ </p></div></div>
<div class="b" id="bn16"><div class="m1"><p>طارم آسمان و گوی زمین</p></div>
<div class="m2"><p>از برای تو ساخته ست چنین</p></div></div>
<div class="b" id="bn17"><div class="m1"><p> فرش غبرا برای تو گسترد</p></div>
<div class="m2"><p> چرخ فیروزه سایبان تو کرد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p> آفرینش همه غلام تواند</p></div>
<div class="m2"><p> از پی قوت و قوام تواند</p></div></div>
<div class="b" id="bn19"><div class="m1"><p> حکمت و فطنت وکیاست و علم</p></div>
<div class="m2"><p> همّت و سیرت و مروت و حلم</p></div></div>
<div class="b" id="bn20"><div class="m1"><p> در وجود تو جمله موجودست</p></div>
<div class="m2"><p> وین همه لطف وجود معبودست</p></div></div>
<div class="b" id="bn21"><div class="m1"><p> صفت تو به قدر آنکه تویی</p></div>
<div class="m2"><p> نتوان گفت آنچنان که تویی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p> نشنیدی‌که آن حکیم چه‌گفت</p></div>
<div class="m2"><p> که به الماس در معنی سفت</p></div></div>
<div class="b" id="bn23"><div class="m1"><p> تو به قیمت ورای دو جهانی</p></div>
<div class="m2"><p> چه کنم قدر خود نمی‌دانی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p> این همه عزت و شرف‌که تراست </p></div>
<div class="m2"><p> تو زخود غافلی عظیم خطاست‌! </p></div></div>