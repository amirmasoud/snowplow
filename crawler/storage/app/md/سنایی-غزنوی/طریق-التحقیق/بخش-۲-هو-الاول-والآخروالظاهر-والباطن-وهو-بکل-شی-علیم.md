---
title: >-
    بخش ۲ - هو الاول والآخروالظاهر والباطن وهو بکل شی‌ء علیم
---
# بخش ۲ - هو الاول والآخروالظاهر والباطن وهو بکل شی‌ء علیم

<div class="b" id="bn1"><div class="m1"><p>حی و قیوم و قادر و قاهر</p></div>
<div class="m2"><p>اول اول آخر آخر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p> نطق‌، ابکم بمانده در صفتش</p></div>
<div class="m2"><p> وهم‌، عاجز شده زمعرفتش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p> نبرد عقل‌ در ‌صفاتش راه</p></div>
<div class="m2"><p> نبود وهم را به ذاتش راه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p> کی رسد وهم در جهان قدم</p></div>
<div class="m2"><p> که بلند است آستان قدم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p> نص قرآن شده است ای عاشق</p></div>
<div class="m2"><p> در صفات جلال او ناطق</p></div></div>
<div class="b" id="bn6"><div class="m1"><p> «شهد الله‌» گواه معرفتش </p></div>
<div class="m2"><p> ‌«وحده لاشریک له» صفتش </p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نه از او زاد کس‌، نه او از کس</p></div>
<div class="m2"><p>«‌قل هو الله‌» دلیل و حجت بس</p></div></div>
<div class="b" id="bn8"><div class="m1"><p> از مکان و زمان بری ذاتش</p></div>
<div class="m2"><p> محض جهلست نفی و اثباتش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p> هست واجب وجود او دائم</p></div>
<div class="m2"><p> زآنکه باشد به ذات خود قائم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p> غایت ملک او نداند کس</p></div>
<div class="m2"><p> همه او و بدو نماند کس</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نیست با هیچ چیز پیوندش </p></div>
<div class="m2"><p> نبود جفت‌ و مثل و مانندش </p></div></div>