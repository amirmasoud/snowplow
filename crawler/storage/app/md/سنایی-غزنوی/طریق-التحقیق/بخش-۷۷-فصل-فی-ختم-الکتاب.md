---
title: >-
    بخش ۷۷ - فصل فی ختم الکتاب
---
# بخش ۷۷ - فصل فی ختم الکتاب

<div class="b" id="bn1"><div class="m1"><p>ای دریغا که در زمانهٔ ما</p></div>
<div class="m2"><p>هزل آید به کارخانهٔ ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p> هزل را خواستگار بسیار است</p></div>
<div class="m2"><p> زنخ و ریشخند در کار است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p> میل ایشان به هزل بیشتر است</p></div>
<div class="m2"><p> هزل‌، آلحق زجد عزیزتر است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p> مرد را هزل زی گناه برد</p></div>
<div class="m2"><p> جد سوی عالم اله برد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p> چون تو جد یافتی ببر از هزل</p></div>
<div class="m2"><p> تا از آن مملکت نباشی عزل</p></div></div>
<div class="b" id="bn6"><div class="m1"><p> من چو زین شیوه رخ بتافته‌ام</p></div>
<div class="m2"><p> هر چه کردم طلب‌، بیافته‌ام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p> از ره هزل پا برون بردم</p></div>
<div class="m2"><p> تختهٔ دل ز هزل بستردم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p> پس برو نقش جد نگاشته‌ام</p></div>
<div class="m2"><p> علم عشق برفراشته‌ام</p></div></div>
<div class="b" id="bn9"><div class="m1"><p> اندرین کارنامهٔ عصمت</p></div>
<div class="m2"><p> بسته‌ام نقش خامهٔ عصمت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p> بس گهرشان فشاندم از سر کلک</p></div>
<div class="m2"><p> در معنی کشیدم اندر سلک</p></div></div>
<div class="b" id="bn11"><div class="m1"><p> این سخن تحفه‌ایست ربانی</p></div>
<div class="m2"><p> رمز و اسرارهای روحانی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p> سخن از آسمان بلندتر است</p></div>
<div class="m2"><p> تانگویی که نظم مختصر است</p></div></div>
<div class="b" id="bn13"><div class="m1"><p> لفظ او شرح رمز و اسرار است</p></div>
<div class="m2"><p> معنی‌اش‌ شمع روی ابرار است</p></div></div>
<div class="b" id="bn14"><div class="m1"><p> نظم نغزش زنکته و امثال</p></div>
<div class="m2"><p> سحر مطلق ولی مباح و حلال </p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بوستانی است پر گل و نسرین</p></div>
<div class="m2"><p>آسمانی است پرمه و پروین</p></div></div>
<div class="b" id="bn16"><div class="m1"><p> مونس عارفان حضرت حق</p></div>
<div class="m2"><p> قائد طالبان قدرت حق</p></div></div>
<div class="b" id="bn17"><div class="m1"><p> اهل دل کاین سخن فرو خوانند</p></div>
<div class="m2"><p> آستین از جهان برافشانند</p></div></div>
<div class="b" id="bn18"><div class="m1"><p> خاطر ناقصم چو کامل شد</p></div>
<div class="m2"><p> به سخنهای بکر حامل شد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p> هر نفس شاهدی دگر زاید</p></div>
<div class="m2"><p> هر یک از یک شگرفتر زاید</p></div></div>
<div class="b" id="bn20"><div class="m1"><p> شاهدانی به چهره همچو هلال</p></div>
<div class="m2"><p> در حجاب حروف زهره جمال</p></div></div>
<div class="b" id="bn21"><div class="m1"><p> اینکه بینی که من ترش رثم</p></div>
<div class="m2"><p> کز عنا پر زچین شد ابرویم</p></div></div>
<div class="b" id="bn22"><div class="m1"><p> سخنم بین چه نغز و شیرین است</p></div>
<div class="m2"><p> منتظم همچو عقد پروین است</p></div></div>
<div class="b" id="bn23"><div class="m1"><p> صورت من اگر چه مختصرست</p></div>
<div class="m2"><p> صفتم بین که عالم هنرست</p></div></div>
<div class="b" id="bn24"><div class="m1"><p> مهر و مه بندهٔ ضمیر منند</p></div>
<div class="m2"><p> عاشق خاطر منیر منند</p></div></div>
<div class="b" id="bn25"><div class="m1"><p> من چو شمعم‌که مجلس افروزم</p></div>
<div class="m2"><p> رشتهٔ جان خود همی سوزم</p></div></div>
<div class="b" id="bn26"><div class="m1"><p> شمع کردار بر لگن سوزان</p></div>
<div class="m2"><p> روشن از من جهان و من سوزان</p></div></div>
<div class="b" id="bn27"><div class="m1"><p> این سخنهاکه مغز جان من است</p></div>
<div class="m2"><p> گر بد ارنیک شد زبان من است</p></div></div>
<div class="b" id="bn28"><div class="m1"><p> نیستم در سخن عیال کسی</p></div>
<div class="m2"><p> نپرم من به پر و بال کسی</p></div></div>
<div class="b" id="bn29"><div class="m1"><p> تو چه دانی چه خون دل خوردم</p></div>
<div class="m2"><p> تامن این را به نظم آوردم</p></div></div>
<div class="b" id="bn30"><div class="m1"><p> فکرم القصه حق گزاری کرد</p></div>
<div class="m2"><p> اندرین نظم جان سپاری کرد</p></div></div>
<div class="b" id="bn31"><div class="m1"><p> پانصد و بیست و هشت آخر سال</p></div>
<div class="m2"><p> بود کاین نظم نغز یافت کمال</p></div></div>
<div class="b" id="bn32"><div class="m1"><p> در جهان زین سخن بدین آیین </p></div>
<div class="m2"><p> کامل و نغز و شاهد و شیرین‌، </p></div></div>
<div class="b" id="bn33"><div class="m1"><p>جز سنایی دگر نگفت کسی</p></div>
<div class="m2"><p>اینچنین گوهری نسفت کسی</p></div></div>
<div class="b" id="bn34"><div class="m1"><p> هست معنیش اندرون حجاب</p></div>
<div class="m2"><p> چون عروس زمشک بسته نقاب</p></div></div>
<div class="b" id="bn35"><div class="m1"><p> نخچوان راکه فخر هر طرفست</p></div>
<div class="m2"><p> در جهانش بدین سخن شرفست</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>در مقامی که این سخن خوانند</p></div>
<div class="m2"><p> عقل و جان سحر مطلقش دانند</p></div></div>
<div class="b" id="bn37"><div class="m1"><p> خاکیان جان نثار او سازند</p></div>
<div class="m2"><p> قدسیان خرقه‌ها در اندازند</p></div></div>
<div class="b" id="bn38"><div class="m1"><p> این زمان بهر عزت و تمکین</p></div>
<div class="m2"><p> جبرئیل از فلک کند تحسین</p></div></div>
<div class="b" id="bn39"><div class="m1"><p> ختم این نظم بر سعادت باد </p></div>
<div class="m2"><p> هر نفس‌ دم به دم زیادت باد </p></div></div>