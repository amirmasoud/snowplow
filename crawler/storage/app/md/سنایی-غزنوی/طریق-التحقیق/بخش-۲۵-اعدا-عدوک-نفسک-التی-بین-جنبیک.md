---
title: >-
    بخش ۲۵ - اَعْدا عَدُوِّکَ نَفْسُکَ الَّتی بَیْنَ جَنْبَیْکَ
---
# بخش ۲۵ - اَعْدا عَدُوِّکَ نَفْسُکَ الَّتی بَیْنَ جَنْبَیْکَ

<div class="b" id="bn1"><div class="m1"><p>گر کنی قهر ازو نفیس شوی</p></div>
<div class="m2"><p>ورمرادش دهی خسیس شوی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p> وه چه ساده دلی و چه نادان</p></div>
<div class="m2"><p> که ندانی تو عصمت از عصیان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p> از صفا ت حمیده بگریزی</p></div>
<div class="m2"><p> در صفا ت ذمیمه آویزی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p> جهد آن کنیه جمله نور شوی</p></div>
<div class="m2"><p> وزصفات ذمیمه دور شوی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p> در تو هم دیوی است و هم ملکی </p></div>
<div class="m2"><p> هم زمینی به قدر و هم فلکی </p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ترک دیوی کنی ملک باشی</p></div>
<div class="m2"><p>از شرف برتر از فلک باشی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p> تا از این همنشین جدا نشوی</p></div>
<div class="m2"><p> دان که شایستهٔ خدا نشوی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p> تواز این همنشین چوگردی دور</p></div>
<div class="m2"><p> ملک باقی تراست و دارسرور</p></div></div>
<div class="b" id="bn9"><div class="m1"><p> تو ازین جایگه فرج یابی‌</p></div>
<div class="m2"><p> چون بدانجا رسی درج یابی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p> گر به اینجایت پای بست کند</p></div>
<div class="m2"><p> باسگ و خوک هم نشست‌کند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p> تاکه دیوت بود به راه دلیل</p></div>
<div class="m2"><p> نکند با تو همرهی جبریل</p></div></div>
<div class="b" id="bn12"><div class="m1"><p> تا زآلایش طبیعی پاک</p></div>
<div class="m2"><p> نشوی‌، کی شوی تو برافلاک</p></div></div>
<div class="b" id="bn13"><div class="m1"><p> پهلو از قدسیان تهی چه کنی‌؟</p></div>
<div class="m2"><p> با دد و دیو همرهی چه‌کنی‌؟</p></div></div>
<div class="b" id="bn14"><div class="m1"><p> شرم بادت که با وجود ملک</p></div>
<div class="m2"><p> ننهی پا ی بر روا ق فلک</p></div></div>
<div class="b" id="bn15"><div class="m1"><p> بر زمین با ددان نشینی تو</p></div>
<div class="m2"><p> صحبت دیو و دد گزینی تو</p></div></div>
<div class="b" id="bn16"><div class="m1"><p> ترک یوسف کنی زبی نظری</p></div>
<div class="m2"><p> همدم گرگ باشی اینت خری‌!</p></div></div>
<div class="b" id="bn17"><div class="m1"><p> با رفیقان بد چه پیوندی؟</p></div>
<div class="m2"><p>زین حریفان چه طرف بربندی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p> حسد و حرص را بجای بمان</p></div>
<div class="m2"><p> برهان خویش را ازین و از آن</p></div></div>
<div class="b" id="bn19"><div class="m1"><p> گرنه یکبارگیت قهر کنند</p></div>
<div class="m2"><p> نوش در کام جانت زهر کنند</p></div></div>
<div class="b" id="bn20"><div class="m1"><p> چون از ایشان به گور فردروی</p></div>
<div class="m2"><p> به قیامت زیور مرد روی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p> چون برندت زخانه مرده به گور</p></div>
<div class="m2"><p> مرد خیزی زیور وقت نشور</p></div></div>
<div class="b" id="bn22"><div class="m1"><p> گر فرشته صفت شدی زاینجا</p></div>
<div class="m2"><p> با فرشته است حشر تو فردا</p></div></div>
<div class="b" id="bn23"><div class="m1"><p> ورتوسگ سیرتی‌به وقت‌نشور </p></div>
<div class="m2"><p> هم سگی خیزی از میانهٔ گور </p></div></div>