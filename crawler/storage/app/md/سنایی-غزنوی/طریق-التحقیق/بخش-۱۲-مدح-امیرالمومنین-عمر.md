---
title: >-
    بخش ۱۲ - مدح امیرالمومنین عمر
---
# بخش ۱۲ - مدح امیرالمومنین عمر

<div class="b" id="bn1"><div class="m1"><p>قوت دین حق زعمّر بود</p></div>
<div class="m2"><p>خانه دین بدو معمر بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p> جگر مشرکان پر از خون کرد</p></div>
<div class="m2"><p> کبرشان از دماغ بیرون کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p> از پی معدلت میان‌، اوبست</p></div>
<div class="m2"><p> کمر عدل در جهان‌، اوبست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p> عادت بدعت از جهان برداشت</p></div>
<div class="m2"><p> که کژی جز که در کمان نداشت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p> برتر از چرخ بود پایهٔ او </p></div>
<div class="m2"><p> دیو بگریختی زسایهٔ او </p></div></div>