---
title: >-
    بخش ۵۴ - فصل فی التسلیم
---
# بخش ۵۴ - فصل فی التسلیم

<div class="b" id="bn1"><div class="m1"><p>لطف او هر که را دلالت داد</p></div>
<div class="m2"><p>آخرش هدیهٔ هدایت داد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p> قهرش آن را که بد مقالت کرد</p></div>
<div class="m2"><p> هدف پاسخ ضلالت کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p> زشتی و خوبی وکم و بیشی</p></div>
<div class="m2"><p> رنج و راحت‌، غنا و درویشی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p> کردهٔ اوست جمله نیک بدان</p></div>
<div class="m2"><p> «‌یفعل الله مایشا» برخوان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p> بد و نیک تو در عمل بسته ست</p></div>
<div class="m2"><p> نقش آن جمله در ازل بسته‌ست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p> هر چه امروز پیش می‌آید </p></div>
<div class="m2"><p> همه برجای خویش می‌آید </p></div></div>