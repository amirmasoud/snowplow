---
title: >-
    بخش ۳۲ - حکایت
---
# بخش ۳۲ - حکایت

<div class="b" id="bn1"><div class="m1"><p>دوش ناگه نهفته از اغیار</p></div>
<div class="m2"><p>یافتم بر در سرایش بار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p> مجلسش زان سوی جهان دیدم</p></div>
<div class="m2"><p> دور از اندیشه و گمان دیدم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p> مجمعی دیده‌ام پر از عشاق</p></div>
<div class="m2"><p> جسته از بند گنبد زراق</p></div></div>
<div class="b" id="bn4"><div class="m1"><p> چار تکبیر کرده بر دو جهان</p></div>
<div class="m2"><p> گشته فارغ زشغل هر دو جهان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p> باده از جام معرفت خورده</p></div>
<div class="m2"><p> راه زان سوی شش جهت کرده</p></div></div>
<div class="b" id="bn6"><div class="m1"><p> همه گویای بی زبان بودند</p></div>
<div class="m2"><p> همه بی دیده‌، نقش خوان بودند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p> ماجرایی که آن زمان می‌رفت</p></div>
<div class="m2"><p> سخن الحق نه بر زبان می‌رفت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p> نکته‌ها رفت بس شگرف آنجا</p></div>
<div class="m2"><p> درنگنجید صورت و حرف آنجا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p> صوت وحرف از جهان جسم بود </p></div>
<div class="m2"><p> بهر ترکیب فعل و اسم بود </p></div></div>