---
title: >-
    بخش ۶۸ - فصل در اکل و شرب
---
# بخش ۶۸ - فصل در اکل و شرب

<div class="b" id="bn1"><div class="m1"><p>ازپی لقمه‌ای چه ترش و چه شور</p></div>
<div class="m2"><p>تاکی این گفت وگوی شیرین شور</p></div></div>
<div class="b" id="bn2"><div class="m1"><p> بر در این و آن چو سگ چه دوی‌؟</p></div>
<div class="m2"><p>گرنه‌ای سگ چنین به تک چه دوی‌</p></div></div>
<div class="b" id="bn3"><div class="m1"><p> بیش خوردن قوی کند گردن</p></div>
<div class="m2"><p> لیک زیرک شوی زکم خوردن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p> آفت علم و حکمتست شکم</p></div>
<div class="m2"><p> هرکه را خورد بیش‌، دانش کم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p> مرد باید که کم خورش باشد</p></div>
<div class="m2"><p> تا درونش به پرورش باشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p> هر چه پرسی ازو نکو داند</p></div>
<div class="m2"><p> سرهای حقیقت او داند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p> فرخ آن کاختیار او همه سال</p></div>
<div class="m2"><p>عمل صالحست و اکل حلال</p></div></div>
<div class="b" id="bn8"><div class="m1"><p> هست به نزد من در این ایام</p></div>
<div class="m2"><p> بینوا زیستن زکسب حرام</p></div></div>
<div class="b" id="bn9"><div class="m1"><p> چونکه جان را زعشق قوت بود</p></div>
<div class="m2"><p> قوت از حی لایموت بود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p> ای عزیز این همه ذلیلی چیست‌؟</p></div>
<div class="m2"><p> وی سبک روح، این ثقیلی چیست‌</p></div></div>
<div class="b" id="bn11"><div class="m1"><p> شکم از لوت چار سو چه کنی‌؟</p></div>
<div class="m2"><p> خویش را بندهٔ گلو چه کنی‌؟</p></div></div>
<div class="b" id="bn12"><div class="m1"><p> لقمه‌ای کم خوری زکسب حلال</p></div>
<div class="m2"><p> به بود از عبادت ده سال</p></div></div>
<div class="b" id="bn13"><div class="m1"><p> مرد باید که قوت جان جوید </p></div>
<div class="m2"><p> هر چه گوید همه زجان گوید </p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نظر از کام و از گلو بگسل</p></div>
<div class="m2"><p>هر چه آن نیست حق‌، ازو بگسل</p></div></div>
<div class="b" id="bn15"><div class="m1"><p> تا تو در بند آرزو باشی</p></div>
<div class="m2"><p> پر بار خسان دوتو باشی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p> چون تو از آرزو بتابی روی</p></div>
<div class="m2"><p> آرزو در پی‌ات کند تک و پوی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p> به حقیقت بدان که ایزد فرد </p></div>
<div class="m2"><p> در ازل روزی‌ات مقدر کرد </p></div></div>