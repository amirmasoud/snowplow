---
title: >-
    بخش ۶۵ - اولئک کالانعام بل هم اضل واُ‌ولئک هم الغافلون
---
# بخش ۶۵ - اولئک کالانعام بل هم اضل واُ‌ولئک هم الغافلون

<div class="b" id="bn1"><div class="m1"><p>رنگ و بویی که در جان بینی</p></div>
<div class="m2"><p>گر همه سود وگر زیان بینی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صلح با عدل و جنگ باستم‌است</p></div>
<div class="m2"><p>با بدی نیک و با نشاط غم است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رهروان را ازآن چه نفع وچه ضر</p></div>
<div class="m2"><p>گر همه خیر باشد ار همه شر،</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عالم دیگر است عالمشان</p></div>
<div class="m2"><p>نیست فرقی زمور تا جمشان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در جهان جز به دیدهٔ عبرت</p></div>
<div class="m2"><p>ننگرند اینت غایت همّت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خاطر از هیچ کس نرنجدشان</p></div>
<div class="m2"><p>هر دو عالم جوی نسنجد شان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر که او لذت جهان جوید</p></div>
<div class="m2"><p>روز و شب در پی جهان پوید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زوگریزان جهان و او پویان</p></div>
<div class="m2"><p>همچو دیوانگان جهان جویان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نتواند بدان جهان پیوست</p></div>
<div class="m2"><p>زین جهان باد دارد اندر دست</p></div></div>