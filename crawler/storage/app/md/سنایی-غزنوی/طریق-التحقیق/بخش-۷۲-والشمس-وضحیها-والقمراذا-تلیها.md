---
title: >-
    بخش ۷۲ - والشمس وضحیها والقمراذا تلیها
---
# بخش ۷۲ - والشمس وضحیها والقمراذا تلیها

<div class="b" id="bn1"><div class="m1"><p>ای مسلم‌ترا سحر خیزی</p></div>
<div class="m2"><p>هر سحر چون زخواب برخیزی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p> سر زبالین شرق برداری </p></div>
<div class="m2"><p> دامن وجیب پر ز زرداری </p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پس کنی در جهان زرافشانی</p></div>
<div class="m2"><p>بر فقیر و توانگر افشانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p> چون زنی بر فلک سراپرده</p></div>
<div class="m2"><p> بندی از نور در هوا پرده</p></div></div>
<div class="b" id="bn5"><div class="m1"><p> در هوا ذره را کنی تعریف</p></div>
<div class="m2"><p> بدن خاک را دهی تشریف</p></div></div>
<div class="b" id="bn6"><div class="m1"><p> چون در آیی به بارگاه حمل</p></div>
<div class="m2"><p> بنمایی هزار گوه عمل</p></div></div>
<div class="b" id="bn7"><div class="m1"><p> پور حسن بر جهان بندی</p></div>
<div class="m2"><p> نقش دیبای گلستان بندی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p> برقع از روی غنچه بگشایی</p></div>
<div class="m2"><p> چهرهٔ یاسمن‌ بیارایی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p> در چمن سبزه تازه روی شود</p></div>
<div class="m2"><p> گلستان پر ز رنگ و بوی شود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p> قدح لاله پر شراب کنی</p></div>
<div class="m2"><p> عارض ارغوان خضاب کنی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p> چون‌کنی یک نظر سوی معدن</p></div>
<div class="m2"><p> خاک گردد به گوهر آبستن</p></div></div>
<div class="b" id="bn12"><div class="m1"><p> در رحم جنبش جنین از توست</p></div>
<div class="m2"><p> ماه را پرتو جبین از توست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p> تو رسانی همی به هفت اقلیم</p></div>
<div class="m2"><p> از هزاران هزار گونه نعیم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p> در نظر شاهد ملیح تویی</p></div>
<div class="m2"><p> بر فلک همدم مسیح تویی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p> یوسف مصر آسمانی‌، تو</p></div>
<div class="m2"><p> کدخدای همه جهانی تو</p></div></div>
<div class="b" id="bn16"><div class="m1"><p> ایتت عزف که صانع عالم</p></div>
<div class="m2"><p> بر وجود تو یاد کرد قسم‌!</p></div></div>
<div class="b" id="bn17"><div class="m1"><p> مردم چشم عالمی به درست</p></div>
<div class="m2"><p> که جهان سر به سر منیراز توست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p> با وجود تو ای جهان آرای</p></div>
<div class="m2"><p>از چه رو اندرین سپنج سرای‌،</p></div></div>
<div class="b" id="bn19"><div class="m1"><p> روز من‌، خسته تیره فام بود</p></div>
<div class="m2"><p> صبح بر چشم من حرام بود</p></div></div>
<div class="b" id="bn20"><div class="m1"><p> چیست جرمم چه کرده‌ام باری‌؟</p></div>
<div class="m2"><p> که نهی هر دمم زنوخاری</p></div></div>
<div class="b" id="bn21"><div class="m1"><p> مژهٔ من زموج خون جگر </p></div>
<div class="m2"><p> همچو دامان ابر داری‌تر </p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چون منی را چنین حزین داری</p></div>
<div class="m2"><p>با غم و غصه همنشین داری</p></div></div>
<div class="b" id="bn23"><div class="m1"><p> عادت چون تویی چنین باشد</p></div>
<div class="m2"><p> جگرم خون کنی همین باشد</p></div></div>
<div class="b" id="bn24"><div class="m1"><p> نه‌، خطا گفتم‌، از تو این ناید</p></div>
<div class="m2"><p> چون تو مهری‌، زمهرکین ناید</p></div></div>
<div class="b" id="bn25"><div class="m1"><p> این همه جور دور گردون است</p></div>
<div class="m2"><p> اوکند اینکه اینچنین دون است</p></div></div>
<div class="b" id="bn26"><div class="m1"><p> نه منم اینچنین بدین آیین</p></div>
<div class="m2"><p>خسته و مستمند و زار و حزین‌،</p></div></div>
<div class="b" id="bn27"><div class="m1"><p> عالمی را همه چنین بینی</p></div>
<div class="m2"><p> همه را با عنا قرین بینی</p></div></div>
<div class="b" id="bn28"><div class="m1"><p> گشته از حادثات دور فلک </p></div>
<div class="m2"><p> سینه‌شان پرزخون زجور فلک </p></div></div>