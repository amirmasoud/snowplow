---
title: >-
    بخش ۱۳ - مدح امیرالمومنین عثمان
---
# بخش ۱۳ - مدح امیرالمومنین عثمان

<div class="b" id="bn1"><div class="m1"><p>دین شرف یافته و دنیا زین</p></div>
<div class="m2"><p>از جمال وجود ذوالنورین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p> منبع جود و جامع قرآن</p></div>
<div class="m2"><p> صد ف در مکرمت عثمان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p> آنکه همت ورای کیوان داشت</p></div>
<div class="m2"><p> جیب جان نور نقد قرآن داشت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p> طلعتش بوده نور هر دیده</p></div>
<div class="m2"><p> سرمهٔ شرم داشت در دیده</p></div></div>
<div class="b" id="bn5"><div class="m1"><p> دلش از حرص و حقد خالی بود </p></div>
<div class="m2"><p> فلکش اختر معالی بود </p></div></div>