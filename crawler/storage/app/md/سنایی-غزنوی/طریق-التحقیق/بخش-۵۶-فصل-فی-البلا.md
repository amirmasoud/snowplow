---
title: >-
    بخش ۵۶ - فصل فی البلا
---
# بخش ۵۶ - فصل فی البلا

<div class="b" id="bn1"><div class="m1"><p>عاشقان را غذا بلا باشد</p></div>
<div class="m2"><p>عاشقی بی بلا کجا باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p> لقمه از سفرهٔ بلا خوردند</p></div>
<div class="m2"><p> می‌زمیخانهٔ رضا خوردند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هرکه را در جهان بلا دادند</p></div>
<div class="m2"><p>اولش شربت رضا دادند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p> نزد آن کس‌که در ره آمد مرد</p></div>
<div class="m2"><p> رنج و راحت یکیست و دارو و درد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p> رهروان از بلا نپرهیزند </p></div>
<div class="m2"><p> چون بلا رخ نمود نگریزند </p></div></div>