---
title: >-
    بخش ۵۰ - شرف المؤمن استغناؤه عن الناس
---
# بخش ۵۰ - شرف المؤمن استغناؤه عن الناس

<div class="b" id="bn1"><div class="m1"><p>جهد آن کن که سرفراز شوی</p></div>
<div class="m2"><p>ور در حلق بی‌ نیاز شوی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر در این و آن به هرزه مپوی</p></div>
<div class="m2"><p>وز در حلق آبروی مجوی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p> عزت از حضرت خدای طلب </p></div>
<div class="m2"><p>منصب و جاه آن سرای‌ طلب </p></div></div>