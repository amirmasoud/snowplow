---
title: >-
    رباعی شمارهٔ ۹۲
---
# رباعی شمارهٔ ۹۲

<div class="b" id="bn1"><div class="m1"><p>ای بی‌تو فراخای جهان بر ما تنگ</p></div>
<div class="m2"><p>ما را به تو فخرست و تو را از ما ننگ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ما با تو به صلحیم و تو را با ما جنگ</p></div>
<div class="m2"><p>آخر بنگویی که دلست این یا سنگ؟</p></div></div>