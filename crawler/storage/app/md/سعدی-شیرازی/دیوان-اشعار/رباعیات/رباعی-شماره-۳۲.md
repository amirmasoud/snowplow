---
title: >-
    رباعی شمارهٔ ۳۲
---
# رباعی شمارهٔ ۳۲

<div class="b" id="bn1"><div class="m1"><p>نوروز که سیل در کمر می‌گردد</p></div>
<div class="m2"><p>سنگ از سر کوهسار در می‌گردد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از چشمهٔ چشم ما برفت اینهمه سیل</p></div>
<div class="m2"><p>گویی که دل تو سخت‌تر می‌گردد</p></div></div>