---
title: >-
    رباعی شمارهٔ ۷۹
---
# رباعی شمارهٔ ۷۹

<div class="b" id="bn1"><div class="m1"><p>از هرچه کنی مرهم ریش اولیتر</p></div>
<div class="m2"><p>دلداری خلق هرچه بیش اولیتر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای دوست به دست دشمنانم مسپار</p></div>
<div class="m2"><p>گر می‌کشیم به دست خویش اولیتر</p></div></div>