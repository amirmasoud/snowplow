---
title: >-
    رباعی شمارهٔ ۱۳۹
---
# رباعی شمارهٔ ۱۳۹

<div class="b" id="bn1"><div class="m1"><p>ماها همه شیرینی و لطف و نمکی</p></div>
<div class="m2"><p>نه ماه زمین که آفتاب فلکی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو آدمیی و دیگران آدمیند؟</p></div>
<div class="m2"><p>نی‌نی تو که خط سبز داری ملکی</p></div></div>