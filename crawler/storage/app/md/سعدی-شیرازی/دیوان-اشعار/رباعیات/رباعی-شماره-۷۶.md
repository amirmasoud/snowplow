---
title: >-
    رباعی شمارهٔ ۷۶
---
# رباعی شمارهٔ ۷۶

<div class="b" id="bn1"><div class="m1"><p>امشب نه بیاض روز برمی‌آید</p></div>
<div class="m2"><p>نه نالهٔ مرغان سحر می‌آید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیدار همه شب و نظر بر سر کوه</p></div>
<div class="m2"><p>تا صبح کی از سنگ به در می‌آید</p></div></div>