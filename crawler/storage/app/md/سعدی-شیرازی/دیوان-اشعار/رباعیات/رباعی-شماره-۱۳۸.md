---
title: >-
    رباعی شمارهٔ ۱۳۸
---
# رباعی شمارهٔ ۱۳۸

<div class="b" id="bn1"><div class="m1"><p>ای پیش تو لعبتان چینی حبشی</p></div>
<div class="m2"><p>کس چون تو صنوبر نخرامد به کشی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر روی بگردانی و گر سر بکشی</p></div>
<div class="m2"><p>ما با تو خوشیم گر تو با ما نه خوشی</p></div></div>