---
title: >-
    رباعی شمارهٔ ۳۴
---
# رباعی شمارهٔ ۳۴

<div class="b" id="bn1"><div class="m1"><p>دستارچه‌ای کان بت دلبر دارد</p></div>
<div class="m2"><p>گر بویی ازان باد صبا بردارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر مردهٔ صد ساله اگر برگذرد</p></div>
<div class="m2"><p>در حال ز خاک تیره سر بردارد</p></div></div>