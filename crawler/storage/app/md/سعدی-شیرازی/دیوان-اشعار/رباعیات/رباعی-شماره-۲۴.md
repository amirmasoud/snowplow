---
title: >-
    رباعی شمارهٔ ۲۴
---
# رباعی شمارهٔ ۲۴

<div class="b" id="bn1"><div class="m1"><p>گر دل به کسی دهند باری به تو دوست</p></div>
<div class="m2"><p>کت خوی خوش و بوی خوش و روی نکوست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از هر که وجود صبر بتوانم کرد</p></div>
<div class="m2"><p>الا ز وجودت که وجودم همه اوست</p></div></div>