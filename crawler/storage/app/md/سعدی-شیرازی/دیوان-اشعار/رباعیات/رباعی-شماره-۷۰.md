---
title: >-
    رباعی شمارهٔ ۷۰
---
# رباعی شمارهٔ ۷۰

<div class="b" id="bn1"><div class="m1"><p>سودای تو از سرم به در می‌نرود</p></div>
<div class="m2"><p>نقشت ز برابر نظر می‌نرود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>افسوس که در پای تو ای سرو روان</p></div>
<div class="m2"><p>سر می‌رود و بی‌تو به سر می‌نرود</p></div></div>