---
title: >-
    رباعی شمارهٔ ۸۰
---
# رباعی شمارهٔ ۸۰

<div class="b" id="bn1"><div class="m1"><p>ای دست جفای تو چو زلف تو دراز</p></div>
<div class="m2"><p>وی بی‌سببی گرفته پای از من باز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای دست از آستین برون کرده به عهد</p></div>
<div class="m2"><p>وامروز کشیده پای در دامن باز</p></div></div>