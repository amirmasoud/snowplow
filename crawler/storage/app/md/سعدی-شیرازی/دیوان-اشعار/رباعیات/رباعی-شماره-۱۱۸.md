---
title: >-
    رباعی شمارهٔ ۱۱۸
---
# رباعی شمارهٔ ۱۱۸

<div class="b" id="bn1"><div class="m1"><p>مه را ز فلک به طرف بام آوردن</p></div>
<div class="m2"><p>وز روم، کلیسیا به شام آوردن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در وقت سحر نماز شام آوردن</p></div>
<div class="m2"><p>بتوان، نتوان تو را به دام آوردن</p></div></div>