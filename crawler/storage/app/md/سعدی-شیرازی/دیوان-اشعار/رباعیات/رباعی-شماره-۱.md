---
title: >-
    رباعی شمارهٔ ۱
---
# رباعی شمارهٔ ۱

<div class="b" id="bn1"><div class="m1"><p>هر ساعتم اندرون بجوشد خون را</p></div>
<div class="m2"><p>وآگاهی نیست مردم بیرون را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>الا مگر آن‌که روی لیلی دیده‌ست</p></div>
<div class="m2"><p>داند که چه درد می‌کشد مجنون را</p></div></div>