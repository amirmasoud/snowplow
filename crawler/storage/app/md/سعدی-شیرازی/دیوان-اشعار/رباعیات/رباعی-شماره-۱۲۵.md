---
title: >-
    رباعی شمارهٔ ۱۲۵
---
# رباعی شمارهٔ ۱۲۵

<div class="b" id="bn1"><div class="m1"><p>ما را نه ترنج از تو مرادست نه به</p></div>
<div class="m2"><p>تو خود شکری پسته و بادام مده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر نار ز پستان تو که باشد و مه</p></div>
<div class="m2"><p>هرگز نبود به از زنخدان تو به</p></div></div>