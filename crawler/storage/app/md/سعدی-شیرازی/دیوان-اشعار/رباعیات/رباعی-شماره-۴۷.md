---
title: >-
    رباعی شمارهٔ ۴۷
---
# رباعی شمارهٔ ۴۷

<div class="b" id="bn1"><div class="m1"><p>بیچاره کسی که بر تو مفتون باشد</p></div>
<div class="m2"><p>دور از تو گرش دلیست پر خون باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن کش نفسی قرار بی‌روی تو نیست</p></div>
<div class="m2"><p>اندیش که بی‌تو مدتی چون باشد</p></div></div>