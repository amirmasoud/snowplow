---
title: >-
    رباعی شمارهٔ ۷۵
---
# رباعی شمارهٔ ۷۵

<div class="b" id="bn1"><div class="m1"><p>این ریش تو سخت زود برمی‌آید</p></div>
<div class="m2"><p>گرچه نه مراد بود برمی‌آید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر آتش رخسار تو دلهای کباب</p></div>
<div class="m2"><p>از بس که بسوخت دود برمی‌آید</p></div></div>