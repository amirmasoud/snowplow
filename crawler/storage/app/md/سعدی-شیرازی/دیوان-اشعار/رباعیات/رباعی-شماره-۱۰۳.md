---
title: >-
    رباعی شمارهٔ ۱۰۳
---
# رباعی شمارهٔ ۱۰۳

<div class="b" id="bn1"><div class="m1"><p>مندیش که سست عهد و بدپیمانم</p></div>
<div class="m2"><p>وز دوستیت فرار گیرد جانم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هرچند که به خط جمال منسوخ شود</p></div>
<div class="m2"><p>من خط تو همچنان زنخ می‌خوانم</p></div></div>