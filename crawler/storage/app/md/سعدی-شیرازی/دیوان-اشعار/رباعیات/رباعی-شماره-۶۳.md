---
title: >-
    رباعی شمارهٔ ۶۳
---
# رباعی شمارهٔ ۶۳

<div class="b" id="bn1"><div class="m1"><p>مجنون اگر احتمال لیلی نکند</p></div>
<div class="m2"><p>شاید که به صدق عشق دعوی نکند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در مذهب عشق هر که جانی دارد</p></div>
<div class="m2"><p>روی دل ازو به هر که دنیی نکند</p></div></div>