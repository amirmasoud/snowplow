---
title: >-
    رباعی شمارهٔ ۶۵
---
# رباعی شمارهٔ ۶۵

<div class="b" id="bn1"><div class="m1"><p>مردان نه بهشت و رنگ و بو می‌خواهند</p></div>
<div class="m2"><p>یا موی خوش و روی نکو می‌خواهند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یاری دارند مثل و مانندش نیست</p></div>
<div class="m2"><p>در دنیی و آخرت هم او می‌خواهند</p></div></div>