---
title: >-
    رباعی شمارهٔ ۷۷
---
# رباعی شمارهٔ ۷۷

<div class="b" id="bn1"><div class="m1"><p>هرچند که هست عالم از خوبان پر</p></div>
<div class="m2"><p>شیرازی و کازرونی و دشتی و لر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مولای منست آن عربی‌زادهٔ حر</p></div>
<div class="m2"><p>کاخر به دهان حلو می‌گوید مر</p></div></div>