---
title: >-
    رباعی شمارهٔ ۱۳۱
---
# رباعی شمارهٔ ۱۳۱

<div class="b" id="bn1"><div class="m1"><p>هرگز بود آدمی بدین زیبایی؟</p></div>
<div class="m2"><p>یا سرو بدین بلند و خوش بالایی؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مسکین دل آنکه از برش برخیزی</p></div>
<div class="m2"><p>خرم تن آنکه از درش بازآیی</p></div></div>