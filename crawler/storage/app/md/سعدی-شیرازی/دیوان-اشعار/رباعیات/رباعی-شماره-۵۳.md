---
title: >-
    رباعی شمارهٔ ۵۳
---
# رباعی شمارهٔ ۵۳

<div class="b" id="bn1"><div class="m1"><p>روزی نظرش بر من درویش آمد</p></div>
<div class="m2"><p>دیدم که معلم بداندیش آمد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نگذاشت که آفتاب بر من تابد</p></div>
<div class="m2"><p>آن سایه گران چو ابر در پیش آمد</p></div></div>