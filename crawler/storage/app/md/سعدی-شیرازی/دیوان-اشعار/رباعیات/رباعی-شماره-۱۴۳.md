---
title: >-
    رباعی شمارهٔ ۱۴۳
---
# رباعی شمارهٔ ۱۴۳

<div class="b" id="bn1"><div class="m1"><p>ای کودک لشکری که لشکر شکنی</p></div>
<div class="m2"><p>تا کی دل ما چو قلب کافر شکنی؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن را که تو تازیانه بر سر شکنی</p></div>
<div class="m2"><p>به زانکه ببینی و عنان برشکنی</p></div></div>