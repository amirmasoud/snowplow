---
title: >-
    رباعی شمارهٔ ۱۰۲
---
# رباعی شمارهٔ ۱۰۲

<div class="b" id="bn1"><div class="m1"><p>آن رفته که بود دل بدو مشغولم</p></div>
<div class="m2"><p>وافکنده به شمشیر جفا مقتولم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بازآمد و آن رونق پارینش نیست</p></div>
<div class="m2"><p>خط خویشتن آورد که من مغرولم</p></div></div>