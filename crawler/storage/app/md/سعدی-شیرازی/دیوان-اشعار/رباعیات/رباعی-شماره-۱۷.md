---
title: >-
    رباعی شمارهٔ ۱۷
---
# رباعی شمارهٔ ۱۷

<div class="b" id="bn1"><div class="m1"><p>خیزم بروم چو صبر نامحتملست</p></div>
<div class="m2"><p>جان در قدمش کنم که آرام دلست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>و اقرار کنم برابر دشمن و دوست</p></div>
<div class="m2"><p>کانکس که مرا بکشت از من بحلست</p></div></div>