---
title: >-
    رباعی شمارهٔ ۱۳۳
---
# رباعی شمارهٔ ۱۳۳

<div class="b" id="bn1"><div class="m1"><p>کی دانستم که بیخطا برگردی؟</p></div>
<div class="m2"><p>برگشتی و خون مستمندان خوردی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بالله اگر آنکه خط کشتن دارد</p></div>
<div class="m2"><p>آن جور پسندد که تو بی‌خط کردی</p></div></div>