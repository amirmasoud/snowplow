---
title: >-
    رباعی شمارهٔ ۳۳
---
# رباعی شمارهٔ ۳۳

<div class="b" id="bn1"><div class="m1"><p>کس عهد وفا چنانکه پروانهٔ خرد</p></div>
<div class="m2"><p>با دوست به پایان نشنیدیم که برد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مقراض به دشمنی سرش برمی‌داشت</p></div>
<div class="m2"><p>پروانه به دوستیش در پا می‌مرد</p></div></div>