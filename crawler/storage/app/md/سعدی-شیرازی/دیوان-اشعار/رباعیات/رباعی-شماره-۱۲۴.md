---
title: >-
    رباعی شمارهٔ ۱۲۴
---
# رباعی شمارهٔ ۱۲۴

<div class="b" id="bn1"><div class="m1"><p>یک روز به اتفاق صحرا من و تو</p></div>
<div class="m2"><p>از شهر برون شویم تنها من و تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دانی که من و تو کی به هم خوش باشیم؟</p></div>
<div class="m2"><p>آنوقت که کس نباشد الا من و تو</p></div></div>