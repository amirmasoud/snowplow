---
title: >-
    رباعی شمارهٔ ۱۸
---
# رباعی شمارهٔ ۱۸

<div class="b" id="bn1"><div class="m1"><p>آن ماه که گفتی ملک رحمانست</p></div>
<div class="m2"><p>این بار اگرش نگه کنی شیطانست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رویی که چو آتش به زمستان خوش بود</p></div>
<div class="m2"><p>امروز چو پوستین به تابستانست</p></div></div>