---
title: >-
    رباعی شمارهٔ ۴
---
# رباعی شمارهٔ ۴

<div class="b" id="bn1"><div class="m1"><p>چون دل ز هوای دوست نتوان پرداخت</p></div>
<div class="m2"><p>درمانش تحمل است و سر پیش انداخت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یا ترک گل لعل همی باید گفت</p></div>
<div class="m2"><p>یا با الم خار همی باید ساخت</p></div></div>