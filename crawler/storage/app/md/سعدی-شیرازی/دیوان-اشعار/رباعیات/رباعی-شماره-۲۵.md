---
title: >-
    رباعی شمارهٔ ۲۵
---
# رباعی شمارهٔ ۲۵

<div class="b" id="bn1"><div class="m1"><p>گر زخم خورم ز دست چون مرهم دوست</p></div>
<div class="m2"><p>یا مغز برآیدم چو بادام از پوست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غیرت نگذاردم که نالم به کسی</p></div>
<div class="m2"><p>تا خلق ندانند که منظور من اوست</p></div></div>