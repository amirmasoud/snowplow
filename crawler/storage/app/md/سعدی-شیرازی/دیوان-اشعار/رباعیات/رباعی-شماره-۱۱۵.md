---
title: >-
    رباعی شمارهٔ ۱۱۵
---
# رباعی شمارهٔ ۱۱۵

<div class="b" id="bn1"><div class="m1"><p>یاران به سماع نای و نی جامه‌دران</p></div>
<div class="m2"><p>ما، دیده به جایی متحیر نگران</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عشق آن منست و لهو از آن دگران</p></div>
<div class="m2"><p>من چشم برین کنم شما گوش بر آن</p></div></div>