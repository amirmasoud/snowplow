---
title: >-
    رباعی شمارهٔ ۷۳
---
# رباعی شمارهٔ ۷۳

<div class="b" id="bn1"><div class="m1"><p>گر تیر جفای دشمنان می‌آید</p></div>
<div class="m2"><p>دل تنگ مکن که دوست می‌فرماید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر یار ذلیل هر ملامت کاید</p></div>
<div class="m2"><p>چون یار عزیز می‌پسندد شاید</p></div></div>