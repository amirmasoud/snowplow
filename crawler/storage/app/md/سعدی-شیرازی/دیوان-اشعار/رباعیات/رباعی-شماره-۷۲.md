---
title: >-
    رباعی شمارهٔ ۷۲
---
# رباعی شمارهٔ ۷۲

<div class="b" id="bn1"><div class="m1"><p>چون صورت خویشتن در آیینه بدید</p></div>
<div class="m2"><p>وان کام و دهان و لب و دندان لذیذ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می‌گفت چنانکه می‌توانست شنید</p></div>
<div class="m2"><p>بس جان به لب آمد که بدین لب نرسید</p></div></div>