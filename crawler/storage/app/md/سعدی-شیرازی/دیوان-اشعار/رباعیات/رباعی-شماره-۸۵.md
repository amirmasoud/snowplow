---
title: >-
    رباعی شمارهٔ ۸۵
---
# رباعی شمارهٔ ۸۵

<div class="b" id="bn1"><div class="m1"><p>رویی که نخواستم که بیند همه کس</p></div>
<div class="m2"><p>الا شب و روز پیش من باشد و بس</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پیوست به دیگران و از من ببرید</p></div>
<div class="m2"><p>یارب تو به فریاد من مسکین رس</p></div></div>