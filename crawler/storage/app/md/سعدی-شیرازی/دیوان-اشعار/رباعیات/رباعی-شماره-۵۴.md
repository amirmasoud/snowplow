---
title: >-
    رباعی شمارهٔ ۵۴
---
# رباعی شمارهٔ ۵۴

<div class="b" id="bn1"><div class="m1"><p>گفتم شب وصل و روز تعطیل آمد</p></div>
<div class="m2"><p>کان شوخ دوان دوان به تعجیل آمد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتم که نمی‌نهی رخی بر رخ من</p></div>
<div class="m2"><p>گفتا برو ابلهی مکن پیل آمد</p></div></div>