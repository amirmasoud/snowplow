---
title: >-
    شمارهٔ  ۵
---
# شمارهٔ  ۵

<div class="b" id="bn1"><div class="m1"><p>چشم تو طلسم جاودانست</p></div>
<div class="m2"><p>یا فتنهٔ آخرالزمانست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا چشم بدی به زیر بنهد</p></div>
<div class="m2"><p>دیگر به کرشمه در نهانست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ما را به کرشمه صید کردست</p></div>
<div class="m2"><p>چشمست که چو چشم آهوانست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با لشکر غمزهٔ تو در شهر</p></div>
<div class="m2"><p>( ... ) الامانست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پیکان خدنگ غمزهٔ تو</p></div>
<div class="m2"><p>شک نیست که زهر بی‌کمانست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از لعل لب شکرفشانت</p></div>
<div class="m2"><p>یک بوسه به صد هزار جانست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ارزان شده است بوسهٔ تو</p></div>
<div class="m2"><p>ارزان چه بود که رایگانست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هستم همه ساله دست بر سر</p></div>
<div class="m2"><p>چون پای فراق در میانست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گویند صبور باش سعدی</p></div>
<div class="m2"><p>این کار به گفت دیگرانست</p></div></div>