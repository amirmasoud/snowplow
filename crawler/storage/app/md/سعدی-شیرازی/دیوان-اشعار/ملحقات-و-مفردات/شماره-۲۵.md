---
title: >-
    شمارهٔ  ۲۵
---
# شمارهٔ  ۲۵

<div class="b" id="bn1"><div class="m1"><p>هر شبی با دلی و صد زاری</p></div>
<div class="m2"><p>منم و آب چشم و بیداری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بنماندست آب در جگرم</p></div>
<div class="m2"><p>بس که چشمم کند گهرباری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل تو از کجا و غم ز کجا؟</p></div>
<div class="m2"><p>تو چه دانی که چیست غمخواری؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آگه از حال من شوی آنگاه</p></div>
<div class="m2"><p>که چو من یک شبی به روز آری</p></div></div>