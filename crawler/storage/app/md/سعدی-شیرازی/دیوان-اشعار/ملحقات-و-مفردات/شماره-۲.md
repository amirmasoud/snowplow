---
title: >-
    شمارهٔ  ۲
---
# شمارهٔ  ۲

<div class="b" id="bn1"><div class="m1"><p>می‌ندانم چکنم چاره من این دستان را</p></div>
<div class="m2"><p>تا به دست آورم آن دلبر پردستان را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>او به شمشیر جفا خون دلم می‌ریزد</p></div>
<div class="m2"><p>تابه خون دل من رنگ کند دستان را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من بیچاره تهیدستم ازان می‌ترسم</p></div>
<div class="m2"><p>که وصالش ندهد دست تهیدستان را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دامن وصلش اگر من به کف آرم روزی</p></div>
<div class="m2"><p>ندهم تا به قیامت دگر از دست آن را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در صفاتش نرسد گرچه بسی شرح دهد</p></div>
<div class="m2"><p>طوطی طبع من آن بلبل پردستان را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هوس اوست دلم را چه توان گفت اگر</p></div>
<div class="m2"><p>دست بر سرو بلندش نرسد پستان را؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نرگس مست وی آزار دلم می‌طلبد</p></div>
<div class="m2"><p>آنکه در عربده می‌آورد او مستان را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر ببینم رخ خوبش نکنم میل به باغ</p></div>
<div class="m2"><p>زانکه چون عارض او نیست گلی بستان را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هر که دیدست نگارین من اندر همه عمر</p></div>
<div class="m2"><p>به تماشا نرود هیچ نگارستان را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نیست بر سعدی ازین واقعه و نیست عجب</p></div>
<div class="m2"><p>گر غم فرقت او نیست کند هستان را</p></div></div>