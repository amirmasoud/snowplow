---
title: >-
    شمارهٔ  ۸
---
# شمارهٔ  ۸

<div class="b" id="bn1"><div class="m1"><p>می‌روم با درد و حسرت از دیارت خیر باد</p></div>
<div class="m2"><p>می‌گذارم جان به خدمت یادگارت خیر باد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سر ز پیشت برنمی‌آرم ز دستور طلب</p></div>
<div class="m2"><p>شرم می‌دارم ز روی گلعذارت خیر باد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر کجا باشم دعا گویم همی بر دولتت</p></div>
<div class="m2"><p>از خدا باد آفرین بر روزگارت خیر باد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر دهد عمرم امان رویت ببینم عاقبت</p></div>
<div class="m2"><p>ور بمیرم در غریبی ز انتظارت خیر باد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر ز چین زلف تو بویی رسد بر خاک ما</p></div>
<div class="m2"><p>زنده برخیزم ز بوی مشکبارت خیر باد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر ز من یاد آوری بنویس آنجا قطعه‌ای</p></div>
<div class="m2"><p>سعدیا آن گفته‌های آبدارت خیر باد</p></div></div>