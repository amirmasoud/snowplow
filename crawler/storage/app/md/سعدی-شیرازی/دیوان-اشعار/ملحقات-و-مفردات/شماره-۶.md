---
title: >-
    شمارهٔ  ۶
---
# شمارهٔ  ۶

<div class="b" id="bn1"><div class="m1"><p>حالم از شرح غمت افسانه ایست</p></div>
<div class="m2"><p>چشمم از عکس رخت بتخانه ایست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر کجا بدگوهری در عالمست</p></div>
<div class="m2"><p>در کنار آنچنان دردانه ایست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر امید زلف چون زنجیر تو</p></div>
<div class="m2"><p>ای بسا عاقل که چون دیوانه‌ایست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفتم او را این چه زلف ( ... )</p></div>
<div class="m2"><p>گفت هان فی‌الجمله در ( ... )</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از لبش یک نکته‌ای ( ... )</p></div>
<div class="m2"><p>وز خمش یک قطره‌ای پیمانه‌ایست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>با فروغ آفتاب حسن او</p></div>
<div class="m2"><p>شمع گردون کمتر از پروانه‌ایست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نازنینا رخ چه می‌پوشی ز من</p></div>
<div class="m2"><p>آخر این مسکین کم از بیگانه‌ایست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از بت آزر حکایتها کنند</p></div>
<div class="m2"><p>بت خود اینست از ( ... )</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دل نه جای تست آخر چون کنم</p></div>
<div class="m2"><p>در جهانم خود همین ویرانه‌ایست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>این نه دل خوانند کین ( ... )</p></div>
<div class="m2"><p>این نه عشق است از ( ... )</p></div></div>