---
title: >-
    شمارهٔ  ۳
---
# شمارهٔ  ۳

<div class="b" id="bn1"><div class="m1"><p>ای مسلمانان فغان زان نرگس جادو فریب</p></div>
<div class="m2"><p>کو به یک ره برد از من صبر و آرام و شکیب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رومیانه روی دارد زنگیانه زلف و خال</p></div>
<div class="m2"><p>چون کمان چاچیان ابروی دارد پرعتیب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از عجایبهای عالم سی و دو چیز عجیب</p></div>
<div class="m2"><p>جمع می‌بینم عیان در روی او من بی حجیب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ماه و پروین تیر و زهره شمس و قوس و کاج و عاج</p></div>
<div class="m2"><p>مورد و نرگس لعل و گل، سبزی و می وصل و فریب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بان و خطمی شمع و صندل شیر و قیر و نور و نار</p></div>
<div class="m2"><p>شهد و شکر مشک و عنبر در و لؤلؤ نار و سیب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>معجزات پنج پیغمبر به رویش در پدید</p></div>
<div class="m2"><p>احمد و داود و عیسی خضر و داماد شعیب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای صنم گر من بمیرم ناچشیده زان لبان</p></div>
<div class="m2"><p>دادگر از تو بخواهد داد من روز حسیب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سعدیا از روی تحقیق این سخن نشنیده‌ای</p></div>
<div class="m2"><p>هر نشیبی را فراز و هر فرازی را نشیب</p></div></div>