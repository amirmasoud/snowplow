---
title: >-
    قطعه شمارهٔ ۹
---
# قطعه شمارهٔ ۹

<div class="b" id="bn1"><div class="m1"><p>کوه عنبر نشسته بر زنخش</p></div>
<div class="m2"><p>راست گویی بهیست مشک‌آلود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر به چنگال صوفیان افتد</p></div>
<div class="m2"><p>ندهندش مگر به شفتالود</p></div></div>