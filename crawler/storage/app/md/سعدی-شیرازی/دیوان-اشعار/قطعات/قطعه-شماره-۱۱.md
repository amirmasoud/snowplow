---
title: >-
    قطعه شمارهٔ ۱۱
---
# قطعه شمارهٔ ۱۱

<div class="b" id="bn1"><div class="m1"><p>بس ای غلام بدیع‌الجمال شیرین‌کار</p></div>
<div class="m2"><p>که سوز عشق تو انداخت در جهان آتش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به نفط گنده چه حاجت که بر دهان گیری</p></div>
<div class="m2"><p>تو را خود از لب لعلست در دهان آتش</p></div></div>