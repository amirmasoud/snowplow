---
title: >-
    قطعه شمارهٔ ۱۳
---
# قطعه شمارهٔ ۱۳

<div class="b" id="bn1"><div class="m1"><p>مرا به صورت شاهد نظر حلال بود</p></div>
<div class="m2"><p>که هرچه می‌نگرم شاهدست در نظرم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دو چشم در سر هر کس نهاده‌اند ولی</p></div>
<div class="m2"><p>تو نقش بینی و من نقشبند می‌نگرم</p></div></div>