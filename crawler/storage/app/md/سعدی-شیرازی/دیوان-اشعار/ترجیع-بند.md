---
title: >-
    ترجیع بند
---
# ترجیع بند

<div class="b" id="bn1"><div class="m1"><p>ای سرو بلند قامت دوست</p></div>
<div class="m2"><p>وه وه که شمایلت چه نیکوست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در پای لطافت تو میراد</p></div>
<div class="m2"><p>هر سرو سهی که بر لب جوست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نازک بدنی که می‌نگنجد</p></div>
<div class="m2"><p>در زیر قبا چو غنچه در پوست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مه پاره به بام اگر برآید</p></div>
<div class="m2"><p>که فرق کند که ماه یا اوست؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آن خرمن گل نه گل که باغ است</p></div>
<div class="m2"><p>نه باغ ارم که باغ مینوست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آن گوی معنبرست در جیب</p></div>
<div class="m2"><p>یا بوی دهان عنبرین بوست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در حلقهٔ صولجان زلفش</p></div>
<div class="m2"><p>بیچاره دل اوفتاده چون گوست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>می‌سوزد و همچنان هوادار</p></div>
<div class="m2"><p>می‌میرد و همچنان دعاگوست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خون دل عاشقان مشتاق</p></div>
<div class="m2"><p>در گردن دیدهٔ بلاجوست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>من بندهٔ لعبتان سیمین</p></div>
<div class="m2"><p>کاخر دل آدمی نه از روست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بسیار ملامتم بکردند</p></div>
<div class="m2"><p>کاندر پی او مرو که بدخوست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ای سخت دلان سست پیمان</p></div>
<div class="m2"><p>این شرط وفا بود که بی‌دوست</p></div></div>
<div class="b2" id="bn13"><p>بنشینم و صبر پیش گیرم</p>
<p>دنبالهٔ کار خویش گیرم</p></div>
<div class="b" id="bn14"><div class="m1"><p>در عهد تو ای نگار دلبند</p></div>
<div class="m2"><p>بس عهد که بشکنند و سوگند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>دیگر نرود به هیچ مطلوب</p></div>
<div class="m2"><p>خاطر که گرفت با تو پیوند</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>از پیش تو راه رفتنم نیست</p></div>
<div class="m2"><p>همچون مگس از برابر قند</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>عشق آمد و رسم عقل برداشت</p></div>
<div class="m2"><p>شوق آمد و بیخ صبر برکند</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>در هیچ زمانه‌ای نزاده‌ست</p></div>
<div class="m2"><p>مادر به جمال چون تو فرزند</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>باد است نصیحت رفیقان</p></div>
<div class="m2"><p>واندوه فراق کوه الوند</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>من نیستم ار کسی دگر هست</p></div>
<div class="m2"><p>از دوست به یاد دوست خرسند</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>این جور که می‌بریم تا کی؟</p></div>
<div class="m2"><p>وین صبر که می‌کنیم تا چند؟</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چون مرغ به طمع دانه در دام</p></div>
<div class="m2"><p>چون گرگ به بوی دنبه در بند</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>افتادم و مصلحت چنین بود</p></div>
<div class="m2"><p>بی بند نگیرد آدمی پند</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>مستوجب این و بیش از اینم</p></div>
<div class="m2"><p>باشد که چو مردم خردمند</p></div></div>
<div class="b2" id="bn25"><p>بنشینم و صبر پیش گیرم</p>
<p>دنبالهٔ کار خویش گیرم</p></div>
<div class="b" id="bn26"><div class="m1"><p>امروز جفا نمی‌کند کس</p></div>
<div class="m2"><p>در شهر مگر تو می‌کنی بس</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>در دام تو عاشقان گرفتار</p></div>
<div class="m2"><p>در بند تو دوستان محبس</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>یا محرقتی بنار خد</p></div>
<div class="m2"><p>من جمرتها السراج تقبس</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>صبحی که مشام جان عشاق</p></div>
<div class="m2"><p>خوشبوی کند اذا تنفس</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>استقبله و ان تولی</p></div>
<div class="m2"><p>استأنسه و ان تعبس</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>اندام تو خود حریر چین است</p></div>
<div class="m2"><p>دیگر چه کنی قبای اطلس؟</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>من در همه قولها فصیحم</p></div>
<div class="m2"><p>در وصف شمایل تو اخرس</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>جان در قدمت کنم ولیکن</p></div>
<div class="m2"><p>ترسم ننهی تو پای بر خس</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>ای صاحب حسن در وفا کوش</p></div>
<div class="m2"><p>کاین حسن وفا نکرد با کس</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>آخر به زکات تندرستی</p></div>
<div class="m2"><p>فریاد دل شکستگان رس</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>من بعد مکن چنان کز این پیش</p></div>
<div class="m2"><p>ورنه به خدا که من از این پس</p></div></div>
<div class="b2" id="bn37"><p>بنشینم و صبر پیش گیرم</p>
<p>دنبالهٔ کار خویش گیرم</p></div>
<div class="b" id="bn38"><div class="m1"><p>گفتار خوش و لبان باریک</p></div>
<div class="m2"><p>ما أطیب فاک جل باریک</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>از روی تو ماه آسمان را</p></div>
<div class="m2"><p>شرم آمد و شد هلال باریک</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>یا قاتلتی بسیف لحظ</p></div>
<div class="m2"><p>والله قتلتنی بهاتیک</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>از بهر خدا، که مالکان، جور</p></div>
<div class="m2"><p>چندین نکنند بر ممالیک</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>شاید که به پادشه بگویند</p></div>
<div class="m2"><p>ترک تو بریخت خون تاجیک</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>دانی که چه شب گذشت بر من؟</p></div>
<div class="m2"><p>لایأت بمثلها اعادیک</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>با اینهمه گر حیات باشد</p></div>
<div class="m2"><p>هم روز شود شبان تاریک</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>فی‌الجمله نماند صبر و آرام</p></div>
<div class="m2"><p>کم تزجرنی و کم اداریک</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>دردا که به خیره عمر بگذشت</p></div>
<div class="m2"><p>ای دل تو مرا نمی‌گذاری ک</p></div></div>
<div class="b2" id="bn47"><p>بنشینم و صبر پیش گیرم</p>
<p>دنبالهٔ کار خویش گیرم</p></div>
<div class="b" id="bn48"><div class="m1"><p>چشمی که نظر نگه ندارد</p></div>
<div class="m2"><p>بس فتنه که با سر دل آرد</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>آهوی کمند زلف خوبان</p></div>
<div class="m2"><p>خود را به هلاک می‌سپارد</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>فریاد ز دست نقش، فریاد</p></div>
<div class="m2"><p>و آن دست که نقش می‌نگارد</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>هرجا که مولهی چو فرهاد</p></div>
<div class="m2"><p>شیرین صفتی برو گمارد</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>کس بار مشاهدت نچیند</p></div>
<div class="m2"><p>تا تخم مجاهدت نکارد</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>نالیدن عاشقان دلسوز</p></div>
<div class="m2"><p>ناپخته مجاز می‌شمارد</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>عیبش مکنید هوشمندان</p></div>
<div class="m2"><p>گر سوخته خرمنی بزارد</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>خاری چه بود به پای مشتاق؟</p></div>
<div class="m2"><p>تیغیش بران که سر نخارد</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>حاجت به در کسیست ما را</p></div>
<div class="m2"><p>کاو حاجت کس نمی‌گزارد</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>گویند برو ز پیش جورش</p></div>
<div class="m2"><p>من می‌روم او نمی‌گذارد</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>من خود نه به اختیار خویشم</p></div>
<div class="m2"><p>گر دست ز دامنم بدارد</p></div></div>
<div class="b2" id="bn59"><p>بنشینم و صبر پیش گیرم</p>
<p>دنبالهٔ کار خویش گیرم</p></div>
<div class="b" id="bn60"><div class="m1"><p>بعد از طلب تو در سرم نیست</p></div>
<div class="m2"><p>غیر از تو به خاطر اندرم نیست</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>ره می‌ندهی که پیشت آیم</p></div>
<div class="m2"><p>وز پیش تو ره که بگذرم نیست</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>من مرغ زبون دام انسم</p></div>
<div class="m2"><p>هرچند که می‌کشی پرم نیست</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>گر چون تو پری در آدمیزاد</p></div>
<div class="m2"><p>گویند که هست باورم نیست</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>مهر از همه خلق برگرفتم</p></div>
<div class="m2"><p>جز یاد تو در تصورم نیست</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>گویند بکوش تا بیابی</p></div>
<div class="m2"><p>می‌کوشم و بخت یاورم نیست</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>قسمی که مرا نیافریدند</p></div>
<div class="m2"><p>گر جهد کنم میسرم نیست</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>ای کاش مرا نظر نبودی</p></div>
<div class="m2"><p>چون حظ نظر برابرم نیست</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>فکرم به همه جهان بگردید</p></div>
<div class="m2"><p>وز گوشهٔ صبر بهترم نیست</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>با بخت جدل نمی‌توان کرد</p></div>
<div class="m2"><p>اکنون که طریق دیگرم نیست</p></div></div>
<div class="b2" id="bn70"><p>بنشینم و صبر پیش گیرم</p>
<p>دنبالهٔ کار خویش گیرم</p></div>
<div class="b" id="bn71"><div class="m1"><p>ای دل نه هزار عهد کردی</p></div>
<div class="m2"><p>کاندر طلب هوا نگردی؟</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>کس را چه گنه تو خویشتن را</p></div>
<div class="m2"><p>بر تیغ زدی و زخم خوردی</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>دیدی که چگونه حاصل آمد</p></div>
<div class="m2"><p>از دعوی عشق روی زردی؟</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>یا دل بنهی به جور و بیداد</p></div>
<div class="m2"><p>یا قصهٔ عشق درنوردی</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>ای سیم تن سیاه گیسو</p></div>
<div class="m2"><p>کز فکر سرم سپید کردی</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>بسیار سیه، سپید کردست</p></div>
<div class="m2"><p>دوران سپهر لاجوردی</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>صلحست میان کفر و اسلام</p></div>
<div class="m2"><p>با ما تو هنوز در نبردی</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>سر بیش گران مکن، که کردیم</p></div>
<div class="m2"><p>اقرار به بندگی و خردی</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>با درد توام خوشست ازیراک</p></div>
<div class="m2"><p>هم دردی و هم دوای دردی</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>گفتی که صبور باش، هیهات</p></div>
<div class="m2"><p>دل موضع صبر بود و بردی</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>هم چاره تحملست و تسلیم</p></div>
<div class="m2"><p>ورنه به کدام جهد و مردی</p></div></div>
<div class="b2" id="bn82"><p>بنشینم و صبر پیش گیرم</p>
<p>دنبالهٔ کار خویش گیرم</p></div>
<div class="b" id="bn83"><div class="m1"><p>بگذشت و نگه نکرد با من</p></div>
<div class="m2"><p>در پای کشان، ز کبر دامن</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>دو نرگس مست نیم خوابش</p></div>
<div class="m2"><p>در پیش و به حسرت از قفا من</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>ای قبلهٔ دوستان مشتاق</p></div>
<div class="m2"><p>گر با همه آن کنی که با من</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>بسیار کسان که جان شیرین</p></div>
<div class="m2"><p>در پای تو ریزد اولا من</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>گفتم که شکایتی بخوانم</p></div>
<div class="m2"><p>از دست تو پیش پادشا من</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>کاین سخت دلی و سست مهری</p></div>
<div class="m2"><p>جرم از طرف تو بود یا من؟</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>دیدم که نه شرط مهربانیست</p></div>
<div class="m2"><p>گر بانگ برآرم از جفا من</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>گر سر برود فدای پایت</p></div>
<div class="m2"><p>دست از تو نمی‌کنم رها من</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>جز وصل توام حرام بادا</p></div>
<div class="m2"><p>حاجت که بخواهم از خدا من</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>گویندم ازو نظر بپرهیز</p></div>
<div class="m2"><p>پرهیز ندانم از قضا من</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>هرگز نشنیده‌ای که یاری</p></div>
<div class="m2"><p>بی‌یار صبور بود تا من</p></div></div>
<div class="b2" id="bn94"><p>بنشینم و صبر پیش گیرم</p>
<p>دنبالهٔ کار خویش گیرم</p></div>
<div class="b" id="bn95"><div class="m1"><p>ای روی تو آفتاب عالم</p></div>
<div class="m2"><p>انگشت نمای آل آدم</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>احیای روان مردگان را</p></div>
<div class="m2"><p>بویت نفس مسیح مریم</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>بر جان عزیزت آفرین باد</p></div>
<div class="m2"><p>بر جسم شریفت اسم اعظم</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>محبوب منی چو دیدهٔ راست</p></div>
<div class="m2"><p>ای سرو روان به ابروی خم</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>دستان که تو داری ای پریروی</p></div>
<div class="m2"><p>بس دل ببری به کف و معصم</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>تنها نه منم اسیر عشقت</p></div>
<div class="m2"><p>خلقی متعشقند و من هم</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>شیرین جهان تویی به تحقیق</p></div>
<div class="m2"><p>بگذار حدیث ما تقدم</p></div></div>
<div class="b" id="bn102"><div class="m1"><p>خوبیت مسلمست و ما را</p></div>
<div class="m2"><p>صبر از تو نمی‌شود مسلم</p></div></div>
<div class="b" id="bn103"><div class="m1"><p>تو عهد وفای خود شکستی</p></div>
<div class="m2"><p>وز جانب ما هنوز محکم</p></div></div>
<div class="b" id="bn104"><div class="m1"><p>مگذار که خستگان بمیرند</p></div>
<div class="m2"><p>دور از تو به انتظار مرهم</p></div></div>
<div class="b" id="bn105"><div class="m1"><p>بی‌ما تو به سر بری همه عمر</p></div>
<div class="m2"><p>من بی‌تو گمان مبر که یکدم</p></div></div>
<div class="b2" id="bn106"><p>بنشینم و صبر پیش گیرم</p>
<p>دنبالهٔ کار خویش گیرم</p></div>
<div class="b" id="bn107"><div class="m1"><p>گل را مبرید پیش من نام</p></div>
<div class="m2"><p>با حسن وجود آن گل اندام</p></div></div>
<div class="b" id="bn108"><div class="m1"><p>انگشت‌نمای خلق بودیم</p></div>
<div class="m2"><p>مانند هلال از آن مه تام</p></div></div>
<div class="b" id="bn109"><div class="m1"><p>بر ما همه عیب‌ها بگفتند</p></div>
<div class="m2"><p>یا قوم الی متی و حتام؟</p></div></div>
<div class="b" id="bn110"><div class="m1"><p>ما خود زده‌ایم جام بر سنگ</p></div>
<div class="m2"><p>دیگر مزنید سنگ بر جام</p></div></div>
<div class="b" id="bn111"><div class="m1"><p>آخر نگهی به سوی ما کن</p></div>
<div class="m2"><p>ای دولت خاص و حسرت عام</p></div></div>
<div class="b" id="bn112"><div class="m1"><p>بس در طلب تو دیگ سودا</p></div>
<div class="m2"><p>پختیم و هنوز کار ما خام</p></div></div>
<div class="b" id="bn113"><div class="m1"><p>درمان اسیر عشق صبرست</p></div>
<div class="m2"><p>تا خود به کجا رسد سرانجام</p></div></div>
<div class="b" id="bn114"><div class="m1"><p>من در قدم تو خاک بادم</p></div>
<div class="m2"><p>باشد که تو بر سرم نهی گام</p></div></div>
<div class="b" id="bn115"><div class="m1"><p>دور از تو شکیب چند باشد؟</p></div>
<div class="m2"><p>ممکن نشود بر آتش آرام</p></div></div>
<div class="b" id="bn116"><div class="m1"><p>در دام غمت چو مرغ وحشی</p></div>
<div class="m2"><p>می‌پیچم و سخت می‌شود دام</p></div></div>
<div class="b" id="bn117"><div class="m1"><p>من بی تو نه راضیم ولیکن</p></div>
<div class="m2"><p>چون کام نمی‌دهی به ناکام</p></div></div>
<div class="b2" id="bn118"><p>بنشینم و صبر پیش گیرم</p>
<p>دنبالهٔ کار خویش گیرم</p></div>
<div class="b" id="bn119"><div class="m1"><p>ای زلف تو هر خمی کمندی</p></div>
<div class="m2"><p>چشمت به کرشمه چشم‌بندی</p></div></div>
<div class="b" id="bn120"><div class="m1"><p>مخرام بدین صفت مبادا</p></div>
<div class="m2"><p>کز چشم بدت رسد گزندی</p></div></div>
<div class="b" id="bn121"><div class="m1"><p>ای آینه ایمنی که ناگاه</p></div>
<div class="m2"><p>در تو رسد آه دردمندی</p></div></div>
<div class="b" id="bn122"><div class="m1"><p>یا چهره بپوش یا بسوزان</p></div>
<div class="m2"><p>بر روی چو آتشت سپندی</p></div></div>
<div class="b" id="bn123"><div class="m1"><p>دیوانهٔ عشقت ای پریروی</p></div>
<div class="m2"><p>عاقل نشود به هیچ پندی</p></div></div>
<div class="b" id="bn124"><div class="m1"><p>تلخست دهان عیشم از صبر</p></div>
<div class="m2"><p>ای تنگ شکر بیار قندی</p></div></div>
<div class="b" id="bn125"><div class="m1"><p>ای سرو به قامتش چه مانی؟</p></div>
<div class="m2"><p>زیباست ولی نه هر بلندی</p></div></div>
<div class="b" id="bn126"><div class="m1"><p>گریم به امید و دشمنانم</p></div>
<div class="m2"><p>بر گریه زنند ریشخندی</p></div></div>
<div class="b" id="bn127"><div class="m1"><p>کاجی ز درم درآمدی دوست</p></div>
<div class="m2"><p>تا دیدهٔ دشمنان بکندی</p></div></div>
<div class="b" id="bn128"><div class="m1"><p>یارب چه شدی اگر به رحمت</p></div>
<div class="m2"><p>باری سوی ما نظر فکندی؟</p></div></div>
<div class="b" id="bn129"><div class="m1"><p>یکچند به خیره عمر بگذشت</p></div>
<div class="m2"><p>من بعد بر آن سرم که چندی</p></div></div>
<div class="b2" id="bn130"><p>بنشینم و صبر پیش گیرم</p>
<p>دنبالهٔ کار خویش گیرم</p></div>
<div class="b" id="bn131"><div class="m1"><p>آیا که به لب رسید جانم</p></div>
<div class="m2"><p>آوخ که ز دست شد عنانم</p></div></div>
<div class="b" id="bn132"><div class="m1"><p>کس دید چو من ضعیف هرگز</p></div>
<div class="m2"><p>کز هستی خویش درگمانم؟</p></div></div>
<div class="b" id="bn133"><div class="m1"><p>پروانه‌ام اوفتان و خیزان</p></div>
<div class="m2"><p>یکباره بسوز و وارهانم</p></div></div>
<div class="b" id="bn134"><div class="m1"><p>گر لطف کنی بجای اینم</p></div>
<div class="m2"><p>ور جور کنی سزای آنم</p></div></div>
<div class="b" id="bn135"><div class="m1"><p>جز نقش تو نیست در ضمیرم</p></div>
<div class="m2"><p>جز نام تو نیست بر زبانم</p></div></div>
<div class="b" id="bn136"><div class="m1"><p>گر تلخ کنی به دوریم عیش</p></div>
<div class="m2"><p>یادت چو شکر کند دهانم</p></div></div>
<div class="b" id="bn137"><div class="m1"><p>اسرار تو پیش کس نگویم</p></div>
<div class="m2"><p>اوصاف تو پیش کس نخوانم</p></div></div>
<div class="b" id="bn138"><div class="m1"><p>با درد تو یاوری ندارم</p></div>
<div class="m2"><p>وز دست تو مخلصی ندانم</p></div></div>
<div class="b" id="bn139"><div class="m1"><p>عاقل بجهد ز پیش شمشیر</p></div>
<div class="m2"><p>من کشتهٔ سر بر آستانم</p></div></div>
<div class="b" id="bn140"><div class="m1"><p>چون در تو نمی‌توان رسیدن</p></div>
<div class="m2"><p>به زان نبود که تا توانم</p></div></div>
<div class="b2" id="bn141"><p>بنشینم و صبر پیش گیرم</p>
<p>دنبالهٔ کار خویش گیرم</p></div>
<div class="b" id="bn142"><div class="m1"><p>آن برگ گلست یا بناگوش</p></div>
<div class="m2"><p>یا سبزه به گرد چشمهٔ نوش</p></div></div>
<div class="b" id="bn143"><div class="m1"><p>دست چو منی قیامه باشد</p></div>
<div class="m2"><p>با قامت چون تویی در آغوش</p></div></div>
<div class="b" id="bn144"><div class="m1"><p>من ماه ندیده‌ام کله‌دار</p></div>
<div class="m2"><p>من سرو ندیده‌ام قباپوش</p></div></div>
<div class="b" id="bn145"><div class="m1"><p>وز رفتن و آمدن چه گویم؟</p></div>
<div class="m2"><p>می‌آرد وجد و می‌برد هوش</p></div></div>
<div class="b" id="bn146"><div class="m1"><p>روزی دهنی به خنده بگشاد</p></div>
<div class="m2"><p>پسته، دهن تو گفت خاموش</p></div></div>
<div class="b" id="bn147"><div class="m1"><p>خاطر پی زهد و توبه می‌رفت</p></div>
<div class="m2"><p>عشق آمد و گفت زرق مفروش</p></div></div>
<div class="b" id="bn148"><div class="m1"><p>مستغرق یادت آنچنانم</p></div>
<div class="m2"><p>کم هستی خویش شد فراموش</p></div></div>
<div class="b" id="bn149"><div class="m1"><p>یاران به نصیحتم چه گویند</p></div>
<div class="m2"><p>بنشین و صبور باش و مخروش</p></div></div>
<div class="b" id="bn150"><div class="m1"><p>ای خام من اینچنین بر آتش</p></div>
<div class="m2"><p>عیبم مکن ار برآورم جوش</p></div></div>
<div class="b" id="bn151"><div class="m1"><p>تا جهد بود به جان بکوشم</p></div>
<div class="m2"><p>وانگه به ضرورت از بن گوش</p></div></div>
<div class="b2" id="bn152"><p>بنشینم و صبر پیش گیرم</p>
<p>دنبالهٔ کار خویش گیرم</p></div>
<div class="b" id="bn153"><div class="m1"><p>طاقت برسید و هم بگفتم</p></div>
<div class="m2"><p>عشقت که ز خلق می‌نهفتم</p></div></div>
<div class="b" id="bn154"><div class="m1"><p>طاقم ز فراق و صبر و آرام</p></div>
<div class="m2"><p>زآن روز که با غم تو جفتم</p></div></div>
<div class="b" id="bn155"><div class="m1"><p>آهنگ دراز شب ز من پرس</p></div>
<div class="m2"><p>کز فرقت تو دمی نخفتم</p></div></div>
<div class="b" id="bn156"><div class="m1"><p>بر هر مژه قطره‌ای چو الماس</p></div>
<div class="m2"><p>دارم که به گریه سنگ سفتم</p></div></div>
<div class="b" id="bn157"><div class="m1"><p>گر کشته شوم عجب مدارید</p></div>
<div class="m2"><p>من خود ز حیات در شگفتم</p></div></div>
<div class="b" id="bn158"><div class="m1"><p>تقدیر درین میانم انداخت</p></div>
<div class="m2"><p>چندانکه کناره می‌گرفتم</p></div></div>
<div class="b" id="bn159"><div class="m1"><p>دی بر سر کوی دوست لختی</p></div>
<div class="m2"><p>خاک قدمش به دیده رفتم</p></div></div>
<div class="b" id="bn160"><div class="m1"><p>نه خوارترم ز خاک بگذار</p></div>
<div class="m2"><p>تا در قدم عزیزش افتم</p></div></div>
<div class="b" id="bn161"><div class="m1"><p>زانگه که برفتی از کنارم</p></div>
<div class="m2"><p>صبر از دل ریش گفت رفتم</p></div></div>
<div class="b" id="bn162"><div class="m1"><p>می‌رفت و به کبر و ناز می‌گفت</p></div>
<div class="m2"><p>بی‌ما چه کنی؟ به لابه گفتم</p></div></div>
<div class="b2" id="bn163"><p>بنشینم و صبر پیش گیرم</p>
<p>دنبالهٔ کار خویش گیرم</p></div>
<div class="b" id="bn164"><div class="m1"><p>باری بگذر که در فراقت</p></div>
<div class="m2"><p>خون شد دل ریش از اشتیاقت</p></div></div>
<div class="b" id="bn165"><div class="m1"><p>بگشای دهن که پاسخ تلخ</p></div>
<div class="m2"><p>گویی شکرست در مذاقت</p></div></div>
<div class="b" id="bn166"><div class="m1"><p>در کشتهٔ خویشتن نگه کن</p></div>
<div class="m2"><p>روزی اگر افتد اتفاقت</p></div></div>
<div class="b" id="bn167"><div class="m1"><p>تو خنده زنان چو شمع و خلقی</p></div>
<div class="m2"><p>پروانه صفت در احتراقت</p></div></div>
<div class="b" id="bn168"><div class="m1"><p>ما خود ز کدام خیل باشیم</p></div>
<div class="m2"><p>تا خیمه زنیم در وثاقت؟</p></div></div>
<div class="b" id="bn169"><div class="m1"><p>ما اخترت صبابتی ولکن</p></div>
<div class="m2"><p>عینی نظرت و ما اطاقت</p></div></div>
<div class="b" id="bn170"><div class="m1"><p>بس دیده که شد در انتظارت</p></div>
<div class="m2"><p>دریا و نمی‌رسد به ساقت</p></div></div>
<div class="b" id="bn171"><div class="m1"><p>تو مست شراب و خواب و ما را</p></div>
<div class="m2"><p>بیخوابی کشت در تیاقت</p></div></div>
<div class="b" id="bn172"><div class="m1"><p>نه قدرت با تو بودنم هست</p></div>
<div class="m2"><p>نه طاقت آنکه در فراقت</p></div></div>
<div class="b2" id="bn173"><p>بنشینم و صبر پیش گیرم</p>
<p>دنبالهٔ کار خویش گیرم</p></div>
<div class="b" id="bn174"><div class="m1"><p>آوخ که چو روزگار برگشت</p></div>
<div class="m2"><p>از من دل و صبر و یار برگشت</p></div></div>
<div class="b" id="bn175"><div class="m1"><p>برگشتن ما ضرورتی بود</p></div>
<div class="m2"><p>وآن شوخ به اختیار برگشت</p></div></div>
<div class="b" id="bn176"><div class="m1"><p>پرورده بدم به روزگارش</p></div>
<div class="m2"><p>خو کرد و چو روزگار برگشت</p></div></div>
<div class="b" id="bn177"><div class="m1"><p>غم نیز چه بودی ار برفتی</p></div>
<div class="m2"><p>آن روز که غمگسار برگشت</p></div></div>
<div class="b" id="bn178"><div class="m1"><p>رحمت کن اگر شکسته‌ای را</p></div>
<div class="m2"><p>صبر از دل بیقرار برگشت</p></div></div>
<div class="b" id="bn179"><div class="m1"><p>عذرش بنه ار به زیر سنگی</p></div>
<div class="m2"><p>سر کوفته‌ای چو مار برگشت</p></div></div>
<div class="b" id="bn180"><div class="m1"><p>زین بحر عمیق جان به در برد</p></div>
<div class="m2"><p>آنکس که هم از کنار برگشت</p></div></div>
<div class="b" id="bn181"><div class="m1"><p>من ساکن خاک پاک عشقم</p></div>
<div class="m2"><p>نتوانم ازین دیار برگشت</p></div></div>
<div class="b" id="bn182"><div class="m1"><p>بیچارگیست چارهٔ عشق</p></div>
<div class="m2"><p>دانی چه کنم چو یار برگشت؟</p></div></div>
<div class="b2" id="bn183"><p>بنشینم و صبر پیش گیرم</p>
<p>دنبالهٔ کار خویش گیرم</p></div>
<div class="b" id="bn184"><div class="m1"><p>هر دل که به عاشقی زبون نیست</p></div>
<div class="m2"><p>دست خوش روزگار دون نیست</p></div></div>
<div class="b" id="bn185"><div class="m1"><p>جز دیدهٔ شوخ عاشقان را</p></div>
<div class="m2"><p>بر چهره دوان سرشک خون نیست</p></div></div>
<div class="b" id="bn186"><div class="m1"><p>کوته نظری به خلوتم گفت</p></div>
<div class="m2"><p>سودا مکن آخرت جنون نیست</p></div></div>
<div class="b" id="bn187"><div class="m1"><p>گفتم ز تو کی برآید این دود</p></div>
<div class="m2"><p>کت آتش غم در اندرون نیست؟</p></div></div>
<div class="b" id="bn188"><div class="m1"><p>عاقل داند که نالهٔ زار</p></div>
<div class="m2"><p>از سوزش سینه‌ای برون نیست</p></div></div>
<div class="b" id="bn189"><div class="m1"><p>تسلیم قضا شود کزین قید</p></div>
<div class="m2"><p>کس را به خلاص رهنمون نیست</p></div></div>
<div class="b" id="bn190"><div class="m1"><p>صبر ار نکنم چه چاره سازم؟</p></div>
<div class="m2"><p>آرام دل از یکی فزون نیست</p></div></div>
<div class="b" id="bn191"><div class="m1"><p>گر بکشد و گر معاف دارد</p></div>
<div class="m2"><p>در قبضهٔ او چو من زبون نیست</p></div></div>
<div class="b" id="bn192"><div class="m1"><p>دانی به چه ماند آب چشمم؟</p></div>
<div class="m2"><p>سیماب، که یکدمش سکون نیست</p></div></div>
<div class="b" id="bn193"><div class="m1"><p>در دهر وفا نبود هرگز</p></div>
<div class="m2"><p>یا بود و به بخت ما کنون نیست</p></div></div>
<div class="b" id="bn194"><div class="m1"><p>جان برخی روی یار کردم</p></div>
<div class="m2"><p>گفتم مگرش وفاست چون نیست</p></div></div>
<div class="b2" id="bn195"><p>بنشینم و صبر پیش گیرم</p>
<p>دنبالهٔ کار خویش گیرم</p></div>
<div class="b" id="bn196"><div class="m1"><p>در پای تو هرکه سر نینداخت</p></div>
<div class="m2"><p>از روی تو پرده بر نینداخت</p></div></div>
<div class="b" id="bn197"><div class="m1"><p>در تو نرسید و پی غلط کرد</p></div>
<div class="m2"><p>آن مرغ که بال و پر نینداخت</p></div></div>
<div class="b" id="bn198"><div class="m1"><p>کس با رخ تو نباخت اسبی</p></div>
<div class="m2"><p>تا جان چو پیاده در نینداخت</p></div></div>
<div class="b" id="bn199"><div class="m1"><p>نفزود غم تو روشنایی</p></div>
<div class="m2"><p>آن را که چو شمع سر نینداخت</p></div></div>
<div class="b" id="bn200"><div class="m1"><p>بارت بکشم که مرد معنی</p></div>
<div class="m2"><p>در باخت سر و سپر نینداخت</p></div></div>
<div class="b" id="bn201"><div class="m1"><p>جان داد و درون به خلق ننمود</p></div>
<div class="m2"><p>خون خورد و سخن به در نینداخت</p></div></div>
<div class="b" id="bn202"><div class="m1"><p>روزی گفتم کسی چون من جان</p></div>
<div class="m2"><p>از بهر تو در خطر نینداخت</p></div></div>
<div class="b" id="bn203"><div class="m1"><p>گفتا نه که تیر چشم مستم</p></div>
<div class="m2"><p>صید از تو ضعیفتر نینداخت</p></div></div>
<div class="b" id="bn204"><div class="m1"><p>با آنکه همه نظر در اویم</p></div>
<div class="m2"><p>روزی سوی ما نظر نینداخت</p></div></div>
<div class="b" id="bn205"><div class="m1"><p>نومید نیم که چشم لطفی</p></div>
<div class="m2"><p>بر من فکند، و گر نینداخت</p></div></div>
<div class="b2" id="bn206"><p>بنشینم و صبر پیش گیرم</p>
<p>دنبالهٔ کار خویش گیرم</p></div>
<div class="b" id="bn207"><div class="m1"><p>ای بر تو قبای حسن چالاک</p></div>
<div class="m2"><p>صد پیرهن از محبتت چاک</p></div></div>
<div class="b" id="bn208"><div class="m1"><p>پیشت به تواضعست گویی</p></div>
<div class="m2"><p>افتادن آفتاب بر خاک</p></div></div>
<div class="b" id="bn209"><div class="m1"><p>ما خاک شویم و هم نگردد</p></div>
<div class="m2"><p>خاک درت از جبین ما پاک</p></div></div>
<div class="b" id="bn210"><div class="m1"><p>مهر از تو توان برید؟ هیهات</p></div>
<div class="m2"><p>کس بر تو توان گزید؟ حاشاک</p></div></div>
<div class="b" id="bn211"><div class="m1"><p>اول دل برده باز پس ده</p></div>
<div class="m2"><p>تا دست بدارمت ز فتراک</p></div></div>
<div class="b" id="bn212"><div class="m1"><p>بعد از تو به هیچ‌کس ندارم</p></div>
<div class="m2"><p>امید و ز کس نیایدم باک</p></div></div>
<div class="b" id="bn213"><div class="m1"><p>درد از جهت تو عین داروست</p></div>
<div class="m2"><p>زهر از قبل تو محض تریاک</p></div></div>
<div class="b" id="bn214"><div class="m1"><p>سودای تو آتشی جهانسوز</p></div>
<div class="m2"><p>هجران تو ورطه‌ای خطرناک</p></div></div>
<div class="b" id="bn215"><div class="m1"><p>روی تو چه جای سحر بابل؟</p></div>
<div class="m2"><p>موی تو چه جای مار ضحاک؟</p></div></div>
<div class="b" id="bn216"><div class="m1"><p>سعدی بس ازین سخن که وصفش</p></div>
<div class="m2"><p>دامن ندهد به دست ادراک</p></div></div>
<div class="b" id="bn217"><div class="m1"><p>گرد ارچه بسی هوا بگیرد</p></div>
<div class="m2"><p>هرگز نرسد به گرد افلاک</p></div></div>
<div class="b" id="bn218"><div class="m1"><p>پای طلب از روش فرو ماند</p></div>
<div class="m2"><p>می‌بینم و حیله نیست الاک</p></div></div>
<div class="b2" id="bn219"><p>بنشینم و صبر پیش گیرم</p>
<p>دنبالهٔ کار خویش گیرم</p></div>
<div class="b" id="bn220"><div class="m1"><p>ای چون لب لعل تو شکر نی</p></div>
<div class="m2"><p>بادام چو چشمت ای پسر نی</p></div></div>
<div class="b" id="bn221"><div class="m1"><p>جز سوی تو میل خاطرم نه</p></div>
<div class="m2"><p>جز در رخ تو مرا نظر نی</p></div></div>
<div class="b" id="bn222"><div class="m1"><p>خوبان جهان همه بدیدم</p></div>
<div class="m2"><p>مثل تو به چابکی دگر نی</p></div></div>
<div class="b" id="bn223"><div class="m1"><p>پیران جهان نشان ندادند</p></div>
<div class="m2"><p>چون تو دگری به هیچ قرنی</p></div></div>
<div class="b" id="bn224"><div class="m1"><p>ای آنکه به باغ دلبری بر</p></div>
<div class="m2"><p>چون قد خوش تو یک شجر نی</p></div></div>
<div class="b" id="bn225"><div class="m1"><p>چندین شجر وفا نشاندم</p></div>
<div class="m2"><p>وز وصل تو ذره‌ای ثمر نی</p></div></div>
<div class="b" id="bn226"><div class="m1"><p>آوازهٔ من ز عرش بگذشت</p></div>
<div class="m2"><p>وز درد دلم تو را خبر نی</p></div></div>
<div class="b" id="bn227"><div class="m1"><p>از رفتن من غمت نباشد</p></div>
<div class="m2"><p>از آمدن تو خود اثر نی</p></div></div>
<div class="b" id="bn228"><div class="m1"><p>باز آیم اگر دهی اجازت</p></div>
<div class="m2"><p>ای راحت جان من، و گر نی</p></div></div>
<div class="b2" id="bn229"><p>بنشینم و صبر پیش گیرم</p>
<p>دنبالهٔ کار خویش گیرم</p></div>
<div class="b" id="bn230"><div class="m1"><p>شد موسم سبزه و تماشا</p></div>
<div class="m2"><p>برخیز و بیا به سوی صحرا</p></div></div>
<div class="b" id="bn231"><div class="m1"><p>کان فتنه که روی خوب دارد</p></div>
<div class="m2"><p>هرجا که نشست خاست غوغا</p></div></div>
<div class="b" id="bn232"><div class="m1"><p>صاحبنظری که دید رویش</p></div>
<div class="m2"><p>دیوانهٔ عشق گشت و شیدا</p></div></div>
<div class="b" id="bn233"><div class="m1"><p>دانی نکند قبول هرگز</p></div>
<div class="m2"><p>دیوانه حدیث مرد دانا</p></div></div>
<div class="b" id="bn234"><div class="m1"><p>چشم از پی دیدن تو دارم</p></div>
<div class="m2"><p>من بی تو خسم کنار دریا</p></div></div>
<div class="b" id="bn235"><div class="m1"><p>از جور رقیب تو ننالم</p></div>
<div class="m2"><p>خارست نخست بار خرما</p></div></div>
<div class="b" id="bn236"><div class="m1"><p>سعدی غم دل نهفته می‌دار</p></div>
<div class="m2"><p>تا می‌نشوی ز غیر رسوا</p></div></div>
<div class="b" id="bn237"><div class="m1"><p>گفتست مگر حسود با تو</p></div>
<div class="m2"><p>زنهار مرو ازین پس آنجا</p></div></div>
<div class="b" id="bn238"><div class="m1"><p>من نیز اگرچه ناشکیبم</p></div>
<div class="m2"><p>روزی دو برای مصلحت را</p></div></div>
<div class="b2" id="bn239"><p>بنشینم و صبر پیش گیرم</p>
<p>دنبالهٔ کار خویش گیرم</p></div>
<div class="b" id="bn240"><div class="m1"><p>بربود جمالت ای مه نو</p></div>
<div class="m2"><p>از ماه شب چهارده ضو</p></div></div>
<div class="b" id="bn241"><div class="m1"><p>چون می‌گذری بگو به طاوس</p></div>
<div class="m2"><p>گر جلوه‌کنان روی چنین رو</p></div></div>
<div class="b" id="bn242"><div class="m1"><p>گر لاف زنی که من صبورم</p></div>
<div class="m2"><p>بعد از تو، حکایتست و مشنو</p></div></div>
<div class="b" id="bn243"><div class="m1"><p>دستی ز غمت نهاده بر دل</p></div>
<div class="m2"><p>چشمی ز پیت فتاده در گو</p></div></div>
<div class="b" id="bn244"><div class="m1"><p>یا از در عاشقان درون آی</p></div>
<div class="m2"><p>یا از دل طالبان برون شو</p></div></div>
<div class="b" id="bn245"><div class="m1"><p>زین جور و تحکمت غرض چیست؟</p></div>
<div class="m2"><p>بنیاد وجود ما کن و رو</p></div></div>
<div class="b" id="bn246"><div class="m1"><p>یا متلف مهجتی و نفسی</p></div>
<div class="m2"><p>الله یقیک محضر السو</p></div></div>
<div class="b" id="bn247"><div class="m1"><p>با من چو جوی ندید معشوق</p></div>
<div class="m2"><p>نگرفت حدیث من به یک جو</p></div></div>
<div class="b" id="bn248"><div class="m1"><p>گفتم کهنم مبین که روزی</p></div>
<div class="m2"><p>بینی که شود به خلعتی نو</p></div></div>
<div class="b" id="bn249"><div class="m1"><p>در سایهٔ شاه آسمان قدر</p></div>
<div class="m2"><p>مه طلعت آفتاب پرتو</p></div></div>
<div class="b" id="bn250"><div class="m1"><p>وز لفظ من این حدیث شیرین</p></div>
<div class="m2"><p>گر می‌نرسد به گوش خسرو</p></div></div>
<div class="b2" id="bn251"><p>بنشینم و صبر پیش گیرم</p>
<p>دنبالهٔ کار خویش گیرم</p></div>