---
title: >-
    غزل شمارهٔ  ۱۲۴
---
# غزل شمارهٔ  ۱۲۴

<div class="b" id="bn1"><div class="m1"><p>روز وصلم قرار دیدن نیست</p></div>
<div class="m2"><p>شب هجرانم آرمیدن نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>طاقت سر بریدنم باشد</p></div>
<div class="m2"><p>وز حبیبم سر بریدن نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مطرب از دست من به جان آمد</p></div>
<div class="m2"><p>که مرا طاقت شنیدن نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دست بیچاره چون به جان نرسد</p></div>
<div class="m2"><p>چاره جز پیرهن دریدن نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ما خود افتادگان مسکینیم</p></div>
<div class="m2"><p>حاجت دام گستریدن نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دست در خون عاشقان داری</p></div>
<div class="m2"><p>حاجت تیغ برکشیدن نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>با خداوندگاری افتادم</p></div>
<div class="m2"><p>کش سر بنده پروریدن نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گفتم ای بوستان روحانی</p></div>
<div class="m2"><p>دیدن میوه چون گزیدن نیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گفت سعدی خیال خیره مبند</p></div>
<div class="m2"><p>سیب سیمین برای چیدن نیست</p></div></div>