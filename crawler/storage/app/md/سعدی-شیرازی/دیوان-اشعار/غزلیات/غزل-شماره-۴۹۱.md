---
title: >-
    غزل شمارهٔ  ۴۹۱
---
# غزل شمارهٔ  ۴۹۱

<div class="b" id="bn1"><div class="m1"><p>حناست آن که ناخن دلبند رشته‌ای</p></div>
<div class="m2"><p>یا خون بی دلیست که در بند کشته‌ای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من آدمی به لطف تو دیگر ندیده‌ام</p></div>
<div class="m2"><p>این صورت و صفت که تو داری فرشته‌ای</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>وین طرفه‌تر که تا دل من دردمند توست</p></div>
<div class="m2"><p>حاضر نبوده یک دم و غایب نگشته‌ای</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در هیچ حلقه نیست که یادت نمی‌رود</p></div>
<div class="m2"><p>در هیچ بقعه نیست که تخمی نکشته‌ای</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ما دفتر از حکایت عشقت نبسته‌ایم</p></div>
<div class="m2"><p>تو سنگدل حکایت ما درنوشته‌ای</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زیب و فریب آدمیان را نهایت است</p></div>
<div class="m2"><p>حوری مگر نه از گل آدم سرشته‌ای</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از عنبر و بنفشه تر بر سر آمده‌ست</p></div>
<div class="m2"><p>آن موی مشکبوی که در پای هشته‌ای</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>من در بیان وصف تو حیران بمانده‌ام</p></div>
<div class="m2"><p>حدیست حسن را و تو از حد گذشته‌ای</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سر می‌نهند پیش خطت عارفان پارس</p></div>
<div class="m2"><p>بیتی مگر ز گفته سعدی نبشته‌ای</p></div></div>