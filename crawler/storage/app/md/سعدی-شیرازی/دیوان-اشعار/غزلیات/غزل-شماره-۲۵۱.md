---
title: >-
    غزل شمارهٔ  ۲۵۱
---
# غزل شمارهٔ  ۲۵۱

<div class="b" id="bn1"><div class="m1"><p>اگر تو برشکنی دوستان سلام کنند</p></div>
<div class="m2"><p>که جور قاعده باشد که بر غلام کنند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هزار زخم پیاپی گر اتفاق افتد</p></div>
<div class="m2"><p>ز دست دوست نشاید که انتقام کنند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به تیغ اگر بزنی بی‌دریغ و برگردی</p></div>
<div class="m2"><p>چو روی باز کنی بازت احترام کنند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرا کمند میفکن که خود گرفتارم</p></div>
<div class="m2"><p>لویشه بر سر اسبان بدلگام کنند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو مرغ خانه به سنگم بزن که بازآیم</p></div>
<div class="m2"><p>نه وحشیم که مرا پای بند دام کنند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یکی به گوشه چشم التفات کن ما را</p></div>
<div class="m2"><p>که پادشاهان گه گه نظر به عام کنند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>که گفت در رخ زیبا حلال نیست نظر</p></div>
<div class="m2"><p>حلال نیست که بر دوستان حرام کنند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز من بپرس که فتوی دهم به مذهب عشق</p></div>
<div class="m2"><p>نظر به روی تو شاید که بر دوام کنند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دهان غنچه بدرد نسیم باد صبا</p></div>
<div class="m2"><p>لبان لعل تو وقتی که ابتسام کنند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>غریب مشرق و مغرب به آشنایی تو</p></div>
<div class="m2"><p>غریب نیست که در شهر ما مقام کنند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>من از تو روی نپیچم که شرط عشق آن است</p></div>
<div class="m2"><p>که روی در غرض و پشت بر ملام کنند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به جان مضایقه با دوستان مکن سعدی</p></div>
<div class="m2"><p>که دوستی نبود هر چه ناتمام کنند</p></div></div>