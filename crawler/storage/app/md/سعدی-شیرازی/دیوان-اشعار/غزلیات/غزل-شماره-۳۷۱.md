---
title: >-
    غزل شمارهٔ  ۳۷۱
---
# غزل شمارهٔ  ۳۷۱

<div class="b" id="bn1"><div class="m1"><p>من از آن روز که در بند توام آزادم</p></div>
<div class="m2"><p>پادشاهم که به دست تو اسیر افتادم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همه غم‌های جهان هیچ اثر می‌نکند</p></div>
<div class="m2"><p>در من از بس که به دیدار عزیزت شادم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خرم آن روز که جان می‌رود اندر طلبت</p></div>
<div class="m2"><p>تا بیایند عزیزان به مبارک بادم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>من که در هیچ مقامی نزدم خیمه‌ی انس</p></div>
<div class="m2"><p>پیش تو رخت بیفکندم و دل بنهادم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دانی از دولت وصلت چه طلب دارم هیچ</p></div>
<div class="m2"><p>یاد تو مصلحت خویش ببرد از یادم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به وفای تو کز آن روز که دلبند منی</p></div>
<div class="m2"><p>دل نبستم به وفای کس و در نگشادم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا خیال قد و بالای تو در فکر من است</p></div>
<div class="m2"><p>گر خلایق همه سروند چو سرو آزادم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به سخن راست نیاید که چه شیرین سخنی</p></div>
<div class="m2"><p>وین عجبتر که تو شیرینی و من فرهادم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دستگاهی نه که در پای تو ریزم چون خاک</p></div>
<div class="m2"><p>حاصل آن است که چون طبل تهی پربادم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>می‌نماید که جفای فلک از دامن من</p></div>
<div class="m2"><p>دست کوته نکند تا نکند بنیادم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ظاهر آن است که با سابقه‌ی حکم ازل</p></div>
<div class="m2"><p>جهد سودی نکند تن به قضا در دادم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ور تحمل نکنم جور زمان را چه کنم</p></div>
<div class="m2"><p>داوری نیست که از وی بستاند دادم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>دلم از صحبت شیراز به کلی بگرفت</p></div>
<div class="m2"><p>وقت آن است که پرسی خبر از بغدادم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>هیچ شک نیست که فریاد من آنجا برسد</p></div>
<div class="m2"><p>عجب ار صاحب دیوان نرسد فریادم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>سعدیا حب وطن گر چه حدیثیست صحیح</p></div>
<div class="m2"><p>نتوان مرد به سختی که من این جا زادم</p></div></div>