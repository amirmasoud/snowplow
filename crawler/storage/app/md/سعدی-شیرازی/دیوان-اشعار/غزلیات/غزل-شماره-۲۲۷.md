---
title: >-
    غزل شمارهٔ  ۲۲۷
---
# غزل شمارهٔ  ۲۲۷

<div class="b" id="bn1"><div class="m1"><p>آخر ای سنگدل سیم زنخدان تا چند</p></div>
<div class="m2"><p>تو ز ما فارغ و ما از تو پریشان تا چند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خار در پای گل از دور به حسرت دیدن</p></div>
<div class="m2"><p>تشنه بازآمدن از چشمه حیوان تا چند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گوش در گفتن شیرین تو واله تا کی</p></div>
<div class="m2"><p>چشم در منظر مطبوع تو حیران تا چند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بیم آنست دمادم که برآرم فریاد</p></div>
<div class="m2"><p>صبر پیدا و جگر خوردن پنهان تا چند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تو سر ناز برآری ز گریبان هر روز</p></div>
<div class="m2"><p>ما ز جورت سر فکرت به گریبان تا چند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>رنگ دستت نه به حناست که خون دل ماست</p></div>
<div class="m2"><p>خوردن خون دل خلق به دستان تا چند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سعدی از دست تو از پای درآید روزی</p></div>
<div class="m2"><p>طاقت بار ستم تا کی و هجران تا چند</p></div></div>