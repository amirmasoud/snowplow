---
title: >-
    غزل شمارهٔ  ۵۳۸
---
# غزل شمارهٔ  ۵۳۸

<div class="b" id="bn1"><div class="m1"><p>گفتم آهن دلی کنم چندی</p></div>
<div class="m2"><p>ندهم دل به هیچ دلبندی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وان که را دیده در دهان تو رفت</p></div>
<div class="m2"><p>هرگزش گوش نشنود پندی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خاصه ما را که در ازل بوده‌ست</p></div>
<div class="m2"><p>با تو آمیزشی و پیوندی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به دلت کز دلت به در نکنم</p></div>
<div class="m2"><p>سختتر زین مخواه سوگندی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یک دم آخر حجاب یک سو نه</p></div>
<div class="m2"><p>تا برآساید آرزومندی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همچنان پیر نیست مادر دهر</p></div>
<div class="m2"><p>که بیاورد چون تو فرزندی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ریش فرهاد بهترک می‌بود</p></div>
<div class="m2"><p>گر نه شیرین نمک پراکندی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کاشکی خاک بودمی در راه</p></div>
<div class="m2"><p>تا مگر سایه بر من افکندی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چه کند بنده‌ای که از دل و جان</p></div>
<div class="m2"><p>نکند خدمت خداوندی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سعدیا دور نیک نامی رفت</p></div>
<div class="m2"><p>نوبت عاشقیست یک چندی</p></div></div>