---
title: >-
    غزل شمارهٔ  ۵۷۸
---
# غزل شمارهٔ  ۵۷۸

<div class="b" id="bn1"><div class="m1"><p>تو خود به صحبت امثال ما نپردازی</p></div>
<div class="m2"><p>نظر به حال پریشان ما نیندازی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وصال ما و شما دیر متفق گردد</p></div>
<div class="m2"><p>که من اسیر نیازم تو صاحب نازی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کجا به صید ملخ همتت فرو آید</p></div>
<div class="m2"><p>بدین صفت که تو باز بلندپروازی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به راستی که نه همبازی تو بودم من</p></div>
<div class="m2"><p>تو شوخ دیده مگس بین که می‌کند بازی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز دست ترک ختایی کسی جفا چندان</p></div>
<div class="m2"><p>نمی‌برد که من از دست ترک شیرازی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>و گر هلاک منت درخور است باکی نیست</p></div>
<div class="m2"><p>قتیل عشق شهید است و قاتلش غازی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کدام سنگدل است آن که عیب ما گوید</p></div>
<div class="m2"><p>گر آفتاب ببینی چو موم بگدازی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>میسرت نشود سر عشق پوشیدن</p></div>
<div class="m2"><p>که عاقبت بکند رنگ روی غمازی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چه جرم رفت که با ما سخن نمی‌گویی</p></div>
<div class="m2"><p>چه دشمنیست که با دوستان نمی‌سازی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>من از فراق تو بیچاره سیل می‌رانم</p></div>
<div class="m2"><p>مثال ابر بهار و تو خیل می‌تازی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هنوز با همه بدعهدیت دعاگویم</p></div>
<div class="m2"><p>که گر به قهر برانی به لطف بنوازی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تو همچو صاحب دیوان مکن که سعدی را</p></div>
<div class="m2"><p>به یک ره از نظر خویشتن بیندازی</p></div></div>