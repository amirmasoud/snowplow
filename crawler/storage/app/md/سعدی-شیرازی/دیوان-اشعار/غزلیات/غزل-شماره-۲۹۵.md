---
title: >-
    غزل شمارهٔ  ۲۹۵
---
# غزل شمارهٔ  ۲۹۵

<div class="b" id="bn1"><div class="m1"><p>خفتن عاشق یکیست بر سر دیبا و خار</p></div>
<div class="m2"><p>چون نتواند کشید دست در آغوش یار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر دگری را شکیب هست ز دیدار دوست</p></div>
<div class="m2"><p>من نتوانم گرفت بر سر آتش قرار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آتش آه است و دود می‌رودش تا به سقف</p></div>
<div class="m2"><p>چشمه چشمست و موج می‌زندش بر کنار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر تو ز ما فارغی ما به تو مستظهریم</p></div>
<div class="m2"><p>ور تو ز ما بی نیاز ما به تو امیدوار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای که به یاران غار مشتغلی دوستکام</p></div>
<div class="m2"><p>غمزده‌ای بر درست چون سگ اصحاب غار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>این همه بار احتمال می‌کنم و می‌روم</p></div>
<div class="m2"><p>اشتر مست از نشاط گرم رود زیر بار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ما سپر انداختیم گردن تسلیم پیش</p></div>
<div class="m2"><p>گر بکشی حاکمی ور بدهی زینهار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تیغ جفا گر زنی ضرب تو آسایشست</p></div>
<div class="m2"><p>روی ترش گر کنی تلخ تو شیرین گوار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سعدی اگر داغ عشق در تو مؤثر شود</p></div>
<div class="m2"><p>فخر بود بنده را داغ خداوندگار</p></div></div>