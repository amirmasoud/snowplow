---
title: >-
    غزل شمارهٔ  ۲۰۲
---
# غزل شمارهٔ  ۲۰۲

<div class="b" id="bn1"><div class="m1"><p>جنگ از طرف دوست دل آزار نباشد</p></div>
<div class="m2"><p>یاری که تحمل نکند یار نباشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر بانگ برآید که سری در قدمی رفت</p></div>
<div class="m2"><p>بسیار مگویید که بسیار نباشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آن بار که گردون نکشد یار سبکروح</p></div>
<div class="m2"><p>گر بر دل عشاق نهد بار نباشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا رنج تحمل نکنی گنج نبینی</p></div>
<div class="m2"><p>تا شب نرود صبح پدیدار نباشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آهنگ دراز شب رنجوری مشتاق</p></div>
<div class="m2"><p>با آن نتوان گفت که بیدار نباشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از دیده من پرس که خواب شب مستی</p></div>
<div class="m2"><p>چون خاستن و خفتن بیمار نباشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر دست به شمشیر بری عشق همان است</p></div>
<div class="m2"><p>کانجا که ارادت بود انکار نباشد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از من مشنو دوستی گل مگر آن گاه</p></div>
<div class="m2"><p>کم پای برهنه خبر از خار نباشد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مرغان قفس را المی باشد و شوقی</p></div>
<div class="m2"><p>کان مرغ نداند که گرفتار نباشد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دل آینه صورت غیب است ولیکن</p></div>
<div class="m2"><p>شرط است که بر آینه زنگار نباشد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سعدی حیوان را که سر از خواب گران شد</p></div>
<div class="m2"><p>در بند نسیم خوش اسحار نباشد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>آن را که بصارت نبود یوسف صدیق</p></div>
<div class="m2"><p>جایی بفروشد که خریدار نباشد</p></div></div>