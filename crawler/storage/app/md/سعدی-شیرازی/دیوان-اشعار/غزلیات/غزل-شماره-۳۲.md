---
title: >-
    غزل شمارهٔ  ۳۲
---
# غزل شمارهٔ  ۳۲

<div class="b" id="bn1"><div class="m1"><p>معلمت همه شوخی و دلبری آموخت</p></div>
<div class="m2"><p>جفا و ناز و عتاب و ستمگری آموخت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غلام آن لب ضحاک و چشم فتانم</p></div>
<div class="m2"><p>که کید سحر به ضحاک و سامری آموخت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تو بت چرا به معلم روی که بتگر چین</p></div>
<div class="m2"><p>به چین زلف تو آید به بتگری آموخت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هزار بلبل دستان سرای عاشق را</p></div>
<div class="m2"><p>بباید از تو سخن گفتن دری آموخت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>برفت رونق بازار آفتاب و قمر</p></div>
<div class="m2"><p>از آن که ره به دکان تو مشتری آموخت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همه قبیله من عالمان دین بودند</p></div>
<div class="m2"><p>مرا معلم عشق تو شاعری آموخت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مرا به شاعری آموخت روزگار آن گه</p></div>
<div class="m2"><p>که چشم مست تو دیدم که ساحری آموخت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مگر دهان تو آموخت تنگی از دل من</p></div>
<div class="m2"><p>وجود من ز میان تو لاغری آموخت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بلای عشق تو بنیاد زهد و بیخ ورع</p></div>
<div class="m2"><p>چنان بکند که صوفی قلندری آموخت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دگر نه عزم سیاحت کند نه یاد وطن</p></div>
<div class="m2"><p>کسی که بر سر کویت مجاوری آموخت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>من آدمی به چنین شکل و قد و خوی و روش</p></div>
<div class="m2"><p>ندیده‌ام مگر این شیوه از پری آموخت</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به خون خلق فروبرده پنجه کاین حناست</p></div>
<div class="m2"><p>ندانمش که به قتل که شاطری آموخت</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چنین بگریم از این پس که مرد بتواند</p></div>
<div class="m2"><p>در آب دیده سعدی شناوری آموخت</p></div></div>