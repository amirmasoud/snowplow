---
title: >-
    غزل شمارهٔ  ۶۰۴
---
# غزل شمارهٔ  ۶۰۴

<div class="b" id="bn1"><div class="m1"><p>زنده بی دوست خفته در وطنی</p></div>
<div class="m2"><p>مثل مرده‌ایست در کفنی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عیش را بی تو عیش نتوان گفت</p></div>
<div class="m2"><p>چه بود بی وجود روح تنی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا صبا می‌رود به بستان‌ها</p></div>
<div class="m2"><p>چون تو سروی نیافت در چمنی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>و آفتابی خلاف امکان است</p></div>
<div class="m2"><p>که برآید ز جیب پیرهنی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>وآن شکن برشکن قبائل زلف</p></div>
<div class="m2"><p>که بلاییست زیر هر شکنی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر سر کوی عشق بازاریست</p></div>
<div class="m2"><p>که نیارد هزار جان ثمنی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جای آن است اگر ببخشایی</p></div>
<div class="m2"><p>که نبینی فقیرتر ز منی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هفت کشور نمی‌کنند امروز</p></div>
<div class="m2"><p>بی مقالات سعدی انجمنی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از دو بیرون نه یا دلت سنگیست</p></div>
<div class="m2"><p>یا به گوشت نمی‌رسد سخنی</p></div></div>