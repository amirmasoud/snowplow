---
title: >-
    غزل شمارهٔ  ۳۴۰
---
# غزل شمارهٔ  ۳۴۰

<div class="b" id="bn1"><div class="m1"><p>هر کسی را هوسی در سر و کاری در پیش</p></div>
<div class="m2"><p>من بی‌کار گرفتار هوای دل خویش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هرگز اندیشه نکردم که تو با من باشی</p></div>
<div class="m2"><p>چون به دست آمدی ای لقمه از حوصله بیش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>این تویی با من و غوغای رقیبان از پس</p></div>
<div class="m2"><p>وین منم با تو گرفته ره صحرا در پیش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همچنان داغ جدایی جگرم می‌سوزد</p></div>
<div class="m2"><p>مگرم دست چو مرهم بنهی بر دل ریش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>باور از بخت ندارم که تو مهمان منی</p></div>
<div class="m2"><p>خیمه پادشه آن گاه فضای درویش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زخم شمشیر غمت را ننهم مرهم کس</p></div>
<div class="m2"><p>طشت زرینم و پیوند نگیرم به سریش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عاشقان را نتوان گفت که بازآی از مهر</p></div>
<div class="m2"><p>کافران را نتوان گفت که برگرد از کیش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>منم امروز و تو و مطرب و ساقی و حسود</p></div>
<div class="m2"><p>خویشتن گو به در حجره بیاویز چو خیش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>من خود از کید عدو باک ندارم لیکن</p></div>
<div class="m2"><p>کژدم از خبث طبیعت بزند سنگ به نیش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تو به آرام دل خویش رسیدی سعدی</p></div>
<div class="m2"><p>می خور و غم مخور از شنعت بیگانه و خویش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ای که گفتی به هوا دل منه و مهر مبند</p></div>
<div class="m2"><p>من چنینم تو برو مصلحت خویش اندیش</p></div></div>