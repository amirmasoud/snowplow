---
title: >-
    غزل شمارهٔ  ۵۴۶
---
# غزل شمارهٔ  ۵۴۶

<div class="b" id="bn1"><div class="m1"><p>جور بر من می‌پسندد دلبری</p></div>
<div class="m2"><p>زور با من می‌کند زورآوری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بار خصمی می‌کشم کز جور او</p></div>
<div class="m2"><p>می‌نشاید رفت پیش داوری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عقل بیچاره‌ست در زندان عشق</p></div>
<div class="m2"><p>چون مسلمانی به دست کافری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بارها گفتم بگریم پیش خلق</p></div>
<div class="m2"><p>تا مگر بر من ببخشد خاطری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>باز گویم پادشاهی را چه غم</p></div>
<div class="m2"><p>گر به خیلش در بمیرد چاکری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای که صبر از من طمع داری و هوش</p></div>
<div class="m2"><p>بار سنگین می‌نهی بر لاغری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زآنچه در پای عزیزان افکنند</p></div>
<div class="m2"><p>ما سری داریم اگر داری سری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چشم عادت کرده با دیدار دوست</p></div>
<div class="m2"><p>حیف باشد بعد از او بر دیگری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در سراپای تو حیران مانده‌ام</p></div>
<div class="m2"><p>در نمی‌باید به حسنت زیوری</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>این سخن سعدی تواند گفت و بس</p></div>
<div class="m2"><p>هر گدایی را نباشد جوهری</p></div></div>