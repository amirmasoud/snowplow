---
title: >-
    غزل شمارهٔ  ۴۵۶
---
# غزل شمارهٔ  ۴۵۶

<div class="b" id="bn1"><div class="m1"><p>ما نتوانیم و عشق پنجه درانداختن</p></div>
<div class="m2"><p>قوت او می‌کند بر سر ما تاختن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر دهیم ره به خویش یا نگذاری به پیش</p></div>
<div class="m2"><p>هر دو به دستت در است کشتن و بنواختن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر تو به شمشیر و تیر حمله بیاری رواست</p></div>
<div class="m2"><p>چاره ما هیچ نیست جز سپر انداختن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کشتی در آب را از دو برون حال نیست</p></div>
<div class="m2"><p>یا همه سود ای حکیم یا همه درباختن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مذهب اگر عاشقیست سنت عشاق چیست</p></div>
<div class="m2"><p>دل که نظرگاه اوست از همه پرداختن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پایه خورشید نیست پیش تو افروختن</p></div>
<div class="m2"><p>یا قد و بالای سرو پیش تو افراختن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر که چنین روی دید جامه چو سعدی درید</p></div>
<div class="m2"><p>موجب دیوانگیست آفت بشناختن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>یا بگدازم چو شمع یا بکشندم به صبح</p></div>
<div class="m2"><p>چاره همین بیش نیست سوختن و ساختن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ما سپر انداختیم با تو که در جنگ دوست</p></div>
<div class="m2"><p>زخم توان خورد و تیغ بر نتوان آختن</p></div></div>