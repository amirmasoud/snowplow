---
title: >-
    غزل شمارهٔ  ۵۸۲
---
# غزل شمارهٔ  ۵۸۲

<div class="b" id="bn1"><div class="m1"><p>یار گرفته‌ام بسی چون تو ندیده‌ام کسی</p></div>
<div class="m2"><p>شمع چنین نیامده‌ست از در هیچ مجلسی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عادت بخت من نبود آن که تو یادم آوری</p></div>
<div class="m2"><p>نقد چنین کم اوفتد خاصه به دست مفلسی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>صحبت از این شریفتر صورت از این لطیفتر</p></div>
<div class="m2"><p>دامن از این نظیفتر وصف تو چون کند کسی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خادمه سرای را گو در حجره بند کن</p></div>
<div class="m2"><p>تا به سر حضور ما ره نبرد موسوسی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>روز وصال دوستان دل نرود به بوستان</p></div>
<div class="m2"><p>یا به گلی نگه کند یا به جمال نرگسی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر بکشی کجا روم تن به قضا نهاده‌ام</p></div>
<div class="m2"><p>سنگ جفای دوستان درد نمی‌کند بسی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>قصه به هر که می‌برم فایده‌ای نمی‌دهد</p></div>
<div class="m2"><p>مشکل درد عشق را حل نکند مهندسی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>این همه خار می‌خورد سعدی و بار می‌برد</p></div>
<div class="m2"><p>جای دگر نمی‌رود هر که گرفت مونسی</p></div></div>