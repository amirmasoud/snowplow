---
title: >-
    غزل شمارهٔ  ۲۴۵
---
# غزل شمارهٔ  ۲۴۵

<div class="b" id="bn1"><div class="m1"><p>هر که بی او زندگانی می‌کند</p></div>
<div class="m2"><p>گر نمی‌میرد گرانی می‌کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من بر آن بودم که ندهم دل به عشق</p></div>
<div class="m2"><p>سروبالا دلستانی می‌کند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مهربانی می‌نمایم بر قدش</p></div>
<div class="m2"><p>سنگدل نامهربانی می‌کند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>برف پیری می‌نشیند بر سرم</p></div>
<div class="m2"><p>همچنان طبعم جوانی می‌کند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ماجرای دل نمی‌گفتم به خلق</p></div>
<div class="m2"><p>آب چشمم ترجمانی می‌کند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آهن افسرده می‌کوبد که جهد</p></div>
<div class="m2"><p>با قضای آسمانی می‌کند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عقل را با عشق زور پنجه نیست</p></div>
<div class="m2"><p>احتمال از ناتوانی می‌کند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چشم سعدی در امید روی یار</p></div>
<div class="m2"><p>چون دهانش درفشانی می‌کند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هم بود شوری در این سر بی خلاف</p></div>
<div class="m2"><p>کاین همه شیرین زبانی می‌کند</p></div></div>