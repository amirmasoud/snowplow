---
title: >-
    غزل شمارهٔ  ۱۱۰
---
# غزل شمارهٔ  ۱۱۰

<div class="b" id="bn1"><div class="m1"><p>هر چه در روی تو گویند به زیبایی هست</p></div>
<div class="m2"><p>وان چه در چشم تو از شوخی و رعنایی هست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سروها دیدم در باغ و تأمل کردم</p></div>
<div class="m2"><p>قامتی نیست که چون تو به دلارایی هست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای که مانند تو بلبل به سخندانی نیست</p></div>
<div class="m2"><p>نتوان گفت که طوطی به شکرخایی هست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نه تو را از من مسکین نه گل خندان را</p></div>
<div class="m2"><p>خبر از مشغله بلبل سودایی هست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>راست گفتی که فرج یابی اگر صبر کنی</p></div>
<div class="m2"><p>صبر نیکست کسی را که توانایی هست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هرگز از دوست شنیدی که کسی بشکیبد</p></div>
<div class="m2"><p>دوستی نیست در آن دل که شکیبایی هست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خبر از عشق نبودست و نباشد همه عمر</p></div>
<div class="m2"><p>هر که او را خبر از شنعت و رسوایی هست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آن نه تنهاست که با یاد تو انسی دارد</p></div>
<div class="m2"><p>تا نگویی که مرا طاقت تنهایی هست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>همه را دیده به رویت نگرانست ولیک</p></div>
<div class="m2"><p>همه کس را نتوان گفت که بینایی هست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گفته بودی همه زرقند و فریبند و فسوس</p></div>
<div class="m2"><p>سعدی آن نیست ولیکن چو تو فرمایی هست</p></div></div>