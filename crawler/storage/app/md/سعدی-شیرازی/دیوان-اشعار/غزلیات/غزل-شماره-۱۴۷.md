---
title: >-
    غزل شمارهٔ  ۱۴۷
---
# غزل شمارهٔ  ۱۴۷

<div class="b" id="bn1"><div class="m1"><p>جان و تنم ای دوست فدای تن و جانت</p></div>
<div class="m2"><p>مویی نفروشم به همه ملک جهانت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شیرینتر از این لب نشنیدم که سخن گفت</p></div>
<div class="m2"><p>تو خود شکری یا عسلست آب دهانت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یک روز عنایت کن و تیری به من انداز</p></div>
<div class="m2"><p>باشد که تفرج بکنم دست و کمانت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر راه بگردانی و گر روی بپوشی</p></div>
<div class="m2"><p>من می‌نگرم گوشه چشم نگرانت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر سرو نباشد رخ چون ماه منیرت</p></div>
<div class="m2"><p>بر ماه نباشد قد چون سرو روانت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آخر چه بلایی تو که در وصف نیایی</p></div>
<div class="m2"><p>بسیار بگفتیم و نکردیم بیانت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر کس که ملامت کند از عشق تو ما را</p></div>
<div class="m2"><p>معذور بدارند چو بینند عیانت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>حیفست چنین روی نگارین که بپوشی</p></div>
<div class="m2"><p>سودی به مساکین رسد آخر چه زیانت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بازآی که در دیده بماندست خیالت</p></div>
<div class="m2"><p>بنشین که به خاطر بگرفتست نشانت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بسیار نباشد دلی از دست بدادن</p></div>
<div class="m2"><p>از جان رمقی دارم و هم برخی جانت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دشنام کرم کردی و گفتی و شنیدم</p></div>
<div class="m2"><p>خرم تن سعدی که برآمد به زبانت</p></div></div>