---
title: >-
    غزل شمارهٔ  ۷۲
---
# غزل شمارهٔ  ۷۲

<div class="b" id="bn1"><div class="m1"><p>پای سرو بوستانی در گل است</p></div>
<div class="m2"><p>سرو ما را پای معنی در دل است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر که چشمش بر چنان روی اوفتاد</p></div>
<div class="m2"><p>طالعش میمون و فالش مقبل است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نیکخواهانم نصیحت می‌کنند</p></div>
<div class="m2"><p>خشت بر دریا زدن بی‌حاصل است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای برادر ما به گرداب اندریم</p></div>
<div class="m2"><p>وان که شنعت می‌زند بر ساحل است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شوق را بر صبر قوت غالب است</p></div>
<div class="m2"><p>عقل را با عشق دعوی باطل است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نسبت عاشق به غفلت می‌کنند</p></div>
<div class="m2"><p>وآن که معشوقی ندارد غافل است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دیده باشی تشنه مستعجل به آب</p></div>
<div class="m2"><p>جان به جانان همچنان مستعجل است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بذل جاه و مال و ترک نام و ننگ</p></div>
<div class="m2"><p>در طریق عشق اول منزل است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر بمیرد طالبی در بند دوست</p></div>
<div class="m2"><p>سهل باشد زندگانی مشکل است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>عاشقی می‌گفت و خوش خوش می‌گریست</p></div>
<div class="m2"><p>جان بیاساید که جانان قاتل است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سعدیا نزدیک رای عاشقان</p></div>
<div class="m2"><p>خلق مجنونند و مجنون عاقل است</p></div></div>