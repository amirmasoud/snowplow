---
title: >-
    غزل شمارهٔ  ۱۵۸
---
# غزل شمارهٔ  ۱۵۸

<div class="b" id="bn1"><div class="m1"><p>مویت رها مکن که چنین بر هم اوفتد</p></div>
<div class="m2"><p>کآشوب حسن روی تو در عالم اوفتد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر در خیال خلق پری وار بگذری</p></div>
<div class="m2"><p>فریاد در نهاد بنی آدم اوفتد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>افتاده تو شد دلم ای دوست دست گیر</p></div>
<div class="m2"><p>در پای مفکنش که چنین دل کم اوفتد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در رویت آن که تیغ نظر می‌کشد به جهل</p></div>
<div class="m2"><p>مانند من به تیر بلا محکم اوفتد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مشکن دلم که حقه راز نهان توست</p></div>
<div class="m2"><p>ترسم که راز در کف نامحرم اوفتد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>وقتست اگر بیایی و لب بر لبم نهی</p></div>
<div class="m2"><p>چندم به جست و جوی تو دم بر دم اوفتد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سعدی صبور باش بر این ریش دردناک</p></div>
<div class="m2"><p>باشد که اتفاق یکی مرهم اوفتد</p></div></div>