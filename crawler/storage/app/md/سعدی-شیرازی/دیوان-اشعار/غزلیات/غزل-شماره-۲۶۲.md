---
title: >-
    غزل شمارهٔ  ۲۶۲
---
# غزل شمارهٔ  ۲۶۲

<div class="b" id="bn1"><div class="m1"><p>عیبی نباشد از تو که بر ما جفا رود</p></div>
<div class="m2"><p>مجنون از آستانه لیلی کجا رود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر من فدای جان تو گردم دریغ نیست</p></div>
<div class="m2"><p>بسیار سر که در سر مهر و وفا رود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ور من گدای کوی تو باشم غریب نیست</p></div>
<div class="m2"><p>قارون اگر به خیل تو آید گدا رود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مجروح تیر عشق اگرش تیغ بر قفاست</p></div>
<div class="m2"><p>چون می‌رود ز پیش تو چشم از قفا رود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حیف آیدم که پای همی بر زمین نهی</p></div>
<div class="m2"><p>کاین پای لایقست که بر چشم ما رود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در هیچ موقفم سر گفت و شنید نیست</p></div>
<div class="m2"><p>الا در آن مقام که ذکر شما رود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای هوشیار اگر به سر مست بگذری</p></div>
<div class="m2"><p>عیبش مکن که بر سر مردم قضا رود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ما چون نشانه پای به گل در بمانده‌ایم</p></div>
<div class="m2"><p>خصم آن حریف نیست که تیرش خطا رود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ای آشنای کوی محبت صبور باش</p></div>
<div class="m2"><p>بیداد نیکوان همه بر آشنا رود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سعدی به در نمی‌کنی از سر هوای دوست</p></div>
<div class="m2"><p>در پات لازمست که خار جفا رود</p></div></div>