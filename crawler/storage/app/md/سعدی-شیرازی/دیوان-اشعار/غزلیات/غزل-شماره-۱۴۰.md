---
title: >-
    غزل شمارهٔ  ۱۴۰
---
# غزل شمارهٔ  ۱۴۰

<div class="b" id="bn1"><div class="m1"><p>چشمت چو تیغ غمزه خونخوار برگرفت</p></div>
<div class="m2"><p>با عقل و هوش خلق به پیکار برگرفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عاشق ز سوز درد تو فریاد درنهاد</p></div>
<div class="m2"><p>مؤمن ز دست عشق تو زنار برگرفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عشقت بنای عقل به کلی خراب کرد</p></div>
<div class="m2"><p>جورت در امید به یک بار برگرفت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شوری ز وصف روی تو در خانگه فتاد</p></div>
<div class="m2"><p>صوفی طریق خانه خمار برگرفت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با هر که مشورت کنم از جور آن صنم</p></div>
<div class="m2"><p>گوید ببایدت دل از این کار برگرفت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دل برتوانم از سر و جان برگرفت و چشم</p></div>
<div class="m2"><p>نتوانم از مشاهده یار برگرفت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سعدی به خفیه خون جگر خورد بارها</p></div>
<div class="m2"><p>این بار پرده از سر اسرار برگرفت</p></div></div>