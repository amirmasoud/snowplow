---
title: >-
    غزل شمارهٔ  ۵۰۸
---
# غزل شمارهٔ  ۵۰۸

<div class="b" id="bn1"><div class="m1"><p>مشتاق توام با همه جوری و جفایی</p></div>
<div class="m2"><p>محبوب منی با همه جرمی و خطایی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من خود به چه ارزم که تمنای تو ورزم</p></div>
<div class="m2"><p>در حضرت سلطان که برد نام گدایی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>صاحب نظران لاف محبت نپسندند</p></div>
<div class="m2"><p>وان گه سپر انداختن از تیر بلایی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>باید که سری در نظرش هیچ نیرزد</p></div>
<div class="m2"><p>آن کس که نهد در طلب وصل تو پایی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بیداد تو عدلست و جفای تو کرامت</p></div>
<div class="m2"><p>دشنام تو خوشتر که ز بیگانه دعایی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جز عهد و وفای تو که محلول نگردد</p></div>
<div class="m2"><p>هر عهد که بستم هوسی بود و هوایی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر دست دهد دولت آنم که سر خویش</p></div>
<div class="m2"><p>در پای سمند تو کنم نعل بهایی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شاید که به خون بر سر خاکم بنویسند</p></div>
<div class="m2"><p>این بود که با دوست به سر برد وفایی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خون در دل آزرده نهان چند بماند</p></div>
<div class="m2"><p>شک نیست که سر برکند این درد به جایی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شرط کرم آنست که با درد بمیری</p></div>
<div class="m2"><p>سعدی و نخواهی ز در خلق دوایی</p></div></div>