---
title: >-
    غزل شمارهٔ  ۵۹۳
---
# غزل شمارهٔ  ۵۹۳

<div class="b" id="bn1"><div class="m1"><p>بسم از هوا گرفتن که پری نماند و بالی</p></div>
<div class="m2"><p>به کجا روم ز دستت که نمی‌دهی مجالی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نه ره گریز دارم نه طریق آشنایی</p></div>
<div class="m2"><p>چه غم اوفتاده‌ای را که تواند احتیالی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همه عمر در فراقت بگذشت و سهل باشد</p></div>
<div class="m2"><p>اگر احتمال دارد به قیامت اتصالی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چه خوش است در فراقی همه عمر صبر کردن</p></div>
<div class="m2"><p>به امید آن که روزی به کف اوفتد وصالی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به تو حاصلی ندارد غم روزگار گفتن</p></div>
<div class="m2"><p>که شبی نخفته باشی به درازنای سالی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>غم حال دردمندان نه عجب گرت نباشد</p></div>
<div class="m2"><p>که چنین نرفته باشد همه عمر بر تو حالی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سخنی بگوی با من که چنان اسیر عشقم</p></div>
<div class="m2"><p>که به خویشتن ندارم ز وجودت اشتغالی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چه نشینی ای قیامت بنمای سرو قامت</p></div>
<div class="m2"><p>به خلاف سرو بستان که ندارد اعتدالی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>که نه امشب آن سماع است که دف خلاص یابد</p></div>
<div class="m2"><p>به طپانچه‌ای و بربط برهد به گوشمالی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دگر آفتاب رویت منمای آسمان را</p></div>
<div class="m2"><p>که قمر ز شرمساری بشکست چون هلالی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خط مشک بوی و خالت به مناسبت تو گویی</p></div>
<div class="m2"><p>قلم غبار می‌رفت و فرو چکید خالی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تو هم این مگوی سعدی که نظر گناه باشد</p></div>
<div class="m2"><p>گنه است برگرفتن نظر از چنین جمالی</p></div></div>