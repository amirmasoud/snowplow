---
title: >-
    غزل شمارهٔ  ۶
---
# غزل شمارهٔ  ۶

<div class="b" id="bn1"><div class="m1"><p>پیش ما رسم شکستن نبود عهد وفا را</p></div>
<div class="m2"><p>الله الله تو فراموش مکن صحبت ما را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قیمت عشق نداند قدم صدق ندارد</p></div>
<div class="m2"><p>سست‌عهدی که تحمل نکند بار جفا را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر مخیر بکنندم به قیامت که چه خواهی</p></div>
<div class="m2"><p>دوست ما را و همه نعمت فردوس شما را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر سرم می‌رود از عهد تو سر بازنپیچم</p></div>
<div class="m2"><p>تا بگویند پس از من که به سر برد وفا را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خنک آن درد که یارم به عیادت به سر آید</p></div>
<div class="m2"><p>دردمندان به چنین درد نخواهند دوا را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>باور از مات نباشد تو در آیینه نگه کن</p></div>
<div class="m2"><p>تا بدانی که چه بودست گرفتار بلا را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از سر زلف عروسان چمن دست بدارد</p></div>
<div class="m2"><p>به سر زلف تو گر دست رسد باد صبا را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سر انگشت تحیر بگزد عقل به دندان</p></div>
<div class="m2"><p>چون تأمل کند این صورت انگشت‌نما را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آرزو می‌کندم شمع صفت پیش وجودت</p></div>
<div class="m2"><p>که سراپای بسوزند من بی سر و پا را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چشم کوته‌نظران بر ورق صورت خوبان</p></div>
<div class="m2"><p>خط همی‌بیند و عارف قلم صنع خدا را</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>همه را دیده به رویت نگرانست ولیکن</p></div>
<div class="m2"><p>خودپرستان ز حقیقت نشناسند هوا را</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مهربانی ز من آموز و گرم عمر نماند</p></div>
<div class="m2"><p>به سر تربت سعدی بطلب مهرگیا را</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>هیچ هشیار ملامت نکند مستی ما را</p></div>
<div class="m2"><p>قُل لِصاحٍ تَرَکَ النّاسَ مِن الوَجْد سُکاریٰ</p></div></div>