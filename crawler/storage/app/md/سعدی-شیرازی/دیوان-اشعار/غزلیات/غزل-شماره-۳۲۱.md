---
title: >-
    غزل شمارهٔ  ۳۲۱
---
# غزل شمارهٔ  ۳۲۱

<div class="b" id="bn1"><div class="m1"><p>آن که هلاک من همی‌خواهد و من سلامتش</p></div>
<div class="m2"><p>هر چه کند ز شاهدی کس نکند ملامتش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>میوه نمی‌دهد به کس باغ تفرج است و بس</p></div>
<div class="m2"><p>جز به نظر نمی‌رسد سیب درخت قامتش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>داروی دل نمی‌کنم کان که مریض عشق شد</p></div>
<div class="m2"><p>هیچ دوا نیاورد باز به استقامتش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر که فدا نمی‌کند دنیی و دین و مال و سر</p></div>
<div class="m2"><p>گو غم نیکوان مخور تا نخوری ندامتش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جنگ نمی‌کنم اگر دست به تیغ می‌برد</p></div>
<div class="m2"><p>بلکه به خون مطالبت هم نکنم قیامتش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کاش که در قیامتش بار دگر بدیدمی</p></div>
<div class="m2"><p>کانچه گناه او بود من بکشم غرامتش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر که هوا گرفت و رفت از پی آرزوی دل</p></div>
<div class="m2"><p>گوش مدار سعدیا بر خبر سلامتش</p></div></div>