---
title: >-
    غزل شمارهٔ  ۳۰۱
---
# غزل شمارهٔ  ۳۰۱

<div class="b" id="bn1"><div class="m1"><p>هر شب اندیشه دیگر کنم و رای دگر</p></div>
<div class="m2"><p>که من از دست تو فردا بروم جای دگر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بامدادان که برون می‌نهم از منزل پای</p></div>
<div class="m2"><p>حسن عهدم نگذارد که نهم پای دگر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر کسی را سر چیزی و تمنای کسیست</p></div>
<div class="m2"><p>ما به غیر از تو نداریم تمنای دگر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زان که هرگز به جمال تو در آیینه وهم</p></div>
<div class="m2"><p>متصور نشود صورت و بالای دگر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>وامقی بود که دیوانه عذرایی بود</p></div>
<div class="m2"><p>منم امروز و تویی وامق و عذرای دگر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>وقت آنست که صحرا گل و سنبل گیرد</p></div>
<div class="m2"><p>خلق بیرون شده هر قوم به صحرای دگر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بامدادان به تماشای چمن بیرون آی</p></div>
<div class="m2"><p>تا فراغ از تو نماند به تماشای دگر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هر صباحی غمی از دور زمان پیش آید</p></div>
<div class="m2"><p>گویم این نیز نهم بر سر غم‌های دگر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بازگویم نه که دوران حیات این همه نیست</p></div>
<div class="m2"><p>سعدی امروز تحمل کن و فردای دگر</p></div></div>