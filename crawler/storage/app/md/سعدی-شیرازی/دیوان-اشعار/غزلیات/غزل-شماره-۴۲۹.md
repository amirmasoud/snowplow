---
title: >-
    غزل شمارهٔ  ۴۲۹
---
# غزل شمارهٔ  ۴۲۹

<div class="b" id="bn1"><div class="m1"><p>تو مپندار کز این در به ملامت بروم</p></div>
<div class="m2"><p>دلم اینجاست بده تا به سلامت بروم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ترک سر گفتم از آن پیش که بنهادم پای</p></div>
<div class="m2"><p>نه به زرق آمده‌ام تا به ملامت بروم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من هوادار قدیمم بدهم جان عزیز</p></div>
<div class="m2"><p>نو ارادت نه که از پیش غرامت بروم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر رسد از تو به گوشم که بمیر ای سعدی</p></div>
<div class="m2"><p>تا لب گور به اعزاز و کرامت بروم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ور بدانم به در مرگ که حشرم با توست</p></div>
<div class="m2"><p>از لحد رقص کنان تا به قیامت بروم</p></div></div>