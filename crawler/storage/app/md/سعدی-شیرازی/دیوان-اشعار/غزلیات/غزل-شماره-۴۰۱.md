---
title: >-
    غزل شمارهٔ  ۴۰۱
---
# غزل شمارهٔ  ۴۰۱

<div class="b" id="bn1"><div class="m1"><p>یک روز به شیدایی در زلف تو آویزم</p></div>
<div class="m2"><p>زان دو لب شیرینت صد شور برانگیزم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر قصد جفا داری اینک من و اینک سر</p></div>
<div class="m2"><p>ور راه وفا داری جان در قدمت ریزم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بس توبه و پرهیزم کز عشق تو باطل شد</p></div>
<div class="m2"><p>من بعد بدان شرطم کز توبه بپرهیزم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سیم دل مسکینم در خاک درت گم شد</p></div>
<div class="m2"><p>خاک سر هر کویی بی فایده می‌بیزم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در شهر به رسوایی دشمن به دفم برزد</p></div>
<div class="m2"><p>تا بر دف عشق آمد تیر نظر تیزم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مجنون رخ لیلی چون قیس بنی عامر</p></div>
<div class="m2"><p>فرهاد لب شیرین چون خسرو پرویزم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گفتی به غمم بنشین یا از سر جان برخیز</p></div>
<div class="m2"><p>فرمان برمت جانا بنشینم و برخیزم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر بی تو بود جنت بر کنگره ننشینم</p></div>
<div class="m2"><p>ور با تو بود دوزخ در سلسله آویزم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>با یاد تو گر سعدی در شعر نمی‌گنجد</p></div>
<div class="m2"><p>چون دوست یگانه شد با غیر نیامیزم</p></div></div>