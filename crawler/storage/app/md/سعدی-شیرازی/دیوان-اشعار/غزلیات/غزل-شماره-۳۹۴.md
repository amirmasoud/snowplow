---
title: >-
    غزل شمارهٔ  ۳۹۴
---
# غزل شمارهٔ  ۳۹۴

<div class="b" id="bn1"><div class="m1"><p>به خدا اگر بمیرم که دل از تو برنگیرم</p></div>
<div class="m2"><p>برو ای طبیبم از سر که دوا نمی‌پذیرم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همه عمر با حریفان بنشستمی و خوبان</p></div>
<div class="m2"><p>تو بخاستی و نقشت بنشست در ضمیرم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مده ای حکیم پندم که به کار در نبندم</p></div>
<div class="m2"><p>که ز خویشتن گزیر است و ز دوست ناگزیرم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>برو ای سپر ز پیشم که به جان رسید پیکان</p></div>
<div class="m2"><p>بگذار تا ببینم که که می‌زند به تیرم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نه نشاط دوستانم نه فراغ بوستانم</p></div>
<div class="m2"><p>بروید ای رفیقان به سفر که من اسیرم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تو در آب اگر ببینی حرکات خویشتن را</p></div>
<div class="m2"><p>به زبان خود بگویی که به حسن بی‌نظیرم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تو به خواب خوش بیاسای و به عیش و کامرانی</p></div>
<div class="m2"><p>که نه من غنوده‌ام دوش و نه مردم از نفیرم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نه توانگران ببخشند فقیر ناتوان را</p></div>
<div class="m2"><p>نظری کن ای توانگر که به دیدنت فقیرم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اگرم چو عود سوزی تن من فدای جانت</p></div>
<div class="m2"><p>که خوش است عیش مردم به روایح عبیرم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نه تو گفته‌ای که سعدی نبرد ز دست من جان</p></div>
<div class="m2"><p>نه به خاک پای مردان چو تو می‌کشی نمیرم</p></div></div>