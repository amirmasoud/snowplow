---
title: >-
    غزل شمارهٔ  ۱۶۳
---
# غزل شمارهٔ  ۱۶۳

<div class="b" id="bn1"><div class="m1"><p>هر که می با تو خورد عربده کرد</p></div>
<div class="m2"><p>هر که روی تو دید عشق آورد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زهر اگر در مذاق من ریزی</p></div>
<div class="m2"><p>با تو همچون شکر بشاید خورد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آفرین خدای بر پدری</p></div>
<div class="m2"><p>که تو فرزند نازنین پرورد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>لایق خدمت تو نیست بساط</p></div>
<div class="m2"><p>روی باید در این قدم گسترد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خواستم گفت خاک پای توام</p></div>
<div class="m2"><p>عقلم اندر زمان نصیحت کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفت در راه دوست خاک مباش</p></div>
<div class="m2"><p>نه که بر دامنش نشیند گرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دشمنان در مخالفت گرمند</p></div>
<div class="m2"><p>و آتش ما بدین نگردد سرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مرد عشق ار ز پیش تیر بلا</p></div>
<div class="m2"><p>روی درهم کشد مخوانش مرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هر که را برگ بی مرادی نیست</p></div>
<div class="m2"><p>گو برو گرد کوی عشق مگرد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سعدیا صاف وصل اگر ندهند</p></div>
<div class="m2"><p>ما و دردی کشان مجلس درد</p></div></div>