---
title: >-
    غزل شمارهٔ  ۵۹۹
---
# غزل شمارهٔ  ۵۹۹

<div class="b" id="bn1"><div class="m1"><p>چون تنگ نباشد دل مسکین حمامی</p></div>
<div class="m2"><p>که‌ش یار هم آواز بگیرند به دامی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دیشب همه شب دست در آغوش سلامت</p></div>
<div class="m2"><p>و امروز همه روز تمنای سلامی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آن بوی گل و سنبل و نالیدن بلبل</p></div>
<div class="m2"><p>خوش بود دریغا که نکردند دوامی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از من مطلب صبر جدایی که ندارم</p></div>
<div class="m2"><p>سنگیست فراق و دل محنت زده جامی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در هیچ مقامی دل مسکین نشکیبد</p></div>
<div class="m2"><p>خو کرده صحبت که برافتد ز مقامی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بی دوست حرام است جهان دیدن مشتاق</p></div>
<div class="m2"><p>قندیل بکش تا بنشینم به ظلامی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چندان بنشینم که برآید نفس صبح</p></div>
<div class="m2"><p>کان وقت به دل می‌رسد از دوست پیامی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آنجا که تویی رفتن ما سود ندارد</p></div>
<div class="m2"><p>الا به کرم پیش نهد لطف تو گامی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زان عین که دیدی اثری بیش نمانده‌ست</p></div>
<div class="m2"><p>جانی به دهان آمده در حسرت کامی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سعدی سخن یار نگوید بر اغیار</p></div>
<div class="m2"><p>هرگز نبرد سوخته‌ای قصه به خامی</p></div></div>