---
title: >-
    غزل شمارهٔ  ۵۹۶
---
# غزل شمارهٔ  ۵۹۶

<div class="b" id="bn1"><div class="m1"><p>مرا تو جان عزیزی و یار محترمی</p></div>
<div class="m2"><p>به هر چه حکم کنی بر وجود من حکمی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غمت مباد و گزندت مباد و درد مباد</p></div>
<div class="m2"><p>که مونس دل و آرام جان و دفع غمی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هزار تندی و سختی بکن که سهل بود</p></div>
<div class="m2"><p>جفای مثل تو بردن که سابق کرمی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ندانم از سر و پایت کدام خوبتر است</p></div>
<div class="m2"><p>چه جای فرق که زیبا ز فرق تا قدمی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگر هزار الم دارم از تو بر دل ریش</p></div>
<div class="m2"><p>هنوز مرهم ریشی و داروی المی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چنین که می‌گذری کافر و مسلمان را</p></div>
<div class="m2"><p>نگه به توست که هم قبله‌ای و هم صنمی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چنین جمال نشاید که هر نظر بیند</p></div>
<div class="m2"><p>مگر که نام خدا گرد خویشتن بدمی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نگویمت که گلی بر فراز سرو روان</p></div>
<div class="m2"><p>که آفتاب جهانتاب بر سر علمی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تو مشکبوی سیه چشم را که دریابد</p></div>
<div class="m2"><p>که همچو آهوی مشکین از آدمی برمی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کمند سعدی اگر شیر شرزه صید کند</p></div>
<div class="m2"><p>تو در کمند نیایی که آهوی حرمی</p></div></div>