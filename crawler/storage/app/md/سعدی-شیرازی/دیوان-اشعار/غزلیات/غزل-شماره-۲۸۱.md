---
title: >-
    غزل شمارهٔ  ۲۸۱
---
# غزل شمارهٔ  ۲۸۱

<div class="b" id="bn1"><div class="m1"><p>امیدوار چنانم که کار بسته برآید</p></div>
<div class="m2"><p>وصال چون به سر آمد فراق هم به سر آید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من از تو سیر نگردم و گر ترش کنی ابرو</p></div>
<div class="m2"><p>جواب تلخ ز شیرین مقابل شکر آید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به رغم دشمنم ای دوست سایه‌ای به سر آور</p></div>
<div class="m2"><p>که موش کور نخواهد که آفتاب برآید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گلم ز دست به دربرد روزگار مخالف</p></div>
<div class="m2"><p>امید هست که خارم ز پای هم به درآید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گرم حیات بماند نماند این غم و حسرت</p></div>
<div class="m2"><p>و گر نمیرد بلبل درخت گل به بر آید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز بس که در نظر آمد خیال روی تو ما را</p></div>
<div class="m2"><p>چنان شدم که به جهدم خیال در نظر آید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هزار قرعه به نامت زدیم و بازنگشتی</p></div>
<div class="m2"><p>ندانم آیت رحمت به طالع که برآید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ضرورتست که روزی به کوه رفته ز دستت</p></div>
<div class="m2"><p>چنان بگرید سعدی که آب تا کمر آید</p></div></div>