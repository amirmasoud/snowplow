---
title: >-
    غزل شمارهٔ  ۵۲۳
---
# غزل شمارهٔ  ۵۲۳

<div class="b" id="bn1"><div class="m1"><p>همه عمر برندارم سر از این خمار مستی</p></div>
<div class="m2"><p>که هنوز من نبودم که تو در دلم نشستی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو نه مثل آفتابی که حضور و غیبت افتد</p></div>
<div class="m2"><p>دگران روند و آیند و تو همچنان که هستی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چه حکایت از فراقت که نداشتم ولیکن</p></div>
<div class="m2"><p>تو چو روی باز کردی دَرِ ماجرا ببستی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نظری به دوستان کن که هزار بار از آن بِه</p></div>
<div class="m2"><p>که تحیّتی نویسی و هدیتی فرستی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دل دردمند ما را که اسیر توست یارا</p></div>
<div class="m2"><p>به وصالْ مرهمی نِه چو به انتظار خستی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نه عجب که قلب دشمن شکنی به روز هَیجا</p></div>
<div class="m2"><p>تو که قلب دوستان را به مفارقت شکستی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>برو ای فقیه دانا به خدای بخش ما را</p></div>
<div class="m2"><p>تو و زهد و پارسایی من و عاشقی و مستی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دل هوشمند باید که به دلبری سپاری</p></div>
<div class="m2"><p>که چو قبله‌ایت باشد بِه از آن که خود پرستی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو زمام بخت و دولت نه به دست جهد باشد</p></div>
<div class="m2"><p>چه کنند اگر زبونی نکنند و زیردستی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گِله از فراق یاران و جفای روزگاران</p></div>
<div class="m2"><p>نه طریق توست سعدی کم خویش گیر و رستی</p></div></div>