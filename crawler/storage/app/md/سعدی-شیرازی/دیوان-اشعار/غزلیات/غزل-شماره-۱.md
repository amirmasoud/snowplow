---
title: >-
    غزل شمارهٔ  ۱
---
# غزل شمارهٔ  ۱

<div class="b" id="bn1"><div class="m1"><p>اوّلِ دفتر به نامِ ایزدِ دانا</p></div>
<div class="m2"><p>صانعِ پروردگارِ حیِّ توانا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اکبر و اعظم، خدایِ عالَم و آدم</p></div>
<div class="m2"><p>صورتِ خوب آفرید و سیرتِ زیبا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از درِ بخشندگیّ و بنده‌نوازی</p></div>
<div class="m2"><p>مرغ هوا را نصیب و ماهی دریا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>قسمتِ خود می‌خورند مُنعِم و درویش</p></div>
<div class="m2"><p>روزیِ خود می‌بَرند پشّه و عَنقا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حاجتِ موری به علمِ غیب بداند</p></div>
<div class="m2"><p>در بُنِ چاهی به زیرِ صخرهٔ صَمّا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جانور از نطفه می‌کُند، شَکر از نِی</p></div>
<div class="m2"><p>برگِ تر از چوبِ خشک و چشمه ز خارا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شربتِ نوش آفرید از مگسِ نَحل</p></div>
<div class="m2"><p>نخلِ تناور کُنَد ز دانهٔ خرما</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از همگان بی‌نیاز و بر همه مُشفِق</p></div>
<div class="m2"><p>از همه عالَم نهان و بر همه پیدا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>پرتوِ نورِ سُرادِقاتِ جلالش</p></div>
<div class="m2"><p>از عظمت ماورایِ فکرتِ دانا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خود نه زبان در دهانِ عارفِ مدهوش</p></div>
<div class="m2"><p>حمد و ثنا می‌کُند که مویْ بر اعضا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هرکه نداند سپاسِ نعمتْ امروز</p></div>
<div class="m2"><p>حیف خورَد بر نصیبِ رحمتْ فردا</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بارخدایا مُهَیمِنیّ و مدبّر</p></div>
<div class="m2"><p>وز همه عیبی مقدّسیّ و مبرّا</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ما نتوانیم حقِّ حمدِ تو گفتن</p></div>
<div class="m2"><p>با همه کرّوبیانِ عالَمِ بالا</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>سعدی از آن‌جا که فهمِ اوست سخن گفت</p></div>
<div class="m2"><p>ور نه کمالِ تو، وهم کِی رسد آن‌جا؟</p></div></div>