---
title: >-
    غزل شمارهٔ  ۳۳۰
---
# غزل شمارهٔ  ۳۳۰

<div class="b" id="bn1"><div class="m1"><p>زینهار از دهان خندانش</p></div>
<div class="m2"><p>و آتش لعل و آب دندانش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مگر آن دایه کاین صنم پرورد</p></div>
<div class="m2"><p>شهد بوده‌ست شیر پستانش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>باغبان گر ببیند این رفتار</p></div>
<div class="m2"><p>سرو بیرون کند ز بستانش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ور چنین حور در بهشت آید</p></div>
<div class="m2"><p>همه خادم شوند غلمانش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چاهی اندر ره مسلمانان</p></div>
<div class="m2"><p>نیست الا چه زنخدانش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چند خواهی چو من بر این لب چاه</p></div>
<div class="m2"><p>متعطش بر آب حیوانش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شاید این روی اگر سبیل کند</p></div>
<div class="m2"><p>بر تماشاکنان حیرانش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ساربانا جمال کعبه کجاست</p></div>
<div class="m2"><p>که بمردیم در بیابانش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بس که در خاک می‌طپند چو گوی</p></div>
<div class="m2"><p>از خم زلف همچو چوگانش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>لاجرم عقل منهزم شد و صبر</p></div>
<div class="m2"><p>که نبودند مرد میدانش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ما دگر بی تو صبر نتوانیم</p></div>
<div class="m2"><p>که همین بود حد امکانش</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>از ملامت چه غم خورد سعدی</p></div>
<div class="m2"><p>مرده از نیشتر مترسانش</p></div></div>