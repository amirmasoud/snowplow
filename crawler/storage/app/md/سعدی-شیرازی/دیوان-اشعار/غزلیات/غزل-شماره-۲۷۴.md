---
title: >-
    غزل شمارهٔ  ۲۷۴
---
# غزل شمارهٔ  ۲۷۴

<div class="b" id="bn1"><div class="m1"><p>چه سروست آن که بالا می‌نماید</p></div>
<div class="m2"><p>عنان از دست دل‌ها می‌رباید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که زاد این صورت منظور محبوب</p></div>
<div class="m2"><p>از این صورت ندانم تا چه زاید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگر صد نوبتش چون قرص خورشید</p></div>
<div class="m2"><p>ببینم آب در چشم من آید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کس اندر عهد ما مانند وی نیست</p></div>
<div class="m2"><p>ولی ترسم به عهد ما نپاید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فراغت زان طرف چندان که خواهی</p></div>
<div class="m2"><p>وزین جانب محبت می‌فزاید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حدیث عشق جانان گفتنی نیست</p></div>
<div class="m2"><p>و گر گویی کسی همدرد باید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>درازای شب از ناخفتگان پرس</p></div>
<div class="m2"><p>که خواب آلوده را کوته نماید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مرا پای گریز از دست او نیست</p></div>
<div class="m2"><p>اگر می‌بنددم ور می‌گشاید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>رها کن تا بیفتد ناتوانی</p></div>
<div class="m2"><p>که با سرپنجگان زور آزماید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نشاید خون سعدی بی سبب ریخت</p></div>
<div class="m2"><p>ولیکن چون مراد اوست شاید</p></div></div>