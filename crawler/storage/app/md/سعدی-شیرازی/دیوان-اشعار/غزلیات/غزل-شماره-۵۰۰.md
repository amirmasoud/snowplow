---
title: >-
    غزل شمارهٔ  ۵۰۰
---
# غزل شمارهٔ  ۵۰۰

<div class="b" id="bn1"><div class="m1"><p>تا کیم انتظار فرمایی</p></div>
<div class="m2"><p>وقت نامد که روی بنمایی؟!</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگرم زنده باز خواهی دید</p></div>
<div class="m2"><p>رنجه شو پیشتر چرا نایی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عمر کوته‌تر است از آن که تو نیز</p></div>
<div class="m2"><p>در درازی وعده افزایی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از تو کی برخورم که در وعده</p></div>
<div class="m2"><p>سپری گشت عهد برنایی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نرسیدیم در تو و نرسد</p></div>
<div class="m2"><p>هیچ بیچاره را شکیبایی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به سر راهت آورم هر شب</p></div>
<div class="m2"><p>دیده‌ای در وداع بینایی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>روز من شب شود و شب روزم</p></div>
<div class="m2"><p>چون ببندی نقاب و بگشایی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بر رخ سعدی از خیال تو دوش</p></div>
<div class="m2"><p>زرگری بود و سیم پالایی</p></div></div>