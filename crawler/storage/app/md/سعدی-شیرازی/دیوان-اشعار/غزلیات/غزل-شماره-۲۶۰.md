---
title: >-
    غزل شمارهٔ  ۲۶۰
---
# غزل شمارهٔ  ۲۶۰

<div class="b" id="bn1"><div class="m1"><p>من چه در پای تو ریزم که خورای تو بود</p></div>
<div class="m2"><p>سر نه چیزیست که شایسته پای تو بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خرم آن روی که در روی تو باشد همه عمر</p></div>
<div class="m2"><p>وین نباشد مگر آن وقت که رای تو بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ذره‌ای در همه اجزای من مسکین نیست</p></div>
<div class="m2"><p>که نه آن ذره معلق به هوای تو بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا تو را جای شد ای سرو روان در دل من</p></div>
<div class="m2"><p>هیچ کس می‌نپسندم که به جای تو بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به وفای تو که گر خشت زنند از گل من</p></div>
<div class="m2"><p>همچنان در دل من مهر و وفای تو بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>غایت آنست که ما در سر کار تو رویم</p></div>
<div class="m2"><p>مرگ ما باک نباشد چو بقای تو بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>من پروانه صفت پیش تو ای شمع چگل</p></div>
<div class="m2"><p>گر بسوزم گنه من نه خطای تو بود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عجبست آن که تو را دید و حدیث تو شنید</p></div>
<div class="m2"><p>که همه عمر نه مشتاق لقای تو بود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خوش بود ناله دلسوختگان از سر درد</p></div>
<div class="m2"><p>خاصه دردی که به امید دوای تو بود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ملک دنیا همه با همت سعدی هیچست</p></div>
<div class="m2"><p>پادشاهیش همین بس که گدای تو بود</p></div></div>