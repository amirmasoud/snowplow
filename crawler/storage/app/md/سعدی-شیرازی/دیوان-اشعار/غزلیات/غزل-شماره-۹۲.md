---
title: >-
    غزل شمارهٔ  ۹۲
---
# غزل شمارهٔ  ۹۲

<div class="b" id="bn1"><div class="m1"><p>کس به چشمم در نمی‌آید که گویم مثل اوست</p></div>
<div class="m2"><p>خود به چشم عاشقان صورت نبندد مثل دوست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر که با مستان نشیند ترک مستوری کند</p></div>
<div class="m2"><p>آبروی نیکنامان در خرابات آب جوست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جز خداوندان معنی را نغلطاند سماع</p></div>
<div class="m2"><p>اولت مغزی بباید تا برون آیی ز پوست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بنده‌ام گو تاج خواهی بر سرم نه یا تبر</p></div>
<div class="m2"><p>هر چه پیش عاشقان آید ز معشوقان نکوست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عقل باری خسروی می‌کرد بر ملک وجود</p></div>
<div class="m2"><p>باز چون فرهاد عاشق بر لب شیرین اوست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عنبرین چوگان زلفش را گر استقصا کنی</p></div>
<div class="m2"><p>زیر هر مویی دلی بینی که سرگردان چو گوست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سعدیا چندان که خواهی گفت وصف روی یار</p></div>
<div class="m2"><p>حسن گل بیش از قیاس بلبل بسیارگوست</p></div></div>