---
title: >-
    غزل شمارهٔ  ۲۳۰
---
# غزل شمارهٔ  ۲۳۰

<div class="b" id="bn1"><div class="m1"><p>شاید این طلعت میمون که به فالش دارند</p></div>
<div class="m2"><p>در دل اندیشه و در دیده خیالش دارند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که در آفاق چنین روی دگر نتوان دید</p></div>
<div class="m2"><p>یا مگر آینه در پیش جمالش دارند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عجب از دام غمش گر بجهد مرغ دلی</p></div>
<div class="m2"><p>این همه میل که با دانه خالش دارند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نازنینی که سر اندر قدمش باید باخت</p></div>
<div class="m2"><p>نه حریفی که توقع به وصالش دارند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>غالب آنست که مرغی چو به دامی افتاد</p></div>
<div class="m2"><p>تا به جایی نرود بی پر و بالش دارند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عشق لیلی نه به اندازه هر مجنونیست</p></div>
<div class="m2"><p>مگر آنان که سر ناز و دلالش دارند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دوستی با تو حرامست که چشمان کشت</p></div>
<div class="m2"><p>خون عشاق بریزند و حلالش دارند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خرما دور وصالی و خوشا درد دلی</p></div>
<div class="m2"><p>که به معشوق توان گفت و مجالش دارند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>حال سعدی تو ندانی که تو را دردی نیست</p></div>
<div class="m2"><p>دردمندان خبر از صورت حالش دارند</p></div></div>