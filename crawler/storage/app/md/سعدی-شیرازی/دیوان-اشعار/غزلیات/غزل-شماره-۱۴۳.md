---
title: >-
    غزل شمارهٔ  ۱۴۳
---
# غزل شمارهٔ  ۱۴۳

<div class="b" id="bn1"><div class="m1"><p>این که تو داری قیامتست نه قامت</p></div>
<div class="m2"><p>وین نه تبسم که معجزست و کرامت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر که تماشای روی چون قمرت کرد</p></div>
<div class="m2"><p>سینه سپر کرد پیش تیر ملامت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر شب و روزی که بی تو می‌رود از عمر</p></div>
<div class="m2"><p>بر نفسی می‌رود هزار ندامت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عمر نبود آن چه غافل از تو نشستم</p></div>
<div class="m2"><p>باقی عمر ایستاده‌ام به غرامت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سرو خرامان چو قد معتدلت نیست</p></div>
<div class="m2"><p>آن همه وصفش که می‌کنند به قامت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چشم مسافر که بر جمال تو افتاد</p></div>
<div class="m2"><p>عزم رحیلش بدل شود به اقامت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اهل فریقین در تو خیره بمانند</p></div>
<div class="m2"><p>گر بروی در حسابگاه قیامت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>این همه سختی و نامرادی سعدی</p></div>
<div class="m2"><p>چون تو پسندی سعادتست و سلامت</p></div></div>