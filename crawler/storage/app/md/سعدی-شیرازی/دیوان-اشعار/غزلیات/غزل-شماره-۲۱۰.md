---
title: >-
    غزل شمارهٔ  ۲۱۰
---
# غزل شمارهٔ  ۲۱۰

<div class="b" id="bn1"><div class="m1"><p>خواب خوش من ای پسر دستخوش خیال شد</p></div>
<div class="m2"><p>نقد امید عمر من در طلب وصال شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر نشد اشتیاق او غالب صبر و عقل من</p></div>
<div class="m2"><p>این به چه زیردست گشت آن به چه پایمال شد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر من اگر حرام شد وصل تو نیست بوالعجب</p></div>
<div class="m2"><p>بوالعجب آن که خون من بر تو چرا حلال شد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پرتو آفتاب اگر بدر کند هلال را</p></div>
<div class="m2"><p>بدر وجود من چرا در نظرت هلال شد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زیبد اگر طلب کند عزت ملک مصر دل</p></div>
<div class="m2"><p>آن که هزار یوسفش بنده جاه و مال شد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>طرفه مدار اگر ز دل نعره بیخودی زنم</p></div>
<div class="m2"><p>کآتش دل چو شعله زد صبر در او محال شد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سعدی اگر نظر کند تا نه غلط گمان بری</p></div>
<div class="m2"><p>کاو نه به رسم دیگران بنده زلف و خال شد</p></div></div>