---
title: >-
    غزل شمارهٔ  ۵۹۵
---
# غزل شمارهٔ  ۵۹۵

<div class="b" id="bn1"><div class="m1"><p>هرگز حسد نبردم بر منصبی و مالی</p></div>
<div class="m2"><p>الا بر آن که دارد با دلبری وصالی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دانی کدام دولت در وصف می‌نیاید</p></div>
<div class="m2"><p>چشمی که باز باشد هر لحظه بر جمالی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خرم تنی که محبوب از در فرازش آید</p></div>
<div class="m2"><p>چون رزق نیکبختان بی محنت سؤالی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همچون دو مغز بادام اندر یکی خزینه</p></div>
<div class="m2"><p>با هم گرفته انسی وز دیگران ملالی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دانی کدام جاهل بر حال ما بخندد</p></div>
<div class="m2"><p>کاو را نبوده باشد در عمر خویش حالی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بعد از حبیب بر من نگذشت جز خیالش</p></div>
<div class="m2"><p>وز پیکر ضعیفم نگذاشت جز خیالی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اول که گوی بردی من بودمی به دانش</p></div>
<div class="m2"><p>گر سودمند بودی بی دولت احتیالی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سال وصال با او یک روز بود گویی</p></div>
<div class="m2"><p>و اکنون در انتظارش روزی به قدر سالی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ایام را به ماهی یک شب هلال باشد</p></div>
<div class="m2"><p>وآن ماه دلستان را هر ابرویی هلالی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>صوفی نظر نبازد جز با چنین حریفی</p></div>
<div class="m2"><p>سعدی غزل نگوید جز بر چنین غزالی</p></div></div>