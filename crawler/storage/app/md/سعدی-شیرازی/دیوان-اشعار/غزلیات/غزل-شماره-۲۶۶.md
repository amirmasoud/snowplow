---
title: >-
    غزل شمارهٔ  ۲۶۶
---
# غزل شمارهٔ  ۲۶۶

<div class="b" id="bn1"><div class="m1"><p>در من این عیب قدیمست و به در می‌نرود</p></div>
<div class="m2"><p>که مرا بی می و معشوق به سر می‌نرود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صبرم از دوست مفرمای و تعنت بگذار</p></div>
<div class="m2"><p>کاین بلاییست که از طبع بشر می‌نرود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مرغ مألوف که با خانه خدا انس گرفت</p></div>
<div class="m2"><p>گر به سنگش بزنی جای دگر می‌نرود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عجب از دیده گریان منت می‌آید</p></div>
<div class="m2"><p>عجب آنست کز او خون جگر می‌نرود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>من از این بازنیایم که گرفتم در پیش</p></div>
<div class="m2"><p>اگرم می‌رود از پیش اگر می‌نرود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خواستم تا نظری بنگرم و بازآیم</p></div>
<div class="m2"><p>گفت از این کوچه ما راه به در می‌نرود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جور معشوق چنان نیست که الزام رقیب</p></div>
<div class="m2"><p>گویی ابریست که از پیش قمر می‌نرود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا تو منظور پدید آمدی ای فتنه پارس</p></div>
<div class="m2"><p>هیچ دل نیست که دنبال نظر می‌نرود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زخم شمشیر غمت را به شکیبایی و عقل</p></div>
<div class="m2"><p>چند مرهم بنهادیم و اثر می‌نرود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ترک دنیا و تماشا و تنعم گفتیم</p></div>
<div class="m2"><p>مهر مهریست که چون نقش حجر می‌نرود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>موضعی در همه آفاق ندانم امروز</p></div>
<div class="m2"><p>کز حدیث من و حسن تو خبر می‌نرود</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ای که گفتی مرو اندر پی خوبان سعدی</p></div>
<div class="m2"><p>چند گویی مگس از پیش شکر می‌نرود</p></div></div>