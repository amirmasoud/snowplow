---
title: >-
    غزل شمارهٔ  ۷۰
---
# غزل شمارهٔ  ۷۰

<div class="b" id="bn1"><div class="m1"><p>ای که از سرو روان قد تو چالاکترست</p></div>
<div class="m2"><p>دل به روی تو ز روی تو طربناکترست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دگر از حربه‌ی خونخوار اجل نندیشم</p></div>
<div class="m2"><p>که نه از غمزه‌ی خون‌ریز تو ناباکترست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چست بودست مرا کسوت معنی همه وقت</p></div>
<div class="m2"><p>باز بر قامت زیبای تو چالاکترست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نظر پاک مرا دشمن اگر طعنه زند</p></div>
<div class="m2"><p>دامن دوست بحمدالله از آن پاکترست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا گل روی تو در باغ لطافت بشکفت</p></div>
<div class="m2"><p>پردهٔ صبر من از دامن گل چاکترست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پای بر دیده‌ی سعدی نه اگر بخرامی</p></div>
<div class="m2"><p>که به صد منزلت از خاک درت خاکترست</p></div></div>