---
title: >-
    غزل شمارهٔ  ۵۱۸
---
# غزل شمارهٔ  ۵۱۸

<div class="b" id="bn1"><div class="m1"><p>تو خون خلق بریزی و روی درتابی</p></div>
<div class="m2"><p>ندانمت چه مکافات این گنه یابی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تَصُدُّ عَنّی فِی الجَوْرِ وَ النّوی لکِن</p></div>
<div class="m2"><p>اِلَیْکَ قَلْبی یا غایَةَ المَنَی صابٍ</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو عندلیب چه فریادها که می‌دارم</p></div>
<div class="m2"><p>تو از غرور جوانی همیشه در خوابی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اِلَی العُداةِ وَصَلْتُمْ وَ تَصْحُبونَهُمُ</p></div>
<div class="m2"><p>وَ فی وِدادِکُمُ قَدْ هَجَرْتُ اَحْبابی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نه هر که صاحب حسن است جور پیشه کند</p></div>
<div class="m2"><p>تو را چه شد که خود اندر کمین اصحابی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اَحِبَّتی اَمَرونی بِتَرْکِ ذِکْراهُ</p></div>
<div class="m2"><p>لَقَدْ اَطَعْتُ وَ لکِنَّ حُبَّهُ آبٍ</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>غمت چگونه بپوشم که دیده بر رویت</p></div>
<div class="m2"><p>همی گواهی بر من دهد به کذابی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مرا تو بر سر آتش نشانده‌ای عجب آنک</p></div>
<div class="m2"><p>منم در آتش و از حال من تو در تابی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>من از تو سیر نگردم که صاحب استسقا</p></div>
<div class="m2"><p>نه ممکن است که هرگز رسد به سیرابی</p></div></div>