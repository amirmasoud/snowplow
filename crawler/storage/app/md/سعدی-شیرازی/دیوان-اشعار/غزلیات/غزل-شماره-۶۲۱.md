---
title: >-
    غزل شمارهٔ  ۶۲۱
---
# غزل شمارهٔ  ۶۲۱

<div class="b" id="bn1"><div class="m1"><p>سرو ایستاده به چو تو رفتار می‌کنی</p></div>
<div class="m2"><p>طوطی خموش به چو تو گفتار می‌کنی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کس دل به اختیار به مهرت نمی‌دهد</p></div>
<div class="m2"><p>دامی نهاده‌ای که گرفتار می‌کنی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تو خود چه فتنه‌ای که به چشمان ترک مست</p></div>
<div class="m2"><p>تاراج عقل مردم هشیار می‌کنی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از دوستی که دارم و غیرت که می‌برم</p></div>
<div class="m2"><p>خشم آیدم که چشم به اغیار می‌کنی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفتی نظر خطاست تو دل می‌بری رواست</p></div>
<div class="m2"><p>خود کرده جرم و خلق گنهکار می‌کنی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هرگز فرامشت نشود دفتر خلاف</p></div>
<div class="m2"><p>با دوستان چنین که تو تکرار می‌کنی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دستان به خون تازه بیچارگان خضاب</p></div>
<div class="m2"><p>هرگز کس این کند که تو عیار می‌کنی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>با دشمنان موافق و با دوستان به خشم</p></div>
<div class="m2"><p>یاری نباشد این که تو با یار می‌کنی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تا من سماع می‌شنوم پند نشنوم</p></div>
<div class="m2"><p>ای مدعی نصیحت بی‌کار می‌کنی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گر تیغ می‌زنی سپر اینک وجود من</p></div>
<div class="m2"><p>صلح است از این طرف که تو پیکار می‌کنی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از روی دوست تا نکنی رو به آفتاب</p></div>
<div class="m2"><p>کز آفتاب روی به دیوار می‌کنی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>زنهار سعدی از دل سنگین کافرش</p></div>
<div class="m2"><p>کافر چه غم خورد چو تو زنهار می‌کنی</p></div></div>