---
title: >-
    غزل شمارهٔ  ۵۹
---
# غزل شمارهٔ  ۵۹

<div class="b" id="bn1"><div class="m1"><p>آن تویی یا سرو بستانی به رفتار آمده‌ست</p></div>
<div class="m2"><p>یا ملک در صورت مردم به گفتار آمده‌ست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن پری کز خلق پنهان بود چندین روزگار</p></div>
<div class="m2"><p>باز می‌بینم که در عالم پدیدار آمده‌ست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عود می‌سوزند یا گل می‌دمد در بوستان</p></div>
<div class="m2"><p>دوستان یا کاروان مشک تاتار آمده‌ست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا مرا با نقش رویش آشنایی اوفتاد</p></div>
<div class="m2"><p>هر چه می‌بینم به چشمم نقش دیوار آمده‌ست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ساربانا یک نظر در روی آن زیبا نگار</p></div>
<div class="m2"><p>گر به جانی می‌دهد اینک خریدار آمده‌ست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>من دگر در خانه ننشینم اسیر و دردمند</p></div>
<div class="m2"><p>خاصه این ساعت که گفتی گل به بازار آمده‌ست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر تو انکار نظر در آفرینش می‌کنی</p></div>
<div class="m2"><p>من همی‌گویم که چشم از بهر این کار آمده‌ست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>وه که گر من بازبینم روی یار خویش را</p></div>
<div class="m2"><p>مرده‌ای بینی که با دنیا دگربار آمده‌ست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آن چه بر من می‌رود در بندت ای آرام جان</p></div>
<div class="m2"><p>با کسی گویم که در بندی گرفتار آمده‌ست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نی که می‌نالد همی در مجلس آزادگان</p></div>
<div class="m2"><p>زان همی‌نالد که بر وی زخم بسیار آمده‌ست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تا نپنداری که بعد از چشم خواب آلود تو</p></div>
<div class="m2"><p>تا برفتی خوابم اندر چشم بیدار آمده‌ست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>سعدیا گر همتی داری منال از جور یار</p></div>
<div class="m2"><p>تا جهان بوده‌ست جور یار بر یار آمده‌ست</p></div></div>