---
title: >-
    غزل شمارهٔ  ۴۶۴
---
# غزل شمارهٔ  ۴۶۴

<div class="b" id="bn1"><div class="m1"><p>دست با سرو روان چون نرسد در گردن</p></div>
<div class="m2"><p>چاره‌ای نیست به جز دیدن و حسرت خوردن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آدمی را که طلب هست و توانایی نیست</p></div>
<div class="m2"><p>صبر اگر هست و گر نیست بباید کردن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بند بر پای توقف چه کند گر نکند</p></div>
<div class="m2"><p>شرط عشق است بلا دیدن و پای افشردن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>روی در خاک در دوست بباید مالید</p></div>
<div class="m2"><p>چون میسر نشود روی به روی آوردن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نیم جانی چه بود تا ندهد دوست به دوست</p></div>
<div class="m2"><p>که به صد جان دل جانان نتوان آزردن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سهل باشد سخن سخت که خوبان گویند</p></div>
<div class="m2"><p>جور شیرین دهنان تلخ نباشد بردن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هیچ شک می‌نکنم کآهوی مشکین تتار</p></div>
<div class="m2"><p>شرم دارد ز تو مشکین خط آهو گردن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>روزی اندر سر کار تو کنم جان عزیز</p></div>
<div class="m2"><p>پیش بالای تو باری چو بباید مردن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سعدیا دیده نگه داشتن از صورت خوب</p></div>
<div class="m2"><p>نه چنان است که دل دادن و جان پروردن</p></div></div>