---
title: >-
    غزل شمارهٔ  ۱۸۸
---
# غزل شمارهٔ  ۱۸۸

<div class="b" id="bn1"><div class="m1"><p>به حدیث در نیایی که لبت شکر نریزد</p></div>
<div class="m2"><p>نچمی که شاخ طوبی به ستیزه بر نریزد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هوس تو هیچ طبعی نپزد که سر نبازد</p></div>
<div class="m2"><p>ز پی تو هیچ مرغی نپرد که پر نریزد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دلم از غمت زمانی نتواند ار ننالد</p></div>
<div class="m2"><p>مژه یک دم آب حسرت نشکیبد ار نریزد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>که نه من ز دست خوبان نبرم به عاقبت جان</p></div>
<div class="m2"><p>تو مرا بکش که خونم ز تو خوبتر نریزد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>درر است لفظ سعدی ز فراز بحر معنی</p></div>
<div class="m2"><p>چه کند به دامنی در که به دوست بر نریزد</p></div></div>