---
title: >-
    غزل شمارهٔ  ۳۱۰
---
# غزل شمارهٔ  ۳۱۰

<div class="b" id="bn1"><div class="m1"><p>ای به خلق از جهانیان ممتاز</p></div>
<div class="m2"><p>چشم خلقی به روی خوب تو باز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>لازم است آن که دارد این همه لطف</p></div>
<div class="m2"><p>که تحمل کنندش این همه ناز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای به عشق درخت بالایت</p></div>
<div class="m2"><p>مرغ جان رمیده در پرواز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آن نه صاحب نظر بود که کند</p></div>
<div class="m2"><p>از چنین روی در به روی فراز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بخورم گر ز دست توست نبید</p></div>
<div class="m2"><p>نکنم گر خلاف توست نماز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر بگریم چو شمع معذورم</p></div>
<div class="m2"><p>کس نگوید در آتشم مگداز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>می‌نگفتم سخن در آتش عشق</p></div>
<div class="m2"><p>تا نگفت آب دیده غماز</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آب و آتش خلاف یک دگرند</p></div>
<div class="m2"><p>نشنیدیم عشق و صبر انباز</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هر که دیدار دوست می‌طلبد</p></div>
<div class="m2"><p>دوستی را حقیقت است و مجاز</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آرزومند کعبه را شرط است</p></div>
<div class="m2"><p>که تحمل کند نشیب و فراز</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سعدیا زنده عاشقی باشد</p></div>
<div class="m2"><p>که بمیرد بر آستان نیاز</p></div></div>