---
title: >-
    غزل شمارهٔ  ۱۸۹
---
# غزل شمارهٔ  ۱۸۹

<div class="b" id="bn1"><div class="m1"><p>آه اگر دست دل من به تمنا نرسد</p></div>
<div class="m2"><p>یا دل از چنبر عشق تو به من وانرسد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غم هجران به سویت‌تر از این قسمت کن</p></div>
<div class="m2"><p>کاین همه درد به جان من تنها نرسد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سروبالای منا گر به چمن برگذری</p></div>
<div class="m2"><p>سرو بالای تو را سرو به بالا نرسد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون تویی را چو منی در نظر آید هیهات</p></div>
<div class="m2"><p>که قیامت رسد این رشته به هم یا نرسد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز آسمان بگذرم ار بر منت افتد نظری</p></div>
<div class="m2"><p>ذره تا مهر نبیند به ثریا نرسد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر سر خوان لبت دست چو من درویشی</p></div>
<div class="m2"><p>به گدایی رسد آخر چو به یغما نرسد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ابر چشمانم اگر قطره چنین خواهد ریخت</p></div>
<div class="m2"><p>بوالعجب دارم اگر سیل به دریا نرسد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هجر بپسندم اگر وصل میسر نشود</p></div>
<div class="m2"><p>خار بردارم اگر دست به خرما نرسد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سعدیا کنگره وصل بلندست و هر آنک</p></div>
<div class="m2"><p>پای بر سر ننهد دست وی آن جا نرسد</p></div></div>