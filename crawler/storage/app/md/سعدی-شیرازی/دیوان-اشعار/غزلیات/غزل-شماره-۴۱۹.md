---
title: >-
    غزل شمارهٔ  ۴۱۹
---
# غزل شمارهٔ  ۴۱۹

<div class="b" id="bn1"><div class="m1"><p>مرا تا نقره باشد می‌فشانم</p></div>
<div class="m2"><p>تو را تا بوسه باشد می‌ستانم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>و گر فردا به زندان می‌برندم</p></div>
<div class="m2"><p>به نقد این ساعت اندر بوستانم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جهان بگذار تا بر من سر آید</p></div>
<div class="m2"><p>که کام دل تو بودی از جهانم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چه دامن‌های گل باشد در این باغ</p></div>
<div class="m2"><p>اگر چیزی نگوید باغبانم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نمی‌دانستم از بخت همایون</p></div>
<div class="m2"><p>که سیمرغی فتد در آشیانم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تو عشق آموختی در شهر ما را</p></div>
<div class="m2"><p>بیا تا شرح آن هم بر تو خوانم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سخن‌ها دارم از دست تو در دل</p></div>
<div class="m2"><p>ولیکن در حضورت بی زبانم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بگویم تا بداند دشمن و دوست</p></div>
<div class="m2"><p>که من مستی و مستوری ندانم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مگو سعدی مراد خویش برداشت</p></div>
<div class="m2"><p>اگر تو سنگدل من مهربانم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>اگر تو سرو سیمین تن بر آنی</p></div>
<div class="m2"><p>که از پیشم برانی من بر آنم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>که تا باشم خیالت می‌پرستم</p></div>
<div class="m2"><p>و گر رفتم سلامت می‌رسانم</p></div></div>