---
title: >-
    غزل شمارهٔ  ۳۱۴
---
# غزل شمارهٔ  ۳۱۴

<div class="b" id="bn1"><div class="m1"><p>مبارک‌تر شب و خرم‌ترین روز</p></div>
<div class="m2"><p>به استقبالم آمد بخت پیروز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دهل‌زن گو دو نوبت زن بشارت</p></div>
<div class="m2"><p>که دوشم قدر بود امروز نوروز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مه است این یا ملک یا آدمیزاد</p></div>
<div class="m2"><p>پری یا آفتاب عالم‌افروز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ندانستی که ضدان در کمینند</p></div>
<div class="m2"><p>نکو کردی علی رغم بدآموز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مرا با دوست ای دشمن وصالست</p></div>
<div class="m2"><p>تو را گر دل نخواهد دیده بردوز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شبان دانم که از درد جدایی</p></div>
<div class="m2"><p>نیاسودم ز فریاد جهان سوز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر آن شب‌های باوحشت نمی‌بود</p></div>
<div class="m2"><p>نمی‌دانست سعدی قدر این روز</p></div></div>