---
title: >-
    غزل شمارهٔ  ۳۲۳
---
# غزل شمارهٔ  ۳۲۳

<div class="b" id="bn1"><div class="m1"><p>هر که نازک بود تن یارش</p></div>
<div class="m2"><p>گو دل نازنین نگه دارش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عاشق گل دروغ می‌گوید</p></div>
<div class="m2"><p>که تحمل نمی‌کند خارش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نیکخواها در آتشم بگذار</p></div>
<div class="m2"><p>وین نصیحت مکن که بگذارش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کاش با دل هزار جان بودی</p></div>
<div class="m2"><p>تا فدا کردمی به دیدارش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عاشق صادق از ملامت دوست</p></div>
<div class="m2"><p>گر برنجد به دوست مشمارش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کس به آرام جان ما نرسد</p></div>
<div class="m2"><p>که نه اول به جان رسد کارش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خانه یار سنگدل این است</p></div>
<div class="m2"><p>هر که سر می‌زند به دیوارش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خون ما خود محل آن دارد</p></div>
<div class="m2"><p>که بود پیش دوست مقدارش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سعدیا گر به جان خطاب کند</p></div>
<div class="m2"><p>ترک جان گوی و دل به دست آرش</p></div></div>