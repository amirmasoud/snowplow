---
title: >-
    غزل شمارهٔ  ۳۸
---
# غزل شمارهٔ  ۳۸

<div class="b" id="bn1"><div class="m1"><p>چه دل‌ها بردی ای ساقی به ساق فتنه‌انگیزت</p></div>
<div class="m2"><p>دریغا بوسه چندی بر زنخدان دلاویزت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خدنگ غمزه از هر سو نهان انداختن تا کی</p></div>
<div class="m2"><p>سپر انداخت عقل از دست ناوک‌های خونریزت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>برآمیزی و بگریزی و بنمایی و بربایی</p></div>
<div class="m2"><p>فغان از قهر لطف اندود و زهر شکرآمیزت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>لب شیرینت ار شیرین بدیدی در سخن گفتن</p></div>
<div class="m2"><p>بر او شکرانه بودی گر بدادی ملک پرویزت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جهان از فتنه و آشوب یک چندی برآسودی</p></div>
<div class="m2"><p>اگر نه روی شهرآشوب و چشم فتنه‌انگیزت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دگر رغبت کجا ماند کسی را سوی هشیاری</p></div>
<div class="m2"><p>چو بیند دست در آغوش مستان سحرخیزت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دمادم درکش ای سعدی شراب صرف و دم درکش</p></div>
<div class="m2"><p>که با مستان مجلس درنگیرد زهد و پرهیزت</p></div></div>