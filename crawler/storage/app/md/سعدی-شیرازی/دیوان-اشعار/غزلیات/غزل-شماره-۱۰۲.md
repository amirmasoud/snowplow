---
title: >-
    غزل شمارهٔ  ۱۰۲
---
# غزل شمارهٔ  ۱۰۲

<div class="b" id="bn1"><div class="m1"><p>تا دست‌ها کمر نکنی بر میان دوست</p></div>
<div class="m2"><p>بوسی به کام دل ندهی بر دهان دوست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دانی حیات کشته شمشیر عشق چیست</p></div>
<div class="m2"><p>سیبی گزیدن از رخ چون بوستان دوست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر ماجرای خسرو و شیرین قلم کشید</p></div>
<div class="m2"><p>شوری که در میان من است و میان دوست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خصمی که تیر کافرش اندر غزا نکشت</p></div>
<div class="m2"><p>خونش بریخت ابروی همچون کمان دوست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دل رفت و دیده خون شد و جان ضعیف ماند</p></div>
<div class="m2"><p>وآن هم برای آن که کنم جان فشان دوست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>روزی به پای مرکب تازی درافتمش</p></div>
<div class="m2"><p>گر کبر و ناز باز نپیچد عنان دوست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هیهات کام من که برآید در این طلب</p></div>
<div class="m2"><p>این بس که نام من برود بر زبان دوست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون جان سپردنیست به هر صورتی که هست</p></div>
<div class="m2"><p>در کوی عشق خوشتر و بر آستان دوست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>با خویشتن همی‌برم این شوق تا به خاک</p></div>
<div class="m2"><p>وز خاک سر برآرم و پرسم نشان دوست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>فریاد مردمان همه از دست دشمن است</p></div>
<div class="m2"><p>فریاد سعدی از دل نامهربان دوست</p></div></div>