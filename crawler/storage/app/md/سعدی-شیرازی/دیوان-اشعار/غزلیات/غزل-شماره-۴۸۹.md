---
title: >-
    غزل شمارهٔ  ۴۸۹
---
# غزل شمارهٔ  ۴۸۹

<div class="b" id="bn1"><div class="m1"><p>ای رخ چون آینه افروخته</p></div>
<div class="m2"><p>الحذر از آه من سوخته</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غیرت سلطان جمالت چو باز</p></div>
<div class="m2"><p>چشم من از هر که جهان دوخته</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عقل کهن بار جفا می‌کشد</p></div>
<div class="m2"><p>دم به دم از عشق نوآموخته</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>وه که به یک بار پراکنده شد</p></div>
<div class="m2"><p>آنچه به عمری بشد اندوخته</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>غم به تولای تو بخریده‌ام</p></div>
<div class="m2"><p>جان به تمنای تو بفروخته</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در دل سعدیست چراغ غمت</p></div>
<div class="m2"><p>مشعله‌ای تا ابد افروخته</p></div></div>