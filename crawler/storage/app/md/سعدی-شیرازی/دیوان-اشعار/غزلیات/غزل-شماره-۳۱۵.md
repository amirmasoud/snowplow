---
title: >-
    غزل شمارهٔ  ۳۱۵
---
# غزل شمارهٔ  ۳۱۵

<div class="b" id="bn1"><div class="m1"><p>پیوند روح می‌کند این باد مشک بیز</p></div>
<div class="m2"><p>هنگام نوبت سحرست ای ندیم خیز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شاهد بخوان و شمع بیفروز و می بنه</p></div>
<div class="m2"><p>عنبر بسای و عود بسوزان و گل بریز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ور دوست دست می‌دهدت هیچ گو مباش</p></div>
<div class="m2"><p>خوشتر بود عروس نکوروی بی جهاز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>امروز باید ار کرمی می‌کند سحاب</p></div>
<div class="m2"><p>فردا که تشنه مرده بود لای گو بخیز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>من در وفا و عهد چنان کند نیستم</p></div>
<div class="m2"><p>کز دامن تو دست بدارم به تیغ تیز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر تیغ می‌زنی سپر اینک وجود من</p></div>
<div class="m2"><p>عیار مدعی کند از دشمن احتراز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فردا که سر ز خاک برآرم اگر تو را</p></div>
<div class="m2"><p>بینم فراغتم بود از روز رستخیز</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا خود کجا رسد به قیامت نماز من</p></div>
<div class="m2"><p>من روی در تو و همه کس روی در حجاز</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سعدی به دام عشق تو در پای بند ماند</p></div>
<div class="m2"><p>قیدی نکرده‌ای که میسر شود گریز</p></div></div>