---
title: >-
    غزل شمارهٔ  ۳۷۵
---
# غزل شمارهٔ  ۳۷۵

<div class="b" id="bn1"><div class="m1"><p>چنان در قید مهرت پای بندم</p></div>
<div class="m2"><p>که گویی آهوی سر در کمندم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گهی بر درد بی درمان بگریم</p></div>
<div class="m2"><p>گهی بر حال بی سامان بخندم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مرا هوشی نماند از عشق و گوشی</p></div>
<div class="m2"><p>که پند هوشمندان کار بندم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مجال صبر تنگ آمد به یک بار</p></div>
<div class="m2"><p>حدیث عشق بر صحرا فکندم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نه مجنونم که دل بردارم از دوست</p></div>
<div class="m2"><p>مده گر عاقلی ای خواجه پندم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چنین صورت نبندد هیچ نقاش</p></div>
<div class="m2"><p>معاذالله من این صورت نبندم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چه جان‌ها در غمت فرسود و تن‌ها</p></div>
<div class="m2"><p>نه تنها من اسیر و مستمندم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تو هم بازآمدی ناچار و ناکام</p></div>
<div class="m2"><p>اگر بازآمدی بخت بلندم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر آوازم دهی من خفته در گور</p></div>
<div class="m2"><p>برآساید روان دردمندم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سری دارم فدای خاک پایت</p></div>
<div class="m2"><p>گر آسایش رسانی ور گزندم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>و گر در رنج سعدی راحت توست</p></div>
<div class="m2"><p>من این بیداد بر خود می‌پسندم</p></div></div>