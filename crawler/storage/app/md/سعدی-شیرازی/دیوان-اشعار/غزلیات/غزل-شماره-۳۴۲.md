---
title: >-
    غزل شمارهٔ  ۳۴۲
---
# غزل شمارهٔ  ۳۴۲

<div class="b" id="bn1"><div class="m1"><p>یار بیگانه نگیرد هر که دارد یار خویش</p></div>
<div class="m2"><p>ای که دستی چرب داری پیشتر دیوار خویش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خدمتت را هر که فرمایی کمر بندد به طوع</p></div>
<div class="m2"><p>لیکن آن بهتر که فرمایی به خدمتگار خویش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من هم اول روز گفتم جان فدای روی تو</p></div>
<div class="m2"><p>شرط مردی نیست برگردیدن از گفتار خویش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>درد عشق از هر که می‌پرسم جوابم می‌دهد</p></div>
<div class="m2"><p>از که می‌پرسی که من خود عاجزم در کار خویش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>صبر چون پروانه باید کردنت بر داغ عشق</p></div>
<div class="m2"><p>ای که صحبت با یکی داری نه در مقدار خویش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یا چو دیدارم نمودی دل نبایستی شکست</p></div>
<div class="m2"><p>یا نبایستی نمود اول مرا دیدار خویش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>حد زیبایی ندارند این خداوندان حسن</p></div>
<div class="m2"><p>ای دریغا گر بخوردندی غم غمخوار خویش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عقل را پنداشتم در عشق تدبیری بود</p></div>
<div class="m2"><p>من نخواهم کرد دیگر تکیه بر پندار خویش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هر که خواهد در حق ما هر چه خواهد گو بگوی</p></div>
<div class="m2"><p>ما نمی‌داریم دست از دامن دلدار خویش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>روز رستاخیز کان جا کس نپردازد به کس</p></div>
<div class="m2"><p>من نپردازم به هیچ از گفت و گوی یار خویش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سعدیا در کوی عشق از پارسایی دم مزن</p></div>
<div class="m2"><p>هر متاعی را خریداریست در بازار خویش</p></div></div>