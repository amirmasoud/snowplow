---
title: >-
    غزل شمارهٔ  ۳۲۸
---
# غزل شمارهٔ  ۳۲۸

<div class="b" id="bn1"><div class="m1"><p>رها نمی‌کند ایام در کنار منش</p></div>
<div class="m2"><p>که داد خود بستانم به بوسه از دهنش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همان کمند بگیرم که صید خاطر خلق</p></div>
<div class="m2"><p>بدان همی‌کند و درکشم به خویشتنش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ولیک دست نیارم زدن در آن سر زلف</p></div>
<div class="m2"><p>که مبلغی دل خلق است زیر هر شکنش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>غلام قامت آن لعبتم که بر قد او</p></div>
<div class="m2"><p>بریده‌اند لطافت چو جامه بر بدنش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز رنگ و بوی تو ای سروقد سیم اندام</p></div>
<div class="m2"><p>برفت رونق نسرین باغ و نسترنش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یکی به حکم نظر پای در گلستان نه</p></div>
<div class="m2"><p>که پایمال کنی ارغوان و یاسمنش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خوشا تفرج نوروز خاصه در شیراز</p></div>
<div class="m2"><p>که برکند دل مرد مسافر از وطنش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عزیز مصر چمن شد جمال یوسف گل</p></div>
<div class="m2"><p>صبا به شهر درآورد بوی پیرهنش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شگفت نیست گر از غیرت تو بر گلزار</p></div>
<div class="m2"><p>بگرید ابر و بخندد شکوفه بر چمنش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در این روش که تویی گر به مرده برگذری</p></div>
<div class="m2"><p>عجب نباشد اگر نعره آید از کفنش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نماند فتنه در ایام شاه جز سعدی</p></div>
<div class="m2"><p>که بر جمال تو فتنه‌ست و خلق بر سخنش</p></div></div>