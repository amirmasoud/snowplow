---
title: >-
    غزل شمارهٔ  ۴۱۳
---
# غزل شمارهٔ  ۴۱۳

<div class="b" id="bn1"><div class="m1"><p>آن نه روی است که من وصف جمالش دانم</p></div>
<div class="m2"><p>این حدیث از دگری پرس که من حیرانم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همه بینند نه این صنع که من می‌بینم</p></div>
<div class="m2"><p>همه خوانند نه این نقش که من می‌خوانم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آن عجب نیست که سرگشته بود طالب دوست</p></div>
<div class="m2"><p>عجب این است که من واصل و سرگردانم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سرو در باغ نشانند و تو را بر سر و چشم</p></div>
<div class="m2"><p>گر اجازت دهی ای سرو روان بنشانم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عشق من بر گل رخسار تو امروزی نیست</p></div>
<div class="m2"><p>دیر سال است که من بلبل این بستانم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به سرت کز سر پیمان محبت نروم</p></div>
<div class="m2"><p>گر بفرمایی رفتن به سر پیکانم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>باش تا جان برود در طلب جانانم</p></div>
<div class="m2"><p>که به کاری به از این بازنیاید جانم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هر نصیحت که کنی بشنوم ای یار عزیز</p></div>
<div class="m2"><p>صبرم از دوست مفرمای که من نتوانم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>عجب از طبع هوسناک منت می‌آید</p></div>
<div class="m2"><p>من خود از مردم بی طبع عجب می‌مانم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گفته بودی که بود در همه عالم سعدی</p></div>
<div class="m2"><p>من به خود هیچ نیم هر چه تو گویی آنم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گر به تشریف قبولم بنوازی ملکم</p></div>
<div class="m2"><p>ور به تازانه قهرم بزنی شیطانم</p></div></div>