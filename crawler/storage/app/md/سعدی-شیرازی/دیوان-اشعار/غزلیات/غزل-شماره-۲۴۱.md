---
title: >-
    غزل شمارهٔ  ۲۴۱
---
# غزل شمارهٔ  ۲۴۱

<div class="b" id="bn1"><div class="m1"><p>میل بین کان سروبالا می‌کند</p></div>
<div class="m2"><p>سرو بین کاهنگ صحرا می‌کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>میل از این خوشتر نداند کرد سرو</p></div>
<div class="m2"><p>ناخوش آن میلست کز ما می‌کند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حاجت صحرا نبود آیینه هست</p></div>
<div class="m2"><p>گر نگارستان تماشا می‌کند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>غافلست از صورت زیبای او</p></div>
<div class="m2"><p>آن که صورت‌های دیبا می‌کند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>من هم اول روز دانستم که عشق</p></div>
<div class="m2"><p>خون مباح و خانه یغما می‌کند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>صبر هم سودی ندارد کآب چشم</p></div>
<div class="m2"><p>راز پنهان آشکارا می‌کند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر مراد ما نباشد گو مباش</p></div>
<div class="m2"><p>چون مراد اوست هل تا می‌کند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>یار زیبا گر بریزد خون یار</p></div>
<div class="m2"><p>زشت نتوان گفت زیبا می‌کند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سعدیا بعد از تحمل چاره نیست</p></div>
<div class="m2"><p>هر ستم کان دوست با ما می‌کند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تا مگس را جان شیرین در تنست</p></div>
<div class="m2"><p>گرد آن گردد که حلوا می‌کند</p></div></div>