---
title: >-
    غزل شمارهٔ  ۱۳۵
---
# غزل شمارهٔ  ۱۳۵

<div class="b" id="bn1"><div class="m1"><p>آن را که میسر نشود صبر و قناعت</p></div>
<div class="m2"><p>باید که ببندد کمر خدمت و طاعت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون دوست گرفتی چه غم از دشمن خونخوار؟</p></div>
<div class="m2"><p>گو بوق ملامت بزن و کوس شناعت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر خود همه بیداد کند هیچ مگویید</p></div>
<div class="m2"><p>تعذیب دلارام به از ذل شفاعت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از هر چه تو گویی به قناعت بشکیبم</p></div>
<div class="m2"><p>امکان شکیب از تو محالست و قناعت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر نسخه روی تو به بازار برآرند</p></div>
<div class="m2"><p>نقاش ببندد در دکان صناعت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جان بر کف دست آمده تا روی تو بیند</p></div>
<div class="m2"><p>خود شرم نمی‌آیدش از ننگ بضاعت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دریاب دمی صحبت یاری که دگربار</p></div>
<div class="m2"><p>چون رفت نیاید به کمند آن دم و ساعت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>انصاف نباشد که من خسته رنجور</p></div>
<div class="m2"><p>پروانه او باشم و او شمع جماعت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>لیکن چه توان کرد که قوت نتوان کرد</p></div>
<div class="m2"><p>با گردش ایام به بازوی شجاعت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دل در هوست خون شد و جان در طلبت سوخت</p></div>
<div class="m2"><p>با این همه سعدی خجل از ننگ بضاعت</p></div></div>