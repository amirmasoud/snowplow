---
title: >-
    غزل شمارهٔ  ۲۵۷
---
# غزل شمارهٔ  ۲۵۷

<div class="b" id="bn1"><div class="m1"><p>از دست دوست هر چه ستانی شکر بود</p></div>
<div class="m2"><p>وز دست غیر دوست تبرزد تبر بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دشمن گر آستین گل افشاندت به روی</p></div>
<div class="m2"><p>از تیر چرخ و سنگ فلاخن بتر بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر خاک پای دوست خداوند شوق را</p></div>
<div class="m2"><p>در دیدگان کشند جلای بصر بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شرط وفاست آن که چو شمشیر برکشد</p></div>
<div class="m2"><p>یار عزیز جان عزیزش سپر بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یا رب هلاک من مکن الا به دست دوست</p></div>
<div class="m2"><p>تا وقت جان سپردنم اندر نظر بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر جان دهی و گر سر بیچارگی نهی</p></div>
<div class="m2"><p>در پای دوست هر چه کنی مختصر بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ما سر نهاده‌ایم تو دانی و تیغ و تاج</p></div>
<div class="m2"><p>تیغی که ماهروی زند تاج سر بود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مشتاق را که سر برود در وفای یار</p></div>
<div class="m2"><p>آن روز روز دولت و روز ظفر بود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ما ترک جان از اول این کار گفته‌ایم</p></div>
<div class="m2"><p>آن را که جان عزیز بود در خطر بود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آن کز بلا بترسد و از قتل غم خورد</p></div>
<div class="m2"><p>او عاقلست و شیوه مجنون دگر بود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>با نیم پختگان نتوان گفت سوز عشق</p></div>
<div class="m2"><p>خام از عذاب سوختگان بی‌خبر بود</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>جانا دل شکسته سعدی نگاه دار</p></div>
<div class="m2"><p>دانی که آه سوختگان را اثر بود</p></div></div>