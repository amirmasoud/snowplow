---
title: >-
    غزل شمارهٔ  ۴۸۶
---
# غزل شمارهٔ  ۴۸۶

<div class="b" id="bn1"><div class="m1"><p>آن سرو ناز بین که چه خوش می‌رود به راه</p></div>
<div class="m2"><p>وآن چشم آهوانه که چون می‌کند نگاه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو سرو دیده‌ای که کمر بست بر میان</p></div>
<div class="m2"><p>یا ماه چارده که به سر برنهد کلاه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گل با وجود او چو گیاه است پیش گل</p></div>
<div class="m2"><p>مه پیش روی او چو ستاره‌ست پیش ماه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سلطان صفت همی‌رود و صد هزار دل</p></div>
<div class="m2"><p>با او چنان که در پی سلطان رود سپاه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گویند از او حذر کن و راه گریز گیر</p></div>
<div class="m2"><p>گویم کجا روم که ندانم گریزگاه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اول نظر که چاه زنخدان بدیدمش</p></div>
<div class="m2"><p>گویی در اوفتاد دل از دست من به چاه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دل خود دریغ نیست که از دست من برفت</p></div>
<div class="m2"><p>جان عزیز بر کف دست است گو بخواه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ای هر دو دیده پای که بر خاک می‌نهی</p></div>
<div class="m2"><p>آخر نه بر دو دیده من به که خاک راه</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>حیف است از آن دهن که تو داری جواب تلخ</p></div>
<div class="m2"><p>وآن سینه سفید که دارد دل سیاه</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بیچارگان بر آتش مهرت بسوختند</p></div>
<div class="m2"><p>آه از تو سنگدل که چه نامهربانی آه</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شهری به گفت و گوی تو در تنگنای شوق</p></div>
<div class="m2"><p>شب روز می‌کنند و تو در خواب صبحگاه</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گفتم بنالم از تو به یاران و دوستان</p></div>
<div class="m2"><p>باشد که دست ظلم بداری ز بی‌گناه</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بازم حفاظ دامن همت گرفت و گفت</p></div>
<div class="m2"><p>از دوست جز به دوست مبر سعدیا پناه</p></div></div>