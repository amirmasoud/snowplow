---
title: >-
    غزل شمارهٔ  ۱۳۳
---
# غزل شمارهٔ  ۱۳۳

<div class="b" id="bn1"><div class="m1"><p>خیال روی توام دوش در نظر می‌گشت</p></div>
<div class="m2"><p>وجود خسته‌ام از عشق بی‌خبر می‌گشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همای شخص من از آشیان شادی دور</p></div>
<div class="m2"><p>چو مرغ حلق بریده به خاک بر می‌گشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل ضعیفم از آن کرد آه خون آلود</p></div>
<div class="m2"><p>که در میانه خونابه جگر می‌گشت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چنان غریو برآورده بودم از غم عشق</p></div>
<div class="m2"><p>که بر موافقتم زهره نوحه گر می‌گشت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز آب دیده من فرش خاک تر می‌شد</p></div>
<div class="m2"><p>ز بانگ ناله من گوش چرخ کر می‌گشت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>قیاس کن که دلم را چه تیر عشق رسید</p></div>
<div class="m2"><p>که پیش ناوک هجر تو جان سپر می‌گشت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>صبور باش و بدین روز دل بنه سعدی</p></div>
<div class="m2"><p>که روز اولم این روز در نظر می‌گشت</p></div></div>