---
title: >-
    غزل شمارهٔ  ۴۸۵
---
# غزل شمارهٔ  ۴۸۵

<div class="b" id="bn1"><div class="m1"><p>ای طراوت برده از فردوس اعلی روی تو</p></div>
<div class="m2"><p>نادر است اندر نگارستان دنیی روی تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دختران مصر را کاسد شود بازار حسن</p></div>
<div class="m2"><p>گر چو یوسف پرده بردارد به دعوی روی تو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر چه از انگشت مانی بر نیاید چون تو نقش</p></div>
<div class="m2"><p>هر دم انگشتی نهد بر نقش مانی روی تو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از گل و ماه و پری در چشم من زیباتری</p></div>
<div class="m2"><p>گل ز من دل برد یا مه یا پری نی روی تو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ماه و پروین از خجالت رخ فرو پوشد اگر</p></div>
<div class="m2"><p>آفتاب آسا کند در شب تجلی روی تو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مردم چشمش بدرد پرده اعمی ز شوق</p></div>
<div class="m2"><p>گر درآید در خیال چشم اعمی روی تو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>روی هر صاحب جمالی را به مه خواندن خطاست</p></div>
<div class="m2"><p>گر رخی را ماه باید خواند باری روی تو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>رسم تقوی می‌نهد در عشقبازی رای من</p></div>
<div class="m2"><p>کوس غارت می‌زند در ملک تقوی روی تو</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون به هر وجهی بخواهد رفت جان از دست ما</p></div>
<div class="m2"><p>خوبتر وجهی بباید جستن اولی روی تو</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چشمم از زاری چو فرهاد است و شیرین لعل تو</p></div>
<div class="m2"><p>عقلم از شورش چو مجنون است و لیلی روی تو</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ملک زیبایی مسلم گشت فرمان تو را</p></div>
<div class="m2"><p>تا چنین خطی مزور کرد انشی روی تو</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>داشتند اصحاب خلوت حرف‌ها بر من ز بد</p></div>
<div class="m2"><p>تا تجلی کرد در بازار تقوی روی تو</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>خرده بر سعدی مگیر ای جان که کاری خرد نیست</p></div>
<div class="m2"><p>سوختن در عشق وانگه ساختن بی روی تو</p></div></div>