---
title: >-
    غزل شمارهٔ  ۲۱
---
# غزل شمارهٔ  ۲۱

<div class="b" id="bn1"><div class="m1"><p>تفاوتی نکند قدر پادشایی را</p></div>
<div class="m2"><p>که التفات کند کمترین گدایی را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به جان دوست که دشمن بدین رضا ندهد</p></div>
<div class="m2"><p>که در به روی ببندند آشنایی را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مگر حلال نباشد که بندگان ملوک</p></div>
<div class="m2"><p>ز خیل خانه برانند بی‌نوایی را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>و گر تو جور کنی رای ما دگر نشود</p></div>
<div class="m2"><p>هزار شکر بگوییم هر جفایی را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همه سلامت نفس آرزو کند مردم</p></div>
<div class="m2"><p>خلاف من که به جان می‌خرم بلایی را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حدیث عشق نداند کسی که در همه عمر</p></div>
<div class="m2"><p>به سر نکوفته باشد در سرایی را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خیال در همه عالم برفت و بازآمد</p></div>
<div class="m2"><p>که از حضور تو خوشتر ندید جایی را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سری به صحبت بیچارگان فرود آور</p></div>
<div class="m2"><p>همین قدر که ببوسند خاک پایی را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>قبای خوشتر از این در بدن تواند بود</p></div>
<div class="m2"><p>بدن نیفتد از این خوبتر قبایی را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>اگر تو روی نپوشی بدین لطافت و حسن</p></div>
<div class="m2"><p>دگر نبینی در پارس پارسایی را</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>منه به جان تو بار فراق بر دل ریش</p></div>
<div class="m2"><p>که پشه‌ای نبرد سنگ آسیایی را</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دگر به دست نیاید چو من وفاداری</p></div>
<div class="m2"><p>که ترک می‌ندهم عهد بی‌وفایی را</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>دعای سعدی اگر بشنوی زیان نکنی</p></div>
<div class="m2"><p>که یحتمل که اجابت بود دعایی را</p></div></div>