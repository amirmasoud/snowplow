---
title: >-
    غزل شمارهٔ  ۲۱۲
---
# غزل شمارهٔ  ۲۱۲

<div class="b" id="bn1"><div class="m1"><p>هر که شیرینی فروشد مشتری بر وی بجوشد</p></div>
<div class="m2"><p>یا مگس را پر ببندد یا عسل را سر بپوشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همچنان عاشق نباشد ور بود صادق نباشد</p></div>
<div class="m2"><p>هر که درمان می‌پذیرد یا نصیحت می‌نیوشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر مطیع خدمتت را کفر فرمایی بگوید</p></div>
<div class="m2"><p>ور حریف مجلست را زهر فرمایی بنوشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شمع پیشت روشنایی نزد آتش می‌نماید</p></div>
<div class="m2"><p>گل به دستت خوبرویی پیش یوسف می‌فروشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سود بازرگان دریا بی‌خطر ممکن نگردد</p></div>
<div class="m2"><p>هر که مقصودش تو باشی تا نفس دارد بکوشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>برگ چشمم می‌نخوشد در زمستان فراقت</p></div>
<div class="m2"><p>وین عجب کاندر زمستان برگ‌های تر بخوشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر که معشوقی ندارد عمر ضایع می‌گذارد</p></div>
<div class="m2"><p>همچنان ناپخته باشد هر که بر آتش نجوشد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا غمی پنهان نباشد رقتی پیدا نگردد</p></div>
<div class="m2"><p>هم گلی دیدست سعدی تا چو بلبل می‌خروشد</p></div></div>