---
title: >-
    غزل شمارهٔ  ۱۲
---
# غزل شمارهٔ  ۱۲

<div class="b" id="bn1"><div class="m1"><p>دوست می‌دارم من این نالیدن دلسوز را</p></div>
<div class="m2"><p>تا به هر نوعی که باشد بگذرانم روز را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شب همه شب انتظار صبح‌رویی می‌رود</p></div>
<div class="m2"><p>کان صباحت نیست این صبح جهان‌افروز را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>وه که گر من بازبینم چهر مهرافزای او</p></div>
<div class="m2"><p>تا قیامت شکر گویم طالع پیروز را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر من از سنگ ملامت روی برپیچم زنم</p></div>
<div class="m2"><p>جان سپر کردند مردانْ ناوکِ دلدوز را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کامجویان را ز ناکامی چشیدن چاره نیست</p></div>
<div class="m2"><p>بر زمستان صبر باید طالب نوروز را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عاقلان خوشه‌چین از سر لیلی غافلند</p></div>
<div class="m2"><p>این کرامت نیست جز مجنون خرمن‌سوز را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عاشقان دین و دنیاباز را خاصیتیست</p></div>
<div class="m2"><p>کان نباشد زاهدان مال و جاه‌اندوز را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دیگری را در کمند آور که ما خود بنده‌ایم</p></div>
<div class="m2"><p>ریسمان در پای حاجت نیست دست‌آموز را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سعدیا دی رفت و فردا همچنان موجود نیست</p></div>
<div class="m2"><p>در میان این و آن فرصت شمار امروز را</p></div></div>