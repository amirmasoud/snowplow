---
title: >-
    غزل شمارهٔ  ۲۰۴
---
# غزل شمارهٔ  ۲۰۴

<div class="b" id="bn1"><div class="m1"><p>گر گویمت که سروی سرو این چنین نباشد</p></div>
<div class="m2"><p>ور گویمت که ماهی مه بر زمین نباشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر در جهان بگردی و آفاق درنوردی</p></div>
<div class="m2"><p>صورت بدین شگرفی در کفر و دین نباشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>لعل است یا لبانت، قند است یا دهانت</p></div>
<div class="m2"><p>تا در برت نگیرم، نیکم یقین نباشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>صورت کنند زیبا بر پرنیان و دیبا</p></div>
<div class="m2"><p>لیکن بر ابروانش سحر مبین نباشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زنبور اگر میانش باشد بدین لطیفی</p></div>
<div class="m2"><p>حقا که در دهانش این انگبین نباشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر هر که در جهان را شاید که خون بریزی</p></div>
<div class="m2"><p>با یار مهربانت باید که کین نباشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر جان نازنینش در پای ریزی ای دل</p></div>
<div class="m2"><p>در کار نازنینان جان نازنین نباشد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ور زان که دیگری را بر ما همی‌گزیند</p></div>
<div class="m2"><p>گو برگزین که ما را بر تو گزین نباشد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>عشقش حرام بادا بر یار سروبالا</p></div>
<div class="m2"><p>تردامنی که جانش در آستین نباشد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سعدی به هیچ علت روی از تو برنپیچد</p></div>
<div class="m2"><p>الا گرش برانی علت جز این نباشد</p></div></div>