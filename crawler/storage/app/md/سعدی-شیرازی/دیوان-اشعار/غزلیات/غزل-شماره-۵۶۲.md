---
title: >-
    غزل شمارهٔ  ۵۶۲
---
# غزل شمارهٔ  ۵۶۲

<div class="b" id="bn1"><div class="m1"><p>دو چشم مست تو برداشت رسم هشیاری</p></div>
<div class="m2"><p>و گر نه فتنه ندیدی به خواب بیداری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زمانه با تو چه دعوی کند به بدمهری</p></div>
<div class="m2"><p>سپهر با تو چه پهلو زند به غداری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>معلمت همه شوخی و دلبری آموخت</p></div>
<div class="m2"><p>به دوستیت وصیت نکرد و دلداری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو گل لطیف ولیکن حریف اوباشی</p></div>
<div class="m2"><p>چو زر عزیز ولیکن به دست اغیاری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به صید کردن دل‌ها چه شوخ و شیرینی</p></div>
<div class="m2"><p>به خیره کشتن تن‌ها چه جلد و عیاری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دلم ربودی و جان می‌دهم به طیبت نفس</p></div>
<div class="m2"><p>که هست راحت درویش در سبکباری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر افتدت گذری بر وجود کشته عشق</p></div>
<div class="m2"><p>سخن بگوی که در جسم مرده جان آری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گرت ارادت باشد به شورش دل خلق</p></div>
<div class="m2"><p>بشور زلف که در هر خمی دلی داری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو بت به کعبه نگونسار بر زمین افتد</p></div>
<div class="m2"><p>به پیش قبله رویت بتان فرخاری</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دهان پر شکرت را مثل به نقطه زنند</p></div>
<div class="m2"><p>که روی چون قمرت شمسه‌ایست پرگاری</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به گرد نقطه سرخت عذار سبز چنان</p></div>
<div class="m2"><p>که نیم دایره‌ای برکشند زنگاری</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هزار نامه پیاپی نویسمت که جواب</p></div>
<div class="m2"><p>اگر چه تلخ دهی در سخن شکرباری</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ز خلق گوی لطافت تو برده‌ای امروز</p></div>
<div class="m2"><p>به خوبرویی و سعدی به خوب گفتاری</p></div></div>