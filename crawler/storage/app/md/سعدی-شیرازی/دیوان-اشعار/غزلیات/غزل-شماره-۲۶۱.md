---
title: >-
    غزل شمارهٔ  ۲۶۱
---
# غزل شمارهٔ  ۲۶۱

<div class="b" id="bn1"><div class="m1"><p>یا رب شب دوشین چه مبارک سحری بود</p></div>
<div class="m2"><p>کاو را به سر کشته هجران گذری بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن دوست که ما را به ارادت نظری هست</p></div>
<div class="m2"><p>با او مگر او را به عنایت نظری بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من بعد حکایت نکنم تلخی هجران</p></div>
<div class="m2"><p>کان میوه که از صبر برآمد شکری بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رویی نتوان گفت که حسنش به چه ماند</p></div>
<div class="m2"><p>گویی که در آن نیم شب از روز دری بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گویم قمری بود کس از من نپسندد</p></div>
<div class="m2"><p>باغی که به هر شاخ درختش قمری بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آن دم که خبر بودم از او تا تو نگویی</p></div>
<div class="m2"><p>کز خویشتن و هر که جهانم خبری بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در عالم وصفش به جهانی برسیدم</p></div>
<div class="m2"><p>کاندر نظرم هر دو جهان مختصری بود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>من بودم و او نی قلم اندر سر من کش</p></div>
<div class="m2"><p>با او نتوان گفت وجود دگری بود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>با غمزه خوبان که چو شمشیر کشیده‌ست</p></div>
<div class="m2"><p>در صبر بدیدم که نه محکم سپری بود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سعدی نتوانی که دگر دیده بدوزی</p></div>
<div class="m2"><p>کان دل بربودند که صبرش قدری بود</p></div></div>