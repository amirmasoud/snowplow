---
title: >-
    غزل شمارهٔ  ۴۰۸
---
# غزل شمارهٔ  ۴۰۸

<div class="b" id="bn1"><div class="m1"><p>امروز مبارک است فالم</p></div>
<div class="m2"><p>کافتاد نظر بر آن جمالم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>الحمد خدای آسمان را</p></div>
<div class="m2"><p>کاختر به درآمد از وبالم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خواب است مگر که می‌نماید</p></div>
<div class="m2"><p>یا عشوه همی‌دهد خیالم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کاین بخت نبود هیچ روزم</p></div>
<div class="m2"><p>وین گل نشکفت هیچ سالم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>امروز بدیدم آن چه دل خواست</p></div>
<div class="m2"><p>دید آن چه نخواست بدسگالم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اکنون که تو روی باز کردی</p></div>
<div class="m2"><p>رو باز به خیر کرد حالم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دیگر چه توقع است از ایام</p></div>
<div class="m2"><p>چون بدر تمام شد هلالم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بازآی کز اشتیاق رویت</p></div>
<div class="m2"><p>بگرفت ز خویشتن ملالم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آزرده‌ام از فراق چونانک</p></div>
<div class="m2"><p>دل باز نمی‌دهد وصالم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>وز غایت تشنگی که بردم</p></div>
<div class="m2"><p>در حلق نمی‌رود زلالم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بیچاره به رویت آمدم باز</p></div>
<div class="m2"><p>چون چاره نماند و احتیالم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>از جور تو هم در تو گیرم</p></div>
<div class="m2"><p>وز دست تو هم بر تو نالم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چون دوست موافق است سعدی</p></div>
<div class="m2"><p>سهل است جفای خلق عالم</p></div></div>