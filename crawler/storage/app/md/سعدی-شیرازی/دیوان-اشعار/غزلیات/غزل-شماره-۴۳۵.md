---
title: >-
    غزل شمارهٔ  ۴۳۵
---
# غزل شمارهٔ  ۴۳۵

<div class="b" id="bn1"><div class="m1"><p>ای سروبالای سهی کز صورت حال آگهی</p></div>
<div class="m2"><p>وز هر که در عالم بهی ما نیز هم بد نیستیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتی به رنگ من گلی هرگز نبیند بلبلی</p></div>
<div class="m2"><p>آری نکو گفتی ولی ما نیز هم بد نیستیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا چند گویی ما و بس کوته کن ای رعنا و بس</p></div>
<div class="m2"><p>نه خود تویی زیبا و بس ما نیز هم بد نیستیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای شاهد هر مجلسی و آرام جان هر کسی</p></div>
<div class="m2"><p>گر دوستان داری بسی ما نیز هم بد نیستیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفتی که چون من در زمی دیگر نباشد آدمی</p></div>
<div class="m2"><p>ای جان لطف و مردمی ما نیز هم بد نیستیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر گلشن خوش بو تویی ور بلبل خوشگو تویی</p></div>
<div class="m2"><p>ور در جهان نیکو تویی ما نیز هم بد نیستیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گویی چه شد کان سروبن با ما نمی‌گوید سخن</p></div>
<div class="m2"><p>گو بی‌وفایی پر مکن ما نیز هم بد نیستیم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر تو به حسن افسانه‌ای یا گوهر یک دانه‌ای</p></div>
<div class="m2"><p>از ما چرا بیگانه‌ای ما نیز هم بد نیستیم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ای در دل ما داغ تو تا کی فریب و لاغ تو</p></div>
<div class="m2"><p>گر به بود در باغ تو ما نیز هم بد نیستیم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>باری غرور از سر بنه و انصاف درد من بده</p></div>
<div class="m2"><p>ای باغ شفتالو و به ما نیز هم بد نیستیم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گفتم تو ما را دیده‌ای وز حال ما پرسیده‌ای</p></div>
<div class="m2"><p>پس چون ز ما رنجیده‌ای ما نیز هم بد نیستیم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گفتی به از من در چگل صورت نبندد آب و گل</p></div>
<div class="m2"><p>ای سست مهر سخت دل ما نیز هم بد نیستیم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>سعدی گر آن زیباقرین بگزید بر ما همنشین</p></div>
<div class="m2"><p>گو هر که خواهی برگزین ما نیز هم بد نیستیم</p></div></div>