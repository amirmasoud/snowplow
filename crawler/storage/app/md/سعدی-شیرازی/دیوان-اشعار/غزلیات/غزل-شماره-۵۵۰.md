---
title: >-
    غزل شمارهٔ  ۵۵۰
---
# غزل شمارهٔ  ۵۵۰

<div class="b" id="bn1"><div class="m1"><p>دیدم امروز بر زمین قمری</p></div>
<div class="m2"><p>همچو سروی روان به رهگذری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گوییا بر من از بهشت خدای</p></div>
<div class="m2"><p>باز کردند بامداد دری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من ندیدم به راستی همه عمر</p></div>
<div class="m2"><p>گر تو دیدی به سرو بر قمری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یا شنیدی که در وجود آمد</p></div>
<div class="m2"><p>آفتابی ز مادر و پدری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفتم از وی نظر بپوشانم</p></div>
<div class="m2"><p>تا نیفتم به دیده در خطری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چاره صبر است و احتمال فراق</p></div>
<div class="m2"><p>چون کفایت نمی‌کند نظری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>می‌خرامید و زیر لب می‌گفت</p></div>
<div class="m2"><p>عاقل از فتنه می‌کند حذری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سعدیا پیش تیر غمزه ما</p></div>
<div class="m2"><p>به ز تقوا ببایدت سپری</p></div></div>