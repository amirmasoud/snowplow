---
title: >-
    غزل شمارهٔ  ۴۷۱
---
# غزل شمارهٔ  ۴۷۱

<div class="b" id="bn1"><div class="m1"><p>وه که جدا نمی‌شود نقش تو از خیال من</p></div>
<div class="m2"><p>تا چه شود به عاقبت در طلب تو حال من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ناله زیر و زار من زارتر است هر زمان</p></div>
<div class="m2"><p>بس که به هجر می‌دهد عشق تو گوشمال من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نور ستارگان ستد روی چو آفتاب تو</p></div>
<div class="m2"><p>دست نمای خلق شد قامت چون هلال من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پرتو نور روی تو هر نفسی به هر کسی</p></div>
<div class="m2"><p>می‌رسد و نمی‌رسد نوبت اتصال من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خاطر تو به خون من رغبت اگر چنین کند</p></div>
<div class="m2"><p>هم به مراد دل رسد خاطر بدسگال من</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>برگذری و ننگری بازنگر که بگذرد</p></div>
<div class="m2"><p>فقر من و غنای تو جور تو و احتمال من</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چرخ شنید ناله‌ام گفت منال سعدیا</p></div>
<div class="m2"><p>کآه تو تیره می‌کند آینه جمال من</p></div></div>