---
title: >-
    غزل شمارهٔ  ۲۰۷
---
# غزل شمارهٔ  ۲۰۷

<div class="b" id="bn1"><div class="m1"><p>تو را خود یک زمان با ما سر صحرا نمی‌باشد</p></div>
<div class="m2"><p>چو شمست خاطر رفتن به جز تنها نمی‌باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دو چشم از ناز در پیشت فراغ از حال درویشت</p></div>
<div class="m2"><p>مگر کز خوبی خویشت نگه در ما نمی‌باشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ملک یا چشمه نوری پری یا لعبت حوری</p></div>
<div class="m2"><p>که بر گلبن گل سوری چنین زیبا نمی‌باشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پری رویی و مه پیکر سمن بویی و سیمین بر</p></div>
<div class="m2"><p>عجب کز حسن رویت در جهان غوغا نمی‌باشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو نتوان ساخت بی رویت بباید ساخت با خویت</p></div>
<div class="m2"><p>که ما را از سر کویت سر دروا نمی‌باشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مرو هر سوی و هر جاگه که مسکینان نیند آگه</p></div>
<div class="m2"><p>نمی‌بیند کست ناگه که او شیدا نمی‌باشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جهانی در پیت مفتون به جای آب گریان خون</p></div>
<div class="m2"><p>عجب می‌دارم از هامون که چون دریا نمی‌باشد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>همه شب می‌پزم سودا به بوی وعده فردا</p></div>
<div class="m2"><p>شب سودای سعدی را مگر فردا نمی‌باشد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چرا بر خاک این منزل نگریم تا بگیرد گل</p></div>
<div class="m2"><p>ولیکن با تو آهن دل دمم گیرا نمی‌باشد</p></div></div>