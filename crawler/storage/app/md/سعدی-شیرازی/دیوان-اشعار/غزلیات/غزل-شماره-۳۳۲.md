---
title: >-
    غزل شمارهٔ  ۳۳۲
---
# غزل شمارهٔ  ۳۳۲

<div class="b" id="bn1"><div class="m1"><p>هر که سودای تو دارد چه غم از هر که جهانش</p></div>
<div class="m2"><p>نگران تو چه اندیشه و بیم از دگرانش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن پی مهر تو گیرد که نگیرد پی خویشش</p></div>
<div class="m2"><p>وان سر وصل تو دارد که ندارد غم جانش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر که از یار تحمل نکند یار مگویش</p></div>
<div class="m2"><p>وان که در عشق ملامت نکشد مرد مخوانش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون دل از دست به درشد مثل کره توسن</p></div>
<div class="m2"><p>نتوان باز گرفتن به همه شهر عنانش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به جفایی و قفایی نرود عاشق صادق</p></div>
<div class="m2"><p>مژه بر هم نزند گر بزنی تیر و سنانش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خفته خاک لحد را که تو ناگه به سر آیی</p></div>
<div class="m2"><p>عجب ار باز نیاید به تن مرده روانش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شرم دارد چمن از قامت زیبای بلندت</p></div>
<div class="m2"><p>که همه عمر نبوده‌ست چنین سرو روانش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گفتم از ورطه عشقت به صبوری به درآیم</p></div>
<div class="m2"><p>باز می‌بینم و دریا نه پدید است کرانش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>عهد ما با تو نه عهدی که تغیر بپذیرد</p></div>
<div class="m2"><p>بوستانیست که هرگز نزند باد خزانش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چه گنه کردم و دیدی که تعلق ببریدی</p></div>
<div class="m2"><p>بنده بی جرم و خطایی نه صواب است مرانش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نرسد ناله سعدی به کسی در همه عالم</p></div>
<div class="m2"><p>که نه تصدیق کند کز سر دردیست فغانش</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گر فلاطون به حکیمی مرض عشق بپوشد</p></div>
<div class="m2"><p>عاقبت پرده برافتد ز سر راز نهانش</p></div></div>