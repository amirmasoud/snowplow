---
title: >-
    غزل شمارهٔ  ۵۴
---
# غزل شمارهٔ  ۵۴

<div class="b" id="bn1"><div class="m1"><p>سرو چمن پیش اعتدال تو پست است</p></div>
<div class="m2"><p>روی تو بازار آفتاب شکسته‌ست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شمع فلک با هزار مشعل انجم</p></div>
<div class="m2"><p>پیش وجودت چراغ بازنشسته‌ست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>توبه کند مردم از گناه به شعبان</p></div>
<div class="m2"><p>در رمضان نیز چشم‌های تو مست است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>این همه زورآوری و مردی و شیری</p></div>
<div class="m2"><p>مرد ندانم که از کمند تو جسته‌ست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>این یکی از دوستان به تیغ تو کشته‌ست</p></div>
<div class="m2"><p>وان دگر از عاشقان به تیر تو خسته‌ست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دیده به دل می‌برد حکایت مجنون</p></div>
<div class="m2"><p>دیده ندارد که دل به مهر نبسته‌ست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دست طلب داشتن ز دامن معشوق</p></div>
<div class="m2"><p>پیش کسی گو کش اختیار به دست است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>با چو تو روحانیی تعلق خاطر</p></div>
<div class="m2"><p>هر که ندارد دواب نفس‌پرست است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>منکر سعدی که ذوق عشق ندارد</p></div>
<div class="m2"><p>نیشکرش در دهان تلخ کبست است</p></div></div>