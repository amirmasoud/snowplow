---
title: >-
    غزل شمارهٔ  ۵۸۵
---
# غزل شمارهٔ  ۵۸۵

<div class="b" id="bn1"><div class="m1"><p>اگر تو پرده بر این زلف و رخ نمی‌پوشی</p></div>
<div class="m2"><p>به هتک پرده صاحب دلان همی‌کوشی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چنین قیامت و قامت ندیده‌ام همه عمر</p></div>
<div class="m2"><p>تو سرو یا بدنی شمس یا بناگوشی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>غلام حلقه سیمین گوشوار توام</p></div>
<div class="m2"><p>که پادشاه غلامان حلقه در گوشی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به کنج خلوت پاکان و پارسایان آی</p></div>
<div class="m2"><p>نظاره کن که چه مستی کنند و مدهوشی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به روزگار عزیزان که یاد می‌کنمت</p></div>
<div class="m2"><p>علی الدوام نه یادی پس از فراموشی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چنان موافق طبع منی و در دل من</p></div>
<div class="m2"><p>نشسته‌ای که گمان می‌برم در آغوشی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چه نیکبخت کسانی که با تو هم سخنند</p></div>
<div class="m2"><p>مرا نه زهره گفت و نه صبر خاموشی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>رقیب نامتناسب چه اهل صحبت توست</p></div>
<div class="m2"><p>که طبع او همه نیش و تو سر به سر نوشی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به تربیت به چمن گفتم ای نسیم صبا</p></div>
<div class="m2"><p>بگوی تا ندهد گل به خار چاووشی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تو سوز سینه مستان ندیدی ای هشیار</p></div>
<div class="m2"><p>چو آتشیت نباشد چگونه برجوشی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تو را که دل نبود عاشقی چه دانی چیست</p></div>
<div class="m2"><p>تو را که سمع نباشد سماع ننیوشی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>وفای یار به دنیا و دین مده سعدی</p></div>
<div class="m2"><p>دریغ باشد یوسف به هر چه بفروشی</p></div></div>