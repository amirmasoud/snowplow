---
title: >-
    غزل شمارهٔ  ۳۱۸
---
# غزل شمارهٔ  ۳۱۸

<div class="b" id="bn1"><div class="m1"><p>امشب مگر به وقت نمی‌خواند این خروس</p></div>
<div class="m2"><p>عشاق بس نکرده هنوز از کنار و بوس</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پستان یار در خم گیسوی تابدار</p></div>
<div class="m2"><p>چون گوی عاج در خم چوگان آبنوس</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یک شب که دوست فتنه خفتست زینهار</p></div>
<div class="m2"><p>بیدار باش تا نرود عمر بر فسوس</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا نشنوی ز مسجد آدینه بانگ صبح</p></div>
<div class="m2"><p>یا از در سرای اتابک غریو کوس</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>لب بر لبی چو چشم خروس ابلهی بود</p></div>
<div class="m2"><p>برداشتن به گفته بیهوده خروس</p></div></div>