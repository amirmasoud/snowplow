---
title: >-
    غزل شمارهٔ  ۷۳
---
# غزل شمارهٔ  ۷۳

<div class="b" id="bn1"><div class="m1"><p>دیده از دیدار خوبان برگرفتن مشکلست</p></div>
<div class="m2"><p>هر که ما را این نصیحت می‌کند بی‌حاصلست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یار زیبا گر هزارت وحشت از وی در دلست</p></div>
<div class="m2"><p>بامدادان روی او دیدن صباح مقبلست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آن که در چاه زنخدانش دل بیچارگان</p></div>
<div class="m2"><p>چون ملک محبوس در زندان چاه بابلست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پیش از این من دعوی پرهیزگاری کردمی</p></div>
<div class="m2"><p>باز می‌گویم که هر دعوی که کردم باطلست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زهر نزدیک خردمندان اگر چه قاتلست</p></div>
<div class="m2"><p>چون ز دست دوست می‌گیری شفای عاجلست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>من قدم بیرون نمی‌یارم نهاد از کوی دوست</p></div>
<div class="m2"><p>دوستان معذور داریدم که پایم در گلست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>باش تا دیوانه گویندم همه فرزانگان</p></div>
<div class="m2"><p>ترک جان نتوان گرفتن تا تو گویی عاقلست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آن که می‌گوید نظر در صورت خوبان خطاست</p></div>
<div class="m2"><p>او همین صورت همی‌بیند ز معنی غافلست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ساربان آهسته ران کآرام جان در محملست</p></div>
<div class="m2"><p>چارپایان بار بر پشتند و ما را بر دلست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گر به صد منزل فراق افتد میان ما و دوست</p></div>
<div class="m2"><p>همچنانش در میان جان شیرین منزلست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سعدی آسانست با هر کس گرفتن دوستی</p></div>
<div class="m2"><p>لیک چون پیوند شد خو باز کردن مشکلست</p></div></div>