---
title: >-
    غزل شمارهٔ  ۳۱۹
---
# غزل شمارهٔ  ۳۱۹

<div class="b" id="bn1"><div class="m1"><p>هر که بی دوست می‌برد خوابش</p></div>
<div class="m2"><p>همچنان صبر هست و پایابش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خواب از آن چشم چشم نتوان داشت</p></div>
<div class="m2"><p>که ز سر برگذشت سیلابش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نه به خود می‌رود گرفته عشق</p></div>
<div class="m2"><p>دیگری می‌برد به قلابش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چه کند پای بند مهر کسی</p></div>
<div class="m2"><p>که نبیند جفای اصحابش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر که حاجت به درگهی دارد</p></div>
<div class="m2"><p>لازمست احتمال بوابش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ناگزیرست تلخ و شیرینش</p></div>
<div class="m2"><p>خار و خرما و زهر و جلابش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سایرست این مثل که مستسقی</p></div>
<div class="m2"><p>نکند رود دجله سیرابش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شب هجران دوست ظلمانیست</p></div>
<div class="m2"><p>ور برآید هزار مهتابش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>برود جان مستمند از تن</p></div>
<div class="m2"><p>نرود مهر مهر احبابش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سعدیا گوسفند قربانی</p></div>
<div class="m2"><p>به که نالد ز دست قصابش</p></div></div>