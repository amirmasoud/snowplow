---
title: >-
    غزل شمارهٔ  ۴۲۴
---
# غزل شمارهٔ  ۴۲۴

<div class="b" id="bn1"><div class="m1"><p>من از تو صبر ندارم که بی تو بنشینم</p></div>
<div class="m2"><p>کسی دگر نتوانم که بر تو بگزینم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بپرس حال من آخر چو بگذری روزی</p></div>
<div class="m2"><p>که چون همی‌گذرد روزگار مسکینم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من اهل دوزخم ار بی تو زنده خواهم شد</p></div>
<div class="m2"><p>که در بهشت نیارد خدای غمگینم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ندانمت که چه گویم تو هر دو چشم منی</p></div>
<div class="m2"><p>که بی وجود شریفت جهان نمی‌بینم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو روی دوست نبینی جهان ندیدن به</p></div>
<div class="m2"><p>شب فراق منه شمع پیش بالینم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ضرورت است که عهد وفا به سر برمت</p></div>
<div class="m2"><p>و گر جفا به سر آید هزار چندینم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نه هاونم که بنالم به کوفتی از یار</p></div>
<div class="m2"><p>چو دیگ بر سر آتش نشان که بنشینم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بگرد بر سرم ای آسیای دور زمان</p></div>
<div class="m2"><p>به هر جفا که توانی که سنگ زیرینم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو بلبل آمدمت تا چو گل ثنا گویم</p></div>
<div class="m2"><p>چو لاله لال بکردی زبان تحسینم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مرا پلنگ به سرپنجه ای نگار نکشت</p></div>
<div class="m2"><p>تو می‌کشی به سر پنجه نگارینم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چو ناف آهو خونم بسوخت در دل تنگ</p></div>
<div class="m2"><p>برفت در همه آفاق بوی مشکینم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هنر بیار و زبان آوری مکن سعدی</p></div>
<div class="m2"><p>چه حاجت است بگوید شکر که شیرینم</p></div></div>