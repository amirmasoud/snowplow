---
title: >-
    غزل شمارهٔ  ۵۷
---
# غزل شمارهٔ  ۵۷

<div class="b" id="bn1"><div class="m1"><p>هر صبحدم نسیم گل از بوستان توست</p></div>
<div class="m2"><p>الحان بلبل از نفس دوستان توست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون خضر دید آن لب جان بخش دلفریب</p></div>
<div class="m2"><p>گفتا که آب چشمهٔ حیوان دهان توست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یوسف به بندگیت کمر بسته بر میان</p></div>
<div class="m2"><p>بودش یقین که ملک ملاحت از آن توست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر شاهدی که در نظر آمد به دلبری</p></div>
<div class="m2"><p>در دل نیافت راه که آنجا مکان توست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هرگز نشان ز چشمهٔ کوثر شنیده‌ای</p></div>
<div class="m2"><p>کاو را نشانی از دهن بی‌نشان توست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از رشک آفتاب جمالت بر آسمان</p></div>
<div class="m2"><p>هر ماه ماه دیدم چون ابروان توست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>این باد روح پرور از انفاس صبحدم</p></div>
<div class="m2"><p>گویی مگر ز طرهٔ عنبرفشان توست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>صد پیرهن قبا کنم از خرمی اگر</p></div>
<div class="m2"><p>بینم که دست من چو کمر در میان توست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گفتند میهمانی عشاق می‌کنی</p></div>
<div class="m2"><p>سعدی به بوسه‌ای ز لبت میهمان توست</p></div></div>