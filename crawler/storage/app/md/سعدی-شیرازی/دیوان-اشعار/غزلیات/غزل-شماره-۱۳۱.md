---
title: >-
    غزل شمارهٔ  ۱۳۱
---
# غزل شمارهٔ  ۱۳۱

<div class="b" id="bn1"><div class="m1"><p>دوشم آن سنگ دل پریشان داشت</p></div>
<div class="m2"><p>یار دل برده دست بر جان داشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دیده در می‌فشاند در دامن</p></div>
<div class="m2"><p>گوییا آستین مرجان داشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اندرونم ز شوق می‌سوزد</p></div>
<div class="m2"><p>ور ننالیدمی چه درمان داشت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>می‌نپنداشتم که روز شود</p></div>
<div class="m2"><p>تا بدیدم سحر که پایان داشت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در باغ بهشت بگشودند</p></div>
<div class="m2"><p>باد گویی کلید رضوان داشت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>غنچه دیدم که از نسیم صبا</p></div>
<div class="m2"><p>همچو من دست در گریبان داشت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>که نه تنها منم ربوده عشق</p></div>
<div class="m2"><p>هر گلی بلبلی غزل خوان داشت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>رازم از پرده برملا افتاد</p></div>
<div class="m2"><p>چند شاید به صبر پنهان داشت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سعدیا ترک جان بباید گفت</p></div>
<div class="m2"><p>که به یک دل دو دوست نتوان داشت</p></div></div>