---
title: >-
    غزل شمارهٔ  ۴۸۷
---
# غزل شمارهٔ  ۴۸۷

<div class="b" id="bn1"><div class="m1"><p>پنجه با ساعد سیمین که نیندازی به</p></div>
<div class="m2"><p>با توانای معربد نکنی بازی به</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون دلش دادی و مهرش ستدی چاره نماند</p></div>
<div class="m2"><p>اگر او با تو نسازد تو در او سازی به</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جز غم یار مخور تا غم کارت بخورد</p></div>
<div class="m2"><p>تو که با مصلحت خویش نپردازی به</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سپر صبر تحمل نکند تیر فراق</p></div>
<div class="m2"><p>با کمان ابرو اگر جنگ نیاغازی به</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با چنین یار که ما عقد محبت بستیم</p></div>
<div class="m2"><p>گر همه مایه زیان می‌کند انبازی به</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بنده را بر خط فرمان خداوند امور</p></div>
<div class="m2"><p>سر تسلیم نهادن ز سرافرازی به</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر چو چنگم بزنی پیش تو سر برنکنم</p></div>
<div class="m2"><p>این چنین یار وفادار که بنوازی به</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هیچ شک نیست به تیر اجل ای یار عزیز</p></div>
<div class="m2"><p>که من از پای درآیم چو تو اندازی به</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مجلس ما دگر امروز به بستان ماند</p></div>
<div class="m2"><p>مطرب از بلبل عاشق به خوش آوازی به</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گوش بر ناله مطرب کن و بلبل بگذار</p></div>
<div class="m2"><p>که نگوید سخن از سعدی شیرازی به</p></div></div>