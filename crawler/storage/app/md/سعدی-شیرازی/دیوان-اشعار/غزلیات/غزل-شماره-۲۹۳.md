---
title: >-
    غزل شمارهٔ  ۲۹۳
---
# غزل شمارهٔ  ۲۹۳

<div class="b" id="bn1"><div class="m1"><p>آفتابست آن پری رخ یا ملایک یا بشر</p></div>
<div class="m2"><p>قامتست آن یا قیامت یا الف یا نیشکر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هد صبری ما تولی رد عقلی ما ثنا</p></div>
<div class="m2"><p>صاد قلبی ما تمشی زاد وجدی ما عبر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گلبنست آن یا تن نازک نهادش یا حریر</p></div>
<div class="m2"><p>آهنست آن یا دل نامهربانش یا حجر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تهت و المطلوب عندی کیف حالی ان نا</p></div>
<div class="m2"><p>حرت و المامول نحوی ما احتیالی ان هجر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>باغ فردوسست گلبرگش نخوانم یا بهار</p></div>
<div class="m2"><p>جان شیرینست خورشیدش نگویم یا قمر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>قل لمن یبغی فرارا منه هل لی سلوه</p></div>
<div class="m2"><p>ام علی التقدیر انی ابتغی این المفر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر فراز سرو سیمینش چو بخرامد به ناز</p></div>
<div class="m2"><p>چشم شورانگیز بین تا نجم بینی بر شجر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>یکره المحبوب وصلی انتهی عما نهی</p></div>
<div class="m2"><p>یرسم المنظور قتلی ارتضی فیما امر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کاش اندک مایه نرمی در خطابش دیدمی</p></div>
<div class="m2"><p>ور مرا عشقش به سختی کشت سهلست این قدر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>قیل لی فی الحب اخطار و تحصیل المنی</p></div>
<div class="m2"><p>دوله القی بمن القی بروحی فی الخطر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گوشه گیر ای یار یا جان در میان آور که عشق</p></div>
<div class="m2"><p>تیربارانست یا تسلیم باید یا حذر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>فالتنائی غصه ما ذاق الامن صبا</p></div>
<div class="m2"><p>و التدانی فرصه ما نال الا من صبر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>دختران طبع را یعنی سخن با این جمال</p></div>
<div class="m2"><p>آبرویی نیست پیش آن آن زیبا پسر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>لحظک القتال یغوی فی هلاکی لا تدع</p></div>
<div class="m2"><p>عطفک المیاس یسعی فی بلائی لا تذر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>آخر ای سرو روان بر ما گذر کن یک زمان</p></div>
<div class="m2"><p>آخر ای آرام جان در ما نظر کن یک نظر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>یا رخیم الجسم لو لا انت شخصی ما انحنی</p></div>
<div class="m2"><p>یا کحیل الطرف لو لا انت دمعی ما انحدر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>دوستی را گفتم اینک عمر شد گفت ای عجب</p></div>
<div class="m2"><p>طرفه می‌دارم که بی دلدار چون بردی به سر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بعض خلانی اتانی سائلا عن قصتی</p></div>
<div class="m2"><p>قلت لا تسئل صفار الوجه یغنی عن خبر</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>گفت سعدی صبر کن یا سیم و زر ده یا گریز</p></div>
<div class="m2"><p>عشق را یا مال باید یا صبوری یا سفر</p></div></div>