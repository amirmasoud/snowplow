---
title: >-
    غزل شمارهٔ  ۵۰۳
---
# غزل شمارهٔ  ۵۰۳

<div class="b" id="bn1"><div class="m1"><p>تو پری زاده ندانم ز کجا می‌آیی</p></div>
<div class="m2"><p>کآدمیزاده نباشد به چنین زیبایی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>راست خواهی نه حلال است که پنهان دارند</p></div>
<div class="m2"><p>مثل این روی و نشاید که به کس بنمایی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سرو با قامت زیبای تو در مجلس باغ</p></div>
<div class="m2"><p>نتواند که کند دعوی همبالایی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در سراپای وجودت هنری نیست که نیست</p></div>
<div class="m2"><p>عیبت آن است که بر بنده نمی‌بخشایی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به خدا بر تو که خون من بیچاره مریز</p></div>
<div class="m2"><p>که من آن قدر ندارم که تو دست آلایی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بی رخت چشم ندارم که جهانی بینم</p></div>
<div class="m2"><p>به دو چشمت که ز چشمم مرو ای بینایی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نه مرا حسرت جاه است و نه اندیشه مال</p></div>
<div class="m2"><p>همه اسباب مهیاست تو در می‌بایی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بر من از دست تو چندان که جفا می‌آید</p></div>
<div class="m2"><p>خوشتر و خوبتر اندر نظرم می‌آیی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دیگری نیست که مهر تو در او شاید بست</p></div>
<div class="m2"><p>چاره بعد از تو ندانیم به جز تنهایی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ور به خواری ز در خویش برانی ما را</p></div>
<div class="m2"><p>همچنان شکر کنیمت که عزیز مایی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>من از این در به جفا روی نخواهم پیچید</p></div>
<div class="m2"><p>گر ببندی تو به روی من و گر بگشایی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چه کند داعی دولت که قبولش نکنند</p></div>
<div class="m2"><p>ما حریصیم به خدمت تو نمی‌فرمایی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>سعدیا دختر انفاس تو بس دل ببرد</p></div>
<div class="m2"><p>به چنین زیور معنی که تو می‌آرایی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>باد نوروز که بوی گل و سنبل دارد</p></div>
<div class="m2"><p>لطف این باد ندارد که تو می‌پیمایی</p></div></div>