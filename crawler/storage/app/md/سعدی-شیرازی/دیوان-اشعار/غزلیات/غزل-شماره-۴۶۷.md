---
title: >-
    غزل شمارهٔ  ۴۶۷
---
# غزل شمارهٔ  ۴۶۷

<div class="b" id="bn1"><div class="m1"><p>آخر نگهی به سوی ما کن</p></div>
<div class="m2"><p>دردی به ارادتی دوا کن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بسیار خلاف عهد کردی</p></div>
<div class="m2"><p>آخر به غلط یکی وفا کن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ما را تو به خاطری همه روز</p></div>
<div class="m2"><p>یک روز تو نیز یاد ما کن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>این قاعده خلاف بگذار</p></div>
<div class="m2"><p>وین خوی معاندت رها کن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>برخیز و در سرای در بند</p></div>
<div class="m2"><p>بنشین و قبای بسته وا کن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آن را که هلاک می‌پسندی</p></div>
<div class="m2"><p>روزی دو به خدمت آشنا کن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون انس گرفت و مهر پیوست</p></div>
<div class="m2"><p>بازش به فراق مبتلا کن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سعدی چو حریف ناگزیر است</p></div>
<div class="m2"><p>تن درده و چشم در قضا کن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شمشیر که می‌زند سپر باش</p></div>
<div class="m2"><p>دشنام که می‌دهد دعا کن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زیبا نبود شکایت از دوست</p></div>
<div class="m2"><p>زیبا همه روز گو جفا کن</p></div></div>