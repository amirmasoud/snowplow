---
title: >-
    غزل شمارهٔ  ۵۳۲
---
# غزل شمارهٔ  ۵۳۲

<div class="b" id="bn1"><div class="m1"><p>چون خراباتی نباشد زاهدی</p></div>
<div class="m2"><p>کش به شب از در درآید شاهدی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>محتسب گو تا ببیند روی دوست</p></div>
<div class="m2"><p>همچو محرابی و من چون عابدی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون من آب زندگانی یافتم</p></div>
<div class="m2"><p>غم نباشد گر بمیرد حاسدی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آنچه ما را در دل است از سوز عشق</p></div>
<div class="m2"><p>می‌نشاید گفت با هر باردی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دوستان گیرند و دلداران ولیک</p></div>
<div class="m2"><p>مهربان نشناسد الا واحدی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از تو روحانی‌ترم در پیش دل</p></div>
<div class="m2"><p>نگذرد شب‌های خلوت واردی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خانه‌ای در کوی درویشان بگیر</p></div>
<div class="m2"><p>تا نماند در محلت زاهدی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر دلی داری و دلبندیت نیست</p></div>
<div class="m2"><p>پس چه فرق از ناطقی تا جامدی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر به خدمت قائمی خواهی منم</p></div>
<div class="m2"><p>ور نمی‌خواهی به حسرت قاعدی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سعدیا گر روزگارت می‌کشد</p></div>
<div class="m2"><p>گو بکش بر دست سیمین ساعدی</p></div></div>