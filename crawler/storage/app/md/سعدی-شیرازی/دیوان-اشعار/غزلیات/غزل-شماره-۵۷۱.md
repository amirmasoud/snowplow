---
title: >-
    غزل شمارهٔ  ۵۷۱
---
# غزل شمارهٔ  ۵۷۱

<div class="b" id="bn1"><div class="m1"><p>تو اگر به حسن دعوی بکنی گواه داری</p></div>
<div class="m2"><p>که جمال سرو بستان و کمال ماه داری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در کس نمی‌گشایم که به خاطرم درآید</p></div>
<div class="m2"><p>تو به اندرون جان آی که جایگاه داری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ملکی مهی ندانم به چه کنیتت بخوانم</p></div>
<div class="m2"><p>به کدام جنس گویم که تو اشتباه داری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر کس نمی‌توانم به شکایت از تو رفتن</p></div>
<div class="m2"><p>که قبول و قوتت هست و جمال و جاه داری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گل بوستان رویت چو شقایق است لیکن</p></div>
<div class="m2"><p>چه کنم به سرخ رویی که دلی سیاه داری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چه خطای بنده دیدی که خلاف عهد کردی</p></div>
<div class="m2"><p>مگر آن که ما ضعیفیم و تو دستگاه داری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نه کمال حسن باشد ترشی و روی شیرین</p></div>
<div class="m2"><p>همه بد مکن که مردم همه نیکخواه داری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تو جفا کنی و صولت دگران دعای دولت</p></div>
<div class="m2"><p>چه کنند از این لطافت که تو پادشاه داری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به یکی لطیفه گفتی ببرم هزار دل را</p></div>
<div class="m2"><p>نه چنان لطیف باشد که دلی نگاه داری</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به خدای اگر چو سعدی برود دلت به راهی</p></div>
<div class="m2"><p>همه شب چنو نخسبی و نظر به راه داری</p></div></div>