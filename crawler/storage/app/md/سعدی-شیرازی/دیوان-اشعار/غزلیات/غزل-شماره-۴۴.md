---
title: >-
    غزل شمارهٔ  ۴۴
---
# غزل شمارهٔ  ۴۴

<div class="b" id="bn1"><div class="m1"><p>بوی گل و بانگ مرغ برخاست</p></div>
<div class="m2"><p>هنگام نشاط و روز صحراست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فراش خزان ورق بیفشاند</p></div>
<div class="m2"><p>نقاش صبا چمن بیاراست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ما را سر باغ و بوستان نیست</p></div>
<div class="m2"><p>هر جا که تویی تفرج آن جاست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گویند نظر به روی خوبان</p></div>
<div class="m2"><p>نهیست نه این نظر که ما راست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در روی تو سر صنع بی چون</p></div>
<div class="m2"><p>چون آب در آبگینه پیداست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چشم چپ خویشتن برآرم</p></div>
<div class="m2"><p>تا چشم نبیندت به جز راست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر آدمیی که مهر مهرت</p></div>
<div class="m2"><p>در وی نگرفت سنگ خاراست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>روزی تر و خشک من بسوزد</p></div>
<div class="m2"><p>آتش که به زیر دیگ سوداست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نالیدن بی‌حساب سعدی</p></div>
<div class="m2"><p>گویند خلاف رای داناست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از ورطه ما خبر ندارد</p></div>
<div class="m2"><p>آسوده که بر کنار دریاست</p></div></div>