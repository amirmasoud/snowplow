---
title: >-
    غزل شمارهٔ  ۱۶۰
---
# غزل شمارهٔ  ۱۶۰

<div class="b" id="bn1"><div class="m1"><p>حدیث عشق به طومار در نمی‌گنجد</p></div>
<div class="m2"><p>بیان دوست به گفتار در نمی‌گنجد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سماع انس که دیوانگان از آن مستند</p></div>
<div class="m2"><p>به سمع مردم هشیار در نمی‌گنجد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>میسرت نشود عاشقی و مستوری</p></div>
<div class="m2"><p>ورع به خانه خمار در نمی‌گنجد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چنان فراخ نشستست یار در دل تنگ</p></div>
<div class="m2"><p>که بیش زحمت اغیار در نمی‌گنجد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تو را چنان که تویی من صفت ندانم کرد</p></div>
<div class="m2"><p>که عرض جامه به بازار در نمی‌گنجد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دگر به صورت هیچ آفریده دل ندهم</p></div>
<div class="m2"><p>که با تو صورت دیوار در نمی‌گنجد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خبر که می‌دهد امشب رقیب مسکین را</p></div>
<div class="m2"><p>که سگ به زاویه غار در نمی‌گنجد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو گل به بار بود همنشین خار بود</p></div>
<div class="m2"><p>چو در کنار بود خار در نمی‌گنجد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چنان ارادت و شوقست در میان دو دوست</p></div>
<div class="m2"><p>که سعی دشمن خون خوار در نمی‌گنجد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به چشم دل نظرت می‌کنم که دیده سر</p></div>
<div class="m2"><p>ز برق شعله دیدار در نمی‌گنجد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز دوستان که تو را هست جای سعدی نیست</p></div>
<div class="m2"><p>گدا میان خریدار در نمی‌گنجد</p></div></div>