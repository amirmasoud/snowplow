---
title: >-
    غزل شمارهٔ  ۳۴
---
# غزل شمارهٔ  ۳۴

<div class="b" id="bn1"><div class="m1"><p>دل هر که صید کردی نکشد سر از کمندت</p></div>
<div class="m2"><p>نه دگر امید دارد که رها شود ز بندت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به خدا که پرده از روی چو آتشت برافکن</p></div>
<div class="m2"><p>که به اتفاق بینی دل عالمی سپندت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نه چمن شکوفه‌ای رست چو روی دلستانت</p></div>
<div class="m2"><p>نه صبا صنوبری یافت چو قامت بلندت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گرت آرزوی آنست که خون خلق ریزی</p></div>
<div class="m2"><p>چه کند که شیر گردن ننهد چو گوسفندت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تو امیر ملک حسنی به حقیقت ای دریغا</p></div>
<div class="m2"><p>اگر التفات بودی به فقیر مستمندت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نه تو را بگفتم ای دل که سر وفا ندارد</p></div>
<div class="m2"><p>به طمع ز دست رفتی و به پای درفکندت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تو نه مرد عشق بودی خود از این حساب سعدی</p></div>
<div class="m2"><p>که نه قوت گریزست و نه طاقت گزندت</p></div></div>