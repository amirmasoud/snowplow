---
title: >-
    غزل شمارهٔ  ۶۹
---
# غزل شمارهٔ  ۶۹

<div class="b" id="bn1"><div class="m1"><p>عشرت خوش است و بر طرف جوی خوشتر است</p></div>
<div class="m2"><p>می بر سماع بلبل خوشگوی خوشتر است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عیش است بر کنار سمن زار خواب صبح؟</p></div>
<div class="m2"><p>نی، در کنار یار سمن بوی خوشتر است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خواب از خمار بادهٔ نوشین بامداد</p></div>
<div class="m2"><p>بر بستر شقایق خودروی خوشتر است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>روی از جمال دوست به صحرا مکن که روی</p></div>
<div class="m2"><p>در روی همنشین وفاجوی خوشتر است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آواز چنگ مطرب خوشگوی گو مباش</p></div>
<div class="m2"><p>ما را حدیث همدم خوشخوی خوشتر است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر شاهد است سبزه بر اطراف گلستان</p></div>
<div class="m2"><p>بر عارضین شاهد گلروی خوشتر است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آب از نسیم باد زره روی گشته گیر</p></div>
<div class="m2"><p>مفتول زلف یار زره موی خوشتر است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گو چشمه آب کوثر و بستان بهشت باش</p></div>
<div class="m2"><p>ما را مقام بر سر این کوی خوشتر است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سعدی! جفا نبرده چه دانی تو قدر یار؟</p></div>
<div class="m2"><p>تحصیل کام دل به تکاپوی خوشتر است</p></div></div>