---
title: >-
    غزل شمارهٔ  ۳۸۷
---
# غزل شمارهٔ  ۳۸۷

<div class="b" id="bn1"><div class="m1"><p>من آن نیم که دل از مهر دوست بردارم</p></div>
<div class="m2"><p>و گر ز کینه دشمن به جان رسد کارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نه روی رفتنم از خاک آستانه دوست</p></div>
<div class="m2"><p>نه احتمال نشستن نه پای رفتارم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کجا روم که دلم پای بند مهر کسیست</p></div>
<div class="m2"><p>سفر کنید رفیقان که من گرفتارم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نه او به چشم ارادت نظر به جانب ما</p></div>
<div class="m2"><p>نمی‌کند که من از ضعف ناپدیدارم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگر هزار تعنت کنی و طعنه زنی</p></div>
<div class="m2"><p>من این طریق محبت ز دست نگذارم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مرا به منظر خوبان اگر نباشد میل</p></div>
<div class="m2"><p>درست شد به حقیقت که نقش دیوارم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در آن قضیه که با ما به صلح باشد دوست</p></div>
<div class="m2"><p>اگر جهان همه دشمن شود چه غم دارم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به عشق روی تو اقرار می‌کند سعدی</p></div>
<div class="m2"><p>همه جهان به درآیند گو به انکارم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کجا توانمت انکار دوستی کردن</p></div>
<div class="m2"><p>که آب دیده گواهی دهد به اقرارم</p></div></div>