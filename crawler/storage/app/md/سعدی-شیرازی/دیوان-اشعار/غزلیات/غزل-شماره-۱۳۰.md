---
title: >-
    غزل شمارهٔ  ۱۳۰
---
# غزل شمارهٔ  ۱۳۰

<div class="b" id="bn1"><div class="m1"><p>دوش دور از رویت ای جان جانم از غم تاب داشت</p></div>
<div class="m2"><p>ابر چشمم بر رخ از سودای دل سیلاب داشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در تفکر عقل مسکین پایمال عشق شد</p></div>
<div class="m2"><p>با پریشانی دل شوریده چشم خواب داشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کوس غارت زد فراقت گرد شهرستان دل</p></div>
<div class="m2"><p>شحنه عشقت سرای عقل در طبطاب داشت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نقش نامت کرده دل محراب تسبیح وجود</p></div>
<div class="m2"><p>تا سحر تسبیح گویان روی در محراب داشت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دیده‌ام می‌جست و گفتندم نبینی روی دوست</p></div>
<div class="m2"><p>خود درفشان بود چشمم کاندر او سیماب داشت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز آسمان آغاز کارم سخت شیرین می‌نمود</p></div>
<div class="m2"><p>کی گمان بردم که شهدآلوده زهر ناب داشت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سعدی این ره مشکل افتادست در دریای عشق</p></div>
<div class="m2"><p>اول آخر در صبوری اندکی پایاب داشت</p></div></div>