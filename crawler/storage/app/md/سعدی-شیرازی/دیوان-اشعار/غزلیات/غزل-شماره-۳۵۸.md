---
title: >-
    غزل شمارهٔ  ۳۵۸
---
# غزل شمارهٔ  ۳۵۸

<div class="b" id="bn1"><div class="m1"><p>زهی سعادت من که‌م تو آمدی به سلام</p></div>
<div class="m2"><p>خوش آمدی و علیک السلام و الاکرام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قیام خواستمت کرد عقل می‌گوید</p></div>
<div class="m2"><p>مکن که شرط ادب نیست پیش سرو قیام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگر کساد شکر بایدت دهن بگشای</p></div>
<div class="m2"><p>ورت خجالت سرو آرزو کند بخرام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تو آفتاب منیری و دیگران انجم</p></div>
<div class="m2"><p>تو روح پاکی و ابنای روزگار اجسام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگر تو آدمیی اعتقاد من این است</p></div>
<div class="m2"><p>که دیگران همه نقشند بر در حمام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تنک مپوش که اندام‌های سیمینت</p></div>
<div class="m2"><p>درون جامه پدید است چون گلاب از جام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از اتفاق چه خوشتر بود میان دو دوست</p></div>
<div class="m2"><p>درون پیرهنی چون دو مغز یک بادام</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سماع اهل دل آواز ناله سعدیست</p></div>
<div class="m2"><p>چه جای زمزمه عندلیب و سجع حمام</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در این سماع همه ساقیان شاهدروی</p></div>
<div class="m2"><p>بر این شراب همه صوفیان دردآشام</p></div></div>