---
title: >-
    غزل شمارهٔ  ۵۲۴
---
# غزل شمارهٔ  ۵۲۴

<div class="b" id="bn1"><div class="m1"><p>یارا قدحی پر کن از آن داروی مستی</p></div>
<div class="m2"><p>تا از سر صوفی برود علت هستی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عاقل متفکر بود و مصلحت اندیش</p></div>
<div class="m2"><p>در مذهب عشق آی و از این جمله برستی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای فتنه نوخاسته از عالم قدرت</p></div>
<div class="m2"><p>غایب مشو از دیده که در دل بنشستی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آرام دلم بستدی و دست شکیبم</p></div>
<div class="m2"><p>برتافتی و پنجه صبرم بشکستی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>احوال دو چشم من بر هم ننهاده</p></div>
<div class="m2"><p>با تو نتوان گفت به خواب شب مستی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سودازده‌ای کز همه عالم به تو پیوست</p></div>
<div class="m2"><p>دل نیک بدادت که دل از وی بگسستی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در روی تو گفتم سخنی چند بگویم</p></div>
<div class="m2"><p>رو باز گشادی و در نطق ببستی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر باده از این خم بود و مطرب از این کوی</p></div>
<div class="m2"><p>ما توبه بخواهیم شکستن به درستی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سعدی غرض از حقه تن آیت حق است</p></div>
<div class="m2"><p>صد تعبیه در توست و یکی باز نجستی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نقاش وجود این همه صورت که بپرداخت</p></div>
<div class="m2"><p>تا نقش ببینی و مصور بپرستی</p></div></div>