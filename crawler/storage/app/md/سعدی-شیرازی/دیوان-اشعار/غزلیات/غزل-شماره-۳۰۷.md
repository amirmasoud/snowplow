---
title: >-
    غزل شمارهٔ  ۳۰۷
---
# غزل شمارهٔ  ۳۰۷

<div class="b" id="bn1"><div class="m1"><p>دل برگرفتی از برم ای دوست دست گیر</p></div>
<div class="m2"><p>کز دست می‌رود سرم ای دوست دست گیر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شرط است دستگیری درمندگان و من</p></div>
<div class="m2"><p>هر روز ناتوان ترم ای دوست دست گیر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پایاب نیست بحر غمت را و من غریق</p></div>
<div class="m2"><p>خواهم که سر برآورم ای دوست دست گیر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سر می‌نهم که پای برآرم ز دام عشق</p></div>
<div class="m2"><p>وین کی شود میسرم ای دوست دست گیر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دل جان همی‌سپارد و فریاد می‌کند</p></div>
<div class="m2"><p>کآخر به کار تو درم ای دوست دست گیر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>راضی شدم به یک نظر اکنون که وصل نیست</p></div>
<div class="m2"><p>آخر بدین محقرم ای دوست دست گیر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از دامن تو دست ندارم که دست نیست</p></div>
<div class="m2"><p>بر دستگیر دیگرم ای دوست دست گیر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سعدی نه بارها به تو برداشت دست عجز</p></div>
<div class="m2"><p>یک بارش از سر کرم ای دوست دست گیر</p></div></div>