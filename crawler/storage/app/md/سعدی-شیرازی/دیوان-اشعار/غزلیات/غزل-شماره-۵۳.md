---
title: >-
    غزل شمارهٔ  ۵۳
---
# غزل شمارهٔ  ۵۳

<div class="b" id="bn1"><div class="m1"><p>دیدار تو حل مشکلات است</p></div>
<div class="m2"><p>صبر از تو خلاف ممکنات است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دیباچهٔ صورت بدیعت</p></div>
<div class="m2"><p>عنوان کمال حسن ذات است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>لب‌های تو خضر اگر بدیدی</p></div>
<div class="m2"><p>گفتی: «لب چشمه حیات است!»</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر کوزهٔ آب نه دهانت</p></div>
<div class="m2"><p>بردار که کوزهٔ نبات است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ترسم تو به سحر غمزه یک روز</p></div>
<div class="m2"><p>دعوی بکنی که معجزات است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زهر از قبل تو نوشدارو</p></div>
<div class="m2"><p>فحش از دهن تو طیبات است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون روی تو صورتی ندیدم</p></div>
<div class="m2"><p>در شهر که مبطل صلات است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عهد تو و توبهٔ من از عشق</p></div>
<div class="m2"><p>می‌بینم و هر دو بی‌ثبات است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آخر نگهی به سوی ما کن</p></div>
<div class="m2"><p>کاین دولت حسن را زکات است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چون تشنه بسوخت در بیابان</p></div>
<div class="m2"><p>چه فایده گر جهان فرات است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سعدی غم نیستی ندارد</p></div>
<div class="m2"><p>جان دادن عاشقان نجات است</p></div></div>