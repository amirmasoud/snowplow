---
title: >-
    غزل شمارهٔ  ۴۰۴
---
# غزل شمارهٔ  ۴۰۴

<div class="b" id="bn1"><div class="m1"><p>غم زمانه خورم یا فراق یار کشم</p></div>
<div class="m2"><p>به طاقتی که ندارم کدام بار کشم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نه قوتی که توانم کناره جستن از او</p></div>
<div class="m2"><p>نه قدرتی که به شوخیش در کنار کشم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نه دست صبر که در آستین عقل برم</p></div>
<div class="m2"><p>نه پای عقل که در دامن قرار کشم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز دوستان به جفا سیرگشت مردی نیست</p></div>
<div class="m2"><p>جفای دوست زنم گر نه مردوار کشم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو می‌توان به صبوری کشید جور عدو</p></div>
<div class="m2"><p>چرا صبور نباشم که جور یار کشم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شراب خورده ساقی ز جام صافی وصل</p></div>
<div class="m2"><p>ضرورت است که درد سر خمار کشم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گلی چو روی تو گر در چمن به دست آید</p></div>
<div class="m2"><p>کمینه دیده سعدیش پیش خار کشم</p></div></div>