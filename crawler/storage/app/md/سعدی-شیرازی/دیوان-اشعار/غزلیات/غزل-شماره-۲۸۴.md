---
title: >-
    غزل شمارهٔ  ۲۸۴
---
# غزل شمارهٔ  ۲۸۴

<div class="b" id="bn1"><div class="m1"><p>به کوی لاله رخان هر که عشقباز آید</p></div>
<div class="m2"><p>امید نیست که دیگر به عقل بازآید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کبوتری که دگر آشیان نخواهد دید</p></div>
<div class="m2"><p>قضا همی‌بردش تا به چنگ باز آید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ندانم ابروی شوخت چگونه محرابیست</p></div>
<div class="m2"><p>که گر ببیند زندیق در نماز آید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بزرگوار مقامی و نیکبخت کسی</p></div>
<div class="m2"><p>که هر دم از در او چون تویی فراز آید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ترش نباشم اگر صد جواب تلخ دهی</p></div>
<div class="m2"><p>که از دهان تو شیرین و دلنواز آید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بیا و گونه زردم ببین و نقش بخوان</p></div>
<div class="m2"><p>که گر حدیث کنم قصه‌ای دراز آید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خروشم از تف سینه‌ست و ناله از سر درد</p></div>
<div class="m2"><p>نه چون دگر سخنان کز سر مجاز آید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به جای خاک قدم بر دو چشم سعدی نه</p></div>
<div class="m2"><p>که هر که چون تو گرامی بود به ناز آید</p></div></div>