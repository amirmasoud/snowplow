---
title: >-
    غزل شمارهٔ  ۹۸
---
# غزل شمارهٔ  ۹۸

<div class="b" id="bn1"><div class="m1"><p>گفتم مگر به خواب ببینم خیال دوست</p></div>
<div class="m2"><p>اینک علی الصباح نظر بر جمال دوست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مردم هلال عید بدیدند و پیش ما</p></div>
<div class="m2"><p>عیدست و آنک ابروی همچون هلال دوست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ما را دگر به سرو بلند التفات نیست</p></div>
<div class="m2"><p>از دوستی قامت بااعتدال دوست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زان بیخودم که عاشق صادق نباشدش</p></div>
<div class="m2"><p>پروای نفس خویشتن از اشتغال دوست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای خواب گرد دیده سعدی دگر مگرد</p></div>
<div class="m2"><p>یا دیده جای خواب بود یا خیال دوست</p></div></div>