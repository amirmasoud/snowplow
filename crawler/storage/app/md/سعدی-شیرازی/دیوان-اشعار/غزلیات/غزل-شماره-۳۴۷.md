---
title: >-
    غزل شمارهٔ  ۳۴۷
---
# غزل شمارهٔ  ۳۴۷

<div class="b" id="bn1"><div class="m1"><p>جزای آن که نگفتیم شکر روز وصال</p></div>
<div class="m2"><p>شب فراق نخفتیم لاجرم ز خیال</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بدار یک نفس ای قائد این زمام جمال</p></div>
<div class="m2"><p>که دیده سیر نمی‌گردد از نظر به جمال</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دگر به گوش فراموش عهد سنگین دل</p></div>
<div class="m2"><p>پیام ما که رساند مگر نسیم شمال</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به تیغ هندی دشمن قتال می‌نکند</p></div>
<div class="m2"><p>چنان که دوست به شمشیر غمزه قتال</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جماعتی که نظر را حرام می‌گویند</p></div>
<div class="m2"><p>نظر حرام بکردند و خون خلق حلال</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>غزال اگر به کمند اوفتد عجب نبود</p></div>
<div class="m2"><p>عجب فتادن مرد است در کمند غزال</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تو بر کنار فراتی ندانی این معنی</p></div>
<div class="m2"><p>به راه بادیه دانند قدر آب زلال</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اگر مراد نصیحت کنان ما این است</p></div>
<div class="m2"><p>که ترک دوست بگویم تصوریست محال</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به خاک پای تو داند که تا سرم نرود</p></div>
<div class="m2"><p>ز سر به در نرود همچنان امید وصال</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>حدیث عشق چه حاجت که بر زبان آری</p></div>
<div class="m2"><p>به آب دیده خونین نبشته صورت حال</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سخن دراز کشیدیم و همچنان باقیست</p></div>
<div class="m2"><p>که ذکر دوست نیارد به هیچ گونه ملال</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به ناله کار میسر نمی‌شود سعدی</p></div>
<div class="m2"><p>ولیک ناله بیچارگان خوش است بنال</p></div></div>