---
title: >-
    غزل شمارهٔ  ۳۳۶
---
# غزل شمارهٔ  ۳۳۶

<div class="b" id="bn1"><div class="m1"><p>رفتی و نمی‌شوی فراموش</p></div>
<div class="m2"><p>می‌آیی و می‌روم من از هوش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سحر است کمان ابروانت</p></div>
<div class="m2"><p>پیوسته کشیده تا بناگوش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پایت بگذار تا ببوسم</p></div>
<div class="m2"><p>چون دست نمی‌رسد به آغوش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جور از قبلت مقام عدل است</p></div>
<div class="m2"><p>نیش سخنت مقابل نوش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بی‌کار بود که در بهاران</p></div>
<div class="m2"><p>گویند به عندلیب مخروش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دوش آن غم دل که می‌نهفتم</p></div>
<div class="m2"><p>باد سحرش ببرد سرپوش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آن سیل که دوش تا کمر بود</p></div>
<div class="m2"><p>امشب بگذشت خواهد از دوش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شهری متحدثان حسنت</p></div>
<div class="m2"><p>الا متحیران خاموش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بنشین که هزار فتنه برخاست</p></div>
<div class="m2"><p>از حلقه عارفان مدهوش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آتش که تو می‌کنی محال است</p></div>
<div class="m2"><p>کاین دیگ فرونشیند از جوش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بلبل که به دست شاهد افتاد</p></div>
<div class="m2"><p>یاران چمن کند فراموش</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ای خواجه برو به هر چه داری</p></div>
<div class="m2"><p>یاری بخر و به هیچ مفروش</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گر توبه دهد کسی ز عشقت</p></div>
<div class="m2"><p>از من بنیوش و پند منیوش</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>سعدی همه ساله پند مردم</p></div>
<div class="m2"><p>می‌گوید و خود نمی‌کند گوش</p></div></div>