---
title: >-
    غزل شمارهٔ  ۴۲
---
# غزل شمارهٔ  ۴۲

<div class="b" id="bn1"><div class="m1"><p>نشاید گفتن آن کس را دلی هست</p></div>
<div class="m2"><p>که ندهد بر چنین صورت دل از دست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نه منظوری که با او می‌توان گفت</p></div>
<div class="m2"><p>نه خصمی کز کمندش می‌توان رست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به دل گفتم ز چشمانش بپرهیز</p></div>
<div class="m2"><p>که هشیاران نیاویزند با مست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سرانگشتان مخضوبش نبینی</p></div>
<div class="m2"><p>که دست صبر برپیچید و بشکست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نه آزاد از سرش بر می‌توان خاست</p></div>
<div class="m2"><p>نه با او می‌توان آسوده بنشست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اگر دودی رود بی آتشی نیست</p></div>
<div class="m2"><p>و گر خونی بیاید کشته‌ای هست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خیالش در نظر، چون آیدم خواب؟</p></div>
<div class="m2"><p>نشاید در به روی دوستان بست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نشاید خرمن بیچارگان سوخت</p></div>
<div class="m2"><p>نمی‌باید دل درمندگان خست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به آخر دوستی نتوان بریدن</p></div>
<div class="m2"><p>به اول خود نمی‌بایست پیوست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دلی از دست بیرون رفته سعدی</p></div>
<div class="m2"><p>نیاید باز تیر رفته از شست</p></div></div>