---
title: >-
    غزل شمارهٔ  ۴۸۴
---
# غزل شمارهٔ  ۴۸۴

<div class="b" id="bn1"><div class="m1"><p>بیا که در غم عشقت مشوشم بی تو</p></div>
<div class="m2"><p>بیا ببین که در این غم چه ناخوشم بی تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شب از فراق تو می‌نالم ای پری رخسار</p></div>
<div class="m2"><p>چو روز گردد گویی در آتشم بی تو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دمی تو شربت وصلم نداده‌ای جانا</p></div>
<div class="m2"><p>همیشه زهر فراقت همی چشم بی تو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر تو با من مسکین چنین کنی جانا</p></div>
<div class="m2"><p>دو پایم از دو جهان نیز در کشم بی تو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پیام دادم و گفتم بیا خوشم می‌دار</p></div>
<div class="m2"><p>جواب دادی و گفتی که من خوشم بی تو</p></div></div>