---
title: >-
    غزل شمارهٔ  ۴۳۱
---
# غزل شمارهٔ  ۴۳۱

<div class="b" id="bn1"><div class="m1"><p>امشب آن نیست که در خواب رود چشم ندیم</p></div>
<div class="m2"><p>خواب در روضه رضوان نکند اهل نعیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خاک را زنده کند تربیت باد بهار</p></div>
<div class="m2"><p>سنگ باشد که دلش زنده نگردد به نسیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بوی پیراهن گم کرده خود می‌شنوم</p></div>
<div class="m2"><p>گر بگویم همه گویند ضلالیست قدیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عاشق آن گوش ندارد که نصیحت شنود</p></div>
<div class="m2"><p>درد ما نیک نباشد به مداوای حکیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>توبه گویندم از اندیشه معشوق بکن</p></div>
<div class="m2"><p>هرگز این توبه نباشد که گناهیست عظیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای رفیقان سفر دست بدارید از ما</p></div>
<div class="m2"><p>که بخواهیم نشستن به در دوست مقیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای برادر غم عشق آتش نمرود انگار</p></div>
<div class="m2"><p>بر من این شعله چنان است که بر ابراهیم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مرده از خاک لحد رقص کنان برخیزد</p></div>
<div class="m2"><p>گر تو بالای عظامش گذری وهی رمیم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>طمع وصل تو می‌دارم و اندیشه هجر</p></div>
<div class="m2"><p>دیگر از هر چه جهانم نه امید است و نه بیم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>عجب از کشته نباشد به در خیمه دوست</p></div>
<div class="m2"><p>عجب از زنده که چون جان به درآورد سلیم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سعدیا عشق نیامیزد و شهوت با هم</p></div>
<div class="m2"><p>پیش تسبیح ملایک نرود دیو رجیم</p></div></div>