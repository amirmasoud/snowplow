---
title: >-
    غزل شمارهٔ  ۵۱۰
---
# غزل شمارهٔ  ۵۱۰

<div class="b" id="bn1"><div class="m1"><p>نه من تنها گرفتارم به دام زلف زیبایی</p></div>
<div class="m2"><p>که هر کس با دلارامی سری دارند و سودایی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قرین یار زیبا را چه پروای چمن باشد</p></div>
<div class="m2"><p>هزاران سرو بستانی فدای سروبالایی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مرا نسبت به شیدایی کند ماه پری پیکر</p></div>
<div class="m2"><p>تو دل با خویشتن داری چه دانی حال شیدایی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همی‌دانم که فریادم به گوشش می‌رسد لیکن</p></div>
<div class="m2"><p>ملولی را چه غم دارد ز حال ناشکیبایی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عجب دارند یارانم که دستش را همی‌بوسم</p></div>
<div class="m2"><p>ندیدستند مسکینان سری افتاده در پایی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اگر فرهاد را حاصل نشد پیوند با شیرین</p></div>
<div class="m2"><p>نه آخر جان شیرینش برآمد در تمنایی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خرد با عشق می‌کوشد که وی را در کمند آرد</p></div>
<div class="m2"><p>ولیکن بر نمی‌آید ضعیفی با توانایی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مرا وقتی ز نزدیکان ملامت سخت می‌آمد</p></div>
<div class="m2"><p>نترسم دیگر از باران که افتادم به دریایی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تو خواهی خشم بر ما گیر و خواهی چشم بر ما کن</p></div>
<div class="m2"><p>که ما را با کسی دیگر نمانده‌ست از تو پروایی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نپندارم که سعدی را بیازاری و بگذاری</p></div>
<div class="m2"><p>که بعد از سایه لطفت ندارد در جهان جایی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>من آن خاک وفادارم که از من بوی مهر آید</p></div>
<div class="m2"><p>و گر بادم برد چون شعر هر جزوی به اقصایی</p></div></div>