---
title: >-
    غزل شمارهٔ  ۲۵۳
---
# غزل شمارهٔ  ۲۵۳

<div class="b" id="bn1"><div class="m1"><p>به بوی آن که شبی در حرم بیاسایند</p></div>
<div class="m2"><p>هزار بادیه سهلست اگر بپیمایند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>طریق عشق جفا بردن است و جانبازی</p></div>
<div class="m2"><p>دگر چه چاره که با زورمند برنایند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگر به بام برآید ستاره پیشانی</p></div>
<div class="m2"><p>چو ماه عید به انگشت‌هاش بنمایند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در گریز نبسته‌ست لیکن از نظرش</p></div>
<div class="m2"><p>کجا روند اسیران که بند بر پایند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز خون عزیزترم نیست مایه‌ای در تن</p></div>
<div class="m2"><p>فدای دست عزیزان اگر بیالایند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مگر به خیل تو با دوستان نپیوندند</p></div>
<div class="m2"><p>مگر به شهر تو بر عاشقان نبخشایند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فدای جان تو گر جان من طمع داری</p></div>
<div class="m2"><p>غلام حلقه به گوش آن کند که فرمایند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هزار سرو خرامان به راستی نرسد</p></div>
<div class="m2"><p>به قامت تو و گر سر بر آسمان سایند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>حدیث حسن تو و داستان عشق مرا</p></div>
<div class="m2"><p>هزار لیلی و مجنون بر آن نیفزایند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مثال سعدی عود است تا نسوزانی</p></div>
<div class="m2"><p>جماعت از نفسش دم به دم نیاسایند</p></div></div>