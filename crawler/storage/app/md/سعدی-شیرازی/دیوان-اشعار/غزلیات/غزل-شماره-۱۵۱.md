---
title: >-
    غزل شمارهٔ  ۱۵۱
---
# غزل شمارهٔ  ۱۵۱

<div class="b" id="bn1"><div class="m1"><p>گر جان طلبی فدای جانت</p></div>
<div class="m2"><p>سهلست جواب امتحانت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سوگند به جانت ار فروشم</p></div>
<div class="m2"><p>یک موی به هر که در جهانت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با آن که تو مهر کس نداری</p></div>
<div class="m2"><p>کس نیست که نیست مهربانت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>وین سر که تو داری ای ستمکار</p></div>
<div class="m2"><p>بس سر برود بر آستانت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بس فتنه که در زمین به پا شد</p></div>
<div class="m2"><p>از روی چو ماه آسمانت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>من در تو رسم به جهد؟ هیهات</p></div>
<div class="m2"><p>کز باد سَبَق برد عنانت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بی یاد تو نیستم زمانی</p></div>
<div class="m2"><p>تا یاد کنم دگر زمانت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کوته نظران کنند و حیفست</p></div>
<div class="m2"><p>تشبیه به سرو بوستانت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>و ابرو که تو داری ای پری زاد</p></div>
<div class="m2"><p>در صید چه حاجت کمانت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گویی بدن ضعیف سعدی</p></div>
<div class="m2"><p>نقشیست گرفته از میانت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گر واسطهٔ سخن نبودی</p></div>
<div class="m2"><p>در وهم نیامدی دهانت</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>شیرینتر از این سخن نباشد</p></div>
<div class="m2"><p>الا دهن شکر فشانت</p></div></div>