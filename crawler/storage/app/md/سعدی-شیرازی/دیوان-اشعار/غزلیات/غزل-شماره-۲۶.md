---
title: >-
    غزل شمارهٔ  ۲۶
---
# غزل شمارهٔ  ۲۶

<div class="b" id="bn1"><div class="m1"><p>ما را همه شب نمی‌برد خواب</p></div>
<div class="m2"><p>ای خفته روزگار دریاب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در بادیه تشنگان بمردند</p></div>
<div class="m2"><p>وز حله به کوفه می‌رود آب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای سخت کمان سست پیمان</p></div>
<div class="m2"><p>این بود وفای عهد اصحاب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خار است به زیر پهلوانم</p></div>
<div class="m2"><p>بی روی تو خوابگاه سنجاب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای دیده عاشقان به رویت</p></div>
<div class="m2"><p>چون روی مجاوران به محراب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>من تن به قضای عشق دادم</p></div>
<div class="m2"><p>پیرانه سر آمدم به کُتّاب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زهر از کف دست نازنینان</p></div>
<div class="m2"><p>در حلق چنان رود که جُلّاب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دیوانه کوی خوبرویان</p></div>
<div class="m2"><p>دردش نکند جفای بواب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سعدی نتوان به هیچ کشتن</p></div>
<div class="m2"><p>اِلّا به فراق روی احباب</p></div></div>