---
title: >-
    غزل شمارهٔ  ۵۴۳
---
# غزل شمارهٔ  ۵۴۳

<div class="b" id="bn1"><div class="m1"><p>ای برق اگر به گوشه آن بام بگذری</p></div>
<div class="m2"><p>آنجا که باد زهره ندارد خبر بری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای مرغ اگر پری به سر کوی آن صنم</p></div>
<div class="m2"><p>پیغام دوستان برسانی بدان پری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آن مشتری خصال گر از ما حکایتی</p></div>
<div class="m2"><p>پرسد جواب ده که به جانند مشتری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گو تشنگان بادیه را جان به لب رسید</p></div>
<div class="m2"><p>تو خفته در کجاوه به خواب خوش اندری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای ماهروی حاضر غایب که پیش دل</p></div>
<div class="m2"><p>یک روز نگذرد که تو صد بار نگذری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دانی چه می‌رود به سر ما ز دست تو</p></div>
<div class="m2"><p>تا خود به پای خویش بیایی و بنگری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بازآی کز صبوری و دوری بسوختیم</p></div>
<div class="m2"><p>ای غایب از نظر که به معنی برابری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>یا دل به ما دهی چو دل ما به دست توست</p></div>
<div class="m2"><p>یا مهر خویشتن ز دل ما به در بری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تا خود برون پرده حکایت کجا رسد</p></div>
<div class="m2"><p>چون از درون پرده چنین پرده می‌دری</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سعدی تو کیستی که دم دوستی زنی</p></div>
<div class="m2"><p>دعوی بندگی کن و اقرار چاکری</p></div></div>