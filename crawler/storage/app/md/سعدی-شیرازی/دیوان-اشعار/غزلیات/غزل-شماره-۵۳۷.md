---
title: >-
    غزل شمارهٔ  ۵۳۷
---
# غزل شمارهٔ  ۵۳۷

<div class="b" id="bn1"><div class="m1"><p>چه باز در دلت آمد که مهر برکندی</p></div>
<div class="m2"><p>چه شد که یار قدیم از نظر بیفکندی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز حد گذشت جدایی میان ما ای دوست</p></div>
<div class="m2"><p>هنوز وقت نیامد که بازپیوندی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بود که پیش تو میرم اگر مجال بود</p></div>
<div class="m2"><p>و گر نه بر سر کویت به آرزومندی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دری به روی من ای یار مهربان بگشای</p></div>
<div class="m2"><p>که هیچ کس نگشاید اگر تو در بندی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مرا و گر همه آفاق خوبرویانند</p></div>
<div class="m2"><p>به هیچ روی نمی‌باشد از تو خرسندی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هزار بار بگفتم که چشم نگشایم</p></div>
<div class="m2"><p>به روی خوب ولیکن تو چشم می‌بندی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مگر در آینه بینی و گر نه در آفاق</p></div>
<div class="m2"><p>به هیچ خلق نپندارمت که مانندی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>حدیث سعدی اگر کائنات بپسندند</p></div>
<div class="m2"><p>به هیچ کار نیاید گرش تو نپسندی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مرا چه بندگی از دست و پای برخیزد</p></div>
<div class="m2"><p>مگر امید به بخشایش خداوندی</p></div></div>