---
title: >-
    غزل شمارهٔ  ۳۶
---
# غزل شمارهٔ  ۳۶

<div class="b" id="bn1"><div class="m1"><p>بنده وار آمدم به زنهارت</p></div>
<div class="m2"><p>که ندارم سلاح پیکارت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>متفق می‌شوم که دل ندهم</p></div>
<div class="m2"><p>معتقد می‌شوم دگربارت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مشتری را بهای روی تو نیست</p></div>
<div class="m2"><p>من بدین مفلسی خریدارت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>غیرتم هست و اقتدارم نیست</p></div>
<div class="m2"><p>که بپوشم ز چشم اغیارت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر چه بی طاقتم چو مور ضعیف</p></div>
<div class="m2"><p>می‌کشم نفس و می‌کشم بارت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نه چنان در کمند پیچیدی</p></div>
<div class="m2"><p>که مخلص شود گرفتارت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>من هم اول که دیدمت گفتم</p></div>
<div class="m2"><p>حذر از چشم مست خون خوارت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دیده شاید که بی تو برنکند</p></div>
<div class="m2"><p>تا نبیند فراق دیدارت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تو ملولی و دوستان مشتاق</p></div>
<div class="m2"><p>تو گریزان و ما طلبکارت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چشم سعدی به خواب بیند خواب</p></div>
<div class="m2"><p>که ببستی به چشم سحارت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تو بدین هر دو چشم خواب آلود</p></div>
<div class="m2"><p>چه غم از چشم‌های بیدارت</p></div></div>