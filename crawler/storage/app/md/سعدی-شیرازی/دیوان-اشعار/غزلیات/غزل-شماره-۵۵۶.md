---
title: >-
    غزل شمارهٔ  ۵۵۶
---
# غزل شمارهٔ  ۵۵۶

<div class="b" id="bn1"><div class="m1"><p>گر کنم در سر وفات سری</p></div>
<div class="m2"><p>سهل باشد زیان مختصری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای که قصد هلاک من داری</p></div>
<div class="m2"><p>صبر کن تا ببینمت نظری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نه حرام است در رخ تو نظر</p></div>
<div class="m2"><p>که حرام است چشم بر دگری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دوست دارم که خاک پات شوم</p></div>
<div class="m2"><p>تا مگر بر سرم کنی گذری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>متحیر نه در جمال توام</p></div>
<div class="m2"><p>عقل دارم به قدر خود قدری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حیرتم در صفات بی چون است</p></div>
<div class="m2"><p>کاین کمال آفرید در بشری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ببری هوش و طاقت زن و مرد</p></div>
<div class="m2"><p>گر تردد کنی به بام و دری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>حق به دست رقیب ناهموار</p></div>
<div class="m2"><p>پیش خصم ایستاده چون سپری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زان که آیینه‌ای بدین خوبی</p></div>
<div class="m2"><p>حیف باشد به دست بی بصری</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آه سعدی اثر کند در کوه</p></div>
<div class="m2"><p>نکند در تو سنگدل اثری</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سنگ را سخت گفتمی همه عمر</p></div>
<div class="m2"><p>تا بدیدم ز سنگ سختتری</p></div></div>