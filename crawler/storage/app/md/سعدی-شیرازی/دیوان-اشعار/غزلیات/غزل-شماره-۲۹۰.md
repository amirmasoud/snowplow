---
title: >-
    غزل شمارهٔ  ۲۹۰
---
# غزل شمارهٔ  ۲۹۰

<div class="b" id="bn1"><div class="m1"><p>تو را سریست که با ما فرو نمی‌آید</p></div>
<div class="m2"><p>مرا دلی که صبوری از او نمی‌آید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کدام دیده به روی تو باز شد همه عمر</p></div>
<div class="m2"><p>که آب دیده به رویش فرو نمی‌آید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جز این قدر نتوان گفت بر جمال تو عیب</p></div>
<div class="m2"><p>که مهربانی از آن طبع و خو نمی‌آید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چه جور کز خم چوگان زلف مشکینت</p></div>
<div class="m2"><p>بر اوفتاده مسکین چو گو نمی‌آید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگر هزار گزند آید از تو بر دل ریش</p></div>
<div class="m2"><p>بد از منست که گویم نکو نمی‌آید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر از حدیث تو کوته کنم زبان امید</p></div>
<div class="m2"><p>که هیچ حاصل از این گفت و گو نمی‌آید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گمان برند که در عودسوز سینه من</p></div>
<div class="m2"><p>بمرد آتش معنی که بو نمی‌آید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چه عاشقست که فریاد دردناکش نیست</p></div>
<div class="m2"><p>چه مجلسست کز او های و هو نمی‌آید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به شیر بود مگر شور عشق سعدی را</p></div>
<div class="m2"><p>که پیر گشت و تغیر در او نمی‌آید</p></div></div>