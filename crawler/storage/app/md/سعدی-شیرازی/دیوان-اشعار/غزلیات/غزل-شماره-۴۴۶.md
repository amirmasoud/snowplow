---
title: >-
    غزل شمارهٔ  ۴۴۶
---
# غزل شمارهٔ  ۴۴۶

<div class="b" id="bn1"><div class="m1"><p>ای کودک خوبروی حیران</p></div>
<div class="m2"><p>در وصف شمایلت سخندان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صبر از همه چیز و هر که عالم</p></div>
<div class="m2"><p>کردیم و صبوری از تو نتوان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دیدی که وفا به سر نبردی</p></div>
<div class="m2"><p>ای سخت کمان سست پیمان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پایان فراق ناپدیدار</p></div>
<div class="m2"><p>و امید نمی‌رسد به پایان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هرگز نشنیده‌ام که کرده‌ست</p></div>
<div class="m2"><p>سرو آنچه تو می‌کنی به جولان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>باور که کند که آدمی را</p></div>
<div class="m2"><p>خورشید برآید از گریبان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بیمار فراق به نباشد</p></div>
<div class="m2"><p>تا بو نکند به زنخدان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>وین گوی سعادت است و دولت</p></div>
<div class="m2"><p>تا با که در افکنی به میدان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ترسم که به عاقبت بماند</p></div>
<div class="m2"><p>در چشم سکندر آب حیوان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دل بود و به دست دلبر افتاد</p></div>
<div class="m2"><p>جان است و فدای روی جانان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>عاقل نکند شکایت از درد</p></div>
<div class="m2"><p>مادام که هست امید درمان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بی مار به سر نمی‌رود گنج</p></div>
<div class="m2"><p>بی خار نمی‌دمد گلستان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گر در نظرت بسوخت سعدی</p></div>
<div class="m2"><p>مه را چه غم از هلاک کتان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>پروانه بکشت خویشتن را</p></div>
<div class="m2"><p>بر شمع چه لازم است تاوان</p></div></div>