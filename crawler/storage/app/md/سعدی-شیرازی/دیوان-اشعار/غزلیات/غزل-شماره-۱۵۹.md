---
title: >-
    غزل شمارهٔ  ۱۵۹
---
# غزل شمارهٔ  ۱۵۹

<div class="b" id="bn1"><div class="m1"><p>نه آن شبست که کس در میان ما گنجد</p></div>
<div class="m2"><p>به خاک پایت اگر ذره در هوا گنجد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کلاه ناز و تکبر بنه کمر بگشای</p></div>
<div class="m2"><p>که چون تو سرو ندیدم که در قبا گنجد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز من حکایت هجران مپرس در شب وصل</p></div>
<div class="m2"><p>عتاب کیست که در خلوت رضا گنجد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرا شکر منه و گل مریز در مجلس</p></div>
<div class="m2"><p>میان خسرو و شیرین شکر کجا گنجد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو شور عشق درآمد قرار عقل نماند</p></div>
<div class="m2"><p>درون مملکتی چون دو پادشا گنجد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نماند در سر سعدی ز بانگ رود و سرود</p></div>
<div class="m2"><p>مجال آن که دگر پند پارسا گنجد</p></div></div>