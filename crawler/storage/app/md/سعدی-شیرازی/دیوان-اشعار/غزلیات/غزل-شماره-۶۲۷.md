---
title: >-
    غزل شمارهٔ  ۶۲۷
---
# غزل شمارهٔ  ۶۲۷

<div class="b" id="bn1"><div class="m1"><p>خواهم اندر پایش افتادن چو گوی</p></div>
<div class="m2"><p>ور به چوگانم زند هیچش مگوی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر سر عشاق طوفان گو ببار</p></div>
<div class="m2"><p>در ره مشتاق پیکان گو بروی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر به داغت می‌کند فرمان ببر</p></div>
<div class="m2"><p>ور به دردت می‌کشد درمان مجوی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ناودان چشم رنجوران عشق</p></div>
<div class="m2"><p>گر فرو ریزند خون آید به جوی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شاد باش ای مجلس روحانیان</p></div>
<div class="m2"><p>تا که خورد این می که من مستم به بوی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر که سودانامه سعدی نبشت</p></div>
<div class="m2"><p>دفتر پرهیزگاری گو بشوی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر که نشنیده‌ست وقتی بوی عشق</p></div>
<div class="m2"><p>گو به شیراز آی و خاک من ببوی</p></div></div>