---
title: >-
    غزل شمارهٔ  ۲۶۳
---
# غزل شمارهٔ  ۲۶۳

<div class="b" id="bn1"><div class="m1"><p>گفتمش سیر ببینم مگر از دل برود</p></div>
<div class="m2"><p>وآن چنان پای گرفته‌ست که مشکل برود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دلی از سنگ بباید به سر راه وداع</p></div>
<div class="m2"><p>تا تحمل کند آن روز که محمل برود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چشم حسرت به سر اشک فرو می‌گیرم</p></div>
<div class="m2"><p>که اگر راه دهم قافله بر گل برود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ره ندیدم چو برفت از نظرم صورت دوست</p></div>
<div class="m2"><p>همچو چشمی که چراغش ز مقابل برود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>موج از این بار چنان کشتی طاقت بشکست</p></div>
<div class="m2"><p>که عجب دارم اگر تخته به ساحل برود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سهل بود آن که به شمشیر عتابم می‌کشت</p></div>
<div class="m2"><p>قتل صاحب نظر آن است که قاتل برود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نه عجب گر برود قاعده صبر و شکیب</p></div>
<div class="m2"><p>پیش هر چشم که آن قد و شمایل برود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کس ندانم که در این شهر گرفتار تو نیست</p></div>
<div class="m2"><p>مگر آن کس که به شهر آید و غافل برود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر همه عمر نداده‌ست کسی دل به خیال</p></div>
<div class="m2"><p>چون بیاید به سر راه تو بی‌دل برود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>روی بنمای که صبر از دل صوفی ببری</p></div>
<div class="m2"><p>پرده بردار که هوش از تن عاقل برود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سعدی ار عشق نبازد چه کند ملک وجود</p></div>
<div class="m2"><p>حیف باشد که همه عمر به باطل برود</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>قیمت وصل نداند مگر آزرده هجر</p></div>
<div class="m2"><p>مانده آسوده بخسبد چو به منزل برود</p></div></div>