---
title: >-
    غزل شمارهٔ  ۲۱۳
---
# غزل شمارهٔ  ۲۱۳

<div class="b" id="bn1"><div class="m1"><p>دوش بی روی تو آتش به سرم بر می‌شد</p></div>
<div class="m2"><p>و آبی از دیده می‌آمد که زمین تر می‌شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا به افسوس به پایان نرود عمر عزیز</p></div>
<div class="m2"><p>همه شب ذکر تو می‌رفت و مکرر می‌شد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون شب آمد همه را دیده بیارامد و من</p></div>
<div class="m2"><p>گفتی اندر بن مویم سر نشتر می‌شد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آن نه می بود که دور از نظرت می‌خوردم</p></div>
<div class="m2"><p>خون دل بود که از دیده به ساغر می‌شد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از خیال تو به هر سو که نظر می‌کردم</p></div>
<div class="m2"><p>پیش چشمم در و دیوار مصور می‌شد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چشم مجنون چو بخفتی همه لیلی دیدی</p></div>
<div class="m2"><p>مدعی بود اگرش خواب میسر می‌شد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هوش می‌آمد و می‌رفت و نه دیدار تو را</p></div>
<div class="m2"><p>می‌بدیدم نه خیالم ز برابر می‌شد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گاه چون عود بر آتش دل تنگم می‌سوخت</p></div>
<div class="m2"><p>گاه چون مجمره‌ام دود به سر بر می‌شد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گویی آن صبح کجا رفت که شب‌های دگر</p></div>
<div class="m2"><p>نفسی می‌زد و آفاق منور می‌شد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سعدیا عقد ثریا مگر امشب بگسیخت</p></div>
<div class="m2"><p>ور نه هر شب به گریبان افق بر می‌شد</p></div></div>