---
title: >-
    غزل شمارهٔ  ۱۹۵
---
# غزل شمارهٔ  ۱۹۵

<div class="b" id="bn1"><div class="m1"><p>از تو دل برنکنم تا دل و جانم باشد</p></div>
<div class="m2"><p>می‌برم جور تو تا وسع و توانم باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر نوازی چه سعادت به از این خواهم یافت</p></div>
<div class="m2"><p>ور کشی زار چه دولت به از آنم باشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون مرا عشق تو از هر چه جهان بازاستد</p></div>
<div class="m2"><p>چه غم از سرزنش هر که جهانم باشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تیغ قهر ار تو زنی قوت روحم گردد</p></div>
<div class="m2"><p>جام زهر ار تو دهی قوت روانم باشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در قیامت چو سر از خاک لحد بردارم</p></div>
<div class="m2"><p>گرد سودای تو بر دامن جانم باشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر تو را خاطر ما نیست خیالت بفرست</p></div>
<div class="m2"><p>تا شبی محرم اسرار نهانم باشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر کسی را ز لبت خشک تمنایی هست</p></div>
<div class="m2"><p>من خود این بخت ندارم که زبانم باشد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جان برافشانم اگر سعدی خویشم خوانی</p></div>
<div class="m2"><p>سر این دارم اگر طالع آنم باشد</p></div></div>