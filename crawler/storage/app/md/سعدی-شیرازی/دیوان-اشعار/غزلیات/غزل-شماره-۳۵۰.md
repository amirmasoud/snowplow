---
title: >-
    غزل شمارهٔ  ۳۵۰
---
# غزل شمارهٔ  ۳۵۰

<div class="b" id="bn1"><div class="m1"><p>من ایستاده‌ام اینک به خدمتت مشغول</p></div>
<div class="m2"><p>مرا از آن چه که خدمت قبول یا نه قبول</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نه دست با تو درآویختن نه پای گریز</p></div>
<div class="m2"><p>نه احتمال فراق و نه اختیار وصول</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کمند عشق نه بس بود زلف مفتولت</p></div>
<div class="m2"><p>که روی نیز بکردی ز دوستان مفتول</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>من آنم ار تو نه آنی که بودی اندر عهد</p></div>
<div class="m2"><p>به دوستی که نکردم ز دوستیت عدول</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ملامتت نکنم گر چه بی‌وفا یاری</p></div>
<div class="m2"><p>هزار جان عزیزت فدای طبع ملول</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مرا گناه خود است ار ملامت تو برم</p></div>
<div class="m2"><p>که عشق بار گران بود و من ظلوم جهول</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر آن چه بر سر من می‌رود ز دست فراق</p></div>
<div class="m2"><p>علی التمام فروخوانم الحدیث یطول</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز دست گریه کتابت نمی‌توانم کرد</p></div>
<div class="m2"><p>که می‌نویسم و در حال می‌شود مغسول</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>من از کجا و نصیحت کنان بیهده گوی</p></div>
<div class="m2"><p>حکیم را نرسد کدخدایی بهلول</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>طریق عشق به گفتن نمی‌توان آموخت</p></div>
<div class="m2"><p>مگر کسی که بود در طبیعتش مجبول</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>اسیر بند غمت را به لطف خویش بخوان</p></div>
<div class="m2"><p>که گر به قهر برانی کجا شود مغلول</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نه زور بازوی سعدی که دست قوت شیر</p></div>
<div class="m2"><p>سپر بیفکند از تیغ غمزه مسلول</p></div></div>