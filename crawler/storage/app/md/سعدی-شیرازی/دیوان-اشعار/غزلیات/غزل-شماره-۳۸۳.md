---
title: >-
    غزل شمارهٔ  ۳۸۳
---
# غزل شمارهٔ  ۳۸۳

<div class="b" id="bn1"><div class="m1"><p>می‌روم وز سر حسرت به قفا می‌نگرم</p></div>
<div class="m2"><p>خبر از پای ندارم که زمین می‌سپرم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می‌روم بی‌دل و بی یار و یقین می‌دانم</p></div>
<div class="m2"><p>که من بی‌دل بی یار نه مرد سفرم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خاک من زنده به تأثیر هوای لب توست</p></div>
<div class="m2"><p>سازگاری نکند آب و هوای دگرم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>وه که گر بر سر کوی تو شبی روز کنم</p></div>
<div class="m2"><p>غلغل اندر ملکوت افتد از آه سحرم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پای می‌پیچم و چون پای دلم می‌پیچد</p></div>
<div class="m2"><p>بار می‌بندم و از بار فروبسته‌ترم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چه کنم دست ندارم به گریبان اجل</p></div>
<div class="m2"><p>تا به تن در ز غمت پیرهن جان بدرم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آتش خشم تو برد آب من خاک آلود</p></div>
<div class="m2"><p>بعد از این باد به گوش تو رساند خبرم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هر نوردی که ز طومار غمم باز کنی</p></div>
<div class="m2"><p>حرف‌ها بینی آلوده به خون جگرم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نی مپندار که حرفی به زبان آرم اگر</p></div>
<div class="m2"><p>تا به سینه چو قلم بازشکافند سرم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به هوای سر زلف تو درآویخته بود</p></div>
<div class="m2"><p>از سر شاخ زبان برگ سخن‌های ترم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گر سخن گویم من بعد شکایت باشد</p></div>
<div class="m2"><p>ور شکایت کنم از دست تو پیش که برم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>خار سودای تو آویخته در دامن دل</p></div>
<div class="m2"><p>ننگم آید که به اطراف گلستان گذرم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بصر روشنم از سرمه خاک در توست</p></div>
<div class="m2"><p>قیمت خاک تو من دانم کاهل بصرم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گر چه در کلبه خلوت بودم نور حضور</p></div>
<div class="m2"><p>هم سفر به که نماندست مجال حضرم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>سرو بالای تو در باغ تصور برپای</p></div>
<div class="m2"><p>شرم دارم که به بالای صنوبر نگرم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گر به تن بازکنم جای دگر باکی نیست</p></div>
<div class="m2"><p>که به دل غاشیه بر سر به رکاب تو درم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>گر به دوری سفر از تو جدا خواهم ماند</p></div>
<div class="m2"><p>شرم بادم که همان سعدی کوته نظرم</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>به قدم رفتم و ناچار به سر بازآیم</p></div>
<div class="m2"><p>گر به دامن نرسد چنگ قضا و قدرم</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>شوخ چشمی چو مگس کردم و برداشت عدو</p></div>
<div class="m2"><p>به مگسران ملامت ز کنار شکرم</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>از قفا سیر نگشتم من بدبخت هنوز</p></div>
<div class="m2"><p>می‌روم وز سر حسرت به قفا می‌نگرم</p></div></div>