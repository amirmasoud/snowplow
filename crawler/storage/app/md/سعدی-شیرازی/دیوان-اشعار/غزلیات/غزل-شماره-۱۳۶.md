---
title: >-
    غزل شمارهٔ  ۱۳۶
---
# غزل شمارهٔ  ۱۳۶

<div class="b" id="bn1"><div class="m1"><p>ای دیدنت آسایش و خندیدنت آفت</p></div>
<div class="m2"><p>گوی از همه خوبان بربودی به لطافت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای صورت دیبای خطایی به نکویی</p></div>
<div class="m2"><p>وی قطره باران بهاری به نظافت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر ملک وجودی که به شوخی بگرفتی</p></div>
<div class="m2"><p>سلطان خیالت بنشاندی به خلافت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای سرو خرامان گذری از در رحمت</p></div>
<div class="m2"><p>وی ماه درفشان نظری از سر رأفت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گویند برو تا برود صحبتت از دل</p></div>
<div class="m2"><p>ترسم هوسم بیش کند بعد مسافت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای عقل نگفتم که تو در عشق نگنجی</p></div>
<div class="m2"><p>در دولت خاقان نتوان کرد خلافت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>با قد تو زیبا نبود سرو به نسبت</p></div>
<div class="m2"><p>با روی تو نیکو نبود مه به اضافت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آن را که دلارام دهد وعده کشتن</p></div>
<div class="m2"><p>باید که ز مرگش نبود هیچ مخافت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>صد سفره دشمن بنهد طالب مقصود</p></div>
<div class="m2"><p>باشد که یکی دوست بیاید به ضیافت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شمشیر ظرافت بود از دست عزیزان</p></div>
<div class="m2"><p>درویش نباید که برنجد به ظرافت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سعدی چو گرفتار شدی تن به قضا ده</p></div>
<div class="m2"><p>دریا در و مرجان بود و هول و مخافت</p></div></div>