---
title: >-
    غزل شمارهٔ  ۲۹۹
---
# غزل شمارهٔ  ۲۹۹

<div class="b" id="bn1"><div class="m1"><p>ای صبر پای دار که پیمان شکست یار</p></div>
<div class="m2"><p>کارم ز دست رفت و نیامد به دست یار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>برخاست آهم از دل و در خون نشست چشم</p></div>
<div class="m2"><p>یا رب ز من چه خاست که بی من نشست یار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در عشق یار نیست مرا صبر و سیم و زر</p></div>
<div class="m2"><p>لیک آب چشم و آتش دل هر دو هست یار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون قامتم کمان صفت از غم خمیده دید</p></div>
<div class="m2"><p>چون تیر ناگهان ز کنارم بجست یار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سعدی به بندگیش کمر بسته‌ای ولیک</p></div>
<div class="m2"><p>منت منه که طرفی از این برنبست یار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اکنون که بی‌وفایی یارت درست شد</p></div>
<div class="m2"><p>در دل شکن امید که پیمان شکست یار</p></div></div>