---
title: >-
    غزل شمارهٔ  ۵۵
---
# غزل شمارهٔ  ۵۵

<div class="b" id="bn1"><div class="m1"><p>مجنون عشق را دگر امروز حالت است</p></div>
<div class="m2"><p>کاسلام دین لیلی و دیگر ضلالت است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فرهاد را از آن چه که شیرین ترش کند</p></div>
<div class="m2"><p>این را شکیب نیست گر آن را ملالت است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عذرا که نانوشته بخواند حدیث عشق</p></div>
<div class="m2"><p>داند که آب دیدهٔ وامق رسالت است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مطرب همین طریق غزل گو نگاه دار</p></div>
<div class="m2"><p>کاین ره که برگرفت به جایی دلالت است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای مدعی که می‌گذری بر کنار آب</p></div>
<div class="m2"><p>ما را که غرقه‌ایم ندانی چه حالت است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زین در کجا رویم که ما را به خاک او</p></div>
<div class="m2"><p>واو را به خون ما که بریزد حوالت است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر سر قدم نمی‌کنمش پیش اهل دل</p></div>
<div class="m2"><p>سر بر نمی‌کنم که مقام خجالت است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جز یاد دوست هر چه کنی عمر ضایع است</p></div>
<div class="m2"><p>جز سر عشق هر چه بگویی بطالت است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ما را دگر معامله با هیچکس نماند</p></div>
<div class="m2"><p>بیعی که بی حضور تو کردم اقالت است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از هر جفات بوی وفایی همی‌دهد</p></div>
<div class="m2"><p>در هر تعنتیت هزار استمالت است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سعدی بشوی لوح دل از نقش غیر او</p></div>
<div class="m2"><p>علمی که ره به حق ننماید جهالت است</p></div></div>