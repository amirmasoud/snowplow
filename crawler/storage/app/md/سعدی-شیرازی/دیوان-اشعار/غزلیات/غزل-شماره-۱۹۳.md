---
title: >-
    غزل شمارهٔ  ۱۹۳
---
# غزل شمارهٔ  ۱۹۳

<div class="b" id="bn1"><div class="m1"><p>شورش بلبلان سحر باشد</p></div>
<div class="m2"><p>خفته از صبح بی‌خبر باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تیرباران عشق خوبان را</p></div>
<div class="m2"><p>دل شوریدگان سپر باشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عاشقان کشتگان معشوقند</p></div>
<div class="m2"><p>هر که زنده‌ست در خطر باشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همه عالم جمال طلعت اوست</p></div>
<div class="m2"><p>تا که را چشم این نظر باشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کس ندانم که دل بدو ندهد</p></div>
<div class="m2"><p>مگر آن کس که بی بصر باشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آدمی را که خارکی در پای</p></div>
<div class="m2"><p>نرود طرفه جانور باشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گو ترش روی باش و تلخ سخن</p></div>
<div class="m2"><p>زهر شیرین لبان شکر باشد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عاقلان از بلا بپرهیزند</p></div>
<div class="m2"><p>مذهب عاشقان دگر باشد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>پای رفتن نماند سعدی را</p></div>
<div class="m2"><p>مرغ عاشق بریده پر باشد</p></div></div>