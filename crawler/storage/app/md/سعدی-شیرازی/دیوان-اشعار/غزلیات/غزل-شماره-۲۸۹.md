---
title: >-
    غزل شمارهٔ  ۲۸۹
---
# غزل شمارهٔ  ۲۸۹

<div class="b" id="bn1"><div class="m1"><p>آن نه عشق است که از دل به دهان می‌آید</p></div>
<div class="m2"><p>وان نه عاشق که ز معشوق به جان می‌آید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گو برو در پس زانوی سلامت بنشین</p></div>
<div class="m2"><p>آن که از دست ملامت به فغان می‌آید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کشتی هر که در این ورطه خونخوار افتاد</p></div>
<div class="m2"><p>نشنیدیم که دیگر به کران می‌آید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یا مسافر که در این بادیه سرگردان شد</p></div>
<div class="m2"><p>دیگر از وی خبر و نام و نشان می‌آید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چشم رغبت که به دیدار کسی کردی باز</p></div>
<div class="m2"><p>باز بر هم منه ار تیر و سنان می‌آید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عاشق آن است که بی خویشتن از ذوق سماع</p></div>
<div class="m2"><p>پیش شمشیر بلا رقص‌کنان می‌آید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>حاش لله که من از تیر بگردانم روی</p></div>
<div class="m2"><p>گر بدانم که از آن دست و کمان می‌آید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کشته بینند و مقاتل نشناسند که کیست</p></div>
<div class="m2"><p>کاین خدنگ از نظر خلق نهان می‌آید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اندرون با تو چنان انس گرفته‌ست مرا</p></div>
<div class="m2"><p>که ملالم ز همه خلق جهان می‌آید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شرط عشق است که از دوست شکایت نکنند</p></div>
<div class="m2"><p>لیکن از شوق حکایت به زبان می‌آید</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سعدیا این همه فریاد تو بی دردی نیست</p></div>
<div class="m2"><p>آتشی هست که دود از سر آن می‌آید</p></div></div>