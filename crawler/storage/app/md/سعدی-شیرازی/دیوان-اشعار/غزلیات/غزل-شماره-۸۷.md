---
title: >-
    غزل شمارهٔ  ۸۷
---
# غزل شمارهٔ  ۸۷

<div class="b" id="bn1"><div class="m1"><p>گر کسی سرو شنیده‌ست که رفته‌ست این است</p></div>
<div class="m2"><p>یا صنوبر که بناگوش و برش سیمین است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نه بلندیست به صورت که تو معلوم کنی</p></div>
<div class="m2"><p>که بلند از نظر مردم کوته‌بین است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خواب در عهد تو در چشم من آید هیهات</p></div>
<div class="m2"><p>عاشقی کار سری نیست که بر بالین است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همه آرام گرفتند و شب از نیمه گذشت</p></div>
<div class="m2"><p>وآنچه در خواب نشد چشم من و پروین است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خود گرفتم که نظر بر رخ خوبان کفر است</p></div>
<div class="m2"><p>من از این بازنگردم که مرا این دین است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>وقت آن است که مردم ره صحرا گیرند</p></div>
<div class="m2"><p>خاصه اکنون که بهار آمد و فروردین است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چمن امروز بهشت است و تو در می‌بایی</p></div>
<div class="m2"><p>تا خلایق همه گویند که حورالعین است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هر چه گفتیم در اوصاف کمالیت او</p></div>
<div class="m2"><p>همچنان هیچ نگفتیم که صد چندین است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آنچه سرپنجهٔ سیمین تو با سعدی کرد</p></div>
<div class="m2"><p>با کبوتر نکند پنجه که با شاهین است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>من دگر شعر نخواهم که نویسم که مگس</p></div>
<div class="m2"><p>زحمتم می‌دهد از بس که سخن شیرین است</p></div></div>