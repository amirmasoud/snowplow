---
title: >-
    غزل شمارهٔ  ۳۰۸
---
# غزل شمارهٔ  ۳۰۸

<div class="b" id="bn1"><div class="m1"><p>فتنه‌ام بر زلف و بالای تو ای بدر منیر</p></div>
<div class="m2"><p>قامت است آن یا قیامت عنبر است آن یا عبیر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گم شدم در راه سودا رهنمایا ره نمای</p></div>
<div class="m2"><p>شخصم از پای اندر آمد دستگیرا دست گیر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر ز پیش خود برانی چون سگ از مسجد مرا</p></div>
<div class="m2"><p>سر ز حکمت برندارم چون مرید از گفت پیر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ناوک فریاد من هر ساعت از مجرای دل</p></div>
<div class="m2"><p>بگذرد از چرخ اطلس همچو سوزن از حریر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون کنم کز دل شکیبایم ز دلبر ناشکیب</p></div>
<div class="m2"><p>چون کنم کز جان گزیر است و ز جانان ناگزیر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بی تو گر در جنتم ناخوش شراب سلسبیل</p></div>
<div class="m2"><p>با تو گر در دوزخم خرم هوای زمهریر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر بپرد مرغ وصلت در هوای بخت من</p></div>
<div class="m2"><p>وه که آن ساعت ز شادی چارپر گردم چو تیر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا روانم هست خواهم راند نامت بر زبان</p></div>
<div class="m2"><p>تا وجودم هست خواهم کند نقشت در ضمیر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر نبارد فضل باران عنایت بر سرم</p></div>
<div class="m2"><p>لابه بر گردون رسانم چون جهودان در فطیر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بوالعجب شوریده‌ام سهوم به رحمت درگذار</p></div>
<div class="m2"><p>سهمگن درمانده‌ام جرمم به طاعت درپذیر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آه دردآلود سعدی گر ز گردون بگذرد</p></div>
<div class="m2"><p>در تو کافردل نگیرد ای مسلمانان نفیر</p></div></div>