---
title: >-
    غزل شمارهٔ  ۵۱۹
---
# غزل شمارهٔ  ۵۱۹

<div class="b" id="bn1"><div class="m1"><p>سر آن ندارد امشب که برآید آفتابی</p></div>
<div class="m2"><p>چه خیال‌ها گذر کرد و گذر نکرد خوابی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به چه دیر ماندی ای صبح که جان من برآمد</p></div>
<div class="m2"><p>بزه کردی و نکردند مؤذنان ثوابی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نفس خروس بگرفت که نوبتی بخواند</p></div>
<div class="m2"><p>همه بلبلان بمردند و نماند جز غرابی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نفحات صبح دانی ز چه روی دوست دارم</p></div>
<div class="m2"><p>که به روی دوست ماند که برافکند نقابی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سرم از خدای خواهد که به پایش اندر افتد</p></div>
<div class="m2"><p>که در آب مرده بهتر که در آرزوی آبی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دل من نه مرد آنست که با غمش برآید</p></div>
<div class="m2"><p>مگسی کجا تواند که بیفکند عقابی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نه چنان گناهکارم که به دشمنم سپاری</p></div>
<div class="m2"><p>تو به دست خویش فرمای اگرم کنی عذابی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دل همچو سنگت ای دوست به آب چشم سعدی</p></div>
<div class="m2"><p>عجب است اگر نگردد که بگردد آسیابی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>برو ای گدای مسکین و دری دگر طلب کن</p></div>
<div class="m2"><p>که هزار بار گفتی و نیامدت جوابی</p></div></div>