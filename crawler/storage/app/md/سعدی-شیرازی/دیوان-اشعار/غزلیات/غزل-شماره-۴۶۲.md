---
title: >-
    غزل شمارهٔ  ۴۶۲
---
# غزل شمارهٔ  ۴۶۲

<div class="b" id="bn1"><div class="m1"><p>طوطی نگوید از تو دلاویزتر سخن</p></div>
<div class="m2"><p>با شهد می‌رود ز دهانت به در سخن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر من نگویمت که تو شیرین عالمی</p></div>
<div class="m2"><p>تو خویشتن دلیل بیاری به هر سخن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>واجب بود که بر سخنت آفرین کنند</p></div>
<div class="m2"><p>لیکن مجال گفت نباشد تو در سخن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در هیچ بوستان چو تو سروی نیامده‌ست</p></div>
<div class="m2"><p>بادام چشم و پسته دهان و شکرسخن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هرگز شنیده‌ای ز بن سرو بوی مشک؟</p></div>
<div class="m2"><p>یا گوش کرده‌ای ز دهان قمر سخن؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>انصاف نیست پیش تو گفتن حدیث خویش</p></div>
<div class="m2"><p>من عهد می‌کنم که نگویم دگر سخن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چشمان دلبرت به نظر سحر می‌کنند</p></div>
<div class="m2"><p>من خود چگونه گویمت اندر نظر سخن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ای باد اگر مجال سخن گفتنت بود</p></div>
<div class="m2"><p>در گوش آن ملول بگوی این قدر سخن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>وصفی چنان که لایق حسنت نمی‌رود</p></div>
<div class="m2"><p>آشفته حال را نبود معتبر سخن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در می‌چکد ز منطق سعدی به جای شعر</p></div>
<div class="m2"><p>گر سیم داشتی بنوشتی به زر سخن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دانندش اهل فضل که مسکین غریق بود</p></div>
<div class="m2"><p>هر گه که در سفینه ببینند تر سخن</p></div></div>