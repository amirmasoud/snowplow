---
title: >-
    غزل شمارهٔ  ۴۹۵
---
# غزل شمارهٔ  ۴۹۵

<div class="b" id="bn1"><div class="m1"><p>می‌برزند ز مشرق شمع فلک زبانه</p></div>
<div class="m2"><p>ای ساقی صبوحی درده می شبانه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عقلم بدزد لختی چند اختیار دانش</p></div>
<div class="m2"><p>هوشم ببر زمانی تا کی غم زمانه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر سنگ فتنه بارد فرق منش سپر کن</p></div>
<div class="m2"><p>ور تیر طعنه آید جان منش نشانه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر می به جان دهندت بستان که پیش دانا</p></div>
<div class="m2"><p>ز آب حیات بهتر خاک شرابخانه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آن کوزه بر کفم نه کآب حیات دارد</p></div>
<div class="m2"><p>هم طعم نار دارد هم رنگ ناردانه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>صوفی چگونه گردد گرد شراب صافی</p></div>
<div class="m2"><p>گنجشک را نگنجد عنقا در آشیانه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دیوانگان نترسند از صولت قیامت</p></div>
<div class="m2"><p>بشکیبد اسب چوبین از سیف و تازیانه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>صوفی و کنج خلوت سعدی و طرف صحرا</p></div>
<div class="m2"><p>صاحب هنر نگیرد بر بی هنر بهانه</p></div></div>