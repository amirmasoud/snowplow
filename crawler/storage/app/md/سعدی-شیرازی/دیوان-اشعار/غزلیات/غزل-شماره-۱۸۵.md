---
title: >-
    غزل شمارهٔ  ۱۸۵
---
# غزل شمارهٔ  ۱۸۵

<div class="b" id="bn1"><div class="m1"><p>کسی به عیب من از خویشتن نپردازد</p></div>
<div class="m2"><p>که هر که می‌نگرم با تو عشق می‌بازد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فرشته‌ای تو بدین روشنی نه آدمیی</p></div>
<div class="m2"><p>نه آدمیست که بر تو نظر نیندازد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نه آدمی که اگر آهنین بود شخصی</p></div>
<div class="m2"><p>در آفتاب جمالت چو موم بگدازد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چنین پسر که تویی راحت روان پدر</p></div>
<div class="m2"><p>سزد که مادر گیتی به روی او نازد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کمان چفته ابرو کشیده تا بن گوش</p></div>
<div class="m2"><p>چو لشکری که به دنبال صید می‌تازد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کدام گل که به روی تو ماند اندر باغ</p></div>
<div class="m2"><p>کدام سرو که با قامتت سر افرازد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>درخت میوه مقصود از آن بلندترست</p></div>
<div class="m2"><p>که دست قدرت کوتاه ما بر او یازد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مسلمش نبود عشق یار آتشروی</p></div>
<div class="m2"><p>مگر کسی که چو پروانه سوزد و سازد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مده به دست فراقم پس از وصال چو چنگ</p></div>
<div class="m2"><p>که مطربش بزند بعد از آن که بنوازد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خلاف عهد تو هرگز نیاید از سعدی</p></div>
<div class="m2"><p>دلی که از تو بپرداخت با که پردازد</p></div></div>