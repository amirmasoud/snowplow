---
title: >-
    غزل شمارهٔ  ۵۸۸
---
# غزل شمارهٔ  ۵۸۸

<div class="b" id="bn1"><div class="m1"><p>عمرم به آخر آمد عشقم هنوز باقی</p></div>
<div class="m2"><p>وز می چنان نه مستم کز عشق روی ساقی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یا غایة الامانی قلبی لدیک فانی</p></div>
<div class="m2"><p>شخصی کما ترانی من غایة اشتیاقی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای دردمند مفتون بر خد و خال موزون</p></div>
<div class="m2"><p>قدر وصالش اکنون دانی که در فراقی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یا سعد کیف صرنا فی بلدة هجرنا</p></div>
<div class="m2"><p>من بعد ما سهرنا و الایدی فی العناقی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بعد از عراق جایی خوش نایدم هوایی</p></div>
<div class="m2"><p>مطرب بزن نوایی زان پرده عراقی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خان الزمان عهدی حتی بقیت وحدی</p></div>
<div class="m2"><p>ردوا علی ودی بالله یا رفاقی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در سرو و مه چه گویی ای مجمع نکویی</p></div>
<div class="m2"><p>تو ماه مشکبویی تو سرو سیم ساقی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ان مت فی هواها دعنی امت فداها</p></div>
<div class="m2"><p>یا عاذلی نباها ذرنی و ما الاقی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چند از حدیث آنان خیزید ای جوانان</p></div>
<div class="m2"><p>تا در هوای جانان بازیم عمر باقی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>قام الغیاث لما زم الجمال زما</p></div>
<div class="m2"><p>و اللیل مدلهما و الدمع فی المآقی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تا در میان نیاری بیگانه‌ای نه یاری</p></div>
<div class="m2"><p>درباز هر چه داری گر مرد اتفاقی</p></div></div>