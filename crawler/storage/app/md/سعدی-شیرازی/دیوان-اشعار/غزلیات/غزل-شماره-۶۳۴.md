---
title: >-
    غزل شمارهٔ  ۶۳۴
---
# غزل شمارهٔ  ۶۳۴

<div class="b" id="bn1"><div class="m1"><p>ای که به حسن قامتت سرو ندیده‌ام سهی</p></div>
<div class="m2"><p>گر همه دشمنی کنی از همه دوستان بهی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جور بکن که حاکمان جور کنند بر رهی</p></div>
<div class="m2"><p>شیر که پایبند شد تن بدهد به روبهی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از نظرت کجا رود ور برود تو همرهی</p></div>
<div class="m2"><p>رفت و رها نمی‌کنی آمد و ره نمی‌دهی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شاید اگر نظر کنی ای که ز دردم آگهی</p></div>
<div class="m2"><p>ور نکنی اثر کند دود دل سحرگهی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سعدی و عمرو زید را هیچ محل نمی‌نهی</p></div>
<div class="m2"><p>وین همه لاف می‌زنیم از دهل میان تهی</p></div></div>