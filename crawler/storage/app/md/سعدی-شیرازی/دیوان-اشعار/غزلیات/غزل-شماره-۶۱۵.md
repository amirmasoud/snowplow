---
title: >-
    غزل شمارهٔ  ۶۱۵
---
# غزل شمارهٔ  ۶۱۵

<div class="b" id="bn1"><div class="m1"><p>ندانمت به حقیقت که در جهان به که مانی</p></div>
<div class="m2"><p>جهان و هر چه در او هست صورتند و تو جانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به پای خویشتن آیند عاشقان به کمندت</p></div>
<div class="m2"><p>که هر که را تو بگیری ز خویشتن برهانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مرا مپرس که چونی به هر صفت که تو خواهی</p></div>
<div class="m2"><p>مرا مگو که چه نامی به هر لقب که تو خوانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چنان به نظره اول ز شخص می‌ببری دل</p></div>
<div class="m2"><p>که باز می‌نتواند گرفت نظره ثانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تو پرده پیش گرفتی و ز اشتیاق جمالت</p></div>
<div class="m2"><p>ز پرده‌ها به درافتاد رازهای نهانی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر آتش تو نشستیم و دود شوق برآمد</p></div>
<div class="m2"><p>تو ساعتی ننشستی که آتشی بنشانی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو پیش خاطرم آید خیال صورت خوبت</p></div>
<div class="m2"><p>ندانمت که چه گویم ز اختلاف معانی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مرا گناه نباشد نظر به روی جوانان</p></div>
<div class="m2"><p>که پیر داند مقدار روزگار جوانی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تو را که دیده ز خواب و خمار باز نباشد</p></div>
<div class="m2"><p>ریاضت من شب تا سحر نشسته چه دانی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>من ای صبا ره رفتن به کوی دوست ندانم</p></div>
<div class="m2"><p>تو می‌روی به سلامت سلام من برسانی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سر از کمند تو سعدی به هیچ روی نتابد</p></div>
<div class="m2"><p>اسیر خویش گرفتی بکش چنان که تو دانی</p></div></div>