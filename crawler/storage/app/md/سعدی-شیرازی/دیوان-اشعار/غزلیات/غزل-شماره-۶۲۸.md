---
title: >-
    غزل شمارهٔ  ۶۲۸
---
# غزل شمارهٔ  ۶۲۸

<div class="b" id="bn1"><div class="m1"><p>تا کی روم از عشق تو شوریده به هر سوی</p></div>
<div class="m2"><p>تا کی دوم از شور تو دیوانه به هر کوی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صد نعره همی‌آیدم از هر بن مویی</p></div>
<div class="m2"><p>خود در دل سنگین تو نگرفت سر موی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر یاد بناگوش تو بر باد دهم جان</p></div>
<div class="m2"><p>تا باد مگر پیش تو بر خاک نهد روی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سرگشته چو چوگانم و در پای سمندت</p></div>
<div class="m2"><p>می‌افتم و می‌گردم چون گوی به پهلوی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خود کشته ابروی توام من به حقیقت</p></div>
<div class="m2"><p>گر کشتنیم بازبفرمای به ابروی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آنان که به گیسو دل عشاق ربودند</p></div>
<div class="m2"><p>از دست تو در پای فتادند چو گیسوی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا عشق سرآشوب تو همزانوی ما شد</p></div>
<div class="m2"><p>سر بر نگرفتم به وفای تو ز زانوی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بیرون نشود عشق توام تا ابد از دل</p></div>
<div class="m2"><p>کاندر ازلم حرز تو بستند به بازوی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>عشق از دل سعدی به ملامت بتوان برد</p></div>
<div class="m2"><p>گر رنگ توان برد به آب از رخ هندوی</p></div></div>