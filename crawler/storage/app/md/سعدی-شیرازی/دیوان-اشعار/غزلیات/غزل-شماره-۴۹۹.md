---
title: >-
    غزل شمارهٔ  ۴۹۹
---
# غزل شمارهٔ  ۴۹۹

<div class="b" id="bn1"><div class="m1"><p>خرم آن روز که چون گل به چمن بازآیی</p></div>
<div class="m2"><p>یا به بستان به در حجره من بازآیی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گلبن عیش من آن روز شکفتن گیرد</p></div>
<div class="m2"><p>که تو چون سرو خرامان به چمن بازآیی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شمع من روز نیامد که شبم بفروزی</p></div>
<div class="m2"><p>جان من وقت نیامد که به تن بازآیی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آب تلخ است مدامم چو صراحی در حلق</p></div>
<div class="m2"><p>تا تو یک روز چو ساغر به دهن بازآیی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کی به دیدار من ای مهرگسل برخیزی</p></div>
<div class="m2"><p>کی به گفتار من ای عهدشکن بازآیی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مرغ سیر آمده‌ای از قفس صحبت و من</p></div>
<div class="m2"><p>دام زاری بنهم بو که به من بازآیی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>من خود آن بخت ندارم که به تو پیوندم</p></div>
<div class="m2"><p>نه تو آن لطف نداری که به من بازآیی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سعدی آن دیو نباشد که به افسون برود</p></div>
<div class="m2"><p>هیچت افتد که چو مردم به سخن بازآیی</p></div></div>