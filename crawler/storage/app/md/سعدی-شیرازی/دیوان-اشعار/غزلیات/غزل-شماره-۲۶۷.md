---
title: >-
    غزل شمارهٔ  ۲۶۷
---
# غزل شمارهٔ  ۲۶۷

<div class="b" id="bn1"><div class="m1"><p>سروبالایی به صحرا می‌رود</p></div>
<div class="m2"><p>رفتنش بین تا چه زیبا می‌رود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا کدامین باغ از او خرمترست</p></div>
<div class="m2"><p>کاو به رامش کردن آنجا می‌رود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>می‌رود در راه و در اجزای خاک</p></div>
<div class="m2"><p>مرده می‌گوید مسیحا می‌رود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>این چنین بیخود نرفتی سنگدل</p></div>
<div class="m2"><p>گر بدانستی چه بر ما می‌رود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اهل دل را گو نگه دارید چشم</p></div>
<div class="m2"><p>کان پری پیکر به یغما می‌رود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر که را در شهر دید از مرد و زن</p></div>
<div class="m2"><p>دل ربود اکنون به صحرا می‌رود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آفتاب و سرو غیرت می‌برند</p></div>
<div class="m2"><p>کآفتابی سروبالا می‌رود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>باغ را چندان بساط افکنده‌اند</p></div>
<div class="m2"><p>کآدمی بر فرش دیبا می‌رود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>عقل را با عشق زور پنجه نیست</p></div>
<div class="m2"><p>کار مسکین از مدارا می‌رود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سعدیا دل در سرش کردی و رفت</p></div>
<div class="m2"><p>بلکه جانش نیز در پا می‌رود</p></div></div>