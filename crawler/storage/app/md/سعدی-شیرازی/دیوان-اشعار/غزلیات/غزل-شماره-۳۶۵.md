---
title: >-
    غزل شمارهٔ  ۳۶۵
---
# غزل شمارهٔ  ۳۶۵

<div class="b" id="bn1"><div class="m1"><p>به خاک پای عزیزت که عهد نشکستم</p></div>
<div class="m2"><p>ز من بریدی و با هیچ کس نپیوستم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کجا روم که بمیرم بر آستان امید</p></div>
<div class="m2"><p>اگر به دامن وصلت نمی‌رسد دستم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شگفت مانده‌ام از بامداد روز وداع</p></div>
<div class="m2"><p>که برنخاست قیامت چو بی تو بنشستم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بلای عشق تو نگذاشت پارسا در پارس</p></div>
<div class="m2"><p>یکی منم که ندانم نماز چون بستم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نماز کردم و از بیخودی ندانستم</p></div>
<div class="m2"><p>که در خیال تو عقد نماز چون بستم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نماز مست شریعت روا نمی‌دارد</p></div>
<div class="m2"><p>نماز من که پذیرد که روز و شب مستم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چنین که دست خیالت گرفت دامن من</p></div>
<div class="m2"><p>چه بودی ار برسیدی به دامنت دستم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>من از کجا و تمنای وصل تو ز کجا</p></div>
<div class="m2"><p>اگر چه آب حیاتی هلاک خود جستم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اگر خلاف تو بوده‌ست در دلم همه عمر</p></div>
<div class="m2"><p>نه نیک رفت خطا کردم و ندانستم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بکش چنان که توانی که سعدی آن کس نیست</p></div>
<div class="m2"><p>که با وجود تو دعوی کند که من هستم</p></div></div>