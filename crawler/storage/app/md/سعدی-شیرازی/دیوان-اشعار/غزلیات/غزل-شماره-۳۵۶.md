---
title: >-
    غزل شمارهٔ  ۳۵۶
---
# غزل شمارهٔ  ۳۵۶

<div class="b" id="bn1"><div class="m1"><p>چو بلبل سحری برگرفت نوبت بام</p></div>
<div class="m2"><p>ز توبه خانه تنهایی آمدم بر بام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نگاه می‌کنم از پیش رایت خورشید</p></div>
<div class="m2"><p>که می‌برد به افق پرچم سپاه ظلام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بیاض روز برآمد چو از دواج سیاه</p></div>
<div class="m2"><p>برهنه بازنشیند یکی سپیداندام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دلم به عشق گرفتار و جان به مهر گرو</p></div>
<div class="m2"><p>درآمد از درم آن دلفریب جان آرام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سرم هنوز چنان مست بوی آن نفس است</p></div>
<div class="m2"><p>که بوی عنبر و گل ره نمی‌برد به مشام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دگر من از شب تاریک هیچ غم نخورم</p></div>
<div class="m2"><p>که هر شبی را روزی مقدر است انجام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تمام فهم نکردم که ارغوان و گل است</p></div>
<div class="m2"><p>در آستینش یا دست و ساعد گلفام</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در آبگینه‌اش آبی که گر قیاس کنی</p></div>
<div class="m2"><p>ندانی آب کدام است و آبگینه کدام</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بیار ساقی دریای مشرق و مغرب</p></div>
<div class="m2"><p>که دیر مست شود هر که می خورد به دوام</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>من آن نیم که حلال از حرام نشناسم</p></div>
<div class="m2"><p>شراب با تو حلال است و آب بی تو حرام</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به هیچ شهر نباشد چنین شکر که تویی</p></div>
<div class="m2"><p>که طوطیان چو سعدی درآوری به کلام</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>رها نمی‌کند این نظم چون زره درهم</p></div>
<div class="m2"><p>که خصم تیغ تعنت برآورد ز نیام</p></div></div>