---
title: >-
    غزل شمارهٔ  ۴۹
---
# غزل شمارهٔ  ۴۹

<div class="b" id="bn1"><div class="m1"><p>خرم آن بقعه که آرامگه یار آنجاست</p></div>
<div class="m2"><p>راحت جان و شفای دل بیمار آنجاست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من در این جای همین صورت بی‌جانم و بس</p></div>
<div class="m2"><p>دلم آنجاست که آن دلبر عیار آنجاست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تنم اینجاست سقیم و دلم آنجاست مقیم</p></div>
<div class="m2"><p>فلک اینجاست ولی کوکب سیار آنجاست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آخر ای باد صبا بویی اگر می‌آری</p></div>
<div class="m2"><p>سوی شیراز گذر کن که مرا یار آنجاست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>درد دل پیش که گویم غم دل با که خورم</p></div>
<div class="m2"><p>روم آنجا که مرا محرم اسرار آنجاست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نکند میل دل من به تماشای چمن</p></div>
<div class="m2"><p>که تماشای دل آنجاست که دلدار آنجاست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سعدی این منزل ویران چه کنی جای تو نیست</p></div>
<div class="m2"><p>رخت بربند که منزلگه احرار آنجاست</p></div></div>