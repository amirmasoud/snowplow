---
title: >-
    غزل شمارهٔ  ۴۴۳
---
# غزل شمارهٔ  ۴۴۳

<div class="b" id="bn1"><div class="m1"><p>بکن چندان که خواهی جور بر من</p></div>
<div class="m2"><p>که دستت بر نمی‌دارم ز دامن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چنان مرغ دلم را صید کردی</p></div>
<div class="m2"><p>که بازش دل نمی‌خواهد نشیمن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگر دانی که در زنجیر زلفت</p></div>
<div class="m2"><p>گرفتار است در پایش میفکن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به حسن قامتت سروی در آفاق</p></div>
<div class="m2"><p>نپندارم که باشد غالب الظن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>الا ای باغبان این سرو بنشان</p></div>
<div class="m2"><p>و گر صاحب دلی آن سرو برکن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جهان روشن به ماه و آفتاب است</p></div>
<div class="m2"><p>جهان ما به دیدار تو روشن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تو بی زیور محلایی و بی رخت</p></div>
<div class="m2"><p>مزکایی و بی زینت مزین</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شبی خواهم که مهمان من آیی</p></div>
<div class="m2"><p>به کام دوستان و رغم دشمن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گروهی عام را کز دل خبر نیست</p></div>
<div class="m2"><p>عجب دارند از آه سینه من</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چو آتش در سرای افتاده باشد</p></div>
<div class="m2"><p>عجب داری که دود آید ز روزن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تو را خود هر که بیند دوست دارد</p></div>
<div class="m2"><p>گناهی نیست بر سعدی معین</p></div></div>