---
title: >-
    غزل شمارهٔ  ۲۷۲
---
# غزل شمارهٔ  ۲۷۲

<div class="b" id="bn1"><div class="m1"><p>آن که نقشی دیگرش جایی مصور می‌شود</p></div>
<div class="m2"><p>نقش او در چشم ما هر روز خوشتر می‌شود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عشق دانی چیست سلطانی که هر جا خیمه زد</p></div>
<div class="m2"><p>بی خلاف آن مملکت بر وی مقرر می‌شود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دیگران را تلخ می‌آید شراب جور عشق</p></div>
<div class="m2"><p>ما ز دست دوست می‌گیریم و شکر می‌شود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دل ز جان برگیر و در بر گیر یار مهربان</p></div>
<div class="m2"><p>گر بدین مقدارت آن دولت میسر می‌شود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هرگزم در سر نبود اندیشه سودا ولیک</p></div>
<div class="m2"><p>پیل اگر دربند می‌افتد مسخر می‌شود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عیش‌ها دارم در این آتش که بینی دم به دم</p></div>
<div class="m2"><p>کاندرونم گر چه می‌سوزد منور می‌شود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا نپنداری که با دیگر کسم خاطر خوشست</p></div>
<div class="m2"><p>ظاهرم با جمع و خاطر جای دیگر می‌شود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>غیرتم گوید نگویم با حریفان راز خویش</p></div>
<div class="m2"><p>باز می‌بینم که در آفاق دفتر می‌شود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آب شوق از چشم سعدی می‌رود بر دست و خط</p></div>
<div class="m2"><p>لاجرم چون شعر می‌آید سخن تر می‌شود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>قول مطبوع از درون سوزناک آید که عود</p></div>
<div class="m2"><p>چون همی‌سوزد جهان از وی معطر می‌شود</p></div></div>