---
title: >-
    غزل شمارهٔ  ۲۳۸
---
# غزل شمارهٔ  ۲۳۸

<div class="b" id="bn1"><div class="m1"><p>بخرام بالله تا صبا بیخ صنوبر برکند</p></div>
<div class="m2"><p>برقع برافکن تا بهشت از حور زیور برکند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زان روی و خال دلستان برکش نقاب پرنیان</p></div>
<div class="m2"><p>تا پیش رویت آسمان آن خال اختر برکند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خلقی چو من بر روی تو آشفته همچون موی تو</p></div>
<div class="m2"><p>پای آن نهد در کوی تو کاول دل از سر برکند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زان عارض فرخنده خو نه رنگ دارد گل نه بو</p></div>
<div class="m2"><p>انگشت غیرت را بگو تا چشم عبهر برکند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ما خار غم در پای جان در کویت ای گلرخ روان</p></div>
<div class="m2"><p>وان گه که را پروای آن کز پای نشتر برکند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ماه است رویت یا ملک قند است لعلت یا نمک</p></div>
<div class="m2"><p>بنمای پیکر تا فلک مهر از دوپیکر برکند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>باری به ناز و دلبری گر سوی صحرا بگذری</p></div>
<div class="m2"><p>واله شود کبک دری طاووس شهپر برکند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سعدی چو شد هندوی تو هل تا پرستد روی تو</p></div>
<div class="m2"><p>کاو خیمه زد پهلوی تو فردای محشر برکند</p></div></div>