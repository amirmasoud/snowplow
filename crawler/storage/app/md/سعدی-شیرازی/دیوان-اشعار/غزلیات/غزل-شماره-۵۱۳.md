---
title: >-
    غزل شمارهٔ  ۵۱۳
---
# غزل شمارهٔ  ۵۱۳

<div class="b" id="bn1"><div class="m1"><p>ای ولوله عشق تو بر هر سر کویی</p></div>
<div class="m2"><p>روی تو ببرد از دل ما هر غم رویی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آخر سر مویی به ترحم نگر آن را</p></div>
<div class="m2"><p>کآهی بودش تعبیه بر هر بن مویی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کم می‌نشود تشنگی دیده شوخم</p></div>
<div class="m2"><p>با آن که روان کرده‌ام از هر مژه جویی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای هر تنی از مهر تو افتاده به کنجی</p></div>
<div class="m2"><p>وی هر دلی از شوق تو آواره به سویی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ما یکدل و تو شرم نداری که برآیی</p></div>
<div class="m2"><p>هر لحظه به دستانی و هر روز به خویی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در کان نبود چون تن زیبای تو سیمی</p></div>
<div class="m2"><p>وز سنگ نخیزد چو دل سخت تو رویی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر هم نزند دست خزان بزم ریاحین</p></div>
<div class="m2"><p>گر باد به بستان برد از زلف تو بویی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>با این همه میدان لطافت که تو داری</p></div>
<div class="m2"><p>سعدی چه بود در خم چوگان تو گویی</p></div></div>