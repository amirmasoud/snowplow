---
title: >-
    غزل شمارهٔ  ۲۲۲
---
# غزل شمارهٔ  ۲۲۲

<div class="b" id="bn1"><div class="m1"><p>حسن تو دایم بدین قرار نماند</p></div>
<div class="m2"><p>مست تو جاوید در خمار نماند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای گل خندان نوشکفته نگه دار</p></div>
<div class="m2"><p>خاطر بلبل که نوبهار نماند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حسن دلاویز پنجه‌ایست نگارین</p></div>
<div class="m2"><p>تا به قیامت بر او نگار نماند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عاقبت از ما غبار ماند زنهار</p></div>
<div class="m2"><p>تا ز تو بر خاطری غبار نماند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پار گذشت آن چه دیدی از غم و شادی</p></div>
<div class="m2"><p>بگذرد امسال و همچو پار نماند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هم بدهد دور روزگار مرادت</p></div>
<div class="m2"><p>ور ندهد دور روزگار نماند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سعدی شوریده بی‌قرار چرایی</p></div>
<div class="m2"><p>در پی چیزی که برقرار نماند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شیوه عشق اختیار اهل ادب نیست</p></div>
<div class="m2"><p>بل چو قضا آید اختیار نماند</p></div></div>