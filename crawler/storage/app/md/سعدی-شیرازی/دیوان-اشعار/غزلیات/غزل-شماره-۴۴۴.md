---
title: >-
    غزل شمارهٔ  ۴۴۴
---
# غزل شمارهٔ  ۴۴۴

<div class="b" id="bn1"><div class="m1"><p>یا رب آن روی است یا برگ سمن</p></div>
<div class="m2"><p>یا رب آن قد است یا سرو چمن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر سمن کس دید جعد مشکبار</p></div>
<div class="m2"><p>در چمن کس دید سرو سیمتن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عقل چون پروانه گردید و نیافت</p></div>
<div class="m2"><p>چون تو شمعی در هزاران انجمن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سخت مشتاقیم پیمانی بکن</p></div>
<div class="m2"><p>سخت مجروحیم پیکانی بکن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>وه کدامت زین همه شیرین‌تر است</p></div>
<div class="m2"><p>خنده یا رفتار یا لب یا سخن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر سر ما خواهی اینک جان و سر</p></div>
<div class="m2"><p>ور سر ما داری اینک مال و تن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر نوازی ور کشی فرمان تو راست</p></div>
<div class="m2"><p>بنده‌ایم اینک سر و تیغ و کفن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>صعقه می‌خواهی حجابی در گذار</p></div>
<div class="m2"><p>فتنه می‌جویی نقابی بر فکن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>من کیم کانجا که کوی عشق توست</p></div>
<div class="m2"><p>در نمی‌گنجد حدیث ما و من</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ای ز وصلت خانه‌ها دار الشفا</p></div>
<div class="m2"><p>وی ز هجرت بیت‌ها بیت الحزن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>وقت آن آمد که خاک مرده را</p></div>
<div class="m2"><p>باد ریزد آب حیوان در دهن</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>پاره گرداند زلیخای صبا</p></div>
<div class="m2"><p>صبحدم بر یوسف گل پیرهن</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نطفه شبنم در ارحام زمین</p></div>
<div class="m2"><p>شاهد گل گشت و طفل یاسمن</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>فیح ریحان است یا بوی بهشت</p></div>
<div class="m2"><p>خاک شیراز است یا باد ختن</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بر گذر تا خیره گردد سروبن</p></div>
<div class="m2"><p>در نگر تا تیره گردد نسترن</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بارگاه زاهدان در هم نورد</p></div>
<div class="m2"><p>کارگاه صوفیان بر هم شکن</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>شاهدان چستند ساقی گو بیار</p></div>
<div class="m2"><p>عاشقان مستند مطرب گو بزن</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>سغبه خلقم چو صوفی در کنش</p></div>
<div class="m2"><p>شهره شهرم چو غازی بر رسن</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>تربیت را حله گو در ما مپوش</p></div>
<div class="m2"><p>عافیت را پرده گو بر ما متن</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چرخ با صد چشم چون روی تو دید</p></div>
<div class="m2"><p>صد زبان می‌خواست تا گوید حسن</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ناسزا خواهم شنید از خاص و عام</p></div>
<div class="m2"><p>سرزنش خواهم کشید از مرد و زن</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>سعدیا گر عاشقی پایی بکوب</p></div>
<div class="m2"><p>عاشقا گر مفلسی دستی بزن</p></div></div>