---
title: >-
    غزل شمارهٔ  ۳۷۲
---
# غزل شمارهٔ  ۳۷۲

<div class="b" id="bn1"><div class="m1"><p>عشقبازی نه من آخر به جهان آوردم</p></div>
<div class="m2"><p>یا گناهیست که اول من مسکین کردم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو که از صورت حال دل ما بی‌خبری</p></div>
<div class="m2"><p>غم دل با تو نگویم که ندانی دردم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای که پندم دهی از عشق و ملامت گویی</p></div>
<div class="m2"><p>تو نبودی که من این جام محبت خوردم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تو برو مصلحت خویشتن اندیش که من</p></div>
<div class="m2"><p>ترک جان دادم از این پیش که دل بسپردم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عهد کردیم که جان در سر کار تو کنیم</p></div>
<div class="m2"><p>و گر این عهد به پایان نبرم نامردم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>من که روی از همه عالم به وصالت کردم</p></div>
<div class="m2"><p>شرط انصاف نباشد که بمانی فردم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>راست خواهی تو مرا شیفته می‌گردانی</p></div>
<div class="m2"><p>گرد عالم به چنین روز نه من می‌گردم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خاک نعلین تو ای دوست نمی‌یارم شد</p></div>
<div class="m2"><p>تا بر آن دامن عصمت ننشیند گردم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>روز دیوان جزا دست من و دامن تو</p></div>
<div class="m2"><p>تا بگویی دل سعدی به چه جرم آزردم</p></div></div>