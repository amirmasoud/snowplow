---
title: >-
    غزل شمارهٔ  ۱۰۸
---
# غزل شمارهٔ  ۱۰۸

<div class="b" id="bn1"><div class="m1"><p>مرا خود با تو چیزی در میان هست</p></div>
<div class="m2"><p>و گر نه روی زیبا در جهان هست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وجودی دارم از مهرت گدازان</p></div>
<div class="m2"><p>وجودم رفت و مهرت همچنان هست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مبر ظن کز سرم سودای عشقت</p></div>
<div class="m2"><p>رود تا بر زمینم استخوان هست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر پیشم نشینی دل نشانی</p></div>
<div class="m2"><p>و گر غایب شوی در دل نشان هست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به گفتن راست ناید شرح حسنت</p></div>
<div class="m2"><p>ولیکن گفت خواهم تا زبان هست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ندانم قامتست آن یا قیامت</p></div>
<div class="m2"><p>که می‌گوید چنین سرو روان هست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>توان گفتن به مه مانی ولی ماه</p></div>
<div class="m2"><p>نپندارم چنین شیرین دهان هست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بجز پیشت نخواهم سر نهادن</p></div>
<div class="m2"><p>اگر بالین نباشد آستان هست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>برو سعدی که کوی وصل جانان</p></div>
<div class="m2"><p>نه بازاریست کان جا قدر جان هست</p></div></div>