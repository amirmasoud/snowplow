---
title: >-
    غزل شمارهٔ  ۲۱۴
---
# غزل شمارهٔ  ۲۱۴

<div class="b" id="bn1"><div class="m1"><p>سرمست ز کاشانه به گلزار برآمد</p></div>
<div class="m2"><p>غلغل ز گل و لاله به یک بار برآمد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مرغان چمن نعره زنان دیدم و گویان</p></div>
<div class="m2"><p>زین غنچه که از طرف چمنزار برآمد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آب از گل رخساره او عکس پذیرفت</p></div>
<div class="m2"><p>و آتش به سر غنچه گلنار برآمد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سجاده نشینی که مرید غم او شد</p></div>
<div class="m2"><p>آوازه اش از خانه خمار برآمد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زاهد چو کرامات بت عارض او دید</p></div>
<div class="m2"><p>از چله میان بسته به زنار برآمد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر خاک چو من بی‌دل و دیوانه نشاندش</p></div>
<div class="m2"><p>اندر نظر هر که پری وار برآمد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>من مفلس از آن روز شدم کز حرم غیب</p></div>
<div class="m2"><p>دیبای جمال تو به بازار برآمد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کام دلم آن بود که جان بر تو فشانم</p></div>
<div class="m2"><p>آن کام میسر شد و این کار برآمد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سعدی چمن آن روز به تاراج خزان داد</p></div>
<div class="m2"><p>کز باغ دلش بوی گل یار برآمد</p></div></div>