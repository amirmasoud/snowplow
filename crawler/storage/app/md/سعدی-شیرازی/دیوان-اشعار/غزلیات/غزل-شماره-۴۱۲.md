---
title: >-
    غزل شمارهٔ  ۴۱۲
---
# غزل شمارهٔ  ۴۱۲

<div class="b" id="bn1"><div class="m1"><p>آن دوست که من دارم وان یار که من دانم</p></div>
<div class="m2"><p>شیرین دهنی دارد دور از لب و دندانم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بخت این نکند با من کان شاخ صنوبر را</p></div>
<div class="m2"><p>بنشینم و بنشانم گل بر سرش افشانم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای روی دلارایت مجموعه زیبایی</p></div>
<div class="m2"><p>مجموع چه غم دارد از من که پریشانم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دریاب که نقشی ماند از طرح وجود من</p></div>
<div class="m2"><p>چون یاد تو می‌آرم خود هیچ نمی‌مانم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با وصل نمی‌پیچم وز هجر نمی‌نالم</p></div>
<div class="m2"><p>حکم آن چه تو فرمایی من بنده فرمانم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای خوبتر از لیلی بیم است که چون مجنون</p></div>
<div class="m2"><p>عشق تو بگرداند در کوه و بیابانم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یک پشت زمین دشمن گر روی به من آرند</p></div>
<div class="m2"><p>از روی تو بیزارم گر روی بگردانم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در دام تو محبوسم در دست تو مغلوبم</p></div>
<div class="m2"><p>وز ذوق تو مدهوشم در وصف تو حیرانم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دستی ز غمت بر دل پایی ز پیت در گل</p></div>
<div class="m2"><p>با این همه صبرم هست وز روی تو نتوانم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در خفیه همی‌نالم وین طرفه که در عالم</p></div>
<div class="m2"><p>عشاق نمی‌خسبند از ناله پنهانم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بینی که چه گرم آتش در سوخته می‌گیرد</p></div>
<div class="m2"><p>تو گرمتری ز آتش من سوخته تر ز آنم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گویند مکن سعدی جان در سر این سودا</p></div>
<div class="m2"><p>گر جان برود شاید من زنده به جانانم</p></div></div>