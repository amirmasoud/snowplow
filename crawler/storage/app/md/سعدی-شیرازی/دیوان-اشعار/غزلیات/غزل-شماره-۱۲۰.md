---
title: >-
    غزل شمارهٔ  ۱۲۰
---
# غزل شمارهٔ  ۱۲۰

<div class="b" id="bn1"><div class="m1"><p>خبرت هست که بی روی تو آرامم نیست</p></div>
<div class="m2"><p>طاقت بار فراق این همه ایامم نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خالی از ذکر تو عضوی چه حکایت باشد</p></div>
<div class="m2"><p>سر مویی به غلط در همه اندامم نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>میل آن دانه خالم نظری بیش نبود</p></div>
<div class="m2"><p>چون بدیدم ره بیرون شدن از دامم نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شب بر آنم که مگر روز نخواهد بودن</p></div>
<div class="m2"><p>بامدادت که نبینم طمع شامم نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چشم از آن روز که برکردم و رویت دیدم</p></div>
<div class="m2"><p>به همین دیده سر دیدن اقوامم نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نازنینا مکن آن جور که کافر نکند</p></div>
<div class="m2"><p>ور جهودی بکنم بهره در اسلامم نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گو همه شهر به جنگم به درآیند و خلاف</p></div>
<div class="m2"><p>من که در خلوت خاصم خبر از عامم نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نه به زرق آمده‌ام تا به ملامت بروم</p></div>
<div class="m2"><p>بندگی لازم اگر عزت و اکرامم نیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به خدا و به سراپای تو کز دوستیت</p></div>
<div class="m2"><p>خبر از دشمن و اندیشه ز دشنامم نیست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دوستت دارم اگر لطف کنی ور نکنی</p></div>
<div class="m2"><p>به دو چشم تو که چشم از تو به انعامم نیست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سعدیا نامتناسب حیوانی باشد</p></div>
<div class="m2"><p>هر که گوید که دلم هست و دلارامم نیست</p></div></div>