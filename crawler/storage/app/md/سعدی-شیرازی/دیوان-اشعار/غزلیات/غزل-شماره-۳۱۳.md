---
title: >-
    غزل شمارهٔ  ۳۱۳
---
# غزل شمارهٔ  ۳۱۳

<div class="b" id="bn1"><div class="m1"><p>برآمد باد صبح و بوی نوروز</p></div>
<div class="m2"><p>به کام دوستان و بخت پیروز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مبارک بادت این سال و همه سال</p></div>
<div class="m2"><p>همایون بادت این روز و همه روز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو آتش در درخت افکند گلنار</p></div>
<div class="m2"><p>دگر منقل منه آتش میفروز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو نرگس چشم بخت از خواب برخاست</p></div>
<div class="m2"><p>حسد گو دشمنان را دیده بردوز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بهاری خرم است ای گل کجایی</p></div>
<div class="m2"><p>که بینی بلبلان را ناله و سوز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جهان بی ما بسی بوده‌ست و باشد</p></div>
<div class="m2"><p>برادر جز نکونامی میندوز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نکویی کن که دولت بینی از بخت</p></div>
<div class="m2"><p>مبر فرمان بدگوی بدآموز</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>منه دل بر سرای عمر سعدی</p></div>
<div class="m2"><p>که بر گنبد نخواهد ماند این گوز</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دریغا عیش اگر مرگش نبودی</p></div>
<div class="m2"><p>دریغ آهو اگر بگذاشتی یوز</p></div></div>