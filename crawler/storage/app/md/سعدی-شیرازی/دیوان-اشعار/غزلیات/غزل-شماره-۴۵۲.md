---
title: >-
    غزل شمارهٔ  ۴۵۲
---
# غزل شمارهٔ  ۴۵۲

<div class="b" id="bn1"><div class="m1"><p>فراق دوستانش باد و یاران</p></div>
<div class="m2"><p>که ما را دور کرد از دوستداران</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دلم در بند تنهایی بفرسود</p></div>
<div class="m2"><p>چو بلبل در قفس روز بهاران</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هلاک ما چنان مهمل گرفتند</p></div>
<div class="m2"><p>که قتل مور در پای سواران</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به خیل هر که می‌آیم به زنهار</p></div>
<div class="m2"><p>نمی‌بینم به جز زنهارخواران</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ندانستم که در پایان صحبت</p></div>
<div class="m2"><p>چنین باشد وفای حق گزاران</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به گنج شایگان افتاده بودم</p></div>
<div class="m2"><p>ندانستم که بر گنجند ماران</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دلا گر دوستی داری به ناچار</p></div>
<div class="m2"><p>بباید بردنت جور هزاران</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خلاف شرط یاران است سعدی</p></div>
<div class="m2"><p>که برگردند روز تیرباران</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چه خوش باشد سری در پای یاری</p></div>
<div class="m2"><p>به اخلاص و ارادت جان سپاران</p></div></div>