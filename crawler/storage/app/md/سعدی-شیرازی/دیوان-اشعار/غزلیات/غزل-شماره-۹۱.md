---
title: >-
    غزل شمارهٔ  ۹۱
---
# غزل شمارهٔ  ۹۱

<div class="b" id="bn1"><div class="m1"><p>سفر دراز نباشد به پای طالب دوست</p></div>
<div class="m2"><p>که زنده ابدست آدمی که کشته اوست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شراب خورده معنی چو در سماع آید</p></div>
<div class="m2"><p>چه جای جامه که بر خویشتن بدرد پوست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر آن که با رخ منظور ما نظر دارد</p></div>
<div class="m2"><p>به ترک خویش بگوید که خصم عربده جوست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حقیر تا نشماری تو آب چشم فقیر</p></div>
<div class="m2"><p>که قطره قطره باران چو با هم آمد جوست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نمی‌رود که کمندش همی‌برد مشتاق</p></div>
<div class="m2"><p>چه جای پند نصیحت کنان بیهده گوست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو در میانه خاک اوفتاده‌ای بینی</p></div>
<div class="m2"><p>از آن بپرس که چوگان از او مپرس که گوست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چرا و چون نرسد بندگان مخلص را</p></div>
<div class="m2"><p>رواست گر همه بد می‌کنی بکن که نکوست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کدام سرو سهی راست با وجود تو قدر</p></div>
<div class="m2"><p>کدام غالیه را پیش خاک پای تو بوست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بسی بگفت خداوند عقل و نشنیدم</p></div>
<div class="m2"><p>که دل به غمزه خوبان مده که سنگ و سبوست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هزار دشمن اگر بر سرند سعدی را</p></div>
<div class="m2"><p>به دوستی که نگوید به جز حکایت دوست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به آب دیده خونین نبشته قصه عشق</p></div>
<div class="m2"><p>نظر به صفحه اول مکن که تو بر توست</p></div></div>