---
title: >-
    غزل شمارهٔ  ۴۳۷
---
# غزل شمارهٔ  ۴۳۷

<div class="b" id="bn1"><div class="m1"><p>بگذار تا مقابل روی تو بگذریم</p></div>
<div class="m2"><p>دزدیده در شمایل خوب تو بنگریم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شوق است در جدایی و جور است در نظر</p></div>
<div class="m2"><p>هم جور به که طاقت شوقت نیاوریم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>روی ار به روی ما نکنی حکم از آن توست</p></div>
<div class="m2"><p>بازآ که روی در قدمانت بگستریم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ما را سریست با تو که گر خلق روزگار</p></div>
<div class="m2"><p>دشمن شوند و سر برود هم بر آن سریم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفتی ز خاک بیشترند اهل عشق من</p></div>
<div class="m2"><p>از خاک بیشتر نه که از خاک کمتریم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ما با توایم و با تو نه‌ایم اینت بلعجب</p></div>
<div class="m2"><p>در حلقه‌ایم با تو و چون حلقه بر دریم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نه بوی مهر می‌شنویم از تو ای عجب</p></div>
<div class="m2"><p>نه روی آن که مهر دگر کس بپروریم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از دشمنان برند شکایت به دوستان</p></div>
<div class="m2"><p>چون دوست دشمن است شکایت کجا بریم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ما خود نمی‌رویم دوان در قفای کس</p></div>
<div class="m2"><p>آن می‌برد که ما به کمند وی اندریم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سعدی تو کیستی که در این حلقه کمند</p></div>
<div class="m2"><p>چندان فتاده‌اند که ما صید لاغریم</p></div></div>