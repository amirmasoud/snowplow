---
title: >-
    قصیدهٔ شمارهٔ ۵۶ - در پند و ستایش
---
# قصیدهٔ شمارهٔ ۵۶ - در پند و ستایش

<div class="b" id="bn1"><div class="m1"><p>بزن که قوت بازوی سلطنت داری</p></div>
<div class="m2"><p>که دست همت مردانت می‌دهد یاری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جهان‌گشای و عدو بند و ملک‌بخش و ستان</p></div>
<div class="m2"><p>که در حمایت صاحبدلان بسیاری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گرت به شب نبدی سر بر آستانهٔ حق</p></div>
<div class="m2"><p>کیت به روز میسر شدی جهانداری؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به دولت تو چنان ایمنست پشت زمین</p></div>
<div class="m2"><p>که خلق در شکم مادرند پنداری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به زیر سایهٔ عدل تو آسمان را نیست</p></div>
<div class="m2"><p>مجال آنکه کند بر کسی ستمکاری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کف عطای تو گر نیست ابر رحمت حق</p></div>
<div class="m2"><p>چه نعمت است که بر بر و بحر می‌باری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مدیح، شیوهٔ درویش نیست تا گویم</p></div>
<div class="m2"><p>مثال بحر محیطی و ابر آذاری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نگویمت که به فضل از کرام ممتازی</p></div>
<div class="m2"><p>نگویمت که به عدل از ملوک مختاری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>وگرچه این همه هستی، نصیحت اولیتر</p></div>
<div class="m2"><p>که پند، راه خلاص است و دوستی باری</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به سعی کوش که ناگه فراغتت نبود</p></div>
<div class="m2"><p>که سر بخاری اگر روی شیر نر خاری</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خدای یوسف صدیق را عزیز نکرد</p></div>
<div class="m2"><p>به خوب رویی، لیکن به خوب کرداری</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>شکوه و لشکر و جاه و جمال و مالت هست</p></div>
<div class="m2"><p>ولی به کار نیاید به جز نکوکاری</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چه روزها به شب آورده‌ای به راحت نفس</p></div>
<div class="m2"><p>چه باشد ار به عبادت شبی به روز آری</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>که پیش اهل دل آب حیات در ظلمات</p></div>
<div class="m2"><p>دعای زنده‌دلانست در شب تاری</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>خدای سلطنتت بر زمین دنیا داد</p></div>
<div class="m2"><p>ز بهر آنکه درو تخم آخرت کاری</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>به نیک و بد چو بباید گذاشت این بهتر</p></div>
<div class="m2"><p>که نام نیک به دست آوری و بگذاری</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>پس از گرفتن عالم چو کوچ خواهد بود</p></div>
<div class="m2"><p>رواست گر همه عالم گرفته انگاری</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>صراط راست که داند در آن جهان رفتن؟</p></div>
<div class="m2"><p>کسی که خو کند اینجا به راست رفتاری</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>جهان ستانی و لشکرکشی چه مانندست</p></div>
<div class="m2"><p>به کامرانی درویش در سبکباری؟</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>به بندگی سر طاعت بنه که بربایی</p></div>
<div class="m2"><p>به رفعت از سر گردون، کلاه جباری</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چو کار با لحد افتاد هر دو یکسانند</p></div>
<div class="m2"><p>بزرگتر ملک و کمترینه بازاری</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ورین گدا به مثل نیکبخت برخیزد</p></div>
<div class="m2"><p>بدان امیر اجلش دهند سالاری</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>تو را که رحمت و دادست و دین، بشارت باد</p></div>
<div class="m2"><p>که جور و ظلم و تعدی ز خلق برداری</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بقای مملکت اندر وجود یک شرطست</p></div>
<div class="m2"><p>که دست هیچ قوی بر ضعیف نگماری</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>به دولتت علم دین حق فراشته باد</p></div>
<div class="m2"><p>به صولتت علم کفر در نگونساری</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>چنانکه تا به قیامت کسی نشان ندهد</p></div>
<div class="m2"><p>بجز دهانه فرنگی و مشک تاتاری</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>هزار سال نگویم بقای عمر تو باد</p></div>
<div class="m2"><p>که این مبالغه دانم ز عقل نشماری</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>همین سعادت و توفیق بر مزیدت باد</p></div>
<div class="m2"><p>که حق‌گزاری و بی‌حق کسی نیازاری</p></div></div>