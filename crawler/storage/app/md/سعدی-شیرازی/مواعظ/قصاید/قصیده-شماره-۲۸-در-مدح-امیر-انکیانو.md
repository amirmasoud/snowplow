---
title: >-
    قصیدهٔ شمارهٔ ۲۸ - در مدح امیر انکیانو
---
# قصیدهٔ شمارهٔ ۲۸ - در مدح امیر انکیانو

<div class="b" id="bn1"><div class="m1"><p>بس بگردید و بگردد روزگار</p></div>
<div class="m2"><p>دل به دنیا در نبندد هوشیار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای که دستت می‌رسد کاری بکن</p></div>
<div class="m2"><p>پیش از آن کاز تو نیاید هیچ کار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اینکه در شهنامه‌ها آورده‌اند</p></div>
<div class="m2"><p>رستم و رویینه‌تن اسفندیار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا بدانند این خداوندان ملک</p></div>
<div class="m2"><p>کز بسی خلق است دنیا یادگار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>این همه رفتند و مای شوخ چشم</p></div>
<div class="m2"><p>هیچ نگرفتیم از ایشان اعتبار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای که وقتی نطفه بودی بی‌خبر</p></div>
<div class="m2"><p>وقت دیگر طفل بودی شیرخوار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مدتی بالا گرفتی تا بلوغ</p></div>
<div class="m2"><p>سرو بالایی شدی سیمین عذار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>همچنین تا مرد نام‌آور شدی</p></div>
<div class="m2"><p>فارس میدان و صید و کارزار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آنچه دیدی بر قرار خود نماند</p></div>
<div class="m2"><p>واین چه بینی هم نماند بر قرار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دیر و زود این شکل و شخص نازنین</p></div>
<div class="m2"><p>خاک خواهد بودن و خاکش غبار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گل بخواهد چید بی‌شک باغبان</p></div>
<div class="m2"><p>ور نچیند خود فرو ریزد ز بار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>این همه هیچ است چون می‌بگذرد</p></div>
<div class="m2"><p>تخت و بخت و امر و نهی و گیر و دار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نام نیکو گر بماند زآدمی</p></div>
<div class="m2"><p>به کاز او ماند سرای زرنگار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>سال دیگر را که می‌داند حساب؟</p></div>
<div class="m2"><p>یا کجا رفت آن که با ما بود پار؟</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>خفتگان بیچاره در خاک لحد</p></div>
<div class="m2"><p>خفته اندر کلهٔ سر سوسمار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>صورت زیبای ظاهر هیچ نیست</p></div>
<div class="m2"><p>ای برادر سیرت زیبا بیار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>هیچ دانی تا خرد به یا روان</p></div>
<div class="m2"><p>من بگویم گر بداری استوار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>آدمی را عقل باید در بدن</p></div>
<div class="m2"><p>ور نه جان در کالبد دارد حمار</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>پیش از آن کاز دست بیرونت برد</p></div>
<div class="m2"><p>گردش گیتی زمام اختیار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گنج خواهی، در طلب رنجی ببر</p></div>
<div class="m2"><p>خرمنی می‌بایدت، تخمی بکار</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چون خداوندت بزرگی داد و حکم</p></div>
<div class="m2"><p>خرده از خردان مسکین در گذار</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چون زبردستیت بخشید آسمان</p></div>
<div class="m2"><p>زیردستان را همیشه نیک دار</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>عذرخواهان را خطاکاری ببخش</p></div>
<div class="m2"><p>زینهاری را به جان ده زینهار</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>شکر نعمت را نکویی کن که حق</p></div>
<div class="m2"><p>دوست دارد بندگان حقگزار</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>لطف او لطفیست بیرون از عدد</p></div>
<div class="m2"><p>فضل او فضلیست بیرون از شمار</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>گر به هر مویی زبانی باشدت</p></div>
<div class="m2"><p>شکر یک نعمت نگویی از هزار</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>نام نیک رفتگان ضایع مکن</p></div>
<div class="m2"><p>تا بماند نام نیکت پایدار</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>ملکبانان را نشاید روز و شب</p></div>
<div class="m2"><p>گاهی اندر خمر و گاهی در خمار</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>کام درویشان و مسکینان بده</p></div>
<div class="m2"><p>تا همه کارت بر آرد کردگار</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>با غریبان لطف بی‌اندازه کن</p></div>
<div class="m2"><p>تا رود نامت به نیکی در دیار</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>زور بازو داری و شمشیر تیز</p></div>
<div class="m2"><p>گر جهان لشکر بگیرد غم مدار</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>از درون خستگان اندیشه کن</p></div>
<div class="m2"><p>وز دعای مردم پرهیزگار</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>منجنیق آه مظلومان به صبح</p></div>
<div class="m2"><p>سخت گیرد ظالمان را در حصار</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>با بدان بد باش و با نیکان نکو</p></div>
<div class="m2"><p>جای گل گل باش و جای خار خار</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>دیو با مردم نیامیزد مترس</p></div>
<div class="m2"><p>بل بترس از مردمان دیوسار</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>هر که دد یا مردم بد پرورد</p></div>
<div class="m2"><p>دیر زود از جان بر آرندش دمار</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>با بدان چندان که نیکویی کنی</p></div>
<div class="m2"><p>قتل مار افسا نباشد جز به مار</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>ای که داری چشم عقل و گوش هوش</p></div>
<div class="m2"><p>پند من در گوش کن چون گوشوار</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>نشکند عهد من الا سنگدل</p></div>
<div class="m2"><p>نشنود قول من الا بختیار</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>سعدیا چندان که می‌دانی بگوی</p></div>
<div class="m2"><p>حق نباید گفتن الا آشکار</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>هر که را خوف و طمع در کار نیست</p></div>
<div class="m2"><p>از ختا باکش نباشد وز تتار</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>دولت نوئین اعظم شهریار</p></div>
<div class="m2"><p>باد تا باشد بقای روزگار</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>خسرو عادل امیر نامور</p></div>
<div class="m2"><p>انکیانو سرور عالی تبار</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>دیگران حلوا به طرغو آورند</p></div>
<div class="m2"><p>من جواهر می‌کنم بر وی نثار</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>پادشاهان را ثنا گویند و مدح</p></div>
<div class="m2"><p>من دعایی می‌کنم درویش‌وار</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>یارب الهامش به نیکویی بده</p></div>
<div class="m2"><p>وز بقای عمر برخوردار دار</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>جاودان از دور گیتی کام دل</p></div>
<div class="m2"><p>در کنارت باد و دشمن بر کنار</p></div></div>