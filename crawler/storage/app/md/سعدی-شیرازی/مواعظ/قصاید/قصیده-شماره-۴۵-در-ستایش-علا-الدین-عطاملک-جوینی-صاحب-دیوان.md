---
title: >-
    قصیدهٔ شمارهٔ ۴۵ - در ستایش علاء الدین عطاملک جوینی صاحب دیوان
---
# قصیدهٔ شمارهٔ ۴۵ - در ستایش علاء الدین عطاملک جوینی صاحب دیوان

<div class="b" id="bn1"><div class="m1"><p>شکر به شکر نهم در دهان مژده دهان</p></div>
<div class="m2"><p>اگر تو باز برآری حدیث من به دهان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بعید نیست که گر تو به عهد بازآیی</p></div>
<div class="m2"><p>به عید وصل تو من خویشتن کنم قربان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تو آن نه‌ای که چو غایب شوی ز دل بروی</p></div>
<div class="m2"><p>تفاوتی نکند قرب دل به بعد مکان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>قرار یک نفسم بی‌تو دست می‌ندهد</p></div>
<div class="m2"><p>هم احتمال جفا به که صبر بر هجران</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>محب صادق اگر صاحبش به تیر زند</p></div>
<div class="m2"><p>محبتش نگذارد که بر کند پیکان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>وصال دوست به جان گر میسرت گردد</p></div>
<div class="m2"><p>بخر که دیر به دست اوفتد چنین ارزان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کدام روز دگر جان به کار بازآید</p></div>
<div class="m2"><p>که جان‌فشان نکنی روز وصل بر جانان؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شکایت از دل سنگین یار نتوان کرد</p></div>
<div class="m2"><p>که خویشتن زده‌ایم آبگینه بر سندان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز دست دوست به نالیدن آمدی سعدی</p></div>
<div class="m2"><p>تو قدر دوست ندانی که دوست داری جان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گر آن بدیع صفت خویشتن به ما ندهد</p></div>
<div class="m2"><p>بیار ساقی و ما را ز خویشتن بستان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>زمان باد بهارست، داد عیش بده</p></div>
<div class="m2"><p>که دور عمر چنان می‌رود که برق ایمان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چگونه پیر جوانی و جاهلی نکند</p></div>
<div class="m2"><p>درین قضیه که گردد جهان پیر جوان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نظارهٔ چمن اردیبهشت خوش باشد</p></div>
<div class="m2"><p>که بر درخت زند باد نوبهار افشان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>مهندسان طبیعت ز جامه خانهٔ غیب</p></div>
<div class="m2"><p>هزار حله برآرند مختلف الوان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ز کارگاه قضا در درخت پوشانند</p></div>
<div class="m2"><p>قبای سبز که تاراج کرده بود خزان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>به کلبهٔ چمن از رنگ و بوی باز کنند</p></div>
<div class="m2"><p>هزار طبلهٔ عطار و تخت بازرگان</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بهار میوه چو مولود نازپرور دوست</p></div>
<div class="m2"><p>که تا بلوغ دهان برنگیرد از پستان</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>نه آفتاب مضرت کند نه سایه گزند</p></div>
<div class="m2"><p>که هر چهار به هم متفق شدند ارکان</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>اوان منقل آتش گذشت و خانهٔ گرم</p></div>
<div class="m2"><p>زمان برکهٔ آبست و صفهٔ ایوان</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بساط لهو بینداز و برگ عیش بنه</p></div>
<div class="m2"><p>به زیر سایهٔ رز بر کنار شادروان</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>تو گر به رقص نیایی شگفت جانوری</p></div>
<div class="m2"><p>ازین هوا که درخت آمدست در جولان</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ز بانگ مشغلهٔ بلبلان عاشق مست</p></div>
<div class="m2"><p>شکوفه جامه دریدست و سرو سرگردان</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>خجل شوند کنون دختران مصر چمن</p></div>
<div class="m2"><p>که گل ز خار برآید چو یوسف از زندان</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>تو خود مطالعهٔ باغ و بوستان نکنی</p></div>
<div class="m2"><p>که بوستان بهاری و باغ لالستان</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>کدام گل بود اندر چمن به زیباییت؟</p></div>
<div class="m2"><p>کدام سرو به بالای تست در بستان؟</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>چه گویم آن خط سبز و دهان شیرین را</p></div>
<div class="m2"><p>بجز خضر نتوان گفت و چشمهٔ حیوان</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>به چند روز دگر کافتاب گرم شود</p></div>
<div class="m2"><p>مقر عیش بود سایه‌بان و سایهٔ بان</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>تو کافتاب زمینی به هیچ سایه مرو</p></div>
<div class="m2"><p>مگر به سایهٔ دستور پادشاه زمان</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>سحاب رحمت و دریای فضل و کان کرم</p></div>
<div class="m2"><p>سپهر حشمت و کوه وقار و کهف امان</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بزرگ روی زمین پادشاه صدرنشین</p></div>
<div class="m2"><p>علاء دولت و دین صدر پادشاه‌نشان</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>که گردنان اکابر نخست فرمانش</p></div>
<div class="m2"><p>نهند بر سر و پس سر نهند بر فرمان</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>وگر حسود نه راضیست گو به رشک بمیر</p></div>
<div class="m2"><p>که مرتبت به سزاوار می‌دهد یزدان</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>نه تافتست چنین آفتاب بر آفاق</p></div>
<div class="m2"><p>نه گستریده چنین سایه بر بسیط جهان</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>بلند پایهٔ قدرش چه جای فهم و قیاس</p></div>
<div class="m2"><p>فراخ مایهٔ فضلش چه جای حصر وبیان</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>به گرد همتش ادراک آدمی نرسد</p></div>
<div class="m2"><p>که فهم برنتواند گذشتن از کیوان</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>برو محاسن اخلاق چون رطب بر بار</p></div>
<div class="m2"><p>درو فنون فضایل چو دانه در رمان</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>چو بر صحیفهٔ املی روان شود قلمش</p></div>
<div class="m2"><p>زبان طعن نهد در بلاغت سحبان</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>چنان رمند و دوند اهل بدعت از نظرش</p></div>
<div class="m2"><p>که از مسیحا دجال و از عمر شیطان</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>به ناز و نعمتش امروز حق نظر کردست</p></div>
<div class="m2"><p>امید هست که فردا به رحمت و رضوان</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>کسان ذخیرهٔ دنیا نهند و غلهٔ او</p></div>
<div class="m2"><p>هنوز سنبله باشد که رفت در میزان</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>بزرگوارا شرح معالیت که دهد</p></div>
<div class="m2"><p>که فکر واصف ازو منقطع شود حیران</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>به گرد نقطهٔ عالم سپهر دایره گرد</p></div>
<div class="m2"><p>ندید شبه تو چندانکه می‌کند دوران</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>که دید تشنهٔ ریان به جز تو در افاق</p></div>
<div class="m2"><p>به عدل و عفو و کرم تشنه وز ادب ریان</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>خدای را به تو فضلی که در جهان دارد</p></div>
<div class="m2"><p>کدام شکر توان گفت در مقابل آن</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>خنک عراق که در سایهٔ حمایت تست</p></div>
<div class="m2"><p>حمایت تو نگویم، عنایت یزدان</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>ز بأس تو نه عجب در بلاد فرس و عرب</p></div>
<div class="m2"><p>که گرگ بر گله یارا نباشدش عدوان</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>بر درخت امیدت همیشه باد که نیست</p></div>
<div class="m2"><p>به دور عدل تو جز بر درخت بار گران</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>سپهر با تو به رفعت برابری نکند</p></div>
<div class="m2"><p>که شرمسار بود مدعی، بلا برهان</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>چو حصر منقبتت در قلم نمی‌آید</p></div>
<div class="m2"><p>چگونه وصف تو گوید زبان مدحت خوان</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>من این قصیده به پایان نمی‌توانم برد</p></div>
<div class="m2"><p>که شرح مکرمتت را نمی‌رسد پایان</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>به خاطرم غزلی سوزناک می‌گذرد</p></div>
<div class="m2"><p>زبانه می‌زند از تنگنای دل به زبان</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>درون خانه ضرورت چو آتشی باشد</p></div>
<div class="m2"><p>به اتفاق برون آید از دریچه دخان</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>نخواستم دگر این باد عشق پیمودن</p></div>
<div class="m2"><p>ولیک می‌نتوان بستن آب طبع روان</p></div></div>