---
title: >-
    قصیدهٔ شمارهٔ ۵۹ - در ستایش امیر انکیانو
---
# قصیدهٔ شمارهٔ ۵۹ - در ستایش امیر انکیانو

<div class="b" id="bn1"><div class="m1"><p>دنیا نیرزد آنکه پریشان کنی دلی</p></div>
<div class="m2"><p>زنهار بد مکن که نکردست عاقلی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این پنج روزه مهلت ایام آدمی</p></div>
<div class="m2"><p>آزار مردمان نکند جز مغفلی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>باری نظر به خاک عزیزان رفته کن</p></div>
<div class="m2"><p>تا مجمل وجود ببینی مفصلی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آن پنجهٔ کمانکش و انگشت خوشنویس</p></div>
<div class="m2"><p>هر بندی اوفتاده به جایی و مفصلی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>درویش و پادشه نشنیدم که کرده‌اند</p></div>
<div class="m2"><p>بیرون ازین دو لقمهٔ روزی تناولی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زان گنجهای نعمت و خروارهای مال</p></div>
<div class="m2"><p>با خویشتن به گور نبردند خردلی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از مال و جاه و منصب و فرمان و تخت و بخت</p></div>
<div class="m2"><p>بهتر ز نام نیک نکردند حاصلی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بعد از هزار سال که نوشیروان گذشت</p></div>
<div class="m2"><p>گویند ازو هنوز که بودست عادلی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ای آنکه خانه در ره سیلاب می‌کنی</p></div>
<div class="m2"><p>بر خاک رودخانه نباشد معولی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دل در جهان مبند که با کس وفا نکرد</p></div>
<div class="m2"><p>هرگز نبود دور زمان بی‌تبدلی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مرگ از تو دور نیست وگر هست فی‌المثل</p></div>
<div class="m2"><p>هر روز باز می‌رویش پیش، منزلی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بنیاد خاک بر سر آبست ازین سبب</p></div>
<div class="m2"><p>خالی نباشد از خللی یا تزلزلی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>دنیا مثال بحر عمیقست پر نهنگ</p></div>
<div class="m2"><p>آسوده عارفان که گرفتند ساحلی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>دانا چه گفت، گفت چو عزلت ضرورتست</p></div>
<div class="m2"><p>من خود به اختیار نشینم به معزلی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>یعنی خلاف رای خداوند حکمت است</p></div>
<div class="m2"><p>امروز خانه کردن و فردا تحولی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>آنگه که سر به بالش گورم نهند باز</p></div>
<div class="m2"><p>از من چه بالشی که بماند چه حنبلی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بعد از خدای هر چه تصور کنی به عقل</p></div>
<div class="m2"><p>ناچارش آخریست همیدون که اولی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>خواهی که رستگار شوی راستکار باش</p></div>
<div class="m2"><p>تا عیب جوی را نرسد بر تو مدخلی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>تیر از کمان چو رفت نیاید به شست باز</p></div>
<div class="m2"><p>پس واجبست در همه کاری تأملی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>باید که قهر و لطف بود پادشاه را</p></div>
<div class="m2"><p>ورنه میسرش نشود حل مشکلی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>وقتی به لطف گوی که سالار قوم را</p></div>
<div class="m2"><p>با گفت و گوی خلق بباید تحملی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>وقتی به قهر گوی که صد کوزهٔ نبات</p></div>
<div class="m2"><p>گه گه چنان به کار نیاید که حنظلی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>مرد آدمی نباشد اگر دل نسوزدش</p></div>
<div class="m2"><p>باری که بیند و خری اوفتاده در گلی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>رستم به نیزه‌ای نکند هرگز آن مصاف</p></div>
<div class="m2"><p>با دشمنان خویش که زالی به مغزلی</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>هرگز به پنج روزه حیات گذشتنی</p></div>
<div class="m2"><p>خرم کسی شود مگر از موت غافلی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>نی کاروان برفت و تو خواهی مقیم بود</p></div>
<div class="m2"><p>ترتیب کرده‌اند تو را نیز محملی</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>گر من سخن درشت نگویم تو نشنوی</p></div>
<div class="m2"><p>بی‌جهد از آینه نبرد زنگ صیقلی</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>حقگوی را زبان ملامت بود دراز</p></div>
<div class="m2"><p>حق نیست اینچه گفتم؟ اگر هست گو بلی</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>تو راست باش تا دگران راستی کنند</p></div>
<div class="m2"><p>دانی که بی‌ستاره نرفتست جدولی</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>خاص از برای وسوسهٔ دیو نفس را</p></div>
<div class="m2"><p>شاید گر این سخن بنویسی به هیکلی</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>جز نیکبخت پند خردمند نشنود</p></div>
<div class="m2"><p>اینست تربیت که پریشان مکن دلی</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>تا هر چه گفته باشمت از خیر در حضور</p></div>
<div class="m2"><p>بعد از تو شرمسار نباشم به محفلی</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>این فکر بکر من که به حسنش نظیر نیست</p></div>
<div class="m2"><p>مردم مخوان اگر دهمش جز به مقبلی</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>وان کیست انکیانه که دادار آسمان</p></div>
<div class="m2"><p>دادست مرو را همه حسن و شمایلی</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>نویین اعظم آنکه به تدبیر و فهم و رای</p></div>
<div class="m2"><p>امروز در بسیط ندارد مقابلی</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>من خود چگونه دم زنم از عقل و طبع خویش</p></div>
<div class="m2"><p>کس پیش آفتاب نکردست مشعلی</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>منت‌پذیر او نه منم در زمین پارس</p></div>
<div class="m2"><p>در حق کیست آنکه ندارد تفضلی</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>عمرت دراز باد نگویم هزار سال</p></div>
<div class="m2"><p>زیرا که اهل حق نپسندند باطلی</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>نفست همیشه پیرو فرمان شرع باد</p></div>
<div class="m2"><p>تا بر سرش ز عقل بداری موکلی</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>تا بلبلان به ناله درآیند بامداد</p></div>
<div class="m2"><p>هر گه که سر برآورد از بوستان گلی</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>همواره بوستان امیدت شکفته باد</p></div>
<div class="m2"><p>سعدی دعای خیر تو گویان چو بلبلی</p></div></div>