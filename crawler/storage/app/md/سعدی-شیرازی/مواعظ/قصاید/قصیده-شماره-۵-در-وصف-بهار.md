---
title: >-
    قصیدهٔ شمارهٔ ۵ - در وصف بهار
---
# قصیدهٔ شمارهٔ ۵ - در وصف بهار

<div class="b" id="bn1"><div class="m1"><p>علم دولت نوروز به صحرا برخاست</p></div>
<div class="m2"><p>زحمت لشکر سرما ز سر ما برخاست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر عروسان چمن بست صبا هر گهری</p></div>
<div class="m2"><p>که به غواصی ابر از دل دریا برخاست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا رباید کله قاقم برف از سر کوه</p></div>
<div class="m2"><p>یزک تابش خورشید به یغما برخاست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>طبق باغ پر از نقل و ریاحین کردند</p></div>
<div class="m2"><p>شکر آن را که زمین از تب سرما برخاست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>این چه بوییست که از ساحت خلخ بدمید؟</p></div>
<div class="m2"><p>وین چه بادیست که از جانب یغما برخاست؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چه هواییست که خلدش به تحسر بنشست؟</p></div>
<div class="m2"><p>چه زمینیست که چرخش به تولا برخاست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>طارم اخضر از عکس چمن حمرا گشت</p></div>
<div class="m2"><p>بس که از طرف چمن لؤلؤ لالا برخاست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>موسم نغمهٔ چنگست که در بزم صبوح</p></div>
<div class="m2"><p>بلبلان را ز چمن ناله و غوغا برخاست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بوی آلودگی از خرقهٔ صوفی آمد</p></div>
<div class="m2"><p>سوز دیوانگی از سینهٔ دانا برخاست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از زمین نالهٔ عشاق به گردون بر شد</p></div>
<div class="m2"><p>وز ثری نعرهٔ مستان به ثریا برخاست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>عارف امروز به ذوقی بر شاهد بنشست</p></div>
<div class="m2"><p>که دل زاهد از اندیشهٔ فردا برخاست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هر دلی را هوس روی گلی در سر شد</p></div>
<div class="m2"><p>که نه این مشغله از بلبل تنها برخاست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گوییا پردهٔ معشوق برافتاد از پیش</p></div>
<div class="m2"><p>قلم عافیت از عاشق شیدا برخاست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>هر کجا طلعت خورشید رخی سایه فکند</p></div>
<div class="m2"><p>بیدلی خسته کمر بسته چو جوزا برخاست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>هرکجا سروقدی چهره چو یوسف بنمود</p></div>
<div class="m2"><p>عاشقی سوخته خرمن چو زلیخا برخاست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>با رخش لاله ندانم به چه رونق بشکفت</p></div>
<div class="m2"><p>با قدش سرو ندانم به چه یارا برخاست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>سر به بالین عدم بازنه ای نرگس مست</p></div>
<div class="m2"><p>که ز خواب سحر آن نرگس شهلا برخاست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>به سخن گفتن او عقل ز هر دل برمید</p></div>
<div class="m2"><p>عاشق آن قد مستم که چه زیبا برخاست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>روز رویش چو برانداخت نقاب شب زلف</p></div>
<div class="m2"><p>گفتی از روز قیامت شب یلدا برخاست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ترک عشقش بنه صبر چنان غارت کرد</p></div>
<div class="m2"><p>که حجاب از حرم راز معما برخاست</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>سعدیا تا کی ازین نامه سیه کردن؟ بس</p></div>
<div class="m2"><p>که قلم را به سر از دست تو سودا برخاست</p></div></div>