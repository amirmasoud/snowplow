---
title: >-
    قصیدهٔ شمارهٔ ۴۲ - در انتقال دولت از سلغریان به قوم دیگر
---
# قصیدهٔ شمارهٔ ۴۲ - در انتقال دولت از سلغریان به قوم دیگر

<div class="b" id="bn1"><div class="m1"><p>این منتی بر اهل زمین بود از آسمان</p></div>
<div class="m2"><p>وین رحمت خدای جهان بود بر جهان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا گرد نان روی زمین منزجر شدند</p></div>
<div class="m2"><p>گردن نهاده بر خط و فرمان ایلخان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اقصای بر و بحر به تأیید عدل او</p></div>
<div class="m2"><p>آمد به تیغ حادثه دربارهٔ امان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بوی چمن برآمد و برف جبل گداخت</p></div>
<div class="m2"><p>گل با شکفتن آمد و بلبل به بوستان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آن دور شد که ناخن درنده تیز بود</p></div>
<div class="m2"><p>و آن روزگار رفت که گرگی کند شبان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر بقعه‌ای که چشم ارادت کند خدای</p></div>
<div class="m2"><p>فرماندهی گمارد بر خلق مهربان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شاهی که عرض لشکر منصور اگر دهد</p></div>
<div class="m2"><p>از قیروان سپاه کشد تا به قیروان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر تاختن به لشکر سیاره آورد</p></div>
<div class="m2"><p>از هم بیوفتند ثریا و فرقدان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سلطان روم و روس به منت دهد خراج</p></div>
<div class="m2"><p>چیپال هند و سند به گردن کشد قلان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ملکی بدین مسافت و حکمی برین نسق</p></div>
<div class="m2"><p>ننوشته‌اند در همه شهنامه داستان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ای پادشاه مشرق و مغرب به اتفاق</p></div>
<div class="m2"><p>بل کمترینه بندهٔ تو پادشه نشان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>حق را به روزگار تو بر خلق منتیست</p></div>
<div class="m2"><p>کاندر حساب عقل نیاید شمار آن</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>در روی دشمنان تو تیری بیوفتاد</p></div>
<div class="m2"><p>کز هیبت تو پشت بدادند چون کمان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>هر که به بندگیت کمر بست تاج یافت</p></div>
<div class="m2"><p>بنهاد مدعی سر و بر سر نهاد جان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>با شیر پنجه کردن روبه نه رای بود</p></div>
<div class="m2"><p>باطل خیال بست و خلاف آمدش گمان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>سر بر سنان نیزه نکردیش روزگار</p></div>
<div class="m2"><p>گر سر به بندگی بنهادی بر آستان</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>گنجشک را که دانهٔ روزی تمام شد</p></div>
<div class="m2"><p>از پیش باز، باز نیاید به آشیان</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>نفس درنده، پند خردمند نشنود</p></div>
<div class="m2"><p>بگذار تا درشت بیوبارد استخوان</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>گردون سنان قهر به باطل نمی‌زند</p></div>
<div class="m2"><p>الا کسی که خود بزند سینه بر سنان</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>اقبال نانهاده به کوشش نمی‌دهند</p></div>
<div class="m2"><p>بر بام آسمان نتوان شد به نردبان</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بخت بلند باید و پس کتف زورمند</p></div>
<div class="m2"><p>بی‌شرطه خاک بر سر ملاح و بادبان</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ای پادشاه روی زمین دور از آن تست</p></div>
<div class="m2"><p>اندیشه کن تقلب دوران آسمان</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بیخی نشان که دولت باقیت بردهد</p></div>
<div class="m2"><p>کاین باغ عمر گاه بهارست و گه خزان</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>هر نوبتی نظر به یکی می‌کند سپهر</p></div>
<div class="m2"><p>هر مدتی زمین به یکی می‌دهد زمان</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چون کام جاودان متصور نمی‌شود</p></div>
<div class="m2"><p>خرم تنی که زنده کند نام جاودان</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>نادان که بخل می‌کند و گنج می‌نهد</p></div>
<div class="m2"><p>مزدور دشمنست تو بر دوستان فشان</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>یارب تو هرچه رای صوابست و فعل خیر</p></div>
<div class="m2"><p>اندر دل وی افکن و بر دست وی بران</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>آهوی طبع بنده چنین مشک می‌دهد</p></div>
<div class="m2"><p>کز پارس می‌برند به تاتارش ارمغان</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بیهوده در بسیط زمین این سخن نرفت</p></div>
<div class="m2"><p>مردم نمی‌برند که خود می‌رود روان</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>سعدی دلاوری و زبان‌آوری مکن</p></div>
<div class="m2"><p>تا عیب نشمرند بزرگان خرده‌دان</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>گر در عراق نقد تو را بر محک زنند</p></div>
<div class="m2"><p>بسیار زر که مس به درآید ز امتحان</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>لیکن به حکم آنکه خداوند معرفت</p></div>
<div class="m2"><p>داند که بوی خوش نتوان داشتن نهان</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>گر چون بنفشه سر به سخن برنمی‌کنم</p></div>
<div class="m2"><p>فکر از دلم چو لاله به در می‌کند زبان</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>چون غنچه عاقبت لبم از یکدگر برفت</p></div>
<div class="m2"><p>تا چون شکوفه پر زر سرخم کنی دهان</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>یارب دعای پیر و جوانت رفیق باد</p></div>
<div class="m2"><p>تا آن زمان که پیر شوی دولتت جوان</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>دست ملوک، لازم فتراک دولتت</p></div>
<div class="m2"><p>چون پای در رکاب کنی بخت هم عنان</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>در اهتمام صاحب صدر بزرگوار</p></div>
<div class="m2"><p>فرمانروای عالم و علامهٔ جهان</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>اکفی الکفاة روی زمین شمس ملک و دین</p></div>
<div class="m2"><p>جانب نگاه‌دار خدای و خدایگان</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>صدر جهان و صاحب صاحبقران که هست</p></div>
<div class="m2"><p>قدر مهان روی زمین پیش او مهان</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>گر مقتضی نحو نبودی نگفتمی</p></div>
<div class="m2"><p>با بحر کف او خبر کان و اسم کان</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>نظم مدیح او نه به اندازهٔ منست</p></div>
<div class="m2"><p>لیکن رواست نظم لالی به ریسمان</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>ای آفتاب ملک، بسی روزها بتاب</p></div>
<div class="m2"><p>وی سایهٔ خدای بسی سالها بمان</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>خالی مباد گلشن خضرای مجلست</p></div>
<div class="m2"><p>ز آواز بلبلان غزل‌گوی مدح‌خوان</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>تا بر درت به رسم بشارت همی زنند</p></div>
<div class="m2"><p>دشمن به چوب تا چو دهل می‌کند فغان</p></div></div>