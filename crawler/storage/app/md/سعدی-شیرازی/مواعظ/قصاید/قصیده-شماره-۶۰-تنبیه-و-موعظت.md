---
title: >-
    قصیدهٔ شمارهٔ ۶۰ - تنبیه و موعظت
---
# قصیدهٔ شمارهٔ ۶۰ - تنبیه و موعظت

<div class="b" id="bn1"><div class="m1"><p>دریغ روز جوانی و عهد برنایی</p></div>
<div class="m2"><p>نشاط کودکی و عیش خویشتن رایی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سر فروتنی انداخت پیریم در پیش</p></div>
<div class="m2"><p>پس از غرور جوانی و دست بالایی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دریغ بازوی سرپنجگی که برپیچد</p></div>
<div class="m2"><p>ستیز دور فلک ساعد توانایی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زهی زمانهٔ ناپایدار عهد شکن</p></div>
<div class="m2"><p>چه دوستیست که با دوستان نمی‌پایی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>که اعتماد کند بر مواهب نعمت</p></div>
<div class="m2"><p>که همچو طفل ببخشی و باز بربایی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به‌زارتر گسلی هر چه خوبتر بندی</p></div>
<div class="m2"><p>تباه‌تر شکنی هر چه خوشتر آرایی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به عمر خویش کسی کامی از توبرنگرفت</p></div>
<div class="m2"><p>که در شکنجهٔ بی‌کامیش نفرسایی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اگر زیادت قدرست در تغیر نفس</p></div>
<div class="m2"><p>نخواستم که به قدر من اندر افزایی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مرا ملامت دیوانگی و سرشغبی</p></div>
<div class="m2"><p>تو را سلامت پیری و پای برجایی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شکوه پیری بگذار و علم و فضل و ادب</p></div>
<div class="m2"><p>کجاست جهل و جوانی و عشق و شیدایی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چو با قضای اجل بر نمی‌توان آمد</p></div>
<div class="m2"><p>تفاوتی نکند گربزی و دانایی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نه آن جلیس انیس از کنار من رفتست</p></div>
<div class="m2"><p>که بعد ازو متصور شود شکیبایی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>دریغ خلعت دیبای احسن‌التقویم</p></div>
<div class="m2"><p>بر آستین تنعم، طراز زیبایی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>غبار خط معنبر نشسته بر گل روی</p></div>
<div class="m2"><p>چنانکه مشک به ماورد بر سمن سایی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>اگر ز باد فنا ای پسر بیندیشی</p></div>
<div class="m2"><p>چو گل به عمر دو روزه غرور ننمایی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>زمان رفته نخواهد به گریه بازآمد</p></div>
<div class="m2"><p>نه آب دیده، که گر خون دل بپالایی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>همیشه باز نباشد در دو لختی چشم</p></div>
<div class="m2"><p>ضرورتست که روزی به گل براندایی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ندوخت جامهٔ کامی به قد کس گردون</p></div>
<div class="m2"><p>که عاقبت به مصیبت نکرد یکتایی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چو خوان یغما بر هم زند همی ناگاه</p></div>
<div class="m2"><p>زمانه مجلس عیش بتان یغمایی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چو تخم خرما فردات پایمال کنند</p></div>
<div class="m2"><p>وگر به سروری امروز نخل خرمایی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>برادران تو بیچاره در ثری رفتند</p></div>
<div class="m2"><p>تو همچنان ز سر کبر بر ثریایی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>خیال بسته و بر باد عمر تکیه زده</p></div>
<div class="m2"><p>به پنج روز که در عشرت تمنایی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>دماغ پخته که من شیرمرد برناام</p></div>
<div class="m2"><p>برو چو با سگ نفس نبهره بر نایی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>اگر بود دلمؤمنچو موم، نرم نهاد</p></div>
<div class="m2"><p>تو موم نیستی ای دل که سنگ خارایی</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>هر آن زمان که ز تو مردمی برآساید</p></div>
<div class="m2"><p>درست شد به حقیقت که مردم‌آسایی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>وگر به جهل برفتی به عذر بازپس آی</p></div>
<div class="m2"><p>که چاره نیست برون از شکسته پیرایی</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>سخن دراز مکن سعدیا و کوته کن</p></div>
<div class="m2"><p>چو روزگار به پیرانه سر به رعنایی</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>وگر عنایت و توفیق حق نگیرد دست</p></div>
<div class="m2"><p>به دست سعی تو بادست تا نپیمایی</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>ببخش بار خدایا بعه فضل و رحمت خویش</p></div>
<div class="m2"><p>که دردمند نوازی و جرم بخشایی</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بضاعتی نه سزاوار حضرت آوردیم</p></div>
<div class="m2"><p>مگر به عین عنایت قبول فرمایی</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>ز درگه کرمت روی ناامیدی نیست</p></div>
<div class="m2"><p>کجا رود مگس از کارگاه حلوایی</p></div></div>