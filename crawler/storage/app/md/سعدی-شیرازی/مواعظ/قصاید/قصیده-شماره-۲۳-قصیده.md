---
title: >-
    قصیدهٔ شمارهٔ ۲۳ - قصیده
---
# قصیدهٔ شمارهٔ ۲۳ - قصیده

<div class="b" id="bn1"><div class="m1"><p>تو را ز دست اجل کی فرار خواهد بود</p></div>
<div class="m2"><p>فرارگاه تو دارالقرار خواهد بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگر تو ملک جهان را به دست آوردی</p></div>
<div class="m2"><p>مباش غره که ناپایدار خواهد بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به مال غره چه باشی که یک دو روزی بعد</p></div>
<div class="m2"><p>همه نصیبهٔ میراث خوار خواهد بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تو را به تخته و تابوت درکشند از تخت</p></div>
<div class="m2"><p>گرت خزانه و لشکر هزار خواهد بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تو را به کنج لحد سالها بباید خفت</p></div>
<div class="m2"><p>تن تو طعمهٔ هر مور و مار خواهد بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اگر تو در چمن روزگار همچو گلی</p></div>
<div class="m2"><p>دمیده بر سر خاک تو خار خواهد بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نیازمندی یاران نداردت سودی</p></div>
<div class="m2"><p>مگر عمل که تو را باز یار خواهد بود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بسا سوار که آنجا پیاده خواهد شد</p></div>
<div class="m2"><p>بسا پیاده که آنجا سوار خواهد بود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بسا امیر که آنجا اسیر خواهد شد</p></div>
<div class="m2"><p>بسا اسیر که فرمانگذار خواهد بود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بسا امام ریایی و پیشوای بزرگ</p></div>
<div class="m2"><p>که روز حشر و جزا شرمسار خواهد بود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چرا ز حال قیامت دمی نیندیشی</p></div>
<div class="m2"><p>که حال بیخبران سخت زار خواهد بود</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بهشت می‌طلبی، از گنه نپرهیزی؟</p></div>
<div class="m2"><p>بهشت منزل پرهیزگار خواهد بود</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گذر ز باطل و مردانه حق‌پرستی کن</p></div>
<div class="m2"><p>ز حق‌پرستی بهتر چه کار خواهد بود؟</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بساز چارهٔ رفتن که رهروان رفتند</p></div>
<div class="m2"><p>که سعدی از تو سخن یادگار خواهد بود</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>به قطره قطره حرامت عذابت خواهد بود</p></div>
<div class="m2"><p>به ذره ذره حلالت شمار خواهد بود</p></div></div>