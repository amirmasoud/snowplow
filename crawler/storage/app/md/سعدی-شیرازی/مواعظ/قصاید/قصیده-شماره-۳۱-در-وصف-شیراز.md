---
title: >-
    قصیدهٔ شمارهٔ ۳۱ - در وصف شیراز
---
# قصیدهٔ شمارهٔ ۳۱ - در وصف شیراز

<div class="b" id="bn1"><div class="m1"><p>خوشا سپیده‌دمی باشد آنکه بینم باز</p></div>
<div class="m2"><p>رسیده بر سر الله اکبر شیراز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بدیده بار دگر آن بهشت روی زمین</p></div>
<div class="m2"><p>که بار ایمنی آرد نه جور قحط و نیاز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نه لایق ظلماتست بالله این اقلیم</p></div>
<div class="m2"><p>که تختگاه سلیمان بدست و حضرت راز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هزار پیر و ولی بیش باشد اندر وی</p></div>
<div class="m2"><p>که کعبه بر سر ایشان همی کند پرواز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به ذکر و فکر و عبادت به روح شیخ کبیر</p></div>
<div class="m2"><p>به حق روزبهان و به حق پنج نماز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>که گوش دار تو این شهر نیکمردان را</p></div>
<div class="m2"><p>ز دست ظالم بد دین و کافر غماز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به حق کعبه و آن کس که کرد کعبه بنا</p></div>
<div class="m2"><p>که دار مردم شیراز در تجمل و ناز</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هر آن کسی که کند قصد قبةالاسلام</p></div>
<div class="m2"><p>بریده باد سرش همچو زر و نقره به گاز</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>که سعدی از حق شیراز روز و شب می‌گفت</p></div>
<div class="m2"><p>که شهرها همه بازند و شهر ما شهباز</p></div></div>