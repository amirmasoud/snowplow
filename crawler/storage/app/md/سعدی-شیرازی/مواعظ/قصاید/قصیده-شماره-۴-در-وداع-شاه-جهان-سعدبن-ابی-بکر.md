---
title: >-
    قصیدهٔ شمارهٔ ۴ - در وداع شاه جهان سعدبن ابی‌بکر
---
# قصیدهٔ شمارهٔ ۴ - در وداع شاه جهان سعدبن ابی‌بکر

<div class="b" id="bn1"><div class="m1"><p>رفتی و صدهزار دلت دست در رکیب</p></div>
<div class="m2"><p>ای جان اهل دل که تواند ز جان شکیب؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گویی که احتمال کند مدتی فراق</p></div>
<div class="m2"><p>آن را که یک نفس نبود طاقت عتیب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا همچو آفتاب برآیی دگر ز شرق</p></div>
<div class="m2"><p>ما جمله دیده بر ره و انگشت بر حسیب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از دست قاصدی که کتابی به من رسد</p></div>
<div class="m2"><p>در پای قاصد افتم و بر سر نهم کتیب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون دیگران ز دل نروی گر روی ز چشم</p></div>
<div class="m2"><p>کاندر میان جانی و از دیده در حجیب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>امید روز وصل دل خلق می‌دهد</p></div>
<div class="m2"><p>ورنه فراق خون بچکانیدی از نهیب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در بوستانسرای تو بعد از تو کی شود</p></div>
<div class="m2"><p>خندان انار و، تازه به و، سرخ روی سیب؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>این عید متفق نشود خلق را نشاط</p></div>
<div class="m2"><p>عید آنکه بر رسیدنت آذین کنند و زیب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>این طلعت خجسته که با تست غم مدار</p></div>
<div class="m2"><p>کاقبال یاورت بود اندر فراز و شیب</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>همراه تست خاطر سعدی به حکم آنک</p></div>
<div class="m2"><p>خلق خوشت چو گفتهٔ سعدیست دلفریب</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تأیید و نصرت و ظفرت باد همعنان</p></div>
<div class="m2"><p>هر بامداد و شب که نهی پای در رکیب</p></div></div>