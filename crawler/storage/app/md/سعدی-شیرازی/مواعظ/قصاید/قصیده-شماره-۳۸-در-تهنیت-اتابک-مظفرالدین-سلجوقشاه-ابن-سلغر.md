---
title: >-
    قصیدهٔ شمارهٔ ۳۸ - در تهنیت اتابک مظفرالدین سلجوقشاه ابن سلغر
---
# قصیدهٔ شمارهٔ ۳۸ - در تهنیت اتابک مظفرالدین سلجوقشاه ابن سلغر

<div class="b" id="bn1"><div class="m1"><p>خدای را چه توان گفت شکر فضل و کرم</p></div>
<div class="m2"><p>بدین نظر که دگرباره کرد بر عالم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به دور دولت سلجوقشاه سلغرشاه</p></div>
<div class="m2"><p>خدایگان معظم اتابک اعظم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سر ملوک زمان پادشاه روی زمین</p></div>
<div class="m2"><p>خلیفهٔ پدر و عم به اتفاق اعم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زمین پارس دگر فر آسمان دارد</p></div>
<div class="m2"><p>به ماه طلعت شاه و ستارگان حشم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یکی به حضرت او داغ خادمی بر روی</p></div>
<div class="m2"><p>یکی به خدمت او دست بندگی بر هم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به قبلهٔ کرمش روی نیکخواهان راست</p></div>
<div class="m2"><p>به خدمت حرمش پشت پادشاهان خم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هنوز کوس بشارت تمام نازده بود</p></div>
<div class="m2"><p>که تهنیت به دیار عرب رسید و عجم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز سر نهادن گردن‌کشان و سالاران</p></div>
<div class="m2"><p>بر آستان جلالش نماند جای قدم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سپاس بار خدایی که شکر نعمت او</p></div>
<div class="m2"><p>هزار سال کم از حق او بود یک دم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خوشست بر دل آزادگان جراحت دوست</p></div>
<div class="m2"><p>به حکم آنکه همش دوست می‌نهد مرهم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شب فراق به روز وصال حامله بود</p></div>
<div class="m2"><p>الم خوشست به اندیشهٔ شفای الم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دگر خلاف نباشد میان آتش و آب</p></div>
<div class="m2"><p>دگر نزاع نیفتد میان گرگ و غنم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ز سایهٔ علم شیر پیکرش نه عجب</p></div>
<div class="m2"><p>که لرزه بر تن شیران فتد چو شیر علم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>اگر دو دیدهٔ دشمن نمی‌تواند دید</p></div>
<div class="m2"><p>که دوستان همه شادند، گو بمیر از غم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>وجود هر که نخواهد دوام دولت او</p></div>
<div class="m2"><p>اسیر باد به زندان ساکنان عدم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>شها به خون عدو ریختن شتاب مکن</p></div>
<div class="m2"><p>که خود هلاک شوند از حسد به خون شکم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>هر آنکه چون قلمت سر به حکم بر ننهد</p></div>
<div class="m2"><p>دو نیمه باد سرش تا به سینه همچو قلم</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چنان به عهد تو مشتاق بود نوبت ملک</p></div>
<div class="m2"><p>که تشنگان به فرات و پیادگان به حرم</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>به حلق خلق فرو ریخت شربتی شیرین</p></div>
<div class="m2"><p>زدند بر دل بدگوی ضربتی محکم</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>جهان نماند و آثار معدلت ماند</p></div>
<div class="m2"><p>به خیر کوش و صلاح و سداد و عفو و کرم</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>که ملک و دولت اضحاک بی‌گناه آزار</p></div>
<div class="m2"><p>نماند و تا به قیامت برو بماند رقم</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>خطای بنده نگیری که مهتران ملوک</p></div>
<div class="m2"><p>شنیده‌اند نصیحت ز کهتران خدم</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>خنک تنی که پس از وی حدیث خیر کنند</p></div>
<div class="m2"><p>که جز حدیث نمی‌ماند از بنی‌آدم</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>به دولتت همه افتادگان بلند شدند</p></div>
<div class="m2"><p>چو آفتاب که بر آسمان برد شبنم</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>مگر کمینهٔ آحاد بندگان سعدی</p></div>
<div class="m2"><p>که سعیش از همه بیشست و حظش از همه کم</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>همیشه خرمیت باد و خیر باد که خلق</p></div>
<div class="m2"><p>نبوده‌اند به ایام کس چنین خرم</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>سری مباد که بر خط بندگی تو نیست</p></div>
<div class="m2"><p>وگر بود به سرنیزه باد چون پرچم</p></div></div>