---
title: >-
    قصیدهٔ شمارهٔ ۵۳ - در ستایش ترکان خاتون و پسرش اتابک محمد
---
# قصیدهٔ شمارهٔ ۵۳ - در ستایش ترکان خاتون و پسرش اتابک محمد

<div class="b" id="bn1"><div class="m1"><p>چه دعا گویمت ای سایهٔ میمون همای</p></div>
<div class="m2"><p>یارب این سایه بسی بر سر اسلام بپای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جود پیدا و وجود از نظر خلق نهان</p></div>
<div class="m2"><p>نام در عالم و خود در کنف ستر خدای</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در سراپردهٔ عصمت به عبادت مشغول</p></div>
<div class="m2"><p>پادشاهان متوقف به در پرده‌سرای</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آفتاب اینهمه شمع از پی و مشعل در پیش</p></div>
<div class="m2"><p>دست بر سینه نهندش که به پروانه درآی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مطلع برج سعادت فلک اختر سعد</p></div>
<div class="m2"><p>بحر دردانهٔ شاهی، صدف گوهرزای</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حرم عفت و عصمت به تو آراسته باد</p></div>
<div class="m2"><p>علم دین محمد به محمد برپای</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خلف دودهٔ سلغر، شرف دولت و ملک</p></div>
<div class="m2"><p>ملک آیت رحمت، ملک ملک‌آرای</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سایهٔ لطف خدا، داعیهٔ راحت خلق</p></div>
<div class="m2"><p>شاه گردنکش دشمن کش عاجز بخشای</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ملک ویران نشود خانهٔ خیر آبادان</p></div>
<div class="m2"><p>دین تغیر نکند قاعدهٔ عدل به جای</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ای حسود ار نشوی خاک در خدمت او</p></div>
<div class="m2"><p>دیگرت باد به دستست برو می‌پیمای</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هر که خواهد که در این مملکت انگشت خلاف</p></div>
<div class="m2"><p>بر خطایی بنهد، گو برو انگشت بخای</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>جهد و مردی ندهد آنچه دهد دولت و بخت</p></div>
<div class="m2"><p>گنج و لشکر نکند آنچه کند همت و رای</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>قدم بنده به خدمت نتوانست رسید</p></div>
<div class="m2"><p>قلم از شوق و ارادت به سر آمد نه به پای</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>جاودان قصر معالیت چنان باد که مرغ</p></div>
<div class="m2"><p>نتواند که برو سایه کند غیر همای</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نیکخواهان تو را تاج کرامت بر سر</p></div>
<div class="m2"><p>بدسگالان تو را بند عقوبت در پای</p></div></div>