---
title: >-
    قصیدهٔ شمارهٔ ۲۹ - تغزل در ستایش شمس‌الدین محمد جوینی صاحب دیوان
---
# قصیدهٔ شمارهٔ ۲۹ - تغزل در ستایش شمس‌الدین محمد جوینی صاحب دیوان

<div class="b" id="bn1"><div class="m1"><p>نظر دریغ مدار از من ای مه منظور</p></div>
<div class="m2"><p>که مه دریغ نمی‌دارد از خلایق نور</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به چشم نیک نگه کرده‌ام تو را همه وقت</p></div>
<div class="m2"><p>چرا چو چشم بد افتاده‌ام ز روی تو دور</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تو را که درد نبودست جان من همه عمر</p></div>
<div class="m2"><p>چو دردمند بنالد نداریش معذور</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تن درست چه داند به خواب نوشین در</p></div>
<div class="m2"><p>که شب چگونه به پایان همی‌برد رنجور؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مرا که سحر سخن در همه جهان رفتست</p></div>
<div class="m2"><p>ز سحر چشم تو بیچاره مانده‌ام مسحور</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دو رسته لؤلؤ منظوم در دهان داری</p></div>
<div class="m2"><p>عبارت لب شیرین چو لؤلؤ منثور</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اگر نه وعدهٔ مؤمن به آخرت بودی</p></div>
<div class="m2"><p>زمین پارس بهشتست گفتمی و تو حور</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تو بر سمندی و بیچارگان اسیر کمند</p></div>
<div class="m2"><p>کنار خانهٔ زین بهره‌مند و ما مهجور</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تو پارسایی و رندی به هم کنی سعدی</p></div>
<div class="m2"><p>میسرت نشود مست باش یا مستور</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چنین سوار درین عرصهٔ ممالک پارس</p></div>
<div class="m2"><p>ملک چگونه نباشد مظفر و منصور؟</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>اجل و اعظم آفاق شمس دولت و دین</p></div>
<div class="m2"><p>که برد گوی نکو نامی از ملوک و صدور</p></div></div>