---
title: >-
    قصیدهٔ شمارهٔ ۵۸ - در ستایش ابوبکر بن سعد
---
# قصیدهٔ شمارهٔ ۵۸ - در ستایش ابوبکر بن سعد

<div class="b" id="bn1"><div class="m1"><p>وجودم به تنگ آمد از جور تنگی</p></div>
<div class="m2"><p>شدم در سفر روزگاری درنگی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جهان زیر پی چون سکندر بریدم</p></div>
<div class="m2"><p>چو یأجوج بگذشتم از سد سنگی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>برون جستم از تنگ ترکان چو دیدم</p></div>
<div class="m2"><p>جهان در هم افتاده چون موی زنگی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو باز آمدم کشور آسوده دیدم</p></div>
<div class="m2"><p>ز گرگان به در رفته آن تیز چنگی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خط ماهرویان چو مشک تتاری</p></div>
<div class="m2"><p>سر زلف خوبان چو درع فرنگی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به نام ایزد آباد و پر ناز و نعمت</p></div>
<div class="m2"><p>پلنگان رها کرده خوی پلنگی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>درون مردمی چون ملک نیک محضر</p></div>
<div class="m2"><p>برون لشکری چون هژبران جنگی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بپرسیدم این کشور آسوده کی شد؟</p></div>
<div class="m2"><p>کسی گفت: سعدی! چه شوریده رنگی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چنان بود در عهد اول که دیدی</p></div>
<div class="m2"><p>جهانی پر آشوب و تشویش و تنگی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چنین شد در ایام سلطان عادل</p></div>
<div class="m2"><p>اتابک ابوبکر بن سعد زنگی</p></div></div>