---
title: >-
    شمارهٔ ۲۹
---
# شمارهٔ ۲۹

<div class="b" id="bn1"><div class="m1"><p>خری از روستائیی بگریخت</p></div>
<div class="m2"><p>جل بیفکند و پاردم بگسیخت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در بیابان چو گور خر می‌تاخت</p></div>
<div class="m2"><p>بانگ می‌کرد و جفته می‌انداخت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>که به جان آمده ز محنت و بند</p></div>
<div class="m2"><p>داغ و بیطار و بار و پشماگند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شادمانا و خرما که منم</p></div>
<div class="m2"><p>که ازین پس به کام خویشتنم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>روستایی چو خر برفت از دست</p></div>
<div class="m2"><p>گفت ای نابکار صبرم هست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پس بخواهی به وقت جو گفتن</p></div>
<div class="m2"><p>که خری بد ز پایگه رفتن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به مزاحت نگفتم این گفتار</p></div>
<div class="m2"><p>هزل بگذار و جد ازو بردار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>همچنین مرد جاهل سرمست</p></div>
<div class="m2"><p>روز درماندگی بخاید دست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ندهند آنچه قیمتش ندهی</p></div>
<div class="m2"><p>نشود کاسهٔ پر ز دیگ تهی</p></div></div>