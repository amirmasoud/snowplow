---
title: >-
    شمارهٔ ۸
---
# شمارهٔ ۸

<div class="b" id="bn1"><div class="m1"><p>چه سرپوشیدگان مرد بودند</p></div>
<div class="m2"><p>که گوی نخوت از مردان ربودند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو با این مردی و زورآزمایی</p></div>
<div class="m2"><p>همی ترسم که از زن کمتر آیی</p></div></div>