---
title: >-
    شمارهٔ ۳۴
---
# شمارهٔ ۳۴

<div class="b" id="bn1"><div class="m1"><p>حرامش باد بدعهد بداندیش</p></div>
<div class="m2"><p>شکم پرکردن از پهلوی درویش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شکم پر زهرمارش بود و کژدم</p></div>
<div class="m2"><p>که راحت خواهد اندر رنج مردم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>روا دارد کسی با ناتوان زور؟</p></div>
<div class="m2"><p>کبوتر دانه خواهد هرگز از مور؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر عنقا ز بی‌برگی بمیرد</p></div>
<div class="m2"><p>شکار از چنگ گنجشکان نگیرد</p></div></div>