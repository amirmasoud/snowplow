---
title: >-
    شمارهٔ ۲۱
---
# شمارهٔ ۲۱

<div class="b" id="bn1"><div class="m1"><p>همه دانند لشکر و میران</p></div>
<div class="m2"><p>که جوانی نیاید از پیران</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عذر من بر عذار من پیداست</p></div>
<div class="m2"><p>بعد ازینم چه عذر باید خواست؟</p></div></div>