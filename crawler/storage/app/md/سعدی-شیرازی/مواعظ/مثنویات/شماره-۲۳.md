---
title: >-
    شمارهٔ ۲۳
---
# شمارهٔ ۲۳

<div class="b" id="bn1"><div class="m1"><p>این دغل دوستان که می‌بینی</p></div>
<div class="m2"><p>مگسانند دور شیرینی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا حطامی که هست می‌نوشند</p></div>
<div class="m2"><p>همچو زنبور بر تو می‌جوشند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>باز وقتی که ده خراب شود</p></div>
<div class="m2"><p>کیسه چون کاسهٔ رباب شود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ترک صحبت کنند و دلداری</p></div>
<div class="m2"><p>معرفت خود نبود پنداری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بار دیگر که بخت باز آید</p></div>
<div class="m2"><p>کامرانی ز در فراز آید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دوغبایی بپز که از چپ و راست</p></div>
<div class="m2"><p>در وی افتند چون مگس در ماست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>راست خواهی سگان بازارند</p></div>
<div class="m2"><p>کاستخوان از تو دوستر دارند</p></div></div>