---
title: >-
    شمارهٔ ۴۲
---
# شمارهٔ ۴۲

<div class="b" id="bn1"><div class="m1"><p>هر که آمد بر خدای قبول</p></div>
<div class="m2"><p>نکند هیچش از خدا مشغول</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یونس اندر دهان ماهی شد</p></div>
<div class="m2"><p>همچنان مونس الهی شد</p></div></div>