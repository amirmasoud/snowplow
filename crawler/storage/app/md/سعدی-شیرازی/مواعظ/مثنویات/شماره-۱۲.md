---
title: >-
    شمارهٔ ۱۲
---
# شمارهٔ ۱۲

<div class="b" id="bn1"><div class="m1"><p>نشنیدم که مرغ رفته ز دام</p></div>
<div class="m2"><p>باز گردید و سر گفته به کام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مرغ وحشی که رفت بر دیوار</p></div>
<div class="m2"><p>که تواند گرفت دیگر بار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رفتگان را به لطف باز آرند</p></div>
<div class="m2"><p>نه به جنگش بتر بیازارند</p></div></div>