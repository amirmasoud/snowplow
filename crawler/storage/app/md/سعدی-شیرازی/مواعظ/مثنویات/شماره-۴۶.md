---
title: >-
    شمارهٔ ۴۶
---
# شمارهٔ ۴۶

<div class="b" id="bn1"><div class="m1"><p>به یک سال در جادویی ارمنی</p></div>
<div class="m2"><p>میان دو شخص افکند دشمنی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سخن چین بدبخت در یکنفس</p></div>
<div class="m2"><p>خلاف افکند در میان دو کس</p></div></div>