---
title: >-
    شمارهٔ ۳۳
---
# شمارهٔ ۳۳

<div class="b" id="bn1"><div class="m1"><p>حدیث پادشاهان عجم را</p></div>
<div class="m2"><p>حکایت نامهٔ ضحاک و جم را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بخواند هوشمند نیکفرجام</p></div>
<div class="m2"><p>نشاید کرد ضایع خیره ایام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مگر کز خوی نیکان پند گیرند</p></div>
<div class="m2"><p>وز انجام بدان عبرت پذیرند</p></div></div>