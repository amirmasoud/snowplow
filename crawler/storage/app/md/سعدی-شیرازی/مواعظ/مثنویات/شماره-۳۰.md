---
title: >-
    شمارهٔ ۳۰
---
# شمارهٔ ۳۰

<div class="b" id="bn1"><div class="m1"><p>حرص فرزند آدم نادان</p></div>
<div class="m2"><p>مثل مورچه است در میدان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این یکی مرده زیر پای دواب</p></div>
<div class="m2"><p>آن یکی دانه می‌برد به‌شتاب</p></div></div>