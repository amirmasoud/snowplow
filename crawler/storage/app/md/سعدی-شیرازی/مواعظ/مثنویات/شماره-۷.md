---
title: >-
    شمارهٔ ۷
---
# شمارهٔ ۷

<div class="b" id="bn1"><div class="m1"><p>یکی را دیدم اندر جایگاهی</p></div>
<div class="m2"><p>که می‌کاوید قبر پادشاهی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به دست از بارگاهش خاک می‌رفت</p></div>
<div class="m2"><p>سرشک از دیده می‌بارید و می‌گفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ندانم پادشه یا پاسبانی</p></div>
<div class="m2"><p>همی بینم که مشتی استخوانی</p></div></div>