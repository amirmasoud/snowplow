---
title: >-
    شمارهٔ ۴۳
---
# شمارهٔ ۴۳

<div class="b" id="bn1"><div class="m1"><p>به حال نیک و بد راضی شو ای مرد</p></div>
<div class="m2"><p>که نتوان طالع بد را نکو کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو سگ را بخت تاریکست و شبرنگ</p></div>
<div class="m2"><p>هم از خردی زنندش کودکان سنگ</p></div></div>