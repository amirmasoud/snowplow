---
title: >-
    شمارهٔ ۳۸
---
# شمارهٔ ۳۸

<div class="b" id="bn1"><div class="m1"><p>کتاب از دست دادن سست راییست</p></div>
<div class="m2"><p>که اغلب خوی مردم بیوفاییست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گرو بستان نه پایندان و سوگند</p></div>
<div class="m2"><p>که پایندان نباشد همچو پابند</p></div></div>