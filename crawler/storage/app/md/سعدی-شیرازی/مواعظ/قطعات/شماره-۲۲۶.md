---
title: >-
    شمارهٔ ۲۲۶
---
# شمارهٔ ۲۲۶

<div class="b" id="bn1"><div class="m1"><p>از من بگوی شاه رعیت نواز را</p></div>
<div class="m2"><p>منت منه که ملک خود آباد می‌کنی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>و ابله که تیشه بر قدم خویش می‌زند</p></div>
<div class="m2"><p>بدبخت گو ز دست که فریاد می‌کنی؟</p></div></div>