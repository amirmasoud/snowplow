---
title: >-
    شمارهٔ ۴۸
---
# شمارهٔ ۴۸

<div class="b" id="bn1"><div class="m1"><p>ای بلند اختر خدایت عمر جاویدان دهاد</p></div>
<div class="m2"><p>وآنچه پیروزی و بهروزی در آنست آن دهاد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جاودان نفس شریفت بندهٔ فرمان حق</p></div>
<div class="m2"><p>بعد از آن بر جملهٔ فرماندهان فرمان دهاد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من بدانم دولت عقبی به نان دادن درست</p></div>
<div class="m2"><p>تا عنان عمر در دستست دستت نان دهاد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>داعیان اندر دعا گویند پیش خسروان</p></div>
<div class="m2"><p>طاق ایوانت به رفعت بوسه بر کیوان دهاد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نعمتی را کز پی مرضات حق دریافتی</p></div>
<div class="m2"><p>حق تعالی از نعیم آخرت تاوان دهاد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای مبارک روز هر روزت به کام دوستان</p></div>
<div class="m2"><p>دولت تو در ترقی باد و دشمن جان دهاد</p></div></div>