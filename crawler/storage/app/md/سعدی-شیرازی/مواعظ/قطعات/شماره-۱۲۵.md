---
title: >-
    شمارهٔ ۱۲۵
---
# شمارهٔ ۱۲۵

<div class="b" id="bn1"><div class="m1"><p>یکی نصیحت درویش‌وار خواهم کرد</p></div>
<div class="m2"><p>اگر موافق شاه زمانه می‌آید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگرچه غالبی از دشمن ضعیف بترس</p></div>
<div class="m2"><p>که تیر آه سحر با نشانه می‌آید</p></div></div>