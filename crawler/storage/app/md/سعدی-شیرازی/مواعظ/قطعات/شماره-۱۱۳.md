---
title: >-
    شمارهٔ ۱۱۳
---
# شمارهٔ ۱۱۳

<div class="b" id="bn1"><div class="m1"><p>نگر تا نبینی ز ظلم شهی</p></div>
<div class="m2"><p>که از ظلم او سینه‌ها چاک بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ازیرا که دیدیم کز بد بتر</p></div>
<div class="m2"><p>بسی اندرین عالم خاک بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو شد روز آمد شب تیره رنگ</p></div>
<div class="m2"><p>چو جمشید بگذشت ضحاک بود</p></div></div>