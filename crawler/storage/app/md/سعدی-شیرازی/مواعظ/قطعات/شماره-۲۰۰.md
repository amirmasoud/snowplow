---
title: >-
    شمارهٔ ۲۰۰
---
# شمارهٔ ۲۰۰

<div class="b" id="bn1"><div class="m1"><p>نجس ار پیرهن شبلی و معروف بپوشد</p></div>
<div class="m2"><p>همه دانند که از سگ نتوان شست پلیدی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گرگ اگر نیز گنهکار نباشد به حقیقت</p></div>
<div class="m2"><p>جای آنست که گویند که یوسف تو دریدی</p></div></div>