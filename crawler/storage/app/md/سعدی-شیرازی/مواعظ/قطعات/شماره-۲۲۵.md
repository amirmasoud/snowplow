---
title: >-
    شمارهٔ ۲۲۵
---
# شمارهٔ ۲۲۵

<div class="b" id="bn1"><div class="m1"><p>ای که گر هر سر موییت زبانی دارد</p></div>
<div class="m2"><p>شکر یک نعمت از انعام خدایی نکنی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حق چندین کرم و رحمت و رأفت شرطست</p></div>
<div class="m2"><p>که به جای آوری و سست وفایی نکنی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پادشاهیت میسر نشود روز به خلق</p></div>
<div class="m2"><p>تا به شب بر در معبود گدایی نکنی</p></div></div>