---
title: >-
    شمارهٔ ۶۲
---
# شمارهٔ ۶۲

<div class="b" id="bn1"><div class="m1"><p>من هرگز آب چاه ندیدم چنین مداد</p></div>
<div class="m2"><p>بر یک ورق نویس که بر هفت بگذرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نی نی ورق چه باشد و کیمخت گوسفند</p></div>
<div class="m2"><p>از چرم گاو از سپر جفت بگذرد</p></div></div>