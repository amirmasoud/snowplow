---
title: >-
    شمارهٔ ۱۴
---
# شمارهٔ ۱۴

<div class="b" id="bn1"><div class="m1"><p>مرا گویند با دشمن برآویز</p></div>
<div class="m2"><p>گرت چالاکی و مردانگی هست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کسی بیهوده خون خویشتن ریخت؟</p></div>
<div class="m2"><p>کند هرگز چنین دیوانگی مست؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تو زر بر کف نمی‌یاری نهادن</p></div>
<div class="m2"><p>سپاهی چون نهد سر بر کف دست؟</p></div></div>