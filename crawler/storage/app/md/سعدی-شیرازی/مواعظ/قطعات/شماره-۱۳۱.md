---
title: >-
    شمارهٔ ۱۳۱
---
# شمارهٔ ۱۳۱

<div class="b" id="bn1"><div class="m1"><p>بردند پیمبران و پاکان</p></div>
<div class="m2"><p>از بی‌ادبان جفای بسیار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل تنگ من که پتک و سندان</p></div>
<div class="m2"><p>پیوسته درم زنند و دینار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>قدر زر و سیم کم نگردد</p></div>
<div class="m2"><p>و آهن نشود بزرگ مقدار</p></div></div>