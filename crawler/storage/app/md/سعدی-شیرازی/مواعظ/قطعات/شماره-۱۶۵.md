---
title: >-
    شمارهٔ ۱۶۵
---
# شمارهٔ ۱۶۵

<div class="b" id="bn1"><div class="m1"><p>مراد و مطلب دنیا و آخرت نبرد</p></div>
<div class="m2"><p>مگر کسی که جوانمرد باشد و بسام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو نیکنام شوی در زمانه ورنه بسست</p></div>
<div class="m2"><p>خدای عز وجل رزق خلق را قسام</p></div></div>