---
title: >-
    شمارهٔ ۳۱
---
# شمارهٔ ۳۱

<div class="b" id="bn1"><div class="m1"><p>دهل را کاندرون زندان بادست</p></div>
<div class="m2"><p>به گردون می‌رسد فریادش از پوست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چرا درد نهانی برد باید؟</p></div>
<div class="m2"><p>رها کن تا بداند دشمن و دوست</p></div></div>