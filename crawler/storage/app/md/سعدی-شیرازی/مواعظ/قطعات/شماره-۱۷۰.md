---
title: >-
    شمارهٔ ۱۷۰
---
# شمارهٔ ۱۷۰

<div class="b" id="bn1"><div class="m1"><p>مثل وقوفک عندالله فی ملاء</p></div>
<div class="m2"><p>یوم التغابن و استیقظ لمزدجر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یا فاعل الذنب هل ترضی لنفسک فی</p></div>
<div class="m2"><p>قید الاساری و اخوان علی سرر</p></div></div>