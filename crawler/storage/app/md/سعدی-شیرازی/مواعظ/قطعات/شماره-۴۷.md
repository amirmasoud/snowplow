---
title: >-
    شمارهٔ ۴۷
---
# شمارهٔ ۴۷

<div class="b" id="bn1"><div class="m1"><p>بر تربت دوستان ماضی</p></div>
<div class="m2"><p>بگذشت بسی ز بوستان باد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر بر سر خاک ما رود نیز</p></div>
<div class="m2"><p>سهلست بقای دوستان باد</p></div></div>