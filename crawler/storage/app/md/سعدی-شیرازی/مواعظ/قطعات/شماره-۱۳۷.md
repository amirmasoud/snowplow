---
title: >-
    شمارهٔ ۱۳۷
---
# شمارهٔ ۱۳۷

<div class="b" id="bn1"><div class="m1"><p>نگین ختم رسالت پیمبر عربی</p></div>
<div class="m2"><p>شفیع روز قیامت محمد مختار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگر نه واسطهٔ موی و روی او بودی</p></div>
<div class="m2"><p>خدای خلق نگفتی قسم به لیل و نهار</p></div></div>