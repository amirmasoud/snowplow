---
title: >-
    شمارهٔ ۱۰۳
---
# شمارهٔ ۱۰۳

<div class="b" id="bn1"><div class="m1"><p>الحق امنای مال ایتام</p></div>
<div class="m2"><p>همچون تو حلال‌زاده بایند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هرگز زن و مرد و کفر و اسلام</p></div>
<div class="m2"><p>نفس از تو خبیث‌تر نزایند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اطفال عزیز نازپرورد</p></div>
<div class="m2"><p>از دست تو دست بر خدایند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>طفلان تو را پدر بمیراد</p></div>
<div class="m2"><p>تا جور وصی بیازمایند</p></div></div>