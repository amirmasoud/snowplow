---
title: >-
    شمارهٔ ۱۱۱
---
# شمارهٔ ۱۱۱

<div class="b" id="bn1"><div class="m1"><p>هر که بر روی زمین مهلت عیشی دارد</p></div>
<div class="m2"><p>ای بسا روز که در زیر زمین خواهد بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کشتی آرام نگیرد که بود بر سر آب</p></div>
<div class="m2"><p>تا جهان بر سر آبست چنین خواهد بود</p></div></div>