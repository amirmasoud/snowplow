---
title: >-
    شمارهٔ ۱۱
---
# شمارهٔ ۱۱

<div class="b" id="bn1"><div class="m1"><p>چو خویشتن نتواند که می‌خورد قاضی</p></div>
<div class="m2"><p>ضرورتست که بر دیگران بگیرد سخت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که گفت پیرزن از میوه می‌کند پرهیز؟</p></div>
<div class="m2"><p>دروغ گفت که دستش نمی‌رسد به درخت</p></div></div>