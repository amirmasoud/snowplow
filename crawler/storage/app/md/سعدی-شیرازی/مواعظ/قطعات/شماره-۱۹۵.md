---
title: >-
    شمارهٔ ۱۹۵
---
# شمارهٔ ۱۹۵

<div class="b" id="bn1"><div class="m1"><p>طبیبی را حکایت کرد پیری</p></div>
<div class="m2"><p>که می‌گردد سرم چون آسیایی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نه گوشی ماند فهمم را نه هوشی</p></div>
<div class="m2"><p>نه دستی ماند جهدم را نه پایی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نه دیدن می‌توانم بی‌تأمل</p></div>
<div class="m2"><p>نه رفتن می‌توانم بی‌عصایی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>روان دردمندم را ببندیش</p></div>
<div class="m2"><p>اگر دستت دهد تدبیر و رایی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>وگر دانی که چشمم را بسازد</p></div>
<div class="m2"><p>بساز از بهر چشمم توتیایی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ندیدم در جهان چون خاک شیراز</p></div>
<div class="m2"><p>وزین ناسازتر آب و هوایی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گرم پای سفر بودی و رفتار</p></div>
<div class="m2"><p>تحول کردمی زینجا به جایی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>حکایت برگرفت آن پیر فرتوت</p></div>
<div class="m2"><p>ز جور دور گیتی ماجرایی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>طبیب محترم درماند عاجز</p></div>
<div class="m2"><p>ز دستش تا به گردن در بلایی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بگفتا صبر کن بر درد پیری</p></div>
<div class="m2"><p>که جز مرگش نمی‌بینم دوایی</p></div></div>