---
title: >-
    شمارهٔ ۱۹۹
---
# شمارهٔ ۱۹۹

<div class="b" id="bn1"><div class="m1"><p>رحم الله معشر الماضین</p></div>
<div class="m2"><p>که به مردی قدم سپردندی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>راحت جان بندگان خدای</p></div>
<div class="m2"><p>راحت جان خود شمردندی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کاش آنان چو زنده می‌نشوند</p></div>
<div class="m2"><p>باری این ناکسان بمردندی</p></div></div>