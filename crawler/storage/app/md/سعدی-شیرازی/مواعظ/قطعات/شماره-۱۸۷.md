---
title: >-
    شمارهٔ ۱۸۷
---
# شمارهٔ ۱۸۷

<div class="b" id="bn1"><div class="m1"><p>نه نیکان را بد افتادست هرگز</p></div>
<div class="m2"><p>نه بدکردار را فرجام نیکو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بدان رفتند و نیکان هم نماندند</p></div>
<div class="m2"><p>چه ماند؟ نام زشت و نام نیکو</p></div></div>