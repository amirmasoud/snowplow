---
title: >-
    شمارهٔ ۱۹۲
---
# شمارهٔ ۱۹۲

<div class="b" id="bn1"><div class="m1"><p>نخواهی کز بزرگان جور بینی</p></div>
<div class="m2"><p>عزیز من به خردان برببخشای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگر طاقت نداری صدمت پیل</p></div>
<div class="m2"><p>چرا باید که بر موران نهی پای؟</p></div></div>