---
title: >-
    شمارهٔ ۱۶۳
---
# شمارهٔ ۱۶۳

<div class="b" id="bn1"><div class="m1"><p>خطاب حاکم عادل مثال بارانست</p></div>
<div class="m2"><p>چه در حدیقهٔ سلطان چه بر کنیسهٔ عام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگر رعایت خلقست منصف همه باش</p></div>
<div class="m2"><p>نه مال زید حلالست و خون عمرو حرام</p></div></div>