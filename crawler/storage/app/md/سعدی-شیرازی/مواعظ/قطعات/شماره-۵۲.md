---
title: >-
    شمارهٔ ۵۲
---
# شمارهٔ ۵۲

<div class="b" id="bn1"><div class="m1"><p>خون دار اگرچه دشمن خردست زینهار</p></div>
<div class="m2"><p>مهمل رها مکن که زمانش بپرورد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا کعب کودکی بود آغاز چشمه سار</p></div>
<div class="m2"><p>چون پیشتر رود ز سر مرد بگذرد</p></div></div>