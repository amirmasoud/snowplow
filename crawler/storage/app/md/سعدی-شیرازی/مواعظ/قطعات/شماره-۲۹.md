---
title: >-
    شمارهٔ ۲۹
---
# شمارهٔ ۲۹

<div class="b" id="bn1"><div class="m1"><p>ره نمودن به خیر ناکس را</p></div>
<div class="m2"><p>پیش اعمی چراغ داشتنست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نیکویی با بدان و بی‌ادبان</p></div>
<div class="m2"><p>تخم در شوره‌بوم کاشتنست</p></div></div>