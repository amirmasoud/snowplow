---
title: >-
    شمارهٔ ۱۴۳
---
# شمارهٔ ۱۴۳

<div class="b" id="bn1"><div class="m1"><p>جزای نیک و بد خلق با خدای انداز</p></div>
<div class="m2"><p>که دست ظلم نماند چنین که هست دراز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو راستی کن و با گردش زمانه بساز</p></div>
<div class="m2"><p>که مکر هم به خداوند مکر گردد باز</p></div></div>