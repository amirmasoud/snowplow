---
title: >-
    شمارهٔ ۲۱۸
---
# شمارهٔ ۲۱۸

<div class="b" id="bn1"><div class="m1"><p>نبایدت که پریشان شود قواعد ملک</p></div>
<div class="m2"><p>نگاه دار دل مردم از پریشانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چنانکه طایفه‌ای در پناه جاه تواند</p></div>
<div class="m2"><p>تو در پناه دعا و نماز ایشانی</p></div></div>