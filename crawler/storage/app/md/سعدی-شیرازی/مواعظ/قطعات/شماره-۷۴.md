---
title: >-
    شمارهٔ ۷۴
---
# شمارهٔ ۷۴

<div class="b" id="bn1"><div class="m1"><p>تشنهٔ سوخته در چشمهٔ روشن چو رسید</p></div>
<div class="m2"><p>تو مپندار که از سیل دمان اندیشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ملحد گرسنه و خانهٔ خالی و طعام</p></div>
<div class="m2"><p>عقل باور نکند کز رمضان اندیشد</p></div></div>