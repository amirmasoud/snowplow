---
title: >-
    شمارهٔ ۹۳
---
# شمارهٔ ۹۳

<div class="b" id="bn1"><div class="m1"><p>بدین الحان داودی عجب نیست</p></div>
<div class="m2"><p>که مرغان هوا حیران بمانند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خدای این حافظان ناخوش آواز</p></div>
<div class="m2"><p>بیامرزاد اگر ساکن بخوانند</p></div></div>