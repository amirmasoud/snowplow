---
title: >-
    شمارهٔ ۲۰۵
---
# شمارهٔ ۲۰۵

<div class="b" id="bn1"><div class="m1"><p>ای پسندیده حیف بر درویش</p></div>
<div class="m2"><p>تا دل پادشه به دست آری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو برای قبول و منصب خویش</p></div>
<div class="m2"><p>حیف باشد که حق بیازاری</p></div></div>