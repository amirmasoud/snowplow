---
title: >-
    شمارهٔ ۱۲۹
---
# شمارهٔ ۱۲۹

<div class="b" id="bn1"><div class="m1"><p>برای ختم سخن دست بر دعا داریم</p></div>
<div class="m2"><p>امیدوار قبول از مهیمن غفار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همیشه تا که فلک را بود تقلب دور</p></div>
<div class="m2"><p>مدام تا که زمین را بود ثبات و قرار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ثبات عمر تو باد و دوام عافیتت</p></div>
<div class="m2"><p>نگاهداشته از نائبات لیل و نهار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تو حاکم همه آفاق وآنکه حاکم تست</p></div>
<div class="m2"><p>ز بخت و تخت جوانی و ملک برخوردار</p></div></div>