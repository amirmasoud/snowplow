---
title: >-
    شمارهٔ ۱۴۲
---
# شمارهٔ ۱۴۲

<div class="b" id="bn1"><div class="m1"><p>دل منه بر جهان که دور بقا</p></div>
<div class="m2"><p>می‌رود همچو سیل سر در زیر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پیر دیگر جوان نخواهد شد</p></div>
<div class="m2"><p>پیریش نیز هم نماند دیر</p></div></div>