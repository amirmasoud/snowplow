---
title: >-
    شمارهٔ ۱۱۲
---
# شمارهٔ ۱۱۲

<div class="b" id="bn1"><div class="m1"><p>اگر ملازم خاک در کسی باشی</p></div>
<div class="m2"><p>چو آستانه ندیم خسیت باید بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز بهر نعمت دنیا که خاک بر سر او</p></div>
<div class="m2"><p>برین مثال که گفتم بسیت باید بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هزار سال تنعم کنی بدان نرسد</p></div>
<div class="m2"><p>که یک زمان به مراد کسیت باید بود</p></div></div>