---
title: >-
    شمارهٔ ۱۵۴
---
# شمارهٔ ۱۵۴

<div class="b" id="bn1"><div class="m1"><p>دوش مرغی به صبح می‌نالید</p></div>
<div class="m2"><p>عقل و صبرم ببرد و طاقت و هوش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یکی از دوستان مخلص را</p></div>
<div class="m2"><p>مگر آواز من رسید به گوش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفت باور نداشتم که تو را</p></div>
<div class="m2"><p>بانگ مرغی چنین کند مدهوش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفتم این شرط آدمیت نیست</p></div>
<div class="m2"><p>مرغ تسبیح خوان و من خاموش</p></div></div>