---
title: >-
    شمارهٔ ۵۴
---
# شمارهٔ ۵۴

<div class="b" id="bn1"><div class="m1"><p>مرد دیگر جوان نخواهد بود</p></div>
<div class="m2"><p>پیریش هم بقا نخواهد کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون درخت خزان که زرد شود</p></div>
<div class="m2"><p>کاشکی همچنان بماندی زرد</p></div></div>