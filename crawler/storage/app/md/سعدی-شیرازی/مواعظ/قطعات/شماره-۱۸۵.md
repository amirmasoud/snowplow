---
title: >-
    شمارهٔ ۱۸۵
---
# شمارهٔ ۱۸۵

<div class="b" id="bn1"><div class="m1"><p>هان ای نهاده تیر جفا در کمان حکم</p></div>
<div class="m2"><p>اندیشه کن ز ناوک دلدوز در کمین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر تیر تو ز جوشن فولاد بگذرد</p></div>
<div class="m2"><p>پیکان آه بگذرد از کوه آهنین</p></div></div>