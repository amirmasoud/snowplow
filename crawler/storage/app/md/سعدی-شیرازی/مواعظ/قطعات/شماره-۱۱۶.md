---
title: >-
    شمارهٔ ۱۱۶
---
# شمارهٔ ۱۱۶

<div class="b" id="bn1"><div class="m1"><p>هر که بینی مراد و راحت خویش</p></div>
<div class="m2"><p>از همه خلق بیشتر خواهد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>و آن میسر شود به کوشش و رنج</p></div>
<div class="m2"><p>که قضا بخشد و قدر خواهد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای که می‌خواهی از نگارین کام</p></div>
<div class="m2"><p>با نگارش بگوی اگر خواهد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دختر اندر شکم پسر نشود</p></div>
<div class="m2"><p>گرچه بابا همی پسر خواهد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تیز در ریش کاروانسالار</p></div>
<div class="m2"><p>گر بدان ده رود که خر خواهد</p></div></div>