---
title: >-
    شمارهٔ ۳۵
---
# شمارهٔ ۳۵

<div class="b" id="bn1"><div class="m1"><p>در سرای به هم کرده از پس پرده</p></div>
<div class="m2"><p>مباش غره که هیچ آفریده واقف نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از آن بترس که مکنون غیب می‌داند</p></div>
<div class="m2"><p>گرش بلند بخوانی وگر نهفته یکیست</p></div></div>