---
title: >-
    شمارهٔ ۷۱
---
# شمارهٔ ۷۱

<div class="b" id="bn1"><div class="m1"><p>سخن گفته دگر باز نیاید به دهن</p></div>
<div class="m2"><p>اول اندیشه کند مرد که عاقل باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا زمانی دگر اندیشه نباید کردن</p></div>
<div class="m2"><p>که چرا گفتم و اندیشهٔ باطل باشد</p></div></div>