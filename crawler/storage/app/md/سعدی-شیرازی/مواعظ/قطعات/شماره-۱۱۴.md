---
title: >-
    شمارهٔ ۱۱۴
---
# شمارهٔ ۱۱۴

<div class="b" id="bn1"><div class="m1"><p>روز قالی فشاندنست امروز</p></div>
<div class="m2"><p>تا غبار از میان ما برود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون مگس در سرای گرد آمد</p></div>
<div class="m2"><p>خوان نباید نهاد تا برود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر که ناخوانده آید از در قوم</p></div>
<div class="m2"><p>نیک باشد که ناشتا برود</p></div></div>