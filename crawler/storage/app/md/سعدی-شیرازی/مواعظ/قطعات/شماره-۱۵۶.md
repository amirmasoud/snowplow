---
title: >-
    شمارهٔ ۱۵۶
---
# شمارهٔ ۱۵۶

<div class="b" id="bn1"><div class="m1"><p>مگسی گفت عنکبوتی را</p></div>
<div class="m2"><p>کاین چه ساقست و ساعد باریک</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفت اگر در کمند من افتی</p></div>
<div class="m2"><p>پیش چشمت جهان کنم تاریک</p></div></div>