---
title: >-
    شمارهٔ ۱۶۷
---
# شمارهٔ ۱۶۷

<div class="b" id="bn1"><div class="m1"><p>مردکی غرقه بود در جیحون</p></div>
<div class="m2"><p>در سمرقند بود پندارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بانگ می‌کرد و زار می‌نالید</p></div>
<div class="m2"><p>که دریغا کلاه و دستارم</p></div></div>