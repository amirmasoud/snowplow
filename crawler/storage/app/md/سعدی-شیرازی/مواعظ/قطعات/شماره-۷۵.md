---
title: >-
    شمارهٔ ۷۵
---
# شمارهٔ ۷۵

<div class="b" id="bn1"><div class="m1"><p>هیچ دانی که آب دیدهٔ پیر</p></div>
<div class="m2"><p>از دو چشم جوان چرا نچکد؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>برف بر بام سالخوردهٔ ماست</p></div>
<div class="m2"><p>آب در خانهٔ شما نچکد</p></div></div>