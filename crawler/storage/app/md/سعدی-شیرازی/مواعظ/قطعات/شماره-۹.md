---
title: >-
    شمارهٔ ۹
---
# شمارهٔ ۹

<div class="b" id="bn1"><div class="m1"><p>احدا سامع المناجات</p></div>
<div class="m2"><p>صمدا کافی المهمات</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هیچ پوشیده از تو پنهان نیست</p></div>
<div class="m2"><p>عالم السر و الخفیات</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زیر و بالا نمی‌توانم گفت</p></div>
<div class="m2"><p>خالق الارض والسموات</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شکر و حمد تو چون توانم گفت</p></div>
<div class="m2"><p>حافظ فی جمع حالات</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر دعایی که می‌کند سعدی</p></div>
<div class="m2"><p>فاستجب یا مجیب دعوات</p></div></div>