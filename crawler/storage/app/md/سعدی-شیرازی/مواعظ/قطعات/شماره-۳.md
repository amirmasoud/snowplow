---
title: >-
    شمارهٔ ۳
---
# شمارهٔ ۳

<div class="b" id="bn1"><div class="m1"><p>سپاس دار خدای لطیف دانا را</p></div>
<div class="m2"><p>که لطف کرد و به هم برگماشت اعدا را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همیشه باد خصومت جهود و ترسا را</p></div>
<div class="m2"><p>که مرگ هر دو طرف تهنیت بود ما را</p></div></div>