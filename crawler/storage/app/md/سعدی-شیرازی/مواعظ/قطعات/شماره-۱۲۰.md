---
title: >-
    شمارهٔ ۱۲۰
---
# شمارهٔ ۱۲۰

<div class="b" id="bn1"><div class="m1"><p>حقیقتیست که دانا سرای عاریتی</p></div>
<div class="m2"><p>ز بهر هشتن و پرداختن نفرماید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من این مقام نه از بهر آن بنا کردم</p></div>
<div class="m2"><p>که پنج روز بقا اعتماد را شاید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خلاف عهد زمان بی‌خلاف معلومست</p></div>
<div class="m2"><p>که هیچ نوع نبخشد که باز نرباید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بلی به نیت آن تا چو رخت بربندم</p></div>
<div class="m2"><p>به جای من دگری همچنین بیاساید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ازین قدر نگریزد که مرغ و ماهی را</p></div>
<div class="m2"><p>به قدر خویش حقیر آشیانه‌ای باید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سرای دام همایست نیک‌بختان را</p></div>
<div class="m2"><p>بود که در همه عمرت یکی به دام آید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بسا کسا که گرش در به روی بگشایی</p></div>
<div class="m2"><p>سعادت ابدت در به روی بگشاید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>حلال نیست که صورت کنند بر دیوار</p></div>
<div class="m2"><p>که رد شرع بود زو خلل بیفزاید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>همین نصیحت سعدی به آب زر بنویس</p></div>
<div class="m2"><p>که خانه را کس ازین خوبتر نیاراید</p></div></div>