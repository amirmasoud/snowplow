---
title: >-
    شمارهٔ ۱۰۵
---
# شمارهٔ ۱۰۵

<div class="b" id="bn1"><div class="m1"><p>امیر ما عسل از دست خلق می‌نخورد</p></div>
<div class="m2"><p>که زهر در قدح انگبین تواند بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عجب که در عسل از زهر می‌کند پرهیز</p></div>
<div class="m2"><p>حذر نمی‌کند از تیر آه زهرآلود</p></div></div>