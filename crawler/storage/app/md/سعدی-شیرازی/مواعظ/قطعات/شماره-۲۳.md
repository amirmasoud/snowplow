---
title: >-
    شمارهٔ ۲۳
---
# شمارهٔ ۲۳

<div class="b" id="bn1"><div class="m1"><p>در چشمت ار حقیر بود صورت فقیر</p></div>
<div class="m2"><p>کوته نظر مباش که در سنگ گوهرست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کیمخت نافه را که حقیرست و شوخگن</p></div>
<div class="m2"><p>قیمت بدان کنند که پر مشک اذفرست</p></div></div>