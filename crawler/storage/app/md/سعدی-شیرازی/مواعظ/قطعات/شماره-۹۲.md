---
title: >-
    شمارهٔ ۹۲
---
# شمارهٔ ۹۲

<div class="b" id="bn1"><div class="m1"><p>آسیا سنگ ده هزار منی</p></div>
<div class="m2"><p>به دور مرد از کمر بگردانند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>لیکن از زیر به زبر بردن</p></div>
<div class="m2"><p>به هزار آدمیش نتوانند</p></div></div>