---
title: >-
    در مرثیهٔ ابوبکر سعد بن زنگی
---
# در مرثیهٔ ابوبکر سعد بن زنگی

<div class="b" id="bn1"><div class="m1"><p>دل شکسته که مرهم نهد دگربارش؟</p></div>
<div class="m2"><p>یتیم خسته که از پای برکند خارش؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خدنگ درد فراق اندرون سینهٔ خلق</p></div>
<div class="m2"><p>چنان نشست که در جان نشست سوفارش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو مرغ کشته قلم سر بریده می‌گردد</p></div>
<div class="m2"><p>چنانکه خون سیه می‌رود ز منقارش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دهان مرده به معنی سخن همی گوید</p></div>
<div class="m2"><p>اگرچه نیست به صورت زبان گفتارش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>که زینهار به دنیا و مال غره مباش</p></div>
<div class="m2"><p>بخواهدت به ضرورت گذاشت یک‌بارش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چه سود کاسهٔ زرین و شربت مسموم</p></div>
<div class="m2"><p>دریغ گنج بقا گر نبودی این مارش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بس اعتماد مکن بر دوام دولت دهر</p></div>
<div class="m2"><p>که آزمودهٔ خلق است خوی غدارش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نظر به حال خداوند دین و دولت کن</p></div>
<div class="m2"><p>که فیض رحمت حق بر روان هشیارش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سپهر تاج کیانی ز تارکش برداشت</p></div>
<div class="m2"><p>نهاد بر سر تربت کلاه و دستارش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گرت به شهد و شکر پرورد زمانهٔ دون</p></div>
<div class="m2"><p>وفای عهد ندارد به دوست مشمارش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دگر شکوفه نخندد به باغ فیروزی</p></div>
<div class="m2"><p>که خون همی رود از دیده‌های اشجارش</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چگونه غم نخورد در فراق او درویش</p></div>
<div class="m2"><p>که غم فزون شد و از سر برفت غمخوارش</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>امیدوار وجودی که از جهان برود</p></div>
<div class="m2"><p>میان خلق بماند به نیکی آثارش</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>از آب چشم عزیزان که بر بساط بریخت</p></div>
<div class="m2"><p>به روز باران مانست صفهٔ بارش</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نظر به حال چنین روز بود در همه عمر</p></div>
<div class="m2"><p>نماز نیم‌شبان و دعای اسحارش</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گمان مبر که به تنهاست در حظیرهٔ خاک</p></div>
<div class="m2"><p>قرین گور و قیامت بسست کردارش</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>گرش ولایت و فرمان و گنج و مال نماند</p></div>
<div class="m2"><p>بماند رحمت پروردگار غفارش</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>قضای حکم ازل بود روز ختم عمل</p></div>
<div class="m2"><p>دگر چه فایده تعداد ذکر و کردارش</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ولیک دوست بگرید به زاری از پی دوست</p></div>
<div class="m2"><p>اگرچه باز نگردد به گریهٔ زارش</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>غمی رسید به روی زمانه از تقدیر</p></div>
<div class="m2"><p>که پشت طاقت گردون دو تا کند بارش</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>همین جراحت و غم بود کز فراق رسول</p></div>
<div class="m2"><p>به روزگار مهاجر رسید و انصارش</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>برفت سایهٔ درویش و سترپوش غریب</p></div>
<div class="m2"><p>بپوش بار خدایا به عفو ستارش</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>به خیل خانهٔ کروبیان عالم قدس</p></div>
<div class="m2"><p>به گرد خیمهٔ روحانیون فرود آرش</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>عدو که گفت به غوغا که درگذشتن دوست</p></div>
<div class="m2"><p>جهان خراب شود سهو بود پندارش</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>هم آن درخت نبود اندرین حدیقهٔ ملک</p></div>
<div class="m2"><p>که بعد از این متفرق شوند اطیارش</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>نمرد نام ابوبکر سعد بن زنگی</p></div>
<div class="m2"><p>که ماند سعد ابوبکر نامبردارش</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>چراغ را که چراغی ازو فرا گیرند</p></div>
<div class="m2"><p>فرو نشیند و باقی بماند انوارش</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>خدایگان زمان و زمین مظفر دین</p></div>
<div class="m2"><p>که قائمست به اعلاء دین و اظهارش</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بزرگوار خدایا به فر و دولت و کام</p></div>
<div class="m2"><p>دوام عمر بده سالهای بسیارش</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>به نیک مردان کز چشم بد بپرهیزش</p></div>
<div class="m2"><p>به راستان که ز ناراستان نگه دارش</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>که نقطه تا متمکن نباشد اندر اصل</p></div>
<div class="m2"><p>درست باز نیامد حساب پرگارش</p></div></div>