---
title: >-
    در مرثیهٔ سعد بن ابوبکر
---
# در مرثیهٔ سعد بن ابوبکر

<div class="b" id="bn1"><div class="m1"><p>به هیچ باغ نبود آن درخت مانندش</p></div>
<div class="m2"><p>که تندباد اجل بی‌دریغ برکندش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به دوستی جهان بر که اعتماد کند؟</p></div>
<div class="m2"><p>که شوخ دیده نظر با کسیست هر چندش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به لطف خویش خدایا روان او خوش دار</p></div>
<div class="m2"><p>بدان حیات بکن زین حیات خرسندش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نمرد سعد ابوبکر سعد بن زنگی</p></div>
<div class="m2"><p>که هست سایهٔ امیدوار فرزندش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر آفتاب بشد سایه همچنان باقیست</p></div>
<div class="m2"><p>بقای اهل حرم باد و خویش و پیوندش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همیشه سبز و جوان باد در حدیقهٔ ملک</p></div>
<div class="m2"><p>درخت دولت بیخ‌آور برومندش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یکی دعای تو گفتم یکی دعای عدوت</p></div>
<div class="m2"><p>بگویم آن را گر نیک نیست مپسندش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هر آنکه پای خلاف تو در رکیب آورد</p></div>
<div class="m2"><p>به خانه باز رود اسب بی‌خداوندش</p></div></div>