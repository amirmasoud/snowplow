---
title: >-
    در زوال خلافت بنی‌عباس
---
# در زوال خلافت بنی‌عباس

<div class="b" id="bn1"><div class="m1"><p>آسمان را حق بود گر خون بگرید بر زمین</p></div>
<div class="m2"><p>بر زوال ملک مستعصم امیرالمؤمنین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای محمد گر قیامت می‌برآری سر ز خاک</p></div>
<div class="m2"><p>سر برآور وین قیامت در میان خلق بین</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نازنینان حرم را خون خلق بی‌دریغ</p></div>
<div class="m2"><p>ز آستان بگذشت و ما را خون چشم از آستین</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زینهار از دور گیتی، و انقلاب روزگار</p></div>
<div class="m2"><p>در خیال کس نیامد کانچنان گردد چنین</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دیده بردار ای که دیدی شوکت باب‌الحرم</p></div>
<div class="m2"><p>قیصران روم سر بر خاک و خاقانان چین</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خون فرزندان عم مصطفی شد ریخته</p></div>
<div class="m2"><p>هم بر آن خاکی که سلطانان نهادندی جبین</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>وه که گر بر خون آن پاکان فرود آید مگس</p></div>
<div class="m2"><p>تا قیامت در دهانش تلخ گردد انگبین</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بعد از این آسایش از دنیا نشاید چشم داشت</p></div>
<div class="m2"><p>قیر در انگشتری ماند چو برخیزد نگین</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دجله خونابست ازین پس گر نهد سر در نشیب</p></div>
<div class="m2"><p>خاک نخلستان بطحا را کند در خون عجین</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>روی دریا در هم آمد زین حدیث هولناک</p></div>
<div class="m2"><p>می‌توان دانست بر رویش ز موج افتاده چین</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گریه بیهودست و بیحاصل بود شستن به آب</p></div>
<div class="m2"><p>آدمی را حسرت از دل و اسب را داغ از سرین</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نوحه لایق نیست بر خاک شهیدان زانکه هست</p></div>
<div class="m2"><p>کمترین دولت ایشان را بهشت برترین</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>لیکن از روی مسلمانی و کوی مرحمت</p></div>
<div class="m2"><p>مهربان را دل بسوزد بر فراق نازنین</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>باش تا فردا که بینی روز داد و رستخیز</p></div>
<div class="m2"><p>وز لحد با زخم خون‌آلوده برخیزد دفین</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بر زمین خاک قدمشان توتیای چشم بود</p></div>
<div class="m2"><p>روز محشر خونشان گلگونهٔ حوران عین</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>قالب مجروح اگر در خاک و خون غلطد چه باک</p></div>
<div class="m2"><p>روح پاک اندر جوار لطف رب‌العالمین</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>تکیه بر دنیا نشاید کرد و دل بر وی نهاد</p></div>
<div class="m2"><p>کاسمان گاهی به مهرست ای برادر گه به کین</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چرخ گردان بر زمین گویی دو سنگ آسیاست</p></div>
<div class="m2"><p>در میان هر دو روز و شب دل مردم طحین</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>زور بازوی شجاعت برنتابد با اجل</p></div>
<div class="m2"><p>چون قضا آمد نماند قوت رای رزین</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>تیغ هندی برنیاید روز پیکار از نیام</p></div>
<div class="m2"><p>شیرمردی را که باشد مرگ پنهان در کمین</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>تجربت بی‌فایده است آنجا که برگردید بخت</p></div>
<div class="m2"><p>حمله آوردن چه سود آن را که در گردید زین</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>کرکسانند از پی مردار دنیا جنگجوی</p></div>
<div class="m2"><p>ای برادر گر خردمندی چو سیمرغان نشین</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ملک دنیا را چه قیمت حاجت اینست از خدای</p></div>
<div class="m2"><p>گو نگه دارد به ما بر ملک ایمان و یقین</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>یارب این رکن مسلمانی به امن‌آباد دار</p></div>
<div class="m2"><p>در پناه شاه عادل پیشوای ملک و دین</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>خسرو صاحبقران غوث زمان بوبکر سعد</p></div>
<div class="m2"><p>آنکه اخلاقش پسندیدست و اوصافش گزین</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>مصلحت بود اختیار رای روشن‌بین او</p></div>
<div class="m2"><p>با زبردستان سخن گفتن نشاید جز به لین</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>لاجرم در بر و بحرش داعیان دولتند</p></div>
<div class="m2"><p>کای هزاران آفرین بر جانت از جان آفرین</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>روزگارت با سعادت باد و سعدت پایدار</p></div>
<div class="m2"><p>رایتت منصور و بختت بار و اقبالت معین</p></div></div>