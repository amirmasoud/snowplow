---
title: >-
    غزل شمارهٔ  ۵۷
---
# غزل شمارهٔ  ۵۷

<div class="b" id="bn1"><div class="m1"><p>آستین بر روی و نقشی در میان افکنده‌ای</p></div>
<div class="m2"><p>خویشتن پنهان و شوری در جهان افکنده‌ای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همچنان در غنچه و آشوب استیلای عشق</p></div>
<div class="m2"><p>در نهاد بلبل فریاد خوان افکنده‌ای</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر یکی نادیده از رویت نشانی می‌دهند</p></div>
<div class="m2"><p>پرده بردار ای که خلقی در گمان افکنده‌ای</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آنچنان رویت نمی‌باید که با بیچارگان</p></div>
<div class="m2"><p>در میان آری حدیثی در میان افکنده‌ای</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هیچ نقاشت نمی‌بیند که نقشی بر کند</p></div>
<div class="m2"><p>و آنکه دید از حیرتش کلک از بنان افکنده‌ای</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>این دریغم می‌کشد کافکنده‌ای اوصاف خویش</p></div>
<div class="m2"><p>در زبان عام و خاصان را زبان افکنده‌ای</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>حاکمی بر زیردستان هر چه فرمایی رواست</p></div>
<div class="m2"><p>پنجهٔ زورآزما با ناتوان افکنده‌ای</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون صدف امید می‌دارم که لؤلؤیی شود</p></div>
<div class="m2"><p>قطره‌ای کز ابر لطفم در دهان افکنده‌ای</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سر به خدمت می‌نهادم چون بدیدم نیک باز</p></div>
<div class="m2"><p>چون سر سعدی بسی بر آستان افکنده‌ای</p></div></div>