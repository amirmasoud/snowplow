---
title: >-
    غزل شمارهٔ  ۴۴
---
# غزل شمارهٔ  ۴۴

<div class="b" id="bn1"><div class="m1"><p>باد گلبوی سحر خوش می‌وزد خیز ای ندیم</p></div>
<div class="m2"><p>بس که خواهد رفت بر بالای خاک ما نسیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای که در دنیا نرفتی بر صراط مستقیم</p></div>
<div class="m2"><p>در قیامت بر صراطت جای تشویش است و بیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>قلب زر اندوده نستانند در بازار حشر</p></div>
<div class="m2"><p>خالصی باید که بیرون آید از آتش سلیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عیبت از بیگانه پوشیده‌ست و می‌بیند بصیر</p></div>
<div class="m2"><p>فعلت از همسایه پنهان است و می‌داند علیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نفس پروردن خلاف رای دانشمند بود</p></div>
<div class="m2"><p>طفل خرما دوست دارد صبر فرماید حکیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>راه نومیدی گرفتم رحمتم دل می‌دهد</p></div>
<div class="m2"><p>کای گنه‌کاران هنوز امید عفو است از کریم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر بسوزانی خداوندا جزای فعل ماست</p></div>
<div class="m2"><p>ور ببخشی رحمتت عام است و احسانت قدیم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر چه شیطان رجیم از راه انصافم ببرد</p></div>
<div class="m2"><p>همچنان امید می‌دارم به رحمان رحیم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آن که جان بخشید و روزی داد و چندین لطف کرد</p></div>
<div class="m2"><p>هم ببخشاید چو مشتی استخوان باشم رمیم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سعدیا بسیار گفتن عمر ضایع کردن است</p></div>
<div class="m2"><p>وقت عذر آوردن است استغفرالله العظیم</p></div></div>