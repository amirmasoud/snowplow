---
title: >-
    غزل شمارهٔ  ۶۴
---
# غزل شمارهٔ  ۶۴

<div class="b" id="bn1"><div class="m1"><p>یاری آن است که زهر از قبلش نوش کنی</p></div>
<div class="m2"><p>نه چو رنجی رسدت یار فراموش کنی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هاون از یار جفا بیند و تسلیم شود</p></div>
<div class="m2"><p>تو چه یاری که چو دیگ از غم دل جوش کنی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>علم از دوش بنه ور عسلی فرماید</p></div>
<div class="m2"><p>شرط آزادگی آن است که بر دوش کنی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>راه دانا دگر و مذهب عاشق دگر است</p></div>
<div class="m2"><p>ای خردمند که عیب من مدهوش کنی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شاهد آن وقت بیاید که تو حاضر گردی</p></div>
<div class="m2"><p>مطرب آن گاه بگوید که تو خاموش کنی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سر تشنیع نداری طلب یار مکن</p></div>
<div class="m2"><p>مگست نیش زند چون طلب نوش کنی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پای در سلسله باید که همان لذت عشق</p></div>
<div class="m2"><p>در تو باشد که گرش دست در آغوش کنی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مرد باید که نظر بر ملخ و مور کند</p></div>
<div class="m2"><p>آن تأمل که تو در زلف و بناگوش کنی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تا چه شکلی تو در آیینه همان خواهی دید</p></div>
<div class="m2"><p>شاهد آیینهٔ توست ار نظر هوش کنی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سخن معرفت از حلقهٔ درویشان پرس</p></div>
<div class="m2"><p>سعدیا شاید از این حلقه که در گوش کنی</p></div></div>