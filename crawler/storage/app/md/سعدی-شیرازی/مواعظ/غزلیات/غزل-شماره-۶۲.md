---
title: >-
    غزل شمارهٔ  ۶۲
---
# غزل شمارهٔ  ۶۲

<div class="b" id="bn1"><div class="m1"><p>اگر لذت ترک لذت بدانی</p></div>
<div class="m2"><p>دگر شهوت نفس، لذت نخوانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هزاران در از خلق بر خود ببندی</p></div>
<div class="m2"><p>گرت باز باشد دری آسمانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سفرهای علوی کند مرغ جانت</p></div>
<div class="m2"><p>گر از چنبر آز بازش پرانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ولیکن تو را صبر عنقا نباشد</p></div>
<div class="m2"><p>که در دام شهوت به گنجشک مانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز صورت پرستیدنت می‌هراسم</p></div>
<div class="m2"><p>که تا زنده‌ای ره به معنی ندانی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر از باغ انست گیاهی برآید</p></div>
<div class="m2"><p>گیاهت نماید گل بوستانی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دریغ آیدت هر دو عالم خریدن</p></div>
<div class="m2"><p>اگر قدر نقدی که داری بدانی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به ملکی دمی زین نشاید خریدن</p></div>
<div class="m2"><p>که از دور عمرت بشد رایگانی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>همین حاصلت باشد از عمر باقی</p></div>
<div class="m2"><p>اگر همچنینش به آخر رسانی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بیا تا به از زندگانی به دستت</p></div>
<div class="m2"><p>چه افتاد تا صرف شد زندگانی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چنان می‌روی ساکن و خواب در سر</p></div>
<div class="m2"><p>که می‌ترسم از کاروان باز مانی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>وصیت همین است جان برادر</p></div>
<div class="m2"><p>که اوقات ضایع مکن تا توانی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>صدف وار باید زبان درکشیدن</p></div>
<div class="m2"><p>که وقتی که حاجت بود در چکانی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>همه عمر تلخی کشیده‌ست سعدی</p></div>
<div class="m2"><p>که نامش برآمد به شیرین زبانی</p></div></div>