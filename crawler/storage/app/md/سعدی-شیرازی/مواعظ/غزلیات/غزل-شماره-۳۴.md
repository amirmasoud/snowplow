---
title: >-
    غزل شمارهٔ  ۳۴
---
# غزل شمارهٔ  ۳۴

<div class="b" id="bn1"><div class="m1"><p>گناه کردن پنهان به از عبادت فاش</p></div>
<div class="m2"><p>اگر خدای پرستی هواپرست مباش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به عین عجب و تکبر نگه به خلق مکن</p></div>
<div class="m2"><p>که دوستان خدا ممکن‌اند در اوباش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر این زمین که تو بینی ملوک طبعانند</p></div>
<div class="m2"><p>که ملک روی زمین پیششان نیرزد لاش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به چشم کوته اغیار در نمی‌آیند</p></div>
<div class="m2"><p>مثال چشمهٔ خورشید و دیدهٔ خفاش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کرم کنند و نبینند بر کسی منت</p></div>
<div class="m2"><p>قفا خورند و نجویند با کسی پرخاش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز دیگدان لئیمان چو دود بگریزند</p></div>
<div class="m2"><p>نه دست کفچه کنند از برای کاسهٔ آش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دل از محبت دنیا و آخرت خالی</p></div>
<div class="m2"><p>که ذکر دوست توان کرد یا حساب قماش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به نیکمردی در حضرت خدای، قبول</p></div>
<div class="m2"><p>میان خلق به رندی و لاابالی فاش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>قدم زنند بزرگان دین و دم نزنند</p></div>
<div class="m2"><p>که از میان تهی بانگ می‌کند خشخاش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کمال نفس خردمند نیکبخت آن است</p></div>
<div class="m2"><p>که سر گران نکند بر قلندر قلاش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مقام صالح و فاجر هنوز پیدا نیست</p></div>
<div class="m2"><p>نظر به حسن معاد است نی به حسن معاش</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>اگر ز مغز حقیقت به پوست خرسندی</p></div>
<div class="m2"><p>تو نیز جامهٔ ازرق بپوش و سر بتراش</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>مراد اهل طریقت لباس ظاهر نیست</p></div>
<div class="m2"><p>کمر به خدمت سلطان ببند و صوفی باش</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>وز آنچه فیض خداوند بر تو می‌پاشد</p></div>
<div class="m2"><p>تو نیز در قدم بندگان او می‌پاش</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چو دور دور تو باشد مراد خلق بده</p></div>
<div class="m2"><p>چو دست دست تو باشد درون کس مخراش</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>نه صورتیست مزخرف عبارت سعدی</p></div>
<div class="m2"><p>چنانکه بر در گرمابه می‌کند نقاش</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>که برقعیست مرصع به لعل و مروارید</p></div>
<div class="m2"><p>فرو گذاشته بر روی شاهد جماش</p></div></div>