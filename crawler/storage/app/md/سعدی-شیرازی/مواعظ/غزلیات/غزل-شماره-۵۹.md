---
title: >-
    غزل شمارهٔ  ۵۹
---
# غزل شمارهٔ  ۵۹

<div class="b" id="bn1"><div class="m1"><p>یارب از ما چه فلاح آید اگر تو نپذیری</p></div>
<div class="m2"><p>به خداوندی و فضلت که نظر باز نگیری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>درد پنهان به تو گویم که خداوند کریمی</p></div>
<div class="m2"><p>یا نگویم که تو خود واقف اسرار ضمیری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر برانی به گناهان قبیح از در خویشم</p></div>
<div class="m2"><p>هم به درگاه تو آیم که لطیفی و خبیری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر به نومیدی از این در برود بندهٔ عاجز</p></div>
<div class="m2"><p>دیگرش چاره نماند که تو بی شبه و نظیری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دست در دامن عفوت زنم و باک ندارم</p></div>
<div class="m2"><p>که کریمی و حکیمی و علیمی و قدیری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خالق خلق و نگارندهٔ ایوان رفیعی</p></div>
<div class="m2"><p>خالق صبح و برآرندهٔ خورشید منیری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>حاجت موری و اندیشهٔ کمتر حیوانی</p></div>
<div class="m2"><p>بر تو پوشیده نماند که سمیعی و بصیری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر همه خلق به خصمی به در آیند و عداوت</p></div>
<div class="m2"><p>چه تفاوت کند آن را که تو مولا و نصیری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>همه را ملک مجاز است بزرگی و امیری</p></div>
<div class="m2"><p>تو خداوند جهانی که نه مردی و نه میری</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سعدیا من ملک‌الموت غنی‌ام تو فقیری</p></div>
<div class="m2"><p>چاره درویشی و عجز است و گدایی و حقیری</p></div></div>