---
title: >-
    غزل شمارهٔ  ۶
---
# غزل شمارهٔ  ۶

<div class="b" id="bn1"><div class="m1"><p>ای یار ناگزیر که دل در هوای توست</p></div>
<div class="m2"><p>جان نیز اگر قبول کنی هم برای توست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غوغای عارفان و تمنای عاشقان</p></div>
<div class="m2"><p>حرص بهشت نیست که شوق لقای توست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر تاج می‌دهی غرض ما قبول تو</p></div>
<div class="m2"><p>ور تیغ می‌زنی طلب ما رضای توست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر بنده می‌نوازی و گر بنده می‌کشی</p></div>
<div class="m2"><p>زجر و نواخت هر چه کنی رای رای توست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر در کمند کافر و گر در دهان شیر</p></div>
<div class="m2"><p>شادی به روزگار کسی کآشنای توست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر جا که روی زنده‌دلی بر زمین تو</p></div>
<div class="m2"><p>هر جا که دست غمزده‌ای بر دعای توست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تنها نه من به قید تو درمانده‌ام اسیر</p></div>
<div class="m2"><p>کز هر طرف شکسته‌دلی مبتلای توست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>قومی هوای نعمت دنیا همی پزند</p></div>
<div class="m2"><p>قومی هوای عقبی و ما را هوای توست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>قوت روان شیفتگان التفات تو</p></div>
<div class="m2"><p>آرام جان زنده‌دلان مرحبای توست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گر ما مقصریم تو بسیار رحمتی</p></div>
<div class="m2"><p>عذری که می‌رود به امید وفای توست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شاید که در حساب نیاید گناه ما</p></div>
<div class="m2"><p>آنجا که فضل و رحمت بی‌منتهای توست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>کس را بقای دائم و عهد مقیم نیست</p></div>
<div class="m2"><p>جاوید پادشاهی و دائم بقای توست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>هر جا که پادشاهی و صدری و سروری</p></div>
<div class="m2"><p>موقوف آستان در کبریای توست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>سعدی ثنای تو نتواند به شرح گفت</p></div>
<div class="m2"><p>خاموشی از ثنای تو حد ثنای توست</p></div></div>