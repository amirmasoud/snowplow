---
title: >-
    غزل شمارهٔ  ۵۰
---
# غزل شمارهٔ  ۵۰

<div class="b" id="bn1"><div class="m1"><p>برخیز تا به عهد امانت وفا کنیم</p></div>
<div class="m2"><p>تقصیرهای رفته به خدمت قضا کنیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بی‌مغز بود سر که نهادیم پیش خلق</p></div>
<div class="m2"><p>دیگر فروتنی به در کبریا کنیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دارالفنا کرای مرمت نمی‌کند</p></div>
<div class="m2"><p>بشتاب تا عمارت دارالبقا کنیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دارالشفای توبه نبسته‌ست در هنوز</p></div>
<div class="m2"><p>تا درد معصیت به تدارک دوا کنیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>روی از خدا به هر چه کنی شرک خالص است</p></div>
<div class="m2"><p>توحید محض کز همه رو در خدا کنیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پیراهن خلاف به دست مراجعت</p></div>
<div class="m2"><p>یکتا کنیم و پشت عبادت دو تا کنیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چند آید این خیال و رود در سرای دل</p></div>
<div class="m2"><p>تا کی مقام دوست به دشمن رها کنیم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون برترین مقام ملک دون قدر ماست</p></div>
<div class="m2"><p>چندین به دست دیو زبونی چرا کنیم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سیم دغل خجالت و بدنامی آورد</p></div>
<div class="m2"><p>خیز ای حکیم تا طلب کیمیا کنیم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بستن قبا به خدمت سالار و شهریار</p></div>
<div class="m2"><p>امیدوارتر که گنه در عبا کنیم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سعدی گدا بخواهد و منعم به زر خرد</p></div>
<div class="m2"><p>ما را وجود نیست بیا تا دعا کنیم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>یارب تو دست گیر که آلا و مغفرت</p></div>
<div class="m2"><p>در خورد توست و در خور ما هر چه ما کنیم</p></div></div>