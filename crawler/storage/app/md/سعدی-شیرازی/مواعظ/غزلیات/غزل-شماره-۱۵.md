---
title: >-
    غزل شمارهٔ  ۱۵
---
# غزل شمارهٔ  ۱۵

<div class="b" id="bn1"><div class="m1"><p>هر که هر بامداد پیش کسیست</p></div>
<div class="m2"><p>هر شبانگاه در سرش هوسیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل منه بر وفای صحبت او</p></div>
<div class="m2"><p>کآنچنان را حریف چون تو بسیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مهربانی و دوستی ورزد</p></div>
<div class="m2"><p>تا تو را مکنتی و دسترسیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گوید اندر جهان تویی امروز</p></div>
<div class="m2"><p>گر مرا مونسی و همنفسیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>باز با دیگری همین گوید</p></div>
<div class="m2"><p>کاین جهان بی تو بر دلم قفسیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همچو زنبور در به در پویان</p></div>
<div class="m2"><p>هر کجا طعمه‌ای بود مگسیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>همه دعوی و فارغ از معنی</p></div>
<div class="m2"><p>راست گویی میان تهی جرسیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پیش آن ذم این کند که خریست</p></div>
<div class="m2"><p>نزد این عیب آن کند که خسیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هر کجا بینی این چنین کس را</p></div>
<div class="m2"><p>التفاتش مکن که هیچ کسیست</p></div></div>