---
title: >-
    غزل شمارهٔ  ۴۷
---
# غزل شمارهٔ  ۴۷

<div class="b" id="bn1"><div class="m1"><p>خرما نتوان خوردن از این خار که کشتیم</p></div>
<div class="m2"><p>دیبا نتوان کردن از این پشم که رشتیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر حرف معاصی خط عذری نکشیدیم</p></div>
<div class="m2"><p>پهلوی کبائر حسناتی ننوشتیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ما کشتهٔ نفسیم و بس آوخ که برآید</p></div>
<div class="m2"><p>از ما به قیامت که چرا نفس نکشتیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>افسوس بر این عمر گرانمایه که بگذشت</p></div>
<div class="m2"><p>ما از سر تقصیر و خطا درنگذشتیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دنیا که در او مرد خدا گل نسرشته‌ست</p></div>
<div class="m2"><p>نامرد که ماییم چرا دل بسرشتیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ایشان چو ملخ در پس زانوی ریاضت</p></div>
<div class="m2"><p>ما مور میان بسته دوان بر در و دشتیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پیری و جوانی پی هم چون شب و روزند</p></div>
<div class="m2"><p>ما شب شد و روز آمد و بیدار نگشتیم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>واماندگی اندر پس دیوار طبیعت</p></div>
<div class="m2"><p>حیف است دریغا که در صلح بهشتیم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون مرغ بر این کنگره تا کی بتوان خواند</p></div>
<div class="m2"><p>یک روز نگه کن که بر این کنگره خشتیم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ما را عجب ار پشت و پناهی بود آن روز</p></div>
<div class="m2"><p>کامروز کسی را نه پناهیم و نه پشتیم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گر خواجه شفاعت نکند روز قیامت</p></div>
<div class="m2"><p>شاید که ز مشاطه نرنجیم که زشتیم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>باشد که عنایت برسد ورنه مپندار</p></div>
<div class="m2"><p>با این عمل دوزخیان کاهل بهشتیم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>سعدی! مگر از خرمن اقبال بزرگان</p></div>
<div class="m2"><p>یک خوشه ببخشند که ما تخم نکشتیم</p></div></div>