---
title: >-
    غزل شمارهٔ  ۵۵
---
# غزل شمارهٔ  ۵۵

<div class="b" id="bn1"><div class="m1"><p>شبی در خرقه رندآسا، گذر کردم به میخانه</p></div>
<div class="m2"><p>ز عشرت می‌پرستان را، منور بود کاشانه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز خلوتگاه ربانی، وثاقی در سرای دل</p></div>
<div class="m2"><p>که تا قصر دماغ ایمن بود ز آواز بیگانه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو ساقی در شراب آمد، به نوشانوش در مجلس</p></div>
<div class="m2"><p>به نافرزانگی گفتند کاول مرد فرزانه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به تندی گفتم آری من، شراب از مجلسی خوردم</p></div>
<div class="m2"><p>که مه پیرامن شمعش، نیارد بود پروانه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دلی کز عالم وحدت، سماع حق شنیده‌ست او</p></div>
<div class="m2"><p>به گوش همتش دیگر، کی آید شعر و افسانه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گمان بردم که طفلانند وز پیری سخن گفتم</p></div>
<div class="m2"><p>مرا پیری خراباتی، جوابی داد مردانه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>که نور عالم علوی، فرا هر روزنی تابد</p></div>
<div class="m2"><p>تو اندر صومعش دیدی و ما در کنج میخانه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کسی کآمد در این خلوت، به یکرنگی هویدا شد</p></div>
<div class="m2"><p>چه پیری عابد زاهد، چه رند مست دیوانه</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گشادند از درون جان در تحقیق سعدی را</p></div>
<div class="m2"><p>چو اندر قفل گردون زد کلید صبح دندانه</p></div></div>