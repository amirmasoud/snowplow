---
title: >-
    غزل شمارهٔ  ۲۴
---
# غزل شمارهٔ  ۲۴

<div class="b" id="bn1"><div class="m1"><p>بیفکن خیمه تا محمل برانند</p></div>
<div class="m2"><p>که همراهان این عالم روانند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زن و فرزند و خویش و یار و پیوند</p></div>
<div class="m2"><p>برادر خواندگان کاروانند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نباید بستن اندر صحبتی دل</p></div>
<div class="m2"><p>که بی ایشان بمانی یا بمانند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نه اول خاک بوده‌ست آدمیزاد</p></div>
<div class="m2"><p>به آخر چون بیندیشی همانند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پس آن بهتر که اول وآخر خویش</p></div>
<div class="m2"><p>بیندیشند و قدر خود بدانند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زمین چندی بخورد از خلق و چندی</p></div>
<div class="m2"><p>هنوز از کبر سر بر آسمانند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یکی بر تربتی فریاد می‌خواند</p></div>
<div class="m2"><p>که اینان پادشاهان جهانند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بگفتم تخته‌ای بر کن ز گوری</p></div>
<div class="m2"><p>ببین تا پادشه یا پاسبانند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بگفتا تخته بر کندن چه حاجت</p></div>
<div class="m2"><p>که می‌دانم که مشتی استخوانند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نصیحت داروی تلخ است و باید</p></div>
<div class="m2"><p>که با جلاب در حلقت چکانند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چنین سقمونیای شکرآلود</p></div>
<div class="m2"><p>ز داروخانهٔ سعدی ستانند</p></div></div>