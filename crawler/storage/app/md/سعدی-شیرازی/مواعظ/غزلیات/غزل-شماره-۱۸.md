---
title: >-
    غزل شمارهٔ  ۱۸
---
# غزل شمارهٔ  ۱۸

<div class="b" id="bn1"><div class="m1"><p>تن آدمی شریف است به جان آدمیت</p></div>
<div class="m2"><p>نه همین لباس زیباست نشان آدمیت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگر آدمی به چشم است و دهان و گوش و بینی</p></div>
<div class="m2"><p>چه میان نقش دیوار و میان آدمیت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خور و خواب و خشم و شهوت شغب است و جهل و ظلمت</p></div>
<div class="m2"><p>حیوان خبر ندارد ز جهان آدمیت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به حقیقت آدمی باش وگر نه مرغ باشد</p></div>
<div class="m2"><p>که همین سخن بگوید به زبان آدمیت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مگر آدمی نبودی که اسیر دیو ماندی</p></div>
<div class="m2"><p>که فرشته ره ندارد به مکان آدمیت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اگر این درنده‌خویی ز طبیعتت بمیرد</p></div>
<div class="m2"><p>همه عمر زنده باشی به روان آدمیت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>رسد آدمی به جایی که به جز خدا نبیند</p></div>
<div class="m2"><p>بنگر که تا چه حد است مکان آدمیت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>طیران مرغ دیدی تو ز پایبند شهوت</p></div>
<div class="m2"><p>به در آی تا ببینی طیران آدمیت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نه بیان فضل کردم که نصیحت تو گفتم</p></div>
<div class="m2"><p>هم از آدمی شنیدیم بیان آدمیت</p></div></div>