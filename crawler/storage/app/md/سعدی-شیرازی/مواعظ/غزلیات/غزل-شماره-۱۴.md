---
title: >-
    غزل شمارهٔ  ۱۴
---
# غزل شمارهٔ  ۱۴

<div class="b" id="bn1"><div class="m1"><p>از جان برون نیامده جانانت آرزوست</p></div>
<div class="m2"><p>زنار نابریده و ایمانت آرزوست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر درگهی که نوبت ارنی همی زنند</p></div>
<div class="m2"><p>موری نه‌ای و ملک سلیمانت آرزوست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>موری نه‌ای و خدمت موری نکرده‌ای</p></div>
<div class="m2"><p>وآنگاه صف صفهٔ مردانت آرزوست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>فرعون‌وار لاف اناالحق همی زنی</p></div>
<div class="m2"><p>وآنگاه قرب موسی عمرانت آرزوست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون کودکان که دامن خود اسب کرده‌اند</p></div>
<div class="m2"><p>دامن سوار کرده و میدانت آرزوست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>انصاف راه خود ز سر صدق داده‌ای</p></div>
<div class="m2"><p>بر درد نارسیده و درمانت آرزوست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر خوان عنکبوت که بریان مگس بود</p></div>
<div class="m2"><p>شهپر جبرئیل، مگس‌رانت آرزوست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هر روز از برای سگ نفس بوسعید</p></div>
<div class="m2"><p>یک کاسه شوربا و دو تا نانت آرزوست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سعدی در این جهان که تویی ذره‌وار باش</p></div>
<div class="m2"><p>گر دل به نزد حضرت سلطانت آرزوست</p></div></div>