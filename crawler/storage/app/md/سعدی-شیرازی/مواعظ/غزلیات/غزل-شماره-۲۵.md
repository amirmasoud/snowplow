---
title: >-
    غزل شمارهٔ  ۲۵
---
# غزل شمارهٔ  ۲۵

<div class="b" id="bn1"><div class="m1"><p>اگر خدای نباشد ز بنده‌ای خشنود</p></div>
<div class="m2"><p>شفاعت همه پیغمبران ندارد سود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قضای کن فیکون است حکم بار خدای</p></div>
<div class="m2"><p>بدین سخن سخنی در نمی‌توان افزود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نه زنگ عاریتی بود بر دل فرعون</p></div>
<div class="m2"><p>که صیقل ید بیضا سیاهیش نزدود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بخواند و راه ندادش کجا رود بدبخت؟</p></div>
<div class="m2"><p>ببست دیدهٔ مسکین و دیدنش فرمود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نصیب دوزخ اگر طلق بر خود انداید</p></div>
<div class="m2"><p>چنان در او جهد آتش که چوب نفط اندود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>قلم به طالع میمون و بخت بد رفته‌ست</p></div>
<div class="m2"><p>اگر تو خشمگنی ای پسر وگر خشنود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گنه نبود و عبادت نبود و بر سر خلق</p></div>
<div class="m2"><p>نبشته بود که این ناجی است و آن مأخوذ</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مقدر است که از هر کسی چه فعل آید</p></div>
<div class="m2"><p>درخت مقل نه خرما دهد نه شفتالود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به سعی ماشطه اصلاح زشت نتوان کرد</p></div>
<div class="m2"><p>چنان که شاهدی از روی خوب نتوان سود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سیاه زنگی هرگز شود سپید به آب؟</p></div>
<div class="m2"><p>سپید رومی هرگز شود سیاه به دود؟</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سعادتی که نباشد طمع مکن سعدی</p></div>
<div class="m2"><p>که چون نکاشته باشند مشکل است درود</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>قلم به آمدنی رفت اگر رضا به قضا</p></div>
<div class="m2"><p>دهی و گر ندهی بودنی بخواهد بود</p></div></div>