---
title: >-
    شمارهٔ  ۵۴
---
# شمارهٔ  ۵۴

<div class="b" id="bn1"><div class="m1"><p>هر که آمد بر خدای قبول</p></div>
<div class="m2"><p>نکند هیچش از خدا مشغول</p></div></div>