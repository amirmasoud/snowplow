---
title: >-
    شمارهٔ  ۷۶
---
# شمارهٔ  ۷۶

<div class="b" id="bn1"><div class="m1"><p>ای گرگ نگفتمت که روزی</p></div>
<div class="m2"><p>بیچاره شوی به دست یوزی</p></div></div>