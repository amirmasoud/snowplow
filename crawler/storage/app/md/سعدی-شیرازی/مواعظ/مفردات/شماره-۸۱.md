---
title: >-
    شمارهٔ ۸۱
---
# شمارهٔ ۸۱

<div class="b" id="bn1"><div class="m1"><p>از روی نکو صبر نمی‌شاید کرد</p></div>
<div class="m2"><p>لیکن نه به اختیار می‌باید کرد</p></div></div>