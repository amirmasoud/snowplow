---
title: >-
    شمارهٔ ۸۲
---
# شمارهٔ ۸۲

<div class="b" id="bn1"><div class="m1"><p>می‌شنیدم به حسن چون قمری</p></div>
<div class="m2"><p>چون بدیدم از آن تو خوبتری</p></div></div>