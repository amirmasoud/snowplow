---
title: >-
    شمارهٔ  ۵۲
---
# شمارهٔ  ۵۲

<div class="b" id="bn1"><div class="m1"><p>بد نه نیکست بی‌خلافت ولیک</p></div>
<div class="m2"><p>مرد خالی نباشد از بد و نیک</p></div></div>