---
title: >-
    شمارهٔ  ۵۱
---
# شمارهٔ  ۵۱

<div class="b" id="bn1"><div class="m1"><p>با هر کسی به مذهب وی باید اتفاق</p></div>
<div class="m2"><p>شرطست یا موافقت جمع یا فراق</p></div></div>