---
title: >-
    شمارهٔ ۸۸
---
# شمارهٔ ۸۸

<div class="b" id="bn1"><div class="m1"><p>و لو ان حبا بالملام یزول</p></div>
<div class="m2"><p>لسمعت افکا یفتریه عذول</p></div></div>