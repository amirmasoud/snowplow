---
title: >-
    شمارهٔ  ۵
---
# شمارهٔ  ۵

<div class="b" id="bn1"><div class="m1"><p>دولت جاوید به طاعت درست</p></div>
<div class="m2"><p>سود مسافر به بضاعت درست</p></div></div>