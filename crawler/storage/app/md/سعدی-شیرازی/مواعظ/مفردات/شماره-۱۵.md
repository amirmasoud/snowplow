---
title: >-
    شمارهٔ  ۱۵
---
# شمارهٔ  ۱۵

<div class="b" id="bn1"><div class="m1"><p>بیچاره که در میان دریا افتاد</p></div>
<div class="m2"><p>مسکین چه کند که دست و پایی نزند</p></div></div>