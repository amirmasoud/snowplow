---
title: >-
    شمارهٔ  ۵۰
---
# شمارهٔ  ۵۰

<div class="b" id="bn1"><div class="m1"><p>مکن عمر ضایع به افسوس و حیف</p></div>
<div class="m2"><p>که فرصت عزیزست و الوقت سیف</p></div></div>