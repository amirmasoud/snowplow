---
title: >-
    شمارهٔ  ۱۸
---
# شمارهٔ  ۱۸

<div class="b" id="bn1"><div class="m1"><p>وقتی دل دوستان به جنگ آزارند</p></div>
<div class="m2"><p>چندانکه نه جای آشتی بگذارند</p></div></div>