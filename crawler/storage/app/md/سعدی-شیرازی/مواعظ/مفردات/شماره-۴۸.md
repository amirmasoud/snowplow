---
title: >-
    شمارهٔ  ۴۸
---
# شمارهٔ  ۴۸

<div class="b" id="bn1"><div class="m1"><p>به کین دشمنان باطل میندیش</p></div>
<div class="m2"><p>که این حیفست ظاهر بر تن خویش</p></div></div>