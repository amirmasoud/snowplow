---
title: >-
    شمارهٔ ۸۵
---
# شمارهٔ ۸۵

<div class="b" id="bn1"><div class="m1"><p>و کل بلیغ بالغ السعی فی دمی</p></div>
<div class="m2"><p>اذا کان فی حی الحبیب حبیب</p></div></div>