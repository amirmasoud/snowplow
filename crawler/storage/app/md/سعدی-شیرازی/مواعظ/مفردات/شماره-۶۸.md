---
title: >-
    شمارهٔ  ۶۸
---
# شمارهٔ  ۶۸

<div class="b" id="bn1"><div class="m1"><p>آن گوی که طاقت جوابش داری</p></div>
<div class="m2"><p>گندم نبری به خانه چون جو کاری</p></div></div>