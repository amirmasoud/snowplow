---
title: >-
    شمارهٔ  ۴
---
# شمارهٔ  ۴

<div class="b" id="bn1"><div class="m1"><p>کس نیست که مهر تو درو شاید بست</p></div>
<div class="m2"><p>پس پیش تو ناچار کمر باید بست</p></div></div>