---
title: >-
    شمارهٔ  ۱۶
---
# شمارهٔ  ۱۶

<div class="b" id="bn1"><div class="m1"><p>توان نان خورد اگر دندان نباشد</p></div>
<div class="m2"><p>مصیبت آن بود که نان نباشد</p></div></div>