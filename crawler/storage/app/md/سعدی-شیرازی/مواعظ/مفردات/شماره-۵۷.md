---
title: >-
    شمارهٔ  ۵۷
---
# شمارهٔ  ۵۷

<div class="b" id="bn1"><div class="m1"><p>دلت خوش باد و چشم از بخت روشن</p></div>
<div class="m2"><p>به کام دوستان و رغم دشمن</p></div></div>