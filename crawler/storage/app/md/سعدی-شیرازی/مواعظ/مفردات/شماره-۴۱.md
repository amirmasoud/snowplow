---
title: >-
    شمارهٔ  ۴۱
---
# شمارهٔ  ۴۱

<div class="b" id="bn1"><div class="m1"><p>چه داند خوابناک مست مخمور</p></div>
<div class="m2"><p>که شب را چون به روز آورد رنجور</p></div></div>