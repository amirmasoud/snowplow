---
title: >-
    شمارهٔ  ۶۲
---
# شمارهٔ  ۶۲

<div class="b" id="bn1"><div class="m1"><p>صاحبدل نیک سیرت علامه</p></div>
<div class="m2"><p>گو کفش دریده باش و خلقان جامه</p></div></div>