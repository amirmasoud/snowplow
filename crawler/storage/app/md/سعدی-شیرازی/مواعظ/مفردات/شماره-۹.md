---
title: >-
    شمارهٔ  ۹
---
# شمارهٔ  ۹

<div class="b" id="bn1"><div class="m1"><p>گر راه نمایی همه عالم راهست</p></div>
<div class="m2"><p>ور دست نگیری هه عالم چاهست</p></div></div>