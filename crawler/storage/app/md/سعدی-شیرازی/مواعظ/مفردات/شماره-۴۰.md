---
title: >-
    شمارهٔ  ۴۰
---
# شمارهٔ  ۴۰

<div class="b" id="bn1"><div class="m1"><p>بزرگی نماند بر آن پایدار</p></div>
<div class="m2"><p>که مردم به چشمش نمایند خوار</p></div></div>