---
title: >-
    رباعی شمارهٔ ۱
---
# رباعی شمارهٔ ۱

<div class="b" id="bn1"><div class="m1"><p>آن کیست که دل نهاد و فارغ بنشست</p></div>
<div class="m2"><p>پنداشت که مهلتی و تأخیری هست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گو میخ مزن که خیمه می‌باید کند</p></div>
<div class="m2"><p>گو رخت منه که بار می‌باید بست</p></div></div>