---
title: >-
    رباعی شمارهٔ ۵۲
---
# رباعی شمارهٔ ۵۲

<div class="b" id="bn1"><div class="m1"><p>آیین برادری و شرط یاری</p></div>
<div class="m2"><p>آن نیست که عیب من هنر پنداری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آنست که گر خلاف شایسته روم</p></div>
<div class="m2"><p>از غایت دوستیم دشمن داری</p></div></div>