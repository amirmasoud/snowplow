---
title: >-
    رباعی شمارهٔ ۳۳
---
# رباعی شمارهٔ ۳۳

<div class="b" id="bn1"><div class="m1"><p>از دست مده طریق احسان پدر</p></div>
<div class="m2"><p>تا بر بخوری ز ملک و فرمان پدر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جان پدرت از ان جهان می‌گوید</p></div>
<div class="m2"><p>زنهار خلاف من مکن جان پدر</p></div></div>