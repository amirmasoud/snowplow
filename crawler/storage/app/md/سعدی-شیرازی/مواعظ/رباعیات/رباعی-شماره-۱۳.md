---
title: >-
    رباعی شمارهٔ ۱۳
---
# رباعی شمارهٔ ۱۳

<div class="b" id="bn1"><div class="m1"><p>ظلم از دل و دست ملک نیرو ببرد</p></div>
<div class="m2"><p>عادل ز زمانه نام نیکو ببرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر تقویت ملک بری ملک بری</p></div>
<div class="m2"><p>ور تو نکنی هر که کند او ببرد</p></div></div>