---
title: >-
    رباعی شمارهٔ ۵
---
# رباعی شمارهٔ ۵

<div class="b" id="bn1"><div class="m1"><p>گر خود ز عبادت استخوانی در پوست</p></div>
<div class="m2"><p>زشتست اگر اعتقاد بندی که نکوست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر بر سر پیکان برود طالب دوست</p></div>
<div class="m2"><p>حقا که هنوز منت دوست بروست</p></div></div>