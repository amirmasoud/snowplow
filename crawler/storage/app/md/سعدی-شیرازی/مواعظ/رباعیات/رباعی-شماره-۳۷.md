---
title: >-
    رباعی شمارهٔ ۳۷
---
# رباعی شمارهٔ ۳۷

<div class="b" id="bn1"><div class="m1"><p>سودی نکند فراخنای بر و دوش</p></div>
<div class="m2"><p>گر آدمیی عقل و هنرپرور و هوش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گاو از من و تو فراختر دارد چشم</p></div>
<div class="m2"><p>پیل از من و تو بزرگتر دارد گوش</p></div></div>