---
title: >-
    رباعی شمارهٔ ۳۵
---
# رباعی شمارهٔ ۳۵

<div class="b" id="bn1"><div class="m1"><p>چون خیل تو صد باشد و خصم تو هزار</p></div>
<div class="m2"><p>خود را به هلاک می‌سپاری هش دار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا بتوانی برآور از خصم دمار</p></div>
<div class="m2"><p>چون جنگ ندانی آشتی عیب مدار</p></div></div>