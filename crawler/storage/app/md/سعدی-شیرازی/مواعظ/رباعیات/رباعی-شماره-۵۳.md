---
title: >-
    رباعی شمارهٔ ۵۳
---
# رباعی شمارهٔ ۵۳

<div class="b" id="bn1"><div class="m1"><p>تا کی به جمال و مال دنیا نازی</p></div>
<div class="m2"><p>آمد گه آنکه راه عقبی سازی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای دیر نشسته وقت آنست که جای</p></div>
<div class="m2"><p>یک چند به نوخاستگان پردازی</p></div></div>