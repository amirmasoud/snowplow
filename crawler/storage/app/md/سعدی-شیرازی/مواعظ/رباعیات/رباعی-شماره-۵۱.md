---
title: >-
    رباعی شمارهٔ ۵۱
---
# رباعی شمارهٔ ۵۱

<div class="b" id="bn1"><div class="m1"><p>گویند که دوش شحنگان تتری</p></div>
<div class="m2"><p>دزدی بگرفتند به صد حیله‌گری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>امروز به آویختنش می‌بردند</p></div>
<div class="m2"><p>می‌گفت رها کن که گریبان ندری</p></div></div>