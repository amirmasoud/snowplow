---
title: >-
    رباعی شمارهٔ ۳۱
---
# رباعی شمارهٔ ۳۱

<div class="b" id="bn1"><div class="m1"><p>هرکس به نصیب خویش خواهند رسید</p></div>
<div class="m2"><p>هرگز ندهند جای پاکان به پلید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر بختوری مراد خود خواهی یافت</p></div>
<div class="m2"><p>ور بخت بدی سزای خود خواهی دید</p></div></div>