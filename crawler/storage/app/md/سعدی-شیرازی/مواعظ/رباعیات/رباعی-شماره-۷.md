---
title: >-
    رباعی شمارهٔ ۷
---
# رباعی شمارهٔ ۷

<div class="b" id="bn1"><div class="m1"><p>بالای قضای رفته فرمانی نیست</p></div>
<div class="m2"><p>چون درد اجل گرفت درمانی نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>امروز که عهد تست نیکویی کن</p></div>
<div class="m2"><p>کاین ده همه وقت از آن دهقانی نیست</p></div></div>