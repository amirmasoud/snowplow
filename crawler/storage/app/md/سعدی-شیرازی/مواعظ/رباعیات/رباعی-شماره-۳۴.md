---
title: >-
    رباعی شمارهٔ ۳۴
---
# رباعی شمارهٔ ۳۴

<div class="b" id="bn1"><div class="m1"><p>گر آدمیی بادهٔ گلرنگ بخور</p></div>
<div class="m2"><p>بر نالهٔ نای و نغمهٔ چنگ بخور</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر بنگ خوری چو سنگ مانی بر جای</p></div>
<div class="m2"><p>یکباره چو بنگ می‌خوری سنگ بخور</p></div></div>