---
title: >-
    رباعی شمارهٔ ۳
---
# رباعی شمارهٔ ۳

<div class="b" id="bn1"><div class="m1"><p>آن کس که خطای خویش بیند که رواست</p></div>
<div class="m2"><p>تقریر مکن صواب نزدش که خطاست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن روی نمایدش که در طینت اوست</p></div>
<div class="m2"><p>آیینهٔ کج جمال ننماید راست</p></div></div>