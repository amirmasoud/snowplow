---
title: >-
    شعر  ۲۳ - فی مدح صاحب دیوان
---
# شعر  ۲۳ - فی مدح صاحب دیوان

<div class="b" id="bn1"><div class="m1"><p>ما هذه الدنیا بدار مخلد</p></div>
<div class="m2"><p>طوبی المدخر النعیم الی غد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کالصاحب الصدر الکبیر العالم ال</p></div>
<div class="m2"><p>... متعفف البر الاجل الامجد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>میزان عدل لایجور ولا یحی ...</p></div>
<div class="m2"><p>ف و ما اعتدی الا علی من یعتدی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بشر الینا بالرجاء بمنه</p></div>
<div class="m2"><p>و تقایض الدنیا بدولة سرمد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مهمارجوت رجوت خیرالمرتجی</p></div>
<div class="m2"><p>و اذا قصدت قصدت خیرالمقصد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مدت حیوة الناس تحت ظلاله</p></div>
<div class="m2"><p>لا زال فی اهنی الحیوة و ارغد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هذی خلال الزاکیات وصفته</p></div>
<div class="m2"><p>لمحمد بن محمدبن محمد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>او یحسب الانسان ماسلک اهتدی</p></div>
<div class="m2"><p>لا، من هداه الله فهو المهتدی</p></div></div>