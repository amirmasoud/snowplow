---
title: >-
    شعر  ۱۱ - و له فی‌الغزل
---
# شعر  ۱۱ - و له فی‌الغزل

<div class="b" id="bn1"><div class="m1"><p>فاح نشر الحمی و هب النسیم</p></div>
<div class="m2"><p>و ترانی من فرط وجدی اهیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ان لیل الوصال صبح مضییء</p></div>
<div class="m2"><p>و نهار الفراق لیل بهیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>و وداع النزیل خطب جزیل</p></div>
<div class="m2"><p>و فراق الانیس داء الیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>فتن العابدین صدر رخیم</p></div>
<div class="m2"><p>آه لو کان فیه قلب رحیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یا وحیدالجمال نفسی وحید</p></div>
<div class="m2"><p>یا عدیم المثال قلبی عدیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سلوتی عنکم احتمال بعید</p></div>
<div class="m2"><p>وافتضاحی بکم ضلال قدیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>معشر اللائمین من یضلل‌الله</p></div>
<div class="m2"><p>بعید بانه یستقیم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اجهلتم بان نارجحیم</p></div>
<div class="m2"><p>مع ذکرالحبیب روض نعیم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کل من یدعی المحبة فیکم</p></div>
<div class="m2"><p>ثم یخشی الملام فهو ملیم</p></div></div>