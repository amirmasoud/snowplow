---
title: >-
    شعر  ۴ - فی الغزل
---
# شعر  ۴ - فی الغزل

<div class="b" id="bn1"><div class="m1"><p>تعذر صمت الواجدین فصاحوا</p></div>
<div class="m2"><p>و من صاح وجدا ما علیه جناح</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اسروا حدیث العشق ما امکن التقی</p></div>
<div class="m2"><p>و ان غلب الشوق الشدید فباحوا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سری طیف من یجلو بطلعته الدجی</p></div>
<div class="m2"><p>و سائر لیل المقبلین صباح</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یطاف علیهم والخلیون نوم</p></div>
<div class="m2"><p>و یسقون من کأس المدامع راح</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سمحت بدنیائی و دینی و مهجتی</p></div>
<div class="m2"><p>و نفسی و عقلی و السماح رباح</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>واقبح ما کان المکاره والاذی</p></div>
<div class="m2"><p>اذا کان من عندالملاح ملاح</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>و لو لم یکن سمع المعانی لبعضنا</p></div>
<div class="m2"><p>سماع الاغانی زخرف و مزاح</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اصیح اشتیاقا کلما ذکرالحمی</p></div>
<div class="m2"><p>و غایة جهد المستهام صیاح</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ولابد من حی الحبیب زیارة</p></div>
<div class="m2"><p>و ان رکزت بین الخیام رماح</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هنالک دائی فرحتی، و منیتی</p></div>
<div class="m2"><p>حیاتی، و موت الطالبین نجاح</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>یقولون لثم الغانیات محرم</p></div>
<div class="m2"><p>اسفک دماء العاشقین مباح؟</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>الا انما السعدی مشتاق اهله</p></div>
<div class="m2"><p>تشوق طیر، لم یطعه جناح</p></div></div>