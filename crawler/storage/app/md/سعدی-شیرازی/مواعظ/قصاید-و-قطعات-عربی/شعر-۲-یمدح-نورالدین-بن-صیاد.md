---
title: >-
    شعر  ۲ - یمدح نورالدین بن صیاد
---
# شعر  ۲ - یمدح نورالدین بن صیاد

<div class="b" id="bn1"><div class="m1"><p>مادام ینسرح الغزلان فی الوادی</p></div>
<div class="m2"><p>احذر یفوتک صید یا بن صیاد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>و اعلم بان امام المرء بادیة</p></div>
<div class="m2"><p>وقاطع البر محتاج الی الزاد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یا من تملک مألوف الذین غدوا</p></div>
<div class="m2"><p>هل یطمن صحیح العقل بالغادی؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>و انما مثل الدنیا و زینتها</p></div>
<div class="m2"><p>ریح تمر بکام و اطواد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اذ لامحالة ثوب العمر منتزع</p></div>
<div class="m2"><p>لافرق بین سقلاط و لباد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مالا بن آدم عندالله منزلة</p></div>
<div class="m2"><p>الا و منزله رحب لقصاد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>طوبی لمن جمع الدنیا و فرقها</p></div>
<div class="m2"><p>فی مصرف الخیر لا باغ ولا عاد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کما تیقن ان الوقت منصرف</p></div>
<div class="m2"><p>ایقن بانک محشور لمیعاد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>و ربما بلغت نفس بجودتها</p></div>
<div class="m2"><p>ما لا یبلغها تهلیل عباد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>رکب الحجاز تجوب البر فی طمع</p></div>
<div class="m2"><p>والبر احسن طاعات و اوراد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>جد، وابتسم، و تواضع، و اعف عن زلل</p></div>
<div class="m2"><p>و انفع خلیلک، و انقطع غلة الصادی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ولا تضرک عیون منک طامحة</p></div>
<div class="m2"><p>ان الثعالب ترجوا فضل آساد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>و هل تکاد تدی حق نعمته؟</p></div>
<div class="m2"><p>والشکر یقصر عن انعامه البادی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ان کنت یا ولدی بالحق منتفعا</p></div>
<div class="m2"><p>هذی طریقة سادات و أمجاد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>قرعت بابک و الاقبال یهتف بی</p></div>
<div class="m2"><p>شرعت فی منهل عذب لوراد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>لاتعتبن علی مافیه من عظة</p></div>
<div class="m2"><p>ان النصیحة مألوفی و معتادی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>قرعت بابک و الاقبال یهتف بی</p></div>
<div class="m2"><p>شرعت فی منهل عذب لوراد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>غنیت باسمک والجدران من طرب</p></div>
<div class="m2"><p>تکاد ترقص کالبعران للحادی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>یا دولة جمعت شملی بریته</p></div>
<div class="m2"><p>بلغتنی املا رغما لحسادی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>یا اسعدالناس جدا ما سعی قدمی</p></div>
<div class="m2"><p>الیک، الا ارادالله اسعادی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>انی اصطفیتک دون الناس قاطبة</p></div>
<div class="m2"><p>اذ لایشبه اعیان بحاد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>دم یا سحاب لجوالفرس منبسطا</p></div>
<div class="m2"><p>و امطر نداک علی الحضار والبادی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>خیر ارید بشیراز حللت به</p></div>
<div class="m2"><p>یا نعمة الله دومی فیه و ازدادی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>لازلت فی سعة الدنیا و نعمتها</p></div>
<div class="m2"><p>ما اهتز روض و غنی طیره الشادی</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>تم القصیدة ابقی الله شانکم</p></div>
<div class="m2"><p>بقاء سمسمة فی کیر حداد</p></div></div>