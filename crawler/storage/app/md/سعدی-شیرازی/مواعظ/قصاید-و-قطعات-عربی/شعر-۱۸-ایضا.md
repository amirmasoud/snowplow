---
title: >-
    شعر  ۱۸ - ایضا
---
# شعر  ۱۸ - ایضا

<div class="b" id="bn1"><div class="m1"><p>یا ندیمی قم تنبه واسقنی واسق الندامی</p></div>
<div class="m2"><p>خلنی اسهر لیلی و دع الناس نیاما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اسقیانی و هدیر الرعدقد ابکی الغماما</p></div>
<div class="m2"><p>و شفاه الزهر تفتر من‌الضحک ابتساما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فی زمان سجع الطیر علی‌الغصن رخاما</p></div>
<div class="m2"><p>و اوان کشف الورد من الوجه اللثما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ایها العاقل اف لبصیر یتعامی</p></div>
<div class="m2"><p>فزبها من قبل ان یجعلک الدهر حطاما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>قل لمن عیر اهل الحب بالجهل و لاما</p></div>
<div class="m2"><p>لا عرفت الحب هیهات ولا ذقت الغراما</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>من تعدی زمن الفرصة بخلا واهتماما</p></div>
<div class="m2"><p>ضیع العمر أیوما عاش او خمسین عاما</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>لاتلمنی فی غلام اودع القلب السقاما</p></div>
<div class="m2"><p>مبداء الحب کم من سید اضحی غلاما</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>منتهی منیة قلبی شادن یسقی المداما</p></div>
<div class="m2"><p>و علی‌الخضرة منثور و رند و خزامی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ذی دلال سلب القلب اذا قال کلاما</p></div>
<div class="m2"><p>و جمال غلب الغصن اذا مال قواما</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>یا عذولی فنی الصبر الی کم والی ما</p></div>
<div class="m2"><p>انا لا أعبؤ بالزجز ولا اخشی الملاما</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ترک الحب علی مقلتی النوم حراما</p></div>
<div class="m2"><p>و حوالی حبال الشوق خالفا و اماما</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ما علی العاقل من لغوی اذا مروا کراما</p></div>
<div class="m2"><p>لکن الجاهل ان خاطبنی قلت سلاما</p></div></div>