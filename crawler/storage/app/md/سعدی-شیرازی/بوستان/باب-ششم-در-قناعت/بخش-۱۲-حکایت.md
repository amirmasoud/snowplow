---
title: >-
    بخش ۱۲ - حکایت
---
# بخش ۱۲ - حکایت

<div class="b" id="bn1"><div class="m1"><p>شنیدم که صاحبدلی نیکمرد</p></div>
<div class="m2"><p>یکی خانه بر قامت خویش کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کسی گفت می‌دانمت دسترس</p></div>
<div class="m2"><p>کز این خانه بهتر کنی، گفت بس</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چه می‌خواهم از طارم افراشتن؟</p></div>
<div class="m2"><p>همینم بس از بهر بگذاشتن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مکن خانه بر راه سیل، ای غلام</p></div>
<div class="m2"><p>که کس را نگشت این عمارت تمام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نه از معرفت باشد و عقل و رای</p></div>
<div class="m2"><p>که بر ره کند کاروانی سرای</p></div></div>