---
title: >-
    بخش ۴ - حکایت
---
# بخش ۴ - حکایت

<div class="b" id="bn1"><div class="m1"><p>یکی را تب آمد ز صاحبدلان</p></div>
<div class="m2"><p>کسی گفت شکر بخواه از فلان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بگفت ای پسر تلخی مردنم</p></div>
<div class="m2"><p>به از جور روی ترش بردنم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شکر عاقل از دست آن کس نخورد</p></div>
<div class="m2"><p>که روی از تکبر بر او سرکه کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرو از پی هر چه دل خواهدت</p></div>
<div class="m2"><p>که تمکین تن نور جان کاهدت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کند مرد را نفس اماره خوار</p></div>
<div class="m2"><p>اگر هوشمندی عزیزش مدار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اگر هرچه باشد مرادت خوری</p></div>
<div class="m2"><p>ز دوران بسی نامرادی بری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تنور شکم دم به دم تافتن</p></div>
<div class="m2"><p>مصیبت بود روز نایافتن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به تنگی بریزاندت روی رنگ</p></div>
<div class="m2"><p>چو وقت فراخی کنی معده تنگ</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کشد مرد پرخواره بار شکم</p></div>
<div class="m2"><p>وگر در نیابد کشد بار غم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شکم بنده بسیار بینی خجل</p></div>
<div class="m2"><p>شکم پیش من تنگ بهتر که دل</p></div></div>