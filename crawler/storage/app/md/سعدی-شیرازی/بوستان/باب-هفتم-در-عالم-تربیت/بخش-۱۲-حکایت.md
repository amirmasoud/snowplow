---
title: >-
    بخش ۱۲ - حکایت
---
# بخش ۱۲ - حکایت

<div class="b" id="bn1"><div class="m1"><p>مرا در نظامیه ادرار بود</p></div>
<div class="m2"><p>شب و روز تلقین و تکرار بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مر استاد را گفتم ای پر خرد</p></div>
<div class="m2"><p>فلان یار بر من حسد می‌برد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو من داد معنی دهم در حدیث</p></div>
<div class="m2"><p>بر آید به هم اندرون خبیث</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شنید این سخن پیشوای ادب</p></div>
<div class="m2"><p>به تندی برآشفت و گفت ای عجب!</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حسودی پسندت نیامد ز دوست</p></div>
<div class="m2"><p>که معلوم کردت که غیبت نکوست؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر او راه دوزخ گرفت از خسی</p></div>
<div class="m2"><p>از این راه دیگر تو در وی رسی</p></div></div>