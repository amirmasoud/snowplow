---
title: >-
    بخش ۴ - حکایت در معنی سلامت جاهل در خاموشی
---
# بخش ۴ - حکایت در معنی سلامت جاهل در خاموشی

<div class="b" id="bn1"><div class="m1"><p>یکی خوب خلق خلق پوش بود</p></div>
<div class="m2"><p>که در مصر یک چند خاموش بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خردمند مردم ز نزدیک و دور</p></div>
<div class="m2"><p>به گردش چو پروانه جویان نور</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تفکر شبی با دل خویش کرد</p></div>
<div class="m2"><p>که پوشیده زیر زبان است مرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر همچنین سر به خود در برم</p></div>
<div class="m2"><p>چه دانند مردم که دانشورم؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سخن گفت و دشمن بدانست و دوست</p></div>
<div class="m2"><p>که در مصر نادان تر از وی هم اوست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حضورش پریشان شد و کار زشت</p></div>
<div class="m2"><p>سفر کرد و بر طاق مسجد نبشت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در آیینه گر خویشتن دیدمی</p></div>
<div class="m2"><p>به بی دانشی پرده ندریدمی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چنین زشت از آن پرده برداشتم</p></div>
<div class="m2"><p>که خود را نکو روی پنداشتم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کم آواز را باشد آوازه تیز</p></div>
<div class="m2"><p>چو گفتی و رونق نماندت گریز</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تو را خامشی ای خداوند هوش</p></div>
<div class="m2"><p>وقار است و، نا اهل را پرده پوش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>اگر عالمی هیبت خود مبر</p></div>
<div class="m2"><p>وگر جاهلی پردهٔ خود مدر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ضمیر دل خویش منمای زود</p></div>
<div class="m2"><p>که هر گه که خواهی توانی نمود</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ولیکن چو پیدا شود راز مرد</p></div>
<div class="m2"><p>به کوشش نشاید نهان باز کرد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>قلم سر سلطان چه نیکو نهفت</p></div>
<div class="m2"><p>که تا کارد بر سر نبودش نگفت</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بهایم خموشند و گویا بشر</p></div>
<div class="m2"><p>زبان بسته بهتر که گویا به شر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چو مردم سخن گفت باید به هوش</p></div>
<div class="m2"><p>وگر نه شدن چون بهایم خموش</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>به نطق است و عقل آدمی‌زاده فاش</p></div>
<div class="m2"><p>چو طوطی سخنگوی نادان مباش</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>به نطق آدمی بهتر است از دواب</p></div>
<div class="m2"><p>دواب از تو به گر نگویی صواب</p></div></div>