---
title: >-
    بخش ۱۹ - حکایت اندر نکوهش غمازی و مذلت غمازان
---
# بخش ۱۹ - حکایت اندر نکوهش غمازی و مذلت غمازان

<div class="b" id="bn1"><div class="m1"><p>یکی گفت با صوفیی در صفا</p></div>
<div class="m2"><p>ندانی فلانت چه گفت از قفا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بگفتا خموش، ای برادر، بخفت</p></div>
<div class="m2"><p>ندانسته بهتر که دشمن چه گفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کسانی که پیغام دشمن برند</p></div>
<div class="m2"><p>ز دشمن همانا که دشمن ترند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کسی قول دشمن نیارد به دوست</p></div>
<div class="m2"><p>جز آن کس که در دشمنی یار اوست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نیارست دشمن جفا گفتنم</p></div>
<div class="m2"><p>چنان کز شنیدن بلرزد تنم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تو دشمن‌تری کآوری بر دهان</p></div>
<div class="m2"><p>که دشمن چنین گفت اندر نهان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سخن چین کند تازه جنگ قدیم</p></div>
<div class="m2"><p>به خشم آورد نیکمرد سلیم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از آن همنشین تا توانی گریز</p></div>
<div class="m2"><p>که مر فتنهٔ خفته را گفت خیز</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سیه چال و مرد اندر او بسته پای</p></div>
<div class="m2"><p>به از فتنه از جای بردن به جای</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>میان دو تن جنگ چون آتش است</p></div>
<div class="m2"><p>سخن‌چین بدبخت هیزم کش است</p></div></div>