---
title: >-
    بخش ۲۰ - حکایت فریدون و وزیر و غماز
---
# بخش ۲۰ - حکایت فریدون و وزیر و غماز

<div class="b" id="bn1"><div class="m1"><p>فریدون وزیری پسندیده داشت</p></div>
<div class="m2"><p>که روشن دل و دوربین دیده داشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رضای حق اول نگه داشتی</p></div>
<div class="m2"><p>دگر پاس فرمان شه داشتی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نهد عامل سفله بر خلق رنج</p></div>
<div class="m2"><p>که تدبیر ملک است و توفیر گنج</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر جانب حق نداری نگاه</p></div>
<div class="m2"><p>گزندت رساند هم از پادشاه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یکی رفت پیش ملک بامداد</p></div>
<div class="m2"><p>که هر روزت آسایش و کام باد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>غرض مشنو از من نصیحت پذیر</p></div>
<div class="m2"><p>تو را در نهان دشمن است این وزیر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کس از خاص لشکر نمانده‌ست و عام</p></div>
<div class="m2"><p>که سیم و زر از وی ندارد به وام</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به شرطی که چون شاه گردن فراز</p></div>
<div class="m2"><p>بمیرد، دهند آن زر و سیم باز</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نخواهد تو را زنده این خودپرست</p></div>
<div class="m2"><p>مبادا که نقدش نیاید به دست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>یکی سوی دستور دولت پناه</p></div>
<div class="m2"><p>به چشم سیاست نگه کرد شاه</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>که در صورت دوستان پیش من</p></div>
<div class="m2"><p>به خاطر چرایی بد اندیش من؟</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>زمین پیش تختش ببوسید و گفت</p></div>
<div class="m2"><p>نشاید چو پرسیدی اکنون نهفت</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چنین خواهم ای نامور پادشاه</p></div>
<div class="m2"><p>که باشند خلقت همه نیک خواه</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چو مرگت بود وعدهٔ سیم من</p></div>
<div class="m2"><p>بقا بیش خواهندت از بیم من</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نخواهی که مردم به صدق و نیاز</p></div>
<div class="m2"><p>سرت سبز خواهند و عمرت دراز؟</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>غنیمت شمارند مردان دعا</p></div>
<div class="m2"><p>که جوشن بود پیش تیر بلا</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>پسندید از او شهریار آنچه گفت</p></div>
<div class="m2"><p>گل رویش از تازگی برشکفت</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ز قدر و مکانی که دستور داشت</p></div>
<div class="m2"><p>مکانش بیفزود و قدرش فراشت</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بد اندیش را زجر و تأدیب کرد</p></div>
<div class="m2"><p>پشیمانی از گفتهٔ خویش خورد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ندیدم ز غماز سرگشته‌تر</p></div>
<div class="m2"><p>نگون طالع و بخت برگشته‌تر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ز نادانی و تیره رایی که اوست</p></div>
<div class="m2"><p>خلاف افکند در میان دو دوست</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>کنند این و آن خوش دگر باره دل</p></div>
<div class="m2"><p>وی اندر میان کور بخت و خجل</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>میان دو کس آتش افروختن</p></div>
<div class="m2"><p>نه عقل است و خود در میان سوختن</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چو سعدی کسی ذوق خلوت چشید</p></div>
<div class="m2"><p>که از هر که عالم زبان درکشید</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بگوی آنچه دانی سخن سودمند</p></div>
<div class="m2"><p>وگر هیچ کس را نیاید پسند</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>که فردا پشیمان برآرد خروش</p></div>
<div class="m2"><p>که آوخ چرا حق نکردم به گوش؟</p></div></div>