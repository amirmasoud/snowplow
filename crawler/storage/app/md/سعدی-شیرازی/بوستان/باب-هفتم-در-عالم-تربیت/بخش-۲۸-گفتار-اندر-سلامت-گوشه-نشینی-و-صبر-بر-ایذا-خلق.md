---
title: >-
    بخش ۲۸ - گفتار اندر سلامت گوشه‌نشینی و صبر بر ایذاء خلق
---
# بخش ۲۸ - گفتار اندر سلامت گوشه‌نشینی و صبر بر ایذاء خلق

<div class="b" id="bn1"><div class="m1"><p>اگر در جهان از جهان رسته‌ای است،</p></div>
<div class="m2"><p>در از خلق بر خویشتن بسته‌ای است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کس از دست جور زبانها نرست</p></div>
<div class="m2"><p>اگر خودنمای است و گر حق پرست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگر برپری چون ملک ز آسمان</p></div>
<div class="m2"><p>به دامن در آویزدت بدگمان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به کوشش توان دجله را پیش بست</p></div>
<div class="m2"><p>نشاید زبان بداندیش بست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فرا هم نشینند تردامنان</p></div>
<div class="m2"><p>که این زهد خشک است و آن دام نان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تو روی از پرستیدن حق مپیچ</p></div>
<div class="m2"><p>بهل تا نگیرند خلقت به هیچ</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو راضی شد از بنده یزدان پاک</p></div>
<div class="m2"><p>گر اینها نگردند راضی چه باک؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بد اندیش خلق از حق آگاه نیست</p></div>
<div class="m2"><p>ز غوغای خلقش به حق راه نیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از آن ره به جایی نیاورده‌اند</p></div>
<div class="m2"><p>که اول قدم پی غلط کرده‌اند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دو کس بر حدیثی گمارند گوش</p></div>
<div class="m2"><p>از این تا بدان، ز اهرمن تا سروش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>یکی پند گیرد دگر ناپسند</p></div>
<div class="m2"><p>نپردازد از حرفگیری به پند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>فرو مانده در کنج تاریک جای</p></div>
<div class="m2"><p>چه دریابد از جام گیتی نمای؟</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>مپندار اگر شیر و گر روبهی</p></div>
<div class="m2"><p>کز اینان به مردی و حیلت رهی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>اگر کنج خلوت گزیند کسی</p></div>
<div class="m2"><p>که پروای صحبت ندارد بسی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>مذمت کنندش که زرق است و ریو</p></div>
<div class="m2"><p>ز مردم چنان می گریزد که دیو</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>وگر خنده روی است و آمیزگار</p></div>
<div class="m2"><p>عفیفش ندانند و پرهیزگار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>غنی را به غیبت بکاوند پوست</p></div>
<div class="m2"><p>که فرعون اگر هست در عالم اوست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>وگر بینوایی بگرید به سوز</p></div>
<div class="m2"><p>نگون بخت خوانندش و تیره‌روز</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>وگر کامرانی در آید ز پای</p></div>
<div class="m2"><p>غنیمت شمارند و فضل خدای</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>که تا چند از این جاه و گردن کشی؟</p></div>
<div class="m2"><p>خوشی را بود در قفا ناخوشی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>و گر تنگدستی تنک مایه‌ای</p></div>
<div class="m2"><p>سعادت بلندش کند پایه‌ای</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بخایندش از کینه دندان به زهر</p></div>
<div class="m2"><p>که دون پرور است این فرومایه دهر</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>چو بینند کاری به دستت در است</p></div>
<div class="m2"><p>حریصت شمارند و دنیا پرست</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>وگر دست همت بداری ز کار</p></div>
<div class="m2"><p>گدا پیشه خوانندت و پخته خوار</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>اگر ناطقی طبل پر یاوه‌ای</p></div>
<div class="m2"><p>وگر خامشی نقش گرماوه‌ای</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>تحمل کنان را نخوانند مرد</p></div>
<div class="m2"><p>که بیچاره از بیم سر برنکرد</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>وگر در سرش هول و مردانگی است</p></div>
<div class="m2"><p>گریزند از او کاین چه دیوانگی است؟!</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>تعنت کنندش گر اندک خوری است</p></div>
<div class="m2"><p>که مالش مگر روزی دیگری است</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>وگر نغز و پاکیزه باشد خورش</p></div>
<div class="m2"><p>شکم بنده خوانند و تن پرورش</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>وگر بی تکلف زید مالدار</p></div>
<div class="m2"><p>که زینت بر اهل تمیز است عار</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>زبان در نهندش به ایذا چو تیغ</p></div>
<div class="m2"><p>که بدبخت زر دارد از خود دریغ</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>و گر کاخ و ایوان منقش کند</p></div>
<div class="m2"><p>تن خویش را کسوتی خوش کند</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>به جان آید از طعنه بر وی زنان</p></div>
<div class="m2"><p>که خود را بیاراست همچون زنان</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>اگر پارسایی سیاحت نکرد</p></div>
<div class="m2"><p>سفر کردگانش نخوانند مرد</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>که نارفته بیرون ز آغوش زن</p></div>
<div class="m2"><p>کدامش هنر باشد و رای و فن؟</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>جهاندیده را هم بدرند پوست</p></div>
<div class="m2"><p>که سرگشتهٔ بخت برگشته اوست</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>گرش حظ از اقبال بودی و بهر</p></div>
<div class="m2"><p>زمانه نراندی ز شهرش به شهر</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>عزب را نکوهش کند خرده بین</p></div>
<div class="m2"><p>که می‌رنجد از خفت و خیزش زمین</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>وگر زن کند گوید از دست دل</p></div>
<div class="m2"><p>به گردن در افتاد چون خر به گل</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>نه از جور مردم رهد زشت روی</p></div>
<div class="m2"><p>نه شاهد ز نامردم زشت گوی</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>غلامی به مصر اندرم بنده بود</p></div>
<div class="m2"><p>که چشم از حیا در بر افکنده بود</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>کسی گفت: «هیچ این پسر عقل و هوش</p></div>
<div class="m2"><p>ندارد، بمالش به تعلیم گوش»</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>شبی بر زدم بانگ بر وی درشت</p></div>
<div class="m2"><p>هم او گفت: «مسکین به جورش بکشت!»</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>گرت برکند خشم روزی ز جای</p></div>
<div class="m2"><p>سراسیمه خوانندت و تیره رای</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>وگر بردباری کنی از کسی</p></div>
<div class="m2"><p>بگویند غیرت ندارد بسی</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>سخی را به اندرز گویند: «بس!</p></div>
<div class="m2"><p>که فردا دو دستت بود پیش و پس»</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>وگر قانع و خویشتن‌دار گشت</p></div>
<div class="m2"><p>به تشنیع خلقی گرفتار گشت</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>که همچون پدر خواهد این سفله مرد</p></div>
<div class="m2"><p>که نعمت رها کرد و حسرت ببرد</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>که یارد به کنج سلامت نشست؟</p></div>
<div class="m2"><p>که پیغمبر از خبث ایشان نرست</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>خدا را که مانند و انباز و جفت</p></div>
<div class="m2"><p>ندارد، شنیدی که ترسا چه گفت؟</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>رهایی نیابد کس از دست کس</p></div>
<div class="m2"><p>گرفتار را چاره صبر است و بس</p></div></div>