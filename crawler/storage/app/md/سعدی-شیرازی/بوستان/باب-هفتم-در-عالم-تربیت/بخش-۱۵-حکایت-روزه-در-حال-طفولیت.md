---
title: >-
    بخش ۱۵ - حکایت روزه در حال طفولیت
---
# بخش ۱۵ - حکایت روزه در حال طفولیت

<div class="b" id="bn1"><div class="m1"><p>به طفلی درم رغبت روزه خاست</p></div>
<div class="m2"><p>ندانستمی چپ کدام است و راست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یکی عابد از پارسایان کوی</p></div>
<div class="m2"><p>همی شستن آموختم دست و روی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>که بسم الله اول به سنت بگوی</p></div>
<div class="m2"><p>دوم نیت آور، سوم کف بشوی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پس آن گه دهن شوی و بینی سه بار</p></div>
<div class="m2"><p>مناخر به انگشت کوچک بخار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به سبابه دندان پیشین بمال</p></div>
<div class="m2"><p>که نهی است در روزه بعد از زوال</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>وز آن پس سه مشت آب بر روی زن</p></div>
<div class="m2"><p>ز رستنگه موی سر تا ذقن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دگر دستها تا به مرفق بشوی</p></div>
<div class="m2"><p>ز تسبیح و ذکر آنچه دانی بگوی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دگر مسح سر، بعد از آن غسل پای</p></div>
<div class="m2"><p>همین است و ختمش به نام خدای</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کس از من نداند در این شیوه به</p></div>
<div class="m2"><p>نبینی که فرتوت شد پیر ده؟</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بگفتند با دهخدای آنچه گفت</p></div>
<div class="m2"><p>فرستاد پیغامش اندر نهفت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>که ای زشت کردار زیبا سخن</p></div>
<div class="m2"><p>نخست آنچه گویی به مردم بکن</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نه مسواک در روزه گفتی خطاست</p></div>
<div class="m2"><p>بنی آدم مرده خوردن رواست؟</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>دهن گو ز ناگفتنیها نخست</p></div>
<div class="m2"><p>بشوی آن که از خوردنیها بشست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>کسی را که نام آمد اندر میان</p></div>
<div class="m2"><p>به نیکوترین نام و نعتش بخوان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چو همواره گویی که مردم خرند</p></div>
<div class="m2"><p>مبر ظن که نامت چو مردم برند</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چنان گوی سیرت به کوی اندرم</p></div>
<div class="m2"><p>که گفتن توانی به روی اندرم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>وگر شرمت از دیدهٔ ناظر است</p></div>
<div class="m2"><p>نه ای بی‌بصر، غیب دان حاضر است؟</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>نیاید همی شرمت از خویشتن</p></div>
<div class="m2"><p>کز او فارغ و شرم داری ز من؟</p></div></div>