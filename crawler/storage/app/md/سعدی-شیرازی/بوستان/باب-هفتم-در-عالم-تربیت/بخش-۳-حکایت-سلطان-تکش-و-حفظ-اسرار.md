---
title: >-
    بخش ۳ - حکایت سلطان تکش و حفظ اسرار
---
# بخش ۳ - حکایت سلطان تکش و حفظ اسرار

<div class="b" id="bn1"><div class="m1"><p>تکش با غلامان یکی راز گفت</p></div>
<div class="m2"><p>که این را نباید به کس باز گفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به یک سالش آمد ز دل بر دهان</p></div>
<div class="m2"><p>به یک روز شد منتشر در جهان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بفرمود جلاد را بی دریغ</p></div>
<div class="m2"><p>که بردار سرهای اینان به تیغ</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یکی زآن میان گفت و زنهار خواست</p></div>
<div class="m2"><p>مکش بندگان کاین گناه از تو خاست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تو اول نبستی که سرچشمه بود</p></div>
<div class="m2"><p>چو سیلاب شد پیش بستن چه سود؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تو پیدا مکن راز دل بر کسی</p></div>
<div class="m2"><p>که او خود نگوید بر هر کسی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جواهر به گنجینه داران سپار</p></div>
<div class="m2"><p>ولی راز را خویشتن پاس دار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سخن تا نگویی بر او دست هست</p></div>
<div class="m2"><p>چو گفته شود یابد او بر تو دست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سخن دیو بندی است در چاه دل</p></div>
<div class="m2"><p>به بالای کام و زبانش مهل</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>توان باز دادن ره نره دیو</p></div>
<div class="m2"><p>ولی باز نتوان گرفتن به ریو</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تو دانی که چون دیو رفت از قفس</p></div>
<div class="m2"><p>نیاید به لا حول کس باز پس</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>یکی طفل بر گیرد از رخش بند</p></div>
<div class="m2"><p>نیاید به صد رستم اندر کمند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>مگوی آن که گر بر ملا اوفتد</p></div>
<div class="m2"><p>وجودی از آن در بلا اوفتد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>به دهقان نادان چه خوش گفت زن:</p></div>
<div class="m2"><p>به دانش سخن گوی یا دم مزن</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>مگوی آنچه طاقت نداری شنود</p></div>
<div class="m2"><p>که جو کشته گندم نخواهی درود</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چه نیکو زده‌ست این مثل برهمن</p></div>
<div class="m2"><p>بود حرمت هر کس از خویشتن</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چو دشنام گویی دعا نشنوی</p></div>
<div class="m2"><p>به جز کشتهٔ خویشتن ندروی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>مگوی و منه تا توانی قدم</p></div>
<div class="m2"><p>از اندازه بیرون وز اندازه کم</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>نباید که بسیار بازی کنی</p></div>
<div class="m2"><p>که مر قیمت خویش را بشکنی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>وگر تند باشی به یک بار و تیز</p></div>
<div class="m2"><p>جهان از تو گیرند راه گریز</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>نه کوتاه دستی و بیچارگی</p></div>
<div class="m2"><p>نه زجر و تطاول به یک‌بارگی</p></div></div>