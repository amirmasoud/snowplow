---
title: >-
    بخش ۲۵ - گفتار اندر پرهیز کردن از صحبت احداث
---
# بخش ۲۵ - گفتار اندر پرهیز کردن از صحبت احداث

<div class="b" id="bn1"><div class="m1"><p>خرابت کند شاهد خانه کن</p></div>
<div class="m2"><p>برو خانه آباد گردان به زن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نشاید هوس باختن با گلی</p></div>
<div class="m2"><p>که هر بامدادش بود بلبلی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو خود را به هر مجلسی شمع کرد</p></div>
<div class="m2"><p>تو دیگر چو پروانه گردش مگرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زن خوب خوش خوی آراسته</p></div>
<div class="m2"><p>چه ماند به نادان نو خاسته؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در او دم چو غنچه دمی از وفا</p></div>
<div class="m2"><p>که از خنده افتد چو گل در قفا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نه چون کودک پیچ بر پیچ شنگ</p></div>
<div class="m2"><p>که چون مقل نتوان شکستن به سنگ</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مبین دلفریبش چو حور بهشت</p></div>
<div class="m2"><p>کز آن روی دیگر چو غول است زشت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گرش پای بوسی نداردت پاس</p></div>
<div class="m2"><p>ورش خاک باشی نداند سپاس</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سر از مغز و دست از درم کن تهی</p></div>
<div class="m2"><p>چو خاطر به فرزند مردم نهی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مکن بد به فرزند مردم نگاه</p></div>
<div class="m2"><p>که فرزند خویشت برآید تباه</p></div></div>