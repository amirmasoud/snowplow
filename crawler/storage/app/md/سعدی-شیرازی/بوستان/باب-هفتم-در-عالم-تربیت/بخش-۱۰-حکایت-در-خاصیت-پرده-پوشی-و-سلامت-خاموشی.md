---
title: >-
    بخش ۱۰ - حکایت در خاصیت پرده پوشی و سلامت خاموشی
---
# بخش ۱۰ - حکایت در خاصیت پرده پوشی و سلامت خاموشی

<div class="b" id="bn1"><div class="m1"><p>یکی پیش داود طائی نشست</p></div>
<div class="m2"><p>که دیدم فلان صوفی افتاده مست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قی آلوده دستار و پیراهنش</p></div>
<div class="m2"><p>گروهی سگان حلقه پیرامنش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو پیر از جوان این حکایت شنید</p></div>
<div class="m2"><p>به آزار از او روی در هم کشید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زمانی بر آشفت و گفت ای رفیق</p></div>
<div class="m2"><p>به کار آید امروز یار شفیق</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>برو زآن مقام شنیعش بیار</p></div>
<div class="m2"><p>که در شرع نهی است و در خرقه عار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به پشتش در آور چو مردان که مست</p></div>
<div class="m2"><p>عنان سلامت ندارد به دست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نیوشنده شد زین سخن تنگدل</p></div>
<div class="m2"><p>به فکرت فرو رفت چون خر به گل</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نه زهره که فرمان نگیرد به گوش</p></div>
<div class="m2"><p>نه یارا که مست اندر آرد به دوش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زمانی بپیچید و درمان ندید</p></div>
<div class="m2"><p>ره سر کشیدن ز فرمان ندید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>میان بست و بی اختیارش به دوش</p></div>
<div class="m2"><p>در آورد و شهری بر او عام جوش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>یکی طعنه می‌زد که درویش بین</p></div>
<div class="m2"><p>زهی پارسایان پاکیزه دین!</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>یکی صوفیان بین که می خورده‌اند</p></div>
<div class="m2"><p>مرقع به سیکی گرو کرده‌اند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>اشارت کنان این و آن را به دست</p></div>
<div class="m2"><p>که آن سر گران است و این نیم مست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>به گردن بر از جور دشمن حسام</p></div>
<div class="m2"><p>به از شنعت شهر و جوش عوام</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بلا دید و روزی به محنت گذاشت</p></div>
<div class="m2"><p>به ناکام بردش به جایی که داشت</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>شب از فکرت و نامرادی نخفت</p></div>
<div class="m2"><p>دگر روز پیرش به تعلیم گفت</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>مریز آبروی برادر به کوی</p></div>
<div class="m2"><p>که دهرت نریزد به شهر آبروی</p></div></div>