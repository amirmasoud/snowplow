---
title: >-
    بخش ۳۵ - گفتار اندر تقویت مردان کار آزموده
---
# بخش ۳۵ - گفتار اندر تقویت مردان کار آزموده

<div class="b" id="bn1"><div class="m1"><p>به پیکار دشمن دلیران فرست</p></div>
<div class="m2"><p>هژبران به ناورد شیران فرست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به رای جهاندیدگان کار کن</p></div>
<div class="m2"><p>که صید آزموده‌ست گرگ کهن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مترس از جوانان شمشیر زن</p></div>
<div class="m2"><p>حذر کن ز پیران بسیار فن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جوانان پیل افکن شیرگیر</p></div>
<div class="m2"><p>ندانند دستان روباه پیر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خردمند باشد جهاندیده مرد</p></div>
<div class="m2"><p>که بسیار گرم آزموده‌ست و سرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جوانان شایستهٔ بخت ور</p></div>
<div class="m2"><p>ز گفتار پیران نپیچند سر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گرت مملکت باید آراسته</p></div>
<div class="m2"><p>مده کار معظم به نوخاسته</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سپه را مکن پیشرو جز کسی</p></div>
<div class="m2"><p>که در جنگها بوده باشد بسی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به خردان مفرمای کار درشت</p></div>
<div class="m2"><p>که سندان نشاید شکستن به مشت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>رعیت نوازی و سر لشکری</p></div>
<div class="m2"><p>نه کاری است بازیچه و سرسری</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نخواهی که ضایع شود روزگار</p></div>
<div class="m2"><p>به ناکاردیده مفرمای کار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نتابد سگ صید روی از پلنگ</p></div>
<div class="m2"><p>ز روبه رمد شیر نادیده جنگ</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چو پرورده باشد پسر در شکار</p></div>
<div class="m2"><p>نترسد چو پیش آیدش کارزار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>به کشتی و نخجیر و آماج و گوی</p></div>
<div class="m2"><p>دلاور شود مرد پرخاشجوی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>به گرمابه پرورده و عیش و ناز</p></div>
<div class="m2"><p>برنجد چو بیند در جنگ باز</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>دو مردش نشانند بر پشت زین</p></div>
<div class="m2"><p>بود کش زند کودکی بر زمین</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>یکی را که دیدی تو در جنگ پشت</p></div>
<div class="m2"><p>بکش گر عدو در مصافش نکشت</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>مخنث به از مرد شمشیر زن</p></div>
<div class="m2"><p>که روز وغا سر بتابد چو زن</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چه خوش گفت گرگین به فرزند خویش</p></div>
<div class="m2"><p>چو قربان پیکار بربست و کیش</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>اگر چون زنان جست خواهی گریز</p></div>
<div class="m2"><p>مرو آب مردان جنگی مریز</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>سواری که در جنگ بنمود پشت</p></div>
<div class="m2"><p>نه خود را که نام آوران را بکشت</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>شجاعت نیاید مگر زآن دو یار</p></div>
<div class="m2"><p>که افتند در حلقهٔ کارزار</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>دو همجنس همسفرهٔ همزبان</p></div>
<div class="m2"><p>بکوشند در قلب هیجا به جان</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>که تنگ آیدش رفتن از پیش تیر</p></div>
<div class="m2"><p>برادر به چنگال دشمن اسیر</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چو بینی که یاران نباشند یار</p></div>
<div class="m2"><p>هزیمت ز میدان غنیمت شمار</p></div></div>