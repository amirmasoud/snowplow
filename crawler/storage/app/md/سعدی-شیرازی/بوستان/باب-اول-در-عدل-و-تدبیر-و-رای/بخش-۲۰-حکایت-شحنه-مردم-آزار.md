---
title: >-
    بخش ۲۰ - حکایت شحنه مردم آزار
---
# بخش ۲۰ - حکایت شحنه مردم آزار

<div class="b" id="bn1"><div class="m1"><p>گزیری به چاهی در افتاده بود</p></div>
<div class="m2"><p>که از هول او شیر نر ماده بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بداندیش مردم به جز بد ندید</p></div>
<div class="m2"><p>بیافتاد و عاجزتر از خود ندید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همه شب ز فریاد و زاری نخفت</p></div>
<div class="m2"><p>یکی بر سرش کوفت سنگی و گفت:</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تو هرگز رسیدی به فریاد کس</p></div>
<div class="m2"><p>که می‌خواهی امروز فریادرس؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همه تخم نامردمی کاشتی</p></div>
<div class="m2"><p>ببین لاجرم بر که برداشتی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>که بر جان ریشت نهد مرهمی</p></div>
<div class="m2"><p>که دلها ز ریشت بنالد همی؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تو ما را همی چاه کندی به راه</p></div>
<div class="m2"><p>به سر لاجرم در فتادی به چاه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دو کس چه کنند از پی خاص و عام</p></div>
<div class="m2"><p>یکی نیک محضر، دگر زشت نام</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>یکی تشنه را تا کند تازه حلق</p></div>
<div class="m2"><p>دگر تا به گردن در افتند خلق</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>اگر بد کنی چشم نیکی مدار</p></div>
<div class="m2"><p>که هرگز نیارد گز انگور بار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نپندارم ای در خزان کشته جو</p></div>
<div class="m2"><p>که گندم ستانی به وقت درو</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>درخت زقوم ار به جان پروری</p></div>
<div class="m2"><p>مپندار هرگز کز او بر خوری</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>رطب ناورد چوب خرزهره بار</p></div>
<div class="m2"><p>چو تخم افکنی، بر همان چشم دار</p></div></div>