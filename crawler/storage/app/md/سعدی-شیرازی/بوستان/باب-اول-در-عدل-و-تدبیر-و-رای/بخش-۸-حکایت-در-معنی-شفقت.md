---
title: >-
    بخش ۸ - حکایت در معنی شفقت
---
# بخش ۸ - حکایت در معنی شفقت

<div class="b" id="bn1"><div class="m1"><p>یکی از بزرگان اهل تمیز</p></div>
<div class="m2"><p>حکایت کند ز ابن عبدالعزیز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که بودش نگینی در انگشتری</p></div>
<div class="m2"><p>فرو مانده در قیمتش جوهری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به شب گفتی از جرم گیتی فروز</p></div>
<div class="m2"><p>دری بود از روشنایی چو روز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>قضا را درآمد یکی خشک سال</p></div>
<div class="m2"><p>که شد بدر سیمای مردم هلال</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو در مردم آرام و قوت ندید</p></div>
<div class="m2"><p>خود آسوده بودن مروت ندید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو بیند کسی زهر در کام خلق</p></div>
<div class="m2"><p>کیش بگذرد آب نوشین به حلق</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بفرمود و بفروختندش به سیم</p></div>
<div class="m2"><p>که رحم آمدش بر غریب و یتیم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به یک هفته نقدش به تاراج داد</p></div>
<div class="m2"><p>به درویش و مسکین و محتاج داد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>فتادند در وی ملامت کنان</p></div>
<div class="m2"><p>که دیگر به دستت نیاید چنان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شنیدم که می‌گفت و باران دمع</p></div>
<div class="m2"><p>فرو می‌دویدش به عارض چو شمع</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>که زشت است پیرایه بر شهریار</p></div>
<div class="m2"><p>دل شهری از ناتوانی فگار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مرا شاید انگشتری بی‌نگین</p></div>
<div class="m2"><p>نشاید دل خلقی اندوهگین</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>خنک آن که آسایش مرد و زن</p></div>
<div class="m2"><p>گزیند بر آرایش خویشتن</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نکردند رغبت هنرپروران</p></div>
<div class="m2"><p>به شادی خویش از غم دیگران</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>اگر خوش بخسبد ملک بر سریر</p></div>
<div class="m2"><p>نپندارم آسوده خسبد فقیر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>وگر زنده دارد شب دیر باز</p></div>
<div class="m2"><p>بخسبند مردم به آرام و ناز</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بحمدالله این سیرت و راه راست</p></div>
<div class="m2"><p>اتابک ابوبکر بن سعد راست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>کس از فتنه در پارس دیگر نشان</p></div>
<div class="m2"><p>نبیند مگر قامت مهوشان</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>یکی پنج بیتم خوش آمد به گوش</p></div>
<div class="m2"><p>که در مجلسی می‌سرودند دوش</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>مرا راحت از زندگی دوش بود</p></div>
<div class="m2"><p>که آن ماهرویم در آغوش بود</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>مر او را چو دیدم سر از خواب مست</p></div>
<div class="m2"><p>بدو گفتم ای سرو پیش تو پست</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>دمی نرگس از خواب نوشین بشوی</p></div>
<div class="m2"><p>چو گلبن بخند و چو بلبل بگوی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>چه می‌خسبی ای فتنه روزگار؟</p></div>
<div class="m2"><p>بیا و می لعل نوشین بیار</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>نگه کرد شوریده از خواب و گفت</p></div>
<div class="m2"><p>مرا فتنه خوانی و گویی مخفت</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>در ایام سلطان روشن نفس</p></div>
<div class="m2"><p>نبیند دگر فتنه بیدار کس</p></div></div>