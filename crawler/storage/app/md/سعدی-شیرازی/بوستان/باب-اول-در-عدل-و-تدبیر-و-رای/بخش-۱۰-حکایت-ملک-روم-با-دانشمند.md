---
title: >-
    بخش ۱۰ - حکایت ملک روم با دانشمند
---
# بخش ۱۰ - حکایت ملک روم با دانشمند

<div class="b" id="bn1"><div class="m1"><p>شنیدم که بگریست سلطان روم</p></div>
<div class="m2"><p>بر نیکمردی ز اهل علوم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که پایابم از دست دشمن نماند</p></div>
<div class="m2"><p>جز این قلعه و شهر با من نماند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بسی جهد کردم که فرزند من</p></div>
<div class="m2"><p>پس از من بود سرور انجمن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کنون دشمن بدگهر دست یافت</p></div>
<div class="m2"><p>سر دست مردی و جهدم بتافت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چه تدبیر سازم، چه درمان کنم؟</p></div>
<div class="m2"><p>که از غم بفرسود جان در تنم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بگفت ای برادر غم خویش خور</p></div>
<div class="m2"><p>که از عمر بهتر شد و بیشتر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تو را این قدر تا بمانی بس است</p></div>
<div class="m2"><p>چو رفتی جهان جای دیگر کس است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اگر هوشمند است و گر بی‌خرد</p></div>
<div class="m2"><p>غم او مخور کاو غم خود خورد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مشقت نیرزد جهان داشتن</p></div>
<div class="m2"><p>گرفتن به شمشیر و بگذاشتن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بدین پنج روزه اقامت مناز</p></div>
<div class="m2"><p>به اندیشه تدبیر رفتن بساز</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>که را دانی از خسروان عجم</p></div>
<div class="m2"><p>ز عهد فریدون و ضحاک و جم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>که بر تخت و ملکش نیامد زوال؟</p></div>
<div class="m2"><p>نماند به جز ملک ایزد تعال</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>که را جاودان ماندن امید ماند</p></div>
<div class="m2"><p>چو کس را نبینی که جاوید ماند؟</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>که را سیم و زر ماند و گنج و مال</p></div>
<div class="m2"><p>پس از وی به چندی شود پایمال</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>وز آن کس که خیری بماند روان</p></div>
<div class="m2"><p>دمادم رسد رحمتش بر روان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بزرگی کز او نام نیکو نماند</p></div>
<div class="m2"><p>توان گفت با اهل دل کاو نماند</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>الا تا درخت کرم پروری</p></div>
<div class="m2"><p>گر امیدواری کز او بر خوری</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>کرم کن که فردا که دیوان نهند</p></div>
<div class="m2"><p>منازل به مقدار احسان دهند</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>یکی را که سعی قدم پیشتر</p></div>
<div class="m2"><p>به درگاه حق، منزلت بیشتر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>یکی باز پس خائن و شرمسار</p></div>
<div class="m2"><p>بترسد همی مرد ناکرده کار</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بهل تا به دندان گزد پشت دست</p></div>
<div class="m2"><p>تنوری چنین گرم و نانی نبست</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بدانی گه غله برداشتن</p></div>
<div class="m2"><p>که سستی بود تخم ناکاشتن</p></div></div>