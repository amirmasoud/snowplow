---
title: >-
    بخش ۲۷ - حکایت
---
# بخش ۲۷ - حکایت

<div class="b" id="bn1"><div class="m1"><p>چو الب ارسلان جان به جان‌بخش داد</p></div>
<div class="m2"><p>پسر تاج شاهی به سر برنهاد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به تربت سپردندش از تاجگاه</p></div>
<div class="m2"><p>نه جای نشستن بد آماجگاه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چنین گفت دیوانه‌ای هوشیار</p></div>
<div class="m2"><p>چو دیدش پسر روز دیگر سوار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زهی ملک و دوران سر در نشیب</p></div>
<div class="m2"><p>پدر رفت و پای پسر در رکیب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چنین است گردیدن روزگار</p></div>
<div class="m2"><p>سبک سیر و بدعهد و ناپایدار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو دیرینه روزی سرآورد عهد</p></div>
<div class="m2"><p>جوان دولتی سر برآرد ز مهد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>منه بر جهان دل که بیگانه‌ای است</p></div>
<div class="m2"><p>چو مطرب که هر روز در خانه‌ای است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نه لایق بود عیش با دلبری</p></div>
<div class="m2"><p>که هر بامدادش بود شوهری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نکویی کن امسال چون ده تو راست</p></div>
<div class="m2"><p>که سال دگر دیگری دهخداست</p></div></div>