---
title: >-
    بخش ۱۳ - حکایت در معنی رحمت با ناتوانان در حال توانایی
---
# بخش ۱۳ - حکایت در معنی رحمت با ناتوانان در حال توانایی

<div class="b" id="bn1"><div class="m1"><p>چنان قحط سالی شد اندر دمشق</p></div>
<div class="m2"><p>که یاران فراموش کردند عشق</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چنان آسمان بر زمین شد بخیل</p></div>
<div class="m2"><p>که لب تر نکردند زرع و نخیل</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بخوشید سرچشمه‌های قدیم</p></div>
<div class="m2"><p>نماند آب، جز آب چشم یتیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نبودی به جز آه بیوه زنی</p></div>
<div class="m2"><p>اگر برشدی دودی از روزنی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو درویش بی رنگ دیدم درخت</p></div>
<div class="m2"><p>قوی بازوان سست و درمانده سخت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نه در کوه سبزی نه در باغ شخ</p></div>
<div class="m2"><p>ملخ بوستان خورده مردم ملخ</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در آن حال پیش آمدم دوستی</p></div>
<div class="m2"><p>از او مانده بر استخوان پوستی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>وگر چه به مکنت قوی حال بود</p></div>
<div class="m2"><p>خداوند جاه و زر و مال بود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بدو گفتم: ای یار پاکیزه خوی</p></div>
<div class="m2"><p>چه درماندگی پیشت آمد؟ بگوی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بغرید بر من که عقلت کجاست؟</p></div>
<div class="m2"><p>چو دانی و پرسی سؤالت خطاست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نبینی که سختی به غایت رسید</p></div>
<div class="m2"><p>مشقت به حد نهایت رسید؟</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نه باران همی آید از آسمان</p></div>
<div class="m2"><p>نه بر می‌رود دود فریادخوان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بدو گفتم: آخر تو را باک نیست</p></div>
<div class="m2"><p>کشد زهر جایی که تریاک نیست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گر از نیستی دیگری شد هلاک</p></div>
<div class="m2"><p>تو را هست، بط را ز طوفان چه باک؟</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نگه کرد رنجیده در من فقیه</p></div>
<div class="m2"><p>نگه کردن عالم اندر سفیه</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>که مرد ار چه بر ساحل است، ای رفیق</p></div>
<div class="m2"><p>نیاساید و دوستانش غریق</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>من از بینوایی نیم روی زرد</p></div>
<div class="m2"><p>غم بینوایان رخم زرد کرد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>نخواهد که بیند خردمند، ریش</p></div>
<div class="m2"><p>نه بر عضو مردم، نه بر عضو خویش</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>یکی اول از تندرستان منم</p></div>
<div class="m2"><p>که ریشی ببینم بلرزد تنم</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>منغص بود عیش آن تندرست</p></div>
<div class="m2"><p>که باشد به پهلوی بیمار سست</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چو بینم که درویش مسکین نخورد</p></div>
<div class="m2"><p>به کام اندرم لقمه زهر است و درد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>یکی را به زندان درش دوستان</p></div>
<div class="m2"><p>کجا ماندش عیش در بوستان؟</p></div></div>