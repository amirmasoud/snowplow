---
title: >-
    بخش ۶ - گفتار اندر نظر در حق رعیت مظلوم
---
# بخش ۶ - گفتار اندر نظر در حق رعیت مظلوم

<div class="b" id="bn1"><div class="m1"><p>تو کی بشنوی نالهٔ دادخواه</p></div>
<div class="m2"><p>به کیوان برت کلهٔ خوابگاه؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چنان خسب کآید فغانت به گوش</p></div>
<div class="m2"><p>اگر دادخواهی برآرد خروش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>که نالد ز ظالم که در دور توست</p></div>
<div class="m2"><p>که هر جور کاو می‌کند جور توست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نه سگ دامن کاروانی درید</p></div>
<div class="m2"><p>که دهقان نادان که سگ پرورید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دلیر آمدی سعدیا در سخن</p></div>
<div class="m2"><p>چو تیغت به دست است فتحی بکن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بگو آنچه دانی که حق گفته به</p></div>
<div class="m2"><p>نه رشوت ستانی و نه عشوه ده</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>طمع بند و دفتر ز حکمت بشوی</p></div>
<div class="m2"><p>طمع بگسل و هرچه دانی بگوی</p></div></div>