---
title: >-
    بخش ۳۲ - حکایت در معنی خاموشی از نصیحت کسی که پند نپذیرد
---
# بخش ۳۲ - حکایت در معنی خاموشی از نصیحت کسی که پند نپذیرد

<div class="b" id="bn1"><div class="m1"><p>حکایت کنند از جفاگستری</p></div>
<div class="m2"><p>که فرماندهی داشت بر کشوری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در ایام او روز مردم چو شام</p></div>
<div class="m2"><p>شب از بیم او خواب مردم حرام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همه روز نیکان از او در بلا</p></div>
<div class="m2"><p>به شب دست پاکان از او بر دعا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گروهی بر شیخ آن روزگار</p></div>
<div class="m2"><p>ز دست ستمگر گرستند زار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>که ای پیر دانای فرخنده رای</p></div>
<div class="m2"><p>بگوی این جوان را بترس از خدای</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بگفتا دریغ آیدم نام دوست</p></div>
<div class="m2"><p>که هر کس نه در خورد پیغام اوست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کسی را که بینی ز حق بر کران</p></div>
<div class="m2"><p>منه با وی، ای خواجه، حق در میان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دریغ است با سفله گفت از علوم</p></div>
<div class="m2"><p>که ضایع شود تخم در شوره بوم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو در وی نگیرد عدو داندت</p></div>
<div class="m2"><p>برنجد به جان و برنجاندت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تو را عادت، ای پادشه، حق روی است</p></div>
<div class="m2"><p>دل مرد حق گوی از این جا قوی است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نگین خصلتی دارد ای نیکبخت</p></div>
<div class="m2"><p>که در موم گیرد نه در سنگ سخت</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>عجب نیست گر ظالم از من به جان</p></div>
<div class="m2"><p>برنجد که دزد است و من پاسبان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>تو هم پاسبانی به انصاف و داد</p></div>
<div class="m2"><p>که حفظ خدا پاسبان تو باد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>تو را نیست منت ز روی قیاس</p></div>
<div class="m2"><p>خداوند را من و فضل و سپاس</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>که در کار خیرت به خدمت بداشت</p></div>
<div class="m2"><p>نه چون دیگرانت معطل گذاشت</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>همه کس به میدان کوشش درند</p></div>
<div class="m2"><p>ولی گوی بخشش نه هر کس برند</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>تو حاصل نکردی به کوشش بهشت</p></div>
<div class="m2"><p>خدا در تو خوی بهشتی بهشت</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>دلت روشن و وقت مجموع باد</p></div>
<div class="m2"><p>قدم ثابت و پایه مرفوع باد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>حیاتت خوش و رفتنت بر صواب</p></div>
<div class="m2"><p>عبادت قبول و دعا مستجاب</p></div></div>