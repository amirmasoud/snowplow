---
title: >-
    بخش ۳۶ - گفتار اندر دلداری هنرمندان
---
# بخش ۳۶ - گفتار اندر دلداری هنرمندان

<div class="b" id="bn1"><div class="m1"><p>دو تن، پرور ای شاه کشور گشای</p></div>
<div class="m2"><p>یکی اهل رزم و دگر اهل رای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز نام آوران گوی دولت برند</p></div>
<div class="m2"><p>که دانا و شمشیر زن پرورند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر آن کاو قلم را نورزید و تیغ</p></div>
<div class="m2"><p>بر او گر بمیرد مگو ای دریغ</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>قلم زن نکودار و شمشیر زن</p></div>
<div class="m2"><p>نه مطرب که مردی نیاید ز زن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نه مردی است دشمن در اسباب جنگ</p></div>
<div class="m2"><p>تو مدهوش ساقی و آواز چنگ</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بسا اهل دولت به بازی نشست</p></div>
<div class="m2"><p>که دولت برفتش به بازی ز دست</p></div></div>