---
title: >-
    بخش ۱ - سرآغاز
---
# بخش ۱ - سرآغاز

<div class="b" id="bn1"><div class="m1"><p>بیا تا برآریم دستی ز دل</p></div>
<div class="m2"><p>که نتوان برآورد فردا ز گل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به فصل خزان در نبینی درخت</p></div>
<div class="m2"><p>که بی برگ ماند ز سرمای سخت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>برآرد تهی دستهای نیاز</p></div>
<div class="m2"><p>ز رحمت نگردد تهیدست باز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مپندار از آن در که هرگز نبست</p></div>
<div class="m2"><p>که نومید گردد بر آورده دست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>قضا خلعتی نامدارش دهد</p></div>
<div class="m2"><p>قدر میوه در آستینش نهد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همه طاعت آرند و مسکین نیاز</p></div>
<div class="m2"><p>بیا تا به درگاه مسکین نواز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو شاخ برهنه برآریم دست</p></div>
<div class="m2"><p>که بی برگ از این بیش نتوان نشست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خداوندگارا نظر کن به جود</p></div>
<div class="m2"><p>که جرم آمد از بندگان در وجود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گناه آید از بندهٔ خاکسار</p></div>
<div class="m2"><p>به امید عفو خداوندگار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کریما به رزق تو پرورده‌ایم</p></div>
<div class="m2"><p>به انعام و لطف تو خو کرده‌ایم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گدا چون کرم بیند و لطف و ناز</p></div>
<div class="m2"><p>نگردد ز دنبال بخشنده باز</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چو ما را به دنیا تو کردی عزیز</p></div>
<div class="m2"><p>به عقبی همین چشم داریم نیز</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>عزیزی و خواری تو بخشی و بس</p></div>
<div class="m2"><p>عزیز تو خواری نبیند ز کس</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>خدایا به عزت که خوارم مکن</p></div>
<div class="m2"><p>به ذل گنه شرمسارم مکن</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>مسلط مکن چون منی بر سرم</p></div>
<div class="m2"><p>ز دست تو به گر عقوبت برم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>به گیتی بتر زین نباشد بدی</p></div>
<div class="m2"><p>جفا بردن از دست همچون خودی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>مرا شرمساری ز روی تو بس</p></div>
<div class="m2"><p>دگر شرمسارم مکن پیش کس</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گرم بر سر افتد ز تو سایه‌ای</p></div>
<div class="m2"><p>سپهرم بود کهترین پایه‌ای</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>اگر تاج بخشی سر افرازدم</p></div>
<div class="m2"><p>تو بردار تا کس نیندازدم</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>تنم می‌بلرزد چو یاد آورم</p></div>
<div class="m2"><p>مناجات شوریده‌ای در حرم</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>که می‌گفت شوریدهٔ دلفگار</p></div>
<div class="m2"><p>الها ببخش و به ذلّم مدار</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>همی‌گفت با حق به زاری بسی</p></div>
<div class="m2"><p>میفکن که دستم نگیرد کسی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>به لطفم بخوان و مران از درم</p></div>
<div class="m2"><p>ندارد به جز آستانت سرم</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>تو دانی که مسکین و بیچاره‌ایم</p></div>
<div class="m2"><p>فرو مانده نفس اماره‌ایم</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>نمی‌تازد این نفس سرکش چنان</p></div>
<div class="m2"><p>که عقلش تواند گرفتن عنان</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>که با نفس و شیطان بر آید به زور؟</p></div>
<div class="m2"><p>مصاف پلنگان نیاید ز مور</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>به مردان راهت که راهی بده</p></div>
<div class="m2"><p>وز این دشمنانم پناهی بده</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>خدایا به ذات خداوندیت</p></div>
<div class="m2"><p>به اوصاف بی مثل و مانندیت</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>به لبیک حجاج بیت‌الحرام</p></div>
<div class="m2"><p>به مدفون یثرب علیه‌السلام</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>به تکبیر مردان شمشیر زن</p></div>
<div class="m2"><p>که مرد وغا را شمارند زن</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>به طاعات پیران آراسته</p></div>
<div class="m2"><p>به صدق جوانان نوخاسته</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>که ما را در آن ورطهٔ یک نفس</p></div>
<div class="m2"><p>ز ننگ دو گفتن به فریاد رس</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>امید است از آنان که طاعت کنند</p></div>
<div class="m2"><p>که بی طاعتان را شفاعت کنند</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>به پاکان کز آلایشم دور دار</p></div>
<div class="m2"><p>وگر زلتی رفت معذور دار</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>به پیران پشت از عبادت دو تا</p></div>
<div class="m2"><p>ز شرم گنه دیده بر پشت پا</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>که چشمم ز روی سعادت مبند</p></div>
<div class="m2"><p>زبانم به وقت شهادت مبند</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>چراغ یقینم فرا راه دار</p></div>
<div class="m2"><p>ز بد کردنم دست کوتاه دار</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>بگردان ز نادیدنی دیده‌ام</p></div>
<div class="m2"><p>مده دست بر ناپسندیده‌ام</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>من آن ذره‌ام در هوای تو نیست</p></div>
<div class="m2"><p>وجود و عدم ز احتقارم یکی است</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>ز خورشید لطفت شعاعی بسم</p></div>
<div class="m2"><p>که جز در شعاعت نبیند کسم</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>بدی را نگه کن که بهتر کس است</p></div>
<div class="m2"><p>گدا را ز شاه التفاتی بس است</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>مرا گر بگیری به انصاف و داد</p></div>
<div class="m2"><p>بنالم که عفوم نه این وعده داد</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>خدایا به ذلت مران از درم</p></div>
<div class="m2"><p>که صورت نبندد دری دیگرم</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>ور از جهل غایب شدم روز چند</p></div>
<div class="m2"><p>کنون کآمدم در به رویم مبند</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>چه عذر آرم از ننگ تردامنی؟</p></div>
<div class="m2"><p>مگر عجز پیش آورم کای غنی</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>فقیرم به جرم و گناهم مگیر</p></div>
<div class="m2"><p>غنی را ترحم بود بر فقیر</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>چرا باید از ضعف حالم گریست؟</p></div>
<div class="m2"><p>اگر من ضعیفم پناهم قوی است</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>خدایا به غفلت شکستیم عهد</p></div>
<div class="m2"><p>چه زور آورد با قضا دست جهد؟</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>چه برخیزد از دست تدبیر ما؟</p></div>
<div class="m2"><p>همین نکته بس عذر تقصیر ما</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>همه هر چه کردم تو بر هم زدی</p></div>
<div class="m2"><p>چه قوت کند با خدایی خودی؟</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>نه من سر ز حکمت به در می‌برم</p></div>
<div class="m2"><p>که حکمت چنین می‌رود بر سرم</p></div></div>