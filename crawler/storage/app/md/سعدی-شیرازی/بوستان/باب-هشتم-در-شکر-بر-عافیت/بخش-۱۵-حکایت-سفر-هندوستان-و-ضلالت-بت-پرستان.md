---
title: >-
    بخش ۱۵ - حکایت سفر هندوستان و ضلالت بت پرستان
---
# بخش ۱۵ - حکایت سفر هندوستان و ضلالت بت پرستان

<div class="b" id="bn1"><div class="m1"><p>بتی دیدم از عاج در سومنات</p></div>
<div class="m2"><p>مرصع چو در جاهلیت منات</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چنان صورتش بسته تمثالگر</p></div>
<div class="m2"><p>که صورت نبندد از آن خوبتر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز هر ناحیت کاروانها روان</p></div>
<div class="m2"><p>به دیدار آن صورت بی روان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>طمع کرده رایان چین و چگل</p></div>
<div class="m2"><p>چو سعدی وفا ز آن بت سخت دل</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زبان آوران رفته از هر مکان</p></div>
<div class="m2"><p>تضرع کنان پیش آن بی زبان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>فرو ماندم از کشف آن ماجرا</p></div>
<div class="m2"><p>که حیی جمادی پرستد چرا؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مغی را که با من سر و کار بود</p></div>
<div class="m2"><p>نکوگوی و هم حجره و یار بود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به نرمی بپرسیدم ای برهمن</p></div>
<div class="m2"><p>عجب دارم از کار این بقعه من</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>که مدهوش این ناتوان پیکرند</p></div>
<div class="m2"><p>مقید به چاه ضلال اندرند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نه نیروی دستش، نه رفتار پای</p></div>
<div class="m2"><p>ورش بفکنی بر نخیزد ز جای</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نبینی که چشمانش از کهرباست؟</p></div>
<div class="m2"><p>وفا جستن از سنگ چشمان خطاست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بر این گفتم آن دوست دشمن گرفت</p></div>
<div class="m2"><p>چو آتش شد از خشم و در من گرفت</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>مغان را خبر کرد و پیران دیر</p></div>
<div class="m2"><p>ندیدم در آن انجمن روی خیر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>فتادند گبران پازند خوان</p></div>
<div class="m2"><p>چو سگ در من از بهر آن استخوان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چو آن راه کژ پیششان راست بود</p></div>
<div class="m2"><p>ره راست در چشمشان کژ نمود</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>که مرد ار چه دانا و صاحبدل است</p></div>
<div class="m2"><p>به نزدیک بی‌دانشان جاهل است</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>فرو ماندم از چاره همچون غریق</p></div>
<div class="m2"><p>برون از مدارا ندیدم طریق</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چو بینی که جاهل به کین اندر است</p></div>
<div class="m2"><p>سلامت به تسلیم و لین اندر است</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>مهین برهمن را ستودم بلند</p></div>
<div class="m2"><p>که ای پیر تفسیر استا و زند</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>مرا نیز با نقش این بت خوش است</p></div>
<div class="m2"><p>که شکلی خوش و قامتی دلکش است</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بدیع آیدم صورتش در نظر</p></div>
<div class="m2"><p>ولیکن ز معنی ندارم خبر</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>که سالوک این منزلم عن قریب</p></div>
<div class="m2"><p>بد از نیک کمتر شناسد غریب</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>تو دانی که فرزین این رقعه‌ای</p></div>
<div class="m2"><p>نصیحتگر شاه این بقعه‌ای</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چه معنی است در صورت این صنم</p></div>
<div class="m2"><p>که اول پرستندگانش منم</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>عبادت به تقلید گمراهی است</p></div>
<div class="m2"><p>خنک رهروی را که آگاهی است</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>برهمن ز شادی بر افروخت روی</p></div>
<div class="m2"><p>پسندید و گفت ای پسندیده گوی</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>سؤالت صواب است و فعلت جمیل</p></div>
<div class="m2"><p>به منزل رسد هر که جوید دلیل</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بسی چون تو گردیدم اندر سفر</p></div>
<div class="m2"><p>بتان دیدم از خویشتن بی خبر</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>جز این بت که هر صبح از اینجا که هست</p></div>
<div class="m2"><p>برآرد به یزدان دادار دست</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>وگر خواهی امشب همینجا بباش</p></div>
<div class="m2"><p>که فردا شود سر این بر تو فاش</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>شب آنجا ببودم به فرمان پیر</p></div>
<div class="m2"><p>چو بیژن به چاه بلا در اسیر</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>شبی همچو روز قیامت دراز</p></div>
<div class="m2"><p>مغان گرد من بی وضو در نماز</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>کشیشان هرگز نیازرده آب</p></div>
<div class="m2"><p>بغلها چو مردار در آفتاب</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>مگر کرده بودم گناهی عظیم</p></div>
<div class="m2"><p>که بردم در آن شب عذابی الیم</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>همه شب در این قید غم مبتلا</p></div>
<div class="m2"><p>یکم دست بر دل، یکی بر دعا</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>که ناگه دهل زن فرو کوفت کوس</p></div>
<div class="m2"><p>بخواند از فضای برهمن خروس</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>خطیب سیه پوش شب بی خلاف</p></div>
<div class="m2"><p>بر آهخت شمشیر روز از غلاف</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>فتاد آتش صبح در سوخته</p></div>
<div class="m2"><p>به یک دم جهانی شد افروخته</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>تو گفتی که در خطهٔ زنگبار</p></div>
<div class="m2"><p>ز یک گوشه ناگه در آمد تتار</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>مغان تبه رای ناشسته روی</p></div>
<div class="m2"><p>به دیر آمدند از در و دشت و کوی</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>کس از مرد در شهر و از زن نماند</p></div>
<div class="m2"><p>در آن بتکده جای درزن نماند</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>من از غصه رنجور و از خواب مست</p></div>
<div class="m2"><p>که ناگاه تمثال برداشت دست</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>به یک بار از ایشان برآمد خروش</p></div>
<div class="m2"><p>تو گفتی که دریا بر آمد به جوش</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>چو بتخانه خالی شد از انجمن</p></div>
<div class="m2"><p>برهمن نگه کرد خندان به من</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>که دانم تو را بیش مشکل نماند</p></div>
<div class="m2"><p>حقیقت عیان گشت و باطل نماند</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>چو دیدم که جهل اندر او محکم است</p></div>
<div class="m2"><p>خیال محال اندر او مدغم است</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>نیارستم از حق دگر هیچ گفت</p></div>
<div class="m2"><p>که حق ز اهل باطل بباید نهفت</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>چو بینی زبر دست را زور دست</p></div>
<div class="m2"><p>نه مردی بود پنجهٔ خود شکست</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>زمانی به سالوس گریان شدم</p></div>
<div class="m2"><p>که من زآنچه گفتم پشیمان شدم</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>به گریه دل کافران کرد میل</p></div>
<div class="m2"><p>عجب نیست سنگ ار بگردد به سیل</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>دویدند خدمت کنان سوی من</p></div>
<div class="m2"><p>به عزت گرفتند بازوی من</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>شدم عذرگویان بر شخص عاج</p></div>
<div class="m2"><p>به کرسی زر کوفت بر تخت ساج</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>بتک را یکی بوسه دادم به دست</p></div>
<div class="m2"><p>که لعنت بر او باد و بر بت پرست</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>به تقلید کافر شدم روز چند</p></div>
<div class="m2"><p>برهمن شدم در مقالات زند</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>چو دیدم که در دیر گشتم امین</p></div>
<div class="m2"><p>نگنجیدم از خرمی در زمین</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>در دیر محکم ببستم شبی</p></div>
<div class="m2"><p>دویدم چپ و راست چون عقربی</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>نگه کردم از زیر تخت و زبر</p></div>
<div class="m2"><p>یکی پرده دیدم مکلل به زر</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>پس پرده مطرانی آذرپرست</p></div>
<div class="m2"><p>مجاور سر ریسمانی به دست</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>به فورم در آن حال معلوم شد</p></div>
<div class="m2"><p>چو داود کآهن بر او موم شد</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>که ناچار چون در کشد ریسمان</p></div>
<div class="m2"><p>بر آرد صنم دست، فریادخوان</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>برهمن شد از روی من شرمسار</p></div>
<div class="m2"><p>که شنعت بود بخیه بر روی کار</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>بتازید و من در پیش تاختم</p></div>
<div class="m2"><p>نگونش به چاهی در انداختم</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>که دانستم ار زنده آن برهمن</p></div>
<div class="m2"><p>بماند، کند سعی در خون من</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>پسندد که از من بر آید دمار</p></div>
<div class="m2"><p>مبادا که سرش کنم آشکار</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>چو از کار مفسد خبر یافتی</p></div>
<div class="m2"><p>ز دستش برآور چو دریافتی</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>که گر زنده‌اش مانی، آن بی هنر</p></div>
<div class="m2"><p>نخواهد تو را زندگانی دگر</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>وگر سر به خدمت نهد بر درت</p></div>
<div class="m2"><p>اگر دست یابد ببرد سرت</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>فریبنده را پای در پی منه</p></div>
<div class="m2"><p>چو رفتی و دیدی امانش مده</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>تمامش بکشتم به سنگ آن خبیث</p></div>
<div class="m2"><p>که از مرده دیگر نیاید حدیث</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>چو دیدم که غوغایی انگیختم</p></div>
<div class="m2"><p>رها کردم آن بوم و بگریختم</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>چو اندر نیستانی آتش زدی</p></div>
<div class="m2"><p>ز شیران بپرهیز اگر بخردی</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>مکش بچهٔ مار مردم گزای</p></div>
<div class="m2"><p>چو کشتی در آن خانه دیگر مپای</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>چو زنبور خانه بیاشوفتی</p></div>
<div class="m2"><p>گریز از محلت که گرم اوفتی</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>به چابک‌تر از خود مینداز تیر</p></div>
<div class="m2"><p>چو افتاد، دامن به دندان بگیر</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>در اوراق سعدی چنین پند نیست</p></div>
<div class="m2"><p>که چون پای دیوار کندی مایست</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>به هند آمدم بعد از آن رستخیز</p></div>
<div class="m2"><p>وز آنجا به راه یمن تا حجیز</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>از آن جمله سختی که بر من گذشت</p></div>
<div class="m2"><p>دهانم جز امروز شیرین نگشت</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>در اقبال و تأیید بوبکر سعد</p></div>
<div class="m2"><p>که مادر نزاید چنو قبل و بعد</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>ز جور فلک دادخواه آمدم</p></div>
<div class="m2"><p>در این سایه‌گستر پناه آمدم</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>دعاگوی این دولتم بنده‌وار</p></div>
<div class="m2"><p>خدایا تو این سایه پاینده دار</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>که مرهم نهادم نه در خورد ریش</p></div>
<div class="m2"><p>که در خورد انعام و اکرام خویش</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>کی این شکر نعمت به جای آورم</p></div>
<div class="m2"><p>و گر پای گردد به خدمت سرم؟</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>فرج یافتم بعد از آن بندها</p></div>
<div class="m2"><p>هنوزم به گوش است از آن پندها</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>یکی آن که هر گه که دست نیاز</p></div>
<div class="m2"><p>برآرم به درگاه دانای راز</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>به یاد آید آن لعبت چینیم</p></div>
<div class="m2"><p>کند خاک در چشم خودبینیم</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>بدانم که دستی که برداشتم</p></div>
<div class="m2"><p>به نیروی خود بر نیفراشتم</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>نه صاحبدلان دست بر می‌کشند</p></div>
<div class="m2"><p>که سررشته از غیب در می‌کشند</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>در خیر باز است و طاعت ولیک</p></div>
<div class="m2"><p>نه هر کس تواناست بر فعل نیک</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>همین است مانع که در بارگاه</p></div>
<div class="m2"><p>نشاید شدن جز به فرمان شاه</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>کلید قدر نیست در دست کس</p></div>
<div class="m2"><p>توانای مطلق خدای است و بس</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>پس ای مرد پوینده بر راه راست</p></div>
<div class="m2"><p>تو را نیست منت، خداوند راست</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>چو در غیب نیکو نهادت سرشت</p></div>
<div class="m2"><p>نیاید ز خوی تو کردار زشت</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>ز زنبور کرد این حلاوت پدید</p></div>
<div class="m2"><p>همان کس که در مار زهر آفرید</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>چو خواهد که ملک تو ویران کند</p></div>
<div class="m2"><p>نخست از تو خلقی پریشان کند</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>وگر باشدش بر تو بخشایشی</p></div>
<div class="m2"><p>رساند به خلق از تو آسایشی</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>تکبر مکن بر ره راستی</p></div>
<div class="m2"><p>که دستت گرفتند و برخاستی</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>سخن سودمند است اگر بشنوی</p></div>
<div class="m2"><p>به مردان رسی گر طریقت روی</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>مقامی بیابی گرت ره دهند</p></div>
<div class="m2"><p>که بر خوان عزت سماطت نهند</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>ولیکن نباید که تنها خوری</p></div>
<div class="m2"><p>ز درویش درمنده یاد آوری</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>فرستی مگر رحمتی در پیم</p></div>
<div class="m2"><p>که بر کردهٔ خویش واثق نیم</p></div></div>