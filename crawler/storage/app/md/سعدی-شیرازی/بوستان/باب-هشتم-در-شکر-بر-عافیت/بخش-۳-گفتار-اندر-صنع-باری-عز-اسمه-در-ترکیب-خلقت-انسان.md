---
title: >-
    بخش ۳ - گفتار اندر صنع باری عز اسمه در ترکیب خلقت انسان
---
# بخش ۳ - گفتار اندر صنع باری عز اسمه در ترکیب خلقت انسان

<div class="b" id="bn1"><div class="m1"><p>ببین تا یک انگشت از چند بند</p></div>
<div class="m2"><p>به صنع الهی به هم در فگند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پس آشفتگی باشد و ابلهی</p></div>
<div class="m2"><p>که انگشت بر حرف صنعش نهی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تأمل کن از بهر رفتار مرد</p></div>
<div class="m2"><p>که چند استخوان پی زد و وصل کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>که بی گردش کعب و زانو و پای</p></div>
<div class="m2"><p>نشاید قدم بر گرفتن ز جای</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از آن سجده بر آدمی سخت نیست</p></div>
<div class="m2"><p>که در صلب او مهره یک لخت نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دو صد مهره بر یکدگر ساخته‌ست</p></div>
<div class="m2"><p>که گل مهره‌ای چون تو پرداخته‌ست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>رگت بر تن است ای پسندیده خوی</p></div>
<div class="m2"><p>زمینی در او سیصد و شصت جوی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بصر در سر و فکر و رای و تمیز</p></div>
<div class="m2"><p>جوارح به دل، دل به دانش عزیز</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بهایم به روی اندر افتاده خوار</p></div>
<div class="m2"><p>تو همچون الف بر قدمها سوار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نگون کرده ایشان سر از بهر خور</p></div>
<div class="m2"><p>تو آری به عزت خورش پیش سر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نزیبد تو را با چنین سروری</p></div>
<div class="m2"><p>که سر جز به طاعت فرود آوری</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به انعام خود دانه دادت نه کاه</p></div>
<div class="m2"><p>نکردت چو انعام سر در گیاه</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ولیکن بدین صورت دلپذیر</p></div>
<div class="m2"><p>فرفته مشو، سیرت خوب گیر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ره راست باید نه بالای راست</p></div>
<div class="m2"><p>که کافر هم از روی صورت چو ماست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>تو را آن که چشم و دهان داد و گوش</p></div>
<div class="m2"><p>اگر عاقلی در خلافش مکوش</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گرفتم که دشمن بکوبی به سنگ</p></div>
<div class="m2"><p>مکن باری از جهل با دوست جنگ</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>خردمند طبعان منت شناس</p></div>
<div class="m2"><p>بدوزند نعمت به میخ سپاس</p></div></div>