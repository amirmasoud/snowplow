---
title: >-
    بخش ۱۰ - حکایت
---
# بخش ۱۰ - حکایت

<div class="b" id="bn1"><div class="m1"><p>یکی کرد بر پارسایی گذر</p></div>
<div class="m2"><p>به صورت جهود آمدش در نظر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قفایی فرو کوفت بر گردنش</p></div>
<div class="m2"><p>ببخشید درویش پیراهنش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خجل گفت کانچ از من آمد خطاست</p></div>
<div class="m2"><p>ببخشای بر من، چه جای عطاست؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به شکرانه گفتا به سر بیستم</p></div>
<div class="m2"><p>که آنم که پنداشتی نیستم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نکو سیرت بی تکلف برون</p></div>
<div class="m2"><p>به از نیکنام خراب اندرون</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به نزدیک من شبرو راهزن</p></div>
<div class="m2"><p>به از فاسق پارسا پیرهن</p></div></div>