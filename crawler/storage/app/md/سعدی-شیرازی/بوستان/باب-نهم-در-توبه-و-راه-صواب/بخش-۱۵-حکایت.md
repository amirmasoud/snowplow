---
title: >-
    بخش ۱۵ - حکایت
---
# بخش ۱۵ - حکایت

<div class="b" id="bn1"><div class="m1"><p>همی یادم آید ز عهد صغر</p></div>
<div class="m2"><p>که عیدی برون آمدم با پدر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به بازیچه مشغول مردم شدم</p></div>
<div class="m2"><p>در آشوب خلق از پدر گم شدم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>برآوردم از هول و دهشت خروش</p></div>
<div class="m2"><p>پدر ناگهانم بمالید گوش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>که ای شوخ چشم آخرت چند بار</p></div>
<div class="m2"><p>بگفتم که دستم ز دامن مدار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به تنها نداند شدن طفل خرد</p></div>
<div class="m2"><p>که مشکل توان راه نادیده برد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تو هم طفل راهی به سعی ای فقیر</p></div>
<div class="m2"><p>برو دامن راه دانان بگیر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مکن با فرومایه مردم نشست</p></div>
<div class="m2"><p>چو کردی، ز هیبت فرو شوی دست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به فتراک پاکان درآویز چنگ</p></div>
<div class="m2"><p>که عارف ندارد ز دریوزه ننگ</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مریدان به قوت ز طفلان کمند</p></div>
<div class="m2"><p>مشایخ چو دیوار مستحکمند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بیاموز رفتار از آن طفل خرد</p></div>
<div class="m2"><p>که چون استعانت به دیوار برد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز زنجیر ناپارسایان برست</p></div>
<div class="m2"><p>که در حلقهٔ پارسایان نشست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>اگر حاجتی داری این حلقه گیر</p></div>
<div class="m2"><p>که سلطان ندارد از این در گزیر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>برو خوشه چین باش سعدی صفت</p></div>
<div class="m2"><p>که گرد آوری خرمن معرفت</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>الا ای مقیمان محراب انس</p></div>
<div class="m2"><p>که فردا نشینید بر خوان قدس</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>متابید روی از گدایان خیل</p></div>
<div class="m2"><p>که صاحب مروت نراند طفیل</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>کنون با خرد باید انباز گشت</p></div>
<div class="m2"><p>که فردا نماند ره بازگشت</p></div></div>