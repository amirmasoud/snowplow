---
title: >-
    بخش ۱۲ - حکایت در عالم طفولیت
---
# بخش ۱۲ - حکایت در عالم طفولیت

<div class="b" id="bn1"><div class="m1"><p>ز عهد پدر یادم آید همی</p></div>
<div class="m2"><p>که باران رحمت بر او هر دمی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که در طفلیم لوح و دفتر خرید</p></div>
<div class="m2"><p>ز بهرم یکی خاتم زر خرید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به در کرد ناگه یکی مشتری</p></div>
<div class="m2"><p>به خرمایی از دستم انگشتری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو نشناسد انگشتری طفل خرد</p></div>
<div class="m2"><p>به شیرینی از وی توانند برد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تو هم قیمت عمر نشناختی</p></div>
<div class="m2"><p>که در عیش شیرین برانداختی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>قیامت که نیکان بر اعلا رسند</p></div>
<div class="m2"><p>ز قعر ثری بر ثریا رسند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تو را خود بماند سر از ننگ پیش</p></div>
<div class="m2"><p>که گردت برآید عملهای خویش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>برادر، ز کار بدان شرم دار</p></div>
<div class="m2"><p>که در روی نیکان شوی شرمسار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در آن روز کز فعل پرسند و قول</p></div>
<div class="m2"><p>اولوالعزم را تن بلرزد ز هول</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به جایی که دهشت خورند انبیا</p></div>
<div class="m2"><p>تو عذر گنه را چه داری؟ بیا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>زنانی که طاعت به رغبت برند</p></div>
<div class="m2"><p>ز مردان ناپارسا بگذرند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تو را شرم ناید ز مردی خویش</p></div>
<div class="m2"><p>که باشد زنان را قبول از تو بیش؟</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>زنان را به عذری معین که هست</p></div>
<div class="m2"><p>ز طاعت بدارند گه گاه دست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>تو بی عذر یک سو نشینی چو زن</p></div>
<div class="m2"><p>رو ای کم ز زن، لاف مردی مزن</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>مرا خود چه باشد زبان آوری</p></div>
<div class="m2"><p>چنین گفت شاه سخن عنصری:</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>«چو از راستی بگذری خم بود</p></div>
<div class="m2"><p>چه مردی بود کز زنی کم بود؟»</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>به ناز و طرب نفس پرورده گیر</p></div>
<div class="m2"><p>به ایام دشمن قوی کرده گیر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>یکی بچهٔ گرگ می‌پرورید</p></div>
<div class="m2"><p>چو پرورده شد خواجه بر هم درید</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چو بر پهلوی جان سپردن بخفت</p></div>
<div class="m2"><p>زبان آوری در سرش رفت و گفت</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>تو دشمن چنین نازنین پروری</p></div>
<div class="m2"><p>ندانی که ناچار زخمش خوری؟</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>نه ابلیس در حق ما طعنه زد</p></div>
<div class="m2"><p>کز اینان نیاید به جز کار بد؟</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>فغان از بدیها که در نفس ماست</p></div>
<div class="m2"><p>که ترسم شود ظن ابلیس راست</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>چو ملعون پسند آمدش قهر ما</p></div>
<div class="m2"><p>خدایش بینداخت از بهر ما</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>کجا سر برآریم از این عار و ننگ</p></div>
<div class="m2"><p>که با او به صلحیم و با حق به جنگ</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>نظر دوست نادر کند سوی تو</p></div>
<div class="m2"><p>چو در روی دشمن بود روی تو</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>گرت دوست باید کز او بر خوری</p></div>
<div class="m2"><p>نباید که فرمان دشمن بری</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>روا دارد از دوست بیگانگی</p></div>
<div class="m2"><p>که دشمن گزیند به همخانگی</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>ندانی که کمتر نهد دوست پای</p></div>
<div class="m2"><p>چو بیند که دشمن بود در سرای؟</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>به سیم سیه تا چه خواهی خرید</p></div>
<div class="m2"><p>که خواهی دل از مهر یوسف برید؟</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>تو از دوست گر عاقلی بر مگرد</p></div>
<div class="m2"><p>که دشمن نیارد نگه در تو کرد</p></div></div>