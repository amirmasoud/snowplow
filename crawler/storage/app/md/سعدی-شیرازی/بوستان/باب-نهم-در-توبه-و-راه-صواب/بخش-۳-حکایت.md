---
title: >-
    بخش ۳ - حکایت
---
# بخش ۳ - حکایت

<div class="b" id="bn1"><div class="m1"><p>کهنسالی آمد به نزد طبیب</p></div>
<div class="m2"><p>ز نالیدنش تا به مردن قریب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که دستم به رگ بر نه، ای نیک رای</p></div>
<div class="m2"><p>که پایم همی بر نیاید ز جای</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بدین ماند این قامت خفته‌ام</p></div>
<div class="m2"><p>که گویی به گل در فرو رفته‌ام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>برو، گفت دست از جهان در گسل</p></div>
<div class="m2"><p>که پایت قیامت برآید ز گل</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نشاط جوانی ز پیران مجوی</p></div>
<div class="m2"><p>که آب روان باز ناید به جوی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اگر در جوانی زدی دست و پای</p></div>
<div class="m2"><p>در ایام پیری به هش باش و رای</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو دوران عمر از چهل در گذشت</p></div>
<div class="m2"><p>مزن دست و پا کآبت از سر گذشت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نشاط از من آن گه رمیدن گرفت</p></div>
<div class="m2"><p>که شامم سپیده دمیدن گرفت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بباید هوس کردن از سر به در</p></div>
<div class="m2"><p>که دور هوسبازی آمد به سر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به سبزه کجا تازه گردد دلم</p></div>
<div class="m2"><p>که سبزه بخواهد دمید از گلم؟</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تفرج کنان در هوا و هوس</p></div>
<div class="m2"><p>گذشتیم بر خاک بسیار کس</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>کسانی که دیگر به غیب اندرند</p></div>
<div class="m2"><p>بیایند و بر خاک ما بگذرند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>دریغا که فصل جوانی برفت</p></div>
<div class="m2"><p>به لهو و لعب زندگانی برفت</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>دریغا چنان روح پرور زمان</p></div>
<div class="m2"><p>که بگذشت بر ما چو برق یمان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ز سودای آن پوشم و این خورم</p></div>
<div class="m2"><p>نپرداختم تا غم دین خورم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>دریغا که مشغول باطل شدیم</p></div>
<div class="m2"><p>ز حق دور ماندیم و غافل شدیم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چه خوش گفت با کودک آموزگار</p></div>
<div class="m2"><p>که کاری نکردیم و شد روزگار</p></div></div>