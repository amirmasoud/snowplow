---
title: >-
    بخش ۱۰ - حکایت
---
# بخش ۱۰ - حکایت

<div class="b" id="bn1"><div class="m1"><p>شبی خفته بودم به عزم سفر</p></div>
<div class="m2"><p>پی کاروانی گرفتم سحر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که آمد یکی سهمگین باد و گرد</p></div>
<div class="m2"><p>که بر چشم مردم جهان تیره کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به ره در یکی دختر خانه بود</p></div>
<div class="m2"><p>به معجر غبار از پدر می‌زدود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پدر گفتش ای نازنین چهر من</p></div>
<div class="m2"><p>که داری دل آشفتهٔ مهر من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نه چندان نشیند در این دیده خاک</p></div>
<div class="m2"><p>که بازش به معجر توان کرد پاک</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر این خاک چندان صبا بگذرد</p></div>
<div class="m2"><p>که هر ذره از ما به جایی برد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تو را نفس رعنا چو سرکش ستور</p></div>
<div class="m2"><p>دوان می‌برد تا سر شیب گور</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اجل ناگهت بگسلاند رکیب</p></div>
<div class="m2"><p>عنان باز نتوان گرفت از نشیب</p></div></div>