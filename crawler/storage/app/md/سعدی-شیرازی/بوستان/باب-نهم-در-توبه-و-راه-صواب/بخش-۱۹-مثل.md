---
title: >-
    بخش ۱۹ - مثل
---
# بخش ۱۹ - مثل

<div class="b" id="bn1"><div class="m1"><p>پلیدی کند گربه بر جای پاک</p></div>
<div class="m2"><p>چو زشتش نماید بپوشد به خاک</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو آزادی از ناپسندیده‌ها</p></div>
<div class="m2"><p>نترسی که بر وی فتد دیده‌ها</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>براندیش از آن بندهٔ پر گناه</p></div>
<div class="m2"><p>که از خواجه مخفی شود چند گاه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر بر نگردد به صدق و نیاز</p></div>
<div class="m2"><p>به زنجیر و بندش بیارند باز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به کین آوری با کسی بر ستیز</p></div>
<div class="m2"><p>که از وی گزیرت بود یا گریز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کنون کرد باید عمل را حساب</p></div>
<div class="m2"><p>نه وقتی که منشور گردد کتاب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کسی گر چه بد کرد هم بد نکرد</p></div>
<div class="m2"><p>که پیش از قیامت غم خود بخورد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر آیینه از آه گردد سیاه</p></div>
<div class="m2"><p>شود روشن آیینهٔ دل به آه</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بترس از گناهان خویش این نفس</p></div>
<div class="m2"><p>که روز قیامت نترسی ز کس</p></div></div>