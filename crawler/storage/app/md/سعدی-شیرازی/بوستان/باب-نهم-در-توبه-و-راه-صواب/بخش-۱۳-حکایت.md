---
title: >-
    بخش ۱۳ - حکایت
---
# بخش ۱۳ - حکایت

<div class="b" id="bn1"><div class="m1"><p>یکی برد با پادشاهی ستیز</p></div>
<div class="m2"><p>به دشمن سپردش که خونش بریز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گرفتار در دست آن کینه توز</p></div>
<div class="m2"><p>همی گفت هر دم به زاری و سوز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگر دوست بر خود نیازردمی</p></div>
<div class="m2"><p>کی از دست دشمن جفا بردمی؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بتا جور دشمن بدردش پوست</p></div>
<div class="m2"><p>رفیقی که بر خود بیازرد دوست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تو از دوست گر عاقلی بر مگرد</p></div>
<div class="m2"><p>که دشمن نیارد نگه در تو کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تو با دوست یکدل شو و یک سخن</p></div>
<div class="m2"><p>که خود بیخ دشمن برآید ز بن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نپندارم این زشت نامی نکوست</p></div>
<div class="m2"><p>به خشنودی دشمن آزار دوست</p></div></div>