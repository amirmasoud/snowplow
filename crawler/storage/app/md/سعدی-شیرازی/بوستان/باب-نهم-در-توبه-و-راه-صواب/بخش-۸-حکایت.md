---
title: >-
    بخش ۸ - حکایت
---
# بخش ۸ - حکایت

<div class="b" id="bn1"><div class="m1"><p>یکی پارسا سیرت حق پرست</p></div>
<div class="m2"><p>فتادش یکی خشت زرین به دست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سر هوشمندش چنان خیره کرد</p></div>
<div class="m2"><p>که سودا دل روشنش تیره کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همه شب در اندیشه کاین گنج و مال</p></div>
<div class="m2"><p>در او تا زیم ره نیابد زوال</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دگر قامت عجزم از بهر خواست</p></div>
<div class="m2"><p>نباید بر کس دوتا کرد و راست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سرایی کنم پای بستش رخام</p></div>
<div class="m2"><p>درختان سقفش همه عود خام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یکی حجره خاص از پی دوستان</p></div>
<div class="m2"><p>در حجره اندر سرا بوستان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بفرسودم از رقعه بر رقعه دوخت</p></div>
<div class="m2"><p>تف دیگدان چشم و مغزم بسوخت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دگر زیردستان پزندم خورش</p></div>
<div class="m2"><p>به راحت دهم روح را پرورش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به سختی بکشت این نمد بسترم</p></div>
<div class="m2"><p>روم زین سپس عبقری گسترم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خیالش خرف کرد و کالیوه رنگ</p></div>
<div class="m2"><p>به مغزش فرو برده خرچنگ چنگ</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>فراغ مناجات و رازش نماند</p></div>
<div class="m2"><p>خور و خواب و ذکر و نمازش نماند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به صحرا برآمد سر از عشوه مست</p></div>
<div class="m2"><p>که جایی نبودش قرار نشست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>یکی بر سر گور گل می سرشت</p></div>
<div class="m2"><p>که حاصل کند زآن گل گور خشت</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>به اندیشه لختی فرو رفت پیر</p></div>
<div class="m2"><p>که ای نفس کوته نظر پند گیر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چه بندی در این خشت زرین دلت</p></div>
<div class="m2"><p>که یک روز خشتی کنند از گلت؟</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>طمع را نه چندان دهان است باز</p></div>
<div class="m2"><p>که بازش نشیند به یک لقمه آز</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بدار ای فرومایه زین خشت دست</p></div>
<div class="m2"><p>که جیحون نشاید به یک خشت بست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>تو غافل در اندیشهٔ سود و مال</p></div>
<div class="m2"><p>که سرمایهٔ عمر شد پایمال</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>غبار هوا چشم عقلت بدوخت</p></div>
<div class="m2"><p>سموم هوس کشت عمرت بسوخت</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بکن سرمهٔ غفلت از چشم پاک</p></div>
<div class="m2"><p>که فردا شوی سرمه در چشم خاک</p></div></div>