---
title: >-
    بخش ۲۰ - حکایت سفر حبشه
---
# بخش ۲۰ - حکایت سفر حبشه

<div class="b" id="bn1"><div class="m1"><p>غریب آمدم در سواد حبش</p></div>
<div class="m2"><p>دل از دهر فارغ سر از عیش خوش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به ره بر یکی دکه دیدم بلند</p></div>
<div class="m2"><p>تنی چند مسکین بر او پای بند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بسیج سفر کردم اندر نفس</p></div>
<div class="m2"><p>بیابان گرفتم چو مرغ از قفس</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یکی گفت کاین بندیان شبروند</p></div>
<div class="m2"><p>نصیحت نگیرند و حق نشنوند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو بر کس نیامد ز دستت ستم</p></div>
<div class="m2"><p>تو را گر جهان شحنه گیرد چه غم؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نیاورده عامل غش اندر میان</p></div>
<div class="m2"><p>نیندیشد از رفع دیوانیان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>وگر عفتت را فریب است زیر</p></div>
<div class="m2"><p>زبان حسابت نگردد دلیر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نکونام را کس نگیرد اسیر</p></div>
<div class="m2"><p>بترس از خدای و مترس از امیر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو خدمت پسندیده آرم به جای</p></div>
<div class="m2"><p>نیندیشم از دشمن تیره رای</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>اگر بنده کوشش کند بنده‌وار</p></div>
<div class="m2"><p>عزیزش بدارد خداوندگار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>وگر کند رای است در بندگی</p></div>
<div class="m2"><p>ز جانداری افتد به خربندگی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>قدم پیش نه کز ملک بگذری</p></div>
<div class="m2"><p>که گر باز مانی ز دد کمتری</p></div></div>