---
title: >-
    بخش ۷ - حکایت در معنی بیداری از خواب غفلت
---
# بخش ۷ - حکایت در معنی بیداری از خواب غفلت

<div class="b" id="bn1"><div class="m1"><p>فرو رفت جم را یکی نازنین</p></div>
<div class="m2"><p>کفن کرد چون کرمش ابریشمین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به دخمه برآمد پس از چند روز</p></div>
<div class="m2"><p>که بر وی بگرید به زاری و سوز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو پوسیده دیدش حریرین کفن</p></div>
<div class="m2"><p>به فکرت چنین گفت با خویشتن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>من از کرم برکنده بودم به زور</p></div>
<div class="m2"><p>بکندند از او باز کرمان گور</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در این باغ سروی نیامد بلند</p></div>
<div class="m2"><p>که باد اجل بیخش از بن نکند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>قضا نقش یوسف جمالی نکرد</p></div>
<div class="m2"><p>که ماهی گورش چو یونس نخورد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دو بیتم جگر کرد روزی کباب</p></div>
<div class="m2"><p>که می‌گفت گوینده‌ای با رباب:</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دریغا که بی ما بسی روزگار</p></div>
<div class="m2"><p>بروید گل و بشکفد نوبهار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بسی تیر و دی ماه و اردیبهشت</p></div>
<div class="m2"><p>برآید که ما خاک باشیم و خشت</p></div></div>