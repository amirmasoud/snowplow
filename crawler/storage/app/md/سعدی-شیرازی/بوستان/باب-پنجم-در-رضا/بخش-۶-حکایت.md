---
title: >-
    بخش ۶ - حکایت
---
# بخش ۶ - حکایت

<div class="b" id="bn1"><div class="m1"><p>شنیدم که دیناری از مفلسی</p></div>
<div class="m2"><p>بیفتاد و مسکین بجستش بسی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به آخر سر ناامیدی بتافت</p></div>
<div class="m2"><p>یکی دیگرش ناطلب کرده یافت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به بدبختی و نیکبختی قلم</p></div>
<div class="m2"><p>بگردید و ما همچنان در شکم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نه روزی به سرپنجگی می‌خورند</p></div>
<div class="m2"><p>که سرپنجگان تنگ روزی ترند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بسا چاره‌دانا به سختی بمرد</p></div>
<div class="m2"><p>که بیچاره گوی سلامت ببرد</p></div></div>