---
title: >-
    بخش ۸ - حکایت مرد درویش و همسایهٔ توانگر
---
# بخش ۸ - حکایت مرد درویش و همسایهٔ توانگر

<div class="b" id="bn1"><div class="m1"><p>بلند اختری نام او بختیار</p></div>
<div class="m2"><p>قوی دستگه بود و سرمایه‌دار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به کوی گدایان درش خانه بود</p></div>
<div class="m2"><p>زرش همچو گندم به پیمانه بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو درویش بیند توانگر به ناز</p></div>
<div class="m2"><p>دلش بیش سوزد به داغ نیاز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زنی جنگ پیوست با شوی خویش</p></div>
<div class="m2"><p>شبانگه چو رفتش تهیدست، پیش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>که کس چون تو بدبخت، درویش نیست</p></div>
<div class="m2"><p>چو زنبور سرخت جز این نیش نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بیاموز مردی ز همسایگان</p></div>
<div class="m2"><p>که آخر نیم قحبهٔ رایگان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کسان را زر و سیم و ملک است و رخت</p></div>
<div class="m2"><p>چرا همچو ایشان نه‌ای نیکبخت؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بر آورد صافی دل صوف پوش</p></div>
<div class="m2"><p>چو طبل از تهیگاه خالی خروش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>که من دست قدرت ندارم به هیچ</p></div>
<div class="m2"><p>به سرپنجه دست قضا بر مپیچ</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نکردند در دست من اختیار</p></div>
<div class="m2"><p>که من خویشتن را کنم بختیار</p></div></div>