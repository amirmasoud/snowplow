---
title: >-
    بخش ۱۳ - گفتار اندر اخلاص و برکت آن و ریا و آفت آن
---
# بخش ۱۳ - گفتار اندر اخلاص و برکت آن و ریا و آفت آن

<div class="b" id="bn1"><div class="m1"><p>عبادت به اخلاص نیت نکوست</p></div>
<div class="m2"><p>وگر نه چه آید ز بی مغز پوست؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چه زنار مغ در میانت چه دلق</p></div>
<div class="m2"><p>که در پوشی از بهر پندار خلق</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مکن گفتمت مردی خویش فاش</p></div>
<div class="m2"><p>چو مردی نمودی مخنث مباش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به اندازهٔ بود باید نمود</p></div>
<div class="m2"><p>خجالت نبرد آن که ننمود و بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>که چون عاریت بر کنند از سرش</p></div>
<div class="m2"><p>نماید کهن جامه‌ای در برش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اگر کوتهی پای چوبین مبند</p></div>
<div class="m2"><p>که در چشم طفلان نمایی بلند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>وگر نقره اندوده باشد نحاس</p></div>
<div class="m2"><p>توان خرج کردن بر ناشناس</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>منه جان من آب زر بر پشیز</p></div>
<div class="m2"><p>که صراف دانا نگیرد به چیز</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زر اندودگان را به آتش برند</p></div>
<div class="m2"><p>پدید آید آنگه که مس یا زرند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ندانی که بابای کوهی چه گفت</p></div>
<div class="m2"><p>به مردی که ناموس را شب نخفت؟</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>برو جان بابا در اخلاص پیچ</p></div>
<div class="m2"><p>که نتوانی از خلق رستن به هیچ</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>کسانی که فعلت پسندیده‌اند</p></div>
<div class="m2"><p>هنوز از تو نقش برون دیده‌اند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چه قدر آورد بنده حوردیس</p></div>
<div class="m2"><p>که زیر قبا دارد اندام پیس؟</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نشاید به دستان شدن در بهشت</p></div>
<div class="m2"><p>که بازت رود چادر از روی زشت</p></div></div>