---
title: >-
    بخش ۱۵ - حکایت
---
# بخش ۱۵ - حکایت

<div class="b" id="bn1"><div class="m1"><p>سیهکاری از نردبانی فتاد</p></div>
<div class="m2"><p>شنیدم که هم در نفس جان بداد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پسر چند روزی گرستن گرفت</p></div>
<div class="m2"><p>دگر با حریفان نشستن گرفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به خواب اندرش دید و پرسید حال</p></div>
<div class="m2"><p>که چون رستی از حشر و نشر و سؤال؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بگفت ای پسر قصه بر من مخوان</p></div>
<div class="m2"><p>به دوزخ در افتادم از نردبان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نکو سیرتی بی تکلف برون</p></div>
<div class="m2"><p>به از نیکنامی خراب اندرون</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به نزدیک من شبرو راهزن</p></div>
<div class="m2"><p>به از فاسق پارسا پیرهن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یکی بر در خلق رنج آزمای</p></div>
<div class="m2"><p>چه مزدش دهد در قیامت خدای؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز عمرو ای پسر چشم اجرت مدار</p></div>
<div class="m2"><p>چو در خانهٔ زید باشی به کار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نگویم تواند رسیدن به دوست</p></div>
<div class="m2"><p>در این ره جز آن کس که رویش در اوست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ره راست رو تا به منزل رسی</p></div>
<div class="m2"><p>تو در ره نه‌ای، زین قبل واپسی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چو گاوی که عصار چشمش ببست</p></div>
<div class="m2"><p>دوان تا به شب، شب همان جا که هست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>کسی گر بتابد ز محراب روی</p></div>
<div class="m2"><p>به کفرش گواهی دهند اهل کوی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>تو هم پشت بر قبله‌ای در نماز</p></div>
<div class="m2"><p>گرت در خدا نیست روی نیاز</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>درختی که بیخش بود برقرار</p></div>
<div class="m2"><p>بپرور، که روزی دهد میوه بار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گرت بیخ اخلاص در بوم نیست</p></div>
<div class="m2"><p>از این بر کسی چون تو محروم نیست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>هر آن کافکند تخم بر روی سنگ</p></div>
<div class="m2"><p>جوی وقت دخلش نیاید به چنگ</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>منه آبروی ریا را محل</p></div>
<div class="m2"><p>که این آب در زیر دارد وحل</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چو در خفیه بد باشم و خاکسار</p></div>
<div class="m2"><p>چه سود آب ناموس بر روی کار؟</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>به روی و ریا خرقه سهل است دوخت</p></div>
<div class="m2"><p>گرش با خدا در توانی فروخت</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چه دانند مردم که در جامه کیست؟</p></div>
<div class="m2"><p>نویسنده داند که در نامه چیست</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چه وزن آورد جایی انبان باد</p></div>
<div class="m2"><p>که میزان عدل است و دیوان داد؟</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>مرائی که چندین ورع می‌نمود</p></div>
<div class="m2"><p>بدیدند و هیچش در انبان نبود</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>کنند ابره پاکیزه‌تر ز آستر</p></div>
<div class="m2"><p>که آن در حجاب است و این در نظر</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بزرگان فراغ از نظر داشتند</p></div>
<div class="m2"><p>از آن پرنیان آستر داشتند</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ور آوازه خواهی در اقلیم فاش</p></div>
<div class="m2"><p>برون حله کن گو درون حشو باش</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>به بازی نگفت این سخن بایزید</p></div>
<div class="m2"><p>که از منکر ایمن‌ترم کز مرید</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>کسانی که سلطان و شاهنشهند</p></div>
<div class="m2"><p>سراسر گدایان این درگهند</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>طمع در گدا، مرد معنی نبست</p></div>
<div class="m2"><p>نشاید گرفتن در افتاده دست</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>همان به گر آبستن گوهری</p></div>
<div class="m2"><p>که همچون صدف سر به خود در بری</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>چو روی پرستیدنت در خداست</p></div>
<div class="m2"><p>اگر جبرئیلت نبیند رواست</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>تو را پند سعدی بس است ای پسر</p></div>
<div class="m2"><p>اگر گوش گیری چو پند پدر</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>گر امروز گفتار ما نشنوی</p></div>
<div class="m2"><p>مبادا که فردا پشیمان شوی</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>از این به نصیحتگری بایدت</p></div>
<div class="m2"><p>ندانم پس از من چه پیش آیدت!</p></div></div>