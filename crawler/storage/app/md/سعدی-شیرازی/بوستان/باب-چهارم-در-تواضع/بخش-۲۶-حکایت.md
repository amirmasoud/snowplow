---
title: >-
    بخش ۲۶ - حکایت
---
# بخش ۲۶ - حکایت

<div class="b" id="bn1"><div class="m1"><p>گدایی شنیدم که در تنگ جای</p></div>
<div class="m2"><p>نهادش عمر پای بر پشت پای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ندانست درویش بیچاره کاوست</p></div>
<div class="m2"><p>که رنجیده دشمن نداند ز دوست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>برآشفت بر وی که کوری مگر؟</p></div>
<div class="m2"><p>بدو گفت سالار عادل عمر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نه کورم ولیکن خطا رفت کار</p></div>
<div class="m2"><p>ندانستم از من گنه در گذار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چه منصف بزرگان دین بوده‌اند</p></div>
<div class="m2"><p>که با زیردستان چنین بوده‌اند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>فروتن بود هوشمند گزین</p></div>
<div class="m2"><p>نهد شاخ پر میوه سر بر زمین</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بنازند فردا تواضع کنان</p></div>
<div class="m2"><p>نگون از خجالت سر گردنان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اگر می‌بترسی ز روز شمار</p></div>
<div class="m2"><p>از آن کز تو ترسد خطا در گذار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مکن خیره بر زیر دستان ستم</p></div>
<div class="m2"><p>که دستی است بالای دست تو هم</p></div></div>