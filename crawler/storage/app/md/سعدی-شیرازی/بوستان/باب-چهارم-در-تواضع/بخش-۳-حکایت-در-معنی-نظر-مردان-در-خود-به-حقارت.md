---
title: >-
    بخش ۳ - حکایت در معنی نظر مردان در خود به حقارت
---
# بخش ۳ - حکایت در معنی نظر مردان در خود به حقارت

<div class="b" id="bn1"><div class="m1"><p>جوانی خردمند پاکیزه بوم</p></div>
<div class="m2"><p>ز دریا بر آمد به دربند روم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در او فضل دیدند و فقر و تمیز</p></div>
<div class="m2"><p>نهادند رختش به جایی عزیز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سر صالحان گفت روزی به مرد</p></div>
<div class="m2"><p>که خاشاک مسجد بیفشان و گرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همان کاین سخن مرد رهرو شنید</p></div>
<div class="m2"><p>برون رفت و بازش کس آنجا ندید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر آن حمل کردند یاران و پیر</p></div>
<div class="m2"><p>که پروای خدمت نبودش فقیر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دگر روز خادم گرفتش به راه</p></div>
<div class="m2"><p>که ناخوب کردی به رأی تباه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ندانستی ای کودک خودپسند</p></div>
<div class="m2"><p>که مردان ز خدمت به جایی رسند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گرستن گرفت از سر صدق و سوز</p></div>
<div class="m2"><p>که ای یار جان پرور دلفروز</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نه گرد اندر آن بقعه دیدم نه خاک</p></div>
<div class="m2"><p>من آلوده بودم در آن جای پاک</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گرفتم قدم لاجرم باز پس</p></div>
<div class="m2"><p>که پاکیزه به مسجد از خاک و خس</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>طریقت جز این نیست درویش را</p></div>
<div class="m2"><p>که افکنده دارد تن خویش را</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بلندیت باید تواضع گزین</p></div>
<div class="m2"><p>که آن بام را نیست سلم جز این</p></div></div>