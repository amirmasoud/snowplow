---
title: >-
    بخش ۷ - حکایت توبه کردن ملک زادهٔ گنجه
---
# بخش ۷ - حکایت توبه کردن ملک زادهٔ گنجه

<div class="b" id="bn1"><div class="m1"><p>یکی پادشه‌زاده در گنجه بود</p></div>
<div class="m2"><p>که دور از تو ناپاک و سرپنجه بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به مسجد در آمد سرایان و مست</p></div>
<div class="m2"><p>می اندر سر و ساتکینی به دست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به مقصوره در پارسایی مقیم</p></div>
<div class="m2"><p>زبانی دلاویز و قلبی سلیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تنی چند بر گفت او مجتمع</p></div>
<div class="m2"><p>چو عالم نباشی کم از مستمع</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو بی عزتی پیشه کرد آن حرون</p></div>
<div class="m2"><p>شدند آن عزیزان خراب اندرون</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو منکر بود پادشه را قدم</p></div>
<div class="m2"><p>که یارد زد از امر معروف دم؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تحکم کند سیر بر بوی گل</p></div>
<div class="m2"><p>فرو ماند آواز چنگ از دهل</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گرت نهی منکر بر آید ز دست</p></div>
<div class="m2"><p>نشاید چو بی دست و پایان نشست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>وگر دست قدرت نداری، بگوی</p></div>
<div class="m2"><p>که پاکیزه گردد به اندرز خوی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چو دست و زبان را نماند مجال</p></div>
<div class="m2"><p>به همت نمایند مردی رجال</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>یکی پیش دانای خلوت نشین</p></div>
<div class="m2"><p>بنالید و بگریست سر بر زمین</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>که باری بر این رند ناپاک و مست</p></div>
<div class="m2"><p>دعا کن که ما بی زبانیم و دست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>دمی سوزناک از دلی با خبر</p></div>
<div class="m2"><p>قوی تر که هفتاد تیغ و تبر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بر آورد مرد جهاندیده دست</p></div>
<div class="m2"><p>چه گفت ای خداوند بالا و پست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>خوش است این پسر وقتش از روزگار</p></div>
<div class="m2"><p>خدایا همه وقت او خوش بدار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>کسی گفتش ای قدوهٔ راستی</p></div>
<div class="m2"><p>بر این بد چرا نیکویی خواستی؟</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چو بد عهد را نیک خواهی ز بهر</p></div>
<div class="m2"><p>چه بد خواستی بر سر خلق شهر؟</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چنین گفت بینندهٔ تیز هوش</p></div>
<div class="m2"><p>چو سر سخن در نیابی مجوش</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>به طامات مجلس نیاراستم</p></div>
<div class="m2"><p>ز داد آفرین توبه‌اش خواستم</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>که هر گه که باز آید از خوی زشت</p></div>
<div class="m2"><p>به عیشی رسد جاودان در بهشت</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>همین پنج روز است عیش مدام</p></div>
<div class="m2"><p>به ترک اندرش عیشهای مدام</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>حدیثی که مرد سخن ساز گفت</p></div>
<div class="m2"><p>کسی ز آن میان با ملک باز گفت</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ز وجد آب در چشمش آمد چو میغ</p></div>
<div class="m2"><p>ببارید بر چهره سیل دریغ</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>به نیران شوق اندرونش بسوخت</p></div>
<div class="m2"><p>حیا دیده بر پشت پایش بدوخت</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بر نیک محضر فرستاد کس</p></div>
<div class="m2"><p>در توبه کوبان که فریاد رس</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>قدم رنجه فرمای تا سر نهم</p></div>
<div class="m2"><p>سر جهل و ناراستی بر نهم</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>دو رویه ستادند بر در سپاه</p></div>
<div class="m2"><p>سخن پرور آمد در ایوان شاه</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>شکر دید و عناب و شمع و شراب</p></div>
<div class="m2"><p>ده از نعمت آباد و مردم خراب</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>یکی غایب از خود، یکی نیم مست</p></div>
<div class="m2"><p>یکی شعر گویان صراحی به دست</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>ز سویی بر آورده مطرب خروش</p></div>
<div class="m2"><p>ز دیگر سو آواز ساقی که نوش</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>حریفان خراب از می لعل رنگ</p></div>
<div class="m2"><p>سر چنگی از خواب در بر چو چنگ</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>نبود از ندیمان گردن فراز</p></div>
<div class="m2"><p>به جز نرگس آن جا کسی دیده باز</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>دف و چنگ با یکدگر سازگار</p></div>
<div class="m2"><p>بر آورده زیر از میان ناله زار</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>بفرمود و در هم شکستند خرد</p></div>
<div class="m2"><p>مبدل شد آن عیش صافی به درد</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>شکستند چنگ و گسستند رود</p></div>
<div class="m2"><p>به در کرد گوینده از سر سرود</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>به میخانه در سنگ بر دن زدند</p></div>
<div class="m2"><p>کدو را نشاندند و گردن زدند</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>می لاله گون از بط سرنگون</p></div>
<div class="m2"><p>روان همچنان کز بط کشته خون</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>خم آبستن خمر نه ماهه بود</p></div>
<div class="m2"><p>در آن فتنه دختر بینداخت زود</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>شکم تا به نافش دریدند مشک</p></div>
<div class="m2"><p>قدح را بر او چشم خونی پر اشک</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>بفرمود تا سنگ صحن سرای</p></div>
<div class="m2"><p>بکندند و کردند نو باز جای</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>که گلگونه خمر یاقوت فام</p></div>
<div class="m2"><p>به شستن نمی‌شد ز روی رخام</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>عجب نیست بالوعه گر شد خراب</p></div>
<div class="m2"><p>که خورد اندر آن روز چندان شراب</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>دگر هر که بربط گرفتی به کف</p></div>
<div class="m2"><p>قفا خوردی از دست مردم چو دف</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>وگر فاسقی چنگ بردی به دوش</p></div>
<div class="m2"><p>بمالیدی او را چو طنبور گوش</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>جوان سر از کبر و پندار مست</p></div>
<div class="m2"><p>چو پیران به کنج عبادت نشست</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>پدر بارها گفته بودش به هول</p></div>
<div class="m2"><p>که شایسته رو باش و پاکیزه قول</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>جفای پدر برد و زندان و بند</p></div>
<div class="m2"><p>چنان سودمندش نیامد که پند</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>گرش سخت گفتی سخنگوی سهل</p></div>
<div class="m2"><p>که بیرون کن از سر جوانی و جهل</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>خیال و غرورش بر آن داشتی</p></div>
<div class="m2"><p>که درویش را زنده نگذاشتی</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>سپر نفکند شیر غران ز جنگ</p></div>
<div class="m2"><p>نیندیشد از تیغ بران پلنگ</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>به نرمی ز دشمن توان کرد دوست</p></div>
<div class="m2"><p>چو با دوست سختی کنی دشمن اوست</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>چو سندان کسی سخت رویی نکرد</p></div>
<div class="m2"><p>که خایسک تأدیب بر سر نخورد</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>به گفتن درشتی مکن با امیر</p></div>
<div class="m2"><p>چو بینی که سختی کند، سست گیر</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>به اخلاق با هر که بینی بساز</p></div>
<div class="m2"><p>اگر زیردست است اگر سرفراز</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>که این گردن از نازکی بر کشد</p></div>
<div class="m2"><p>به گفتار خوش، و آن سر اندر کشد</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>به شیرین زبانی توان برد گوی</p></div>
<div class="m2"><p>که پیوسته تلخی برد تندخوی</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>تو شیرین زبانی ز سعدی بگیر</p></div>
<div class="m2"><p>ترش روی را گو به تلخی بمیر</p></div></div>