---
title: >-
    بخش ۲۸ - حکایت ذوالنون مصری
---
# بخش ۲۸ - حکایت ذوالنون مصری

<div class="b" id="bn1"><div class="m1"><p>چنین یاد دارم که سقای نیل</p></div>
<div class="m2"><p>نکرد آب بر مصر سالی سبیل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گروهی سوی کوهساران شدند</p></div>
<div class="m2"><p>به فریاد خواهان باران شدند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گرستند و از گریه جویی روان</p></div>
<div class="m2"><p>نیامد مگر گریهٔ آسمان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به ذوالنون خبر برد از ایشان کسی</p></div>
<div class="m2"><p>که بر خلق رنج است و زحمت بسی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فرو ماندگان را دعایی بکن</p></div>
<div class="m2"><p>که مقبول را رد نباشد سخن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شنیدم که ذوالنون به مدین گریخت</p></div>
<div class="m2"><p>بسی بر نیامد که باران بریخت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خبر شد به مدین پس از روز بیست</p></div>
<div class="m2"><p>که ابر سیه دل بر ایشان گریست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سبک عزم باز آمدن کرد پیر</p></div>
<div class="m2"><p>که پر شد به سیل بهاران غدیر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بپرسید از او عارفی در نهفت</p></div>
<div class="m2"><p>چه حکمت در این رفتنت بود؟ گفت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شنیدم که بر مرغ و مور و ددان</p></div>
<div class="m2"><p>شود تنگ روزی به فعل بدان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>در این کشور اندیشه کردم بسی</p></div>
<div class="m2"><p>پریشان‌تر از خود ندیدم کسی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>برفتم مبادا که از شر من</p></div>
<div class="m2"><p>ببندد در خیر بر انجمن</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بهی بایدت لطف کن کان بهان</p></div>
<div class="m2"><p>ندیدندی از خود بتر در جهان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>تو آنگه شوی پیش مردم عزیز</p></div>
<div class="m2"><p>که مر خویشتن را نگیری به چیز</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بزرگی که خود را به خردی شمرد</p></div>
<div class="m2"><p>به دنیا و عقبی بزرگی ببرد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>از این خاکدان بنده‌ای پاک شد</p></div>
<div class="m2"><p>که در پای کمتر کسی خاک شد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>الا ای که بر خاک ما بگذری</p></div>
<div class="m2"><p>به خاک عزیزان که یاد آوری</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>که گر خاک شد سعدی، او را چه غم؟</p></div>
<div class="m2"><p>که در زندگی خاک بوده‌ست هم</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>به بیچارگی تن فرا خاک داد</p></div>
<div class="m2"><p>وگر گرد عالم برآمد چو باد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بسی برنیاید که خاکش خورد</p></div>
<div class="m2"><p>دگر باره بادش به عالم برد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>مگر تا گلستان معنی شکفت</p></div>
<div class="m2"><p>بر او هیچ بلبل چنین خوش نگفت</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>عجب گر بمیرد چنین بلبلی</p></div>
<div class="m2"><p>که بر استخوانش نروید گلی</p></div></div>