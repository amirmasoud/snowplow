---
title: >-
    بخش ۱۴ - حکایت
---
# بخش ۱۴ - حکایت

<div class="b" id="bn1"><div class="m1"><p>ملک صالح از پادشاهان شام</p></div>
<div class="m2"><p>برون آمدی صبحدم با غلام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بگشتی در اطراف بازار و کوی</p></div>
<div class="m2"><p>به رسم عرب نیمه بر بسته روی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>که صاحب نظر بود و درویش دوست</p></div>
<div class="m2"><p>هر آن کاین دو دارد ملک صالح اوست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دو درویش در مسجدی خفته یافت</p></div>
<div class="m2"><p>پریشان دل و خاطر آشفته یافت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شب سردشان دیده نابرده خواب</p></div>
<div class="m2"><p>چو حربا تأمل کنان آفتاب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یکی زآن دو می گفت با دیگری</p></div>
<div class="m2"><p>که هم روز محشر بود داوری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر این پادشاهان گردن فراز</p></div>
<div class="m2"><p>که در لهو و عیشند و با کام و ناز</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در آیند با عاجزان در بهشت</p></div>
<div class="m2"><p>من از گور سر بر نگیرم ز خشت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بهشت برین ملک و مأوای ماست</p></div>
<div class="m2"><p>که بند غم امروز بر پای ماست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>همه عمر از اینان چه دیدی خوشی</p></div>
<div class="m2"><p>که در آخرت نیز زحمت کشی؟</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>اگر صالح آنجا به دیوار باغ</p></div>
<div class="m2"><p>بر آید، به کفشش بدرم دماغ</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چو مرد این سخن گفت و صالح شنید</p></div>
<div class="m2"><p>دگر بودن آنجا مصالح ندید</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>دمی رفت تا چشمهٔ آفتاب</p></div>
<div class="m2"><p>ز چشم خلایق فرو شست خواب</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>دوان هر دو را کس فرستاد و خواند</p></div>
<div class="m2"><p>به هیبت نشست و به حرمت نشاند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بر ایشان ببارید باران جود</p></div>
<div class="m2"><p>فرو شستشان گرد ذل از وجود</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>پس از رنج سرما و باران و سیل</p></div>
<div class="m2"><p>نشستند با نامداران خیل</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>گدایان بی جامه شب کرده روز</p></div>
<div class="m2"><p>معطر کنان جامه بر عود سوز</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>یکی گفت از اینان ملک را نهان</p></div>
<div class="m2"><p>که ای حلقه در گوش حکمت جهان</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>پسندیدگان در بزرگی رسند</p></div>
<div class="m2"><p>ز ما بندگانت چه آمد پسند؟</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>شهنشه ز شادی چو گل بر شکفت</p></div>
<div class="m2"><p>بخندید در روی درویش و گفت</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>من آن کس نیم کز غرور حشم</p></div>
<div class="m2"><p>ز بیچارگان روی در هم کشم</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>تو هم با من از سر بنه خوی زشت</p></div>
<div class="m2"><p>که ناسازگاری کنی در بهشت</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>من امروز کردم در صلح باز</p></div>
<div class="m2"><p>تو فردا مکن در به رویم فراز</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چنین راه اگر مقبلی پیش گیر</p></div>
<div class="m2"><p>شرف بایدت دست درویش گیر</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بر از شاخ طوبی کسی بر نداشت</p></div>
<div class="m2"><p>که امروز تخم ارادت نکاشت</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ارادت نداری سعادت مجوی</p></div>
<div class="m2"><p>به چوگان خدمت توان برد گوی</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>تو را کی بود چون چراغ التهاب</p></div>
<div class="m2"><p>که از خود پری همچو قندیل از آب؟</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>وجودی دهد روشنایی به جمع</p></div>
<div class="m2"><p>که سوزیش در سینه باشد چو شمع</p></div></div>