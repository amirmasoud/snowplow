---
title: >-
    بخش ۵ - مدح محمد بن سعد بن ابوبکر
---
# بخش ۵ - مدح محمد بن سعد بن ابوبکر

<div class="b" id="bn1"><div class="m1"><p>اتابک محمد شه نیکبخت</p></div>
<div class="m2"><p>خداوند تاج و خداوند تخت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جوان جوان‌بخت روشن‌ضمیر</p></div>
<div class="m2"><p>به دولت جوان و به تدبیر پیر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به دانش بزرگ و به همت بلند</p></div>
<div class="m2"><p>به بازو دلیر و به دل هوشمند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زهی دولت مادر روزگار</p></div>
<div class="m2"><p>که رودی چنین پرورد در کنار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به دست کرم آب دریا ببرد</p></div>
<div class="m2"><p>به رفعت محل ثریا ببرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زهی چشم دولت به روی تو باز</p></div>
<div class="m2"><p>سر شهریاران گردن فراز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>صدف را که بینی ز دردانه پر</p></div>
<div class="m2"><p>نه آن قدر دارد که یکدانه در</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تو آن در مکنون یکدانه‌ای</p></div>
<div class="m2"><p>که پیرایهٔ سلطنت خانه‌ای</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نگه دار یارب به چشم خودش</p></div>
<div class="m2"><p>بپرهیز از آسیب چشم بدش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خدایا در آفاق نامی کنش</p></div>
<div class="m2"><p>به توفیق طاعت گرامی کنش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مقیمش در انصاف و تقوی بدار</p></div>
<div class="m2"><p>مرادش به دنیا و عقبی برآر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>غم از دشمن ناپسندش مباد</p></div>
<div class="m2"><p>وز اندیشه بر دل گزندش مباد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بهشتی درخت آورد چون تو بار</p></div>
<div class="m2"><p>پسر نامجوی و پدر نامدار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>از آن خاندان خیر بیگانه دان</p></div>
<div class="m2"><p>که باشند بدخواه این خاندان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>زهی دین و دانش، زهی عدل و داد</p></div>
<div class="m2"><p>زهی ملک و دولت که پاینده باد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>نگنجد کرمهای حق در قیاس</p></div>
<div class="m2"><p>چه خدمت گزارد زبان سپاس؟</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>خدایا تو این شاه درویش دوست</p></div>
<div class="m2"><p>که آسایش خلق در ظل اوست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بسی بر سر خلق پاینده دار</p></div>
<div class="m2"><p>به توفیق طاعت دلش زنده دار</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>برومند دارش درخت امید</p></div>
<div class="m2"><p>سرش سبز و رویش به رحمت سفید</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>به راه تکلف مرو سعدیا</p></div>
<div class="m2"><p>اگر صدق داری بیار و بیا</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>تو منزل شناسی و شه راهرو</p></div>
<div class="m2"><p>تو حقگوی و خسرو حقایق شنو</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چه حاجت که نه کرسی آسمان</p></div>
<div class="m2"><p>نهی زیر پای قزل ارسلان</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>مگو پای عزت بر افلاک نه</p></div>
<div class="m2"><p>بگو روی اخلاص بر خاک نه</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بطاعت بنه چهره بر آستان</p></div>
<div class="m2"><p>که این است سر جاده راستان</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>اگر بنده‌ای سر بر این در بنه</p></div>
<div class="m2"><p>کلاه خداوندی از سر بنه</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>به درگاه فرمانده ذوالجلال</p></div>
<div class="m2"><p>چو درویش پیش توانگر بنال</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>چو طاعت کنی لبس شاهی مپوش</p></div>
<div class="m2"><p>چو درویش مخلص برآور خروش</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>که پروردگارا توانگر تویی</p></div>
<div class="m2"><p>توانا و درویش پرور تویی</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>نه کشور خدایم نه فرماندهم</p></div>
<div class="m2"><p>یکی از گدایان این درگهم</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>تو بر خیر و نیکی دهم دسترس</p></div>
<div class="m2"><p>وگر نه چه خیر آید از من به کس؟</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>دعا کن به شب چون گدایان به سوز</p></div>
<div class="m2"><p>اگر می‌کنی پادشاهی به روز</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>کمر بسته گردن کشان بر درت</p></div>
<div class="m2"><p>تو بر آستان عبادت سرت</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>زهی بندگان را خداوندگار</p></div>
<div class="m2"><p>خداوند را بندهٔ حق گزار</p></div></div>