---
title: >-
    بخش ۱۳ - حکایت در معنی استیلای عشق بر عقل
---
# بخش ۱۳ - حکایت در معنی استیلای عشق بر عقل

<div class="b" id="bn1"><div class="m1"><p>یکی پنجهٔ آهنین راست کرد</p></div>
<div class="m2"><p>که با شیر زورآوری خواست کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو شیرش به سرپنجه در خود کشید</p></div>
<div class="m2"><p>دگر زور در پنجه در خود ندید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یکی گفتش آخر چه خسبی چو زن؟</p></div>
<div class="m2"><p>به سرپنجه آهنینش بزن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شنیدم که مسکین در آن زیر گفت</p></div>
<div class="m2"><p>نشاید بدین پنجه با شیر گفت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو بر عقل دانا شود عشق چیر</p></div>
<div class="m2"><p>همان پنجه آهنین است و شیر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تو در پنجه شیر مرد اوژنی</p></div>
<div class="m2"><p>چه سودت کند پنجهٔ آهنی؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو عشق آمد از عقل دیگر مگوی</p></div>
<div class="m2"><p>که در دست چوگان اسیر است گوی</p></div></div>