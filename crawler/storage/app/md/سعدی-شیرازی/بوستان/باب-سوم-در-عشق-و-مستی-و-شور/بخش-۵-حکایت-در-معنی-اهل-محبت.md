---
title: >-
    بخش ۵ - حکایت در معنی اهل محبت
---
# بخش ۵ - حکایت در معنی اهل محبت

<div class="b" id="bn1"><div class="m1"><p>شنیدم که بر لحن خنیاگری</p></div>
<div class="m2"><p>به رقص اندر آمد پری پیکری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز دلهای شوریده پیرامنش</p></div>
<div class="m2"><p>گرفت آتش شمع در دامنش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پراکنده خاطر شد و خشمناک</p></div>
<div class="m2"><p>یکی گفتش از دوستداران، چه باک؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تو را آتش ای دوست دامن بسوخت</p></div>
<div class="m2"><p>مرا خود به یک بار خرمن بسوخت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگر یاری از خویشتن دم مزن</p></div>
<div class="m2"><p>که شرک است با یار و با خویشتن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چنین دارم از پیر داننده یاد</p></div>
<div class="m2"><p>که شوریده‌ای سر به صحرا نهاد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پدر در فراقش نخورد و نخفت</p></div>
<div class="m2"><p>پسر را ملامت بکردند و گفت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از انگه که یارم کس خویش خواند</p></div>
<div class="m2"><p>دگر با کسم آشنایی نماند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به حقش که تا حق جمالم نمود</p></div>
<div class="m2"><p>دگر هر چه دیدم خیالم نمود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نشد گم که روی از خلایق بتافت</p></div>
<div class="m2"><p>که گم کرده خویش را باز یافت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>پراکندگانند زیر فلک</p></div>
<div class="m2"><p>که هم دد توان خواندشان هم ملک</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ز یاد ملک چون ملک نارمند</p></div>
<div class="m2"><p>شب و روز چون دد ز مردم رمند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>قوی بازوانند کوتاه دست</p></div>
<div class="m2"><p>خردمند شیدا و هشیار مست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گه آسوده در گوشه‌ای خرقه دوز</p></div>
<div class="m2"><p>گه آشفته در مجلسی خرقه سوز</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نه سودای خودشان، نه پروای کس</p></div>
<div class="m2"><p>نه در کنج توحیدشان جای کس</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>پریشیده عقل و پراکنده هوش</p></div>
<div class="m2"><p>ز قول نصیحتگر آکنده گوش</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>به دریا نخواهد شدن بط غریق</p></div>
<div class="m2"><p>سمندر چه داند عذاب حریق؟</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>تهیدست مردان پر حوصله</p></div>
<div class="m2"><p>بیابان نوردان پی قافله</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>عزیزان پوشیده از چشم خلق</p></div>
<div class="m2"><p>نه زنار داران پوشیده دلق</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ندارند چشم از خلایق پسند</p></div>
<div class="m2"><p>که ایشان پسندیده حق بسند</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>پر از میوه و سایه‌ور چون رزند</p></div>
<div class="m2"><p>نه چون ما سیه‌کار و ازرق رزند</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>به خود سر فرو برده همچون صدف</p></div>
<div class="m2"><p>نه مانند دریا بر آورده کف</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>نه مردم همین استخوانند و پوست</p></div>
<div class="m2"><p>نه هر صورتی جان معنی در اوست</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>نه سلطان خریدار هر بنده‌ای است</p></div>
<div class="m2"><p>نه در زیر هر ژنده‌ای زنده‌ای است</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>اگر ژاله هر قطره‌ای در شدی</p></div>
<div class="m2"><p>چو خرمهره بازار از او پر شدی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>چو غازی به خود بر نبندند پای</p></div>
<div class="m2"><p>که محکم رود پای چوبین ز جای</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>حریفان خلوت سرای الست</p></div>
<div class="m2"><p>به یک جرعه تا نفخهٔ صور مست</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>به تیغ از غرض بر نگیرند چنگ</p></div>
<div class="m2"><p>که پرهیز و عشق آبگینه‌ست و سنگ</p></div></div>