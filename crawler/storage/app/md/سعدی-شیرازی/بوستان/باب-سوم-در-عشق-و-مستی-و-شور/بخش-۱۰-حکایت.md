---
title: >-
    بخش ۱۰ - حکایت
---
# بخش ۱۰ - حکایت

<div class="b" id="bn1"><div class="m1"><p>یکی در نشابور دانی چه گفت</p></div>
<div class="m2"><p>چو فرزندش از فرض خفتن بخفت؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>توقع مدار ای پسر گر کسی</p></div>
<div class="m2"><p>که بی سعی هرگز به جایی رسی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سمیلان چو می بر نگیرد قدم</p></div>
<div class="m2"><p>وجودی است بی منفعت چون عدم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>طمع دار سود و بترس از زیان</p></div>
<div class="m2"><p>که بی بهره باشند فارغ زیان</p></div></div>