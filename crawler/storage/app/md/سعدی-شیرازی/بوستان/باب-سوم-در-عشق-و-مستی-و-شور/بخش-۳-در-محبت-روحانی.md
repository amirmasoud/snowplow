---
title: >-
    بخش ۳ - در محبت روحانی
---
# بخش ۳ - در محبت روحانی

<div class="b" id="bn1"><div class="m1"><p>چو عشقی که بنیاد آن بر هواست</p></div>
<div class="m2"><p>چنین فتنه‌انگیز و فرمانرواست،</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عجب داری از سالکان طریق</p></div>
<div class="m2"><p>که باشند در بحر معنی غریق؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به سودای جانان به جان مشتغل</p></div>
<div class="m2"><p>به ذکر حبیب از جهان مشتغل</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به یاد حق از خلق بگریخته</p></div>
<div class="m2"><p>چنان مست ساقی که می ریخته</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نشاید به دارو دوا کردشان</p></div>
<div class="m2"><p>که کس مطلع نیست بر دردشان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>الست از ازل همچنانشان به گوش</p></div>
<div class="m2"><p>به فریاد قالوا بلی در خروش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گروهی عمل دار عزلت نشین</p></div>
<div class="m2"><p>قدمهای خاکی، دم آتشین</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به یک نعره کوهی ز جا بر کنند</p></div>
<div class="m2"><p>به یک ناله شهری به هم بر زنند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو بادند پنهان و چالاک پوی</p></div>
<div class="m2"><p>چو سنگند خاموش و تسبیح گوی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سحرها بگریند چندان که آب</p></div>
<div class="m2"><p>فرو شوید از دیده‌شان کحل خواب</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>فرس کشته از بس که شب رانده‌اند</p></div>
<div class="m2"><p>سحرگه خروشان که وامانده‌اند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>شب و روز در بحر سودا و سوز</p></div>
<div class="m2"><p>ندانند ز آشفتگی شب ز روز</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چنان فتنه بر حسن صورت‌نگار</p></div>
<div class="m2"><p>که با حسن صورت ندارند کار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ندادند صاحبدلان دل به پوست</p></div>
<div class="m2"><p>وگر ابلهی داد، بی‌مغز اوست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>می صرف وحدت کسی نوش کرد</p></div>
<div class="m2"><p>که دنیا و عقبی فراموش کرد</p></div></div>