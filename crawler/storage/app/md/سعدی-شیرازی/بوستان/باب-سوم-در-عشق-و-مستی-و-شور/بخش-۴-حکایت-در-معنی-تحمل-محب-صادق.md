---
title: >-
    بخش ۴ - حکایت در معنی تحمل محب صادق
---
# بخش ۴ - حکایت در معنی تحمل محب صادق

<div class="b" id="bn1"><div class="m1"><p>شنیدم که وقتی گدازاده‌ای</p></div>
<div class="m2"><p>نظر داشت با پادشازاده‌ای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همی‌رفت و می‌پخت سودای خام</p></div>
<div class="m2"><p>خیالش فرو برده دندان به کام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز میدانش خالی نبودی چو میل</p></div>
<div class="m2"><p>همه وقت پهلوی اسبش چو پیل</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دلش خون شد و راز در دل بماند</p></div>
<div class="m2"><p>ولی پایش از گریه در گل بماند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رقیبان خبر یافتندش ز درد</p></div>
<div class="m2"><p>دگرباره گفتندش اینجا مگرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دمی رفت و یاد آمدش روی دوست</p></div>
<div class="m2"><p>دگر خیمه زد بر سر کوی دوست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>غلامی شکستش سر و دست و پای</p></div>
<div class="m2"><p>که باری نگفتیمت ایدر مپای</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دگر رفت و صبر و قرارش نبود</p></div>
<div class="m2"><p>شکیبایی از روی یارش نبود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مگس وارش از پیش شکر به جور</p></div>
<div class="m2"><p>براندندی و بازگشتی بفور</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کسی گفتش ای شوخ دیوانه رنگ</p></div>
<div class="m2"><p>عجب صبر داری تو بر چوب و سنگ!</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بگفت این جفا بر من از دست اوست</p></div>
<div class="m2"><p>نه شرطیست نالیدن از دست دوست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>من اینک دم دوستی می‌زنم</p></div>
<div class="m2"><p>گر او دوست دارد وگر دشمنم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ز من صبر بی او توقع مدار</p></div>
<div class="m2"><p>که با او هم امکان ندارد قرار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نه نیروی صبرم نه جای ستیز</p></div>
<div class="m2"><p>نه امکان بودن نه پای گریز</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>مگو زین در بارگه سر بتاب</p></div>
<div class="m2"><p>وگر سر چو میخم نهد در طناب</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>نه پروانه جان داده در پای دوست</p></div>
<div class="m2"><p>به از زنده در کنج تاریک اوست؟</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بگفت ار خوری زخم چوگان اوی؟</p></div>
<div class="m2"><p>بگفتا به پایش در افتم چو گوی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بگفتا سرت گر ببرد به تیغ؟</p></div>
<div class="m2"><p>بگفت این قدر نبود از وی دریغ</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>مرا خود ز سر نیست چندان خبر</p></div>
<div class="m2"><p>که تاج است بر تارکم یا تبر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>مکن با من ناشکیبا عتیب</p></div>
<div class="m2"><p>که در عشق صورت نبندد شکیب</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چو یعقوبم ار دیده گردد سپید</p></div>
<div class="m2"><p>نبرم ز دیدار یوسف امید</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>یکی را که سر خوش بود با یکی</p></div>
<div class="m2"><p>نیازارد از وی به هر اندکی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>رکابش ببوسید روزی جوان</p></div>
<div class="m2"><p>برآشفت و برتافت از وی عنان</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بخندید و گفتا عنان برمپیچ</p></div>
<div class="m2"><p>که سلطان عنان برنپیچد ز هیچ</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>مرا با وجود تو هستی نماند</p></div>
<div class="m2"><p>به یاد توام خودپرستی نماند</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>گرم جرم بینی مکن عیب من</p></div>
<div class="m2"><p>تویی سر بر آورده از جیب من</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بدان زهره دستت زدم در رکاب</p></div>
<div class="m2"><p>که خود را نیاوردم اندر حساب</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>کشیدم قلم در سر نام خویش</p></div>
<div class="m2"><p>نهادم قدم بر سر کام خویش</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>مرا خود کشد تیر آن چشم مست</p></div>
<div class="m2"><p>چه حاجت که آری به شمشیر دست؟</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>تو آتش به نی در زن و در گذر</p></div>
<div class="m2"><p>که نه خشک در بیشه ماند نه تر</p></div></div>