---
title: >-
    بخش ۲۴ - حکایت پروانه و صدق محبت او
---
# بخش ۲۴ - حکایت پروانه و صدق محبت او

<div class="b" id="bn1"><div class="m1"><p>کسی گفت پروانه را کای حقیر</p></div>
<div class="m2"><p>برو دوستی در خور خویش گیر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رهی رو که بینی طریق رجا</p></div>
<div class="m2"><p>تو و مهر شمع از کجا تا کجا؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سمندر نه‌ای گرد آتش مگرد</p></div>
<div class="m2"><p>که مردانگی باید آنگه نبرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز خورشید پنهان شود موش کور</p></div>
<div class="m2"><p>که جهل است با آهنین پنجه زور</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کسی را که دانی که خصم تو اوست</p></div>
<div class="m2"><p>نه از عقل باشد گرفتن به دوست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تو را کس نگوید نکو می‌کنی</p></div>
<div class="m2"><p>که جان در سر کار او می‌کنی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گدایی که از پادشه خواست دخت</p></div>
<div class="m2"><p>قفا خورد و سودای بیهوده پخت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کجا در حساب آرد او چون تو دوست</p></div>
<div class="m2"><p>که روی ملوک و سلاطین در اوست؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مپندار کاو در چنان مجلسی</p></div>
<div class="m2"><p>مدارا کند با چو تو مفلسی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>وگر با همه خلق نرمی کند</p></div>
<div class="m2"><p>تو بیچاره‌ای با تو گرمی کند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نگه کن که پروانهٔ سوزناک</p></div>
<div class="m2"><p>چه گفت، ای عجب گر بسوزم چه باک؟</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مرا چون خلیل آتشی در دل است</p></div>
<div class="m2"><p>که پنداری این شعله بر من گل است</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نه دل دامن دلستان می‌کشد</p></div>
<div class="m2"><p>که مهرش گریبان جان می‌کشد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نه خود را بر آتش به خود می‌زنم</p></div>
<div class="m2"><p>که زنجیر شوق است در گردنم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>مرا همچنان دور بودم که سوخت</p></div>
<div class="m2"><p>نه این دم که آتش به من در فروخت</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>نه آن می‌کند یار در شاهدی</p></div>
<div class="m2"><p>که با او توان گفتن از زاهدی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>که عیبم کند بر تولای دوست؟</p></div>
<div class="m2"><p>که من راضیم کشته در پای دوست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>مرا بر تلف حرص دانی چراست؟</p></div>
<div class="m2"><p>چو او هست اگر من نباشم رواست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بسوزم که یار پسندیده اوست</p></div>
<div class="m2"><p>که در وی سرایت کند سوز دوست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>مرا چند گویی که در خورد خویش</p></div>
<div class="m2"><p>حریفی به دست آر همدرد خویش</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بدان ماند اندرز شوریده حال</p></div>
<div class="m2"><p>که گویی به کژدم گزیده منال</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>کسی را نصیحت مگو ای شگفت</p></div>
<div class="m2"><p>که دانی که در وی نخواهد گرفت</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ز کف رفته بیچاره‌ای را لگام</p></div>
<div class="m2"><p>نگویند کآهسته را ای غلام</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چه نغز آمد این نکته در سندباد</p></div>
<div class="m2"><p>که عشق آتش است ای پسر پند، باد</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>به باد آتش تیز برتر شود</p></div>
<div class="m2"><p>پلنگ از زدن کینه ور تر شود</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>چو نیکت بدیدم بدی می‌کنی</p></div>
<div class="m2"><p>که رویم فرا چون خودی می‌کنی</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ز خود بهتری جوی و فرصت شمار</p></div>
<div class="m2"><p>که با چون خودی گم کنی روزگار</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>پی چون خودی خودپرستان روند</p></div>
<div class="m2"><p>به کوی خطرناک مستان روند</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>من اول که این کار سر داشتم</p></div>
<div class="m2"><p>دل از سر به یک بار برداشتم</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>سر انداز در عاشقی صادق است</p></div>
<div class="m2"><p>که بدزهره بر خویشتن عاشق است</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>اجل ناگهی در کمینم کشد</p></div>
<div class="m2"><p>همان به که آن نازنینم کشد</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>چو بی شک نبشته‌ست بر سر هلاک</p></div>
<div class="m2"><p>به دست دلارام خوشتر هلاک</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>نه روزی به بیچارگی جان دهی؟</p></div>
<div class="m2"><p>همان به که در پای جانان دهی</p></div></div>