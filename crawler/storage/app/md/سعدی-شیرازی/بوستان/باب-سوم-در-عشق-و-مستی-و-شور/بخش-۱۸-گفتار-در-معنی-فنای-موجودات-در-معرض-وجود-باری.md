---
title: >-
    بخش ۱۸ - گفتار در معنی فنای موجودات در معرض وجود باری
---
# بخش ۱۸ - گفتار در معنی فنای موجودات در معرض وجود باری

<div class="b" id="bn1"><div class="m1"><p>ره عقل جز پیچ بر پیچ نیست</p></div>
<div class="m2"><p>بر عارفان جز خدا هیچ نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>توان گفتن این با حقایق شناس</p></div>
<div class="m2"><p>ولی خرده گیرند اهل قیاس</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>که پس آسمان و زمین چیستند؟</p></div>
<div class="m2"><p>بنی آدم و دام و دد کیستند؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پسندیده پرسیدی ای هوشمند</p></div>
<div class="m2"><p>بگویم گر آید جوابت پسند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>که هامون و دریا و کوه و فلک</p></div>
<div class="m2"><p>پری و آدمی‌زاد و دیو و ملک</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همه هرچه هستند از آن کمترند</p></div>
<div class="m2"><p>که با هستیش نام هستی برند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عظیم است پیش تو دریا به موج</p></div>
<div class="m2"><p>بلند است خورشید تابان به اوج</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ولی اهل صورت کجا پی برند</p></div>
<div class="m2"><p>که ارباب معنی به ملکی درند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>که گر آفتاب است یک ذره نیست</p></div>
<div class="m2"><p>وگر هفت دریاست یک قطره نیست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چو سلطان عزت علم بر کشد</p></div>
<div class="m2"><p>جهان سر به جیب عدم در کشد</p></div></div>