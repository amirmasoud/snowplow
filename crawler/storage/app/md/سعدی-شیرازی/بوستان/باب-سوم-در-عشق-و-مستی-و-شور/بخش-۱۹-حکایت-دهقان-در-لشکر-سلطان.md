---
title: >-
    بخش ۱۹ - حکایت دهقان در لشکر سلطان
---
# بخش ۱۹ - حکایت دهقان در لشکر سلطان

<div class="b" id="bn1"><div class="m1"><p>رئیس دهی با پسر در رهی</p></div>
<div class="m2"><p>گذشتند بر قلب شاهنشهی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پسر چاوشان دید و تیغ و تبر</p></div>
<div class="m2"><p>قباهای اطلس، کمرهای زر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یلان کماندار نخجیر زن</p></div>
<div class="m2"><p>غلامان ترکش کش تیرزن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یکی در برش پرنیانی قباه</p></div>
<div class="m2"><p>یکی بر سرش خسروانی کلاه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پسر کان همه شوکت و پایه دید</p></div>
<div class="m2"><p>پدر را به غایت فرومایه دید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>که حالش بگردید و رنگش بریخت</p></div>
<div class="m2"><p>ز هیبت به بیغوله‌ای در گریخت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پسر گفتش آخر بزرگ دهی</p></div>
<div class="m2"><p>به سرداری از سر بزرگان مهی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چه بودت که ببریدی از جان امید</p></div>
<div class="m2"><p>بلرزیدی از باد هیبت چو بید؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بلی، گفت سالار و فرماندهم</p></div>
<div class="m2"><p>ولی عزتم هست تا در دهم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بزرگان از آن دهشت آلوده‌اند</p></div>
<div class="m2"><p>که در بارگاه ملک بوده‌اند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تو، ای بی خبر، همچنان در دهی</p></div>
<div class="m2"><p>که بر خویشتن منصبی می‌نهی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نگفتند حرفی زبان آوران</p></div>
<div class="m2"><p>که سعدی نگوید مثالی بر آن</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>مگر دیده باشی که در باغ و راغ</p></div>
<div class="m2"><p>بتابد به شب کرمکی چون چراغ</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>یکی گفتش ای کرمک شب فروز</p></div>
<div class="m2"><p>چه بودت که بیرون نیایی به روز؟</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ببین کآتشی کرمک خاکزاد</p></div>
<div class="m2"><p>جواب از سر روشنایی چه داد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>که من روز و شب جز به صحرا نیم</p></div>
<div class="m2"><p>ولی پیش خورشید پیدا نیم</p></div></div>