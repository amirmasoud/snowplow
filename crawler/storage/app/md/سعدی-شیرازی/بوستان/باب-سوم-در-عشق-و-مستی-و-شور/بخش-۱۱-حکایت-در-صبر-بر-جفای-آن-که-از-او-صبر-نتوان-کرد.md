---
title: >-
    بخش ۱۱ - حکایت در صبر بر جفای آن که از او صبر نتوان کرد
---
# بخش ۱۱ - حکایت در صبر بر جفای آن که از او صبر نتوان کرد

<div class="b" id="bn1"><div class="m1"><p>شکایت کند نوعروسی جوان</p></div>
<div class="m2"><p>به پیری ز داماد نامهربان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که مپسند چندین که با این پسر</p></div>
<div class="m2"><p>به تلخی رود روزگارم به سر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کسانی که با ما در این منزلند</p></div>
<div class="m2"><p>نبینم که چون من پریشان دلند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زن و مرد با هم چنان دوستند</p></div>
<div class="m2"><p>که گویی دو مغز و یکی پوستند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ندیدم در این مدت از شوی من</p></div>
<div class="m2"><p>که باری بخندید در روی من</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شنید این سخن پیر فرخنده فال</p></div>
<div class="m2"><p>سخندان بود مرد دیرینه سال</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یکی پاسخش داد شیرین و خوش</p></div>
<div class="m2"><p>که گر خوبروی است بارش بکش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دریغ است روی از کسی تافتن</p></div>
<div class="m2"><p>که دیگر نشاید چنو یافتن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چرا سر کشی زان که گر سر کشد</p></div>
<div class="m2"><p>به حرف وجودت قلم در کشد؟</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>یکم روز بر بنده‌ای دل بسوخت</p></div>
<div class="m2"><p>که می‌گفت و فرماندهش می‌فروخت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تو را بنده از من به افتد بسی</p></div>
<div class="m2"><p>مرا چون تو دیگر نیفتد کسی</p></div></div>