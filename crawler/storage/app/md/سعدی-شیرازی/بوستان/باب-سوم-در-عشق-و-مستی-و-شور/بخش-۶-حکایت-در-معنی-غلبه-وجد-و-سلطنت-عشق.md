---
title: >-
    بخش ۶ - حکایت در معنی غلبه وجد و سلطنت عشق
---
# بخش ۶ - حکایت در معنی غلبه وجد و سلطنت عشق

<div class="b" id="bn1"><div class="m1"><p>یکی شاهدی در سمرقند داشت</p></div>
<div class="m2"><p>که گفتی به جای سمر قند داشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جمالی گرو برده از آفتاب</p></div>
<div class="m2"><p>ز شوخیش بنیاد تقوی خراب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تعالی الله از حسن تا غایتی</p></div>
<div class="m2"><p>که پنداری از رحمت است آیتی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همی رفتی و دیده‌ها در پیش</p></div>
<div class="m2"><p>دل دوستان کرده جان برخیش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نظر کردی این دوست در وی نهفت</p></div>
<div class="m2"><p>نگه کرد باری به تندی و گفت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>که ای خیره سر چند پویی پیم</p></div>
<div class="m2"><p>ندانی که من مرغ دامت نیم؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گرت بار دیگر ببینم به تیغ</p></div>
<div class="m2"><p>چو دشمن ببرم سرت بی دریغ</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کسی گفتش اکنون سر خویش گیر</p></div>
<div class="m2"><p>از این سهل تر مطلبی پیش گیر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نپندارم این کام حاصل کنی</p></div>
<div class="m2"><p>مبادا که جان در سر دل کنی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چو مفتون صادق ملامت شنید</p></div>
<div class="m2"><p>بدرد از درون ناله‌ای برکشید</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>که بگذار تا زخم تیغ هلاک</p></div>
<div class="m2"><p>بغلطاندم لاشه در خون و خاک</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مگر پیش دشمن بگویند و دوست</p></div>
<div class="m2"><p>که این کشته دست و شمشیر اوست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نمی‌بینم از خاک کویش گریز</p></div>
<div class="m2"><p>به بیداد گو آبرویم بریز</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>مرا توبه فرمایی ای خودپرست</p></div>
<div class="m2"><p>تو را توبه زین گفت اولی ترست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ببخشای بر من که هرچ او کند</p></div>
<div class="m2"><p>وگر قصد خون است نیکو کند</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بسوزاندم هر شبی آتشش</p></div>
<div class="m2"><p>سحر زنده گردم به بوی خوشش</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>اگر میرم امروز در کوی دوست</p></div>
<div class="m2"><p>قیامت زنم خیمه پهلوی دوست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>مده تا توانی در این جنگ پشت</p></div>
<div class="m2"><p>که زنده‌ست سعدی که عشقش بکشت</p></div></div>