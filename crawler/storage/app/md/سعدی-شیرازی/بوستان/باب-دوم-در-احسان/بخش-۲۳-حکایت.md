---
title: >-
    بخش ۲۳ - حکایت
---
# بخش ۲۳ - حکایت

<div class="b" id="bn1"><div class="m1"><p>یکی را خری در گل افتاده بود</p></div>
<div class="m2"><p>ز سوداش خون در دل افتاده بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیابان و باران و سرما و سیل</p></div>
<div class="m2"><p>فرو هشته ظلمت بر آفاق ذیل</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همه شب در این غصه تا بامداد</p></div>
<div class="m2"><p>سقط گفت و نفرین و دشنام داد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نه دشمن برست از زبانش نه دوست</p></div>
<div class="m2"><p>نه سلطان که این بوم و بر زآن اوست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>قضا را خداوند آن پهن دشت</p></div>
<div class="m2"><p>در آن حال منکر بر او بر گذشت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شنید این سخنهای دور از صواب</p></div>
<div class="m2"><p>نه صبر شنیدن، نه روی جواب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ملک شرمگین در حشم بنگریست</p></div>
<div class="m2"><p>که سودای این بر من از بهر چیست؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>یکی گفت شاها به تیغش بزن</p></div>
<div class="m2"><p>که نگذاشت کس را نه دختر نه زن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نگه کرد سلطان عالی محل</p></div>
<div class="m2"><p>خودش در بلا دید و خر در وحل</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ببخشود بر حال مسکین مرد</p></div>
<div class="m2"><p>فرو خورد خشم سخنهای سرد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>زرش داد و اسب و قبا پوستین</p></div>
<div class="m2"><p>چه نیکو بود مهر در وقت کین</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>یکی گفتش ای پیر بی عقل و هوش</p></div>
<div class="m2"><p>عجب رستی از قتل، گفتا خموش</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>اگر من بنالیدم از درد خویش</p></div>
<div class="m2"><p>وی انعام فرمود در خورد خویش</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بدی را بدی سهل باشد جزا</p></div>
<div class="m2"><p>اگر مردی أَحسِن إلی مَن أساء</p></div></div>