---
title: >-
    بخش ۵ - حکایت عابد با شوخ دیده
---
# بخش ۵ - حکایت عابد با شوخ دیده

<div class="b" id="bn1"><div class="m1"><p>زباندانی آمد به صاحبدلی</p></div>
<div class="m2"><p>که محکم فرومانده‌ام در گلی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یکی سفله را ده درم بر من است</p></div>
<div class="m2"><p>که دانگی از او بر دلم ده من است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همه شب پریشان از او حال من</p></div>
<div class="m2"><p>همه روز چون سایه دنبال من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بکرد از سخنهای خاطر پریش</p></div>
<div class="m2"><p>درون دلم چون در خانه ریش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خدایش مگر تا ز مادر بزاد</p></div>
<div class="m2"><p>جز این ده درم چیز دیگر نداد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ندانسته از دفتر دین الف</p></div>
<div class="m2"><p>نخوانده به جز باب لاینصرف</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خور از کوه یک روز سر بر نزد</p></div>
<div class="m2"><p>که آن قلتبان حلقه بر در نزد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در اندیشه‌ام تا کدامم کریم</p></div>
<div class="m2"><p>از آن سنگدل دست گیرد به سیم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شنید این سخن پیر فرخ نهاد</p></div>
<div class="m2"><p>درستی دو، در آستینش نهاد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زر افتاد در دست افسانه گوی</p></div>
<div class="m2"><p>برون رفت از آنجا چو زر تازه روی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>یکی گفت: شیخ! این ندانی که کیست؟</p></div>
<div class="m2"><p>بر او گر بمیرد نباید گریست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گدایی که بر شیر نر زین نهد</p></div>
<div class="m2"><p>ابو زید را اسب و فرزین نهد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بر آشفت عابد که خاموش باش</p></div>
<div class="m2"><p>تو مرد زبان نیستی، گوش باش</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>اگر راست بود آنچه پنداشتم</p></div>
<div class="m2"><p>ز خلق آبرویش نگه داشتم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>وگر شوخ چشمی و سالوس کرد</p></div>
<div class="m2"><p>الا تا نپنداری افسوس کرد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>که خود را نگه داشتم آبروی</p></div>
<div class="m2"><p>ز دست چنان گربزی یاوه گوی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بد و نیک را بذل کن سیم و زر</p></div>
<div class="m2"><p>که این کسب خیر است و آن دفع شر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>خنک آن که در صحبت عاقلان</p></div>
<div class="m2"><p>بیاموزد اخلاق صاحبدلان</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>گرت عقل و رای است و تدبیر و هوش</p></div>
<div class="m2"><p>به عزت کنی پند سعدی به گوش</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>که اغلب در این شیوه دارد مقال</p></div>
<div class="m2"><p>نه در چشم و زلف و بناگوش و خال</p></div></div>