---
title: >-
    بخش ۲۵ - حکایت
---
# بخش ۲۵ - حکایت

<div class="b" id="bn1"><div class="m1"><p>یکی را پسر گم شد از راحله</p></div>
<div class="m2"><p>شبانگه بگردید در قافله</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز هر خیمه پرسید و هر سو شتافت</p></div>
<div class="m2"><p>به تاریکی آن روشنایی نیافت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو آمد بر مردم کاروان</p></div>
<div class="m2"><p>شنیدم که می‌گفت با ساروان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ندانی که چون راه بردم به دوست!</p></div>
<div class="m2"><p>هر آن کس که پیش آمدم گفتم اوست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از آن اهل دل در پی هر کسند</p></div>
<div class="m2"><p>که باشد که روزی به مردی رسند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>برند از برای دلی بارها</p></div>
<div class="m2"><p>خورند از برای گلی خارها</p></div></div>