---
title: >-
    بخش ۱۲ - گفتار اندر گردش روزگار
---
# بخش ۱۲ - گفتار اندر گردش روزگار

<div class="b" id="bn1"><div class="m1"><p>تو با خلق سهلی کن ای نیکبخت</p></div>
<div class="m2"><p>که فردا نگیرد خدا با تو سخت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر از پا در آید، نماند اسیر</p></div>
<div class="m2"><p>که افتادگان را بود دستگیر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به آزار فرمان مده بر رهی</p></div>
<div class="m2"><p>که باشد که افتد به فرماندهی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو تمکین و جاهت بود بر دوام</p></div>
<div class="m2"><p>مکن زور بر ضعف درویش عام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>که افتد که با جاه و تمکین شود</p></div>
<div class="m2"><p>چو بیدق که ناگاه فرزین شود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نصیحت شنو مردم دوربین</p></div>
<div class="m2"><p>نپاشند در هیچ دل تخم کین</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خداوند خرمن زیان می‌کند</p></div>
<div class="m2"><p>که بر خوشه‌چین سر گران می‌کند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نترسد که نعمت به مسکین دهند</p></div>
<div class="m2"><p>وزآن بار غم بر دل این نهند؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بسا زورمندا که افتاد سخت</p></div>
<div class="m2"><p>بس افتاده را یاوری کرد بخت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دل زیر دستان نباید شکست</p></div>
<div class="m2"><p>مبادا که روزی شوی زیردست</p></div></div>