---
title: >-
    بخش ۱ - سر آغاز
---
# بخش ۱ - سر آغاز

<div class="b" id="bn1"><div class="m1"><p>اگر هوشمندی به معنی گرای</p></div>
<div class="m2"><p>که معنی بماند ز صورت به جای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که را دانش و جود و تقوی نبود</p></div>
<div class="m2"><p>به صورت درش هیچ معنی نبود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کسی خسبد آسوده در زیر گل</p></div>
<div class="m2"><p>که خسبند از او مردم آسوده دل</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>غم خویش در زندگی خور که خویش</p></div>
<div class="m2"><p>به مرده نپردازد از حرص خویش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زر و نعمت اکنون بده کان تست</p></div>
<div class="m2"><p>که بعد از تو بیرون ز فرمان تست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نخواهی که باشی پراکنده دل</p></div>
<div class="m2"><p>پراکندگان را ز خاطر مهل</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پریشان کن امروز گنجینه چست</p></div>
<div class="m2"><p>که فردا کلیدش نه در دست تست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تو با خود ببر توشه خویشتن</p></div>
<div class="m2"><p>که شفقت نیاید ز فرزند و زن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کسی گوی دولت ز دنیا برد</p></div>
<div class="m2"><p>که با خود نصیبی به عقبی برد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به غمخوارگی چون سرانگشت من</p></div>
<div class="m2"><p>نخارد کس اندر جهان پشت من</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مکن، بر کف دست نه هر چه هست</p></div>
<div class="m2"><p>که فردا به دندان بری پشت دست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به پوشیدن ستر درویش کوش</p></div>
<div class="m2"><p>که ستر خدایت بود پرده پوش</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>مگردان غریب از درت بی نصیب</p></div>
<div class="m2"><p>مبادا که گردی به درها غریب</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بزرگی رساند به محتاج خیر</p></div>
<div class="m2"><p>که ترسد که محتاج گردد به غیر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>به حال دل خستگان در نگر</p></div>
<div class="m2"><p>که روزی تو دلخسته باشی مگر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>درون فروماندگان شاد کن</p></div>
<div class="m2"><p>ز روز فروماندگی یاد کن</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>نه خواهنده‌ای بر در دیگران؟</p></div>
<div class="m2"><p>به شکرانه خواهنده از در مران</p></div></div>