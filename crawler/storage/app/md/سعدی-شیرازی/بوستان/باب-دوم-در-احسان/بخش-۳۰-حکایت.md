---
title: >-
    بخش ۳۰ - حکایت
---
# بخش ۳۰ - حکایت

<div class="b" id="bn1"><div class="m1"><p>شنیدم که مردی غم خانه خورد</p></div>
<div class="m2"><p>که زنبور بر سقف او لانه کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زنش گفت از اینان چه خواهی؟ مکن</p></div>
<div class="m2"><p>که مسکین پریشان شوند از وطن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بشد مرد نادان پس کار خویش</p></div>
<div class="m2"><p>گرفتند یک روز زن را به نیش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زن بی خرد بر در و بام و کوی</p></div>
<div class="m2"><p>همی کرد فریاد و می‌گفت شوی:</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مکن روی بر مردم ای زن ترش</p></div>
<div class="m2"><p>تو گفتی که زنبور مسکین مکش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کسی با بدان نیکویی چون کند؟</p></div>
<div class="m2"><p>بدان را تحمل، بد افزون کند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو اندر سری بینی آزار خلق</p></div>
<div class="m2"><p>به شمشیر تیزش بیازار حلق</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سگ آخر که باشد که خوانش نهند؟</p></div>
<div class="m2"><p>بفرمای تا استخوانش دهند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چه نیکو زده‌ست این مثل پیر ده</p></div>
<div class="m2"><p>ستور لگدزن گران بار به</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>اگر نیکمردی نماید عسس</p></div>
<div class="m2"><p>نیارد به شب خفتن از دزد، کس</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نی نیزه در حلقهٔ کارزار</p></div>
<div class="m2"><p>بقیمت تر از نیشکر صد هزار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نه هر کس سزاوار باشد به مال</p></div>
<div class="m2"><p>یکی مال خواهد، یکی گوشمال</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چو گربه نوازی کبوتر برد</p></div>
<div class="m2"><p>چو فربه کنی گرگ، یوسف درد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بنایی که محکم ندارد اساس</p></div>
<div class="m2"><p>بلندش مکن ور کنی زو هراس</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چه خوش گفت بهرام صحرانشین</p></div>
<div class="m2"><p>چو یکران توسن زدش بر زمین</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>دگر اسبی از گله باید گرفت</p></div>
<div class="m2"><p>که گر سر کشد باز شاید گرفت</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ببند ای پسر دجله در آب کاست</p></div>
<div class="m2"><p>که سودی ندارد چو سیلاب خاست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چو گرگ خبیث آمدت در کمند</p></div>
<div class="m2"><p>بکش ور نه دل بر کن از گوسفند</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>از ابلیس هرگز نیاید سجود</p></div>
<div class="m2"><p>نه از بد گهر نیکویی در وجود</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بد اندیش را جاه و فرصت مده</p></div>
<div class="m2"><p>عدو در چه و دیو در شیشه به</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>مگو شاید این مار کشتن به چوب</p></div>
<div class="m2"><p>چو سر زیر سنگ تو دارد بکوب</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>قلم زن که بد کرد با زیردست</p></div>
<div class="m2"><p>قلم بهتر او را به شمشیر دست</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>مدبر که قانون بد می‌نهد</p></div>
<div class="m2"><p>تو را می‌برد تا به دوزخ دهد</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>مگو ملک را این مدبر بس است</p></div>
<div class="m2"><p>مدبر مخوانش که مدبر کس است</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>سعید آورد قول سعدی به جای</p></div>
<div class="m2"><p>که ترتیب ملک است و تدبیر رای</p></div></div>