---
title: >-
    بخش ۲۸ - حکایت
---
# بخش ۲۸ - حکایت

<div class="b" id="bn1"><div class="m1"><p>جوانی به دانگی کرم کرده بود</p></div>
<div class="m2"><p>تمنای پیری بر آورده بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به جرمی گرفت آسمان ناگهش</p></div>
<div class="m2"><p>فرستاد سلطان به کشتنگهش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تکاپوی ترکان و غوغای عام</p></div>
<div class="m2"><p>تماشاکنان بر در و کوی و بام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو دید اندر آشوب، درویش پیر</p></div>
<div class="m2"><p>جوان را به دست خلایق اسیر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دلش بر جوانمرد مسکین بخست</p></div>
<div class="m2"><p>که باری دل آورده بودش به دست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>برآورد زاری که سلطان بمرد</p></div>
<div class="m2"><p>جهان ماند و خوی پسندیده برد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به هم بر همی‌سود دست دریغ</p></div>
<div class="m2"><p>شنیدند ترکان آهخته تیغ</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به فریاد از ایشان بر آمد خروش</p></div>
<div class="m2"><p>تپانچه زنان بر سر و روی و دوش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>پیاده به سر تا در بارگاه</p></div>
<div class="m2"><p>دویدند و بر تخت دیدند شاه</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>جوان از میان رفت و بردند پیر</p></div>
<div class="m2"><p>به گردن بر تخت سلطان اسیر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به ولش بپرسید و هیبت نمود</p></div>
<div class="m2"><p>که مرگ منت خواستن بر چه بود؟</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چو نیک است خوی من و راستی</p></div>
<div class="m2"><p>بد مردم آخر چرا خواستی؟</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>برآورد پیر دلاور زبان</p></div>
<div class="m2"><p>که ای حلقه در گوش حکمت جهان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>به قول دروغی که سلطان بمرد</p></div>
<div class="m2"><p>نمردی و بیچاره‌ای جان ببرد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ملک زین حکایت چنان بر شکفت</p></div>
<div class="m2"><p>که چیزش ببخشید و چیزی نگفت</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>وز این جانب افتان و خیزان جوان</p></div>
<div class="m2"><p>همی رفت بیچاره هر سو دوان</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>یکی گفتش از چار سوی قصاص</p></div>
<div class="m2"><p>چه کردی که آمد به جانت خلاص؟</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>به گوشش فرو گفت کای هوشمند</p></div>
<div class="m2"><p>به جانی و دانگی رهیدم ز بند</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>یکی تخم در خاک از آن می‌نهد</p></div>
<div class="m2"><p>که روز فرو ماندگی بر دهد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>جوی باز دارد بلایی درشت</p></div>
<div class="m2"><p>عصایی شنیدی که عوجی بکشت</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>حدیث درست آخر از مصطفاست</p></div>
<div class="m2"><p>که بخشایش و خیر دفع بلاست</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>عدو را نبینی در این بقعه پای</p></div>
<div class="m2"><p>که بوبکر سعد است کشور خدای</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بگیر ای جهانی به روی تو شاد</p></div>
<div class="m2"><p>جهانی، که شادی به روی تو باد</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>کس از کس به دور تو باری نبرد</p></div>
<div class="m2"><p>گلی در چمن جور خاری نبرد</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>تویی سایهٔ لطف حق بر زمین</p></div>
<div class="m2"><p>پیمبر صفت رحمة ‌العالمین</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>تو را قدر اگر کس نداند چه غم؟</p></div>
<div class="m2"><p>شب قدر را می‌ندانند هم</p></div></div>