---
title: >-
    بخش ۲ - گفتار اندر نواخت ضعیفان
---
# بخش ۲ - گفتار اندر نواخت ضعیفان

<div class="b" id="bn1"><div class="m1"><p>پدرمرده را سایه بر سر فکن</p></div>
<div class="m2"><p>غبارش بیفشان و خارش بکن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ندانی چه بودش فرو مانده سخت؟</p></div>
<div class="m2"><p>بود تازه بی بیخ هرگز درخت؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو بینی یتیمی سر افکنده پیش</p></div>
<div class="m2"><p>مده بوسه بر روی فرزند خویش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یتیم ار بگرید که نازش خرد؟</p></div>
<div class="m2"><p>وگر خشم گیرد که بارش برد؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>الا تا نگرید که عرش عظیم</p></div>
<div class="m2"><p>بلرزد همی چون بگرید یتیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به رحمت بکن آبش از دیده پاک</p></div>
<div class="m2"><p>به شفقت بیفشانش از چهره خاک</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اگر سایه خود برفت از سرش</p></div>
<div class="m2"><p>تو در سایه خویشتن پرورش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>من آنگه سر تاجور داشتم</p></div>
<div class="m2"><p>که سر بر کنار پدر داشتم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اگر بر وجودم نشستی مگس</p></div>
<div class="m2"><p>پریشان شدی خاطر چند کس</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کنون دشمنان گر برندم اسیر</p></div>
<div class="m2"><p>نباشد کس از دوستانم نصیر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مرا باشد از درد طفلان خبر</p></div>
<div class="m2"><p>که در طفلی از سر برفتم پدر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>یکی خار پای یتیمی بکند</p></div>
<div class="m2"><p>به خواب اندرش دید صدر خجند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>همی گفت و در روضه‌ها می‌چمید</p></div>
<div class="m2"><p>کز آن خار بر من چه گلها دمید</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>مشو تا توانی ز رحمت بری</p></div>
<div class="m2"><p>که رحمت برندت چو رحمت بری</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چو انعام کردی مشو خودپرست</p></div>
<div class="m2"><p>که من سرورم دیگران زیردست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>اگر تیغ دورانش انداخته‌ست</p></div>
<div class="m2"><p>نه شمشیر دوران هنوز آخته‌ست؟</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چو بینی دعاگوی دولت هزار</p></div>
<div class="m2"><p>خداوند را شکر نعمت گزار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>که چشم از تو دارند مردم بسی</p></div>
<div class="m2"><p>نه تو چشم داری به دست کسی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>«کرم» خوانده‌ام سیرت سروران</p></div>
<div class="m2"><p>غلط گفتم، اخلاق پیغمبران</p></div></div>