---
title: >-
    بخش ۱۴ - حکایت
---
# بخش ۱۴ - حکایت

<div class="b" id="bn1"><div class="m1"><p>یکی سیرت نیکمردان شنو</p></div>
<div class="m2"><p>اگر نیکبختی و مردانه رو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که شبلی ز حانوت گندم فروش</p></div>
<div class="m2"><p>به ده برد انبان گندم به دوش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نگه کرد و موری در آن غله دید</p></div>
<div class="m2"><p>که سرگشته هر گوشه‌ای می‌دوید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز رحمت بر او شب نیارست خفت</p></div>
<div class="m2"><p>به مأوای خود بازش آورد و گفت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مروت نباشد که این مور ریش</p></div>
<div class="m2"><p>پراکنده گردانم از جای خویش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>درون پراکندگان جمع دار</p></div>
<div class="m2"><p>که جمعیتت باشد از روزگار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چه خوش گفت فردوسی پاک زاد</p></div>
<div class="m2"><p>که رحمت بر آن تربت پاک باد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>میازار موری که دانه‌کش است</p></div>
<div class="m2"><p>که جان دارد و جان شیرین خوش است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سیاه اندرون باشد و سنگدل</p></div>
<div class="m2"><p>که خواهد که موری شود تنگدل</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مزن بر سر ناتوان دست زور</p></div>
<div class="m2"><p>که روزی به پایش در افتی چو مور</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>درون فروماندگان شاد کن</p></div>
<div class="m2"><p>ز روز فروماندگی یاد کن</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نبخشود بر حال پروانه شمع</p></div>
<div class="m2"><p>نگه کن که چون سوخت در پیش جمع</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گرفتم ز تو ناتوان تر بسی است</p></div>
<div class="m2"><p>تواناتر از تو هم آخر کسی است</p></div></div>