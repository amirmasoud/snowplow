---
title: >-
    حکایت شمارهٔ ۱۹
---
# حکایت شمارهٔ ۱۹

<div class="n" id="bn1"><p>آورده‌اند که انوشیروان عادل را در شکارگاهی صید کباب کردند و نمک نبود غلامی به روستا رفت تا نمک آرد نوشیروان گفت نمک به قیمت بستان تا رسمی نشود و ده خراب نگردد گفتند از این قدر چه خلل آید گفت بنیاد ظلم در جهان اوّل اندکی بوده است هر که آمد بر او مزیدی کرده تا بدین غایت رسیده</p></div>
<div class="b" id="bn2"><div class="m1"><p>اگر ز باغ رعیت ملک خورد سیبی </p></div>
<div class="m2"><p>بر آورند غلامان او درخت از بیخ</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به پنج بیضه که سلطان ستم روا دارد </p></div>
<div class="m2"><p>زنند لشکریانش هزار مرغ به سیخ</p></div></div>