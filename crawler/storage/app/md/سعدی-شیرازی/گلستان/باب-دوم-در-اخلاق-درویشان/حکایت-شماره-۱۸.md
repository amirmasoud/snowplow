---
title: >-
    حکایت شمارهٔ ۱۸
---
# حکایت شمارهٔ ۱۸

<div class="n" id="bn1"><p>عابدی را پادشاهی طلب کرد.</p></div>
<div class="n" id="bn2"><p>اندیشید که دارویی بخورم تا ضعیف شوم مگر اعتقادی که دارد در حق من زیادت کند.</p></div>
<div class="n" id="bn3"><p>آورده‌اند که داروی قاتل بخورد و بمرد.</p></div>
<div class="b" id="bn4"><div class="m1"><p>آن که چون پسته دیدمش همه مغز </p></div>
<div class="m2"><p>پوست بر پوست بود همچو پیاز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پارسایان روی در مخلوق </p></div>
<div class="m2"><p>پشت بر قبله می‌کنند نماز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون بنده خدای خویش خواند </p></div>
<div class="m2"><p>باید که به جز خدا نداند</p></div></div>