---
title: >-
    حکایت شمارهٔ ۳۴
---
# حکایت شمارهٔ ۳۴

<div class="n" id="bn1"><p>مطابق این سخن پادشاهی را مهمی پیش آمد. گفت اگر این حالت به مراد من بر آید چندین درم دهم زاهدان را.</p></div>
<div class="n" id="bn2"><p>چون حاجتش بر آمد و تشویش خاطرش برفت وفای نذرش به وجود شرط لازم آمد. یکی را از بندگان خاص کیسه درم داد تا صرف کند بر زاهدان.</p></div>
<div class="n" id="bn3"><p>گویند غلامی عاقل هشیار بود. همه روز بگردید و شبانگه باز آمد و درم‌ها بوسه داد و پیش ملک بنهاد و گفت: زاهدان را چندان که گردیدم نیافتم!</p></div>
<div class="n" id="bn4"><p>گفت: این چه حکایت است؟! آنچه من دانم در این ملک چهارصد زاهد است.</p></div>
<div class="n" id="bn5"><p>گفت: ای خداوند جهان! آن که زاهد است نمی‌ستاند و آن که می‌ستاند زاهد نیست.</p></div>
<div class="n" id="bn6"><p>ملک بخندید و ندیمان را گفت: چندان که مرا در حق خداپرستان ارادت است و اقرار، مر این شوخ دیده را عداوت است و انکار و حق به جانب اوست!</p></div>
<div class="b" id="bn7"><div class="m1"><p>زاهد که درم گرفت و دینار</p></div>
<div class="m2"><p>زاهدتر از او یکی به دست آر</p></div></div>