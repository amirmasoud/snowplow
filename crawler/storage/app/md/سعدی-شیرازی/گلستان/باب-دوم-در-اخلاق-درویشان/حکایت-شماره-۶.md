---
title: >-
    حکایت شمارهٔ ۶
---
# حکایت شمارهٔ ۶

<div class="n" id="bn1"><p>زاهدی مهمان پادشاهی بود. چون به طعام بنشستند کمتر از آن خورد که ارادت او بود و چون به نماز برخاستند بیش از آن کرد که عادت او، تا ظنّ صلاحیت در حق او زیادت کنند.</p></div>
<div class="b" id="bn2"><div class="m1"><p>ترسم نرسی به کعبه، ای اعرابی </p></div>
<div class="m2"><p>کاین ره که تو می‌روی به ترکستان است</p></div></div>
<div class="n" id="bn3"><p>چون به مقام خویش آمد سفره خواست تا تناولی کند. پسری صاحب فراست داشت. گفت: ای پدر! باری به مجلس سلطان در طعام نخوردی؟ گفت: در نظر ایشان چیزی نخوردم که به کار آید. گفت: نماز را هم قضا کن که چیزی نکردی که به کار آید.</p></div>
<div class="b" id="bn4"><div class="m1"><p>ای هنرها گرفته بر کف دست</p></div>
<div class="m2"><p> عیبها بر گرفته زیر بغل</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا چه خواهی خریدن ای مغرور</p></div>
<div class="m2"><p>روز درماندگی به سیم دغل</p></div></div>