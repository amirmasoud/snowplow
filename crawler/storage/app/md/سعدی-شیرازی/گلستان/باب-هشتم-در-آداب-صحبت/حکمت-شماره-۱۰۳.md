---
title: >-
    حکمت شمارهٔ ۱۰۳
---
# حکمت شمارهٔ ۱۰۳

<div class="n" id="bn1"><p>نصیحت پادشاهان کردن کسی را مسلم بود که بیم سر ندارد یا امید زر.</p></div>
<div class="b" id="bn2"><div class="m1"><p>موحد چه در پای ریزی زرش</p></div>
<div class="m2"><p>چه شمشیر هندی نهی بر سرش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>امید و هراسش نباشد ز کس</p></div>
<div class="m2"><p>بر این است بنیاد توحید و بس</p></div></div>