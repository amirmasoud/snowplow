---
title: >-
    حکمت شمارهٔ ۴۳
---
# حکمت شمارهٔ ۴۳

<div class="n" id="bn1"><p>هرکه با بزرگان ستیزد، خون خود ریزد.</p></div>
<div class="b" id="bn2"><div class="m1"><p>خویشتن را بزرگ پنداری</p></div>
<div class="m2"><p>راست گفتند یک دو بیند لوچ</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زود بینی شکسته پیشانی</p></div>
<div class="m2"><p>تو که بازی کنی به سر با قوچ</p></div></div>