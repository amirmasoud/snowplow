---
title: >-
    حکمت شمارهٔ ۹۷
---
# حکمت شمارهٔ ۹۷

<div class="n" id="bn1"><p>زر از معدن به کان کندن به در آید وز دست بخیل به جان کندن.</p></div>
<div class="b" id="bn2"><div class="m1"><p>دونان نخورند و گوش دارند</p></div>
<div class="m2"><p>گویند امید به که خورده</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>روزی بینی به کام دشمن</p></div>
<div class="m2"><p>زر مانده و خاکسار مرده</p></div></div>