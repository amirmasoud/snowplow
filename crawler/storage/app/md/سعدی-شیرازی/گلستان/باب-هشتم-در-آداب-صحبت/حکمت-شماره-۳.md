---
title: >-
    حکمت شمارهٔ ۳
---
# حکمت شمارهٔ ۳

<div class="n" id="bn1"><p>دو کس رنج بیهوده بردند و سعی بی فایده کردند: یکی آن که اندوخت و نخورد و دیگر آن که آموخت و نکرد.</p></div>
<div class="b" id="bn2"><div class="m1"><p>علم چندان که بیشتر خوانی</p></div>
<div class="m2"><p>چون عمل در تو نیست نادانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نه محقق بود نه دانشمند</p></div>
<div class="m2"><p>چارپایی بر او کتابی چند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آن تهی مغز را چه علم و خبر</p></div>
<div class="m2"><p>که بر او هیزم است یا دفتر</p></div></div>