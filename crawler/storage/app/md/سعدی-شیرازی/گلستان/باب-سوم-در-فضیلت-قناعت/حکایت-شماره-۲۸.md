---
title: >-
    حکایت شمارهٔ ۲۸
---
# حکایت شمارهٔ ۲۸

<div class="n" id="bn1"><p>درویشی را شنیدم که به غاری در نشسته بود و در به روی از جهانیان بسته و ملوک و اغنیا را در چشم همت او شوکت و هیبت نمانده.</p></div>
<div class="b" id="bn2"><div class="m1"><p>هر که بر خود در سؤال گشاد</p></div>
<div class="m2"><p>تا بمیرد نیازمند بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آز بگذار و پادشاهی کن</p></div>
<div class="m2"><p>گردن بی طمع بلند بود</p></div></div>
<div class="n" id="bn4"><p>یکی از ملوک آن طرف اشارت کرد که توقع به کرم اخلاق مردان چنین است که به نمک با ما موافقت کنند. شیخ رضا داد، به حکم آن که اجابت دعوت سنت است. دیگر روز ملک به عذر قدومش رفت. عابد از جای برجست و در کنارش گرفت و تلطف کرد و ثنا گفت. چو غایب شد، یکی از اصحاب پرسید شیخ را که چندین ملاطفت امروز با پادشه که تو کردی خلاف عادت بود و دیگر ندیدیم، گفت نشنیده‌ای که گفته‌اند:</p></div>
<div class="b" id="bn5"><div class="m1"><p>هر که را بر سماط بنشستی</p></div>
<div class="m2"><p>واجب آمد به خدمتش برخاست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گوش تواند که همه عمر وی</p></div>
<div class="m2"><p>نشنود آواز دف و چنگ و نی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دیده شکیبد ز تماشای باغ</p></div>
<div class="m2"><p>بی گل و نسرین به سر آرد دماغ</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ور نبود بالش آکنده پر</p></div>
<div class="m2"><p>خواب توان کرد خزف زیر سر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ور نبود دلبر همخوابه پیش</p></div>
<div class="m2"><p>دست توان کرد در آغوش خویش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>وین شکم بی هنر پیچ پیچ</p></div>
<div class="m2"><p>صبر ندارد که بسازد به هیچ</p></div></div>