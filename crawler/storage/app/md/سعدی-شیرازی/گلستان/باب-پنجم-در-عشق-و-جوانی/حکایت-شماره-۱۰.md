---
title: >-
    حکایت شمارهٔ ۱۰
---
# حکایت شمارهٔ ۱۰

<div class="n" id="bn1"><p>در عنفوان جوانی چنان که افتد و دانی با شاهدی سری و سرّی داشتم به حکم آن که حلقی داشت طیِّبُ الاَدا وَ خَلقی کالبدرِ اذا بَدا.</p></div>
<div class="b" id="bn2"><div class="m1"><p>آن که نبات عارضش آب حیات می‌خورد</p></div>
<div class="m2"><p>در شکرش نگه کند هر که نبات می‌خورد</p></div></div>
<div class="n" id="bn3"><p>اتفاقاً به خلاف طبع از وی حرکتی بدیدم که نپسندیدم. دامن از او در کشیدم و مهره برچیدم و گفتم:</p></div>
<div class="b" id="bn4"><div class="m1"><p>برو هر چه می‌بایدت پیش گیر</p></div>
<div class="m2"><p>سر ما نداری سر خویش گیر</p></div></div>
<div class="n" id="bn5"><p>شنیدمش که همی‌رفت و می‌گفت:</p></div>
<div class="b" id="bn6"><div class="m1"><p>شب پره گر وصل آفتاب نخواهد</p></div>
<div class="m2"><p>رونق بازار آفتاب نکاهد</p></div></div>
<div class="n" id="bn7"><p>این بگفت و سفر کرد و پریشانی او در من اثر.</p></div>
<div class="b" id="bn8"><div class="m1"><p>فَقَدتُ زَمانَ الوَصلِ و المَرءُ جاهِلٌ</p></div>
<div class="m2"><p>بِقَدرِ لَذیذِ العَیشِ قَبلَ المَصائِبِ</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>باز آی و مرا بکش که پیشت مردن</p></div>
<div class="m2"><p>خوشتر که پس از تو زندگانی کردن</p></div></div>
<div class="n" id="bn10"><p>اما به شکر و منت باری پس از مدتی باز آمد، آن حلق داوودی متغیر شده و جمال یوسفی به زیان آمده و بر سیب زنخدانش چون به گردی نشسته و رونق بازار حسنش شکسته، متوقع که در کنارش گیرم. کناره گرفتم و گفتم:</p></div>
<div class="b" id="bn11"><div class="m1"><p>آن روز که خط شاهدت بود</p></div>
<div class="m2"><p>صاحب نظر از نظر براندی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>امروز بیامدی به صلحش</p></div>
<div class="m2"><p>کش فتحه و ضمه بر نشاندی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>تازه بهارا ورقت زرد شد</p></div>
<div class="m2"><p>دیگ منه کآتش ما سرد شد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چند خرامی و تکبر کنی</p></div>
<div class="m2"><p>دولت پارینه تصور کنی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>پیش کسی رو که طلبکار توست</p></div>
<div class="m2"><p>ناز بر آن کن که خریدار توست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>سبزه در باغ گفته‌اند خوش است</p></div>
<div class="m2"><p>داند آن کس که این سخن گوید</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>یعنی از روی نیکوان خط سبز</p></div>
<div class="m2"><p>دل عشاق بیشتر جوید</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بوستان تو گندنازاریست</p></div>
<div class="m2"><p>بس که بر می‌کنی و می‌روید</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>گر صبر کنی ور نکنی موی بناگوش</p></div>
<div class="m2"><p>این دولت ایام نکویی به سر آید</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گر دست به جان داشتمی همچو تو بر ریش</p></div>
<div class="m2"><p>نگذاشتمی تا به قیامت که بر آید</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>سؤال کردم و گفتم جمال روی تو را</p></div>
<div class="m2"><p>چه شد که مورچه بر گرد ماه جوشیده‌ست</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>جواب داد ندانم چه بود رویم را</p></div>
<div class="m2"><p>مگر به ماتم حسنم سیاه پوشیده‌ست</p></div></div>