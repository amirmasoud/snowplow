---
title: >-
    شمارهٔ ۵
---
# شمارهٔ ۵

<div class="b" id="bn1"><div class="m1"><p>بنگر که یار خاطر ما شاد می‌کند</p></div>
<div class="m2"><p>با غیر هم‌نشین و مرا یاد می‌کند</p></div></div>