---
title: >-
    شمارهٔ ۱۹
---
# شمارهٔ ۱۹

<div class="b" id="bn1"><div class="m1"><p>دردا که بت ستیزه کاری دارم</p></div>
<div class="m2"><p>وز غمزه او جان فگاری دارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مردم گویند روزگارت چونست</p></div>
<div class="m2"><p>می پندارند روزگاری دارم</p></div></div>