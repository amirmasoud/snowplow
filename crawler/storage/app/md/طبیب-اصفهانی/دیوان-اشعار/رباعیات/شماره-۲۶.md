---
title: >-
    شمارهٔ ۲۶
---
# شمارهٔ ۲۶

<div class="b" id="bn1"><div class="m1"><p>ای آن که به غیر از تو مرا یاری نه</p></div>
<div class="m2"><p>جز یاد توام مونس و غمخواری نه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شبهای دراز هجر دور از رخ تو</p></div>
<div class="m2"><p>چون شمع به جز گریه مرا کاری نه</p></div></div>