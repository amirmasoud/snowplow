---
title: >-
    شمارهٔ ۱۶
---
# شمارهٔ ۱۶

<div class="b" id="bn1"><div class="m1"><p>عمریست که از دلم جنون می جوشد</p></div>
<div class="m2"><p>از دیده ام اشگ لاله گون می جوشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در وادی عشق چشم گریان منست</p></div>
<div class="m2"><p>آن چشمه کزو همیشه خون می جوشد</p></div></div>