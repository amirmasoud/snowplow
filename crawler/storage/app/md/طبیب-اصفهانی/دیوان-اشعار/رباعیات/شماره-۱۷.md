---
title: >-
    شمارهٔ ۱۷
---
# شمارهٔ ۱۷

<div class="b" id="bn1"><div class="m1"><p>ای آن که چو بگذری تو بر یاد دلم</p></div>
<div class="m2"><p>جز گریه نگیرد زغمت داد دلم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا از تو جدا فتادم ای وای بمن</p></div>
<div class="m2"><p>یادت نرسد اگر به فریاد دلم</p></div></div>