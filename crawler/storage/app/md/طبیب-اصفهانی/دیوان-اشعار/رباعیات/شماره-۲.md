---
title: >-
    شمارهٔ ۲
---
# شمارهٔ ۲

<div class="b" id="bn1"><div class="m1"><p>افگار توام تاب و توانی بفرست</p></div>
<div class="m2"><p>بیمار غمم قوت جانی بفرست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از نامه خشکی بتو راضیست طبیب</p></div>
<div class="m2"><p>گر نیست گلی برگ خزانی بفرست</p></div></div>