---
title: >-
    شمارهٔ ۲۸
---
# شمارهٔ ۲۸

<div class="b" id="bn1"><div class="m1"><p>در محفل غیر باده چون نوش کنی</p></div>
<div class="m2"><p>درباره من حرف کسان گوش کنی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون عهد وفای خویش ترسم که مرا</p></div>
<div class="m2"><p>یک باره ز خاطرت فراموش کنی</p></div></div>