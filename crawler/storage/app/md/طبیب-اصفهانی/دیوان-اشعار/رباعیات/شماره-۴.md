---
title: >-
    شمارهٔ ۴
---
# شمارهٔ ۴

<div class="b" id="bn1"><div class="m1"><p>ای آنکه همیشه جور کارت باشد</p></div>
<div class="m2"><p>آوردن عاشقان شعارت باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این دیده که بی روی تو خون می گرید</p></div>
<div class="m2"><p>تا چند براه انتظارت باشد</p></div></div>