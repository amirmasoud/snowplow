---
title: >-
    شمارهٔ ۲۱
---
# شمارهٔ ۲۱

<div class="b" id="bn1"><div class="m1"><p>کی پیش کسی حکایتی از تو کنم</p></div>
<div class="m2"><p>یا شکوه بی نهایتی از تو کنم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غیر و من و شکوه از تو شرمم بادا</p></div>
<div class="m2"><p>هم با تو مگر شکایتی از تو کنم</p></div></div>