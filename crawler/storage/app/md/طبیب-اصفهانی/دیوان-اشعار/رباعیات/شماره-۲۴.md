---
title: >-
    شمارهٔ ۲۴
---
# شمارهٔ ۲۴

<div class="b" id="bn1"><div class="m1"><p>نه ذوق من از وصل نگاری دارم</p></div>
<div class="m2"><p>نه شوق گل و سیر بهاری دارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کافیست مرا همین که در کنج غمی</p></div>
<div class="m2"><p>بنشسته خیال چون تو یاری دارم</p></div></div>