---
title: >-
    شمارهٔ ۱
---
# شمارهٔ ۱

<div class="b" id="bn1"><div class="m1"><p>این دیده به راه انتظاریست مرا</p></div>
<div class="m2"><p>وین گوش به گفتگوی یاریست مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دیگر سوی که بینم و از که شنوم</p></div>
<div class="m2"><p>این هر دو چو از برای کاریست مرا</p></div></div>