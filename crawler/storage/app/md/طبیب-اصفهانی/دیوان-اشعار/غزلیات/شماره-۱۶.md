---
title: >-
    شمارهٔ ۱۶
---
# شمارهٔ ۱۶

<div class="b" id="bn1"><div class="m1"><p>دل غمدیده به دنبال کسی افتادست</p></div>
<div class="m2"><p>دادخواهی ز پی دادرسی افتادست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از من خسته خدا را به به تغافل مگذار</p></div>
<div class="m2"><p>که مرا کار به آخر نفسی افتادست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حسرت مرغ اسیری کشدم کز دامی</p></div>
<div class="m2"><p>کرده پرواز و به کنج قفسی افتادست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رفته هوشم ز سر و صبر ز دل، از تو مرا</p></div>
<div class="m2"><p>تا به سر شوری و در دل هوسی افتادست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یار صد حیف که هم‌صحبت غیر است طبیب</p></div>
<div class="m2"><p>گلی افسوس در آغوش خسی افتادست</p></div></div>