---
title: >-
    شمارهٔ ۱۵۳
---
# شمارهٔ ۱۵۳

<div class="b" id="bn1"><div class="m1"><p>ببخشا ای که میر کاروانی</p></div>
<div class="m2"><p>به واپس مانده‌ای بر ره روانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>درین گلشن من آن مرغ غریبم</p></div>
<div class="m2"><p>که بر شاخی ندارم آشیانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فغان نو به دام افتاده صیدی‌ست</p></div>
<div class="m2"><p>به گوشت گر رسد امشب فغانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تو و ای فاخته سروت که ما را</p></div>
<div class="m2"><p>بود بس جلوه سر و روانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جبین طاعتم بنگر که فرسود</p></div>
<div class="m2"><p>ز بس سودم به خاک آستانی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پشیمان گردی از بیداد چون خاست</p></div>
<div class="m2"><p>ز دل آهی، و تیری از کمانی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>طبیب خسته وقتش خوش کز او ماند</p></div>
<div class="m2"><p>ز حرف عشق هرسو داستانی</p></div></div>