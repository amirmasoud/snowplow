---
title: >-
    شمارهٔ ۱۲۵
---
# شمارهٔ ۱۲۵

<div class="b" id="bn1"><div class="m1"><p>به این خوشم که ز دردت به دیده خواب ندارم</p></div>
<div class="m2"><p>ولی دریغ که دردم فزون و تاب ندارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رسیده ضعف بجائی مرا که از من خسته</p></div>
<div class="m2"><p>تو حال پرسی و من طاقت جواب ندارم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نیم غمین که فتادم ز پا غمم همه اینست</p></div>
<div class="m2"><p>که می روی تو و من قوت شتاب ندارم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زپا فتادگی خود طبیب ازین همه نالم</p></div>
<div class="m2"><p>که رفت محمل و من پای در رکاب ندارم</p></div></div>