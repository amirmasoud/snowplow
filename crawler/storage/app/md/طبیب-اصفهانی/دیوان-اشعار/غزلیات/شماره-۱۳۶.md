---
title: >-
    شمارهٔ ۱۳۶
---
# شمارهٔ ۱۳۶

<div class="b" id="bn1"><div class="m1"><p>جانا در انتظار تو شد روزگار من</p></div>
<div class="m2"><p>و آن انتظار هیچ نیاید بکار من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بهر تسلیم بود این بس که آورد</p></div>
<div class="m2"><p>گاهی مرا به یاد، فراموشکار من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شاید مرا بیاد تو آرد درین چمن</p></div>
<div class="m2"><p>مشت پری که مانده بجا یادگار من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>فرخنده طایری ز ریاض محبتم</p></div>
<div class="m2"><p>بر خویشتن ببال که کردی شکار من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از ساغر نشاط مده باده ام که نیست</p></div>
<div class="m2"><p>جز خون دل طبیب می خوشگوار من</p></div></div>