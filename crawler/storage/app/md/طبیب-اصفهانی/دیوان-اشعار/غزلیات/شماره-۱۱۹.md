---
title: >-
    شمارهٔ ۱۱۹
---
# شمارهٔ ۱۱۹

<div class="b" id="bn1"><div class="m1"><p>چه خونها در دل ایام کردیم</p></div>
<div class="m2"><p>که صبحی را بمستی شام کردیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چه می بود آن، که تا در جام کردیم</p></div>
<div class="m2"><p>وداع ننگ و ترک نام کردیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مسلمانان درین مدت چرا گوش</p></div>
<div class="m2"><p>بحرف زاهد خود کام کردیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شکایت نیست ما را هیچ از غیر</p></div>
<div class="m2"><p>که ما خود خویش را بدنام کردیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هزاران شکر کز دلهای غمناک</p></div>
<div class="m2"><p>غمی در یوزه دردی وام کردیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بمرغان اسیر از ما بشارت</p></div>
<div class="m2"><p>که طرح آشیان در دام کردیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از آن از دیده خوبان فتادیم</p></div>
<div class="m2"><p>که در پاس وفا ابرام کردیم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>طبیب از ما که می‌گوید به مستان</p></div>
<div class="m2"><p>که ما عهد نوی با جام کردیم</p></div></div>