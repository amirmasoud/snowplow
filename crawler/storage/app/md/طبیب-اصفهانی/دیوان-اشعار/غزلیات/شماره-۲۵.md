---
title: >-
    شمارهٔ ۲۵
---
# شمارهٔ ۲۵

<div class="b" id="bn1"><div class="m1"><p>سینه گرم و مژه خونبار و سحر نزدیک است</p></div>
<div class="m2"><p>باخبر باش که آهم به اثر نزدیک است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به رفیقان وطن کیست که از ما گوید</p></div>
<div class="m2"><p>که به ساحل نرسیدیم و خطر نزدیک است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>منم آن باغ که دارد به کمین صد آفت</p></div>
<div class="m2"><p>زین چه حاصل که نهالم به ثمر نزدیک است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بطلب کوش که تا منزل مقصود طبیب</p></div>
<div class="m2"><p>راه دور است ولی پیش نظر نزدیک است</p></div></div>