---
title: >-
    شمارهٔ ۱۰۴
---
# شمارهٔ ۱۰۴

<div class="b" id="bn1"><div class="m1"><p>در دل اگر باشدم غیر وصال توکام</p></div>
<div class="m2"><p>هجر تو بر من حلال وصل تو بر من حرام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مرغ دلم اوفتاد از غم عشقت به بند</p></div>
<div class="m2"><p>آب نداند چه و داند نداند کدام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>قلم اگر رای تست سرفکنم خود ز تیغ</p></div>
<div class="m2"><p>صیدم اگر کام تست پای نهم خود بدام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جان و دلش آورم تحفه و باشم خجل</p></div>
<div class="m2"><p>قاصد فرخنده چون از توام آرد پیام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون تو در آیی بحرف طوطی خوش لهجه کیست</p></div>
<div class="m2"><p>چون تو گزاری سخن مرغ سخنگو کدام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون نکنی بسملم ایکه کشی بی دریغ</p></div>
<div class="m2"><p>فی المثل افتد ترا صید حرم گر بدام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در سر پرشور من در دل رنجور من</p></div>
<div class="m2"><p>عشق تو دارد محل شوق تو دارد مقام</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گشته طبیب حزین از می عشق تو مست</p></div>
<div class="m2"><p>جام الستش پرست تا که گرفتست جام</p></div></div>