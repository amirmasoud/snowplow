---
title: >-
    شمارهٔ ۹۵
---
# شمارهٔ ۹۵

<div class="b" id="bn1"><div class="m1"><p>چون ناله ز جور تو ستمگر نکند کس؟</p></div>
<div class="m2"><p>هرچند کند ناله و باور نکند کس</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن جامه که از خون جگر تر نکند کس</p></div>
<div class="m2"><p>در کوی تو شرطست که در بر نکند کس</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خم در قدحم ریز که در میکده عشق</p></div>
<div class="m2"><p>آن به که می از شیشه به ساغر نکند کس</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر سوخت دلت عشق به آتشکده بشتاب</p></div>
<div class="m2"><p>کاین سوختگی چاره به کوثر نکند کس</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بنمای به من رخ که دم بازپسین است</p></div>
<div class="m2"><p>حیفست که نظاره دیگر نکند کس</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آسوده چنانند شهیدان تو در خاک</p></div>
<div class="m2"><p>ترسم که شود محشر و سر بر نکند کس</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در انجمن ما که سرایش همه خونست</p></div>
<div class="m2"><p>ظلمست که از باده لبی تر نکند کس</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مائیم و طبیب و در میخانه که آنجا</p></div>
<div class="m2"><p>اندیشه‌ای از گردش اختر نکند کس</p></div></div>