---
title: >-
    شمارهٔ ۱۱۳
---
# شمارهٔ ۱۱۳

<div class="b" id="bn1"><div class="m1"><p>به صد بلا ز غمت گرچه مبتلا شده‌ام</p></div>
<div class="m2"><p>هزار شکر که با درد آشنا شده‌ام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز خار ما همه گل می‌دمد به دامن دشت</p></div>
<div class="m2"><p>به جستجوی تو تا من برهنه‌پا شده‌ام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جدا ز گلشن کویت طبیب از حسرت</p></div>
<div class="m2"><p>به سان بلبل تصویر بی‌نوا شده‌ام</p></div></div>