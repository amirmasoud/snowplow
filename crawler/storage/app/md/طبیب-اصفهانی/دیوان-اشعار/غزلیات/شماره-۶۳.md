---
title: >-
    شمارهٔ ۶۳
---
# شمارهٔ ۶۳

<div class="b" id="bn1"><div class="m1"><p>جدا از روی تو چشمم چو خونفشان گردد</p></div>
<div class="m2"><p>ز خون دل مژه ام شاخ ارغوان گردد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گرفته ام بغمش الفتی و می ترسم</p></div>
<div class="m2"><p>خدا نکرده بمن یار مهربان گردد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زشادمانی وصل تواش نصیب مباد</p></div>
<div class="m2"><p>دل فگار اگر بی تو شادمان گردد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>قفس شکسته و از هم گسسته دام کسی</p></div>
<div class="m2"><p>ز آشیان به چه امید سرگران گردد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مباش غافل از افتادگی جاده طبیب</p></div>
<div class="m2"><p>زخاکساری خود خضر کاروان گردد</p></div></div>