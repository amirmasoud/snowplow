---
title: >-
    شمارهٔ ۹۴
---
# شمارهٔ ۹۴

<div class="b" id="bn1"><div class="m1"><p>هر چند بر آن عارض گلگون نگرد کس</p></div>
<div class="m2"><p>دل میکشدش باز که افزون نکرد کس</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کو طاقت نظاره بزمی که بود یار</p></div>
<div class="m2"><p>همصبحت اغیار وزبیرون نگرد کس</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خسرو نگران بر رخ شیرین وزغیرت</p></div>
<div class="m2"><p>فرهاد نخواهد که به گلگون نگرد کس</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زین پیش چه حاصل که بحالم نظرت بود</p></div>
<div class="m2"><p>باید که باین خسته دل اکنون نگرد کس</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هیهات که بر سر و روان دیده گشاید</p></div>
<div class="m2"><p>در جلوه گر آن قامت موزون نگرد کس</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از حسرت دیدار تو گرجان بسپارد</p></div>
<div class="m2"><p>بر روی تو از شرم دگر چون نگرد کس</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آنجا که سخن می رود از سر محبت</p></div>
<div class="m2"><p>تا چند ترا گوش بافسون نگرد کس</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>داغ دلش از آه جگر سوز طبیبست</p></div>
<div class="m2"><p>هر لاله که بر دامن هامون نگرد کس</p></div></div>