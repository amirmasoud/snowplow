---
title: >-
    شمارهٔ ۶
---
# شمارهٔ ۶

<div class="b" id="bn1"><div class="m1"><p>از هجر بت یگانه ما</p></div>
<div class="m2"><p>خون می چکد از ترانه ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر خاست ز آسیای افلاک</p></div>
<div class="m2"><p>افغان زشکست دانه ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>افتاده زمین خراب و بیخود</p></div>
<div class="m2"><p>از ناله عاشقانه ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در بحر وجود همچو گوهر</p></div>
<div class="m2"><p>با خود بود آب و دانه ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>وارسته ز خود شدیم و گردید</p></div>
<div class="m2"><p>اوج فلک آشیانه ما</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>صد طعنه زند به لاله و گل</p></div>
<div class="m2"><p>خار و خس آشیانه ما</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از دولت عشق پادشاهیم</p></div>
<div class="m2"><p>غم لشکر و دل خزانه ما</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خارای غم و حریر محنت</p></div>
<div class="m2"><p>فرشست در آستانه ما</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بر خویش طبیب پیچد افلاک</p></div>
<div class="m2"><p>از خنده بی‌خودانه ما</p></div></div>