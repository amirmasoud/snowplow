---
title: >-
    شمارهٔ ۱۵۵
---
# شمارهٔ ۱۵۵

<div class="b" id="bn1"><div class="m1"><p>چو باشد مایل بیداد شاهی</p></div>
<div class="m2"><p>چه خیزد از فغان دادخواهی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ببخشا بر تهیدستان خدا را</p></div>
<div class="m2"><p>بشکر آنکه داری دستگاهی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شبست و وادی و گمکرده راهم</p></div>
<div class="m2"><p>مگر آید زغیبم خضر راهی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زخیل آن سگانم کو ندارد</p></div>
<div class="m2"><p>بغیر از آستان تو پناهی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عجب دارم که چون میرم باین سوز</p></div>
<div class="m2"><p>گلی روید زخاکم یا گیاهی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در آن ملکی که شاهی داورش نیست</p></div>
<div class="m2"><p>مبارک ملکی و فرخنده شاهی!</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>طبیب خسته را بی‌جا مرنجان</p></div>
<div class="m2"><p>حذر می‌کن ز آه بی گناهی</p></div></div>