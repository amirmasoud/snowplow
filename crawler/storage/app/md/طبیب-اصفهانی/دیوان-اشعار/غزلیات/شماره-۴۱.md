---
title: >-
    شمارهٔ ۴۱
---
# شمارهٔ ۴۱

<div class="b" id="bn1"><div class="m1"><p>به گلشنی که ز رویت نقاب می‌افتد</p></div>
<div class="m2"><p>ز چشم شبنم او آفتاب می‌افتد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به حشر دیدهٔ بی‌اشک را بهایی نیست</p></div>
<div class="m2"><p>گهر ز قدر فتد چون ز آب می‌افتد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فریب وعده‌ات آبی نزد بر آتش دل</p></div>
<div class="m2"><p>چو تشنه‌ای که به دام سراب می‌افتد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به غیر گلشن کویت طبیب راه بخست</p></div>
<div class="m2"><p>اگر بخلد رود در عذاب می‌افتد</p></div></div>