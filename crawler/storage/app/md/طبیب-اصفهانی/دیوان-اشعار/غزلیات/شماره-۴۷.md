---
title: >-
    شمارهٔ ۴۷
---
# شمارهٔ ۴۷

<div class="b" id="bn1"><div class="m1"><p>عاشقان را نگر از خاره تنی ساخته اند</p></div>
<div class="m2"><p>که به بیداد چو تو دلشکنی ساخته اند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بحذر باش درین بزم که جادو نگهان</p></div>
<div class="m2"><p>کار ما مژه برهمزدنی ساخته اند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>غافلند از گل رخسار تو ای رشگ چمن</p></div>
<div class="m2"><p>بلبلانی که به خار چمنی ساخته اند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خورم افسوس باین مرده دلانی که ز کف</p></div>
<div class="m2"><p>داده جان را و بفرسوده تنی ساخته اند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آهوانی که درین صید گهند از هرگام</p></div>
<div class="m2"><p>بخدنگ چو تو ناوک فکنی ساخته اند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>محفل عشق کجا و دل غمناک طبیب</p></div>
<div class="m2"><p>ای خوش آنان که به بیت الحزنی ساخته اند</p></div></div>