---
title: >-
    شمارهٔ ۱۴۶
---
# شمارهٔ ۱۴۶

<div class="b" id="bn1"><div class="m1"><p>ای که بر خاک شهیدان گذر انداخته‌ای</p></div>
<div class="m2"><p>قتل ما را چه به وقت دگر انداخته‌ای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کشته ناز تواند این همه خونین‌کفنان</p></div>
<div class="m2"><p>که درین بادیه بر یکدگر انداخته‌ای</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بلعجب این که به غیری که هواخواه تو نیست</p></div>
<div class="m2"><p>نگرانی و مرا از نظر انداخته‌ای</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>صید بی بال و پری را ز چمن دور مدار</p></div>
<div class="m2"><p>تا به کی دورم ازین خاک درانداخته‌ای</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در ره عشق از آن یار خبر نیست طبیب</p></div>
<div class="m2"><p>که درین مرحله از پای درانداخته‌ای</p></div></div>