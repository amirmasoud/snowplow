---
title: >-
    شمارهٔ ۷۳
---
# شمارهٔ ۷۳

<div class="b" id="bn1"><div class="m1"><p>تا قیامت دمد از خاک من خون آلود</p></div>
<div class="m2"><p>لاله از سینه چاک و کفن خون آلود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صبح از جامه رنگین شفق مستغنیست</p></div>
<div class="m2"><p>پیر کنعان چه کند پیرهن خون آلود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>می توان یافت که در پای دلش خاری هست</p></div>
<div class="m2"><p>گل کند از لب هر کس سخن خون آلود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گرچه خون می خورد از رشگ رخت گل به چمن</p></div>
<div class="m2"><p>می کند وصف ترا با دهن خون آلود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اشگ خونین همه از دیده حیران ریزد</p></div>
<div class="m2"><p>گر در آئینه فتد عکس من خون آلود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خون خجلت نشود شسته بخون چند طبیب</p></div>
<div class="m2"><p>شوئی از دیده خونبار تن خون آلود</p></div></div>