---
title: >-
    شمارهٔ ۱۲۰
---
# شمارهٔ ۱۲۰

<div class="b" id="bn1"><div class="m1"><p>آنکه پیوسته به رویت نگرانست، منم</p></div>
<div class="m2"><p>وانکه حیران تو بیش از دگرانست منم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آنکه از کوی تو ای خانه برانداز امید</p></div>
<div class="m2"><p>بسته رخت سفر و دلنگرانست منم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نقد جان می دهم و جنس وفا می طلبم</p></div>
<div class="m2"><p>آن خریدار متاعی که گرانست منم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>میگساران همه از جای، سبک برجستند</p></div>
<div class="m2"><p>آن سیه بخت که در خواب گرانست منم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عاشقان تو همه نام و نشانی دارند</p></div>
<div class="m2"><p>آنکه در کوی تو بینام و نشانست منم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پایه قرب مرا بین که بخلوتگه یار</p></div>
<div class="m2"><p>آنکه او محرم هر راز نهانست منم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>با تو پیمان وفا غیر بسی بست و شکست</p></div>
<div class="m2"><p>آنکه در عهده وفای تو همانست منم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>رهرو عشق بسی هست طبیبا، لیکن</p></div>
<div class="m2"><p>آنکه در مرحله از گرم روانست، منم</p></div></div>