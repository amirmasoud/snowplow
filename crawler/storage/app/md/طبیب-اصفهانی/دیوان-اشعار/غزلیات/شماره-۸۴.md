---
title: >-
    شمارهٔ ۸۴
---
# شمارهٔ ۸۴

<div class="b" id="bn1"><div class="m1"><p>می‌رود از خویش دل چون دیده حیران می‌شود</p></div>
<div class="m2"><p>ای خوش آن عاشق که محو روی جانان می‌شود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دور از انصافست کز بهر دعا برداشتن</p></div>
<div class="m2"><p>آشنا دستی که با چاک گریبان می‌شود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از شکفتن می‌رود بر باد گل‌های چمن</p></div>
<div class="m2"><p>گریه می‌آید مرا بر هرکه خندان می‌شود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در جهان بخت سیه روشندلان را لازمست</p></div>
<div class="m2"><p>تیره چون گردید شب اختر نمایان می‌شود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ابر می‌گردد سفید از ریزش باران طبیب</p></div>
<div class="m2"><p>خانه دل با صفا از چشم گریان می‌شود</p></div></div>