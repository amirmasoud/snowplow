---
title: >-
    شمارهٔ ۵۴
---
# شمارهٔ ۵۴

<div class="b" id="bn1"><div class="m1"><p>مرا بتیست که دلها ازین ستم شکند</p></div>
<div class="m2"><p>که عهد بندد و بی موجبی بهم شکند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>براه عشق توام کاش هر کجا خاری است</p></div>
<div class="m2"><p>گهی بدیده خلد گاه بر قدم شکند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فتاده ام چو بدامت خدای را صیاد</p></div>
<div class="m2"><p>روامدار که بال و پرم بهم شکند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بساغر دل پرخون ما چه خواهد کرد</p></div>
<div class="m2"><p>کسی که جام جمش گردهی بهم شکند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ببزم خاص مخوان غیر را که می ترسم</p></div>
<div class="m2"><p>از آن ستم دل خاصان محترم شکند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چمن نگر که زمانش نمیکشد تا شام</p></div>
<div class="m2"><p>گلی که طرف کله را به صبحدم شکند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>وصال گاه بگاهم بدل چه خواهد کرد</p></div>
<div class="m2"><p>چنین که از ستم هجر دمبدم شکند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>طبیب جز دل افسرده ات که پرخون است</p></div>
<div class="m2"><p>کسی ندیده سفالی که جام جم شکند</p></div></div>