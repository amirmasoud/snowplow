---
title: >-
    شمارهٔ ۱۵۰
---
# شمارهٔ ۱۵۰

<div class="b" id="bn1"><div class="m1"><p>از می لعل بکف تا دو سه جامی داری</p></div>
<div class="m2"><p>نوش کن نوش که خوش عیش مدامی داری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بنده ای همچو منت نیست بهیچم مفروش</p></div>
<div class="m2"><p>خبرت نیست که ارزنده غلامی داری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>می روم هر نفس از خود، من ای باد صبا</p></div>
<div class="m2"><p>می توان یافت که از دوست پیامی داری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کی ز قید تو توان رست که در صید گهت</p></div>
<div class="m2"><p>هر طرف می نگرم دانه و دامی داری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نیست اکنون چو ترا قوت رفتار طبیب</p></div>
<div class="m2"><p>زین چه حاصل که بکویش دو سه گامی داری</p></div></div>