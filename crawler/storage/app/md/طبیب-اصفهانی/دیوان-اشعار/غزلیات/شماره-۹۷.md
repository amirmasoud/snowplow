---
title: >-
    شمارهٔ ۹۷
---
# شمارهٔ ۹۷

<div class="b" id="bn1"><div class="m1"><p>چه دامست این که هر مرغی که می‌گردد گرفتارش</p></div>
<div class="m2"><p>نمی‌آید به خاطر پر گشودن‌های گلزارش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عجب نبود ز خاکش تا قیامت بوی خون آید</p></div>
<div class="m2"><p>بیابانی که آب از دیده من می‌خورد خارش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ندارد آگهی از محنت شب‌های مهجوران</p></div>
<div class="m2"><p>کسی کو شب به راحت خفته باشد در بر یارش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مگیر از ساقی دوران قدح گر زندگی خواهی</p></div>
<div class="m2"><p>که از زهر جفا لبریز باشد جام سرشارش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>درین بستان بود طبع من آن طوطی که می‌ریزد</p></div>
<div class="m2"><p>به جای شهد زهر و جای شکر خون منقارش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>طبیب از دولت وصل تو کامش کی شود حاصل</p></div>
<div class="m2"><p>همان بهتر که باشد با غم هجری سرو کارش</p></div></div>