---
title: >-
    شمارهٔ ۱۶۳
---
# شمارهٔ ۱۶۳

<div class="b" id="bn1"><div class="m1"><p>مشکل که دهد دست مرا با تو وصالی</p></div>
<div class="m2"><p>تو نخل برومندی ومن خشگ نهالی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ما را که بجز دست تهی نیست بضاعت</p></div>
<div class="m2"><p>اندیشه وصل تو؟ تمنای محالی!</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آن نیست که پیوسته کنم وصل تمنا</p></div>
<div class="m2"><p>گر ماه بماهی بود و سال بسالی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جانکاه تر از هجر تو نومیدی وصلست</p></div>
<div class="m2"><p>ای کاش که میداشتم امید وصالی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از جلوه شوخی دهدم یاد و خروشم</p></div>
<div class="m2"><p>بینم چو درین دشت خرامنده غزالی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کامی که مرا از دو جهانست سه چیزست</p></div>
<div class="m2"><p>کنجی و حریفی دو سه و صحبت حالی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از خامه فشانی طبیب اینهمه گوهر!</p></div>
<div class="m2"><p>این کلک گهربار مبیناد زوالی</p></div></div>