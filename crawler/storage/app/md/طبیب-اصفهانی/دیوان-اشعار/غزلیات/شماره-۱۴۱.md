---
title: >-
    شمارهٔ ۱۴۱
---
# شمارهٔ ۱۴۱

<div class="b" id="bn1"><div class="m1"><p>دارم به چمن چه کار، بی تو</p></div>
<div class="m2"><p>نشناسم گل زخار، بی تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فریاد که خوش فرو گرفته</p></div>
<div class="m2"><p>ما را غم روزگار بی تو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یعقوب صفت جهان روشن</p></div>
<div class="m2"><p>در چشم منست تار بی تو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون دیده روز نیست چشمم</p></div>
<div class="m2"><p>وقف ره انتظار بی تو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون شمع سر مزار، گیرم</p></div>
<div class="m2"><p>از بزم طرب کنار بی تو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دارد بلبل هزار افسوس</p></div>
<div class="m2"><p>در هر سر شاخسار بی تو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون نقش قدم طبیب از ضعف</p></div>
<div class="m2"><p>افتاده به رهگذار بی تو</p></div></div>