---
title: >-
    شمارهٔ ۵۸
---
# شمارهٔ ۵۸

<div class="b" id="bn1"><div class="m1"><p>از دیده ام فکندی وهنگام آن نبود</p></div>
<div class="m2"><p>کردی جدائی از من و شرط آنچنان نبود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ما را شبی بکوی تو ماندن گمان نبود</p></div>
<div class="m2"><p>چندان گمان بحوصله آسمان نبود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کشتی نهانم و بتو ترسم گمان برند</p></div>
<div class="m2"><p>بر دامن تو کاش زخونم نشان نبود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خوابت ربوده بود خیال کسی؟ که دوش</p></div>
<div class="m2"><p>می گفتمت فسانه و گوشت بر آن نبود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کم کن جدا که دوش بمحفل ز خوی تو</p></div>
<div class="m2"><p>بس شکوه ها که بود مرا و زبان نبود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در دم نهفته بود دریغا ببزم وصل</p></div>
<div class="m2"><p>کاین بر زبان هیچکسم ترجمان نبود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اشکم بدیده سوخت دریغا زتاب دل</p></div>
<div class="m2"><p>ای کاش رهزنی پی این کاروان نبود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شد قسمتم طبیب چو وصلش، اجل رسید</p></div>
<div class="m2"><p>صد حیف زندگانی ما جاودان نبود</p></div></div>