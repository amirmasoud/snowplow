---
title: >-
    شمارهٔ ۶۲
---
# شمارهٔ ۶۲

<div class="b" id="bn1"><div class="m1"><p>کجا کسی غم شبهای تار من دارد</p></div>
<div class="m2"><p>بحز وفا، که سری در کنار من دارد؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>باین خوشم که تو را شرمسار من سازد</p></div>
<div class="m2"><p>تحملی که دل برد بار من دارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سزای دوستیم بین که هر کجا ستمی است</p></div>
<div class="m2"><p>ذخیره از پی جان فگار من دارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گذشت یار زمن سرگران و دانستم</p></div>
<div class="m2"><p>که کار با من و با روزگار من دارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چرا خموش نباشم چو بشنوی از من</p></div>
<div class="m2"><p>شکایتی که دل بیقرار من دارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مبادا از تو خیالم بجز خیالت اگر</p></div>
<div class="m2"><p>گذار در دل امیدوار من دارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زدود آه دل من طبیب معلومست</p></div>
<div class="m2"><p>که آتشی بکمین خار خار من دارد</p></div></div>