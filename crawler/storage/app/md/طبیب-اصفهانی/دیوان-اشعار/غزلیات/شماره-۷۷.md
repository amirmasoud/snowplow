---
title: >-
    شمارهٔ ۷۷
---
# شمارهٔ ۷۷

<div class="b" id="bn1"><div class="m1"><p>نه همین زآتش عشقت دل ما می‌سوزد</p></div>
<div class="m2"><p>هرکه را هست دلی، سوخته یا می‌سوزد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خاک این بادیه بین کز قدم گرم‌روان</p></div>
<div class="m2"><p>بس که گرم است در او پای صبا می‌سوزد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آتش ناله ما بس که جهان را افروخت</p></div>
<div class="m2"><p>هرکه را می‌نگری زآتش ما می‌سوزد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>محفل امشب ز فروغ رخ ساقی گرم است</p></div>
<div class="m2"><p>گل جدا، باده جدا، شمع جدا می‌سوزد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خضر اگر غوطه به سرچشمه حیوان دهدم</p></div>
<div class="m2"><p>بس که دل‌سوخته‌ام آب بقا می‌سوزد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سایه داغ جنون تا به سرم افتادست</p></div>
<div class="m2"><p>گر کند سایه به من بال هما می‌سوزد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گشته با غیر چرا گرم سخن یار، طبیب</p></div>
<div class="m2"><p>گرنه از آتش می شرم و حیا می‌سوزد</p></div></div>