---
title: >-
    شمارهٔ ۴۹
---
# شمارهٔ ۴۹

<div class="b" id="bn1"><div class="m1"><p>روزی که دور از برم آن خوشخرام شد</p></div>
<div class="m2"><p>من بودم و تحملی اما تمام شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اینست اگر فراغت آزادگان باغ</p></div>
<div class="m2"><p>آسوده طایری که گرفتار دام شد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زین باده ای که گشته ازو خون غم حلال</p></div>
<div class="m2"><p>خوش نوش ای حریف که بر ما حرام شد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آگاه ازین نگشت که در بندگی فتاد</p></div>
<div class="m2"><p>محمود و درگمان که ایازش غلام شد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سود وفا، زیان جفا، گفتمش طبیب</p></div>
<div class="m2"><p>آگه نیم ازین دو پسندش کدام شد</p></div></div>