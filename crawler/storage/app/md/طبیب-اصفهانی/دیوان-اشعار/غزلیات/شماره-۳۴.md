---
title: >-
    شمارهٔ ۳۴
---
# شمارهٔ ۳۴

<div class="b" id="bn1"><div class="m1"><p>چون رحمتت فزاید بر عذرخواهی ای دوست</p></div>
<div class="m2"><p>عذر گناه خواهم با بی‌گناهی ای دوست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خواندی در آستانت روزی گدای خویشم</p></div>
<div class="m2"><p>زان روز ننگم آید از پادشاهی ای دوست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دریا همه گرفتم ساحل شودچه حاصل</p></div>
<div class="m2"><p>اکنون که گشت ما را کشتی تباهی ای دوست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تابنده اختری کو، کز پرتوی درآرد</p></div>
<div class="m2"><p>تاریک شام ما را از این سیاهی ای دوست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>وقتست کز تو خواهم داد غرور حسنت</p></div>
<div class="m2"><p>ترسم که مانع آید از دادخواهی ای دوست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بی‌تو طبیب خسته از بس فغان و زاری</p></div>
<div class="m2"><p>آهش به مه رسیده اشکش به ماهی ای دوست</p></div></div>