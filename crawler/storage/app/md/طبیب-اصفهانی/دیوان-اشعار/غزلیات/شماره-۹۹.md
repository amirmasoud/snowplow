---
title: >-
    شمارهٔ ۹۹
---
# شمارهٔ ۹۹

<div class="b" id="bn1"><div class="m1"><p>سر منزل سلمی که منم دل نگرانش</p></div>
<div class="m2"><p>از رشگ نخواهم که بیابند نشانش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شوخی که منم زخمی تیری ز کمانش</p></div>
<div class="m2"><p>فتراک ز مرغان حرم گشته گرانش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بودیم بره منتظرش عمری و غافل</p></div>
<div class="m2"><p>بگذشت و دریغا نگرفتیم عنانش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آن چشم که یکبار برویم نگشودی</p></div>
<div class="m2"><p>بینم بچه سان بر رخ غیری نگرانش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آن گوش که یکبار ندادی بفغانم</p></div>
<div class="m2"><p>تا چند توان داد بحرف دگرانش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از عشوه پیداش بیابند حریفان</p></div>
<div class="m2"><p>کن ذوق که یابم ز نظرهای نهانش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مخصوص منش هست نهان لطفی و ایکاش</p></div>
<div class="m2"><p>اغیار ندانند بمن لطف نهانش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در باغ جهان نیست نهالی که نباشد</p></div>
<div class="m2"><p>با نخل برومند تو پیوند نهانش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آواره از آن بادیه ام من که فتادست</p></div>
<div class="m2"><p>چون ریگ روان بر سر هم تشنه لبانش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خوش باش که رفته است طبیب آنقدر ازخویش</p></div>
<div class="m2"><p>کز دل نرسد حرف شکایت بزبانش</p></div></div>