---
title: >-
    شمارهٔ ۱۰۰
---
# شمارهٔ ۱۰۰

<div class="b" id="bn1"><div class="m1"><p>زد مرا زخمی و از پیش نظر بگذشت حیف</p></div>
<div class="m2"><p>نازده بر سینه‌ام زخم دگر، بگذشت حیف</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کشتی ما را که عمری بود جویای نهنگ</p></div>
<div class="m2"><p>بر کنار افکند موج و از خطر بگذشت حیف</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفتم از باغ تو چینم میوه‌ای، تا در گشود</p></div>
<div class="m2"><p>باغبان بر روی من وقت ثمر بگذشت حیف</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کار خود را چاره از آه سحر جویند خلق</p></div>
<div class="m2"><p>چاره کار من از آه سحر بگذشت حیف</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از هجوم خار در گلشن ز بس جا تنگ گشت</p></div>
<div class="m2"><p>عندلیب از وصل گل با چشم تر بگذشت حیف</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بعد عمری از پی پرسش طبیب خسته را</p></div>
<div class="m2"><p>گرچه یار آمد به سر، زآن پیشتر بگذشت حیف</p></div></div>