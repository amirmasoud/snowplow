---
title: >-
    شمارهٔ ۱۱۸
---
# شمارهٔ ۱۱۸

<div class="b" id="bn1"><div class="m1"><p>خوش آن خلوت که چون آیی به روی غیر در بندم</p></div>
<div class="m2"><p>تو بگشایی میان و من پی خدمت کمر بندم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نگاری کز رخش یک لحظه نتوانم نظر بندم</p></div>
<div class="m2"><p>نمی‌دانم چه سان از کوی او رخت سفر بندم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چه طرفی زآشیان بستند مرغان تا درین گلشن</p></div>
<div class="m2"><p>روم من آشیان تازه‌ای بر یکدگر بندم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز دهقانی که چشم تربیت دارم چه حالست این</p></div>
<div class="m2"><p>که نخلم را فکند از پای تا رفتم ثمر بندم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دلت از ناله‌ام گر با ترحم آشنا گردد</p></div>
<div class="m2"><p>اشارت کن که چون نی بهر نالیدن کمر بندم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به نالیدن خوشم ورنه مرا کاری نمی‌باشد</p></div>
<div class="m2"><p>از آن هرشب در کاشانه بر روی اثر بندم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>طبیب این لازم عشقست کان بیدادگر با من</p></div>
<div class="m2"><p>کند هرچند جور افزون بر او دل بیشتر بندم</p></div></div>