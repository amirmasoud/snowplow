---
title: >-
    شمارهٔ ۱۰۳
---
# شمارهٔ ۱۰۳

<div class="b" id="bn1"><div class="m1"><p>چه خواهد شد اگر سلطان دهد گوشی به فرمانم</p></div>
<div class="m2"><p>که عمری شد که من بر درگهش از داد خواهانم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ملک آسوده در خلوت چه می‌داند چه می‌آید</p></div>
<div class="m2"><p>زاستغنای دربان و تغافل‌های خاصانم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در آن گلشن که هرکس گل به دامن می‌رود آنجا</p></div>
<div class="m2"><p>سرت گردم، چرا دادی به دست خار دامانم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دریغ از من مدارای ابر رحمت رشحه فیضی</p></div>
<div class="m2"><p>بود روزی گل امید گردد خار حرمانم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به آن چشمی که می‌باید به زاغ کج‌نوا بینی</p></div>
<div class="m2"><p>مبین سویم که من خوش نغمه مرغ این گلستانم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مرنجان دل اگر خندان مرا در انجمن بینی</p></div>
<div class="m2"><p>اگر در ظاهرم خندان ولی در پرده گریانم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بیا ای بی‌وفا یک ره به خاک من گذاری کن</p></div>
<div class="m2"><p>چو اکنون از جفا کردی به خاک راه یکسانم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>درون سینه‌ام جا کرده از بس شور عشق او</p></div>
<div class="m2"><p>فرو ریزد به سان شمع آتش در گریبانم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مده پندم طبیب اکنون که عشقم دل ربود از کف</p></div>
<div class="m2"><p>دریغا رفت آن عهدی که دل بودی به فرمانم</p></div></div>