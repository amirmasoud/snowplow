---
title: >-
    شمارهٔ ۵۲
---
# شمارهٔ ۵۲

<div class="b" id="bn1"><div class="m1"><p>دو هفته شد که زمن یار سرگران دارد</p></div>
<div class="m2"><p>بطاقتی که ندارم مگر گمان دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نگاه گرم برویت که می تواند کرد</p></div>
<div class="m2"><p>چنین که روی ترا شرم در میان دارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر از بهشت برین دور داردم غم نیست</p></div>
<div class="m2"><p>مباد دورم از آن خاک آستان دارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بیا بشکور ما ای که عافیت خواهی</p></div>
<div class="m2"><p>دیار ما نه زمین و نه آسمان دارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بحیرتم که ز وادی گذشت یار و طبیب</p></div>
<div class="m2"><p>هنوز چشم بدنبال کاروان دارد</p></div></div>