---
title: >-
    شمارهٔ ۷۸
---
# شمارهٔ ۷۸

<div class="b" id="bn1"><div class="m1"><p>مرغی که بکوی تو ز پرواز نشیند</p></div>
<div class="m2"><p>از جور تو هر چند رمد باز نشیند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شد یار و درآمد ز درم غیر و روانیست</p></div>
<div class="m2"><p>جغد آید و در منزل شهباز نشیند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مرغ دل ما از قفس سینه پریده است</p></div>
<div class="m2"><p>تا بر لب بام که ز پرواز نشیند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دیرند اسیران تو ناکام بدامت</p></div>
<div class="m2"><p>رحمست بصیدی که ز آغاز نشیند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شد شمع بسی کشته و آتشکده خاموش</p></div>
<div class="m2"><p>کی آتش ما سوختگان باز نشیند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا چند طبیب از غم بیگانه پریشان</p></div>
<div class="m2"><p>وقتست که در انجمن راز نشیند</p></div></div>