---
title: >-
    شمارهٔ ۱۰۷
---
# شمارهٔ ۱۰۷

<div class="b" id="bn1"><div class="m1"><p>صبح محشر که من از خواب گران برخیزم</p></div>
<div class="m2"><p>بود آیا که برویت نگران برخیزم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بس ملولم ز جهان بلبل خوش نغمه کجاست</p></div>
<div class="m2"><p>کز سر هر دو جهان دست فشان برخیزم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از وصالم چه تمتع ز تو ای آفت جان</p></div>
<div class="m2"><p>تا نشینی بکنارم ز میان برخیزم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آه از آن لحظه که در بزم نشینی تو و من</p></div>
<div class="m2"><p>خیزم از انجمن و دل نگران برخیزم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گل دو روزیست، همان به که ازین طرف چمن</p></div>
<div class="m2"><p>پیش از آن دم که وزد باد خزان برخیزم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر طرف مینگرم بی خبرانند طبیب</p></div>
<div class="m2"><p>به که از محفل این بیخبران برخیزم</p></div></div>