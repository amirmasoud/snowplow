---
title: >-
    شمارهٔ ۶۵
---
# شمارهٔ ۶۵

<div class="b" id="bn1"><div class="m1"><p>هرکه از خون جگر چون لاله ساغر می‌کشد</p></div>
<div class="m2"><p>منت احسان کی از چرخ ستمگر می‌کشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زآستان بی‌نیازی تا کف خاکی به جاست</p></div>
<div class="m2"><p>کی سر ما خاکساران ناز افسر می‌کشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>می‌رود گرد یتیمی، کی بشستن از گهر؟</p></div>
<div class="m2"><p>منت خشکی دلم از دیدهٔ تر می‌کشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عزت دنیا هم‌آغوش است با حسن سلوک</p></div>
<div class="m2"><p>رشته هموار سر از جیب گوهر می‌کشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با ضعیفان دشمنی، دارد خطرها در کمین</p></div>
<div class="m2"><p>انتقام شمع را از شعله، صرصر می‌کشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>منت صیقل مرا بر دل گران آمد طبیب</p></div>
<div class="m2"><p>زنگ را آیینه من سنگ در بر می‌کشد</p></div></div>