---
title: >-
    شمارهٔ ۴۵ - نوایی
---
# شمارهٔ ۴۵ - نوایی

<div class="b" id="bn1"><div class="m1"><p>غمش در نهانخانهٔ دل نشیند</p></div>
<div class="m2"><p>بنازی که لیلی به محمل نشیند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به دنبال محمل چنان زار گریم</p></div>
<div class="m2"><p>که از گریه‌ام ناقه در گل نشیند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خلد گر به پا خاری آسان بر آرم</p></div>
<div class="m2"><p>چه سازم به خاری که در دل نشیند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پی ناقه‌اش رفتم آهسته ترسم</p></div>
<div class="m2"><p>غباری به دامان محمل نشیند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مرنجان دلم را که این مرغ وحشی</p></div>
<div class="m2"><p>ز بامی که برخاست مشکل نشیند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عجب نیست خندد اگر گل به سروی</p></div>
<div class="m2"><p>که در این چمن پای در گل نشیند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بنازم به بزم محبت که آنجا</p></div>
<div class="m2"><p>گدایی به شاهی مقابل نشیند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>طبیب از طلب در دو گیتی میاسا</p></div>
<div class="m2"><p>کسی چون میان دو منزل نشیند؟</p></div></div>