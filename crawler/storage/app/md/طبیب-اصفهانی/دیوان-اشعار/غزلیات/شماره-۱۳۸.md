---
title: >-
    شمارهٔ ۱۳۸
---
# شمارهٔ ۱۳۸

<div class="b" id="bn1"><div class="m1"><p>از بَرَت کی من به این الفت جدا خواهم شدن</p></div>
<div class="m2"><p>من تن و تو جان جدا از جان کجا خواهم شدن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر تو بوی پیرهن داری ز مشتاقان دریغ</p></div>
<div class="m2"><p>از پی دریوزه در پیش صبا خواهم شدن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>وقت آن آمد که گیرم گوشه‌ای از همدمان</p></div>
<div class="m2"><p>بس که دیدم بی‌وفایی بی‌وفا خواهم شدن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر بری کآورد نخلم هستیم بر خاک ریخت</p></div>
<div class="m2"><p>من چه دانستم که بی‌برگ و نوا خواهم شدن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>می‌تپد مرغ دلم در سینه چون بسمل طبیب</p></div>
<div class="m2"><p>غالبا در دام عشقی مبتلا خواهم شدن</p></div></div>