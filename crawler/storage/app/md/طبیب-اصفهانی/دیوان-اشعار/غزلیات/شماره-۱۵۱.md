---
title: >-
    شمارهٔ ۱۵۱
---
# شمارهٔ ۱۵۱

<div class="b" id="bn1"><div class="m1"><p>به صید جسته از دامی چه خوش می‌گفت صیّادی</p></div>
<div class="m2"><p>که از دام علایق گر توانی جست، آزادی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو با بیگانگان بنشین به عشرت کز غم آزادی</p></div>
<div class="m2"><p>که با غم آشنایان را نباشد خاطر شادی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من آن روزی ز شهد عشق شیرین‌کام گردیدم</p></div>
<div class="m2"><p>که در این بیستون نه خسروی بود و نه فرهادی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به حرمان دل حسرت نصیبم گو بهار آمد</p></div>
<div class="m2"><p>به هر گلشن که دیدم قمریی یا سرو آزادی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز هجر عافیت دشمن، به گردون رفت فریادم</p></div>
<div class="m2"><p>تو بی‌پروا نشد یک شب دهی گوشی به فریادی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز لعلت خواستم کامی کنم حاصل ندانستم</p></div>
<div class="m2"><p>که دارد در میان چشمت ز مژگان تیغ بیدادی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>حیاتی در گذر دارم وداعی در نظر دارم</p></div>
<div class="m2"><p>فغانی با اثر دارم تو هم ای گریه امدادی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چه سود اوراق عمر تو طبیب از خنده غفلت</p></div>
<div class="m2"><p>درین گلشن به رنگ گل که بر باد فنا دادی</p></div></div>