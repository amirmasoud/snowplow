---
title: >-
    شمارهٔ ۲۷
---
# شمارهٔ ۲۷

<div class="b" id="bn1"><div class="m1"><p>کاروان عشق را بانگ درای دیگرست</p></div>
<div class="m2"><p>گوش ما بر ناله دردآشنای دیگرست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر دل تنگم در فیضی است هر زخم ستم</p></div>
<div class="m2"><p>بر تنم هر داغ باغ دلگشای دیگرست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شمع ما را از نسیم صبحدم اندیشه نیست</p></div>
<div class="m2"><p>روشنائی، محفل ما را ز جای دیگرست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زندگی بی آه و اشگم نیست ممکن همچو شمع</p></div>
<div class="m2"><p>در دیار عاشقان آب و هوای دیگرست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>صید لاغر را کنند آزاد، حیرانم چرا</p></div>
<div class="m2"><p>هر قدم در راه من دام بلای دیگرست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر چه میکاهد زتن، بر روح افزاید طبیب</p></div>
<div class="m2"><p>در جهان نیستی نشو و نمای دیگرست</p></div></div>