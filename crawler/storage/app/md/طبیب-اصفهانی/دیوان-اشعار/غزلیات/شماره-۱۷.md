---
title: >-
    شمارهٔ ۱۷
---
# شمارهٔ ۱۷

<div class="b" id="bn1"><div class="m1"><p>از غم لیلی به وادی گرچه مجنون می‌گریست</p></div>
<div class="m2"><p>گر رموز عشق دانی لیل افزون می‌گریست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رفته در محفل سخن از آتشین‌رویی که دوش</p></div>
<div class="m2"><p>شمع را دیدم که از اندازه بیرون می‌گریست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خون از چشم آشنا می‌ریخت در بزم وصال</p></div>
<div class="m2"><p>وای بر بیگانه کآنجا آشنا خون می‌گریست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آه دردآلود را در دل نهفتم شام هجر</p></div>
<div class="m2"><p>آسمان از بس که از بیم شبیخون می‌گریست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دیده خونبار ما بود آنکه در محفل طبیب</p></div>
<div class="m2"><p>هر زمان در حسرت آن لعل میگون می‌گریست</p></div></div>