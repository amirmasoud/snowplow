---
title: >-
    شمارهٔ ۱۳۴
---
# شمارهٔ ۱۳۴

<div class="b" id="bn1"><div class="m1"><p>خفتن نتوان درین گلستان</p></div>
<div class="m2"><p>از ناله شب نخفته مرغان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای شب نه غم منی خدا را</p></div>
<div class="m2"><p>تا چند نمی‌رسی به پایان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من مانده و همرهان روانه</p></div>
<div class="m2"><p>من خفته و کاروان شتابان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هستم ز تو من به جان خریدار</p></div>
<div class="m2"><p>دردی که نمی‌رسد به درمان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جویند و چه سود چون نیابند</p></div>
<div class="m2"><p>روزی که شوم ز دیده پنهان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>من گریه‌کنان نشسته غمگین</p></div>
<div class="m2"><p>تو خنده‌زنان گذشته شادان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دردی دارم طبیب کآن را</p></div>
<div class="m2"><p>نتْوان گفت و نهفت نتوان</p></div></div>