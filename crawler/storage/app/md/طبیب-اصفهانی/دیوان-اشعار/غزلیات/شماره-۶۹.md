---
title: >-
    شمارهٔ ۶۹
---
# شمارهٔ ۶۹

<div class="b" id="bn1"><div class="m1"><p>گو روزگار هرچه تواند به ما کند</p></div>
<div class="m2"><p>ما و تو را مباد که از هم جدا کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مرغ شکسته بالم و صیاد بی‌وفا</p></div>
<div class="m2"><p>ترسم به این بهانه ز دامم رها کند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آزار ما بسست، که خود را بخون کشد</p></div>
<div class="m2"><p>کاوش کسی که با دل مجروح ما کند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گستاخ می وزد بحریم چمن رواست</p></div>
<div class="m2"><p>گر عندلیب شکوه زباد صبا کند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در هجر اگر طبیب شبی روز کرده ای</p></div>
<div class="m2"><p>دانی شب فراق به روزم چه‌ها کند</p></div></div>