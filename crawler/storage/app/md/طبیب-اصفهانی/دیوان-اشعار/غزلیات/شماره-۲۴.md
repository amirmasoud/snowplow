---
title: >-
    شمارهٔ ۲۴
---
# شمارهٔ ۲۴

<div class="b" id="bn1"><div class="m1"><p>برگیر مهر از آنکه بکام دل تو نیست</p></div>
<div class="m2"><p>برکن دل از کسی که دلش مایل تو نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا چند گوئیم که بخوبان مبند دل</p></div>
<div class="m2"><p>ناصح ترا چکار، دل من دل تو نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دادی نویدی وصلم و خرسند نیستم</p></div>
<div class="m2"><p>با یکدیگر یکی، چو، زبان و دل تو نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ره در دلش که سخت تر از سنگ خاره است</p></div>
<div class="m2"><p>ای دیده غیر گریه بی حاصل تو نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفتی که نیست جای، کسی را، بمحفلم</p></div>
<div class="m2"><p>غیر از طبیب جای که در محفل تو نیست؟</p></div></div>