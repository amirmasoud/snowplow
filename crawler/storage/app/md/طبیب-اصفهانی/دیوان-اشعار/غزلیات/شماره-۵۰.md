---
title: >-
    شمارهٔ ۵۰
---
# شمارهٔ ۵۰

<div class="b" id="bn1"><div class="m1"><p>بمن از ناز نگاهش نگرید</p></div>
<div class="m2"><p>نگه گاه بگاهش نگرید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رخ او چون گل وخطش چو گیاه</p></div>
<div class="m2"><p>گل این باغ و گیاهش نگرید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>لشگر انگیخته عشقم از اشگ</p></div>
<div class="m2"><p>دشت پیمای سپاهش نگرید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از ره آتش نفسی می آید</p></div>
<div class="m2"><p>شعله آتش و آهش نگرید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بهلالی چه گشائید نظر؟</p></div>
<div class="m2"><p>شکن طرف کلاهش نگرید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سر عشاق بسی بر سر هم</p></div>
<div class="m2"><p>ریخته بر سر راهش نگرید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>همه دم گرم فغانست طبیب</p></div>
<div class="m2"><p>اثر ناله و آهش نگرید</p></div></div>