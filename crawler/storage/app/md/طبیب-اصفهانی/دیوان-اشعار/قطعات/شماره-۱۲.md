---
title: >-
    شمارهٔ ۱۲
---
# شمارهٔ ۱۲

<div class="b" id="bn1"><div class="m1"><p>ندهی گوش خود به فریادم</p></div>
<div class="m2"><p>یا به گوشَت نمی‌رسد دادم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو چو لیلی و من چو مجنونم</p></div>
<div class="m2"><p>تو چو شیرین و من چو فرهادم</p></div></div>