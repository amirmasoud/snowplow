---
title: >-
    ساقی نامه
---
# ساقی نامه

<div class="b" id="bn1"><div class="m1"><p>بیا ساقی دلم آشفته تست</p></div>
<div class="m2"><p>درین میخانه گفته گفته تست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بده جامی که بس حالم خرابست</p></div>
<div class="m2"><p>دل اندوهناکم در عذابست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>طبیبا چون ترا طی شد جوانی</p></div>
<div class="m2"><p>گذشت ایام عیش وکامرانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کنون اندیشه کن وقتست وقتست</p></div>
<div class="m2"><p>خموشی پیشه کن وقتست وقتست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بکنج بی کسی رو با دل خوش</p></div>
<div class="m2"><p>که کنج بی کسی جائیست دلکش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بسا محفل بسا مجلس که دیدی</p></div>
<div class="m2"><p>بسا کز جام عشرت می کشیدی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کناری رو چو تیرت از کمان جست</p></div>
<div class="m2"><p>خجل منشین شکارت چون شد از دست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گریزان شو زمشت جاهلی چند</p></div>
<div class="m2"><p>بده دستی بدست کاهلی چند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>که شاید عاقبت کامت برآید</p></div>
<div class="m2"><p>میان کاملان نامت برآید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زتنهائی دلت آشفته تا کی</p></div>
<div class="m2"><p>رفیقان رفته و تو خفته تا کی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>درین وادی مکن خوابی و برخیز</p></div>
<div class="m2"><p>ازین چشمه بکش آبی و برخیز</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>اگر گردی چو اختر گرد آفاق</p></div>
<div class="m2"><p>شوی تا بر سر این نیلوفری طاق</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>درون پرده راه جستجو نیست</p></div>
<div class="m2"><p>همه اسرار و اذن گفتگو نیست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بود اسرار پنهانت شود حل</p></div>
<div class="m2"><p>برآئی چون برین خاکستری تل</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>خداوندا «طبیب » منفعل را</p></div>
<div class="m2"><p>اسیر خاکدان آب و گل را</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>از این زندان غم آزادیش بخش</p></div>
<div class="m2"><p>درین ویران ره آبادیش بخش</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>منم چون لاله در هامون نشسته</p></div>
<div class="m2"><p>بخاک افتاده و در خون نشسته</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>به بخت خود چو مجنون مانده در جنگ</p></div>
<div class="m2"><p>نشسته تا کمر چون کوه در سنگ</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>نمی بینم درین صحرای اندوه</p></div>
<div class="m2"><p>هم آوازی که با ما خاست جز کوه</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ولی او هم هم آوازی چه داند</p></div>
<div class="m2"><p>جمادی رسم دمسازی چه داند</p></div></div>