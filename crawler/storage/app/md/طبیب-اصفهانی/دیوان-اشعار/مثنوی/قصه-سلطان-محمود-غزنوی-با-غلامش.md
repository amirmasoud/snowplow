---
title: >-
    قصه سلطان محمود غزنوی با غلامش
---
# قصه سلطان محمود غزنوی با غلامش

<div class="b" id="bn1"><div class="m1"><p>شنیدم من که محمود جوانبخت</p></div>
<div class="m2"><p>که بودش در جهان هم تاج و هم تخت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نه غزنینش همی زیر نگین بود</p></div>
<div class="m2"><p>که سلطان همه روی زمین بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>غلامی داشت از خاصان و نامش</p></div>
<div class="m2"><p>ایاز و جمله خاصان غلامش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>غلامی بر جوانی دلربائی</p></div>
<div class="m2"><p>هوس بیگانه ای عشق آشنائی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نه تنها بود سلطان بنده او</p></div>
<div class="m2"><p>بسا سلطان بخاک افکنده او</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دل سلطان بعشقش گشت مایل</p></div>
<div class="m2"><p>دلست این و نیفتد کار با دل</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بهر کاریش بودی صد بهانه</p></div>
<div class="m2"><p>نبودی تا ایازش در میانه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سخن جزو صف آن سرچشمه نوش</p></div>
<div class="m2"><p>نگفتی و نکردی از کسی گوش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گهی از عارضش گفتی گه از رخ</p></div>
<div class="m2"><p>زهی چون اختر تابنده فرخ</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گهی از لعل و آن لعل قدح نوش</p></div>
<div class="m2"><p>گهی از گوش و آن در بناگوش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گهی از طره چون مشگ نابش</p></div>
<div class="m2"><p>گهی از چشم و چشم نیمخوابش</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>غرض در شهر شد آن راز گفته</p></div>
<div class="m2"><p>علم زد آتش در دل نهفته</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>همه خاصان شدند آگاه واز رشگ</p></div>
<div class="m2"><p>فرو می ریختند از دیده ها اشگ</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ازین غیرت گریبان چاک کردند</p></div>
<div class="m2"><p>وزین حسرت نشیمن خاک کردند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>که ما هم بنده درگاه شاهیم</p></div>
<div class="m2"><p>همه دیرینه دولتخواه شاهیم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چه شد آخر که شه با ما جفا کرد</p></div>
<div class="m2"><p>حقوق خدمت دیرین رها کرد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بما تا چند سلطان اینچنین است</p></div>
<div class="m2"><p>که گه با ما بخشم و گه بکین است</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>همان بهتر که از ما هوشیاری</p></div>
<div class="m2"><p>که گیرد شاه از حرفش شماری</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ز کار عشق آن ماه دو هفته</p></div>
<div class="m2"><p>ز سلطان باز پرسد رفته رفته</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ز خاصانش یکی دلداده از دست</p></div>
<div class="m2"><p>پی تقدیم این خدمت کمر بست</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>نخستین کرد سلطان را دعائی</p></div>
<div class="m2"><p>دعای با تظلم آشنائی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>براهت بخت یار و چرخ یاور</p></div>
<div class="m2"><p>بدستت گاه تیغ و گاه ساغر</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ز جور دهر جانت در امان باد</p></div>
<div class="m2"><p>دلت بازیردستان مهربان باد</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>باین گفتار چون شر را دعا کرد</p></div>
<div class="m2"><p>زمین بوسید و عرض مدعا کرد</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>که شاها در جهانت هست نامی</p></div>
<div class="m2"><p>چه می خواهی تو از عشق غلامی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>غلامی را کجا آن قدر و مقدار</p></div>
<div class="m2"><p>که باشد چون تو شاهی را سزاوار</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بسا می بینمت از خویش غافل</p></div>
<div class="m2"><p>نمی سوزد چرا بر دولتت دل</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>خردمندی زدی دوشینه فالی</p></div>
<div class="m2"><p>همانا اخترت دارد وبالی</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بجز حرف ایازت بر زبان نیست</p></div>
<div class="m2"><p>سخن از آنچه باید در میان نیست</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>دریغا بخت شه را خواب برده ست</p></div>
<div class="m2"><p>که پنداری جهان را آب برده ست</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>دریغا بخت سلطان خفته تا چند</p></div>
<div class="m2"><p>ازین غم خاطرش آشفته تا چند</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>ازو سلطان چو این پیغام بشنید</p></div>
<div class="m2"><p>چو این پیغام را هر شام بشنید</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>بسی گردید گریان و همی گفت</p></div>
<div class="m2"><p>میان گریه خندان و همی گفت</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>دریغا کار ما را با دل افتاد</p></div>
<div class="m2"><p>بدل افتاد کار و مشکل افتاد</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>دل محمود در دست ایازست</p></div>
<div class="m2"><p>که کار دل همه عجز و نیازست</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>مبادا از دلم پرسی که چونست</p></div>
<div class="m2"><p>که چون کاوش کنی دریای خونست</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>نمی دانم دلم را این چه حالست</p></div>
<div class="m2"><p>که غمناکست و در عین وصالست</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>دلم دردا بحال مشکلم سوخت</p></div>
<div class="m2"><p>که آتش نیست پیدا و دلم سوخت</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>بگو تا کی خرابم داری ای دل</p></div>
<div class="m2"><p>قرین اضطرابم داری ای دل</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>شبانم تیره و خواب مرا نه</p></div>
<div class="m2"><p>لبانم تشنه وآبی مرا نه</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>بخون گشته دلم رحمی که وقتست</p></div>
<div class="m2"><p>بکار مشکلم رحمی، که وقتست</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>ولی با آن صنم عشقم هوس نیست</p></div>
<div class="m2"><p>که می دانم هوس را عاقبت چیست</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>بغیر از عشق پاکم نیست منظور</p></div>
<div class="m2"><p>که چشم من مباد از روی او دور</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>ز خاک اوست پنداری گل من</p></div>
<div class="m2"><p>مبادا خالی از یادش دل من</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>مرا زنهار نشماری هوسناک</p></div>
<div class="m2"><p>که دست ماست چون دامان او پاک</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>بود اما چو مهرش پرتوافکن</p></div>
<div class="m2"><p>بغمازان شود این راز روشن</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>که نه از بادحسنش شدم مست</p></div>
<div class="m2"><p>دلم رفت از خرامیدنش از دست</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>شما را نیست در گوهر فروغی</p></div>
<div class="m2"><p>که پندارید می گویم دروغی</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>چو شه را جان ازین اندوه بگداخت</p></div>
<div class="m2"><p>بعزم دشت روزی خیمه افراخت</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>شکار افکن پی سیری و گشتی</p></div>
<div class="m2"><p>همی رفتند از دشتی بدشتی</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>همه چون دامن صحرا گرفتند</p></div>
<div class="m2"><p>بمستی نشئه از صهبا گرفتند</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>ز نزدیکان شد گفتا جوانی</p></div>
<div class="m2"><p>که می آید به چشمم کاروانی</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>بگفتا شه غلامان را که پویند</p></div>
<div class="m2"><p>ز حال کاروانی باز جویند</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>کزین آمد شدن، مقصودشان چیست</p></div>
<div class="m2"><p>ازین منزل بریدن سودشان چیست</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>پس از رفتن چو یک یک بازگشتند</p></div>
<div class="m2"><p>بخاصان دگر دمساز گشتند</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>بشه گفتند ایشان کاروانند</p></div>
<div class="m2"><p>که در اندیشه سود وزیانند</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>بما از آنچه باید باز گفتند</p></div>
<div class="m2"><p>نپنداریم رازی را نهفتند</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>گرفتیم آنچه می بایست در گوش</p></div>
<div class="m2"><p>دریغا گشت از خاطر فراموش</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>پس آنگه گفت سلطان کای ظریفان</p></div>
<div class="m2"><p>که با شاهید دیرینه حریفان</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>همه تیغ زبان بر من کشیدید</p></div>
<div class="m2"><p>چگویم زانچه گفتید و شنیدید</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>خبر گیرید ایاز اینجاست یا نه</p></div>
<div class="m2"><p>شکار افکن سوی صحراست یا نه</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>بگفتندش که اینجا حاضرست او</p></div>
<div class="m2"><p>پی خدمتگزاری ناظرست او</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>بگفتا پس ایاز نکته دان را</p></div>
<div class="m2"><p>ایاز نکته دان خوش بیان را</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>که رو تا کاروان و حالشان پرس</p></div>
<div class="m2"><p>زکار و بار نیکوفالشان پرس</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>بگو از آنچه باید گفت و گو کرد</p></div>
<div class="m2"><p>بجو از آنچه باید جستجو کرد</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>ایاز کاردان بس شادمان شد</p></div>
<div class="m2"><p>زمین بوسید و سوی کاروان شد</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>چو شد آنجا با کرامش فزودند</p></div>
<div class="m2"><p>به میر کاروانش ره نمودند</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>بشارت داد میرکاروان را</p></div>
<div class="m2"><p>نوازش کرد دیگر رهروان را</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>خبر پرسید از آغاز و انجام</p></div>
<div class="m2"><p>نخستین آنکه از مصرید یا شام</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>چه می پوئید و با که کار دارید</p></div>
<div class="m2"><p>چه می جوئید و چه دربار دارید</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>چو از این سرزمین بندید محمل</p></div>
<div class="m2"><p>کدامین شهر را سازید منزل</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>بگفتندش که ما از شهر چینیم</p></div>
<div class="m2"><p>زشهر چین و از آن سرزمینیم</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>متاع ما متاعی نیست لایق</p></div>
<div class="m2"><p>که باشد طبع شاهان را موافق</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>زمصر و شام ما را آگهی نیست</p></div>
<div class="m2"><p>متاع ما بجز دست تهی نیست</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>ازین جنبش اگر گیریم آرام</p></div>
<div class="m2"><p>بجز ایزد که می داند سرانجام</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>چو از این سرزمین رحلت نمائیم</p></div>
<div class="m2"><p>خدا داند کجا محمل گشائیم</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>غرض از آنچه باید باز پرسید</p></div>
<div class="m2"><p>خبر زآغاز و از انجام پرسید</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>چو شد کاروان و شاد برگشت</p></div>
<div class="m2"><p>خرابی رفته بود آباد برگشت</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>بگفتی آنچه باشد گفتنی بود</p></div>
<div class="m2"><p>بسفتی هر گهر کو سفتنی بود</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>پس آنگه شه در آوردش در آغوش</p></div>
<div class="m2"><p>هزارش بوسه دادی بر لب نوش</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>بگفت او را که بدخواهت خجل باد</p></div>
<div class="m2"><p>تو گر خون مرا ریزی بحل باد</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>ایاز، آن خسروی کش من غلامم</p></div>
<div class="m2"><p>به عشق او برآید کاش نامم</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>کزین پس چون زمن دستان نگارند</p></div>
<div class="m2"><p>یکی از عشقبازانم شمارند</p></div></div>