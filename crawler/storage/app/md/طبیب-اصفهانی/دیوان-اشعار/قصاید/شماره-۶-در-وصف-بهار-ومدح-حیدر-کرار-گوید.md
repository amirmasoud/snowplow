---
title: >-
    شمارهٔ ۶ - در وصف بهار ومدح حیدر کرار گوید
---
# شمارهٔ ۶ - در وصف بهار ومدح حیدر کرار گوید

<div class="b" id="bn1"><div class="m1"><p>مژده بلبل راکه آمد گل بباغ شاخسار</p></div>
<div class="m2"><p>شددگر صحن چمن چون محفل از رخسار یار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سبزه را افراخت قامت از نم فیض هوا</p></div>
<div class="m2"><p>لاله را افروخت عارض از دم گرم بهار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همچون نخل طور آتش می دمد از شاخ گل</p></div>
<div class="m2"><p>چون تنور نوح می جوشد زلال از چشمه سار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جوش گل بنگر که نتواند فراهم آورد</p></div>
<div class="m2"><p>بلبلی از بهر طرح آشیان یک مشت خار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بسکه طرف گلستان جوش طراوت می زند</p></div>
<div class="m2"><p>از زمین از سعی صرصر برنمی خیزد غبار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در چمن از فیض ترتیب هوا آسیب نیست</p></div>
<div class="m2"><p>دست گلچین را چو دامان تماشائی ز خار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای حریفان خنده کبک و نوای عندلیب</p></div>
<div class="m2"><p>بر فراز کوهسار و در نشیب مرغزار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>می کند ترغیب مستان را بگلگشت چمن</p></div>
<div class="m2"><p>می کند تکلیف، رندان را بسیر کوهسار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از نوای بلبلان مست در صحن چمن</p></div>
<div class="m2"><p>وزلقای شاهدان شوخ در شهر و دیار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>غم بخود لرزد چو از سیمای سلطان لشکری</p></div>
<div class="m2"><p>گل بخود بالد چو از غوغای لشکر شهریار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بلبل باغ و حریف دیر و هنگام صبوح</p></div>
<div class="m2"><p>اینک اینک کرده از مستی غزلخوانی شعار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>این بتوصیف شراب ارغوان در میکده</p></div>
<div class="m2"><p>آن بتعریف هوای بوستان در لاله زار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>وقت آن آمد که در صحن چمن گردند باز</p></div>
<div class="m2"><p>شادمان وبهره مند وتر دماغ و کامگار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بلبل از سیمای گل چون قمری از بالای سرو</p></div>
<div class="m2"><p>ساقی از مینای می چون عاشق از رخسار یار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>می کند مشاطه باد سحر آراسته</p></div>
<div class="m2"><p>نوعروسان گلستان را به این نقش و نگار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>تا ترا دانا کند از حکمت یزدان پاک</p></div>
<div class="m2"><p>تا ترا بینا کند بر قدرت پروردگار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ساحت گلزار در چشمم چو دختر خانه ای است</p></div>
<div class="m2"><p>روشنت گردد اگر از دیده بر درای غبار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>کز پی ارشاد مرغان چمن گسترده است</p></div>
<div class="m2"><p>حکم ایزد سرو از گلبن حصیر از خارزار</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>تا فشاند واعظ بلبل گلاب موعظه</p></div>
<div class="m2"><p>تا گشاید عابد سوسن زبان اعتذار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>مرحبا حکمت که در صحن چمن انداخته</p></div>
<div class="m2"><p>حبذا قدرت که در گلزار کرده آشکار</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>از شکوفه سبحه بهر خرقه پوش نسترن</p></div>
<div class="m2"><p>وزسمن سجاده بهر شبنم شب زنده دار</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بسکه بر خاک چمن روی تضرع سوده اند</p></div>
<div class="m2"><p>از هراس و بیم و خوف و وحشت پروردگار</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ارغوان بین لعل رنگ و یاسمن سجاده گون</p></div>
<div class="m2"><p>گل نگر خونین حبین سنبل نگرنیلی عذار</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>صبح و شام از حسرت او بر دهان انگشت سرو</p></div>
<div class="m2"><p>روز و شب در خدمت او بر کمر دست چنار</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>گاه می خندد ز حکمش برق تابان قاهقاه</p></div>
<div class="m2"><p>گاه می گرید زبیمش ابر نیسان زار زار</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>لاله سرخوش زمینای عطایش باده نوش</p></div>
<div class="m2"><p>نرگس مخمور از جام سخایش میگسار</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>سرزد از جوش بهار طبع رنگین مطلعی</p></div>
<div class="m2"><p>از نهال خامه ام ناهیدگل از شاخسار</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>تا نباشم در شمار بیغمان روزگار</p></div>
<div class="m2"><p>ای جگر آهی بکش ای چشم تر اشگی ببار</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>پیش ما قدری نباشد دیده بی اشگ را</p></div>
<div class="m2"><p>چون صدف شد بی گهر افتاد ز چشم اعتبار</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>گر رسد از اشگ گرم من بدریا قطره ای</p></div>
<div class="m2"><p>ورکشم در بوستان از سینه آهی شعله بار</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>هر حبابی بر لب دریا شود تبخاله ای</p></div>
<div class="m2"><p>بر رخ گلها کند هر شبنمی کار شرار</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>گریه دلتنگیم ترسم که از خاطر رود</p></div>
<div class="m2"><p>خنده ام از بسکه می آید باهل روزگار</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>صحبت ابنای دنیا پر مکرر گشته است</p></div>
<div class="m2"><p>وقت آنکس خوش که کنج عزلتی کرد اختیار</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>گوشه گیران از حوادث در حصار راحتند</p></div>
<div class="m2"><p>از خطر ایمن شود کشتی که آید بر کنار</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>بسکه با غم های عالم گرم الفت گشته ام</p></div>
<div class="m2"><p>نگذرد یاد سرورم در دل امیدوار</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>هر زمان در بحر غم از طالع برگشته ام</p></div>
<div class="m2"><p>موج سانم می زند بر سینه دشت و کنار</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>تیره بختی را تماشا کن که در آغاز عشق</p></div>
<div class="m2"><p>روزگارم کرد از کین مبتلا ی هجر یار</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>تا بکی باشم براه وعده ات در انتظار</p></div>
<div class="m2"><p>ای ترا با عاشقان دایم فراموشی شعار</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>داغها دارم بدل از جور هجرت دور نیست</p></div>
<div class="m2"><p>سر زند گر تا بحشرم لاله از خاک مزار</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>بی گل روی تو از بس چون خزان افسرده ام</p></div>
<div class="m2"><p>خار در چشمم خلد از جلوه فصل بهار</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>دست بیتابی مبادا خار دامانت شود</p></div>
<div class="m2"><p>همچو گل برچیده دامان بر مزارم کن گذار</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>چشم من چون دیده روزن نمی آید بهم</p></div>
<div class="m2"><p>بسکه حیران مانده از شوقت براه انتظار</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>کی زیادت می توانم رفت از تأثیر ضعف</p></div>
<div class="m2"><p>ناتوانی های من آید مرا آخر بکار</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>ای که چشم سرمه سایت آخته تیغ ستم</p></div>
<div class="m2"><p>وی که حسن نیمرنگت ریخته خون بهار</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>من ندیدم در جهان از غمزه ات خونریزتر</p></div>
<div class="m2"><p>جز گه رزم عدو بر دست حیدر ذوالفقار</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>ای بعهد خسرو عدلت جفا بی دسترس</p></div>
<div class="m2"><p>وی بدور شحنه حزمت ستم ناپایدار</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>جو دعامت هر کجا برقع گشاید از جبین</p></div>
<div class="m2"><p>ابر دریا دل فشاند آب خجلت از عذار</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>چون سموم خشم تو بر عرصه هیجا رود</p></div>
<div class="m2"><p>چون شمیم خلق تو در بزم گردد مشگبار</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>آن کند با دوستان مهرت که با گلشن نسیم</p></div>
<div class="m2"><p>و آن کند با دشمنان قهرت که صرصر با غبار</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>اوج عرش و ذات تو چون یوسفست و قعر چاه</p></div>
<div class="m2"><p>سطح ارض و شخص تو پیغمبر و در جوف غار</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>کاسه دریوزه در کف بحر گیرد از صدف</p></div>
<div class="m2"><p>گاه احسان چون شود ابر کفت گوهر نثار</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>جود در طبعت مکین همچون جواهر در جبال</p></div>
<div class="m2"><p>فیض با ذاتت قرین همچون لآلی در بحار</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>گرچه باشد اختیار عالمی در دست تو</p></div>
<div class="m2"><p>دست زرپاش تو در ریزش بود بی اختیار</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>گر نباشد آسمان در جستجویت پس چرا</p></div>
<div class="m2"><p>چون دل عاشق زتاب هجر باشد بیقرار</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>ای سرافرازی که نعل مرکبت را می سزد</p></div>
<div class="m2"><p>گر هلال آسانماید چرخ زیب گوشوار</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>چون نماید در مدیحش نکته سنجی خامه ام</p></div>
<div class="m2"><p>مرکبی کورابود چون حیدر صفدر سوار</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>لوحش الله گرم جولانی که باشد برق تاز</p></div>
<div class="m2"><p>همچو آهی کز دل تنگی جهد بی اختیار</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>بسکه باشد سخت پی گرپا بیفشارد بکوه</p></div>
<div class="m2"><p>باز می ماند بسنگ خاره اش راه گذار</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>چار خصلت رایض تقدیر آورد از ازل</p></div>
<div class="m2"><p>گشت او را از پی کسب هنر آموزگار</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>جلدی کوشش ز سیل و خوش عنانی از نسیم</p></div>
<div class="m2"><p>تندی پویش ز برق و گرم تازی از شرار</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>جای آن دارد که از مهر آورد خنگ سپهر</p></div>
<div class="m2"><p>چون عنان در گردنش دست و چو تنگش در کنار</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>کی بتوصیف جلالت می توان پرداختن</p></div>
<div class="m2"><p>ای که اوصافت نیاید همچون انجم در شمار</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>نزد دانای خرد پرور عجب نبود که بود</p></div>
<div class="m2"><p>مدعی را در خلافت با وجودت اقتدار</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>کآسمان را سعد و نحسست و زمین را لعل و سنگ</p></div>
<div class="m2"><p>در چمن خارست و گل، در انجمن اغیار و یار</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>بی سخن بر نقص او را کش گواهی می دهد</p></div>
<div class="m2"><p>گر طرف سازد صدف را کش بدر شاهوار</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>یا که خورشید درخشان را بسنجد باسها</p></div>
<div class="m2"><p>یا ز سیم قلب کوبد در ترازوی عیار</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>مانده ام تا از درت محروم ای بحر کرم</p></div>
<div class="m2"><p>گشته ام تا از برت مهجور ای کوه وقار</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>چون شفق در خون تپیده چون گلم خونین جگر</p></div>
<div class="m2"><p>چون هلالم قد خمیده چون سحابم اشگبار</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>چون درآید در ضمیرم باد طوف مرقدت</p></div>
<div class="m2"><p>می برم فیضی که عاشق می برد از وصل یار</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>آستانت را که باشد مهبط انوار قدس</p></div>
<div class="m2"><p>بوسه ها دارم نیاز و سجده ها دارم نثار</p></div></div>