---
title: >-
    شمارهٔ ۱۰ - در بیوفائی روزگار و مدیحه رسول مختار گوید
---
# شمارهٔ ۱۰ - در بیوفائی روزگار و مدیحه رسول مختار گوید

<div class="b" id="bn1"><div class="m1"><p>ای مبارک همای فرخ فال</p></div>
<div class="m2"><p>مرحبا مرحبا تعال تعال</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از کجا می رسی بگوی بگوی</p></div>
<div class="m2"><p>بکجا می روی بنال بنال</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا مر از آن نوا بسوزد دل</p></div>
<div class="m2"><p>تا مرا ز آن صدا بگردد حال</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>لذتی می برم زبانگ تو من</p></div>
<div class="m2"><p>همچون پیغمبر از صدای بلال</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پیکرم کاست کاست آین القوم</p></div>
<div class="m2"><p>جگرم سوخت سوخت کیف الحال</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آیدم گریه و کنم گریه</p></div>
<div class="m2"><p>گاه بر ربع و گاه بر اطلال</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آن همه سنبل و گل وریحان</p></div>
<div class="m2"><p>هست بر جای یا که شد پامال؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>می چکد باز ژاله بر لاله</p></div>
<div class="m2"><p>می وزد باز آن نسیم شمال؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جایگه کرده اند بر سر سرو</p></div>
<div class="m2"><p>آن نکو قمریان خوش پر و بال؟</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آشیان بسته اند بر گلبن</p></div>
<div class="m2"><p>آن نکو بلبلان خوش خط و خال؟</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>می شود ابرو می زند باران</p></div>
<div class="m2"><p>بر رخ سبزه و بشاخ نهال؟</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>زده اند آن خیام نیلی گون</p></div>
<div class="m2"><p>بر سر چشمه های آب زلال؟</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>آن نکو دختران مهر گسل</p></div>
<div class="m2"><p>و آن پری پیکران فارغبال</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>طاق ابرویشان خمیده کمان</p></div>
<div class="m2"><p>چشم جادویشان رمیده غزال</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>هر یکی در کمال چون عذرا</p></div>
<div class="m2"><p>هر یکی در جمال چون ابسال</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>همچو لیلی همه به ناز و نیاز</p></div>
<div class="m2"><p>همچو سلمی همه بغنج و دلال</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>می روند و دو چشم در ابرو</p></div>
<div class="m2"><p>می دوند و دو زلف در دنبال</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گوششان رنجه گشتی از حلقه</p></div>
<div class="m2"><p>ساقشان سوده گشتی از خلخال</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>در مهادند با ظهور جیاد</p></div>
<div class="m2"><p>در خیامند با سنام جمال؟</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بطن وادی بود همان منزل</p></div>
<div class="m2"><p>یا نمودند زان محل ارحال؟</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>زاهل جودی بگو و آن دولت</p></div>
<div class="m2"><p>زاهل بطحا بگو و آن اقبال!</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چه شد آنان که رفتشان ثروت</p></div>
<div class="m2"><p>چه شد آنان که رفتشان اموال؟</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>آن همه خیل تند پویه جیاد</p></div>
<div class="m2"><p>وان همه خیل کوهکو به جمال</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>آن مناظر کجا و آن نکهت</p></div>
<div class="m2"><p>آن مناکح کجا و آن اجمال</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>آن همه فرشهای گوناگون</p></div>
<div class="m2"><p>و آن همه ظرفهای مالامال</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>چون به بینم که کاش بودم کور</p></div>
<div class="m2"><p>چون بگویم که کاش بودم لال؟</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>جای آن فرشها سیاه گلیم</p></div>
<div class="m2"><p>جای آن ظرفها شکسته سفال</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>چه شدند آن حواری و غلمان</p></div>
<div class="m2"><p>چه شدند آن شیوخ و آن اطفال؟</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>آه از آن شیوخ دانا دل</p></div>
<div class="m2"><p>در کارامات جمله چون ابدال</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>هر یکی خسروی بگاه کرم</p></div>
<div class="m2"><p>هر یکی حاتمی بوقت نوال</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>آه از آن کودکان در در گوش</p></div>
<div class="m2"><p>هر یکی صاحب کمال و جمال</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>خم گیسوی هر یکی چو کمند</p></div>
<div class="m2"><p>طاق ابروی هر یکی چو غزال</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>خبرت هست هیچ از آن خوبان</p></div>
<div class="m2"><p>خبرت هست هیچ از آن ابطال؟</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>آگهی ز آن غیوث و آن امطار</p></div>
<div class="m2"><p>آگهی زآن لیوث و آن اشبال؟</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>هر یکی فارسی بروز نبرد</p></div>
<div class="m2"><p>هر یکی مالکی به یوم قتال</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>همه آلوده شان بسم خنجر</p></div>
<div class="m2"><p>همه آلوده شان بخون چنگال</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>یالقوم و هم حیاة القلب</p></div>
<div class="m2"><p>یا لقوم و هم قرار البال</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>مالکم من یجیرکم من خل</p></div>
<div class="m2"><p>مالکم من یوالکم من وال</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>کم لماقد سلبتم النسوان</p></div>
<div class="m2"><p>کم لماقد نبهتم الاموال</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>فارغا منکم فجعت شهور</p></div>
<div class="m2"><p>باکیا فیکم سهرت لیال</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>یا برید الحمی حماک الله</p></div>
<div class="m2"><p>چون بآن حی روی باستعجال</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>در پی عرض حال من بشتاب</p></div>
<div class="m2"><p>بر در خسرو خجسته فعال</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>برسان از منش سلام آنگه</p></div>
<div class="m2"><p>که نمایند قوم شد رحال</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>فخر کونین سید ثقلین</p></div>
<div class="m2"><p>مخزن جود و منبع افضال</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>آن امیری که نام او ز ازل</p></div>
<div class="m2"><p>احمد آمد زایزد متعال</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>لامع از جبهه اش بود دولت</p></div>
<div class="m2"><p>لایح از چهره اش بود اقبال</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>نبود در عطای او تقصیر</p></div>
<div class="m2"><p>نبود در سخای او اهمال</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>آمدی در برش جدی به بیان</p></div>
<div class="m2"><p>آمدی در کفش حصی بمقال</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>دیده پاک اوست درج حیا</p></div>
<div class="m2"><p>سینه صاف اوست بحر زلال</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>معبدش گاه در حریم حرم</p></div>
<div class="m2"><p>مسجدش گاه در کهوف جبال</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>روضه اوست قبله حاجات</p></div>
<div class="m2"><p>مرقد اوست کعبه آمال</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>انت غیث الندی الدی الأحسان</p></div>
<div class="m2"><p>انت بحر السخالدی الافضال</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>لیس فی بحر جودک المیزان</p></div>
<div class="m2"><p>لیس فی قدر بذلک المکیال</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>مرحبا آلک ذوو الرحمه</p></div>
<div class="m2"><p>حبذا صبحک ذووالافضال</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>دین تو ناسخ همه ادیان</p></div>
<div class="m2"><p>شرع تو کاشف حرام و حلال</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>شرع پیغمبران ماضی را</p></div>
<div class="m2"><p>بعث تو کرد در جهان ابطال</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>گر ترا کرد قادر بی چون</p></div>
<div class="m2"><p>آخرین پیمبران ارسال</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>آری آخر به شغل های خطیر</p></div>
<div class="m2"><p>بفرستند بهترین رجال</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>پور یعقوب یوسف صدیق</p></div>
<div class="m2"><p>با زلیخا چو شد درون حجال</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>نگشادی اگر نه نام تو بود</p></div>
<div class="m2"><p>زان جمال مصور آن اقفال</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>هر کجا با صحابه بنشینی</p></div>
<div class="m2"><p>از غم هر دو کون فارغبال</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>گه پی جستجوی صلح و صلاح</p></div>
<div class="m2"><p>گه پی گفتگوی جنگ و جدال</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>آورد عرش مسند از خورشید</p></div>
<div class="m2"><p>گسترد فرش جبرئیل از بال</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>هم ترا مروحه زبان ملک</p></div>
<div class="m2"><p>هم ترا مشربه زجام هلال</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>هم بیاور گهی بدست عنان</p></div>
<div class="m2"><p>هم درآور گهی بپای مغال</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>روزگاری شد ای رسول کریم</p></div>
<div class="m2"><p>که بخاک اندر پی چو آب زلال</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>نه ترا با کسی عتاب و خطاب</p></div>
<div class="m2"><p>نه ترا با کسی جواب و سؤال</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>گمرهانند جمله در تدلیس</p></div>
<div class="m2"><p>مشرکانند جمله در اضلال</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>اعتبر یا اخی لما فعلوا</p></div>
<div class="m2"><p>فی امورالوصی من اخلال</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>طرحوامانبیهسم قدنص</p></div>
<div class="m2"><p>نبدو امار سولهم قد قال</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>چند در خوابی ای مبارک پی</p></div>
<div class="m2"><p>چند در خوابی ای همایون فال</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>منبرت چند بی خطاب و خطیب</p></div>
<div class="m2"><p>مسجدت چند بی اذان بلال</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>سنبلت را کنون زگردبشوی</p></div>
<div class="m2"><p>نرگست را کنون بدست بمال</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>گلشنت را تهی کن از خاشاک</p></div>
<div class="m2"><p>مسجدت را تهی کن از آرذال</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>خطبه معدلت بخوان از نو</p></div>
<div class="m2"><p>عالمی کن ز عدل مالامال</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>تا که گردد زجنبش گردون</p></div>
<div class="m2"><p>گاه شادی عیان و گاه ملال</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>بدسگالت غمین بود شب و روز</p></div>
<div class="m2"><p>نیکخواه تو شاد درمه و سال</p></div></div>